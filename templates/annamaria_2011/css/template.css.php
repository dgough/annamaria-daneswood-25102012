<?php 

if (extension_loaded('zlib') && !ini_get('zlib.output_compression')) @ob_start('ob_gzhandler');
header('Content-type: text/css; charset: UTF-8');
header('Expires: ' . gmdate('D, d M Y H:i:s', time() + 3600) . ' GMT');

define('DS', DIRECTORY_SEPARATOR);
define('PATH_ROOT', dirname(__FILE__) . DS);

/* layout styling */
include(PATH_ROOT . 'reset.css');
include(PATH_ROOT . 'template_css.css');

/* ie browser */
if (array_key_exists('HTTP_USER_AGENT', $_SERVER)) {
	$is_ie7 = strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'msie 7') !== false;
	$is_ie6 = strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'msie 6') !== false;
	if ($is_ie7 || $is_ie6) include(PATH_ROOT . 'iehacks.css');
	if ($is_ie7) include(PATH_ROOT . 'ie7hacks.css');
	else if ($is_ie6) include(PATH_ROOT . 'ie6hacks.css');
}

?>