
var select_swap = new Class({

	initialize: function(element)
	{
		this.element = $(element);
		this.options = $('select_options');
		if(!this.options) return;
		
		this.element.addEvent('change', this.select.bind(this));
		this.hide();
	},
	
	hide: function(){
		this.options.getElements('.option').each(function(el){
			el.setStyle('display','none');
		})
	},
	
	select: function(){
		var value = this.element.getValue();
		
		this.hide();
		
		var option = this.options.getElement('#'+value);
		if(option){
			option.setStyle('display','block');
		}
	}
})