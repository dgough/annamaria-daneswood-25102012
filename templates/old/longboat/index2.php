<?php
/**
 * YOOtheme template
 *
 * @author yootheme.com
 * @copyright Copyright (C) 2007 YOOtheme Ltd & Co. KG. All rights reserved.
 */

// no direct access
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );
global $option;

/*** template parameters ***/
$template_parameters = array(
	/* javascript */
	"loadJavascript"      => true           /* true | false, set this to enable/disable all the templates javascripts */
);

include_once($mosConfig_absolute_path . '/templates/' . $cur_template . '/lib/php/yootools.php');
include_once($mosConfig_absolute_path . '/templates/' . $cur_template . '/lib/php/yoolayout.php');

$template_baseurl = $mosConfig_live_site . '/templates/' . $cur_template;

// add template javascript
if ($yootools->getParam('loadJavascript')) {
	$yootools->addScript($template_baseurl . '/lib/js/mootools.js.php');
	$yootools->addScript($yootools->getJavaScript());
	$yootools->addScript($template_baseurl . '/lib/js/template.js.php');
}

// add template css
$yootools->addStyleSheet($template_baseurl . '/css/template.css.php?color=' . $yootools->getParam('color')
															. '&amp;styleswitcherFont=' . $yootools->getParam('styleswitcherFont')
															. '&amp;styleswitcherWidth=' . $yootools->getParam('styleswitcherWidth')
															. '&amp;widthThinPx=' . $yootools->getParam('widthThinPx')
															. '&amp;widthWidePx=' . $yootools->getParam('widthWidePx')
															. '&amp;widthFluidPx=' . $yootools->getParam('widthFluidPx')
															. '&amp;leftWidth=' . $yootools->getParam('leftWidth')
															. '&amp;rightWidth=' . $yootools->getParam('rightWidth')
															. '&amp;option=' . $option);

$yootools->addStyleSheet($template_baseurl . '/lib/js/lightbox/css/slimbox.css');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo _LANGUAGE; ?>" xml:lang="<?php echo _LANGUAGE; ?>">
<head>
<?php $yootools->showHead(); ?>
<?php mosShowHead(); ?>
<?php if($my->id) initEditor(); ?>
<meta http-equiv="Content-Type" content="text/html; <?php echo _ISO; ?>" />

<!-- Add YOOtools extension styles here -->
<link href="<?php echo $mosConfig_live_site ?>/components/com_virtuemart/fetchscript.php?gzip=0&amp;subdir[0]=/themes/default/templates/calendar/css&amp;file[0]=calendar.css" type="text/css" rel="stylesheet" />
</head>
<body id="page2" style="padding: 15px" class="<?php echo $yootools->getCurrentStyle(); ?> <?php echo $yootools->getParam('itemcolor'); ?>">
<?php mosMainBody() ?>
</body>
</html>