jQuery(function($){
    
    jQuery.extend(jQuery.fn,{
        
        /**
        * Creates a slideshow
        */
        slideshow:function(options) {
            
            // Some default options
            defaults = jQuery.extend({
                
                "autoplay"      : true,             // Auto start the slideshow
                "controls"      : true,             // Show navigation controls
                "animation"     : "horizontal",     // Animation of slideshow
                "numeric"       : false,            // Show numeric navigation controls
                "pause"         : 5000,             // Set the pause period for each slide
                "speed"         : 1000              // Set the slideshow animation speed
                
            }, options);
            
            // Create the base object to store common settings
            var base = {};
            base.options = $.extend(defaults, options);
            
            // Set some variables
            base.element        = $(this);                              // Set the base slideshow element for use throughout
            base.numSlides      = base.element.find("ul li").size();    // Get the number of slides
            base.width          = base.element.width();                 // Get the slideshow width
            base.height         = base.element.height();                // Get the slideshow height
            base.currentSlide   = 1;                                    // Set the initial slide counter
            base.nextSlide      = 2;                                    // Set the next slide in order
            base.direction      = "next";                               // Set the slide direction
            
            
            
            /**
            * Initialise the slideshow
            */
            base.init = function() {
                
                // Create a slideshow only for more than one item
                if (base.numSlides > 1) {
                    
                    // Clone the first slide and set dimensions
                    base.element.find("ul li:first").clone(true).appendTo(base.element.find("ul"));
                    base.element.find("ul li").css({ "width": base.width });
                    base.element.find("ul li").css({ "height": base.height });
                    base.numSlides += 1;
                    
                    // Position the slides
                    base.position_slides();
                    
                    // Add the control buttons
                    if (base.options.controls) base.add_controls();
                    
                    // Initiate the animation
                    if (base.options.autoplay) base.slideTimer = setTimeout(base.animate_slide, base.options.pause);
                    
                }
                
            }
            
            
            
            /**
            * Positions the slides
            */
            base.position_slides = function() {
                
                // Float each slide for horizontal scrolling
                if (base.options.animation == "horizontal") {
                    
                    base.element.find("ul").css({ "width": (base.numSlides * base.width)+"px" });
                    base.element.find("ul li").css({ "float": "left" });
                    
                }
                
                else if (base.options.animation == "vertical") {
                    
                    base.element.find("ul").css({ "height": (base.numSlides * base.height)+"px" });
                    base.element.find("ul li").css({ "clear": "both" });
                    
                }
                
                else if (base.options.animation == "fade") {
                    
                    base.element.find("ul").css({ "position": "relative" });
                    base.element.find("ul li").css({ "position": "absolute", "top": "0", "left": "0" });
                    
                    // Add counters to the slide classes
                    var s = 1;
                    
                    base.element.find("ul li").each(function() {
                        
                        $(this).addClass("slide-"+s);
                        $(this).css({ "z-index": (base.numSlides - s) });
                        s ++;
                        
                    });
                    
                }
                
            }
            
            
            
            /**
            * Adds the controls
            */
            base.add_controls = function() {
                
                base.element.append("<ol class=\"pager\"></ol>");
                base.element.find(".pager").css({ "z-index" : 999 });
                
                // Numeric controls
                if (base.options.numeric) {
                    
                    // Add the controls
                    for (s = 1; s <= (base.numSlides - 1); s ++ ) base.element.find(".pager").append("<li class=\"slide-"+s+(s == 1 ? " first selected" : "")+"\"><a href=\""+s+"\">"+s+"</a></li>");
                    
                    // Detect navigation selection
                    base.element.find(".pager a").click(function(event) {
                        
                        event.preventDefault();
                        
                        if ($(this).attr("href") != base.currentSlide) {
                            
                            // Pause the animation
                            base.pause_animation();
                            
                            // Set the slide counter
                            base.nextSlide = parseFloat($(this).attr("href"));
                            base.direction = "jump";
                            
                            // Animate
                            base.animate_slide();
                            
                        }
                        
                    });
                    
                }
                
                // Add the controls
                base.element.find(".pager").prepend("<li class=\"prev prev-"+base.options.animation+"\"><a href=\"#\">Previous</a></li>");
                base.element.find(".pager").append("<li class=\"next next-"+base.options.animation+"\"><a href=\"#\">Next</a></li>");
                
                // Detect navigation selection
                base.element.find(".prev, .next").click(function(event) {
                    
                    event.preventDefault();
                    
                    // Make sure an animation is not in progress
                    if (!base.animating) {
                        
                        // Pause the animation
                        base.pause_animation();
                        
                        // Decide if we are sliding forwards or backwards
                        base.direction = $(this).hasClass("next") ? "next" : "prev";
                        
                        // Set the starting animation position
                        base.reset_slide();
                        
                        // Increment the slide counter
                        base.nextSlide = $(this).hasClass("next") ? (base.currentSlide + 1) : (base.currentSlide - 1);
                        base.direction = $(this).hasClass("next") ? "next" : base.direction = "prev";
                        
                        //alert(base.nextSlide);
                        
                        // Animate
                        base.animate_slide();
                        
                    }
                    
                });
                
            }
            
            
            
            /**
            * Pause the animation
            */
            base.pause_animation = function() {
                
                clearTimeout(base.slideTimer);
                base.options.autoplay = false;
                
            }
            
            
            
            /**
            * Animate the slideshow
            */
            base.animate_slide = function() {
                
                // Set the animation flag
                base.animating = true;
                
                if (base.options.animation == "horizontal") base.animate_horizontal();
                
                else if (base.options.animation == "vertical") base.animate_vertical();
                
                else if (base.options.animation == "fade") base.animate_fade();
                
            }
            
            
            
            /**
            * Determine if to continue animation - autoplay
            */
            base.continue_animation = function() {
                
                // Unset the animation flag
                base.animating = false;
                
                // Set the current slide status
                base.currentSlide = base.nextSlide;
                
                // Set the button as selected
                base.element.find(".pager li").removeClass("selected");
                base.element.find(".pager li.slide-"+(base.currentSlide >= base.numSlides ? 1 : base.currentSlide)).addClass("selected");
                
                if (base.options.autoplay) {
                    
                    // Make sure the slide continues to the 'next' slide
                    base.direction = "next";
                    base.nextSlide = base.currentSlide + 1;
                    
                    // Set the starting animation position
                    base.reset_slide();
                    
                    // Continue the animation
                    base.slideTimer = setTimeout(base.animate_slide, base.options.pause);
                    
                }
                
            }
            
            
            
            /**
            * Reset the slide position for continual animation
            */
            base.reset_slide = function() {
                
                // Return to the start of the slideshow for forward navigation
                if (base.currentSlide >= base.numSlides && base.direction == "next") {
                    
                    if (base.options.animation == "horizontal") base.element.find("ul").css({ "margin-left": "0px" });
                    else if (base.options.animation == "vertical") base.element.find("ul").css({ "margin-top": "0px" });
                    
                    base.currentSlide = 1;
                    base.nextSlide = 2;
                    
                }
                
                // Go to the end of the slideshow for backward navigation
                else if (base.currentSlide <= 1 && base.direction == "prev") {
                    
                    if (base.options.animation == "horizontal") base.element.find("ul").css({ "margin-left": ("-"+((base.numSlides - 1) * base.width)+"px") });
                    if (base.options.animation == "vertical") base.element.find("ul").css({ "margin-top": ("-"+((base.numSlides - 1) * base.height)+"px") });
                    
                    base.currentSlide = base.numSlides;
                    base.nextSlide = base.numSlides - 1;
                    
                }
                
            }
            
            
            
            /**
            * Animates horizontally
            */
            base.animate_horizontal = function() {
                
                // Set the animation margin properties
                var anim = {};
                if (base.direction == "next") anim["marginLeft"] = "-="+base.width+"px";
                else if (base.direction == "prev") anim["marginLeft"] = "+="+base.width+"px";
                else if (base.direction == "jump") anim["marginLeft"] = "-"+((base.nextSlide - 1) * base.width)+"px";
                
                // animate to the next slide
                base.element.find("ul").animate(anim, base.options.speed, '', function() {
                    
                    // Continue animation
                    base.continue_animation();
                    
                });
                
            }
            
            
            
            /**
            * Animates vertically
            */
            base.animate_vertical = function(jumpTo) {
                
                // Set the animation margin properties
                var anim = {};
                if (base.direction == "next") anim["marginTop"] = "-="+base.height+"px";
                else if (base.direction == "prev") anim["marginTop"] = "+="+base.height+"px";
                else if (base.direction == "jump") anim["marginTop"] = "-"+((base.nextSlide - 1) * base.height)+"px";
                
                // animate to the next slide
                base.element.find("ul").animate(anim, base.options.speed, false, function() {
                    
                    // Continue animation
                    base.continue_animation();
                    
                });
                
            }
            
            
            
            /**
            * Animates with a fade
            */
            base.animate_fade = function() {
                
                // animate to the next slide
                base.element.find("ul li").css({ "display": "none" });
                base.element.find("ul li.slide-"+base.currentSlide).css({ "display": "block" });
                
                base.element.find("ul li.slide-"+base.currentSlide).fadeOut((base.options.speed / 2));
                base.element.find("ul li.slide-"+base.nextSlide).fadeIn((base.options.speed / 2), function() {
                        
                    // Continue animation
                    base.continue_animation();
                    
                });
                
            }
            
            // Initialise
            base.init();
            
        }
    
    });
    
});
