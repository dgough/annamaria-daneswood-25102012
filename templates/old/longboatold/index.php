<?php
//error_reporting(E_ALL);
ini_set('display_errors','On');

// no direct access
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );
global $option;

$template_baseurl = $mosConfig_live_site . '/templates/' . $cur_template;

/**
 * This sets the header text for the homepage
 */
global $mainframe, $whitelabel;
$database->setQuery("SELECT * FROM #__menu WHERE menutype = 'mainmenu' AND parent = 0 AND published = 1 ORDER BY ordering LIMIT 0,1");
$database->loadObject($menu); 
$params = new mosParameters($menu->params);
if($mainframe->getItemid() == $menu->id) $mainframe->_head['title'] = $params->get('header');

// set custom title for frontpage only
if ($option == 'com_frontpage' || $option =='') {
	$title = 'Anna Maria Island Luxury Vacation Rentals and Beach Homes';
	$GLOBALS['mosConfig_sitename'] = '';
	$mainframe->setPageTitle($title);
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo _LANGUAGE; ?>" xml:lang="<?php echo _LANGUAGE; ?>">
<head>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $template_baseurl . '/lib/js/mootools.js.php'; ?>"></script>
<?php mosShowHead(); ?>
<?php if($my->id) initEditor(); ?>
<meta http-equiv="Content-Type" content="text/html; <?php echo _ISO; ?>" />
<link href="<?php echo $template_baseurl . '/css/template.css.php?t=123910231' ?>" type="text/css" rel="stylesheet" />
<link href="<?php echo $mosConfig_live_site ?>/components/com_virtuemart/fetchscript.php?gzip=0&amp;subdir[0]=/themes/default/templates/calendar/css&amp;file[0]=calendar.css" type="text/css" rel="stylesheet" />
</head>

<body id="page" class="<?php echo $option ?> <?php echo $whitelabel->countModules('right', true) ? 'right-col' : '' ?> <?php echo $whitelabel->countModules('left', true) ? 'left-col' : '' ?>">
	
	<div id="wrapper">
		<?php if($whitelabel->countModules('toolbar', true)) { ?>
		<div id="toolbar_top">
			<?php $whitelabel->loadModules('toolbar', -1, true); ?>
		</div>
		<?php } ?>
		<div id="header">
			<?php $whitelabel->loadModules('header', -2, true); ?>
			<div id="banner">
				<?php $whitelabel->loadModules('header1', -2, true); ?>
				<?php $whitelabel->loadModules('user2', -2, true); ?>
			</div>
			<div id="call-to-action">
				<p>call: USA 941 778 4178, USA TOLL FREE 800 737 9855, UK FREE PHONE 0800 035 2685</p>
			</div>
		</div>
		
		<?php $whitelabel->loadModules('header2', -1, true); ?>
		
		<div id="content-main" class="clearfix">
			<?php if($whitelabel->countModules('left', true)) { ?>
			<div id="content-left">
				<?php $whitelabel->loadModules('left', -2, true); ?>
			</div>
			<?php } ?>
			
			
			<?php if($whitelabel->countModules('right', true)) { ?>
			<div id="content-right">
				<?php $whitelabel->loadModules('right', -2, true); ?>
			</div>
			<?php } ?>
			<div id='content'>
			<?php mosMainBody(); ?>
			</div>
			
		</div>
		
		<?php if($whitelabel->countModules('bottom', true)) { ?>
		<div id="content-bottom" class="clearfix">
			<?php $whitelabel->loadModules('bottom', -2, true); ?>
		</div>
		<?php } ?>
		
	</div>
	
	<div id="footer-menu" class="clearfix">
		<div class="footer-inner">
			<?php $whitelabel->loadModules('footer', -2, true); ?>
		</div>
	</div>
	
	<div id="footer">
		<div class="footer-inner">
			<?php $whitelabel->loadModules('footer1', -2, true); ?>
		</div>
	</div>
	
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-1415804-1");
pageTracker._trackPageview();
} catch(err) {}</script>					

</body>
</html>