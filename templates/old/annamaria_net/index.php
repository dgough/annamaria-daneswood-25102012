<?php
/**
 * Organic Development template
 *
 * @author organic-development.com
 * @copyright Copyright (C) 2008 Organic Developmet Ltd. All rights reserved.
 */

// no direct access
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/*** template parameters ***/
$template_parameters = array(
	/* left / right column layout */
	"leftWidth"           => '200px',      	 /* left hand column width */
	"rightWidth"          => '200px',      	 /* right hand column width */
	"showLeft"            => 1,      	 	/* left hand column on */
	"showRight"           => 1,      	 	/* right hand column on */
	"roundedWrapper"      => 0,      	 	/* whether or not the wrapper has rounded corners */
	"roundedInner"        => 0,      	 	/* whether the inner has rounded corners */
	"loadJavascript"      => 1      	 	
);

//Include yootheme 
include_once($mosConfig_absolute_path . '/templates/' . $cur_template . '/lib/php/yootools.php');
include_once($mosConfig_absolute_path . '/templates/' . $cur_template . '/lib/php/yoolayout.php');

// Use this variable to reference images in the tempalte directory
$template_baseurl = $moConfig_live_site . '/templates/' . $cur_template;

// add the main template css files
$yootools->setStyleSheet($template_baseurl);

// add template javascript
if ($yootools->getParam('loadJavascript')) {
	$yootools->addScript($template_baseurl . '/lib/js/mootools.js.php');
	$yootools->addScript($yootools->getJavaScript());
	$yootools->addScript($template_baseurl . '/lib/js/template.js.php');
}

// Load slimbox
if($yootools->getParam('loadSlimbox')){
	$yootools->addStyleSheet($template_baseurl . '/lib/js/lightbox/css/slimbox.css');
}

if($option == 'com_hotproperty' || $option == 'com_virtuemart'){
	$yootools->setParam('rightcolumn',false);
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo _LANGUAGE; ?>" xml:lang="<?php echo _LANGUAGE; ?>">
<head>
<meta http-equiv="Content-Type" content="text/html; <?php echo _ISO; ?>" />
<?php if($my->id) initEditor(); ?>
<?php
/* Load YOOtools stylesheets etc */
$yootools->showHead();
/* Add YOOtools extension styles here */ 
$yootools->getYooModules();
?>
<?php mosShowHead(); ?>
</head>

<body id="page" class="<?php echo $yootools->getMenuLabel('mainmenu') .' '. $yootools->getCurrentStyle() .' '. $yootools->getParam('leftcolumn') .' '. $yootools->getParam('rightcolumn') .' '. $yootools->getParam('itemcolor'); ?>">

	<?php if(mosCountModules('absolute')) { ?>
	<div id="absolute">
		<?php mosLoadModules('absolute', -1); ?>
	</div>
	<?php } ?>
	
	<div id="page-body">
		<div class="wrapper floatholder">
			<?php if($yootools->getParam('roundedWrapper')){ ?>
			<div class="wrapper-t">
				<div class="wrapper-r">
					<div class="wrapper-b">
						<div class="wrapper-l">
							<div class="wrapper-tl">
								<div class="wrapper-tr">
									<div class="wrapper-bl">
										<div class="wrapper-br">
										<?php } ?>
											<div id="header">
												<div class="header-l">
													<div class="header-r">	
														<?php if(mosCountModules('search') || $yootools->getParam('styleswitcherFont') || $yootools->getParam('styleswitcherWidth') || $yootools->getParam('date')){ ?>										
														<div id="toolbar">
															<div class="floatbox ie_fix_floats">																
																<?php if(mosCountModules('search')) { ?>
																<div id="search">
																	<?php mosLoadModules('search', -1); ?>
																</div>
																<?php } ?>													
																<?php if($yootools->getParam('styleswitcherFont') || $yootools->getParam('styleswitcherWidth')) { ?>
																<div id="styleswitcher">
																	<?php if($yootools->getParam('styleswitcherWidth')) { ?>
																	<a id="switchwidthfluid" href="javascript:void(0)" title="Fluid width"></a>
																	<a id="switchwidthwide" href="javascript:void(0)" title="Wide width"></a>
																	<a id="switchwidththin" href="javascript:void(0)" title="Thin width"></a>
																	<?php } ?>
																	<?php if($yootools->getParam('styleswitcherFont')) { ?>
																	<a id="switchfontlarge" href="javascript:void(0)" title="Increase font size"></a>
																	<a id="switchfontmedium" href="javascript:void(0)" title="Default font size"></a>
																	<a id="switchfontsmall" href="javascript:void(0)" title="Decrease font size"></a>
																	<?php } ?>
																</div>
																<?php } ?>																
																<?php if($yootools->getParam('date')) { ?>
																<div id="date">
																	<?php echo (strftime(_DATE_FORMAT_LC, time() + ($mosConfig_offset * 60 * 60))); ?>
																</div>
																<?php } ?>
															</div>
														</div>
														<?php } ?>
														<?php if(mosCountModules('topmenu') || mosCountModules('headernet')){ ?>
														<div id="headerbar">
															<div class="floatbox ie_fix_floats">
															
																<?php if(mosCountModules('topmenu')) { ?>
																<div id="topmenu">
																	<?php mosLoadModules('topmenu', -1); ?>
																</div>
																<?php } ?>
																
																<?php if(mosCountModules('headernet')) { ?>
																<div id="headermodule">
																	<?php mosLoadModules('headernet', -3); ?>
																</div>
																<?php } ?>
																
															</div>
														</div>
														<?php } ?>
														<?php if(mosCountModules('logo')) { ?>
														<div id="logo">
															<?php mosLoadModules('logo', -1); ?>
														</div>
														<?php } ?>
														
														<?php if (mosCountModules('topnet')) { ?>
														<div id="menu2">
															<?php mosLoadModules( 'topnet', -1 ); ?>
														</div>
														<?php } ?>
												
													</div>
												</div>
											</div>
											<!-- header end -->
											
										<?php if (mosCountModules('banner')) { ?>
										<div id="banner">
											<?php mosLoadModules( 'banner', -1 ); ?>
										</div>
										<?php } ?>
										
										<div class='inner'>
											<?php if($yootools->getParam('roundedInner')){ ?>
											<div class='inner-t'>
												<div class='inner-r'>
													<div class='inner-b'>
														<div class='inner-l'>
															<div class='inner-tl'>
																<div class='inner-tr'>
																	<div class='inner-br'>
																		<div class='inner-bl'>
																			<?php } ?>								
																			<?php if(mosCountModules('top1') || mosCountModules('top2') || mosCountModules('top3') || mosCountModules('top4')) { ?>
																				<div id="top">
																					<div class="top-b">
																						<div class="top-l">
																							<div class="top-r">
																								<div class="top-tl">
																									<div class="top-tr">
																										<div class="top-bl">
																											<div class="top-br">
																												<div class="floatbox ie_fix_floats">
																											
																													<?php if(mosCountModules('top1')) { ?>
																													<div class="topbox <?php echo $yootools->getParam('topboxwidth') .' '. $yootools->getParam('topbox12seperator'); ?> float-left">
																														<?php mosLoadModules('top1', -2); ?>
																													</div>
																													<?php } ?>
																																										
																													<?php if(mosCountModules('top2')) { ?>
																													<div class="topbox <?php echo $yootools->getParam('topboxwidth') .' '. $yootools->getParam('topbox23seperator'); ?> float-left">
																														<?php mosLoadModules('top2', -2); ?>
																													</div>
																													<?php } ?>
																													
																													<?php if(mosCountModules('top3')) { ?>
																													<div class="topbox <?php echo $yootools->getParam('topboxwidth') .' '. $yootools->getParam('topbox34seperator'); ?> float-left">
																														<?php mosLoadModules('top3', -2); ?>
																													</div>
																													<?php } ?>
																																										
																													<?php if(mosCountModules('top4')) { ?>
																													<div class="topbox <?php echo $yootools->getParam('topboxwidth'); ?> float-left">
																														<?php mosLoadModules('top4', -2); ?>
																													</div>
																													<?php } ?>
																										
																												</div>
																											</div>
																										</div>
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<!-- top end -->
																				<?php } ?>
																				
																				<div id="middle">
																					<?php if(mosCountModules('user1net') || mosCountModules('user2')) { ?>
																						<div id="maintop" class="floatbox">
																				
																							<?php if(mosCountModules('user1net')) { ?>
																							<div class="maintopbox <?php echo $yootools->getParam('maintopboxwidth') .' '. $yootools->getParam('maintopbox12seperator'); ?> float-left">
																								<?php mosLoadModules('user1net', -3); ?>
																							</div>
																							<?php } ?>
																			
																							<?php if(mosCountModules('user2')) { ?>
																							<div class="maintopbox <?php echo $yootools->getParam('maintopboxwidth'); ?> float-left">
																								<?php mosLoadModules('user2', -3); ?>
																							</div>
																							<?php } ?>
																								
																						</div>
																						<!-- maintop end -->
																					<?php } ?>							
																					<div class="background">
																					
																						<?php if($yootools->getParam('rightcolumn')) { ?>
																						<div id="right">
																							<div id="right_container" class="clearfix">
																								<?php mosLoadModules('right', -3); ?>
																							</div>
																						</div>
																						<!-- right end -->
																						<?php } ?>									
																										
																						<?php if($yootools->getParam('leftcolumn')) { ?>
																						<div id="left">
																							<div id="left_container" class="clearfix">														
																								<?php mosLoadModules('leftnet', -3); ?>				
																							</div>
																						</div>
																						<!-- left end -->
																						<?php } ?>
																						
																						<div id="main">
																							<div id="main_container" class="clearfix">																	
																								<div id="mainmiddle" class="floatbox">																						
																								
																									<div id="content">
																										<div id="content_container" class="clearfix">									
																											<?php if(mosCountModules('advert1net') || mosCountModules('advert2')) { ?>
																											<div id="contenttop" class="floatbox">
																										
																												<?php if(mosCountModules('advert1net')) { ?>
																												<div class="contenttopbox left <?php echo $yootools->getParam('contenttopboxwidth') .' '. $yootools->getParam('contenttopbox12seperator'); ?> float-left">
																													<?php mosLoadModules('advert1net', -3); ?>
																												</div>
																												<?php } ?>
																								
																												<?php if(mosCountModules('advert2')) { ?>
																												<div class="contenttopbox right <?php echo $yootools->getParam('contenttopboxwidth'); ?> float-left">
																													<?php mosLoadModules('advert2', -3); ?>
																												</div>
																												<?php } ?>
																	
																											</div>
																											<!-- contenttop end -->
																											<?php } ?>
														
																											<?php if($yootools->getParam('breadcrumb')){ ?>
																											<div id="breadcrumb">
																												<?php mosPathWay(); ?>
																											</div>
																											<!-- breadcrumb end -->
																											<?php } ?>
																				
																											<div class="floatbox">
																												<div id='mainbody'>
																													<?php mosMainBody(); ?>
																												</div>
																											</div>
													
																											<?php if(mosCountModules('advert3') || mosCountModules('advert4')) { ?>
																											<div id="contentbottom" class="floatbox">
																													
																												<?php if(mosCountModules('advert3')) { ?>
																												<div class="contentbottombox left <?php echo $yootools->getParam('contentbottomboxwidth') .' '. $yootools->getParam('contentbottombox12seperator'); ?> float-left">
																													<?php mosLoadModules('advert3', -3); ?>
																												</div>
																												<?php } ?>
																								
																												<?php if(mosCountModules('advert4')) { ?>
																												<div class="contentbottombox right <?php echo $yootools->getParam('contentbottomboxwidth'); ?> float-left">
																													<?php mosLoadModules('advert4', -3); ?>
																												</div>
																												<?php } ?>
																	
																											</div>
																											<!-- mainbottom end -->
																											<?php } ?>
													
																										</div>
																									</div>
																									<!-- content end -->								
																								</div>
																								<!-- mainmiddle end -->				
																							</div>
																						</div>
																						<!-- main end -->
																						<?php if(mosCountModules('user3') || mosCountModules('user4')) { ?>
																						<div id="mainbottom" class="floatbox">
																		
																							<?php if(mosCountModules('user3')) { ?>
																							<div class="mainbottombox <?php echo $yootools->getParam('mainbottomboxwidth') .' '. $yootools->getParam('mainbottombox12seperator'); ?> float-left">
																								<?php mosLoadModules('user3', -3); ?>
																							</div>
																							<?php } ?>
																		
																							<?php if(mosCountModules('user4')) { ?>
																							<div class="mainbottombox <?php echo $yootools->getParam('mainbottomboxwidth'); ?> float-left">
																								<?php mosLoadModules('user4', -3); ?>
																							</div>
																							<?php } ?>
																		
																						</div>
																						<!-- mainbottom end -->
																						<?php } ?>										
																					</div>
																				</div>
																				<!-- middle end -->
																			
																				<?php if(mosCountModules('bottom1') || mosCountModules('bottom2') || mosCountModules('bottom3') || mosCountModules('bottom4')) { ?>
																				<div id="bottom">
																					<div class="bottom-b">
																						<div class="bottom-l">
																							<div class="bottom-r">
																								<div class="bottom-tl">
																									<div class="bottom-tr">
																										<div class="bottom-bl">
																											<div class="bottom-br">
																												<div class="floatbox ie_fix_floats">
																												
																													<?php if(mosCountModules('bottom1')) { ?>
																													<div class="bottombox <?php echo $yootools->getParam('bottomboxwidth') .' '. $yootools->getParam('bottombox12seperator'); ?> float-left">
																														<?php mosLoadModules('bottom1', -2); ?>
																													</div>
																													<?php } ?>
																																																		
																													<?php if(mosCountModules('bottom2')) { ?>
																													<div class="bottombox <?php echo $yootools->getParam('bottomboxwidth') .' '. $yootools->getParam('bottombox23seperator'); ?> float-left">
																														<?php mosLoadModules('bottom2', -2); ?>
																													</div>
																													<?php } ?>
																													
																													<?php if(mosCountModules('bottom3')) { ?>
																													<div class="bottombox <?php echo $yootools->getParam('bottomboxwidth') .' '. $yootools->getParam('bottombox34seperator'); ?> float-left">
																														<?php mosLoadModules('bottom3', -2); ?>
																													</div>
																													<?php } ?>
																																																
																													<?php if(mosCountModules('bottom4')) { ?>
																													<div class="bottombox <?php echo $yootools->getParam('bottomboxwidth'); ?> float-left">
																														<?php mosLoadModules('bottom4', -2); ?>
																													</div>
																													<?php } ?>
																												
																												</div>
																											</div>
																										</div>
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<!-- bottom end -->
																				<?php } ?>										
	
											
																			<?php if($yootools->getParam('roundedInner')){ ?>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<?php } ?>
											</div>
											
										<?php if($yootools->getParam('roundedWrapper')){ ?>	
										</div>	
									</div>	
								</div>	
							</div>	
						</div>	
					</div>	
				</div>	
			</div>	
			<?php } ?>
			<!-- wrapper end -->
			<?php if(mosCountModules('footernet') || mosCountModules('legals') || mosCountModules('debug')) { ?>
			<div id="footer">
				<div class="wrapper floatholder">
				
					<?php if(mosCountModules('footernet')) { ?>
					<div id="footer">
						<a class="anchor" href="#page">&nbsp;</a>
						<?php mosLoadModules('footernet', -1); ?>
					</div>
					<?php } ?>
					<?php if(mosCountModules('legals')) { ?>
					<div id="legals">
						<?php mosLoadModules('legals', -1); ?>
					</div>
					<?php } ?>
					<?php mosLoadModules( 'debug', -1 );?>
				</div>

			</div>			
			<!-- page-footer end -->			
			<?php } ?>
		</div>		
	</div>
	<!-- page-body end -->						
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-1415804-5");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>