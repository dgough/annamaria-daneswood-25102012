<?php
/**
 * YOOLayout setup
 *
 * @author		yootheme.com
 * @copyright	Copyright (C) 2007 YOOtheme Ltd & Co. KG. All rights reserved.
 */

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );

$yootools = &YOOTools::getInstance($template_parameters);


// set css-class for layoutstyle
if ($yootools->getParam('showLeft') && $yootools->checkModules('left')) {
	$yootools->setStyle(true, 'leftcolumn', 'showLeft');
}


// set css-class for rightbackground
if ($yootools->getParam('showRight') && $yootools->checkModules('right') && !$mainframe->get( 'loadEditor' )) { 
	$yootools->setStyle(true, 'rightcolumn', 'showRight');
}


if(mosCountModules('top1') || mosCountModules('top2') || mosCountModules('top3') || mosCountModules('top4')){
	// set css-class for topbox
	$yootools->setStyle('top1 + top2 + top3 + top4 == 1', 'topboxwidth', 'width100');
	$yootools->setStyle('top1 + top2 + top3 + top4 == 2', 'topboxwidth', 'width50');
	$yootools->setStyle('top1 + top2 + top3 + top4 == 3', 'topboxwidth', 'width33');
	$yootools->setStyle('top1 + top2 + top3 + top4 == 4', 'topboxwidth', 'width25');
	// set css-class for topbox seperators
	$yootools->setStyle('top1 && ( top2 || top3 || top4 )', 'topbox12seperator', 'seperator');
	$yootools->setStyle('top2 && ( top3 || top4 )', 'topbox23seperator', 'seperator');
	$yootools->setStyle('top3 && top4', 'topbox34seperator', 'seperator');
}


if(mosCountModules('user1') || mosCountModules('user2')){
	// set css-class for maintopbox
	$yootools->setStyle('user1 + user2 == 1', 'maintopboxwidth', 'width100');
	$yootools->setStyle('user1 + user2 == 2', 'maintopboxwidth', 'width50');
	// set css-class for maintopbox seperators
	$yootools->setStyle('user1 && user2', 'maintopbox12seperator', 'seperator');
}


if(mosCountModules('advert1') || mosCountModules('advert2')){
	// set css-class for contenttopbox
	$yootools->setStyle('advert1 + advert2 == 1', 'contenttopboxwidth', 'width100');
	$yootools->setStyle('advert1 + advert2 == 2', 'contenttopboxwidth', 'width50');
	// set css-class for contenttopbox seperators
	$yootools->setStyle('advert1 && advert2', 'contenttopbox12seperator', 'seperator');
}


if(mosCountModules('adver3') || mosCountModules('advert4')){
	// set css-class for contentbottombox
	$yootools->setStyle('advert3 + advert4 == 1', 'contentbottomboxwidth', 'width100');
	$yootools->setStyle('advert3 + advert4 == 2', 'contentbottomboxwidth', 'width50');
	// set css-class for contentbottombox seperators
	$yootools->setStyle('advert3 && advert4', 'contentbottombox12seperator', 'seperator');
}


if(mosCountModules('user3') || mosCountModules('user4')){
	// set css-class for mainbottombox
	$yootools->setStyle('user3 + user4 == 1', 'mainbottomboxwidth', 'width100');
	$yootools->setStyle('user3 + user4 == 2', 'mainbottomboxwidth', 'width50');
	// set css-class for mainbottombox seperators
	$yootools->setStyle('user3 && user4', 'mainbottombox12seperator', 'seperator');
}


if(mosCountModules('bottom1') || mosCountModules('bottom2') || mosCountModules('bottom3') || mosCountModules('bottom4')){
	// set css-class for bottombox
	$yootools->setStyle('bottom1 + bottom2 + bottom3 + bottom4 == 1', 'bottomboxwidth', 'width100');
	$yootools->setStyle('bottom1 + bottom2 + bottom3 + bottom4 == 2', 'bottomboxwidth', 'width50');
	$yootools->setStyle('bottom1 + bottom2 + bottom3 + bottom4 == 3', 'bottomboxwidth', 'width33');
	$yootools->setStyle('bottom1 + bottom2 + bottom3 + bottom4 == 4', 'bottomboxwidth', 'width25');
	// set css-class for bottombox seperators
	$yootools->setStyle('bottom1 && ( bottom2 || bottom3 || bottom4 )', 'bottombox12seperator', 'seperator');
	$yootools->setStyle('bottom2 && ( bottom3 || bottom4 )', 'bottombox23seperator', 'seperator');
	$yootools->setStyle('bottom3 && bottom4', 'bottombox34seperator', 'seperator');
}


// set tools color
$yootools->setParam('tools', array('black' => 'yootools-black'));

// set template url
$yootools->setParam('tplurl', $mosConfig_live_site . '/templates/' . $cur_template);

// set color
if ($yootools->getCurrentColor() != "default") {
	$yootools->setParam('color', $yootools->getCurrentColor());
}
?>