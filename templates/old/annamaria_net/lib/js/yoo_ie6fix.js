/* (C) 2008 YOOtheme.com */

function loadIE6Fix() {
	correctPngBackground('.correct-png', 'crop');
	correctPngBackground('.menubar-l, .menubar-r');
	correctPngBackground('.wrapper-tl, .wrapper-tr');
	correctPngBackground('div.module h3, div.module_menu h3, div.module-black h3, div.module-orange h3, div.module-blue h3, div.module-green h3');	
	correctPngBackground('a.readmore, a.readon', 'crop');
	sfHover('.menu span.separator');
	sfHover('.menu li');
	sfHover('#search div');
	sfHover('#search input');
	sfFocus('#search div');
	sfFocus('#search input');
}

/* Add functions on window load */
window.addEvent('domready', loadIE6Fix);
window.addEvent('load', correctPngInline);