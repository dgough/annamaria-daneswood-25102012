<?php
/**
 * YOOTools
 *
 * @author		yootheme.com
 * @copyright	Copyright (C) 2007 YOOtheme Ltd & Co. KG. All rights reserved.
 */

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );

class YOOTools {

	/* template stylesheets */
	var $stylesheets;

	/* template javascripts */
	var $scripts;

	/* yootheme global default template settings */
	var $defaults;  

	/* javascript settings */
	var $javascript;

	/* template settings */
	var $settings;

	function YOOTools($settings = array()) {
		
		$this->stylesheets = array();
		$this->scripts = array();	
		$this->defaults = array(
			/* color */
			"color"               => "default",
			/* item color variation */
			"item1"               => "grey",
			"item2"               => "blue",
			"item3"               => "pink",
			"item4"               => "orange",
			"item5"               => "green",
			"item6"               => "default",
			"item7"               => "default",
			"item8"               => "default",
			"item9"               => "default",
			"item10"              => "default",
			/* layout */
			"date"    	          => true,			
			"styleswitcherFont"   => true,
			"styleswitcherWidth"  => true,
			"layout"              => "left",
			/* style switcher */
			"fontDefault"         => "font-medium",
			"widthDefault"        => "width-wide",
			"widthThinPx"         => 780,
			"widthWidePx"         => 900,
			"widthFluidPx"        => 0.9,
			/* javascript */
			"loadJavascript"      => true
			);

		$this->javascript = array(
			/* color */
			"color"               => "'<VAL>'",
			/* layout */
			"layout"              => "'<VAL>'",
			/* style switcher */
			"fontDefault"         => "'<VAL>'", 
			"widthDefault"        => "'<VAL>'",
			"widthThinPx"         => "<VAL>",
			"widthWidePx"         => "<VAL>",
			"widthFluidPx"        => "<VAL>"
			);

		/* ie browser checks */
		if (array_key_exists('HTTP_USER_AGENT', $_SERVER)) {
			$this->defaults['msie7'] = strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'msie 7') !== false;
			$this->defaults['msie6'] = strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'msie 6') !== false;
		} else {
			$this->defaults['msie7'] = false;
			$this->defaults['msie6'] = false;
		}

		$this->settings = $settings + $this->defaults;
	}

	function &getInstance($settings = array()) {
		static $instance;

		if ($instance == null) {
			$instance = new YOOTools($settings);
		}
		
		return $instance;
	}

	function getParam($key, $default = '') {

		if (array_key_exists($key, $this->settings)) {
			return $this->settings[$key];
		}
		
		return $default;
	}

	function setParam($key, $value = '') {
		$this->settings[$key] = $value;
	}

	/* Javascript */
	
	function getJavaScript() { 
		$js = "var YtSettings = { ";
		$seperator = false;
		foreach($this->javascript as $key => $val) {
			$setting = $this->getParam($key);
			if(is_bool($setting)) {
				$setting ? $setting = "true" : $setting = "false";
			}
			if(is_float($setting)) {
				$setting = number_format($setting, 2, ".", "");
			}
			$seperator ? $js .= ", " : $seperator = true;			
			$js .= $key . ": " . str_replace("<VAL>", $setting, $val);
		}		
		$js .= " };";
		return $js;
	}

	function showJavaScript() {
		echo $this->getJavaScript();
	}

	/* Styles */

	function setStyle($condition, $key, $value)
	{
		if ($this->evalStyleCondition($condition)) {
			$this->setParam($key, $value);
		}
	}
	
	function evalStyleCondition($condition)
	{
		if (is_bool($condition)) return $condition;

		$parts    = explode(' ', $condition);
		$commands = array('+', '-', '&&', '||', '(', ')', '==', '!=');

		for($i = 0; $i < count($parts); $i++) {
			if (!(in_array($parts[$i], $commands) || is_numeric($parts[$i]))) {
				$name	   = strtolower($parts[$i]);
				$parts[$i] = mosCountModules($name);
			}
		}

		$str = 'return '.implode(' ', $parts).';';
		return eval($str);
	}

	/* Styleswitcher */

	function getCurrentStyle() {
		$styleFont  = isset($_COOKIE['ytstylefont']) ? $_COOKIE['ytstylefont'] : $this->getParam('fontDefault');
		$styleWidth = isset($_COOKIE['ytstylewidth']) ? $_COOKIE['ytstylewidth'] : $this->getParam('widthDefault');

		return $styleFont . " " . $styleWidth;
	}
	
	function getCurrentColor() {
		$color  = isset($_COOKIE['ytcolor']) ? $_COOKIE['ytcolor'] : $this->getParam('color');
		
		if(isset($_GET['yt_color'])) {
			setcookie('ytcolor', $_GET['yt_color'], time() + 3600, '/'); 
			$color = $_GET['yt_color'];
		}
		
		return $color;
	}
	
	function getActiveMenuItemNumber($menu, $level) {
		global $database, $my, $Itemid, $mosConfig_shownoauth;

		$and = '';
		if (!$mosConfig_shownoauth) {
			$and = "\n AND access <= $my->gid";
		}

		$sql = "SELECT m.* FROM #__menu AS m"
		. "\n WHERE menutype = '" . $menu . "'"
		. "\n AND published = 1"
		. $and
		. "\n ORDER BY parent, ordering";

		$database->setQuery($sql);
		$rows = $database->loadObjectList('id');
		$path = array();
		$item_id = $Itemid;
		
		while ($item_id != 0) {
			if (array_key_exists($item_id, $rows)) {
				$path[] = $item_id;
				$item_id = $rows[$item_id]->parent;
			} else {
				break;
			}
		}
		
		$path = array_reverse($path);
				
		if (array_key_exists($level, $path)) {
			return $rows[$path[$level]]->ordering;
		}
		
		return null;	
	}
	
	/* Html helper */

	function addStyleSheet($data, $condition = true, $browser = '') {
		$this->stylesheets[] = array('data' => $data, 'condition' => $condition, 'browser' => $browser);
	}

	function addScript($data, $condition = true, $browser = '') {
		$this->scripts[] = array('data' => $data, 'condition' => $condition, 'browser' => $browser);
	}

	function showHead() {
		$html = '';

		foreach ($this->stylesheets as $style) {
			if ($style['condition']) {
				$html_tag = '<link href="' . $style['data'] . '" rel="stylesheet" type="text/css" />';
				$html .= $this->wrapBrowserCondition($style['browser'], $html_tag);
			}
		}
		
		foreach ($this->scripts as $script) {
			if ($script['condition']) {
				if (ereg("[.js|.php]\$", $script['data'])) {
					$html_tag = '<script language="javascript" src="' . $script['data'] . '" type="text/javascript"></script>';
				} else {
					$html_tag = '<script language="javascript" type="text/javascript">' . $script['data'] . '</script>';
				}
				$html .= $this->wrapBrowserCondition($script['browser'], $html_tag);
			}
		}
		
		echo $html;
	}
	
	function wrapBrowserCondition($browser, $html_tag) {
		$html = '';
		
		switch ($browser) {
			case 'msie':
				if ($this->getParam('msie6') || $this->getParam('msie7')) {
					$html = '<!--[if lte IE 7]>' . $html_tag . '<![endif]-->' . "\n";
				}
				break;
			case 'msie6':
				if ($this->getParam('msie6')) {
					$html = '<!--[if IE 6]>' . $html_tag . '<![endif]-->' . "\n";
				}
				break;
			case 'msie7':
				if ($this->getParam('msie7')) {
					$html = '<!--[if IE 7]>' . $html_tag . '<![endif]-->' . "\n";
				}
				break;
			default:
				$html = $html_tag . "\n";
				break;
		}
		
		return $html;
	}	
}
?>