<?php
/**
 * YOOtheme template
 *
 * @author yootheme.com
 * @copyright Copyright (C) 2007 YOOtheme Ltd & Co. KG. All rights reserved.
 */

// no direct access
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/*** template parameters ***/
$template_parameters = array(
	/* default color variation */
	"color"               => "default",      /* default (white) | grey | turquoise | deepblue | black | blue | green | red | lilac | peach */
	/* item color variation */
	"item1"               => "default",      /* default (grey) | green | pink | orange | blue */
	"item2"               => "blue",         /* default (grey) | green | pink | orange | blue */
	"item3"               => "pink",         /* default (grey) | green | pink | orange | blue */
	"item4"               => "orange",       /* default (grey) | green | pink | orange | blue */
	"item5"               => "green",        /* default (grey) | green | pink | orange | blue */
	"item6"               => "default",      /* default (grey) | green | pink | orange | blue */
	"item7"               => "default",      /* default (grey) | green | pink | orange | blue */
	"item8"               => "default",      /* default (grey) | green | pink | orange | blue */
	"item9"               => "default",      /* default (grey) | green | pink | orange | blue */
	"item10"              => "default",      /* default (grey) | green | pink | orange | blue */
	/* layout */
	"date"    	          => true,           /* true | false */
	"styleswitcherFont"   => true,           /* true | false */
	"styleswitcherWidth"  => false,          /* true | false */
	"layout"              => "left",         /* left | right */
	/* style switcher */
	"fontDefault"         => "font-medium",  /* font-small | font-medium | font-large */
	"widthDefault"        => "width-wide",   /* width-thin | width-wide | width-fluid */
	"widthThinPx"         => 780,            /* template width for style "width-thin", (pixels) */
	"widthWidePx"         => 940,            /* template width for style "width-wide", (pixels) */
	"widthFluidPx"        => 0.9,            /* template width for style "width-fluid", (0.9 means 90%) */
	/* javascript */
	"loadJavascript"      => true            /* true | false, set this to enable/disable all the templates javascripts */
);

include_once($mosConfig_absolute_path . '/templates/' . $cur_template . '/lib/php/yootools.php');
include_once($mosConfig_absolute_path . '/templates/' . $cur_template . '/lib/php/yoolayout.php');

$template_baseurl = $mosConfig_live_site . '/templates/' . $cur_template;

// add template javascript
if ($yootools->getParam('loadJavascript')) {
	$yootools->addScript($template_baseurl . '/lib/js/mootools.js.php');
	$yootools->addScript($yootools->getJavaScript());
	$yootools->addScript($template_baseurl . '/lib/js/template.js.php');
}

// add template css
$yootools->addStyleSheet($template_baseurl . '/css/template.css.php?color=' . $yootools->getParam('color')
											. '&amp;styleswitcherFont=' . $yootools->getParam('styleswitcherFont')
											. '&amp;styleswitcherWidth=' . $yootools->getParam('styleswitcherWidth')
											. '&amp;widthThinPx=' . $yootools->getParam('widthThinPx')
											. '&amp;widthWidePx=' . $yootools->getParam('widthWidePx')
											. '&amp;widthFluidPx=' . $yootools->getParam('widthFluidPx'));

$yootools->addStyleSheet($template_baseurl . '/lib/js/lightbox/css/slimbox.css');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo _LANGUAGE; ?>" xml:lang="<?php echo _LANGUAGE; ?>">
<head>
<?php mosShowHead(); ?>
<?php if($my->id) initEditor(); ?>
<meta http-equiv="Content-Type" content="text/html; <?php echo _ISO; ?>" />
<?php $yootools->showHead(); ?>

<!-- Add YOOtools extension styles here 
<link href="<?php echo $mosConfig_live_site;?>/modules/mod_yoo_slider/mod_yoo_slider.css.php" rel="stylesheet" type="text/css" />
<link href="<?php echo $mosConfig_live_site;?>/modules/mod_yoo_login/mod_yoo_login.css.php" rel="stylesheet" type="text/css" />
<link href="<?php echo $mosConfig_live_site;?>/modules/mod_yoo_drawer/mod_yoo_drawer.css.php" rel="stylesheet" type="text/css" />
<link href="<?php echo $mosConfig_live_site;?>/modules/mod_yoo_carousel/mod_yoo_carousel.css.php" rel="stylesheet" type="text/css" />
<link href="<?php echo $mosConfig_live_site;?>/modules/mod_yoo_toppanel/mod_yoo_toppanel.css.php" rel="stylesheet" type="text/css" />
<link href="<?php echo $mosConfig_live_site;?>/modules/mod_yoo_accordion/mod_yoo_accordion.css.php" rel="stylesheet" type="text/css" />
-->
<style type="text/css">
	body{
		background:#fff url(<?php echo $mosConfig_live_site.'/templates/'.$cur_template; ?>/images/layout/body-bg.jpg) repeat-x left top;
	}
</style>
</head>

<body class="<?php echo $yootools->getCurrentStyle(); ?> <?php echo $yootools->getParam('leftcolumn'); ?> <?php echo $yootools->getParam('rightcolumn'); ?> <?php echo $yootools->getParam('itemcolor'); ?>">

<div id="wrapper">
    <div id="header">
        <div id="headertop"> 
            <div id="logo"><a href="/">Anna Maria</a></div>
            <div id="nav">
            	<?php mosLoadModules('toolbar', -2); ?>
            </div>
        </div>
        <div id="headerbottom">
        	<div id="tr"></div>
            <div id="tl"></div>
            <div id="banner">
            	<?php mosLoadModules('banner', -2); ?>
            </div>
            <div id="brbl"></div>
        </div>
        <div id="searchbox">
        	<?php mosLoadModules('user2', -2); ?>
        </div>
	</div>
	<div id="cwrapper">
    	<div id="left">&nbsp; 
        	<?php mosLoadModules('left', -2); ?>
        </div>
		<div id="main">
        	<?php mosMainBody(); ?>
        </div>
        <div class="clearfix" style="clear:both;">&nbsp;
        <p>&nbsp; </p>
        </div>
	</div>
	<div id="footer">
        <?php mosLoadModules('footer', -2); ?>
	</div>
</div>

</body>
</html>