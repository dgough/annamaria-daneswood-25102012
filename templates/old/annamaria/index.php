<?php
/**
 * YOOtheme template
 *
 * @author yootheme.com
 * @copyright Copyright (C) 2007 YOOtheme Ltd & Co. KG. All rights reserved.
 */

// no direct access
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );
global $option;

/*** template parameters ***/
$template_parameters = array(
	/* left / right column layout */
	"leftWidth"           => '174px',      	 	/* left hand column width */
	/*"rightWidth"          => '200px', */     	 	/* right hand column width */
	"showLeft"            => 1,      	 	/* left hand column on */
	"showRight"           => 0,      	 	/* right hand column on */
	/* default color variation */
	"color"               => "default",     /* default (white) | grey | turquoise | deepblue | black | blue | green | red | lilac | peach */
	/* item color variation */
	"item1"               => "default",     /* default (grey) | green | pink | orange | blue */

	/* layout */
	"date"    	          => true,          /* true | false */
	"styleswitcherFont"   => false,          /* true | false */
	"styleswitcherWidth"  => false,         /* true | false */
	"layout"              => "left",        /* left | right */
	/* style switcher */
	"fontDefault"         => "font-medium", /* font-small | font-medium | font-large */
	"widthDefault"        => "width-wide",  /* width-thin | width-wide | width-fluid */
	"widthThinPx"         => 780,           /* template width for style "width-thin", (pixels) */
	"widthWidePx"         => 960,           /* template width for style "width-wide", (pixels) */
	"widthFluidPx"        => 0.9,           /* template width for style "width-fluid", (0.9 means 90%) */
	/* javascript */
	"loadJavascript"      => true           /* true | false, set this to enable/disable all the templates javascripts */
);

$template_parameters['showRight'] = ($Itemid==133 ? 1 : 0);

include_once($mosConfig_absolute_path . '/templates/' . $cur_template . '/lib/php/yootools.php');
include_once($mosConfig_absolute_path . '/templates/' . $cur_template . '/lib/php/yoolayout.php');

$template_baseurl = $mosConfig_live_site . '/templates/' . $cur_template;

// add template javascript
if ($yootools->getParam('loadJavascript')) {
	$yootools->addScript($template_baseurl . '/lib/js/mootools.js.php');
	$yootools->addScript($yootools->getJavaScript());
	$yootools->addScript($template_baseurl . '/lib/js/template.js.php');
}

// add template css
$yootools->addStyleSheet($template_baseurl . '/css/template.css.php?color=' . $yootools->getParam('color')
															. '&amp;styleswitcherFont=' . $yootools->getParam('styleswitcherFont')
															. '&amp;styleswitcherWidth=' . $yootools->getParam('styleswitcherWidth')
															. '&amp;widthThinPx=' . $yootools->getParam('widthThinPx')
															. '&amp;widthWidePx=' . $yootools->getParam('widthWidePx')
															. '&amp;widthFluidPx=' . $yootools->getParam('widthFluidPx')
															. '&amp;leftWidth=' . $yootools->getParam('leftWidth')
															. '&amp;rightWidth=' . $yootools->getParam('rightWidth')
															. '&amp;option=' . $option);

$yootools->addStyleSheet($template_baseurl . '/lib/js/lightbox/css/slimbox.css');


/**
 * This sets the header text for the homepage
 */
global $mainframe;
$database->setQuery("SELECT * FROM #__menu WHERE menutype = 'mainmenu' AND parent = 0 AND published = 1 ORDER BY ordering LIMIT 0,1");
$database->loadObject($menu); 
$params = new mosParameters($menu->params);
if($mainframe->getItemid() == $menu->id) $mainframe->_head['title'] = $params->get('header');

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo _LANGUAGE; ?>" xml:lang="<?php echo _LANGUAGE; ?>">
<head>
<?php $yootools->showHead(); ?>
<?php mosShowHead(); ?>
<?php if($my->id) initEditor(); ?>
<meta http-equiv="Content-Type" content="text/html; <?php echo _ISO; ?>" />

<!-- Add YOOtools extension styles here -->
<link href="<?php echo $mosConfig_live_site;?>/modules/mod_yoo_login/mod_yoo_login.css.php" rel="stylesheet" type="text/css" />
<?php /*
<link href="<?php echo $mosConfig_live_site;?>/modules/mod_yoo_slider/mod_yoo_slider.css.php" rel="stylesheet" type="text/css" />
<link href="<?php echo $mosConfig_live_site;?>/modules/mod_yoo_drawer/mod_yoo_drawer.css.php" rel="stylesheet" type="text/css" />
<link href="<?php echo $mosConfig_live_site;?>/modules/mod_yoo_carousel/mod_yoo_carousel.css.php" rel="stylesheet" type="text/css" />
<link href="<?php echo $mosConfig_live_site;?>/modules/mod_yoo_toppanel/mod_yoo_toppanel.css.php" rel="stylesheet" type="text/css" />
<link href="<?php echo $mosConfig_live_site;?>/modules/mod_yoo_accordion/mod_yoo_accordion.css.php" rel="stylesheet" type="text/css" />
*/ ?>
<link href="<?php echo $mosConfig_live_site ?>/components/com_virtuemart/fetchscript.php?gzip=0&amp;subdir[0]=/themes/default/templates/calendar/css&amp;file[0]=calendar.css" type="text/css" rel="stylesheet" />
</head>

<body id="page" class="<?php echo $yootools->getCurrentStyle(); ?> <?php echo $yootools->getParam('leftcolumn'); ?> <?php echo $yootools->getParam('rightcolumn'); ?> <?php echo $yootools->getParam('itemcolor'); ?>">

	<?php if(mosCountModules('absolute')) { ?>
	<div id="absolute">
		<?php mosLoadModules('absolute', -1); ?>
	</div>
	<?php } ?>
	
	<div id="page-body">
		<div class="wrapper floatholder">
			<div class="wrapper-t">
			<div class="wrapper-r">
			<div class="wrapper-b">
			<div class="wrapper-l">
							<div class="wrapper-tl">
								<div class="wrapper-tr">
									<div class="wrapper-bl">
										<div class="wrapper-br">
				
											<div id="header">
												<div class="header-l">
													<div class="header-r">
												
														<div id="toolbar">
															<div class="floatbox ie_fix_floats">
																
																<?php if(mosCountModules('search')) { ?>
																<div id="search">
																	<?php mosLoadModules('search', -1); ?>
																</div>
																<?php } ?>
													
																<?php if($yootools->getParam('styleswitcherFont') || $yootools->getParam('styleswitcherWidth')) { ?>
																<div id="styleswitcher">
																	<?php if($yootools->getParam('styleswitcherWidth')) { ?>
																	<a id="switchwidthfluid" href="javascript:void(0)" title="Fluid width"></a>
																	<a id="switchwidthwide" href="javascript:void(0)" title="Wide width"></a>
																	<a id="switchwidththin" href="javascript:void(0)" title="Thin width"></a>
																	<?php } ?>
																	<?php if($yootools->getParam('styleswitcherFont')) { ?>
																	<a id="switchfontlarge" href="javascript:void(0)" title="Increase font size"></a>
																	<a id="switchfontmedium" href="javascript:void(0)" title="Default font size"></a>
																	<a id="switchfontsmall" href="javascript:void(0)" title="Decrease font size"></a>
																	<?php } ?>
																</div>
																<?php } ?>
																
																<?php mosLoadModules('toolbar', -2); ?>
																
																<?php if($yootools->getParam('date')) { ?>
																<div id="date">
																	<?php 
																		// %A, %e. %B %Y
																		$time = time() + ($mosConfig_offset * 60 * 60);																		
																		echo strftime( '%A, %e. %B %Y', $time );																		
																	?>
																</div>
																<?php } ?>
																
																
															</div>
														</div>
														
														<div id="headerbar">
															<div class="floatbox ie_fix_floats">
															
																<?php if(mosCountModules('topmenu')) { ?>
																<div id="topmenu">
																	<?php mosLoadModules('topmenu', -1); ?>
																</div>
																<?php } ?>
																
																<?php if(mosCountModules('header')) { ?>
																<div id="headermodule">
																	<?php mosLoadModules('header', -3); ?>
																</div>
																<?php } ?>
																
															</div>
														</div>
														<div id="logo">	</div>
														<?php if (mosCountModules('top')) { ?>
														<div id="menu">
															<?php mosLoadModules( 'top', -1 ); ?>
														</div>
														<?php } ?>
																									
													</div>
												</div>
											</div>
											<!-- header end -->
										<?php if (mosCountModules('banner')) { ?>
										<div id="banner">
											<?php mosLoadModules( 'banner', -1 ); ?>
										</div>
										<?php } ?>
										<?php if(mosCountModules('top1') || mosCountModules('top2') || mosCountModules('top3') || mosCountModules('top4')) { ?>
											<div id="top">
												<div class="top-b">
													<div class="top-l">
														<div class="top-r">
															<div class="top-tl">
																<div class="top-tr">
																	<div class="top-bl">
																		<div class="top-br">
																			<div class="floatbox ie_fix_floats">
																		
																				<?php if(mosCountModules('top1')) { ?>
																				<div class="topbox <?php echo $yootools->getParam('topboxwidth'); ?> <?php echo $yootools->getParam('topbox12seperator'); ?> float-left">
																					<?php mosLoadModules('top1', -2); ?>
																				</div>
																				<?php } ?>
																																	
																				<?php if(mosCountModules('top2')) { ?>
																				<div class="topbox <?php echo $yootools->getParam('topboxwidth'); ?> <?php echo $yootools->getParam('topbox23seperator'); ?> float-left">
																					<?php mosLoadModules('top2', -2); ?>
																				</div>
																				<?php } ?>
																				
																				<?php if(mosCountModules('top3')) { ?>
																				<div class="topbox <?php echo $yootools->getParam('topboxwidth'); ?> <?php echo $yootools->getParam('topbox34seperator'); ?> float-left">
																					<?php mosLoadModules('top3', -2); ?>
																				</div>
																				<?php } ?>
																																	
																				<?php if(mosCountModules('top4')) { ?>
																				<div class="topbox <?php echo $yootools->getParam('topboxwidth'); ?> float-left">
																					<?php mosLoadModules('top4', -2); ?>
																				</div>
																				<?php } ?>
																	
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<!-- top end -->
											<?php } ?>
											<div class="outer">
											<div class="outer-l">
											<div class="outer-t">
											<div class="outer-r">											
											<div class="outer-b">
											<div class="outer-tl">
												<div class="outer-tr">
													<div class="outer-bl">
														<div class="outer-br">
															<div id="middle">
																<?php 
																$main_class = '';
																if((mosCountModules('user1') || mosCountModules('user2')) && !($option == 'com_hotproperty' && $task == 'view')) { ?>
																	<div id="maintop">
																		
																		<?php if(mosCountModules('user1')) { ?>
																		<div class="maintopbox">
																			<?php mosLoadModules('user1', -3); ?>
																		</div>
																		<?php } ?>
																		<div id='header_border_top'></div>	
																		<div id='header_border_bottom'></div>
																		<?php if(mosCountModules('user2')) { ?>
																		<div class="adv-search">
																			<?php mosLoadModules('user2', -3); ?>
																		</div>
																		<?php } ?>
																	</div>
																	<!-- maintop end -->
																<?php 
																}else{
																	$main_class = 'no-top';
																}
																?>							
																<div class="background <?php echo $main_class ?>">
																	
																	<?php if(mosCountModules('right2') && $yootools->getParam('showRight')) { ?>
																	<div id="right">
																		<div id="right_container" class="clearfix">
																			<?php mosLoadModules('right2', -3); ?>
																		</div>
																	</div>
																	<!-- right end -->
																	<?php } ?>									
																					
																	<?php if(mosCountModules('left') && $yootools->getParam('showLeft')) { ?>
																	<div id="left">
																		<div id="left_container" class="clearfix">
																		
																		<?php mosLoadModules('left', -3); ?>
								
																		</div>
																	</div>
																	<!-- left end -->
																	<?php } ?>
																	
																	<div id="main">
																		<div id="main_container" class="clearfix">
																					
																			<div id="mainmiddle" class="floatbox">											
															
																				<div id="content">
																					<div id="content_container" class="clearfix">
													
																						<?php if(mosCountModules('advert1') || mosCountModules('advert2')) { ?>
																						<div id="contenttop" class="floatbox">
																					
																							<?php if(mosCountModules('advert1') && mosGetParam($_REQUEST,'page','') == 'booking.index') { ?>
																																														
																							<div class="contenttopbox left <?php echo $yootools->getParam('contenttopboxwidth'); ?> <?php echo $yootools->getParam('contenttopbox12seperator'); ?> float-left adv1">
																								<?php mosLoadModules('advert1', -3); ?>
																								
																							</div>
																							<?php } ?>
																			
																							<?php if(mosCountModules('advert2')) { ?>
																							<div class="contenttopbox right <?php echo $yootools->getParam('contenttopboxwidth'); ?> float-left" call>
																								<?php mosLoadModules('advert2', -3); ?>
																							</div>
																							<?php } ?>
												
																						</div>
																						<!-- contenttop end -->
																						<?php } ?>
                                                                                        
																						
                                                                                        <?php if($option!='com_frontpage') { ?>
																							<div id="breadcrumb"> <?php mosPathWay(); ?>
																							</div>
																							<?php } ?>
                                                                                        
																						<?php
																						$msg = mosGetParam($_REQUEST, 'mosmsg');
																						if($msg){
																						?>					
                                                                                        <div class="mosmessage">
																							<?php echo $msg; ?>
																						</div>
																						<?php } ?>
															
																						<div class="floatbox" id='mainBody'>
																							<?php mosMainBody(); ?>
																						</div>
								
																						<?php if(mosCountModules('advert3') || mosCountModules('advert4')) { ?>
																						<div id="contentbottom" class="floatbox">
																								
																							<?php if(mosCountModules('advert3')) { ?>
																							<div class="contentbottombox left <?php echo $yootools->getParam('contentbottomboxwidth'); ?> <?php echo $yootools->getParam('contentbottombox12seperator'); ?> float-left">
																								<?php mosLoadModules('advert3', -3); ?>
																							</div>
																							<?php } ?>
																			
																							<?php if(mosCountModules('advert4')) { ?>
																							<div class="contentbottombox right <?php echo $yootools->getParam('contentbottomboxwidth'); ?> float-left">
																								<?php mosLoadModules('advert4', -3); ?>
																							</div>
																							<?php } ?>
												
																						</div>
																						<!-- mainbottom end -->
																						<?php } ?>
								
																					</div>
																				</div>
																				<!-- content end -->
												
																			</div>
																			<!-- mainmiddle end -->
								
																		</div>
																	</div>
																	<!-- main end -->
																	<?php if(mosCountModules('user3') || mosCountModules('user4')) { ?>
																	<div id="mainbottom" class="floatbox">
													
																		<?php if(mosCountModules('user3')) { ?>
																		<div class="mainbottombox <?php echo $yootools->getParam('mainbottomboxwidth'); ?> <?php echo $yootools->getParam('mainbottombox12seperator'); ?> float-left">
																			<?php mosLoadModules('user3', -3); ?>
																		</div>
																		<?php } ?>
													
																		<?php if(mosCountModules('user4')) { ?>
																		<div class="mainbottombox <?php echo $yootools->getParam('mainbottomboxwidth'); ?> float-left">
																			<?php mosLoadModules('user4', -3); ?>
																		</div>
																		<?php } ?>
													
																	</div>
																	<!-- mainbottom end -->
																	<?php } ?>										
																</div>
															</div>
															<!-- middle end -->
														
															<?php if(mosCountModules('bottom1') || mosCountModules('bottom2') || mosCountModules('bottom3') || mosCountModules('bottom4')) { ?>
															<div id="bottom">
																<div class="bottom-b">
																	<div class="bottom-l">
																		<div class="bottom-r">
																			<div class="bottom-tl">
																				<div class="bottom-tr">
																					<div class="bottom-bl">
																						<div class="bottom-br">
																							<div class="floatbox ie_fix_floats">
																							
																								<?php if(mosCountModules('bottom1')) { ?>
																								<div class="bottombox <?php echo $yootools->getParam('bottomboxwidth'); ?> <?php echo $yootools->getParam('bottombox12seperator'); ?> float-left">
																									<?php mosLoadModules('bottom1', -2); ?>
																								</div>
																								<?php } ?>
																																													
																								<?php if(mosCountModules('bottom2')) { ?>
																								<div class="bottombox <?php echo $yootools->getParam('bottomboxwidth'); ?> <?php echo $yootools->getParam('bottombox23seperator'); ?> float-left">
																									<?php mosLoadModules('bottom2', -2); ?>
																								</div>
																								<?php } ?>
																								
																								<?php if(mosCountModules('bottom3')) { ?>
																								<div class="bottombox <?php echo $yootools->getParam('bottomboxwidth'); ?> <?php echo $yootools->getParam('bottombox34seperator'); ?> float-left">
																									<?php mosLoadModules('bottom3', -2); ?>
																								</div>
																								<?php } ?>
																																											
																								<?php if(mosCountModules('bottom4')) { ?>
																								<div class="bottombox <?php echo $yootools->getParam('bottomboxwidth'); ?> float-left">
																									<?php mosLoadModules('bottom4', -2); ?>
																								</div>
																								<?php } ?>
																							
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															</div>
															<!-- bottom end -->
															<?php } ?>
														</div>
													</div>
												</div>
											</div>
											</div>
											</div>
											</div>
											</div>
										</div>	
									</div>	
								</div>	
							</div>	
						</div>	
					</div>	
				</div>	
			</div>	
			<div id="page-footer">
				<div class="wrapper floatholder">
				
					<?php if(mosCountModules('footer')) { ?>
					<div id="footer">
						<a class="anchor" href="#page">&nbsp;</a>
						<?php mosLoadModules('footer', -1); ?>
					</div>
					<?php } ?>
					<?php if(mosCountModules('legals')) { ?>
					<div id="legals">
						<?php mosLoadModules('legals', -1); ?>
					</div>
					<?php } ?>
					<?php mosLoadModules( 'debug', -1 );?>
				</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-1415804-1");
pageTracker._trackPageview();
} catch(err) {}</script>
			</div>
			<!-- page-footer end -->			
		</div>		
	</div>
	<!-- page-body end -->						

</body>
</html>