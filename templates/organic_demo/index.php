<?php
/**
* "Minted One-Point-Five" Administrator Template for Joomla 1.0.x - Version 1.0 (index.php)
* License: http://www.gnu.org/copyleft/gpl.html
* Author: Fotis Evangelou
* Date Created: October 16th, 2006
* Copyright (c) 2006 JoomlaWorks.gr - http://www.joomlaworks.gr
* Project page at http://www.joomlaworks.gr - Demos at http://demo.joomlaworks.gr

* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

/** ensure this file is being included by a parent file */
defined( '_VALID_MOS' ) or die( 'Restricted access' );

$tstart = mosProfiler::getmicrotime();
require( 'templates/organic_demo/lang/en.php' );
$iso = explode( '=', _ISO );
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="templates/organic_demo/css/template_css.css" type="text/css" />
<link rel="stylesheet" href="templates/organic_demo/css/theme.css" type="text/css" />
<script type="text/javascript" language="JavaScript" src="templates/organic_demo/js/hideloading.js"></script>
<script type="text/javascript" language="JavaScript" src="templates/organic_demo/js/showhide.js"></script>
<?php
if ( $my->id ) { initEditor(); }
mosShowHead();
?>
</head>
<body>
<div id="minted">
  <div id="header">
    <div id="hc">
      <div id="hl">
        <div id="hr">
          <div id="home"></div>
        </div>
      </div>
    </div>
  </div>
  <div id="content_wrapper">
    <div class="topMenu">
		<?php mosLoadModules("top", -2);?>
		<div class='clear'></div>
	</div>
    <div id="pathway">
      <?php mosPathway(); ?>
      <div class="clr"></div>
    </div>	
  <div id="loading"><?php echo $admin_pageloading; ?></div>


      
		<?php
		if($_GET['option'] == "com_emailmarketing"){
		
		//organic_demo add: Add the main menu bar
		//echo '<div class="menutablerright">';
		//	print NewMenu();
		//echo '</div>';
		}

		?>
    
    <div id="inner" align="center">
      <?php mosMainBody(); ?>
      <div class="clr"></div>
    </div>
  </div>
  <link rel="stylesheet" href="templates/organic_demo/css/tabpane.css" type="text/css" />
  <div class="footer"> </div>
    <?php
			if ( $mosConfig_debug ) {
				echo '<div class="smallgrey">';
				$tend = mosProfiler::getmicrotime();
				$totaltime = ($tend - $tstart);
				printf ("Page was generated in %f seconds", $totaltime);
				echo '</div>';
			}
			?>
  </div>
</div>
<script type="text/javascript" language="JavaScript" src="templates/organic_demo/js/stoploading.js"></script>
<!-- JW "Minted One-Point-Five" Admin Template (v1.0) -->
</body>
</html>
