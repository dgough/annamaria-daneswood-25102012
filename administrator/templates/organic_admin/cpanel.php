<?php
/**
* "Minted One-Point-Five" Administrator Template for Joomla 1.0.x - Version 1.0 (cpanel.php)
* License: http://www.gnu.org/copyleft/gpl.html
* Author: Fotis Evangelou
* Date Created: October 16th, 2006
* Copyright (c) 2006 JoomlaWorks.gr - http://www.joomlaworks.gr
* Project page at http://www.joomlaworks.gr - Demos at http://demo.joomlaworks.gr

* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

/** ensure this file is being included by a parent file */
defined( '_VALID_MOS' ) or die( 'Restricted access' );
global $mosConfig_live_site;
?>
<script language="javascript" type="text/javascript" src="<?php $mosConfig_live_site ?>/includes/js/overlib_mini.js"></script>
<script language="javascript" type="text/javascript" src="<?php $mosConfig_live_site ?>/includes/js/overlib_hideform_mini.js"></script>
<table width="100%" id="controlpanel">
  <tr>
    <td width="62%" valign="top">
    	<?php mosLoadAdminModules( 'icon', 0 ); ?>    	   
		<?php 				
			$url = 'http://www.organic-development.com/xml/';
			$xml = @file_get_contents($url.'products.xml');
			
			if($xml != ''){
				$xmlDoc = new SimpleXMLElement($xml);
					if(count($xmlDoc) > 0){
					?>
					<div class='clear'></div><br /><br />
					<table class="adminheading" border="0">
					<tr>
						<th style="color:#333">Website enhancements from Organic Development</th>
					</tr>
					</table>
					<?php if(isset($xmlDoc->intro)){
						echo '<div id="footer_intro">'.$xmlDoc->intro.'</div>';
					}
					?>
					<div id='footer_icons'>
					<?php 	
									
					foreach($xmlDoc->products->product as $x){

						$att = $x->attributes();
						if(!file_exists($mosConfig_absolute_path.'/components/'.$att['component']) || @$att['force_show']){?>
						<div style="float: left">
							<div class='icon'>
								<a href='<?php echo $att['url'] ?>' target="_blank" title='Click here for more information about <?php echo $att['name'] ?>'>
									<img src='<?php echo $url.$att['icon'] ?>' alt='<?php echo $att['name'] ?>' />
									<span><?php echo $att['name'] ?></span>
								</a>
							</div>
						</div>			
						<?php }
					}
					?>
					</div>
				<?php
				}
			}			
		?>
	</td>
    <td width="38%" valign="top">
		<form action="index2.php" method="post" name="adminForm">
			<?php mosLoadAdminModules( 'cpanel', 1 ); ?>
		</form>
	</td>
  </tr>
</table>
<div class="clr"></div>