<?php

/**
 * Rounded corner class
 *
 */
class rounded {

	/**
	 * Start the rounded corners
	 *
	 * @param unknown_type $id
	 */
	function start($id = '') {
		?>
		<div class='rounded' <?php echo $id ? "id='$id'" : ''?>>
			<div class='round_t'>
				<div class='round_r'>
					<div class='round_b'>
						<div class='round_l'>
							<div class='round_tl'>
								<div class='round_tr'>
									<div class='round_br'>
										<div class='round_bl'>
											<div class='rounded_inner'>
		<?php
	}
	
	/**
	 * Close the rounded corners
	 *
	 */
	function end() {
		?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>	  
					</div>	  
				</div>									  
			</div>		
		</div>		
		<?php
	}
}
?>