<?php
/**
* "Minted One-Point-Five" Administrator Template for Joomla 1.0.x - Version 1.0 (login.php)
* License: http://www.gnu.org/copyleft/gpl.html
* Author: Fotis Evangelou
* Date Created: October 12th, 2006
* Copyright (c) 2006 JoomlaWorks.gr - http://www.joomlaworks.gr
* Project page at http://www.joomlaworks.gr - Demos at http://demo.joomlaworks.gr

* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

/** ensure this file is being included by a parent file */
defined( '_VALID_MOS' ) or die( 'Restricted access' );

$tstart = mosProfiler::getmicrotime();
require( 'templates/organic_admin/lang/en.php' );
require( 'templates/organic_admin/rounded.php' );
require( 'includes/admin.php' );
$rounded = new rounded();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php 
//Enable mootools be default
$mosConfig_mootools = 1;
echo $mainframe->getHead(); 
?>
<style type="text/css">
@import url(templates/organic_admin/css/template_css.css);
</style>
<script language="javascript" type="text/javascript">
	function setFocus() {
		document.loginForm.usrname.select();
		document.loginForm.usrname.focus();
	}
</script>
<link rel="shortcut icon" href="<?php echo $mosConfig_live_site .'/images/favicon.ico';?>" />
</head>
<body onload="setFocus();" id="login">
<div id="minted">
	<?php $rounded->start('header') ?>
		<div id='header_logo'></div>
		<div id="content_wrapper">
		
			<?php $rounded->start('login_form') ?>
				<div class='module_inner'>
					
					<div class="login_credentials">
						<h2><?php echo $login_h1; ?>: New Server</h2>
						<?php
						// handling of mosmsg text in url
						include_once( $mosConfig_absolute_path .'/administrator/modules/mod_mosmsg.php' ); 
						?>
						<noscript>
						<div class="message"><?php echo $login_nojs; ?></div>
						</noscript>
						<form action="index.php" method="post" name="loginForm" id="loginForm">
							<div class='column'>
								<label><?php echo $login_username; ?></label>
								<input name="usrname" type="text" class="inputbox" />
							</div>
							<div class='column'>
								<label><?php echo $login_password; ?></label>
								<input name="pass" type="password" class="inputbox" />
							</div>
							<div class='column'>
								<div class="enter"><input class="enter_inner" type="submit" name="submit" value="<?php echo $login_enter; ?>" /></div>
							</div>
							<div class='clear'></div>
						</form>	
						<?php echo $login_welcometext; ?> 		
					</div>
				</div>
			<?php $rounded->end() ?>
			<?php /*
			<?php $rounded->start('left') ?><div class='module_inner'><h3 class='title'>Organic Development Products</h3><?php mosLoadAdminModules( 'left' ); ?>&nbsp;</div><?php $rounded->end() ?>
			<div id='middle'>
				
				<?php $rounded->start('footer') ?><div class='module_inner'><h3 class='title'>Organic Development News</h3><?php mosLoadAdminModules( 'user1' ); ?></div><?php $rounded->end() ?>
				<?php $rounded->start('pagerank') ?><div class='module_inner'><h3 class='title'>Your Site Pagerank</h3><?php mosLoadAdminModules( 'user2' ); ?></div><?php $rounded->end() ?>
				<?php $rounded->start('keywords') ?><div class='module_inner'><h3 class='title'>Keyword Checker</h3><script type="text/javascript" src="http://freekeywords.wordtracker.com/plugin/search.js"></script></div><?php $rounded->end() ?>
			</div>
			<?php $rounded->start('right') ?><div class='module_inner'><h3 class='title'>Organic Development Services</h3><?php mosLoadAdminModules( 'right' ); ?>&nbsp;</div><?php $rounded->end() ?>
			*/ ?>
			<div class='clear'></div>
		</div>
	<?php $rounded->end() ?>							
</div>								
<div class="footer"></div>
<!-- JW "Minted One-Point-Five" Admin Template (v1.0) -->
</body>
</html>
