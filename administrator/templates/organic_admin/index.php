<?php
/**
* "Minted One-Point-Five" Administrator Template for Joomla 1.0.x - Version 1.0 (index.php)
* License: http://www.gnu.org/copyleft/gpl.html
* Author: Fotis Evangelou
* Date Created: October 16th, 2006
* Copyright (c) 2006 JoomlaWorks.gr - http://www.joomlaworks.gr
* Project page at http://www.joomlaworks.gr - Demos at http://demo.joomlaworks.gr

* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

/** ensure this file is being included by a parent file */
defined( '_VALID_MOS' ) or die( 'Restricted access' );

$tstart = mosProfiler::getmicrotime();
require( 'templates/organic_admin/lang/en.php' );
require( 'templates/organic_admin/rounded.php' );
$rounded = new rounded();
$iso = explode( '=', _ISO );
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php 
echo $mainframe->setPageTitle($admin_title);
echo $mainframe->getHead();
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo $mosConfig_live_site; ?>/administrator/templates/organic_admin/css/template_css.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $mosConfig_live_site; ?>/administrator/templates/organic_admin/css/theme.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $mosConfig_live_site; ?>/administrator/templates/organic_admin/css/tabpane.css" type="text/css" />
<script type="text/javascript" src="<?php echo $mosConfig_live_site; ?>/administrator/templates/organic_admin/js/hideloading.js"></script>
<script type="text/javascript" src="<?php echo $mosConfig_live_site; ?>/includes/js/JSCookMenu_mini.js"></script>
<script type="text/javascript" src="<?php echo $mosConfig_live_site; ?>/administrator/includes/js/ThemeOffice/theme.js"></script>
<script type="text/javascript" src="<?php echo $mosConfig_live_site; ?>/includes/js/joomla.javascript.js"></script>
<script type="text/javascript" src="<?php echo $mosConfig_live_site ?>/includes/js/overlib_mini.js"></script>
<script type="text/javascript" src="<?php echo $mosConfig_live_site ?>/includes/js/overlib_hideform_mini.js"></script>
<?php
include_once( $mosConfig_absolute_path . '/editor/editor.php' );
initEditor();
?>
</head>
<body onload="MM_preloadImages('images/help_f2.png','images/archive_f2.png','images/back_f2.png','images/cancel_f2.png','images/delete_f2.png','images/edit_f2.png','images/new_f2.png','images/preview_f2.png','images/publish_f2.png','images/save_f2.png','images/unarchive_f2.png','images/unpublish_f2.png','images/upload_f2.png')">
<div id="minted">
		<?php $rounded->start('header') ?>
			<div id='header_logo'>
				<div id='version'>Version 1</div>
				<div id="sitename"><?php echo $mosConfig_sitename ?></div>
			</div>

			<div id="content_wrapper">
				<div class="menubar">
					<div class="mod_fullmenu"><?php if($my->usertype == 'Super Administrator') mosLoadAdminModule( 'fullmenu' );?></div>
					<div class="logout"><a href="index2.php?option=logout"><?php echo $admin_logout; ?></a><?php echo $my->username;?></div>
					<div class="mod_header"><?php mosLoadAdminModules( 'header', 2 );?></div>
					<div id="loading"><?php echo $admin_pageloading; ?></div>
					<div class="clr"></div>
				</div>
				<div class="menubar">
					<table class="pathway_toolbar white">
						<tr>
							<td class="mod_pathway"><?php mosLoadAdminModule( 'pathway' );?></td>
							<td class="mod_toolbar" align="right"><?php mosLoadAdminModule( 'toolbar' );?></td>
						</tr>
					</table>
				<div class="clr"></div>
				</div>
				<div id="inner">
					<?php mosLoadAdminModule( 'mosmsg' );?>
					<?php mosMainBody_Admin(); ?>
					<div class="clr"></div>
				</div>
			</div>
			<div class="footer"></div>
			<?php
			if ( $mosConfig_debug ) {
				echo '<div class="smallgrey">';
				$tend = mosProfiler::getmicrotime();
				$totaltime = ($tend - $tstart);
				printf ("Page was generated in %f seconds", $totaltime);
				echo '</div>';
			}
			?>
	<?php $rounded->end() ?>									
</div>							
<?php mosLoadAdminModules( 'debug' );?>
<script type="text/javascript" language="JavaScript" src="templates/organic_admin/js/stoploading.js"></script>
<!-- JW "Minted One-Point-Five" Admin Template (v1.0) -->
</body>
</html>
