<?php
// $Id: admin.sm2emailmarketing.php,v 1.25 2008/04/21 05:13:03 sm2tony Exp $
/**
 * sm2emailmarketing - Email Marketing Newsletter
 *
 * Joomla! Newsletter Component that supports multiple list mailing and message queueing
 *
 * @package Joomla!
 * @subpackage com_sm2emailmarketing
 * @copyright Copyright (C) 2006 SM2 / All Rights Reserved
 * @license commercial
 * @author SM2 <info@sm2joomla.com>
 */

/**
 * Makes sure this is included by a parent file
 */
defined('_VALID_MOS') or die('Direct Access to this location is not allowed.');

// sm2emailmarketing error handling:
require_once ($mosConfig_absolute_path . '/administrator/components/'.$option.'/includes/errorhandler.admin.php');

global $emErrorHandler, $sm2emailmarketingConfig;
$emErrorHandler = new emError;

// include language file, or default to english
if (file_exists ($mosConfig_absolute_path . '/components/'.$option.'/languages/'.$mosConfig_lang.'.php')) {
    require_once ($mosConfig_absolute_path . '/components/'.$option.'/languages/'.$mosConfig_lang.'.php');
} else {
    require_once ($mosConfig_absolute_path . '/components/'.$option.'/languages/english.php');
} // end if

/**
 * Include required files
 */
require_once($mainframe->getPath('admin_html'));
require_once($mainframe->getPath('class'));
require_once($mosConfig_absolute_path.'/administrator/components/'.$option.'/licence.php');

// include the task file
sm2emailmarketingInclude($task, $option);

// check for upgrades
sm2EM_CheckUpgrade($option, $task);

// determine if lists are enabled
if (getPluginEnabledFromFilename($option, 'list.plugin.php')) {
    define('_SM2EM_LISTS', 1);
}

$cid = mosGetParam($_REQUEST, 'cid', array(0));

// check to see if there is a change in the registered users
updatesm2emailmarketingUsers();

// switch by task
switch ($task) {

    /**
     * List Tasks
     */
    case 'showlists':
        showLists($option);
        break;
    case 'newlist':
        $cid=array(0);
    case 'copylist':
    case 'editlist':
        editList($cid[0], $option, $task);
        break;
    case 'savelist':
    case 'applylist':
        saveList($option, $task);
        break;
    case 'deletelist':
        deleteList($cid, $option);
        break;
    case 'publishlist':
        publishLists($cid, 1, $option);
        break;
    case 'unpublishlist':
        publishLists($cid, 0, $option);
        break;
    case 'accesspubliclist':
        accessList($cid[0], 0, $option);
        break;
    case 'accessregisteredlist':
        accessList($cid[0], 1, $option);
        break;
    case 'accessspeciallist':
        accessList($cid[0], 2, $option);
        break;
    case 'alllist':
        allList($cid[0], $option);
        break;
    case 'nonelist':
        noneList($cid[0], $option);
        break;

    /**
     * Template Tasks
     */
    case 'showtemplates':
        showTemplates($option);
        break;
    case 'newtemplate':
        $cid=array(0);
    case 'copytemplate':
    case 'edittemplate':
        editTemplate($cid[0], $option, $task);
        break;
    case 'savetemplate':
    case 'applytemplate':
    case 'applypreviewtemplate':
        saveTemplate($option, $task);
        break;
    case 'deletetemplate':
        deleteTemplate($cid,$option);
        break;
    case 'publishtemplate':
        publishTemplates($cid, 1, $option);
        break;
    case 'unpublishtemplate':
        publishTemplates($cid, 0, $option);
        break;
    case 'previewtemplate':
        previewTemplate($cid[0], $option);
        break;

    /**
     * Registered User Tasks
     */
    case 'showregistered':
        showRegistered($option);
        break;
    case 'editregistered':
        editRegistered($cid[0], $option);
        break;
    case 'saveregistered':
    case 'applyregistered':
        saveRegistered($option, $task);
        break;
    case 'confirmregistered':
        confirmRegistered($cid, 1, $option);
        break;
    case 'unconfirmregistered':
        confirmRegistered($cid, 0, $option);
        break;
    case 'confirmallregistered':
        confirmAllRegistered(1, $option);
        break;
    case 'unconfirmallregistered':
        confirmAllRegistered(0, $option);
        break;
    case 'unsubscriberegistered':
    	unsubscribeRegistered($cid, $option);
    	break;
    case 'htmlregistered':
        receiveHTMLRegistered($cid, 1, $option);
        break;
    case 'unhtmlregistered':
        receiveHTMLRegistered($cid, 0, $option);
        break;
    case 'htmlallregistered':
        receiveHTMLAllRegistered(1, $option);
        break;
    case 'unhtmlallregistered':
        receiveHTMLAllRegistered(0, $option);
        break;

    /**
     * Unregistered User Tasks
     */
    case 'showunregistered':
        showUnregistered($option);
        break;
    case 'newunregistered':
        $cid=array(0);
    case 'editunregistered':
        editUnregistered($cid[0], $option);
        break;
    case 'saveunregistered':
    case 'applyunregistered':
        saveUnregistered($option, $task);
        break;
    case 'deleteunregistered':
        deleteUnregistered($cid, $option);
        break;
    case 'unsubscribeunregistered':
    	unsubscribeUnregistered($cid, $option);
    	break;
    case 'confirmunregistered':
        confirmUnregistered($cid, 1, $option);
        break;
    case 'unconfirmunregistered':
        confirmUnregistered($cid, 0, $option);
        break;
    case 'confirmallunregistered':
        confirmAllUnregistered(1, $option);
        break;
    case 'unconfirmallunregistered':
        confirmAllUnregistered(0, $option);
        break;
    case 'htmlunregistered':
        receiveHTMLUnregistered($cid, 1, $option);
        break;
    case 'unhtmlunregistered':
        receiveHTMLUnregistered($cid, 0, $option);
        break;
    case 'htmlallunregistered':
        receiveHTMLAllUnregistered(1, $option);
        break;
    case 'unhtmlallunregistered':
        receiveHTMLAllUnregistered(0, $option);
        break;
    case 'importunregistered':
        importUnregistered($option);
        break;

    /**
     * Message Tasks
     */
    case 'showmessages':
        showMessages($option);
        break;
    case 'newmessage':
        $cid=array(0);
    case 'copymessage':
    case 'editmessage':
        editMessage($cid[0], $option, $task);
        break;
    case 'checkmessage':
        checkMessage($cid[0], $option, $task);
        break;
    case 'sendtestmessage':
    	sendtestMessage($cid[0], $option, $task);
    	break;
    case 'savemessage':
    case 'applymessage':
    case 'savepreviewmessage':
        saveMessage($option, $task);
        break;
    case 'sendmessage':
        sendMessage($cid[0], $option);
        break;
    case 'previewmessage':
    	previewMessage($cid[0], $option, $task);
    	break;
    case 'showarchivemessage':
        showMessageArchive($cid[0], $option);
        break;
    case 'deletemessage':
        deleteMessage($cid, $option);
        break;

    /**
     * Statistics Tasks
     */
    case 'showstatistics':
        showStatistics($option);
        break;

    case 'showmessagestatistics':
        showMessageStats($cid[0], $option);
        break;
    case 'showstatisticsgraph':
        showStatisticsGraph($cid[0], $option);
        break;

    /**
     * Queue Tasks
     */
    case 'showqueue':
        showQueue($option);
        break;
    case 'processqueue':
        processQueue($cid[0], $option);
        break;
    case 'deletequeue':
        deleteQueue($cid[0], $option);
        break;

    /**
     * Bounce Tasks
     */
    case 'showbounce':
        showBounce($option);
        break;


    /**
     * Configuration Tasks
     */
    case 'showconfiguration':
        editConfiguration($option);
        break;
    case 'saveconfiguration':
    case 'applyconfiguration':
        saveConfiguration($option, $task);
        break;

    /**
     * Plugin Tasks
     */
    case 'showplugins':
        showPlugins($option);
        break;
    case 'editplugin':
        editPlugin($cid[0], $option);
        break;
    case 'saveplugin':
    case 'applyplugin':
        savePlugin($option, $task);
        break;
    case 'orderupplugin':
        orderPlugin($cid[0], -1, $option);
        break;
    case 'orderdownplugin':
        orderPlugin($cid[0], 1, $option);
        break;
    case 'saveorderplugin':
        savePluginOrder($cid, $option);
        break;
    case 'enableplugin':
        changePluginState($cid, 1, $option);
        break;
    case 'disableplugin':
        changePluginState($cid, 0, $option);
        break;

    /**
     * Tool Tasks
     */
    case 'showtools':
        showTools($option);
        break;
    case 'installtools':
    case 'installdefault':
        sm2em_installDefault($option);
        break;
    case 'cleantools':
        cleanTemporaryTables($option);
        break;
    case 'deltools':
        delTools($option);
        break;


    case 'createThumbnail':
    	createThumbnail($option);
    	break;



    case 'showhelp':
        include_once($mosConfig_absolute_path.'/administrator/components/'.$option.'/sm2emailmarketing.upgrade.php' );
        HTML_sm2emailmarketing::showHelp($option);
        break;

    default:
        // make the message admin the homepage
        mosRedirect('index2.php?option='.$option.'&task=showmessages');

} // end switch


function sm2emailmarketingReturnToolTip($constant, $image='') {

    // determine if the constant exists
    if (!defined($constant)) return '';
    // determine if the constant is empty
    $title = constant($constant);
    if (empty($title)) return $constant;

    // determine if the desciption constant exists
    if (!defined($constant.'_DESC')) return $title;
    // determine if the desciption constant is empty
    $tooltip = constant($constant.'_DESC');
    if (empty($tooltip)) return $title;

    $href = '#';
    if ($image=='') {
        $text = $title;
    } else {
        $text = '';
    }
    if (defined('_JLEGACY')) {
        $href='';
    }

    // if we get this far then we should build the tool tip
    return '<span class="editlinktip">'.mosToolTip(addslashes($tooltip), $title, '', $image, $text, $href, 0).'</span>';

} // sm2emailmarketingReturnToolTip()


function createThumbnail($option) {
	global $mosConfig_absolute_path;
    require_once($mosConfig_absolute_path.'/administrator/components/'.$option.'/includes/thumbnailImage.class.php');
    $thumb = new thumbnailImage();

	$image = mosGetParam($_GET, 'image', '');
	if (empty($image)) {
		$thumb->setSrcImage($mosConfig_absolute_path.'/images/stories/no-thumbnail.gif');
	} else {
		$thumb->setSrcImage($mosConfig_absolute_path.'/images/stories/'.$image);
	}

    // set parameters
    $thumb->setImgHeight('200');
    $thumb->setImgWidth('200');


    // Execute command:
    $thumb->createThumbnail();
} // createThumbnail()


/**
 * Tool Functions
 *
 * Look at moving these into their own include file
 */
function showTools($option) {
    global $sm2emailmarketingConfig;

    $GLOBALS['emktgSection']='tool';

    // determine if the tasks should be shown
    $showClean = 1;
    if ($sm2emailmarketingConfig->get('temp_tables', 'temporary')=='temporary') {
        $showClean = 0;
    }
    $showLists = 1;
    if (!defined('_SM2EM_LISTS')) {
        $showLists = 0;
    }

    // load the language elements
    $lang = sm2emailmarketingLanguage();

    // display the form
    $tmpl = sm2emailmarketingPatTemplate('tools.tpl', $option);

    $tmpl->addVar('header', 'FORM_NAME', _EMKTG_TOOLS_FORM);
    $tmpl->addObject('tools', $lang, 'LANG');
    $tmpl->addVar('tools', 'showclean', $showClean);
    $tmpl->addVar('tools', 'showlists', $showLists);

    $tmpl->displayParsedTemplate('tools');

} // showTools


function sm2em_installDefault($option) {
    global $database, $sm2emailmarketingConfig, $emErrorHandler, $mosConfig_fromname, $mosConfig_mailfrom, $mosConfig_live_site;
    $GLOBALS['emktgSection']='tool';

    $msg = _EMKTG_INSTALL_DEFAULT_DONE;

    // check to see if we need to create the default
    $database->setQuery('SELECT COUNT(*) FROM #__emktg_template'
        .' WHERE template_name='.$database->Quote(_EMKTG_DEFAULT_TEMPLATE_NAME));
    $templatecount = $database->loadResult();

    $database->setQuery('SELECT COUNT(*) FROM #__emktg_message'
        .' WHERE subject='.$database->Quote(_EMKTG_DEFAULT_MESSAGE_SUBJECT));
    $messagecount = $database->loadResult();

    $count = $templatecount + $messagecount;

    if ($count == 0) {
        // insert a default template
        $row = new sm2emailmarketingTemplate();
        $row->templateid = 0;
        $row->template_name = _EMKTG_DEFAULT_TEMPLATE_NAME;
        $row->layout_text = _EMKTG_DEFAULT_TEMPLATE_TEXT;
        $row->layout_html = _EMKTG_DEFAULT_TEMPLATE_HTML;
        $row->published = 1;

        if (!$row->check()) {
            $emErrorHandler->addError($row->getError(), true);
        }

        if (!$row->store()) {
            $emErrorHandler->addError($row->getError(), true);
        }
        $templateid = $row->templateid;

        // insert a default message
        $row = new sm2emailmarketingMessage();
        $row->messageid = 0;
        $row->subject = _EMKTG_DEFAULT_MESSAGE_SUBJECT;
        $row->fromname = $sm2emailmarketingConfig->get('fromname', $mosConfig_fromname);
        $row->fromemail = $sm2emailmarketingConfig->get('fromemail', $mosConfig_mailfrom);
        $row->bounceemail = $sm2emailmarketingConfig->get('bounceemail', $mosConfig_mailfrom);
        $row->message_html = str_replace('[BASEURL]', $mosConfig_live_site, _EMKTG_DEFAULT_MESSAGE_HTML);
        $row->message_text = _EMKTG_DEFAULT_MESSAGE_TEXT;
        $row->templateid = $templateid;
        $row->attachments = '';
        $row->content_items = '';
        $row->intro_only = 0;
        $row->send_date = '';

        if (!$row->check()) {
            $emErrorHandler->addError($row->getError(), true);
        }

        if (!$row->store()) {
            $emErrorHandler->addError($row->getError(), true);
        }
    } else {
        // already existing
        $msg = _EMKTG_INSTALL_DEFAULT_FAILED;
    }

    // load the language elements
    $lang = sm2emailmarketingLanguage();

    // display the form
    $tmpl = sm2emailmarketingPatTemplate('tools.tpl', $option);

    $tmpl->addVar('header', 'FORM_NAME', _EMKTG_INSTALL_DEFAULT);
    $tmpl->addObject('installdefault', $lang, 'LANG');
    $tmpl->addVar('installdefault', 'INSTALL_DEFAULT_MSG', $msg);

    $tmpl->displayParsedTemplate('installdefault');

} // sm2em_installDefault()


function cleanTemporaryTables($option) {
    $GLOBALS['emktgSection']='tool';

    $droppedTables = cleanupSessionTemporaryTables();

    foreach ($droppedTables as $index=>$value) {
        $droppedTables[$index] = new stdClass();
        $droppedTables[$index]->table = str_replace('`', '', $value);
    }

    // load the language elements
    $lang = sm2emailmarketingLanguage();

    // display the form
    $tmpl = sm2emailmarketingPatTemplate('tools.tpl', $option);

    $tmpl->addVar('header', 'FORM_NAME', _EMKTG_TOOLS_CLEAN_FORM);
    $tmpl->addObject('clean', $lang, 'LANG');
    $tmpl->addVar('clean', 'COUNT', count($droppedTables));
    $tmpl->addObject('cleanrows', $droppedTables);

    $tmpl->displayParsedTemplate('clean');

} // cleanTemporaryTables()


function delTools($option) {
    global $mosConfig_absolute_path;

    $type = mosGetParam($_GET, 'type', '');

    // load the language elements
    $lang = sm2emailmarketingLanguage();

    switch ($type) {
        case 'messages':
            $msgs = array_merge(_deleteMessages(), _deleteStatistics());
            $header = _EMKTG_TOOLS_DELETE_MESSAGES;
            break;
        case 'statistics':
            $msgs = _deleteStatistics();
            $header = _EMKTG_TOOLS_DELETE_STATISTICS;
            break;
        case 'subscribers':
            $msgs = array_merge(_deleteSubscribers(), _deleteStatistics());
            $header = _EMKTG_TOOLS_DELETE_SUBSCRIBERS;
            break;
        case 'lists':
            $msgs = array_merge(_deleteLists(), _deleteSubscribers(), _deleteStatistics());
            $header = _EMKTG_TOOLS_DELETE_LISTS;
            break;
        case 'all':
            $msgs = array_merge(
                _deleteMessages(),
                _deleteStatistics(),
                _deleteSubscribers(),
                _deleteLists(),
                _deleteOthers()
            );
            $header = _EMKTG_TOOLS_DELETE_ALL;
            // load the plugin data to fix errors
            require_once($mosConfig_absolute_path.'/administrator/components/'.$option.'/includes/plugin.admin.php');
            updatePlugins($option);
            break;
        default:
            $msgs = array(_EMKTG_TOOLS_DELETE_UNKNOWN);
            $header = _EMKTG_TOOLS_FORM;
            break;
    }

    $msgs = array_unique($msgs);

    // display the form
    $tmpl = sm2emailmarketingPatTemplate('tools.tpl', $option);

    $tmpl->addVar('header', 'FORM_NAME', $header);
    $tmpl->addObject('del', $lang, 'LANG');
    $tmpl->addVar('del', 'msgs', implode('<br />', $msgs));

    $tmpl->displayParsedTemplate('del');

} // delTools()

function _deleteMessages() {
    global $database;

    $msgs = array();

    $database->setQuery('TRUNCATE TABLE #__emktg_message');
    if (!$database->query()) {
        $msgs[] = str_replace('%t', _EMKTG_MESSAGE, _EMKTG_TOOLS_DELETE_FAILED);
    }

    $database->setQuery('TRUNCATE TABLE #__emktg_message_plugin');
    if (!$database->query()) {
        $msgs[] = str_replace('%t', _EMKTG_MESSAGE, _EMKTG_TOOLS_DELETE_FAILED);
    }

    if (count($msgs)) {
        return $msgs;
    } else {
        return array(str_replace('%t', _EMKTG_MESSAGE, _EMKTG_TOOLS_DELETE_SUCCESS));
    }

} // _deleteMessages()


function _deleteStatistics() {
    global $database;

    $msgs = array();

    $database->setQuery('TRUNCATE TABLE #__emktg_stats_message');
    if (!$database->query()) {
        $msgs[] = str_replace('%t', _EMKTG_HELP_STATISTICS, _EMKTG_TOOLS_DELETE_FAILED);
    }

    $database->setQuery('TRUNCATE TABLE #__emktg_stats_overall');
    if (!$database->query()) {
        $msgs[] = str_replace('%t', _EMKTG_HELP_STATISTICS, _EMKTG_TOOLS_DELETE_FAILED);
    }

    if (count($msgs)) {
        return $msgs;
    } else {
        return array(str_replace('%t', _EMKTG_HELP_STATISTICS, _EMKTG_TOOLS_DELETE_SUCCESS));
    }
} // _deleteStatistics()


function _deleteSubscribers() {
    global $database;

    $msgs = array();

    $database->setQuery('TRUNCATE TABLE #__emktg_list_subscriber');
    if (!$database->query()) {
        $msgs[] = str_replace('%t', _EMKTG_SUBSCRIBERS, _EMKTG_TOOLS_DELETE_FAILED);
    }

    $database->setQuery('TRUNCATE TABLE #__emktg_list_user');
    if (!$database->query()) {
        $msgs[] = str_replace('%t', _EMKTG_SUBSCRIBERS, _EMKTG_TOOLS_DELETE_FAILED);
    }

    $database->setQuery('TRUNCATE TABLE #__emktg_subscriber');
    if (!$database->query()) {
        $msgs[] = str_replace('%t', _EMKTG_SUBSCRIBERS, _EMKTG_TOOLS_DELETE_FAILED);
    }

    $database->setQuery('TRUNCATE TABLE #__emktg_user');
    if (!$database->query()) {
        $msgs[] = str_replace('%t', _EMKTG_SUBSCRIBERS, _EMKTG_TOOLS_DELETE_FAILED);
    }

    if (count($msgs)) {
        return $msgs;
    } else {
        return array(str_replace('%t', _EMKTG_SUBSCRIBERS, _EMKTG_TOOLS_DELETE_SUCCESS));
    }
} // _deleteSubscribers()


function _deleteLists() {
    global $database;

    $msgs = array();

    $database->setQuery('TRUNCATE TABLE #__emktg_list');
    if (!$database->query()) {
        $msgs[] = str_replace('%t', _EMKTG_LIST, _EMKTG_TOOLS_DELETE_FAILED);
    }

    if (count($msgs)) {
        return $msgs;
    } else {
        return array(str_replace('%t', _EMKTG_LIST, _EMKTG_TOOLS_DELETE_SUCCESS));
    }
} // _deleteLists()


function _deleteOthers() {

    global $database;

    $msgs = array();

    $database->setQuery('TRUNCATE TABLE #__emktg_plugin');
    if (!$database->query()) {
        $msgs[] = str_replace('%t', _EMKTG_HELP_PLUGINS, _EMKTG_TOOLS_DELETE_FAILED);
    } else {
        $msgs[] = str_replace('%t', _EMKTG_HELP_PLUGINS, _EMKTG_TOOLS_DELETE_SUCCESS);
    }

    $database->setQuery('TRUNCATE TABLE #__emktg_template');
    if (!$database->query()) {
        $msgs[] = str_replace('%t', _EMKTG_TEMPLATE, _EMKTG_TOOLS_DELETE_SUCCESS);
    } else {
        $msgs[] = str_replace('%t', _EMKTG_TEMPLATE, _EMKTG_TOOLS_DELETE_SUCCESS);
    }

    $msg = array_unique($msgs);

    return $msgs;
} // _deleteOthers()
?>