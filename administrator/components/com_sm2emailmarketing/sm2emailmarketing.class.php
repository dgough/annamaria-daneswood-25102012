<?php
// $Id: sm2emailmarketing.class.php,v 1.81 2008/04/21 20:27:06 sm2tony Exp $
/**
 * sm2emailmarketing - Email Marketing Newsletter
 *
 * Joomla! Newsletter Component that supports multiple list mailing and message queueing
 *
 * @package Joomla!
 * @subpackage com_sm2emailmarketing
 * @copyright Copyright (C) 2006 SM2 / All Rights Reserved
 * @license commercial
 * @author SM2 <info@sm2joomla.com>
 */

/**
 * Makes sure this is included by a parent file
 */
defined('_VALID_MOS') or die('Direct Access to this location is not allowed.');

/**
 * sm2emailmarketing Classes
 *
 * Database Table classes for the Events_Sessions and Events_Registrations tables
 */

class sm2emailmarketingTemplate extends mosDBTable {

    var $templateid = null;
    var $template_name = null;
    var $layout_text = null;
    var $layout_html = null;
    var $published = null;

    /**
     * Constructor
     */
    function sm2emailmarketingTemplate() {
        global $database;
        $this->mosDBTable('#__emktg_template', 'templateid', $database);
    } // sm2emailmarketingTemplate()

    function publish_array($cid=null, $publish=1) {
        if (!is_array($cid) || count($cid) < 1) {
            $this->_error = 'No Templates selected.';
            return false;
        }

        $cids = implode( ',', $cid );

        $this->_db->setQuery('UPDATE '.$this->_tbl.' SET published='.(int)$publish
        . ' WHERE '.$this->_tbl_key.' IN ('.$cids.')'
        );
        if (!$this->_db->query()) {
            $this->_error = $this->_db->getErrorMsg();
            return false;
        }

        $this->_error = '';
        return true;
    } // publish_array()

    function deleteCheck($id) {
        $k = $this->_tbl_key;
        if ($id) {
            $this->$k = intval($id);
        }
		//only allow deletion if template doesn't have any messages (sent or unsent) associated with it

        $this->_db->setQuery('SELECT COUNT(messageid) FROM #__emktg_message WHERE '.$this->_tbl_key.' = '.(int)$this->$k);
		//echo $this->_db->getQuery();
		//exit();
		if (!$this->_db->query()) {
            $this->_error = $this->_db->getErrorMsg();
            return false;
        }
        if ($this->_db->loadresult()) {
            $this->_error = 'Can not delete this template because there are messages that are associated with it';
            return true;
        }

        return false;

    } // deleteCheck()

} // class - sm2emailmarketingTemplate


class sm2emailmarketingList extends mosDBTable {

    var $listid = null;
    var $list_name = null;
    var $description = null;
    var $subscribe = null;
    var $unsubscribe = null;
    var $access = null;
    var $published = null;

    /**
     * Constructor
     */
    function sm2emailmarketingList() {
        global $database;
        $this->mosDBTable('#__emktg_list', 'listid', $database);
    } // sm2emailmarketingList()

    function publish_array($cid=null, $publish=1) {
        if (!is_array($cid) || count($cid) < 1) {
            $this->_error = _EMKTG_LIST_PUBLISH_ERROR;
            return false;
        }

        $cids = implode(',', $cid);

        $this->_db->setQuery('UPDATE '.$this->_tbl.' SET published='.(int)$publish
        . ' WHERE '.$this->_tbl_key.' IN ('.$cids.')'
        );
        if (!$this->_db->query()) {
            $this->_error = $this->_db->getErrorMsg();
            return false;
        }

        $this->_error = '';
        return true;
    } // publish_array()

    function deleteRelated($id) {
        $k = $this->_tbl_key;
        if ($id) {
            $this->$k = intval($id);
        }

        $this->_db->setQuery('DELETE FROM #__emktg_message_list WHERE '.$this->_tbl_key.' = '.(int)$this->$k);

        if (!$this->_db->query()) {
            $this->_error = $this->_db->getErrorMsg();
            return false;
        }

        $this->_db->setQuery('DELETE FROM #__emktg_list_user WHERE '.$this->_tbl_key.' = '.(int)$this->$k);

        if (!$this->_db->query()) {
            $this->_error = $this->_db->getErrorMsg();
            return false;
        }

        $this->_db->setQuery('DELETE FROM #__emktg_list_subscriber WHERE '.$this->_tbl_key.' = '.(int)$this->$k);

        if (!$this->_db->query()) {
            $this->_error = $this->_db->getErrorMsg();
            return false;
        }

        return true;

    } // deleteRelated()


    function addAllRegistered() {
        if (empty($this->listid)) return false;

        $this->_db->setQuery('INSERT IGNORE INTO #__emktg_list_subscriber'
            .' SELECT '.(int)$this->listid.' , - subscriberid'
            .' FROM #__emktg_subscriber'
            .' WHERE unsubscribe_date=0 AND confirmed=1');
        if (!$this->_db->query()) return false;

        return $this->_db->getAffectedRows();
    } // addAllRegistered()


    function addAllUnregistered() {
        if (empty($this->listid)) return false;

        $this->_db->setQuery('INSERT IGNORE INTO #__emktg_list_user'
            .' SELECT '.(int)$this->listid.' , id'
            .' FROM #__emktg_user'
            .' WHERE unsubscribe_date=0 AND confirmed=1');
        if (!$this->_db->query()) return false;

        return $this->_db->getAffectedRows();
    } // addAllUnregistered()


    function removeAllRegistered() {
        if (empty($this->listid)) return false;

        $this->_db->setQuery('DELETE FROM #__emktg_list_subscriber'
            .' WHERE listid='.(int)$this->listid);
        if (!$this->_db->query()) return false;

        return $this->_db->getAffectedRows();
    } // removeAllRegistered()


    function removeAllUnregistered() {
        if (empty($this->listid)) return false;

        $this->_db->setQuery('DELETE FROM #__emktg_list_user'
            .' WHERE listid='.(int)$this->listid);
        if (!$this->_db->query()) return false;

        return $this->_db->getAffectedRows();
    } // removeAllUnregistered()

} // class - sm2emailmarketingList


class sm2emailmarketingMessage extends mosDBTable {

    var $messageid = null;
    var $subject = null;
    var $fromname = null;
    var $fromemail = null;
    var $bounceemail = null;
    var $message_html = null;
    var $message_text = null;
    var $templateid = null;
    var $attachments = null;
    var $content_items = null;
    var $intro_only = null;
    var $send_date = null;

    /**
     * Constructor
     */
    function sm2emailmarketingMessage() {
        global $database;
        $this->mosDBTable('#__emktg_message', 'messageid', $database);
    } // sm2emailmarketingMessage()


    function deleteRelated($id) {
        $k = $this->_tbl_key;
        if ($id) {
            $this->$k = intval($id);
        }

        $this->_db->setQuery('DELETE FROM #__emktg_message_plugin'
            .' WHERE `'.$this->_tbl_key.'` = '.(int)$this->$k);

        if (!$this->_db->query()) {
            $this->_error = $this->_db->getErrorMsg();
            return false;
        }

        $this->_db->setQuery('DELETE FROM #__emktg_stats_message'
            .' WHERE `'.$this->_tbl_key.'` = '.(int)$this->$k);

        if (!$this->_db->query()) {
            $this->_error = $this->_db->getErrorMsg();
            return false;
        }

        $this->_db->setQuery('DELETE FROM #__emktg_stats_overall'
            .' WHERE `'.$this->_tbl_key.'` = '.(int)$this->$k);

        if (!$this->_db->query()) {
            $this->_error = $this->_db->getErrorMsg();
            return false;
        }

        return true;

    } // deleteRelated()


    /**
     * replace the common info for the message
     */
    function replaceTags($preview = false) {
        global $database, $sm2emailmarketingConfig, $mosConfig_live_site, $mainframe, $mosConfig_absolute_path;

        // initialise
        $datenow = strftime(_DATE_FORMAT_LC);
        $timenow = date('g:i a');


        // determine the content items to put into the content
        $this->_getContentItems();
        // process the template and add the content to it
        $this->_process_template();
        // items
        $this->_replaceItemsTag();
        // articles
        $this->_replaceArticlesTags();
        // cleanup
        $this->_cleanContentItems();

        // prepare the attachments
        if (!$preview) {
            $path = $sm2emailmarketingConfig->get('attachment_path', $mosConfig_absolute_path.'/images/stories/');
            $attachments = explode(',', $this->attachments);
            foreach ($attachments as $index=>$value) {
                $attachments[$index] = $path.'/'.$value;
            }
            $this->attachments = $attachments;
        }


        // Get round the windows \r bug
        //$this->message_text = str_replace("\r", null, $this->message_text);
        //replace multiple linebreaks with maximum 2 only.
        //$this->message_text = preg_replace("/\n{2,}/m", "\n\n", $this->message_text);

        // new experimental TEXT processing to try to get it to look better
        $this->message_text = str_replace("\r", "\n", $this->message_text);
        $this->message_text = preg_replace("/\n{2,}/m", "\n\n", $this->message_text);
        $this->message_text = preg_replace("/(\S)\n(\S)/m", "$1 $2", $this->message_text);

        //Replace [BASEURL] with the actual url of site
        $this->message_html = str_replace('[BASEURL]', $mosConfig_live_site, $this->message_html);
        $this->message_text = str_replace('[BASEURL]', $mosConfig_live_site, $this->message_text);

        //Replace [DATE] with current date
        $this->message_html = str_replace('[DATE]', $datenow, $this->message_html);
        $this->message_text = str_replace('[DATE]', $datenow, $this->message_text);

        //Replace [TIME] with current time
        $this->message_html = str_replace('[TIME]', $timenow, $this->message_html);
        $this->message_text = str_replace('[TIME]', $timenow, $this->message_text);

        //Replace [SUBJECT] with current time
        $this->message_html = str_replace('[SUBJECT]', $this->subject, $this->message_html);
        $this->message_text = str_replace('[SUBJECT]', $this->subject, $this->message_text);

        $archiveURL = $this->getArchiveLink();
        $this->message_html = str_replace('[ARCHIVE_URL]', $archiveURL, $this->message_html);
        $this->message_text = str_replace('[ARCHIVE_URL]', $archiveURL, $this->message_text);

        // process any extra mosimage and mospagebreak tags
        $this->message_html = preg_replace('/\{mosimage\}/', '', $this->message_html);
        $this->message_html = preg_replace('/\{mospagebreak[^\}]*\}/', '', $this->message_html);
        $this->message_text = preg_replace('/\{mosimage\}/', '', $this->message_text);
        $this->message_text = preg_replace('/\{mospagebreak[^\}]*\}/', '', $this->message_text);

        // embed images
        $this->_embeddedImages = array();

        if ($sm2emailmarketingConfig->get('use_embedded_images',0) && !$preview) {
            $matches = array();
            $found = preg_match_all('#<img.*src="(.*)".*>#iUs', $this->message_html, $matches);
            $this->_process_found_images($found, $matches);
            
            if ($sm2emailmarketingConfig->get('embedded_css_images',0)) {
                $matches = array();
                $found = preg_match_all('#url\((.*)\)#iUs', $this->message_html, $matches);
                $this->_process_found_images($found, $matches);
            }
            
        }

        // create versions of the message_html and message_text that are cache
        $this->cache = new stdClass();
        $this->cache->message_html = $this->message_html;
        $this->cache->message_text = $this->message_text;
        if (!$preview) {
            $this->cache->attachments = $this->attachments = $attachments;
            $this->cache->embeddedImages = $this->embeddedImages = $this->_embeddedImages;
        }


        //output ready for display / sending
        $this->message_htmlDisplay = $this->message_html;
        $this->message_textDisplay = nl2br($this->message_text);

    } // replaceTags()


    /**
     * Processes the template and includes the initial content
     * Checks first that the appropriate content tags exist
     */
    function _process_template() {
        // get the list of templates (html and text)
        $template = new sm2emailmarketingTemplate();
        $template->load($this->templateid);

        // check to see if we have any text in the text version
        // if not let's grab the text from the html version and strip the tags
        if(!$this->message_text) {
            $this->message_text = $this->message_html;
            // new experimental TEXT processing to try to get it to look better
            $this->message_text = str_replace('</p>', "</p>\n\n", $this->message_text);
            $this->message_text = str_replace('<br />', "\n", $this->message_text);
            $this->message_text = strip_tags($this->message_text, '<a>');
        }

        // check to see if we have any text in the html version
        // if not let's grab the text from the text version and replace \n to br
        if(!$this->message_html) {
            $this->message_html = nl2br($this->message_text);
        }

        // check to see if we must force the ITEMS tag to exist
        if (!strstr($this->message_html, '[ITEMS]')
            && !strstr($template->layout_html, '[ITEMS]')
            && !strstr($this->message_html, '[ARTICLE]')
            && !strstr($template->layout_html, '[ARTICLE]')) {
            $this->message_html = $this->message_html.'[ITEMS]';
        }
        if (!strstr($this->message_text, '[ITEMS]')
            && !strstr($this->message_text, '[ARTICLE]')) {
            $this->message_text = $this->message_text.'[ITEMS]';
        }

        //wrap the appropriate template around the content
        //need to check if [CONTENT] is in template, otherwise append to end of message
        if (!strstr($template->layout_html, '[CONTENT]')) {
            $template->layout_html = $template->layout_html.'<br />[CONTENT]';
        }

        if (!strstr($template->layout_text, '[CONTENT]')) {
            $template->layout_text = $template->layout_text.'[CONTENT]';
        }

        $this->message_html = str_replace('[CONTENT]', $this->message_html, $template->layout_html);
        $this->message_text = str_replace('[CONTENT]', $this->message_text, $template->layout_text);
    } // _process_template()


    /**
     * Method to replace ITEMS tag with Content Items
     */
    function _replaceItemsTag() {

        // determine if we have anything to replace
        if (empty($this->content_items)) {
            // nothing to replace so remove the tags
            $this->message_html = str_replace('[ITEMS]', '', $this->message_html);
            $this->message_text = str_replace('[ITEMS]', '', $this->message_text);
            return;
        }

        // if no tags then leave
        if (!strstr($this->message_html, '[ITEMS]')
            && !strstr($this->message_text, '[ITEMS]')) return;

        $contentItemsHTML = '';

        // replace the tag
        foreach ($this->_contentItems as $contentItem) {
            if ($this->intro_only) {
                // display the intro only
                if (empty($contentItem->sectionid) && empty($contentItem->catid)) {
                    // this is a static content item
                    // display the title linked
                    if (!empty($contentItem->fulltext)) {
                        $contentItemsHTML .= '<br />'
                            .'<a href="'.$contentItem->url.'">'.$contentItem->title.'</a>'."\n\n";
                    }
                } else {
                    // this is an article
                    // display title, introtext and readmore link
                    $contentItemsHTML .= '<h3>'.$contentItem->title.'</h3>'."\n\n"
                        .$contentItem->introtext;
                    if (!empty($contentItem->fulltext)) {
                        $contentItemsHTML .='<br /><a href="'.$contentItem->url.'">'._READ_MORE.'</a>'."\n\n";
                    }
                }
            } else {
                // display the full message
                $contentItemsHTML .= '<h3>'.$contentItem->title.'</h3>'."\n\n"
                    .$contentItem->introtext
                    .$contentItem->fulltext."\n\n";
            }

            // process the images for this
            $this->_processContentMosImage($contentItem, $contentItemsHTML);
        }

        $this->message_html = str_replace('[ITEMS]', $contentItemsHTML, $this->message_html);
        $this->message_text = str_replace('[ITEMS]', $this->_convertArticleToText($contentItemsHTML), $this->message_text);

    } // _replaceItemsTag()


    /**
     * Method to replace the Article Tag Block
     */
    function _replaceArticlesTags() {
        $replaceHTML = '';
        $replaceTEXT = '';
        $replaceTOCHTML = '';
        $replaceTOCTEXT = '';

        // determine if we have anything to replace
        if (!empty($this->content_items)) {
            // process the content to replace

            // determine the html replacement
            $matches = null;
            if (!preg_match_all('/\[ARTICLE\](.*)\[\/ARTICLE\]/s', $this->message_html, $matches, PREG_SET_ORDER)) {
                $htmlSection = '';
            } else {
                $htmlSection = $matches[0][1];
            }
            // determine the text replacement
            if (!preg_match_all('/\[ARTICLE\](.*)\[\/ARTICLE\]/s', $this->message_text, $matches, PREG_SET_ORDER)) {
                $textSection = '';
            } else {
                $textSection = $matches[0][1];
            }
            // determine html toc
            if (!preg_match_all('/\[ARTICLE_TOC\](.*)\[\/ARTICLE_TOC\]/s', $this->message_html, $matches, PREG_SET_ORDER)) {
                $htmlTOC = '';
            } else {
                $htmlTOC = $matches[0][1];
            }
            // determine the text toc
            if (!preg_match_all('/\[ARTICLE_TOC\](.*)\[\/ARTICLE_TOC\]/s', $this->message_text, $matches, PREG_SET_ORDER)) {
                $textTOC = '';
            } else {
                $textTOC = $matches[0][1];
            }
            unset($matches);

            // check to see if we should be copying some values
            if (empty($htmlSection) && !empty($textSection)) {
                // copy text to html
                $htmlSection = nl2br($textSection);
            } else if (empty($textSection) && !empty($htmlSection)) {
                // copy html to text
                $textSection = strip_tags($htmlSection);
            }

            if (empty($htmlTOC) && !empty($textTOC)) {
                // copy text toc to html
                $htmlTOC = nl2br($textTOC);
            } else if (empty($textTOC) && !empty($htmlTOC)) {
                // copy html TOC to text
                $textTOC = strip_tags($htmlTOC);
            }

            // replace the tag
            if (!empty($htmlSection) || !empty($htmlTOC)
                || !empty($textSection) || !empty($textTOC)) {
                foreach ($this->_contentItems as $contentItem) {
                    $html = $htmlSection;
                    $text = $textSection;
                    $tocHTML = $htmlTOC;
                    $tocTEXT = $textTOC;

                    if (!empty($html)) {
                        $html = str_replace('[ARTICLE_TITLE]', '<a name="'.$contentItem->id.'">'.$contentItem->title.'</a>', $html);
                        $html = str_replace('[ARTICLE_SECTION]', $contentItem->section, $html);
                        $html = str_replace('[ARTICLE_CATEGORY]', $contentItem->category, $html);
                        $html = str_replace('[ARTICLE_INTRO]', $contentItem->introtext, $html);
                        $html = str_replace('[ARTICLE_FULL]', $contentItem->fulltext, $html);
                        $html = str_replace('[ARTICLE_URL]', $contentItem->url, $html);
                        $html = str_replace('[ARTICLE_READMORE]', '<a href="'.$contentItem->url.'">'._READ_MORE.'</a>', $html);
                    }

                    if (!empty($tocHTML)) {
                        $tocHTML = str_replace('[ARTICLE_TOC_TITLE]', $contentItem->title, $tocHTML);
                        $tocHTML = str_replace('[ARTICLE_TOC_LINK]', '#'.$contentItem->id, $tocHTML);
                        $tocHTML = str_replace('[ARTICLE_TOC_ITEM]', '<a href="#'.$contentItem->id.'">'.$contentItem->title.'</a>', $tocHTML);
                    }

                    if (!empty($text)) {
                        $text = str_replace('[ARTICLE_TITLE]', $contentItem->title, $text);
                        $text = str_replace('[ARTICLE_SECTION]', $contentItem->section, $text);
                        $text = str_replace('[ARTICLE_CATEGORY]', $contentItem->category, $text);
                        $text = str_replace('[ARTICLE_INTRO]', $this->_convertArticleToText($contentItem->introtext), $text);
                        $text = str_replace('[ARTICLE_FULL]', $this->_convertArticleToText($contentItem->fulltext), $text);
                        $text = str_replace('[ARTICLE_URL]', $contentItem->url, $text);
                        $text = str_replace('[ARTICLE_READMORE]', _READ_MORE.' '.$contentItem->url, $text);
                    }

                    if (!empty($tocTEXT)) {
                        $tocTEXT = str_replace('[ARTICLE_TOC_TITLE]', $contentItem->title, $tocTEXT);
                        $tocTEXT = str_replace('[ARTICLE_TOC_LINK]', '#'.$contentItem->id, $tocTEXT);
                        $tocTEXT = str_replace('[ARTICLE_TOC_ITEM]', $contentItem->title, $tocTEXT);
                    }

                    // process images
                    $this->_processContentMosImage($contentItem, $html);
                    $text = str_replace('{mosimage}', '', $text);

                    $replaceHTML .= $html;
                    $replaceTEXT .= $text;
                    $replaceTOCHTML .= $tocHTML;
                    $replaceTOCTEXT .= $tocTEXT;
                }
            }

            // make sure that preg_replace does not replace dollar values
            $replaceHTML = str_replace('$', '\$', $replaceHTML);
            $replaceTEXT = str_replace('$', '\$', $replaceTEXT);
            $replaceTOCHTML = str_replace('$', '\$', $replaceTOCHTML);
            $replaceTOCTEXT = str_replace('$', '\$', $replaceTOCTEXT);
            // replace the article tags
            $this->message_text = str_replace('$', '\$', $this->message_text);
            $this->message_html = preg_replace('/\[ARTICLE\](.*)\[\/ARTICLE\]/s', $replaceHTML, $this->message_html);
            $this->message_html = preg_replace('/\[ARTICLE_TOC\](.*)\[\/ARTICLE_TOC\]/s', $replaceTOCHTML, $this->message_html);
            $this->message_text = preg_replace('/\[ARTICLE\](.*)\[\/ARTICLE\]/s', $replaceTEXT, $this->message_text);
            $this->message_text = preg_replace('/\[ARTICLE_TOC\](.*)\[\/ARTICLE_TOC\]/s', $replaceTOCTEXT, $this->message_text);
        }
    } // _replaceArticlesTags()


    /**
     * Method to get the content items data
     */
    function _getContentItems() {
        global $mosConfig_live_site, $mainframe, $database;

        if (isset($this->_contentItems)) return;
        $this->_contentItems = array();
        if (empty($this->content_items)) return;

        // get the list of content items to be included
        $database->setQuery('SELECT a.id, a.title, a.introtext,'
            .' a.`fulltext`, a.images, a.sectionid, a.catid,'
            .' s.name AS section, c.name AS category'
            .' FROM #__content a'
            .' LEFT JOIN #__sections s ON (a.sectionid=s.id)'
            .' LEFT JOIN #__categories c ON (a.catid=c.id)'
            .' WHERE a.id IN ('.$this->content_items.') '
            .' ORDER BY a.title ASC');
        $contentitems = $database->loadObjectList('id');

        // determine what the read more link will be
        $readMore = 'index.php?'
            .'option=com_content'
            .'&amp;task=view&amp;id=';
        if (!function_exists('sefRelToAbs')) {
            $readMore = $mosConfig_live_site.'/'.$readMore;
        }

        $contentitemsArray = explode(',',$this->content_items);

        foreach ($contentitemsArray as $contentid) {
            // process the current content item
            $contentItem = $contentitems[$contentid];

            // skip if nothing in this item (just in case)
            if (empty($contentItem)) continue;

            // determine the readmore link
            $readMoreItemId = $mainframe->getItemid($contentItem->id);
            $readMoreLink = $readMore.$contentItem->id;
            if (!empty($readMoreItemId)) {
                $readMoreLink .= '&amp;Itemid='.$readMoreItemId;
            }

            // sef the link if necessary
            if (function_exists('sefRelToAbs') && !defined('_JLEGACY')) {
                $readMoreLink = sefRelToAbs($readMoreLink);
            }

            $contentItem->url = $readMoreLink;

            $this->_contentItems[] = $contentItem;

        }
    } // _getContentItems()


    /**
     * Method to clean up after content processing
     */
    function _cleanContentItems() {
        unset($this->_contentItems);
    } // _cleanContentItems()
    
    
    /**
     * Method to process the images found and replace them with embedded
     */
    function _process_found_images($found, $matches) {
        global $mosConfig_live_site, $mosConfig_absolute_path;
        static $usedCids = array();
        
        if ($found) {
            for ($i = 0; $i < $found; $i++) {
                // check that this image needs to be processed
                if (substr($matches[1][$i], 0, 4)=='http'
                    && substr($matches[1][$i], 0, strlen($mosConfig_live_site)) != $mosConfig_live_site) continue;

                $embed = new stdClass();

                // get the path of the attachment
                $embed->image = str_replace($mosConfig_live_site, '', $matches[1][$i]);
                if ($embed->image[0]!='/') $embed->image = '/'.$embed->image;
                $embed->image = $mosConfig_absolute_path.$embed->image;
                $embed->cid = md5($embed->image);

                if (in_array($embed->cid, $usedCids)) continue;
                $usedCids[] = $embed->cid;

                $embed->name = '';
                $embed->encoding = 'base64';

                // determine the file extension
                $parts = explode('.', $embed->image);
                $ext = array_pop($parts);
                unset($parts);

                // determine the mime type
                switch (strtolower($ext)) {
                    case 'jpg':
                    case 'jpeg':
                        $embed->type = 'image/jpeg';
                        break;
                    case 'png':
                        $embed->type = 'image/png';
                        break;
                    case 'gif':
                        $embed->type = 'image/gif';
                        break;
                    default:
                        $embed->type = 'application/octet-stream';
                        break;
                }

                $this->_embeddedImages[] = $embed;

                $this->message_html = str_replace($matches[1][$i], 'cid:'.$embed->cid, $this->message_html);

            }
        }
    } // _process_found_images()


    /**
     * Method to process Content MosImage tags
     */
    function _processContentMosImage($content, &$messageHTML) {
        // see if there is anything to process
        if (empty($content->images)) return;

        // process the images for the contentitem
        $images = $this->getImageArray($content->images);
        foreach($images as $image) {
            $imagestring = '<img src="/images/stories/'.$image->image.'" align="'.$image->align.'" alt="'.$image->alttext.'" border="'.$image->border.'" />';
            $messageHTML = preg_replace('/{mosimage}/', $imagestring, $messageHTML, 1);
        }
        $messageHTML = str_replace('{mosimage}', '', $messageHTML);
    } // _processContentMosImage()


    /**
     * Method to get text version of content article
     */
    function _convertArticleToText($contentItemsHTML) {
        global $sm2emailmarketingConfig;

        // see if we should convert
        if (!$sm2emailmarketingConfig->get('convert_articles_to_text',0)) {
            return '';
        }

        // generate text version from the HTML version
        //$contentItemsText = str_replace("\r", "", $contentItemsHTML);
        $contentItemsText = $contentItemsHTML;
        // new experimental TEXT processing to try to get it to look better
        $contentItemsText = str_replace("\r", '', $contentItemsText);
        $contentItemsText = str_replace('</p>', "</p>\n\n", $contentItemsText);

        $contentItemsText = str_replace('<br />', "\n", $contentItemsText);
        $contentItemsText = str_replace('<a href="', '', $contentItemsText);
        $contentItemsText = strip_tags($contentItemsText);
        $contentItemsText = str_replace('">', '', $contentItemsText);
        $contentItemsText = str_replace('Read more...', '  Read more...', $contentItemsText);
        $contentItemsText = str_replace('&nbsp;', '', $contentItemsText);

        return $contentItemsText;
    } // _convertArticleToText()


    /**
     * Method to prepare the message for sending to each email
     * This just involves setting the current message html and text versions
     * to that which is in the cache
     *
     * If the cache does not exist then it is not processed
     *
     * This is needed because in PHP 5 there is an issue that object assignment acts as referncing
     *
     */
    function getMessageFromCache() {
        if (empty($this->cache)) return;

        $this->message_html = $this->cache->message_html;
        $this->message_text = $this->cache->message_text;
        $this->attachments = $this->cache->attachments;
        $this->embeddedImages = $this->cache->embeddedImages;
    } // getMessageFromCache()

    /**
     * replace the unique userinfo for the message
     */
    function replaceUserInfo($name=_EMKTG_PREVIEW_NAME, $email='', $subscriber_id=null) {
        global $sm2emailmarketingConfig, $mosConfig_live_site, $mosConfig_mailfrom, $option;

        if (empty($email)) {
            $email = $sm2emailmarketingConfig->get('fromemail', $mosConfig_mailfrom);
        }

        //replace the users name
        $this->message_html = str_replace('[NAME]', $name, $this->message_html);
        $this->message_text = str_replace('[NAME]', $name, $this->message_text);

        // replace the subscribers email
        $this->message_html = str_replace('[EMAIL]', $email, $this->message_html);
        $this->message_text = str_replace('[EMAIL]', $email, $this->message_text);

        // replace the forward tag
        $forward = $this->getForwardLink($subscriber_id);
        $this->message_html = str_replace('[FORWARD]', $forward['html'], $this->message_html);
        $this->message_text = str_replace('[FORWARD]', $forward['text'], $this->message_text);

    } // replaceUserInfo()


    function getImageArray($images) {
        // split around newline
        $images = explode("\n",$images);

        $return = array();

        // then split each line around |'s to get image properties
        foreach ($images as $index=>$image) {
            $imageParts = explode('|', $image);

            if (count($imageParts) < 1) continue;

            $image = new stdClass();
            $image->image = $imageParts[0];
            $image->align = @$imageParts[1];
            $image->alttext = @$imageParts[2];
            $image->border = @$imageParts[3];

            $return[] = $image;
        } // end for

        return $return;

    } // end function


    function _getItemId() {
        static $itemid = null;

        if ($itemid===null) {
            // determine the itemid of the archive
            $this->_db->setQuery('SELECT `id`'
                .' FROM #__menu'
                .' WHERE `link` LIKE '.$this->_db->Quote('%option=com_sm2emailmarketing%')
                .' AND `type`='.$this->_db->Quote('components')
                .' ORDER BY `id` LIMIT 0, 1');
            $id = (int)$this->_db->loadResult();

            $itemid = (empty($id) ? '' : '&Itemid='.$id);
        }

        return $itemid;
    } // _getItemId()


    function getArchiveLink($force=false) {
        static $url = null;

        if ($url===null || $force) {
            global $mosConfig_live_site;
            $itemid = $this->_getItemId();

            $url = 'index.php?option=com_sm2emailmarketing'
                .'&task=showarchivemessage'
                .'&id='.$this->messageid
                .$itemid;
            if (function_exists('sefRelToAbs') && !defined('_JLEGACY')) {
                $url = sefRelToAbs($url);
            } else {
                $url = $mosConfig_live_site.'/'.$url;
            }
        }

        return $url;

    } // getArchiveLink()


    function getForwardLink($subscriber_id=null, $force=false) {
        static $replaceData = null;
        static $search = array('[SID]','[CONFIRM]');
        global $mosConfig_secret, $sm2emailmarketingConfig;

        // handle null subscriber_id
        if ($subscriber_id===null || !$sm2emailmarketingConfig->get('forward', 0))
            return array('html'=>'','text'=>'');

        if ($replaceData===null || $force) {
            global $mosConfig_live_site;

            $itemid = $this->_getItemId();

            $replaceData = array('html'=>'','text'=>'');

            $replaceURL = 'index.php?option=com_sm2emailmarketing'
                .'&task=forward'
                .'&id='.$this->messageid
                .'&subscriberid=[SID]'
                .'&confirm=[CONFIRM]'
                .$itemid;

            if (function_exists('sefRelToAbs') && !defined('_JLEGACY')) {
                $replaceURL = sefRelToAbs($replaceURL);
            } else {
                $replaceURL = $mosConfig_live_site.'/'.$replaceURL;
            }

            $replaceData['html'] = '<a href="'.$replaceURL.'">'
                .$sm2emailmarketingConfig->get('forward_text', $replaceURL)
                .'</a>';
            $replaceData['text'] = ' '.$sm2emailmarketingConfig->get('forward_text')
                .' '.$replaceURL.' ';
        }

        // determine the confirmation
        $confirm = md5($mosConfig_secret.$this->messageid.$subscriber_id);
        // determine what to replace
        $replace = array($subscriber_id, $confirm);
        $return = $replaceData;
        $return['html'] = str_replace($search, $replace, $return['html']);
        $return['text'] = str_replace($search, $replace, $return['text']);
        
        if (defined('_SM2EM_FORWARD'))
            $return['html'] = $return['text'] = '';

        return $return;

    } // getForwardLink()


    function sendMessage($option, $preview=false) {
        global $mosConfig_absolute_path;

        // prepare the queue for processing
        sm2EM_PrepareQueue($option, $preview);
        $pluginsUsed = 0;

        // get the plugins to process
        $this->_db->setQuery('SELECT * FROM #__emktg_plugin WHERE enabled=1 ORDER BY ordering');
        $plugins = $this->_db->loadObjectList();

        // process the plugins
        foreach ($plugins as $plugin) {
            // load the plugin
            if (!file_exists($mosConfig_absolute_path.'/administrator/components/'.$option.'/plugins/'.$plugin->filename)) continue;
            require_once($mosConfig_absolute_path.'/administrator/components/'.$option.'/plugins/'.$plugin->filename);
            $class = $plugin->classname;
            $obj = new $class();
            // process the send of the plugin
            if (method_exists($obj, 'processSend')) {
                $pluginsUsed += $obj->processSend($this->messageid, $plugin->pluginid, $option);
            }
        } // foreach()

        // finalise the queue
        sm2EM_FinaliseQueue($pluginsUsed, $option, $preview);
    } // sendMessage()

} // class - sm2emailmarketingMessage


class sm2emailmarketingUser extends mosDBTable {

    var $id = null;
    var $receive_html = null;
    var $subscribe_date = null;
    var $unsubscribe_date = null;
    var $unsubscribe_reason = null;
    var $confirmed = null;

    /**
     * Constructor
     */
    function sm2emailmarketingUser() {
        global $database;
        $this->mosDBTable('#__emktg_user', 'id', $database);
    } // sm2emailmarketingUser()


    /**
    * Inserts a new row if id is zero or updates an existing row in the database table
    *
    * overloaded to be able to force the insert if needed
    *
    * @param boolean If false, null object variables are not updated
    * @param boolean If true then will force to do an insert rather than update
    * @return null|string null if successful otherwise returns and error message
    */
    function store( $updateNulls=false, $forceInsert = false ) {
        $k = $this->_tbl_key;

        if ($this->$k && !$forceInsert) {
            $ret = $this->_db->updateObject( $this->_tbl, $this, $this->_tbl_key, $updateNulls );
        } else {
            $ret = $this->_db->insertObject( $this->_tbl, $this, $this->_tbl_key );
        }
        if( !$ret ) {
            $this->_error = strtolower(get_class( $this )).'::store failed <br />' . $this->_db->getErrorMsg();
            return false;
        } else {
            return true;
        }
    }


    function subscribe($listids=array()) {
        $this->_db->setQuery('DELETE FROM #__emktg_list_user'
        .' WHERE '.$this->_tbl_key.' = '.(int)$this->id
        );
        if (!$this->_db->query()) {
            $this->_error = $this->_db->getErrorMsg();
            return false;
        }

        $values = array();
        foreach ($listids as $listid) {
            $values[] = '('.$listid.', '.$this->id.')';
        } // foreach

        if (count($values)>0) {
            $sql = 'INSERT INTO #__emktg_list_user (`listid`, `id`) VALUES '.implode(',', $values);
            $this->_db->setQuery($sql);
            if (!$this->_db->query()) {
                $this->_error = $this->_db->getErrorMsg();
                return false;
            }
        }

        $this->_error = '';
        return true;
    } // subscribe()

    function confirm($cid=null, $confirm=1) {
        if (!is_array($cid) || count($cid) < 1) {
            $this->_error = _EMKTG_REGISTERED_CONFIRM_ERROR;
            return false;
        }

        $cids = implode( ',', $cid );

        $this->_db->setQuery('UPDATE '.$this->_tbl.' SET confirmed='.(int)$confirm
            .' WHERE '.$this->_tbl_key.' IN ('.$cids.')'
        );
        if (!$this->_db->query()) {
            $this->_error = $this->_db->getErrorMsg();
            return false;
        }

        $this->_error = '';
        return true;
    } // confirm()

    function confirmAll($confirm=1) {
        $this->_db->setQuery('UPDATE '.$this->_tbl.' SET confirmed='.(int)$confirm);
        if (!$this->_db->query()) {
            $this->_error = $this->_db->getErrorMsg();
            return false;
        }

        $this->_error = '';
        return true;
    } // confirmAll()

    function receiveHTML($cid=null, $html=1) {
        if (!is_array($cid) || count($cid) < 1) {
            $this->_error = _EMKTG_REGISTERED_RECEIVE_HTML_ERROR;
            return false;
        }

        $cids = implode( ',', $cid );

        $this->_db->setQuery('UPDATE '.$this->_tbl.' SET receive_html='.(int)$html
            .' WHERE '.$this->_tbl_key.' IN ('.$cids.')'
        );
        if (!$this->_db->query()) {
            $this->_error = $this->_db->getErrorMsg();
            return false;
        }

        $this->_error = '';
        return true;
    } // receiveHTML()

    function receiveHTMLAll($html=1) {
        $this->_db->setQuery('UPDATE '.$this->_tbl.' SET receive_html='.(int)$html);
        if (!$this->_db->query()) {
            $this->_error = $this->_db->getErrorMsg();
            return false;
        }

        $this->_error = '';
        return true;
    } // receiveHTMLAll()


    function unsubscribe($cid=null, $reason=5) {
        if(!is_array($cid)) {
            $cid = array($cid);
        }
        if (!is_array($cid) || count($cid) < 1) {
            $this->error = _EMKTG_REGISTERED_CONFIRM_ERROR;
            return false;
        }
        $cids = implode(',',$cid);
        $sql = 'UPDATE '.$this->_tbl.' SET unsubscribe_date=NOW(), unsubscribe_reason='.(int)$reason
            .' WHERE '.$this->_tbl_key.' IN ('.$cids.')';
        $this->_db->setQuery($sql);
        if (!$this->_db->query()) {
            $this->_error = $this->_db->getErrorMsg();
            return false;
        }

        // get everything so that we can send the emails
        sm2emAdminUnsubscribeEmailFromQuery('SELECT u.name, u.email, 5 AS unsubscribe_reason'
            .' FROM #__users u INNER JOIN '.$this->_tbl.' s ON (u.id=s.id)'
            .' WHERE u.id IN ('.$cids.')');

        $this->_error = '';
        return true;

    } // unsubscribe()

} // class - sm2emailmarketingUser


/**
 * Unsubscribe reason codes
 * 0 - subscribed
 * 1 - user requested
 * 2 - error email
 * 3 - blacklist email
 * 4 - bounce email
 * 5 - Admin requested
 */
class sm2emailmarketingSubscriber extends mosDBTable {

    var $subscriberid = null;
    var $name = null;
    var $email = null;
    var $receive_html = null;
    var $subscribe_date = null;
    var $unsubscribe_date = null;
    var $unsubscribe_reason = null;
    var $confirmed = null;

    /**
     * Constructor
     */
    function sm2emailmarketingSubscriber() {
        global $database;
        $this->mosDBTable('#__emktg_subscriber', 'subscriberid', $database);
    } // sm2emailmarketingSubscriber()

    function deleteRelated($id) {
        $k = $this->_tbl_key;
        if ($id) {
            $this->$k = intval($id);
        }

        $this->_db->setQuery('DELETE FROM #__emktg_list_subscriber WHERE '.$this->_tbl_key.' = '.(int)$this->$k);

        if (!$this->_db->query()) {
            $this->_error = $this->_db->getErrorMsg();
            return false;
        }

        return true;

    } // deleteRelated()

    function subscribe($listids=array(), $keep=false) {

        if (!$keep) {
            $this->_db->setQuery('DELETE FROM #__emktg_list_subscriber'
                .' WHERE '.$this->_tbl_key.' = '.-$this->subscriberid
            );
            if (!$this->_db->query()) {
                $this->_error = $this->_db->getErrorMsg();
                return false;
            }
            $insert = 'INSERT INTO';
        } else {
            $insert = 'INSERT IGNORE INTO';
        }


        $values = array();
        foreach ($listids as $listid) {
            $values[] = '('.(int)$listid.', -'.(int)$this->subscriberid.')';
        } // foreach

        if (count($values)>0) {
            $sql = $insert.' #__emktg_list_subscriber (`listid`, `subscriberid`) values '.implode(', ', $values);
            $this->_db->setQuery($sql);
            if (!$this->_db->query()) {
                $this->_error = $this->_db->getErrorMsg();
                return false;
            }
        }

        $this->_error = '';
        return true;
    } // subscribe()

    function unsubscribe($cid=null, $reason=5){
    	if(!is_array($cid)) {
    		$cid = array($cid);
    	}
    	if (!is_array($cid) || count($cid) < 1) {
    		$this->error = _EMKTG_UNREGISTERED_CONFIRM_ERROR;
    		return false;
    	}
    	$cids = implode(',',$cid);
    	$sql = 'UPDATE '.$this->_tbl.' SET unsubscribe_date=NOW(), unsubscribe_reason='.(int)$reason
            .' WHERE '.$this->_tbl_key.' IN ('.$cids.')';
    	$this->_db->setQuery($sql);
    	if (!$this->_db->query()) {
    		$this->_error = $this->_db->getErrorMsg();
    		return false;
    	}

        // get everything so that we can send the emails
        sm2emAdminUnsubscribeEmailFromQuery('select name, email, 5 as unsubscribe_reason '
            .' from '.$this->_tbl
            .' where '.$this->_tbl_key.' in ('.$cids.')');

    	$this->_error = '';
    	return true;

    } // unsubscribe()


    function confirm($cid=null, $confirm=1) {
        if (!is_array($cid) || count($cid) < 1) {
            $this->_error = _EMKTG_UNREGISTERED_CONFIRM_ERROR;
            return false;
        }

        $cids = implode( ',', $cid );

        $this->_db->setQuery('UPDATE '.$this->_tbl.' SET confirmed='.(int)$confirm
            .' WHERE '.$this->_tbl_key.' IN ('.$cids.')'
        );
        if (!$this->_db->query()) {
            $this->_error = $this->_db->getErrorMsg();
            return false;
        }

        $this->_error = '';
        return true;
    } // confirm()

    function confirmAll($confirm=1) {
        $this->_db->setQuery('UPDATE '.$this->_tbl.' SET confirmed='.(int)$confirm);
        if (!$this->_db->query()) {
            $this->_error = $this->_db->getErrorMsg();
            return false;
        }

        $this->_error = '';
        return true;
    } // confirmAll()

    function receiveHTML($cid=null, $html=1) {
        if (!is_array($cid) || count($cid) < 1) {
            $this->_error = _EMKTG_UNREGISTERED_RECEIVE_HTML_ERROR;
            return false;
        }

        $cids = implode( ',', $cid );

        $this->_db->setQuery('UPDATE '.$this->_tbl.' SET receive_html='.(int)$html
            .' WHERE '.$this->_tbl_key.' IN ('.$cids.')'
        );
        if (!$this->_db->query()) {
            $this->_error = $this->_db->getErrorMsg();
            return false;
        }

        $this->_error = '';
        return true;
    } // receiveHTML()

    function receiveHTMLAll($html=1) {
        $this->_db->setQuery('UPDATE '.$this->_tbl.' SET receive_html='.(int)$html);
        if (!$this->_db->query()) {
            $this->_error = $this->_db->getErrorMsg();
            return false;
        }

        $this->_error = '';
        return true;
    } // receiveHTMLAll()

    /**
     * Method to get a subscriber by their email
     */
    function getByEmail($email) {
        $this->_db->setQuery('SELECT * '
            .' FROM '.$this->_tbl
            .' WHERE `email` LIKE '.$this->_db->Quote($email)
            .' LIMIT 0, 1');
        $result = $this->_db->loadAssocList();

        if (is_array($result)) {
            $result = reset($result);
            if (is_array($result)) {
                foreach ($result as $field=>$value) {
                    if (isset($this->$field)) $this->$field = $value;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }

        return true;
    } // getByEmail()

} // class - sm2emailmarketingSubscriber


class sm2emailmarketingPlugin extends mosDBTable {

    var $pluginid = null;
    var $name = null;
    var $description = null;
    var $filename = null;
    var $classname = null;
    var $params = null;
    var $enabled = null;
    var $ordering = null;

    /**
     * Constructor
     */
    function sm2emailmarketingPlugin() {
        global $database;
        $this->mosDBTable('#__emktg_plugin', 'pluginid', $database);
    } // sm2emailmarketingMessage()

    function loadFromFilename($filename) {
        $this->_db->SetQuery('SELECT pluginid FROM #__emktg_plugin WHERE filename='.$this->_db->Quote($filename));
        $this->load($this->_db->loadResult());
    } // loadFromFilename()

    function enable($cid=null, $enabled=1) {
        if (!is_array($cid) || count($cid) < 1) {
            $this->_error = _EMKTG_PLUGIN_CONFIRM_ERROR;
            return false;
        }

        $cids = implode( ',', $cid );

        $this->_db->setQuery('UPDATE '.$this->_tbl.' SET enabled='.(int)$enabled
            .' WHERE '.$this->_tbl_key.' IN ('.$cids.')'
        );

        if (!$this->_db->query()) {
            $this->_error = $this->_db->getErrorMsg();
            return false;
        }

        $this->_error = '';
        return true;
    } // enable()

    function loadPluginObj(&$obj, $option) {
        global $mosConfig_absolute_path;
        require_once($mosConfig_absolute_path.'/administrator/components/'.$option.'/plugins/'.$this->filename);
        $classname = $this->classname;
        $obj = new $classname();

    } // loadPluginObj()

} // class - sm2emailmarketingPlugin


class sm2emPluginObj {

    var $name = 'SM2 Email Marketing Generic';
    var $filename = 'generic.plugin.php';
    var $description = 'Generic Plugin';
    var $classname = 'sm2emPluginObj';
    var $fieldPrefix = 'gen';
    var $_plugin_validation = '';
    var $_plugin_extra_validation = '';

    /**
     * Constructor
     */
    function sm2emPluginObj() {
    } // sm2emPluginObj()


    /**
     * getPararms
     *
     * Method to retreive the parameters for a plugin
     *
     * @param int $pluginid Id of the current plugin
     * @return mixed Returns a param object or false if nothing exists
     */
    function getParams($pluginid=null) {
        global $mosConfig_absolute_path, $option;
        static $params = null;

        if (empty($pluginid)) return false;

        if ($params===null) {

            $row = new sm2emailmarketingPlugin();
            $row->load($pluginid);

            $pluginFile = $mosConfig_absolute_path.'/administrator/components/'.$option.'/plugins/'.str_replace('.php', '.xml', $row->filename);
            if (file_exists($pluginFile)) {
                $params =& new sm2emailmarketingParams($row->params, $pluginFile, 'sm2emailmarketing_plugin');
            }
        }

        return $params;

    } // getParams()


    /**
     * getMessageData
     *
     * Method to get the options for the message and this plugin
     */
    function getMessageData($messageid, $pluginid) {
        static $selected = null;

        if ($selected===null) {
            $selected = sm2emailmarketingGetPluginMessageData($messageid, $pluginid);
        }

        return $selected;
    } // getMessageData()


    /**
     * installPlugin
     *
     * Method to install the plugin
     * Determines if the plugin is valid
     * then adds the plugin data to the plugin table
     */
    function installPlugin() {
        global $database, $emErrorHandler;

        // assume enabled first
        $enabled = 1;

        // check to see if it should be disabled
        if (!$this->validatePlugin()) $enabled = 0;

        // build the insert query
        $row = new sm2emailmarketingPlugin();
        $row->name = $this->name;
        $row->description = $this->description;
        $row->filename = $this->filename;
        $row->classname = $this->classname;
        $row->enabled = $enabled;
        $row->ordering = 999;

        // attempt to insert/update the record
        if (!$row->store()) {
            $emErrorHandler->addError($row->getError(), true);
        }

        $row->updateOrder();

    } // installPlugin()


    /**
     * validatePlugin
     *
     * Method to validate if the plugin is usable
     *
     * @params int $pluginid Id of the plugin to validate
     * @return boolean
     */
    function validatePlugin($pluginid=null) {
        return true;
    } // validatePlugin()


    /**
     * Method to display the extra fields on the message form
     *
     * @param int $message Current Message Object
     * @param int $plugin Current Plugin object
     * @param string $option Current Joomla Option
     * @return mixed Returns html content of the plungin filters
     */
    function displayMessageExtras($message, $plugin, $option) {
        return '';
    } // displayMessageExtras()


    /**
     * Method to display the filters on the message form
     *
     * @param int $messageid Id of the current Message
     * @param int $pluginid Id of the current plugin
     * @param string $option Current Joomla Option
     * @return mixed Returns html content of the plungin filters
     */
    function displayMessageFilters($messageid, $pluginid, $option) {
        if (method_exists($this, 'displayMethodEdit')) {
            return $this->displayMethodEdit($messageid, $pluginid, $option);
        } else {
            return '';
        }
    } // displayMessageFilters()


    /**
     * processMessageEdit
     *
     * Method to process the fields from the Message Management form
     * Data is stored in the emktg_message_plugin table
     *
     * @param int $messageid Id of the current Message
     * @param int $pluginid Id of the current plugin
     * @param string $option Current Joomla Option
     * @return boolean
     */
    function processMessageEdit($messageid, $pluginid, $option) {
        global $database;

        if (!$this->validatePlugin($pluginid)) return false;

        // get the data passed by the form
        $data = mosGetParam($_POST, $this->fieldPrefix, array());

        // see if there is an existing record
        sm2emailmarketingSetPluginMessageData($messageid, $pluginid, $data);

        return true;

    } // processMessageEdit


    /**
     * processSend
     *
     * Method to process a plugin send
     * Retrieves all of the users
     *
     * @param int $messageid Id of the current Message
     * @param int $pluginid Id of the current plugin
     * @param string $option Current Joomla Option
     * @return mixed Returns false if error or Returns an array of name,email objects
     */
    function processSend($messageid, $pluginid, $option) {
        global $database;

        if (!$this->validatePlugin($pluginid)) return false;

        $params = $this->getParams($pluginid);

        // determine which users to send to
        $pluginData = sm2emailmarketingGetPluginMessageData($messageid, $pluginid);

        if (empty($pluginData)) return false;

        $return = false;

        /**
         * Code here to determine which users to send to
         */

        return $return;

    } // processSend()

    /**
     * processMessage
     *
     * Method to process a plugin send
     * Retrieves all of the users
     *
     * @param int $messageid Id of the current Message
     * @param int $pluginid Id of the current plugin
     * @param string $option Current Joomla Option
     * @param object $row The row of message data to be sent
     * @param int $userid The Id of the user being processed. May not be a joomla user id
     */
    function processMessage($messageid, $pluginid, $option, &$row, $userid) {

        /**
         * Code here to modify the parts of the $row
         * i.e. $row->message_text, $row->message_html
         */

    } // processMessage()

} // class sm2emPluginObj

// handle the parameters better for Joomla! 1.5
if (defined('_JLEGACY')) {
    require_once($GLOBALS['mosConfig_absolute_path'].'/administrator/components/com_sm2emailmarketing/includes/mos.xml.php');
} else {
    class sm2Parameters extends mosParameters {
    } // class sm2Parameters
}

class sm2emailmarketingParams extends sm2Parameters {
    /**
    * Constructor
    * @param string The raw parms text
    * @param string Path to the xml setup file
    * @var string The type of setup file
    */
    function sm2emailmarketingParams($text, $path='', $type='component') {
        $this->mosParameters($text, $path, $type);
    } // sm2emailmarketingParams()


    /**
    * @param string The name of the control, or the default text area if a setup file is not found
    * @return string HTML
    */
    function render( $name='params' ) {
        global $mosConfig_absolute_path;

        if ($this->_path) {
            if (!is_object( $this->_xmlElem )) {
                require_once( $mosConfig_absolute_path . '/includes/domit/xml_domit_lite_include.php' );

                $xmlDoc = new DOMIT_Lite_Document();
                $xmlDoc->resolveErrors( true );

                // load the data from the file seperately so we can replace constants
                $xmlText = $xmlDoc->getTextFromFile($this->_path);
                $xmlText = $this->processLangElements($xmlText);

                if ($xmlDoc->parseXML( $xmlText, false, true )) {
                    $root =& $xmlDoc->documentElement;

                    $tagName = $root->getTagName();
                    $isParamsFile = ($tagName == 'mosinstall' || $tagName == 'mosparams');
                    if ($isParamsFile && $root->getAttribute( 'type' ) == $this->_type) {
                        if ($params = &$root->getElementsByPath( 'params', 1 )) {
                            $this->_xmlElem =& $params;
                        }
                    }
                }
            }
        }

        if (is_object( $this->_xmlElem )) {
            $html = array();
            $html[] = '<table width="100%" class="paramlist">';

            $element =& $this->_xmlElem;

            if ($description = $element->getAttribute( 'description' )) {
                // add the params description to the display
                $html[] = '<tr><td colspan="3">' . $description . '</td></tr>';
            }

            //$params = mosParseParams( $row->params );
            $this->_methods = get_class_methods( get_class( $this ) );

            foreach ($element->childNodes as $param) {
                $result = $this->renderParam( $param, $name );

                $html[] = '<tr>';
                if ($param->getAttribute('type')=='header') {
                    $html[] = '<th colspan="2">' . $param->getAttribute('label') . '</th>';
                } else {
                    $html[] = '<td width="40%" align="right" valign="top"><span class="editlinktip">' . $result[0] . '</span></td>';
                    $html[] = '<td>' . $result[1] . '</td>';
                }
                $html[] = '</tr>';
            }
            $html[] = '</table>';

            if (count( $element->childNodes ) < 1) {
                $html[] = '<tr><td colspan="2"><i>' . _NO_PARAMS . '</i></td></tr>';
            }
            return implode( "\n", $html );
        } else {
            return '<textarea name="'.$name.'" cols="40" rows="10" class="text_area">'.$this->_raw.'</textarea>';
        }
    } // render()


    /**
    * @param string The name of the form element
    * @param string The value of the element
    * @param object The xml element for the parameter
    * @param string The control name
    * @return string The html for the element
    */
    function _form_header( $name, $value, &$node, $control_name ) {
        return $value;
    } // _form_header()


    /**
     * Looks for an replaces any language elements in the value
     * Language Elements are entered as {#_(ELEMENT)}
     * i.e. {#__EMKTG_USABLETAGS} will be replaced with the value of the constant _EMKTG_USABLETAGS
     */
    function processLangElements($value) {
        // look for and replace any constants
        $matches = null;
        preg_match_all('/\{#_([^\}]*)\}/', $value, $matches, PREG_SET_ORDER);
        if (!empty($matches)) {
            foreach ($matches as $match) {
                if (defined($match[1])) {
                    $value = str_replace($match[0], constant($match[1]), $value);
                } else {
                    $value = str_replace($match[0], $match[1], $value);
                }
            }
        }

        // look for and replace any joomla config variables
        $matches = null;
        preg_match_all('/\{\$_((mos|jos)Config_[^\}]*)\}/', $value, $matches, PREG_SET_ORDER);
        if (!empty($matches)) {
            foreach ($matches as $match) {
                if (isset($GLOBALS[$match[1]])) {
                    $value = str_replace($match[0], $GLOBALS[$match[1]], $value);
                } else {
                    $value = str_replace($match[0], '', $value);
                }
            }
        }

        return $value;
    } // processLangElements

} // sm2emailmarketingParams()


function sm2emailmarketingConfig($option) {
    global $database, $sm2emailmarketingConfig, $mosConfig_absolute_path;

    $database->setQuery('SELECT `params` FROM #__components WHERE `link`='.$database->Quote('option='.$option).' AND `option`='.$database->Quote($option));
    $row = null;
    $database->loadObject($row);

    if (!is_object($row)) {
        $row = new stdClass();
        $row->params = '';
    }

    $sm2emailmarketingConfig = new sm2emailmarketingParams($row->params, $mosConfig_absolute_path.'/administrator/components/'.$option.'/config.sm2emailmarketing.xml', 'config');

} // sm2emailmarketingConfig()


function getPluginParams($option, $filename) {
    global $mosConfig_absolute_path;

    $row = new sm2emailmarketingPlugin();
    $row->loadFromFilename($filename);

    $params =& new sm2emailmarketingParams( $row->params, $mosConfig_absolute_path.'/administrator/components/'.$option.'/plugins/'.str_replace('.php', '.xml', $filename), 'sm2emailmarketing_plugin' );

    return $params;
} // getPluginParams()


function getPluginEnabledFromFilename($option, $filename) {

    $row = new sm2emailmarketingPlugin();
    $row->loadFromFilename($filename);

    return $row->enabled;

} // getPluginEnabledFromFilename()


function updatesm2emailmarketingUsers() {
    global $database;

    // add any new registered users who are not in the emktg user table
    $query = 'SELECT u.* '
        .' FROM #__users u '
        .' LEFT JOIN #__emktg_user au ON u.id = au.id'
        .' WHERE au.id IS NULL';
    $database->setQuery($query);
    $rows = $database->loadObjectList();
    $error = $database->getErrorMsg();

    // quit on error
    if (!empty($error)) {
        echo '<p><b>Error (admin.sm2emailmarketing.php-> line ' . __LINE__ . '):</b> '._EMKTG_USER_UPDATE_ERROR1.': <br />' . $error . '</p>';
        return false;
    }

    if (is_array($rows)) {
        foreach ($rows as $row) {
            $query = 'INSERT INTO #__emktg_user (`id`, `subscribe_date`)'
                .' VALUES ('.(int)$row->id.', NOW())';
            $database->setQuery($query);
            $database->query();
            $error .= $database->getErrorMsg();
        } // end foreach
    }

    // output error messages
    if (!empty($error)) {
        echo '<p><b>Error (admin.sm2emailmarketing.php-> line ' . __LINE__ . '):</b> '._EMKTG_USER_UPDATE_ERROR2.': <br />' . $error . '</p>';
        return false;
    } // end if

    // delete any deleted registered users who are still in the emktg user table
    $query = 'SELECT au.*'
        .' FROM #__emktg_user au'
        .' LEFT JOIN #__users u ON u.id = au.id'
        .' WHERE u.id IS NULL';
    $database->setQuery($query);
    $rows = $database->loadObjectList();
    $error = $database->getErrorMsg();

    // quit on error
    if (!empty($error)) {
        echo '<p><b>Error (admin.sm2emailmarketing.php-> line ' . __LINE__ . '):</b> '._EMKTG_USER_UPDATE_ERROR1.': <br />' . $error . '</p>';
        return false;
    }

    if (is_array($rows)) {
        foreach ($rows as $row) {
            $query = 'DELETE FROM #__emktg_list_user'
                .' WHERE id='.(int)$row->id;
            $database->setQuery($query);
            $database->query();
            $error .= $database->getErrorMsg();

            $query = 'DELETE FROM #__emktg_user'
                .' WHERE id='.(int)$row->id;
            $database->setQuery($query);
            $database->query();
            $error .= $database->getErrorMsg();
        } // end foreach
    }

    // output error messages
    if (!empty($error)) {
        echo '<p><b>Error (admin.sm2emailmarketing.php-> line ' . __LINE__ . '):</b> '._EMKTG_USER_UPDATE_ERROR2.': <br />' . $error . '</p>';
        return false;
    } // end if

} // updatesm2emailmarketingUsers()


function sm2emailmarketingGetPluginMessageData($messageid, $pluginid) {
    global $database;

    $database->setQuery('SELECT plugin_data'
        .' FROM #__emktg_message_plugin'
        .' WHERE messageid='.(int)$messageid
        .' AND pluginid='.(int)$pluginid);
    $pluginData = $database->loadResult();

    if (!empty($pluginData)) {
        return unserialize($pluginData);
    } else {
        return '';
    }

} // sm2emailmarketingGetPluginMessageData()


function sm2emailmarketingSetPluginMessageData($messageid, $pluginid, $pluginData) {
    global $database;

    $pluginData = serialize($pluginData);

    // see if there is an existing record
    $database->setQuery('SELECT COUNT(*) FROM #__emktg_message_plugin'
        .' WHERE messageid='.(int)$messageid.' AND pluginid='.(int)$pluginid);
    $count = $database->loadResult();

    if ($count==0) {
        $qrysql = 'INSERT INTO #__emktg_message_plugin (messageid, pluginid, plugin_data)'
            .' VALUES ('.(int)$messageid.', '.(int)$pluginid.', '.$database->Quote($pluginData).')';
    } else {
        $qrysql = 'UPDATE #__emktg_message_plugin SET plugin_data='.$database->Quote($pluginData)
            .' WHERE messageid='.(int)$messageid.' AND pluginid='.(int)$pluginid;
    }
    $database->setQuery($qrysql);

    return $database->query();

} // sm2emailmarketingSetPluginMessageData


function sm2emailmarketingQueueInsert($messageid, $userid, $pluginid, $name, $email, $receive_html) {
    global $database, $mosConfig_mailfrom, $sm2emailmarketingConfig;
    static $domain=null;
    static $blackListQueue = null;
    static $tablename = null;

    // initialise the static variables so they only have to be processed once
    if (is_null($domain)) {
        $domainParts = explode('@', $mosConfig_mailfrom);
        $domain = $domainParts[1];
    }

    if (is_null($blackListQueue)) {
        $blackListQueue = $sm2emailmarketingConfig->get('blacklist_queue', 0);
    }

    if (is_null($tablename)) {
        if ($sm2emailmarketingConfig->get('plugin_join', 1)) {
            $tablename = '#__emktg_queue_preprocess';
        } else {
            $tablename = '#__emktg_stats_message';
        }
    }

    // see if we should process the queue for blacklist
    if ($blackListQueue) {
        if (sm2emCheckBlackList($email)) return;
    }

    //@TODO don't add any 'noemail' + domain
    if (preg_match('/^noemail_\d+\.?\d+\@'.$domain.'$/', $email)) return;
    if (substr($email, 0, 8)=='noemail_') return;

    $database->setQuery('INSERT IGNORE INTO '.$tablename
        .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
        .' VALUES'
        .' ('.(int)$messageid.', '.(int)$userid.', '.(int)$pluginid.', '.$database->Quote($name).', '
        .$database->Quote($email).', '.(int)$receive_html.')');
    $database->query();

} // sm2emailmarketingQueueInsert()


function determineSessionTableSuffix() {
    global $sm2emailmarketingConfig;
    static $suffix = null;

    if ($suffix===null) {
        $suffix = '';
        if ($sm2emailmarketingConfig->get('temp_tables', 'temporary')=='temporary') {
            $suffix = '';
        } else {
            // determine the session information
            $session_id = mosGetParam( $_SESSION, 'session_id', '' );
            if (!empty($session_id)) {
                $suffix = '_'.$session_id;
            }
        }
    }

    return $suffix;
} // determineSessionTableSuffix()


function determineQueueProcessTable($preview=false) {
    global $sm2emailmarketingConfig;
    static $tablename = null;

    if ($tablename===null) {
        if ($sm2emailmarketingConfig->get('plugin_join', 1)) {
            $tablename = '#__emktg_queue_preprocess'
                .determineSessionTableSuffix();
        } else {
            $tablename = '#__emktg_stats_message';
            if ($preview) {
                $tablename .= determineSessionTableSuffix();
            }
        }
    }

    return $tablename;
} // determineQueueProcessTable()


function sm2emailmarketingPatTemplate($template, $option) {
    global $mosConfig_absolute_path, $emErrorHandler;
    global $mosConfig_live_site, $task, $emktgSection, $sm2emailmarketingConfig;
    global $mainframe;

    if (!class_exists('patFactory'))
        require_once($mosConfig_absolute_path.'/includes/patTemplate/patTemplate.php');

    // create the template
    if (defined('_JLEGACY'))
        $tmpl =& patFactory::createTemplate($option);
    else
        $tmpl =& patFactory::createTemplate();

    $tmpl->addModuleDir('Function', $mosConfig_absolute_path.'/administrator/components/'.$option.'/functions');

    // set the path to look for html files
    if ($mainframe->isAdmin()) {
        $root = $mosConfig_absolute_path.'/administrator/components/'.$option.'/admintmpl';
        $location = $mosConfig_live_site.'/administrator/index2.php?option='.$option.'&amp;task=';
    } else {
        $root = $mosConfig_absolute_path.'/components/'.$option.'/tmpl';
        $location = $mosConfig_live_site.'/index.php?option='.$option.'&task=';
    }
    $tmpl->setRoot( $root );
    $tmpl->setNameSpace('jos');
    if (file_exists($root.'/common.tpl')) {
    	$tmpl->readTemplatesFromFile( 'common.tpl' );
	}
    $tmpl->readTemplatesFromInput($template);

    $turl = $mosConfig_live_site.'/includes/js/tabs/';

    $tmpl->addGlobalVar('TASK', $task);
    $tmpl->addGlobalVar('SECTION', $emktgSection);
    $tmpl->addGlobalVar('LOCATION', $location);
    if (defined('_SM2EM_LISTS')) {
        $tmpl->addVar('adminMenu', 'SHOWLIST', 1);
    }
    if ($mainframe->isAdmin()) {
        if ($sm2emailmarketingConfig->get('bounce',0) && function_exists('imap_open')) {
            $tmpl->addVar('adminMenu', 'SHOWBOUNCE', 1);
        }
        if (defined('_SM2EM_LITE')) {
            $tmpl->addVar('header', 'SM2EM_LITE', 1);
        }
    }
    $tmpl->addVar('sm2includeTabs', 'taburl', $turl);
    $tmpl->addVar('message', 'ERROR', $emErrorHandler->getErrors());

    // Joomla! 1.5 hacks
    if (defined('_JLEGACY')) {
        $tmpl->addGlobalVar('JLEGACY', _JLEGACY);
        $tmpl->addGlobalVar('OPTION', $option);
        $tmpl->addGlobalVar('SITEURL', $mosConfig_live_site);
    }

    return $tmpl;
} // sm2emailmarketingPatTemplate()


function sm2emailmarketingLanguage() {
    $lang = new stdClass();

    $constants = get_defined_constants();
    foreach ($constants as $index=>$value) {
        if ($index[0]!='_') continue;
        $lang->$index = $value;
    }
    unset($constants);

    return $lang;
} // sm2emailmarketingLanguage()

function sm2emailmarketing_addAbsolutePath(&$text) {
	global $mosConfig_live_site;

	$matches = array();
    preg_match_all('/(href|src)="(.*)"/iU', $text, $matches, PREG_SET_ORDER);

    foreach ($matches as $match) {
    	if ((strpos($match[2], 'http')===false)
            && (substr(strtolower($match[2]), 0, 7) != 'mailto:')
            && (substr($match[2], 0, 1) != '#')
            && (substr($match[2], 0, 4) != 'cid:')) {
    		$text = str_replace($match[0], $match[1].'="'.$mosConfig_live_site.'/'.$match[2].'"', $text);
    	}
    }
} // sm2emailmarketing_addAbsolutePath()

function sm2emailmarketingProcessCopyTitle($text) {
    // look for the copy of text
    $matches = null;
    if (preg_match('/^copy (\((\d+)\) )?of/i', $text, $matches)) {
        // at least one match was found (in fact it should be only one)

        // determine if there is a number to process
        if (!empty($matches[2])) {
            // this is a copy (x) of already
            $text = str_replace($matches[0], 'Copy ('.($matches[2] + 1).') of', $text);
        } else {
            // this is a copy of already
            // to be safe we will replace all copy of with nothing first
            $text = preg_replace('/copy of /i', '', $text);
            // now set to copy (2) of
            $text = 'Copy (2) of '.$text;
        }
    } else {
        // add the copy of to the start of this
        $text = 'Copy of '.$text;
    }

    return $text;

} // sm2emailmarketingProcessCopyTitle

/**
 * function to process a message
 *
 * @param integer $id Id of the message being processed
 * @param object $message Object containing all of the details of the message
 * @param object $queueItem Object containing the data of the queued item. i.e. user details
 * @param array $plugins Array of Objects containing the details of the plugins available
 * @param string $option current option being processed
 * @param boolean $preview Boolean Flag indicating whether the message is a preview and should not display some things
 * @return array Simple array with 2 elements message_html and message_text
 */
function sm2emProcessMessage($id, $message, $queueItem, $plugins, $option, $preview = false) {
    global $mosConfig_absolute_path;

    $message->replaceUserInfo($queueItem->name, $queueItem->email, $queueItem->userid);

    // process the plugins
    foreach ($plugins as $plugin) {
        // load the plugin
        if (!file_exists($mosConfig_absolute_path.'/administrator/components/'.$option.'/plugins/'.$plugin->filename)) continue;
        require_once($mosConfig_absolute_path.'/administrator/components/'.$option.'/plugins/'.$plugin->filename);
        $class = $plugin->classname;
        $obj = new $class();
        // process the send of the plugin
        if (method_exists($obj, 'processMessage')) {
            $obj->processMessage($id, $plugin->pluginid, $option, $message, $queueItem->userid);
        }
    } // foreach()


    // process the unsubscribe link and image tracking and get the message html and text
    if (!$preview) {
        list ($message_html, $message_text) = sm2emUnsubscribeLinkImageTracking($queueItem->id,
            $queueItem->email, $queueItem->userid, $message->message_html, $message->message_text, $option);
    }

    sm2emailmarketing_addAbsolutePath($message_html);

    return array($message_html, $message_text);

} // sm2emProcessMessage

/**
 * function to process the unsubscribelink and add the tracking image for a user
 *
 * Note:
 *   $message_html and $message_text are no by reference (passthru) because we dont
 *   want to change their original values otherwise the loop process will only work
 *   the first time
 *
 * @param integer $id Queue Id for this user
 * @param string $email Email address of the user
 * @param integer $userid User id of the user
 * @param string $message_html HTML of the message
 * @param string $message_text TEXT of the message
 * @param string $option current option being processed
 * @return array Simple array with 2 elements message_html and message_text
 */
function sm2emUnsubscribeLinkImageTracking($id, $email, $userid, $message_html, $message_text, $option) {
    global $mosConfig_live_site, $sm2emailmarketingConfig, $mosConfig_sef;

    // unsubscribe code
    $code = md5($userid.$email);
    $unSubscribeUrl = 'index.php?option='.$option.'&task=unsubscribe&email='.$email.'&code='.$code;
    if (function_exists('sefRelToAbs') && !defined('_JLEGACY')) {
        $unSubscribeUrl = sefRelToAbs($unSubscribeUrl);
    } else {
        $unSubscribeUrl = $mosConfig_live_site.'/'.$unSubscribeUrl;
    }

    if (strpos($message_html, '[UNSUBSCRIBE]')===false) $message_html .= '[UNSUBSCRIBE]';
    if (strpos($message_text, '[UNSUBSCRIBE]')===false) $message_text .= '[UNSUBSCRIBE]';

    // get the unsubscribe message from the config and make sure it doesnt have a [UNSUBSCRIBE] in it like it used to
    $message_unsubscribe = str_replace('[UNSUBSCRIBE]', '', $sm2emailmarketingConfig->get('unsubscribe_text', _EMKTG_UNSUBSCRIBE_CLICKTOCONFIRM));
    if (empty($message_unsubscribe)) $message_unsubscribe = _EMKTG_UNSUBSCRIBE_CLICKTOCONFIRM;
    if (defined('_SM2EM_FORWARD')) {
        $message_html = str_replace('[UNSUBSCRIBE]', '', $message_html);
        $message_text = str_replace('[UNSUBSCRIBE]', '', $message_text);
    } else {
        $message_html = str_replace('[UNSUBSCRIBE]', '<a href="'.$unSubscribeUrl.'">'.$message_unsubscribe.'</a>', $message_html);
        $message_text = str_replace('[UNSUBSCRIBE]', $message_unsubscribe.' '.$unSubscribeUrl, $message_text);
    }

    //tracking code
    $message_html .= '<img src="'.$mosConfig_live_site.'/index2.php?option='.$option.'&task=image&id='.$id.'" border="0" height="1" width="1" />';

    return array($message_html, $message_text);

} // sm2emUnsubscribeLinkImageTracking()

function sm2EM_CheckUpgrade($option, $task) {
    global $mosConfig_absolute_path, $mosConfig_live_site;

    // development test
    if (substr($mosConfig_live_site, -6)=='.local' && $task != 'showhelp') {
        return;
    }

    // check to see if there is anything to process
    $path = $mosConfig_absolute_path.'/administrator/components/'.$option.'/upgrade';

    $dir = dir($path);

    $processUpgrade = false;
    while (false !== ($file = $dir->read())) {
        if ($file=='.' || $file=='..') continue;
        $processUpgrade = true;
    }

    if (!$processUpgrade) return;

    include_once($mosConfig_absolute_path.'/administrator/components/'.$option.'/sm2emailmarketing.upgrade.php');

    sm2EM_Upgrade($option, $task, $path);

} // sm2EM_CheckUpgrade()

function sm2EM_PrepareQueue($option, $preview=false) {
    global $database, $sm2emailmarketingConfig;

    $tableType = 'TEMPORARY';
    $sessionSuffix = determineSessionTableSuffix();
    if (!empty($sessionSuffix)) $tableType = '';

    $tables = array(
        'preprocess'=>'CREATE '.$tableType.' TABLE '
            .'IF NOT EXISTS '
            .'`#__emktg_queue_preprocess'.$sessionSuffix.'` ('
            .'`id` INT UNSIGNED NOT NULL AUTO_INCREMENT, '
            .'`messageid` INT NOT NULL, '
            .'`userid` INT NOT NULL, '
            .'`pluginid` INT UNSIGNED NOT NULL, '
            .'`name` VARCHAR(100) NOT NULL, '
            .'`email` VARCHAR(254) NOT NULL, '
            .'`html` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0, '
            .'PRIMARY KEY (`id`), '
            .'UNIQUE `uniq` (`messageid`, `userid`, `pluginid`, `email`)'
            .')TYPE=MyISAM;',
        'preprocess_trunc'=>'DELETE FROM '
            .'`#__emktg_queue_preprocess'.$sessionSuffix.'`',
        'postprocess'=>'CREATE '.$tableType.' TABLE '
            .'IF NOT EXISTS '
            .'`#__emktg_queue_postprocess'.$sessionSuffix.'` ('
            .'`id` INT UNSIGNED NOT NULL AUTO_INCREMENT, '
            .'`messageid` INT NOT NULL, '
            .'`userid` INT NOT NULL, '
            .'`pluginid` INT UNSIGNED NOT NULL, '
            .'`name` VARCHAR(100) NOT NULL, '
            .'`email` VARCHAR(254) NOT NULL, '
            .'`html` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0, '
            .'`numplugins` INT NOT NULL DEFAULT 0, '
            .'PRIMARY KEY (`id`), '
            .'UNIQUE `uniq` (`messageid`, `userid`, `pluginid`, `email`)'
            .')TYPE=MyISAM;',
        'postprocess_trunc'=>'DELETE FROM '
            .'`#__emktg_queue_postprocess'.$sessionSuffix.'`',
        'preview'=>'CREATE '.$tableType.' TABLE '
            .'IF NOT EXISTS '
            .'`#__emktg_stats_message'.$sessionSuffix.'` ('
            .'`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, '
            .'`messageid` INT(11) NOT NULL, '
            .'`userid` INT(11) NOT NULL, '
            .'`pluginid` INT(11) NOT NULL, '
            .'`name` VARCHAR(100) NOT NULL, '
            .'`email` VARCHAR(254) NOT NULL, '
            .'`html` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0, '
            .'`read` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0, '
            .'`status` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0, '
            .'`send_date` DATETIME NOT NULL DEFAULT '
            .$database->Quote('0000-00-00 00:00:00').', '
            .'PRIMARY KEY (`id`), '
            .'UNIQUE `uniq` (`messageid`, `email`), '
            .'INDEX `userid` (`userid`), '
            .'INDEX `pluginid` (`pluginid`), '
            .'INDEX `name` (`name`), '
            .'INDEX `email` (`email`), '
            .'INDEX `status` (`status`), '
            .'INDEX `send` (`send_date` ASC)'
            .')TYPE=MyISAM;',
        'preview_trunc'=>'DELETE FROM '
            .'`#__emktg_stats_message'.$sessionSuffix.'`'
    );

    if ($sm2emailmarketingConfig->get('plugin_join', 1)) {
        // we are using and so create the temporary tables
        // create the preprocess table first
        $database->setQuery($tables['preprocess']);
        $database->query();
        // make sure the preprocess table is empty
        $database->setQuery($tables['preprocess_trunc']);
        $database->query();

        // now create the postprocess table
        $database->setQuery($tables['postprocess']);
        $database->query();
        // make sure the postprocess table is empty
        $database->setQuery($tables['postprocess_trunc']);
        $database->query();
    }

    // create a temporary copy of the message stats table as temporary
    // which will replace the real table for the duration of this session
    if ($preview) {
        $database->setQuery($tables['preview']);
        $database->query();
        // make sure the preview queue table is empty
        $database->setQuery($tables['preview_trunc']);
        $database->query();
    }


    // force the call to determineQueueProcessTable now
    // so we can be sure it is set correctly
    // because we know about the preview status here
    determineQueueProcessTable($preview);

} // sm2EM_PrepareQueue

function sm2EM_FinaliseQueue($totalPlugins=0, $option, $preview=false) {
    global $database, $sm2emailmarketingConfig;

    $sessionSuffix = determineSessionTableSuffix();

    // determine which queue table to use
    $statsMessage = '#__emktg_stats_message';
    if ($preview) {
        $statsMessage .= $sessionSuffix;
    }

    if ($sm2emailmarketingConfig->get('plugin_join', 1)) {
        // we are using and so process the temporary table data

        // find all of the users who have been found in each of the plugins
        // insert the data in the postprocess table so we can look at the numplugins
        $database->setQuery('INSERT IGNORE INTO '
            .'#__emktg_queue_postprocess'.$sessionSuffix.' '
            .'(`messageid`, `userid`, `pluginid`, `name`, `email`, `html`, `numplugins`) '
            .'SELECT messageid, userid, min(pluginid) AS pluginid, name, email, `html`, COUNT(*) AS numplugins '
            .'FROM #__emktg_queue_preprocess'.$sessionSuffix.' `
            .`GROUP BY messageid, userid, name, email, `html`');
        $database->query();

        // find only those records that >= the totalPlugins
        // and insert into the emktg stats message table
        // duplicates will be ignored without errors or warnings
        $database->setQuery('INSERT IGNORE INTO '
            .$statsMessage.' '
            .'(`messageid`, `userid`, `pluginid`, `name`, `email`, `html`) '
            .'SELECT messageid, userid, pluginid, name, email, `html` `
            .`FROM #__emktg_queue_postprocess'.$sessionSuffix.' '
            .'WHERE numplugins >='.(int)$totalPlugins);
        $database->query();

        // delete the temporary table (even though this should automatically happen)
        $database->setQuery('DROP TABLE #__emktg_queue_preprocess'.$sessionSuffix);
        $database->query();
        $database->setQuery('DROP TABLE #__emktg_queue_postprocess'.$sessionSuffix);
        $database->query();
    }

} // sm2EM_FinaliseQueue

function sm2emCheckBlackList($email) {
    static $whitelist = null;
    static $blacklist = null;
    global $sm2emailmarketingConfig;

    if (!is_array($whitelist)) {
        $whitelist = str_replace('<br />', "\n", $sm2emailmarketingConfig->get('whitelist', ''));
        $whitelist = str_replace("\n\n", "\n", $whitelist);
        $whitelist = explode("\n", $whitelist);
        if (!is_array($whitelist)) $whitelist = array();
        if (count($whitelist)==1) {
            if (empty($whitelist[0])) {
                $whitelist = array();
            }
        }
        foreach ($whitelist as $index=>$pattern) {
            $pattern = trim($pattern);
            if (empty($pattern)) {
                unset($whitelist[$index]);
                continue;
            }
            $whitelist[$index] = '/'.str_replace('*', '.*', trim($pattern)).'/';
        }
    }
    if (!is_array($blacklist)) {
        $blacklist = str_replace('<br />', "\n", $sm2emailmarketingConfig->get('blacklist', ''));
        $blacklist = str_replace("\n\n", "\n", $blacklist);
        $blacklist = explode("\n", $blacklist);
        if (!is_array($blacklist)) $blacklist = array();
        if (count($blacklist)==1) {
            if (empty($blacklist[0])) {
                $blacklist = array();
            }
        }
        foreach ($blacklist as $index=>$pattern) {
            $pattern = trim($pattern);
            if (empty($pattern)) {
                unset($blacklist[$index]);
                continue;
            }
            $blacklist[$index] = '/'.str_replace('*', '.*', trim($pattern)).'/';
        }
    }

    // check the whitelist
    foreach ($whitelist as $pattern) {
        if (preg_match($pattern, $email)) {
            return false;
        }
    }

    // check the blacklist
    foreach ($blacklist as $pattern) {
        if (preg_match($pattern, $email)) {
            return true;
        }
    }

    return false;
} // sm2emCheckBlackList()


function sm2emAdminUnsubscribeEmail($row) {
    global $sm2emailmarketingConfig;
    global $mosConfig_mailfrom, $mosConfig_fromname;

    // determine if we should process

    // return if this option is disabled
    if (!$sm2emailmarketingConfig->get('admin_unsubscribe', 0)) return;
    // return if there is no data
    if (empty($row) || !is_object($row)) return;

    // determine the reason for unsubscribing
    if (empty($row->unsubscribe_reason)) {
        $row->unsubscribe_reason = 5;
    }
    // return if admin
    if (($row->unsubscribe_reason==5) && (!$sm2emailmarketingConfig->get('admin_unsubscribe_admin', 0))) return;
    // return if there is no name data
    if (empty($row->name)) return;
    // return if there is no email address
    if (empty($row->email)) return;


    // gather the data to be emailed
    $row->time = strftime(_DATE_FORMAT_LC2);
    if (defined('_EMKTG_UNSUBSCRIBE_REASON_'.$row->unsubscribe_reason)) {
        $row->reason = constant('_EMKTG_UNSUBSCRIBE_REASON_'.$row->unsubscribe_reason);
    } else {
        $row->reason = _EMKTG_UNSUBSCRIBE_REASON_5;
    }

    // prepare the message
    $subject = $sm2emailmarketingConfig->get('admin_unsubscribe_subject', _EMKTG_CONFIGURATION_UNSUBSCRIBE_ADMIN_SUBJECT_DEFAULT);
    $msg = $sm2emailmarketingConfig->get('admin_unsubscribe_text', _EMKTG_CONFIGURATION_UNSUBSCRIBE_ADMIN_MSG_DEFAULT);

    $msg = str_replace('<br />', "\n", $msg);

    $msg = str_replace('[NAME]', $row->name, $msg);
    $msg = str_replace('[EMAIL]', $row->email, $msg);
    $msg = str_replace('[REASON]', $row->reason, $msg);
    $msg = str_replace('[TIME]', $row->time, $msg);

    mosMail($mosConfig_mailfrom, $mosConfig_fromname, $mosConfig_mailfrom, $subject, $msg);

} // sm2emAdminUnsubscribeEmail()


function sm2emAdminUnsubscribeEmailFromQuery($query) {
    global $database, $sm2emailmarketingConfig;

    // return if this option is disabled
    if (!$sm2emailmarketingConfig->get('admin_unsubscribe', 0)) return;

    // get the data to process
    $database->setQuery($query);
    $rows = $database->loadObjectList();

    foreach ($rows as $row) {
        sm2emAdminUnsubscribeEmail($row);
    }

} // sm2emAdminUnsubscribeEmailFromQuery()

/**
 * Function to test whether the correct permissions
 * are assigned to the Joomla! database user.
 * Attempts to use SHOW GRANTS first
 * If this fails it will attempt to create a temporary
 * table and insert to it as the test.
 *
 */
function testJoomlaDatabaseUserPrivileges() {
    global $database, $emErrorHandler, $sm2emailmarketingConfig;

    // do not check if already set to session
    if ($sm2emailmarketingConfig->get('temp_tables', 'temporary')=='session')
        return true;

    // first determine the current user
    // not necessary in newer mysql version
    $database->setQuery('SELECT CURRENT_USER()');
    // break the result into parts on the @
    $currentUser = explode('@', @$database->loadResult());
    // quote each part
    foreach ($currentUser as $index=>$part) {
        $currentUser[$index] = $database->Quote($part);
    }
    // before joining the parts back together again
    $currentUser = implode('@', $currentUser);

    // query to get the grants
    $database->setQuery('SHOW GRANTS FOR '.$currentUser);
    $grants = @$database->loadResultArray();

    $found = false;
    $createTemp = false;
    $currentDB = '`'.$GLOBALS['mosConfig_db'].'`';

    if (is_array($grants)) {
        // look at each grant
        foreach ($grants as $grant) {
            // fix for directadmin database creation
            $grant = stripslashes($grant);
            if (strpos($grant, $currentDB) ||
                strpos($grant, '*.*')) {
                if (strpos($grant, 'ALL PRIVILEGES')) {
                    $found = true;
                    $createTemp = true;
                }
                if (strpos($grant, 'CREATE TEMPORARY TABLE')) {
                    $found = true;
                    $createTemp = true;
                    break;
                }
            }
        }
    }

    // could not determine if the privilege is set
    // via the grant command
    if (!$found) {
        // attempt to create a temporary table and insert
        $database->setQuery('CREATE TEMPORARY TABLE `sm2em_temp` ('
            .' `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,'
            .' `text` VARCHAR(100),'
            .' PRIMARY KEY (`id`)'
            .' )TYPE=MyISAM;');
        if (!@$database->query()) {
            $found = true;
            $createTemp = false;
        }

        if (!$found) {
            $database->setQuery('INSERT INTO `sm2em_temp` (`text`)'
                .' VALUES ('.$database->Quote('test').')');
            if (!@$database->query()) {
                $found = true;
                $createTemp = false;
            }

            if (!$found) {
                $database->setQuery('DROP TABLE `sm2em_temp`');
                @$database->query();
                $found = true;
                $createTemp = true;
            }
        }
    }

    if (!$found) {
        $emErrorHandler->addError(_EMKTG_PRIV_UNKNOWN);
    } else if (!$createTemp) {
        $emErrorHandler->addError(_EMKTG_PRIV_UNSET);
    }

} // testJoomlaDatabaseUserPrivileges()


/**
 * Function to remove any temporary tables for a user if they exist
 */
function cleanupTemporaryTables() {
    global $database;

    $sessionSuffix = determineSessionTableSuffix();
    if (empty($sessionSuffix)) return;

    $database->setQuery('DROP TABLE IF EXISTS '
        .'#__emktg_queue_preprocess'.$sessionSuffix.', '
        .'#__emktg_queue_postprocess'.$sessionSuffix.', '
        .'#__emktg_stats_message'.$sessionSuffix);
    $database->query();
} // cleanupTemporaryTables()


/**
 * Function to remove all unused session tables
 */
function cleanupSessionTemporaryTables() {
    global $database;

    $session_id = mosGetParam( $_SESSION, 'session_id', '' );

    $database->setQuery('SELECT session_id'
        .' FROM #__session');
    $sessions = $database->loadResultArray();

    $tablePatterns = array(
        '#__emktg_queue_preprocess',
        '#__emktg_queue_postprocess',
        '#__emktg_stats_message'
    );

    $dropTables = array();

    // process all of the table patterns
    foreach ($tablePatterns as $like) {
        // find all of the tables that match the like
        $database->setQuery('SHOW TABLES LIKE '
            .$database->Quote(
                $database->replacePrefix($like.'_%')));
        $tables = $database->loadResultArray();

        if (!is_array($tables)) continue;

        // process the tables found
        foreach ($tables as $table) {
            // grab the session information
            $parts = explode('_', $table);
            $tableSession = array_pop($parts);
            unset ($parts);
            // skip if we are in the sessions and not the current session
            if (in_array($tableSession, $sessions)
                && $session_id!=$tableSession) continue;

            // if we get here the table needs to be deleted
            $dropTables[] = $table;
        }
    }

    // delete any tables found
    if (count($dropTables)) {
        $toDropTables = array_chunk($dropTables, 5);
        foreach ($toDropTables as $chunk) {
            $database->setQuery('DROP TABLE IF EXISTS '
                .implode(', ', $chunk));
            $database->query();
        }
    }

    return $dropTables;

} // cleanupSessionTemporaryTables()


/**
 * Function to get the lists that the user is allowed access to
 */
function getAvailableLists() {
    global $my, $database;
    static $lists = null;

    if ($lists===null) {
        // get the list of lists I have access to
        $database->setQuery('SELECT listid FROM #__emktg_list'
            .' WHERE access<='.(int)$my->gid.' AND published=1');
        $lists = $database->loadResultArray();

        if (!is_array($lists)) $lists = array();
    }

    return $lists;
} // getAvailableLists()

/**
 * Load the SEF code
 */
function sm2emSEF() {
    global $mosConfig_absolute_path, $mosConfig_live_site, $mosConfig_sef;
    global $sm2emailmarketingConfig;

    if (!$sm2emailmarketingConfig->get('sef', 0)) return;

    if (file_exists( $mosConfig_absolute_path .'/components/com_sef/sef.php' )) {
        require_once( $mosConfig_absolute_path .'/components/com_sef/sef.php' );
    } else {
        require_once( $mosConfig_absolute_path .'/includes/sef.php' );
    }
} // sm2emSEF()


/**
 * Joomla! 1.5 specific fixes
 */
if (defined('_JLEGACY')) {
    // bug in Joomla! 1.5.0 whereby patFactory is not registered for autoload correctly
    $classes = JLoader::register('patFactory' , $GLOBALS['mosConfig_absolute_path'].DS.'plugins'.DS.'system'.DS.'legacy'.DS.'patfactory.php');
}
?>