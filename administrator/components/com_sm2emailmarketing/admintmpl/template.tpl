<jos:comment>
@package Joomla!
@subpackage com_sm2emailmarketing
@copyright Copyright (C) 2006 SM2 / All Rights Reserved
@license commercial
@author SM2 <info@sm2joomla.com>
</jos:comment>

<sm2:tmpl name="sm2include" src="common.tpl" parse="on" />
<jos:tmpl name="header">
      <th style="background: url(./components/{OPTION}/images/templateManager.gif) no-repeat left; height: 70px; padding-left: 80px;" width="100%" align="left">
        {FORM_NAME}
      </th>
</jos:tmpl>

<jos:tmpl name="manager">
<jos:link src="menuCSS" />
<jos:link src="adminMenu" />
<jos:link src="error" />
<form action="index2.php" method="post" name="adminForm">

  <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminheading">
    <tr>
<jos:link src="header" />
      <td valign="bottom">{LANG_EMKTG_SEARCH_TITLE}</td>
      <td valign="bottom"><input type="text" name="search" value="{SEARCH}" class="inputbox" onChange="document.adminForm.submit();" /></td>
      <td valign="bottom"><input type="submit" value="Submit"></td>
    </tr>
  </table>
  <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
    <tr>
      <th width="20" align="center" nowrap><input type="checkbox" name="toggle" value="" onclick="checkAll({NUM_ROWS});" /></th>
      <th class="title" width="90%" nowrap>{LANG_EMKTG_TEMPLATE_NAME}</th>
      <th width="10%" align="center" nowrap>{LANG_CMN_PUBLISHED}</th>
    </tr>
    <jos:tmpl name="rows" type="simplecondition" requiredvars="ROW_ID" varscope="manager">
    <tr class="row{ROW_K}">
      <td width="20" align="center">
        {ROW_CHECKEDDISPLAY}
      </td>
      <td width="90%">
        <a href="{ROW_LINK}" title="{LANG_EMKTG_TEMPLATE_EDIT}"><jos:var name="ROW_TEMPLATE_NAME" modifier="htmlspecialchars" modifierType="php" /></a>
      </td>
      <td width="10%" align="center">{ROW_PUBLISHEDDISPLAY}</td>
    </tr>
    </jos:tmpl>
  </table>
  {LISTFOOTER}
  <input type="hidden" name="option" value="{OPTION}" />
  <input type="hidden" name="task" value="showtemplates" />
  <input type="hidden" name="boxchecked" value="0" />
  <input type="hidden" name="hidemainmenu" value="0" />
  <input type="hidden" name="action" value="" />
  <input type="hidden" name="return" value="mngr" />
</form>
</jos:tmpl>

<jos:tmpl name="edit">
<jos:link src="menuCSS" />
<jos:link src="adminMenu" />
    <script language="javascript" type="text/javascript">
    function submitbutton(pressbutton) {
      var form = document.adminForm;

      // if cancel then dont validate
      if (pressbutton == 'showtemplates') {
        submitform(pressbutton);
        return;
      }

      // do field validation
      var msg = "";
      // validate title
      if (form.template_name.value=='') {
        msg += "{LANG_EMKTG_TEMPLATE_NAME_DESC}.\n";
      }

      // display validation error or submit
      if (msg!="") {
        alert( "{LANG_EMKTG_VALIDATION_ERROR}\n"+msg );
      } else {
        submitform( pressbutton );
      }
    }
    </script>
    <jos:link src="error" />
    <form action="index2.php" method="post" name="adminForm">
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminheading">
      <tr>
<jos:link src="header" />
      </tr>
    </table>
    <div class="quickTips">
    	<dl id="toggleDl">
    		<dt>{LANG_EMKTG_QUICKTIP}</dt>
    		<dd>{LANG_EMKTG_QUICKTIP_TEMPLATE}</dd>
    	</dl>
    </div>
    <table cellpadding="4" cellspacing="1" border="0" class="adminform">
      <tr>
        <td><jos:emktgtooltip>_EMKTG_TEMPLATE_NAME</jos:emktgtooltip>&nbsp;{LANG_EMKTG_REQUIRED}</td>
        <td>
          <input class="inputbox" type="text" name="template_name" tabindex="1" value="{ROW_TEMPLATE_NAME}" size="50" maxlength="100" />
        </td>
      </tr>
      <tr>
        <td valign="top"><jos:emktgtooltip>_EMKTG_TEMPLATE_LAYOUT_HTML</jos:emktgtooltip></td>
        <td>
          <textarea class="inputbox" name="layout_html" tabindex="3" cols="70" rows="20">{ROW_LAYOUT_HTML}</textarea>
        </td>
      </tr>
      <tr>
        <td valign="top"><jos:emktgtooltip>_EMKTG_TEMPLATE_LAYOUT_TEXT</jos:emktgtooltip></td>
        <td>
          <textarea class="inputbox" name="layout_text" tabindex="4" cols="70" rows="20">{ROW_LAYOUT_TEXT}</textarea>
        </td>
      </tr>
      <tr>
        <td><jos:emktgtooltip>_EMKTG_TEMPLATE_PUBLISH</jos:emktgtooltip></td>
        <td>
          {LIST_PUBLISHED}
        </td>
      </tr>
    </table>
    <input type="hidden" name="option" value="{OPTION}" />
    <input type="hidden" name="templateid" value="{ROW_TEMPLATEID}" />
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="action" value="" />
    <input type="hidden" name="return" value="edit" />
    </form>
</jos:tmpl>

<jos:tmpl name="previewframe">
<jos:link src="menuCSS" />
<jos:link src="adminMenu" />
<jos:link src="error" />
    <script language="javascript" type="text/javascript">
    function submitbutton(pressbutton) {
      document.location.href='{RETURN}';
    }
    </script>
    <form action="index2.php" method="post" name="adminForm">
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminheading">
      <tr>
<jos:link src="header" />
      </tr>
    </table>
    <div class="quickTips">
    	<dl id="toggleDl">
    		<dt>{LANG_EMKTG_QUICKTIP}</dt>
    		<dd>{LANG_EMKTG_QUICKTIP_TEMPLATE_PREVIEW}</dd>
    	</dl>
    </div>
    <table cellpadding="4" cellspacing="1" border="0" class="adminform">
      <tr>
        <td>
          <iframe
            src="index2.php?option={OPTION}&task=previewtemplate&action=preview&no_html=1&cid[]={ID}"
            width="100%"
            height="500"
            scrolling="auto"
            align="top"
            frameborder="0"></iframe>
        </td>
      </tr>
    </table>
    <input type="hidden" name="option" value="{OPTION}" />
    <input type="hidden" name="templateid" value="{ID}" />
    <input type="hidden" name="task" value="" />
    </form>
</jos:tmpl>