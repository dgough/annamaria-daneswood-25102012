<jos:comment>
@package Joomla!
@subpackage com_sm2emailmarketing
@copyright Copyright (C) 2006 SM2 / All Rights Reserved
@license commercial
@author SM2 <info@sm2joomla.com>
</jos:comment>

<jos:tmpl name="header">
      <th style="background: url(./components/{OPTION}/images/bounceManager.gif) no-repeat left; height: 70px; padding-left: 80px;" width="100%" align="left">
        {FORM_NAME}
      </th>
</jos:tmpl>

<jos:tmpl name="manager">
<jos:link src="menuCSS" />
<jos:link src="adminMenu" />
<jos:link src="error" />
<form action="index2.php" method="post" name="adminForm">
  <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminheading">
    <tr>
<jos:link src="header" />
    </tr>
  </table>
  <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
    <tr>
      <th class="title" width="20">ID</th>
      <th width="10%" align="left">To</th>
      <th width="40%" align="left">Subject</th>
      <th width="10%" align="right">Size</th>
      <th width="10%" align="center">Bounce</th>
      <th width="10%" align="center">Deleted</th>
      <th width="20%" align="left">Date</th>
    </tr>
    <jos:tmpl name="rows" type="simplecondition" requiredvars="ROW_ID" varscope="manager">
    <tr>
      <td align="right">
        {ROW_ID}
      </td>
      <td>{ROW_TO}</td>
      <td>{ROW_SUBJECT}</td>
      <td align="right">{ROW_SIZE}</td>
      <td align="center"><jos:tmpl varscope="rows" type="condition" conditionvar="ROW_BOUNCED"><jos:sub condition="1">Y</jos:sub><jos:sub condition="__default">N</jos:sub></jos:tmpl></td>
      <td align="center"><jos:tmpl varscope="rows" type="condition" conditionvar="ROW_DELETED"><jos:sub condition="1">Y</jos:sub><jos:sub condition="__default">N</jos:sub></jos:tmpl></td>
      <td>{ROW_DATE}</td>
    </tr>
    </jos:tmpl>
  </table>
  <input type="hidden" name="option" value="{OPTION}" />
  <input type="hidden" name="task" value="showbounce" />
  <input type="hidden" name="bounce_start" value="{START}" />
  <input type="hidden" name="boxchecked" value="0" />
  <input type="hidden" name="hidemainmenu" value="0" />
</form>
<script language="javascript" type="text/javascript">
    function submitbutton(pressbutton) {
      var form = document.adminForm;

      if (form.bounce_start.value!=-1) {
        submitform(pressbutton);
  	  }
    }

    function timerSubmit() {
        submitbutton('showbounce');
    }

    timer=setTimeout('timerSubmit()',10000);
</script>
</jos:tmpl>
