<jos:comment>
@package Joomla!
@subpackage com_sm2emailmarketing
@copyright Copyright (C) 2006 SM2 / All Rights Reserved
@license commercial
@author SM2 <info@sm2joomla.com>
</jos:comment>

<jos:tmpl name="header">
      <th style="background: url(./components/{OPTION}/images/config.gif) no-repeat left; height: 70px; padding-left: 80px;" width="100%" align="left">
        {FORM_NAME}
      </th>
</jos:tmpl>


<jos:tmpl name="tools">
<jos:link src="menuCSS" />
<jos:link src="adminMenu" />
<jos:link src="error" />
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminheading">
      <tr>
<jos:link src="header" />
      </tr>
    </table>
    <table cellpadding="4" cellspacing="1" border="0" class="adminform" width="90%">
      <tr>
        <td>
          <dl>
            <dt><a href="{LOCATION}installtools">{LANG_EMKTG_TOOLS_INSTALL_DEFAULT}</a></dt>
            <dd>{LANG_EMKTG_TOOLS_INSTALL_DEFAULT_DESC}</dd>
            <jos:tmpl type="simplecondition" requiredvars="showclean=1" varscope="tools">
            <dt><a href="{LOCATION}cleantools">{LANG_EMKTG_TOOLS_CLEAN_TEMP}</a></dt>
            <dd>{LANG_EMKTG_TOOLS_CLEAN_TEMP_DESC}</dd>
            </jos:tmpl>
            <jos:tmpl type="simplecondition" requiredvars="showlists=1" varscope="tools">
            <dt><a href="{LOCATION}importunregistered">{LANG_EMKTG_TOOLS_IMPORT_SUBSCRIBERS}</a></dt>
            <dd>{LANG_EMKTG_TOOLS_IMPORT_SUBSCRIBERS_DESC}</dd>
            </jos:tmpl>
          </dl>
          
          <dl>
            <dt><a href="{LOCATION}deltools&amp;type=messages" onclick="return confirm('{LANG_EMKTG_TOOLS_DELETE_MESSAGES_CONFIRM}');">{LANG_EMKTG_TOOLS_DELETE_MESSAGES}</a></dt>
            <dd>{LANG_EMKTG_TOOLS_DELETE_MESSAGES_DESC}</dd>
            <dt><a href="{LOCATION}deltools&amp;type=statistics" onclick="return confirm('{LANG_EMKTG_TOOLS_DELETE_STATISTICS_CONFIRM}');">{LANG_EMKTG_TOOLS_DELETE_STATISTICS}</a></dt>
            <dd>{LANG_EMKTG_TOOLS_DELETE_STATISTICS_DESC}</dd>
            <dt><a href="{LOCATION}deltools&amp;type=subscribers" onclick="return confirm('{LANG_EMKTG_TOOLS_DELETE_SUBSCRIBERS_CONFIRM}');">{LANG_EMKTG_TOOLS_DELETE_SUBSCRIBERS}</a></dt>
            <dd>{LANG_EMKTG_TOOLS_DELETE_SUBSCRIBERS_DESC}</dd>
            <dt><a href="{LOCATION}deltools&amp;type=lists" onclick="return confirm('{LANG_EMKTG_TOOLS_DELETE_LISTS_CONFIRM}');">{LANG_EMKTG_TOOLS_DELETE_LISTS}</a></dt>
            <dd>{LANG_EMKTG_TOOLS_DELETE_LISTS_DESC}</dd>
            <dt><a href="{LOCATION}deltools&amp;type=all" onclick="return confirm('{LANG_EMKTG_TOOLS_DELETE_ALL_CONFIRM}');">{LANG_EMKTG_TOOLS_DELETE_ALL}</a></dt>
            <dd>{LANG_EMKTG_TOOLS_DELETE_ALL_DESC}</dd>
          </dl>
          <p class="message">{LANG_EMKTG_TOOLS_DELETE_NOTICE}</p>
        </td>
      </tr>
    </table>
</jos:tmpl>

<jos:tmpl name="installdefault">
<jos:link src="menuCSS" />
<jos:link src="adminMenu" />
<jos:link src="error" />
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminheading">
      <tr>
<jos:link src="header" />
      </tr>
    </table>
    <table cellpadding="4" cellspacing="1" border="0" class="adminform">
      <tr>
        <td>
          {INSTALL_DEFAULT_MSG}
        </td>
      </tr>
    </table>
</jos:tmpl>


<jos:tmpl name="clean">
<jos:link src="menuCSS" />
<jos:link src="adminMenu" />
<jos:link src="error" />
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminheading">
      <tr>
<jos:link src="header" />
      </tr>
    </table>
    <table cellpadding="4" cellspacing="1" border="0" class="adminform">
      <jos:tmpl type="condition" conditionvar="count" varscope="clean">
        <jos:sub condition="0">
          <tr>
            <td>
              {LANG_EMKTG_TOOLS_CLEAN_NO_TABLES}
            </td>
          </tr>
        </jos:sub>
        <jos:sub condition="__default">
          <tr>
            <th>{LANG_EMKTG_TOOLS_CLEAN_TABLES}</th>
          </tr>
          <jos:tmpl name="cleanrows">
            <tr>
              <td>
                {TABLE}
              </td>
            </tr>
          </jos:tmpl>
        </jos:sub>
      </jos:tmpl>
    </table>
</jos:tmpl>

<jos:tmpl name="del">
<jos:link src="menuCSS" />
<jos:link src="adminMenu" />
<jos:link src="error" />
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminheading">
      <tr>
<jos:link src="header" />
      </tr>
    </table>
    <table cellpadding="4" cellspacing="1" border="0" class="adminform" width="90%">
      <tr>
        <td>{MSGS}</td>
      </tr>
    </table>
</jos:tmpl>
    