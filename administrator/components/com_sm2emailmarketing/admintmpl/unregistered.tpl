<jos:comment>
@package Joomla!
@subpackage com_sm2emailmarketing
@copyright Copyright (C) 2006 SM2 / All Rights Reserved
@license commercial
@author SM2 <info@sm2joomla.com>
</jos:comment>

<jos:tmpl name="header">
      <th style="background: url(./components/{OPTION}/images/unRegisteredSubscribers.gif) no-repeat left; height: 70px; padding-left: 80px;" width="100%" align="left">
        {FORM_NAME}
      </th>
</jos:tmpl>

<jos:tmpl name="manager">
<jos:link src="menuCSS" />
<jos:link src="adminMenu" />
<jos:link src="error" />
<form action="index2.php" method="post" name="adminForm">
  <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminheading">
    <tr>
<jos:link src="header" />
      <td valign="bottom">
        <table border="0" width="100%">
          <tr>
            <td valign="bottom">{LANG_EMKTG_SEARCH_TITLE}</td>
            <td valign="bottom">
              <input type="text" name="search" value="{SEARCH}" class="inputbox" onChange="document.adminForm.submit();" />
            </td>
            <td valign="bottom">
              <select name="receive_html" id="filter_receive_html" class="inputbox" onChange="document.adminForm.submit();">
                <option value="-1">{LANG_EMKTG_FILTER_RECEIVE_HTML_TEXT}</option>
                <jos:tmpl name="filter_receive_html" type="simplecondition" requiredvars="value">
                  <option value="{VALUE}" {SELECTED} >{TEXT}</option>
                </jos:tmpl>
              </select>
            </td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td valign="bottom">
              <select name="confirmed" id="filter_confirmed" class="inputbox" onChange="document.adminForm.submit();">
                <option value="-1">{LANG_EMKTG_FILTER_CONFIRMED_TEXT}</option>
                <jos:tmpl name="filter_confirmed" type="simplecondition" requiredvars="value">
                  <option value="{VALUE}" {SELECTED} >{TEXT}</option>
                </jos:tmpl>
              </select>
            </td>
            <td valign="bottom">
              <select name="subscribed" id="filter_subscribed" class="inputbox" onChange="document.adminForm.submit();">
                <option value="-1">{LANG_EMKTG_FILTER_SUBSCRIBED_TEXT}</option>
                <jos:tmpl name="filter_subscribed" type="simplecondition" requiredvars="value">
                  <option value="{VALUE}" {SELECTED} >{TEXT}</option>
                </jos:tmpl>
              </select>
            </td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td valign="bottom" colspan="2">
              <select name="list" id="filter_list" class="inputbox" onChange="document.adminForm.submit();">
                <option value="-1">{LANG_EMKTG_FILTER_LIST_TEXT}</option>
                <jos:tmpl name="filter_list" type="simplecondition" requiredvars="value">
                  <option value="{VALUE}" {SELECTED} >{TEXT}</option>
                </jos:tmpl>
              </select>
            </td>
            <td valign="bottom"><input type="submit" value="Submit"></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
    <tr>
      <th width="20" align="center" nowrap><input type="checkbox" name="toggle" value="" onclick="checkAll({NUM_ROWS});" /></th>
      <th class="title" width="20%" nowrap>{LANG_CMN_NAME}</th>
      <th class="title" width="30%" nowrap>{LANG_CMN_EMAIL}</th>
      <th width="10%" align="left" nowrap>{LANG_EMKTG_RECEIVE_HTML}</th>
      <th width="10%" align="left" nowrap>{LANG_EMKTG_CONFIRMED}</th>
      <th width="15%" align="left" nowrap>{LANG_EMKTG_SUBSCRIBE_DATE}</th>
      <th width="15%" align="left" nowrap>{LANG_EMKTG_UNSUBSCRIBE_DATE}</th>
    </tr>
    <jos:tmpl name="rows" type="simplecondition" requiredvars="ROW_ID" varscope="manager">
    <tr class="row{ROW_K}">
      <td width="20" align="center">
        {ROW_CHECKEDDISPLAY}
      </td>
      <td width="20%">
        <a href="{ROW_LINK}" title="{LANG_EMKTG_SUBSCRIPTION_EDIT}"><jos:var name="ROW_NAME" modifier="htmlspecialchars" modifierType="php" /></a>
      </td>
      <td width="30%">{ROW_EMAIL}</td>
      <td width="10%" align="center">{ROW_RECEIVE_HTMLDISPLAY}</td>
      <td width="10%" align="center">{ROW_CONFIRMEDDISPLAY}</td>
      <td width="15%" align="left">{ROW_SUBSCRIBE_DATE}</td>
      <td width="15%" align="left">{ROW_UNSUBSCRIBE_DATE}</td>
    </tr>
    </jos:tmpl>
  </table>
  {LISTFOOTER}
  <input type="hidden" name="option" value="{OPTION}" />
  <input type="hidden" name="task" value="showunregistered" />
  <input type="hidden" name="boxchecked" value="0" />
  <input type="hidden" name="hidemainmenu" value="0" />
</form>
</jos:tmpl>

<jos:tmpl name="edit">
<jos:link src="menuCSS" />
<jos:link src="adminMenu" />
    <script language="javascript" type="text/javascript">
    function submitbutton(pressbutton) {
      submitform(pressbutton);
    }
    function confirmSubscribe(msg) {
      var form = document.adminForm;
      if (form.resubscribe.checked) {
        if (!confirm(msg)) {
          form.resubscribe.checked = false;
        }
      }
    }
    </script>
    <jos:link src="error" />
    <form action="index2.php" method="post" name="adminForm">
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminheading">
      <tr>
<jos:link src="header" />
      </tr>
    </table>
    <div class="quickTips">
    	<dl id="toggleDl">
    		<dt>{LANG_EMKTG_QUICKTIP}</dt>
    		<dd>{LANG_EMKTG_QUICKTIP_UNREGISTERED}</dd>
    	</dl>
    </div>
    <table cellpadding="4" cellspacing="1" border="0" class="adminform">
      <tr>
        <td width="100px";><jos:emktgtooltip>_CMN_NAME</jos:emktgtooltip></td>
        <td>
          <input class="inputbox" type="text" name="name" tabindex="1" value="{ROW_NAME}" size="50" maxlength="50" />
        </td>
      </tr>
      <tr>
        <td valign="top"><jos:emktgtooltip>_CMN_EMAIL</jos:emktgtooltip></td>
        <td valign="top" valign="top">
          <input class="inputbox" type="text" name="email" tabindex="2" value="{ROW_EMAIL}" size="50" maxlength="254" />
        </td>
      </tr>
      <tr>
        <td valign="top"><jos:emktgtooltip>_EMKTG_RECEIVE_HTML</jos:emktgtooltip></td>
        <td valign="top">
          {LIST_RECEIVE_HTML}
        </td>
      </tr>
      <tr>
        <td valign="top"><jos:emktgtooltip>_EMKTG_CONFIRMED</jos:emktgtooltip></td>
        <td valign="top">
          {LIST_CONFIRMED}
        </td>
      </tr>
      <tr>
        <td valign="top">{LANG_EMKTG_SUBSCRIBE_DATE}</td>
        <td valign="top">
          {ROW_SUBSCRIBE_DATE}
        </td>
      </tr>
      <tr>
        <td valign="top">{LANG_EMKTG_UNSUBSCRIBE_STATUS}</td>
        <td valign="top">
          {ROW_STATUS}
          <jos:tmpl type="simplecondition" varscope="edit" requiredvars="row_resubscribe">
          <br />
          {LANG_EMKTG_MODULE_SUBSCRIBE}
          <input type="checkbox" name="resubscribe" value="1" <jos:tmpl type="simplecondition" varscope="edit" requiredvars="row_resubscribe_msg">onchange="confirmSubscribe('{ROW_RESUBSCRIBE_MSG}');"</jos:tmpl> />
          </jos:tmpl>
        </td>
      </tr>
      <tr>
        <td valign="top"><jos:emktgtooltip>_EMKTG_SUBSCRIPTION</jos:emktgtooltip></td>
        <td valign="top">
          {LIST_SUBSCRIPTION}
        </td>
      </tr>
    </table>
    <input type="hidden" name="option" value="{OPTION}" />
    <input type="hidden" name="subscriberid" value="{ROW_SUBSCRIBERID}" />
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="subscribe_date" value="{ROW_SUBSCRIBE_DATE}" />
    </form>
</jos:tmpl>




<jos:tmpl name="uploadform">
<jos:link src="menuCSS" />
<jos:link src="adminMenu" />
    <script language="javascript" type="text/javascript">
    function submitbutton(pressbutton) {
      var form = document.adminForm;

      if (pressbutton=="showunregistered") {
        submitform(pressbutton);
        return;
      }

      // do field validation
      var msg = "";

      if (form.upload.value=='') {
        msg += "{LANG_EMKTG_UNREGISTERED_IMPORT_UPLOAD_DESC}\n";
      }

      // display validation error or submit
      if (msg!="") {
        alert("{LANG_EMKTG_VALIDATION_ERROR}\n"+msg );
      } else {
        submitform( pressbutton );
      }
    }
    </script>
    <jos:link src="error" />
    <form action="index2.php" method="post" name="adminForm" enctype="multipart/form-data">
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminheading">
      <tr>
<jos:link src="header" />
      </tr>
    </table>
    <div class="quickTips">
    	<dl id="toggleDl">
    		<dt>{LANG_EMKTG_QUICKTIP}</dt>
    		<dd>{LANG_EMKTG_QUICKTIP_IMPORT_1}</dd>
    	</dl>
    </div>
    <table cellpadding="4" cellspacing="1" border="0" class="adminform">
      <tr>
        <td width="100px"><jos:emktgtooltip>_EMKTG_UNREGISTERED_IMPORT_UPLOAD</jos:emktgtooltip></td>
        <td>
          <input class="inputbox" type="file" name="upload" tabindex="1" />
        </td>
      </tr>
    </table>
    <input type="hidden" name="option" value="{OPTION}" />
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="step" value="2" />
    </form>
</jos:tmpl>


<jos:tmpl name="importform">
<jos:link src="menuCSS" />
<jos:link src="adminMenu" />
    <script language="javascript" type="text/javascript">
    function submitbutton(pressbutton) {
      var form = document.adminForm;

      if (pressbutton=="showunregistered") {
        submitform(pressbutton);
        return;
      }

      // do field validation
      var msg = "";

      // display validation error or submit
      if (msg!="") {
        alert("{LANG_EMKTG_VALIDATION_ERROR}\n"+msg );
      } else {
        submitform( pressbutton );
      }
    }
    </script>
    <jos:link src="error" />
    <form action="index2.php" method="post" name="adminForm" enctype="multipart/form-data">
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminheading">
      <tr>
<jos:link src="header" />
      </tr>
    </table>
    <div class="quickTips">
    	<dl id="toggleDl">
    		<dt>{LANG_EMKTG_QUICKTIP}</dt>
    		<dd>{LANG_EMKTG_QUICKTIP_IMPORT_2}</dd>
    	</dl>
    </div>
    <table cellpadding="4" cellspacing="1" border="0" class="adminform">
      <tr>
        <td width="100px"><jos:emktgtooltip>_EMKTG_UNREGISTERED_IMPORT_NAME</jos:emktgtooltip></td>
        <td>
          <select name="import[name]" class="inputbox">
            <jos:link src="fieldlist" />
          </select>
        </td>
      </tr>
      <tr>
        <td width="100px"><jos:emktgtooltip>_EMKTG_UNREGISTERED_IMPORT_EMAIL</jos:emktgtooltip></td>
        <td>
          <select name="import[email]" class="inputbox">
            <jos:link src="fieldlist" />
          </select>
        </td>
      </tr>
      <tr>
        <td width="100px" rowspan="2" valign="top"><jos:emktgtooltip>_EMKTG_UNREGISTERED_IMPORT_RECEIVE_HTML</jos:emktgtooltip></td>
        <td valign="top">
          Default:
          <select name="import[default_receive_html]" class="inputbox">
            <option value="1">{LANG_EMKTG_RECEIVE_HTML}</option>
            <option value="0">{LANG_EMKTG_RECEIVE_TEXT}</option>
          </select>
        </td>
      </tr>
      <tr>
        <td>
          <select name="import[receive_html]" class="inputbox">
            <option value="">[ Select optional Field ]</option>
            <jos:link src="fieldlist" />
          </select>
        </td>
      </tr>
      <tr>
        <td width="100px" rowspan="2" valign="top"><jos:emktgtooltip>_EMKTG_UNREGISTERED_IMPORT_LIST</jos:emktgtooltip></td>
        <td valign="top">
          Default:
          <select name="import[default_lists][]" class="inputbox" multiple>
            <jos:tmpl name="lists" type="simplecondition" requiredvars="value">
              <option value="{VALUE}">{TEXT}</option>
            </jos:tmpl>
          </select>
        </td>
      </tr>
      <tr>
        <td>
          <select name="import[lists][]" class="inputbox" multiple>
            <option value="">[ Select optional Fields ]</option>
            <jos:link src="fieldlist" />
          </select>
        </td>
      </tr>
    </table>
    <input type="hidden" name="option" value="{OPTION}" />
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="step" value="3" />
    <input type="hidden" name="file" value="{FILE}" />
    </form>
</jos:tmpl>

<jos:tmpl name="fieldlist">
            <option value="{VALUE}">{TEXT}</option>
</jos:tmpl>

<jos:tmpl name="resultsform">
<jos:link src="menuCSS" />
<jos:link src="adminMenu" />
    <script language="javascript" type="text/javascript">
    function submitbutton(pressbutton) {
      var form = document.adminForm;

      if (pressbutton=="showunregistered") {
        submitform(pressbutton);
        return;
      }

      // do field validation
      var msg = "";

      // display validation error or submit
      if (msg!="") {
        alert("{LANG_EMKTG_VALIDATION_ERROR}\n"+msg );
      } else {
        submitform( pressbutton );
      }
    }
    </script>
    <jos:link src="error" />
    <form action="index2.php" method="post" name="adminForm" enctype="multipart/form-data">
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminheading">
      <tr>
<jos:link src="header" />
      </tr>
    </table>
    <table cellpadding="4" cellspacing="1" border="0" class="adminform">
      <tr>
        <th colspan="2">{LANG_EMKTG_UNREGISTERED_IMPORT_SUMMARY}</th>
      </tr>
      <tr>
        <td width="100px">{LANG_EMKTG_UNREGISTERED_IMPORT_RECORDS}</td>
        <td>{NUM_RECORDS}</td>
      </tr>
      <tr>
        <td width="100px">{LANG_EMKTG_UNREGISTERED_IMPORT_ERRORS}</td>
        <td>{NUM_ERRORS}</td>
      </tr>
    </table>
    <jos:tmpl type="condition" conditionvar="NUM_ERRORS" varscope="resultsform">
      <jos:sub condition="0"></jos:sub>
      <jos:sub condition="__default">
        {TRUNCERRORS}
        <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
          <tr>
            <th width="2%" class="title">#</td>
            <th width="100%" class="title">{LANG_EMKTG_PLUGIN_DETAILS}</th>
          </tr>
          <jos:tmpl name="errors" type="simplecondition" requiredvars="index">
            <tr>
              <td>{INDEX}</td>
              <td>{DETAILS}</td>
            </tr>
          </jos:tmpl>
        </table>
      </jos:sub>
    </jos:tmpl>
    <input type="hidden" name="option" value="{OPTION}" />
    <input type="hidden" name="task" value="" />
    </form>
</jos:tmpl>