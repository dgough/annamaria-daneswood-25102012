<jos:comment>
@package Joomla!
@subpackage com_sm2emailmarketing
@copyright Copyright (C) 2006 SM2 / All Rights Reserved
@license commercial
@author SM2 <info@sm2joomla.com>
</jos:comment>

<jos:tmpl name="header">
      <th style="background: url(./components/{OPTION}/images/queueManager.gif) no-repeat left; height: 70px; padding-left: 80px;" width="100%" align="left">
        {FORM_NAME}
      </th>
</jos:tmpl>

<jos:tmpl name="manager">
<jos:link src="menuCSS" />
<jos:link src="adminMenu" />
<jos:link src="error" />
<form action="index2.php" method="post" name="adminForm">
  <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminheading">
    <tr>
<jos:link src="header" />
    </tr>
    <jos:tmpl type="simplecondition" requiredvars="SM2EM_LITE" varscope="header,manager">
    <tr>
      <td>{LANG_EMKTG_LICENCE_LITE_MESSAGE}{LANG_EMKTG_LICENCE_SM2_PLUG}</td>
    </tr>
    </jos:tmpl>
  </table>
  <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
    <tr>
      <th width="20" align="center" nowrap><input type="checkbox" name="toggle" value="" onclick="checkAll({NUM_ROWS});" /></th>
      <th class="title" width="90%" nowrap>{LANG_EMKTG_MESSAGE_SUBJECT}</th>
      <th width="10%" align="left" nowrap>{LANG_EMKTG_QUEUE_LEFT}</th>
    </tr>
    <jos:tmpl name="rows" type="simplecondition" requiredvars="ROW_ID" varscope="manager">
    <tr class="row{ROW_K}">
      <td width="20" align="center">
        {ROW_CHECKEDDISPLAY}
      </td>
      <td width="90%">
        <a href="{ROW_LINK}" title="{LANG_EMKTG_QUEUE_PROCESS} {LANG_EMKTG_MESSAGE}"><jos:var name="ROW_SUBJECT" modifier="htmlspecialchars" modifierType="php" /></a>
      </td>
      <td width="10%" align="right">{ROW_NUMLEFT}</td>
    </tr>
    </jos:tmpl>
  </table>
  {LISTFOOTER}
  <input type="hidden" name="option" value="{OPTION}" />
  <input type="hidden" name="task" value="showqueue" />
  <input type="hidden" name="boxchecked" value="0" />
  <input type="hidden" name="hidemainmenu" value="0" />
</form>
</jos:tmpl>

<jos:tmpl name="edit">
<jos:link src="menuCSS" />
<jos:link src="adminMenu" />
    <script language="javascript" type="text/javascript">
    function submitbutton(pressbutton) {
      var form = document.adminForm;

      submitform(pressbutton);
    }

    function timerSubmit() {
        submitbutton('processqueue');
    }
    </script>
    <jos:link src="error" />
    <form action="index2.php" method="post" name="adminForm" id="adminform">
      <input type="hidden" name="option" value="{OPTION}" />
      <input type="hidden" name="cid[]" value="{MSG_MESSAGEID}" />
      <input type="hidden" name="starttime" value="{STARTTIME}" />
      <input type="hidden" name="task" value="processqueue" />
    </form>
    <table class="adminheading">
      <tr>
<jos:link src="header" />
      </tr>
      <jos:tmpl type="simplecondition" requiredvars="SM2EM_LITE" varscope="header,edit">
      <tr>
        <td>{LANG_EMKTG_LICENCE_LITE_MESSAGE}{LANG_EMKTG_LICENCE_SM2_PLUG}</td>
      </tr>
      </jos:tmpl>
    </table>
    <table cellpadding="4" cellspacing="1" border="0" class="adminform">
      <tr>
        <td width="10%">{LANG_EMKTG_MESSAGE_SUBJECT}</td>
        <td width="90%">
          {MSG_SUBJECT}
        </td>
      </tr>
      <tr>
        <td>{LANG_EMKTG_TEMPLATE}</td>
        <td>
          {TPL_TEMPLATE_NAME}
        </td>
      </tr>
      <tr>
        <td>{LANG_EMKTG_QUEUE_SIZE}</td>
        <td>{STATS_QUEUESIZE}</td>
      </tr>
    </table>
    {MSG_PAUSEMSG}
    {MSG_CONTMSG}
    <br/><br/>
    {MSG_REDIRECTSCRIPT}
</jos:tmpl>