<jos:comment>
@package Joomla!
@subpackage com_sm2emailmarketing
@copyright Copyright (C) 2006 SM2 / All Rights Reserved
@license commercial
@author SM2 <info@sm2joomla.com>
</jos:comment>

<jos:tmpl name="header">
      <th style="background: url(./components/{OPTION}/images/tools.gif) no-repeat left; height: 70px; padding-left: 80px;" width="100%" align="left">
        {FORM_NAME}
      </th>
</jos:tmpl>

<jos:tmpl name="manager">
<jos:link src="menuCSS" />
<jos:link src="adminMenu" />
<jos:link src="error" />
<script language="JavaScript" type="text/javascript">
function saveorderplugin(n) {
    for ( var j = 0; j <= n; j++ ) {
		box = eval( "document.adminForm.cb" + j );
		if ( box ) {
			if ( box.checked == false ) {
				box.checked = true;
			}
		} else {
			alert("You cannot change the order of items, as an item in the list is `Checked Out`");
			return;
		}
	}
	submitform('saveorderplugin');
}
</script>
<form action="index2.php" method="post" name="adminForm">
  <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminheading">
    <tr>
<jos:link src="header" />
    </tr>
    <jos:tmpl type="simplecondition" requiredvars="SM2EM_LITE" varscope="header,manager">
    <tr>
      <td>{LANG_EMKTG_LICENCE_LITE_PLUGIN}{LANG_EMKTG_LICENCE_SM2_PLUG}</td>
    </tr>
    </jos:tmpl>
  </table>
  <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
    <tr>
      <th width="20" align="center" nowrap><input type="checkbox" name="toggle" value="" onclick="checkAll({NUM_ROWS});" /></th>
      <th class="title" width="20%" nowrap>{LANG_EMKTG_PLUGIN_NAME}</th>
      <th width="67%" align="left" nowrap>{LANG_EMKTG_PLUGIN_DESCRIPTION}</th>
      <th colspan="2" width="5%">{LANG_EMKTG_PLUGIN_REORDER}</th>
      <th width="2%">{LANG_EMKTG_PLUGIN_ORDER}</th>
      <th width="1%">
        <a href="javascript: saveorderplugin({NUM_ROWS}-1)">
          <img src="images/filesave.png" border="0" width="16" height="16" alt="{LANG_EMKTG_PLUGIN_SAVE_ORDER}" title="{LANG_EMKTG_PLUGIN_SAVE_ORDER}" />
        </a>
      </th>
      <th width="5%">{LANG_EMKTG_ENABLED}</th>
    </tr>
    <jos:tmpl name="rows" type="simplecondition" requiredvars="ROW_ID" varscope="manager">
    <tr class="row{ROW_K}">
      <td width="20" align="center">
        {ROW_CHECKEDDISPLAY}
      </td>
      <td width="20%" valign="top">
        {ROW_LINK}
      </td>
      <td width="72%" valign="top">
        {ROW_DESCRIPTION}
      </td>
      <td>{ROW_ORDERUPICON}</td>
      <td>{ROW_ORDERDOWNICON}</td>
      <td align="center" colspan="2">
        <input type="text" name="order[]" size="5" value="{ROW_ORDERING}" class="text_area" style="text-align: center" />
      </td>
      <td align="center">{ROW_ENABLEDDISPLAY}</td>
    </tr>
    </jos:tmpl>
  </table>
  {LISTFOOTER}
  <input type="hidden" name="option" value="{OPTION}" />
  <input type="hidden" name="task" value="showplugins" />
  <input type="hidden" name="boxchecked" value="0" />
  <input type="hidden" name="hidemainmenu" value="0" />
</form>
</jos:tmpl>

<jos:tmpl name="edit">
<jos:link src="menuCSS" />
<jos:link src="adminMenu" />
    <script language="javascript" type="text/javascript">
    function submitbutton(pressbutton) {
      var form = document.adminForm;

      // if cancel then dont validate
      if (pressbutton == 'showplugins') {
        submitform(pressbutton);
        return;
      }

      // do field validation
      var msg = "";

      // display validation error or submit
      if (msg!="") {
        alert( "{LANG_EMKTG_VALIDATION_ERROR}\n"+msg );
      } else {
        submitform( pressbutton );
      }
    }
    </script>
    <jos:link src="error" />
    <form action="index2.php" method="post" name="adminForm" id="adminform">
    <table class="adminheading">
      <tr>
<jos:link src="header" />
      </tr>
      <jos:tmpl type="simplecondition" requiredvars="SM2EM_LITE" varscope="header,edit">
    <tr>
      <td>{LANG_EMKTG_LICENCE_LITE_PLUGIN}{LANG_EMKTG_LICENCE_SM2_PLUG}</td>
    </tr>
    </jos:tmpl>
    </table>
    <table width="100%">
      <tr>
        <td width="60%" valign="top">
          <table class="adminform">
            <tr>
              <th colspan="2">{LANG_EMKTG_PLUGIN_DETAILS}</th>
            </tr>
            <tr>
              <td width="20%" align="right">{LANG_EMKTG_PLUGIN_NAME}</td>
              <td width="80%">
                {ROW_NAME}
                <input type="hidden" name="name" value="{ROW_NAME}" />
              </td>
            </tr>
            <tr>
              <td valign="top">{LANG_EMKTG_PLUGIN_DESCRIPTION}</td>
              <td valign="top" valign="top">
                {ROW_DESCRIPTION}
                <input type="hidden" name="description" value="{ROW_DESCRIPTION}" />
              </td>
            </tr>
            <tr>
              <td valign="top"><jos:emktgtooltip>_EMKTG_ENABLED</jos:emktgtooltip></td>
              <td valign="top" valign="top">
                {LIST_ENABLED}
              </td>
            </tr>
          </table>
        </td>
        <td width="40%" valign="top">
          <table class="adminform">
            <tr>
              <th>{LANG_EMKTG_PLUGIN_PARAMETER}</th>
            </tr>
            <tr>
              <td>
                {PARAMS}
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <input type="hidden" name="option" value="{OPTION}" />
    <input type="hidden" name="pluginid" value="{ROW_PLUGINID}" />
    <input type="hidden" name="filename" value="{ROW_FILENAME}" />
    <input type="hidden" name="task" value="" />
    </form>
</jos:tmpl>