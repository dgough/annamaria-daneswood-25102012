<jos:comment>
@package Joomla!
@subpackage com_sm2emailmarketing
@copyright Copyright (C) 2006 SM2 / All Rights Reserved
@license commercial
@author SM2 <info@sm2joomla.com>
</jos:comment>

<jos:tmpl name="sm2includeTabs" useglobals="yes">
	<link id="luna-tab-style-sheet" type="text/css" rel="stylesheet" href="{SITEURL}/includes/js/tabs/tabpane.css" />
	<script type="text/javascript" src="{SITEURL}/includes/js/tabs/tabpane.js"></script>
	<style type="text/css">
	.dynamic-tab-pane-control .tab-row .tab {
		background-image: url({TABURL}tab.png);
	}
	.dynamic-tab-pane-control .tab-row .tab.selected {
		background-image:	url({TABURL}tab_active.png) !important;
	}
	.dynamic-tab-pane-control .tab-row .tab.hover {
		background-image:	url({TABURL}tab_hover.png);
	</style>
</jos:tmpl>

<jos:tmpl name="sm2startTabPane" varscope="body">
	<div class="tab-page" id="{PANEID}">
	<script language="javascript" type="text/javascript">
	<!--
	var tabPane1 = new WebFXTabPane( document.getElementById( '{PANEID}' ), {PANECOOKIES} );
	//-->
	</script>
</jos:tmpl>

<jos:tmpl name="sm2startTab" varscope="body">
	<div class="tab-page" id="{PANEID}">
	<h2 class="tab">
			<jos:var name="tabtitle" modifier="translate" />
	</h2>
		<script language="javascript" type="text/javascript">
		<!--
	tabPane1.addTabPage( document.getElementById( '{PANEID}' ) );
		//-->
	</script>
</jos:tmpl>

<jos:tmpl name="sm2endTab">
	</div>
</jos:tmpl>

<jos:tmpl name="sm2endTabPane">
	</div>
</jos:tmpl>

<jos:tmpl name="error">
	<jos:tmpl name="message" type="condition" conditionvar="ERROR">
		<jos:sub condition="__empty" />
    	<jos:sub condition="__default">
			<div class="error">{ERROR}</div>
		</jos:sub>
	</jos:tmpl>
</jos:tmpl>

<jos:tmpl name="includeCalendar" useglobals="yes">
	<link rel="stylesheet" type="text/css" media="all" href="{SITEURL}/includes/js/calendar/calendar-mos.css" title="green" />
	<script type="text/javascript" src="{SITEURL}/includes/js/calendar/calendar.js"></script>
	<script type="text/javascript" src="{SITEURL}/includes/js/calendar/lang/calendar-en.js"></script>
</jos:tmpl>

<jos:tmpl name="menuCSS" useglobals="yes">
<link href="{SITEURL}/administrator/components/{OPTION}/styles/sm2emailmarketing.css" rel="stylesheet" type="text/css" media="screen" />
<!--[if IE 7]>
<link href="{SITEURL}/administrator/components/{OPTION}/styles/sm2emailmarketing_ie7.css" rel="stylesheet" type="text/css" media="screen" />
<![endif]-->
<script type="text/javascript" src="{SITEURL}/administrator/components/{OPTION}/includes/toggleDl.js"></script>
</jos:tmpl>

<jos:tmpl name="adminMenu" useglobals="yes">
<div id="adminMenu">
<ul>
	<li class="messagemanager<jos:tmpl useglobals="yes" type="simplecondition" requiredvars="section=message"> active</jos:tmpl>"><a href="{LOCATION}showmessages">Messages</a></li>
	<li class="stats<jos:tmpl useglobals="yes" type="simplecondition" requiredvars="section=statistic"> active</jos:tmpl>"><a href="{LOCATION}showstatistics">Statistics</a></li>
	<li class="registered<jos:tmpl useglobals="yes" type="simplecondition" requiredvars="section=registered"> active</jos:tmpl>"><a href="{LOCATION}showregistered">Client Mgmt Subscribers</a></li>
	<jos:tmpl varscope="adminMenu" type="simplecondition" requiredvars="showlist">
	<li class="unregistered<jos:tmpl useglobals="yes" type="simplecondition" requiredvars="section=unregistered"> active</jos:tmpl>"><a href="{LOCATION}showunregistered">List Only Subscribers</a></li>
	<li class="list<jos:tmpl useglobals="yes" type="simplecondition" requiredvars="section=list"> active</jos:tmpl>"><a href="{LOCATION}showlists">Lists</a></li>
	</jos:tmpl>
	<li class="template<jos:tmpl useglobals="yes" type="simplecondition" requiredvars="section=template"> active</jos:tmpl>"><a href="{LOCATION}showtemplates">Templates</a></li>
	<li class="queue<jos:tmpl useglobals="yes" type="simplecondition" requiredvars="section=queue"> active</jos:tmpl>"><a href="{LOCATION}showqueue">Queues</a></li>
	<jos:tmpl varscope="adminMenu" type="simplecondition" requiredvars="showbounce">
	<li class="bounce<jos:tmpl useglobals="yes" type="simplecondition" requiredvars="section=bounce"> active</jos:tmpl>"><a href="{LOCATION}showbounce">Bounces</a></li>
	</jos:tmpl>
	<li class="plugin<jos:tmpl useglobals="yes" type="simplecondition" requiredvars="section=plugin"> active</jos:tmpl>"><a href="{LOCATION}showplugins">Plugins</a></li>
	<li class="tool<jos:tmpl useglobals="yes" type="simplecondition" requiredvars="section=tool"> active</jos:tmpl>"><a href="{LOCATION}showtools">Tools</a></li>
	<li class="config<jos:tmpl useglobals="yes" type="simplecondition" requiredvars="section=configuration"> active</jos:tmpl>"><a href="{LOCATION}showconfiguration">Configuration</a></li>
	<li class="help<jos:tmpl useglobals="yes" type="simplecondition" requiredvars="section="> active</jos:tmpl>"><a href="{LOCATION}showhelp">Help</a></li>
</ul>
<br class="clearing" />
</div>
</jos:tmpl>