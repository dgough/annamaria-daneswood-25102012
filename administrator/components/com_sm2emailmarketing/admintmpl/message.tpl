<jos:comment>
@package Joomla!
@subpackage com_sm2emailmarketing
@copyright Copyright (C) 2006 SM2 / All Rights Reserved
@license commercial
@author SM2 <info@sm2joomla.com>
</jos:comment>

<jos:tmpl name="header">
      <th style="background: url(./components/{OPTION}/images/messageManager.gif) no-repeat left; height: 70px; padding-left: 80px;" width="100%" align="left">
        {FORM_NAME}
      </th>
</jos:tmpl>

<jos:tmpl name="manager">
<jos:link src="menuCSS" />
<jos:link src="adminMenu" />
<jos:link src="error" />
<form action="index2.php" method="post" name="adminForm">
  <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminheading">
    <tr>
<jos:link src="header" />
      <td valign="bottom">{LANG_EMKTG_SEARCH_TITLE}</td>
      <td valign="bottom"><input type="text" name="search" value="{SEARCH}" class="inputbox" onChange="document.adminForm.submit();" /></td>
      <td valign="bottom"><input type="submit" value="Submit"></td>
    </tr>
    <jos:tmpl type="simplecondition" requiredvars="SM2EM_LITE" varscope="header,manager">
    <tr>
      <td colspan="4">{LANG_EMKTG_LICENCE_LITE_MESSAGE}{LANG_EMKTG_LICENCE_SM2_PLUG}</td>
    </tr>
    </jos:tmpl>
  </table>
  <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
    <tr>
      <th width="20" align="center" nowrap><input type="checkbox" name="toggle" value="" onclick="checkAll({NUM_ROWS});" /></th>
      <th class="title" width="60%" nowrap>{LANG_EMKTG_MESSAGE_SUBJECT}</th>
      <th width="25%" align="left" nowrap>{LANG_EMKTG_TEMPLATE}</th>
      <th width="15%" align="left" nowrap>{LANG_EMKTG_MESSAGE_SENDDATE}</th>
    </tr>
    <jos:tmpl name="rows" type="simplecondition" requiredvars="ROW_ID" varscope="manager">
    <tr class="row{K}">
      <td width="20" align="center">
        {ROW_CHECKEDDISPLAY}
      </td>
      <td width="60%">
        <a href="{ROW_LINK}" title="{LANG_EMKTG_MESSAGE_EDIT}"><jos:var name="ROW_SUBJECT" modifier="htmlspecialchars" modifierType="php" /></a>
      </td>
      <td width="25%"><jos:var name="ROW_TEMPLATE_NAME" modifier="htmlspecialchars" modifierType="php" /></td>
      <td width="15%">{ROW_SEND_DATEDISPLAY}</td>
    </tr>
    </jos:tmpl>
  </table>
  {LISTFOOTER}
  <input type="hidden" name="option" value="{OPTION}" />
  <input type="hidden" name="task" value="showmessages" />
  <input type="hidden" name="boxchecked" value="0" />
  <input type="hidden" name="hidemainmenu" value="0" />
</form>
</jos:tmpl>

<jos:tmpl name="edit">
<jos:link src="menuCSS" />
<jos:link src="adminMenu" />
    <script language="javascript" type="text/javascript">
    function submitbutton(pressbutton) {
      var form = document.adminForm;
      var action = "";

      switch (pressbutton) {
        case 'showmessages':
            submitform(pressbutton);
            return;
            break;
        case 'viewstatistics':
            if (form.send_date.value=="") {
                alert("{LANG_EMKTG_MESSAGE_NO_STATS}");
            } else {
                submitform(pressbutton);
            }
            return;
            break;
        case 'previewmessage':
            action = "Preview";
            break;
        case 'sendmessage':
            if (form.send_date.value!="") {
                if (action=="") {
                    action = "Send";
                    var msg = "{LANG_EMKTG_MESSAGE_SEND_ERROR}";
                }
                if (confirm(msg)) {
                    var url = "index2.php?option="+form.option.value
                        +"&task=copymessage&hidemainmenu="
                        +form.hidemainmenu.value+"&cid[]="+form.messageid.value;
                    document.location.href=url;
                }
                return;
            }
            break;
      }

      // save the content selected
      var temp = new Array;
      for (var i=0, n=form.selectedContent.options.length; i < n; i++) {
      	temp[i] = form.selectedContent.options[i].value;
      }
      form.content_items.value = temp.join(',');

      // do field validation
      var msg = "";
      // validate subject
      if (form.subject.value=='') {
        msg += "{LANG_EMKTG_MESSAGE_SUBJECT_DESC}\n";
      }
      // from
      if (form.fromname.value=='') {
        msg += "{LANG_EMKTG_MESSAGE_FROMNAME_DESC}\n";
      }
      if (form.fromemail.value=='') {
        msg += "{LANG_EMKTG_MESSAGE_FROMEMAIL_DESC}\n";
      }
      if (form.bounceemail.value=='') {
        msg += "{LANG_EMKTG_MESSAGE_BOUNCEEMAIL_DESC}\n";
      }
      if (form.templateid.selectedIndex==0) {
        msg += "{LANG_EMKTG_TEMPLATE_DESC}\n";
      }
      {ROW_MESSAGE_HTMLVALIDATION}
      {PLUGINEXTRAVALIDATION}
      {PLUGINVALIDATION}
      if (form.message_html.value=='' && form.message_text.value=='') {
        msg += "{LANG_EMKTG_MESSAGE_NONE_DESC}\n";
      }

      // display validation error or submit
      if (msg!="") {
        alert("{LANG_EMKTG_VALIDATION_ERROR}\n"+msg );
      } else {
        submitform( pressbutton );
      }
    }
    </script>
    <jos:link src="error" />
<jos:link src="includeCalendar" />
    <form action="index2.php" method="post" name="adminForm">
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminheading">
      <tr>
<jos:link src="header" />
      </tr>
      <jos:tmpl type="simplecondition" requiredvars="SM2EM_LITE" varscope="header,edit">
      <tr>
        <td>{LANG_EMKTG_LICENCE_LITE_PLUGIN}<br />{LANG_EMKTG_LICENCE_LITE_MESSAGE}{LANG_EMKTG_LICENCE_SM2_PLUG}</td>
      </tr>
      </jos:tmpl>
    </table>
    <div class="quickTips">
    	<dl id="toggleDl">
    		<dt>{LANG_EMKTG_QUICKTIP}</dt>
    		<dd>{LANG_EMKTG_QUICKTIP_MESSAGE}</dd>
    	</dl>
    </div>
    <table cellpadding="4" cellspacing="1" border="0" class="adminform">
      <tr>
        <th colspan="2"><img src="../components/{OPTION}/images/icon-number-1.gif"> {LANG_EMKTG_MESSAGE_CONTENT}</th>
      </tr>
      <tr>
        <td><jos:emktgtooltip>_EMKTG_MESSAGE_SUBJECT</jos:emktgtooltip>&nbsp;{LANG_EMKTG_REQUIRED}</td>
        <td>
          <input class="inputbox" type="text" name="subject" tabindex="1" value="{ROW_SUBJECT}" size="50" maxlength="254" />
        </td>
      </tr>
      <tr>
        <td><jos:emktgtooltip>_EMKTG_MESSAGE_FROMNAME</jos:emktgtooltip>&nbsp;{LANG_EMKTG_REQUIRED}</td>
        <td>
          <input class="inputbox" type="text" name="fromname" tabindex="2" value="{ROW_FROMNAME}" size="50" maxlength="50" />
        </td>
      </tr>
      <tr>
        <td><jos:emktgtooltip>_EMKTG_MESSAGE_FROMEMAIL</jos:emktgtooltip>&nbsp;{LANG_EMKTG_REQUIRED}</td>
        <td>
          <input class="inputbox" type="text" name="fromemail" tabindex="3" value="{ROW_FROMEMAIL}" size="50" maxlength="254" />
        </td>
      </tr>
      <tr>
        <td><jos:emktgtooltip>_EMKTG_MESSAGE_BOUNCEEMAIL</jos:emktgtooltip>&nbsp;{LANG_EMKTG_REQUIRED}</td>
        <td>
          <input class="inputbox" type="text" name="bounceemail" tabindex="4" value="{ROW_BOUNCEEMAIL}" size="50" maxlength="254" />
        </td>
      </tr>
      <tr>
        <td><jos:emktgtooltip>_EMKTG_TEMPLATE</jos:emktgtooltip>&nbsp;{LANG_EMKTG_REQUIRED}</td>
        <td>
          {LIST_TEMPLATEID}
        </td>
      </tr>
      <tr>
        <td valign="top"><jos:emktgtooltip>_EMKTG_MESSAGE_HTML</jos:emktgtooltip></td>
        <td valign="top" valign="top">
          {ROW_MESSAGE_HTMLDISPLAY}
        </td>
      </tr>
      <tr>
        <td valign="top"><jos:emktgtooltip>_EMKTG_MESSAGE_TEXT</jos:emktgtooltip>&nbsp;{LANG_EMKTG_REQUIRED}</td>
        <td valign="top">
          <textarea class="inputbox" name="message_text" tabindex="7" cols="70" rows="20">{ROW_MESSAGE_TEXT}</textarea>
        </td>
      </tr>
      <tr>
        <td colspan="2">&nbsp;</td>
      </tr>
      <tr>
        <th colspan="2"><img src="../components/{OPTION}/images/icon-number-2.gif"> {LANG_EMKTG_MESSAGE_EXTRAS}</th>
      </tr>
      <tr>
        <td valign="top"><jos:emktgtooltip>_EMKTG_MESSAGE_ATTACHMENTS</jos:emktgtooltip></td>
        <td valign="top">
          {LIST_ATTACHMENTS}
        </td>
      </tr>
      <tr>
        <td valign="top"><jos:emktgtooltip>_EMKTG_MESSAGE_CONTENT_ITEMS</jos:emktgtooltip></td>
        <td valign="top">
          <table cellpadding="4" cellspacing="1" border="0" class="adminform">
            <tr>
              <th width="50%"><jos:emktgtooltip>_EMKTG_MESSAGE_AVAILABLE_CONTENT</jos:emktgtooltip></th>
              <th width="50%"><jos:emktgtooltip>_EMKTG_MESSAGE_SELECTED_CONTENT</jos:emktgtooltip></th>
            </tr>
            <tr>
              <td><input type="button" class="button" name="content_add" value="{LANG_EMKTG_MESSAGE_CONTENT_ADD}" onclick="addSelectedToList('adminForm', 'availContent', 'selectedContent');" /></td>
              <td>
                <input type="button" class="button" name="content_remove" value="{LANG_EMKTG_MESSAGE_CONTENT_REMOVE}" onclick="delSelectedFromList('adminForm', 'selectedContent');" />
                &nbsp;
                <input type="button" class="button" name="content_up" value="{LANG_EMKTG_MESSAGE_CONTENT_UP}" onclick="moveInList('adminForm', 'selectedContent', document.getElementById('selectedContent').selectedIndex, -1);" />
                &nbsp;
                <input type="button" class="button" name="content_down" value="{LANG_EMKTG_MESSAGE_CONTENT_DOWN}" onclick="moveInList('adminForm', 'selectedContent', document.getElementById('selectedContent').selectedIndex, +1);" />
              </td>
            <tr>
              <td>
                {LIST_AVAILCONTENT}
              </td>
              <td>
                {LIST_SELECTEDCONTENT}
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td valign="top"><jos:emktgtooltip>_EMKTG_MESSAGE_INTRO_ONLY</jos:emktgtooltip></td>
        <td valign="top">
          <input type="checkbox" name="intro_only" value="1" {ROW_INTRO_ONLY} />
        </td>
      </tr>
      <tr>
        <td colspan="2">&nbsp;</td>
      </tr>
      {MESSAGEEXTRAS}
      <tr>
        <th colspan="2"><img src="../components/{OPTION}/images/icon-number-3.gif"> {LANG_EMKTG_MESSAGE_RECIPIENTS}</th>
      </tr>
      <tr>
        <td colspan="2">{LANG_EMKTG_MESSAGE_RECIPIENTS_DESC}</td>
      </tr>
      <tr>
        <td colspan="2">&nbsp;</td>
      </tr>
      {PLUGINDATA}
      <tr>
      	<th colspan="2"><img src="../components/{OPTION}/images/icon-number-4.gif"> {LANG_EMKTG_MESSAGE_FINISH}</th>
      </tr>
      <tr>
      	<td colspan="2">
      	  {LANG_EMKTG_MESSAGE_FINISH_DESC}
        </td>
      </tr>
    </table>
    <input type="hidden" name="option" value="{OPTION}" />
    <input type="hidden" name="messageid" value="{ROW_MESSAGEID}" />
    <input type="hidden" name="send_date" value="{ROW_SEND_DATE}" />
    <input type="hidden" name="content_items" value="{ROW_CONTENT_ITEMS}" />
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="hidemainmenu" value="0" />
    </form>
</jos:tmpl>

<jos:tmpl name="preview">
<jos:link src="menuCSS" />
<jos:link src="adminMenu" />
<jos:link src="error" />
<jos:link src="sm2includeTabs" />
	<form action="index2.php" method="post" name="adminForm">
    <input type="hidden" name="option" value="{OPTION}" />
    <input type="hidden" name="cid[0]" value="{ROW_MESSAGEID}" />
    <input type="hidden" name="task" value="previewmessage" />
    <input type="hidden" name="hidemainmenu" value="0" />
    <input type="hidden" name="limitstart" value="" />

    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminheading">
      <tr>
<jos:link src="header" />
      </tr>
    </table>
    <table cellpadding="4" cellspacing="1" border="0" class="adminform">
      <tr>
      	<th>{LANG_EMKTG_MESSAGE_CONTENT}</th>
        <th>{LANG_EMKTG_MESSAGE_SENDTEST}</th>
      </tr>
      <tr>
      <td width="400px">
        	{LANG_EMKTG_MESSAGE_SUBJECT}: <em>{ROW_SUBJECT}</em><br />
        	{LANG_EMKTG_MESSAGE_FROMNAME}: <em>{ROW_FROMNAME}</em><br />
        	{LANG_EMKTG_MESSAGE_FROMEMAIL}: <em>{ROW_FROMEMAIL}</em><br />
        	{LANG_EMKTG_MESSAGE_BOUNCEEMAIL}: <em>{ROW_BOUNCEEMAIL}</em><br />
        </td>
      	<td>
      		If you would like to send a test message, you can do so by filling out the form below and pushing the 'Send Test' button above.<br /><br />
      		<label for="testname" style="float: left; width: 80px;">Name:</label><input type="text" name="testname"><br />
      		<label for="testemail" style="float: left; width: 80px;">Email Address:</label><input type="text" name="testemail"><br />
      		<label for="testhtml" style="float: left; width: 76px;">Send HTML?</label><input type="checkbox" name="testhtml" value="1"><br /><br />
      	</td>
      </tr>
    </table>
  <table width="100%">
	<tr>
	<td>
      <p>For J1.5 simulate the tabs and just have a single iframe that the tabs change the source of.
        This means also modifying the default code so that in J1.0 it uses an iframe for the recipients as well.
      </p>

      <div>
        <div class="sm2tabArea">
          <a class="sm2tab{TAB0ACTIVE}" href="index2.php?option={OPTION}&task=previewmessage&action=0&&cid[]={ROW_MESSAGEID}">{LANG_EMKTG_MESSAGE_PREVIEW_HTML}</a>
          <a class="sm2tab{TAB2ACTIVE}" href="index2.php?option={OPTION}&task=previewmessage&action=2&cid[]={ROW_MESSAGEID}">{LANG_EMKTG_MESSAGE_PREVIEW_TEXT}</a>
          <a class="sm2tab{TAB3ACTIVE}" href="index2.php?option={OPTION}&task=previewmessage&action=3&cid[]={ROW_MESSAGEID}">{LANG_EMKTG_MESSAGE_PREVIEW_RECIPIENTS}</a>
        </div>
        <div class="sm2tabMain">
          <div class="sm2tabIframeWrapper">
            <jos:tmpl type="condition" conditionvar="action" varscope="preview">
              <jos:sub condition="0">
                <iframe class="sm2tabContent" name="myIframe" src="index2.php?option={OPTION}&task=previewmessage&action=1&no_html=1&cid[]={ROW_MESSAGEID}"
                  marginheight="8" marginwidth="8" frameborder="0" height="500"></iframe>
              </jos:sub>
              <jos:sub condition="2">
                <div class="sm2tabContent">{TEXT_MESSAGE}</div>
              </jos:sub>
              <jos:sub condition="3">
                <div class="sm2tabContent">
<jos:tmpl type="condition" conditionvar="PREVIEW_TOTAL" varscope="preview">
<jos:sub condition="0">
<div>{LANG_EMKTG_MESSAGE_PREVIEW_NO_RECIPIENTS}</div>
</jos:sub>
<jos:sub condition="__default">
<table cellpadding="4" cellspacing="1" border="0" cols="2" class="adminform">
  <tr>
    <td colspan="2">{LANG_EMKTG_MESSAGE_PREVIEW_MSG}</td>
  </tr>
  <tr>
    <td colspan="2">{LANG_EMKTG_MESSAGE_PREVIEW_TOTAL}:&nbsp;&nbsp;{TOTAL}</td>
  </tr>
  <jos:tmpl type="simplecondition" requiredvars="SM2EM_LITE" varscope="header,preview">
  <tr>
    <td colspan="2" class="message">{LANG_EMKTG_LICENCE_LITE_MESSAGE}{LANG_EMKTG_LICENCE_SM2_PLUG}</td>
  </tr>
  </jos:tmpl>
  <tr>
    <th>{LANG_EMKTG_RECEIVE_HTML} ({TOTAL_HTML})</th>
    <th>{LANG_EMKTG_RECEIVE_TEXT} ({TOTAL_TEXT})</th>
  </tr>
  <tr>
    <td valign="top" style="background-color: #f9f9f9;"><!-- html -->
    <jos:tmpl name="html" type="simplecondition" requiredvars="HTML_ID">
      {HTML_NAME} ({HTML_EMAIL})<br />
    </jos:tmpl>
    </td>
    <td valign="top" style="background-color: #eeeeee;"><!-- text -->
    <jos:tmpl name="text" type="simplecondition" requiredvars="TEXT_ID">
      {TEXT_NAME} ({TEXT_EMAIL})<br />
    </jos:tmpl>
    </td>
  </tr>
  <tr>
    <th colspan="2" style="text-align: center;">{PAGE_LINK}</th>
  </tr>
</table>
</jos:sub>
</jos:tmpl>
                </div>
              </jos:sub>
            </jos:tmpl>
          </div>
        </div>
      </div>
	</td>
	</tr>
  </table>
  </form>
</jos:tmpl>


<jos:tmpl name="archive">
<jos:link src="menuCSS" />
<jos:link src="adminMenu" />
<jos:link src="error" />
<jos:link src="sm2includeTabs" />
	<form action="index2.php" method="post" name="adminForm">
    <input type="hidden" name="option" value="{OPTION}" />
    <input type="hidden" name="id" value="{REC_ID}" />
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="hidemainmenu" value="0" />

    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminheading">
      <tr>
<jos:link src="header" />
      </tr>
    </table>
    <table cellpadding="4" cellspacing="1" border="0" class="adminform">
      <tr>
        <td>{LANG_EMKTG_MESSAGE_SUBJECT}:</td>
        <td>{ROW_SUBJECT}</td>
      </tr>
      <tr>
        <td>{LANG_EMKTG_ARCHIVE_RECIPIENT_NAME}:</td>
        <td>{REC_NAME}</td>
      </tr>
      <tr>
        <td>{LANG_EMKTG_ARCHIVE_RECIPIENT_EMAIL}:</td>
        <td>{REC_EMAIL}</td>
      </tr>
      <tr>
        <td>{LANG_EMKTG_MESSAGE_SENDDATE}:</td>
        <td>{REC_SEND_DATEDISPLAY}</td>
      </tr>
    </table>
    <table width="100%">
	  <tr>
	    <th>{LANG_EMKTG_MESSAGE}</th>
      </tr>
      <tr>
        <td>
<iframe
src="index2.php?option={OPTION}&task=showarchivemessage&action=1&no_html=1&cid[]={REC_ID}"
width="100%"
height="500"
scrolling="auto"
align="top"
frameborder="0"></iframe>
        </td>
      </tr>
    </table>
    </form>
</jos:tmpl>


<jos:tmpl name="check">
<jos:link src="menuCSS" />
<jos:link src="adminMenu" />
    <script language="javascript" type="text/javascript">
    function submitbutton(pressbutton) {
      var form = document.adminForm;
      var action = "";

      submitform(pressbutton);
    }
    </script>
    <jos:link src="error" />
    <form action="index2.php" method="post" name="adminForm">
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminheading">
      <tr>
<jos:link src="header" />
      </tr>
    </table>
    <!--
    <div class="quickTips">
    	<dl id="toggleDl">
    		<dt>{LANG_EMKTG_QUICKTIP}</dt>
    		<dd>{LANG_EMKTG_QUICKTIP_MESSAGE}</dd>
    	</dl>
    </div>
    -->
    <table cellpadding="4" cellspacing="1" border="0" class="adminform">
      <tr>
        <th colspan="2">{LANG_EMKTG_MESSAGE_ALREADY_SENT}</th>
      </tr>
      <tr>
        <td>{LANG_EMKTG_MESSAGE_SUBJECT}</td>
        <td>{ROW_SUBJECT}</td>
      </tr>
      <tr>
        <td>{LANG_EMKTG_MESSAGE_FROMNAME}</td>
        <td>{ROW_FROMNAME}</td>
      </tr>
      <tr>
        <td>{LANG_EMKTG_MESSAGE_FROMEMAIL}</td>
        <td>{ROW_FROMEMAIL}</td>
      </tr>
      <tr>
        <td>{LANG_EMKTG_MESSAGE_SENDDATE}</td>
        <td>{ROW_SEND_DATE}</td>
      </tr>
      <tr>
        <td colspan="2">{LANG_EMKTG_MESSAGE_ALREADY_SENT_MSG}</td>
      </tr>
    </table>
    <input type="hidden" name="option" value="{OPTION}" />
    <input type="hidden" name="cid[]" value="{ROW_MESSAGEID}" />
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="hidemainmenu" value="0" />
    </form>
</jos:tmpl>
