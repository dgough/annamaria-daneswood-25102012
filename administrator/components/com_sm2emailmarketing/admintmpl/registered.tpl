<jos:comment>
@package Joomla!
@subpackage com_sm2emailmarketing
@copyright Copyright (C) 2006 SM2 / All Rights Reserved
@license commercial
@author SM2 <info@sm2joomla.com>
</jos:comment>

<jos:tmpl name="header">
      <th style="background: url(./components/{OPTION}/images/registeredSubscribers.gif) no-repeat left; height: 70px; padding-left: 80px;" width="100%" align="left">
        {FORM_NAME}
      </th>
</jos:tmpl>

<jos:tmpl name="manager">
<jos:link src="menuCSS" />
<jos:link src="adminMenu" />
<jos:link src="error" />
<form action="index2.php" method="post" name="adminForm">
  <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminheading">
    <tr>
<jos:link src="header" />
      <td valign="bottom">
        <table border="0" width="100%">
          <tr>
            <td valign="bottom">{LANG_EMKTG_SEARCH_TITLE}</td>
            <td valign="bottom">
              <input type="text" name="search" value="{SEARCH}" class="inputbox" onChange="document.adminForm.submit();" />
            </td>
            <td valign="bottom">
              <select name="receive_html" id="filter_receive_html" class="inputbox" onChange="document.adminForm.submit();">
                <option value="-1">{LANG_EMKTG_FILTER_RECEIVE_HTML_TEXT}</option>
                <jos:tmpl name="filter_receive_html" type="simplecondition" requiredvars="value">
                  <option value="{VALUE}" {SELECTED} >{TEXT}</option>
                </jos:tmpl>
              </select>
            </td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td valign="bottom">
              <select name="confirmed" id="filter_confirmed" class="inputbox" onChange="document.adminForm.submit();">
                <option value="-1">{LANG_EMKTG_FILTER_CONFIRMED_TEXT}</option>
                <jos:tmpl name="filter_confirmed" type="simplecondition" requiredvars="value">
                  <option value="{VALUE}" {SELECTED} >{TEXT}</option>
                </jos:tmpl>
              </select>
            </td>
            <td valign="bottom">
              <select name="subscribed" id="filter_subscribed" class="inputbox" onChange="document.adminForm.submit();">
                <option value="-1">{LANG_EMKTG_FILTER_SUBSCRIBED_TEXT}</option>
                <jos:tmpl name="filter_subscribed" type="simplecondition" requiredvars="value">
                  <option value="{VALUE}" {SELECTED} >{TEXT}</option>
                </jos:tmpl>
              </select>
            </td>
            <jos:tmpl type="simplecondition" requiredvars="LANG_SM2EM_LISTS" varscope="manager">
              <td></td>
            </tr>
            <tr>
              <td></td>
              <td valign="bottom" colspan="2">
                <select name="list" id="filter_list" class="inputbox" onChange="document.adminForm.submit();">
                  <option value="-1">{LANG_EMKTG_FILTER_LIST_TEXT}</option>
                  <jos:tmpl name="filter_list" type="simplecondition" requiredvars="value">
                    <option value="{VALUE}" {SELECTED} >{TEXT}</option>
                  </jos:tmpl>
                </select>
              </td>
            </jos:tmpl>
            <td valign="bottom"><input type="submit" value="Submit"></td>
        </table>
      </td>
    </tr>
  </table>
  <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
    <tr>
      <th width="20" align="center" nowrap><input type="checkbox" name="toggle" value="" onclick="checkAll({NUM_ROWS});" /></th>
      <th class="title" width="20%" nowrap>{LANG_CMN_NAME}</th>
      <th class="title" width="30%" nowrap>{LANG_CMN_EMAIL}</th>
      <th width="10%" align="left" nowrap>{LANG_EMKTG_RECEIVE_HTML}</th>
      <th width="10%" align="left" nowrap>{LANG_EMKTG_CONFIRMED}</th>
      <th width="15%" align="left" nowrap>{LANG_EMKTG_SUBSCRIBE_DATE}</th>
      <th width="15%" align="left" nowrap>{LANG_EMKTG_UNSUBSCRIBE_DATE}</th>
    </tr>
    <jos:tmpl name="rows" type="simplecondition" requiredvars="ROW_ID" varscope="manager">
    <tr class="row{ROW_K}">
      <td width="20" align="center">
        {ROW_CHECKEDDISPLAY}
      </td>
      <td width="20%">
        <a href="{ROW_LINK}" title="{LANG_EMKTG_SUBSCRIPTION_EDIT}"><jos:var name="ROW_NAME" modifier="htmlspecialchars" modifierType="php" /></a>
      </td>
      <td width="30%">{ROW_EMAIL}</td>
      <td width="10%" align="center">{ROW_RECEIVE_HTMLDISPLAY}</td>
      <td width="10%" align="center">{ROW_CONFIRMEDDISPLAY}</td>
      <td width="15%" align="left">{ROW_SUBSCRIBE_DATE}</td>
      <td width="15%" align="left">{ROW_UNSUBSCRIBE_DATE}</td>
    </tr>
    </jos:tmpl>
  </table>
  {LISTFOOTER}
  <input type="hidden" name="option" value="{OPTION}" />
  <input type="hidden" name="task" value="showregistered" />
  <input type="hidden" name="boxchecked" value="0" />
  <input type="hidden" name="hidemainmenu" value="0" />
</form>
</jos:tmpl>

<jos:tmpl name="edit">
<jos:link src="menuCSS" />
<jos:link src="adminMenu" />
    <script language="javascript" type="text/javascript">
    function submitbutton(pressbutton) {
      submitform(pressbutton);
    }
    function confirmSubscribe(msg) {
      var form = document.adminForm;
      if (form.resubscribe.checked) {
        if (!confirm(msg)) {
          form.resubscribe.checked = false;
        }
      }
    }
    </script>
    <jos:link src="error" />
    <form action="index2.php" method="post" name="adminForm">
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminheading">
      <tr>
<jos:link src="header" />
      </tr>
    </table>
    <div class="quickTips">
    	<dl id="toggleDl">
    		<dt>{LANG_EMKTG_QUICKTIP}</dt>
    		<dd>{LANG_EMKTG_QUICKTIP_REGISTERED}</dd>
    	</dl>
    </div>
    <table cellpadding="4" cellspacing="1" border="0" class="adminform">
      <tr>
        <td width="100px">{LANG_CMN_NAME}</td>
        <td>
          {ROW_NAME}
          <jos:tmpl varscope="edit" type="simplecondition" requiredvars="editregisteredlink">
            <a href="javascript: void(0);" onClick="return editRegistered();">
              <img src="images/filesave.png" border="0" title="Edit Person" />
            </a>
            <script type="text/javascript" language="javascript">
			function editRegistered() {
                if (confirm('Editing this person will cancel any changes you have made.\nAre you sure you wish to Edit this Person?')) {
                    document.location.href='{EDITREGISTEREDLINK}';
                }
                return false;
            }
            </script>
          </jos:tmpl>
        </td>
      </tr>
      <tr>
        <td valign="top">{LANG_CMN_EMAIL}</td>
        <td valign="top" valign="top">
          {ROW_EMAIL}
        </td>
      </tr>
      <tr>
        <td valign="top"><jos:emktgtooltip>_EMKTG_RECEIVE_HTML</jos:emktgtooltip></td>
        <td valign="top">
          {LIST_RECEIVE_HTML}
        </td>
      </tr>
      <tr>
        <td valign="top"><jos:emktgtooltip>_EMKTG_CONFIRMED</jos:emktgtooltip></td>
        <td valign="top">
          {LIST_CONFIRMED}
        </td>
      </tr>
      <tr>
        <td valign="top">{LANG_EMKTG_SUBSCRIBE_DATE}</td>
        <td valign="top">
          {ROW_SUBSCRIBE_DATE}
        </td>
      </tr>
      <tr>
        <td valign="top">{LANG_EMKTG_UNSUBSCRIBE_STATUS}</td>
        <td valign="top">
          {ROW_STATUS}
          <jos:tmpl type="simplecondition" varscope="edit" requiredvars="row_resubscribe">
          <br />
          {LANG_EMKTG_MODULE_SUBSCRIBE}
          <input type="checkbox" name="resubscribe" value="1" <jos:tmpl type="simplecondition" varscope="edit" requiredvars="row_resubscribe_msg">onchange="confirmSubscribe('{ROW_RESUBSCRIBE_MSG}');"</jos:tmpl> />
          </jos:tmpl>
        </td>
      </tr>
      <jos:tmpl type="simplecondition" requiredvars="LIST_SUBSCRIPTION" varscope="edit">
      <tr>
        <td valign="top"><jos:emktgtooltip>_EMKTG_SUBSCRIPTION</jos:emktgtooltip></td>
        <td valign="top">
          {LIST_SUBSCRIPTION}
        </td>
      </tr>
      </jos:tmpl>
    </table>
    <input type="hidden" name="option" value="{OPTION}" />
    <input type="hidden" name="id" value="{ROW_ID}" />
    <input type="hidden" name="task" value="" />
    </form>
</jos:tmpl>