<jos:comment>
@package Joomla!
@subpackage com_sm2emailmarketing
@copyright Copyright (C) 2006 SM2 / All Rights Reserved
@license commercial
@author SM2 <info@sm2joomla.com>
</jos:comment>

<jos:tmpl name="header">
      <th style="background: url(./components/{OPTION}/images/statistics.gif) no-repeat left; height: 70px; padding-left: 80px;" width="100%" align="left">
        {FORM_NAME}
      </th>
</jos:tmpl>

<jos:tmpl name="manager">
<jos:link src="menuCSS" />
<jos:link src="adminMenu" />
<jos:link src="error" />
<form action="index2.php" method="post" name="adminForm">
  <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminheading">
    <tr>
<jos:link src="header" />
    </tr>
    <jos:tmpl type="simplecondition" requiredvars="SM2EM_LITE" varscope="header,manager">
    <tr>
      <td>{LANG_EMKTG_LICENCE_LITE_MESSAGE}{LANG_EMKTG_LICENCE_SM2_PLUG}</td>
    </tr>
    </jos:tmpl>
  </table>
  <table cellpadding="4" cellspacing="1" border="0" class="adminform">
      <tr>
        <td class="label">{LANG_EMKTG_STATISTICS_MESSAGES_SENT}</td>
        <td>
          {STATS_MESSAGEID}
        </td>
        <td rowspan="8"><img src="index2.php?option={OPTION}&amp;task=showstatisticsgraph&amp;cid[]=0&amp;no_html=1&amp;width=500&amp;height=140&amp;border=0&amp;legend=1" border="0" /></td>
      </tr>
      <tr>
        <td class="label">{LANG_EMKTG_STATISTICS_HTML_SENT}</td>
        <td>
          {STATS_HTML_SENT}
        </td>
      </tr>
      <tr>
        <td class="label" style="padding-left:20px;">{LANG_EMKTG_STATISTICS_HTML_READ}</td>
        <td>
          {STATS_HTML_READ}
        </td>
      </tr>
      <tr>
        <td class="label" style="padding-left:20px;">{LANG_EMKTG_STATISTICS_HTML_UNREAD}</td>
        <td>
          {STATS_HTML_UNREAD}
        </td>
      </tr>
      <tr>
        <td class="label">{LANG_EMKTG_STATISTICS_TEXT_SENT}</td>
        <td>
          {STATS_TEXT_SENT}
        </td>
      </tr>
      <tr>
        <td class="label">{LANG_EMKTG_STATISTICS_ERRORS}</td>
        <td>
          {STATS_ERRORS}
        </td>
      </tr>
      <tr>
        <td class="label">{LANG_EMKTG_STATISTICS_BLACKLISTS}</td>
        <td>
          {STATS_BLACKLISTS}
        </td>
      </tr>
      <tr>
        <td class="label">{LANG_EMKTG_STATISTICS_BOUNCES}</td>
        <td>
          {STATS_BOUNCES}
        </td>
      </tr>
    </table>
  <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
    <tr>
      <th width="20" align="center" nowrap><input type="checkbox" name="toggle" value="" onclick="checkAll({NUM_ROWS});" /></th>
      <th class="title" width="60%" nowrap>{LANG_EMKTG_MESSAGE_SUBJECT}</th>
      <th width="15%" align="right" nowrap>{LANG_EMKTG_MESSAGE_SENDDATE}</th>
      <th width="5%" nowrap>{LANG_EMKTG_STATISTICS_HTML_SENT}</th>
      <th width="5%" nowrap>{LANG_EMKTG_STATISTICS_TEXT_SENT}</th>
      <th width="5%" nowrap>{LANG_EMKTG_STATISTICS_HTML_READ}</th>
      <th width="5%" nowrap>{LANG_EMKTG_STATISTICS_ERRORS}</th>
      <th width="5%" nowrap>{LANG_EMKTG_STATISTICS_BLACKLISTS}</th>
      <th width="5%" nowrap>{LANG_EMKTG_STATISTICS_BOUNCES}</th>
    </tr>
    <jos:tmpl name="rows" type="simplecondition" requiredvars="ROW_ID" varscope="manager">
    <tr class="row{ROW_K}">
      <td width="20" align="center">
        {ROW_CHECKEDDISPLAY}
      </td>
      <td width="60%"><a href="{ROW_LINK}"><jos:var name="ROW_SUBJECT" modifier="htmlspecialchars" modifierType="php" /></a></td>
      <td width="15%" align="right">{ROW_SEND_DATEDISPLAY}</td>
      <td width="5%" align="right">{ROW_HTML_SENT}</td>
      <td width="5%" align="right" nowrap>{ROW_TEXT_SENT}</td>
      <td width="5%" align="right">{ROW_HTML_READ}</td>
      <td width="5%" align="right">{ROW_ERRORS}</td>
      <td width="5%" align="right">{ROW_BLACKLISTS}</td>
      <td width="5%" align="right">{ROW_BOUNCES}</td>
    </tr>
    </jos:tmpl>
  </table>
  {LISTFOOTER}
  <input type="hidden" name="option" value="{OPTION}" />
  <input type="hidden" name="task" value="showstatistics" />
  <input type="hidden" name="boxchecked" value="0" />
  <input type="hidden" name="hidemainmenu" value="0" />
</form>
</jos:tmpl>

<jos:tmpl name="edit">
<jos:link src="menuCSS" />
<jos:link src="adminMenu" />
    <script language="javascript" type="text/javascript">
    function submitbutton(pressbutton) {
      var form = document.adminForm;

      submitform(pressbutton);
    }
    </script>
    <jos:link src="error" />
    <form action="index2.php" method="post" name="adminForm">
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminheading">
      <tr>
<jos:link src="header" />
      </tr>
      <jos:tmpl type="simplecondition" requiredvars="SM2EM_LITE" varscope="header,edit">
      <tr>
        <td>{LANG_EMKTG_LICENCE_LITE_MESSAGE}{LANG_EMKTG_LICENCE_SM2_PLUG}</td>
      </tr>
      </jos:tmpl>
    </table>
    <table cellpadding="4" cellspacing="1" border="0" class="adminform">
      <tr>
        <td class="label">{LANG_EMKTG_MESSAGE_SUBJECT}</td>
        <td>
          <jos:var name="ROW_SUBJECT" modifier="htmlspecialchars" modifierType="php" />
        </td>
        <td rowspan="8"><img src="index2.php?option={OPTION}&amp;task=showstatisticsgraph&amp;cid[]={ID}&amp;no_html=1&amp;width=400&amp;height=150&amp;border=0&amp;legend=1" border="0" /></td>
      </tr>
      <tr>
        <td class="label">{LANG_EMKTG_TEMPLATE}</td>
        <td>
          <jos:var name="ROW_TEMPLATE_NAME" modifier="htmlspecialchars" modifierType="php" />
        </td>
      </tr>
      <tr>
        <td class="label">{LANG_EMKTG_MESSAGE_SENDDATE}</td>
        <td>
          {ROW_SEND_DATEDISPLAY}
        </td>
      </tr>
      <tr>
        <td class="label">{LANG_EMKTG_STATISTICS_HTML_SENT}</td>
        <td>
          {ROW_HTML_SENT}
        </td>
      </tr>
      <tr>
        <td class="label" style="padding-left:20px;">{LANG_EMKTG_STATISTICS_HTML_READ}</td>
        <td>
          {ROW_HTML_READ}
        </td>
      </tr>
      <tr>
        <td class="label" style="padding-left:20px;">{LANG_EMKTG_STATISTICS_HTML_UNREAD}</td>
        <td>
          {ROW_HTML_UNREAD}
        </td>
      </tr>
      <tr>
        <td class="label">{LANG_EMKTG_STATISTICS_TEXT_SENT}</td>
        <td>
          {ROW_TEXT_SENT}
        </td>
      </tr>
      <tr>
        <td class="label">{LANG_EMKTG_STATISTICS_ERRORS}</td>
        <td>
          {ROW_ERRORS}
        </td>
      </tr>
      <tr>
        <td class="label">{LANG_EMKTG_STATISTICS_BLACKLISTS}</td>
        <td>
          {ROW_BLACKLISTS}
        </td>
      </tr>
      <tr>
        <td class="label">{LANG_EMKTG_STATISTICS_BOUNCES}</td>
        <td>
          {ROW_BOUNCES}
        </td>
      </tr>
    </table>
    <table cellpadding="4" cellspacing="1" border="0" cols="6" class="adminform">
      <tr>
        <th>{LANG_EMKTG_STATISTICS_HTML_READ}</th>
        <th>{LANG_EMKTG_STATISTICS_HTML_UNREAD}</th>
        <th>{LANG_EMKTG_STATISTICS_TEXT_SENT}</th>
        <th>{LANG_EMKTG_STATISTICS_ERRORS}</th>
        <th>{LANG_EMKTG_STATISTICS_BLACKLISTS}</th>
        <th>{LANG_EMKTG_STATISTICS_BOUNCES}</th>
      </tr>
      <tr>
        <td valign="top" style="background-color: #f9f9f9;"><!-- read -->
        <jos:tmpl name="htmlread" type="simplecondition" requiredvars="HTMLREAD_ID">
          <a href="index2.php?option={OPTION}&amp;task=showarchivemessage&amp;cid[]={HTMLREAD_ID}">{HTMLREAD_NAME} ({HTMLREAD_EMAIL})</a><br />
        </jos:tmpl>
        </td>
        <td valign="top" style="background-color: #eeeeee;"><!-- unread -->
        <jos:tmpl name="htmlunread" type="simplecondition" requiredvars="HTMLUNREAD_ID">
          <a href="index2.php?option={OPTION}&amp;task=showarchivemessage&amp;cid[]={HTMLUNREAD_ID}">{HTMLUNREAD_NAME} ({HTMLUNREAD_EMAIL})</a><br />
        </jos:tmpl>
        </td>
        <td valign="top" style="background-color: #f9f9f9;"><!-- text -->
        <jos:tmpl name="textsent" type="simplecondition" requiredvars="TEXTSENT_ID">
          <a href="index2.php?option={OPTION}&amp;task=showarchivemessage&amp;cid[]={TEXTSENT_ID}">{TEXTSENT_NAME} ({TEXTSENT_EMAIL})</a><br />
        </jos:tmpl>
        </td>
        <td valign="top" style="background-color: #eeeeee;"><!-- errors -->
        <jos:tmpl name="errors" type="simplecondition" requiredvars="ERROR_ID">
          <a href="{ERROR_LINK}">{ERROR_NAME} ({ERROR_EMAIL})</a><br />
        </jos:tmpl>
        </td>
        <td valign="top" style="background-color: #f9f9f9;"><!-- blacklists -->
        <jos:tmpl name="blacklists" type="simplecondition" requiredvars="BLACKLIST_ID">
          <a href="{BLACKLIST_LINK}">{BLACKLIST_NAME} ({BLACKLIST_EMAIL})</a><br />
        </jos:tmpl>
        </td>
        <td valign="top" style="background-color: #eeeeee;"><!-- bounces -->
        <jos:tmpl name="bounces" type="simplecondition" requiredvars="BOUNCE_ID">
          <a href="{BOUNCE_LINK}">{BOUNCE_NAME} ({BOUNCE_EMAIL})</a><br />
        </jos:tmpl>
        </td>
      </tr>
    </table>
    <table class="adminlist">
      <tr><th colspan="3">{PAGE_LINK}</th></tr>
    </table>
    <input type="hidden" name="limitstart" value="" />
    <input type="hidden" name="option" value="{OPTION}" />
    <input type="hidden" name="task" value="showmessagestatistics" />
    <input type="hidden" name="cid[]" value="{ID}" />
    </form>
</jos:tmpl>