<jos:comment>
@package Joomla!
@subpackage com_sm2emailmarketing
@copyright Copyright (C) 2006 SM2 / All Rights Reserved
@license commercial
@author SM2 <info@sm2joomla.com>
</jos:comment>

<jos:tmpl name="header">
      <th style="background: url(./components/{OPTION}/images/config.gif) no-repeat left; height: 70px; padding-left: 80px;" width="100%" align="left">
        {FORM_NAME}
      </th>
</jos:tmpl>

<jos:tmpl name="edit">
<jos:link src="menuCSS" />
<jos:link src="adminMenu" />
    <script language="javascript" type="text/javascript">
    function submitbutton(pressbutton) {
      var form = document.adminForm;

      // if cancel then dont validate
      if (pressbutton == 'cancel') {
        submitform(pressbutton);
        return;
      }

      // do field validation
      var msg = "";

      // display validation error or submit
      if (msg!="") {
        alert( "{LANG_EMKTG_VALIDATION_ERROR}\n"+msg );
      } else {
        submitform( pressbutton );
      }
    }
    </script>
    <jos:link src="error" />
    <form action="index2.php" method="post" name="adminForm">
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminheading">
      <tr>
<jos:link src="header" />
      </tr>
    </table>
    <table cellpadding="4" cellspacing="1" border="0" class="adminform">
      <tr>
        <td>
          {PARAMS}
        </td>
      </tr>
    </table>
    <input type="hidden" name="option" value="{OPTION}" />
    <input type="hidden" name="task" value="" />
    </form>
</jos:tmpl>



<jos:tmpl name="installdefault">
<jos:link src="menuCSS" />
<jos:link src="adminMenu" />
<jos:link src="error" />
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminheading">
      <tr>
<jos:link src="header" />
      </tr>
    </table>
    <table cellpadding="4" cellspacing="1" border="0" class="adminform">
      <tr>
        <td>
          {INSTALL_DEFAULT_MSG}
        </td>
      </tr>
    </table>
</jos:tmpl>