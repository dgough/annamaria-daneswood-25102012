<jos:comment>
@package Joomla!
@subpackage com_sm2emailmarketing
@copyright Copyright (C) 2006 SM2 / All Rights Reserved
@license commercial
@author SM2 <info@sm2joomla.com>
</jos:comment>

<jos:tmpl name="header">
      <th style="background: url(./components/{OPTION}/images/listManager.gif) no-repeat left; height: 70px; padding-left: 80px;" width="100%" align="left">
        {FORM_NAME}
      </th>
</jos:tmpl>

<jos:tmpl name="manager">
<jos:link src="menuCSS" />
<jos:link src="adminMenu" />
<jos:link src="error" />
<form action="index2.php" method="post" name="adminForm">
  <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminheading">
    <tr>
<jos:link src="header" />
      <td valign="bottom">{LANG_EMKTG_SEARCH_TITLE}</td>
      <td valign="bottom"><input type="text" name="search" value="{SEARCH}" class="inputbox" onChange="document.adminForm.submit();" /></td>
      <td valign="bottom"><input type="submit" value="Submit"></td>
    </tr>
  </table>
  <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
    <tr>
      <th width="20" align="center" nowrap><input type="checkbox" name="toggle" value="" onclick="checkAll({NUM_ROWS});" /></th>
      <th class="title" width="60%" nowrap>{LANG_EMKTG_LIST_NAME}</th>
      <th class="title" width="10%" style="text-align: right;"><jos:emktgtooltip>_EMKTG_LIST_NUM_USERS</jos:emktgtooltip></th>
      <th class="title" width="10%" style="text-align: right;"><jos:emktgtooltip>_EMKTG_LIST_NUM_SUBSCRIBERS</jos:emktgtooltip></th>
      <th width="1%">&nbsp;</th>
      <th width="1%">&nbsp;</th>
      <th class="title" width="10%" align="left" nowrap>{LANG_EMKTG_ACCESS}</th>
      <th class="title" width="10%" align="left" nowrap>{LANG_CMN_PUBLISHED}</th>
    </tr>
    <jos:tmpl name="rows" type="simplecondition" requiredvars="ROW_ID" varscope="manager">
    <tr class="row{ROW_K}">
      <td width="20" align="center">
        {ROW_CHECKEDDISPLAY}
      </td>
      <td width="80%">
        <a href="{ROW_LINK}" title="{LANG_EMKTG_LIST_EDIT}"><jos:var name="ROW_LIST_NAME" modifier="htmlspecialchars" modifierType="php" /></a>
      </td>
      <td width="10%" align="right">{ROW_LISTUSERS}</td>
      <td width="10%" align="right">{ROW_LISTSUBSCRIBERS}</td>
      <td width="1%" align="center">
        <a href="{ROW_ALLLINK}" onclick="return confirm('{LANG_EMKTG_LIST_SUBSCRIBE_ALL_CONFIRM}');">
          <jos:emktgtooltip image="../../../administrator/images/expandall.png">_EMKTG_LIST_SUBSCRIBE_ALL</jos:emktgtooltip>
        </a>
      </td>
      <td width="1%" align="center">
        <a href="{ROW_NONELINK}" onclick="return confirm('{LANG_EMKTG_LIST_SUBSCRIBE_NONE_CONFIRM}');">
          <jos:emktgtooltip image="../../../administrator/images/collapseall.png">_EMKTG_LIST_SUBSCRIBE_NONE</jos:emktgtooltip>
        </a>
      </td>
      <td width="10%">{ROW_ACCESSDISPLAY}</td>
      <td width="10%">{ROW_PUBLISHEDDISPLAY}</td>
    </tr>
    </jos:tmpl>
  </table>
  {LISTFOOTER}
  <input type="hidden" name="option" value="{OPTION}" />
  <input type="hidden" name="task" value="showlists" />
  <input type="hidden" name="boxchecked" value="0" />
  <input type="hidden" name="hidemainmenu" value="0" />
</form>
</jos:tmpl>

<jos:tmpl name="edit">
<jos:link src="menuCSS" />
<jos:link src="adminMenu" />
    <script language="javascript" type="text/javascript">
    function submitbutton(pressbutton) {
      var form = document.adminForm;

      // if cancel then dont validate
      if (pressbutton == 'showlists') {
        submitform(pressbutton);
        return;
      }

      // do field validation
      var msg = "";
      // validate title
      if (form.list_name.value=='') {
        msg += "{LANG_EMKTG_LIST_NAME_DESC}\n";
      }

      // display validation error or submit
      if (msg!="") {
        alert( "{LANG_EMKTG_VALIDATION_ERROR}\n"+msg );
      } else {
        /*{EDITORCONTENTS}*/
        submitform( pressbutton );
      }
    }
    </script>
    <jos:link src="error" />
    <form action="index2.php" method="post" name="adminForm">
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminheading">
      <tr>
<jos:link src="header" />
      </tr>
    </table>
    <div class="quickTips">
    	<dl id="toggleDl">
    		<dt>{LANG_EMKTG_QUICKTIP}</dt>
    		<dd>{LANG_EMKTG_QUICKTIP_LIST}</dd>
    	</dl>
    </div>
    <table cellpadding="4" cellspacing="1" border="0" class="adminform">
      <tr>
        <td><jos:emktgtooltip>_EMKTG_LIST_NAME</jos:emktgtooltip>&nbsp;{LANG_EMKTG_REQUIRED}</td>
        <td>
          <input class="inputbox" type="text" name="list_name" value="{ROW_LIST_NAME}" size="50" maxlength="100" />
        </td>
      </tr>
      <tr>
        <td valign="top"><jos:emktgtooltip>_EMKTG_LIST_DESCRIPTION</jos:emktgtooltip></td>
        <td valign="top" valign="top">
          <textarea class="inputbox" name="description" cols="36">{ROW_DESCRIPTION}</textarea>
        </td>
      </tr>

      <tr>
        <td valign="top"><jos:emktgtooltip>_EMKTG_LIST_ACCESS</jos:emktgtooltip></td>
        <td valign="top">
          {LIST_ACCESS}
        </td>
      </tr>
      <tr>
        <td valign="top"><jos:emktgtooltip>_EMKTG_LIST_PUBLISH</jos:emktgtooltip></td>
        <td valign="top">
          {LIST_PUBLISHED}
        </td>
      </tr>
    </table>
    <input type="hidden" name="option" value="{OPTION}" />
    <input type="hidden" name="listid" value="{ROW_LISTID}" />
    <input type="hidden" name="task" value="" />
    </form>
<table cellpadding="4" cellspacing="1" border="0" cols="2" class="adminform">
  <tr>
    <th colspan="2">{LANG_EMKTG_SUBSCRIBERS}</td>
  </tr>
<jos:tmpl type="condition" conditionvar="SUBSCRIBER_TOTAL" varscope="edit">
<jos:sub condition="0">
  <tr>
    <td colspan="2">{LANG_EMKTG_LIST_NO_SUBSCRIBERS}</td>
  </tr>
</jos:sub>
<jos:sub condition="__default">
  <tr>
    <td colspan="2">
      <jos:emktgtooltip>_EMKTG_LIST_TOTAL_SUBSCRIBERS</jos:emktgtooltip>:&nbsp;&nbsp;{LIST_TOTAL}<br />
      <jos:emktgtooltip>_EMKTG_LIST_TOTAL_USERS</jos:emktgtooltip>:&nbsp;&nbsp;{USER_TOTAL}<br />
      {LANG_EMKTG_TOTAL_SUBSCRIBERS}:&nbsp;&nbsp;{SUBSCRIBER_TOTAL}<br />
    </td>
  </tr>
  <tr>
    <th>{LANG_EMKTG_REGISTERED_FORM}</th>
    <th>{LANG_EMKTG_UNREGISTERED_FORM}</th>
  </tr>
  <tr>
    <td valign="top" style="background-color: #f9f9f9;"><!-- html -->
    <jos:tmpl name="list_user" type="simplecondition" requiredvars="ID">
      {NAME} ({EMAIL})<br />
    </jos:tmpl>
    </td>
    <td valign="top" style="background-color: #eeeeee;"><!-- text -->
    <jos:tmpl name="list_subscriber" type="simplecondition" requiredvars="SUBSCRIBERID">
      {NAME} ({EMAIL})<br />
    </jos:tmpl>
    </td>
  </tr>
</table>
<table class="adminlist">
  <tr>
    <th colspan="3">{PAGE_LINKS}</th>
  </tr>
</jos:sub>
</jos:tmpl>
</table>
</jos:tmpl>