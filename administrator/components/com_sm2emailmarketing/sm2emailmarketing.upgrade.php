<?php
// $Id: sm2emailmarketing.upgrade.php,v 1.6 2008/03/31 01:59:10 sm2tony Exp $
/**
 * sm2emailmarketing - Email Marketing Newsletter
 *
 * Joomla! Newsletter Component that supports multiple list mailing and message queueing
 *
 * @package Joomla!
 * @subpackage com_sm2emailmarketing
 * @copyright Copyright (C) 2006 SM2 / All Rights Reserved
 * @license commercial
 * @author SM2 <info@sm2joomla.com>
 */

// ensure this file is being included by a parent file
defined('_VALID_MOS') or die('Direct Access to this location is not allowed.');


function sm2EM_Upgrade($option, $task, $path) {
    global $mosConfig_live_site, $mosConfig_fileperms, $componentTables, $adminMenus, $oldFiles, $upgradeFunctions;
    // find each of the files to process

    $upgradeFiles = mosReadDirectory($path, 'upgrade\.\d{1,}\.\d{1,}\.\d{1,}\.php');

    if (!empty($upgradeFiles)) {
        foreach ($upgradeFiles as $upgrade) {
            $componentTables = $adminMenus = $oldFiles = $upgradeFunctions = array();

            include_once($path.'/'.$upgrade);
            sm2EM_processUpgrade();

            // delete the upgrade file
            if (!@unlink($path.'/'.$upgrade)) {
                mosChmodRecursive($path.'/'.$upgrade, $mosConfig_fileperms);
                @unlink($path.'/'.$upgrade);
            }

        }
    }
} // sm2EM_Upgrade


function sm2EM_processUpgrade() {
    global $database, $mosConfig_absolute_path, $mosConfig_fileperms, $componentTables, $adminMenus, $oldFiles, $upgradeFunctions;

    if (!empty($componentTables)) {
        // get the list of site tables
        $siteTables = $database->getTableList();
        array_walk($siteTables, 'arrayConvertToPrefix');

        $tableFields = $database->getTableFields(array_intersect($siteTables, array_keys($componentTables)));

        $tablesUpdated = array();

        // look at all of the $componentTables
        foreach ($componentTables as $table=>$tableData) {
            // check for existing table
            if (!in_array($table, $siteTables)) {
                // create the table
                if (!empty($tableData['create'])) {
                    $tablesUpdated[] = $table;
                    foreach ($tableData['create'] as $sql) {
                        $database->setQuery($sql);
                        $database->query();
                    }
                }
                continue;
            }

            // check to see if all of the fields exist
            if (!empty($tableData['fields'])) {
                $newFields = array_diff(array_keys($tableData['fields']), array_keys($tableFields[$table]));
                if ((is_array($newFields)) && (count($newFields)>0)) {
                    $tablesUpdated[] = $table;
                }
                foreach ($newFields as $newField) {
                    foreach ($tableData['fields'][$newField] as $sql) {
                        $database->setQuery($sql);
                        $database->query();
                    }
                }
            }

            // check for fields to change
            if (!empty($tableData['change'])) {
                foreach ($tableData['change'] as $field=>$changeData) {
                    if ($tableFields[$table][$field] != $changeData['type']) {
                        $database->setQuery($changeData['sql']);
                        $database->query();
                    }
                }
            }

            // check the keys for this table
            if (!empty($tableData['keys'])) {
                $database->setQuery('SHOW INDEX FROM '.$table);
                $indexes = $database->loadResultArray(2);
                array_walk($indexes, 'arrayConvertToPrefix');
                $newIndexes = array_diff(array_keys($tableData['keys']), $indexes);
                if ((is_array($newIndexes)) && (count($newIndexes)>0)) {
                    $tablesUpdated[] = $table;
                }
                foreach ($newIndexes as $newIndex) {
                    $database->setQuery($tableData['keys'][$newIndex]);
                    $database->query();
                }
            }

            // check for fields to drop
            if (!empty($tableData['dropfields'])) {
                $database->setQuery('ALTER TABLE `'.$table.'` DROP COLUMN `'
                    .implode('`, DROP COLUMN `', $tableData['dropfields'])
                    .'`');
                $database->query();
            }

            // check for tables to be dropped
            if (isset($tableData['drop'])) {
                if ($tableData['drop']===true) {
                    $database->setQuery('DROP TABLE IF EXISTS `'.$table.'`');
                    $database->query();
                }
            }
        }

        // check to see if we have to display something
        if (count($tablesUpdated)>0) {
            echo '<div class="message">Updated the following tables: '.implode(', ',$tablesUpdated).'</div>';
            echo $database->_errorMsg;
        }
    }


    if (!empty($adminMenus)) {
        // process the adminMenus
        foreach ($adminMenus as $com_option=>$menus) {
            // determine the parent id for all com_option menu items
            $database->setQuery('SELECT id FROM #__components WHERE `link` = '.$database->Quote('option='.$com_option).' AND `option`='.$database->Quote($com_option));
            $parentId = $database->loadResult();

            // find all of the menus
            $database->setQuery('SELECT * FROM #__components WHERE `option`='.$database->Quote($com_option).' AND `id` != '.(int)$parentId.' ORDER BY ordering');
            $currentMenus = $database->loadObjectList();

            $processedOrdering = array();

            foreach ($currentMenus as $currentMenu) {

                if (array_key_exists($currentMenu->ordering, $menus)) {

                    // update the menu
                    $qryParts = array();
                    foreach ($menus[$currentMenu->ordering] as $field=>$value) {
                        $qryParts[] = '`'.$field.'` = '.$database->Quote($value);
                    }
                    $database->setQuery('UPDATE #__components SET '.implode(', ', $qryParts).' WHERE id='.(int)$currentMenu->id);
                    $database->query();

                    $processedOrdering[] = $currentMenu->ordering;

                } else {

                    // delete the menu
                    $database->setQuery('DELETE FROM #__components WHERE id='.(int)$currentMenu->id);
                    $database->query();

                }

            } // foreach $currentMenus

            // check to see if we need to insert
            $newMenus = array_diff(array_keys($menus), $processedOrdering);
            foreach ($newMenus as $newOrder) {
                $currentMenu = $menus[$newOrder];

                $currentMenu['parent'] = $parentId;
                $currentMenu['option'] = $com_option;
                $currentMenu['ordering'] = $newOrder;

                $fields = array();
                $values = array();
                foreach ($currentMenu as $field=>$value) {
                    $fields[] = '`'.$field.'`';
                    $values[] = $database->Quote($value);
                } // foreach $currentMenu

                $database->setQuery('INSERT INTO #__components ('.implode(',', $fields).') values ('.implode(',', $values).')');
                $database->query();

            } // foreach $newMenus
        } // foreach $adminMenus
    }

    if (!empty($oldFiles)) {
        foreach ($oldFiles as $old) {
            if (@unlink($mosConfig_absolute_path.'/'.$old)) {
                mosChmodRecursive($mosConfig_absolute_path.'/'.$old, $mosConfig_fileperms);
                @unlink($mosConfig_absolute_path.'/'.$old);
            }
        }
    }

    if (!empty($upgradeFunctions)) {
        foreach ($upgradeFunctions as $function) {
            $return = $function();
        }
    }

} // sm2EM_processUpgrade()


function arrayConvertToPrefix(&$item, $key) {
    global $mosConfig_dbprefix;

    $item = str_replace($mosConfig_dbprefix, '#__', $item);
} // arrayConvertToPrefix
?>