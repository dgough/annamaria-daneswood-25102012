<?php
// $Id: toolbar.sm2emailmarketing.php,v 1.9 2008/03/31 01:59:10 sm2tony Exp $
/**
 * sm2emailmarketing - Email Marketing Newsletter
 *
 * Joomla! Newsletter Component that supports multiple list mailing and message queueing
 *
 * @package Joomla!
 * @subpackage com_sm2emailmarketing
 * @copyright Copyright (C) 2006 SM2 / All Rights Reserved
 * @license commercial
 * @author SM2 <info@sm2joomla.com>
 */

/**
 * Makes sure this is included by a parent file
 */
defined('_VALID_MOS') or die('Direct Access to this location is not allowed.');

/**
 * Include required files
 */
require_once( $mainframe->getPath( 'toolbar_html' ) );
require_once( $mainframe->getPath( 'toolbar_default' ) );

// switch by action
switch ($task) {

    /**
     * List Tasks
     */
    case 'showlists':
        menu_sm2emailmarketing::LIST_DEFAULT();
        break;
    case 'newlist':
        menu_sm2emailmarketing::LIST_NEW();
        break;
    case 'editlist':
    case 'copylist':
        menu_sm2emailmarketing::LIST_EDIT();
        break;

    /**
     * Template Tasks
     */
    case 'showtemplates':
        menu_sm2emailmarketing::TEMPLATE_DEFAULT();
        break;
    case 'newtemplate':
    case 'edittemplate':
    case 'copytemplate':
        menu_sm2emailmarketing::TEMPLATE_EDIT();
        break;
    case 'previewtemplate':
        menu_sm2emailmarketing::TEMPLATE_PREVIEW();
        break;

    /**
     * Registered User Tasks
     */
    case 'showregistered':
        menu_sm2emailmarketing::REGISTERED_DEFAULT();
        break;
    case 'editregistered':
        menu_sm2emailmarketing::REGISTERED_EDIT();
        break;

    /**
     * Unregistered User Tasks
     */
    case 'showunregistered':
        menu_sm2emailmarketing::UNREGISTERED_DEFAULT();
        break;
    case 'newunregistered':
    case 'editunregistered':
        menu_sm2emailmarketing::UNREGISTERED_EDIT();
        break;
    case 'importunregistered':
        menu_sm2emailmarketing::UNREGISTERED_IMPORT();
        break;

    /**
     * Message Tasks
     */
    case 'showmessages':
        menu_sm2emailmarketing::MESSAGE_DEFAULT();
        break;
    case 'newmessage':
    case 'editmessage':
    case 'copymessage':
        menu_sm2emailmarketing::MESSAGE_EDIT();
        break;
    case 'checkmessage':
        menu_sm2emailmarketing::MESSAGE_CHECK();
        break;
    case 'previewmessage':
    case 'savepreviewmessage':
    	menu_sm2emailmarketing::MESSAGE_PREVIEW();
    	break;

    /**
     * Queue Tasks
     */
    case 'showqueue':
        menu_sm2emailmarketing::QUEUE_DEFAULT();
        break;
    case 'processqueue':
        menu_sm2emailmarketing::QUEUE_PROCESS();
        break;

    /**
     * Plugin Tasks
     */
    case 'showplugins':
        menu_sm2emailmarketing::PLUGIN_DEFAULT();
        break;
    case 'editplugin':
        menu_sm2emailmarketing::PLUGIN_EDIT();
        break;

    /**
     * Configuration Tasks
     */
    case 'showconfiguration':
        menu_sm2emailmarketing::CONFIGURATION_DEFAULT();
        break;

    /**
     * Statistics Tasks
     */
    case 'showstatistics':
        menu_sm2emailmarketing::STATISTICS_DEFAULT();
        break;
    case 'showmessagestatistics':
        menu_sm2emailmarketing::STATISTICS_VIEW();
        break;

    default:
        //menu_sm2emailmarketing::STATISTICS_DEFAULT();
        break;


} // end switch
