<?php
// $Id: sm2em_cb.php,v 1.17 2008/04/21 03:35:09 sm2tony Exp $
/**
 * sm2emailmarketing - Email Marketing Newsletter
 *
 * Joomla! Newsletter Component that supports multiple list mailing and message queueing
 *
 * @package Joomla!
 * @subpackage com_sm2emailmarketing
 * @copyright Copyright (C) 2006 SM2 / All Rights Reserved
 * @license commercial
 * @author SM2
 */

/**
 * Makes sure this is included by a parent file
 */
defined('_VALID_MOS') or die('Direct Access to this location is not allowed.');

$_PLUGINS->registerFunction( 'onUserActive', 'userActivated','getSM2EMTab' );
$_PLUGINS->registerFunction( 'onBeforeDeleteUser', 'userDeleted','getSM2EMTab' );

global $mosConfig_absolute_path, $my;

class getSM2EMTab extends cbTabHandler {

    var $receiveOptions = array(0=>'None',1=>'Text only',2=>'HTML');

    /**
     * Constructor
     */
    function getSM2EMTab() {
        $this->cbTabHandler();
    } // getSM2EMTab()


    function setReceiveOptions() {
        if (isset($this->params)) {
            if (is_object($this->params)) {
                $this->receiveOptions = array();
                $this->receiveOptions[0] = $this->params->get('receive_none','None');
                $this->receiveOptions[1] = $this->params->get('receive_text','Text only');
                $this->receiveOptions[2] = $this->params->get('receive_html','HTML');
            }
        }
    } // setReceiveOptions()


    function getSM2EMUser($id, $update=false) {
        global $mainframe, $mosConfig_lang;
        // load the sm2emailmarketingUser object
        require_once($mainframe->getPath('class', 'com_sm2emailmarketing'));
        
        if (file_exists($GLOBALS['mosConfig_absolute_path'].'/components/com_sm2emailmarketing/languages/'.$mosConfig_lang.'.php')) {
            include_once ($GLOBALS['mosConfig_absolute_path'].'/components/com_sm2emailmarketing/languages/'.$mosConfig_lang.'.php');
        } else {
            include_once ($GLOBALS['mosConfig_absolute_path'].'/components/com_sm2emailmarketing/languages/english.php');
        } // end if
        sm2emailmarketingConfig('com_sm2emailmarketing');
        $user = new sm2emailmarketingUser();

        $user->load((int)$id);

        if (!$update) {

            // validate the unsubscribe_date
            if ($user->unsubscribe_date===null) {
                $user->unsubscribe_date='0000-00-00 00:00:00';
            }

            // determine the status
            if ($user->unsubscribe_date!='0000-00-00 00:00:00' && $user->unsubscribe_reason==0) {
                $user->unsubscribe_reason=5;
            }


            if (empty($id)) {
                $user->unsubscribe_reason='NULL';
            }

            $statusVar = '_EMKTG_UNSUBSCRIBE_REASON_'.$user->unsubscribe_reason;
            if (defined($statusVar)) {
                $user->status = constant($statusVar);
            } else {
                $user->unsubscribe_reason = 5;
                $user->status = _EMKTG_UNSUBSCRIBE_REASON_5;
            }

            if (empty($id)) {
                $user->unsubscribe_reason=0;
            }

        }

        return $user;

    } // getSM2EMUser()


    function getDisplayTab($tab, $user, $ui) {
        global $my, $database;

        $this->setReceiveOptions();

        // check to see if this is a logged in user
        if (empty($my->id)) {
            mosNotAuth();
            return false;
        }

        if (empty($user->id)) {
            $userid = 0;
        } else {
            $userid = $user->id;
        }


        if(!$this->checkSM2EMInstalled()) {
            return _UE_NEWSLETTERNOTINSTALLED;
        }

        $params = $this->params;
        //$showReceive = $params->get('default_receive', 0);
        //$showReceive = (empty($showReceive) ? true : false);

        // determine this users receive_html option
        // load the sm2emailmarketingUser object
        $sm2emUser = $this->getSM2EMUser($userid);

        if ($sm2emUser->unsubscribe_date!='0000-00-00 00:00:00') {
            $sm2emUser->receive_html = 0;
        } else {
            $sm2emUser->receive_html++;
        }

        $emailreceived ='';

        if (!empty($userid)) {
            // determine the itemid
            $database->setQuery('SELECT `id`'
                .' FROM #__menu'
                .' WHERE `link` LIKE '.$database->Quote('%option=com_sm2emailmarketing%')
                .' AND `type`='.$database->Quote('components')
                .' ORDER BY `id` LIMIT 0, 1');
            $itemid = (int)$database->loadResult();
            $itemid = (empty($itemid) ? '' : '&Itemid='.$itemid);

            // get list of past emails sent
            $database->setQuery('SELECT sm.messageid, m.subject'
                    .' FROM #__emktg_stats_message sm'
                    .' LEFT JOIN #__emktg_message m on sm.messageid = m.messageid'
                    .' WHERE sm.userid='.(int)$userid);
            $archivedemail = $database->loadAssocList();


            foreach($archivedemail as $email) {
                $url = 'index.php?option=com_sm2emailmarketing'
                    .'&task=showarchivemessage'
                    .'&id='.$email['messageid']
                    .'&Itemid='.$itemid;
                if (function_exists('sefRelToAbs') && !defined('_JLEGACY')) {
                    $url = sefRelToAbs($url);
                }
                $emailreceived .= '<a href="'.$url.'">'
                    .$email['subject'].'</a><br />';
            }
        }

        $lists = '';
        $database->setQuery('SELECT list_name FROM #__emktg_list l'
            .' INNER JOIN #__emktg_list_user lu ON l.listid=lu.listid'
            .' WHERE lu.id='.(int)$userid
            .' ORDER BY list_name');
        $listNames = $database->loadResultArray();
        if (is_array($listNames)) {
            $lists = implode('<br />', $listNames);
        }
        unset($listNames);

        ob_start();
?>
<table style="width:100%">
  <tr>
    <td width="25%" valign="top"><?php echo _EMKTG_UNSUBSCRIBE_STATUS; ?></td>
    <td width="75%"><?php echo $sm2emUser->status; ?></td>
  </tr>
  <tr>
    <td colspan="2" class="titleCell" valign="top" align="left"><?php echo $this->params->get('receive', 'Communication formats'); ?>:</td>
  </tr>
  <tr>
    <td width="25%">&nbsp;</td>
    <td width="75%">
      <?php echo $this->receiveOptions[$sm2emUser->receive_html]; ?>
    </td>
  </tr>
  <?php if (!empty($lists)) { ?>
  <tr>
    <td colspan="2" class="titleCell" valign="top" align="left"><?php echo $this->params->get('list_subscribed', 'Lists you have subscribed to'); ?>:</td>
  </tr>
  <tr>
    <td width="25%">&nbsp;</td>
    <td width="75%">
      <?php echo $lists; ?>
    </td>
  </tr>
  <?php } ?>
  <?php if (!empty($emailreceived)) { ?>
  <tr>
    <td colspan="2" class="titleCell" valign="top" align="left"><?php echo $this->params->get('archive_emails', 'Emails that you have been sent, click to view'); ?>:</td>
  </tr>
  <tr>
    <td width="25%">&nbsp;</td>
    <td width="75%">
      <?php echo $emailreceived; ?>
    </td>
  </tr>
  <?php } ?>
</table>
<?php
        $return = ob_get_contents();
        ob_end_clean();
        return $return;
    }


    /**
     * Generates the HTML to display the user edit tab
     * @param object tab reflecting the tab database entry
     * @param object mosUser reflecting the user being displayed
     * @param int 1 for front-end, 2 for back-end
     * @returns mixed : either string HTML for tab content, or false if ErrorMSG generated
     */
    function getEditTab($tab, $user, $ui) {
        global $my, $database;

        $this->setReceiveOptions();

        // check to see if this is a logged in user
        if (empty($my->id) && !empty($user)) {
            mosNotAuth();
            return false;
        }

        if (empty($user->id)) {
            $userid = 0;
        } else {
            $userid = $user->id;
        }


        if (!$this->checkSM2EMInstalled()) {
            return _UE_NEWSLETTERNOTINSTALLED;
        }

        $params = $this->params;
        $defaultReceive = $params->get('default_receive', 0);

        $lists = $this->findLists($userid, $ui);

        // load the sm2emailmarketingUser object
        $sm2emUser = $this->getSM2EMUser($userid);

        $receive_html = 2;
        if ($sm2emUser->unsubscribe_date!='0000-00-00 00:00:00') {
            // they are unsubscribed
            $receive_html = 0;
        } else {
            if ($sm2emUser->receive_html!==null) {
                $receive_html = $sm2emUser->receive_html + 1;
            }
        }

        if (!empty($defaultReceive)) {
            if ($defaultReceive==1) {
                unset($this->receiveOptions[2]);
            } else {
                unset($this->receiveOptions[1]);
            }
        }

        // get the htmlemail list
        $htmlemail = array();
        foreach ($this->receiveOptions as $index=>$value) {
            $htmlemail[] = mosHTML::makeOption($index,$value);
        }

        // determine the extra attributes needed by the receive_htmlDisplay
        $attributes = 'class="inputbox" size="1"';
        $onChange = '';


        if ($sm2emUser->unsubscribe_reason > 0) {
            // determine the resubscribe message
            $msgVar = '_EMKTG_RESUBSCRIBE_MSG_'.$sm2emUser->unsubscribe_reason;

            if (defined($msgVar)) {
                $onChange = ' onchange="confirmSubscribe(\''.constant($msgVar).'\');"';
            }
        }

        // determine what javascript processing to do on confirmSubscription
        $confirmJs = 'if (!confirm(msg)) {
          form.sm2em_receive_html.selectedIndex=0;
        }';
        $formJs = 'document.adminForm;';


        if ($ui==1) {
           //$formJs = 'document.EditUser;';
            // determine the confirmSubscription javascript based on the unsubscribe reason
            if ($sm2emUser->unsubscribe_reason > 1) {
                $confirmJs = 'form.sm2em_receive_html.selectedIndex=0;
        form.sm2em_receive_html.disabled = true;
        form.sm2em_receive_html.readonly = true;
        alert(\''.constant('_EMKTG_RESUBSCRIBE_ERROR_'.$sm2emUser->unsubscribe_reason).'\');';
            } else {
                $onChange = '';
            }
        }

        if ($ui==3) {
            $onChange = '';
            $sm2emUser->status = _EMKTG_UNSUBSCRIBE_REASON_NULL;
        }

        $receive_htmlDisplay = mosHTML::selectList($htmlemail, 'sm2em_receive_html', $attributes.$onChange, 'value', 'text', $receive_html);
        unset($htmlemail);

        ob_start();
?>
  <tr>
    <td colspan="2" class="titleCell" valign="top" align="left"><?php echo _EMKTG_UNSUBSCRIBE_STATUS; ?></td>
  </tr>
  <tr>
    <td width="25%">&nbsp;</td>
    <td width="75%">
    <script language="javascript" type="text/javascript">
    function confirmSubscribe(msg) {
      if (msg=='') return;
      var form = <?php echo $formJs; ?>;
      if (form.sm2em_receive_html.selectedIndex!=0) {
        <?php echo $confirmJs; ?>
      }
    }
    </script>
    <?php echo $sm2emUser->status; ?>
    </td>
  </tr>
  <tr>
    <td colspan="2" class="titleCell" valign="top" align="left"><?php echo $this->params->get('receive', 'Communication formats'); ?>:</td>
  </tr>
  <tr>
    <td width="25%">&nbsp;</td>
    <td width="75%">
      <?php echo $receive_htmlDisplay; ?>
    </td>
  </tr>
  <?php if ($lists) { ?>
  <tr>
    <td colspan="2" class="titleCell" valign="top" align="left"><?php echo $this->params->get('list_desc', 'Lists available to you'); ?>:</td>
  </tr>
  <tr>
    <td width="25%">&nbsp;</td>
    <td width="75%">
      <?php echo $lists; ?>
    </td>
  </tr>
  <?php } ?>
<?php
        $return = ob_get_contents();
        ob_end_clean();
        return $return;
    } // getEditTab()


    /**
     * Saves the user edit tab postdata into the tab's permanent storage
     * @param object tab reflecting the tab database entry
     * @param object mosUser reflecting the user being displayed
     * @param int 1 for front-end, 2 for back-end
     * @param array _POST data for saving edited tab content as generated with getEditTab
     * @returns mixed : either string HTML for tab content, or false if ErrorMSG generated
     */
    function saveEditTab($tab, &$user, $ui, $postdata) {

        $receive_html = mosGetParam($_POST, 'sm2em_receive_html', 0);
        $lists = mosGetParam($_POST, 'sm2em_lists', array());

        // update the table as to whether they receive html or not

        // load the sm2emailmarketingUser object
        $sm2emUser = $this->getSM2EMUser($user->id, true);

        $forceInsert = false;

        $params = $this->params;
        $defaultReceive = $params->get('default_receive', 0);
        if (!empty($defaultReceive) && !empty($receive_html)) $receive_html = $defaultReceive;

        // check for non existing user
        if (empty($sm2emUser->id)) {
            $sm2emUser->id = $user->id;
            $sm2emUser->subscribe_date = date('Y-m-d H:i:s');
            $forceInsert = true;
        }

        if (empty($receive_html)) {
            $sm2emUser->unsubscribe_date = date('Y-m-d H:i:s');
            if (empty($sm2emUser->unsubscribe_reason)) {
                if ($ui==2) {
                    $sm2emUser->unsubscribe_reason = 5;
                } else {
                    $sm2emUser->unsubscribe_reason = 1;
                }
                if (!$forceInsert) {
                    $msgSQL = 'select u.name, u.email, '.$sm2emUser->unsubscribe_reason.' as unsubscribe_reason'
                        .' from #__users u inner join #__emktg_user s on (u.id=s.id) '
                        .' where u.id='.(int)$user->id;
                }
            }
        } else {
            $sm2emUser->receive_html = $receive_html-1;
            $sm2emUser->unsubscribe_date = '0000-00-00 00:00:00';
            $sm2emUser->unsubscribe_reason = 0;
        }

        if (!$sm2emUser->store(false, $forceInsert)) {
            echo "<script> alert('".$user->_db->getErrorMsg()."'); window.history.go(-1); </script>\n";
            exit();
        }

        //update lists (what lists are they subscribed to)
        $sm2emUser->subscribe($lists);

        if (!empty($msgSQL)) {
            sm2emAdminUnsubscribeEmailFromQuery($msgSQL);
        }

        return true;

    } // saveEditTab()


    /**
     * Generates the HTML to display the registration tab/area
     * @param object tab reflecting the tab database entry
     * @param object mosUser reflecting the user being displayed (here null)
     * @param int 1 for front-end, 2 for back-end
     * @return mixed : either string HTML for tab content, or false if ErrorMSG generated
     */
    function getDisplayRegistration($tab, $user, $ui) {
        return $this->getEditTab($tab, $user, $ui);
    } // getDisplayRegistration()


    /**
     * Saves the registration tab/area postdata into the tab's permanent storage
     * @param object tab reflecting the tab database entry
     * @param object mosUser reflecting the user being displayed (here null)
     * @param int 1 for front-end, 2 for back-end
     * @param array _POST data for saving edited tab content as generated with getEditTab
     * @return boolean : either string HTML for tab content, or false if ErrorMSG generated
     */
    function saveRegistrationTab($tab, &$user, $ui, $postdata) {
        return $this->saveEditTab($tab, $user, $ui, $postdata);
    } // saveRegistrationTab()


    /**
     * Called once ConfirmationCode verified and user found and approved by moderator if needed
     * @param object tab reflecting the tab database entry
     * @param object mosUser reflecting the user being displayed
     * @param int 1 for front-end, 2 for back-end
     * @returns mixed : either string HTML for tab content, or false if ErrorMSG generated
     */
    function userActivated($user, $success) {

        if (empty($user->id)) return false;

        if ($success) {
            // update the table as to whether they confirmed
            // load the sm2emailmarketingUser object
            $sm2emuser = $this->getSM2EMUser($user->id, true);

            $sm2emuser->confirmed = $success;

            if (!$sm2emuser->store()) {
                return false;
            }
        }

        return true;
    } // userActivated()


    function userDeleted($user) {
        global $database;

        if (empty($user->id)) return false;

        // delete from queue if not yet processed
        $database->setQuery('DELETE FROM #__emktg_stats_message'
            .' WHERE userid='.(int)$user->id
            .' AND send_date=0');
        if (!$database->query()) return false;

        // delete from the lists
        $database->setQuery('DELETE FROM #__emktg_list_user'
            .' WHERE id='.(int)$user->id);
        if (!$database->query()) return false;

        // delete from the lists
        $database->setQuery('DELETE FROM #__emktg_user '
            .' WHERE id='.(int)$user->id);
        if (!$database->query()) return false;

        return true;

    } // userDeleted()


    /**
     * Check if the SM2 Email marketing component is installed
     */
    function checkSM2EMInstalled() {
        global $mosConfig_absolute_path;

        if (file_exists($mosConfig_absolute_path.'/components/com_sm2emailmarketing')) {
            return true;
        } else {
            return false;
        }
    } // checkSM2EMInstalled


    function findLists($userid, $ui=1){
        global $database, $acl;

        //get list of mailing lists available (public access only)
        //only if lists plugin is turned on
        $database->setQuery('SELECT enabled FROM #__emktg_plugin WHERE filename = '.$database->Quote('list.plugin.php'));
        $showlists = $database->loadResult();
        if (empty($showlists)) return;

        $database->setQuery('SELECT listid AS value FROM #__emktg_list_user WHERE id='.(int)$userid);
        $selected = $database->loadObjectList();

        $lists = array();
        if ($ui==2) {
            //admin - show all lists
            $database->setQuery('SELECT listid AS value, list_name AS text FROM #__emktg_list WHERE published=1');
            //also show lists they are subscribed to (selected)
        } else {
            //registering - show lists only avaialable to public and registered users
            $grp = $acl->getAroGroup($userid);
            $gid = 1;
            if ($grp !== null) {
                if ($acl->is_group_child_of( $grp->name, 'Registered', 'ARO' ) || $acl->is_group_child_of( $grp->name, 'Public Backend', 'ARO' )) {
                    // fudge Authors, Editors, Publishers and Super Administrators into the Special Group
                    $gid = 2;
                }
            }
            $database->setQuery('SELECT listid AS value, list_name AS text FROM #__emktg_list WHERE access<='.(int)$gid.' AND published=1');
        }
        $lists = $database->loadObjectList();
        if (!$lists) {
            return;
        }
        $size = min(count($lists), 10);
        return mosHTML::selectList($lists, 'sm2em_lists[]', 'class="inputbox" multiple size="'.$size.'"', 'value', 'text', $selected);
    }

} // class getSM2EMTab

?>