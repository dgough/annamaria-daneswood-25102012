<?php
// $Id: content.plugin.php,v 1.13 2008/03/31 01:59:10 sm2tony Exp $
/**
 * sm2emailmarketing - Email Marketing Newsletter
 *
 * Joomla! Newsletter Component that supports multiple list mailing and message queueing
 *
 * @package Joomla!
 * @subpackage com_sm2emailmarketing
 * @copyright Copyright (C) 2006 SM2 / All Rights Reserved
 * @license commercial
 * @author SM2 <info@sm2joomla.com>
 */

/**
 * Makes sure this is included by a parent file
 */
defined('_VALID_MOS') or die('Direct Access to this location is not allowed.');

class sm2emailmarketing_plugin_content extends sm2emPluginObj {

    var $name = 'SM2 Email Marketing Content';
    var $filename = 'content.plugin.php';
    var $description = 'Allows SM2 Email Marketing to send messages based on Content Editing';
    var $classname = 'sm2emailmarketing_plugin_content';
    var $fieldPrefix = 'emktg_content';

    /**
     * Constructor
     */
    function sm2emailmarketing_plugin_content() {
    } // sm2emailmarketing_plugin_content()


    /**
     * displayMessageFilters
     *
     * Method to display the fields on the Message Management form
     */
    function displayMessageFilters($messageid, $pluginid, $option) {
        global $database, $acl;

        if (!$this->validatePlugin($pluginid)) return false;

        $params = $this->getParams($pluginid);

        // check to see if atleast one option is selected
        $filterCount = 0;
        $filterCount += $params->get('content_items', 1);
        $filterCount += $params->get('section', 1);
        $filterCount += $params->get('category', 1);
        $filterCount += $params->get('published', 1);
        $filterCount += $params->get('checked_out', 1);
        $filterCount += $params->get('created', 1);
        $filterCount += $params->get('modified', 1);
        $filterCount += $params->get('access', 1);

        if (empty($filterCount)) return false;

        // get the selected lists
        $selected = $this->getMessageData($messageid, $pluginid);

        // start output buffering
        ob_start();
        ?>
        <tr>
          <th colspan="2">
            <?php echo $params->get('section_title', 'Content Management'); ?>
          </th>
        </tr>
        <tr>
          <td colspan="2">
            <?php echo $params->get('section_description'); ?>
          </td>
        </tr>
        <?php

        // show the content items filter
        if ($params->get('content_items', 1)) {
            $this->_displayContent($params, $selected);
        }

        // show the section filter
        if ($params->get('section', 1)) {
            $this->_displaySection($params, $selected);
        }

        // show the category filter
        if ($params->get('category', 1)) {
            $this->_displayCategory($params, $selected);
        }

        // show the published filter
        if ($params->get('published', 1)) {
            $this->_displayPublished($params, $selected);
        }

        // show the checked out filter
        if ($params->get('checked_out', 1)) {
            $this->_displayCheckedOut($params, $selected);
        }

        // show the created filter
        if ($params->get('created', 1)) {
            $this->_displayCreated($params, $selected);
        }

        // show the modified filter
        if ($params->get('modified', 1)) {
            $this->_displayModified($params, $selected);
        }

        // show the access filter
        if ($params->get('access', 1)) {
            $this->_displayAccess($params, $selected);
        }

        $result = ob_get_contents();
        ob_end_clean();
        return $result;

    } // displayMessageFilters()


    function _displayContent($params, $selected) {
        global $database;

        $availContent = array();
        $availContent[] = mosHTML::makeOption('0', '-- Articles --');

        $qrysql = 'SELECT id, title, title_alias FROM #__content'
            .' WHERE (sectionid > 0) AND (catid > 0) AND (state = 1)'
            .' ORDER BY title';
        $database->setQuery($qrysql);
        $content = $database->loadObjectList();

        foreach ($content as $item) {
            $text = $item->title;
            if (!empty($item->title_alias)) {
                $text .= ' ('.$item->title_alias.')';
            }
            $availContent[] = mosHTML::makeOption($item->id, $text);
        }

        $availContent[] = mosHTML::makeOption('0', '-- Static --');

        $qrysql = 'SELECT id, title, title_alias FROM #__content'
            .' WHERE (sectionid = 0) AND (catid = 0) AND (state = 1)'
            .' ORDER BY title';
        $database->setQuery($qrysql);
        $content = $database->loadObjectList();

        foreach ($content as $item) {
            $text = $item->title;
            if (!empty($item->title_alias)) {
                $text .= ' ('.$item->title_alias.')';
            }
            $availContent[] = mosHTML::makeOption($item->id, $text);
        }

        // if there is not content dont display this option
        if (count($availContent)==2) return;

        $size = min(count($availContent), 10);

        $thisSelected = isset($selected['content_items']) ? convertArrayToOptions($selected['content_items']) : null;

        $contentItems = mosHTML::selectList($availContent, $this->fieldPrefix.'[content_items][]', 'class="inputbox" multiple size="'.$size.'" style="width:95%;"', 'value', 'text', $thisSelected);
        $contentItems = preg_replace('/<option value="0"[^>]*>([^<]*)<\/option>/', '<optgroup label="$1" />', $contentItems);

        $title = $params->get('content_items_title', 'Content Items');
        $desc = $params->get('content_items_description');
        if (!empty($desc)) {
            $title = '<span class="editlinktip">'.mosToolTip(addslashes($desc), $title, '', '', $title, '#', 0).'</span>';
        }
        ?>
        <tr>
          <td valign="top"><?php echo $title; ?></td>
          <td valign="top"><?php echo $contentItems; ?></td>
        </tr>
        <?php
    } // _displayContent()


    function _displaySection($params, $selected) {
        global $database;

        $thisSelected = isset($selected['sections']) ? convertArrayToOptions($selected['sections']) : null;

        $sections = mosAdminMenus::SelectSection($this->fieldPrefix.'[section][]', $thisSelected, ' multiple style="width:95%;"');
        $size = min(substr_count($sections, '</option>'),10);
        $sections = str_replace('size="1"', 'size="'.$size.'"', $sections);
        if (defined('_JLEGACY')) {
            $sections = preg_replace('/<option value="-1"[^\>]*>([^<]*)<\/option>/iu', '<optgroup label="$1" />', $sections);
        } else {
            $sections = preg_replace('/<option value="0"[^\>]*>([^<]*)<\/option>/iu', '<optgroup label="$1" />', $sections);
        }

        $title = $params->get('sections_title', 'Sections');
        $desc = $params->get('sections_description');
        if (!empty($desc)) {
            $title = '<span class="editlinktip">'.mosToolTip(addslashes($desc), $title, '', '', $title, '#', 0).'</span>';
        }
        ?>
        <tr>
          <td valign="top"><?php echo $title; ?></td>
          <td valign="top"><?php echo $sections; ?></td>
        </tr>
        <?php
    } // _displaySection()


    function _displayCategory($params, $selected) {
        global $database;

        $database->setQuery('SELECT cc.id AS value, '
            .'CONCAT(s.title, '.$database->Quote(' / ').', cc.title) AS text'
            .' FROM #__categories AS cc'
            .' INNER JOIN #__sections AS s ON s.id = cc.section'
            .' ORDER BY s.ordering, cc.ordering');
        $categories = $database->loadObjectList();

        array_unshift($categories, mosHTML::makeOption('-1', '-- Select a Category --'));

        $size = min(count($categories), 10);

        $thisSelected = isset($selected['category']) ? convertArrayToOptions($selected['category']) : null;

        $categories = mosHTML::selectList($categories, $this->fieldPrefix.'[category][]', 'class="inputbox" multiple size="'.$size.'" style="width:95%;"', 'value', 'text', $thisSelected);
        $categories = preg_replace('/<option value="-1"[^>]*>([^<]*)<\/option>/iu', '<optgroup label="$1" />', $categories);

        $title = $params->get('category_title', 'Categories');
        $desc = $params->get('category_description');
        if (!empty($desc)) {
            $title = '<span class="editlinktip">'.mosToolTip(addslashes($desc), $title, '', '', $title, '#', 0).'</span>';
        }
        ?>
        <tr>
          <td valign="top"><?php echo $title; ?></td>
          <td valign="top"><?php echo $categories; ?></td>
        </tr>
        <?php
    } // _displayCategory()


    function _displayPublished($params, $selected) {
        global $database;

        $published = array(
            mosHTML::makeOption('', ''),
            mosHTML::makeOption('-1', _CMN_ARCHIVE),
            mosHTML::makeOption('0', _CMN_UNPUBLISHED),
            mosHTML::makeOption('1', _CMN_PUBLISHED)
        );

        $thisSelected = isset($selected['published']) ? $selected['published'] : null;

        $published = mosHTML::selectList($published, $this->fieldPrefix.'[published]', 'class="inputbox"', 'value', 'text', $thisSelected);

        $title = $params->get('published_title', 'Published');
        $desc = $params->get('published_description');
        if (!empty($desc)) {
            $title = '<span class="editlinktip">'.mosToolTip(addslashes($desc), $title, '', '', $title, '#', 0).'</span>';
        }
        ?>
        <tr>
          <td valign="top"><?php echo $title; ?></td>
          <td valign="top"><?php echo $published; ?></td>
        </tr>
        <?php
    } // _displayPublished()


    function _displayCheckedOut($params, $selected) {
        global $database;

        $checkedOut = array(
            mosHTML::makeOption('', ''),
            mosHTML::makeOption('0', _CMN_NO),
            mosHTML::makeOption('1', _CMN_YES)
        );

        $thisSelected = isset($selected['checked_out']) ? $selected['checked_out'] : null;

        $checkedOut = mosHTML::selectList($checkedOut, $this->fieldPrefix.'[checked_out]', 'class="inputbox"', 'value', 'text', $thisSelected);

        $title = $params->get('checked_out_title', 'Checked Out');
        $desc = $params->get('checked_out_description');
        if (!empty($desc)) {
            $title = '<span class="editlinktip">'.mosToolTip(addslashes($desc), $title, '', '', $title, '#', 0).'</span>';
        }
        ?>
        <tr>
          <td valign="top"><?php echo $title; ?></td>
          <td valign="top"><?php echo $checkedOut; ?></td>
        </tr>
        <?php
    } // _displayCheckedOut()


    function _displayCreated($params, $selected) {

        $title = $params->get('created_title', 'Creation Date');
        $desc = $params->get('created_description');
        if (!empty($desc)) {
            $title = '<span class="editlinktip">'.mosToolTip(addslashes($desc), $title, '', '', $title, '#', 0).'</span>';
        }

        if (!isset($selected['created']['start'])) $selected['created']['start'] = null;
        if (!isset($selected['created']['end'])) $selected['created']['end'] = null;

        ?>
        <tr>
          <td valign="top"><?php echo $title; ?></td>
          <td valign="top">
            <table>
              <tr>
                <td><?php echo $params->get('created_from', 'From'); ?></td>
                <td>
                  <input class="text_area" type="text" name="<?php echo $this->fieldPrefix; ?>[created][start]" id="<?php echo $this->fieldPrefix; ?>_created_start" size="25" maxlength="19" value="<?php echo $selected['created']['start']; ?>" />
                  <input name="reset" type="reset" class="button" onClick="return showCalendar('<?php echo $this->fieldPrefix; ?>_created_start', 'y-mm-dd');" value="...">
                </td>
              </tr>
              <tr>
                <td><?php echo $params->get('created_to', 'To'); ?></td>
                <td>
                  <input class="text_area" type="text" name="<?php echo $this->fieldPrefix; ?>[created][end]" id="<?php echo $this->fieldPrefix; ?>_created_end" size="25" maxlength="19" value="<?php echo $selected['created']['end']; ?>" />
                  <input name="reset" type="reset" class="button" onClick="return showCalendar('<?php echo $this->fieldPrefix; ?>_created_end', 'y-mm-dd');" value="...">
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <?php
    } // _displayCreated()


    function _displayModified($params, $selected) {

        $title = $params->get('modified_title', 'Modification Date');
        $desc = $params->get('modified_description');
        if (!empty($desc)) {
            $title = '<span class="editlinktip">'.mosToolTip(addslashes($desc), $title, '', '', $title, '#', 0).'</span>';
        }

        if (!isset($selected['modified']['start'])) $selected['modified']['start'] = null;
        if (!isset($selected['modified']['end'])) $selected['modified']['end'] = null;

        ?>
        <tr>
          <td valign="top"><?php echo $title; ?></td>
          <td valign="top">
            <table>
              <tr>
                <td><?php echo $params->get('modified_from', 'From'); ?></td>
                <td>
                  <input class="text_area" type="text" name="<?php echo $this->fieldPrefix; ?>[modified][start]" id="<?php echo $this->fieldPrefix; ?>_modified_start" size="25" maxlength="19" value="<?php echo $selected['modified']['start']; ?>" />
                  <input name="reset" type="reset" class="button" onClick="return showCalendar('<?php echo $this->fieldPrefix; ?>_modified_start', 'y-mm-dd');" value="...">
                </td>
              </tr>
              <tr>
                <td><?php echo $params->get('modified_to', 'To'); ?></td>
                <td>
                  <input class="text_area" type="text" name="<?php echo $this->fieldPrefix; ?>[modified][end]" id="<?php echo $this->fieldPrefix; ?>_modified_end" size="25" maxlength="19" value="<?php echo $selected['modified']['end']; ?>" />
                  <input name="reset" type="reset" class="button" onClick="return showCalendar('<?php echo $this->fieldPrefix; ?>_modified_end', 'y-mm-dd');" value="...">
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <?php
    } // _displayModified()


    function _displayAccess($params, $selected) {
        global $database;

        $access = array(
            mosHTML::makeOption('', ''),
            mosHTML::makeOption('0', 'Public'),
            mosHTML::makeOption('1', 'Registered'),
            mosHTML::makeOption('2', 'Special')
        );

        $thisSelected = isset($selected['access']) ? $selected['access'] : null;

        $access = mosHTML::selectList($access, $this->fieldPrefix.'[access]', 'class="inputbox"', 'value', 'text', $thisSelected);

        $title = $params->get('access_title', 'Access');
        $desc = $params->get('access_description');
        if (!empty($desc)) {
            $title = '<span class="editlinktip">'.mosToolTip(addslashes($desc), $title, '', '', $title, '#', 0).'</span>';
        }
        ?>
        <tr>
          <td valign="top"><?php echo $title; ?></td>
          <td valign="top"><?php echo $access; ?></td>
        </tr>
        <?php
    } // _displayAccess()


    /**
     * processSend
     *
     * Method to process a plugin send
     * Retrieves all of the users
     * Returns an array of name,email objects
     */
    function processSend($messageid, $pluginid, $option) {
        global $database;

        if (!$this->validatePlugin($pluginid)) return false;

        $params = $this->getParams($pluginid);

        // determine which users to send to
        $pluginData = $this->getMessageData($messageid, $pluginid);

        if (empty($pluginData)) return false;

        $return = false;

        // determine the tablename to process into
        $tablename = determineQueueProcessTable();

        // process the content_items filter
        if ($params->get('content_items', 1)) {
            if (!empty($pluginData['content_items'])) {
                // find the users who created the content items
                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT DISTINCT '.$messageid.', u.id, '.$pluginid.', u.name, u.email, eu.receive_html'
                    .' FROM #__users u'
                	.' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' INNER JOIN #__content c ON eu.id = c.created_by'
                    .' WHERE (c.id IN ('.implode(', ', $pluginData['content_items']).'))'
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY u.name');
                $database->query();

                // find the users who modified the content items
                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' from #__users u'
                    .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' INNER JOIN #__content c ON eu.id = c.modified_by'
                    .' WHERE (c.id IN ('.implode(', ', $pluginData['content_items']).'))'
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY u.name');
                $database->query();
                $return = true;
            }
        }

        // process the section filter
        if ($params->get('section', 1)) {
            if (!empty($pluginData['section'])) {
                // find the users who created the content items
                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' from #__users u'
                    .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' INNER JOIN #__content c ON eu.id = c.created_by'
                    .' WHERE (c.sectionid IN ('.implode(', ', $pluginData['section']).'))'
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY u.name');
                $database->query();

                // find the users who modified the content items
                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' from #__users u'
                    .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' INNER JOIN #__content c ON eu.id = c.modified_by'
                    .' WHERE (c.sectionid IN ('.implode(', ', $pluginData['section']).')) '
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY u.name');
                $database->query();
                $return = true;
            }
        }

        // process the category filter
        if ($params->get('category', 1)) {
            if (!empty($pluginData['category'])) {
                // find the users who created the content items
                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' from #__users u'
                    .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' INNER JOIN #__content c ON eu.id = c.created_by '
                    .' WHERE (c.catid IN ('.implode(', ', $pluginData['category']).'))'
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY u.name');
                $database->query();

                // find the users who modified the content items
                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' from #__users u'
                    .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' INNER JOIN #__content c ON eu.id = c.modified_by '
                    .' WHERE (c.catid IN ('.implode(', ', $pluginData['category']).'))'
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY u.name');
                $database->query();
                $return = true;
            }
        }

        // process the published filter
        if ($params->get('published', 1)) {
            if (isset($pluginData['published']) && is_numeric($pluginData['published'])) {
                // find the users who created the content items
                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' from #__users u'
                    .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' INNER JOIN #__content c ON eu.id = c.created_by'
                    .' WHERE (c.state = '.$pluginData['published'].')'
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY u.name');
                $database->query();

                // find the users who modified the content items
                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' from #__users u'
                    .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' INNER JOIN #__content c ON eu.id = c.modified_by '
                    .' WHERE (c.state = '.$pluginData['published'].')'
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY u.name');
                $database->query();
                $return = true;
            }
        }

        // process the Checked Out filter
        if ($params->get('checked_out', 1)) {
            if (isset($pluginData['checked_out']) && is_numeric($pluginData['checked_out'])) {
                $where = '';
                if ($pluginData['checked_out']==0) {
                    $where = ' WHERE (c.checked_out = 0)';
                } elseif ($pluginData['checked_out']==1) {
                    $where = ' WHERE (c.checked_out > 0)';
                }

                if (!empty($where)) {
                    // find the users who created the content items
                    $database->setQuery('INSERT IGNORE INTO '.$tablename
                        .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                        .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                        .' from #__users u'
                        .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                        .' INNER JOIN #__content c ON eu.id = c.created_by '
                        .$where
                        .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                        .' ORDER BY u.name');
                    $database->query();

                    // find the users who modified the content items
                    $database->setQuery('INSERT IGNORE INTO '.$tablename
                        .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                        .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                        .' from #__users u'
                        .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                        .' INNER JOIN #__content c ON eu.id = c.modified_by '
                        .$where
                        .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                        .' ORDER BY u.name');
                    $database->query();
                }
                $return = true;
            }
        }

        // process the created filter
        if ($params->get('created', 1)) {
            $start = $pluginData['created']['start'];
            $end = $pluginData['created']['end'];

            // determine how to find the data
            if (!empty($start) && !empty($end)) {
                // fix nevers
                if (strtolower($start)=='never') $start = '0000-00-00';
                if (strtolower($end)=='never') $end = '0000-00-00';

                // fix dates if reversed
                if ($end < $start) {
                    $temp = $start;
                    $start = $end;
                    $end = $temp;
                }
                $where = '';

                if ($start==$end) {
                    $end = explode('-', $start);
                    $end = mktime(0, 0, 0, $end[1], $end[2]+1, $end[0]);
                    $end = date('Y-m-d', $end);
                    $where = ' WHERE (c.created BETWEEN '
                        .$database->Quote($start).' AND '
                        .$database->Quote($end).')';
                } else if (empty($start)) {
                    $where = ' WHERE (c.created <= '
                        .$database->Quote($end).')';
                } else if (empty($end)) {
                    $where = ' WHERE (c.created >= '
                        .$database->Quote($start).')';
                } else {
                    $where = ' WHERE (c.created BETWEEN '
                        .$database->Quote($start).' AND '
                        .$database->Quote($end).')';
                }

                if (!empty($where)) {
                    // find the creators
                    $database->setQuery('INSERT IGNORE INTO '.$tablename
                        .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                        .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                        .' from #__users u'
                        .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                        .' INNER JOIN #__content c ON eu.id = c.created_by '
                        .$where
                        .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                        .' ORDER BY name');
                    $database->query();

                    // find the modifiers
                    $database->setQuery('INSERT IGNORE INTO '.$tablename
                        .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                        .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                        .' from #__users u'
                        .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                        .' INNER JOIN #__content c ON eu.id = c.modified_by '
                        .$where
                        .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                        .' ORDER BY name');
                    $database->query();
                    $return = true;
                }
            }
        }


        // process the modified filter
        if ($params->get('modified', 1)) {
            $start = $pluginData['modified']['start'];
            $end = $pluginData['modified']['end'];

            // determine how to find the data
            if (!empty($start) || !empty($end)) {
                // fix nevers
                if (strtolower($start)=='never') $start = '0000-00-00';
                if (strtolower($end)=='never') $end = '0000-00-00';

                // fix dates if reversed
                if ($end < $start) {
                    $temp = $start;
                    $start = $end;
                    $end = $temp;
                }
                $where = '';

                if ($start==$end) {
                    $where = ' WHERE (c.modified = '
                        .$database->Quote($start).')';
                } else if (empty($start)) {
                    $where = ' WHERE (c.modified <= '
                        .$database->Quote($end).')';
                } else if (empty($end)) {
                    $where = ' WHERE (c.modified >= '
                        .$database->Quote($start).')';
                } else {
                    $where = ' WHERE (c.modified BETWEEN '
                        .$database->Quote($start).' AND '
                        .$database->Quote($end).')';
                }

                if (!empty($where)) {
                    // find the creators
                    $database->setQuery('INSERT IGNORE INTO '.$tablename
                        .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                        .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                        .' from #__users u'
                        .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                        .' INNER JOIN #__content c ON eu.id = c.created_by '
                        .$where
                        .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                        .' ORDER BY name');
                    $database->query();

                    // find the modifiers
                    $database->setQuery('INSERT IGNORE INTO '.$tablename
                        .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                        .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                        .' from #__users u'
                        .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                        .' INNER JOIN #__content c ON eu.id = c.modified_by '
                        .$where
                        .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                        .' ORDER BY name');
                    $database->query();
                    $return = true;
                }
            }
        }

        // process the access filter
        if ($params->get('access', 1)) {
            if (isset($pluginData['access']) && is_numeric($pluginData['access'])) {
                // find the users who created the content items
                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' from #__users u'
                    .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' INNER JOIN #__content c ON eu.id = c.created_by '
                    .' WHERE (c.access = '.$pluginData['access'].')'
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY u.name');
                $database->query();

                // find the users who modified the content items
                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' from #__users u'
                    .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' INNER JOIN #__content c ON eu.id = c.modified_by '
                    .' WHERE (c.state = '.$pluginData['access'].')'
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY u.name');
                $database->query();
                $return = true;
            }
        }

        return $return;

    } // processSend()

} // class sm2emailmarketing_plugin_content

$obj = new sm2emailmarketing_plugin_content();
return $obj;
?>