<?php
// $Id: docman.plugin.php,v 1.14 2008/03/31 01:59:10 sm2tony Exp $
/**
 * sm2emailmarketing - Email Marketing Newsletter
 *
 * Joomla! Newsletter Component that supports multiple list mailing and message queueing
 *
 * @package Joomla!
 * @subpackage com_sm2emailmarketing
 * @copyright Copyright (C) 2006 SM2 / All Rights Reserved
 * @license commercial
 * @author SM2 <info@sm2joomla.com>
 */

/**
 * Makes sure this is included by a parent file
 */
defined('_VALID_MOS') or die('Direct Access to this location is not allowed.');

class sm2emailmarketing_plugin_DOCMan extends sm2emPluginObj {

    var $name = 'DOCMan';
    var $filename = 'docman.plugin.php';
    var $description = 'Allows SM2 Email Marketing to send messages based on DOCMan documents.';
    var $classname = 'sm2emailmarketing_plugin_DOCMan';
    var $fieldPrefix = 'DOCMan';

    /**
     * Constructor
     */
    function sm2emailmarketing_plugin_DOCMan() {
    } // sm2emailmarketing_plugin_DOCMan()


    /**
     * validatePlugin
     *
     * Method to validate if the plugin is usable
     */
    function validatePlugin($pluginid=null) {
        global $mosConfig_absolute_path;

        if (file_exists($mosConfig_absolute_path.'/administrator/components/com_docman')) {
            return true;
        } else {
            return false;
        }
    } // validatePlugin()

    /**
     * displayMessageEdit
     *
     * Method to display the fields on the Message Management form
     */
    function displayMessageFilters($messageid, $pluginid, $option) {
        global $database, $acl;

        if (!$this->validatePlugin($pluginid)) return false;

        $params = $this->getParams($pluginid);

        // check to see if atleast one option is selected
        $filterCount = 0;
        $filterCount += $params->get('groups', 1);
        $filterCount += $params->get('download_document', 1);
        $filterCount += $params->get('download_category', 1);
        $filterCount += $params->get('download_licence', 1);
        $filterCount += $params->get('upload_document', 1);
        $filterCount += $params->get('upload_category', 1);
        $filterCount += $params->get('upload_licence', 1);
        $filterCount += $params->get('upload_checked_out', 1);
        $filterCount += $params->get('upload_date', 1);

        if (empty($filterCount)) return false;

        // get the selected lists
        $selected = $this->getMessageData($messageid, $pluginid);

        // start output buffering
        ob_start();
        ?>
        <tr>
          <th colspan="2">
            <?php echo $params->get('section_title', 'DOCMan'); ?>
          </th>
        </tr>
        <tr>
          <td colspan="2">
            <?php echo $params->get('section_description'); ?>
          </td>
        </tr>
        <?php

        // display the groups filter
        if ($params->get('groups', 1)) {
            $this->_displayFilter('groups', 'Groups',
                'SELECT groups_id AS value, groups_name AS text FROM #__docman_groups ORDER BY groups_name',
                $params, $selected);
        }

        // display the download document filter
        if ($params->get('download_document', 1)) {
            $this->_displayFilter('download_document', 'Download Document',
                'SELECT id AS value, dmname AS text FROM #__docman ORDER BY dmname',
                $params, $selected);
        }

        // display the download category filter
        if ($params->get('download_category', 1)) {
            $this->_displayCategory('download_category', 'Download Category',
                $params, $selected);
        }

        // display the download licence filter
        if ($params->get('download_licence', 1)) {
            $this->_displayFilter('download_licence', 'Download Licence',
                'SELECT id AS value, name AS text FROM #__docman_licenses ORDER BY name',
                $params, $selected);
        }

        // display the upload document filter
        if ($params->get('upload_document', 1)) {
            $this->_displayFilter('upload_document', 'Upload Document',
                'SELECT id AS value, dmname AS text FROM #__docman ORDER BY dmname',
                $params, $selected);
        }

        // display the upload category filter
        if ($params->get('upload_category', 1)) {
            $this->_displayCategory('upload_category', 'Upload Category',
                $params, $selected);
        }

        // display the upload licence filter
        if ($params->get('upload_licence', 1)) {
            $this->_displayFilter('upload_licence', 'Upload Licence',
                'SELECT id AS value, name AS text FROM #__docman_licenses ORDER BY name',
                $params, $selected);
        }

        // display the upload checked out filter
        if ($params->get('upload_checked_out', 1)) {
            $this->_displayCheckedOut($params, $selected);
        }

        // display the upload date filter
        if ($params->get('upload_date', 1)) {
            $this->_displayUploadDate($params, $selected);
        }

        $result = ob_get_contents();
        ob_end_clean();
        return $result;

    } // displayMessageFilters()


    function _displayFilter($filter, $filterTitle, $sql, $params, $selected) {
        global $database;

        $title = $params->get($filter.'_title', $filterTitle);
        $desc = $params->get($filter.'_description');
        if (!empty($desc)) {
            $title = '<span class="editlinktip">'.mosToolTip(addslashes($desc), $title, '', '', $title, '#', 0).'</span>';
        }

        $database->setQuery($sql);
        $options = $database->loadObjectList();

        array_unshift($options, mosHTML::makeOption('-1', '-- '.$title.' --'));

        $size = min(count($options), 10);

        $thisSelected = isset($selected[$filter]) ? convertArrayToOptions($selected[$filter]) : null;

        $list = mosHTML::selectList($options, $this->fieldPrefix.'['.$filter.'][]', 'class="inputbox" multiple size="'.$size.'" style="width:95%;"', 'value', 'text', $thisSelected);
        $list = preg_replace('/<option value="-1"[^>]*>([^<]*)<\/option>/iu', '<optgroup label="$1" />', $list);
        ?>
        <tr>
          <td valign="top"><?php echo $title; ?></td>
          <td valign="top"><?php echo $list; ?></td>
        </tr>
        <?php
    } // _displayFilter()


    function _displayCategory($filter, $filterTitle, $params, $selected) {
        global $database, $mosConfig_absolute_path;

        $title = $params->get($filter.'_title', $filterTitle);
        $desc = $params->get($filter.'_description');
        if (!empty($desc)) {
            $title = '<span class="editlinktip">'.mosToolTip(addslashes($desc), $title, '', '', $title, '#', 0).'</span>';
        }

        require_once($mosConfig_absolute_path.'/administrator/components/com_docman/classes/DOCMAN_utils.class.php');
        // make sure the $_DMUSER object exists for the categoryArray to work correctly
        $GLOBALS['_DMUSER'] = new stdClass();
        $GLOBALS['_DMUSER']->gid = $GLOBALS['my']->gid;
        @$list = DOCMAN_utils::categoryArray();
        // cleanup the $_DMUSER since we dont need it any more
        unset($GLOBALS['_DMUSER']);
        // assemble menu items to the array
        $options = array();
        foreach ($list as $item) {
            $options[] = mosHTML::makeOption($item->id, $item->treename);
        }
        array_unshift($options, mosHTML::makeOption('-1', '-- '.$title.' --'));

        $size = min(count($options), 10);

        $thisSelected = isset($selected[$filter]) ? convertArrayToOptions($selected[$filter]) : null;

        $list = mosHTML::selectList($options, $this->fieldPrefix.'['.$filter.'][]', 'class="inputbox" multiple size="'.$size.'" style="width:95%;"', 'value', 'text', $thisSelected);
        $list = preg_replace('/<option value="-1"[^>]*>([^<]*)<\/option>/iu', '<optgroup label="$1" />', $list);
        ?>
        <tr>
          <td valign="top"><?php echo $title; ?></td>
          <td valign="top"><?php echo $list; ?></td>
        </tr>
        <?php
    } // _displayCategory()


    function _displayCheckedOut($params, $selected) {
        global $database;

        $checkedOut = array(
            mosHTML::makeOption('', ''),
            mosHTML::makeOption('0', _CMN_NO),
            mosHTML::makeOption('1', _CMN_YES)
        );

        $thisSelected = isset($selected['upload_checked_out']) ? $selected['upload_checked_out'] : null;

        $checkedOut = mosHTML::selectList($checkedOut, $this->fieldPrefix.'[upload_checked_out]', 'class="inputbox"', 'value', 'text', $thisSelected);

        $title = $params->get('upload_checked_out_title', 'Checked Out');
        $desc = $params->get('upload_checked_out_description');
        if (!empty($desc)) {
            $title = '<span class="editlinktip">'.mosToolTip(addslashes($desc), $title, '', '', $title, '#', 0).'</span>';
        }
        ?>
        <tr>
          <td valign="top"><?php echo $title; ?></td>
          <td valign="top"><?php echo $checkedOut; ?></td>
        </tr>
        <?php
    } // _displayCheckedOut()


    function _displayUploadDate($params, $selected) {

        $title = $params->get('upload_date_title', 'Upload Date');
        $desc = $params->get('upload_date_description');
        if (!empty($desc)) {
            $title = '<span class="editlinktip">'.mosToolTip(addslashes($desc), $title, '', '', $title, '#', 0).'</span>';
        }

        if (!isset($selected['upload_date']['start'])) $selected['upload_date']['start'] = null;
        if (!isset($selected['upload_date']['end'])) $selected['upload_date']['end'] = null;
        ?>
        <tr>
          <td valign="top"><?php echo $title; ?></td>
          <td valign="top">
            <table>
              <tr>
                <td><?php echo $params->get('upload_date_from', 'From'); ?></td>
                <td>
                  <input class="text_area" type="text" name="<?php echo $this->fieldPrefix; ?>[upload_date][start]" id="<?php echo $this->fieldPrefix; ?>_upload_date_start" size="25" maxlength="19" value="<?php echo $selected['upload_date']['start']; ?>" />
                  <input name="reset" type="reset" class="button" onClick="return showCalendar('<?php echo $this->fieldPrefix; ?>_upload_date_start', 'y-mm-dd');" value="...">
                </td>
              </tr>
              <tr>
                <td><?php echo $params->get('upload_date_to', 'To'); ?></td>
                <td>
                  <input class="text_area" type="text" name="<?php echo $this->fieldPrefix; ?>[upload_date][end]" id="<?php echo $this->fieldPrefix; ?>_upload_date_end" size="25" maxlength="19" value="<?php echo $selected['upload_date']['end']; ?>" />
                  <input name="reset" type="reset" class="button" onClick="return showCalendar('<?php echo $this->fieldPrefix; ?>_upload_date_end', 'y-mm-dd');" value="...">
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <?php
    } // _displayUploadDate()


    /**
     * processSend
     *
     * Method to process a plugin send
     * Retrieves all of the users
     * Returns an array of name,email objects
     */
    function processSend($messageid, $pluginid, $option) {
        global $database;

        if (!$this->validatePlugin($pluginid)) return false;

        $params = $this->getParams($pluginid);

        // determine which users to send to
        $pluginData = $this->getMessageData($messageid, $pluginid);

        if (empty($pluginData)) return false;

        $return = false;

        // determine the tablename to process into
        $tablename = determineQueueProcessTable();

        // process the groups
        if ($params->get('groups', 1)) {
            if (!empty($pluginData['groups'])) {
                // find the users who have this group
                $database->setQuery('SELECT groups_members FROM #__docman_groups'
                    .' WHERE groups_id IN ('.implode(',', $pluginData['groups']).')');
                $groupMembers = $database->loadResultArray();
                $groupMembers = implode(',', $groupMembers);
                $groupMembers = explode(',', $groupMembers);
                $groupMembers = array_unique($groupMembers);

                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' from #__users u'
                    .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' WHERE (eu.id IN ('.implode(',', $groupMembers).'))'
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY u.name');
                $database->query();
                $return = true;
            }
        }

        // process the download document filter
        if ($params->get('download_document', 1)) {
            if (!empty($pluginData['download_document'])) {
                // find the users who have downloaded these documents
                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' from #__users u'
                    .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' INNER JOIN #__docman_log l on eu.id=l.log_user'
                    .' WHERE (l.log_docid IN ('.implode(', ', $pluginData['download_document']).'))'
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY u.name');
                $database->query();
                $return = true;
            }
        }

        // process the download category filter
        if ($params->get('download_category', 1)) {
            if (!empty($pluginData['download_category'])) {
                // find the users who have downloaded these documents
                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' from #__users u'
                    .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' INNER JOIN #__docman_log l ON eu.id=l.log_user'
                    .' INNER JOIN #__docman d ON l.log_docid=d.id'
                    .' WHERE (d.catid IN ('.implode(', ', $pluginData['download_category']).'))'
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY u.name');
                $database->query();
                $return = true;
            }
        }

        // process the download licence filter
        if ($params->get('download_licence', 1)) {
            if (!empty($pluginData['download_licence'])) {
                // find the users who have downloaded these documents
                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' from #__users u'
                    .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' INNER JOIN #__docman_log l ON eu.id=l.log_user'
                    .' INNER JOIN #__docman d ON l.log_docid=d.id'
                    .' WHERE (d.dmlicense_id IN ('.implode(', ', $pluginData['download_licence']).'))'
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY u.name');
                $database->query();
                $return = true;
            }
        }

        // process the upload document filter
        if ($params->get('upload_document', 1)) {
            if (!empty($pluginData['upload_document'])) {
                // find the users who have upload these documents
                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' from #__users u'
                    .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' INNER JOIN #__docman d ON eu.id=d.dmowner'
                    .' WHERE (d.id IN ('.implode(', ', $pluginData['upload_document']).'))'
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY u.name');
                $database->query();

                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' from #__users u'
                    .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' INNER JOIN #__docman d ON eu.id=d.dmlastupdateby'
                    .' WHERE (d.id IN ('.implode(', ', $pluginData['upload_document']).'))'
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY u.name');
                $database->query();

                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' from #__users u'
                    .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' INNER JOIN #__docman d ON eu.id=d.dmsubmitedby'
                    .' WHERE (d.id IN ('.implode(', ', $pluginData['upload_document']).'))'
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY u.name');
                $database->query();

                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' from #__users u'
                    .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' INNER JOIN #__docman d ON eu.id=d.dmmantainedby'
                    .' WHERE (d.id IN ('.implode(', ', $pluginData['upload_document']).'))'
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY u.name');
                $database->query();
                $return = true;
            }
        }

        // process the upload category filter
        if ($params->get('upload_category', 1)) {
            if (!empty($pluginData['upload_category'])) {
                // find the users who have upload these documents
                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' from #__users u'
                    .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' INNER JOIN #__docman d ON eu.id=d.dmowner'
                    .' WHERE (d.catid IN ('.implode(', ', $pluginData['upload_category']).'))'
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY u.name');
                $database->query();

                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' from #__users u'
                    .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' INNER JOIN #__docman d ON eu.id=d.dmlastupdateby'
                    .' WHERE (d.catid IN ('.implode(', ', $pluginData['upload_category']).'))'
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY u.name');
                $database->query();

                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' from #__users u'
                    .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' INNER JOIN #__docman d ON eu.id=d.dmsubmitedby'
                    .' WHERE (d.catid in ('.implode(', ', $pluginData['upload_category']).'))'
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY u.name');
                $database->query();

                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' from #__users u'
                    .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' INNER JOIN #__docman d ON eu.id=d.dmmantainedby'
                    .' WHERE (d.catid IN ('.implode(', ', $pluginData['upload_category']).'))'
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY u.name');
                $database->query();
                $return = true;
            }
        }

        // process the upload licence filter
        if ($params->get('upload_licence', 1)) {
            if (!empty($pluginData['upload_licence'])) {
                // find the users who have upload these documents
                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' from #__users u'
                    .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' INNER JOIN #__docman d ON eu.id=d.dmowner'
                    .' WHERE (d.dmlicense_id IN ('.implode(', ', $pluginData['upload_licence']).'))'
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY u.name');
                $database->query();

                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' from #__users u'
                    .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' INNER JOIN #__docman d ON eu.id=d.dmlastupdateby'
                    .' WHERE (d.dmlicense_id IN ('.implode(', ', $pluginData['upload_licence']).'))'
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY u.name');
                $database->query();

                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' from #__users u'
                    .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' INNER JOIN #__docman d ON eu.id=d.dmsubmitedby'
                    .' WHERE (d.dmlicense_id in ('.implode(', ', $pluginData['upload_licence']).'))'
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY u.name');
                $database->query();

                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' from #__users u'
                    .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' INNER JOIN #__docman d ON eu.id=d.dmmantainedby'
                    .' WHERE (d.dmlicense_id IN ('.implode(', ', $pluginData['upload_licence']).'))'
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY u.name');
                $database->query();
                $return = true;
            }
        }

        // process the Checked Out filter
        if ($params->get('checked_out', 1)) {
            if (isset($pluginData['checked_out']) && is_numeric($pluginData['checked_out'])) {
                $where = '';
                if ($pluginData['checked_out']==0) {
                    $where = ' WHERE (d.checked_out = 0)';
                } elseif ($pluginData['checked_out']==1) {
                    $where = ' WHERE (d.checked_out > 0)';
                }

                if (!empty($where)) {
                    // find the users who created the content items
                    $database->setQuery('INSERT IGNORE INTO '.$tablename
                        .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                        .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                        .' from #__users u'
                        .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                        .' INNER JOIN #__docman d ON eu.id=d.dmowner'
                        .$where
                        .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                        .' ORDER BY u.name');
                    $database->query();

                    $database->setQuery('INSERT IGNORE INTO '.$tablename
                        .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                        .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                        .' from #__users u'
                        .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                        .' INNER JOIN #__docman d ON eu.id=d.dmlastupdateby'
                        .$where
                        .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                        .' ORDER BY u.name');
                    $database->query();

                    $database->setQuery('INSERT IGNORE INTO '.$tablename
                        .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                        .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                        .' from #__users u'
                        .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                        .' INNER JOIN #__docman d ON eu.id=d.dmsubmitedby'
                        .$where
                        .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                        .' ORDER BY u.name');
                    $database->query();

                    $database->setQuery('INSERT IGNORE INTO '.$tablename
                        .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                        .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                        .' from #__users u'
                        .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                        .' INNER JOIN #__docman d ON eu.id=d.dmmantainedby'
                        .$where
                        .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                        .' ORDER BY u.name');
                    $database->query();
                }
                $return = true;
            }
        }

        // process the upload date filter
        if ($params->get('upload_date', 1)) {
            $start = $pluginData['upload_date']['start'];
            $end = $pluginData['upload_date']['end'];

            // determine how to find the data
            if (!empty($start) || !empty($end)) {
                // fix nevers
                if (strtolower($start)=='never') $start = '0000-00-00';
                if (strtolower($end)=='never') $end = '0000-00-00';

                // fix dates if reversed
                if ($end < $start) {
                    $temp = $start;
                    $start = $end;
                    $end = $temp;
                }
                $where = '';

                if ($start==$end) {
                    $end = explode('-', $start);
                    $end = mktime(0, 0, 0, $end[1], $end[2]+1, $end[0]);
                    $end = date('Y-m-d', $end);
                    $where = ' WHERE (d.dmlastupdateon BETWEEN '
                        .$database->Quote($start).' AND '
                        .$database->Quote($end).')';
                } else if (empty($start)) {
                    $where = ' WHERE (d.dmlastupdateon <= '
                        .$database->Quote($end).')';
                } else if (empty($end)) {
                    $where = ' WHERE (d.dmlastupdateon >= '
                        .$database->Quote($start).')';
                } else {
                    $where = ' WHERE (d.dmlastupdateon BETWEEN '
                        .$database->Quote($start).' AND '
                        .$database->Quote($end).')';
                }

                if (!empty($where)) {
                    // find the users who have orders created that match the order date filter
                    $database->setQuery('INSERT IGNORE INTO '.$tablename
                        .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                        .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                        .' from #__users u'
                        .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                        .' INNER JOIN #__docman d ON eu.id=d.dmowner'
                        .$where
                        .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                        .' ORDER BY u.name');
                    $database->query();

                    $database->setQuery('INSERT IGNORE INTO '.$tablename
                        .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                        .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                        .' from #__users u'
                        .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                        .' INNER JOIN #__docman d ON eu.id=d.dmlastupdateby'
                        .$where
                        .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                        .' ORDER BY u.name');
                    $database->query();

                    $database->setQuery('INSERT IGNORE INTO '.$tablename
                        .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                        .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                        .' from #__users u'
                        .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                        .' INNER JOIN #__docman d ON eu.id=d.dmsubmitedby'
                        .$where
                        .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                        .' ORDER BY u.name');
                    $database->query();

                    $database->setQuery('INSERT IGNORE INTO '.$tablename
                        .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                        .' SELECT DISTINCT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                        .' from #__users u'
                        .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                        .' INNER JOIN #__docman d ON eu.id=d.dmmantainedby'
                        .$where
                        .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                        .' ORDER BY u.name');
                    $database->query();
                    $return = true;
                }
            }
        }

        return $return;

    } // processSend()

} // class sm2emailmarketing_plugin_DOCMan

$obj = new sm2emailmarketing_plugin_DOCMan();
return $obj;
?>