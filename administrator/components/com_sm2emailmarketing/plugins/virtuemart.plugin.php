<?php
// $Id: virtuemart.plugin.php,v 1.13 2008/03/31 01:59:10 sm2tony Exp $
/**
 * sm2emailmarketing - Email Marketing Newsletter
 *
 * Joomla! Newsletter Component that supports multiple list mailing and message queueing
 *
 * @package Joomla!
 * @subpackage com_sm2emailmarketing
 * @copyright Copyright (C) 2006 SM2 / All Rights Reserved
 * @license commercial
 * @author SM2 <info@sm2joomla.com>
 */

/**
 * Makes sure this is included by a parent file
 */
defined('_VALID_MOS') or die('Direct Access to this location is not allowed.');

class sm2emailmarketing_plugin_VirtueMart extends sm2emPluginObj {

    var $name = 'VirtueMart';
    var $filename = 'virtuemart.plugin.php';
    var $description = 'Allows SM2 Email Marketing to send messages based on VirtueMart Orders. Note: plugin will be invalid if VirtueMart is setup as a Catalog.';
    var $classname = 'sm2emailmarketing_plugin_VirtueMart';
    var $fieldPrefix = 'virtuemart';
    var $vm = 'vm';

    /**
     * Constructor
     */
    function sm2emailmarketing_plugin_VirtueMart() {
    } // sm2emailmarketing_plugin_VirtueMart()


    /**
     * validatePlugin
     *
     * Method to validate if the plugin is usable
     */
    function validatePlugin($pluginid=null) {
        global $mosConfig_absolute_path;

        if (file_exists($mosConfig_absolute_path.'/administrator/components/com_virtuemart/virtuemart.cfg.php')) {
            $config_data = file($mosConfig_absolute_path.'/administrator/components/com_virtuemart/virtuemart.cfg.php');
            foreach ($config_data as $line) {
                if (strpos($line, 'USE_AS_CATALOGUE')===false) continue;
                @eval($line);
                if (defined('USE_AS_CATALOGUE')) {
                    if (constant('USE_AS_CATALOGUE')) {
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    return true;
                }
            }
        } else {
            return false;
        }
    } // validatePlugin()

    /**
     * displayMessageEdit
     *
     * Method to display the fields on the Message Management form
     */
    function displayMessageFilters($messageid, $pluginid, $option) {
        global $database, $acl;

        if (!$this->validatePlugin($pluginid)) return false;

        $params = $this->getParams($pluginid);

        $this->vm = $params->get('prefix', 'vm');

        // check to see if atleast one option is selected
        $filterCount = 0;
        $filterCount += $params->get('shopper_group', 1);
        $filterCount += $params->get('billing_country', 1);
        $filterCount += $params->get('billing_state', 1);
        $filterCount += $params->get('product', 1);
        $filterCount += $params->get('category', 1);
        $filterCount += $params->get('manufacturer', 1);
        $filterCount += $params->get('vendor', 1);
        $filterCount += $params->get('payment_method', 1);
        $filterCount += $params->get('order_status', 1);
        $filterCount += $params->get('order_date', 1);

        if (empty($filterCount)) return false;

        // get the selected lists
        $selected = $this->getMessageData($messageid, $pluginid);

        // start output buffering
        ob_start();
        ?>
        <tr>
          <th colspan="2">
            <?php echo $params->get('section_title', 'VirtueMart'); ?>
          </th>
        </tr>
        <tr>
          <td colspan="2">
            <?php echo $params->get('section_description'); ?>
          </td>
        </tr>
        <?php

        // display the shopper group filter
        if ($params->get('shopper_group', 1)) {
            $this->_displayFilter('shopper_group', 'Shopper Group',
                'SELECT shopper_group_id AS value, shopper_group_name AS text FROM #__'.$this->vm.'_shopper_group ORDER BY shopper_group_name',
                $params, $selected);
        }

        // display the billing country filter
        if ($params->get('billing_country', 1)) {
            $this->_displayFilter('billing_country', 'Billing Country',
                'SELECT country_id AS value, country_name AS text FROM #__'.$this->vm.'_country ORDER BY country_name',
                $params, $selected);
        }

        // display the billing state filter
        if ($params->get('billing_state', 1)) {
            $this->_displayFilter('billing_state', 'Billing State',
                'SELECT state_id AS value, state_name AS text FROM #__'.$this->vm.'_state ORDER BY state_name',
                $params, $selected);
        }

        // display the product filter
        if ($params->get('product', 1)) {
            $this->_displayFilter('product', 'Product',
                'SELECT product_id AS value, CONCAT(product_name, '
                    .$database->Quote('(').', product_sku, '
                    .$database->Quote(')').') AS text'
                    .' FROM #__'.$this->vm.'_product ORDER BY product_name, product_sku',
                $params, $selected);
        }

        // display the category filter
        if ($params->get('category', 1)) {
            $this->_displayCategory($params, $selected);
        }

        // display the manufacturer filter
        if ($params->get('manufacturer', 1)) {
            $this->_displayFilter('manufacturer', 'Manufacturer',
                'SELECT manufacturer_id AS value, mf_name AS text FROM #__'.$this->vm.'_manufacturer ORDER BY mf_name',
                $params, $selected);
        }

        // display the vendor filter
        if ($params->get('vendor', 1)) {
            $this->_displayFilter('vendor', 'Vendor',
                'SELECT vendor_id AS value, vendor_name AS text FROM #__'.$this->vm.'_vendor ORDER BY vendor_name',
                $params, $selected);
        }

        // display the payment method filter
        if ($params->get('payment_method', 1)) {
            $this->_displayFilter('payment_method', 'Payment Method',
                'SELECT payment_method_id AS value, payment_method_name AS text FROM #__'.$this->vm.'_payment_method ORDER BY payment_method_name',
                $params, $selected);
        }

        // display the order status filter
        if ($params->get('order_status', 1)) {
            $this->_displayFilter('order_status', 'Order Status',
                'SELECT order_status_id AS value, CONCAT(order_status_name, '
                    .$database->Quote(' (').', order_status_code, '
                    .$database->Quote(')').') AS text FROM #__'.$this->vm.'_order_status ORDER BY list_order',
                $params, $selected);
        }

        // dsiplay the order date filter
        if ($params->get('order_date', 1)) {
            $this->_displayOrderDate($params, $selected);
        }

        $result = ob_get_contents();
        ob_end_clean();
        return $result;

    } // displayMessageFilters()


    function _displayFilter($filter, $filterTitle, $sql, $params, $selected) {
        global $database;

        $title = $params->get($filter.'_title', $filterTitle);
        $desc = $params->get($filter.'_description');
        if (!empty($desc)) {
            $title = '<span class="editlinktip">'.mosToolTip(addslashes($desc), $title, '', '', $title, '#', 0).'</span>';
        }

        $database->setQuery($sql);
        $options = $database->loadObjectList();

        array_unshift($options, mosHTML::makeOption('-1', '-- '.$title.' --'));

        $size = min(count($options), 10);

        $thisSelected = isset($selected[$filter]) ? convertArrayToOptions($selected[$filter]) : null;

        $list = mosHTML::selectList($options, $this->fieldPrefix.'['.$filter.'][]', 'class="inputbox" multiple size="'.$size.'" style="width:95%;"', 'value', 'text', $thisSelected);
        $list = preg_replace('/<option value="-1"[^>]*>([^<]*)<\/option>/iu', '<optgroup label="$1" />', $list);
        ?>
        <tr>
          <td valign="top"><?php echo $title; ?></td>
          <td valign="top"><?php echo $list; ?></td>
        </tr>
        <?php
    } // _displayFilter()


    function _displayCategory($params, $selected) {
        global $database;

        $title = $params->get('category_title', 'Category');
        $desc = $params->get('category_description');
        if (!empty($desc)) {
            $title = '<span class="editlinktip">'.mosToolTip(addslashes($desc), $title, '', '', $title, '#', 0).'</span>';
        }

        $database->setQuery('SELECT category_id AS value, category_name AS text'
            .' FROM #__'.$this->vm.'_category'
            .' ORDER BY category_name');
        $options = $database->loadObjectList();

        array_unshift($options, mosHTML::makeOption('-1', '-- '.$title.' --'));

        $size = min(count($options), 10);

        $thisSelected = isset($selected['category']) ? convertArrayToOptions($selected['category']) : null;

        $list = mosHTML::selectList($options, $this->fieldPrefix.'[category][]', 'class="inputbox" multiple size="'.$size.'" style="width:95%;"', 'value', 'text', $thisSelected);
        $list = preg_replace('/<option value="-1">([^<]*)<\/option>/iu', '<optgroup label="$1" />', $list);
        ?>
        <tr>
          <td valign="top"><?php echo $title; ?></td>
          <td valign="top"><?php echo $list; ?></td>
        </tr>
        <?php
    } // _displayCategory()


    function _displayOrderDate($params, $selected) {

        $title = $params->get('order_date_title', 'Order Date');
        $desc = $params->get('order_date_description');
        if (!empty($desc)) {
            $title = '<span class="editlinktip">'.mosToolTip(addslashes($desc), $title, '', '', $title, '#', 0).'</span>';
        }
        if (!isset($selected['order_date']['start'])) $selected['order_date']['start'] = null;
        if (!isset($selected['order_date']['end'])) $selected['order_date']['end'] = null;
        ?>
        <tr>
          <td valign="top"><?php echo $title; ?></td>
          <td valign="top">
            <table>
              <tr>
                <td><?php echo $params->get('order_date_from', 'From'); ?></td>
                <td>
                  <input class="text_area" type="text" name="<?php echo $this->fieldPrefix; ?>[order_date][start]" id="<?php echo $this->fieldPrefix; ?>_order_date_start" size="25" maxlength="19" value="<?php echo $selected['order_date']['start']; ?>" />
                  <input name="reset" type="reset" class="button" onClick="return showCalendar('<?php echo $this->fieldPrefix; ?>_order_date_start', 'y-mm-dd');" value="...">
                </td>
              </tr>
              <tr>
                <td><?php echo $params->get('order_date_to', 'To'); ?></td>
                <td>
                  <input class="text_area" type="text" name="<?php echo $this->fieldPrefix; ?>[order_date][end]" id="<?php echo $this->fieldPrefix; ?>_order_date_end" size="25" maxlength="19" value="<?php echo $selected['order_date']['end']; ?>" />
                  <input name="reset" type="reset" class="button" onClick="return showCalendar('<?php echo $this->fieldPrefix; ?>_order_date_end', 'y-mm-dd');" value="...">
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <?php
    } // _displayOrderDate()


    /**
     * processSend
     *
     * Method to process a plugin send
     * Retrieves all of the users
     * Returns an array of name,email objects
     */
    function processSend($messageid, $pluginid, $option) {
        global $database;

        if (!$this->validatePlugin($pluginid)) return false;

        $params = $this->getParams($pluginid);

        $this->vm = $params->get('prefix', 'vm');

        // determine which users to send to
        $pluginData = $this->getMessageData($messageid, $pluginid);

        if (empty($pluginData)) return false;

        $return = false;

        // determine the tablename to process into
        $tablename = determineQueueProcessTable();

        // process the shopper filter
        if ($params->get('shopper_group', 1)) {
            if (!empty($pluginData['shopper_group'])) {
                // find the users who have this shopper group
                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' from #__users u'
                    .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' INNER JOIN #__'.$this->vm.'_shopper_vendor_xref s ON eu.id = s.user_id'
                    .' WHERE (s.shopper_group_id IN ('.implode(', ', $pluginData['shopper_group']).'))'
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY u.name');
                $database->query();
                $return = true;
            }
        }

        // process the billing country filter
        if ($params->get('billing_country', 1)) {
            if (!empty($pluginData['billing_country'])) {
                // get the list of country codes (why they dont store this as the id ill never know
                $database->setQuery('SELECT DISTINCT country_3_code FROM #__'.$this->vm.'_country'
                    .' WHERE (country_id IN ('.implode(', ', $pluginData['billing_country']).'))');
                $countryCodes = $database->loadResultArray();
                if (!empty($countryCodes)) {
                    // find the users who have the correct country code
                    $database->setQuery('INSERT IGNORE INTO '.$tablename
                        .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                        .' SELECT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                        .' from #__users u'
                        .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                        .' INNER JOIN #__'.$this->vm.'_user_info i ON eu.id = i.user_id'
                        .' WHERE (i.country IN ('.implode(', ', $countryCodes).'))'
                        .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                        .' ORDER BY u.name');
                    $database->query();
                    $return = true;
                }
            }
        }

        // process the billing state filter
        if ($params->get('billing_state', 1)) {
            if (!empty($pluginData['billing_state'])) {
                // get the list of states codes (why they dont store this as the id ill never know
                $database->setQuery('SELECT DISTINCT state_2_code FROM #__'.$this->vm.'_state'
                    .' WHERE (state_id IN ('.implode(', ', $pluginData['billing_state']).'))');
                $stateCodes = $database->loadResultArray();
                if (!empty($stateCodes)) {
                    // find the users who have the correct country code
                    $database->setQuery('INSERT IGNORE INTO '.$tablename
                        .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                        .' SELECT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                        .' from #__users u'
                        .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                        .' INNER JOIN #__'.$this->vm.'_user_info i ON eu.id = i.user_id'
                        .' WHERE (i.state IN ('.implode(', ', $stateCodes).'))'
                        .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                        .' ORDER BY u.name');
                    $database->query();
                    $return = true;
                }
            }
        }

        // process the product filter
        if ($params->get('product', 1)) {
            if (!empty($pluginData['product'])) {
                // find the users who have orders for this product
                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' from #__users u'
                    .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' INNER JOIN #__'.$this->vm.'_orders o ON eu.id = o.user_id'
                    .' INNER JOIN #__'.$this->vm.'_order_item i ON o.order_id=i.order_id'
                    .' WHERE (i.product_id IN ('.implode(', ', $pluginData['product']).'))'
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY u.name');
                $database->query();
                $return = true;
            }
        }

        // process the category filter
        if ($params->get('category', 1)) {
            if (!empty($pluginData['category'])) {
                if (
                (!is_array($pluginData['category'])) ||
                (count($pluginData['category'])==0) ||
                ((count($pluginData['category'])==1) && (empty($pluginData['category'][0])))
                ) {
                    $pluginData['category'] = array(0);
                }
                // find the users who have orders for products in this category
                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' from #__users u'
                    .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' INNER JOIN #__'.$this->vm.'_orders o ON eu.id = o.user_id'
                    .' INNER JOIN #__'.$this->vm.'_order_item i ON o.order_id=i.order_id'
                    .' INNER JOIN #__'.$this->vm.'_product_category_xref c ON i.product_id=c.product_id'
                    .' WHERE (c.category_id IN ('.implode(', ', $pluginData['category']).'))'
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY u.name');
                $database->query();
                $return = true;
            }
        }

        // process the manufacturer filter
        if ($params->get('manufacturer', 1)) {
            if (!empty($pluginData['manufacturer'])) {
                if (
                (!is_array($pluginData['manufacturer'])) ||
                (count($pluginData['manufacturer'])==0) ||
                ((count($pluginData['manufacturer'])==1) && (empty($pluginData['manufacturer'][0])))
                ) {
                    $pluginData['manufacturer'] = array(0);
                }
                // find the users who have orders for products by this manufacturer
                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' from #__users u'
                    .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' INNER JOIN #__'.$this->vm.'_orders o ON eu.id = o.user_id'
                    .' INNER JOIN #__'.$this->vm.'_order_item i ON o.order_id=i.order_id'
                    .' INNER JOIN #__'.$this->vm.'_product_mf_xref m ON i.product_id=m.product_id'
                    .' WHERE (m.manufacturer_id IN ('.implode(', ', $pluginData['manufacturer']).'))'
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY u.name');
                $database->query();
                $return = true;
            }
        }

        // process the vendor filter
        if ($params->get('vendor', 1)) {
            if (!empty($pluginData['vendor'])) {
                // find the users who have orders for products by this vendor
                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' from #__users u'
                    .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' INNER JOIN #__'.$this->vm.'_orders o ON eu.id = o.user_id'
                    .' INNER JOIN #__'.$this->vm.'_order_item i ON o.order_id=i.order_id'
                    .' INNER JOIN #__'.$this->vm.'_product p ON i.product_id=p.product_id'
                    .' WHERE (p.vendor_id IN ('.implode(', ', $pluginData['vendor']).'))'
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY u.name');
                $database->query();
                $return = true;
            }
        }

        // process the payment method filter
        if ($params->get('payment_method', 1)) {
            if (!empty($pluginData['payment_method'])) {
                // find the users who have orders with this payment method
                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' from #__users u'
                    .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' INNER JOIN #__'.$this->vm.'_orders o ON eu.id = o.user_id'
                    .' INNER JOIN #__'.$this->vm.'_order_payment p ON o.order_id=p.order_id'
                    .' WHERE (p.payment_method_id IN ('.implode(', ', $pluginData['payment_method']).'))'
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY u.name');
                $database->query();
                $return = true;
            }
        }

        // process the order status filter
        if ($params->get('order_status', 1)) {
            if (!empty($pluginData['order_status'])) {
                // find the users who have orders with this order status
                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' from #__users u'
                    .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' INNER JOIN #__'.$this->vm.'_orders o ON eu.id = o.user_id'
                    .' INNER JOIN #__'.$this->vm.'_order_status s ON o.order_status=s.order_status_code'
                    .' WHERE (s.order_status_id IN ('.implode(', ', $pluginData['order_status']).'))'
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY u.name');
                $database->queery();
                $return = true;
            }
        }

        // process the order date filter
        if ($params->get('order_date', 1)) {
            $start = $pluginData['order_date']['start'];
            $end = $pluginData['order_date']['end'];

            // determine how to find the data
            if (!empty($start) || !empty($end)) {
                // fix nevers
                if (strtolower($start)=='never') $start = '0000-00-00';
                if (strtolower($end)=='never') $end = '0000-00-00';

                // fix dates if reversed
                if ($end < $start) {
                    $temp = $start;
                    $start = $end;
                    $end = $temp;
                }
                $where = '';

                // process the start and end to make them timestamps rather than dates
                $orderstart = explode('-', $start);
                $orderstart = mktime(0, 0, 0, $orderstart[1], $orderstart[2], $orderstart[0]);
                $orderend = explode('-', $end);
                $orderend = mktime(0, 0, 0, $orderend[1], $orderend[2]+1, $orderend[0]);

                if ($start==$end) {
                    $orderend = explode('-', $start);
                    $orderend = mktime(0, 0, 0, $orderend[1], $orderend[2]+1, $orderend[0]);
                    $where = ' WHERE (o.cdate BETWEEN $orderstart AND $orderend)';
                } else if (empty($start)) {
                    $where = ' WHERE (o.cdate <= $orderend)';
                } else if (empty($end)) {
                    $where = ' WHERE (o.cdate >= $orderstart)';
                } else {
                    $where = ' WHERE (o.cdate  BETWEEN $orderstart AND $orderend)';
                }

                if (!empty($where)) {
                    // find the users who have orders created that match the order date filter
                    $database->setQuery('INSERT IGNORE INTO '.$tablename
                        .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                        .' SELECT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                        .' from #__users u'
                        .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                        .' INNER JOIN #__'.$this->vm.'_orders o ON eu.id = o.user_id'
                        .$where
                        .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                        .' ORDER BY u.name');
                    $database->query();
                    $return = true;
                }
            }
        }

        return $return;

    } // processSend()

} // class sm2emailmarketing_plugin_VirtueMart

$obj = new sm2emailmarketing_plugin_VirtueMart();
return $obj;
?>