<?php
// $Id: user.plugin.php,v 1.20 2007/09/24 00:57:31 sm2tony Exp $
/**
 * sm2emailmarketing - Email Marketing Newsletter
 *
 * Joomla! Newsletter Component that supports multiple list mailing and message queueing
 *
 * @package Joomla!
 * @subpackage com_sm2emailmarketing
 * @copyright Copyright (C) 2006 SM2 / All Rights Reserved
 * @license commercial
 * @author SM2 <info@sm2joomla.com>
 */

/**
 * Makes sure this is included by a parent file
 */
defined('_VALID_MOS') or die('Direct Access to this location is not allowed.');

class sm2emailmarketing_plugin_user extends sm2emPluginObj {

    var $name = 'SM2 Email Marketing User';
    var $filename = 'user.plugin.php';
    var $description = 'Allows SM2 Email Marketing to send messages based on Joomla User data';
    var $classname = 'sm2emailmarketing_plugin_user';
    var $fieldPrefix = 'emktg_user';


    /**
     * Constructor
     */
    function sm2emailmarketing_plugin_user() {
    } // sm2emailmarketing_plugin_user()


    /**
     * displayMessageEdit
     *
     * Method to display the fields on the Message Management form
     */
    function displayMessageFilters($messageid, $pluginid, $option) {
        global $database, $acl;

        if (!$this->validatePlugin($pluginid)) return false;

        $params = $this->getParams($pluginid);

        // get the selected lists
        $selected = $this->getMessageData($messageid, $pluginid);

        if (isset($selected['gid'])) {
            $selected['gid'] = convertArrayToOptions($selected['gid']);
        } else {
            $selected['gid'] = null;
        }
        if (!isset($selected['blocked'])) $selected['blocked'] = null;
        if (!isset($selected['last_login']['start'])) $selected['last_login']['start'] = null;
        if (!isset($selected['last_login']['end'])) $selected['last_login']['end'] = null;
        if (!isset($selected['loginStatus'])) $selected['loginStatus'] = null;

        $groupTitle = $params->get('group_title', 'User Groups');
        if ($params->get('group_description')) {
            $groupTitle = '<span class="editlinktip">'.mosToolTip(addslashes($params->get('group_description')), $groupTitle, '', '', $groupTitle, '#', 0).'</span>';
        }

        $gtree = $acl->get_group_children_tree( null, 'USERS', false );

        // add the select all item to the start of the list
        $selectAll = mosHTML::makeOption('-1', $params->get('group_select_all', 'Send to All Groups'));
        array_unshift($gtree, $selectAll);

        // build the user groups list
        $lists['gid'] = mosHTML::selectList($gtree, $this->fieldPrefix.'[gid][]', 'class="inputbox" size="10" multiple', 'value', 'text', $selected['gid']);

        $blockedTitle = $params->get('blocked_title', 'Blocked Users');
        if ($params->get('blocked_description')) {
            $blockedTitle = '<span class="editlinktip">'.mosToolTip(addslashes($params->get('blocked_description')), $blockedTitle, '', '', $blockedTitle, '#', 0).'</span>';
        }

        $blockedOptions = array(
            mosHTML::makeOption('', ''),
            mosHTML::makeOption('0', _CMN_NO),
            mosHTML::makeOption('1', _CMN_YES)
        );

        $lists['blocked'] = mosHTML::selectList($blockedOptions, $this->fieldPrefix.'[blocked]', 'class="inputbox"', 'value', 'text', $selected['blocked']);

        $lastLoginTitle = $params->get('last_login_title', 'Last Login');
        if ($params->get('last_login_description')) {
            $lastLoginTitle = '<span class="editlinktip">'.mosToolTip(addslashes($params->get('last_login_description')), $lastLoginTitle, '', '', $lastLoginTitle, '#', 0).'</span>';
        }

        //never logged in
        $loginstatus = array(
        	mosHTML::makeOption('', ''),
        	mosHTML::makeOption('1', 'Has Logged In Before'),
        	mosHTML::makeOption('2', 'Has Never Logged In'),
        	mosHTML::makeOption('3', 'Never Logged In, Resend Login Details')
        );

        $lists['loginstatus'] = mosHTML::selectList($loginstatus, $this->fieldPrefix.'[loginStatus]', 'class="inputbox"', 'value', 'text', $selected['loginStatus']);
        $loginStatusTitle = $params->get('login_status_title', 'Login Status');
        // start output buffering
        ob_start();
        ?>
        <tr>
          <th colspan="2"><?php echo $params->get('section_title', 'Joomla Users'); ?></th>
        </tr>
        <tr>
          <td colspan="2"><?php echo $params->get('section_description'); ?></td>
        </tr>
<?php   if ($params->get('groups', 1)) { ?>
        <tr>
          <td valign="top"><?php echo $groupTitle; ?></td>
          <td valign="top"><?php echo $lists['gid']; ?></td>
        </tr>
<?php   } ?>
<?php   if ($params->get('blocked', 1)) { ?>
        <tr>
          <td valign="top"><?php echo $blockedTitle; ?></td>
          <td valign="top"><?php echo $lists['blocked']; ?></td>
        </tr>
<?php   } ?>
<?php   if ($params->get('last_login', 1)) { ?>
        <tr>
          <td valign="top"><?php echo $lastLoginTitle; ?></td>
          <td valign="top">
            <table>
              <tr>
                <td><?php echo $params->get('last_login_from', 'From'); ?></td>
                <td>
                  <input class="text_area" type="text" name="<?php echo $this->fieldPrefix; ?>[last_login][start]" id="<?php echo $this->fieldPrefix; ?>_last_login_start" size="25" maxlength="19" value="<?php echo $selected['last_login']['start']; ?>" />
                  <input name="reset" type="reset" class="button" onClick="return showCalendar('<?php echo $this->fieldPrefix; ?>_last_login_start', 'y-mm-dd');" value="...">
                </td>
              </tr>
              <tr>
                <td><?php echo $params->get('last_login_to', 'To'); ?></td>
                <td>
                  <input class="text_area" type="text" name="<?php echo $this->fieldPrefix; ?>[last_login][end]" id="<?php echo $this->fieldPrefix; ?>_last_login_end" size="25" maxlength="19" value="<?php echo $selected['last_login']['end']; ?>" />
                  <input name="reset" type="reset" class="button" onClick="return showCalendar('<?php echo $this->fieldPrefix; ?>_last_login_end', 'y-mm-dd');" value="...">
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td valign="top"><?php echo $loginStatusTitle; ?></td>
          <td valign="top"><?php echo $lists['loginstatus']; ?></td>
        </tr>
<?php   } ?>

        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
        <?php
        $result = ob_get_contents();
        ob_end_clean();
        return $result;
    } // displayMessageFilters()


    /**
     * processMessageEdit
     *
     * Method to process the fields from the Message Management form
     * Data is stored in the emktg_message_plugin table
     */
    function processMessageEdit($messageid, $pluginid, $option) {
        global $database;

        if (!$this->validatePlugin($pluginid)) return false;

        // get the data passed by the form
        $data = mosGetParam($_POST, $this->fieldPrefix, array());

        if (@is_array($data['gid'])) {
            if (in_array(-1, $data['gid'])) {
                $data['gid'] = array(-1);
            }
        }

        // see if there is an existing record
        sm2emailmarketingSetPluginMessageData($messageid, $pluginid, $data);

    } // processMessageEdit


    /**
     * processSend
     *
     * Method to process a plugin send
     * Retrieves all of the users
     * Returns an array of name,email objects
     */
    function processSend($messageid, $pluginid, $option) {
        global $database;

        if (!$this->validatePlugin($pluginid)) return false;

        $params = $this->getParams($pluginid);

        // determine which users to send to
        $pluginData = $this->getMessageData($messageid, $pluginid);

        if (empty($pluginData)) return false;

        $return = false;

        // determine the tablename to process into
        $tablename = determineQueueProcessTable();

        if ($params->get('groups', 1)) {
            if (!empty($pluginData['gid'])) {
                $where = ' WHERE gid IN ('.implode(',', $pluginData['gid']).')';
                if (in_array('-1', $pluginData['gid'])) {
                    $where = ' WHERE gid != -1';
                }

                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' FROM #__users u'
                	.' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .$where
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY name');
                $database->query();
                $return = true;
            }
        }

        if ($params->get('blocked', 1)) {
            if (!empty($pluginData['blocked'])) {
                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' FROM #__users u'
                	.' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' WHERE block='.$pluginData['blocked']
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY name');
                $database->query();
                $return = true;
            }
        }

        if ($params->get('last_login', 1)) {
            $start = $pluginData['last_login']['start'];
            $end = $pluginData['last_login']['end'];

            // determine how to find the data
            if (!empty($start) || !empty($end)) {
                // fix nevers
                if (strtolower($start)=='never') $start = '0000-00-00';
                if (strtolower($end)=='never') $end = '0000-00-00';

                // fix dates if reversed
                if ($end < $start) {
                    $temp = $start;
                    $start = $end;
                    $end = $temp;
                }
                $where = '';

                if ($start==$end) {
                    $where = ' WHERE (lastvisitDate = '.$database->Quote($start).')';
                } else if (empty($start)) {
                    $where = ' WHERE (lastvisitDate <= '.$database->Quote($end).')';
                } else if (empty($end)) {
                    $where = ' WHERE (lastvisitDate >= '.$database->Quote($start).')';
                } else {
                    $where = ' WHERE (lastvisitDate BETWEEN '
                        .$database->Quote($start).' AND '
                        .$database->Quote($end).')';
                }

                if (!empty($where)) {
                    $database->setQuery('INSERT IGNORE INTO '.$tablename
                        .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                        .' SELECT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                        .' from #__users u'
                        .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                        .$where
                        .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                        .' ORDER BY name');
                    $database->query();
                    $return = true;
                }
            }
        }

        //login_status
        if($params->get('last_login', 1)){
        	if (!empty($pluginData['loginStatus'])) {
        		switch ($pluginData['loginStatus']) {
        			case 1:
        				$clause = ' `lastvisitDate` > 0';
        				break;
        			case 2:
        			case 3:
        				$clause = ' `lastvisitDate` = 0 OR `lastvisitDate` IS NULL';
        				break;
        		}
                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' from #__users u'
                	.' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' WHERE '.$clause
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date =0'
                    .' ORDER BY name');
                $database->query();
                $return = true;
        	}
        }

        return $return;

    } // processSend()

    /**
     * processMessage
     *
     * Method to process a plugin send
     * Retrieves all of the users
     * Returns an array of name,email objects
     */
    function processMessage($messageid, $pluginid, $option, &$row, $userid) {
        global $database;

        $params = $this->getParams($pluginid);

        // determine which users to send to
        $pluginData = $this->getMessageData($messageid, $pluginid);

        if (empty($pluginData)) {
            //remove [DETAILS] tag - because option to send the details was not chosen
            $row->message_html = str_replace('[DETAILS]', '', $row->message_html);
            $row->message_text = str_replace('[DETAILS]', '', $row->message_text);
            return;
        }

        if($params->get('last_login', 1)){
        	if (!empty($pluginData['loginStatus'])) {
        		if ($pluginData['loginStatus']==3) {
        			// check to see if details is there

        			if (strpos($row->message_html, '[DETAILS]')===false) {
        				$row->message_html .= '[DETAILS]';
        			}
        			if (strpos($row->message_text, '[DETAILS]')===false) {
        				$row->message_text .= '[DETAILS]';
        			}

                    $logindetails = '';
                    if (!empty($userid)) {
        				// load the user information
        				$user = new mosUser($database);
        				$user->load($userid);

                        if ($user->lastvisitDate=='0000-00-00 00:00:00') {

            				// generate the password
            				$pwd='';
            				$pwd = mosMakePassword();

            				// update the database

            				$user->password = md5($pwd);
            				$user->store();

            				// create the details data
            				// need to load client management config to decide
            				// whether using email or username as login
            				$userLoginType = $this->userLoginType();
            				if ($userLoginType) { //use email
            					$logindetails = 'Email Login: '.$user->email.'<br />Password: '.$pwd.'<br />';
            				} else { //use username
            					$logindetails = 'Username: '.$user->username.'<br />Password: '.$pwd.'<br />';
            				}
                        }
                    }

    				// replace in row data
    				$row->message_html = str_replace('[DETAILS]', $logindetails, $row->message_html);
    				$row->message_text = str_replace('[DETAILS]', $logindetails, $row->message_text);
        		}
        	}
        }


        //remove [DETAILS] tag - because option to send the details was not chosen
        $row->message_html = str_replace('[DETAILS]', '', $row->message_html);
		$row->message_text = str_replace('[DETAILS]', '', $row->message_text);


    } // processMessage()

    function userLoginType() {
    	global $mosConfig_absolute_path;
    	if(file_exists($mosConfig_absolute_path.'/administrator/components/com_member/member_config.php')){
    		global $sm2CMConfig;
    		include_once($mosConfig_absolute_path.'/administrator/components/com_member/member_config.php');
    		if(!empty ($sm2CMConfig['email_login'])){
    			return true;
    		} else {
    			return false;
    		}
    	} else {
    		return false;
    	}

    }

} // class sm2emailmarketing_plugin_user

$obj = new sm2emailmarketing_plugin_user();
return $obj;
?>