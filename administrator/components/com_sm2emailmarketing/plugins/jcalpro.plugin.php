<?php
// $Id: jcalpro.plugin.php,v 1.18 2008/04/18 09:27:11 sm2tony Exp $
/**
 * sm2emailmarketing - Email Marketing Newsletter
 *
 * Joomla! Newsletter Component that supports multiple list mailing and message queueing
 *
 * @package Joomla!
 * @subpackage com_sm2emailmarketing
 * @copyright Copyright (C) 2006 SM2 / All Rights Reserved
 * @license commercial
 * @author SM2 <info@sm2joomla.com>
 */

/**
 * Makes sure this is included by a parent file
 */
defined('_VALID_MOS') or die('Direct Access to this location is not allowed.');

class sm2emailmarketing_plugin_JCalPro extends sm2emPluginObj {

    var $name = 'JCal Pro';
    var $filename = 'jcalpro.plugin.php';
    var $description = 'Allows SM2 Email Marketing to send messages based on JCal Pro Events. Note: plugin will only be able to send to emails used as contacts in JCal Pro Events if the email already exists as an SM2 Email Marketing Subscriber.';
    var $classname = 'sm2emailmarketing_plugin_JCalPro';
    var $fieldPrefix = 'jcalpro';

    /**
     * Constructor
     */
    function sm2emailmarketing_plugin_JCalPro() {
    } // sm2emailmarketing_plugin_JCalPro()


    /**
     * validatePlugin
     *
     * Method to validate if the plugin is usable
     */
    function validatePlugin($pluginid=null) {
        global $mosConfig_absolute_path;

        if (file_exists($mosConfig_absolute_path.'/administrator/components/com_jcalpro/admin.config.inc.php')) {
//            require_once($mosConfig_absolute_path.'/administrator/components/com_jcalpro/admin.config.inc.php');
            return true;
        } else {
            return false;
        }
    } // validatePlugin()


    /**
     * displayMessageExtras
     *
     * Method to display the list of extra content that can be included in the message
     */
    function displayMessageExtras($row, $plugin, $option) {
        global $database;

        $pluginid = $plugin->pluginid;
        if (!$this->validatePlugin($pluginid)) return false;
        if (!$plugin->enabled) return false;

        $params = $this->getParams($pluginid);

        if (!$params->get('details', 1)) return false;

        $extraCount = 0;
        $extraCount += $params->get('extra_category', 1);
        $extraCount += $params->get('extra_event', 1);
        $extraCount += $params->get('extra_date', 1);
        $extraCount += $params->get('extra_next', 1);

        if (empty($extraCount)) return false;

        // get the selected lists
        $selected = $this->getMessageData($row->messageid, $pluginid);

        if (!is_array($selected)) {
            $selected = array($selected);
        }

        // start output buffering
        ob_start();
        ?>
        <tr>
          <th colspan='2'>
            <?php echo $params->get('extra_title', 'JCal Pro'); ?>
          </th>
        </tr>
        <tr>
          <td colspan='2'>
            <?php echo $params->get('extra_description'); ?>
          </td>
        </tr>
        <?php
        // display the category filter
        if ($params->get('extra_category', 1)) {
            $this->_extraCategory($params, $selected);
        }

        // display the event filter
        if ($params->get('extra_event', 1)) {
            $this->_displayExtra('extra_event', 'Event',
                'SELECT extid AS value, title AS text FROM #__jcalpro_events ORDER BY title',
                $params, $selected);
        }

        // display the date filter
        if ($params->get('extra_date', 1)) {
            $this->_extraDate($params, $selected);
        }

        // display the next filter
        if ($params->get('extra_extra', 1)) {
            $this->_extraNext($params, $selected);
        }

        $this->_extraNumEvents($params, $selected);


        $result = ob_get_contents();
        ob_end_clean();
        return $result;
    } // displayMessageExtras()


    function _displayExtra($filter, $filterTitle, $sql, $params, $selected) {
        global $database;

        $title = $params->get($filter.'_title', $filterTitle);
        $desc = $params->get($filter.'_description');
        if (!empty($desc)) {
            $title = '<span class="editlinktip">'.mosToolTip(addslashes($desc), $title, '', '', $title, '#', 0).'</span>';
        }

        $database->setQuery($sql);
        $options = $database->loadObjectList();

        array_unshift($options, mosHTML::makeOption('-1', '-- '.$title.' --'));

        $size = min(count($options), 10);

        $checked = '';
        if (@$selected['extra_selected']==$filter) {
            $thisSelected = isset($selected[$filter]) ? convertArrayToOptions($selected[$filter]) : null;
            $checked =  ' checked="checked"';
        } else {
            $thisSelected = null;
        }

        $list = mosHTML::selectList($options, $this->fieldPrefix.'['.$filter.'][]', 'class="inputbox" multiple size="'.$size.'" style="width:95%;"', 'value', 'text', $thisSelected);
        $list = preg_replace('/<option value="-1"[^>]*>([^<]*)<\/option>/iu', '<optgroup label="$1" />', $list);

        ?>
        <tr>
          <td valign="top"><?php echo $title; ?></td>
          <td valign="top">
            <label for="<?php echo $this->fieldPrefix.'_'.$filter; ?>">
              <input id="<?php echo $this->fieldPrefix.'_'.$filter; ?>" type="radio" name="<?php echo $this->fieldPrefix.'[extra_selected]'; ?>" value="<?php echo $filter; ?>" <?php echo $checked; ?> />
              <?php echo $list; ?>
            </label>
          </td>
        </tr>
        <?php
    } // _extraFilter()


    function _extraCategory($params, $selected) {
        global $database;

        $title = $params->get('extra_category_title', 'Category');
        $desc = $params->get('extra_category_description');
        if (!empty($desc)) {
            $title = '<span class="editlinktip">'.mosToolTip(addslashes($desc), $title, '', '', $title, '#', 0).'</span>';
        }

        $database->setQuery('SELECT cat_id AS value, cat_name AS text'
            .' FROM #__jcalpro_categories'
            .' ORDER BY cat_name');
        $options = $database->loadObjectList();

        array_unshift($options, mosHTML::makeOption('', ''));

        $checked = '';
        if (@$selected['extra_selected']=='extra_category') {
            $thisSelected = isset($selected['extra_category']) ? $selected['extra_category'] : null;
            $checked = ' checked="checked"';
        } else {
            $thisSelected = null;
        }

        $list = mosHTML::selectList($options, $this->fieldPrefix.'[extra_category]', 'class="inputbox" style="width:95%;"', 'value', 'text', $thisSelected);

        ?>
        <tr>
          <td valign="top"><?php echo $title; ?></td>
          <td valign="top">
            <label for="<?php echo $this->fieldPrefix.'_extra_category'; ?>">
              <input id="<?php echo $this->fieldPrefix.'_extra_category'; ?>" type="radio" name="<?php echo $this->fieldPrefix.'[extra_selected]'; ?>" value="extra_category" <?php echo $checked; ?> />
              <?php echo $list; ?>
            </label>
          </td>
        </tr>
        <?php
    } // _extraCategory()


    function _extraDate($params, $selected) {

        $title = $params->get('extra_date_title', 'Date');
        $desc = $params->get('extra_date_description');
        if (!empty($desc)) {
            $title = '<span class="editlinktip">'.mosToolTip(addslashes($desc), $title, '', '', $title, '#', 0).'</span>';
        }
        $checked = '';
        if (@$selected['extra_selected']=='extra_date') {
            if (!isset($selected['extra_date']['start'])) $selected['extra_date']['start'] = null;
            if (!isset($selected['extra_date']['end'])) $selected['extra_date']['end'] = null;
            $checked = 'checked="checked"';
        } else {
            $selected['extra_date']['start'] = null;
            $selected['extra_date']['end'] = null;
        }

        ?>
        <tr>
          <td valign="top"><?php echo $title; ?></td>
          <td valign="top">
            <input id="<?php echo $this->fieldPrefix.'extra_date'; ?>" type="radio" name="<?php echo $this->fieldPrefix.'[extra_selected]'; ?>" value="extra_date" <?php echo $checked; ?> />

            <table>
              <tr>
                <td><?php echo $params->get('date_from', 'From'); ?></td>
                <td>
                  <label for="<?php echo $this->fieldPrefix.'extra_date'; ?>">
                  <input class="text_area" type="text" name="<?php echo $this->fieldPrefix; ?>[extra_date][start]" id="<?php echo $this->fieldPrefix; ?>_extra_date_start" size="25" maxlength="19" value="<?php echo $selected['extra_date']['start']; ?>" />
                  <input name="reset" type="reset" class="button" onClick="return showCalendar('<?php echo $this->fieldPrefix; ?>_extra_date_start', 'y-mm-dd');" value="...">
                  </label>
                </td>
              </tr>
              <tr>
                <td><?php echo $params->get('date_to', 'To'); ?></td>
                <td>
                  <label for="<?php echo $this->fieldPrefix.'extra_date'; ?>">
                  <input class="text_area" type="text" name="<?php echo $this->fieldPrefix; ?>[extra_date][end]" id="<?php echo $this->fieldPrefix; ?>_extra_date_end" size="25" maxlength="19" value="<?php echo $selected['extra_date']['end']; ?>" />
                  <input name="reset" type="reset" class="button" onClick="return showCalendar('<?php echo $this->fieldPrefix; ?>_extra_date_end', 'y-mm-dd');" value="...">
                  </label>
                </td>
              </tr>
            </table>
            </label>
          </td>
        </tr>
        <?php
    } // _extraDate()


    function _extraNext($params, $selected) {
        global $database;

        $title = $params->get('extra_next_title', 'Next Events');
        $desc = $params->get('extra_next_description');
        if (!empty($desc)) {
            $title = '<span class="editlinktip">'.mosToolTip(addslashes($desc), $title, '', '', $title, '#', 0).'</span>';
        }

        $checked = (@$selected['extra_selected']=='extra_next') ? ' checked="checked"' : '';

        ?>
        <tr>
          <td valign="top"><?php echo $title; ?></td>
          <td valign="top">
            <label for="<?php echo $this->fieldPrefix.'_extra_next'; ?>">
              <input id="<?php echo $this->fieldPrefix.'_extra_next'; ?>" type="radio" name="<?php echo $this->fieldPrefix.'[extra_selected]'; ?>" value="extra_next" <?php echo $checked; ?> />
              <?php echo $desc; ?>
            </label>
          </td>
        </tr>
        <?php
    } // _extraNext()


    function _extraNumEvents($params, $selected) {
        global $database;

        $title = $params->get('extra_numevents_title', '# Events');
        $desc = $params->get('extra_numevents_description');
        if (!empty($desc)) {
            $title = '<span class="editlinktip">'.mosToolTip(addslashes($desc), $title, '', '', $title, '#', 0).'</span>';
        }

        $options = array();
        foreach (explode(',', $params->get('extra_list_size', '1,2,3,5,10')) as $value) {
            $options[] = mosHTML::makeOption((int)$value, (int)$value);
        }

        $thisSelected = isset($selected['extra_numevents']) ? $selected['extra_numevents'] : $params->get('extra_list_default', 1);

        $list = mosHTML::selectList($options, $this->fieldPrefix.'[extra_numevents]', 'class="inputbox"', 'value', 'text', $thisSelected);

        ?>
        <tr>
          <td valign="top"><?php echo $title; ?></td>
          <td valign="top">
              <?php echo $list; ?>
            </label>
          </td>
        </tr>
        <?php
    } // _extraNumEvents()


    /**
     * displayMessageFilters
     *
     * Method to display the fields on the Message Management form
     */
    function displayMessageFilters($messageid, $pluginid, $option) {
        global $database, $acl;

        if (!$this->validatePlugin($pluginid)) return false;

        $params = $this->getParams($pluginid);

        // check to see if atleast one option is selected
        $filterCount = 0;
        $filterCount += $params->get('category', 1);
        $filterCount += $params->get('event', 1);
        $filterCount += $params->get('date', 1);

        if (empty($filterCount)) return false;

        // get the selected lists
        $selected = $this->getMessageData($messageid, $pluginid);

        // start output buffering
        ob_start();
        ?>
        <tr>
          <th colspan='2'>
            <?php echo $params->get('section_title', 'JCal Pro'); ?>
          </th>
        </tr>
        <tr>
          <td colspan='2'>
            <?php echo $params->get('section_description'); ?>
          </td>
        </tr>
        <?php

        // display the category filter
        if ($params->get('category', 1)) {
            $this->_displayCategory($params, $selected);
        }

        // display the event filter
        if ($params->get('event', 1)) {
            $this->_displayFilter('event', 'Event',
                'SELECT extid AS value, title AS text FROM #__jcalpro_events ORDER BY title',
                $params, $selected);
        }

        // display the date filter
        if ($params->get('date', 1)) {
            $this->_displayDate($params, $selected);
        }

        $result = ob_get_contents();
        ob_end_clean();
        return $result;

    } // displayMessageFilters()


    function _displayFilter($filter, $filterTitle, $sql, $params, $selected) {
        global $database;

        $title = $params->get($filter.'_title', $filterTitle);
        $desc = $params->get($filter.'_description');
        if (!empty($desc)) {
            $title = '<span class="editlinktip">'.mosToolTip(addslashes($desc), $title, '', '', $title, '#', 0).'</span>';
        }

        $database->setQuery($sql);
        $options = $database->loadObjectList();

        array_unshift($options, mosHTML::makeOption('-1', '-- '.$title.' --'));

        $size = min(count($options), 10);

        $thisSelected = isset($selected[$filter]) ? convertArrayToOptions($selected[$filter]) : null;

        $list = mosHTML::selectList($options, $this->fieldPrefix.'['.$filter.'][]', 'class="inputbox" multiple size="'.$size.'" style="width:95%;"', 'value', 'text', $thisSelected);
        $list = preg_replace('/<option value="-1"[^>]*>([^<]*)<\/option>/iu', '<optgroup label="$1" />', $list);
        ?>
        <tr>
          <td valign="top"><?php echo $title; ?></td>
          <td valign="top"><?php echo $list; ?></td>
        </tr>
        <?php
    } // _displayFilter()


    function _displayCategory($params, $selected) {
        global $database;

        $title = $params->get('category_title', 'Category');
        $desc = $params->get('category_description');
        if (!empty($desc)) {
            $title = '<span class="editlinktip">'.mosToolTip(addslashes($desc), $title, '', '', $title, '#', 0).'</span>';
        }

        $database->setQuery('SELECT cat_id AS value, cat_name AS text'
            .' FROM #__jcalpro_categories'
            .' ORDER BY cat_name');
        $options = $database->loadObjectList();

        array_unshift($options, mosHTML::makeOption('-1', '-- '.$title.' --'));

        $size = min(count($options), 10);

        $thisSelected = isset($selected['category']) ? convertArrayToOptions($selected['category']) : null;

        $list = mosHTML::selectList($options, $this->fieldPrefix.'[category][]', 'class="inputbox" multiple size="'.$size.'" style="width:95%;"', 'value', 'text', $thisSelected);
        $list = preg_replace('/<option value="-1">(.*)<\/option>/iu', '<optgroup label="$1" />', $list);
        ?>
        <tr>
          <td valign="top"><?php echo $title; ?></td>
          <td valign="top"><?php echo $list; ?></td>
        </tr>
        <?php
    } // _displayCategory()


    function _displayDate($params, $selected) {

        $title = $params->get('date_title', 'Date');
        $desc = $params->get('date_description');
        if (!empty($desc)) {
            $title = '<span class="editlinktip">'.mosToolTip(addslashes($desc), $title, '', '', $title, '#', 0).'</span>';
        }
        if (!isset($selected['date']['start'])) $selected['date']['start'] = null;
        if (!isset($selected['date']['end'])) $selected['date']['end'] = null;
        ?>
        <tr>
          <td valign="top"><?php echo $title; ?></td>
          <td valign="top">
            <table>
              <tr>
                <td><?php echo $params->get('date_from', 'From'); ?></td>
                <td>
                  <input class="text_area" type="text" name="<?php echo $this->fieldPrefix; ?>[date][start]" id="<?php echo $this->fieldPrefix; ?>_date_start" size="25" maxlength="19" value="<?php echo $selected['date']['start']; ?>" />
                  <input name="reset" type="reset" class="button" onClick="return showCalendar('<?php echo $this->fieldPrefix; ?>_date_start', 'y-mm-dd');" value="...">
                </td>
              </tr>
              <tr>
                <td><?php echo $params->get('date_to', 'To'); ?></td>
                <td>
                  <input class="text_area" type="text" name="<?php echo $this->fieldPrefix; ?>[date][end]" id="<?php echo $this->fieldPrefix; ?>_date_end" size="25" maxlength="19" value="<?php echo $selected['date']['end']; ?>" />
                  <input name="reset" type="reset" class="button" onClick="return showCalendar('<?php echo $this->fieldPrefix; ?>_date_end', 'y-mm-dd');" value="...">
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <?php
    } // _displayDate()



    /**
     * processMessage
     *
     * Method to process a plugin send
     * Retrieves all of the users
     *
     * @param int $messageid Id of the current Message
     * @param int $pluginid Id of the current plugin
     * @param string $option Current Joomla Option
     * @param object $row The row of message data to be sent
     * @param int $userid The Id of the user being processed. May not be a joomla user id
     */
    function processMessage($messageid, $pluginid, $option, &$row, $userid) {
        static $replace = null;

        $params = $this->getParams($pluginid);
        $pluginData = $this->getMessageData($messageid, $pluginid);

        if ($replace===null) {
            $replace = $this->determineReplacement($pluginid, $row, $params, $pluginData);
        }

        $row->message_html = preg_replace('/\[JCAL\](.*)\[\/JCAL\]/s', $replace->html, $row->message_html);
        $row->message_text = preg_replace('/\[JCAL\](.*)\[\/JCAL\]/s', $replace->text, $row->message_text);

    } // processMessage()


    /**
     * Code to determine what to replace
     */
    function determineReplacement($pluginid, $row, $params, $pluginData) {
        global $database, $mosConfig_live_site;

        $replace = new stdClass();
        $replace->html = '';
        $replace->text = '';

        // first line of validation
        // plugin not valid
        if (!$this->validatePlugin($pluginid)) return $replace;

        // no options selected
        if (empty($pluginData)) return $replace;

        // details turned off
        if (!$params->get('details', 1)) return $replace;

        // no extra sections selected
        if (empty($pluginData['extra_selected'])) return $replace;

        // determine the html replacement
        $matches = null;
        if (!preg_match_all("/\[JCAL\](.*)\[\/JCAL\]/s", $row->message_html, $matches, PREG_SET_ORDER)) {
            $htmlSection = '';
        } else {
            $htmlSection = $matches[0][1];
        }
        if (!preg_match_all("/\[JCAL\](.*)\[\/JCAL\]/s", $row->message_text, $matches, PREG_SET_ORDER)) {
            $textSection = '';
        } else {
            $textSection = $matches[0][1];
        }
        unset($matches);

        // determine what should be displayed
        $sql = '';
        switch ($pluginData['extra_selected']) {
            case 'extra_category':
                $sql = 'SELECT e.*'
                    .' FROM #__jcalpro_events e'
                    .' WHERE e.published=1 AND e.approved=1'
                    .' AND e.cat='.(int)$pluginData['extra_category']
                    .' ORDER BY e.start_date, e.title';
                break;
            case 'extra_event':
                $sql = 'SELECT e.*'
                    .' FROM #__jcalpro_events e'
                    .' WHERE e.published=1 AND e.approved=1'
                    .' AND e.extid IN ('.implode(',', $pluginData['extra_event']).')'
                    .' ORDER BY e.start_date, e.title';
                break;
            case 'extra_date':
                $start = $pluginData['extra_date']['start'];
                $end = $pluginData['extra_date']['end'];

                // determine how to find the data
                if (!empty($start) || !empty($end)) {
                    // fix nevers
                    if (strtolower($start)=='never') $start = '0000-00-00';
                    if (empty($start)) $start = '0000-00-00';
                    if (strtolower($end)=='never') $end = '0000-00-00';
                    if (empty($end)) $end = '0000-00-00';

                    // fix dates if reversed
                    if ($end < $start) {
                        if ($start!='0000-00-00' && $end!='0000-00-00') {
                            $temp = $start;
                            $start = $end;
                            $end = $temp;
                        }
                    }
                    $where = '';

                    if ($start==$end) {
                        $end = explode('-', $start);
                        $end = date('Y-m-d', mktime(0, 0, 0, $end[1], $end[2]+1, $end[0]));
                        $where = ' AND (e.start_date BETWEEN '
                            .$database->Quote($start).' AND '
                            .$database->Quote($end).')';
                    } else if (empty($start) || $start=='0000-00-00') {
                        $where = ' AND (e.start_date <= '.$database->Quote($end).')';
                    } else if (empty($end) || $end=='0000-00-00') {
                        $where = ' AND (e.start_date >= '.$database->Quote($start).')';
                    } else {
                        $end = explode('-', $end);
                        $end = date('Y-m-d', mktime(0, 0, 0, $end[1], $end[2]+1, $end[0]));
                        $where = ' AND (e.start_date  BETWEEN '
                            .$database->Quote($start).' AND '
                            .$database->Quote($end).')';
                    }

                    if (!empty($where)) {
                        $sql = 'SELECT e.*'
                            .' FROM #__jcalpro_events e'
                            .' WHERE e.published=1 AND e.approved=1'
                            .$where
                            .' ORDER BY e.start_date, e.title';
                    }
                }
                break;
            case 'extra_next':
                $sql = 'SELECT e.*'
                    .' FROM #__jcalpro_events e'
                    .' WHERE e.published=1 AND e.approved=1'
                    .' AND e.start_date > now()'
                    .' ORDER BY e.start_date, e.title';
                break;
            default:
                return $replace;
                break;
        }

        if ($sql=='') return $replace;

        if (!empty($pluginData['extra_numevents'])) {
            $limit = (int) $pluginData['extra_numevents'];
        }

        if (empty($limit)) {
            $limit = $params->get('extra_list_default', 1);
        }

        // run the query
        $database->setQuery($sql, 0, $limit);
        $events = $database->loadObjectList();

        // try to determine the Itemid to use to create the more link
        $database->setQuery('SELECT id FROM #__menu'
            .' WHERE `type`='.$database->Quote('components')
            .' AND `link` like '.$database->Quote('%option=com_jcalpro%')
            .' AND `published`=1 and `access`=0');
        $itemid = $database->loadResult();
        $ItemidStr = '';
        if (!empty($itemid)) {
            $ItemidStr = '&amp;Itemid='.$itemid;
        }

        if (!is_array($events)) return $replace;

        // replace the content
        foreach ($events as $event) {
            $html = $htmlSection;
            $text = $textSection;

            $startDate = strftime(_DATE_FORMAT_LC2, strtotime($event->start_date));
            $endDate = strftime(_DATE_FORMAT_LC2, strtotime($event->end_date));
            $event->description = html_entity_decode(trim($event->description));
            $event->contact = html_entity_decode(trim($event->contact));

            $readMore = 'index.php?option=com_jcalpro&extmode=view'
                .'&extid='.(int)$event->extid.$ItemidStr;

            if (function_exists('sefRelToAbs') && !defined('_JLEGACY')) {
                $readMore = sefRelToAbs($readMore);
            } else {
                $readMore = $mosConfig_live_site.'/'.$readMore;
            }

            $href = '<a href="'.$event->url.'">'.$event->url.'</a>';
            $mailto = '<a href="mailto:'.$event->email.'">'.$event->email.'</a>';
            $readMoreLink = '<a href="'.$readMore.'">'._READ_MORE.'</a>';

            $html = str_replace('[JCAL_TITLE]', $event->title, $html);
            $html = str_replace('[JCAL_DESCRIPTION]', $event->description, $html);
            $html = str_replace('[JCAL_CONTACT]', $event->contact, $html);
            $html = str_replace('[JCAL_EMAIL]', $event->email, $html);
            $html = str_replace('[JCAL_MAILTO]', $mailto, $html);
            $html = str_replace('[JCAL_URL]', $event->url, $html);
            $html = str_replace('[JCAL_LINK]', $href, $html);
            $html = str_replace('[JCAL_START]', $startDate, $html);
            $html = str_replace('[JCAL_END]', $endDate, $html);
            $html = str_replace('[JCAL_READMORE]', $readMore, $html);
            $html = str_replace('[JCAL_READMORELINK]', $readMoreLink, $html);

            $text = str_replace('[JCAL_TITLE]', $event->title, $text);
            $text = str_replace('[JCAL_DESCRIPTION]', strip_tags($event->description), $text);
            $text = str_replace('[JCAL_CONTACT]', strip_tags($event->contact), $text);
            $text = str_replace('[JCAL_EMAIL]', $event->email, $text);
            $text = str_replace('[JCAL_MAILTO]', $event->email, $text);
            $text = str_replace('[JCAL_URL]', $event->url, $text);
            $text = str_replace('[JCAL_LINK]', $event->url, $text);
            $text = str_replace('[JCAL_START]', $startDate, $text);
            $text = str_replace('[JCAL_END]', $endDate, $text);
            $text = str_replace('[JCAL_READMORE]', $readMoreLink, $text);
            $text = str_replace('[JCAL_READMORELINK]', $readMoreLink, $text);

            $replace->html .= $html;
            $replace->text .= $text;
        }

        $replace->html = str_replace('$', '\$', $replace->html);
        $replace->text = str_replace('$', '\$', $replace->text);

        return $replace;
    } // determineReplacement()


    /**
     * processSend
     *
     * Method to process a plugin send
     * Retrieves all of the users
     * Returns an array of name,email objects
     */
    function processSend($messageid, $pluginid, $option) {
        global $database;

        if (!$this->validatePlugin($pluginid)) return false;

        $params = $this->getParams($pluginid);

        // determine which users to send to
        $pluginData = $this->getMessageData($messageid, $pluginid);

        if (empty($pluginData)) return false;

        $return = false;

        // determine the tablename to process into
        $tablename = determineQueueProcessTable();

        // process the shopper filter
        if ($params->get('category', 1)) {
            if (!empty($pluginData['category'])) {
                // find the contacts for this category that are also subscribed
                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT '.(int)$messageid.', -u.subscriberid, '.(int)$pluginid.', u.name, u.email, u.receive_html'
                    .' FROM #__emktg_subscriber u'
                    .' INNER JOIN #__jcalpro_events e ON u.email = e.email'
                    .' WHERE (e.cat IN ('.implode(', ', $pluginData['category']).'))'
                    .' AND u.confirmed=1 AND u.unsubscribe_date=0'
                    .' ORDER BY u.name');
                $database->query();

                // find the contacts for this category that are also registered subscribers
                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' from #__users u'
                    .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' INNER JOIN #__jcalpro_events e ON u.email = e.email'
                    .' WHERE (e.cat IN ('.implode(', ', $pluginData['category']).'))'
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date=0'
                    .' ORDER BY u.name');
                $database->query();
                $return = true;
            }
        }

        // process the billing country filter
        if ($params->get('event', 1)) {
            if (!empty($pluginData['event'])) {
                // find the contacts for this category that are also subscribed
                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT '.(int)$messageid.', -u.id, '.(int)$pluginid.', u.name, u.email, u.receive_html'
                    .' FROM #__emktg_subscriber u'
                    .' INNER JOIN #__jcalpro_events e ON u.email = e.email'
                    .' WHERE (e.extid IN ('.implode(', ', $pluginData['event']).'))'
                    .' AND u.confirmed=1 AND u.unsubscribe_date=0'
                    .' ORDER BY u.name');
                $database->query();

                // find the contacts for this category that are also registered subscribers
                $database->setQuery('INSERT IGNORE INTO '.$tablename
                    .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                    .' SELECT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                    .' from #__users u'
                    .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                    .' INNER JOIN #__jcalpro_events e ON u.email = e.email'
                    .' WHERE (e.extid IN ('.implode(', ', $pluginData['event']).'))'
                    .' AND eu.confirmed=1 AND eu.unsubscribe_date=0'
                    .' ORDER BY u.name');
                $database->query();
                $return = true;
            }
        }

        // process the order date filter
        if ($params->get('date', 1)) {
            $start = $pluginData['date']['start'];
            $end = $pluginData['date']['end'];

            // determine how to find the data
            if (!empty($start) || !empty($end)) {
                // fix nevers
                if (strtolower($start)=='never') $start = '0000-00-00';
                if (empty($start)) $start = '0000-00-00';
                if (strtolower($end)=='never') $end = '0000-00-00';
                if (empty($end)) $end = '0000-00-00';

                // fix dates if reversed
                if ($end < $start) {
                    if ($start!='0000-00-00' && $end!='0000-00-00') {
                        $temp = $start;
                        $start = $end;
                        $end = $temp;
                    }
                }
                $where = '';

                if ($start==$end) {
                    $end = explode('-', $start);
                    $end = date('Y-m-d', mktime(0, 0, 0, $end[1], $end[2]+1, $end[0]));
                    $where = ' WHERE (e.start_date BETWEEN '
                        .$database->Quote($start).' AND '
                        .$database->Quote($end).')';
                } else if (empty($start) || $start=='0000-00-00') {
                    $where = ' WHERE (e.start_date <= '.$database->Quote($end).')';
                } else if (empty($end) || $end=='0000-00-00') {
                    $where = ' WHERE (e.start_date >= '.$database->Quote($start).')';
                } else {
                    $end = explode('-', $end);
                    $end = date('Y-m-d', mktime(0, 0, 0, $end[1], $end[2]+1, $end[0]));
                    $where = ' WHERE (e.start_date  BETWEEN '
                        .$database->Quote($start).' AND '
                        .$database->Quote($end).')';
                }

                if (!empty($where)) {
                    // find the contacts for this date range that are also subscribed
                    $database->setQuery('INSERT IGNORE INTO '.$tablename
                        .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                        .' SELECT '.(int)$messageid.', -u.id, '.(int)$pluginid.', u.name, u.email, u.receive_html'
                        .' FROM #__emktg_subscriber u'
                        .' INNER JOIN #__jcalpro_events e ON u.email = e.email'
                        .$where
                        .' AND u.confirmed=1 AND u.unsubscribe_date=0'
                        .' ORDER BY u.name');
                    $database->query();

                    // find the contacts for this date range that are also registered subscribers
                    $database->setQuery('INSERT IGNORE INTO '.$tablename
                        .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
                        .' SELECT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, eu.receive_html'
                        .' from #__users u'
                        .' INNER JOIN #__emktg_user eu ON eu.id = u.id'
                        .' INNER JOIN #__jcalpro_events e ON u.email = e.email'
                        .$where
                        .' AND eu.confirmed=1 AND eu.unsubscribe_date=0'
                        .' ORDER BY u.name');
                    $database->query();
                    $return = true;
                }
            }
        }

        return $return;

    } // processSend()

} // class sm2emailmarketing_plugin_JCalPro

$obj = new sm2emailmarketing_plugin_JCalPro();
return $obj;
?>