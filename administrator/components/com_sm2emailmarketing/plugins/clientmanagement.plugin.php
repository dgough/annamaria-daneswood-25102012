<?php
// $Id: clientmanagement.plugin.php,v 1.21 2007/09/25 04:52:10 sm2tony Exp $
/**
 * sm2emailmarketing - Email Marketing Newsletter
 *
 * Joomla! Newsletter Component that supports multiple list mailing and message queueing
 *
 * @package Joomla!
 * @subpackage com_sm2emailmarketing
 * @copyright Copyright (C) 2006 SM2 / All Rights Reserved
 * @license commercial
 * @author SM2 <info@sm2joomla.com>
 */

/**
 * Makes sure this is included by a parent file
 */
defined('_VALID_MOS') or die('Direct Access to this location is not allowed.');

class sm2emailmarketing_plugin_clientmanagement extends sm2emPluginObj {

    var $name = 'SM2 Email Marketing Client Management';
    var $filename = 'clientmanagement.plugin.php';
    var $description = 'Allows SM2 Email Marketing to send messages to users based on options within SM2 Client Management';
    var $classname = 'sm2emailmarketing_plugin_clientmanagement';
    var $fieldPrefix = 'fields';

    /**
     * Constructor
     */
    function sm2emailmarketing_plugin_clientmanagement() {
    } // sm2emailmarketing_plugin_clientmanagement()


    /**
     * validatePlugin
     *
     * Method to validate if the plugin is usable
     */
    function validatePlugin($pluginid=null) {
        global $mosConfig_absolute_path;

        if (file_exists($mosConfig_absolute_path.'/administrator/components/com_member')) {
            return true;
        } else {
            return false;
        }
    } // validatePlugin()


    /**
     * displayMessageEdit
     *
     * Method to display the fields on the Message Management form
     */
    function displayMessageFilters($messageid, $pluginid, $option) {
        global $database, $mosConfig_absolute_path;

        if (!$this->validatePlugin($pluginid)) return false;

        $params = $this->getParams($pluginid);

        // get the selected lists
        $selected = $this->getMessageData($messageid, $pluginid);

        // start output buffering
        ob_start();
        ?>
        <tr>
          <th colspan="2">
          	<?php echo $params->get('section_title', 'Client Management'); ?>
          </th>
        </tr>
        <tr>
          <td colspan="2">
          	<?php echo $params->get('section_description'); ?>
          </td>
        </tr>
        <?php
        // display the organisation list
        // get the individual member first
        $database->setQuery('SELECT orgid, name FROM #__member_organisation '
            .' WHERE orgid=1');
        $ind = null;
        $database->loadObject($ind);
        if (!is_object($ind)) {
            $ind = new stdClass();
            $ind->orgid=1;
            $ind->name='Individual';
        }

        // get the rest
        $database->setQuery('SELECT orgid, name FROM #__member_organisation '
            .' WHERE published=1 AND orgid !=1 ORDER BY name');
        $orgs = $database->loadObjectList();
        array_unshift($orgs, $ind);

        $test='';
        if (is_array($selected)) {
            if (array_key_exists('orgid',$selected)) {
                $test = $selected['orgid'];
            }
        }
        if (!is_array($test)) $test = array();
        $size = count($orgs);
        if ($size > 10) $size=10;
        $fieldHTML = '<select name="'.$this->fieldPrefix.'[orgid][]" size="'.(int)$size.'" style="width: 600px;" multiple>';
        foreach ($orgs as $row) {
            $fieldHTML .= '<option value="'.(int)$row->orgid.'"';
            if (in_array($row->orgid, $test)) $fieldHTML .= ' selected';
            $fieldHTML .='>'.$row->name.'</option>';
        }
        $fieldHTML .= '</select>';
        ?>
    <tr>
      <td><strong>Organisation:</strong></td>
      <td> Hold control (or command) to select multiple </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td> <?php echo $fieldHTML; ?> <br>
        <br>
      </td>
    </tr>
    <?php


        // find all of the fields that can be used
        $qry = 'SELECT fieldid, title, type FROM #__member_fields'
            .' WHERE `type` IN ('
                .$database->Quote('checkbox')
                .','.$database->Quote('multicheckbox')
                .','.$database->Quote('select')
                .','.$database->Quote('multiselect')
                .','.$database->Quote('radio').')'
            .' AND sys=0 AND published=1'
            .' ORDER BY tabid, ordering';
        $database->setQuery($qry);
        $fields = $database->loadObjectList();
        foreach ($fields as $fieldObj) {
            // get the first couple of fields
            $fieldid = $fieldObj->fieldid;
            $title = $fieldObj->title;

            // get the fieldvalues
            $qry = 'SELECT fieldvalueid, fieldtitle FROM #__member_field_values WHERE fieldid='.(int)$fieldid.' ORDER BY ordering';
            $database->setQuery($qry);
            $fValues = $database->loadObjectList();

            switch ($fieldObj->type) {
                case 'checkbox':
                    $fieldHTML = '<input type="'.$fieldObj->type.'" name="'.$this->fieldPrefix.'['.$fieldid.']" value="1" ';
                    if (is_array($selected)) {
                        if (array_key_exists($fieldid,$selected)) {
                            if ($selected[$fieldid]==1) $fieldHTML .= 'checked ';
                        }
                    }
                    $fieldHTML .= '/>';
                    $msg = '';
                    break;
                case 'radio':
                case 'multicheckbox':
                case 'select':
                case 'multiselect':
                    $size = count($fValues);
                    if ($size > 10) $size=10;
                    $test='';
                    if (is_array($selected)) {
                        if (array_key_exists($fieldid,$selected)) {
                            $test = $selected[$fieldid];
                        }
                    }
                    if (!is_array($test)) $test = array();
                    $fieldHTML = '<select name="'.$this->fieldPrefix.'['.$fieldid.'][]" size="'.(int)$size.'" style="width: 600px;" multiple>';
                    foreach ($fValues as $row) {
                        $fieldHTML .= '<option value="'.$row->fieldvalueid.'"';
                        if (in_array($row->fieldvalueid, $test)) $fieldHTML .= ' selected';
                        $fieldHTML .= '>'.$row->fieldtitle.'</option>';
                    }
                    $fieldHTML .= '</select>';
                    $msg = 'Hold control (or command) to select multiple';
                    break;
            }
?>
    <tr>
      <td><strong><?php echo $title; ?>:</strong></td>
      <td> <?php echo $msg; ?> </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td> <?php echo $fieldHTML; ?> <br>
        <br>
      </td>
    </tr>


        <?php
        }

        $result = ob_get_contents();
        ob_end_clean();
        return $result;
    } // displayMessageFilters()


    /**
     * processMessageEdit
     *
     * Method to process the fields from the Message Management form
     * Data is stored in the emktg_message_plugin table
     */
    function processMessageEdit($messageid, $pluginid, $option) {
        global $database;

        if (!$this->validatePlugin($pluginid)) return false;

        // get the data passed by the form
        $data = mosGetParam($_POST, $this->fieldPrefix, array());

        sm2emailmarketingSetPluginMessageData($messageid, $pluginid, $data);

    } // processMessageEdit


    /**
     * processSend
     *
     * Method to process a plugin send
     * Retrieves all of the users
     * Returns an array of name,email objects
     */
    function processSend($messageid, $pluginid, $option) {
        global $database;

        if (!$this->validatePlugin($pluginid)) return false;

        $params = $this->getParams($pluginid);

        // determine which lists to get the recipients for
        $pluginData = $this->getMessageData($messageid, $pluginid);

        if (empty($pluginData)) return false;

        // determine the tablename to process into
        $tablename = determineQueueProcessTable();

        //base queries
        $sql_member = 'INSERT IGNORE INTO '.$tablename
            .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
            .' SELECT '.$messageid.', m.user_id, '.$pluginid.', u.name, u.email, eu.receive_html'
            .' FROM #__member m'
            .' INNER JOIN #__users u ON u.id = m.user_id'
            .' INNER JOIN #__emktg_user eu ON eu.id = m.user_id'
            .' WHERE eu.confirmed=1 and eu.unsubscribe_date=0';

        $sql_member_organisation = 'INSERT IGNORE INTO '.$tablename
            .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
            .' SELECT '.$messageid.', m.user_id, '.$pluginid.', u.name, u.email, eu.receive_html'
            .' FROM #__member m'
            .' INNER JOIN #__member_organisation mo ON mo.orgid = m.orgid'
            .' INNER JOIN #__users u ON u.id = m.id'
            .' INNER JOIN #__emktg_user eu ON eu.id = m.user_id'
            .' WHERE eu.confirmed=1 and eu.unsubscribe_date =0';

        // process the org data first
        if (is_array(@$pluginData['orgid'])) {
            $database->setQuery($sql_member.' AND m.orgid IN ('.implode(', ', $pluginData['orgid']).')');
            $database->query();
            unset($pluginData['orgid']);
        }

        // look for any custom fields
        $database->setQuery('SELECT fieldid, `name`, `table`, type FROM #__member_fields'
        	 	.' WHERE fieldid IN ('.implode(',', array_keys($pluginData)).')');
        $fields = $database->loadObjectList('fieldid');

        foreach ($pluginData as $fieldid=>$filter) {
        	if (!is_array($filter)) {
        		if($fields[$fieldid]->table == '#__member') {
    				$table = 'm';
    				$sql = $sql_member;
    			} else {
    				$table = 'mo';
    				$sql = $sql_member_organisation;
        		}
				$sql .= ' AND ('.$table.'.'.$fields[$fieldid]->name.' = '. $filter.')';
        	} else {
        		if($fields[$fieldid]->table == '#__member') {
    				$table = 'm';
    				$sql = $sql_member;
    			} else {
    				$table = 'mo';
    				$sql = $sql_member_organisation;
        		}

    			$sql .= ' AND (';
    			foreach($filter as $value) {
    				$sql .= $table.'.'.$fields[$fieldid]->name.' LIKE '
                        .$database->Quote('%'.$value.'%')
                        .' OR ';
    			}
        		$sql .= '1=0)';
        	}
        	$database->setQuery($sql);
        	$database->query();
        }

        return true;

    } // processSend()

} // class sm2emailmarketing_plugin_state

$obj = new sm2emailmarketing_plugin_clientmanagement();
return $obj;
?>