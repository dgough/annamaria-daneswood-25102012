<?php
// $Id: communitybuilder.plugin.php,v 1.10 2007/09/24 00:57:31 sm2tony Exp $
/**
 * sm2emailmarketing - Email Marketing Newsletter
 *
 * Joomla! Newsletter Component that supports multiple list mailing and message queueing
 *
 * @package Joomla!
 * @subpackage com_sm2emailmarketing
 * @copyright Copyright (C) 2006 SM2 / All Rights Reserved
 * @license commercial
 * @author SM2 <info@sm2joomla.com>
 */

/**
 * Makes sure this is included by a parent file
 */
defined('_VALID_MOS') or die('Direct Access to this location is not allowed.');

class sm2emailmarketing_plugin_communitybuilder extends sm2emPluginObj {

    var $name = 'Community Builder';
    var $filename = 'communitybuilder.plugin.php';
    var $description = 'Allows SM2 Email Marketing to send messages to users based on custom fields within Community Builder';
    var $classname = 'sm2emailmarketing_plugin_communitybuilder';
    var $fieldPrefix = 'communitybuilder';

    /**
     * Constructor
     */
    function sm2emailmarketing_plugin_communitybuilder() {
    } // sm2emailmarketing_plugin_communitybuilder()


    /**
     * validatePlugin
     *
     * Method to validate if the plugin is usable
     */
    function validatePlugin($pluginid=null) {
        global $mosConfig_absolute_path;

        if (file_exists($mosConfig_absolute_path.'/administrator/components/com_comprofiler')) {
            switch ($this->installCBPlugin()) {
                case 0:
                    echo '<div class="message">Community Builder Plugin installed.</div>';
                    break;
                case 1:
                    echo '<div class="message">Unable to copy Community Builder Plugin files.</div>';
                    break;
                case 2:
                    echo '<div class="message">Unable to determine if Community Builder Plugin is installed.</div>';
                    break;
                case 3:
                    echo '<div class="message">Unable to install Community Builder plugin.</div>';
                    break;
                case 4:
                    echo '<div class="message">Unable to create SM2 Email Marketing tab in Community Builder.</div>';
                    break;
            }
            return true;
        } else {
            return false;
        }
    } // validatePlugin()


    /**
     * displayMessageEdit
     *
     * Method to display the fields on the Message Management form
     */
    function displayMessageFilters($messageid, $pluginid, $option) {
        global $database, $mosConfig_absolute_path;

        if (!$this->validatePlugin($pluginid)) return false;

        $params = $this->getParams($pluginid);

        // get the selected lists
        $selected = $this->getMessageData($messageid, $pluginid);

        // start output buffering
        ob_start();
        ?>
        <tr>
          <th colspan="2">
          	<?php echo $params->get('section_title', 'Community Builder'); ?>
          </th>
        </tr>
        <tr>
          <td colspan="2">
          	<?php echo $params->get('section_description'); ?>
          </td>
        </tr>
    <?php

        // find all of the fields that can be used
        $qry = 'SELECT fieldid, title, type FROM #__comprofiler_fields'
            .' WHERE `table`='
                .$database->Quote('#__comprofiler')
            .' AND `type` IN ('
                .$database->Quote('checkbox')
                .','.$database->Quote('multicheckbox')
                .','.$database->Quote('select')
                .','.$database->Quote('multiselect')
                .','.$database->Quote('radio').')'
            .' AND sys=0 AND published=1'
            .' ORDER BY tabid, ordering';
        $database->setQuery($qry);
        $fields = $database->loadObjectList();
        foreach ($fields as $fieldObj) {
            // get the first couple of fields
            $fieldid = $fieldObj->fieldid;
            $title = $fieldObj->title;

            // get the fieldvalues
            $qry = 'SELECT fieldvalueid, fieldtitle FROM #__comprofiler_field_values WHERE fieldid='.(int)$fieldid.' ORDER BY ordering';
            $database->setQuery($qry);
            $fValues = $database->loadObjectList();

            switch ($fieldObj->type) {
                case 'checkbox':
                    $fieldHTML = '<input type="'.$fieldObj->type.'" name="'.$this->fieldPrefix.'['.$fieldid.']" value="1" ';
                    if (is_array($selected)) {
                        if (array_key_exists($fieldid,$selected)) {
                            if ($selected[$fieldid]==1) $fieldHTML .= 'checked ';
                        }
                    }
                    $fieldHTML .= '/>';
                    $msg = '';
                    break;
                case 'radio':
                case 'multicheckbox':
                case 'select':
                case 'multiselect':
                    $size = count($fValues);
                    if ($size > 10) $size=10;
                    $test='';
                    if (is_array($selected)) {
                        if (array_key_exists($fieldid,$selected)) {
                            $test = $selected[$fieldid];
                        }
                    }
                    if (!is_array($test)) $test = array();
                    $fieldHTML = '<select name="'.$this->fieldPrefix.'['.$fieldid.'][]" size="'.(int)$size.'" style="width: 600px;" multiple>';
                    foreach ($fValues as $row) {
                        $fieldHTML .= '<option value="'.$row->fieldvalueid.'"';
                        if (in_array($row->fieldvalueid, $test)) $fieldHTML .= ' selected';
                        $fieldHTML .= '>'.$row->fieldtitle.'</option>';
                    }
                    $fieldHTML .= '</select>';
                    $msg = 'Hold control (or command) to select multiple';
                    break;
            }
?>
    <tr>
      <td><strong><?php echo $title; ?>:</strong></td>
      <td> <?php echo $msg; ?> </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td> <?php echo $fieldHTML; ?> <br>
        <br>
      </td>
    </tr>


        <?php
        }

        $result = ob_get_contents();
        ob_end_clean();
        return $result;
    } // displayMessageFilters()


    /**
     * processMessageEdit
     *
     * Method to process the fields from the Message Management form
     * Data is stored in the emktg_message_plugin table
     */
    function processMessageEdit($messageid, $pluginid, $option) {
        global $database;

        if (!$this->validatePlugin($pluginid)) return false;

        // get the data passed by the form
        $data = mosGetParam($_POST, $this->fieldPrefix, array());

        sm2emailmarketingSetPluginMessageData($messageid, $pluginid, $data);

    } // processMessageEdit


    /**
     * processSend
     *
     * Method to process a plugin send
     * Retrieves all of the users
     * Returns an array of name,email objects
     */
    function processSend($messageid, $pluginid, $option) {
        global $database;

        if (!$this->validatePlugin($pluginid)) return false;

        $params = $this->getParams($pluginid);

        // determine which lists to get the recipients for
        $pluginData = $this->getMessageData($messageid, $pluginid);

        if (empty($pluginData)) return false;

        // determine the tablename to process into
        $tablename = determineQueueProcessTable();

        //base queries
        $sqlstart = 'INSERT IGNORE INTO '.$tablename
            .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
            .' SELECT '.$messageid.', c.user_id, '.$pluginid.', u.name, u.email, eu.receive_html'
            .' FROM #__comprofiler c '
            .' INNER JOIN #__users u ON u.id = c.user_id'
            .' INNER JOIN #__emktg_user eu ON eu.id = c.user_id'
            .' WHERE eu.confirmed=1 and eu.unsubscribe_date=0';

        // look for any custom fields
        $database->setQuery('SELECT fieldid, `name`, `type` FROM #__comprofiler_fields'
    	 	.' WHERE fieldid IN ('.implode(',', array_keys($pluginData)).')');
        $fields = $database->loadObjectList('fieldid');

        // get the fieldvalues
        $database->setQuery('SELECT fieldvalueid, fieldtitle FROM #__comprofiler_field_values'
            .' WHERE fieldid IN ('.implode(',', array_keys($pluginData)).')');
        $fieldValues = $database->loadObjectList('fieldvalueid');

        foreach ($pluginData as $fieldid=>$filter) {
            $sql = $sqlstart;
        	if (!is_array($filter)) {
				$sql .= ' AND (c.'.$fields[$fieldid]->name.' = '. $filter.')';
        	} else {
    			$sql .= ' AND (';
    			foreach($filter as $fieldValueId) {
                    $value = $fieldValues[$fieldValueId]->fieldtitle;
    				$sql .= 'concat('
                        .$database->Quote('|')
                        .', c.`'.$fields[$fieldid]->name.'`, '
                        .$database->Quote('|')
                        .') LIKE '
                        .$database->Quote('%|'.$database->getEscaped($value).'|%')
                        .' OR ';
    			}
        		$sql .= '1=0)';
        	}
        	$database->setQuery($sql);
        	$database->query();
        }

        return true;

    } // processSend()


    function installCBPlugin() {
        global $mosConfig_absolute_path, $database, $option;

        // create some paths
        $srcPath = $mosConfig_absolute_path.'/administrator/components/'.$option
            .'/plugins/plug_sm2emcbplugin';
        $destPath = $mosConfig_absolute_path
            .'/components/com_comprofiler/plugin/user/plug_sm2emcbplugin';

        // check to see if we need to transfer
        if (file_exists($srcPath.'/sm2em_cb.php')) {

            // see if the plugin is already installed
            if (is_dir($destPath)) {
                // copy the files instead
                if (@copy($srcPath.'/index.html', $destPath.'/index.html')) {
                    @unlink($srcPath.'/index.html');
                } else {
                    return 1;
                }
                if (@copy($srcPath.'/sm2em_cb.php', $destPath.'/sm2em_cb.php')) {
                    @unlink($srcPath.'/sm2em_cb.php');
                } else {
                    return 1;
                }
                if (@copy($srcPath.'/sm2em_cb.xml', $destPath.'/sm2em_cb.xml')) {
                    @unlink($srcPath.'/sm2em_cb.xml');
                } else {
                    return 1;
                }
                @unlink($srcPath);
            } else {

                // move the existing folder
                if (!@rename($srcPath, $destPath)) {
                    // unable to move the folder so display an error
                    return 1;
                }
            }

            // try to change the permissions on the folder now it has been moved
            @chmod($destPath, 0777);

            // try to change the permissions on the main plugin file
            if (is_file($destPath.'/sm2em_cb.php')) {
                @chmod($destPath.'/sm2em_cb.php', 0777);
            }

            // update database
            $database->setQuery('SELECT `id` FROM `#__comprofiler_plugin`'
                .' WHERE `folder` = '.$database->Quote('plug_sm2emcbplugin'));
            $id = (int) $database->loadResult();

            $error = $database->getErrorMsg();

            if (!empty($error)) {
                return 2;
            } else {

                // check for install
                if ($id < 1) {
                    // install the plugin
                    $plugin = new stdClass();
                    $plugin->id = 0;
                    $plugin->name = 'SM2 Email Marketing CB Plugin';
                    $plugin->element = 'sm2em_cb';
                    $plugin->type = 'user';
                    $plugin->folder = 'plug_sm2emcbplugin';
                    $plugin->ordering = 99;

                    if (!$database->insertObject('#__comprofiler_plugin', $plugin, 'id')) {
                        return 3;
                    }

                    // create the tab for the plugin
                    $tab = new stdClass();
                    $tab->tabid = 0;
                    $tab->title = 'SM2 Email Marketing';
                    $tab->description = 'Email Marekting options.';
                    $tab->ordering = 99;
                    $tab->width = '.5';
                    $tab->enabled = 0;
                    $tab->pluginclass = 'getSM2EMTab';
                    $tab->pluginid = $plugin->id;
                    $tab->params = null;
                    $tab->sys = 0;
                    $tab->displaytype = 'tab';
                    $tab->position = 'cb_tabmain';

                    if (!$database->insertObject('#__comprofiler_tabs', $tab, 'tabid')) {
                        return 4;
                    }
                    return 0;
                }

            }

        }

        return -1;
    } // installCBPlugin()

} // class sm2emailmarketing_plugin_communitybuilder

$obj = new sm2emailmarketing_plugin_communitybuilder();
return $obj;
?>