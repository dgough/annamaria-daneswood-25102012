<?php
// $Id: list.plugin.php,v 1.16 2007/09/24 00:57:31 sm2tony Exp $
/**
 * sm2emailmarketing - Email Marketing Newsletter
 *
 * Joomla! Newsletter Component that supports multiple list mailing and message queueing
 *
 * @package Joomla!
 * @subpackage com_sm2emailmarketing
 * @copyright Copyright (C) 2006 SM2 / All Rights Reserved
 * @license commercial
 * @author SM2 <info@sm2joomla.com>
 */

/**
 * Makes sure this is included by a parent file
 */
defined('_VALID_MOS') or die('Direct Access to this location is not allowed.');

class sm2emailmarketing_plugin_list extends sm2emPluginObj {

    var $name = 'SM2 Email Marketing List';
    var $filename = 'list.plugin.php';
    var $description = 'Allows SM2 Email Marketing to send messages to user subscribed to SM2 Email Marketing Lists';
    var $classname = 'sm2emailmarketing_plugin_list';
    var $fieldPrefix = 'sm2emailmarketing_listids';

    /**
     * Constructor
     */
    function sm2emailmarketing_plugin_list() {
    } // sm2emailmarketing_plugin_list()


    /**
     * displayMessageEdit
     *
     * Method to display the fields on the Message Management form
     */
    function displayMessageFilters($messageid, $pluginid, $option) {
        global $database;

        if (!$this->validatePlugin($pluginid)) return false;

        $params = $this->getParams($pluginid);

        // get the selected lists
        $selected = array();

        $pluginData = $this->getMessageData($messageid, $pluginid);

        if (!empty($pluginData)) {
            $selected = convertArrayToOptions($pluginData);
        }

        $listTitle = $params->get('list_title', 'Lists');
        if ($params->get('list_description')) {
            $listTitle = '<span class="editlinktip">'.mosToolTip(addslashes($params->get('list_description')), $listTitle, '', '', $listTitle, '#', 0).'</span>';
        }

        // get the list of lists
        $database->setQuery('SELECT listid AS value, list_name AS text FROM #__emktg_list ORDER BY list_name');
        $data = $database->loadObjectList();

        $size = min(count($data), 10);
        if ($size<1) {
            $lists = $params->get('no_lists', 'No lists available.');
        } else {
            $lists = mosHTML::selectList($data, $this->fieldPrefix.'[]', 'class="inputbox" size="'.$size.'" multiple', 'value', 'text', $selected);
        }

        // start output buffering
        ob_start();
        ?>
        <tr>
          <th colspan="2"><?php echo $params->get('section_title', 'List Recipients'); ?></th>
        </tr>
        <tr>
          <td colspan="2"><?php echo $params->get('section_description'); ?></td>
        </tr>
        <tr>
          <td valign="top"><?php echo $listTitle; ?></td>
          <td valign="top"><?php echo $lists; ?></td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
        <?php
        $result = ob_get_contents();
        ob_end_clean();
        return $result;
    } // displayMessageFilters()


    /**
     * processMessageEdit
     *
     * Method to process the fields from the Message Management form
     * Data is stored in the emktg_message_plugin table
     */
    function processMessageEdit($messageid, $pluginid, $option) {
        global $database;

        if (!$this->validatePlugin($pluginid)) return false;

        // get the data passed by the form
        $listids = mosGetParam($_POST, $this->fieldPrefix, array());

        sm2emailmarketingSetPluginMessageData($messageid, $pluginid, $listids);

    } // processMessageEdit


    /**
     * processSend
     *
     * Method to process a plugin send
     * Retrieves all of the users
     * Returns an array of name,email objects
     */
    function processSend($messageid, $pluginid, $option) {
        global $database;

        if (!$this->validatePlugin($pluginid)) return false;

        $params = $this->getParams($pluginid);

        // determine which lists to get the recipients for
        $pluginData = $this->getMessageData($messageid, $pluginid);

        if (empty($pluginData)) return false;

        $listids = implode(',', $pluginData);

        // determine the tablename to process into
        $tablename = determineQueueProcessTable();

        // find the registered users that have subscribed to this list
        $database->setQuery('INSERT IGNORE INTO '.$tablename
            .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
            .' SELECT '.(int)$messageid.', u.id, '.(int)$pluginid.', u.name, u.email, au.receive_html'
            .' FROM #__emktg_user au'
            .' INNER JOIN #__users u ON u.id = au.id'
            .' INNER JOIN #__emktg_list_user lu ON lu.id=u.id'
            .' WHERE lu.listid IN ('.$listids.')'
            .' AND au.confirmed=1 AND au.unsubscribe_date=0'
            .' ORDER BY u.name');
        $database->query();

        // find the unregistered users that have subscribed to this list
        $database->setQuery('INSERT IGNORE INTO '.$tablename
            .' (`messageid`, `userid`, `pluginid`, `name`, `email`, `html`)'
            .' SELECT '.(int)$messageid.', -s.subscriberid, '.(int)$pluginid.', s.name, s.email, s.receive_html'
            .' FROM #__emktg_subscriber s'
            .' INNER JOIN #__emktg_list_subscriber ls ON s.subscriberid=-ls.subscriberid'
            .' WHERE ls.listid IN ('.$listids.')'
            .' AND s.confirmed=1 AND s.unsubscribe_date=0'
            .' ORDER BY s.name');
        $database->query();

        return true;

    } // processSend()

} // class sm2emailmarketing_plugin_list

$obj = new sm2emailmarketing_plugin_list();
return $obj;
?>