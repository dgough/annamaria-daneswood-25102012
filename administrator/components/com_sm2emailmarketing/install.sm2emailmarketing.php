<?php
// $Id: install.sm2emailmarketing.php,v 1.15 2008/03/31 01:59:10 sm2tony Exp $
/**
 * sm2emailmarketing - Email Marketing Newsletter
 *
 * Joomla! Newsletter Component that supports multiple list mailing and message queueing
 *
 * @package Joomla!
 * @subpackage com_sm2emailmarketing
 * @copyright Copyright (C) 2006 SM2 / All Rights Reserved
 * @license commercial
 * @author SM2 <info@sm2joomla.com>
 */


/**
 *  Ensures this file is being included by a parent file
 */
defined('_VALID_MOS') or die('Direct Access to this location is not allowed.');

/**
 * Joomla! install function
 *
 * <p>Creates the alm directory in /images/stories and gives it the correct permissions.</p>
 *
 * @global string filesystem path to main site directory
 */
function com_install() {
    global $mosConfig_absolute_path, $database;

    // display some information about the component
?>
  <h3>SM2 Email Marketing</h3>
  <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminHeading">
    <tr>
      <td>
        <p><strong>SM2 Email Marketing</strong> is a Newsletter / Mailing List component
          that supports many features not seen in Joomla Newsletter components.</p>
        <p>Features of SM2 Email Marketing include:
          <ul>
            <li>Support for Joomla (Registered) and non-Joomla (unregistered) users.</li>
            <li>Plugin Architecture</li>
            <li>Message Queueing</li>
            <li>Sending messages to multiple lists</li>
            <li>Unique email sending</li>
            <li>Templating</li>
            <li>Front-end Registration</li>
            <li>Statistics</li>
            <li>Integration with SM2 Client Management Component</li>
          </ul>
        <p>
      </td>
    </tr>
  </table>
  <div class="message">Note: SM2 Email Marketing requires the free <a href="http://ioncube.com/loader/">ionCube PHP Loader</a> to be installed.</div>
<?php
    // ioncube  tests
    echo '<div>';
    if (!extension_loaded('ionCube Loader')) {
        // ioncube not loaded

        // check to see if we can load it using enable_dl
        $dl = ini_get('enable_dl');
        $sm = ini_get('safe_mode');
        if (!empty($sm)) {
            echo 'Warning: safe mode is enabled. You will have to edit the php.ini to load ionCube!';
        } else if (empty($dl)) {
            echo 'Warning: enable_dl is turned off. You will have to edit the php.ini to load inoCube!';
        } else {
            echo 'To install ionCube download the ionCube loader and copy the folder to the root of your Joomla site, or follow the contained instructions to install ionCube for your webserver.';
        }
    } else {
        echo 'ionCube loaded sucessfully';
    }
    echo '</div>';


    // add plugins
    require_once($mosConfig_absolute_path.'/administrator/components/com_sm2emailmarketing/sm2emailmarketing.class.php');
    require_once($mosConfig_absolute_path.'/administrator/components/com_sm2emailmarketing/includes/plugin.admin.php');
    echo 'Installing / Updating plugins... ';
    updatePlugins('com_sm2emailmarketing');
    echo 'Done.<br />';

    // create the folder for upgrades
    mosMakePath($mosConfig_absolute_path.'/administrator/components/com_sm2emailmarketing', '/upgrade', 0777);

    // create sm2emailmarketing image directory if it doesn't exist
    mosMakePath($mosConfig_absolute_path.'/images', '/sm2emailmarketing', 0777);

    // move the files used for the demo template to the images folder
    @copy($mosConfig_absolute_path.'/components/com_sm2emailmarketing/images/index.html',
        $mosConfig_absolute_path.'/images/sm2emailmarketing/index.html');
    @copy($mosConfig_absolute_path.'/components/com_sm2emailmarketing/images/logo.gif',
        $mosConfig_absolute_path.'/images/sm2emailmarketing/logo.gif');
    @copy($mosConfig_absolute_path.'/components/com_sm2emailmarketing/images/bullet.gif',
        $mosConfig_absolute_path.'/images/sm2emailmarketing/bullet.gif');
    @copy($mosConfig_absolute_path.'/components/com_sm2emailmarketing/images/mastheadBg.gif',
        $mosConfig_absolute_path.'/images/sm2emailmarketing/mastheadBg.gif');

    // update the menus
    if (defined('_JLEGACY')) {
        $menu_icon = 'js/ThemeOffice/messages.png';
    } else {
        $menu_icon = 'js/ThemeOffice/messaging_inbox.png';
    }
    echo 'Updating Components Menu... ';
    $database->setQuery('UPDATE #__components'
        .' SET `admin_menu_img`='.$database->Quote($menu_icon)
        .' WHERE `admin_menu_link`='.$database->Quote('option=com_sm2emailmarketing'));
    $database->query();

    echo 'Done.<br />';
    echo '<p>Click <a href="index2.php?option=com_sm2emailmarketing&task=installdefault">here</a>'
        .'to install a default Template and Message</p>';

} // com_install()
?>