<?php
// $Id: toolbar.sm2emailmarketing.html.php,v 1.12 2008/03/31 01:59:10 sm2tony Exp $
/**
 * sm2emailmarketing - Email Marketing Newsletter
 *
 * Joomla! Newsletter Component that supports multiple list mailing and message queueing
 *
 * @package Joomla!
 * @subpackage com_sm2emailmarketing
 * @copyright Copyright (C) 2006 SM2 / All Rights Reserved
 * @license commercial
 * @author SM2 <info@sm2joomla.com>
 */

/**
 *  Ensures this file is being included by a parent file
 */
defined('_VALID_MOS') or die('Direct Access to this location is not allowed.');

/**
 * Class to display html toolbars for backend
 *
 * <p>Creates the html toolbars using the mosMenuBar class.</p>
 *
 * @package sm2emailmarketing
 * @subpackage backend
 * @author Tony Blair <tonanbarbarian@gmail.com>
 */
class menu_sm2emailmarketing {

    /**
     * List Default Menu
     */
    function LIST_DEFAULT() {
        mosMenuBar::startTable();
        mosMenuBar::publishList('publishlist');
        mosMenuBar::unpublishList('unpublishlist');
        mosMenuBar::divider();
        mosMenuBar::custom('copylist', 'copy.png', 'copy_f2.png', _EMKTG_COPY, true);
        mosMenuBar::addNew('newlist');
        mosMenuBar::editList('editlist');
        mosMenuBar::deleteList('', 'deletelist');
        mosMenuBar::endTable();
    } // LIST_DEFAULT()

    function LIST_NEW() {
        mosMenuBar::startTable();
        mosMenuBar::save('savelist');
        mosMenuBar::apply('applylist');
        mosMenuBar::cancel('showlists');
        mosMenuBar::endTable();
    } // LIST_NEW()

    function LIST_EDIT() {
        mosMenuBar::startTable();
        mosMenuBar::save('savelist');
        mosMenuBar::apply('applylist');
        mosMenuBar::cancel('showlists');
        mosMenuBar::endTable();
    } // LIST_EDIT()

    /**
     * Template Tasks
     */
    function TEMPLATE_DEFAULT() {
        mosMenuBar::startTable();
        mosMenuBar::custom('previewtemplate', 'preview.png', 'preview_f2.png', _EMKTG_TOOLBAR_PREVIEW, true);
        mosMenuBar::divider();
        mosMenuBar::publishList('publishtemplate');
        mosMenuBar::unpublishList('unpublishtemplate');
        mosMenuBar::divider();
        mosMenuBar::custom('copytemplate', 'copy.png', 'copy_f2.png', _EMKTG_TOOLBAR_COPY, true);
        mosMenuBar::addNew('newtemplate');
        mosMenuBar::editList('edittemplate');
        mosMenuBar::deleteList('','deletetemplate');
        mosMenuBar::endTable();
    } // TEMPLATE_DEFAULT()

    function TEMPLATE_EDIT() {
        mosMenuBar::startTable();
        mosMenuBar::custom('applypreviewtemplate', 'preview.png', 'preview_f2.png', _EMKTG_TOOLBAR_PREVIEW, false);
        mosMenuBar::divider();
        mosMenuBar::save('savetemplate');
        mosMenuBar::apply('applytemplate');
        mosMenuBar::cancel('showtemplates');
        mosMenuBar::endTable();
    } // TEMPLATE_EDIT()

    function TEMPLATE_PREVIEW() {
        mosMenuBar::startTable();
        mosMenuBar::cancel();
        mosMenuBar::endTable();
    } // TEMPLATE_PREVIEW

    /**
     * Registered User Tasks
     */
    function REGISTERED_DEFAULT() {
        mosMenuBar::startTable();
        mosMenubar::custom('unsubscriberegistered', 'cancel.png', 'cancel_f2.png', _EMKTG_TOOLBAR_UNSUBSCRIBE, true);
        mosMenuBar::divider();
        mosMenuBar::custom('confirmregistered', 'apply.png', 'apply_f2.png', _EMKTG_TOOLBAR_CONFIRM, true);
        mosMenuBar::custom('unconfirmregistered', 'cancel.png', 'cancel_f2.png', _EMKTG_TOOLBAR_UNCONFIRM, true);
        mosMenuBar::divider();
        mosMenuBar::custom('htmlregistered', 'html.png', 'html_f2.png', _EMKTG_TOOLBAR_HTML, true);
        mosMenuBar::custom('unhtmlregistered', 'edit.png', 'edit_f2.png', _EMKTG_TOOLBAR_TEXT, true);
        mosMenuBar::divider();
        mosMenuBar::editList('editregistered');
        mosMenuBar::endTable();
    } // REGISTERED_DEFAULT()

    function REGISTERED_EDIT() {
        mosMenuBar::startTable();
        mosMenuBar::save('saveregistered');
        mosMenuBar::apply('applyregistered');
        mosMenuBar::cancel('showregistered');
        mosMenuBar::endTable();
    } // REGISTERED_EDIT()


    /**
     * Unregistered User Tasks
     */
    function UNREGISTERED_DEFAULT() {
        mosMenuBar::startTable();
        mosMenuBar::custom('importunregistered', 'upload.png', 'upload_f2.png', _EMKTG_TOOLBAR_IMPORT, false);
        mosMenuBar::divider();
        mosMenubar::custom('unsubscribeunregistered', 'cancel.png', 'cancel_f2.png', _EMKTG_TOOLBAR_UNSUBSCRIBE, true);
        mosMenuBar::divider();
        mosMenuBar::custom('confirmunregistered', 'apply.png', 'apply_f2.png', _EMKTG_TOOLBAR_CONFIRM, true);
        mosMenuBar::custom('unconfirmunregistered', 'cancel.png', 'cancel_f2.png', _EMKTG_TOOLBAR_UNCONFIRM, true);
        mosMenuBar::divider();
        mosMenuBar::custom('htmlunregistered', 'html.png', 'html_f2.png', _EMKTG_TOOLBAR_HTML, true);
        mosMenuBar::custom('unhtmlunregistered', 'edit.png', 'edit_f2.png', _EMKTG_TOOLBAR_TEXT, true);
        mosMenuBar::divider();
        mosMenuBar::addNew('newunregistered');
        mosMenuBar::editList('editunregistered');
        mosMenuBar::deleteList('', 'deleteunregistered');
        mosMenuBar::endTable();
    } // UNREGISTERED_DEFAULT()

    function UNREGISTERED_EDIT() {
        mosMenuBar::startTable();
        mosMenuBar::save('saveunregistered');
        mosMenuBar::apply('applyunregistered');
        mosMenuBar::cancel('showunregistered');
        mosMenuBar::endTable();
    } // UNREGISTERED_EDIT()

    function UNREGISTERED_IMPORT() {
        mosMenuBar::startTable();
        if (!isset($GLOBALS['sm2em_import_processed'])) {
            mosMenuBar::custom('importunregistered', 'forward.png', 'forward_f2.png', _CMN_NEXT, false);
        }
        mosMenuBar::cancel('showunregistered');
        mosMenuBar::endTable();
    } // UNREGISTERED_IMPORT()

    /**
     * Message Tasks
     */
    function MESSAGE_DEFAULT() {
        mosMenuBar::startTable();
        mosMenuBar::custom('previewmessage', 'preview.png', 'preview_f2.png', _EMKTG_TOOLBAR_PREVIEW, true);
        mosMenuBar::custom('copymessage', 'copy.png', 'copy_f2.png', _EMKTG_TOOLBAR_COPY, true);
        mosMenuBar::addNew('newmessage');
        mosMenuBar::editList('editmessage');
        mosMenuBar::deleteList('', 'deletemessage');
        mosMenuBar::endTable();
    } // MESSAGE_DEFAULT()

    function MESSAGE_PREVIEW() {
    	mosMenuBar::startTable();
        mosMenuBar::custom('sendtestmessage', 'upload.png', 'upload_f2.png', _EMKTG_TOOLBAR_SENDTEST, false);
        mosMenuBar::divider();
        menu_sm2emailmarketing::customConfirm('sendmessage', 'upload.png', 'upload_f2.png', _EMKTG_TOOLBAR_SEND, _EMKTG_TOOLBAR_SEND_CONFIRM);
        mosMenuBar::divider();
        mosMenuBar::cancel('showmessages');
        mosMenuBar::endTable();
    }

    function MESSAGE_EDIT() {
        mosMenuBar::startTable();
        mosMenuBar::custom('savepreviewmessage', 'preview.png', 'preview_f2.png', _EMKTG_TOOLBAR_PREVIEW, false);
        mosMenuBar::divider();
        mosMenuBar::save('savemessage');
        mosMenuBar::apply('applymessage');
        mosMenuBar::cancel('showmessages');
        mosMenuBar::endTable();
    } // MESSAGE_EDIT()

    function MESSAGE_CHECK() {
        mosMenuBar::startTable();
        mosMenuBar::custom('previewmessage', 'preview.png', 'preview_f2.png', _EMKTG_TOOLBAR_PREVIEW, false);
        mosMenuBar::custom('copymessage', 'copy.png', 'copy_f2.png', _EMKTG_TOOLBAR_COPY, false);
        mosMenuBar::cancel('showmessages');
        mosMenuBar::endTable();
    } // MESSAGE_CHECK()

    /**
     * Statistics Tasks
     */
    function STATISTICS_DEFAULT() {
        mosMenuBar::startTable();
        mosMenuBar::custom('showmessagestatistics', 'preview.png', 'preview_f2.png', _EMKTG_TOOLBAR_VIEW, true);
        mosMenuBar::endTable();
    } // STATISTICS_DEFAULT()

    function STATISTICS_VIEW() {
        mosMenuBar::startTable();
        mosMenuBar::cancel('showstatistics');
        mosMenuBar::endTable();
    }

    /**
     * Queue Tasks
     */
    function QUEUE_DEFAULT() {
        mosMenuBar::startTable();
        mosMenuBar::custom('processqueue', 'upload.png', 'upload_f2.png', _EMKTG_TOOLBAR_PROCESS, true);
        mosMenuBar::deleteList('', 'deletequeue');
        mosMenuBar::endTable();
    } // QUEUE_DEFAULT()

    function QUEUE_PROCESS() {
        mosMenuBar::startTable();
        mosMenuBar::custom('processqueue', 'forward.png', 'forward_f2.png', _EMKTG_TOOLBAR_NEXTBATCH, false);
        mosMenuBar::cancel('showqueue');
        mosMenuBar::endTable();
    } // QUEUE_PROCESS()

    /**
     * Plugin Tasks
     */
    function PLUGIN_DEFAULT() {
        mosMenuBar::startTable();
        mosMenuBar::custom('enableplugin', 'apply.png', 'apply_f2.png', _EMKTG_TOOLBAR_ENABLE, true);
        mosMenuBar::custom('disableplugin', 'cancel.png', 'cancel_f2.png', _EMKTG_TOOLBAR_DISABLE, true);
        mosMenuBar::editList('editplugin');
        mosMenuBar::endTable();
    } // PLUGIN_DEFAULT()

    function PLUGIN_EDIT() {
        mosMenuBar::startTable();
        mosMenuBar::save('saveplugin');
        mosMenuBar::apply('applyplugin');
        mosMenuBar::cancel('showplugins');
        mosMenuBar::endTable();
    } // PLUGIN_EDIT()

    /**
     * Configuration Tasks
     */
    function CONFIGURATION_DEFAULT() {
        mosMenuBar::startTable();
        mosMenuBar::save('saveconfiguration');
        mosMenuBar::apply('applyconfiguration');
        mosMenuBar::cancel();
        mosMenuBar::endTable();
    } // CONFIGURATION_DEFAULT()

    /**
    * Writes a custom option and task button for the button bar with confirmation
    * @param string The task to perform (picked up by the switch($task) blocks
    * @param string The image to display
    * @param string The image to display when moused over
    * @param string The alt text for the icon image
    * @param boolean True if required to check that a standard list item is checked
    */
    function customConfirm( $task='', $icon='', $iconOver='', $alt='', $confirmMessage=_EMKTG_TOOLBAR_CUSTCONFIRM ) {
        if (defined('_JLEGACY')) {
            $bar = & JToolBar::getInstance('toolbar');
            $icon = explode('.', $icon);
            $icon = reset($icon);
            $bar->appendButton( 'Confirm', $confirmMessage, $icon, $alt, $task, false, false);

            return;
        }
        $icon   = ( $iconOver ? $iconOver : $icon );
        $image  = mosAdminMenus::ImageCheckAdmin( $icon, '/administrator/images/', NULL, NULL, $alt, $task, 1, 'middle', $alt );


        $href = "javascript:if (confirm('$confirmMessage')){ submitbutton('$task');}";

        if ($icon && $iconOver) {
            ?>
            <td>
                <a class="toolbar" href="<?php echo $href;?>">
                    <?php echo $image; ?>
                    <br /><?php echo $alt; ?></a>
            </td>
            <?php
        } else {
            ?>
            <td>
                <a class="toolbar" href="<?php echo $href;?>">
                    <br /><?php echo $alt; ?></a>
            </td>
            <?php
        }

    } // customConfirm()

} // class menu_sm2emailmarketing()

