<?php
// $Id: list.admin.php,v 1.16 2008/03/31 01:59:10 sm2tony Exp $
/**
 * sm2emailmarketing - Email Marketing Newsletter
 *
 * Joomla! Newsletter Component that supports multiple list mailing and message queueing
 *
 * @package Joomla!
 * @subpackage com_sm2emailmarketing
 * @copyright Copyright (C) 2006 SM2 / All Rights Reserved
 * @license commercial
 * @author SM2 <info@sm2joomla.com>
 */

/**
 * Makes sure this is included by a parent file
 */
defined('_VALID_MOS') or die('Direct Access to this location is not allowed.');

function showLists($option) {
    global $database, $mainframe, $mosConfig_list_limit, $mosConfig_absolute_path, $emErrorHandler;

    if (!defined('_SM2EM_LISTS')) {
        mosRedirect('index2.php?option='.$option, _EMKTG_LIST_DISABLED);
        exit();
    }

    // get some data from the request information
    $limit = $mainframe->getUserStateFromRequest( 'viewlistlimit', 'limit', $mosConfig_list_limit );
    $limitstart = $mainframe->getUserStateFromRequest( 'view'.$option.'liststart', 'limitstart', 0 );
    $search = $mainframe->getUserStateFromRequest('search'.$option.'lists', 'search', '');
    $search = $database->getEscaped(trim(strtolower($search)));

    // determine if there is anything to search for
    if (!empty($search)) {
        $where = ' WHERE (LOWER(al.list_name) LIKE '
            .$database->Quote('%'.$search.'%').')';
    } else {
        $where = '';
    }

    // Get the total number of records
    $qrysql  = 'SELECT COUNT(*)'
        .' FROM #__emktg_list al'
        .$where;

    $database->setQuery($qrysql);
    $total = $database->loadResult();

    // handle the page navigation
    include_once($mosConfig_absolute_path.'/administrator/includes/pageNavigation.php');
    $pageNav = new mosPageNav($total, $limitstart, $limit);

    // build the query to get the data to display
    $qrysql  = 'SELECT al.listid, al.list_name, al.access, al.published, g.name AS groupname'
        .' FROM #__emktg_list al'
        .' LEFT JOIN #__groups AS g ON g.id = al.access'
        .$where
        .' ORDER BY al.list_name'
        .' LIMIT '.(int)$pageNav->limitstart.', '.(int)$pageNav->limit;

    // get the data to display
    $database->setQuery($qrysql);
    $rows = $database->loadObjectList();
    if ($database->getErrorNum()) {
        $emErrorHandler->addError('(list.admin.php->showLists() line ' . __LINE__ . '):'._EMKTG_LIST_ERROR);
        return false;
    }

    // determine the number of list subscribers to each list
    $database->setQuery('SELECT ls.listid, COUNT(distinct ls.subscriberid) as num'
        .' FROM #__emktg_list_subscriber ls'
        .' INNER JOIN #__emktg_subscriber s'
            .' ON (-ls.subscriberid=s.subscriberid)'
        .' WHERE s.unsubscribe_date is null AND s.confirmed=1'
        .' GROUP BY ls.listid');
    $listSubscribers = $database->loadObjectList('listid');

    // determine the number of client managed subscribers
    $database->setQuery('SELECT listid, COUNT(distinct lu.id) as num'
        .' FROM #__emktg_list_user lu'
        .' INNER JOIN #__emktg_user u'
            .' ON (lu.id=u.id)'
        .' WHERE u.unsubscribe_date is null AND u.confirmed=1'
        .' GROUP BY listid');
    $listUsers = $database->loadObjectList('listid');

    // preparse the rows found
    if (!empty($rows)) {
        $k = 0;
        for ($i=0; $i < count($rows); $i++) {
            $row = &$rows[$i];
            $row->id = $row->listid;
            $row->checked_out=0;
            $row->k = $k;

            $row->link = 'index2.php?option='.$option.'&task=editlist&hidemainmenu=0&cid[]='.$row->id;

            $row->alllink = 'index2.php?option='.$option.'&task=alllist&cid[]='.$row->id;
            $row->nonelink = 'index2.php?option='.$option.'&task=nonelist&cid[]='.$row->id;

            $row->accessDisplay = mosCommonHTML::AccessProcessing($row, $i);
            $row->accessDisplay = str_replace(",'accesspublic')", ",'accesspubliclist')", $row->accessDisplay);
            $row->accessDisplay = str_replace(",'accessregistered')", ",'accessregisteredlist')", $row->accessDisplay);
            $row->accessDisplay = str_replace(",'accessspecial')", ",'accessspeciallist')", $row->accessDisplay);

            $row->checkedDisplay = mosCommonHTML::CheckedOutProcessing($row, $i);

            $row->publishedDisplay = mosCommonHTML::PublishedProcessing($row, $i);
            $row->publishedDisplay = str_replace(",'unpublish')", ",'unpublishlist')", $row->publishedDisplay);
            $row->publishedDisplay = str_replace(",'publish')", ",'publishlist')", $row->publishedDisplay);

            $row->listSubscribers = 0;
            if (array_key_exists($row->id, $listSubscribers)) {
                $row->listSubscribers = $listSubscribers[$row->id]->num;
            }
            $row->listUsers = 0;
            if (array_key_exists($row->id, $listUsers)) {
                $row->listUsers = $listUsers[$row->id]->num;
            }

            $k = 1 - $k;
        } // for
    }

    // load the language elements
    $lang = sm2emailmarketingLanguage();
    mosCommonHTML::loadOverlib();

    // call the object to display the list
    $tmpl = sm2emailmarketingPatTemplate('list.tpl', $option);

    $tmpl->addVar('header', 'FORM_NAME', _EMKTG_LIST_FORM);
    $tmpl->addVar('manager', 'SEARCH', $search);
    $tmpl->addVar('manager', 'NUM_ROWS', count($rows));
    $tmpl->addObject('manager', $lang, 'LANG');
    $tmpl->addObject('rows', $rows, 'ROW_');
    $tmpl->addVar('manager', 'LISTFOOTER', $pageNav->getListFooter());

    $tmpl->displayParsedTemplate('manager');

} // showLists()

function editList($id, $option, $task) {
    global $database, $mosConfig_list_limit, $mosConfig_absolute_path, $mosConfig_sef;
    global $mainframe;

    // create an instance of the table class
    $row = new sm2emailmarketingList();

    // Load the row from the db table
    $row->load($id);

    // initialise lists
    $lists = new stdClass();

    $lists->access = mosAdminMenus::Access($row);
    $lists->published = mosAdminMenus::Published($row);

    $params = getPluginParams($option, 'list.plugin.php');

    switch ($task) {
        case 'newlist':
            $title= _E_ADD;
            break;
        case 'copylist':
            $title = _EMKTG_COPY;
            $row->listid = 0;
            $row->list_name = sm2emailmarketingProcessCopyTitle($row->list_name);
            break;
        case 'editlist':
        default:
            $title = _E_EDIT;
            break;
    }

    // load the language elements
    $lang = sm2emailmarketingLanguage();

    // display the form
    mosMakeHtmlSafe($row);
    mosCommonHTML::loadOverlib();

    $tmpl = sm2emailmarketingPatTemplate('list.tpl', $option);

    $tmpl->addVar('header', 'FORM_NAME', $title.' '._EMKTG_LIST);
    $tmpl->addVar('edit', 'HTMLEMAIL', $params->get('htmlemail',1));
    $tmpl->addObject('edit', $lang, 'LANG');
    $tmpl->addObject('edit', $row, 'ROW_');
    $tmpl->addObject('edit', $lists, 'LIST_');

    // extra processing to display the list of subscribers
    $limit = (int)mosGetParam($_GET, 'limit', $mosConfig_list_limit );
    $limitstart = (int)mosGetParam($_GET, 'limitstart', 0 );

    // determine the number of list subscribers
    $database->setQuery('SELECT COUNT(distinct ls.subscriberid)'
        .' FROM #__emktg_list_subscriber ls'
        .' INNER JOIN #__emktg_subscriber s'
            .' ON (-ls.subscriberid=s.subscriberid)'
        .' WHERE s.unsubscribe_date is null AND s.confirmed=1'
        .' AND ls.listid='.(int) $row->listid);
    $numSubscribers = (int) $database->loadResult();

    // determine the number of client managed subscribers
    $database->setQuery('SELECT COUNT(distinct lu.id)'
        .' FROM #__emktg_list_user lu'
        .' INNER JOIN #__emktg_user eu'
            .' ON (lu.id=eu.id)'
        .' WHERE eu.unsubscribe_date is null AND eu.confirmed=1'
        .' AND lu.listid='.(int) $row->listid);
    $numUsers = (int) $database->loadResult();

    // add to template
    $totalSubscribers = $numSubscribers + $numUsers;
    $tmpl->addVar('edit', 'LIST_TOTAL', $numSubscribers);
    $tmpl->addVar('edit', 'USER_TOTAL', $numUsers);
    $tmpl->addVar('edit', 'SUBSCRIBER_TOTAL', $totalSubscribers);

    // handle the page navigation
    // but use th front end because it uses links rather than a form

    /**
     * Note:
     * This code attempts to force the pagination to use links
     * rather than a form. This is necessary because we are already in a form
     *
     * Joomla! 1.5 can do this using the minor hack outlined below
     * EXCEPT when the admin template uses functions to override the
     * processing of the pagination links. In that case if it is not
     * coded correctly (which the default khepri template is not)
     * then it will use the form based links reguardless.
     *
     * Since this is a Joomla! bug SM2 Extensions has made the decision
     * to not try to build a work around for this at this time.
     * You are recommend to download a WORKING Joomla! Admin template instead.
     */
    if (!defined('_JLEGACY')) {
        $mosConfig_sef = 0;
        include_once($mosConfig_absolute_path.'/includes/sef.php');
    } else {
        $tempClientId = $mainframe->_clientId;
        $mainframe->_clientId = 0;

        $app    = &JFactory::getApplication();
        $router = &$app->getRouter();
        $router->setVars(
            array(
                'option'=>$option,
                'task'=>'editlist',
                'hidemainmenu'=>0,
                'cid[]'=>(int)$row->listid
            )
        );
    }

    include_once($mosConfig_absolute_path.'/includes/pageNavigation.php');
    $link = 'administrator/index2.php?option='.$option
        .'&task=editlist&hidemainmenu=0&cid[]='.(int)$row->listid;

    $pageNav = new mosPageNav($totalSubscribers, $limitstart, $limit);
    $tmpl->addVar( 'edit', 'PAGE_LINKS', $pageNav->writePagesLinks( $link ) );
    $tmpl->addVar( 'edit', 'PAGE_LIST_OPTIONS', $pageNav->getLimitBox( $link ) );
    $tmpl->addVar( 'edit', 'PAGE_COUNTER', $pageNav->writePagesCounter() );

    if (defined('_JLEGACY')) {
        $mainframe->_clientId = $tempClientId;
    }

    if ($totalSubscribers) {
        // get the paged list of subscribers and users

        // get the list of subscribers
        $database->setQuery('SELECT s.subscriberid, s.name, s.email'
            .' FROM #__emktg_list_subscriber ls'
            .' INNER JOIN #__emktg_subscriber s'
                .' ON (-ls.subscriberid=s.subscriberid)'
            .' WHERE s.unsubscribe_date is null AND s.confirmed=1'
            .' AND ls.listid='.(int) $row->listid
            .' ORDER BY s.name, s.email'
            .' LIMIT '.(int)$pageNav->limitstart.', '.(int)$pageNav->limit);
        $tmpl->addObject('list_subscriber', $database->loadObjectList());

        // get the list of users
        $database->setQuery('SELECT u.id, u.name, u.email'
            .' FROM #__emktg_list_user lu'
            .' INNER JOIN #__emktg_user eu'
                .' ON (lu.id=eu.id)'
            .' INNER JOIN #__users u'
                .' ON (eu.id=u.id)'
            .' WHERE eu.unsubscribe_date is null AND eu.confirmed=1'
            .' AND lu.listid='.(int) $row->listid
            .' ORDER BY u.name, u.email'
            .' LIMIT '.(int)$pageNav->limitstart.', '.(int)$pageNav->limit);
        $tmpl->addObject('list_user', $database->loadObjectList());

    }


    $tmpl->displayParsedTemplate('edit');

} // editList()

function saveList($option, $task) {
    global $emErrorHandler;

    // create an instance of the table class
    $row = new sm2emailmarketingList();

    // attempt to bind the class to the data posted
    if (defined('_JLEGACY')) {
        $post = JRequest::get('post');
    } else {
        $post = $_POST;
    }

    if (!$row->bind($post)) {
        $emErrorHandler->addError($row->getError(), true);
    }

    // check the record
    if (!$row->check()) {
        $emErrorHandler->addError($row->getError(), true);
    }

    // attempt to insert/update the record
    if (!$row->store()) {
        $emErrorHandler->addError($row->getError(), true);
    }

    switch ($task) {
        case 'applylist':
            mosRedirect('index2.php?option='.$option.'&task=editlist&hidemainmenu=0&cid[]='.$row->listid, _EMKTG_LIST_SAVED);
            break;
        case 'savelist':
        default:
            mosRedirect('index2.php?option='.$option.'&task=showlists', _EMKTG_LIST_SAVED);
            break;
    }

} // saveList()

function deleteList($ids, $option) {
    global $emErrorHandler;

    // todo 51 limit the delete so that only lists that have no unsent messages can be deleted
    // validate that ids are ok
    if (!is_array($ids) or count($ids) < 1) {
        $emErrorHandler->addError('(list.admin.php->deleteList() line ' . __LINE__ . '):Nothing selected to Delete', true);
    }

    // load the table object and perform a delete
    $row = new sm2emailmarketingList();

    foreach ($ids as $id) {
        if (!$row->delete($id)) {
            $emErrorHandler->addError($row->getError(), true);
        }
    }

    mosRedirect('index2.php?option='.$option.'&task=showlists', _EMKTG_LIST_DELETED);

} // deleteList()

function publishLists($id=null, $publish=1,  $option) {
    global $emErrorHandler;

    if ($publish) {
        $action = _CMN_PUBLISHED;
        $successful = _EMKTG_LIST_PUBLISHED;
    } else {
        $action = _CMN_UNPUBLISHED;
        $successful = _EMKTG_LIST_UNPUBLISHED;
    }
    $pleaseSelect = _EMKTG_LIST_PLEASE_SELECT.$action.'.';

    // check to make sure something to publish has been selected
    if (!is_array($id) || count($id) < 1) {
        $emErrorHandler->addError('(list.admin.php->publishLists() line ' . __LINE__ . '):Nothing selected to Publish', true);
    }

    // load the table object and perform a publish
    $row = new sm2emailmarketingList();

    if (!$row->publish_array($id,$publish)) {
        $emErrorHandler->addError($row->getError(), true);
    }

    mosRedirect('index2.php?option='.$option.'&task=showlists', $successful);

} // publishLists()

function accessList($id, $access, $option) {
    $row = new sm2emailmarketingList();
    $row->load($id);
    $row->access = $access;

    if (!$row->check()) {
        return $row->getError();
    }
    if (!$row->store()) {
        return $row->getError();
    }

    mosRedirect('index2.php?option='.$option.'&task=showlists', _EMKTG_LIST_ACCESS_CHANGED);

} // accessList()


function allList($id, $option) {

    $row = new sm2emailmarketingList();
    $row->load($id);
    $registered = $row->addAllRegistered();
    $unregistered = $row->addAllUnregistered();

    if ($registered===false || $unregistered===false) {
        $msg = sprintf(_EMKTG_LIST_SUBSCRIBE_ALL_ERROR, $row->list_name);
    } else {
        $msg = sprintf(_EMKTG_LIST_SUBSCRIBE_ALL_DONE, $row->list_name, (int) ($registered+$unregistered));
    }

    mosRedirect('index2.php?option='.$option.'&task=showlists', $msg);

} // allList()


function noneList($id, $option) {

    $row = new sm2emailmarketingList();
    $row->load($id);
    $registered = $row->removeAllRegistered();
    $unregistered = $row->removeAllUnregistered();

    if ($registered===false || $unregistered===false) {
        $msg = sprintf(_EMKTG_LIST_SUBSCRIBE_NONE_ERROR, $row->list_name);
    } else {
        $msg = sprintf(_EMKTG_LIST_SUBSCRIBE_NONE_DONE, $row->list_name, (int) ($registered+$unregistered));
    }

    mosRedirect('index2.php?option='.$option.'&task=showlists', $msg);

} // noneList()
