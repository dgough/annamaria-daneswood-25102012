<?php
// $Id: registered.admin.php,v 1.16 2008/03/31 01:59:10 sm2tony Exp $
/**
 * sm2emailmarketing - Email Marketing Newsletter
 *
 * Joomla! Newsletter Component that supports multiple list mailing and message queueing
 *
 * @package Joomla!
 * @subpackage com_sm2emailmarketing
 * @copyright Copyright (C) 2006 SM2 / All Rights Reserved
 * @license commercial
 * @author SM2 <info@sm2joomla.com>
 */

/**
 * Makes sure this is included by a parent file
 */
defined('_VALID_MOS') or die('Direct Access to this location is not allowed.');

function showRegistered($option) {
    global $database, $mainframe, $mosConfig_list_limit, $mosConfig_absolute_path, $emErrorHandler;

    // get some data from the request information
    $limit = $mainframe->getUserStateFromRequest( 'viewregisteredlimit', 'limit', $mosConfig_list_limit );
    $limitstart = $mainframe->getUserStateFromRequest( 'view'.$option.'registeredstart', 'limitstart', 0 );
    $search = $mainframe->getUserStateFromRequest('search'.$option.'registered', 'search', '');
    $search = $database->getEscaped(trim(strtolower($search)));
    $receiveHTMLFilter = (int) $mainframe->getUserStateFromRequest('receive'.$option.'registered', 'receive_html', -1);
    $listFilter = (int) $mainframe->getUserStateFromRequest('list'.$option.'registered', 'list', -1);
    $confirmedFilter = (int) $mainframe->getUserStateFromRequest('confirmed'.$option.'registered', 'confirmed', -1);
    $subscribedFilter = (int) $mainframe->getUserStateFromRequest('subscribed'.$option.'registered', 'subscribed', -1);

    $where = array('(1=1)');

    // determine if there is anything to search for
    if (!empty($search)) {
        $where[] = '((lower(u.name) like '.$database->Quote('%'.$search.'%').') '
            .' or (lower(u.email) like '.$database->Quote('%'.$search.'%').'))';
    }

    if ($receiveHTMLFilter >= 0) {
        $where[] = '(au.receive_html='.$receiveHTMLFilter.')';
    }

    if (defined('_SM2EM_LISTS')) {
        if ($listFilter==0) {
            $where[] = '(lu.listid is null)';
        } else if ($listFilter > 0) {
            $where[] = '(lu.listid='.$listFilter.')';
        }
    }

    if ($confirmedFilter >= 0) {
        $where[] = '(au.confirmed='.$confirmedFilter.')';
    }

    if ($subscribedFilter==0) {
        $where[] = '(au.unsubscribe_date!=0)';
    } else if ($subscribedFilter==1) {
        $where[] = '(au.unsubscribe_date=0)';
    }

    // Get the total number of records
    $database->setQuery('SELECT COUNT(DISTINCT au.id)'
        .' FROM #__emktg_user au'
        .' INNER JOIN #__users u'
            .' ON (au.id=u.id)'
        .' LEFT JOIN #__emktg_list_user lu'
            .' ON (u.id=lu.id)'
        .' WHERE '.implode(' AND ', $where));
    $total = $database->loadResult();

    // handle the page navigation
    include_once($mosConfig_absolute_path.'/administrator/includes/pageNavigation.php');
    $pageNav = new mosPageNav($total, $limitstart, $limit);

    // build the query to get the data to display
    $database->setQuery('SELECT distinct u.*, au.*'
        .' FROM #__emktg_user au'
        .' INNER JOIN #__users u'
            .' ON (au.id=u.id)'
        .' LEFT JOIN #__emktg_list_user lu'
            .' ON (u.id=lu.id)'
        .' WHERE '.implode(' AND ', $where)
        .' ORDER BY u.name'
        .' LIMIT '.(int)$pageNav->limitstart.', '.(int)$pageNav->limit);
    $rows = $database->loadObjectList();
    if ($database->getErrorNum()) {
        $emErrorHandler->addError('(registered.admin.php->showRegistered() line ' . __LINE__ . '): '._EMKTG_REGISTERED_ERROR);
        return false;
    }

    // preparse the rows found
    if (!empty($rows)) {
        $k = 0;
        for ($i=0; $i < count($rows); $i++) {
            $row = &$rows[$i];
            $row->checked_out=0;
            $row->k = $k;

            $row->link = 'index2.php?option='.$option.'&task=editregistered&hidemainmenu=0&cid[]='.$row->id;

            $row->checkedDisplay = mosCommonHTML::CheckedOutProcessing($row, $i);

            $img    = $row->receive_html ? 'tick.png' : 'publish_x.png';
            $task   = $row->receive_html ? 'unhtmlregistered' : 'htmlregistered';
            $alt    = $row->receive_html ? _EMKTG_RECEIVE_HTML : _EMKTG_RECEIVE_TEXT;
            $action = $row->receive_html ? _EMKTG_RECEIVE_TEXT : _EMKTG_RECEIVE_HTML;
            $row->receive_htmlDisplay = '<a href="javascript: void(0);" onclick="return listItemTask(\'cb'. $i .'\',\''. $task .'\')" title="'. $action .'"><img src="images/'. $img .'" border="0" alt="'. $alt .'" /></a>';

            $img    = $row->confirmed ? 'tick.png' : 'publish_x.png';
            $task   = $row->confirmed ? 'unconfirmregistered' : 'confirmregistered';
            $alt    = $row->confirmed ? _EMKTG_CONFIRMED : _EMKTG_UNCONFIRMED;
            $action = $row->confirmed ? _EMKTG_UNCONFIRM : _EMKTG_CONFIRM;
            $row->confirmedDisplay = '<a href="javascript: void(0);" onclick="return listItemTask(\'cb'. $i .'\',\''. $task .'\')" title="'. $action .'"><img src="images/'. $img .'" border="0" alt="'. $alt .'" /></a>';

            $k = 1 - $k;
        } // for
    }

    // load the language elements
    $lang = sm2emailmarketingLanguage();

    // call the object to display the list
    $tmpl = sm2emailmarketingPatTemplate('registered.tpl', $option);

    $tmpl->addVar('header', 'FORM_NAME', _EMKTG_REGISTERED_FORM);
    $tmpl->addVar('manager', 'SEARCH', $search);
    $tmpl->addVar('manager', 'NUM_ROWS', count($rows));
    $tmpl->addObject('manager', $lang, 'LANG');
    $tmpl->addObject('rows', $rows, 'ROW_');
    $tmpl->addVar('manager', 'LISTFOOTER', $pageNav->getListFooter());

    // build the filter list values

    // receiveHTML filter options
    $receiveHTMLOptions = array(
        mosHTML::makeOption(0, _EMKTG_RECEIVE_TEXT),
        mosHTML::makeOption(1, _EMKTG_RECEIVE_HTML)
    );
    foreach ($receiveHTMLOptions as $index=>$obj) {
        $receiveHTMLOptions[$index]->selected = '';
        if ($receiveHTMLFilter==$obj->value) {
            $receiveHTMLOptions[$index]->selected = 'selected="selected"';
        }
    }
    $tmpl->addObject('filter_receive_html', $receiveHTMLOptions);

    // confirmed filter options
    $confirmedOptions = array(
        mosHTML::makeOption(0, _EMKTG_UNCONFIRMED),
        mosHTML::makeOption(1, _EMKTG_CONFIRMED)
    );
    foreach ($confirmedOptions as $index=>$obj) {
        $confirmedOptions[$index]->selected = '';
        if ($confirmedFilter==$obj->value) {
            $confirmedOptions[$index]->selected = 'selected="selected"';
        }
    }
    $tmpl->addObject('filter_confirmed', $confirmedOptions);

    // subscribed filter options
    $subscribedOptions = array(
        mosHTML::makeOption(0, _EMKTG_UNSUBSCRIBE_REASON_NULL),
        mosHTML::makeOption(1, _EMKTG_UNSUBSCRIBE_REASON_0)
    );
    foreach ($subscribedOptions as $index=>$obj) {
        $subscribedOptions[$index]->selected = '';
        if ($subscribedFilter==$obj->value) {
            $subscribedOptions[$index]->selected = 'selected="selected"';
        }
    }
    $tmpl->addObject('filter_subscribed', $subscribedOptions);

    // list filter options
    if (defined('_SM2EM_LISTS')) {

        $database->setQuery('SELECT listid AS `value`, list_name AS `text`'
            .' FROM #__emktg_list'
            .' WHERE published=1 AND access=0'
            .' ORDER BY list_name');
        $listOptions = $database->loadObjectList();

        array_unshift($listOptions, mosHTML::makeOption(0, _EMKTG_FILTER_NOLIST_TEXT));
        foreach ($listOptions as $index=>$obj) {
            $listOptions[$index]->selected = '';
            if ($listFilter==$obj->value) {
                $listOptions[$index]->selected = 'selected="selected"';
            }
        }
        $tmpl->addObject('filter_list', $listOptions);
    }

    $tmpl->displayParsedTemplate('manager');

} // showRegistered()

function editRegistered($id, $option) {
    global $database, $acl, $mainframe;

    // create an instance of the table class
    $row = new sm2emailmarketingUser();

    // Load the row from the db table
    $row->load($id);

    // get the name and email of this user
    $temp = new mosUser($database);
    $temp->load($id);

    $row->name = $temp->name;
    $row->email = $temp->email;

    // initialise lists
    $lists = new stdClass();

    $lists->confirmed = mosHTML::yesnoRadioList('confirmed', 'class="inputbox"', $row->confirmed);
    $lists->receive_html = mosHTML::yesnoRadioList('receive_html', 'class="inputbox"', $row->receive_html);

    // fudge the group stuff
    $grp = $acl->getAroGroup($id);
    $gid = 1;

    if ($acl->is_group_child_of($grp->name, 'Registered', 'ARO') || $acl->is_group_child_of($grp->name, 'Public Backend', 'ARO')) {
        // fudge Authors, Editors, Publishers and Super Administrators into the Special Group
        $gid = 2;
    }

    // build the list of lists we can subscribe to
    $sql = 'SELECT listid, list_name FROM #__emktg_list'
        .' WHERE published=1 AND access <='.(int)$gid
        .' ORDER BY list_name';
    $database->setQuery($sql);
    $availLists = $database->loadObjectList();

    // get the list of subscribed lists
    $database->setQuery('SELECT listid FROM #__emktg_list_user WHERE id='.(int)$id);
    $subscribedLists = $database->loadResultArray();

    if (defined('_SM2EM_LISTS')) {
        $lists->subscription = '';
        foreach ($availLists as $rec) {
            if (in_array($rec->listid, $subscribedLists)) {
                $checked = ' checked';
            } else {
                $checked = '';
            }
            $lists->subscription .= '<label><input type="checkbox" name="listid[]" value="'.$rec->listid.'" '.$checked.' />';
            $lists->subscription .= '&nbsp;'.$rec->list_name."</label><br />\n";

        } // foreach
    }

    // load the language elements
    $lang = sm2emailmarketingLanguage();

    // determine the status
    if ($row->unsubscribe_date!='0000-00-00 00:00:00' && $row->unsubscribe_reason==0) {
        $row->unsubscribe_reason=5;
    }
    $statusVar = '_EMKTG_UNSUBSCRIBE_REASON_'.$row->unsubscribe_reason;
    if (!empty($lang->$statusVar)) {
        $row->status = $lang->$statusVar;
    } else {
        $row->unsubscribe_reason = 5;
        $row->status = $lang->_EMKTG_UNSUBSCRIBE_REASON_5;
    }

    if ($row->unsubscribe_reason > 0) {
        // allow re-subscribing
        $row->resubscribe = 1;

        // determine the resubscribe message
        $msgVar = '_EMKTG_RESUBSCRIBE_MSG_'.$row->unsubscribe_reason;

        if (!empty($lang->$msgVar)) {
            $row->resubscribe_msg = $lang->$msgVar;
        }
    }

    // display the form
    mosMakeHtmlSafe($row);
    mosCommonHTML::loadOverlib();

    $tmpl = sm2emailmarketingPatTemplate('registered.tpl', $option);

    $tmpl->addVar('header', 'FORM_NAME', _E_EDIT.' '._EMKTG_REGISTERED_FORM);
    $tmpl->addObject('edit', $lang, 'LANG');
    $tmpl->addObject('edit', $row, 'ROW_');
    $tmpl->addObject('edit', $lists, 'LIST_');

    // determine the edit link
    if (file_exists($mainframe->getPath('class', 'com_member'))) {
        $editRegisteredLink = 'index2.php?option=com_member'
            .'&task=edit&hidemainmenu=1&id='.$row->id;
    } else if (file_exists($mainframe->getPath('class', 'com_comprofiler'))) {
        $editRegisteredLink = 'index2.php?option=com_comprofiler'
            .'&task=edit&hidemainmenu=1&cid[]='.$row->id;
    } else {
        $editRegisteredLink = 'index2.php?option=com_users'
            .'&task=editA&hidemainmenu=1&id='.$row->id;
    }
    $tmpl->addVar('edit', 'editregisteredlink', $editRegisteredLink);

    $tmpl->displayParsedTemplate('edit');

} // editRegistered()

function saveRegistered($option, $task) {
    global $emErrorHandler;

    $listIds = mosGetParam($_REQUEST, 'listid', array());

    // load the table object and perform a publish
    $row = new sm2emailmarketingUser();

    // attempt to bind the class to the data posted
    if (defined('_JLEGACY')) {
        $post = JRequest::get('post');
    } else {
        $post = $_POST;
    }

    if (!$row->bind($post)) {
        $emErrorHandler->addError($row->getError(), true);
    }

    // see if we need to resubscribe
    $resubscribe = (int)mosGetParam($_POST, 'resubscribe', 0);
    if ($resubscribe) {
        $row->unsubscribe_date = $row->_db->getNullDate();
        $row->unsubscribe_reason = 0;
    }

    // attempt to insert/update the record
    if (!$row->store()) {
        $emErrorHandler->addError($row->getError(), true);
    }

    if (defined('_SM2EM_LISTS')) {
        if (!$row->subscribe($listIds)) {
            $emErrorHandler->addError($row->getError(), true);
        }
    }

    switch ($task) {
        case 'applyregistered':
            mosRedirect('index2.php?option='.$option.'&task=editregistered&hidemainmenu=0&cid[]='.$row->id, _EMKTG_SUBSCRIBER_SAVED);
            break;
        case 'saveregistered':
        default:
            mosRedirect('index2.php?option='.$option.'&task=showregistered', _EMKTG_SUBSCRIBER_SAVED);
            break;
    }

} // confirmRegistered()

function unsubscribeRegistered($ids, $option) {
    global $emErrorHandler;

	// validate that ids are ok
    if (!is_array($ids) or count($ids) < 1) {
        $emErrorHandler->addError('(registered.admin.php->unsubsubscribeRegistered() line ' . __LINE__ . '):Nothing selected to process', true);
    }

    //load the table object and unsubscribe selected users
    $row = new sm2emailmarketingUser();

    if (!$row->unsubscribe($ids)) {
        $emErrorHandler->addError($row->getError(), true);
    }

    mosRedirect('index2.php?option='.$option.'&task=showregistered', _EMKTG_SUBSCRIBER_UNSUBSCRIBED);
} // unsubscribeRegistered()

function confirmRegistered($id=null, $confirm=1,  $option) {
    global $emErrorHandler;

    if ($confirm) {
        $action = _EMKTG_CONFIRMED;
        $successful = _EMKTG_SUBSCRIBER_CONFIRMED;
    } else {
        $action = _EMKTG_UNCONFIRMED;
        $successful = _EMKTG_SUBSCRIBER_UNCONFIRMED;
    }
    $pleaseSelect = _EMKTG_SUBSCRIBER_PLEASE_SELECT.$action.'.';

    // check to make sure something to publish has been selected
    if (!is_array($id) || count($id) < 1) {
        $emErrorHandler->addError('(registered.admin.php->confirmRegistered() line ' . __LINE__ . '):Nothing selected to process', true);
    }

    // load the table object and perform a publish
    $row = new sm2emailmarketingUser();

    if (!$row->confirm($id,$confirm)) {
        $emErrorHandler->addError($row->getError(), true);
    }

    mosRedirect('index2.php?option='.$option.'&task=showregistered', $successful);

} // confirmRegistered()

function confirmAllRegistered($confirm=1,  $option) {
    global $emErrorHandler;

    if ($confirm) {
        $action = _EMKTG_CONFIRMED;
        $successful = _EMKTG_SUBSCRIBER_CONFIRMED;
    } else {
        $action = _EMKTG_UNCONFIRMED;
        $successful = _EMKTG_SUBSCRIBER_UNCONFIRMED;
    }

    // load the table object and perform a publish
    $row = new sm2emailmarketingUser();

    if (!$row->confirmAll($confirm)) {
        $emErrorHandler->addError($row->getError(), true);
    }

    mosRedirect('index2.php?option='.$option.'&task=showregistered', $successful);

} // confirmAllRegistered()

function receiveHTMLRegistered($id=null, $html=1,  $option) {
    global $emErrorHandler;

    if ($html) {
        $action = _EMKTG_RECEIVE_HTML;
        $successful = _EMKTG_SUBSCRIBER_RECEIVE_HTML;
    } else {
        $action = _EMKTG_RECEIVE_TEXT;
        $successful = _EMKTG_SUBSCRIBER_RECEIVE_TEXT;
    }
    $pleaseSelect = _EMKTG_SUBSCRIBER_PLEASE_SELECT.$action.'.';

    // check to make sure something to publish has been selected
    if (!is_array($id) || count($id) < 1) {
        $emErrorHandler->addError('(registered.admin.php->receiveHTMLRegistered() line ' . __LINE__ . '):Nothing selected to process', true);
    }

    // load the table object and perform a publish
    $row = new sm2emailmarketingUser();

    if (!$row->receiveHTML($id,$html)) {
        $emErrorHandler->addError($row->getError(), true);
    }

    mosRedirect('index2.php?option='.$option.'&task=showregistered', $successful);

} // receiveHTMLRegistered()

function receiveHTMLAllRegistered($html=1,  $option) {
    global $emErrorHandler;

    if ($html) {
        $action = _EMKTG_RECEIVE_HTML;
        $successful = _EMKTG_SUBSCRIBER_RECEIVE_HTML;
    } else {
        $action = _EMKTG_RECEIVE_TEXT;
        $successful = _EMKTG_SUBSCRIBER_RECEIVE_TEXT;
    }

    // load the table object and perform a publish
    $row = new sm2emailmarketingUser();

    if (!$row->receiveHTMLAll($html)) {
        $emErrorHandler->addError($row->getError(), true);
    }

    mosRedirect('index2.php?option='.$option.'&task=showregistered', $successful);

} // receiveHTMLAllRegistered()
