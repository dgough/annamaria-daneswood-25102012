// Copyright 2006 | Thierry Koblentz - www.TJKDesign.com All Rights reserved
// ToggleDL() Version 1.5.4 report bugs or errors to thierry@tjkdesign.com

function doToggleDL(x){
	var zDD=document.getElementById('toggleDl').getElementsByTagName('dd');
	var zDT=document.getElementById('toggleDl').getElementsByTagName('dt');
		zDD[x].className=(zDD[x].className=='hideDD')?'showDD':'hideDD';
		zDT[x].className=(zDT[x].className=='DTplus')?'DTminus':'DTplus';
}
function ToggleDLopen(){//we open all of them
	var zDD=document.getElementById('toggleDl').getElementsByTagName('dd');
	var zDT=document.getElementById('toggleDl').getElementsByTagName('dt');
	for(var i=0;i<zDT.length;i++){
		zDD[i].className='showDD';
		zDT[i].className='DTminus';
	}
	return false;
}
function ToggleDLclose(){//we close all of them
	var zDD=document.getElementById('toggleDl').getElementsByTagName('dd');
	var zDT=document.getElementById('toggleDl').getElementsByTagName('dt');
	for(var i=0;i<zDT.length;i++){
		zDD[i].className='hideDD';
		zDT[i].className='DTplus';
	}
	return false;
}
function ToggleDL(){
if (document.getElementById && document.getElementsByTagName && document.getElementById('toggleDl')){
  if(document.getElementById('toggleDl')) {
	var zDT=document.getElementById('toggleDl').getElementsByTagName('dt');
	var zDD=document.getElementById('toggleDl').getElementsByTagName('dd');
	var ToggleON = document.getElementById('ToggleON');
	var ToggleOFF = document.getElementById('ToggleOFF');
	if (ToggleON && ToggleOFF){// Show All - Hide All "links"
		ToggleON.onclick = ToggleDLopen;
		ToggleON.title = "Show all answers";
		ToggleON.href = "#";
		ToggleOFF.onclick = ToggleDLclose;
		ToggleOFF.title = "Hide all answers";
		ToggleOFF.href = "#";
	}
	for(var i=0;i<zDT.length;i++){
		var zContent = zDT[i].innerHTML;
		var zHref = "<a href='#' onclick=\"doToggleDL("+i+");return false\" title='Show/hide the answer'>";
		zDT[i].innerHTML = zHref + zContent + "</a>";
		zDD[i].className='hideDD';
		zDT[i].className='DTplus';
		}
	}
  }
}

window.onload = function() { ToggleDL(); }
