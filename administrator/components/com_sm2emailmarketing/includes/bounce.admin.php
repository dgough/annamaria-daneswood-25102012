<?php
// $Id: bounce.admin.php,v 1.12 2008/02/28 00:49:57 sm2tony Exp $
/**
 * sm2emailmarketing - Email Marketing Newsletter
 *
 * Joomla! Newsletter Component that supports multiple list mailing and message queueing
 *
 * @package Joomla!
 * @subpackage com_sm2emailmarketing
 * @copyright Copyright (C) 2006 SM2 / All Rights Reserved
 * @license commercial
 * @author SM2 <info@sm2joomla.com>
 */

/**
 * Makes sure this is included by a parent file
 */
defined('_VALID_MOS') or die('Direct Access to this location is not allowed.');

function showBounce($option) {
    global $sm2emailmarketingConfig, $emErrorHandler;

    // create an instantance of the bounce processor
    $bounces = new sm2emBounce($sm2emailmarketingConfig);

    $bounces->process();

    // load the language elements
    $lang = sm2emailmarketingLanguage();

    // call the object to display the list
    $tmpl = sm2emailmarketingPatTemplate("bounce.tpl", $option);

    $tmpl->addVar('header', 'FORM_NAME', _EMKTG_BOUNCE_FORM);
    $tmpl->addVar('manager', 'NUM_ROWS', count($bounces->rows));
    $tmpl->addObject('manager', $lang, 'LANG');
    $tmpl->addObject('rows', $bounces->rows, "ROW_");
    $tmpl->addVar('manager', 'START', $bounces->getNextStart());

    $tmpl->displayParsedTemplate("manager");

//    echo "<pre>".print_r($bounces, true)."</pre>";

} // showBounce()


class sm2emBounce {

    // IMAP resource
    var $resource = null;

    // connection information
    var $mailbox = null;
    var $username = null;
    var $password = null;
    var $options = null;

    var $mode = true;
    var $unsubscribe = false;

    var $rows = array();

    var $batch = 0;
    var $start = 1;
    var $processed = 0;
    var $deleted = 0;
    var $bounced = 0;
    var $num = 0;

    /**
     * Constructor
     *
     * Simply look in the config and record all of the vars we need so we have our own copy
     */
    function sm2emBounce($sm2emailmarketingConfig) {
        // set a long timeout so this has time to process correctly
        set_time_limit(6000);

        // see if we are in test mode
        $this->mode = (int)$sm2emailmarketingConfig->get("bounce_mode", 0);

        $this->unsubscribe = (int)$sm2emailmarketingConfig->get("bounce_unsubscribe", 5);

        $this->batch = (int)$sm2emailmarketingConfig->get("bounce_batch", 0);
        $this->processed = 0;
        $this->bounced = 0;
        $this->deleted = 0;
        $this->start = (int)mosGetParam($_REQUEST, "bounce_start", 1);
        $this->num = 0;

        // try to build the connection information from
        $this->mailbox = "{".$sm2emailmarketingConfig->get("bounce_server","localhost");

        $port = $sm2emailmarketingConfig->get("bounce_port", "");
        $service = $sm2emailmarketingConfig->get("bounce_service", "pop3");
        if (empty($port)) {
            switch ($service) {
                case "pop3":
                    $port = 110;
                    break;
                case "imap":
                    $port = 143;
                    break;
                case "nntp":
                    $port = 119;
                    break;
            }
        }

        if (($service=="pop3") && ($port==110)) {
            $service .= "/notls";
        }

        $this->mailbox .= ":".$port."/".$service."}INBOX";

        // get the username and password
        $this->username = $sm2emailmarketingConfig->get("bounce_username", "");
        $this->password = $sm2emailmarketingConfig->get("bounce_password", "");

    } // sm2emBounce()


    /**
     *
     */
    function getNextStart() {
        $processed = $this->start + $this->batch - 1;
        if ($processed == $this->num) {
            $next = -1;
        } else {
            $next = $processed - $this->deleted + 1;
        }
        return $next;

    } // getNextStart()


    /**
     * Method to connect to the server
     */
    function connect() {
        global $emErrorHandler;

        $this->resource = @imap_open($this->mailbox, $this->username, $this->password, $this->options);

        if (!$this->resource) {
            $emErrorHandler->addError('(bounce.admin.php->sm2emBounce::connect() line ' . __LINE__ . '):'._EMKTG_BOUNCE_ERROR_CONNECT);
            return false;
        }

        return true;
    } // connect


    /**
     * Method to disconnect from the server
     */
    function disconnect() {
        // see if we need to expunge
        if ($this->deleted) {
            @imap_expunge($this->resource);
        }

        // close the connection
        @imap_close($this->resource);

        $this->resource = null;

    } // disconnect()


    /**
     * Method to delete the message from the server
     */
    function remove(&$row) {
        switch ($this->mode) {
            case 0:
                // test mode so return
                break;
            case 1:
                // check to see if bounced
                // if not break otherwise continue to 2
                if (!$row->bounced) break;
            case 2:
                $this->deleted++;
                $row->deleted = 1;
                @imap_delete($this->resource, $row->id);
                break;
        }
        return;
    } // remove()


    /**
     * Method to process a single message
     */
    function processMessage($msgNum) {
        global $database;

        // create a blank object to put all the data into
        $row = new stdClass();
        $row->id = $msgNum;

        // grab the header
        $header = @imap_headerinfo($this->resource, $msgNum);

        $row->date = $header->date;
        if (isset($header->subject)) {
            $row->subject = $header->subject;
        } else {
            $row->subject = "Unknown";
        }
        if (isset($header->toaddress)) {
            $row->to = $header->toaddress;
        } else {
            $row->to = "Unknown";
        }
        $row->size = $header->Size;
        $row->unseen = ($header->Recent == "N" || $header->Unseen == "U");
        $row->processed = 0;
        $row->bounced = 0;

        // only process messages that have not yet been processed
        // only really works reliably with imap - pop shows always unprocessed
        if ($row->unseen) {
            // grab the message body and look for our header
            $body = imap_body($this->resource, $msgNum);

            // check to see if it contains the correct mailer
            if (strpos($body, "X-Mailer: SM2 Email Marketing")!==false) {
                $match = $messageid = $recipient = null;
                // look for the messageid
                preg_match ("/X-SM2MessageID: (.*)/i",$body,$match);
                if (is_array($match) && isset($match[1])) {
                    $messageid = (int)trim($match[1]);
                }
                $match = null;
                // look for the recipient
                preg_match ("/X-SM2Recipient: (.*)/i",$body,$match);
                if (is_array($match) && isset($match[1])) {
                    $recipient = trim($match[1]);
                }

                if (!empty($messageid) && !empty($recipient)) {
                    $row->bounced = 1;
                    $this->bounced++;
                    $row->to = $recipient;

                    // determine what to do (only process if not mode 0 - test)
                    if ($this->mode) {
                        // queries to process this
                        //

                        // update the message stats
                        $database->setQuery('UPDATE #__emktg_stats_message SET status=4'
                            .' WHERE messageid='.(int)$messageid
                            .' AND LOWER(email) = '.$database->Quote(strtolower($recipient)));
                        $database->Query();

                        // record in the overall stats that there has been an error
                        $database->setQuery('UPDATE #__emktg_stats_overall SET `bounces`=(`bounces`+1) WHERE messageid='.(int)$messageid);
                        $database->Query();
                    }
                }

                $row->processed = 1;
                $this->processed++;
            }
        }

        // remove this message
        $this->remove($row);

        $this->rows[$msgNum] = $row;
    } // processMessage


    /**
     * Method to process all of the messages in the mailbox
     */
    function process() {
        // connect to the server
        if (!@$this->connect()) {
            return false;
        }

        // determine the number of messages
        $this->num = @imap_num_msg($this->resource);

        if ($this->num==0) {
            $this->disconnect();
            return true;
        }

        // determine where to process in the batch
        $unprocessed = $this->num - $this->start  + 1;
        if ($this->batch) {
            $this->batch = min($unprocessed, $this->batch);
        } else {
            $this->batch = $unprocessed;
        }
        $start = $this->start;
        $end = $start + $this->batch - 1;

        for ($i=$start; $i <= $end; $i++) {
            $this->processMessage($i);
        }

        $this->disconnect();

        $this->processUnsubscribe();

        return true;

    } // process()


    /**
     * Method to unsubscribe bounced emails
     */
    function processUnsubscribe() {
        global $database;

        // see if we should process the bounces to unsubscribe
        if ($this->getNextStart()>=0) return;

        // check if we need to unsubscribe
        if ($this->unsubscribe) {
            // unsubscribe from users
            $database->setQuery('SELECT userid, `status`'
                .' FROM #__emktg_stats_message'
                .' WHERE userid > 0'
                .' GROUP BY userid, `status`'
                .' HAVING `status`=4'
                .' AND COUNT(id) >= '.(int)$this->unsubscribe);
            $bounceids = $database->loadResultArray();

            if (!is_array($bounceids) || count($bounceids)==0) {
                $bounceids = array(0);
            }

            // find these that have not already been processed
            $database->setQuery('SELECT id'
                .' FROM #__emktg_user'
                .' WHERE unsubscribe_reason=0'
                .' AND id IN ('.implode(',', $bounceids).')');
            $userids = $database->loadResultArray();

            if (!is_array($userids) || count($userids)==0) {
                $userids = array(0);
            }

            $database->setQuery('UPDATE #__emktg_user'
                .' SET unsubscribe_date = now(),'
                .' unsubscribe_reason=4'
                .' WHERE id IN ('.implode(',', $userids).')');
            $database->query();

            sm2emAdminUnsubscribeEmailFromQuery('SELECT u.name, u.email, 4 AS unsubscribe_reason'
                .' FROM #__users u INNER JOIN #__emktg_user s ON (u.id=s.id)'
                .' WHERE u.id IN ('.implode(',', $userids).')');

            // unsubscribe from subscribers
            $database->setQuery('SELECT -userid, `status`'
                .' FROM #__emktg_stats_message'
                .' WHERE userid < 0'
                .' GROUP BY userid, `status`'
                .' HAVING `status`=4'
                .' AND COUNT(id) >= '.(int)$this->unsubscribe);
            $bounceids = $database->loadResultArray();

            if (!is_array($bounceids) || count($bounceids)==0) {
                $bounceids = array(0);
            }

            // find these that have not already been processed
            $database->setQuery('SELECT subscriberid'
                .' FROM #__emktg_subscriber'
                .' WHERE unsubscribe_reason=0'
                .' AND subscriberid IN ('.implode(',', $bounceids).')');
            $userids = $database->loadResultArray();

            if (!is_array($userids) || count($userids)==0) {
                $userids = array(0);
            }

            $database->setQuery('UPDATE #__emktg_subscriber'
                .' SET unsubscribe_date = now(),'
                .' unsubscribe_reason=4'
                .' WHERE subscriberid IN ('.implode(',', $userids).')');
            $database->query();

            sm2emAdminUnsubscribeEmailFromQuery('SELECT name, email, 4 AS unsubscribe_reason'
                .' FROM #__emktg_subscriber'
                .' WHERE subscriberid IN ('.implode(',', $userids).')');
        }
    } // processUnsubscribe()

} // class sm2emBounce
?>