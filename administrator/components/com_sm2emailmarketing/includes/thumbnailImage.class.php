<?php
// $Id: thumbnailImage.class.php,v 1.3 2006/10/23 01:14:55 blair_tony Exp $
/**
 * sm2emailmarketing - Email Marketing Newsletter
 * 
 * Joomla! Newsletter Component that supports multiple list mailing and message queueing
 * 
 * @package Joomla!
 * @subpackage com_sm2emailmarketing
 * @copyright Copyright (C) 2006 SM2 / All Rights Reserved
 * @license commercial
 * @author SM2 <info@sm2joomla.com>
 */

class thumbnailImage {
	// reserve variable memory-space
	var $srcImage;
	var $destType;
	var $wmImage;
	var $imgHeight;
	var $imgWidth;
	var $base64 = false;
	var $crop = false;
	var $quality = 80;
	
	
	// Start variable switches
	function setSrcImage($img) {
		$this->srcImage = $img;
	}
	
	function getSrcImage() {
		if($this->getBase64()) {
			return base64_decode($this->srcImage);
		} else {
			return $this->srcImage;
		}
	}

	function setDestType($type) {
		$this->destType = $type;
	}

	function getDestType() {
		return $this->destType;
	}

	function setWmImage($img) {
		$this->wmImage = $img;
	}

	function getWmImage() {
		return $this->wmImage;
	}

	function setImgHeight($height) {
		$this->imgHeight = $height;
	}

	function getImgHeight() {
		return $this->imgHeight;
	}

	function setImgWidth($width) {
		$this->imgWidth = $width;
	}

	function getImgWidth() {
		return $this->imgWidth;
	}

	function setBase64($switch) {
		$this->base64 = $switch;
	}

	function getBase64() {
		return $this->base64;
	}

	function setCrop($switch) {
		$this->crop = $switch;
	}

	function getCrop() {
		return $this->crop;
	}

	function setQuality($quality) {
		$this->quality = $quality;
	}

	function getQuality() {
		return $this->quality;
	}

	// Create image based on variables
	function createThumbnail() {
		if($this->getSrcImage() != "" && file_exists($this->getSrcImage())) {
			// continue procedure
			$srcSize = getImageSize($this->getSrcImage());
			
			// If NOT cropping, then do these calculations
			if($this->getCrop() == false) {
				if(($this->getImgWidth() <= $srcSize[0] || $this->getImgHeight() <= $srcSize[1]) && ($this->getImgHeight() > 0 && $this->getImgWidth() > 0)) {
					$srcRatio  = $srcSize[0]/$srcSize[1]; // width/height ratio 
					$destRatio = $this->getImgWidth()/$this->getImgHeight(); 
					if ($destRatio > $srcRatio) { 
						$temph = $this->getImgHeight(); 
						$tempw = $this->getImgHeight()*$srcRatio; 
					} else { 
						$tempw = $this->getImgWidth(); 
						$temph = $this->getImgWidth()/$srcRatio; 
					} 
				} else {
					$tempw = $srcSize[0];
					$temph = $srcSize[1];
				}
				$offsetx = 0;
				$offsety = 0;
			} else {
				// do cropping calculations
				$tempw = $this->getImgWidth();
				$temph = number_format((($srcSize[1]*$this->getImgWidth())/$srcSize[0]), 0);
				
				if($temph < $this->getImgHeight()) {
					$tempw = number_format((($srcSize[0]*$this->getImgHeight())/$srcSize[1]), 0);
					$temph = $this->getImgHeight();
				}
				
				if($temph > $this->getImgHeight()) {
					$offsety = number_format(($temph/2)-($this->getImgHeight()/2), 0);
					$offsetx = 0;
				} else {
					$offsety = 0;
					$offsetx = number_format(($tempw/2)-($this->getImgWidth()/2), 0);
       			}
			}
			
			// src image 
			switch ($srcSize[2]) { 
				case 1: //GIF 
					$srcImg = imageCreateFromGif($this->getSrcImage()); 
				break; 
				
				case 2: //JPEG 
					$srcImg = imageCreateFromJpeg($this->getSrcImage());
				break; 
				
				case 3: //PNG 
					$srcImg = imageCreateFromPng($this->getSrcImage());
				break; 
				
				default: 
					createErrorMsg('You have not supplied a valid src image');
					exit;
				break; 
			}

			// Extra step for Cropping AND setting Final Height & Width
			if($this->getCrop() != false) {
				$tempImg = imageCreateTrueColor($tempw, $temph);
				imageantialias($tempImg, true);
				imageCopyResampled($tempImg, $srcImg, 0, 0, 0, 0, $tempw, $temph, $srcSize[0], $srcSize[1]);
				$srcImg = $tempImg;
				// true color image, with anti-aliasing 
				$destImg = imageCreateTrueColor($this->getImgWidth(), $this->getImgHeight());
				imageantialias($destImg, true);
				$srcW = $this->getImgWidth();
				$srcH = $this->getImgHeight();
				$destW = $this->getImgWidth();
				$destH = $this->getImgHeight();
			} else {
				// true color image, with anti-aliasing 
				$destImg = imageCreateTrueColor($tempw, $temph);
				imageantialias($destImg, true);
				$srcW = $srcSize[0];
				$srcH = $srcSize[1];
				$destW = $tempw;
				$destH = $temph;
			}
			
			
			imageAntiAlias($destImg, true); 
			imagecopyresampled($destImg, $srcImg, 0, 0, $offsetx, $offsety, $destW, $destH, $srcW, $srcH);
			
			
			if($this->getDestType() != "") {
				$outputType = $this->getDestType();
			} else {
				$outputType = $srcSize[2];
			}
			
			
			// get watermark
			if($this->getWmImage() != "") {
				$watermark = imagecreatefrompng($this->getWmImage()); 
				$watermark_width = imagesx($watermark);
				$watermark_height = imagesy($watermark);
				// Resize and Merge watermark
				imagealphablending($destImg, true);
				imageCopyResampled($destImg, $watermark, 0, 0, 0, 0, $destW, $destH, $watermark_width, $watermark_height);
			}
			
			
			switch ($outputType) { 
				
				case 2: 
					header('Content-Type: image/jpeg'); 
					imageJpeg($destImg, '', $this->getQuality());
				break; 
				case 1: //gif
				case 3: 
					header('Content-Type: image/png'); 
					imagePng($destImg); 
				break; 
			}
			
		} else {
			$this->createErrorMsg('You have not supplied a valid src image');
		}
	}
	
	
	// Error handling function
	function createErrorMsg($msg) {
		// Create Error
	}
	
}

?>