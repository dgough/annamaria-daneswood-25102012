<?php
// $Id: plugin.admin.php,v 1.16 2008/03/31 01:59:10 sm2tony Exp $
/**
 * sm2emailmarketing - Email Marketing Newsletter
 *
 * Joomla! Newsletter Component that supports multiple list mailing and message queueing
 *
 * @package Joomla!
 * @subpackage com_sm2emailmarketing
 * @copyright Copyright (C) 2006 SM2 / All Rights Reserved
 * @license commercial
 * @author SM2 <info@sm2joomla.com>
 */

/**
 * Makes sure this is included by a parent file
 */
defined('_VALID_MOS') or die('Direct Access to this location is not allowed.');

function showPlugins($option) {
    global $database, $mainframe, $mosConfig_list_limit, $mosConfig_absolute_path, $emErrorHandler;

    // update the plugins
    updatePlugins($option);

    // get some data from the request information
    $limit = $mainframe->getUserStateFromRequest( 'viewpluginlimit', 'limit', $mosConfig_list_limit );
    $limitstart = $mainframe->getUserStateFromRequest( 'view'.$option.'pluginstart', 'limitstart', 0 );

    // Get the total number of records
    $qrysql  = 'SELECT COUNT(*) FROM #__emktg_plugin';

    $database->setQuery($qrysql);
    $total = $database->loadResult();

    // handle the page navigation
    include_once($mosConfig_absolute_path.'/administrator/includes/pageNavigation.php');
    $pageNav = new mosPageNav($total, $limitstart, $limit);

    // build the query to get the data to display
    $qrysql  = 'SELECT *'
        .' FROM #__emktg_plugin'
        .' ORDER BY ordering, name'
        .' LIMIT '.(int)$pageNav->limitstart.', '.(int)$pageNav->limit;

    // get the data to display
    $database->setQuery($qrysql);
    $rows = $database->loadObjectList();
    if ($database->getErrorNum()) {
        $emErrorHandler->addError('(plugin.admin.php->showPlugins() line ' . __LINE__ . '): '._EMKTG_PLUGIN_ERROR);
        return false;
    }

    // preparse the rows found
    if (!empty($rows)) {
        $k = 0;
        for ( $i=0, $n=count( $rows ); $i < $n; $i++ ) {
            $row = &$rows[$i];
            $row->id = $row->pluginid;
            $row->checked_out=0;
            $row->k = $k;

            $img    = $row->enabled ? 'tick.png' : 'publish_x.png';
            $task   = $row->enabled ? 'disableplugin' : 'enableplugin';
            $action = $row->enabled ? _EMKTG_DISABLE : _EMKTG_ENABLE;
            $alt = $row->enabled ? _EMKTG_ENABLED : _EMKTG_DISABLED;

            $plugin = new sm2emailmarketingPlugin();
            $plugin->load($row->id);
            $pluginObj = null;
            $plugin->loadPluginObj($pluginObj, $option);

            if ($pluginObj->validatePlugin()) {
                $row->enabledDisplay = '<a href="javascript: void(0);" '
                    .'onclick="return listItemTask(\'cb'. $i .'\',\''. $task .'\')" '
                    .'title="'. $action .'">'
                    .'<img src="images/'. $img .'" border="0" alt="'. $alt .'" title="'. $alt .'" />'
                    .'</a>';
            } else {
                $row->enabledDisplay = sm2emailmarketingReturnToolTip('_EMKTG_PLUGIN_INVALID', 'warning.png');
            }

            $row->link = '<a href="index2.php?option='.$option.'&task=editplugin&hidemainmenu=0&cid[]='
                    .$row->id.'" title="'._EMKTG_PLUGIN_EDIT.'">'.htmlspecialchars($row->name, ENT_QUOTES).'</a>';

            $row->checkedDisplay = mosCommonHTML::CheckedOutProcessing($row, $i);

            $row->orderUpIcon = $pageNav->orderUpIcon($i, true, 'orderupplugin', _EMKTG_PLUGIN_ORDER_UP);

            $row->orderDownIcon = $pageNav->orderDownIcon($i, $n, true, 'orderdownplugin', _EMKTG_PLUGIN_ORDER_DOWN);

            $k = 1 - $k;
        } // for
    }

    // load the language elements
    $lang = sm2emailmarketingLanguage();

    // call the object to display the list
    mosCommonHTML::loadOverlib();

    $tmpl = sm2emailmarketingPatTemplate('plugin.tpl', $option);

    $tmpl->addVar('header', 'FORM_NAME', _EMKTG_PLUGIN_FORM);
    $tmpl->addVar('manager', 'NUM_ROWS', count($rows));
    $tmpl->addObject('manager', $lang, 'LANG');
    $tmpl->addObject('rows', $rows, 'ROW_');
    $tmpl->addVar('manager', 'LISTFOOTER', $pageNav->getListFooter());

    $tmpl->displayParsedTemplate('manager');

} // showPlugins()

function editPlugin($id, $option) {
    global $mosConfig_absolute_path;

    // create an instance of the table class
    $row = new sm2emailmarketingPlugin();

    // Load the row from the db table
    $row->load($id);

    // initialise lists
    $params =& new sm2emailmarketingParams( $row->params, $mosConfig_absolute_path.'/administrator/components/'.$option.'/plugins/'.str_replace('.php', '.xml', $row->filename), 'sm2emailmarketing_plugin' );

    $pluginObj = null;
    $row->loadPluginObj($pluginObj, $option);

    $lists = new stdClass();
    if ($pluginObj->validatePlugin()) {
        $lists->enabled = mosHTML::yesnoRadioList('enabled', 'class="inputbox"', $row->enabled);
    } else {
        $lists->enabled = sm2emailmarketingReturnToolTip('_EMKTG_PLUGIN_INVALID', 'warning.png')
            .'<input type="hidden" name="enabled" value="'.$row->enabled.'" />';
    }

    // load the language elements
    $lang = sm2emailmarketingLanguage();

    // display the form
    mosMakeHtmlSafe($row, ENT_QUOTES);
    mosCommonHTML::loadOverlib();

    $tmpl = sm2emailmarketingPatTemplate('plugin.tpl', $option);

    $tmpl->addVar('header', 'FORM_NAME', _EMKTG_PLUGIN_EDIT);
    $tmpl->addVar('edit', 'PARAMS', $params->render());
    $tmpl->addObject('edit', $lang, 'LANG');
    $tmpl->addObject('edit', $row, 'ROW_');
    $tmpl->addObject('edit', $lists, 'LIST_');

    $tmpl->displayParsedTemplate('edit');

} // editPlugin

function savePlugin($option, $task) {
    global $emErrorHandler;

    // create an instance of the table class
    $row = new sm2emailmarketingPlugin();

    // check the record
    if (!$row->check()) {
        $emErrorHandler->addError($row->getError(), true);
    }

    // attempt to bind the class to the data posted
    if (!$row->bind($_POST)) {
        $emErrorHandler->addError($row->getError(), true);
    }

    // process the parameters
    $params = mosGetParam( $_POST, 'params', '' );
    if (is_array( $params )) {
        $txt = array();
        foreach ( $params as $k=>$v) {
            $txt[] = $k.'='.$v;
        }

        //$row->params = implode( "\n", $txt );
        $row->params = mosParameters::textareaHandling($txt);
    }

    // attempt to insert/update the record
    if (!$row->store()) {
        $emErrorHandler->addError($row->getError(), true);
    }

    switch ($task) {
        case 'applyplugin':
            mosRedirect('index2.php?option='.$option.'&task=editplugin&hidemainmenu=0&cid[]='.$row->pluginid, _EMKTG_PLUGIN_SAVED);
            break;
        case 'saveplugin':
        default:
            mosRedirect('index2.php?option='.$option.'&task=showplugins', _EMKTG_PLUGIN_SAVED);
            break;
    }

} // savePlugin()

/**
 * Function to look for new plugins and install them and removed old plugins from the table
 */
function updatePlugins($option) {
    global $mosConfig_absolute_path, $database;

    // create the plugin object
    $plugin = new sm2emailmarketingPlugin();

    // get the currently existing plugins
    $database->setQuery('SELECT pluginid, name, filename, classname FROM #__emktg_plugin');
    $filenames = $database->loadObjectList('filename');

    // get a lookup array ready
    $foundFilenames = array();

    // directory to look for the plugins
    $dir = $mosConfig_absolute_path.'/administrator/components/'.$option.'/plugins/';

    // Open the directory, and proceed to read its contents
    if (is_dir($dir)) {
        if ($dh = opendir($dir)) {
            // loop thru all of the files
            while (($file = readdir($dh)) !== false) {
                // skip the directory files
                if (($file=='.') || ($file=='..')) continue;

                // make sure the file is a plugin file
                if (substr($file,-11)!='.plugin.php') continue;

                // add to the foundFilenames
                $foundFilenames[] = $file;

                // lets get the object
                $obj = require_once($mosConfig_absolute_path.'/administrator/components/'.$option.'/plugins/'.$file);

                // see if we need to install
                if (!isset($filenames[$file])) {
                    $obj->installPlugin();
                } else {
                    $plugin->load($filenames[$file]->pluginid);

                    // check for enabled
                    $enabled = $plugin->enabled;
                    if (!$obj->validatePlugin()) $enabled = 0;

                    $plugin->enabled = $enabled;
                    $plugin->store();
                }
            }
            closedir($dh);
        }
    }

    // now finally look for plugins to be removed from the table
    foreach ($filenames as $filename=>$obj) {
        if (!in_array($filename, $foundFilenames)) {
            $plugin->delete($obj->pluginid);
        }
    }

    $plugin->updateOrder();

} // updatePlugins()


function orderPlugin($id, $inc, $option) {
    global $database;

    $row = new sm2emailmarketingPlugin($database);
    $row->load($id);
    $row->move($inc);

    mosRedirect('index2.php?option='.$option.'&task=showplugins', _EMKTG_PLUGIN_ORDER_SAVED);
} // orderPlugin()


function savePluginOrder($cid, $option) {
    global $database, $emErrorHandler;

    $total = count($cid);
    $order = mosGetParam($_POST, 'order', array(0));
    $row = new sm2emailmarketingPlugin($database);
    $conditions = array();

    // update ordering values
    for ($i=0; $i < $total; $i++) {
        $row->load($cid[$i]);
        if ($row->ordering != $order[$i]) {
            $row->ordering = $order[$i];
            if (!$row->store()) {
                $emErrorHandler->addError($row->getError(), true);
            } // if
        } // if
    } // for

    $row->updateOrder();

    mosRedirect('index2.php?option='.$option.'&task=showplugins', _EMKTG_PLUGIN_ORDER_SAVED);
} // savePluginOrder


function changePluginState($id=null, $enabled=1,  $option) {
    global $database, $emErrorHandler;

    if ($enabled) {
        $action = _EMKTG_ENABLED;
        $successful = _EMKTG_PLUGIN_ENABLED;
    } else {
        $action = _EMKTG_DISABLED;
        $successful = _EMKTG_PLUGIN_DISABLED;
    }
    $pleaseSelect = _EMKTG_PLUGIN_PLEASE_SELECT.$action.'.';

    // check to make sure something to publish has been selected
    if (!is_array($id) || count($id) < 1) {
        $emErrorHandler->addError('(plugin.admin.php->changePluginState() line ' . __LINE__ . '):Nothing selected to change', true);
    }

    // load the table object and perform a publish
    $row = new sm2emailmarketingPlugin();

    if (!$row->enable($id,$enabled)) {
        $emErrorHandler->addError($row->getError(), true);
    }

    mosRedirect('index2.php?option='.$option.'&task=showplugins', $successful);

} // changePluginState()
