<?php
// $Id: template.admin.php,v 1.13 2008/04/21 04:07:40 sm2tony Exp $
/**
 * sm2emailmarketing - Email Marketing Newsletter
 *
 * Joomla! Newsletter Component that supports multiple list mailing and message queueing
 *
 * @package Joomla!
 * @subpackage com_sm2emailmarketing
 * @copyright Copyright (C) 2006 SM2 / All Rights Reserved
 * @license commercial
 * @author SM2 <info@sm2joomla.com>
 */

/**
 * Makes sure this is included by a parent file
 */
defined('_VALID_MOS') or die('Direct Access to this location is not allowed.');

function showTemplates($option) {
    global $database, $mainframe, $mosConfig_list_limit, $mosConfig_absolute_path, $emErrorHandler;

    // get some data from the request information
    $limit = $mainframe->getUserStateFromRequest( 'viewtemplatelimit', 'limit', $mosConfig_list_limit );
    $limitstart = $mainframe->getUserStateFromRequest( 'view'.$option.'templatestart', 'limitstart', 0 );
    $search = $mainframe->getUserStateFromRequest('search'.$option.'template', 'search', '');
    $search = $database->getEscaped(trim(strtolower($search)));

    // determine if there is anything to search for
    if (!empty($search)) {
        $where = ' WHERE (LOWER(template_name) LIKE '
            .$database->Quote('%'.$search.'%').')';
    } else {
        $where = '';
    }

    // Get the total number of records
    $qrysql  = 'SELECT COUNT(*)'
        .' FROM #__emktg_template'
        .$where;

    $database->setQuery($qrysql);
    $total = $database->loadResult();

    // handle the page navigation
    include_once($mosConfig_absolute_path.'/administrator/includes/pageNavigation.php');
    $pageNav = new mosPageNav($total, $limitstart, $limit);

    // build the query to get the data to display
    $qrysql  = 'SELECT templateid, template_name, published'
        .' FROM #__emktg_template'
        .$where
        .' ORDER BY template_name'
        .' LIMIT '.(int)$pageNav->limitstart.', '.(int)$pageNav->limit;

    // get the data to display
    $database->setQuery($qrysql);
    $rows = $database->loadObjectList();
    if ($database->getErrorNum()) {
        $emErrorHandler->addError('(template.admin.php->showTemplates() line ' . __LINE__ . '): '._EMKTG_TEMPLATE_ERROR);
        return false;
    }

    // preparse the rows
    if (!empty($rows)) {
        $k = 0;
        for ($i=0; $i < count($rows); $i++) {
            $row = &$rows[$i];
            $row->id = $row->templateid;
            $row->checked_out=0;
            $row->k = $k;

            $row->link = 'index2.php?option='.$option.'&task=edittemplate&hidemainmenu=0&cid[]='.$row->id;

            $row->checkedDisplay = mosCommonHTML::CheckedOutProcessing($row, $i);
            $row->publishedDisplay = mosCommonHTML::PublishedProcessing($row, $i);

            $row->publishedDisplay = str_replace(",'unpublish')", ",'unpublishtemplate')", $row->publishedDisplay);
            $row->publishedDisplay = str_replace(",'publish')", ",'publishtemplate')", $row->publishedDisplay);

            $k = 1 - $k;
        } // for
    }

    // load the language elements
    $lang = sm2emailmarketingLanguage();

    // call the object to display the list
    $tmpl = sm2emailmarketingPatTemplate('template.tpl', $option);

    $tmpl->addVar('header', 'FORM_NAME', _EMKTG_TEMPLATE_FORM);
    $tmpl->addVar('manager', 'SEARCH', $search);
    $tmpl->addVar('manager', 'NUM_ROWS', count($rows));
    $tmpl->addObject('manager', $lang, 'LANG');
    $tmpl->addObject('rows', $rows, 'ROW_');
    $tmpl->addVar('manager', 'LISTFOOTER', $pageNav->getListFooter());

    $tmpl->displayParsedTemplate('manager');

} // showTemplates()

function editTemplate($id, $option, $task) {
    // create an instance of the table class
    $row = new sm2emailmarketingTemplate();

    // Load the row from the db table
    $row->load($id);

    // initialise lists
    $lists = new stdClass();

    $lists->published = mosAdminMenus::Published($row);

    switch ($task) {
        case 'newtemplate':
            $title= _E_ADD;
            break;
        case 'copytemplate':
            $title = _EMKTG_COPY;
            $row->templateid = 0;
            $row->template_name = sm2emailmarketingProcessCopyTitle($row->template_name);
            break;
        case 'edittemplate':
        default:
            $title = _E_EDIT;
            break;
    }

    // load the language elements
    $lang = sm2emailmarketingLanguage();

    // display the form
    mosMakeHtmlSafe($row);
    mosCommonHTML::loadOverlib();

    $tmpl = sm2emailmarketingPatTemplate('template.tpl', $option);

    $tmpl->addVar('header', 'FORM_NAME', $title.' '._EMKTG_TEMPLATE);
    $tmpl->addObject('edit', $lang, 'LANG');
    $tmpl->addObject('edit', $row, 'ROW_');
    $tmpl->addObject('edit', $lists, 'LIST_');

    $tmpl->displayParsedTemplate('edit');

} // editTemplate()

function saveTemplate($option, $task) {
    global $emErrorHandler;

    // create an instance of the table class
    $row = new sm2emailmarketingTemplate();

    // attempt to bind the class to the data posted
    if (defined('_JLEGACY')) {
        $post = JRequest::get('post');
        $post['layout_html'] = JRequest::getVar('layout_html', '', 'POST', 'string', JREQUEST_ALLOWRAW);
    } else {
        $post = $_POST;
    }

    if (!$row->bind($post)) {
        $emErrorHandler->addError($row->getError(), true);
    }

    // check the record
    if (!$row->check()) {
        $emErrorHandler->addError($row->getError(), true);
    }

    // attempt to insert/update the record
    if (!$row->store()) {
        $emErrorHandler->addError($row->getError(), true);
    }

    switch ($task) {
        case 'applytemplate':
            mosRedirect('index2.php?option='.$option.'&task=edittemplate&hidemainmenu=0&cid[]='.$row->templateid, _EMKTG_TEMPLATE_SAVED);
            break;
        case 'applypreviewtemplate':
            mosRedirect('index2.php?option='.$option.'&task=previewtemplate&return=edit&cid[]='.$row->templateid, _EMKTG_TEMPLATE_SAVED);
            break;
        case 'savetemplate':
        default:
            mosRedirect('index2.php?option='.$option.'&task=showtemplates', _EMKTG_TEMPLATE_SAVED);
            break;
    }

} // saveTemplate()

function deleteTemplate($ids, $option) {
    global $emErrorHandler;

    // validate that ids are ok
    if (!is_array($ids) or count($ids) < 1) {
        $emErrorHandler->addError('(template.admin.php->deleteTemplate() line ' . __LINE__ . '):Nothing selected to process', true);
    }

    // load the table object and perform a delete
    $row = new sm2emailmarketingTemplate();

    foreach ($ids as $id) {
        if ($row->deleteCheck($id)) {
            $emErrorHandler->addError($row->getError(), true);
        }
        if (!$row->delete($id)) {
            $emErrorHandler->addError($row->getError(), true);
        }
    }

    mosRedirect('index2.php?option='.$option.'&task=showtemplates', _EMKTG_TEMPLATE_DELETE);

} // deleteTemplate()

function publishTemplates($id=null, $publish=1,  $option) {
    global $emErrorHandler;

    if ($publish) {
        $action = _CMN_PUBLISHED;
        $successful = _EMKTG_TEMPLATE_PUBLISHED;
    } else {
        $action = _CMN_UNPUBLISHED;
        $successful = _EMKTG_TEMPLATE_PUBLISHED;
    }
    $pleaseSelect = _EMKTG_TEMPLATE_PLEASE_SELECT.$action.'.';

    // check to make sure something to publish has been selected
    if (!is_array($id) || count($id) < 1) {
        $emErrorHandler->addError('(template.admin.php->publishTemplates() line ' . __LINE__ . '):Nothing selected to process', true);
    }

    // load the table object and perform a publish
    $row = new sm2emailmarketingTemplate();

    if (!$row->publish_array($id,$publish)) {
        $emErrorHandler->addError($row->getError(), true);
    }

    mosRedirect('index2.php?option='.$option.'&task=showtemplates', $successful);

} // publishTemplates

function previewTemplate($id, $option) {
	global $mosConfig_live_site;
    $action = mosGetParam($_REQUEST, 'action', '');
    $return = mosGetParam($_REQUEST, 'return', 'mngr');

    switch ($action) {
        case 'preview':
            // create an instance of the table class
            $row = new sm2emailmarketingTemplate();

            // Load the row from the db table
            $row->load($id);

            //
            $row->layout_html = str_replace('[CONTENT]', _EMKTG_LOREM, $row->layout_html);
            $row->layout_text = str_replace('[CONTENT]', _EMKTG_LOREM, $row->layout_text);
            $row->layout_html = str_replace('[BASEURL]', $mosConfig_live_site, $row->layout_html);
            $row->layout_text = str_replace('[BASEURL]', $mosConfig_live_site, $row->layout_text);
            //

            echo $row->layout_html;
            break;
        default:
            // determine where to go on the cancel button
            if ($return=='mngr') {
                $return = 'index2.php?option='.$option.'&task=showtemplates';
            } else {
                $return = 'index2.php?option='.$option.'&task=edittemplate&hidemainmenu=0&cid[]='.$id;
            }
//            HTML_sm2emailmarketing_Templates::previewTemplateFrame($id, $option, $return);

            // load the language elements
            $lang = sm2emailmarketingLanguage();
            $tmpl = sm2emailmarketingPatTemplate('template.tpl', $option);

            $tmpl->addVar('header', 'FORM_NAME', _EMKTG_PREVIEW.' '._EMKTG_TEMPLATE.' - HTML Version');
            $tmpl->addVar('previewframe', 'return', $return);
            $tmpl->addVar('previewframe', 'id', $id);
            $tmpl->addObject('previewframe', $lang, 'LANG');

            $tmpl->displayParsedTemplate('previewframe');
            break;

    } // action
} // previewTemplate()
