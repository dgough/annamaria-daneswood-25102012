<?php
// $Id: unregistered.admin.php,v 1.25 2008/04/02 05:42:05 sm2tony Exp $
/**
 * sm2emailmarketing - Email Marketing Newsletter
 *
 * Joomla! Newsletter Component that supports multiple list mailing and message queueing
 *
 * @package Joomla!
 * @subpackage com_sm2emailmarketing
 * @copyright Copyright (C) 2006 SM2 / All Rights Reserved
 * @license commercial
 * @author SM2 <info@sm2joomla.com>
 */

/**
 * Makes sure this is included by a parent file
 */
defined('_VALID_MOS') or die('Direct Access to this location is not allowed.');

function showUnregistered($option) {
    global $database, $mainframe, $mosConfig_list_limit, $mosConfig_absolute_path, $emErrorHandler;

    if (!defined('_SM2EM_LISTS')) {
        mosRedirect('index2.php?option='.$option, _EMKTG_LIST_DISABLED);
        exit();
    }

    // get some data from the request information
    $limit = (int) $mainframe->getUserStateFromRequest( 'viewunregisteredlimit', 'limit', $mosConfig_list_limit );
    $limitstart = (int) $mainframe->getUserStateFromRequest( 'view'.$option.'unregisteredstart', 'limitstart', 0 );
    $search = $mainframe->getUserStateFromRequest('search'.$option.'unregistered', 'search', '');
    $search = $database->getEscaped(trim(strtolower($search)));
    $receiveHTMLFilter = (int) $mainframe->getUserStateFromRequest('receive'.$option.'unregistered', 'receive_html', -1);
    $listFilter = (int) $mainframe->getUserStateFromRequest('list'.$option.'unregistered', 'list', -1);
    $confirmedFilter = (int) $mainframe->getUserStateFromRequest('confirmed'.$option.'unregistered', 'confirmed', -1);
    $subscribedFilter = (int) $mainframe->getUserStateFromRequest('subscribed'.$option.'unregistered', 'subscribed', -1);

    $where = array('(1=1)');
    $useJoin = false;

    // determine if there is anything to search for
    if (!empty($search)) {
        $where[] = '((lower(s.name) like '.$database->Quote('%'.$search.'%').') '
            .' or (lower(s.email) like '.$database->Quote('%'.$search.'%').'))';
    }

    if ($receiveHTMLFilter >= 0) {
        $where[] = '(s.receive_html='.$receiveHTMLFilter.')';
    }

    if ($listFilter==0) {
        // disabling no list subscription search
        // because it is too slow for large numbers of subscribers
        //$where[] = '(ls.listid is null)';
    } else if ($listFilter > 0) {
        $where[] = '(ls.listid='.$listFilter.')';
        $useJoin = true;
    }

    if ($confirmedFilter >= 0) {
        $where[] = '(s.confirmed='.$confirmedFilter.')';
    }

    if ($subscribedFilter==0) {
        $where[] = '(s.unsubscribe_date!=0)';
    } else if ($subscribedFilter==1) {
        $where[] = '(s.unsubscribe_date=0)';
    }

    // Get the total number of records
    $database->setQuery('SELECT COUNT(DISTINCT s.subscriberid)'
        .' FROM #__emktg_subscriber s'
        .($useJoin ? ' INNER JOIN #__emktg_list_subscriber ls'
            .' ON (s.subscriberid=-ls.subscriberid)' : '')
        .' WHERE '.implode(' AND ', $where));
    $total = $database->loadResult();

    // handle the page navigation
    include_once($mosConfig_absolute_path.'/administrator/includes/pageNavigation.php');
    $pageNav = new mosPageNav($total, $limitstart, $limit);

    // build the query to get the data to display
    $database->setQuery('SELECT DISTINCT s.*'
        .' FROM #__emktg_subscriber s'
        .($useJoin ? ' LEFT JOIN #__emktg_list_subscriber ls'
            .' ON (s.subscriberid=-ls.subscriberid)' : '')
        .' WHERE '.implode(' AND ', $where)
        .' ORDER BY `name`'
        .' LIMIT '.(int)$pageNav->limitstart.', '.(int)$pageNav->limit);

    // get the data to display
    $rows = $database->loadObjectList();
    if ($database->getErrorNum()) {
        $emErrorHandler->addError('(unregistered.admin.php->showUnregistered() line ' . __LINE__ . '): '._EMKTG_UNREGISTERED_ERROR);
        return false;
    }

    // preparse the rows found
    if (!empty($rows)) {
        $k = 0;
        for ($i=0; $i < count($rows); $i++) {
            $row = &$rows[$i];
            $row->id = $row->subscriberid;
            $row->checked_out=0;
            $row->k = $k;

            $row->link = 'index2.php?option='.$option.'&task=editunregistered&hidemainmenu=0&cid[]='.$row->id;

            $row->checkedDisplay = mosCommonHTML::CheckedOutProcessing($row, $i);

            $img    = $row->receive_html ? 'tick.png' : 'publish_x.png';
            $task   = $row->receive_html ? 'unhtmlunregistered' : 'htmlunregistered';
            $alt    = $row->receive_html ? _EMKTG_RECEIVE_HTML : _EMKTG_RECEIVE_TEXT;
            $action = $row->receive_html ? _EMKTG_RECEIVE_TEXT : _EMKTG_RECEIVE_HTML;
            $row->receive_htmlDisplay = '<a href="javascript: void(0);" onclick="return listItemTask(\'cb'. $i .'\',\''. $task .'\')" title="'. $action .'"><img src="images/'. $img .'" border="0" alt="'. $alt .'" /></a>';

            $img    = $row->confirmed ? 'tick.png' : 'publish_x.png';
            $task   = $row->confirmed ? 'unconfirmunregistered' : 'confirmunregistered';
            $alt    = $row->confirmed ? _EMKTG_CONFIRMED : _EMKTG_UNCONFIRMED;
            $action = $row->confirmed ? _EMKTG_UNCONFIRM : _EMKTG_CONFIRM;
            $row->confirmedDisplay = '<a href="javascript: void(0);" onclick="return listItemTask(\'cb'. $i .'\',\''. $task .'\')" title="'. $action .'"><img src="images/'. $img .'" border="0" alt="'. $alt .'" /></a>';

            $k = 1 - $k;
        } // for
    }

    // load the language elements
    $lang = sm2emailmarketingLanguage();

    // call the object to display the list
    $tmpl = sm2emailmarketingPatTemplate('unregistered.tpl', $option);

    $tmpl->addVar('header', 'FORM_NAME', _EMKTG_UNREGISTERED_FORM);
    $tmpl->addVar('manager', 'SEARCH', $search);
    $tmpl->addVar('manager', 'NUM_ROWS', count($rows));
    $tmpl->addObject('manager', $lang, 'LANG');
    $tmpl->addObject('rows', $rows, 'ROW_');
    $tmpl->addVar('manager', 'LISTFOOTER', $pageNav->getListFooter());

    // build the filter list values

    // receiveHTML filter options
    $receiveHTMLOptions = array(
        mosHTML::makeOption(0, _EMKTG_RECEIVE_TEXT),
        mosHTML::makeOption(1, _EMKTG_RECEIVE_HTML)
    );
    foreach ($receiveHTMLOptions as $index=>$obj) {
        $receiveHTMLOptions[$index]->selected = '';
        if ($receiveHTMLFilter==$obj->value) {
            $receiveHTMLOptions[$index]->selected = 'selected="selected"';
        }
    }
    $tmpl->addObject('filter_receive_html', $receiveHTMLOptions);

    // confirmed filter options
    $confirmedOptions = array(
        mosHTML::makeOption(0, _EMKTG_UNCONFIRMED),
        mosHTML::makeOption(1, _EMKTG_CONFIRMED)
    );
    foreach ($confirmedOptions as $index=>$obj) {
        $confirmedOptions[$index]->selected = '';
        if ($confirmedFilter==$obj->value) {
            $confirmedOptions[$index]->selected = 'selected="selected"';
        }
    }
    $tmpl->addObject('filter_confirmed', $confirmedOptions);

    // subscribed filter options
    $subscribedOptions = array(
        mosHTML::makeOption(0, _EMKTG_UNSUBSCRIBE_REASON_NULL),
        mosHTML::makeOption(1, _EMKTG_UNSUBSCRIBE_REASON_0)
    );
    foreach ($subscribedOptions as $index=>$obj) {
        $subscribedOptions[$index]->selected = '';
        if ($subscribedFilter==$obj->value) {
            $subscribedOptions[$index]->selected = 'selected="selected"';
        }
    }
    $tmpl->addObject('filter_subscribed', $subscribedOptions);

    // list filter options
    $database->setQuery('SELECT listid AS `value`, list_name AS `text`'
        .' FROM #__emktg_list'
        .' WHERE published=1 AND access=0'
        .' ORDER BY list_name');
    $listOptions = $database->loadObjectList();

    //array_unshift($listOptions, mosHTML::makeOption(0, _EMKTG_FILTER_NOLIST_TEXT));
    foreach ($listOptions as $index=>$obj) {
        $listOptions[$index]->selected = '';
        if ($listFilter==$obj->value) {
            $listOptions[$index]->selected = 'selected="selected"';
        }
    }
    $tmpl->addObject('filter_list', $listOptions);

    $tmpl->displayParsedTemplate('manager');

} // showUnregistered()

function editUnregistered($id, $option) {
    global $database;

    // create an instance of the table class
    $row = new sm2emailmarketingSubscriber();

    // Load the row from the db table
    $row->load($id);

    // initialise lists
    $lists = new stdClass();

    $lists->confirmed = mosHTML::yesnoRadioList('confirmed', 'class="inputbox"', $row->confirmed);
    $lists->receive_html = mosHTML::yesnoRadioList('receive_html', 'class="inputbox"', $row->receive_html);

    // build the list of lists we can subscribe to
    $sql = 'SELECT listid, list_name FROM #__emktg_list'
        .' WHERE published=1 AND access=0'
        .' ORDER BY list_name';
    $database->setQuery($sql);
    $availLists = $database->loadObjectList();

    // get the list of subscribed lists
    $database->setQuery('SELECT listid FROM #__emktg_list_subscriber WHERE subscriberid=-'.(int)$id);
    $subscribedLists = $database->loadResultArray();

    $lists->subscription = '';
    foreach ($availLists as $rec) {
        if (in_array($rec->listid, $subscribedLists)) {
            $checked = ' checked';
        } else {
            $checked = '';
        }
        $lists->subscription .= '<label><input type="checkbox" name="listid[]" value="'.$rec->listid.'" '.$checked.' />';
        $lists->subscription .= '&nbsp;'.$rec->list_name."</label><br />\n";

    } // foreach

    // load the language elements
    $lang = sm2emailmarketingLanguage();

    // determine the status
    if ($row->unsubscribe_date!='0000-00-00 00:00:00' && $row->unsubscribe_reason==0) {
        $row->unsubscribe_reason=5;
    }
    $statusVar = '_EMKTG_UNSUBSCRIBE_REASON_'.$row->unsubscribe_reason;
    if (!empty($lang->$statusVar)) {
        $row->status = $lang->$statusVar;
    } else {
        $row->unsubscribe_reason = 5;
        $row->status = $lang->_EMKTG_UNSUBSCRIBE_REASON_5;
    }

    if ($row->unsubscribe_reason > 0 && !empty($id)) {
        // allow re-subscribing
        $row->resubscribe = 1;

        // determine the resubscribe message
        $msgVar = '_EMKTG_RESUBSCRIBE_MSG_'.$row->unsubscribe_reason;

        if (!empty($lang->$msgVar)) {
            $row->resubscribe_msg = $lang->$msgVar;
        }
    }

    // set field titles
//    $titles = new stdClass();
//    $titles->name = sm2emailmarketingReturnToolTip('_CMN_NAME');
//    $titles->email = sm2emailmarketingReturnToolTip('_CMN_EMAIL');
//    $titles->receive_html = sm2emailmarketingReturnToolTip('_EMKTG_RECEIVE_HTML');
//    $titles->confirmed = sm2emailmarketingReturnToolTip('_EMKTG_CONFIRMED');
//    $titles->subscribe_date = _EMKTG_SUBSCRIBE_DATE;
//    $titles->subscription = sm2emailmarketingReturnToolTip('_EMKTG_SUBSCRIPTION');

    // display the form
    mosMakeHtmlSafe($row);
    mosCommonHTML::loadOverlib();

    $tmpl = sm2emailmarketingPatTemplate('unregistered.tpl', $option);

    $tmpl->addVar('header', 'FORM_NAME', _E_EDIT.' '._EMKTG_UNREGISTERED_FORM);
    $tmpl->addObject('edit', $lang, 'LANG');
    $tmpl->addObject('edit', $row, 'ROW_');
    $tmpl->addObject('edit', $lists, 'LIST_');

    $tmpl->displayParsedTemplate('edit');

} // editUnregistered()

function saveUnregistered($option, $task) {
    global $emErrorHandler;

    $listIds = mosGetParam($_REQUEST, 'listid', array());

    // load the table object and perform a publish
    $row = new sm2emailmarketingSubscriber();

    // attempt to bind the class to the data posted
    if (defined('_JLEGACY')) {
        $post = JRequest::get('post');
    } else {
        $post = $_POST;
    }

    if (!$row->bind($post)) {
        $emErrorHandler->addError($row->getError(), true);
    }

    if (empty($row->subscribe_date)) {
        $row->subscribe_date = date('Y-m-d H:i:s');
    }

    // see if we need to resubscribe
    $resubscribe = (int)mosGetParam($_POST, 'resubscribe', 0);
    if ($resubscribe) {
        $row->unsubscribe_date = $row->_db->getNullDate();
        $row->unsubscribe_reason = 0;
    }

    // make sure the name and email are trim
    $row->name = trim($row->name);
    $row->email = trim($row->email);

    // attempt to insert/update the record
    if (!$row->store()) {
        $emErrorHandler->addError($row->getError(), true);
    }

    if (!$row->subscribe($listIds)) {
        $emErrorHandler->addError($row->getError(), true);
    }

    switch ($task) {
        case 'applyunregistered':
            mosRedirect('index2.php?option='.$option.'&task=editunregistered&hidemainmenu=0&cid[]='.$row->subscriberid, _EMKTG_SUBSCRIBER_SAVED);
            break;
        case 'saveunregistered':
        default:
            mosRedirect('index2.php?option='.$option.'&task=showunregistered', _EMKTG_SUBSCRIBER_SAVED);
            break;
    }

} // confirmUnregistered()

//
function unsubscribeUnregistered($ids, $option) {
    global $emErrorHandler;

	// validate that ids are ok
    if (!is_array($ids) or count($ids) < 1) {
        $emErrorHandler->addError('(unregistered.admin.php->unsubsubscribeUnregistered() line ' . __LINE__ . '):Nothing selected to process', true);
    }

    //load the table object and unsubscribe selected users
    $row = new sm2emailmarketingSubscriber();

    if (!$row->unsubscribe($ids)) {
        $emErrorHandler->addError($row->getError(), true);
    }

    mosRedirect('index2.php?option='.$option.'&task=showunregistered', _EMKTG_SUBSCRIBER_UNSUBSCRIBED);
} // unsubscribeUnregistered()

function deleteUnregistered($ids, $option) {
    global $emErrorHandler;

    // validate that ids are ok
    if (!is_array($ids) or count($ids) < 1) {
        $emErrorHandler->addError('(unregistered.admin.php->deleteUnregistered() line ' . __LINE__ . '):Nothing selected to process', true);
    }

    // load the table object and perform a delete
    $row = new sm2emailmarketingSubscriber();

    foreach ($ids as $id) {
        if (!$row->deleteRelated($id)) {
            $emErrorHandler->addError($row->getError(), true);
        }
        if (!$row->delete($id)) {
            $emErrorHandler->addError($row->getError(), true);
        }
    }

    mosRedirect('index2.php?option='.$option.'&task=showunregistered', _EMKTG_SUBSCRIBER_DELETED);

} // deleteUnregistered()

function confirmUnregistered($id=null, $confirm=1,  $option) {
    global $emErrorHandler;

    if ($confirm) {
        $action = _EMKTG_CONFIRMED;
        $successful = _EMKTG_SUBSCRIBER_CONFIRMED;
    } else {
        $action = _EMKTG_UNCONFIRMED;
        $successful = _EMKTG_SUBSCRIBER_UNCONFIRMED;
    }
    $pleaseSelect = _EMKTG_SUBSCRIBER_PLEASE_SELECT.$action.'.';

    // check to make sure something to publish has been selected
    if (!is_array($id) || count($id) < 1) {
        $emErrorHandler->addError('(unregistered.admin.php->confirmUnregistered() line ' . __LINE__ . '):Nothing selected to process', true);
    }

    // load the table object and perform a publish
    $row = new sm2emailmarketingSubscriber();

    if (!$row->confirm($id,$confirm)) {
        $emErrorHandler->addError($row->getError(), true);
    }

    mosRedirect('index2.php?option='.$option.'&task=showunregistered', $successful);

} // confirmUnregistered()

function confirmAllUnregistered($confirm=1,  $option) {
    global $emErrorHandler;

    if ($confirm) {
        $action = _EMKTG_CONFIRMED;
        $successful = _EMKTG_SUBSCRIBER_CONFIRMED;
    } else {
        $action = _EMKTG_UNCONFIRMED;
        $successful = _EMKTG_SUBSCRIBER_UNCONFIRMED;
    }

    // load the table object and perform a publish
    $row = new sm2emailmarketingSubscriber();

    if (!$row->confirmAll($confirm)) {
        $emErrorHandler->addError($row->getError(), true);
    }

    mosRedirect('index2.php?option='.$option.'&task=showunregistered', $successful);

} // confirmAllUnregistered()

function receiveHTMLUnregistered($id=null, $html=1,  $option) {
    global $emErrorHandler;

    if ($html) {
        $action = _EMKTG_RECEIVE_HTML;
        $successful = _EMKTG_SUBSCRIBER_RECEIVE_HTML;
    } else {
        $action = _EMKTG_RECEIVE_TEXT;
        $successful = _EMKTG_SUBSCRIBER_RECEIVE_TEXT;
    }
    $pleaseSelect = _EMKTG_SUBSCRIBER_PLEASE_SELECT.$action.'.';

    // check to make sure something to publish has been selected
    if (!is_array($id) || count($id) < 1) {
        $emErrorHandler->addError('(unregistered.admin.php->receiveHTMLUnregistered() line ' . __LINE__ . '):Nothing selected to process', true);
    }

    // load the table object and perform a publish
    $row = new sm2emailmarketingSubscriber();

    if (!$row->receiveHTML($id,$html)) {
        $emErrorHandler->addError($row->getError(), true);
    }

    mosRedirect('index2.php?option='.$option.'&task=showunregistered', $successful);

} // receiveHTMLUnregistered()

function receiveHTMLAllUnregistered($html=1,  $option) {
    global $emErrorHandler;

    if ($html) {
        $action = _EMKTG_RECEIVE_HTML;
        $successful = _EMKTG_SUBSCRIBER_RECEIVE_HTML;
    } else {
        $action = _EMKTG_RECEIVE_TEXT;
        $successful = _EMKTG_SUBSCRIBER_RECEIVE_TEXT;
    }

    // load the table object and perform a publish
    $row = new sm2emailmarketingSubscriber();

    if (!$row->receiveHTMLAll($html)) {
        $emErrorHandler->addError($row->getError(), true);
    }

    mosRedirect('index2.php?option='.$option.'&task=showunregistered', $successful);

} // receiveHTMLAllUnregistered()


function importUnregistered($option) {

    // determine which step we are in then call the appropriate function
    $step = (int) mosGetParam($_REQUEST, 'step', 1);

    switch ($step) {
        case 1:
        default:
            // display the upload interface
            importFormUnregistered($option);
            break;
        case 2:
            // display the field matching screen
            importUploadUnregistered($option);
            break;
        case 3:
            // process the upload and display the results
            importProcessUnregistered($option);
            break;
    }
} // importUnregistered()


function importFormUnregistered($option) {

    // cleanup any old csv files not processed
    cleanupOldCSVFiles();

    // load the language elements
    $lang = sm2emailmarketingLanguage();

    $tmpl = sm2emailmarketingPatTemplate('unregistered.tpl', $option);

    $tmpl->addVar('header', 'FORM_NAME', _EMKTG_UNREGISTERED_IMPORT.' '._EMKTG_UNREGISTERED_FORM);
    $tmpl->addObject('uploadform', $lang, 'LANG');

    $tmpl->displayParsedTemplate('uploadform');

} // importFormUnregistered()


function importUploadUnregistered($option) {
    global $mosConfig_absolute_path, $database;

    // get the information about the uploaded file
    $file = mosGetParam($_FILES, 'upload', array());

    if (!is_array($file)) {
        mosRedirect('index2.php?option='.$option.'&task=importunregistered', _EMKTG_UNREGISTERED_UPLOAD_ERR);
        return;
    }

    // check for any errors
    switch ($file['error']) {
        case UPLOAD_ERR_INI_SIZE:
        case UPLOAD_ERR_FORM_SIZE:
            // file was too large
            mosRedirect('index2.php?option='.$option.'&task=importunregistered', _EMKTG_UNREGISTERED_UPLOAD_ERR_FORM_SIZE);
            return;

        case UPLOAD_ERR_PARTIAL:
            // only partial file returned
            mosRedirect('index2.php?option='.$option.'&task=importunregistered', _EMKTG_UNREGISTERED_UPLOAD_ERR_PARTIAL);
            return;

        case UPLOAD_ERR_NO_FILE:
            // no file uploaded
            mosRedirect('index2.php?option='.$option.'&task=importunregistered', _EMKTG_UNREGISTERED_UPLOAD_ERR_NO_FILE);
            return;
    } // switch error

    // determine what the new filename will be
    // format is YYYYMMDD[tmp_name].csv
    $newFilename = date('Ymd').basename($file['tmp_name']).'.csv';

    // move the uploaded file to somewhere where we can work on it
    $tempPath = $mosConfig_absolute_path.'/media/'.$newFilename;
    if (defined('_JLEGACY')) {
        $config =& JFactory::getConfig();
        $tempPath = $config->getValue('config.tmp_path').DS.$newFilename;
    }
    move_uploaded_file($file['tmp_name'], $tempPath);

    // get the field names from the csv file

    // open the file
    $file = fopen($tempPath, 'r');

    // process the first row as keys
    $keys = fgetcsv($file, 2048);

    // close the file
    fclose($file);

    $fields = array();
    foreach ($keys as $index=>$key) {
        $fields[] = mosHTML::makeOption($index, $key);
    }

    // get the list of available lists
    $database->setQuery('SELECT listid AS value, list_name AS text'
        .' FROM #__emktg_list'
        //.' WHERE published=1'
        .' ORDER BY list_name');
    $lists = $database->loadObjectList();


    // load the language elements
    $lang = sm2emailmarketingLanguage();

    $tmpl = sm2emailmarketingPatTemplate('unregistered.tpl', $option);

    $tmpl->addVar('header', 'FORM_NAME', _EMKTG_UNREGISTERED_IMPORT.' '._EMKTG_UNREGISTERED_FORM);
    $tmpl->addObject('importform', $lang, 'LANG');
    $tmpl->addObject('fieldlist', $fields);
    $tmpl->addObject('lists', $lists);
    $tmpl->addVar('importform', 'file', $newFilename);

    $tmpl->displayParsedTemplate('importform');

} // importUploadUnregistered()


function importProcessUnregistered($option) {
    global $mosConfig_absolute_path, $database;

    // get the information about the uploaded file
    $csvFileName = mosGetParam($_REQUEST, 'file', '');

    // hide the next toolbar button
    $GLOBALS['sm2em_import_processed'] = 1;

    // validate the file
    if (empty($csvFileName)) {
        // file not passed
        mosRedirect('index2.php?option='.$option.'&task=importunregistered', _EMKTG_UNREGISTERED_IMPORT_ERR_FILE);
        return;
    }

    $tempPath = $mosConfig_absolute_path.'/media/'.$csvFileName;
    if (defined('_JLEGACY')) {
        $config =& JFactory::getConfig();
        $tempPath = $config->getValue('config.tmp_path').DS.$csvFileName;
    }
    if (!file_exists($tempPath)) {
        // file is gone
        mosRedirect('index2.php?option='.$option.'&task=importunregistered', _EMKTG_UNREGISTERED_IMPORT_ERR_FILE);
        return;
    }

    // get the import options passed
    $import = mosGetParam($_REQUEST, 'import', array());

    // validate import options
    if (empty($import)) {
        // nothing passed
        mosRedirect('index2.php?option='.$option.'&task=importunregistered', _EMKTG_UNREGISTERED_IMPORT_ERR_OPTIONS);
        return;
    }

    // validate name
    if (@$import['name']=='') {
        // no name
        mosRedirect('index2.php?option='.$option.'&task=importunregistered', _EMKTG_UNREGISTERED_IMPORT_ERR_OPTIONS);
        return;
    }

    // validate the email
    if (@$import['email']=='') {
        // no email
        mosRedirect('index2.php?option='.$option.'&task=importunregistered', _EMKTG_UNREGISTERED_IMPORT_ERR_OPTIONS);
        return;
    }

    // validate the receive_html
    if (@$import['default_receive_html']=='') {
        // no default_receive_html
        mosRedirect('index2.php?option='.$option.'&task=importunregistered', _EMKTG_UNREGISTERED_IMPORT_ERR_OPTIONS);
        return;
    }

    // load the table object
    $row = new sm2emailmarketingSubscriber();

    // get the field names from the csv file
    // open the file
    $file = fopen($tempPath, 'r');

    // some arrays to store errors and notices in
    $errors = array();
    $notices = array();
    $index = 0;
    $size = 2048;

    // process the first row of keys and ignore
    fgetcsv($file, $size);

    // get a lowercase list of the existing lists
    $database->setQuery('SELECT listid, LOWER(list_name) AS list_name'
        .' FROM #__emktg_list');
    $existingLists = $database->loadObjectList('list_name');

    // create the list table object so we can create new lists if needed
    $newList = new sm2emailmarketingList();
    $newList->published = 1;

    // process the rest of the data
    while ($data = fgetcsv($file, $size)) {
        $index++;

        // set the id to force insert
        $row->subscriberid = 0;

        // set the name
        $row->name = trim($data[(int)$import['name']]);

        // set the email address
        $row->email = trim($data[(int)$import['email']]);

        // set the receive html
        $row->receive_html = (int)$import['default_receive_html'];
        if (!empty($import['receive_html'])) {
            switch (strtolower($data[(int)$import['receive_html']])) {
                case '':
                case null:
                default:
                    break;
                case 'html':
                case 1:
                    $row->receive_html = 1;
                    break;
                case 'text':
                case 0:
                    $row->receive_html = 0;
                    break;
            }
        }

        // set the subscribe_date
        $row->subscribe_date = date('Y-m-d H:i:s');

        // set confirmed
        $row->confirmed = 1;

        // store the data
        if (!@$row->store()) {
            // see what type of error
            if ($row->_db->_errorNum==1062) {
                // see if we can find this subscribers information
                if (!$row->getByEmail($row->email)) {
                    // apparently a duplicate, but we cannot find them
                    // so process as an error and skip to next record
                    $errors[$index] = getImportErrorObj($row, $index, _EMKTG_UNREGISTERED_IMPORT_ERR_STORE);
                    continue;
                }
                // if we get to here we are ok to processed
                $errors[$index] = getImportErrorObj($row, $index, _EMKTG_UNREGISTERED_IMPORT_DUP, false);
            } else {
                // unknown error so process and skip to the next record
                $errors[$index] = getImportErrorObj($row, $index, _EMKTG_UNREGISTERED_IMPORT_ERR_STORE);
                continue;
            }
        }

        // determine the lists to subscribe to
        $lists = array();
        if (is_array(@$import['default_lists'])) {
            $lists = array_merge($lists, $import['default_lists']);
        }

        // process the list fields passed
        $importLists = array();
        if (is_array(@$import['lists'])) {
            foreach ($import['lists'] as $key) {
                if (empty($data[(int)$key])) continue;
                $listnames = $data[(int)$key];
                $importLists = array_merge($importLists, explode('|', $listnames));
            }
        }

        // process the importLists
        foreach ($importLists as $listName) {
            if (array_key_exists(strtolower($listName), $existingLists)) {
                // list exists so add id to lists
                $lists[] = $existingLists[strtolower($listName)]->listid;
            } else {
                // create the new list
                $newList->listid = 0;
                $newList->list_name = $listName;

                if (!$newList->store()) {
                    continue;
                }
                $lists[] = $newList->listid;
                $existingLists[strtolower($listName)] = $newList;
            }
        }

        // ok now subscribe the subscriber to the lists
        if (!@$row->subscribe($lists, true)) {
            // store error so record the information in the errors
            $errors[$index] = getImportErrorObj($row, $index, _EMKTG_UNREGISTERED_IMPORT_ERR_SUBSCRIBE);
        }

    } // while

    // close the file
    fclose($file);

    // cleanup the old data file
    @unlink($tempPath);

    // load the language elements
    $lang = sm2emailmarketingLanguage();

    $tmpl = sm2emailmarketingPatTemplate('unregistered.tpl', $option);

    $tmpl->addVar('header', 'FORM_NAME', _EMKTG_UNREGISTERED_IMPORT.' '._EMKTG_UNREGISTERED_FORM);
    $tmpl->addObject('resultsform', $lang, 'LANG');
    if (count($errors)>100) {
        $tmpl->addVar('resultsform', 'truncerrors', _EMKTG_UNREGISTERED_IMPORT_ERR_MAX);
        $errors = array_slice($errors, 0, 100);
    }
    $tmpl->addObject('errors', $errors);
    $tmpl->addVar('resultsform', 'NUM_RECORDS', $index);
    $tmpl->addVar('resultsform', 'NUM_ERRORS', count($errors));

    $tmpl->displayParsedTemplate('resultsform');

} // importProcessUnregistered()


function getImportErrorObj($row, $index, $msg, $isError=true) {
    $errorObj = new stdClass();
    $errorObj->index=$index;
    $errorObj->details='';

    // build the error information
    if (empty($row->name)) {
        $msg = str_replace('%n', 'Unknown', $msg);
    } else {
        $msg = str_replace('%n', $row->name, $msg);
    }
    if (empty($row->email)) {
        $msg = str_replace('%e', 'Unknown', $msg);
    } else {
        $msg = str_replace('%e', $row->email, $msg);
    }
    if ($isError) {
        $error = $row->getError();
    } else {
        $error = '';
    }
    $msg = str_replace('%d', $error, $msg);
    $errorObj->details = $msg;

    return $errorObj;

} // getImportErrorObj()


function cleanupOldCSVFiles($days = 1) {
    global $mosConfig_absolute_path;

    // read the csv files from the media directory
    $tempPath = $mosConfig_absolute_path.'/media';
    if (defined('_JLEGACY')) {
        $config =& JFactory::getConfig();
        $tempPath = $config->getValue('config.tmp_path');
    }
    $CSVFiles = mosReadDirectory($tempPath, '.csv$', false, true);

    // get the compare date
    $compareDate = date('Ymd', mktime(0,0,0,date('m'),date('d')-$days, date('Y')));

    // process the files found
    foreach ($CSVFiles as $file) {
        // get the filename
        $filename = basename($file);

        // find the date at the start of the filename
        $date = substr($filename, 0, 8);

        // if no date then go to next file
        if (!is_numeric($date)) continue;

        // if the date is less than or equal to the compare date delete the file
        if (intval($date) <= intval($compareDate)) unlink($file);
    }

} // cleanupOldCSVFiles
