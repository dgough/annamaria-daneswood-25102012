<?php
// $Id: statistic.admin.php,v 1.21 2007/11/20 05:38:19 sm2james Exp $
/**
 * sm2emailmarketing - Email Marketing Newsletter
 *
 * Joomla! Newsletter Component that supports multiple list mailing and message queueing
 *
 * @package Joomla!
 * @subpackage com_sm2emailmarketing
 * @copyright Copyright (C) 2006 SM2 / All Rights Reserved
 * @license commercial
 * @author SM2 <info@sm2joomla.com>
 */

/**
 * Makes sure this is included by a parent file
 */
defined('_VALID_MOS') or die('Direct Access to this location is not allowed.');


function showStatistics($option) {
    global $database, $mainframe, $mosConfig_list_limit, $mosConfig_absolute_path, $emErrorHandler;

    // get some data from the request information
    $limit = $mainframe->getUserStateFromRequest( "viewstatisticslimit", 'limit', $mosConfig_list_limit );
    $limitstart = $mainframe->getUserStateFromRequest( "view{$option}statisticslimitstart", 'limitstart', 0 );

    // get the overall stats
    $database->setQuery('SELECT COUNT(m.messageid) AS messageid, SUM(s.html_sent) AS html_sent, SUM(s.text_sent) AS text_sent,'
        .' SUM(s.html_read) AS html_read, SUM(s.errors) AS errors,'
        .' SUM(s.blacklists) AS blacklists, SUM(s.bounces) AS bounces'
        .' FROM #__emktg_message m'
        .' INNER JOIN #__emktg_stats_overall s ON (m.messageid=s.messageid)');
    $stats=null;
    $database->loadObject($stats);

    if (!is_object($stats)) {
        $stats = new stdClass();
        $stats->messageid = $stats->html_sent = $stats->text_sent = $stats->html_read
            = $stats->errors = $stats->blacklists = $stats->bounces = 0;
    }
    $stats->html_unread = ($stats->html_sent - $stats->html_read - $stats->bounces);

    // Get the total number of records
    $qrysql  = 'SELECT COUNT(*) FROM #__emktg_stats_overall';

    $database->setQuery($qrysql);
    $total = $database->loadResult();

    // handle the page navigation
    include_once($mosConfig_absolute_path."/administrator/includes/pageNavigation.php");
    $pageNav = new mosPageNav($total, $limitstart, $limit);

    // build the query to get the data to display
    /*
    $qrysql  = "select m.messageid, m.subject, m.send_date, t.template_name, "
        ."\n s.html_sent, s.text_sent, s.html_read, s.errors, s.blacklists, s.bounces "
        ."\n from #__emktg_message m "
        ."\n inner join #__emktg_stats_overall s on (m.messageid=s.messageid) "
        ."\n inner join #__emktg_template t on (m.templateid=t.templateid) "
        ."\n order by m.send_date desc ";
    */
    $qrysql  = 'SELECT m.messageid, m.subject, m.send_date,'
        .' s.html_sent, s.text_sent, s.html_read, s.errors, s.blacklists, s.bounces'
        .' FROM #__emktg_message m'
        .' INNER JOIN #__emktg_stats_overall s ON (m.messageid=s.messageid)'
        .' INNER JOIN #__emktg_template t ON (m.templateid=t.templateid)'
        .' ORDER BY m.send_date desc'
        .' LIMIT '.(int)$pageNav->limitstart.', '.(int)$pageNav->limit;

    // get the data to display
    $database->setQuery($qrysql);
    $rows = $database->loadObjectList();
    if ($database->getErrorNum()) {
        $emErrorHandler->addError('(statistics.admin.php->showStatistics() line ' . __LINE__ . '): '._EMKTG_STATISTICS_ERROR);
        return false;
    }

    // preparse the rows found
    if (!empty($rows)) {
        $k = 0;
        for ($i=0; $i < count($rows); $i++) {
            $row = &$rows[$i];
            $row->id = $row->messageid;
            $row->checked_out=0;
            $row->k = $k;

            $row->link = "index2.php?option=$option&task=showmessagestatistics&hidemainmenu=0&cid[]=".$row->id;

            $row->checkedDisplay = mosCommonHTML::CheckedOutProcessing($row, $i);

            $row->send_dateDisplay = mosFormatDate($row->send_date, _EMKTG_MESSAGE_SENDDATE_FORMAT);
        } // for
    }

    // load the language elements
    $lang = sm2emailmarketingLanguage();

    // call the object to display the list
    $tmpl = sm2emailmarketingPatTemplate("statistics.tpl", $option);

    $tmpl->addVar('header', 'FORM_NAME', _EMKTG_STATISTICS_FORM);
    $tmpl->addVar('manager', 'NUM_ROWS', count($rows));
    $tmpl->addObject('manager', $lang, 'LANG');
    $tmpl->addObject('rows', $rows, "ROW_");
    $tmpl->addObject('manager', $stats, "STATS_");
    $tmpl->addVar('manager', 'LISTFOOTER', $pageNav->getListFooter());

    $tmpl->displayParsedTemplate("manager");

} // showStatistics()


function showMessageStats($id, $option) {
    global $database, $mosConfig_absolute_path, $mainframe;

    $limitstart = $mainframe->getUserStateFromRequest( "view{$option}statsstart", 'limitstart', 0 );
    if (empty($limitstart)) $limitstart = 0;
    $limit = 100;

    // get the data about this message
    $database->setQuery('SELECT m.messageid, m.subject, m.send_date, t.template_name,'
        .' s.html_sent, s.text_sent, s.html_read, s.errors, s.blacklists, s.bounces'
        .' FROM #__emktg_message m'
        .' INNER JOIN #__emktg_stats_overall s ON (m.messageid=s.messageid)'
        .' INNER JOIN #__emktg_template t ON (m.templateid=t.templateid)'
        .' WHERE m.messageid = '.(int)$id);
    $row = null;
    $database->loadObject($row);

    if (!is_object($row)) {
        mosRedirect("index2.php?option=$option&amp;task=showstatistics", _EMKTG_STATISTICS_ERROR);
        return;
    }

    $row->send_dateDisplay = mosFormatDate($row->send_date, _EMKTG_MESSAGE_SENDDATE_FORMAT);

    // determine what the total is
    $total = 0;
    $row->html_unread = ($row->html_sent - $row->html_read - $row->bounces);
    $total = max($row->html_read, $row->html_unread, $row->text_sent, $row->errors, $row->blacklists, $row->bounces);

    // handle the page navigation
    include_once($mosConfig_absolute_path."/administrator/includes/pageNavigation.php");
    $pageNav = new mosPageNav($total, $limitstart, $limit);

    // load the language elements
    $lang = sm2emailmarketingLanguage();
    $tmpl = sm2emailmarketingPatTemplate("statistics.tpl", $option);
    $tmpl->addVar('header', 'FORM_NAME', _EMKTG_STATISTICS_FORM);
    $tmpl->addObject('edit', $lang, 'LANG');
    $tmpl->addObject('edit', $row, "ROW_");


    // find the lists of users - paginated to use less memory
    // and process the template a little differently as well

    // html read
    $database->setQuery('SELECT * FROM #__emktg_stats_message'
        .' WHERE (messageid='.(int)$id.')'
        .' AND (html=1) AND (`read`=1) AND (send_date > 0) AND (status=1)'
        .' ORDER BY email, name'
        .' LIMIT '.(int)$pageNav->limitstart.', '.(int)$pageNav->limit);
    $htmlRead = $database->loadObjectList();
    if (is_array($htmlRead)) {
        foreach ($htmlRead as $rec) {
            $tmpl->addObject('htmlread', $rec, "HTMLREAD_");
            $tmpl->parseTemplate('htmlread', 'a');
        }
    }

    // html unread
    $database->setQuery('SELECT * FROM #__emktg_stats_message'
        .' WHERE (messageid='.(int)$id.')'
        .' AND (html=1) AND (`read`=0) AND (send_date > 0) AND (status=1)'
        .' ORDER BY email, name'
        .' LIMIT '.(int)$pageNav->limitstart.', '.(int)$pageNav->limit);
    $htmlUnread = $database->loadObjectList();
    if (is_array($htmlUnread)) {
        foreach ($htmlUnread as $rec) {
            $tmpl->addObject('htmlunread', $rec, "HTMLUNREAD_");
            $tmpl->parseTemplate('htmlunread', 'a');
        }
    }

    // text only
    $database->setQuery('SELECT * FROM #__emktg_stats_message'
        .' WHERE (messageid='.(int)$id.')'
        .' AND (html=0) AND (send_date > 0) AND (status=1)'
        .' ORDER BY email, name'
        .' LIMIT '.(int)$pageNav->limitstart.', '.(int)$pageNav->limit);
    $textOnly = $database->loadObjectList();
    if (is_array($textOnly)) {
        foreach ($textOnly as $rec) {
            $tmpl->addObject('textsent', $rec, "TEXTSENT_");
            $tmpl->parseTemplate('textsent', 'a');
        }
    }

    // determine if client management is installed
    $clientManagement = false;
    if (file_exists($mosConfig_absolute_path."/administrator/components/com_member")) {
        $clientManagement = true;
    }

    // errors
    $database->setQuery('SELECT * FROM #__emktg_stats_message'
        .' WHERE (messageid='.(int)$id.')'
        .' AND (status=2) AND (send_date > 0)'
        .' ORDER BY email, name'
        .' LIMIT '.(int)$pageNav->limitstart.', '.(int)$pageNav->limit);
    $errors = $database->loadObjectList();

    // process the errors to create the link to edit this subscription
    if (is_array($errors)) {
        foreach (array_keys($errors) as $index) {
            $error = &$errors[$index];

            if ($error->userid < 0) {
                // link to the list subscribers edit page
                $error->link = "index2.php?option=$option&amp;task=editunregistered"
                    ."&amp;hidemainmenu=0&amp;cid[]=".abs($error->userid);
            } else {
                // link to the client management subscriber
                if ($clientManagement) {
                    $error->link = "index2.php?option=com_member&amp;task=edit&amp;hidemainmenu=1&amp;id=".abs($error->userid);
                } else {
                    $error->link = "index2.php?option=com_users&amp;task=editA&amp;hidemainmenu=1&amp;id=".abs($error->userid);
                }
            }
            $tmpl->addObject('errors', $error, "ERROR_");
            $tmpl->parseTemplate('errors', 'a');
        }
    }

    // blacklists
    $database->setQuery('SELECT * FROM #__emktg_stats_message'
        .' WHERE (messageid='.(int)$id.')'
        .' AND (status=3) AND (send_date > 0)'
        .' ORDER BY email, name'
        .' LIMIT '.(int)$pageNav->limitstart.', '.(int)$pageNav->limit);
    $blacklists = $database->loadObjectList();
    if (is_array($blacklists)) {
        foreach ($blacklists as $rec) {
            $tmpl->addObject('blacklists', $rec, "BLACKLIST_");
            $tmpl->parseTemplate('blacklists', 'a');
        }
    }

    // bounces
    $database->setQuery('SELECT * FROM #__emktg_stats_message'
        .' WHERE (messageid='.(int)$id.')'
        .' AND (status=4) AND (send_date > 0)'
        .' ORDER BY email, name'
        .' LIMIT '.(int)$pageNav->limitstart.', '.(int)$pageNav->limit);
    $bounces = $database->loadObjectList();
    if (is_array($bounces)) {
        foreach ($bounces as $rec) {
            $tmpl->addObject('bounces', $rec, "BOUNCE_");
            $tmpl->parseTemplate('bounces', 'a');
        }
    }

    $tmpl->addVar('edit', 'PAGE_LINK', $pageNav->getPagesLinks());
    $tmpl->addVar('edit', 'PAGE_COUNTER', $pageNav->getPagesCounter());
    $tmpl->addVar('edit', 'ID', $id);

    $tmpl->displayParsedTemplate("edit");
} // showMessageStats()

function showStatisticsGraph($id, $option) {
    global $database, $mosConfig_absolute_path;
    global $sm2emPHPLotPath;

    // get some optional values from the request
    $fileformat = mosGetParam($_GET, "fileformat", "png");
    $width = intval(mosGetParam($_GET, "width", 500));
    $height = intval(mosGetParam($_GET, "height", 200));
    $border = mosGetParam($_GET, "border", "");
    $title = mosGetParam($_GET, "title", "");
    $legend = intval(mosGetParam($_GET, "legend", 1));

    $sm2emPHPLotPath = $mosConfig_absolute_path."/administrator/components/$option/includes/phplot";

    // include the phplot code
    include_once($sm2emPHPLotPath."/phplot.php");

    $graph =& new PHPlot($width, $height);
//    $graph->data_colors = array("SkyBlue", "green", "orange", "blue", "red", "violet", "azure1");
//    $graph->error_bar_colors = array("SkyBlue", "green", "orange", "blue", "red", "violet", "azure1");
	$text_color = array(51, 51, 51);
	$title_color = array(198, 73, 52);
	$grid_color = $text_color; // The border-color for the legend
	$colors = array(
				array(1, 114, 186),
				array(16, 84, 121),
				array(107, 163, 17),
				array(255, 138, 0),
				array(215, 238, 254)
			);
	$error_colors = array(
				array(204, 0, 0),
				array(159, 146, 0)
			);
	$graph->SetTextColor($text_color);
	$graph->SetTitleColor($title_color);
	$graph->SetGridColor($grid_color);
	$graph->SetDataColors($colors);
	$graph->SetErrorBarColors($error_colors);


    // build the data
    // get the data about this message
    if (empty($id)) {
        $database->setQuery('SELECT COUNT(m.messageid) AS messageid, SUM(s.html_sent) AS html_sent, SUM(s.text_sent) AS text_sent,'
            .' SUM(s.html_read) AS html_read, SUM(s.errors) AS errors,'
            .' SUM(s.blacklists) AS blacklists, SUM(s.bounces) AS bounces'
            .' FROM #__emktg_message m'
            .' INNER JOIN #__emktg_stats_overall s ON (m.messageid=s.messageid)');

    } else {
        $database->setQuery('SELECT m.messageid, m.subject,'
            .' s.html_sent, s.text_sent, s.html_read, s.errors,'
            .' s.blacklists, s.bounces'
            .' FROM #__emktg_message m'
            .' INNER JOIN  #__emktg_stats_overall s on (m.messageid=s.messageid)'
            .' INNER JOIN #__emktg_template t on (m.templateid=t.templateid)'
            .' WHERE m.messageid = '.(int)$id);
    }
    $row = null;
    $database->loadObject($row);

    if (!is_object($row)) return;

    $row->html_unread = ($row->html_sent - $row->html_read - $row->bounces);

    $plotData = array("data");
    $plotLegend = array();

    if (!empty($row->html_read)) {
        $plotData[]=$row->html_read;
        $plotLegend[] = _EMKTG_STATISTICS_HTML_READ;
    }

    if (!empty($row->html_unread)) {
        $plotData[]=$row->html_unread;
        $plotLegend[] = _EMKTG_STATISTICS_HTML_UNREAD;
    }

    if (!empty($row->text_sent)) {
        $plotData[]=$row->text_sent;
        $plotLegend[] = _EMKTG_STATISTICS_TEXT_SENT;
    }

    if (!empty($row->errors)) {
        $plotData[]=$row->errors;
        $plotLegend[] = _EMKTG_STATISTICS_ERRORS;
    }

    if (!empty($row->blacklists)) {
        $plotData[] = $row->blacklists;
        $plotLegend[] = _EMKTG_STATISTICS_BLACKLISTS;
    }

    if (!empty($row->bounces)) {
        $plotData[] = $row->bounces;
        $plotLegend[] = _EMKTG_STATISTICS_BOUNCES;
    }

    if (empty($plotLegend)) return;

    $plotData = array($plotData);

    $graph->SetDataType("text-data");
    $graph->SetDataValues($plotData);

    $graph->SetBackgroundColor(array(249,249,249));

    if (empty($title)) {
        if (!is_numeric($title)) {
            if (empty($id)) {
                $graph->SetTitle(_EMKTG_STATISTICS_OVERALL);
            } else {
                $graph->SetTitle($row->subject);
            }
        }
    } else {
        $graph->SetTitle($title);
    }

    if (!empty($border)) {
        if (is_numeric($border)) {
            $border = "plain";
        }

        switch ($border) {
            case "plain":
            case "raised":
                $graph->SetImageBorderType($border);
                break;
        }
    }

//
    if ($legend) {
        $graph->SetLegend($plotLegend);
    }

    $graph->SetFileFormat($fileformat);

    $graph->SetPlotType("pie");

    //Draw it
    $graph->DrawGraph();

} //showStatisticsGraph()
?>