<?php
// $Id: errorhandler.admin.php,v 1.5 2007/02/11 07:12:59 sm2tony Exp $
/**
 * sm2emailmarketing - Email Marketing Newsletter
 *
 * Joomla! Newsletter Component that supports multiple list mailing and message queueing
 *
 * @package Joomla!
 * @subpackage com_sm2emailmarketing
 * @copyright Copyright (C) 2006 SM2 / All Rights Reserved
 * @license commercial
 * @author SM2 <info@sm2joomla.com>
 *
 * Example Usage:
 * 		- Initialise:			$emError = new emError;
 * 		- Add an error:			$emError->addError("What on earth is this???");
 * 		- Add error and exit:	$emError->addError("What on earth is this???", true);
 * 		- Get errors for echo:	echo $emError->getErrors();
 */

 class emError {

 	var $emErrors = Array();

 	function addError($message, $exit=false) {
 		array_push($this->emErrors, $message);
 		if($exit == true) $this->emExit();
 	}

 	// Use for fatalistic errors:
 	function emExit() {
 		$_SESSION['emError'] = $this->emErrors;
 		echo "<script type='text/javascript'> window.history.go(-1); </script>\n";
 		exit;
 	}

 	// Initialise:
 	function emError() {
        // do not change error reporting - use Joomla! default
        //error_reporting(E_ALL);
 		if(!empty($_SESSION['emError'])) {
 			$this->emErrors = array_merge($this->emErrors, $_SESSION['emError']);
 			unset($_SESSION['emError']);
 		}
 	}

 	// Throw back formatted errors:
 	function getErrors() {
 		$return = '';
 		$count = count($this->emErrors);
 		if($count > 0) {
 			$return .= '<h3>'._EMKTG_ERROR.'</h3>';
 			if($count > 1) {
 				$return .= '<ol><li>';
 				$return .= implode('</li><li>', $this->emErrors);
 				$return .= '</li></ol>';
 			} else {
 				$return .= '<p>'.implode(',', $this->emErrors).'</p>';
 			}
 		}
 		return $return;
 	}

 }

?>
