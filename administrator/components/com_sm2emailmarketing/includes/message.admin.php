<?php
// $Id: message.admin.php,v 1.45 2008/03/31 01:59:10 sm2tony Exp $
/**
 * sm2emailmarketing - Email Marketing Newsletter
 *
 * Joomla! Newsletter Component that supports multiple list mailing and message queueing
 *
 * @package Joomla!
 * @subpackage com_sm2emailmarketing
 * @copyright Copyright (C) 2006 SM2 / All Rights Reserved
 * @license commercial
 * @author SM2 <info@sm2joomla.com>
 */

/**
 * Makes sure this is included by a parent file
 */

defined('_VALID_MOS') or die('Direct Access to this location is not allowed.');

function showMessages($option) {
    global $database, $mainframe, $mosConfig_list_limit, $mosConfig_absolute_path, $emErrorHandler;

    // get some data from the request information
    $limit = $mainframe->getUserStateFromRequest( 'viewmessagelimit', 'limit', $mosConfig_list_limit );
    $limitstart = $mainframe->getUserStateFromRequest( 'view'.$option.'messagestart', 'limitstart', 0 );
    $search = $mainframe->getUserStateFromRequest('search'.$option.'messages', 'search', '');
    $search = $database->getEscaped(trim(strtolower($search)));

    // test to see if necessary permission is set
    testJoomlaDatabaseUserPrivileges();

    // see if we may need to clean up some tables
    cleanupTemporaryTables();

    // determine if there is anything to search for
    if (!empty($search)) {
        $where = ' WHERE (LOWER(am.subject) LIKE '
            .$database->Quote('%'.$search.'%').')';
    } else {
        $where = '';
    }

    // Get the total number of records
    $qrysql  = 'SELECT COUNT(*)'
        .' FROM #__emktg_message am'
        .$where;

    $database->setQuery($qrysql);
    $total = $database->loadResult();

    // handle the page navigation
    include_once($mosConfig_absolute_path.'/administrator/includes/pageNavigation.php');
    $pageNav = new mosPageNav($total, $limitstart, $limit);

    // build the query to get the data to display
    $qrysql  = 'SELECT am.messageid, am.subject, at.template_name, am.send_date'
        .' FROM #__emktg_message am'
        .' LEFT JOIN #__emktg_template at ON am.templateid = at.templateid'
        .$where
        .' ORDER BY am.messageid desc'
        .' LIMIT '.(int)$pageNav->limitstart.', '.(int)$pageNav->limit;

    // get the data to display
    $database->setQuery($qrysql);
    $rows = $database->loadObjectList();
    if ($database->getErrorNum()) {
        $emErrorHandler->addError('<p><strong>Error (message.admin.php->showMessages() line ' . __LINE__ . '):</strong>'._EMKTG_MESSAGE_ERROR.'</p>');
        return false;
    }

    // preparse the rows found
    if (!empty($rows)) {
        $k = 0;
        for ($i=0; $i < count($rows); $i++) {
            $row = &$rows[$i];
            $row->id = $row->messageid;
            $row->checked_out=0;
            $row->k = $k;

            $row->link = 'index2.php?option='.$option.'&task=checkmessage&hidemainmenu=0&cid[]='.$row->id;

            $row->checkedDisplay = mosCommonHTML::CheckedOutProcessing($row, $i);

            if ($row->send_date!=0) {
                $row->send_dateDisplay = mosFormatDate($row->send_date, _EMKTG_MESSAGE_SENDDATE_FORMAT);
            } else {
                $row->send_dateDisplay = '';
            }

            $k = 1 - $k;
        } // for
    }

    // load the language elements
    $lang = sm2emailmarketingLanguage();

    // call the object to display the list
    $tmpl = sm2emailmarketingPatTemplate('message.tpl', $option);

    $tmpl->addVar('header', 'FORM_NAME', _EMKTG_MESSAGE_FORM);
    $tmpl->addVar('manager', 'SEARCH', $search);
    $tmpl->addVar('manager', 'NUM_ROWS', count($rows));
    $tmpl->addObject('manager', $lang, 'LANG');
    $tmpl->addObject('rows', $rows, 'ROW_');
    $tmpl->addVar('manager', 'LISTFOOTER', $pageNav->getListFooter());

    $tmpl->displayParsedTemplate('manager');

} // showMessages()


function checkMessage($id, $option, $task) {
    global $database;

    // create an instance of the table class
    $row = new sm2emailmarketingMessage();

    // Load the row from the db table
    $row->load($id);

    if ($row->send_date == $database->getNullDate()) {
        // not sent yet so send to editMessage
        mosRedirect('index2.php?option='.$option.'&task=editmessage&hidemainmenu=0&cid[]='.$id);
    } else {
        // load the language elements
        $lang = sm2emailmarketingLanguage();

        // call the object to display the list
        $tmpl = sm2emailmarketingPatTemplate('message.tpl', $option);

        $tmpl->addVar('header', 'FORM_NAME', _E_EDIT.' '._EMKTG_MESSAGE);
        $tmpl->addObject('check', $lang, 'LANG');
        $tmpl->addObject('check', $row, 'ROW_');

        $tmpl->displayParsedTemplate('check');
    }
} // checkMessage()

function editMessage($id, $option, $task) {
    global $database, $sm2emailmarketingConfig, $mosConfig_absolute_path, $mosConfig_fromname, $mosConfig_mailfrom;

    // create an instance of the table class
    $row = new sm2emailmarketingMessage();

    // Load the row from the db table
    $row->load($id);

    // load the plugins and setup so they can be processed easily
    // get the data from the plugins//where enabled=1
    $database->setQuery('SELECT * FROM #__emktg_plugin ORDER BY ordering');
    $plugins = $database->loadObjectList();
    $contentPluginIndex = null;
    foreach (array_keys($plugins) as $index) {
        if (!file_exists($mosConfig_absolute_path.'/administrator/components/'.$option.'/plugins/'.$plugins[$index]->filename)) {
            unset($plugins[$index]);
            continue;
        }
        if ($plugins[$index]->filename == 'content.plugin.php') {
            $contentPluginIndex = $index;
        }
        $plugins[$index]->_plugin = require_once($mosConfig_absolute_path.'/administrator/components/'.$option.'/plugins/'.$plugins[$index]->filename);
    } // foreach()


    // initialise lists
    $lists = new stdClass();

    // get the list of templates
    $templates = array();
    $templates[] = mosHTML::makeOption('0', _EMKTG_TEMPLATE_DESC);
    $database->setQuery('SELECT templateid AS value, template_name AS text FROM #__emktg_template WHERE published=1 ORDER BY template_name');
    $templates = array_merge($templates, $database->loadObjectList());

    $lists->templateid = mosHTML::selectList($templates, 'templateid', 'tabindex="5" class="inputbox" size="1"', 'value', 'text', $row->templateid);
    unset($templates);

    // get the image list
    $path = $sm2emailmarketingConfig->get('attachment_path', $mosConfig_absolute_path.'/images/stories/');
    $lists->attachments = getAttachments('attachments[]', convertArrayToOptions(explode(',', $row->attachments)), '', $path);

    // process the plugins to display the extras
    $messageExtras = '';
    $pluginExtraValidation = '';
    foreach ($plugins as $plugin) {
        //if (!isset($plugin->_plugin)) continue;
        if (!is_object($plugin->_plugin)) continue;
        if (method_exists($plugin->_plugin, 'displayMessageExtras')) {
            $messageExtras .= $plugin->_plugin->displayMessageExtras($row, $plugin, $option);
        }
        $pluginExtraValidation .= $plugin->_plugin->_plugin_extra_validation;
    }

    // get the list of content items available
    $query = 'SELECT A.id AS value, CONCAT(B.title, '
        .$database->Quote('/').', C.title, '
        .$database->Quote('/').', A.title) AS text, A.created'
        .' FROM #__content AS A, #__sections AS B, #__categories AS C'
        .' WHERE A.sectionid = B.id AND A.catid = C.id'
        .' AND A.state=1'
        .' ORDER BY B.title, C.title, A.title';
    $database->setQuery($query);
    $contentitems = $database->loadObjectList();

    // determine if we need to display the content date
    $showContentDate = false;
    if ($contentPluginIndex!==null) {
        if (array_key_exists($contentPluginIndex, $plugins)) {
            $contentPlugin = $plugins[$contentPluginIndex];
            $pluginFile = $mosConfig_absolute_path.'/administrator/components/'.$option.'/plugins/'.str_replace('.php', '.xml', $contentPlugin->filename);
            if (file_exists($pluginFile)) {
                $params =& new sm2emailmarketingParams($contentPlugin->params, $pluginFile, 'sm2emailmarketing_plugin');
            }

            if ($params->get('available_date', 0)) {
                $showContentDate = true;
                foreach ($contentitems as $index=>$value) {
                    $contentitems[$index]->text .= ' ['
                        .mosFormatDate($value->created,
                            _EMKTG_MESSAGE_SENDDATE_FORMAT)
                        .']';
                }
            }
        }
    }


    $size = min(count($contentitems), 10);
    $lists->availcontent = mosHTML::selectList($contentitems, 'availContent', 'tabindex="9" id="availContent" class="inputbox" style="width:100%;" size="'.$size.'" multiple', 'value', 'text', null);
    unset($contentitems);

    // build the list of selected items
    if (empty($row->content_items)) {
        $foundItems = array();
    } else {
        $foundItems = explode(',',$row->content_items);
    }
    // use the same query but get the data differently
    $contentMatrix = $database->loadObjectList('value');

    $contentitems = array();
    foreach ($foundItems as $contentid) {
        if ($showContentDate) {
            $contentMatrix[$contentid]->text .= ' ['
                .mosFormatDate($contentMatrix[$contentid]->created,
                    _EMKTG_MESSAGE_SENDDATE_FORMAT)
                .']';
        }
        // add the found items to the content items array in order
        $contentitems[] = $contentMatrix[$contentid];
    }
    $lists->selectedcontent = mosHTML::selectList($contentitems, 'selectedContent', 'tabindex="10" id="selectedContent" class="inputbox" style="width:100%;" size="'.$size.'" multiple', 'value', 'text', null);
    unset($contentitems);

    if (!empty($row->intro_only)) {
        $row->intro_only = ' checked';
    } else {
        $row->intro_only = '';
    }

    ob_start();
    editorArea( 'editor1',  $row->message_html , 'message_html', 500, 200, '70', '20' );
    $row->message_htmlDisplay = ob_get_contents();
    ob_end_clean();

    ob_start();
    getEditorContents('editor1', 'message_html');
    $row->message_htmlValidation = ob_get_contents();
    ob_end_clean();

    if (empty($row->fromname)) {
        $row->fromname = $sm2emailmarketingConfig->get('fromname', $mosConfig_fromname);
    }

    if (empty($row->fromemail)) {
        $row->fromemail = $sm2emailmarketingConfig->get('fromemail', $mosConfig_mailfrom);
    }

    if (empty($row->bounceemail)) {
        $row->bounceemail = $sm2emailmarketingConfig->get('bounceemail', $mosConfig_mailfrom);
    }

    if ($row->send_date==0) {
        $row->send_date = '';
    }

    // get the filters from the plugins
    $pluginData = '';
    $pluginValidation = '';
    foreach ($plugins as $plugin) {
        //if (!isset($plugin->_plugin)) continue;
        if (!is_object($plugin->_plugin)) continue;
        if (!$plugin->enabled) continue;
        if (method_exists($plugin->_plugin, 'displayMessageFilters')) {
            $pluginData .= $plugin->_plugin->displayMessageFilters($id, $plugin->pluginid, $option);
        }
        $pluginValidation .= $plugin->_plugin->_plugin_validation;
    } // foreach()


    switch ($task) {
        case 'newmessage':
            $title= _E_ADD;
            break;
        case 'copymessage':
            $title = _EMKTG_COPY;
            $row->messageid = 0;
            $row->subject = sm2emailmarketingProcessCopyTitle($row->subject);
            $row->send_date = '';
            break;
        case 'editmessage':
        default:
            $title = _E_EDIT;
            break;

    }

    // load the language elements
    $lang = sm2emailmarketingLanguage();

    // display the form
    mosMakeHtmlSafe($row, ENT_QUOTES, array('message_htmlDisplay', 'message_htmlValidation'));
    mosCommonHTML::loadOverlib();

    $tmpl = sm2emailmarketingPatTemplate('message.tpl', $option);

    $tmpl->addVar('header', 'FORM_NAME', $title.' '._EMKTG_MESSAGE);
    $tmpl->addVar('edit', 'messageextras', $messageExtras);
    $tmpl->addVar('edit', 'PLUGINDATA', $pluginData);
    $tmpl->addVar('edit', 'PLUGINVALIDATION', $pluginValidation);
    $tmpl->addVar('edit', 'PLUGINEXTRAVALIDATION', $pluginExtraValidation);

    $tmpl->addObject('edit', $lang, 'LANG');
    $tmpl->addObject('edit', $row, 'ROW_');
    $tmpl->addObject('edit', $lists, 'LIST_');

    $tmpl->displayParsedTemplate('edit');

} // editMessage()

function saveMessage($option, $task) {
    global $database, $mosConfig_absolute_path, $emErrorHandler;

    // create an instance of the table class
    $row = new sm2emailmarketingMessage();

    // attempt to bind the class to the data posted
    if (defined('_JLEGACY')) {
        $post = JRequest::get('post', JREQUEST_ALLOWRAW);
    } else {
        $post = $_POST;
    }

    if (!$row->bind($post)) {
        $emErrorHandler->addError($row->getError(), true);
    }

    // check the record
    if (!$row->check()) {
        $emErrorHandler->addError($row->getError(), true);
    }

    // fix the two arrays
    if (!empty($row->attachments)) {
    	$row->attachments = implode(',', $row->attachments);
    }

    // attempt to insert/update the record
    if (!$row->store(true)) {
        $emErrorHandler->addError($row->getError(), true);
    }

    // store the data from the plugins //where enabled=1
    $database->setQuery('SELECT * FROM #__emktg_plugin ORDER BY ordering');
    $data = $database->loadObjectList();
    $pluginData = '';
    foreach ($data as $rec) {
        if (!file_exists($mosConfig_absolute_path.'/administrator/components/'.$option.'/plugins/'.$rec->filename)) continue;
        $obj = require_once($mosConfig_absolute_path.'/administrator/components/'.$option.'/plugins/'.$rec->filename);
        if (method_exists($obj, 'processMessageEdit')) {
            $pluginData .= $obj->processMessageEdit($row->messageid, $rec->pluginid, $option);
        }
    } // foreach()

    switch ($task) {
    	case 'sendmessage':
    		sendMessage($row->messageid, $option, $task);
    		break;
        case 'applymessage':
            mosRedirect('index2.php?option='.$option.'&task=editmessage&hidemainmenu=0&cid[]='.$row->messageid, _EMKTG_MESSAGE_SAVED);
            break;
        case 'savepreviewmessage':
        	previewMessage($row->messageid, $option, $task);
        	break;
        case 'savemessage':
        default:
            mosRedirect('index2.php?option='.$option.'&task=showmessages', _EMKTG_MESSAGE_SAVED);
            break;
    }

} // saveMessage()


function deleteMessage($ids, $option) {
    global $emErrorHandler;

    // validate that ids are ok
    if (!is_array($ids) or count($ids) < 1) {
        $emErrorHandler->addError('(message.admin.php->deleteMessage() line ' . __LINE__ . '):Nothing selected to process', true);
    }

    // load the table object and perform a delete
    $row = new sm2emailmarketingMessage();

    foreach ($ids as $id) {
        if (!$row->deleteRelated($id)) {
            $emErrorHandler->addError($row->getError(), true);
        }
        if (!$row->delete($id)) {
            $emErrorHandler->addError($row->getError(), true);
        }
    }

    mosRedirect('index2.php?option='.$option.'&task=showmessages', _EMKTG_MESSAGE_DELETED);

} // deleteMessage()


function sendMessage($messageid, $option) {
    global $database, $mosConfig_absolute_path, $emErrorHandler;

    // create an instance of the table class
    $row = new sm2emailmarketingMessage();

    // attempt to bind the class to the data posted
    if (!$row->load($messageid)) {
        $emErrorHandler->addError($row->getError(), true);
    }

    $row->sendMessage($option);

    $row->send_date = date('Y-m-d H:i:s');
    $row->store();

    // insert a record into the overall stats
    $database->setQuery('INSERT INTO #__emktg_stats_overall (messageid, html_sent, text_sent, html_read)'
        .' VALUES ('.(int)$row->messageid.', 0, 0, 0)');
    $database->query();

    // send to the queue processing
    mosRedirect('index2.php?option='.$option.'&task=processqueue&hidemainmenu=0&cid[]='.$row->messageid);

} // sendMessage()


function getAttachments($name, $active, $javascript=NULL, $directory=NULL) {
    global $mosConfig_absolute_path;

    if (!$javascript) {
        $javascript = 'onchange="javascript:if (document.forms[0].image.options[selectedIndex].value!=\'\') {document.imagelib.src=\'../images/stories/\' + document.forms[0].image.options[selectedIndex].value} else {document.imagelib.src=\'../images/blank.png\'}"';
    }
    if ( !$directory ) {
        $directory = $mosConfig_absolute_path.'/images/stories/';
    }

    $fileList = array();
    $files = mosReadDirectory($directory);
    foreach ($files as $file) {
        if (is_dir($mosConfig_absolute_path.$directory.'/'.$file)) continue;
        $fileList[] = mosHTML::makeOption($file);
    }
    $size = min(count($fileList), 10);
    $files = mosHTML::selectList($fileList, $name, 'class="inputbox" size="'.$size.'" tabindex="8" multiple '/*.$javascript*/, 'value', 'text', $active);

    return $files;
} // getAttachments()


function convertArrayToOptions($array) {
    $return = array();

    if (empty($array)) return $return;
    if (!is_array($array)) return $return;

    foreach ($array as $index=>$item) {
        $return[$index] = mosHTML::makeOption($item);
    }
    return $return;
} // convertArrayToOptions()


function previewMessage($id, $option) {
    global $database, $sm2emailmarketingConfig, $mainframe;
    global $mosConfig_absolute_path, $mosConfig_live_site, $mosConfig_fromname, $mosConfig_mailfrom, $mosConfig_list_limit;

    sm2emSEF();

    // get some data from the request information
    $action = mosGetParam($_GET, 'action', 0);
    $limitstart = (int)mosGetParam($_GET, 'limitstart', 0 );

    // create an instance of the table class
    $row = new sm2emailmarketingMessage();

    // Load the row from the db table
    $row->load($id);

	//build message
    $row->replaceTags(true);

    if (empty($row->fromname)) {
        $row->fromname = $sm2emailmarketingConfig->get('fromname', $mosConfig_fromname);
    }
    if (empty($row->fromemail)) {
        $row->fromemail = $sm2emailmarketingConfig->get('fromemail', $mosConfig_mailfrom);
    }
    if (empty($row->bounceemail)) {
        $row->bounceemail = $sm2emailmarketingConfig->get('bounceemail', $mosConfig_mailfrom);
    }
    if ($row->send_date==0) {
        $row->send_date = '';
    }

    // get the plugins to process
    $database->setQuery('SELECT * FROM #__emktg_plugin WHERE enabled=1 ORDER BY ordering');
    $plugins = $database->loadObjectList();

    // process the plugins
    foreach ($plugins as $plugin) {
        // load the plugin
        if (!file_exists($mosConfig_absolute_path.'/administrator/components/'.$option.'/plugins/'.$plugin->filename)) continue;
        require_once($mosConfig_absolute_path.'/administrator/components/'.$option.'/plugins/'.$plugin->filename);
        $class = $plugin->classname;
        $obj = new $class();
        // process the send of the plugin
        if (method_exists($obj, 'processMessage')) {
            $obj->processMessage($id, $plugin->pluginid, $option, $row, 0);
        }
    } // foreach()

    // load the language elements
    $lang = sm2emailmarketingLanguage();

    if (!is_array($row->attachments)) $row->attachments = explode(',', $row->attachments);
	$attachments = implode('<br />', $row->attachments);
    unset($row->attachments);
	$row->message_htmlDisplay = $row->message_html.'<br style="clear: both;" /><br /><hr /><strong>Attachments:</strong><br />'.$attachments;
	$row->message_textDisplay = nl2br($row->message_text.'<hr /><strong>Attachments:</strong><br />'.$attachments);

    sm2emailmarketing_addAbsolutePath($row->message_htmlDisplay);

    if ($action!=1) {
        // display outside of a iframe
        $title = _EMKTG_MESSAGE_PREVIEW;
        mosMakeHtmlSafe($row, ENT_QUOTES, array('message_htmlDisplay','message_textDisplay'));

        $tmpl = sm2emailmarketingPatTemplate('message.tpl', $option);

        $tmpl->addVar('header', 'FORM_NAME', $title);
        $tmpl->addObject('preview', $sm2emailmarketingConfig->toObject(), 'CONFIG_');
        $tmpl->addObject('preview', $lang, 'LANG');
        $tmpl->addObject('preview', $row, 'ROW_');
        $tmpl->addVar('preview', 'action', $action);
    }

    switch ($action) {
    	case 1:
    		echo $row->message_htmlDisplay;
    		break;
		case 2:
            $tmpl->addVar('preview', 'tab2active', ' activeTab');
            $tmpl->addVar('preview', 'TEXT_MESSAGE', $row->message_textDisplay);
			break;
		case 3:
            $tmpl->addVar('preview', 'tab3active', ' activeTab');
//            if (empty($limitstart)) {
                // precess the send message
                $row->sendMessage($option, true);
//            }

            $sessionSuffix = determineSessionTableSuffix();

            // get the data in the queue
            $database->setQuery('SELECT COUNT(*)'
                .' FROM #__emktg_stats_message'.$sessionSuffix
                .' WHERE `html`=1');
            $totalHTML = $database->loadResult();

            $database->setQuery('SELECT COUNT(*)'
                .' FROM #__emktg_stats_message'.$sessionSuffix
                .' WHERE `html`=0');
            $totalText = $database->loadResult();

            $total = max($totalHTML, $totalText);
            $totalAll = $totalHTML + $totalText;

            // handle the page navigation
            if (defined('_JLEGACY')) {
                $tempClientId = $mainframe->_clientId;
                $mainframe->_clientId = 0;
            }
            include_once($mosConfig_absolute_path.'/administrator/includes/pageNavigation.php');
            $pageNav = new mosPageNav($total, $limitstart, _EMKTG_MESSAGE_PREVIEW_PAGE);
            $link = 'administrator/index2.php?option='.$option
                .'&task=previewmessage&action=3&cid[]='.(int)$id;

            if (defined('_JLEGACY')) {
                $app    = &JFactory::getApplication();
                $router = &$app->getRouter();
                $router->setVars(
                    array(
                        'option'=>$option,
                        'task'=>'previewmessage',
                        'action'=>3,
                        'cid[]'=>(int)$id
                    )
                );
            }

            $tmpl->addVar('preview', 'PREVIEW_TOTAL', $total);
            $tmpl->addVar('preview', 'TOTAL_HTML', $totalHTML);
            $tmpl->addVar('preview', 'TOTAL_TEXT', $totalText);
            $tmpl->addVar('preview', 'TOTAL', $totalAll);
            $tmpl->addVar('preview', 'PAGE_LINK', $pageNav->writePagesLinks($link));

            if (defined('_JLEGACY')) {
                $mainframe->_clientId = $tempClientId;
            }

            $database->setQuery('SELECT id, name, email, `html`'
                .' FROM #__emktg_stats_message'.$sessionSuffix
                .' WHERE `html`=1'
                .' ORDER BY name, email'
                .' LIMIT '.(int)$pageNav->limitstart.', '.(int)$pageNav->limit);
            $html = $database->loadObjectList();
            if (is_array($html)) {
                foreach ($html as $rec) {
                    $tmpl->addObject('html', $rec, 'HTML_');
                    $tmpl->parseTemplate('html', 'a');
                }
            }

            $database->setQuery('SELECT id, name, email, `html`'
                .' FROM #__emktg_stats_message'.$sessionSuffix
                .' WHERE `html`=0'
                .' ORDER BY name, email'
                .' LIMIT '.(int)$pageNav->limitstart.', '.(int)$pageNav->limit);
            $text = $database->loadObjectList();
            if (is_array($text)) {
                foreach ($text as $rec) {
                    $tmpl->addObject('text', $rec, 'TEXT_');
                    $tmpl->parseTemplate('text', 'a');
                }
            }

		    break;
        default:
            $tmpl->addVar('preview', 'tab0active', ' activeTab');
            break;
    }

    if ($action!=1)
        $tmpl->displayParsedTemplate('preview');

} // previewMessage()


function sendtestMessage($id, $option) {
	global $mosConfig_absolute_path, $sm2emailmarketingConfig, $mosConfig_fromname, $mosConfig_mailfrom, $_VERSION;
    global $database;

    sm2emSEF();

    $msg = '';
	$name = mosGetParam($_POST, 'testname', '');
	$email = mosGetParam($_POST, 'testemail', '');
	$use_html = mosGetParam($_POST, 'testhtml', 0);
	$emailaddresses = array();
	$path = $sm2emailmarketingConfig->get('attachment_path', $mosConfig_absolute_path.'/images/stories/');

    // determine what to set the memory limit to - if at all
    $currentMemoryLimit = (int)ini_get('memory_limit');
    $memoryLimit = (int)$sm2emailmarketingConfig->get('process_memory', 0);
    if ($memoryLimit) {
        if ($memoryLimit < $currentMemoryLimit) $memoryLimit += $currentMemoryLimit;
        ini_set('memory_limit', $memoryLimit.'M');
    }

    //build base message (without personalisation such as name etc)
    // create an instance of the table class
    $row = new sm2emailmarketingMessage();

    // Load the row from the db table
    $row->load($id);

    // validate from information
    if (empty($row->fromname)) {
        $row->fromname = $sm2emailmarketingConfig->get('fromname', '');
    }
    if (empty($row->fromemail)) {
        $row->fromemail = $sm2emailmarketingConfig->get('fromemail', '');
    }
    if (empty($row->bounceemail)) {
        $row->bounceemail = $sm2emailmarketingConfig->get('bounceemail', '');
    }

    //use Joomla! class to send mail
    $mail = mosCreateMail($row->fromemail, $row->fromname, $row->subject, '');
    // identify that we are sending and what the messageid is
    $mail->addCustomHeader('X-Mailer: SM2 Email Marketing');
    $mail->addCustomHeader('X-SM2MessageID: '.$row->messageid);
    // stop auto responders apparently
    $mail->addCustomHeader('Precedence: bulk');

    if (!empty($row->bounceemail)) {
        $mail->Sender = $row->bounceemail;
    }

	//error checking
	if (!$name) {
		$name = $sm2emailmarketingConfig->get('fromname', $mosConfig_fromname);
	}
	if (!$email) {
		$email = $sm2emailmarketingConfig->get('fromemail', $mosConfig_mailfrom);
		$emailaddresses = explode(',', $email);
	} else {
		$emailaddresses = explode(',', $email);
	}

    if (count($emailaddresses) > 10) {
        $emailaddresses = array_slice($emailaddresses, 0, 10);
    }

    // if demo and admin only allow sending 1 message at a time
    if (empty($_VERSION->SITE)) {
        $emailaddresses = array($emailaddresses[0]);

        // check to see how long since last message sent
        $lastSend = mosGetParam($_COOKIE, 'sm2em_restricted_send', 0);
        if ($lastSend > (time() - _EMKTG_LICENCE_DEMO_RESTRICT_SEC)) {
            mosRedirect('index2.php?option='.$option.'&amp;task=previewmessage&amp;cid[]='.$id, _EMKTG_LICENCE_DEMO_RESTRICT_SEC_WARNING);
            return;
        }
        setcookie('sm2em_restricted_send', time());
    }


	//build message
	$row->replaceTags();

    // get the plugins to process
    $database->setQuery('SELECT * FROM #__emktg_plugin WHERE enabled=1 ORDER BY ordering');
    $plugins = $database->loadObjectList();

    // process the plugins
    foreach ($plugins as $plugin) {
        // load the plugin
        if (!file_exists($mosConfig_absolute_path.'/administrator/components/'.$option.'/plugins/'.$plugin->filename)) continue;
        require_once($mosConfig_absolute_path.'/administrator/components/'.$option.'/plugins/'.$plugin->filename);
        $class = $plugin->classname;
        $obj = new $class();
        // process the send of the plugin
        if (method_exists($obj, 'processMessage')) {
            $obj->processMessage($id, $plugin->pluginid, $option, $row, 0);
        }
    } // foreach()

    //end build base message

	//send test message using the global configuration to determine delivery method
	foreach($emailaddresses as $email) {
        $thisrow = $row;
		//personalise the message
		$thisrow->replaceUserInfo($name,$email); //message is all set for sending
		sm2emailmarketing_addAbsolutePath($thisrow->message_html);

        $mail->addCustomHeader('X-SM2Recipient: '.$email);
		$mail->AddAddress($email, $name);
		$mail->Subject = $thisrow->subject;

		//get the correct format of message
		if ($use_html) { //html message to be sent
			$mail->IsHTML(true);
			$mail->Body = $thisrow->message_html;
            $mail->AltBody = $thisrow->message_text;
		} else { //plain text to be sent
			$mail->IsHTML(false);
			$mail->Body = $thisrow->message_text;
		}

        $wordWrap = $sm2emailmarketingConfig->get('word_wrap', 0);
        if ($wordWrap) {
            // word wrap both bodies
            $mail->Body = $mail->WrapText($mail->Body, $wordWrap);
            if (!empty($mail->AltBody))
                 $mail->AltBody = $mail->WrapText($mail->AltBody, $wordWrap);
        }

		foreach ($thisrow->attachments as $attachment) {
            $mail->AddAttachment($attachment, basename($attachment));
        }
        foreach ($thisrow->embeddedImages as $embed) {
            $mail->AddEmbeddedImage($embed->image, $embed->cid, $embed->name, $embed->encoding, $embed->type);
        }

		//send message
        $sendMailOk = $mail->Send();

        if (!$sendMailOk) {
            mosRedirect('index2.php?option='.$option.'&task=editmessage&hidemainmenu=0&cid[]='.$id, _EMKTG_MESSAGE_TESTERROR.'::'.$mail->ErrorInfo);
            exit;
        } else {
            $msg = _EMKTG_MESSAGE_TESTSENT;
        }

		$mail->ClearAllRecipients();
        // clear the attachments so that they are not added again
        $mail->ClearAttachments();
	}

	//redirect back to the editmessage area of the message
	mosRedirect('index2.php?option='.$option.'&task=editmessage&hidemainmenu=0&cid[]='.$id, $msg);
}


function showMessageArchive($id, $option) {
    global $database, $sm2emailmarketingConfig;

    $action = mosGetParam($_GET, 'action', 0);

    $database->setQuery('SELECT * FROM #__emktg_stats_message WHERE id='.(int)$id);
    $rec = null;
    $database->loadObject($rec);

    // validate that this is an object correctly
    if (!is_object($rec)) {
        mosRedirect('index2.php?option='.$option.'&amp;task=showmessages&amp;hidemainmenu=0', _EMKTG_MESSAGE_ARCHIVE_NONEXISTENT);
        return;
    }

    $rec->send_dateDisplay = mosFormatDate($rec->send_date, _EMKTG_MESSAGE_SENDDATE_FORMAT);

    // create an instance of the table class
    $row = new sm2emailmarketingMessage();

    // Load the row from the db table
    $row->load($rec->messageid);

    //build message
    $row->replaceTags();
    $row->replaceUserInfo($rec->name, $rec->email);
    unset($row->cache, $row->embeddedImages);

    // load the language elements
    $lang = sm2emailmarketingLanguage();

    if (!is_array($row->attachments)) $row->attachments = explode(',', $row->attachments);
    $attachments = implode('<br />', $row->attachments);
    unset($row->attachments);
    $row->message_htmlDisplay = $row->message_html.'<br style="clear: both;" /><br /><hr /><strong>Attachments:</strong><br />'.$attachments;
    $row->message_textDisplay = nl2br($row->message_text.'<hr /><strong>Attachments:</strong><br />'.$attachments);

    sm2emailmarketing_addAbsolutePath($row->message_htmlDisplay);

    switch ($action) {
        case 1:
            if ($rec->html) {
                echo $row->message_htmlDisplay;
            } else {
                echo $row->message_textDisplay;
            }

            break;
        default:

            // display outside of a iframe
            $title = _EMKTG_ARCHIVE_FORM;
            //HTML_sm2emailmarketing_Messages::previewMessage($row, $option, $title, $lang);
            mosMakeHtmlSafe($row, ENT_QUOTES, array('message_htmlDisplay','message_textDisplay'));

            $tmpl = sm2emailmarketingPatTemplate('message.tpl', $option);

            $tmpl->addVar('header', 'FORM_NAME', $title);
            $tmpl->addObject('archive', $lang, 'LANG');
            $tmpl->addObject('archive', $rec, 'REC_');


            $tmpl->addObject('archive', $row, 'ROW_');

            $tmpl->displayParsedTemplate('archive');
            break;
    }

} // showMessageArchive()
