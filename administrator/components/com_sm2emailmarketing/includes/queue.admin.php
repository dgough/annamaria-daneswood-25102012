<?php
// $Id: queue.admin.php,v 1.31 2008/03/31 01:59:10 sm2tony Exp $
/**
 * sm2emailmarketing - Email Marketing Newsletter
 *
 * Joomla! Newsletter Component that supports multiple list mailing and message queueing
 *
 * @package Joomla!
 * @subpackage com_sm2emailmarketing
 * @copyright Copyright (C) 2006 SM2 / All Rights Reserved
 * @license commercial
 * @author SM2 <info@sm2joomla.com>
 */

/**
 * Makes sure this is included by a parent file
 */
defined('_VALID_MOS') or die('Direct Access to this location is not allowed.');

function showQueue($option) {
    global $database, $mainframe, $mosConfig_list_limit, $mosConfig_absolute_path, $emErrorHandler;

    // get some data from the request information
    $limit = $mainframe->getUserStateFromRequest( 'viewqueuelimit', 'limit', $mosConfig_list_limit );
    $limitstart = $mainframe->getUserStateFromRequest( 'view'.$option.'queuestart', 'limitstart', 0 );

    // Get the total number of records
    $qrysql  = 'SELECT COUNT(DISTINCT messageid) FROM #__emktg_stats_message WHERE send_date IS NULL';

    $database->setQuery($qrysql);
    $total = $database->loadResult();

    // handle the page navigation
    include_once($mosConfig_absolute_path.'/administrator/includes/pageNavigation.php');
    $pageNav = new mosPageNav($total, $limitstart, $limit);

    // build the query to get the data to display
    /*
    $qrysql  = 'select m.messageid, m.subject, m.send_date, t.template_name, count(*) as numleft '
        .' from #__emktg_message m '
        .' inner join #__emktg_stats_message s on (m.messageid=s.messageid) '
        .' inner join #__emktg_template t on (m.templateid=t.templateid) '
        .' where s.send_date is null '
        .' group by m.messageid, m.subject, m.send_date, t.template_name '
        .' order by m.send_date desc ';
    */
    $qrysql  = 'SELECT m.messageid, m.subject, m.send_date, COUNT(*) AS numleft'
        .' FROM #__emktg_message m '
        .' INNER JOIN #__emktg_stats_message s ON (m.messageid=s.messageid)'
        .' INNER JOIN #__emktg_template t ON (m.templateid=t.templateid)'
        .' WHERE s.send_date IS NULL'
        .' GROUP BY m.messageid, m.subject, m.send_date'
        .' ORDER BY m.send_date desc'
        .' LIMIT '.(int)$pageNav->limitstart.', '.(int)$pageNav->limit;

    // get the data to display
    $database->setQuery($qrysql);
    $rows = $database->loadObjectList();
    if ($database->getErrorNum()) {
        $emErrorHandler->addError('(queue.admin.php->showQueue() line ' . __LINE__ . '): '._EMKTG_QUEUE_ERROR);
        return false;
    }

    // preparse the rows found
    if (!empty($rows)) {
        $k = 0;
        for ($i=0; $i < count($rows); $i++) {
            $row = &$rows[$i];
            $row->id = $row->messageid;
            $row->checked_out=0;
            $row->k = $k;

            $row->link = 'index2.php?option='.$option.'&task=processqueue&hidemainmenu=0&cid[]='.(int)$row->id;

            $row->checkedDisplay = mosCommonHTML::CheckedOutProcessing($row, $i);
        } // for
    }

    // load the language elements
    $lang = sm2emailmarketingLanguage();

    // call the object to display the list
    $tmpl = sm2emailmarketingPatTemplate('queue.tpl', $option);

    $tmpl->addVar('header', 'FORM_NAME', _EMKTG_QUEUE_FORM);
    $tmpl->addVar('manager', 'NUM_ROWS', count($rows));
    $tmpl->addObject('manager', $lang, 'LANG');
    $tmpl->addObject('rows', $rows, 'ROW_');
    $tmpl->addVar('manager', 'LISTFOOTER', $pageNav->getListFooter());

    $tmpl->displayParsedTemplate('manager');

} // showQueue()


function processQueue($id, $option) {
    global $database, $sm2emailmarketingConfig, $mosConfig_absolute_path, $mosConfig_live_site, $_VERSION;

    // handle SEF
    sm2emSEF();

    // force this response to be html in case there is a freaky server config
    header('Content-type: text/html');

    $path = $sm2emailmarketingConfig->get('attachment_path', $mosConfig_absolute_path.'/images/stories/');

    // determine what to set the memory limit to - if at all
    $currentMemoryLimit = (int)ini_get('memory_limit');
    $memoryLimit = (int)$sm2emailmarketingConfig->get('process_memory', 0);
    if ($memoryLimit) {
        if ($memoryLimit < $currentMemoryLimit) $memoryLimit += $currentMemoryLimit;
        ini_set('memory_limit', $memoryLimit.'M');
    }

    // determine the email limit if there is one
    $email_limit = (int)$sm2emailmarketingConfig->get('email_limit', 0);

    // get the start time
    $starttime = mosGetParam($_REQUEST, 'starttime', null);
    $starting = false;
    if ($starttime === null) {
        $starting = true;
        $starttime = time();
    }

    // check if the time is passed
    $multiplier = 60; // script time in minutes
    if ((time() - $starttime) > ($sm2emailmarketingConfig->get('script_timeout', 60) * $multiplier)) {
        // running to long
        $msg = str_replace('[MIN]', $sm2emailmarketingConfig->get('script_timeout', 60), _EMKTG_QUEUE_TIMEOUT_EXCEEDED);
        mosRedirect('index2.php?option='.$option.'&task=showqueue', $msg);
    }

    // create an instance of the table class
    $row = new sm2emailmarketingMessage();

    // Load the row from the db table
    $row->load($id);

    // prepare the template data
    $template = new sm2emailmarketingTemplate();
    $template->load($row->templateid);

    // get the list of emails to send out in this batch
    $database->setQuery('SELECT *'
        .' FROM #__emktg_stats_message'
        .' WHERE (messageid='.(int)$id.')'
        .' AND (send_date IS NULL)'
        .' LIMIT 0, '.(int)$sm2emailmarketingConfig->get('emails_between_pauses', 50));
    $emails = $database->loadObjectList();

    if (count($emails)==0) {
        // finished processing
        mosRedirect('index2.php?option='.$option.'&task=showstatistics', _EMKTG_QUEUE_FINISHED);
        return;
    }

    // get some statistic information
    $stats = new stdClass();
    $database->setQuery('SELECT COUNT(*) FROM #__emktg_stats_message WHERE (messageid='.(int)$id.')');
    $stats->queueSize = $database->loadResult();

    $database->setQuery('SELECT COUNT(*) FROM #__emktg_stats_message WHERE (messageid='.(int)$id.') AND (send_date IS NULL)');
    $stats->unprocessed = $database->loadResult();

    $stats->processed = $stats->queueSize - $stats->unprocessed;
    $counter = $stats->processed;

    if (!$starting && $email_limit > 0 && ($counter % $email_limit)==0) {
        // reached email limit so stop
        mosRedirect('index2.php?option='.$option.'&task=showqueue', _EMKTG_EMAIL_LIMIT_REACHED);
        return;
    }

    // propare the explanation text and redirection code
    $row->pauseMsg = str_replace('[PAUSE]', $sm2emailmarketingConfig->get('pause_time', 10), _EMKTG_QUEUE_PAUSE_MSG)."\n";
    $row->contMsg = str_replace('[A]', '<a href="javascript:timerSubmit();">', str_replace('[/A]', '</a>', _EMKTG_QUEUE_CONT_MSG))."\n";
    $row->redirectScript = '<script language="javascript" type="text/javascript">timer=setTimeout(\'timerSubmit()\','.($sm2emailmarketingConfig->get('pause_time', 10) * 1000).');</script>'."\n";

    // load the language elements
    $lang = sm2emailmarketingLanguage();

    // display the start of the processing
    $tmpl = sm2emailmarketingPatTemplate('queue.tpl', $option);

    $tmpl->addVar('header', 'FORM_NAME', _EMKTG_QUEUE_PROCESS.' '._EMKTG_QUEUE);
    $tmpl->addVar('edit', 'STARTTIME', $starttime);
    $tmpl->addObject('edit', $lang, 'LANG');
    $tmpl->addObject('edit', $row, 'MSG_');
    $tmpl->addObject('edit', $template, 'TPL_');
    $tmpl->addObject('edit', $stats, 'STATS_');
    $tmpl->displayParsedTemplate('edit');

    //build message
    require_once($mosConfig_absolute_path.'/administrator/components/'.$option.'/includes/message.admin.php');

    $row->replaceTags();
    // get the plugins to process
    $database->setQuery('SELECT * FROM #__emktg_plugin WHERE enabled=1 ORDER BY ordering');
    $plugins = $database->loadObjectList();

    // validate from information
    if (empty($row->fromname)) {
        $row->fromname = $sm2emailmarketingConfig->get('fromname', '');
    }
    if (empty($row->fromemail)) {
        $row->fromemail = $sm2emailmarketingConfig->get('fromemail', '');
    }
    if (empty($row->bounceemail)) {
        $row->bounceemail = $sm2emailmarketingConfig->get('bounceemail', '');
    }

    //use Joomla! class to send mail
	$mail = mosCreateMail($row->fromemail, $row->fromname, $row->subject, '');
    // identify that we are sending and what the messageid is
    $mail->addCustomHeader('X-Mailer: SM2 Email Marketing');
    $mail->addCustomHeader('X-SM2MessageID: '.(int)$row->messageid);
    // stop auto responders apparently
    $mail->addCustomHeader('Precedence: bulk');

    if (!empty($row->bounceemail)) {
        $mail->Sender = $row->bounceemail;
    }

    foreach ($emails as $rec) {
        $counter++;
    	$thisrow = $row;

        $thisrow->getMessageFromCache();

        $rec->email = html_entity_decode($rec->email, ENT_QUOTES);

        // check if this should be blacklisted
        if (sm2emCheckBlackList($rec->email)) {
            // blacklisted so process
            $database->setQuery('UPDATE #__emktg_stats_message SET status=3'
                .', send_date = NOW() WHERE id='.(int)$rec->id);
            $database->Query();

            // record in the overall stats that there has been an error
            $database->setQuery('UPDATE #__emktg_stats_overall SET `blacklists`=(`blacklists`+1) WHERE messageid='.(int)$rec->messageid);
            $database->Query();

            // determine if we need to unsubscribe
            if ($sm2emailmarketingConfig->get('blacklist_unsubscribe', 1)) {
                // look in the subscriber table
                $database->setQuery('UPDATE #__emktg_subscriber'
                    .' SET unsubscribe_date = NOW(), unsubscribe_reason = 3'
                    .' WHERE LOWER(email) = '.$database->Quote($rec->email));
                $database->Query();

                // see if there is a user that matches
                $database->setQuery('SELECT id FROM #__users'
                    .' WHERE LOWER(email) = '.$database->Quote($rec->email));
                $userid = $database->loadResult();

                if (!empty($userid)) {
                    $database->setQuery('UPDATE #__emktg_user'
                        .' SET unsubscribe_date = NOW(), unsubscribe_reason = 3'
                    .' WHERE id = '.(int)$userid);
                    $database->Query();
                }

                // send the admin email
                $rec->unsubscribe_reason=3;
                sm2emAdminUnsubscribeEmail($rec);
                unset($rec->unsubscribe_reason);
            }
            continue;
        }

        $mail->addCustomHeader('X-SM2Recipient: '.$rec->email);
        $mail->AddAddress($rec->email, $rec->name);

        $message_html = '';
        $message_text = '';

        list ($message_html, $message_text) =  sm2emProcessMessage($id, $thisrow, $rec, $plugins, $option);

        //send the message
        if($rec->html ==1) { //send html message
        	$mail->IsHTML(true);
			$mail->Body = $message_html;
            $mail->AltBody = $message_text;
        	echo _EMKTG_QUEUE_HTMLMSGSENT.'<br />';
        } else {
        	$mail->IsHTML(false);
			$mail->Body = $message_text;
        	echo _EMKTG_QUEUE_TEXTMSGSENT.'<br />';
        }

        $wordWrap = $sm2emailmarketingConfig->get('word_wrap', 0);
        if ($wordWrap) {
            // word wrap both bodies
            $mail->Body = $mail->WrapText($mail->Body, $wordWrap);
            if (!empty($mail->AltBody))
                 $mail->AltBody = $mail->WrapText($mail->AltBody, $wordWrap);
        }

        foreach ($thisrow->attachments as $attachment) {
            $mail->AddAttachment($attachment, basename($attachment));
        }
        foreach ($thisrow->embeddedImages as $embed) {
            $mail->AddEmbeddedImage($embed->image, $embed->cid, $embed->name, $embed->encoding, $embed->type);
        }

        //send message only if Joomla! 1 or 1.5
        if (!empty($_VERSION->SITE) || defined('_JLEGACY')) {
            $sendMailOk = $mail->Send();
        } else {
            $sendMailOk = true;
        }

        if (!$sendMailOk) {
            // send failed for some reason - most likely a bad email address

            // display the information that this could not be sent
            echo '<span style="color: red">'.$counter.'.&nbsp;'.$rec->name.' :: '.$rec->email
                .' - '._EMKTG_QUEUE_ERRORSENDINGMSG.'</span><br />';

            // update to indicate this message has passed through the queue
            // but could not be sent successfully
            $database->setQuery('UPDATE #__emktg_stats_message SET status=2'
                .', send_date = NOW() WHERE id='.(int)$rec->id);
            $database->Query();

            // record in the overall stats that there has been an error
            $database->setQuery('UPDATE #__emktg_stats_overall SET `errors`=(`errors`+1) WHERE messageid='.(int)$rec->messageid);
            $database->Query();
        } else {
            // send was successful

            // display the information about this message
            echo $counter.'.&nbsp;'.$rec->name.' :: '.$rec->email.'<br />';
            // update to indicate this message has been sent
            $database->setQuery('UPDATE #__emktg_stats_message SET send_date = NOW(), status=1 WHERE id='.(int)$rec->id);
            $database->Query();

            //update message stats (overall html, text, read)
            if ($rec->html) {
                $database->setQuery('UPDATE #__emktg_stats_overall SET html_sent=(html_sent+1) WHERE messageid='.(int)$rec->messageid);
                $database->Query();
            } else {
                $database->setQuery('UPDATE #__emktg_stats_overall SET text_sent=(text_sent+1) WHERE messageid='.(int)$rec->messageid);
                $database->Query();
            }
        }

        // clear the email recipients so that the next email is sent to new people only
        $mail->ClearAllRecipients();
        // clear the attachments so that they are not added again
        $mail->ClearAttachments();
        // try to remove the X-SM2Recipient header
        // this assumes that PHP Mailer does not add its own custom headers before/during the send
        array_pop($mail->CustomHeader);

        if (($email_limit > 0) && (($counter % $email_limit)==0)) {
            echo '<div class="message">'._EMKTG_EMAIL_LIMIT_REACHED.'</div>';
            break;
        }

    } // foreach emails

} // processQueue()

function deleteQueue($id, $option) {
	global $database;
	$database->setQuery('UPDATE #__emktg_stats_message SET send_date = '.$database->Quote('2000-01-01').' WHERE send_date = 0');
	if($database->query()){
		$msg = _EMKTG_QUEUE_DELETED;
	} else {
		$msg = _EMKTG_QUEUE_NOTDELETED;
	}
	mosRedirect('index2.php?option='.$option.'&task=showqueue', $msg);
}

?>