<?php
// $Id: configuration.admin.php,v 1.7 2008/03/31 01:59:10 sm2tony Exp $
/**
 * sm2emailmarketing - Email Marketing Newsletter
 *
 * Joomla! Newsletter Component that supports multiple list mailing and message queueing
 *
 * @package Joomla!
 * @subpackage com_sm2emailmarketing
 * @copyright Copyright (C) 2006 SM2 / All Rights Reserved
 * @license commercial
 * @author SM2 <info@sm2joomla.com>
 */

/**
 * Makes sure this is included by a parent file
 */
defined('_VALID_MOS') or die('Direct Access to this location is not allowed.');

function editConfiguration($option) {
    global $sm2emailmarketingConfig;

    // load the language elements
    $lang = sm2emailmarketingLanguage();

    // display the config form
    mosCommonHTML::loadOverlib();

    $tmpl = sm2emailmarketingPatTemplate("config.tpl", $option);

    $tmpl->addVar('header', 'FORM_NAME', _EMKTG_CONFIGURATION_FORM);
    $tmpl->addVar('edit', 'PARAMS', $sm2emailmarketingConfig->render());
    $tmpl->addObject('edit', $lang, 'LANG');

    $tmpl->displayParsedTemplate('edit');

} // editConfiguration()

function saveConfiguration ($option, $task) {
    global $database;

    // get the data passed
    $config = mosGetParam( $_POST, 'params', '' );
    if (is_array( $config )) {
        $txt = array();
        foreach ( $config as $k=>$v) {
            $txt[] = $k.'='.stripslashes($v);
        }
        $txt = mosParameters::textareaHandling($txt);

        $database->setQuery('UPDATE #__components SET params='.$database->quote($txt)
            .' WHERE `link`='.$database->Quote('option='.$option)
            .' AND `option`='.$database->Quote($option));
        $database->query();
    }

    switch ($task) {
        case "applyconfiguration":
            mosRedirect("index2.php?option=$option&task=showconfiguration", _EMKTG_CONFIGURATION_UPDATED);
            break;
        case "saveconfiguration":
        default:
            mosRedirect("index2.php?option=$option", _EMKTG_CONFIGURATION_UPDATED);
            break;
    }

} // saveConfiguration()

?>