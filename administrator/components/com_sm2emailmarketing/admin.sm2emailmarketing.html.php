<?php
// $Id: admin.sm2emailmarketing.html.php,v 1.12 2008/04/01 03:48:32 sm2tony Exp $
/**
 * SM2 EmailMarketing - Email Marketing Newsletter
 *
 * Joomla! Newsletter Component that supports multiple list mailing and message queueing
 *
 * @package Joomla!
 * @subpackage com_sm2emailmarketing
 * @copyright Copyright (C) 2006 SM2 / All Rights Reserved
 * @license commercial
 * @author SM2 <info@sm2joomla.com>
 */

/**
 * Makes sure this is included by a parent file
 */
defined('_VALID_MOS') or die('Direct Access to this location is not allowed.');

class HTML_sm2emailmarketing {

	/**
	 * Note: This help cannot currently handle multiple languages for the
	 * help instructions
	 */
    function showHelp($option) {
    	global $mosConfig_live_site;
        $tabs = new mosTabs(1);
        $loc = $mosConfig_live_site.'/administrator/index2.php?option='.$option.'&amp;task=';
        $section = mosGetParam($_GET, 'section', '');

        $active = array(
            'general'=>'',
            'message'=>'',
            'stats'=>'',
            'registered'=>'',
            'unregistered'=>'',
            'list'=>'',
            'template'=>'',
            'queue'=>'',
            'bounces'=>'',
            'plugins'=>'',
            'config'=>'',
            'licence'=>''
        );
        $include = '';

        $active[$section]=' activeTab';

        switch ($section) {
            case 'general':
            default:
                $active['general']=' activeTab';
                $include = 'components/'.$option.'/docs/general.html';
                break;
            case 'message':
                $include = 'components/'.$option.'/docs/message.html';
                break;
            case 'stats':
                $include = 'components/'.$option.'/docs/stats.html';
                break;
            case 'registered':
                $include = 'components/'.$option.'/docs/registered.html';
                break;
            case 'unregistered':
                $include = 'components/'.$option.'/docs/unregistered.html';
                break;
            case 'list':
                $include = 'components/'.$option.'/docs/list.html';
                break;
            case 'template':
                $include = 'components/'.$option.'/docs/template.html';
                break;
            case 'queue':
                $include = 'components/'.$option.'/docs/queue.html';
                break;
            case 'bounces':
                $include = 'components/'.$option.'/docs/bounces.html';
                break;
            case 'plugins':
                $include = 'components/'.$option.'/docs/plugins.html';
                break;
            case 'config':
                $include = 'components/'.$option.'/docs/config.html';
                break;
            case 'licence':
                $include = 'components/'.$option.'/docs/licence.html';
                break;
        }
?>
<link href="<?php echo $mosConfig_live_site; ?>/administrator/components/<?php echo $option; ?>/styles/sm2emailmarketing.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo $mosConfig_live_site; ?>/administrator/components/<?php echo $option; ?>/includes/toggleDl.js"></script>
<div id="adminMenu">
<ul>
	<li class="messagemanager"><a href="<?php echo $loc.'showmessages' ?>">Messages</a></li>
    <li class="stats"><a href="<?php echo $loc.'showstatistics' ?>">Statistics</a></li>
	<li class="registered"><a href="<?php echo $loc.'showregistered' ?>">Client Mgmt Subscribers</a></li>
	<li class="unregistered"><a href="<?php echo $loc.'showunregistered' ?>">List Only Subscribers</a></li>
	<li class="list"><a href="<?php echo $loc.'showlists' ?>">Lists</a></li>
	<li class="template"><a href="<?php echo $loc.'showtemplates' ?>">Templates</a></li>
	<li class="queue"><a href="<?php echo $loc.'showqueue' ?>">Queues</a></li>
	<li class="plugin"><a href="<?php echo $loc.'showplugins' ?>">Plugins</a></li>
    <li class="tool"><a href="<?php echo $loc.'showtools' ?>">Tools</a></li>
	<li class="config"><a href="<?php echo $loc.'showconfiguration' ?>">Configuration</a></li>
	<li class="help active"><a href="<?php echo $loc.'showhelp' ?>">Help</a></li>
</ul>
<br class="clearing" />
</div>
  <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminheading">
    <tr>
      <th style="background: none; padding-left: 0px; text-align: center;" width="100%" align="center">
        <?php echo _EMKTG.' - '._EMKTG_FULL_TITLE."\n"; ?>
      </th>
    </tr>
    <tr>
    </tr>
    <tr>
      <th style="background: url(components/<?php echo $option; ?>/images/help.gif) no-repeat left; height: 80px; padding-left: 80px;" width="100%" align="left">
        <?php echo _EMKTG_HELP; ?>
      </th>
    </tr>
  </table>
  <?php $loc .= 'showhelp&section='; ?>
  <div>
    <div class="sm2tabArea">
      <a class="sm2tab<?php echo $active['general']; ?>" href="<?php echo $loc; ?>general">
        <?php echo _EMKTG_HELP_GENERAL; ?>
      </a>
      <a class="sm2tab<?php echo $active['message']; ?>" href="<?php echo $loc; ?>message">
        <?php echo _EMKTG_HELP_MESSAGE; ?>
      </a>
      <a class="sm2tab<?php echo $active['stats']; ?>" href="<?php echo $loc; ?>stats">
        <?php echo _EMKTG_HELP_STATISTICS; ?>
      </a>
      <a class="sm2tab<?php echo $active['registered']; ?>" href="<?php echo $loc; ?>registered">
        <?php echo _EMKTG_HELP_REGISTERED; ?>
      </a>
      <a class="sm2tab<?php echo $active['unregistered']; ?>" href="<?php echo $loc; ?>unregistered">
        <?php echo _EMKTG_HELP_UNREGISTERED; ?>
      </a>
      <a class="sm2tab<?php echo $active['list']; ?>" href="<?php echo $loc; ?>list">
        <?php echo _EMKTG_HELP_LIST; ?>
      </a>
      <a class="sm2tab<?php echo $active['template']; ?>" href="<?php echo $loc; ?>template">
        <?php echo _EMKTG_HELP_TEMPLATE; ?>
      </a>
      <a class="sm2tab<?php echo $active['queue']; ?>" href="<?php echo $loc; ?>queue">
        <?php echo _EMKTG_HELP_QUEUE; ?>
      </a>
      <a class="sm2tab<?php echo $active['bounces']; ?>" href="<?php echo $loc; ?>bounces">
        <?php echo _EMKTG_HELP_BOUNCES; ?>
      </a>
      <a class="sm2tab<?php echo $active['plugins']; ?>" href="<?php echo $loc; ?>plugins">
        <?php echo _EMKTG_HELP_PLUGINS; ?>
      </a>
      <a class="sm2tab<?php echo $active['config']; ?>" href="<?php echo $loc; ?>config">
        <?php echo _EMKTG_HELP_CONFIG; ?>
      </a>
      <a class="sm2tab<?php echo $active['licence']; ?>" href="<?php echo $loc; ?>licence">
        <?php echo _EMKTG_HELP_LICENCE; ?>
      </a>
    </div>
    <div class="sm2tabMain">
      <div class="sm2tabIframeWrapper">
        <div class="sm2tabContent">
          <?php include($include); ?>
        </div>
      </div>
    </div>
  </div>
<?php

    } // showHelp()

} // class HTML_sm2emailmarketing()
?>