<?php
// $Id: uninstall.sm2emailmarketing.php,v 1.5 2006/10/23 01:14:44 blair_tony Exp $
/**
 * sm2emailmarketing - Email Marketing Newsletter
 * 
 * Joomla! Newsletter Component that supports multiple list mailing and message queueing
 * 
 * @package Joomla!
 * @subpackage com_sm2emailmarketing
 * @copyright Copyright (C) 2006 SM2 / All Rights Reserved
 * @license commercial
 * @author SM2 <info@sm2joomla.com>
 */

/**
 *  Ensures this file is being included by a parent file
 */
defined('_VALID_MOS') or die('Direct Access to this location is not allowed.');
 
/**
 * Joomla! uninstall function
 */
function com_uninstall() {
    
} // com_uninstall()

?>