<?php
// $Id: Emktgtooltip.php,v 1.3 2006/10/23 01:14:55 blair_tony Exp $
/**
 * sm2emailmarketing - Email Marketing Newsletter
 *
 * Joomla! Newsletter Component that supports multiple list mailing and message queueing
 *
 * @package Joomla!
 * @subpackage com_sm2emailmarketing
 * @copyright Copyright (C) 2006 SM2 / All Rights Reserved
 * @license commercial
 * @author SM2 <info@sm2joomla.com>
 */

class patTemplate_Function_eMktgTooltip extends patTemplate_Function {
   /**
    * name of the function
    * @access   private
    * @var      string
    */
    var $_name = 'Tooltip';

   /**
    * call the function
    *
    * @access   public
    * @param    array   parameters of the function (= attributes of the tag)
    * @param    string  content of the tag
    * @return   string  content to insert into the template
    */
    function call($params, $content) {

        if (!isset($params['image'])) $params['image']="";

        return sm2emailmarketingReturnToolTip($content, $params['image']);

    } // call

} // class patTemplate_Function_eMktgTooltip
?>