<?php
/*
/**
* CHRONOFORMS version 1.0 stable
* Copyright (c) 2006 Chrono_Man, ChronoEngine.com. All rights reserved.
* Author: Chrono_Man (ChronoEngine.com)
* See readme.html.
* Visit http://www.ChronoEngine.com for regular update and information.
**/

/* ensure that this file is called from another file */
defined( '_VALID_MOS' ) or die( 'Direct access to this file is prohibited.' );
class HTML_ChronoContact {
  function showChronoContact( $rows, $pageNav, $option ) {
    // HTML starts here, combined with short PHP commands for table creation
    global $database, $mainframe, $my, $mosConfig_absolute_path, $mosConfig_dbprefix, $mosConfig_list_limit, $mosConfig_live_site;
    mosCommonHTML::loadOverlib();
    ?>
    <form action="index2.php" method="post" name="adminForm">
    <table class="adminheading">
    <tr>
	  <th>Chrono Forms - Forms Manager</th>
    </tr>
	</table>
    <table class="adminlist">
	<tr>
	  <th width="20" class='title'>#</th>
	  <th width="5%" class='title'>Form ID</th>
	  <th width="20" class='title'><input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($rows); ?>);" /></th>

	  <th width="20%" align="left" class='title'>Name</th>
	  <th width="60%" align="left" class='title'>Link</th>
	  <th width="20%" align="right" class='title'>Data</th>
	</tr>
    <?php
	$k = 0;
    for ($i=0, $n=count($rows); $i < $n; $i++) {
	  $row = $rows[$i];

		$link	= 'index2.php?option=com_chronocontact&task=editA&hidemainmenu=1&id='. $row->id;
		

	  //$checked 	= mosCommonHTML::CheckedOutProcessing( $row, $i );


	  ?>
	  <tr class="<?php echo "row$k"; ?>">
		<td width="20" align="center"><?php echo $i+$pageNav->limitstart+1;?></td>
		<td width="20" align="center"><?php echo $row->id; ?></td>
		<td width="20"><input type="checkbox" id="cb<?php echo $i;?>" name="cid[]" value="<?php echo $row->id; ?>" onclick="isChecked(this.checked);" /></td>
		
		<td width="20%" align="left" ><a href="#edit" onclick="return listItemTask('cb<?php echo $i;?>','edit')"><?php echo $row->name; ?></a></td>
		<td width="60%" align="left" ><a target="_blank" href="<?php echo $mosConfig_live_site; ?>/index.php?option=com_chronocontact&amp;chronoformname=<?php echo $row->name; ?>">index.php?option=com_chronocontact&amp;chronoformname=<?php echo $row->name; ?></a></td>
		<?php
		$result = $database->getTableList();
		if (in_array($mosConfig_dbprefix."chronoforms_".$row->id, $result)) {
		?>
			<td width="20%" align="left" ><a href="#show" onclick="return listItemTask('cb<?php echo $i;?>','show')"><?php echo 'Show saved Data'; ?></a></td>
		<?php }else{ ?>
			<td width="20%" align="left" ><a href="#createtable" onclick="return listItemTask('cb<?php echo $i;?>','createtable')"><?php echo 'Create Table'; ?></a></td>
		<?php
		}
		?>
	  </tr>
      <?php
			$k = 1 - $k;
		}
    ?>
		<?php echo $pageNav->getListFooter(); ?>
	  </table>
	  <input type="hidden" name="option" value="<?php echo $option; ?>" />
	  <input type="hidden" name="task" value="" />
	  <input type="hidden" name="boxchecked" value="0" />
	  <input type="hidden" name="hidemainmenu" value="0">
	  </form>
    <?php
	}

  // this method represents the edit mask
  function editChronoContact( $row, $option ) {
   global $mosConfig_live_site, $database;
		mosMakeHtmlSafe( $row, ENT_QUOTES );
		$tabs = new mosTabs(1);
		// JavaScript Code for checking forms starts here
		$paramsvalues = mosParseParams( $row->paramsall );
    ?>
	  <script language="javascript" type="text/javascript">
		function submitbutton(pressbutton) {
			var form = document.adminForm;
			if (pressbutton == "cancel") {
				submitform( pressbutton );
				return;
			}
			// do field validation
			if ((form.name.value == '')||(form.name.value.search(/ /) > -1)) {
				alert( "Please enter the form name without any spaces" );
			}else if (form.emailresults.value == '2') {
				if ((form.extraemail.value == '')||(form.emailsubject.value == '')||(document.getElementById('paramsfrom_email').value == '')||(document.getElementById('paramsfrom_name').value == '')) {
					alert( "If you choosed to email results then you must add : Email Address, Email subject, From Name and From Email" );
				} else {
					submitform( pressbutton );
				}
			} else {
				submitform( pressbutton );
			}
		}
		function addmenuitem(){
			var form = document.adminForm;
			if (form.menutext.value == '') {
				alert( "Please enter a text for the menu link" );
			} else {
				submitform( 'addmenuitem' );
			}
		}
		function toggleList(e){
		element = document.getElementById(e).style;
		element.display == 'none' ? element.display = 'block' :
		element.display='none';
		}

	  </script>

	  <?php echo "<script  type=\"text/javascript\" "
    . "src=\"$mosConfig_live_site/includes/js/overlib_mini.js\"></script>"; ?>
	<?php echo mosToolTip( "This is a tooltip with some usefull info about the field" ); ?> ToolTip
	  <form action="index2.php" method="post" name="adminForm" id="adminForm" class="adminForm">
	  <?php
		  	$tabs->startPane( 'chronoforms' );
	        $tabs->startTab( "General", 'general' );
	  ?>
	<table border="0" cellpadding="3" cellspacing="0">
  <tr style="background-color:#c9c9c9 ">
		<td><?php echo mosToolTip( "This is the Form name, It must be Unique." ); ?></td>
		<td><strong>Form Name:</strong> <br><strong style="color:#FF0000 ">must be unique between forms</strong></td>
		<td></td>
		<td><input type="text" class="inputbox" size="50" maxlength="100" name="name" value="<?php echo $row->name; ?>" /></td>
	</tr>
	<tr>
		<td><?php echo mosToolTip( "Email results after form submit ?" ); ?></td>
		<td><strong>Email the results ?</strong> </td>
		<td></td>
		<td><select name="emailresults" id="emailresults" >
          <option value="0"<?php if($row->emailresults == 0){echo 'selected';}; ?>>No</option>
		  <option value="2"<?php if($row->emailresults == 2){echo 'selected';}; ?>>Yes</option>
        </select></td>
	</tr>
	<tr style="background-color:#c9c9c9 ">
		<td><?php echo mosToolTip( "To which email(s) form values will be sent, separate multiple emails with commas ," ); ?></td>
		<td><strong>Email Address(es):</strong> </td>
		<td><a href="javascript:toggleList('extraemail')">[+/-]</a></td>
		<td><input type="text" class="inputbox" size="50" maxlength="100" name="extraemail" id="extraemail" value="<?php echo $row->extraemail; ?>" /></td>
	</tr>
	<tr>
		<td><?php echo mosToolTip( "The email default subject" ); ?></td>
		<td><strong>Email Subject:</strong> </td>
		<td><a href="javascript:toggleList('emailsubject')">[+/-]</a></td>
		<td><input type="text" class="inputbox" size="50" maxlength="100" name="emailsubject" id="emailsubject" value="<?php echo $row->emailsubject; ?>" /></td>
	</tr>
	<tr style="background-color:#c9c9c9 ">
		<td><?php echo mosToolTip( "The CC Email(s)" ); ?></td>
		<td><strong>CC Email:</strong> </td>
		<td></td>
		<td>
		<input type="text" name="params[ccemail]" id="cc_email" class="inputbox" size="50" maxlength="100" value="<?php echo $paramsvalues->ccemail; ?>">
		</td>
	</tr>
	<tr>
		<td><?php echo mosToolTip( "The BCC Email(s)" ); ?></td>
		<td><strong>BCC Email:</strong> </td>
		<td></td>
		<td>
		<input type="text" name="params[bccemail]" id="bcc_email" class="inputbox" size="50" maxlength="100" value="<?php echo $paramsvalues->bccemail; ?>">
		</td>
	</tr>
	<tr style="background-color:#c9c9c9 ">
		<td><?php echo mosToolTip( "Put here any code you want to use inside the form tag, like a validation javascript function to be executed when the form is submitted, code example : onsubmit=return validateForm()" ); ?></td>
		<td><strong>Form tag attachment:</strong><br><strong style="color:#FF0000 ">something like onSubmit()</strong> </td>
		<td></td>
		<td><input type="text" class="inputbox" size="100" maxlength="500" name="attformtag" value="<?php echo $row->attformtag; ?>" /></td>
	</tr>
	<tr>
		<td><?php echo mosToolTip( "Form method" ); ?></td>
		<td><strong>Form method:</strong> </td>
		<td></td>
		<td>
		<select name="params[formmethod]" id="params[formmethod]">
		<option<?php if($paramsvalues->formmethod == 'post'){ echo ' selected';} ?> value="post">Post</option>
		<option<?php if($paramsvalues->formmethod == 'get'){ echo ' selected';} ?> value="get">Get</option>
		</select>
		</td>
	</tr>
	
	<tr style="background-color:#c9c9c9 ">
		<td><?php echo mosToolTip( "Enter here the fields names you want to omit from being sent into email" ); ?></td>
		<td><strong>Omitted fields names:</strong> </td>
		<td></td>
		<td>
		<input type="text" name="params[omittedfields]" id="params[omittedfields]" class="inputbox" size="50" maxlength="100" value="<?php echo $paramsvalues->omittedfields; ?>">
		</td>
	</tr>
	<tr>
		<td><?php echo mosToolTip( "Record and send users IP?" ); ?></td>
		<td><strong>Record and send users IP?</strong> </td>
		<td></td>
		<td>
		<select name="params[recip]" id="params[recip]">
			<option<?php if($paramsvalues->recip == 'Yes'){ echo ' selected';} ?> value="Yes">Yes</option>
			<option<?php if($paramsvalues->recip == 'No'){ echo ' selected';} ?> value="No">No</option>
		</select>
		</td>
	</tr>

	<tr style="background-color:#c9c9c9 ">
		<td><?php echo mosToolTip( "The email which will appear as *from*, must put a valid email here" ); ?></td>
		<td><strong>From Email:</strong> </td>
		<td></td>
		<td>
		<input type="text" name="params[from_email]" id="paramsfrom_email" class="inputbox" size="50" maxlength="100" value="<?php echo $paramsvalues->from_email; ?>">
		</td>
	</tr>
	<tr>
		<td><?php echo mosToolTip( "the name which will appear as from, must put a valid name here, e.g: Admin" ); ?></td>
		<td><strong>From name:</strong> </td>
		<td></td>
		<td>
		<input type="text" name="params[from_name]" id="paramsfrom_name" class="inputbox" size="50" maxlength="100" value="<?php echo $paramsvalues->from_name; ?>">
		</td>
	</tr>
	<tr style="background-color:#c9c9c9 ">
		<td><?php echo mosToolTip( "ReplyTo email" ); ?></td>
		<td><strong>ReplyTo email:</strong> </td>
		<td></td>
		<td>
		<input type="text" name="params[replyto_email]" id="params[replyto_email]" class="inputbox" size="50" maxlength="100" value="<?php echo $paramsvalues->replyto_email; ?>">
		</td>
	</tr>
	<tr>
		<td><?php echo mosToolTip( "ReplyTo name" ); ?></td>
		<td><strong>ReplyTo name:</strong> </td>
		<td></td>
		<td>
		<input type="text" name="params[replyto_name]" id="params[replyto_name]" class="inputbox" size="50" maxlength="100" value="<?php echo $paramsvalues->replyto_name; ?>">
		</td>
	</tr>
	<tr style="background-color:#c9c9c9 ">
		<td><?php echo mosToolTip( "Please choose wheather you want to receive the email as a list of items *titles* with answers or with the form completed(still beta)." ); ?></td>
		<td><strong>In which format to send the results email:</strong> </td>
		<td></td>
		<td>
			<select name="params[email_type]" id="params[email_type]">
				<!--<option<?php if($paramsvalues->email_type == '0'){ echo ' selected';} ?> value="0">Form completed(under development)</option>-->
				<option<?php if($paramsvalues->email_type == '1'){ echo ' selected';} ?> value="1">Fields Titles</option>
				<option<?php if($paramsvalues->email_type == '2'){ echo ' selected';} ?> value="2">My Template</option>
			</select>
		</td>
	</tr>
	<tr>
		<td><?php echo mosToolTip( "if debug is ON then ChronoForms will show diagnostic output" ); ?></td>
		<td><strong>Debug:</strong> </td>
		<td></td>
		<td>
			<select name="params[debug]" id="params[debug]">
				<option<?php if($paramsvalues->debug == '0'){ echo ' selected';} ?> value="0">OFF</option>
				<option<?php if($paramsvalues->debug == '1'){ echo ' selected';} ?> value="1">ON</option>
			</select>
		</td>
	</tr>
	<tr style="background-color:#c9c9c9 ">
		<td><?php echo mosToolTip( "The most used is ENGINE (v4+) however you can consider changing this to TYPE of you get a mysql error when you try to create a table" ); ?></td>
		<td><strong>MYSQL Statement ? ENGINE or TYPE</strong> </td>
		<td></td>
		<td>
			<select name="params[mysql_type]" id="params[mysql_type]">
				<option<?php if($paramsvalues->mysql_type == '1'){ echo ' selected';} ?> value="1">ENGINE</option>
				<option<?php if($paramsvalues->mysql_type == '2'){ echo ' selected';} ?> value="2">TYPE</option>
			</select>
		</td>
	</tr>
	<tr>
		<td><?php echo mosToolTip( "Enable mambots to be applied to this form contents?" ); ?></td>
		<td><strong>Enable mambots?</strong> </td>
		<td></td>
		<td>
		<select name="params[enmambots]" id="params[enmambots]">
			<option<?php if($paramsvalues->enmambots == 'No'){ echo ' selected';} ?> value="No">No</option>
			<option<?php if($paramsvalues->enmambots == 'Yes'){ echo ' selected';} ?> value="Yes">Yes</option>
		</select>
		</td>
	</tr>
	</table>
	<?php
			$tabs->endTab();
			$tabs->startTab( "Special fields", 'spfields' );
	?>
	<table border="0" cellpadding="3" cellspacing="0">
	<tr style="background-color:#c9c9c9 ">
		<td><?php echo mosToolTip( "Email results to this field value, this field name may contain user(s) email(s)" ); ?></td>
		<td><strong>Email field:</strong> </td>
		<td></td>
		<td>
		<input type="text" name="params[emailfield]" id="params[emailfield]" class="inputbox" size="50" maxlength="100" value="<?php echo $paramsvalues->emailfield; ?>">
		</td>
	</tr>
	<tr>
		<td><?php echo mosToolTip( "Email results with a subject of this field value, this will override the subject assigned above" ); ?></td>
		<td><strong>Subject field:</strong> </td>
		<td></td>
		<td>
		<input type="text" name="params[subjectfield]" id="params[subjectfield]" class="inputbox" size="50" maxlength="100" value="<?php echo $paramsvalues->subjectfield; ?>">
		</td>
	</tr>
	<tr style="background-color:#c9c9c9 ">
		<td><?php echo mosToolTip( "CC results to this field value, this field name may contain user(s) email(s)" ); ?></td>
		<td><strong>CC field:</strong> </td>
		<td></td>
		<td>
		<input type="text" name="params[ccfield]" id="params[ccfield]" class="inputbox" size="50" maxlength="100" value="<?php echo $paramsvalues->ccfield; ?>">
		</td>
	</tr>
	<tr>
		<td><?php echo mosToolTip( "BCC results to this field value, this field name may contain user(s) email(s)" ); ?></td>
		<td><strong>BCC field:</strong> </td>
		<td></td>
		<td>
		<input type="text" name="params[bccfield]" id="params[bccfield]" class="inputbox" size="50" maxlength="100" value="<?php echo $paramsvalues->bccfield; ?>">
		</td>
	</tr>
	<tr style="background-color:#c9c9c9 ">
		<td><?php echo mosToolTip( "Email results with a from email with this field value" ); ?></td>
		<td><strong>From email field:</strong> </td>
		<td></td>
		<td>
		<input type="text" name="params[fromemailfield]" id="params[fromemailfield]" class="inputbox" size="50" maxlength="100" value="<?php echo $paramsvalues->fromemailfield; ?>">
		</td>
	</tr>
	<tr>
		<td><?php echo mosToolTip( "Email results with a from name with this field value" ); ?></td>
		<td><strong>From name field:</strong> </td>
		<td></td>
		<td>
		<input type="text" name="params[fromnamefield]" id="params[fromnamefield]" class="inputbox" size="50" maxlength="100" value="<?php echo $paramsvalues->fromnamefield; ?>">
		</td>
	</tr>
	</table>
	<?php
			$tabs->endTab();
			$tabs->startTab( "Form Code", 'codes' );
	?>
	<table border="0" cellpadding="3" cellspacing="0">
	<tr style="background-color:#c9c9c9 ">
		<td><?php echo mosToolTip( "Plz enter the form HTML code here, the code may contains proper PHP code with tags and do NOT use the *form* tags, it will be created automaticlly" ); ?></td>
		<td><strong>Form HTML:</strong><br><strong style="color:#FF0000 ">(may contain PHP code with tags)</strong> </td>
		<td><a href="javascript:toggleList('html')">[+/-]</a></td>
		<td><textarea name="html" id="html" style="display:none" cols="80" rows="30"><?php echo $row->html; ?></textarea></td>
	</tr>
	<tr>
		<td><?php echo mosToolTip( "you can use some Javascript code here, plz dont enter the script opening or close tags" ); ?></td>
		<td><strong>Form JavaScript:</strong><br><strong style="color:#FF0000 ">(without the script tags)</strong> </td>
		<td><a href="javascript:toggleList('scriptcode')">[+/-]</a></td>
		<td><textarea name="scriptcode" id="scriptcode" style="display:none" cols="80" rows="10"><?php echo $row->scriptcode; ?></textarea></td>
	</tr>

	<tr style="background-color:#c9c9c9 ">
		<td><?php echo mosToolTip( "well formatted PHP code to be executed just when form is submitted but BEFORE Mail is sent" ); ?></td>
		<td><strong>On Submit code - before sending email:</strong><br><strong style="color:#FF0000 ">(PHP code with tags)</strong> </td>
		<td><a href="javascript:toggleList('onsubmitcodeb4')">[+/-]</a></td>
		<td><textarea name="onsubmitcodeb4" id="onsubmitcodeb4" style="display:none" cols="80" rows="10"><?php echo $row->onsubmitcodeb4; ?></textarea></td>
	</tr>

	<tr>
		<td><?php echo mosToolTip( "well formatted PHP code to be executed just when form is submitted but AFTER Mail is sent" ); ?></td>
		<td><strong>On Submit code - after sending email:</strong><br><strong style="color:#FF0000 ">(PHP code with tags)</strong> </td>
		<td><a href="javascript:toggleList('onsubmitcode')">[+/-]</a></td>
		<td><textarea name="onsubmitcode" id="onsubmitcode" style="display:none" cols="80" rows="10"><?php echo $row->onsubmitcode; ?></textarea></td>
	</tr>

	<tr style="background-color:#c9c9c9 ">
		<td><?php echo mosToolTip( "This is the submission results email template, you can create a template of HTML for what you want to receive, plz use {field_name} to be replaced with this field submitted result" ); ?></td>
		<td><strong>Email template:</strong> </td>
		<td><a href="javascript:toggleList('emailtemplate')">[+/-]</a></td>
		<td><textarea name="emailtemplate" id="emailtemplate" style="display:none" cols="80" rows="10"><?php echo $row->emailtemplate; ?></textarea></td>
	</tr>
	</table>
	<?php
			$tabs->endTab();
			$tabs->startTab( "Form URLs", 'urls' );
	?>
	<table border="0" cellpadding="3" cellspacing="0">
	<tr style="background-color:#c9c9c9 ">
		<td><?php echo mosToolTip( "This is the URL where the form will go after its submitted" ); ?></td>
		<td><strong>Redirect URL:</strong> </td>
		<td></td>
		<td><input type="text" class="inputbox" size="50" maxlength="100" name="redirecturl" value="<?php echo $row->redirecturl; ?>" /><strong>  by default(if empty) will load an empty body area</strong></td>
	</tr>

	<tr>
		<td><?php echo mosToolTip( "This is the *action* URL, you can use this to submit the form results to an external page like your payment gateway for example" ); ?></td>
		<td><strong>Submit URL:</strong><br><strong style="color:#FF0000 ">the "form" "action" URL</strong> </td>
		<td></td>
		<td><input type="text" class="inputbox" size="50" maxlength="100" name="submiturl" value="<?php echo $row->submiturl; ?>" /><strong> dont put anything here unless you know what you are doing</strong></td>
	</tr>
	</table>
	<?php
			$tabs->endTab();
			$tabs->startTab( "Elements titles", 'titlesx' );
	?>
	<table border="0" cellpadding="3" cellspacing="0">
	<tr style="background-color:#c9c9c9 ">
		<td><?php echo mosToolTip( "Plz enter the form HTML elements titles here, these are the text which will replace the elements names in the emails sent or the stored data viewer pages, you must enter one lement name followed by = then the element title per line" ); ?></td>
		<td><strong>Elements titles:</strong><br><strong style="color:#FF0000 ">e.g:<br>fname=first name<br>
		lname=Last name<br>...<br>..</strong> </td>
		<td><a href="javascript:toggleList('titles')">[+/-]</a></td>
		<td><textarea name="titles" id="titles" cols="80" rows="20"><?php echo $row->titlesall; ?></textarea></td>
	</tr>
	</table>
	<?php
			$tabs->endTab();
			$tabs->startTab( "AutoGenerated code", 'autogen' );
	?>
	<table border="0" cellpadding="3" cellspacing="0">
	<tr style="background-color:#c9c9c9 ">
		<td><?php echo mosToolTip( "This code has been auto generated when you created a table for this form and is required for saving data" ); ?></td>
		<td><strong>Auto generated:</strong><br><strong style="color:#FF0000 ">Dont touch this unless you know what you are doing</strong> </td>
		<td><a href="javascript:toggleList('autogenerated')">[+/-]</a></td>
		<td><textarea name="autogenerated" id="autogenerated" cols="80" rows="20"><?php echo $row->autogenerated; ?></textarea></td>
	</tr>
	</table>
	<?php
			$tabs->endTab();
			$tabs->startTab( "File Uploads", 'uploads' );
	?>
	<table border="0" cellpadding="3" cellspacing="0">
	<tr style="background-color:#c9c9c9 ">
		<td><?php echo mosToolTip( "Choose if you want to enable file uploads through your form" ); ?></td>
		<td><strong>Enable uploads:</strong><br><strong style="color:#FF0000 "></strong> </td>
		<td></td>
		<td>
		<select name="params[uploads]" id="params[uploads]">
			<option<?php if($paramsvalues->uploads == 'No'){ echo ' selected';} ?> value="No">No</option>
			<option<?php if($paramsvalues->uploads == 'Yes'){ echo ' selected';} ?> value="Yes">Yes</option>
		</select>
		</td>
	</tr>

	<tr>
		<td><?php echo mosToolTip( "Which fields are of type FILE ? Use this syntax to control allowable extensions : field_name_1:jpg|gif|doc|zip, field_name_2:rar|avi , * = all are ok, but a petit hacker may **** your machine this way =)" ); ?></td>
		<td><strong>Field names &amp; allowed Extensions for each:</strong><br><strong style="color:#FF0000 ">E.g: field_name_1:jpg|gif|doc|zip, field_name_2:rar|avi , * = all are ok</strong> </td>
		<td></td>
		<td>
		<input type="text" name="params[uploadfields]" id="params[uploadfields]" class="inputbox" size="100" maxlength="500" value="<?php echo $paramsvalues->uploadfields; ?>">
		</td>
	</tr>
	<tr>
		<td><?php echo mosToolTip( "Max size for uploaded files" ); ?></td>
		<td><strong>Max allowable size:</strong><br><strong style="color:#FF0000 ">in KB</strong> </td>
		<td></td>
		<td>
		<input type="text" name="params[uploadmax]" id="params[uploadmax]" class="inputbox" size="50" maxlength="100" value="<?php echo $paramsvalues->uploadmax; ?>">
		</td>
	</tr>
	<tr>
		<td><?php echo mosToolTip( "Min size for uploaded files" ); ?></td>
		<td><strong>Min allowable size:</strong><br><strong style="color:#FF0000 ">in KB</strong> </td>
		<td></td>
		<td>
		<input type="text" name="params[uploadmin]" id="params[uploadmin]" class="inputbox" size="50" maxlength="100" value="<?php echo $paramsvalues->uploadmin; ?>">
		</td>
	</tr>

	</table>
	<?php
			$tabs->endTab();
			$tabs->startTab( "DataView fields", 'dvfields' );
	?>
	<table border="0" cellpadding="3" cellspacing="0">
	<tr style="background-color:#c9c9c9 ">
		<td><?php echo mosToolTip( "Put here a list of fields names you would like to appear at the datagrid for this form data page, more than 1 field can be used separarted with a comma ," ); ?></td>
		<td><strong>Extra dataview columns fields names:</strong> </td>
		<td></td>
		<td>
		<input type="text" name="params[dvfields]" id="params[dvfields]" class="inputbox" size="50" maxlength="100" value="<?php echo $paramsvalues->dvfields; ?>">
		</td>
	</tr>
	</table>
	
	<?php
			$tabs->endTab();
            $tabs->startTab( "Anti Spam", 'antispam' );
            // :: HACK :: show GD capability
            if(function_exists('gd_info')){
          	  $gd_info = gd_info();
			  $imagever_ok = true;
			}else{
				$imagever_ok = false;
			}
            if($imagever_ok){
				if ( !$gd_info['GD Version'] ) {
					$imagever_ok = false;
					$gd_info['GD Version'] = "GD library not found.";
				} else {
					if ( $gd_info['FreeType Support'] ) {
						$gd_info['FreeType Support'] = 'Yes';
					} else {
						$gd_info['FreeType Support'] = 'No';
						$imagever_ok = false;
					}
					if ( $gd_info['PNG Support'] ) {
						$gd_info['PNG Support'] = 'Yes';
					} else {
						$gd_info['PNG Support'] = 'No';
						$imagever_ok = false;
					}
				}
			}
			?>
	<table border="0" cellpadding="3" cellspacing="0">
	<tr style="background-color:#c9c9c9 ">
		<td><?php echo mosToolTip( "If you want to use image verification select *Yes* here then add {imageverification} inside your form html where you want the image verification to appear."); ?></td>
<?php
        if ( $imagever_ok ) {
            echo "<td><strong>Use Image verification:</strong><br><strong style='color:#FF0000' ></strong></td>
                <td>&nbsp;</td>
                <td>";
				?>
                <select name="params[imagever]" id="params[imagever]">
					<option<?php if($paramsvalues->imagever == 'No'){ echo ' selected';} ?> value="No">No</option>
					<option<?php if($paramsvalues->imagever == 'Yes'){ echo ' selected';} ?> value="Yes">Yes</option>
				</select>
				<?php
		} else {
		    echo "Image verification cannot be enabled becasue the GD Library has not been detected at your PHP installation.
		      <input type='hidden' name='params[imagever]' id='params[imagever]' value ='No' />";
		}
?>
		</td>
	</tr>
	<?php if ( $imagever_ok ) { ?>
	<tr>
	   <td>&nbsp;</td>
	   <td>GD Version</td>
	   <td><?php echo $gd_info['GD Version']; ?></td>
	</tr>
		<tr>
	   <td>&nbsp;</td>
	   <td>FreeType Support</td>
	   <td><?php echo $gd_info['FreeType Support']; ?></td>
	</tr>
	</tr>
		<tr>
	   <td>&nbsp;</td>
	   <td>PNG Support</td>
	   <td><?php echo $gd_info['PNG Support']; ?></td>

	</tr>
	<?php } ?>
<?php
	if ( $imagever_ok ) {
	    echo "<tr>
	       <td>&nbsp;</td>
	       <td>Sample image: </td>
	       <td><img src='$mosConfig_live_site/administrator/components/com_chronocontact/chrono_verification.php'></td>
	       </tr>";
	}
?>
	<!-- // end hack -->
	</table>
	<?php
			$tabs->endTab();
			$tabs->startTab( "Validation", 'validation' );
	?>
	<table border="0" cellpadding="3" cellspacing="0">
	<tr style="background-color:#c9c9c9 ">
		<td><?php echo mosToolTip( "Enable Validation ?" ); ?></td>
		<td><strong>Enable Validation ?</strong> </td>
		<td></td>
		<td>
		<select name="params[validate]" id="params[validate]">
			<option<?php if($paramsvalues->validate == 'No'){ echo ' selected';} ?> value="No">No</option>
			<option<?php if($paramsvalues->validate == 'Yes'){ echo ' selected';} ?> value="Yes">Yes</option>
		</select>
		</td>
	</tr>
	<tr>
		<td><?php echo mosToolTip( "Put list of fields names separated with comma *,* if more than one" ); ?></td>
		<td><strong>1 - required (not blank)</strong> </td>
		<td></td>
		<td>
		<input type="text" name="params[val_required]" id="params[val_required]" class="inputbox" size="50" maxlength="100" value="<?php echo $paramsvalues->val_required; ?>">
		</td>
	</tr>
	<tr>
		<td><?php echo mosToolTip( "Put list of fields names separated with comma *,* if more than one" ); ?></td>
		<td><strong>2- validate-number (a valid number)</strong> </td>
		<td></td>
		<td>
		<input type="text" name="params[val_validate_number]" id="params[val_validate_number]" class="inputbox" size="50" maxlength="100" value="<?php echo $paramsvalues->val_validate_number; ?>">
		</td>
	</tr>
	<tr>
		<td><?php echo mosToolTip( "Put list of fields names separated with comma *,* if more than one" ); ?></td>
		<td><strong>3- validate-digits (digits only)</strong> </td>
		<td></td>
		<td>
		<input type="text" name="params[val_validate_digits]" id="params[val_validate_digits]" class="inputbox" size="50" maxlength="100" value="<?php echo $paramsvalues->val_validate_digits; ?>">
		</td>
	</tr>
	<tr>
		<td><?php echo mosToolTip( "Put list of fields names separated with comma *,* if more than one" ); ?></td>
		<td><strong>4- validate-alpha (letters only)</strong> </td>
		<td></td>
		<td>
		<input type="text" name="params[val_validate_alpha]" id="params[val_validate_alpha]" class="inputbox" size="50" maxlength="100" value="<?php echo $paramsvalues->val_validate_alpha; ?>">
		</td>
	</tr>
	<tr>
		<td><?php echo mosToolTip( "Put list of fields names separated with comma *,* if more than one" ); ?></td>
		<td><strong>5- validate-alphanum (only letters and numbers)</strong> </td>
		<td></td>
		<td>
		<input type="text" name="params[val_validate_alphanum]" id="params[val_validate_alphanum]" class="inputbox" size="50" maxlength="100" value="<?php echo $paramsvalues->val_validate_alphanum; ?>">
		</td>
	</tr>
	<tr>
		<td><?php echo mosToolTip( "Put list of fields names separated with comma *,* if more than one" ); ?></td>
		<td><strong>6- validate-date (a valid date value)</strong> </td>
		<td></td>
		<td>
		<input type="text" name="params[val_validate_date]" id="params[val_validate_date]" class="inputbox" size="50" maxlength="100" value="<?php echo $paramsvalues->val_validate_date; ?>">
		</td>
	</tr>
	<tr>
		<td><?php echo mosToolTip( "Put list of fields names separated with comma *,* if more than one" ); ?></td>
		<td><strong>7- validate-email (a valid email address)</strong> </td>
		<td></td>
		<td>
		<input type="text" name="params[val_validate_email]" id="params[val_validate_email]" class="inputbox" size="50" maxlength="100" value="<?php echo $paramsvalues->val_validate_email; ?>">
		</td>
	</tr>
	<tr>
		<td><?php echo mosToolTip( "Put list of fields names separated with comma *,* if more than one" ); ?></td>
		<td><strong>8- validate-url (a valid URL)</strong> </td>
		<td></td>
		<td>
		<input type="text" name="params[val_validate_url]" id="params[val_validate_url]" class="inputbox" size="50" maxlength="100" value="<?php echo $paramsvalues->val_validate_url; ?>">
		</td>
	</tr>
	<tr>
		<td><?php echo mosToolTip( "Put list of fields names separated with comma *,* if more than one" ); ?></td>
		<td><strong>9- validate-date-au (a date formatted as; dd/mm/yyyy)</strong> </td>
		<td></td>
		<td>
		<input type="text" name="params[val_validate_date_au]" id="params[val_validate_date_au]" class="inputbox" size="50" maxlength="100" value="<?php echo $paramsvalues->val_validate_date_au; ?>">
		</td>
	</tr>
	<tr>
		<td><?php echo mosToolTip( "Put list of fields names separated with comma *,* if more than one" ); ?></td>
		<td><strong>10- validate-currency-dollar (a valid dollar value)</strong> </td>
		<td></td>
		<td>
		<input type="text" name="params[val_validate_currency_dollar]" id="params[val_validate_currency_dollar]" class="inputbox" size="50" maxlength="100" value="<?php echo $paramsvalues->val_validate_currency_dollar; ?>">
		</td>
	</tr>
	<tr>
		<td><?php echo mosToolTip( "(first option e.g. *Select one...* is not selected option) -- Put list of fields names separated with comma *,* if more than one" ); ?></td>
		<td><strong>11- validate-selection</strong> </td>
		<td></td>
		<td>
		<input type="text" name="params[val_validate_selection]" id="params[val_validate_selection]" class="inputbox" size="50" maxlength="100" value="<?php echo $paramsvalues->val_validate_selection; ?>">
		</td>
	</tr>
	<tr>
		<td><?php echo mosToolTip( "(At least one textbox/radio element must be selected in a group - see below*) -- Put list of fields names separated with comma *,* if more than one" ); ?></td>
		<td><strong>12- validate-one-required</strong> </td>
		<td></td>
		<td>
		<input type="text" name="params[val_validate_one_required]" id="params[val_validate_one_required]" class="inputbox" size="50" maxlength="100" value="<?php echo $paramsvalues->val_validate_one_required; ?>">
		</td>
	</tr>
	<tr><td colspan="4"><strong>Special thanks to "bouton" for referring to this feature!!</strong></td></tr>
	</table>
<?php
	$tabs->endTab();
	$tabs->endPane();
?>

		<input type="hidden" name="id" value="<?php echo $row->id; ?>" />
		<input type="hidden" name="option" value="<?php echo $option; ?>" />
		<input type="hidden" name="task" value="" />
	</form>
  <?php
	}

	function maketableChronoContact( $row, $option, $html_message  ) {
   global $mosConfig_live_site;
		mosMakeHtmlSafe( $row, ENT_QUOTES );
		?>
		<?php echo "<script  type=\"text/javascript\" "
    . "src=\"$mosConfig_live_site/includes/js/overlib_mini.js\"></script>"; ?>

	  <form action="index2.php" method="post" name="adminForm">
	  <?php if($html_message == '</table>'){ echo "Not enough form fields to create table";}
	  else if($html_message == 'exists') { echo "A table has already been created for this form";}
	  else { ?>
		<?php echo $html_message; ?>
		<input type="submit" name="submit2" value="Create Table">
		<?php } ?>
	  <input type="hidden" name="option" value="<?php echo $option; ?>" />
	  <input type="hidden" name="formid" value="<?php echo $row->id; ?>" />
	  <input type="hidden" name="task" value="finalizetable" />
	  <input type="hidden" name="boxchecked" value="0" />
	  <input type="hidden" name="hidemainmenu" value="0">
	  </form>
	<?php
		}

	function settings( $option, $params, $id ) {
		global $mosConfig_live_site;
		?>
		<script type="text/javascript" src="<?php echo $mosConfig_live_site;?>/includes/js/overlib_mini.js"></script>
		<div id="overDiv" style="position:absolute; visibility:hidden; z-index:10000;"></div>
		<form action="index2.php" method="post" name="adminForm">
		<table style="width:75%;" class="adminheading">
		<tr>
			<th class="config">
			ChronoForms Global Settings
			</th>
		</tr>
		</table>

		<table style="width:75%;" class="adminform">
		<tr>
			<th>
			Parameters
			</th>
		</tr>
		<tr>
			<td>
			<?php
			echo $params->render();
			?>
			</td>
		</tr>
		</table>

		<input type="hidden" name="id" value="<?php echo $id; ?>" />
		<input type="hidden" name="option" value="<?php echo $option ?>" />
		<input type="hidden" name="task" value="" />
		</form>

		<?php

		}

function showdataChronoContact( $rows, $pageNav, $option, $formid ) {
    // HTML starts here, combined with short PHP commands for table creation
    global $my, $database;
    mosCommonHTML::loadOverlib();
	$database->setQuery( "SELECT * FROM #__chrono_contact WHERE id='".$formid."'" );
	$formdata = $database->loadObjectList();
	$paramsvalues = mosParseParams( $formdata[0]->paramsall );
	$dvfields = $paramsvalues->dvfields;
	$dvlist = array();
	if ( !empty($dvfields) ) {
	  $dvlist = explode(",", $dvfields);
	}
    ?>
    <form action="index2.php" method="post" name="adminForm">
    <table class="adminheading">
    <tr>
	  <th><img src="/joomla100/images/joomla_logo_black.png"  align="middle" />Chrono Forms - Stored Data Manager</th>
    </tr>
	</table>
    <table class="adminlist">
	<tr>
	  <th width="20px" class='title'>#</th>
	  <th width="20px" class='title'><input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($rows); ?>);" /></th>

	  <th class='title' width="20%">Record id</th>
	  <th class='title' width="20%" align="center">date - time</th>
	  <?php foreach($dvlist as $dvitem){ ?>
	  	<th class='title' width="20%" align="center"><?php echo $dvitem; ?></th>
	  <?php } ?>
	</tr>

    <?php
	$k = 0;
    for ($i=0, $n=count($rows); $i < $n; $i++) {
	  $row = $rows[$i];

	  /*$link	= 'index2.php?option=com_chronocontact&task=editA&hidemainmenu=1&id='. $row->id;
	  $img  = $row->published ? 'tick.png' : 'publish_x.png';
	  $task = $row->published ? 'unpublish' : 'publish';
      $alt = $row->published ? 'Published' : 'Unpublished';*/

	  //$checked 	= mosCommonHTML::CheckedOutProcessing( $row, $i );


	  ?>
	  <tr class="<?php echo "row$k"; ?>">
		<td  width="20px" align="center"><?php echo $i+$pageNav->limitstart+1;?></td>
		<td width="20px"><input type="checkbox" id="cb<?php echo $i;?>" name="cid[]" value="<?php echo $row->cf_id; ?>_<?php echo $formid; ?>" onclick="isChecked(this.checked);" /></td>

		<td width="20%"><a href="#viewdata" onclick="return listItemTask('cb<?php echo $i;?>','viewdata')"><?php echo "Record  ".$row->cf_id; ?></a></td>

		<td width="20%" align="left"><?php echo $row->recordtime; ?></td>
		<?php foreach($dvlist as $dvitem){ ?>
	  		<td width="20%" align="left"><?php echo $row->$dvitem; ?></td>
		<?php } ?>
	  </tr>
      <?php
			$k = 1 - $k;
		}
    ?>
		<?php echo $pageNav->getListFooter(); ?>
	  </table>
	  <input type="hidden" name="option" value="<?php echo $option; ?>" />
	  <input type="hidden" name="task" value="show" />
	  <input type="hidden" name="formid[]" value="<?php echo $formid; ?>" />
	  <input type="hidden" name="cidxx[]" value="<?php echo $formid; ?>" />
	  <input type="hidden" name="boxchecked" value="0" />
	  <input type="hidden" name="hidemainmenu" value="0">
	  </form>
    <?php
	}
  function viewdataChronoContact( $row, $option, $tablename, $formid ) {
   global $mosConfig_live_site, $database;
		mosMakeHtmlSafe( $row, ENT_QUOTES );
		$tables = array( $tablename );
 		$result = $database->getTableFields( $tables );
		$table_fields = array_keys($result[$tablename]);
		?>
		<form action="index2.php" method="post" name="adminForm">

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="adminlist">
<tr><td width="50%"><strong>Field name</strong></td><td width="50%"><strong>Field Data</strong></td></tr>
<?php
$k = 0;
foreach($table_fields as $table_field){
?>
<tr class="<?php echo "row$k"; ?>"><td width="50%">
<strong><?php echo $table_field ; ?>	:</strong>  </td><td width="50%"><?php echo $row->$table_field ; ?>
</td></tr>
<?php $k = 1 - $k; } ?>
</table>
		<input type="hidden" name="option" value="<?php echo $option; ?>" />
	  <input type="hidden" name="task" value="" />
	  <input type="hidden" name="formid" value="<?php echo $formid; ?>" />
	  <input type="hidden" name="boxchecked" value="0" />
	  <input type="hidden" name="hidemainmenu" value="0">
	  </form>
		<?php
		}
		
	function restoreChronoContact( $id, $option ){
	
	?>
	<form action="index2.php" method="post" name="adminForm" enctype="multipart/form-data">

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="adminlist">
<tr><td>Choose your .cfbak form file: &nbsp;<input type="file" name="file"></td></tr>
<tr><td><input type="submit" value="submit file"></td></tr>
</table>
		<input type="hidden" name="option" value="<?php echo $option; ?>" />
	  <input type="hidden" name="task" value="restore2" />
	  <input type="hidden" name="formid" value="<?php echo $id; ?>" />
	  <input type="hidden" name="boxchecked" value="0" />
	  <input type="hidden" name="hidemainmenu" value="0">
	  </form>
	<?php
	}
}
?>
