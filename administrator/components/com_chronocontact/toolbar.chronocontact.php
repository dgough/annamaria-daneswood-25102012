<?php

/*
/**
* CHRONOFORMS version 1.0 stable
* Copyright (c) 2006 Chrono_Man, ChronoEngine.com. All rights reserved.
* Author: Chrono_Man (ChronoEngine.com)
* See readme.html.
* Visit http://www.ChronoEngine.com for regular update and information.
**/

/* ensure that this file is called from another file */
defined( '_VALID_MOS' ) or die( 'Direct access to this file is prohibited.' );
require_once( $mainframe->getPath( 'toolbar_html' ) );
switch($task) {
	case "new":
	case "edit":
		menuChronoContact::MENU_Edit();
		break;
	case "viewdata":
		menuChronoContact::MENU_Show();
		break;
	case "createtable":
	case "restore1":
		menuChronoContact::MENU_Cancel();
		break;
	case "show":
	case "deleterecord":
	case "cancelview":
		menuChronoContact::MENU_Show2();
		break;
	case 'config':
		MENUChronoContact::CONFIG_MENU();
		break;
	default:
		menuChronoContact::MENU_Default();
		break;
}
?>
