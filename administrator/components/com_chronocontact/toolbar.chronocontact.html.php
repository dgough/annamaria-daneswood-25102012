<?php
/*
/**
* CHRONOFORMS version 1.0 stable
* Copyright (c) 2006 Chrono_Man, ChronoEngine.com. All rights reserved.
* Author: Chrono_Man (ChronoEngine.com)
* See readme.html.
* Visit http://www.ChronoEngine.com for regular update and information.
**/

/* ensure that this file is called from another file */
defined( '_VALID_MOS' ) or die( 'Direct access to this file is prohibited.' );
class menuChronoContact {
	function MENU_Default() {
		mosMenuBar::startTable();
		mosMenuBar::publishList();
		mosMenuBar::unpublishList();
		mosMenuBar::divider();
		mosMenuBar::addNew();
		mosMenuBar::custom($task = 'copy', $icon = 'copy_f2.png', $iconOver = 'copy_f2.png', $alt = 'Copy form', $listSelect = true) ;
		mosMenuBar::editList();
		mosMenuBar::deleteList();
		mosMenuBar::spacer();
		mosMenuBar::spacer();
		mosMenuBar::spacer();
		mosMenuBar::spacer();
		mosMenuBar::custom($task = 'show', $icon = 'query.png', $iconOver = 'query.png', $alt = 'Show Data', $listSelect = true) ;
		mosMenuBar::custom($task = 'createtable', $icon = 'properties_f2.png', $iconOver = 'properties_f2.png', $alt = 'Create table', $listSelect = true) ;
		mosMenuBar::spacer();
		mosMenuBar::spacer();
		mosMenuBar::custom($task = 'backup', $icon = 'backup.png', $iconOver = 'backup.png', $alt = 'Backup Form', $listSelect = true) ;
		mosMenuBar::custom($task = 'restore1', $icon = 'forward_f2.png', $iconOver = 'forward_f2.png', $alt = 'Restore Form', $listSelect = false) ;
		mosMenuBar::endTable();
	}
	function MENU_Edit() {
		mosMenuBar::startTable();
		mosMenuBar::save();
		mosMenuBar::cancel();
		mosMenuBar::spacer();
		mosMenuBar::endTable();
	}
	function MENU_Show() {
		mosMenuBar::startTable();
		mosMenuBar::cancel('cancelview', "Cancel");
		mosMenuBar::endTable();
	}
	function MENU_Show2() {
		mosMenuBar::startTable();
		mosMenuBar::cancel('cancelconfig', "Cancel");
		mosMenuBar::deleteList('','deleterecord','Delete');
		mosMenuBar::spacer();
		mosMenuBar::spacer();
		mosMenuBar::custom($task = 'backexcel', $icon = 'backup.png', $iconOver = 'backup.png', $alt = 'Backup to Excel', $listSelect = false) ;
		mosMenuBar::endTable();
	}
	function CONFIG_MENU() {
		mosMenuBar::startTable();
		mosMenuBar::save('saveconfig', "Save");
		mosMenuBar::spacer();
		mosMenuBar::cancel('cancelconfig', "Cancel");
		mosMenuBar::endTable();
	}
	function MENU_Cancel() {
		mosMenuBar::startTable();
		mosMenuBar::cancel();
		mosMenuBar::spacer();
		mosMenuBar::endTable();
	}
}
?>

