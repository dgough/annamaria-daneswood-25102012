<?php
/**
 * Joom!Fish - Multi Lingual extention and translation manager for Joomla!
 * Copyright (C) 2003-2007 Think Network GmbH, Munich
 *
 * All rights reserved.  The Joom!Fish project is a set of extentions for
 * the content management system Joomla!. It enables Joomla!
 * to manage multi lingual sites especially in all dynamic information
 * which are stored in the database.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * The "GNU General Public License" (GPL) is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * -----------------------------------------------------------------------------
 * $Id: joomfish.class.php 541 2007-06-21 15:43:05Z akede $
 *
*/

// Set flag that this is a parent file
define( "_VALID_MOS", 1 );

require_once( '../../includes/auth.php' );

/** required standard extentions **/
require_once( mosMainFrame::getBasePath() .'/components/com_joomfish/includes/defines.php' );
require_once( JOOMFISH_LIBPATH .DS. 'joomla' .DS. 'language.php' );

// limit access to functionality
$option = strval( mosGetParam( $_SESSION, 'option', '' ) );
$task 	= strval( mosGetParam( $_SESSION, 'task', '' ) );

switch ($option) {
	case 'com_joomfish':
		if ( $task != 'edit' ) {
			echo _NOT_AUTH;
			return;
		}
		break;

	default:
		echo _NOT_AUTH;
		return;
		break;
}

$css = mosGetParam( $_REQUEST, 't', '' );

// css file handling
// check to see if template exists
if ( $css != '' && is_dir($mosConfig_absolute_path .'/templates/'. $css .'/css/template_css.css' )) {
	$css 	= $css;
} else if ( $css == '' ) {
	$css 	= 'rhuk_solarflare_ii';
}

include_once ( $mosConfig_absolute_path . '/language/' . $mosConfig_lang . '.php' );
$iso = split( '=', _ISO );
// xml prolog
echo '<?xml version="1.0" encoding="'. $iso[1] .'"?' .'>';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<base href="<?php echo($mosConfig_live_site); ?>/" />
<head>
<title>Content Translation Preview</title>
<script language="Javascript" type="text/javascript">
window.resizeTo(800,600);
</script>
<link rel="stylesheet" href="templates/<?php echo $css;?>/css/template_css.css" type="text/css" />
	<script>

		var form = window.opener.document.adminForm	;
		var title = form.refField_title.value;
		var title_orig = form.origText_title.value;

		var alltext = form.refField_introtext.value;
		var alltext_orig = window.opener.document.getElementById("original_value_introtext").innerHTML;
		if (form.refField_fulltext) {
			alltext += form.refField_fulltext.value;
			alltext_orig += window.opener.document.getElementById("original_value_fulltext").innerHTML;
		}

		// can't do the images
		images = window.opener.document.getElementById("original_value_images");
		images = images.innerHTML;
		imagelist = images.split( '\n' );
		var temp = new Array();
		img=0
		for (var i=0, n=imagelist.length; i < n; i++) {
			value = imagelist[i];
			// trim leading spaces
			value = value.replace(/^\s*/g,'')
			if (value){
				parts = value.split( '|' );
				temp[img] = '<img src="<?php echo $mosConfig_live_site;?>/images/stories/' + parts[0] + '" align="' + parts[1] + '" border="' + parts[3] + '" alt="' + parts[2] + '" hspace="6" />';
				img++;
			}
		}

		var temp2 = alltext_orig.split( '{mosimage}' );

		var alltext_orig = temp2[0];
		for (var i=0, n=temp2.length-1; i < n; i++) {
			alltext_orig += temp[i] + temp2[i+1];
		}

		
		if (form.refField_images && form.refField_images.value.legth>0){
			images = form.refField_images.value;
		}
		else {
			images = window.opener.document.getElementById("original_value_images");
			images = images.innerHTML;
		}
		imagelist = images.split( '\n' );
		var temp = new Array();
		img=0
		for (var i=0, n=imagelist.length; i < n; i++) {
			value = imagelist[i];
			// trim leading spaces
			value = value.replace(/^\s*/g,'')
			if (value){
				parts = value.split( '|' );
				temp[img] = '<img src="<?php echo $mosConfig_live_site;?>/images/stories/' + parts[0] + '" align="' + parts[1] + '" border="' + parts[3] + '" alt="' + parts[2] + '" hspace="6" />';
				img++;
			}
		}

		var temp2 = alltext.split( '{mosimage}' );

		var alltext = temp2[0];
		for (var i=0, n=temp2.length-1; i < n; i++) {
			alltext += temp[i] + temp2[i+1];
		}
		
	</script>
<meta http-equiv="Content-Type" content="text/html; <?php echo _ISO; ?>" />
</head>
<body style="background-color:#FFFFFF;margin:0px;">
<table align="center" width="100%" cellspacing="2" cellpadding="2" border="0">
	<tr>
		<th ><h2><?php echo JText::_("Original");?></h2></th>
		<th ><h2><?php echo JText::_("Translation");?></h2></th>
	</tr>
	<tr>
		<td class="contentheading" style="width:50%!important"><script>document.write(title_orig);</script></td>
		<td class="contentheading" ><script>document.write(title);</script></td>
	</tr>
	<tr>
		<script>document.write("<td valign=\"top\" >" + alltext_orig + "</td>");</script>
		<script>document.write("<td valign=\"top\" >" + alltext + "</td>");</script>
	</tr>
	<tr>
		<td align="right"><a href="#" onClick="window.close()"><?php echo JText::_("Close");?></a></td>
		<td align="left"><a href="javascript:;" onClick="window.print(); return false"><?php echo JText::_("Print");?></a></td>
	</tr>
</table>
</body>
</html>