<?php
/**
 * Joom!Fish - Multi Lingual extention and translation manager for Joomla!
 * Copyright (C) 2003-2007 Think Network GmbH, Munich
 *
 * All rights reserved.  The Joom!Fish project is a set of extentions for
 * the content management system Joomla!. It enables Joomla!
 * to manage multi lingual sites especially in all dynamic information
 * which are stored in the database.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * The "GNU General Public License" (GPL) is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * -----------------------------------------------------------------------------
 * $Id: toolbar.joomfish.html.php 567 2007-07-17 05:53:43Z akede $
 *
*/

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

class MENU_joomfish {

	function CONFIG_MENU() {
		mosMenuBar::startTable();
		mosMenuBar::save();
		mosMenuBar::spacer();
		mosMenuBar::apply();
		mosMenuBar::spacer();
		mosMenuBar::cancel();
		mosMenuBar::spacer();
		mosMenuBar::endTable();
	}

	function CONFIGLANG_MENU() {

		mosMenuBar::startTable();
		mosMenuBar::deleteList();
		mosMenuBar::spacer();
		mosMenuBar::save();
		mosMenuBar::spacer();
		mosMenuBar::apply();
		mosMenuBar::spacer();
		mosMenuBar::cancel();
		mosMenuBar::spacer();
		mosMenuBar::endTable();
	}

	function EDIT_MENU() {
	    mosMenuBar::startTable();
	    mosMenuBar::editList();
	    mosMenuBar::spacer();
		mosMenuBar::custom( 'cpanel', '../components/com_joomfish/images/joomfish_bar.png', '../components/com_joomfish/images/joomfish_bar_f2.png', JText::_('CPanel'), false );
	    mosMenuBar::endTable();
	}

  	function EDIT_ELEMENTS_MENU() {
	    mosMenuBar::startTable();
	    mosMenuBar::editList('edit',JTEXT::_('VIEW'));
	    mosMenuBar::spacer();
	    mosMenuBar::custom( 'install', 'upload.png', 'upload_f2.png', JText::_('INSTALL'), false);
	    mosMenuBar::spacer();
		mosMenuBar::custom( 'cpanel', '../components/com_joomfish/images/joomfish_bar.png', '../components/com_joomfish/images/joomfish_bar_f2.png', JText::_('CPanel'), false );
	    mosMenuBar::endTable();
  	}

  	function INSTALL_ELEMENTS_MENU() {
	    mosMenuBar::startTable();
	    mosMenuBar::deleteList();
	    mosMenuBar::spacer();
	    mosMenuBar::custom( 'back', 'back.png', 'back_f2.png', JText::_('Back'), false);
	    mosMenuBar::spacer();
		mosMenuBar::custom( 'cpanel', '../components/com_joomfish/images/joomfish_bar.png', '../components/com_joomfish/images/joomfish_bar_f2.png', JText::_('CPanel'), false );
	    mosMenuBar::endTable();
  	}

	function TRANSLATION_OVERVIEW_MENU() {
		mosMenuBar::startTable();
		mosMenuBar::publishList();
		mosMenuBar::spacer();
		mosMenuBar::unpublishList();
//		mosMenuBar::divider();
//		mosMenuBar::custom('setdefaulttext', 'new.png', 'new_f2.png', JText::_('Set default text'), false);
//		mosMenuBar::custom('setcomplete', 'copy.png', 'copy_f2.png', JText::_('Set complete'), false);
		mosMenuBar::spacer();
    	mosMenuBar::editListX();
		mosMenuBar::spacer();
		mosMenuBar::deleteList();
		mosMenuBar::spacer();
		mosMenuBar::custom( 'cpanel', '../components/com_joomfish/images/joomfish_bar.png', '../components/com_joomfish/images/joomfish_bar_f2.png', JText::_('CPanel'), false );
		mosMenuBar::endTable();
	}

	function TRANSLATE_MENU() {
		mosMenuBar::startTable();
		if (mosGetParam($_REQUEST,"catid","")=="content"){
			mosMenuBar::preview('../components/com_joomfish/previewtranslation',true);
		}

		//mosMenuBar::divider();
		mosMenuBar::save();
		mosMenuBar::spacer();
		mosMenuBar::apply();
		mosMenuBar::spacer();
		mosMenuBar::cancel();
		mosMenuBar::spacer();
		mosMenuBar::endTable();
	}

	function ORPHAN_DETAIL_MENU() {
		mosMenuBar::startTable();
		mosMenuBar::back();
		mosMenuBar::spacer();
		mosMenuBar::custom( 'remove', 'delete.png', 'delete_f2.png', JText::_('Delete'), false );
		mosMenuBar::spacer();
		mosMenuBar::custom( 'cpanel', '../components/com_joomfish/images/joomfish_bar.png', '../components/com_joomfish/images/joomfish_bar_f2.png', JText::_('CPanel'), false );
		mosMenuBar::endTable();

	}

	function ORPHAN_MENU() {
		mosMenuBar::startTable();
		mosMenuBar::deleteList();
		mosMenuBar::spacer();
		mosMenuBar::custom( 'cpanel', '../components/com_joomfish/images/joomfish_bar.png', '../components/com_joomfish/images/joomfish_bar_f2.png', JText::_('CPanel'), false );
		mosMenuBar::endTable();

	}

	function UPGRADE_MENU() {
		mosMenuBar::startTable();
		mosMenuBar::spacer();
		mosMenuBar::custom( 'upgrade', 'upload.png', 'upload_f2.png', JText::_('Upgrade'), false );
		mosMenuBar::spacer();
		mosMenuBar::custom( 'cpanel', '../components/com_joomfish/images/joomfish_bar.png', '../components/com_joomfish/images/joomfish_bar_f2.png', JText::_('CPanel'), false );
		mosMenuBar::endTable();
	}

	function DEFAULT_MENU() {
		mosMenuBar::startTable();
		mosMenuBar::back();
		mosMenuBar::spacer();
		mosMenuBar::custom( 'cpanel', '../components/com_joomfish/images/joomfish_bar.png', '../components/com_joomfish/images/joomfish_bar_f2.png', JText::_('CPanel'), false );
		mosMenuBar::endTable();
	}
}
?>
