<?php
/**
 * Joom!Fish - Multi Lingual extention and translation manager for Joomla!
 * Copyright (C) 2003-2007 Think Network GmbH, Munich
 * 
 * All rights reserved.  The Joom!Fish project is a set of extentions for 
 * the content management system Joomla!. It enables Joomla! 
 * to manage multi lingual sites especially in all dynamic information 
 * which are stored in the database.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * The "GNU General Public License" (GPL) is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * -----------------------------------------------------------------------------
 * $Id: uninstall.joomfish.php 613 2007-08-30 12:04:14Z akede $
 *
*/

// Don't allow direct linking
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

function com_uninstall()
{
	global $database;
	
	// uninstall modules
	$database->setQuery( "DELETE FROM `#__modules` WHERE `module`= 'mod_jflanguageselection';");
	$database->query();
	@unlink( mosMainFrame::getBasePath()."/modules/mod_jflanguageselection.css" );
	@unlink( mosMainFrame::getBasePath()."/modules/mod_jflanguageselection.php" );
	@unlink( mosMainFrame::getBasePath()."/modules/mod_jflanguageselection.xml" );
	
	$database->setQuery( "DELETE FROM `#__modules` WHERE `module`= 'mod_translate';");
	$database->query();
	@unlink( mosMainFrame::getBasePath()."/administrator/modules/mod_translate_no.png" );
	@unlink( mosMainFrame::getBasePath()."/administrator/modules/mod_translate.php" );
	@unlink( mosMainFrame::getBasePath()."/administrator/modules/mod_translate.png" );
	@unlink( mosMainFrame::getBasePath()."/administrator/modules/mod_translate.xml" );
	
    
    // uninstall bots
	$database->setQuery( "DELETE FROM `#__mambots` WHERE `element`like 'jf%.searchbot';");
	$database->query();
    @unlink( mosMainFrame::getBasePath()."/mambots/search/jfcategories.searchbot.php" );
    @unlink( mosMainFrame::getBasePath()."/mambots/search/jfcategories.searchbot.xml" );
    @unlink( mosMainFrame::getBasePath()."/mambots/search/jfcontact.searchbot.php" );
    @unlink( mosMainFrame::getBasePath()."/mambots/search/jfcontact.searchbot.xml" );
    @unlink( mosMainFrame::getBasePath()."/mambots/search/jfcontent.searchbot.php" );
    @unlink( mosMainFrame::getBasePath()."/mambots/search/jfcontent.searchbot.xml" );
    @unlink( mosMainFrame::getBasePath()."/mambots/search/jfcontact.searchbot.php" );
    @unlink( mosMainFrame::getBasePath()."/mambots/search/jfcontact.searchbot.xml" );
    @unlink( mosMainFrame::getBasePath()."/mambots/search/jfnewsfeeds.searchbot.php" );
    @unlink( mosMainFrame::getBasePath()."/mambots/search/jfnewsfeeds.searchbot.xml" );
    @unlink( mosMainFrame::getBasePath()."/mambots/search/jfsections.searchbot.php" );
    @unlink( mosMainFrame::getBasePath()."/mambots/search/jfsections.searchbot.xml" );
    @unlink( mosMainFrame::getBasePath()."/mambots/search/jfweblinks.searchbot.php" );
    @unlink( mosMainFrame::getBasePath()."/mambots/search/jfweblinks.searchbot.xml" );
    
    
    $database->setQuery( "DELETE FROM `#__mambots` WHERE `element`= 'jfdatabase.systembot';");
	$database->query();
    @unlink( mosMainFrame::getBasePath()."/mambots/system/jfdatabase.systembot.php" );
    @unlink( mosMainFrame::getBasePath()."/mambots/system/jfdatabase.systembot.xml" );

    $database->setQuery( "DELETE FROM `#__mambots` WHERE `element`= 'jfalternative';");
	$database->query();
    @unlink( mosMainFrame::getBasePath()."/mambots/content/jfalternative.php" );
    @unlink( mosMainFrame::getBasePath()."/mambots/content/jfalternative.xml" );

	echo "<strong>Joom!Fish was uninstalled successfully.</strong></p>";
	echo "All Bot's are uninstalled";
}

?>