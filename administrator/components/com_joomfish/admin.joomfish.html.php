<?php
/**
 * Joom!Fish - Multi Lingual extention and translation manager for Joomla!
 * Copyright (C) 2003-2007 Think Network GmbH, Munich
 *
 * All rights reserved.  The Joom!Fish project is a set of extentions for
 * the content management system Joomla!. It enables Joomla!
 * to manage multi lingual sites especially in all dynamic information
 * which are stored in the database.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * The "GNU General Public License" (GPL) is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * -----------------------------------------------------------------------------
 * $Id: admin.joomfish.html.php 749 2007-11-11 01:02:32Z apostolov $
 *
*/

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );
$adminPath = dirname( __FILE__);

define('JFCPANEL_NEWS', 4);
define('JFCPANEL_UNPUBLISHED', 2);
define('JFCPANEL_STATE', 1);

/**
 * Method to translate with a pre_reg function call
 *
 * @param array $matches
 * @return string
 */
function jfTranslate($matches){
	$translation = JText::_($matches[1]);
	return $translation;
}

class HTML_joomfish {
	/**
	 * Creates the CPanel for the Joom!Fish component
	 * @param array	status informations for the panel
	 */
	function showCPanel( $panelStates, $contentInfo ) {
		global $act, $task, $option, $mosConfig_live_site, $mosConfig_absolute_path, $joomFishManager;
		HTML_joomfish::_header()
		?>
		<tr>
			<td width="55%" valign="top">
				<div id="cpanel">
				<?php
				$link = 'index2.php?option=com_joomfish&amp;task=overview&amp;act=translate';
				HTML_joomfish::_quickiconButton( $link, 'addedit.png', JText::_('Translation') );

				$link = 'index2.php?option=com_joomfish&amp;task=overview&amp;act=orphans';
				HTML_joomfish::_quickiconButton( $link, 'query.png', JText::_('Orphans') );

				$link = 'index2.php?option=com_joomfish&amp;task=overview&amp;act=manage';
				HTML_joomfish::_quickiconButton( $link, 'menu.png', JText::_('Manage Translations') );

				$link = 'index2.php?option=com_joomfish&amp;task=show&amp;act=help';
				HTML_joomfish::_quickiconButton( $link, 'support.png', JText::_('HELP AND HOWTO') );

				echo '<div style="clear: both;" />';

				$link = 'index2.php?option=com_joomfish&amp;task=show&amp;act=config_component&amp;hidemainmenu=1';
				HTML_joomfish::_quickiconButton( $link, 'config.png', JText::_('Configuration') );

				$link = 'index2.php?option=com_joomfish&amp;task=show&amp;act=config_language&amp;hidemainmenu=1';
				HTML_joomfish::_quickiconButton( $link, 'langmanager.png', JText::_('Language Configuration') );

				$link = 'index2.php?option=com_joomfish&amp;task=show&amp;act=config_elements';
				HTML_joomfish::_quickiconButton( $link, 'cpanel.png', JText::_('Content elements') );

				$link = 'index2.php?option=com_joomfish&amp;task=show&amp;act=credits';
				HTML_joomfish::_quickiconButton( $link, 'credits.png', JText::_('About and Credits') );

				?>
			</div>
			</td>
			<td width="45%" valign="top">
				<div style="width: 100%;">
					<?php

					// Prepare formating for the output of the state
					$stateRows = array();
					$alertState = false;
					$alertContent = false;
					foreach ($panelStates as $key => $state ) {
						if( ereg( '_state', $key ) ) {
							if( !$state ) {
								$alertState = true;
							}
							$stateHeader = substr($key, 0, strpos($key, '_'));
							$stateGroups[$stateHeader] = $panelStates[$stateHeader];
						} elseif ( ereg( 'mbf', $key )) {
							if( $state == 2 ) {
								$panelStates [ $key . '_href' ] = "";
								$panelStates[ $key . '_text' ] = JText::_('INSTAL_UPGRADED');
							} elseif($state == 1) {
								$panelStates[ $key . '_href' ] = "$mosConfig_live_site/administrator/index2.php?option=com_joomfish&amp;task=show&amp;act=upgrade_install&amp;hidemainmenu=1";
								$panelStates[ $key . '_text' ] = JText::_('INSTAL_NOT_UPGRADED');
								$alertState = true;
							}
						}

					}

					if( array_key_exists('unpublished', $contentInfo) && is_array($contentInfo['unpublished']) ) {
						$alertContent = true;
					}

					$publishedTabs = $joomFishManager->getCfg('showCPanels');

					// Tabs
					$tabs = new mosTabs(1);
					$tabs->startPane( 'modules-jfcpane' );

					$tabs->startTab( JText::_('Information'), 'jfinformation' );
					$sysInfo =  $panelStates['system'];
					?>
					<table class="adminlist" border="1">
						<tr><th><JText>JOOMFISH_TITLE</JText></th></tr>
						<tr><td><?php JText::printf('INFORMATION_TEXT', $sysInfo['translations']);?></tr>
						<tr><td><?php echo JText::_('INFORMATION_SHOP');?></td></tr>
						<tr><td><p><?php JText::printf ('INFORMATION_DONATE', intval($sysInfo['translations']) *0.10);?></p>
						<div align="center">
							<iframe src="<?php echo $mosConfig_live_site;?>/administrator/components/com_joomfish/donate.php?translations=<?php echo intval($sysInfo['translations']);?>" frameborder="0" style='border:0px;height:80px;width:180px;overflow:hidden;'></iframe>
						</div>
						</td></tr>
					</table>
					<?php
					$tabs->endTab();

					if( $publishedTabs & JFCPANEL_NEWS ) {
						$tabs->startTab( JText::_('News'), 'jfnews' );
						require_once($mosConfig_absolute_path . "/includes/domit/xml_domit_rss_lite.php");

					    $cacheDir = $mosConfig_absolute_path . "/cache/";
					    $LitePath = $mosConfig_absolute_path . "/includes/Cache/Lite.php";
					    @$rss = &new xml_domit_rss_document_lite();
					    $rss->useCacheLite(true, $LitePath, $cacheDir, 86400 );
					    $rss->loadRSS('http://www.joomfish.net/rss-feed/extension-news/');
						?>
						<table class="adminlist">
							<?php
							if( $rss->getChannelCount() == 0 ) {
								?>
							<tr>
								<th colspan="3"><JText>Joom!Fish news</JText></th>
							</tr>
							<tr>
								<td colspan="3"><JText>NEWS_INTRODUCTION</JText></td>
							</tr>
							<?php
							} else {
								$k=0;
								$channel = &$rss->getChannel(0);
								?>
							<tr>
								<th colspan="3"><a href="<?php echo $channel->getLink() ?>" target="_blank"><?php echo $channel->getTitle()?></a></th>
							</tr>
							<tr>
								<td colspan="3"><JText>NEWS_INTRODUCTION</JText></td>
							</tr>
								<?php
					            $numItems = $channel->getItemCount();
					            if( !$numItems ) {
					            	?>
					                <tr><th><JText>No news items found</JText></th></tr><?php
					            } else {
					            	$numItems = ($numItems > 5) ? 5 : $numItems;
					                for( $j = 0; $j < $numItems; $j++ ) {
					                    $item = & $channel->getItem($j);?>
					                	<tr><td class="row<?php echo $k;?>">
					                        <a href="<?php echo $item->getLink()?>" target="_blank"><?php echo $item->getTitle()?></a><?php
					                        if( $description = HTML_joomfish::_truncateString($item->getDescription(), 500) ) {?>
					                            <br /><?php echo $description?><?php
					                        }?>
					                    </td></tr><?php
					                }
					            }
								$k = 1 - $k;
							}
							?>
						</table>
						<?php
						$tabs->endTab();
					}

					if( $publishedTabs & JFCPANEL_UNPUBLISHED || $alertContent ) {
						$tabTitle = $alertContent==true ? '<span style="color: red; font-weight: bold;">' .JText::_('TITLE_UNPUBLISHED'). '</span>' : JText::_('TITLE_UNPUBLISHED');
						$tabs->startTab( $tabTitle, 'jfcontentstate' );
						?>
						<table class="adminlist" border="1">
							<tr>
								<th><JText>UNPUBLISHED CONTENT ELEMENTS</JText></th>
								<th style="text-align: center;"><JText>Language</JText></th>
								<th style="text-align: center;"><JText>Publish</JText></th>
							</tr>
							<?php
							$k=0;
							if( $alertContent ) {
								$curReftable = '';
								foreach ($contentInfo['unpublished'] as $ceInfo ) {
									$contentElement = $joomFishManager->getContentElement( $ceInfo['catid'] );

									// Trap for content elements that may have been removed
									if (is_null($contentElement)){
										$name = "<span style='font-style:italic'>".JText::sprintf("CONTENT_ELEMENT_MISSING",$ceInfo["reference_table"])."</span>";
									}
									else {
										$name = $contentElement->Name;
									}
									if ($ceInfo["reference_table"] != $curReftable){
										$curReftable = $ceInfo["reference_table"];
										$k=0;
										?>
							<tr><td colspan="3"><strong><?php echo $name;?></strong></td></tr>
										<?php
									}

									$contentObject = new ContentObject( $ceInfo['language_id'], $contentElement );
									$contentObject->loadFromContentID($ceInfo['reference_id']);
									$link = 'index2.php?option=com_joomfish&amp;task=edit&amp;&amp;act=translate&amp;&amp;catid=' .$ceInfo['catid']. '&cid[]=0|' .$ceInfo['reference_id'].'|'.$ceInfo['language_id'];
									$hrefEdit = "<a href='".$link."'>".$contentObject->title. "</a>";

									$link = 'index2.php?option=com_joomfish&amp;task=publish&amp;&amp;act=translate&amp;&amp;catid=' .$ceInfo['catid']. '&cid[]=0|' .$ceInfo['reference_id'].'|'.$ceInfo['language_id'];
									$hrefPublish = '<a href="'.$link.'"><img src="images/publish_x.png" width="12" height="12" border="0" alt="" /></a>';
									?>
							<tr class="row<?php echo $k;?>">
								<td align="left"><?php echo $hrefEdit;?></td>
								<td style="text-align: center;"><?php echo $ceInfo['language'];?></td>
	      						<td style="text-align: center;"><?php echo $hrefPublish;?></td>
							</tr>
									<?php
									$k = 1 - $k;
								}
							} else {
									?>
							<tr class="row0">
								<td colspan="3"><JText>No unpublished translations found</JText></td>
							</tr>
									<?php
							}
							?>
						</table>
						<?php
						$tabs->endTab();
					}

					if( $publishedTabs & JFCPANEL_STATE || $alertState ) {
						$tabTitle = $alertState==true ? '<span style="color: red; font-weight: bold;">' .JText::_('System State'). '</span>' : JText::_('System State');
						$tabs->startTab( $tabTitle, 'jfsystemstate' );
						?>
						<table class="adminlist">
							<?php
							foreach ($stateGroups as $key=>$stateRow) {
								?>
							<tr>
								<th colspan="3"><JText><?php echo $key. ' state';?></JText></th>
							</tr>
								<?php
								$k=0;
								foreach ($stateRow as $row) {
									?>
							<tr class="row<?php echo $k;?>">
								<td><?php
									if( $row->link != '' ) {
										$row->description = '<a href="' .$row->link. '">' .$row->description. '</a>';
									}
									echo $row->description;
								?></td>
								<td colspan="2"><?php echo $row->resultText;?></td>
									<?php
									$k = 1 - $k;
								}
							}
							?>

							<?php if( key_exists( 'mbfInstall', $panelStates ) && $panelStates['mbfInstall'] != 0 ) { ?>
							<tr>
								<th colspan="3"><JText>Translation State</JText></th>
							</tr>
							<tr class="row">
								<td><JText>Mambelfish Install</JText></td>
								<td><?php echo $panelStates['mbfInstall_text' ];?></td>
								<td><?php if( $panelStates[ 'mbfInstall_href' ] != "" ) : ?>
								<a href="<?php echo $panelStates[ 'mbfInstall_href' ];?>"><img src="<?php echo $mosConfig_live_site;?>/administrator/images/dbrestore.png" border="0" alt="<?php echo JText::_('UPGRADE_INSTALLATION');?>" /></a>
								<?php endif; ?>
								</td>
							</tr>
							<?php } ?>
						</table>
						<?php
						$tabs->endTab();
					}

					$tabs->endPane();
					?>
				</div>
			</td>
		</tr>
		<?php
		HTML_joomfish::_JoomlaFooter( '', $act, $option );
	}
	/**
	 * Method to show the component configuration
	 *
	 * @param object $joomfishManager
	 * @param array $languageList
	 */
	function showComponentConfiguration( $joomfishManager, $languageList) {
		global $act, $task, $option, $database, $mosConfig_lang, $mosConfig_live_site;
		HTML_joomfish::_JoomlaHeader( JText::_('PREF_TITLE'), 'config');
		?>
	    <tr>
	      <th colspan="3"><JText>Component Configuration</JText></th>
	    </tr>
	    <tr align="center" valign="middle">
	      <td width="30%" align="left" valign="top"><strong><JText>Component Admin Interface Language</JText></strong></td>
	      <td width="20%" align="left" valign="top">
	      <?php
	      $frmField = mosHTML::selectList( $languageList, 'frmComponentAdminLanguage', 'class="inputbox" size="1"', 'value', 'text', $joomfishManager->getCfg( 'componentAdminLang' ) );
	      echo $frmField;
	      ?>
			  </td>
		  <td align="left" valign="top">
		  	<?php $tip = JText::_('COMPONENT_CONFIGURATION_HELP');
		  	echo mosToolTip( $tip );
			?>
		  </td>
	    </tr>
	    <tr>
	      <th colspan="3"><JText>SHOWIF</JText></th>
	    </tr>
	    <tr align="center" valign="middle">
	      <td width="30%" align="left" valign="top"><strong><JText>No Translation available</JText></strong></td>
	      <td width="20%" align="left" valign="top">
	      <?php
	      $options[] = mosHTML::makeOption( '0', JText::_('Original content') );
	      $options[] = mosHTML::makeOption( '1', JText::_('Placeholder') );
	      $options[] = mosHTML::makeOption( '2', JText::_('Original with info') ); // activated mic
	      $options[] = mosHTML::makeOption( '3', JText::_('Original with alt') );
	      $frmField = mosHTML::selectList( $options, 'frmTranslationMode', 'class="inputbox" size="1"', 'value', 'text', $joomfishManager->getCfg( 'noTranslation' ) );
	      echo $frmField;
	      ?>
			  </td>
		  <td align="left" valign="top">
		  	<?php $tip =  JText::_('NOTRANSLATION_HELP');
		  	echo mosToolTip( $tip );
			?>
		  </td>
	    </tr>
	    <tr align="center" valign="middle">
	      <td width="30%" align="left" valign="top"><strong><JText>Placeholder</JText></strong></td>
	      <td width="20%" align="left" valign="top">
	      <input type="text" name="frmTranslationDefaultText" size="75" value="<?php echo $joomfishManager->getCfg('defaultText');?>" />
		  </td>
		  <td align="left">
		  	<?php
		  	$tip = JText::_('TRANS_DEFAULT_HELP');
		  	echo mosToolTip( $tip );
			?>
		  </td>
	    </tr>


	    <tr>
	      <th colspan="3"><JText>Managing translations</JText></th>
	    </tr>
		<tr>
	      <td width="20%" align="left" valign="top"><strong><JText>Store original version as</JText></strong></td>
	      <td width="20%" align="left" valign="top">
	      <?php
	      $options = array();
	      $options[] = mosHTML::makeOption( 'md5', JText::_( 'MD5 key only' ) );
	      $options[] = mosHTML::makeOption( 'both', JText::_('MD5 key and clear text') );
	      $frmField = mosHTML::selectList( $options, 'frmStorageOfOriginal', 'class="inputbox" size="1"', 'value', 'text', $joomfishManager->getCfg( 'storageOfOriginal' ) );
	      echo $frmField;
	      ?>
		 </td>
		  <td align="left" valign="top">	<?php
		  $tip = JText::_('MNGORIGINAL_HELP');
		  echo mosToolTip( $tip );
			?>
		  </td>
		</tr>

		<tr>
	      <th colspan="3"><JText>Access Preferences</JText></th>
	    </tr>
		<tr>
	      <td width="20%" align="left" valign="top"><strong><JText>Frontend publish</JText></strong></td>
	      <td width="20%" align="left" valign="top">
	      <?php
	      $options = array();
	      $options[] = mosHTML::makeOption( '0', JText::_('Noone'));
	      $options[] = mosHTML::makeOption( '1', JText::_('Publishers') );
	      $frmField = mosHTML::selectList( $options, 'frmPublish', 'class="inputbox" size="1"', 'value', 'text', $joomfishManager->getCfg( 'frontEndPublish' ) );
	      echo $frmField;
	      ?>
		 </td>
		  <td align="left" valign="top">	<?php
		  $tip = JText::_('FEPUBLISH_HELP');
		  echo mosToolTip( $tip );
			?>
		</tr>

		<tr>
	      <th colspan="3"><JText>Inactive Language Preview Preference</JText></th>
	    </tr>
		<tr>
	      <td width="20%" align="left" valign="top"><strong><JText>Enable inactive languages for managers and above</JText></strong></td>
	      <td width="20%" align="left" valign="top">
	      <?php
	      $options = array();
	      $options[] = mosHTML::makeOption( '0', JText::_('No'));
	      $options[] = mosHTML::makeOption( '1', JText::_('Yes') );
	      $frmField = mosHTML::selectList( $options, 'frmFrontEndPreview', 'class="inputbox" size="1"', 'value', 'text', $joomfishManager->getCfg( 'frontEndPreview' ) );
	      echo $frmField;
	      ?>
		 </td>
		  <td align="left" valign="top">	<?php
		  $tip = JText::_('PREVIEW_HELP');
		  echo mosToolTip( $tip );
			?>
		</tr>

		<tr>
	      <th colspan="3"><JText>Control Panel Moduls</JText></th>
	    </tr>
		<tr>
	      <td width="20%" align="left" valign="top"><strong><JText>Which modules are active on the control panel</JText></strong></td>
	      <td width="20%" align="left" valign="top">
	      <?php
	      $cpanelModules = $joomfishManager->getCfg( 'showCPanels' );
	      ?>
	      <select name="frmShowCPanels[]" class="inputbox" size="3" multiple>
			<option value="<?php echo JFCPANEL_NEWS;?>" <?php echo ($cpanelModules & JFCPANEL_NEWS) ? 'SELECTED' : '';?>><JText>News</JText></option>
			<option value="<?php echo JFCPANEL_UNPUBLISHED;?>" <?php echo ($cpanelModules & JFCPANEL_UNPUBLISHED) ? 'SELECTED' : '';?>><JText>TITLE_UNPUBLISHED</JText></option>
			<option value="<?php echo JFCPANEL_STATE;?>" <?php echo ($cpanelModules & JFCPANEL_STATE) ? 'SELECTED' : '';?>><JText>TITLE_STATE</JText></option>
		  </select>
		 </td>
		  <td align="left" valign="top">	<?php
		  $tip = JText::_('CPANEL_HELP');
		  echo mosToolTip( $tip );
			?>
		</tr>

		<tr>
	      <th colspan="3"><JText>Advanced Users Only - Experimental Query Analysis Caching</JText></th>
	    </tr>
		<tr>
	      <td width="20%" align="left" valign="top"><strong><JText>Enable experimental query analysis caching</JText></strong></td>
	      <td width="20%" align="left" valign="top">
	      <?php
	      $options = array();
	      $options[] = mosHTML::makeOption( '0', JText::_('No'));
	      $options[] = mosHTML::makeOption( '1', JText::_('Yes') );
	      $frmField = mosHTML::selectList( $options, 'qacaching', 'class="inputbox" size="1"', 'value', 'text', $joomfishManager->getCfg( 'qacaching',0 ) );
	      echo $frmField;
	      ?>
		 </td>
		  <td align="left" valign="top">	<?php
		  $tip = JText::_('QA_CACHING_HELP');
		  echo mosToolTip( $tip );
			?>
		</tr>
		<tr>
	      <td width="20%" align="left" valign="top"><strong><JText>Enable query analysis logging</JText> (<a href="<?php echo $mosConfig_live_site."/administrator/components/com_joomfish/qalog.txt"?>">qalog.txt</a>)</strong></td>
	      <td width="20%" align="left" valign="top">
	      <?php
	      $options = array();
	      $options[] = mosHTML::makeOption( '0', JText::_('No'));
	      $options[] = mosHTML::makeOption( '1', JText::_('Yes') );
	      $frmField = mosHTML::selectList( $options, 'qalogging', 'class="inputbox" size="1"', 'value', 'text', $joomfishManager->getCfg( 'qalogging',0 ) );
	      echo $frmField;
	      ?>
		 </td>
		  <td align="left" valign="top">	<?php
		  $tip = JText::_('QA_LOGGING_HELP');
		  echo mosToolTip( $tip );
			?>
		</tr>




		<?php
		HTML_joomfish::_JoomlaFooter('show', $act, $option);
	}

	/**
	 * This method creates the output for the language configuration
	 *
	 * @param class $joomfishManager
	 * @param array $languageList
	 */
	function showLanguageConfiguration( $joomfishManager, $languageList) {
		global $act, $task, $option, $database, $mosConfig_lang, $mosConfig_live_site;
		HTML_joomfish::_JoomlaHeader( JText::_('Language Title'), 'langmanager' );
		?>
    	<tr>
	      <td width="30%" align="left" valign="top"><strong><JText>Default Language</JText></strong></td>
	      <td align="left" valign="top"><strong style="color: red;"><?php echo $mosConfig_lang;?></strong>&nbsp;<JText>DEFAULT_LANGUAGE_HELP</JText></td>
	    </tr>
	    <tr>
	      <td width="30%" align="left" valign="top"><strong><JText>Component Admin Interface Language</JText></strong></td>
	      <td><JText>LANGUAGE_HELP</JText></td>
	    <tr>
	      <td align="center" valign="top" colspan="2">
				  <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
				    <tr>
				    <th class="title">&nbsp;</th>
					<th class="title" width="20%" align="left"><JText>TITLE_NAME</JText>&nbsp;
				    	<?php
				    	$tip = JText::_('NAME_HELP');
				    	echo mosToolTip( $tip );
				    	?>
					</th>
				    <th width="5%" align="left"><JText>TITLE_ACTIVE</JText></th>
				    <th width="10%" nowrap="nowrap" align="left"><JText>TITLE_ISOCODE</JText>&nbsp;
				    	<?php
				    	$tip = JText::_('ISOCODE_HELP');
				    	echo mosToolTip( $tip );
				    	?>
				    </th>
				    <th width="10%" nowrap="nowrap" align="left"><JText>TITLE_SHORTCODE</JText>&nbsp;
				    	<?php
				    	$tip = JText::_('SHORTCODE_HELP');
				    	echo mosToolTip( $tip );
				    	?>
				    </th>
				    <th width="15%" nowrap="nowrap" align="left"><JText>TITLE_JOOMLA</JText></th>
				    <th nowrap="nowrap" align="left"><JText>TITLE_FALLBACK</JText>&nbsp;
				    	<?php
				    	$tip = JText::_('FALLBACK_HELP');
				    	echo mosToolTip( $tip );
				    	?>
				    </th>
				    <th width="30%" nowrap="nowrap" align="left"><JText>TITLE_IMAGE</JText>&nbsp;
				    	<?php
				    	$tip = JText::_('IMAGES_DIR_HELP');
				    	echo mosToolTip( $tip );
				    	?>
				    </th>
				    <th nowrap="nowrap" align="left"><JText>TITLE_ORDER</JText>&nbsp;
				    	<?php
				    	$tip = JText::_('ORDER_HELP');
				    	echo mosToolTip( $tip );
				    	?>
				    </th>
				    <th nowrap="nowrap" align="left"><JText>TITLE_CONFIG</JText>&nbsp;
				    	<?php
				    	$tip = JText::_('CONFIG_HELP');
				    	echo mosToolTip( $tip );
				    	?>
				    </th>
				    </tr>
				    <?php
				    $k=0;
				    $i=0;
				    reset($languageList);
				foreach ($languageList as $language ) { ?>
					<tr class="<?php echo 'row' . $k; ?>">
			      	<td align="center">
				        <input type="hidden" name="frmLanguageID[]" value="<?php echo $language->id; ?>" />
				        <?php if ( $mosConfig_lang != $language->code ) : ?>
			      			<input type="checkbox" name="frmLanguageDelete[]" value="<?php echo $language->id; ?>" onclick="isChecked(this.checked);" />
			      		<?php endif;?>
			      	</td>
					<td><input type="text" name="frmLanguageName[]" value="<?php echo $language->name; ?>" size="30" maxlength="100" /></td>
					<td align="center"><input type="checkbox" name="frmLanguageActive[]"<?php echo $language->active==1 ? ' checked' : ''; ?> value="<?php echo $language->id; ?>" /></td>
					<td><input type="text" name="frmLanguageISO[]" value="<?php echo $language->iso; ?>" size="10" maxlength="20" /></td>
				      <td><input type="text" name="frmLanguageShortCode[]" value="<?php echo $language->shortcode; ?>" size="10" maxlength="20" /></td>
				      <td><input type="text" name="frmLanguageCode[]" value="<?php echo $language->code; ?>" size="13" maxlength="20" /></td>
				      <td><input type="text" name="frmLanguageFallbackCode[]" value="<?php echo $language->fallback_code; ?>" size="10" maxlength="20" /></td>
					  <td nowrap="nowrap">
			      		<input type="text" name="frmLanguageImage[]" value="<?php echo $language->image; ?>" size="30" />
				      	<?php
			      	    // new by mic
			      	    if( !empty( $language->image ) || !empty( $language->shortcode ) ) {
                            if( eregi( '/', $language->image ) ) {
                                $flag_image_src =  $language->image;
                            }else{
                                $flag_image_src = $mosConfig_live_site
                                . '/components/com_joomfish/images/flags/' . $language->shortcode . '.gif';
                            } ?>
                            &nbsp;
                                <img src="<?php echo $flag_image_src; ?>" alt="<?php echo $language->name; ?>" title="<?php echo $language->name; ?>" width="20" height="14" border="0" />
                            <?php
                        } ?>
                      </td>
				      <td><input type="text" name="frmLanguageOrder[]" value="<?php echo $language->ordering; ?>" size="5" maxlength="5" /></td>
				      <td align="center"><input type="hidden" name="frmLanguageParams[]" value="<?php echo $language->params; ?>" />
				      	<a href="index2.php?option=com_joomfish&amp;task=edit&amp;act=translateConfig&amp;hidemainmenu=1&amp;lang=<?php echo $language->id;?>"><img src="<?php echo $mosConfig_live_site;?>/images/M_images/edit.png" alt="<?php echo _E_EDIT;?>" border="0" /></a></td>
				      <?php
								$k = 1 - $k;
								$i++;
				}
						?>
						</tr>
					</table>
			  </td>
	    </tr>
		<?php
		HTML_joomfish::_JoomlaFooter('show', $act, $option);
	}

	function showElementOverview( $joomfishManager, $pageNav ) {
		global $my, $act, $task, $database, $mosConfig_live_site;
		$filterOptions = '';
		$filterOptions .= '<td  nowrap align="center">' .JText::_('DISPLAY'). ':<br/>' .$pageNav->getLimitBox(). '</td>';
		HTML_joomfish::_JoomlaHeader( JText::_('Content elements'), 'cpanel', $filterOptions );
	?>
  </table>

  <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
	<tr>
		<td>
		<table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminList">
	    <tr>
	      <th width="20" nowrap>&nbsp;</th>
	      <th class="title" width="35%" align="left"><JText>TITLE_NAME</JText></th>
	      <th width="15%" align="left"><JText>TITLE_AUTHOR</JText></th>
	      <th width="15%" nowrap="nowrap" align="left"><JText>TITLE_VERSION</JText></th>
	      <th nowrap="nowrap" align="left"><JText>TITLE_DESCRIPTION</JText></th>
	    </tr>
	    <?php
	    $elements = $joomfishManager->getContentElements();
	    $k=0;
	    $i=0;
	    $element_values = array_values($elements);
	    for ( $i=$pageNav->limitstart; $i<$pageNav->limitstart + $pageNav->limit && $i<$pageNav->total; $i++ ) {
	    	$element = $element_values[$i];
	    	$key = $element->referenceInformation['tablename'];
					?>
	    <tr class="<?php echo "row$k"; ?>">
	      <td width="20">
	        <?php		if ($element->checked_out && $element->checked_out != $my->id) { ?>
	        &nbsp;
	        <?php		} else { ?>
	        <input type="checkbox" id="cb<?php echo $i;?>" name="cid[]" value="<?php echo $key; ?>" onclick="isChecked(this.checked);" />
	        <?php		} ?>
	      </td>
	      <td>
	      	<a href="#edit" onclick="return listItemTask('cb<?php echo $i;?>','edit')"><?php echo $element->Name; ?></a>
				</td>
	      <td><?php echo $element->Author ? $element->Author : '&nbsp;'; ?></td>
	      <td><?php echo $element->Version ? $element->Version : '&nbsp;'; ?></td>
	      <td><?php echo $element->Description ? $element->Description : '&nbsp;'; ?></td>
					<?php
					$k = 1 - $k;
	    }
			?>
		</tr>
		<tr>
	      <th align="center" colspan="5"><?php echo $pageNav->writePagesLinks(); ?></th>
	    </tr>
	    <tr>
	      <td align="center" colspan="5"><?php echo $pageNav->writePagesCounter(); ?></td>
	    </tr>
		</table>
		</td>
	</tr>
	<?php
	HTML_joomfish::_JoomlaFooter($task, 'config_elements');
	}

	function showElementConfiguration( $joomfishManager, $id ) {
		global $my, $act, $task, $database, $option, $mosConfig_live_site;
		HTML_joomfish::_JoomlaHeader( JText::_('Content elements'), 'joomfish', '', false );
		$contentElement = $joomfishManager->getContentElement( $id );
	?>
    <table class="adminform">
	  <tr>
	    <td width="100%">
		<?php
		$tabs = new mosTabs(0);
		$tabs->startPane("contentelements");
		$tabs->startTab(JText::_('CONFIGURATION'),"ElementConfig-page");
		?>
	  <table width="100%" border="0" cellpadding="4" cellspacing="2" class="adminForm">
	    <tr>
	      <th colspan="3"><JText>General information</JText></th>
	    </tr>
	    <tr align="center" valign="middle">
	      <td width="30%" align="left" valign="top"><strong><JText>TITLE_NAME</JText></strong></td>
	      <td width="20%" align="left" valign="top"><?php echo $contentElement->Name;?></td>
			  <td align="left"></td>
	    </tr>
	    <tr align="center" valign="middle">
	      <td width="30%" align="left" valign="top"><strong><JText>TITLE_AUTHOR</JText></strong></td>
	      <td width="20%" align="left" valign="top"><?php echo $contentElement->Author;?></td>
			  <td align="left"></td>
	    </tr>
	    <tr align="center" valign="middle">
	      <td width="30%" align="left" valign="top"><strong><JText>TITLE_VERSION</JText></strong></td>
	      <td width="20%" align="left" valign="top"><?php echo $contentElement->Version;?></td>
			  <td align="left"></td>
	    </tr>
	    <tr align="center" valign="middle">
	      <td width="30%" align="left" valign="top"><strong><JText>TITLE_DESCRIPTION</JText></strong></td>
	      <td width="20%" align="left" valign="top"><?php echo $contentElement->Description;?></td>
			  <td align="left"></td>
	    </tr>
	  </table>
	  	<?php
	  	$tabs->endTab();
	  	$tabs->startTab(JText::_('DB Reference'),"ElementReference-page");

	  	$contentTable = $contentElement->getTable();
		?>
	  <table width="100%" border="0" cellpadding="4" cellspacing="2" class="adminForm">
	    <tr>
	      <th colspan="2"><JText>DATABASE_INFORMATION</JText></th>
	    </tr>
	    <tr align="center" valign="middle">
	      <td width="15%" align="left" valign="top"><strong><JText>DATABASETABLE</JText></strong><br /><JText>DATABASETABLE_HELP</JText></td>
	      <td width="60%" align="left" valign="top"><?php echo $contentTable->Name;?></td>
	    </tr>
	    <tr align="center" valign="middle">
	      <td width="15%" align="left" valign="top"><strong><JText>DATABASEFIELDS</JText></strong><br /><JText>DATABASEFIELDS_HELP</JText></td>
	      <td width="60%" align="left" valign="top">
			  <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
				<tr>
					<th><JText>DBFIELDNAME</JText></th>
					<th><JText>DBFIELDTYPE</JText></th>
					<th><JText>DBFIELDLABLE</JText></th>
					<th><JText>TRANSLATE</JText></th>
				</tr>
				<?php
				$k=0;
				foreach( $contentTable->Fields as $tableField ) {
					?>
			  <tr class="<?php echo "row$k"; ?>">
					<td><?php echo $tableField->Name ? $tableField->Name : "&nbsp;";?></td>
					<td><?php echo $tableField->Type ? $tableField->Type : "&nbsp;";?></td>
					<td><?php echo $tableField->Lable ? $tableField->Lable : "&nbsp;";?></td>
					<td><?php echo $tableField->Translate ? _CMN_YES : _CMN_NO;?></td>
				</tr>
					<?php
					$k=1-$k;
				}
				?>
				</table>
				<?php
				?>
				</td>
	    </tr>
	  </table>
	  	<?php
	  	$tabs->endTab();
	  	$tabs->startTab(JText::_('Sample data'),"ElementSamples-page");
	  	$contentTable = $contentElement->getTable();
		?>
	  <table width="100%" border="0" cellpadding="4" cellspacing="2" class="adminForm">
	    <tr>
	      <th><JText>Sample data</JText></th>
	    </tr>
	    <tr align="center" valign="middle">
	      <td width="100%" align="center" valign="top">
			  <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
				<tr>
				<?php
				$sqlFields = "";
				foreach( $contentTable->Fields as $tableField ) {
					if( $sqlFields!='' ) $sqlFields .= ',';
					$sqlFields .= '`' .$tableField->Name. '`';
					?>
					<th nowrap><?php echo $tableField->Lable;?></th>
					<?php
				}
				?>
				</tr>
				<?php
				$k=0;
				$idname = $joomfishManager->getPrimaryKey($contentTable->Name);
				$sql = "SELECT $sqlFields"
				. "\nFROM #__" .$contentTable->Name
				. "\nORDER BY $idname limit 0,10";
				$database->setQuery( $sql	);
				$rows = $database->loadObjectList();
				if( $rows != null ) {
					foreach ($rows as $row) {
					?>
					  <tr class="<?php echo "row$k"; ?>">
						<?php
						foreach( $contentTable->Fields as $tableField ) {
							$fieldName = $tableField->Name;
							$fieldValue = $row->$fieldName;
							if( $tableField->Type='htmltext' ) {
								$fieldValue = htmlspecialchars( $fieldValue );
							}

							if( $fieldValue=='' ) $fieldValue="&nbsp;";
							if( strlen($fieldValue) > 97 ) {
								$fieldValue = substr( $fieldValue, 0, 100) . '...';
							}

							?>
							<td valign="top"><?php echo $fieldValue;?></td>
							<?php
						}
						?>
						</tr>
							<?php
							$k=1-$k;
					}
				}
				?>
				</table>
				<?php
				?>
				</td>
	    </tr>
	  </table>
	<?php
	$tabs->endTab();
	$tabs->endPane();
		?>
	</td></tr>
		<?php
		HTML_joomfish::_JoomlaFooter('', $task, $option);
	}

	/**
	 * shows the element installer dialog
	 */
	function showContentElementInstaller( $cefiles=null, $message=null ) {
		global $task, $act, $option;

		HTML_joomfish::_pageHeader();
		?>
		<form enctype="multipart/form-data" action="index2.php" method="post" name="filename">
		<table class="adminheading">
		<tr>
			<th class="install"><JText>Install</JText> <JText>Content Elements</JText></th>
		</tr>
		</table>
		<?php if( $message != null ) : ?>
		<div class="message"><?php echo $message;?></div>
		<?php endif; ?>
		<table class="adminform">
		<tr>
			<th><JText>Upload XML file</JText></th>
		</tr>
		<tr>
			<td align="left"><JText>File name</JText>:
			<input class="text_area" name="userfile" type="file" size="70"/>
			<input class="button" type="submit" value="<?php echo JText::_('Upload file and install');?>" />
			</td>
		</tr>
		</table>

		<input type="hidden" name="task" value="uploadfile"/>
		<input type="hidden" name="act" value="config_elements"/>
		<input type="hidden" name="option" value="com_joomfish"/>
		</form>
		<p>&nbsp;</p>
		<?php if( $cefiles != null ) { ?>
		<form action="index2.php" method="post" name="adminForm">
			<table class="adminheading">
			<tr>
				<th class="install"><JText>Content Elements</JText></th>
			</tr>
			</table>

			<table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
		    <tr>
		      <th width="20" nowrap>&nbsp;</th>
		      <th class="title" width="35%" align="left"><JText>TITLE_NAME</JText></th>
		      <th width="15%" align="left"><JText>TITLE_AUTHOR</JText></th>
		      <th width="15%" nowrap="nowrap" align="left"><JText>TITLE_VERSION</JText></th>
		      <th nowrap="nowrap" align="left"><JText>TITLE_DESCRIPTION</JText></th>
		    </tr>
			<?php
		    $k=0;
		    $i=0;
		    foreach (array_values($cefiles) as $element ) {
		    	$key = $element->referenceInformation['tablename'];
						?>
		    <tr class="<?php echo "row$k"; ?>">
		      <td width="20">
		        <?php		if ($element->checked_out && $element->checked_out != $my->id) { ?>
		        &nbsp;
		        <?php		} else { ?>
				<input type="radio" id="cb<?php echo $i;?>" name="cid[]" value="<?php echo $key; ?>" onclick="isChecked(this.checked);">
		        <?php		} ?>
		      </td>
		      <td><?php echo $element->Name; ?></td>
		      <td><?php echo $element->Author ? $element->Author : '&nbsp;'; ?></td>
		      <td><?php echo $element->Version ? $element->Version : '&nbsp;'; ?></td>
		      <td><?php echo $element->Description ? $element->Description : '&nbsp;'; ?></td>
		     </tr>
				<?php
				$k = 1 - $k;
				$i++;
		    }
		} else {
			?>
			<tr><td class="small">
			There are no custom elements installed
			</td></tr>
			<?php
		}
		?>
		</table>
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="act" value="config_elements" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="option" value="<?php echo $option;?>" />
		</form>
	    <table width="100%" border="0">
		<tr>
		  <td width="99%" align="right" valign="top">
			<?php
			$x = "@";
			$y="Support";
			$z="JoomFish.net";
			$mail=$y.$x.$z;
			?><div align="center"><span class="smallgrey">Joom!Fish Version <?php echo JoomFishManager::getVersionFooter();?>, &copy; 2003-2007 Copyright by <a href="http://www.ThinkNetwork.com" target="_blank" class="smallgrey">Think Network</a> under <a href="index2.php?option=com_joomfish&amp;task=showInformation&amp;fileCode=license" class="smallgrey">Open Source License.</a> Contact: <?php echo mosHTML::emailCloaking( $mail, 0);?></span></div>
		  </td>
		  </tr>
		</table>
		<?php
		HTML_joomfish::_pageFooter();
	}

	/**
	 * shows the translation overview
	 *
	 * @param array $rows		of rows within this particular overview
	 * @param array $search		of search parameters
	 * @param object $pageNav	of page navigation information
	 * @param array $langlist	of languages available
	 * @param array $clist		of content element files
	 * @param array $catid		of selected id's
	 * @param int $language_id	of the current language
	 * @param array $filterlist	of additional filter settings
	 */
	function showTranslationOverview( $rows, $search, $pageNav, $langlist, $clist, $catid , $language_id, $filterlist=array()) {
		global $my, $act, $task, $database, $option, $mosConfig_live_site;
		$filterOptions = '<td align="right"><table><tr>';
		$filterOptions .= '<td  nowrap align="center">' .JText::_('Display'). ':<br/>' .$pageNav->getLimitBox(). '</td>';
		$filterOptions .= '<td  nowrap align="center">' .JText::_('Languages'). ':<br/>' .$langlist. '</td>';
		$filterOptions .= '<td  nowrap align="center">' .JText::_('Content elements'). ':<br/>' .$clist. '</td>';
		$filterOptions .= '</tr></table>';

		if (isset($filterlist) && count($filterlist)>0){
			$filterOptions .= '<table><tr>';
			foreach ($filterlist as $fl){
				if (is_array($fl))		$filterOptions .= "<td nowrap align='center'>".$fl["title"].":<br/>".$fl["html"]."</td>";
			}
			$filterOptions .= '</tr></table>';
		}

		HTML_joomfish::_JoomlaHeader( JText::_('TITLE_TRANSLATION'), 'edit', $filterOptions );
	?>
  </table>

  <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
    <tr>
      <th width="20"><input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($rows); ?>);" /></th>
      <th class="title" width="20%" align="left"  nowrap="nowrap"><JText>TITLE</JText></th>
      <th width="10%" align="left" nowrap="nowrap"><JText>Language</JText></th>
      <th width="20%" align="left" nowrap="nowrap"><JText>TITLE_TRANSLATION</JText></th>
      <th width="15%" align="left" nowrap="nowrap"><JText>TITLE_DATECHANGED</JText></th>
      <th width="15%" nowrap="nowrap" align="center"><JText>TITLE_STATE</JText></th>
      <th align="center" nowrap="nowrap"><JText>TITLE_PUBLISHED</JText></th>
    </tr>
	<?php
	if( !isset($catid) || $catid == "" || $language_id==-1) {
		?>
	<tr><td colspan="8"><p><JText>NOELEMENT_SELECTED</JText></p></td></tr>
		<?php
	}
	else {
		?>
    <?php
    $k=0;
    $i=0;
    foreach ($rows as $row ) {
				?>
    <tr class="<?php echo "row$k"; ?>">
      <td width="20">
        <?php		if ($row->checked_out && $row->checked_out != $my->id) { ?>
        &nbsp;
        <?php		} else { ?>
        <input type="checkbox" id="cb<?php echo $i;?>" name="cid[]" value="<?php echo $row->translation_id."|".$row->id."|".$row->language_id; ?>" onclick="isChecked(this.checked);" />
        <?php		} ?>
      </td>
      <td>
      	<a href="#edit" onclick="hideMainMenu(); return listItemTask('cb<?php echo $i;?>','edit');"><?php echo $row->title; ?></a>
			</td>
      <td nowrap><?php echo $row->language ? $row->language : JText::_('NOTRANSLATIONYET') ; ?></td>
      <td><?php echo $row->titleTranslation ? $row->titleTranslation : '&nbsp;'; ?></td>
	  <td><?php echo $row->lastchanged ? strftime("%A, %d %B %Y %H:%M",strtotime($row->lastchanged)):"";?></td>
				<?php
				switch( $row->state ) {
					case 1:
						$img = 'status_g.png';
						break;
					case 0:
						$img = 'status_y.png';
						break;
					case -1:
					default:
						$img = 'status_r.png';
						break;
				}
				?>
      <td align="center"><img src="components/com_joomfish/images/<?php echo $img;?>" width="12" height="12" border="0" alt="" /></td>
				<?php
				if (isset($row->published) && $row->published) {
					$img = 'publish_g.png';
				} else {
					$img = 'publish_x.png';
				}

				$href='';
				if( $row->state>=0 ) {
					$href = '<a href="javascript: void(0);" ';
					$href .= 'onclick="return listItemTask(\'cb' .$i. '\',\'' .($row->published ? 'unpublish' : 'publish'). '\')">';
					$href .= '<img src="images/' .$img. '" width="12" height="12" border="0" alt="" />';
					$href .= '</a>';
				}
				else {
					$href = '<img src="images/' .$img. '" width="12" height="12" border="0" alt="" />';
				}
				?>
      <td align="center"><?php echo $href;?></td>
				<?php
				$k = 1 - $k;
				$i++;
    }
		?>
	</tr>
    <tr>
      <th align="center" colspan="10">
		<?php echo $pageNav->writePagesLinks(); ?></th>
    </tr>
    <tr>
      <td align="center" colspan="10"> <?php echo $pageNav->writePagesCounter(); ?></td>
    </tr>
	</table>
<br />
<table cellspacing="0" cellpadding="4" border="0" align="center">
  <tr align="center">
    <td> <img src="components/com_joomfish/images/status_g.png" width="12" height="12" border=0 alt="<JText>STATE_OK</JText>" />
    </td>
    <td> <JText>TRANSLATION_UPTODATE</JText> |</td>
    <td> <img src="components/com_joomfish/images/status_y.png" width="12" height="12" border=0 alt="<JText>STATE_CHANGED</JText>" />
    </td>
    <td> <JText>TRANSLATION_INCOMPLETE</JText> |</td>
    <td> <img src="components/com_joomfish/images/status_r.png" width="12" height="12" border=0 alt="<JText>STATE_NOTEXISTING</JText>" />
    </td>
    <td> <JText>TRANSLATION_NOT_EXISTING</JText></td>
  </tr>
  <tr align="center">
    <td> <img src="images/publish_g.png" width="12" height="12" border=0 alt="Translation visible" />
    </td>
    <td> <JText>TRANSLATION_PUBLISHED</JText>  |</td>
    <td> <img src="images/publish_x.png" width="12" height="12" border=0 alt="Finished" />
    </td>
    <td> <JText>TRANSLATION_NOT_PUBLISHED</JText></td>
    <td> &nbsp;
    </td>
    <td><JText>STATE_TOGGLE</JText></td>
  </tr>
<?php } ?>
	<?php
	HTML_joomfish::_JoomlaFooter( 'overview', $act, $option);
	}

	/**
	 * shows the overview of orphan translations
	 *
	 * @param unknown_type $rows
	 * @param unknown_type $search
	 * @param unknown_type $pageNav
	 * @param unknown_type $langlist
	 * @param unknown_type $clist
	 * @param unknown_type $catid
	 * @param unknown_type $language_id
	 * @param unknown_type $filterlist
	 */
	function showOrphanOverview( $rows, $search, $pageNav, $langlist="", $clist, $catid , $language_id, $filterlist=array()) {
		global $my, $act, $task, $database, $option, $mosConfig_live_site;
		$filterOptions = '<td align="right"><table><tr>';
		$filterOptions .= '<td  nowrap align="center">' .JText::_('DISPLAY'). ':<br/>' .$pageNav->getLimitBox(). '</td>';
		if (strlen($langlist)>0) $filterOptions .= '<td  nowrap align="center">' .JText::_('Languages'). ':<br/>' .$langlist. '</td>';
		$filterOptions .= '<td  nowrap align="center">' .JText::_('Content elements'). ':<br/>' .$clist. '</td>';
		$filterOptions .= '</tr></table>';

		if (isset($filterlist) && count($filterlist)>0){
			$filterOptions .= '<table><tr>';
			foreach ($filterlist as $fl){
				if (is_array($fl))		$filterOptions .= "<td nowrap align='center'>".$fl["title"].":<br/>".$fl["html"]."</td>";
			}
			$filterOptions .= '</tr></table>';
		}
		HTML_joomfish::_JoomlaHeader( JText::_('CLEANUP ORPHANS'), 'orphan', $filterOptions );
	?>
  </table>

  <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
    <tr>
	  <th width="20"><input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($rows); ?>);" /></th>
      <th class="title" width="20%" align="left"  nowrap="nowrap"><JText>TITLE</JText></th>
      <th width="10%" align="left" nowrap="nowrap"><JText>Language</JText></th>
      <th width="20%" align="left" nowrap="nowrap"><JText>TITLE_TRANSLATION</JText></th>
      <th width="15%" align="left" nowrap="nowrap"><JText>TITLE_DATECHANGED</JText></th>
      <th width="15%" nowrap="nowrap" align="center"><JText>TITLE_STATE</JText></th>
      <th align="center" nowrap="nowrap"><JText>TITLE_PUBLISHED</JText></th>
    </tr>
	<?php
	if( !isset($catid) || $catid == ""  ) {
		?>
  <tr><td colspan="8"><p><JText>NOELEMENT_SELECTED</JText></p></td></tr>
		<?php
	}
	else {
		?>
    <?php
    $k=0;
    $i=0;
    foreach ($rows as $row ) {
				?>
    <tr class="<?php echo "row$k"; ?>">
      <td width="20">
        <?php		if ($row->checked_out && $row->checked_out != $my->id) { ?>
        &nbsp;
        <?php		} else { ?>
        <input type="checkbox" id="cb<?php echo $i;?>" name="cid[]" value="<?php echo $row->jfc_id."|".$row->jfc_refid."|".$row->language_id; ?>" onclick="isChecked(this.checked);" />
        <?php		} ?>
      </td>
      <td>
      	<a href="#edit" onclick="return listItemTask('cb<?php echo $i;?>','edit')"><?php echo $row->title; ?></a>
			</td>
      <td nowrap><?php echo $row->language ? $row->language :  JText::_('NOTRANSLATIONYET'); ?></td>
      <td><?php echo $row->titleTranslation ? $row->titleTranslation : '&nbsp;'; ?></td>
	  <td><?php echo $row->lastchanged ? strftime("%A, %d %B %Y %H:%M",strtotime($row->lastchanged)):"";?></td>
				<?php
				switch( $row->state ) {
					case 1:
						$img = 'status_g.png';
						break;
					case 0:
						$img = 'status_y.png';
						break;
					case -1:
					default:
						$img = 'status_r.png';
						break;
				}
				?>
      <td align="center"><img src="components/com_joomfish/images/<?php echo $img;?>" width="12" height="12" border="0" alt="" /></td>
				<?php
				if (isset($row->published) && $row->published) {
					$img = 'publish_g.png';
				} else {
					$img = 'publish_x.png';
				}

				$href='';
				if( $row->state>=0 ) {
					$href = '<a href="javascript: void(0);" ';
					$href .= 'onclick="return listItemTask(\'cb' .$i. '\',\'' .($row->published ? 'unpublish' : 'publish'). '\')">';
					$href .= '<img src="images/' .$img. '" width="12" height="12" border="0" alt="" />';
					$href .= '</a>';
				}
				else {
					$href = '<img src="images/' .$img. '" width="12" height="12" border="0" alt="" />';
				}
				?>
      <td align="center"><?php echo $href;?></td>
				<?php
				$k = 1 - $k;
				$i++;
				?>
	</tr>
    <?php }?>
    <tr>
      <th align="center" colspan="10"> <?php echo $pageNav->writePagesLinks(); ?></th>
    </tr>
    <tr>
      <td align="center" colspan="10"> <?php echo $pageNav->writePagesCounter(); ?></td>
    </tr>
	</table>
<br />
<table cellspacing="0" cellpadding="4" border="0" align="center">
  <tr align="center">
    <td> <img src="components/com_joomfish/images/status_g.png" width="12" height="12" border=0 alt="<JText>STATE_OK</JText>" />
    </td>
    <td> <JText>TRANSLATION_UPTODATE</JText> |</td>
    <td> <img src="components/com_joomfish/images/status_y.png" width="12" height="12" border=0 alt="<JText>STATE_CHANGED</JText>" />
    </td>
    <td> <JText>TRANSLATION_INCOMPLETE</JText> |</td>
    <td> <img src="components/com_joomfish/images/status_r.png" width="12" height="12" border=0 alt="<JText>STATE_NOTEXISTING</JText>" />
    </td>
    <td> <JText>TRANSLATION_NOT_EXISTING</JText></td>
  </tr>
  <tr align="center">
    <td> <img src="images/publish_g.png" width="12" height="12" border=0 alt="Translation visible" />
    </td>
    <td> <JText>TRANSLATION_PUBLISHED</JText> |</td>
    <td> <img src="images/publish_x.png" width="12" height="12" border=0 alt="Finished" />
    </td>
    <td> <JText>TRANSLATION_NOT_PUBLISHED</JText> </td>
    <td> &nbsp;
    </td>
    <td> <JText>STATE_TOGGLE</JText> </td>
  </tr>
<?php } ?>
	<?php
	HTML_joomfish::_JoomlaFooter('overview', $act, $option);
	}

	/**
	 * shows the overview of the check
	 *
	 * @param unknown_type $rows
	 * @param unknown_type $search
	 * @param unknown_type $pageNav
	 * @param unknown_type $langlist
	 * @param unknown_type $clist
	 * @param unknown_type $catid
	 * @param unknown_type $language_id
	 * @param unknown_type $filterlist
	 */
	function showManagementOverview($type='' ) {
		global $act, $task, $option, $mosConfig_live_site;
		?>
  <script language="javascript" type="text/javascript">
  function updateResultDiv( resultInfo, type ) {
	resultDiv = document.getElementById("manage_results");
	if( type == 'div' ) {
		resultDiv.innerHTML = resultInfo.innerHTML;
	} else {
		resultDiv.innerHTML = resultInfo;
	}
  }

  function executeCopyOriginal( toLanguage, confirmCheck, copyCat  ) {
  		if( toLanguage == null || toLanguage.value == -1 ) return;

		var link = 'index3.php?option=com_joomfish&task=copy&act=manage&type=original_language&phase=2';
  		if( confirmCheck.checked == true ) {
			if( !window.confirm( '<?php echo JText::_('CONFIRM_COPY_TO', true);?>' ) ) return;
			link += '&overwrite=1';
  		} else {
  			link += '&overwrite=0';
  		}
		link += '&language_id=' +toLanguage.value;

		// adding all requested catids to one string
		var catString = '';
		for(var i=0; i<copyCat.length; i++) {
			if( copyCat[i].value != null && copyCat[i].checked == true ) {
				if(catString != '') {
					catString += ',';
				}
				catString += copyCat[i].value;
			}
		}
		link += '&state_catid=' + catString;

		ajaxFrame = document.getElementById("ajaxFrame");
		ajaxFrame.contentDocument.location.href = link;
  }
  </script>
		<?php
	    HTML_joomfish::_JoomlaHeader( JText::_('TITLE_Management'), 'check', '' );
	    ?>
		<tr>
			<td width="35%" valign="top">
				<div id="cpanel">
				<?php
				$link = 'index3.php?option=com_joomfish&amp;task=check&amp;act=manage&amp;type=translation_status';
				HTML_joomfish::_quickiconButton( $link, 'checkin.png', JText::_('Check Translation Status'), '/administrator/images/', 'ajaxFrame', "updateResultDiv('" .JText::_('Processing'). "', 'text');" );
				$link = 'index3.php?option=com_joomfish&amp;task=check&amp;act=manage&amp;type=original_status';
				HTML_joomfish::_quickiconButton( $link, 'checkin.png', JText::_('Check Original Status'), '/administrator/images/', 'ajaxFrame', "updateResultDiv('" .JText::_('Processing'). "', 'text');" );
				$link = 'index3.php?option=com_joomfish&amp;task=copy&amp;act=manage&amp;type=original_language';
				HTML_joomfish::_quickiconButton( $link, 'dbrestore.png', JText::_('Copy Original to Language'), '/administrator/images/', 'ajaxFrame', "updateResultDiv('" .JText::_('Processing', 'text'). "');" );
/*
				echo '<div style="clear: both;" />';
				$link = 'index3.php?option=com_joomfish&amp;task=copy&amp;act=manage&amp;type=translation_language';
				HTML_joomfish::_quickiconButton( $link, 'dbrestore.png', JText::_('Copy Translation to Langage'), '/administrator/images/', 'ajaxFrame', "updateResultDiv('" .JText::_('Processing', 'text'). "');" );
				$link = 'index3.php?option=com_joomfish&amp;task=update&amp;act=manage&amp;type=original_value';
				HTML_joomfish::_quickiconButton( $link, 'query.png', JText::_('Update original values'), '/administrator/images/', 'ajaxFrame', "updateResultDiv('" .JText::_('Processing'). "', 'text');" );
*/
				?>
				</div>
			</td>
			<td width="45%" valign="top">
				<div style="width: 98%; height: 100%;">
					<h3><JText>Management info</JText></h3>
					<div id="manage_results"><?php echo JText::_('MANAGEMENT_INTRO');?></div>
				</div>
				<iframe style="display: none;" id="ajaxFrame" name="ajaxFrame" ></iframe>
			</td>
		</tr>
		<?php
		HTML_joomfish::_JoomlaFooter($task, 'config_elements');
	}

	/**
	 * This method copies the result information from the iframe into the available div area
	 *
	 * @param string $resultInformation	inner HTML output
	 * @param string $reload	url for a requested URL
	 */
	function showManagementResult( $resultInformation=null, $reload='' ) {
		?>
		<div id="resultContent"><?php echo $resultInformation;?></div>
		<script language="javascript" type="text/javascript">
		resultDiv = document.getElementById("resultContent");
		window.parent.updateResultDiv( resultDiv, 'div' );
		<?php
		if( $reload != '' ) {
			echo "document.location.href='$reload';";
		}
		?>
		</script>
		<?php
	}

	/**
	 * This method renders a nice status overview table from the content element files
	 *
	 * @param unknown_type $contentelements
	 */
	function renderOriginalStatusTable($originalStatus, $message='', $langCodes=null) {
		$htmlOutput = '';

		$htmlOutput = '<table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">';
		$htmlOutput .= '<tr><th>' .JText::_('Content'). '</th><th>' .JText::_('table exist'). '</th><th>' .JText::_('original total'). '</th><th>' .JText::_('Orphans'). '</th>';
		if(is_array($langCodes)) {
			foreach ($langCodes as $code) {
				$htmlOutput .= '<th>' .$code. '</th>';
			}
		}
		$htmlOutput .= '</tr>';

		$ceName = '';
		foreach ($originalStatus as $statusRow ) {
			$href = 'index2.php?option=com_joomfish&amp;task=overview&amp;act=translate&amp;catid='.$statusRow['catid'];
			$htmlOutput .= '<tr>';
			$htmlOutput .= '<td><a href="' .$href. '" target="_blank">' .$statusRow['name']. '</a></td>';
			$htmlOutput .= '<td style="text-align: center;">' .($statusRow['missing_table'] ? JText::_('missing') : JText::_('valid')). '</td>';
			$htmlOutput .= '<td style="text-align: center;">' .$statusRow['total']. '</td>';
			$htmlOutput .= '<td style="text-align: center;">' .$statusRow['orphans']. '</td>';
			if(is_array($langCodes)) {
				foreach ($langCodes as $code) {
					if( array_key_exists('langentry_' .$code, $statusRow)) {
						$persentage = intval( ($statusRow['langentry_' .$code]*100) / $statusRow['total'] );
						$htmlOutput .= '<td>' .$persentage. '%</td>';
					} else {
						$htmlOutput .= '<td>&nbsp;</td>';
					}
				}
			}
			$htmlOutput .= '</tr>';
		}

		if($message!='') {
			$htmlOutput .= '<tr><td colspan="5" class="message">' .$message. '</td></tr>';
		}
		$htmlOutput .= '</table>';

		return $htmlOutput;
	}

	/**
	 * Status table for translation checks
	 *
	 * @param array $translationStatus
	 * @return unknown
	 */
	function renderTranslationStatusTable($translationStatus, $message='') {
		$htmlOutput = '';

		$htmlOutput .= '<table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">';
		$htmlOutput .= '<tr><th>' .JText::_('Content'). '</th><th>' .JText::_('language'). '</th><th>' .JText::_('translation total'). '</th><th>' .JText::_('TITLE_PUBLISHED'). '</th><th>' .JText::_('valid'). '</th><th>' .JText::_('unvalid'). '</th></tr>';

		foreach ($translationStatus as $statusRow ) {
			$href = 'index2.php?option=com_joomfish&amp;task=overview&amp;act=translate&amp;catid='.$statusRow['catid'].'&amp;language_id='.$statusRow['language_id'];
			$htmlOutput .= '<tr>';
			$htmlOutput .= '<td><a href="'.$href.'" target="_blank">' .$statusRow['content']. '</a></td>';
			$htmlOutput .= '<td>' .$statusRow['language']. '</td>';
			$htmlOutput .= '<td style="text-align: center;">' .$statusRow['total']. '</td>';
			$htmlOutput .= '<td style="text-align: center;">' .$statusRow['published']. '</td>';
			$htmlOutput .= '<td style="text-align: center;">' .$statusRow['state_valid']. '</td>';
			$htmlOutput .= '<td style="text-align: center;">' .$statusRow['state_unvalid']. '</td>';
			$htmlOutput .= '</tr>';
		}

		if($message!='') {
			$htmlOutput .= '<tr><td colspan="7" class="message">' .$message. '</td></tr>';
		}
		$htmlOutput .= '</table>';

		return $htmlOutput;
	}

	/**
	 * This method renders the information page for the copy process
	 *
	 * @param unknown_type $contentelements
	 */
	function renderCopyInformation($original2languageInfo, $message='', $langList=null) {
		$htmlOutput = '';

		if($message!='') {
			$htmlOutput .= '<span class="message">' .$message. '</span><br />';
		}
		$htmlOutput .= '<table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">';
		$htmlOutput .= '<tr><th width="25%">' .JText::_('Content'). '</th><th width="10%">' .JText::_('original total'). '</th><th width="10%">' .JText::_('processed'). '</th><th width="10%">' .JText::_('copied'). '</th><th>' .JText::_('copy to language'). '</th>';
		$htmlOutput .= "</tr>\n";

		$ceName = '';
		foreach ($original2languageInfo as $statusRow ) {
			$href = 'index2.php?option=com_joomfish&amp;task=overview&amp;act=translate&amp;catid='.$statusRow['catid'];
			$htmlOutput .= '<tr>';
			$htmlOutput .= '<td><a href="' .$href. '" target="_blank">' .$statusRow['name']. '</a></td>';
			$htmlOutput .= '<td style="text-align: center;">' .$statusRow['total']. '</td>';
			$htmlOutput .= '<td style="text-align: center;">' .$statusRow['processed']. '</td>';
			$htmlOutput .= '<td style="text-align: center;">' .$statusRow['copied']. '</td>';
			$htmlOutput .= '<td style="text-align: center;"><input name="copy_catid" type="checkbox" value="' .$statusRow['catid'].'" /></td>';
			$htmlOutput .= "</tr>\n";
		}

		if($langList != null) {
			$htmlOutput .= '<tr><td>' .JText::_('select language'). '</td>';
			$htmlOutput .= '<td style="text-align: center;" colspan="3" nowrap="nowrap">' .$langList. '<input id="confirm_overwrite" name="confirm_overwrite" type="checkbox" value="1" />' .JText::_('overwrite existing translations'). '&nbsp;';
			$htmlOutput .= '<input id="copy_original" name="copy_original" type="button" value="' .JText::_('copy'). '" onClick="executeCopyOriginal(document.getElementById(\'select_language\'), document.getElementById(\'confirm_overwrite\'), document.getElementsByName(\'copy_catid\'))" /></td>';
			$htmlOutput .= "</tr>\n";
		}

		$htmlOutput .= '</table>';

		return $htmlOutput;
	}

	/**
	 * This method renders the information page for the copy process
	 *
	 * @param unknown_type $contentelements
	 */
	function renderCopyProcess($original2languageInfo, $message='') {
		$htmlOutput = '';

		$htmlOutput = '<table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">';
		$htmlOutput .= '<tr><th>' .JText::_('Content'). '</th><th width="10%">' .JText::_('original total'). '</th><th width="10%">' .JText::_('processed'). '</th><th width="10%">' .JText::_('copied'). '</th>';
		$htmlOutput .= '</tr>';

		$ceName = '';
		foreach ($original2languageInfo as $statusRow ) {
			$href = 'index2.php?option=com_joomfish&amp;task=overview&amp;act=translate&amp;catid='.$statusRow['catid'];
			$htmlOutput .= '<tr>';
			$htmlOutput .= '<td><a href="' .$href. '" target="_blank">' .$statusRow['name']. '</a></td>';
			$htmlOutput .= '<td style="text-align: center;">' .$statusRow['total']. '</td>';
			$htmlOutput .= '<td style="text-align: center;">' .$statusRow['processed']. '</td>';
			$htmlOutput .= '<td style="text-align: center;">' .$statusRow['copied']. '</td>';
			$htmlOutput .= '</tr>';
		}
		if($message!='') {
			$htmlOutput .= '<tr><td colspan="7" class="message">' .$message. '</td></tr>';
		}
		$htmlOutput .= '</table>';

		return $htmlOutput;
	}

	/**
	* @return void
	* @param object $actContentObject
	* @param array $langlist
	* @param string $catid
	* @desc Shows the dialog for the content translation
	*/
	function showTranslation( $actContentObject, $langlist, $catid , $select_id , $tranFilters) {
		global $my, $act, $task, $database, $option, $select_language_id, $mosConfig_live_site;
		$elementTable = $actContentObject->getTable();

		HTML_joomfish::_JoomlaHeader( JText::_('TITLE_TRANSLATION'), 'joomfish', '', false );

		$editorFields=null;
		foreach ($tranFilters as $filter) {
			echo "<input type='hidden' name='".$filter->filterType."_filter_value' value='".$filter->filter_value."'/>";
		}

		// check system and user editor and load appropriate copying script
		global $mosConfig_editor, $adminPath, $my;
		// Per User Editor selection
		$editorParams = new mosParameters( $my->params );
		$editor = $editorParams->get( 'editor', '' );
		if (!$editor) {
			$editor = $mosConfig_editor;
		}
		echo "\n<!-- editor is $editor //-->\n";
		$editorFile = $adminPath."/editors/".strtolower($editor).".php";
		if (file_exists($editorFile)){
			require_once($editorFile);
		}
		else {
	?>

	<script language="javascript" type="text/javascript">
	function copyToClipboard(value,action) {
		try {
			if (document.getElementById) {
				innerHTML="";
				if (action=="copy") {
					srcEl = document.getElementById("original_value_"+value);
					innerHTML = srcEl.innerHTML;
				}
				if (window.clipboardData){
					window.clipboardData.setData("Text",innerHTML);
					alert("<?php echo JText::_('CLIPBOARD_COPIED'); ?>");
				}
				else {
					srcEl = document.getElementById("text_origText_"+value);
					srcEl.value = innerHTML;
					srcEl.select();
					alert("<?php echo JText::_('CLIPBOARD_COPY');?>");
				}
			}
		}
		catch(e){
			alert("<?php echo JText::_('CLIPBOARD_NOSUPPORT');?>");
		}
	}
    </script>
    <?php } ?>

	<script language="javascript" type="text/javascript">
	function confirmChangeLanguage(fromLang, fromIndex){
		selections = document.getElementsByName("language_id")[0].options;
		selection = document.getElementsByName("language_id")[0].selectedIndex;
		//alert(selection+" from "+ fromIndex+" which is "+fromLang+" xx "+document.getElementsByName("language_id")[0].value);
		var toLang = selections[selection].text;
		var toValue = selection = document.getElementsByName("language_id")[0].value;
		if (fromIndex!=toValue){
			answer = confirm("<?php echo ereg_replace( '<br />', '\n', JText::_('JS_CHANGE_TRANSLATION_LANGUAGE')); ?>");
			if (!answer) {
				document.getElementsByName("language_id")[0].selectedIndex=fromIndex;
			}
		}
		else {
			alert("<?php echo ereg_replace( '<br />', '\n', JText::_('JS_REINSTATE_TRANSLATION_LANGUAGE')); ?>");
		}
	}
    </script>

   	<table width="100%">
	  <tr>
	    <td>
		<table width="90%" border="0" cellpadding="2" cellspacing="2" class="adminform">
			<?php
			$k=1;
			for( $i=0; $i<count($elementTable->Fields); $i++ ) {
				$field =& $elementTable->Fields[$i];
				$originalValue = $field->originalValue;

				if( $field->Translate ) {
					$translationContent = $field->translationContent;

					// This causes problems in Japanese/Russian etc.
					mosMakeHtmlSafe( $translationContent );
				?>
		    <tr class="<?php echo "row$k"; ?>">
		      <th colspan="3"><?php echo JText::_('DBFIELDLABLE') .': '. $field->Lable;?></th>
		    </tr>
	      	<?php
	      	if (strtolower($field->Type)!='params'){
	      	?>
		    <tr class="<?php echo "row$k"; ?>">
		      <td align="left" valign="top"><JText>ORIGINAL</JText></td>
		      <td align="left" valign="top" id="original_value_<?php echo $field->Name?>">
		      <?php
		      	echo $field->originalValue;
		      ?>
		      </td>
			  <td valign="top">
				<input type="hidden" name="origValue_<?php echo $field->Name;?>" value='<?php echo md5( $field->originalValue );?>' />
				<?php if( strtolower($field->Type)!='htmltext' ) {?>
					<input type="hidden" name="origText_<?php echo $field->Name;?>" value='<?php echo $field->originalValue;?>' />
					<a class="toolbar" onclick="document.adminForm.refField_<?php echo $field->Name;?>.value = document.adminForm.origText_<?php echo $field->Name;?>.value;" onmouseout="MM_swapImgRestore();"  onmouseover="MM_swapImage('copy_<?php echo $field->Name;?>','','images/copy_f2.png',1);"><img src="images/copy.png" alt="<?php echo JText::_('copy');?>" border="0" name="copy_<?php echo $field->Name;?>" align="middle" /></a>
				<?php }	else { ?>
					<input type="text" id="text_origText_<?php echo $field->Name;?>" value='' style="position:absolute;left:-999px;top:-999px"/>
					<a class="toolbar" onclick="copyToClipboard('<?php echo $field->Name;?>','copy');" onmouseout="MM_swapImgRestore();"  onmouseover="MM_swapImage('copy_<?php echo $field->Name;?>','','images/copy_f2.png',1);"><img src="images/copy.png" alt="<?php echo JText::_('copy');?>" border="0" name="copy_<?php echo $field->Name;?>" align="middle" /></a>
				<?php  }?>
			  </td>
		    </tr>
		    <tr class="<?php echo "row$k"; ?>">
		      <td align="left" valign="top"><JText>Translation</JText></td>
		      <td align="left" valign="top">
					  <input type="hidden" name="id_<?php echo $field->Name;?>" value="<?php echo $translationContent->id;?>" />
						<?php
						if( strtolower($field->Type)=='text' || strtolower($field->Type)=='titletext' ) {
							$length = ($field->Length>0)?$field->Length:60;
							$maxLength = ($field->MaxLength>0)?$field->MaxLength:60;
							?>
							<input class="inputbox" type="text" name="refField_<?php echo $field->Name;?>" size="<?php echo $length;?>" value="<?php echo $translationContent->value; ?>" maxlength="<?php echo $maxLength;?>"/>

							<?php
						} else if( strtolower($field->Type)=='textarea' ) {
							$ta_rows = ($field->Rows>0)?$field->Rows:15;
							$ta_cols = ($field->Columns>0)?$field->Columns:30;
							?>
							<textarea name="refField_<?php echo $field->Name;?>" rows="<?php echo $ta_rows;?>" cols="<?php echo $ta_cols;?>" ><?php echo $translationContent->value; ?></textarea>
							<?php
						} else if( strtolower($field->Type)=='htmltext' ) {
							?>
							<?php
							$editorFields[] = array( "editor_".$field->Name, "refField_".$field->Name );
							// parameters : areaname, content, hidden field, width, height, rows, cols
							editorArea( "editor_".$field->Name,  $translationContent->value , "refField_".$field->Name, '100%;', '300', '70', '15' ) ;
						}
						?>
				</td>
				<td valign="top">
					<?php if( strtolower($field->Type)!='htmltext' ) {?>
						<a class="toolbar" onclick="document.adminForm.refField_<?php echo $field->Name;?>.value = '';" onmouseout="MM_swapImgRestore();"  onmouseover="MM_swapImage('clear_<?php echo $field->Name;?>','','images/delete_f2.png',1);"><img src="images/delete.png" alt="<?php echo JText::_('clear');?>" border="0" name="clear_<?php echo $field->Name;?>" align="middle" /></a>
					<?php } else {?>
					<!-- ToDo
						<a class="toolbar" onclick="editor_setHTML('refField_<?php echo $field->Name;?>', ''); document.adminForm.refField_<?php echo $field->Name;?>.value = '';" onmouseout="MM_swapImgRestore();"  onmouseover="MM_swapImage('clear_<?php echo $field->Name;?>','','images/delete_f2.png',1);"><img src="images/delete.png" alt="<?php echo JText::_('clear');?>" border="0" name="clear_<?php echo $field->Name;?>" align="middle" /></a>
					-->
					<a class="toolbar" onclick="copyToClipboard('<?php echo $field->Name;?>','clear');" onmouseout="MM_swapImgRestore();"  onmouseover="MM_swapImage('clear_<?php echo $field->Name;?>','','images/delete_f2.png',1);"><img src="images/delete.png" alt="<?php echo JText::_('clear');?>" border="0" name="clear_<?php echo $field->Name;?>" align="middle" /></a>

					<?php }?>
					</td>
		    </tr>
	      	<?php
	      	}
	      	else {
	      	?>
		    <tr class="<?php echo "row$k"; ?>">
		      <td colspan="3">
		      <table class='translateparams'>
			      <tr>
				      <td valign="top" style="text-align:center!important"><JText>ORIGINAL</JText></td>
				      <td valign="top" style="text-align:center!important"><JText>Translation</JText></td>
				      <td valign="top" align="right">
						<input type="hidden" name="origValue_<?php echo $field->Name;?>" value='<?php echo md5( $field->originalValue );?>' />
						<input type="hidden" name="origText_<?php echo $field->Name;?>" value='<?php echo $field->originalValue;?>' />
						<a class="toolbar" onclick="copyParams('orig', '<?php echo $field->Name;?>');" onmouseout="MM_swapImgRestore();"  onmouseover="MM_swapImage('copy_<?php echo $field->Name;?>','','images/copy_f2.png',1);">
						   <img src="images/copy.png" alt="<?php echo JText::_('copy');?>" border="0" name="copy_<?php echo $field->Name;?>" align="middle" />
						</a>
						<a class="toolbar" onclick="copyParams('defaultvalue', '<?php echo $field->Name;?>');"  onmouseout="MM_swapImgRestore();"  onmouseover="MM_swapImage('clear_<?php echo $field->Name;?>','','images/delete_f2.png',1);">
						    <img src="images/delete.png" alt="<?php echo JText::_('clear');?>" border="0" name="clear_<?php echo $field->Name;?>" align="middle" />
						</a>
					   </td>
			     </tr>
			     <tr>
			      <td align="left" valign="top" class="origparams" id="original_value_<?php echo $field->Name?>">
			      <?php
			      	$tpclass = "TranslateParams_".$elementTable->Name;
			      	if (!class_exists($tpclass)){
			      		$tpclass = "TranslateParams";
			      	}
		      		$transparams = new $tpclass($field->originalValue,$translationContent->value, $field->Name,$elementTable->Fields);
			      	$transparams->showOriginal();
			      	$transparams->showDefault();

			      ?>
			      </td>
			      <td align="left" valign="top" class="translateparams">
						  <input type="hidden" name="id_<?php echo $field->Name;?>" value="<?php echo $translationContent->id;?>" />
							<?php
							$retval = $transparams->editTranslation();
							if ($retval){
								$editorFields[] = $retval;
							}
							?>
					</td>
			      </tr>
		      </table>
		      </td>
		    </tr>
	      	<?php
	      	}
	      	?>
				<?php
				}
				$k=1-$k;
			}
				?>
		</table>
	  </td>
	  <td valign="top" width="30%">
		<?php
		$tabs = new mosTabs(0);
		$tabs->startPane("translation");
		$tabs->startTab(JText::_('PUBLISHING'),"ItemInfo-page");
	  ?>
  	<table width="100%" border="0" cellpadding="4" cellspacing="2" class="adminForm">
      <tr>
        <td width="34%"><strong><JText>TITLE_STATE</JText>:</strong></td>
        <td width="50%"><?php echo $actContentObject->state > 0 ? JText::_('STATE_OK') : ($actContentObject->state < 0 ? JText::_('STATE_NOTEXISTING') : JText::_('STATE_CHANGED'));?></td>
      </tr>
      <tr>
        <td><strong><?php echo JText::_('LANGUAGE');?>:</strong></td>
        <td><?php echo $langlist;?></td>
      </tr>
      <tr>
        <td><strong><?php echo JText::_('TITLE_PUBLISHED')?>:</strong></td>
        <td><input type="checkbox" name="published" value="1" <?php echo $actContentObject->published&0x0001 ? 'checked="checked"' : ''; ?> /></td>
      </tr>
      <tr>
        <td><strong><?php echo JText::_('TITLE_DATECHANGED');?>:</strong></td>
        <td><?php echo $actContentObject->lastchanged ? strftime("%A, %d %B %Y %H:%M",strtotime($actContentObject->lastchanged)) : JText::_('new');?></td>
      </tr>
	  </table>
	  <?php
	  $tabs->endTab();
	  $tabs->endPane();
		?>
	  <input type="hidden" name="select_language_id" value="<?php echo $select_language_id;?>" />
	  <input type="hidden" name="reference_id" value="<?php echo $actContentObject->id;?>" />
	  <input type="hidden" name="reference_table" value="<?php echo (isset($elementTable->name) ? $elementTable->name : '');?>" />
	  <input type="hidden" name="catid" value="<?php echo $catid;?>" />
	</td></tr>
	<?php
	HTML_joomfish::_JoomlaFooter($task, $act, $editorFields);
	}


	/**
	 * show and edit the config value translation page
	 *
	 * @param unknown_type $itemsToTranslate
	 * @param unknown_type $language
	 */
	function translateConfig( $itemsToTranslate, $language ){
		global $my,  $act, $task, $database, $option, $select_language_id, $mosConfig_live_site;
		HTML_joomfish::_JoomlaHeader(JText::_("Translate Configuration : ").$language->name);
		$langParams = new mosParameters( $language->params );
         ?>
  <input type="hidden" name="lang" value="<?php echo $language->id;?>" />
   <table width="100%" border="1" cellpadding="4" cellspacing="2" class="adminlist">
   	  <tr>
   	  	<th width="20%"><JText>Config Field</JText></th>
   	  	<th ><JText>Original</JText></th>
   	  	<th ><JText>Translation</JText></th>
   	  </tr>
	<?php
		$k=0;
		foreach ($itemsToTranslate as $item=>$type) {
			echo "<tr class='row$k'>";
			echo	"<td><JText>Translate_".$item."</JText></td>";
			echo	"<td>".$GLOBALS["mosConfig_".$item]."</td>";
			switch ($type) {
				case "text":
					echo "<td><input class='inputbox' type='text' name='transConf$item' size='80' value='". $langParams->get($item,"")."'></td>";
					break;
				case 'textarea':
					echo "<td><textarea class='text_area' name='transConf$item' cols='55' rows='4'>". $langParams->get($item,"")."</textarea></td>";
					break;
				default:
					echo "<td>$type  : ".JText::_("Not yet supported")."</td>";
					break;
			 }
			echo "<tr/>\n";
			$k=($k+1)%2;
		}
	?>
    </table>
		<?php
		HTML_joomfish::_JoomlaFooter('save',$act, $option);

	}

	/**
	 * show the Orphan translations
	 *
	 * @param array $rows
	 * @param array $catid	category id's
	 */
	function showOrphan($rows, $catid)
	{
		global $my, $act, $task, $database, $option, $select_language_id, $mosConfig_live_site;
		HTML_joomfish::_JoomlaHeader("Orphan Detail");
         ?>
  <input type="hidden" name="cid[]" value="<?php echo $rows[0]->id."|".$rows[0]->reference_id."|".$rows[0]->language_id; ?>"/>
   <input type="hidden" name="catid" value="<?php echo $catid;?>" />
   <table width="100%" border="1" cellpadding="4" cellspacing="2" class="adminForm">
	<tr align="center" valign="middle">
	      <th width="10%" align="left" valign="top"><JText>DBFIELDLABLE</JText></th>
	      <th width="12%" align="left" valign="top"><JText>Original</JText></th>
	      <th width="78%" align="left" valign="top"><JText>Translation</JText></th>
        </tr>
        <?php
        $style1="style='background-color:rgb(241,243,245)'";
        $style2="style='background-color:rgb(255,228,196)'";
        $style=$style1;
        ?>
		<tr align="center" valign="middle" <?php echo $style; ?>>
	      <td align="left" valign="top"><?php echo "Debug Info";?></td>
	      <td colspan="2" align="left" valign="top"><?php echo "Original Table:<b>".$rows[0]->reference_table."</b> === Orginal Id: <b>".$rows[0]->reference_id."</b>";?></td>
        </tr>
        <?php
        foreach ($rows as $row) {
        	$style=$style==$style1?$style2:$style1;
		?>
        <tr align="center" valign="middle" <?php echo $style; ?>>
	      <td align="left" valign="top"><?php echo $row->reference_field;?></td>
	      <td align="left" valign="top"><?php echo $row->original_value;?></td>
	      <td align="left" valign="top"><?php echo $row->value;?></td>
	    </tr>
	    <?php
        }
	    ?>
   </table>

		<?php
		HTML_joomfish::_JoomlaFooter('show', $act, $option);
	}

	/**
	 * Show the upgrade dialog
	 *
	 */
	function showUpgradeInstall( $joomFishManager, $upgradeSucessful, $upgradeDone, $lists ) {
		global $act, $task, $option, $mosConfig_live_site;
		HTML_joomfish::_JoomlaHeader( JText::_('UPGRADE INSTALLATION'), 'upgrade' );

		if( !$upgradeSucessful && !$upgradeDone ) {
		?>
	    <tr>
	      <td width="30%" align="left" valign="top">&nbsp;</td>
	      <td align="left" valign="top"><JText>UPGRADE_INFO</JText></td>
	    </tr>
	    <tr>
	      <td width="30%" align="left" valign="top"><strong><JText>UPGRADE_DELETE_TABLES</JText></strong></td>
	      <td><?php echo mosHTML::yesnoRadioList( 'confirmDelete', '', $lists['confirmDelete'] );?></td>
	    <tr>
	    <tr><td colspan="2">&nbsp;</td></tr>
	    <tr>
	      <td width="30%" align="left" valign="top"><strong><JText>UPGRADE_BACKUP_TABLES</JText></strong></td>
	      <td><?php echo mosHTML::yesnoRadioList( 'backupTables', '', $lists['backupTables'] );?></td>
	    <tr>
	    <tr>
	      <td width="30%" align="left" valign="top"><strong><JText>UPGRADE_RENAME_TABLES</JText></strong></td>
	      <td><?php echo mosHTML::yesnoRadioList( 'renameTables', '', $lists['renameTables'] );?></td>
	    <tr>
	    <?php
		} else {
			$summary = $lists['summary'];
		?>
		<tr>
			<th>&nbsp;</th>
			<th colspan="2"><JText>TITLE_STATE</JText></th>
		</tr>
		<tr>
			<td width="30%"><JText>TITLE_STATE</JText>:</td>
			<td width="30%"><?php echo ($summary['state'] ? JText::_('UPGRADE_SUCCESSFUL') : JText::_('UPGRADE_FAILURE') );?></td>
			<td><?php if( key_exists( 'message', $summary ) ) echo $summary['message'];?></td>
		</tr>
		<tr>
			<td><JText>MBFBOT</JText>:</td>
			<td><?php echo ($summary['mbf_bot_unpublished'] ? JText::_('MBF_UNPUBLISHED') : JText::_('MBF_NOTUNPUBLISHED') );?></td>
			<td>
			<?php echo ($summary['mbf_bot_unpublished'] ? '' : $summary['mbf_bot_unpublished_error']);?>
			</td>
		</tr>
		<tr>
			<td><JText>MBFMODULE</JText>:</td>
			<td><?php echo ($summary['mbf_module_unpublished'] ? JText::_('MBF_UNPUBLISHED') : JText::_('MBF_NOTUNPUBLISHED') );?></td>
			<td>
			<?php echo ($summary['mbf_module_unpublished'] ? '' : $summary['mbf_module_unpublished_error']);?>
			</td>
		</tr>
		<tr><td>&nbsp;</tr>
		<?php if( $lists['backupTables'] ) { ?>
		<tr>
			<td><JText>CONTENT_BACKUP</JText>:</td>
			<td>
			<?php echo ($summary['jf_content_backup'] ? JText::_('BAK_CONTENT_SUCESSFUL') : JText::_('BAK_CONTENT_FAILURE') );?><br />
			<?php echo ($summary['jf_languages_backup'] ? JText::_('BAK_LANGUAGES_SUCESSFUL') : JText::_('_BAK_LANGUAGES_FAILURE') );?>
			</td>
			<td>
			<?php echo ($summary['jf_content_backup'] ? '' : $summary['jf_languages_backup_error']);?>
			<?php echo ($summary['jf_languages_backup'] ? '' : $summary['jf_languages_backup_error']);?>
			</td>
		</tr>
		<tr><td>&nbsp;</tr>
		<?php } ?>
		<tr>
			<td><JText>CONTENT_TABLES</JText>:</td>
			<td>
				<?php echo ($summary['jf_content_deleted'] ? JText::_('DEL_SUCESSFUL') : JText::_('DEL_FAILURE') );?><br />
				<?php echo ($summary['jf_content_copied'] ? JText::_('COPY_SUCESSFUL') : JText::_('COPY_FAILURE') );?>
			</td>
			<td>
			<?php echo ($summary['jf_content_deleted'] ? '' : $summary['jf_content_deleted_error']);?>
			<?php echo ($summary['jf_content_copied'] ? '' : $summary['jf_content_copied_error']);?>
			</td>
		</tr>
		<tr>
			<td><JText>LANGUAGE_TABLES</JText>:</td>
			<td>
				<?php echo ($summary['jf_languages_deleted'] ? JText::_('DEL_SUCESSFUL') : JText::_('DEL_FAILURE') );?><br />
				<?php echo ($summary['jf_languages_copied'] ? JText::_('COPY_SUCESSFUL') : JText::_('COPY_FAILURE') );?>
			</td>
			<td>
			<?php echo ($summary['jf_languages_deleted'] ? '' : $summary['jf_languages_deleted_error']);?>
			<?php echo ($summary['jf_languages_copied'] ? '' : $summary['jf_languages_copied_error']);?>
			</td>
		</tr>
		<?php
		}

		HTML_joomfish::_JoomlaFooter( 'show', $act, $option );
	}

	function showInformation( $fileCode='TN' ) {
		global $act, $task, $option, $mosConfig_live_site;
		HTML_joomfish::_JoomlaHeader( 'Joom!Fish documentation', 'credits', '' );
		?>
	<tr align="center" valign="middle">
      <td align="left" valign="top" width="70%">
		<h2>PREAMBLE</h2>
		The JoomFish is an extention for the open source CMS Joomla!.<br />
		Joomla! is Copyright (C) 2005 Open Source Matters. All rights reserved.<br />
		license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php<br />
		Joomla! is free software. This version may have been modified pursuant<br />
		to the GNU General Public License, and as distributed it includes or<br />
		is derivative of works licensed under the GNU General Public License or<br />
		other free or open source software licenses.<br />
		See /COPYRIGHT.php for copyright notices and details.<br />
		&nbsp;<br />
		Within this license the "product" refers to the name "Joom!Fish" or "Mambel Fish".<br />
		Also the term "Joom!Fish - Joomla! Babel Fish" must not be used by any derived software.

					<?php
					switch ( $fileCode ) {
						case "changelog":
					?>
					<h2>Changelog</h2>
					<?php
					echo nl2br(file_get_contents(dirname( __FILE__) ."/documentation/CHANGELOG.php"));
					break;

						case "license":
					?>
					<h2>Think Network Open Source License</h2>
					<?php
					echo nl2br(file_get_contents(dirname( __FILE__) ."/documentation/LICENSE.php"));
					break;

						case "readme":
						default:
					?>
					<h2>Read ME</h2>
					<?php
					echo nl2br(file_get_contents(dirname( __FILE__) ."/documentation/ReadMe.php"));
					break;
					}
			?>
		</td>
		<td align="left" valign="top" nowrap>
			<?php HTML_joomfish::_sideMenu();?>
			<?php HTML_joomfish::_creditsCopyright(); ?>
		</td>
	</tr>
		<?php
		HTML_joomfish::_footer($task, $option);
	}

	/**
	 * Show the side menu
	 *
	 */
	function _sideMenu() {
		global $mosConfig_live_site;
  	?>
		<img src="<?php echo $mosConfig_live_site;?>/administrator/components/com_joomfish/images/joomfish_slogan.png" border="0" alt="<?php echo JText::_('Language Title');?>"  />
		<p><span class="contentheading"><JText>Related topics</JText>:</span>
		<ul>
			<li><a href="http://www.joomfish.net" target="_blank"><JText>Official Project WebSite</JText></a></li>
			<li><a href="http://www.joomfish.net/forum/" target="_blank"><JText>Official Project Forum</JText></a></li>
			<li><a href="http://joomlacode.org/gf/project/joomfish/tracker/" target="_blank"><JText>Bug and Feature tracker</JText></a></li>
		</ul>
		</p>
		<p><span class="contentheading"><JText>Documentation and Tutorials</JText>:</span>
		<ul>
			<li><a href="http://www.joomfish.net/joomfish-documentation-overview.html" target="_blank"><JText>Online Documentation and Tutorials</JText></a></li>
			<li><a href="index2.php?option=com_joomfish&amp;task=postInstall"><JText>Installation notes</JText></a></li>
			<li><a href="index2.php?option=com_joomfish&amp;task=showInformation&amp;fileCode=changelog"><JText>Changelog</JText></a></li>
		</ul>
		</p>
		<p><span class="contentheading"><JText>License</JText>:</span>
		<ul>
			<li><a href="index2.php?option=com_joomfish&amp;task=showInformation&amp;fileCode=license">GPL based Think Network Open Source license</a></li>
		</ul>
		</p>
		<p><span class="contentheading"><JText>Additional Sites</JText>:</span>
		<ul>
			<li><a href="http://www.joomla.org" target="_blank">Joomla!</a></li>
		</ul>
		</p>
  	<?php
	}


	function _header() {
		HTML_joomfish::_pageHeader();
		?>
    <script language="javascript" type="text/javascript">
  	/* <![CDATA[ */
    function submitbutton(pressbutton) {
    	var form = document.adminForm;
    	if (pressbutton == 'cancel') {
    		submitform( pressbutton );
    		return;
    	} else {
    		submitform( pressbutton );
    	}
    }
    /* ]]> */
    </script>
	<div id="joomfish">
    <form action="index2.php" method="post" name="adminForm">
	<table width="90%" border="0" cellpadding="2" cellspacing="2" class="adminheading">
	<tr><th width="100%" align="left" colspan="2">
		<JText>JOOMFISH_HEADER</JText>
	</th></tr>
	</table>
	<table width="90%" border="0" cellpadding="2" cellspacing="2" class="adminform">
	<?php
	}

	function _footer($task, $option) {
	?>
    </tr>
  </table>

	  <input type="hidden" name="task" value="<?php echo $task; ?>" />
	  <input type="hidden" name="option" value="<?php echo $option; ?>" />
    </form>
    </div>
	<?php
		HTML_joomfish::_pageFooter();
	}

	/**
	 * The page header generates a general output handling for all admin pages
	 *
	 */	function _pageHeader(){
		global $mainframe;
		$cssHTML = '<link rel="stylesheet" href="' .$GLOBALS['mosConfig_live_site']. '/administrator/components/com_joomfish/css/joomfish.css" type="text/css" />' . "\n";
		$mainframe->addCustomHeadTag( $cssHTML );
		ob_start();
	}

	/**
	 * The page footer ensures a final translation of the content and outputs the page
	 *
	 */
	function _pageFooter() {
		$content = ob_get_contents();
		ob_end_clean();
		// define the regular expression for the translation
		$regex = "|<JText>(.*?)</JText>|";
		$content = preg_replace_callback($regex,"jfTranslate",$content);
		echo $content;
	}


	function _JoomlaHeader( $title, $title_class='', $headingOptions= 'fish', $adminform = true ) {
		HTML_joomfish::_pageHeader();
	?>
	    <div id="overDiv" style="position:absolute; visibility:hidden; z-index:10000;"></div>
	    <div id="joomfish">
	    <form action="index2.php" method="post" name="adminForm">
	    <?php HTML_joomfish::_JAdminHeading( $title, $title_class, $headingOptions );?>
		<?php
		if ( $adminform ) {
			HTML_joomfish::_openAdminForm();
		}
	}

	function _JAdminHeading( $title, $title_class, $headingOptions = 'fish' ) {
		global $mosConfig_live_site;
  	?>
	<table width="90%" border="0" cellpadding="2" cellspacing="2" class="adminheading">
		<tr>
			<th width="100%" class="<?php echo $title_class;?>" align="left" colspan="2"><?php echo $title;?></th>
			<?php if( $headingOptions == 'fish' ) { ?>
		    <td><img src="<?php echo $mosConfig_live_site;?>/administrator/components/com_joomfish/images/fishl.png" alt="" border="0" /></td>
		    <?php
			} elseif ( $headingOptions != '' ) {
				echo $headingOptions;
			}
		    ?></tr>
	</table>
	<?php
	}

	function _openAdminForm() {
  	?>
	<table width="90%" border="0" cellpadding="2" cellspacing="2" class="adminform" >
  	<?php
	}

	function _JoomlaFooter($task, $act, $editorFields=null) {
		global $mosConfig_live_site;
	?>
    </table>
    <input type="hidden" name="hidemainmenu" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="task" value="<?php echo $task; ?>" />
	<input type="hidden" name="act" value="<?php echo $act; ?>" />
	<input type="hidden" name="option" value="com_joomfish" />
    </form>
	<script  type="text/javascript" src="<?php echo $mosConfig_live_site;?>/includes/js/overlib_mini.js"></script>
    <script language="javascript" type="text/javascript">
    function submitbutton(pressbutton) {
    	var form = document.getElementsByName ('adminForm');
    	<?php
    	if( isset($editorFields) && is_array($editorFields) ) {
    		foreach ($editorFields as $editor) {
    			// Where editor[0] = your areaname and editor[1] = the field name
    			getEditorContents( $editor[0], $editor[1]) ;
    		}
    	}
    	?>
    	if (pressbutton == 'cancel') {
    		submitform( pressbutton );
    		return;
    	} else {
    		submitform( pressbutton );
    	}
    }
    </script>
    <table width="100%" border="0">
	<tr>
	  <td width="99%" align="right" valign="top">
		<?php
		$x = "@";
		$y="Support";
		$z="JoomFish.net";
		$mail=$y.$x.$z;
		?><div align="center"><span class="smallgrey">Joom!Fish Version <?php echo JoomFishManager::getVersionFooter();?>, &copy; 2003-2007 Copyright by <a href="http://www.ThinkNetwork.com" target="_blank" class="smallgrey">Think Network</a> under <a href="index2.php?option=com_joomfish&amp;task=showInformation&amp;fileCode=license" class="smallgrey">Open Source License.</a> Contact: <?php echo mosHTML::emailCloaking( $mail, 0);?></span></div>
	  </td>
	  </tr>
	</table>
	</div>
	<?php
		HTML_joomfish::_pageFooter();
	}

	function _creditsCopyright() {
		?>
		<p>
		<span class="smallgrey"><strong>Credits:</strong></span><br />
		<span class="smallgrey">To all the people of the JoomFish community!<br />
		Present development team:
		<ul>
			<li>Alex Kempkens (<?php
			$x = "@";
			$y="Alex";
			$z="JoomFish.net";
			$mail=$y.$x.$z;

			$mode=0;
			echo mosHTML::emailCloaking( $mail, $mode );
			?>)</li>
			<li>Geraint Edwards (<?php
			$x = "@";
			$y="joomfish.credits";
			$z="copyn.plus.com";
			$mail=$y.$x.$z;

			$mode=0;
			echo mosHTML::emailCloaking( $mail, $mode );
			?>)</li>
			<li>Ivo Apostolov (<?php
			$x = "@";
			$y="ivo";
			$z="joomfish.net";
			$mail=$y.$x.$z;

			$mode=0;
			echo mosHTML::emailCloaking( $mail, $mode );
			?>)</li>
			<li>Robin Muilwijk</li>
		</ul>
		<br />

		Logo design by:
		<ul>
			<li>Tommy White (<?php
			$x = "@";
			$y="tommy";
			$z="tommywhite.com";
			$mail=$y.$x.$z;

			$mode=0;
			echo mosHTML::emailCloaking( $mail, $mode );
			?>)</li>
		</ul>

		&nbsp;<br />
		Special thank's for testing, good suggestions & translations to:<br />
		Bernhard, Michael, Luc, Olivier, Robin, Rune, Victor, Akarawuth</span><br />

		&nbsp;<br />
		<span class="smallgrey"><strong>Contact:</strong></span><br />
		<?php
		$x = "@";
		$y="Support";
		$z="JoomFish.net";
		$mail=$y.$x.$z;

		echo '<span class="smallgrey">' .mosHTML::emailCloaking( $mail, 1,  $mail) .'</span>';
		?>
		<br />
		&nbsp;<br />
		<span class="smallgrey"><strong>Version:</strong></span><br />
		<span class="smallgrey"><?php echo JoomFishManager::getVersion();?></span><br />
		&nbsp;<br />
		<span class="smallgrey"><strong>Copyright:</strong></span><br />
		<span class="smallgrey">&copy; 2003-2007 </span><a href="http://www.ThinkNetwork.com" target="_blank" class="smallgrey"><span class="smallgrey">Think Network, Munich</span></a><br />
		<a href="index2.php?option=com_joomfish&amp;task=showInformation&amp;fileCode=license" class="smallgrey"><span class="smallgrey">Open Source License.</span></a>
		</p>
		<?php
	}

	/**
	 * This method creates a standard cpanel button
	 *
	 * @param unknown_type $link
	 * @param unknown_type $image
	 * @param unknown_type $text
	 */
	function _quickiconButton( $link, $image, $text, $path='/administrator/images/', $target='', $onclick='' ) {
		if( $target != '' ) {
			$target = 'target="' .$target. '"';
		}
		if( $onclick != '' ) {
			$onclick = 'onclick="' .$onclick. '"';
		}
		?>
		<div style="float:left;">
			<div class="icon">
				<a href="<?php echo $link; ?>" <?php echo $target;?>  <?php echo $onclick;?>>
					<?php echo mosAdminMenus::imageCheckAdmin( $image, $path, NULL, NULL, $text ); ?>
					<span><?php echo $text; ?></span>
				</a>
			</div>
		</div>
		<?php
	}

	/**
	 * Method to truncate a original text to a certain length
	 * When the original is longer than the text '...' is added
	 * @param string	original text
	 * @param int		max text length
	 * @return string	truncated text
	 */
	function _truncateString($origText, $maxLength) {
		$retString = '';
		if(strlen($origText) > $maxLength) {
			$retString = substr($origText, 0, $maxLength);
			$retString .= ' ...';
		} else {
			$retString = $origText;
		}
		return $retString;
	}
}
?>
