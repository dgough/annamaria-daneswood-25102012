<?php
/**
 * Joom!Fish - multilingual extention and translation manager for Joomla!
 * Copyright (C) 2003-2007 Think Network GmbH, Munich
 *
 * All rights reserved.  The Joom!Fish project is a set of extentions for
 * the content management system Joomla!. It enables Joomla!
 * to manage multilingual sites especially in all dynamic information
 * which are stored in the database.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * The "GNU General Public License" (GPL) is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * -----------------------------------------------------------------------------
 * $Id: joomfish.help.inc.php 727 2007-10-22 09:06:04Z akede $
 *
*/

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

class HTML_joomfish_help {

	function showWelcome() {
		global $act, $task, $option, $mosConfig_live_site;
		HTML_joomfish::_JoomlaHeader( JText::_('JOOMFISH_TITLE'). ' '.JText::_('JOOMFISH_CREDITS'), 'credits', false );
    ?>
	<tr align="center" valign="middle">
      <td align="left" valign="top" width="70%">
		<p>
      	<span class="moduleheading">����� ����� � ����!���</span></p>

        <strong>����!��� - ������? ���������� �� ����������� ���������� - �� ����� ��������? ����� �� ���� � ����� ���?</strong>
		<p>��, ���� � �������! <b>���� �� � �������� �� ����������� ������, ����� �� �������� �� ����� ��������!</b> ����� ���� �� ����� �� ��� ������!</p>
		<p>�� ��� ������ ���� ������� ����?<br />
		<strong>��!</strong><br />
		&nbsp;<br />
		���, ���� � ������ - ���� �� ������ �� � ������� ������?<br />
		�������� ... - ��! <br />
		&nbsp;<br />
		<strong>�� �� ���� �����? ...</strong><br />
		���-�������, ����� ������ �� ��������� �: <a href="http://www.amazon.de/exec/obidos/ASIN/0345391802/thinknetwork-21" target="_blank">�� ��������� ���, �� ���������, �� �� �������� � �� ���������!</a><br />
		&nbsp;<br />
		������ �����, � �������� ������ �������������� ���������� �� ���� ��������� � �� ���� ��������� ����� ;-)</p>
		<p>
		<strong>��, ������� ��!</strong><br />
		������, ������ ���� � "��������� ������ �� ��������" � �����������.<br />
		������ �� �� <a href="index2.php?option=com_joomfish&task=help">��������� ������</a>?<br />
		&nbsp;<br />
		�� ��� � ����������, �� �������� ����!���<br>

		<p>
		<span class="moduleheading">�������</span><br />
		�� ������ ���, ����� ��� ��� �� ��������� �� ����� ������. ������ ���������� �� ��������� �� ������ �����!<br />
		<br />
		����� ���� ���� �� ��������� ������������ �� ��������� �� ������ ����������� �� ��������� � ����!���, ��� ���� ��� ������ � �������� �� ����, ���� ��� ���������� �� �� ���� �� ���-������� ��� ��������� �� ������!<br /> ���� ��, �� �� ������� �� �� ��������� � ������ ���� � �����������, ��� ����� �� �������� ������� ��� � ���.
		</p>
		<p>
		������������� ��� ����� �� ����!���, ������ ��������� � ���� �� ����������� ������ �� ������.
		&nbsp;<br />
		�����</p>
		</p>
		</td>
		<td align="left" valign="top" nowrap>
			<?php HTML_joomfish::_sideMenu();?>
			<?php HTML_joomfish::_creditsCopyright(); ?>
		</td>
	<?php
		HTML_joomfish::_footer($task, $option);
  }


	function showHelp() {
		global $act, $task, $option, $mosConfig_live_site;
		HTML_joomfish::_JoomlaHeader( JText::_('JOOMFISH_TITLE'). ' ' .JText::_('HELP AND HOWTO'), 'support', false );
    ?>
	<tr align="center" valign="middle">
      <td align="left" valign="top" width="70%">
		<h2>�� ���� �� ��������?</h2>
	   <p>������ �� ��� �� ������ ���� � �������� �� ��������� ���� �� ������� ����� ����������� ������? ����!��� � ���� �� ���������� ��������.<br />
		&nbsp;<br />
		����������� �� ��������� �� ����������� ������������ � �����, ������������ � ��������. ���������� � ������� ������, �� ���� ������������� �� ����� ����������, ������ �� �������� ������� �����, �� �� ������ �������� �� ��������� � ���.<br />
		&nbsp;</p>
		
		<h2>����� ��������� ��� ����������?</h2>
		<p>������ ������� �� ����������, ����� ������� ����� ��� ����� ����������, ����� � �������� � ������ ����� �� ������! ��� ����� ��������, ����� �� �������� � ��������� ������� �� ������! ���� ������� �� ����� ���������� �� ��������� ���������� � ��������� �� �������� �� ����������� ����. 
		<strong>��</strong>
		��������, �������� � ������ �������� <em>����������</em> ����� ��������� � ���� �� ����� �� ����� ��� �� ������ :-(.<br />
		&nbsp;<br />
		����!��� � ������� �� �� �� ��������� �� ��������� ������������ �� ;-) � �� ���� ����� �� ��������� ����� �� �����������. ����� ��������� �� ������������ ��� �������� ������� �� ������������.<br />
		&nbsp;</p>

		<h2>����� ����� ���������� �� ���?</h2>
		��� �� ������ �� �������� ����� �� ������� ������ ��� ���� ������� �� ���������� ������.
		����� � �� ����������� ������� �� ������, ����� ��� �� ������� � ����� ��-������ �����.
		� ������� � ����� ��� ����� ������ ��������� � ������ �� ����������, ����� ������ �� ���������� ������� �� ������.<br />
		&nbsp;<br />
		� ����!��� ���� ���� ���� �� ������� ������ ���������� � �� ����, ��� ����� �� ���� �� �� ���������. 
		��������� �� �� ���� �����, ������ �� ��������� ������ ���������� �� �����, ����� ��� �������.</p>

		<h2>��� ������?</h2>
		<p>����� �����. � ������ ����������, ������ �� ������ <a href="index2.php?&task=show&act=config_component&hidemainmenu=1">"���������"</a> � �� ��������� ������ ��������� �� ����������. ���� � ���� ��������.<br />
		��������� ����� ������ �� ��������� � ������� � �� ����� � ��� ����� ������ �� ���� ���������� ������������ �� ����� ��.</p>

		<p>������� ���� � �� ������. ����������� �� ���������� �� ������������ �� �������� �������� � ������ �����. ������ ���� ������� ��� ������� �� �������� ������������ �� ������� �������������� ���� ����������� �� ����������!
		���� ��������������� ������ �� ������ <a href="index2.php?option=com_joomfish&task=config_elements">"����������� �� ����������"</a> � ������ ����������� ���������. ���� ��������� �� �������� � XML �������, ����� �� ������� � ���-������� "contentelements" � ����������������� ����� �� ����!���.<br />
		��� ������ �� �������� ��� ������� ��� �� ��������� ����������� �����, ���� �� �� �������� �� � �������� ������.<br />
		���-�������, ����� ������ �� ��������� � ���� XML ��������� ����� ��� ���������� ���������� ;-)<br />
		&nbsp;<br />
		��� �������� ��� XML ���� �� ��� ��� �� ������� ���������/�����, ������ �������, �� ��� ��������� ��������� � � ������������ PHP ���������.</p>

		<p>�� �� ��������� ������� �� ����������, ������ ��������� ������ <a href="index2.php?option=com_joomfish&task=translate">"������"</a>. ��� �� �������� ������� ��� ������������ �� ������ �� �����. ���� ���������� �� ������� � ����, ����� ������ �� ��������� � �������.</p>
	   &nbsp;<br />
		 <p>���� � ������ �� ����. ����, �������� � �.�. - ������ �� ��������� �-����.<br>
	   &nbsp;<br />
	   �����</p>		</td>
		<td align="left" valign="top" nowrap>
			<?php HTML_joomfish::_sideMenu();?>
			<?php HTML_joomfish::_creditsCopyright(); ?>
		</td>
	</tr>

<?php
		HTML_joomfish::_footer($task, $option);
  }

  function showMambotWarning(){
		global $act, $task, $option, $mosConfig_live_site, $_VERSION;
		HTML_joomfish::_header();
		?>
     <td align="left" valign="top">
       <h2 style="color:red">���, ��� ������� � ������������</h2>
       <p>������������ "<?php echo mosMainFrame::getBasePath();?>/mambots/system/" � ��� ����� �� �����</p>
       <p>����:</p>
       <ul>
       <li>��-������������ ����!���</li>
       <li>��������� ������� ����� ������������</li>
       <li>���������������</li>
       </ul>
       <br/>���������� ��.</p>
       </td>
	<?php
		HTML_joomfish::_footer($task, $option);

  }
	function showPostInstall( $success=false ) {
		global $act, $task, $option, $mosConfig_live_site, $_VERSION;
		HTML_joomfish::_header();
	?>
     <td align="left" valign="top">
       <h2>����� ����� � ����!���</h2>
       <p>
       ����������� ����!��� �� ��������� �� ��������� ����������� ����.<br />
       ������ ���������� �� ����� ���� �� � ����� ������, �� ���� � ���������� �� � �������� ����� �� ����������.</p>   ���� ��������� ������������ ����������, ���� ����� �� �������� �� ��������� � �������.
       ������ �� �������� ����� � ��������� � <a href="http://www.joomfish.net" target="_blank">����� �� ����!���</a>.

       <p>���� ������ �� ����!��� ������ ���������� �� ������ 1.0.7+. ��� �������� �������� ������, ���� �������� ���� �� 1.0.7.</p>

	   <h3>1. ������: ����������� �� ����</h3>
	   <p>
	   ����� ������ �� ����������� ������������ �����.
	   ������ ����� �� ����� ������� � ����� �� <a href="http://extensions.joomla.org" target="_blank">������!</a>.<br />
	   ���� ������������ ������� ���� ������������ <a href="/administrator/index2.php?option=com_installer&element=language">���������� �� ������!</a>.</p>

	   <h3>2. ������: ��������� �� �������</h3>
	   <p>���� ���� ��� ������ � �������������, ������ �� ���������� �������� �����. ���� ����� ����
	   <a href="/administrator/index2.php?option=com_joomfish&task=config_language">���������� �� ����� � ����!���</a>.<br />
	   &nbsp;<br />
	   <b>���� ���������:</b> ������ ��������� ����� � �����, ��������� �������� ������ �� �� ��������.</p>

	   <h2>��� �� ������� ������������?</h2>
	   <p>���� ���� ��� ��������� �������, ����� �� �����������, �� ������������ � ��������� � ������ ����� � ������ �� ���� ��������� �� ���.</p>

	   <h3>3. ������: ������ �� ������������ � ������ �����</h3>
	   <p>���� ������ �� �������� �� ��������� ������� �� �������� � �����.<br />
	   ������� � <a href="index2.php?option=com_joomfish">����������� �����</a> � �������� ��������� ������.<br />
	   � �������� �� ���������, ���� ����� �������� ����� �� ����� ������ �� ��������� � ���� ���� ��������.<br />
	   ������ �� ���������� � ���������� ������. ���� ��������� ����� �� ������ ���� �� �������� � ����� �� ��������. ����� � �� ����������� �������.<br />
	   &nbsp;<br />
	   <b>���� ���������:</b> ��� ����� �� ����� � �����, ������ �� �� �������� � ������� ��������.</p>

   	   <h3>4. ������: ������ �� ������������ ����������</h3>
   	   <p>�� ������ ��������� ����������, ������ ��������� �������� � ������ 3.
   	   �� ����� ���������� (����� �� �� � ������), ������ <a href="index2.php?option=com_joomfish&task=config_elements">�� ����������� ������������ ��������</a>.</p>

	   <h2>������ �� ������� ����!���</h2>
	   <p>���� ������ � �������� � ������ �� ������! 1.0.<br />
	   �� ������! 1.5 ��� �� ��������� ���� ������, �������� �� ������ ���������.
	   �������������� � ���� �������, �� �� ������ �������� �����.
	   ������������ ������� ������ ��� ����.
	   </p>

	   <p>��� ����� ������� �������, ���� ����������� ������ ��, �� �� �� ����������� ������ � ����!���.<br>
		 &nbsp;<br />
		������ �� ����!���<br>
		 &nbsp;<br />
		 �����, �������, ��� � ���
		 </p>
	   </td>
		<td align="left" valign="top" nowrap>
			<?php HTML_joomfish::_sideMenu();?>
			<?php HTML_joomfish::_creditsCopyright(); ?>
		</td>
<?php
		HTML_joomfish::_footer($task, $option);
  }
}
?>
