<?php
/**
 * Joom!Fish - multilingual extention and translation manager for Joomla!
 * Copyright (C) 2003-2007 Think Network GmbH, Munich
 *
 * All rights reserved.  The Joom!Fish project is a set of extentions for
 * the content management system Joomla!. It enables Joomla!
 * to manage multilingual sites especially in all dynamic information
 * which are stored in the database.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * The "GNU General Public License" (GPL) is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * -----------------------------------------------------------------------------
 * $Id: joomfish.help.inc_bulgarian.php 754 2007-11-11 02:17:54Z apostolov $
 *
*/

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

class HTML_joomfish_help {

	function showWelcome() {
		global $act, $task, $option, $mosConfig_live_site;
		HTML_joomfish::_JoomlaHeader( JText::_('JOOMFISH_TITLE'). ' '.JText::_('JOOMFISH_CREDITS'), 'credits', false );
    ?>
	<tr align="center" valign="middle">
      <td align="left" valign="top" width="70%">
		<p>
      	<span class="moduleheading">Добре дошли в Джум!Фиш</span></p>

        <strong>Джум!Фиш - превод? Управление на мултиезично съдържание - За какво говорите? Какво за бога е бабел фиш?</strong>
		<p>ОК, едно е СИГУРНО! <b>Това не е средство за автоматичен превод, което се предлага от някой компании!</b> Името вече бе взето от тях назаем!</p>
		<p>Но Вие знаете Форд Перфект нали?<br />
		<strong>НЕ!</strong><br />
		&nbsp;<br />
		Уфф, това е трудно - може ли живота да е толкова труден?<br />
		Всъщност ... - да! <br />
		&nbsp;<br />
		<strong>Да но сега какво? ...</strong><br />
		Най-доброто, което можете да направите е: <a href="http://www.amazon.de/exec/obidos/ASIN/0345391802/thinknetwork-21" target="_blank">да натиснете тук, да прочетете, да се посмеете и да разберете!</a><br />
		&nbsp;<br />
		Бъдете бързи, в противен случай галактическата магистрала ще бъде построена и ще бъде прекалено късно ;-)</p>
		<p>
		<strong>Да, разбира се!</strong><br />
		Готини, Вашата маса в "Ресторант накрая на вселента" е резервирана.<br />
		Искате ли да <a href="index2.php?option=com_joomfish&task=help">прочетете менюто</a>?<br />
		&nbsp;<br />
		За нас е удовлствие, че ползвате Джум!Фиш<br>

		<p>
		<span class="moduleheading">Кредити</span><br />
		За всички Вас, които все още не разбирате за какво говоря. Трябва обезателно за започнете да четете книги!<br />
		<br />
		Искам също така да използвам възможността да благодаря на всички потребители на Мамбелфиш и Джум!Фиш, тъй като Вие стоите в основата на това, тези две приложения да се едни от най-важните във вселената на Джумла!<br /> Моля Ви, да не спирате да ни тормозите с Вашите идеи и предложения, как можем да подобрим рибката още и още.
		</p>
		<p>
		Благодарности към екипа на Джум!Фиш, всички преводачи и Томи за невероятния дизайн на рибата.
		&nbsp;<br />
		Алекс</p>
		</p>
		</td>
		<td align="left" valign="top" nowrap>
			<?php HTML_joomfish::_sideMenu();?>
			<?php HTML_joomfish::_creditsCopyright(); ?>
		</td>
	<?php
		HTML_joomfish::_footer($task, $option);
  }


	function showHelp() {
		global $act, $task, $option, $mosConfig_live_site;
		HTML_joomfish::_JoomlaHeader( JText::_('JOOMFISH_TITLE'). ' ' .JText::_('HELP AND HOWTO'), 'support', false );
    ?>
	<tr align="center" valign="middle">
      <td align="left" valign="top" width="70%">
		<h2>От къде да започнем?</h2>
	   <p>Чудили ли сте се някога дали е възможно да изградите сайт на няколко езика използвайки Джумла? Джум!Фиш е един от възможните отговори.<br />
		&nbsp;<br />
		Компонентът Ви позволява да управлявате съдържанието в сайта, компонентите и модулите. Компонента е толкова гъвкав, че след инсталирането на други компоненти, трябва да добавите няколко файла, за да можете свободно да преведете и тях.<br />
		&nbsp;</p>
		
		<h2>Какво разбираме под съдържание?</h2>
		<p>Когато говорим за съдържание, имаме предвид текст или друга информация, която е записана в базата данни на Джумла! Има някой текстове, които са записани в езиковите файлове на Джумла! Тези файлове ви дават възможност да променяте текстовете в зависмост от избрания от потребителя език. 
		<strong>Но</strong>
		новините, статиите и цялото останало <em>съдържание</em> което създавате е само на езика на който Вие го пишете :-(.<br />
		&nbsp;<br />
		Джум!Фиш е решение за да ви помогнете да преведете съдържанието си ;-) и по този начин да направите сайта си мултиезичен. Всяка дефиниция от съдържанието ние наричаме елемент от съдържанието.<br />
		&nbsp;</p>

		<h2>Какво прави компонента за Вас?</h2>
		Ние не искаме да създадем метод за машинен превод или друг базиран на компютрите превод.
		Целта е да подпомогнем процеса на превод, който Вие ще правите в малко по-голяма среда.
		В момента в който Вие имате външни редактори и автори на съдържание, някой трябва да контролира процеса на превод.<br />
		&nbsp;<br />
		С Джум!Фиш това лице може да провери цялото съдържание и да види, кои части от него не са преведени. 
		Базирайки се на този човек, можете да преведете цялото съдържание по начин, който Вие желаете.</p>

		<h2>Как работи?</h2>
		<p>Доста лесно. В менюто компоненти, можете да видите <a href="index2.php?&task=show&act=config_component&hidemainmenu=1">"Настройки"</a> и да промените всички настройки на компонента. Това е нещо обичайно.<br />
		Основното което трябва да настроите в момента е на колко и кои езици искате да бъде превеждано съдържанието на сайта Ви.</p>

		<p>Втората част е по сложна. Дефиницията на елементите на съдържанието са директно свързани с базата данни. Поради тази причина ние решихме да забраним възможността за тяхното администриране през настройките на компонента!
		През администрацията можете да видите <a href="index2.php?option=com_joomfish&task=config_elements">"Настройките на елементите"</a> и всички инсталирани дефиниции. Тези дефиниции се съдържат в XML файлове, които се намират в под-папката "contentelements" в административната папка на Джум!Фиш.<br />
		Ако искате да добавите нов елемент или да промените съществуващ такъв, това не би трябвало да е особенно трудно.<br />
		Най-важното, което трябва да проверите е дали XML файловете водят към правилната информация ;-)<br />
		&nbsp;<br />
		Ако добавите нов XML файл за все още не въведен компонент/модул, бъдете сигурни, че сте направили промените и в необходимите PHP скриптове.</p>

		<p>За да започнете процеса на превеждане, просто натиснете менюто <a href="index2.php?option=com_joomfish&task=translate">"Превод"</a>. Там ще откриете списъци със съдържанието на базата Ви данни. След избирането на елемент и език, лесно можете да започнете с превода.</p>
	   &nbsp;<br />
		 <p>Това е всичко за сега. Идеи, проблеми и т.н. - просто ми изпратете е-мейл.<br>
	   &nbsp;<br />
	   Алекс</p>		</td>
		<td align="left" valign="top" nowrap>
			<?php HTML_joomfish::_sideMenu();?>
			<?php HTML_joomfish::_creditsCopyright(); ?>
		</td>
	</tr>

<?php
		HTML_joomfish::_footer($task, $option);
  }

  function showMambotWarning(){
		global $act, $task, $option, $mosConfig_live_site, $_VERSION;
		HTML_joomfish::_header();
		?>
     <td align="left" valign="top">
       <h2 style="color:red">Опс, има проблем с инсталацията</h2>
       <p>Директорията "<?php echo mosMainFrame::getBasePath();?>/mambots/system/" е без права за запис</p>
       <p>Моля:</p>
       <ul>
       <li>Де-инсталирайте Джум!Фиш</li>
       <li>Променете правата върху директорията</li>
       <li>Преинсталирайте</li>
       </ul>
       <br/>Благодарим Ви.</p>
       </td>
	<?php
		HTML_joomfish::_footer($task, $option);

  }
	function showPostInstall( $success=false ) {
		global $act, $task, $option, $mosConfig_live_site, $_VERSION;
		HTML_joomfish::_header();
	?>
     <td align="left" valign="top">
       <h2>Добре дошли в Джум!Фиш</h2>
       <p>
       Компонентът Джум!Фиш Ви позволява да изградите мултиезичен сайт.<br />
       Самото изграждане на такъв сайт не е лесна работа, за това и компонента не е особенно лесен за управление.</p>   Моля прочетете инструкциите внимателно, след което се опитайте да започнете с превода.
       Можете да получите помощ и поддръжка в <a href="http://www.joomfish.net" target="_blank">сайта на Джум!Фиш</a>.

       <p>Тази версия на Джум!Фиш работи единствено на версии 1.0.7+. Ако ползвате различна версия, моля обновете поне до 1.0.7.</p>

	   <h3>1. Стъпка: Инсталиране на език</h3>
	   <p>
	   Първо трябва да инсталирате допълнителни езици.
	   Такива могат да бъдат открити в сайта на <a href="http://extensions.joomla.org" target="_blank">Джумла!</a>.<br />
	   Моля инсталирайте езиците през стандартният <a href="/administrator/index2.php?option=com_installer&element=language">Инсталатор на Джумла!</a>.</p>

	   <h3>2. Стъпка: Настройка на езиците</h3>
	   <p>След като сте готови с инсталирането, трябва да активирате исканите езици. Това става през
	   <a href="/administrator/index2.php?option=com_joomfish&task=config_language">Управление на езици в Джум!фиш</a>.<br />
	   &nbsp;<br />
	   <b>Моля проверете:</b> Когато промените езика в сайта, основните текстове трябва да се променят.</p>

	   <h2>Как да преведа съдържанието?</h2>
	   <p>След като сте настроили езиците, които са инсталирани, но съдържанието е динамично в базата данни и трябва да бъде преведено от Вас.</p>

	   <h3>3. Стъпка: Превод на информацията в базата данни</h3>
	   <p>Като начало се опитайте да преведете имената на менютата в сайта.<br />
	   Отидете в <a href="index2.php?option=com_joomfish">контролният панел</a> и изберете функцията превод.<br />
	   В прегледа на преводите, моля първо изберете езика на който искате да преведете и след това елемента.<br />
	   Можете да използвате и различните филтри. Моля преведете името на менюто като го отворите в режим за редакция. Важно е да публикувате превода.<br />
	   &nbsp;<br />
	   <b>Моля проверете:</b> При смяна на езика в сайта, трябва да се превежда и иметона връзката.</p>

   	   <h3>4. Стъпка: Превод на допълнителна информация</h3>
   	   <p>За цялото динамично съдържание, просто следвайте принципа в стъпка 3.
   	   За други компоненти (които не са в ядрото), трябва <a href="index2.php?option=com_joomfish&task=config_elements">да инсталирате допълнителни елементи</a>.</p>

	   <h2>Статус на проекта Джум!Фиш</h2>
	   <p>Тази версия е стабилна и работи на Джумла! 1.0.<br />
	   За Джумла! 1.5 ние ще подготвим нова версия, базирана на новата платформа.
	   Разработването и вече започна, но ще отнеме известно време.
	   Проверявайте редовно нашият уеб сайт.
	   </p>

	   <p>Ако имате някакви въпроси, моля използвайте форума ни, за да се забавляваме заедно с Джум!Фиш.<br>
		 &nbsp;<br />
		Екипът на Джум!Фиш<br>
		 &nbsp;<br />
		 Алекс, Герайнт, Иво и Роб
		 </p>
	   </td>
		<td align="left" valign="top" nowrap>
			<?php HTML_joomfish::_sideMenu();?>
			<?php HTML_joomfish::_creditsCopyright(); ?>
		</td>
<?php
		HTML_joomfish::_footer($task, $option);
  }
}
?>
