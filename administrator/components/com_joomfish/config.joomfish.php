<?php
$joomfish_componentAdminLang =   "english";			// Which language the component admini should have
$joomfish_noTranslation = 2;			// What to show when no translation available
$joomfish_defaultText = "";		// Standard text if no translation - only for certain content elements!
$joomfish_frontEndPublish = "1";			// Whether publishers and above can publish from the frontend
$joomfish_frontEndPreview = "0";			// Whether managers and above can see inactive languages in the frontend
$joomfish_storageOfOriginal = "md5";			// md5 := only md5 values (default); both := md5 and clean text
$joomfish_showCPanels = "7";			// binary encoded information which panles to show
$joomfish_mbfupgradeDone = "0";			// Flag showing if the upgrade was done already. modify manually if you need to do the upgrade again
$joomfish_qacaching = "";			// Flag showing if experimental query analysis caching is enabled
$joomfish_qalogging = "";			// Flag showing if query analysis logging is enabled
?>