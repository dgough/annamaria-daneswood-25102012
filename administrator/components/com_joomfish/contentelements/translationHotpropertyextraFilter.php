<?php
/**
 * Joom!Fish - Multi Lingual extention and translation manager for Joomla!
 * Copyright (C) 2003-2007 Think Network GmbH, Munich
 *
 * All rights reserved.  The Joom!Fish project is a set of extentions for
 * the content management system Joomla!. It enables Joomla!
 * to manage multi lingual sites especially in all dynamic information
 * which are stored in the database.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * The "GNU General Public License" (GPL) is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * -----------------------------------------------------------------------------
 * $Id: translationPollnameFilter.php 567 2007-07-17 05:53:43Z akede $
 *
*/

// Don't allow direct linking
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

class translationHotpropertyextraFilter extends translationFilter
{
	function translationHotpropertyextraFilter ($contentElement){
		$this->filterNullValue=-1;
		$this->filterType="hotpropertyextra";
		$this->filterField = $contentElement->getFilter("hotpropertyextra");
		
		parent::translationFilter($contentElement);
	}

	/**
 * Creates hotpropertyextra_filter_value filter
 *
 * @param unknown_type $filtertype
 * @param unknown_type $contentElement
 * @return unknown
 */
	function _createfilterHTML(){
		global $database;

		$options=array();
		$options[] = mosHTML::makeOption( '-1', JText::_('All Properties') );

		//	$sql = "SELECT c.id, c.title FROM #__categories as c ORDER BY c.title";
		$sql = "SELECT id, name FROM #__hp_properties WHERE published = 1 ORDER BY name";
		$database->setQuery($sql);
		$props = $database->loadObjectList();
		
		foreach($props as $prop){
			$options[] = mosHTML::makeOption( $prop->id,$prop->name);
		}
		
		$list=array();
		$list["title"]= JText::_('Select Property');
		$list["html"] = mosHTML::selectList( $options, 'hotpropertyextra_filter_value', 'class="inputbox" size="1" onchange="document.adminForm.submit();"', 'value', 'text', $this->filter_value );

		return $list;
	}

	
	function _createfilter(){
		
		return "c.property = '$this->filter_value'";		
	}
}
?>