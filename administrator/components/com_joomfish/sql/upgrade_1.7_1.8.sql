/**
 * Joom!Fish - Multi Lingual extention and translation manager for Joomla!
 * Copyright (C) 2003-2007 Think Network GmbH, Munich
 *
 * All rights reserved.  The Joom!Fish project is a set of extentions for
 * the content management system Joomla!. It enables Joomla!
 * to manage multi lingual sites especially in all dynamic information
 * which are stored in the database.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * The "GNU General Public License" (GPL) is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * -----------------------------------------------------------------------------
 * $Id: upgrade_1.7_1.8.sql 567 2007-07-17 05:53:43Z akede $
 *
*/

ALTER TABLE `jos_jf_content` ADD `original_text` TEXT NULL AFTER `original_value` ;
ALTER TABLE `jos_jf_content` ADD INDEX `jfContent` ( `language_id` , `reference_id` , `reference_table` ) ;
ALTER TABLE `jos_jf_content` ADD INDEX `jfContentPublished` (`reference_id`, `reference_field`, `reference_table`, `language_id`, `published`);

ALTER TABLE `jos_languages` CHANGE `iso` `iso` VARCHAR( 20 ) NULL DEFAULT NULL;
ALTER TABLE `jos_languages` ADD `shortcode` VARCHAR( 20 ) NULL AFTER `code` ;
ALTER TABLE `jos_languages` ADD `params` TEXT NULL AFTER `image` ;


## Update records
UPDATE `jos_components` SET `admin_menu_img` = '../administrator/components/com_joomfish/images/joomfish_icon.png', `ordering`=0 WHERE `jos_components`.`admin_menu_alt` = 'Joom!Fish' and `jos_components`.`option` = 'com_joomfish' LIMIT 1;
UPDATE `jos_components` SET `name` = 'Translation', `admin_menu_link` = 'option=com_joomfish&task=overview&act=translate', `admin_menu_img` = 'js/ThemeOffice/document.png', `ordering`=1 WHERE `jos_components`.`admin_menu_alt` = 'Translation' and `jos_components`.`option` = 'com_joomfish' LIMIT 1;
UPDATE `jos_components` SET `name` = 'Configuration', `admin_menu_link` = 'option=com_joomfish&task=show&act=config_component&hidemainmenu=1' WHERE `jos_components`.`admin_menu_alt` = 'Configuration' and `jos_components`.`option` = 'com_joomfish' LIMIT 1;
UPDATE `jos_components` SET `name` = 'Languages', `admin_menu_link` = 'option=com_joomfish&task=show&act=config_language&hidemainmenu=1' WHERE `jos_components`.`admin_menu_alt` = 'Languages' and `jos_components`.`option` = 'com_joomfish' LIMIT 1;
UPDATE `jos_components` SET `name` = 'Content elements', `admin_menu_link` = 'option=com_joomfish&task=show&act=config_elements' WHERE `jos_components`.`admin_menu_alt` = 'Content elements' and `jos_components`.`option` = 'com_joomfish' LIMIT 1;
UPDATE `jos_components` SET `admin_menu_link` = 'option=com_joomfish&task=show&act=credits' WHERE `jos_components`.`admin_menu_alt` = 'About' and `jos_components`.`option` = 'com_joomfish' LIMIT 1;

# INSERT new records !!!
# CORRECT THE PARENT ID!
INSERT INTO `jos_components` ( `name` , `link` , `menuid` , `parent` , `admin_menu_link` , `admin_menu_alt` , `option` , `ordering` , `admin_menu_img` , `iscore` , `params` )
VALUES (
'', '', '0', '65', '', 'jfseparator', '', '1', '../administrator/components/com_joomfish/images/jfspacer.png', '0', 'jfseparator'
), (
'Help', '', '0', '65', 'option=com_joomfish&task=show&act=help', 'Help', 'com_joomfish', '6', 'js/ThemeOffice/help.png', '0', ''
), (
'', '', '0', '65', '', 'jfseparator', '', '5', '../administrator/components/com_joomfish/images/jfspacer.png', '0', 'jfseparator'
), (
'Control Panel', 'option=com_joomfish', '0', '65', 'option=com_joomfish', 'Joom!Fish', 'com_joomfish', '0', '../administrator/components/com_joomfish/images/joomfish_icon.png', '0', ''
);
