<?php
/**
 * Joom!Fish - Multi Lingual extention and translation manager for Joomla!
 * Copyright (C) 2003-2007 Think Network GmbH, Munich
 *
 * All rights reserved.  The Joom!Fish project is a set of extentions for
 * the content management system Joomla!. It enables Joomla!
 * to manage multi lingual sites especially in all dynamic information
 * which are stored in the database.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * The "GNU General Public License" (GPL) is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * -----------------------------------------------------------------------------
 * $Id: admin.joomfish.php 613 2007-08-30 12:04:14Z akede $
 *
*/

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

// disable Zend php4 compatability mode - this causes problem with passing translations by reference
// see http://forum.joomla.org/index.php/topic,80065.msg451560.html#msg451560 for details of problem
// See http://uk.php.net/ini.core for description of the flag
@ini_set("zend.ze1_compatibility_mode","Off");

/** required standard extentions **/
require_once( mosMainFrame::getBasePath() .'/components/com_joomfish/includes/defines.php' );

require_once( $mainframe->getPath( 'admin_html' ) );
require_once( JOOMFISH_ADMINPATH .DS. 'controler.php' );

require_once( JOOMFISH_LIBPATH .DS. 'joomla' .DS. 'language.php' );
require_once( JOOMFISH_LIBPATH .DS. 'joomla' .DS. 'registry.php' );

require_once( $mainframe->getPath( 'class' ) );
$joomFishManager = new JoomFishManager( dirname( __FILE__ ) );

$adminLang = 'english';
$adminLang = strtolower( $joomFishManager->getCfg( 'componentAdminLang' ) );
$lang =& JLanguageHelper::getLanguage($adminLang);
$lang->load( 'com_joomfish', JOOMFISH_LANGPATH );
//$lang->setDebug( true );

require_once( $mainframe->getPath( 'admin_html' ) );

// load additional help html files
if( file_exists( $adminPath . '/help/joomfish.help.inc_' . $adminLang . '.php' ) ){
	require_once( $adminPath . '/help/joomfish.help.inc_' . $adminLang . '.php' );
}else {
	require_once( $adminPath .'/help/joomfish.help.inc.php' );
}

$act = mosGetParam( $_REQUEST, 'act', '' );
$task = mosGetParam( $_REQUEST, 'task', '' );

if($mosConfig_debug) {
	reset ($_POST);
	while (list ($key, $val) = each ($_POST)) {
   	echo "$key => $val<br />\n";
	}
}

// Setting default values if variables are not defined
//$catid = mosGetParam( $_REQUEST, 'catid', '' );
$catid = $mainframe->getUserStateFromRequest('selected_catid', 'catid', '');
$option = mosGetParam( $_REQUEST, 'option', 'com_joomfish' );
//$select_language_id = mosGetParam( $_REQUEST, 'select_language_id', -1 );
$select_language_id = $mainframe->getUserStateFromRequest('selected_lang','select_language_id', '-1');

$language_id = mosGetParam( $_REQUEST, 'language_id', $select_language_id );
$select_language_id = ($select_language_id == -1 && $language_id != -1) ? $language_id : $select_language_id;
$contentid = mosGetParam( $_REQUEST, 'contentid', null );
$jfc_id  = mosGetParam( $_REQUEST, 'jfc_id ', null );


$controler =& new JoomfishControler();
//echo "task = $task | act=$act";
// Transition of tasks according to certain actions
switch ($task) {
	case 'cpanel':
		$task = '';
		$controler->performTask( $task );
		break;

	case 'overview':
		switch ($act) {
			case 'config_component':
				$controler->saveComponentConfiguration();
				break;
			case 'config_language':
				$controler->saveLanguageConfig();
				break;
			case 'translate':
				$controler->showTranslationOverview( $select_language_id, $catid );
				break;
			case 'orphans':
				$controler->showOrphanOverview( $select_language_id, $catid );
				break;
			case 'manage':
				$controler->showManagementOverview( $select_language_id, $catid );
				break;
		}
		break;

	case 'show':
		switch ($act) {
			case 'config_component':
				$controler->showComponentConfig();
				break;
			case 'config_language':
				$controler->showLanguageConfig();
				break;
			case 'config_elements':
				$controler->showElementOverview();
				break;
			case 'translate':
				$controler->showTranslate();
				break;
			case 'help':
				$controler->showHelp();
				break;
			case 'credits':
				$controler->showCredits();
				break;
				break;
			case 'orphans':
				$controler->showOrphans();
				break;
			case 'upgrade_install':
				$controler->showUpgrade();
				break;
		}
		break;

	case 'edit':
		$cid = mosGetParam( $_REQUEST, 'cid', array(0) );
		$translation_id = 0;
		switch ($act) {
			case 'translate':
				if( strpos($cid[0], '|') >= 0 ) {
					list($translation_id, $contentid, $language_id) = explode('|', $cid[0]);
					$select_language_id = ($select_language_id == -1 && $language_id != -1) ? $language_id : $select_language_id;
				}
				$controler->editTranslation( $translation_id, $language_id, $catid, $contentid, $select_language_id );
				break;
			case 'config_elements':
				$controler->showElementConfiguration( $cid[0] );
				break;
			case 'orphans':
				list($translation_id, $ref_id, $langid) = explode('|', $cid[0]);
 				$controler->showOrphanDetail($translation_id, $ref_id, $catid, $langid);
 				break;
			case 'translateConfig':
				$controler->translateConfig();
				break;
		}
		break;

	case 'cancel':
	case 'back':
		switch ($act) {
		 	case 'translate':
				$controler->showTranslate();
		 		break;

			case 'translateConfig':
				$controler->showLanguageConfig();
				break;

			case 'config_elements':
		 		$task = 'show';
		 		$controler->showElementOverview();
		 		break;

			default:
			$task = '';
			$controler->performTask( $task );
		}
		break;

	case "unpublish":
	case "publish":
		$publish = $task=="publish" ? 1 : 0;
		$cid = mosGetParam( $_REQUEST, 'cid', array(0) );
		if( strpos($cid[0], '|') >= 0 ) {
			list($translation_id, $contentid, $language_id) = explode('|', $cid[0]);
			$select_language_id = ($select_language_id == -1 && $language_id != -1) ? $language_id : $select_language_id;
		}
		$controler->publishTranslation( $catid, $cid, $publish );
		$controler->showTranslate();
		break;

	case 'save':
	case 'apply':
		switch ($act) {
			case 'config_component':
				$controler->saveComponentConfiguration();
				break;

			case 'config_language':
				$controler->saveLanguageConfig();
				break;

			case 'translateConfig':
				$controler->saveTranslateConfig();
				if( $task == 'save' ) {
					$controler->showLanguageConfig();
				} else {
					$controler->translateConfig();
				}
				break;

				case 'translate':
				$contentid = mosGetParam( $_REQUEST, 'reference_id', null );
				$controler->saveTranslation( $language_id, $catid, $contentid, $jfc_id );
				if( $task == 'save' ) {
					$controler->showTranslate();
				} else {
					$controler->editTranslation( 0, $language_id, $catid, $contentid, $select_language_id );
				}
				break;
		}
		break;

	case 'remove':
	case 'delete':
		switch ($act) {
			case 'config_elements':
				$controler->removeContentElement();
				break;
			case 'config_language':
				$controler->removeLanguages();
				break;
			case 'translate':
				$controler->removeTranslation();
				break;
			case 'orphans':
				$cid = mosGetParam( $_REQUEST, 'cid', array(0) );
				$controler->_removeTranslation( $catid, $cid );
				$controler->showOrphanOverview( $select_language_id, $catid );
		}
		break;

	case 'install':
		switch ($act) {
			case 'config_elements':
			$controler->showContentElementsInstaller();
			break;
		}
		break;

	case 'uploadfile':
		$controler->showCElementConfig();
		break;

	default:
		$controler->performTask( $task );
		break;
}


// Write debug file for language settings
if ( $lang->getDebug() ) {
	$filename = $lang->getLanguagePath( JOOMFISH_LANGPATH, $mosConfig_lang );
	if( !$handle = fopen($filename . '/debug.' .$mosConfig_lang. '.com_joomfish.ini', 'a') ) {
         echo "Cannot open file ($filename)";
         exit;
    }

    // Write $somecontent to our opened file.
    if (fwrite($handle, $lang->exportLanguageStrings()) === FALSE) {
        echo "Cannot write to file ($filename)";
        exit;
    }
    fclose($handle);
}


$controler->redirect();


?>