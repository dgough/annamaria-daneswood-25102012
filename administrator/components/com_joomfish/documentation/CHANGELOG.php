<?
/**
 * Joom!Fish - Multi Lingual extention and translation manager for Joomla!
 * Copyright (C) 2003-2007 Think Network GmbH, Munich
 *
 * All rights reserved.  The Joom!Fish project is a set of extentions for
 * the content management system Joomla!. It enables Joomla!
 * to manage multi lingual sites especially in all dynamic information
 * which are stored in the database.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,

 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * The "GNU General Public License" (GPL) is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * -----------------------------------------------------------------------------
 * $Id: CHANGELOG.php 788 2007-12-16 16:09:19Z akede $
 *
*/

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );
?>

1. Copyright and disclaimer
---------------------------
This application is opensource software released under a variant of the GPL.
Please see source code and the LICENSE file for more details.

Copyright 2003-2007 Think Network GmbH, Munich
- All Rights Reserved.


2. Changelog
------------
This is a non-exhaustive (but still near complete) changelog for
the Joom!Fish 1.x, including beta and release candidate versions.

The Joom!Fish 1.x is based on the JoomFish 1.5 releases but includes some
drastic technical changes.


Legend:

* -> Security Fix
# -> Bug Fix
+ -> Addition
^ -> Change
- -> Removed
! -> Note

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

---------- Joom!Fish 1.8.2 Release  ----------------
16-12-2007 Alex Kempkens
 ! packaging 1.8.2 release

15-12-2007 Geraint Edwards
 # Minor bug in category search bot 
 # Mismatch in array index name

28-11-2007 Geraint Edwards
 # Add trap for broken query or bad database connection in setreftables

24-11-2007 Geraint Edwards
 # Fix for null values in primary key causing setreftables failure.  Specifically for static content items. 

16-11-2007 Alex Kempkens
 # fixed little issue with global config overwrite check

---------- Joom!Fish 1.8.1 Release  ----------------
12-11-2007 Alex Kempkens
 # German language files updated
 # fix bug in system bot while language detection
 # implemented an unlink for all existing files during installation
 # corrected copy 2 original to acknowledge filters
 # corrected all release dates

10-11-2007 Ivo Apostolov
 # Commenting CSS file
 + Split between encodings of Bulgarian Language file and adding UTF-8 as default
 # Update of the Bulgarian language files
 # Final tests for 1.8.1 release, fixing CSS validation issue on Language Module

08-11-2007 Alex Kempkens
 # updated spanish language files
 # fixed issues with MambelFish upgrade
 + added frensh language files, thanks Daneel

07-11-2007 Geraint Edwards
 #  Ensured "static content" category filter option appiesy only when translating content

07-11-2007 Geraint Edwards
 + Implemented experimental setreftable cache and log (configurable option defaulting to off)
 + Added JF specific caching routine for content elemtent data relevant to missing translations

06-11-2007 Ivo Apostolov
 # updated Bulgarian language file

27-10-2007 Alex Kempkens
 # [forum] Parse error: parse error! with 1.8 Stable
 # [forum] Typos in English language

27-10-2007 Geraint Edwards
 # [7829] Upgrade process assumes table prefix is jos_ - this should be #__

---------- Joom!Fish 1.8 Release  ----------------

22-10-2007 Alex Kempkens
 # language files update
 # Joom!Fish 1.8 stable packageing

18-10-2007 Alex Kempkens
 # corrected smaller issues with HTML encoding
 + switch for mambot to disable cookies
 ! (useful in combination with SEF extensions which automatically change URL's according to a language)

15-10-2007 Alex Kempkens
 + Lao language files added, thanks Krit
 # changed Thai flag
 + configuration function for the cpanel moduls
 + news cpanel modul with latest information from joomfish.net
 # corrected frontend missing translation tag to: FRONTPAGE_NO_TRANSLATION_AVAILABLE
 + added several missing language tags
 + added informal german language files
 # corrected language creation within mldatabase file

15-10-2007 Geraint Edwards
 + Added "Static Content" option to category filter

29-09-2007 Geraint Edwards
 # Preview translation file missing from xml package file

14-09-2007 Alex Kempkens
 # Updated Thai language files, thanks Krit

14-09-2007 Geraint Edwards
 # [#7105] Workaround to enable translation of URLs for wrapper menu items
 # Translation status links now open in new window to avoid having to recalculate unnecessarily

03-10-2007 Alex Kempkens
 # [#7403] Hebrew is not IW

28-09-2007 Alex Kempkens
 # corrected minor display issues within the cpanel
 # corrected spelling error in system bot
 # [#5506] Update of the control panel Status tab, added missing alternative translate bot

10-09-2007 Alex Kempkens
 + Italian language files, thanks to Marco Bertolini for he translation

08-09-2007 Alex Kempkens
 + Implemented a switch in the system bot which allows to decide that a unknown site request will be redirected with a 303 HTTP request
 # corrected the implementation of the systembot flag for the language determination logic
 # [#7020] Translation

07-09-2007 Alex Kempkens
 # [#6994] Wrong url in cpanel Extension State

07-09-2007 Geraint Edwards
 # [#6995] Added a small message that informs translator when mambot params disable global config translations from being used
 # Added error trap for unpublished state summary wben it encounters a deleted content element file

07-09-2007 Alex Kempkens
 # [#6994] Wrong url in cpanel Extension State

06-09-2007 Alex Kempkens
 + Hungarian language files, thanks Jozsef Tamas Herczeg for the translation
 # [#6944] 1.8 RC - Error message in admin and front page on Joomla! 1.0.12
 # [#6961] Errore di Funzionamento JoomFish

 ---------- Joom!Fish 1.8 Release Candidate Version ----------------

04-09-2007 Alex Kempkens
 # Final packageing for RC

03-09-2007 Alex Kempkens
 # updated Turkish language file, thanks Aytug Akar for the translation

02-09-2007 Alex Kempkens
 # updated spanish language files, thanks Victor for the translation
 + added Norwegian language files
 # corrected spelling failures in language files

01-09-2007 Alex Kempkens
 # Updated German language files
 + Turkish and Spanish language directores, translations in process
 + additional system check for the JFBot being the first system bot (ordering)
 # Corrected wrong SQL statement for change of ISO code field

30-08-2007 Geraint Edwards
 + Enured that unauthorised users trying to access in active languages are redirected to a valid active language

30-08-2007 Alex Kempkens
 # Completed work on the management interface
 + some language tags for future menu handling, idea is to change menu values when changing admin interface language
 - removed not implemented management functions, will be 2.0
 # fixed smaller issues with banner CE files
 # Updated the uninstaller
 # Updated XML file to hold system check information
 # [#5506] Update of the control panel Status tab
 # [#6124] Calling wrong language for help.page
 # [#6132] Date displayed wrong

27-08-2007 Geraint Edwards
 + Added configurable ability for managers and above to preview the site in an inactive language

26-08-2007 Alex Kempkens
 # Update XMl files
 # Updated installation process
 + German language files and help as dummy

14-08-2007 Geraint Edwards
 # Ensure fallback language code is only called when rows exists that have not been translated

02-08-2007 Alex Kempkens
 # fixed language flag link to shortcode instad of iso
 # changed order of language manager fields

 31-07-2007 Geraint Edwards
 + Added the concept of a fallback language.  The idea being that if a translation doesn't exist in the specified language but one does in the 'fallback language' that the 'fallback' version is delivered instead.

24-07-2007 Alex Kempkens
 # fixed little issue related to SQL queries without mosDBTable reference
 ! from now on all objects which are not inherited from mosDBTable will not be processed through the Joom!Fish !
 ! http://forum.joomla.org/index.php/topic,180785.0.html

24-07-2007 Geraint Edwards
 + add preview of flag in language config
 + add textarea handling for paramater config
 + Provide side by side previews of original and translation

19-07-2007 Alex Kempkens
 # [#6108] Missing closing anchor tag
 # fixed incorrect & in language file tag DEFAULT_LANGUAGE_HELP
 # fixed lacking alt tags for images
 # [#6107] translateConfig not working
 # [#6103] Wrong line in joomfish.xml
 + [#4131] Appearance of the Default Text + Original Content
 ! added div.jfdefaulttext style for graphical control over the default text
 # [#6123] Double definition of lower case

17-07-2007 Alex Kempkenss
 # corrected license reference to point to GPL v2
 # corrected usage of shortcode for alternative language bot
 + [#5504] multilingual section search bot, task completed

16-07-2007 Alex Kempkens
 + adding the functionality to copy original values to translations
 # changed some text files
 # changed the way how the JScript result is used

16-07-2007 Geraint Edwards
 + Added translation preview for content items - includes {mosimge} handling

13-07-2007 Alex Kempkens
 # Updated translate management functionality with translation status and original status results

12-07-2007 Alex Kempkens
 + completed translation status check
 # Fixed bug in translation form and footer values

09-07-2007 Alex Kempkens
 + Integrated Interface for Translation Management with first check methods

04-07-2007 Alex Kempkens
 + [#5504] Create search bots for newsfeeds

02-07-2007 Alex Kempkens
 # [#5873] Deleting of a traslation
 # [#5507] Page navigation in CE list doesn't work
 + [#5504] Create search bots for categories

21-06-2007 Alex Kempkens
 # corrected license information to all files as the change to LGPL wasn't correct in first place
 # corrected documentation files
 + index for better performance of queries
 # updated language files

15-06-2007 Alex Kempkens
 # [#5505] Delete of default language[#5505] Delete of default language

06-06-2007 Geraint Edwards
 ^ Filters now remember state within session, memory cleared by changing content type
 ^ Changed 'translation published' filter to a 'translation availability' filter

03-06-2007 Ivo Apostolov
 ^ Changed Edit to View in CE Screens
 + Added the string VIEW=View in english.ini

03-06-2007 Alex Kempkens
 # changed some versioning information in the XML files and also corrected emails



25-05-2007 Alex Kempkens
 # fixed Mambelfish upgrade facilities
 ! started to implement the check/management functionality, not yet finished

23-05-2007 Alex Kempkens
 # module: ignore mosmsg in href creation to ensure it is visible in frontend

21-05-2007 Alex Kempkens
 # [#447] PDF with Joomfish component - set the local setting to LC_TIME to avoid this problem
 ! the problem is based on a bug in the PDF class which uses the wrong printf definition
 # Ensure that missing content element files to not result in a bug in the overview

18-05-2007 Alex Kempkens
 # Fixed issue with loading the correct language for the admin backend
 # [#598] table name with a caps in it doesn't work
 + functionallity to store original values in clean text; doesn't work with htmltext fields yet; work in progress
 ! [#447] PDF with Joomfish component - the PDF seems corrupt, acrobat cannot open the document
   It seems this issue is related to ISO codes which do not match to the server configuration; try to use only 2-char iso code
   definitions or 4-char defintions as an alternative

17-05-2007 Geraint Edwards
 + Now handles multiple aliases for the same table in one query
 + Config variable translation via language manager

15-05-2007 Alex Kempkens
 + Parameter management in systembot, language parameters are now used to overwrite system config vars
 + new parameter for the systembot to allow overwriting of system vars
 + language dialog; started implementing new dialog for system parameter management per language

07-05-2007 Alex Kempkens
 # [artf7459] Main Content not showing on none-translated articles (additional textinfos had been overwritten, check meta please)
 # [artf5572] Deactivated site-default shown: Added system warning and fallback to first active language;
 ! This situation is a wrong configuration. The language configured in the system configuration should be always active; thats
   why the system warning can't be disabled
 # Fixed problem in module while displaying no entry (e.g only one active language but show active is disabled)
 + new module mode to show flags plus language name
 # [artf7272] Images text area in translate content - Small change - rows reduced to 10; colums are on max 80
 ! [artf4465] Orphan manager is implemented; the delete executes direct delete calls which are not intercepted by the fish
 # [artf7335] Module: Cannont redeclare Class JFModuleHTML
 ! [artf7251] Fatal error: showtranslationoverview() [function.require]: Failed opening;  should be fixed with refactoring
 # admin module cid definition for new structure (three values instead of two)
 # JScript issues with language files; within JScript <br /> should be manually converted to \n
 + Implemented overview tab fur unpublished translations

30-04-2007 Alex Kempkens
 # finished language file changes; old files are removed from the trunk still exist in the documentation directory
 ! switch on debug mode to see missing translations. In this case a file with missing tags is written
 ! many tags are changed to be more clear in their working; duplicated tags still need to be removed
 + controler class file to separate task transition and controler actions
 + integrated hidemainmenu for editing translations
 # completed language convertion to INI files
 # completed transition to controler model
 ! REINSTALLATION OF DATABASE IS RECOMMENDED FOR TESTING PURPOSES - UPGRADE FUNCTIONALLITY IS MISSING!

30-04-2007 Geraint Edwards
 + modified filter for contents

28-04-2007 Alex Kempkens
 # proceeding with language file chnages
 # proceeding with the refactoring of act => task processes
 # refactoring the abstract tasker classes

20-04-2007 Alex Kempkens
 + added JLanguage classes and introduced INI files for languages
 # refraced certain language strings
 # changed language string interpretation
 # Refactoring of admin Tasker class and functions
 # Refactored act => task

13-04-2007 Alex Kempkens
 # fixed certain XHTML standards (thx Mic for the hints)
 # optimized presentation of language flags beside select box

26-Mar-2007 Alex Kempkens
 + implemented content element installer
 # Optimized use of icons for more consistency
 + Updated sql/upgrade_1.7_1.8.sql to reflect new needed Fields

24-Mar-2007 Alex Kempkens
 # [artf7277] : Translate the Publish date? - To be considered.
 + added new information tab
10-Jan-2007 Ivo Apostolov
 + Added Raw Images Display as option in the module

10-Jan-2007 Alex Kempkens
 # Optimzed language determination
 # Changed module to use shortcode instead of isocode
 # Fixed issues with using the shortcode instead of iso code
 + added index.html files to all folders
 # [artf4809] : deleted language still shows in joomfish; BEAWARE ALL RELATED TRANSLATIONS WILL BE REMOVED!
 # Changed help text of language description for the removal info

8-Jan-2007 Alex Kempkens
 # Adjusted module and system bot to use short code instead of iso for language change
 # Changed Copyright to 2007

1-Dec 2006 Ivo Apostolov
 + Altered the install and uninstall functions with the new searchbots.
 ^ GUI Change in Content Element View

30-Nov 2006 Alex Kempkens
 + Additional fields for content elements weblinks and modules in order to translate URL's as well
 # versions of CE files

30-Nov 2006 Geraint Edwards
 + searchbot for contacts and weblinks

29-Nov-2006 Alex Kempkens
 # [artf6928] : Language Configuration can not be saved
 # [artf6926] : Remove predifned collation from installation queries
 # [artf6927] : Replace ENGINE with TYPE in SQL Queries

27-Nov-2006 Alex Kempkens
 # [artf6533] : ISO Code field too short
 + [artf5193} : Joom!Fish to manage a independent Locale for each language
 # [artf5664] : pathway notice
 # Changed admin icons to the new Joomla! style
 # removed all reference to $mosConfig_absolute_path and replaced them with the mosMainframe method call
 # started to rearrange the admin workflow for the translation status overview and content element manager

17-Aug-2006 Geraint Edwards
 + Added javascript confirmation when changing the language of existing translation to warn of risk of overwriting translation

16-Aug-2006 Geraint Edwards
 # [artf5624] Frontend translation doesn't work if nothing is translated fixed
 # [artf4465] Added orphan translation management code
 # [artf4311] Removed hard coded english terms
 + Removed duplicated SQL queries to determin active languages

01-Aug-2006 Alex Kempkens
 # Updated language copyright statements and some minor fixes

24-July-2006 Alex Kempkens
 ^ Some incorrect references to the GPL and changed them to GPL as decided

24-July-2006 Geraint Edwards
 # [artf4821] [artf4623] Added section filter for content and category items
 + Added new language selection choice - dropdown showing flag of active language.

21-June-2006 Alex Kempkens
 # [artf4895] acitve = active?
 # [artf5188] admin-backend module field "spacer" - removed
 ^ Reorganize help file
 # [artf5333] Fixed french translation file
 # [artf5224] Metakey and metadesc - fixed at 100 characters
 # Problem with the counting of contents in the translation overview, removed the "All languages" option
 ^ Language files to fit to the new terms

19-June-2006 Geraint Edwards
 # [artf4622] defaultText now works even if absolutely no translations have been made (but only for content table)

24-May-2006 Alex Kempkens
 # [artf4735] fixed $_SERVER['HTTP_ACCEPT_LANGUAGE']
 # [artf4764] Active language, implemented an id id="active_language" and an example in the css
 # [artf4661] jf_content.value data type, just admin.joomfish.php had wrong info, everything else was fine
 + [artf4417] Add inactive languages for language selection in the administration
 # [artf4771] pagenavigation not included correctly

18-May-2006 Alex Kempkens
 # [artf4690] checked for empty $_SERVER['QUERY_STRING']
 # [artf4626] Tooltip in language selection

---------- Joom!Fish 1.7 Release  ----------------
03-May-2006 Alex Kempkens
 ! packaging for stable release

01-May-2006 Alex Kempkens
 # artf4491 : Minor errors in English language file
 # artf4521 : `value` field and `introtext` and `fulltext` fields haven't the same type
 ! Attention for an update from the RC you might want to alter this coulm manually!
 # artf4492 : Commenting _JF_LANG_INCLUDED variable
 # artf4344 : Add to default text
 ! Finishing for stable release

26-Apr-2006 Alex Kempkens
 + Added Thai language file and flag

17-Apr-2006 Geraint Edwards
 # joomfishmanager is not available during installation therefore causing problems setting short_loc wasn't

16-Apr-2006 Geraint Edwards
 + Add translation of paramaters

13-Apr-2006 Geraint Edwards
 # Fixed artf4366"

12-Apr-2006 Geraint Edwards
 # Fixed artf4341 not counting all the rows!

10-Apr-2006 Ivo Apostolov
 + Adding Bulgarian Help File
 ^ Updated bulgarian help file in installer

10-Apr-2006 Alex Kempkens
 # artf4307 : Error on install
 # Corrected spelling error in file list

 ---------- Joom!Fish 1.7 Release Candidate Version ----------------
13-Apr-2006 Geraint Edwards
 # artf4366 : search only worked in Joomla 1.0.9
 # artf4373: lang value from query ignored on first visit
 # artf4374: Allow configuration of default language for first time visitors

12-Apr-2006 Geraint Edwards
 # artf4341 :  Content items not all shown in translation list

09-Apr-2006 Alex Kempkens
 ^ updated Norwegian language file
 # artf4307 : Error on install


08-Apr-2006 Geraint Edwards
 # 4310 (created by fix to artf4285)

07-Apr-2006 Ivo Apostolov
+ Added Swedish Lang file

06-Apr-2006 Alex Kempkens
 # Fixed little issue with Default-Text (could be related to artf4106: Default Text not working)
 # issue with new bot-flag
 + default_text define to language files
 + Added norwegian help file

05-Apr-2006 Alex Kempkens
 - Removed check component link in cpanel - feature is post ponded for 1.8 release
 ^ toolbar of content elements
 ! preparing for RC 1.7
 + task2291 Paramter for enabling/disabling language determination on browser
 ^ changed the name of localized help files into joomfish.help.inc_<language_name>.php

04-Apr-2006 Geraint Edwards
 # artf4218 : Bad HTTP_REFERRER test

 04-Apr-2006 Alex Kempkens
 # artf4160 : Contacts: Description text bypassed
 # artf4158 : Installation adds extra english language

03-Apr-2006 Geraint Edwards
 + support for copy/paste for xstandard editor

02-Apr-2006 Geraint Edwards
 # artf4228 Inadequate test on empty table prefix

01-Apr-2006 Geraint Edwards
 # art4163 defaultText not working with content (also fixed handling of blank defaultText to use values from language files)

30-Mar-2006 Geraint Edwards
 + Filter out blank poll options and allow filtering of options based on poll name :)

28-Mar-2006 Geraint Edwards
 + Added copy and clear buttons for htmltext - currentlt supports tmedit, tinymce, jce, fckeditor
 # Fix for "message with original content if no translation" - needs multi-language extention

28-Mar-2006 Alex Kempkens
 + artf4053: searchbot for actual language
 ^ corrected credits
 # artf4159 : Changelog-link not working

 ---------- Joom!Fish 1.7 Beta 2 Version ----------------
28-Mar-2006 Alex Kempkens
 ! Packaged distribution file

27-Mar-2006 Ivo Apostolov
 # Fixes of hardcoded texts
 # Update of language files

27-Mar-2006 Alex Kempkens
 + Upgrade functionallity implentend (artf3139: Upgrading from Mambelfish)
 # German language file updated
 + English language file: upgrade dialog added

27-Mar-2006 Geraint Edwards
 + Add translation filter memory (it really does make things easier to manage!)
 + "New copy to clipboard functionality and associated messages."
   artf3465: Copy and clear buttons for all fields

24-Mar-2006 Alex Kempkens
 # artf3987	Entitleing of fields (included mosStripslashes again for ALL fields)
 # artf4042	Slashed added to quotation marks in translation! (same as artf3987)
 # artf4076	Can't insert an image in translation (same as artf3987)
 # artf4081	In Translation when link is added to text; link error! (same as artf3987)
 # artf4052	[Trivial] Tooltip description of imagefilename is wrong
 # artf4087	module HTML errors - unescaped & or unknown entity "&..."
 ^ Corrected language define names to JOOMFISH
 # artf4020	Search bot not searching static text
 # artf4000	Uninstall should remove mambots and modules
 ^ Updated German language file

23-Mar-2006
 # artf4055: Bug in install.joomfish.php


20-Mar-2006 Ivo Apostolov
 + Updated Norwegian, French and Spanish Language Files

18-Mar-2006 Geraint Edwards
 + Added the following filters:
   Module filter - to hide admin modules
   MenuType filter - to filter by specific menus
   Published filter - to look for missing or unpublished translations (ideally by language)
 # artf3990 Depricated by reference call

17-Mar-2006
  # Fix of MySQL error in installation. http://forum.joomla.org/index.php/topic,47297.0.html

---------- Joom!Fish 1.7 Beta Version ----------------
17-Mar-2006 Alex Kempkens
 # Issue with wrong copy text in translation dialog

17-Mar-2006 Geraint Edwards
 # Removed references to collation in SQL table creation
 # missing global declaration of select_language_id
 # fix for SEF language switching

17-Mar-2006 Ivo Apostolov
 # Removing collation from joomfish.xml

16-Mar-2006 Alex Kempkens
 ^ Module styles and possible alternatives, based on suggestions from accessiblity team
 ! todo, adding a bottom for the language select list
 ^ reworked the look and feel of the documentation, credits and licenes pages
 + central control panel for the component
 ^ several language defines to have new text within the cpanel and other pages
 ^ marked several language defines as deprecated (will be removed next version)
 # [mx:#5157] Undefined variable: mosConfig_locale
 # [mx:#5164] Language selector in fontend, &amp;amp; bug with combo box style
 # [mx:#5179] Component icon not available in backend
 # [mx:#5195] CSS validation URL violation
 # [mx:#5274] No language switchinging on startpage via drop down list
 # [mx:#5374] Undefined index: mbf_dir
 # [mx:#7236] overlib doesn't work
 # [mx:#7150] Undefined variable: hrefVars
 # fixed bug in system-bot, with missing default site lang
 # fixed bug related to uncorrect status lamps
 + Added all in one installer
 ^ Text in post install screen
 # Fixed content element dialog
 # artf1224 : Make content images, alt texts and captions translatable

15-Mar-2006 Geraint Edwards
 + Added translation filters for admin interface
 + Added new defines to english.php to cover translation filters
 # [mx:#5167] phpwarnings $mosConfig_dbprefix

10-Mar-2006 Ivo Apostolov
 ^ Changes in english language file. Item Information is changed to Publishing

09-Mar-2006 Ivo Apostolov
 # fixed bug in joomfish.xml

11-Mar-2006 Alex Kempkens
 # fixed module issue with language parameter & SEF

08-Mar-2006 Alex Kempkens
 + Finised work on the module
 + Added possibility to hide active language within the module
 # fixed artf3826 : Generate *closed* IMG Tags to comply with W3C standard
 # fixed artf3034 : Undefined property notice
 ^ SQL definition to include the JoomFish component as possible link
 ^ removed uninstall queries
 ^ Added bosnian flag, changed swedish flag within XML
 ^ updated norwegian language file
 # fixed artf1330 : Meta Desc and Tag translatable

06-Mar-2006 Geraint Edwards
 + Added config paramater to enable publishing of translations from frontend
 + Removed assumption that primary key for a table is called "id"
 + Added support for translation sql queries involving more than one table at once
 + Take account of tablename and field name aliases in translation
 + Translate lists all in one go (performance improvement)
 # Content can now be created in the frontend in a non-default language
 # PHP5 copy by reference problem eliminated
 # date formatting fix for admin interface
 + Added Welsh flag

06-Mar-2006 Alex Kempkens
 + Added language determination to system bot
 ^ moved module display from component to module code
 ^ added special module css for Joom!Fish display
 ^ reworked module display using ul or div - not be continued

02-Mar-2006 Ivo Apostolov
 + Added Bulgarian Language File
 ^ Updated SQL file for manual installation, added queries for the bots, change of the module name
 ^ Changed the toolbar.html.php - Cosmetic changes to fit to the outlook of Joomla!
 + Added the Poll Options Content Element, which was missing
 # Changed the name of the component in mosLoadComponents in the module.


01-Mar-2006 Alex Kempkens
 + Added Systembot for mlDatabase integration
 ^ splitted mldatabase class into intepended file
 ^ prepared jf_language_selection module xml and parameters

22-Feb-2006 Alex Kempkens
 ^ Naming from mambelfish to joomfish in most of the files
 ^ Corrected copyright notice and references to other sites, documents
 ^ renamed classes to fit to the new name
 ^ refactored the database tables in order to provide more effective use

21-Feb-2006 Alex Kempkens
 ^ Reworked the copyright and nameing convertions from the old release
 + added new structure and basic files to the SVN

 === Base of work procress is JoomFish 1.5 release                              ===
