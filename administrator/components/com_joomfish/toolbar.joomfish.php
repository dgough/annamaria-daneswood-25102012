<?php
/**
 * Joom!Fish - Multi Lingual extention and translation manager for Joomla!
 * Copyright (C) 2003-2007 Think Network GmbH, Munich
 *
 * All rights reserved.  The Joom!Fish project is a set of extentions for
 * the content management system Joomla!. It enables Joomla!
 * to manage multi lingual sites especially in all dynamic information
 * which are stored in the database.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * The 'GNU General Public License' (GPL) is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * -----------------------------------------------------------------------------
 * $Id: toolbar.joomfish.php 567 2007-07-17 05:53:43Z akede $
 *
*/

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

global $mosConfig_lang;

require_once( $mainframe->getPath( 'toolbar_html' ) );
require_once( $mainframe->getPath( 'toolbar_default' ) );
require_once( $mainframe->getPath( 'class' ) );

$task = mosGetParam ($_REQUEST, 'task', '' );
$act = mosGetParam ($_REQUEST, 'act', '' );
$joomFishManager = new JoomFishManager( dirname( __FILE__ ) );
if( count($joomFishManager->getActiveLanguages()) == 0 ) {
	$act = 'config_component';
}

switch ($task) {

	case 'overview':
	case 'save':
	case "unpublish":
	case "publish":
		switch ($act) {
			case 'translate':
				MENU_joomfish::TRANSLATION_OVERVIEW_MENU();
				break;
			case 'orphans':
				MENU_joomfish::ORPHAN_MENU();
				break;
			default:
				MENU_joomfish::DEFAULT_MENU();
				break;
		}
		break;

	case 'show':
		switch ($act) {
			case 'config_component':
				MENU_joomfish::CONFIG_MENU();
				break;
			case 'config_language':
				MENU_joomfish::CONFIGLANG_MENU();
				break;
			case 'config_elements':
				MENU_joomfish::EDIT_ELEMENTS_MENU();
				break;
			case 'credits':
			case 'help':
			case 'postinstall':
				MENU_joomfish::DEFAULT_MENU();
				break;

			case 'upgrade_install':
				MENU_joomfish::UPGRADE_MENU();
				break;
		}
		break;


	case 'install':
	case 'uploadfile':
		MENU_joomfish::INSTALL_ELEMENTS_MENU();
		break;

	case 'back':
	case 'home':
	case 'cancel':
		if ($act=='config_elements') {
			MENU_joomfish::EDIT_ELEMENTS_MENU();
		} elseif ($act=='credits') {
			MENU_joomfish::DEFAULT_MENU();
		} elseif ($act=='translate') {
			MENU_joomfish::TRANSLATION_OVERVIEW_MENU();
		}
		break;

	case 'remove':
		switch ($act) {
			case 'config_elements':
				MENU_joomfish::INSTALL_ELEMENTS_MENU();
				break;
			case 'translate':
				MENU_joomfish::TRANSLATION_OVERVIEW_MENU();
				break;
			case 'config_language':
				MENU_joomfish::CONFIGLANG_MENU();
				break;
		}
		break;

	case 'edit':
	case 'apply':
		switch ($act) {
			case 'translate':
			case 'translateConfig':
				MENU_joomfish::TRANSLATE_MENU();
				break;
			case 'orphans':
				MENU_joomfish::ORPHAN_DETAIL_MENU();
				break;
			case 'config_elements':
				MENU_joomfish::DEFAULT_MENU();
				break;
		}
		break;

	case 'upgrade':
	default:
		MENU_joomfish::DEFAULT_MENU();
		break;
}
?>