<?php
/**
* @copyright (C) 2006 by Azrul Rahim - All rights reserved!
* @license http://www.azrul.com Copyrighted Commercial Software
**/
(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

require_once ($mainframe->getPath('toolbar_default'));

$cms	=& cmsInstance('CMSCore');

if(cmsVersion() == _CMS_JOOMLA10 || cmsVersion() == _CMS_MAMBO){
	include_once($cms->get_path('root') . '/administrator/components/com_myblog/toolbar10.myblog.php');
}else if(cmsVersion() == _CMS_JOOMLA15){
	include_once($cms->get_path('root') . '/administrator/components/com_myblog/toolbar15.myblog.php');
}

switch ($task) {
	case 'latestnews':	
		menuMyBlog :: UPDATES_MENU();
		break;
		
	case 'dashboard':	
		menuMyBlog :: DASHBOARD_MENU();
		break;
		
	case "config" :
		menuMyBlog :: CONFIG_MENU();
		break;

	case "edit" :
		menuMyBlog :: FILE_MENU();
		break;
		
	case "about" :
		menuMyBlog :: ABOUT_MENU();
		break;

	case "stats" :
		menuMyBlog :: STATS_MENU();
		break;
		
	case "category":
		menuMyBlog :: TAGS_MENU();
	   break;
	   
	case "blogs":
		menuMyBlog :: ENTRIES_MENU();
		break;
	
	case "bots":
		menuMyBlog :: BOTS_MENU();
		break;
	
	case "addBot":
		menuMyBlog :: ADDBOT_MENU();
		break;
	
	case "install":
		menuMyBlog :: INSTALL_MENU();
		break;

	case "contentmambots":
		menuMyBlog :: MAMBOTS_MENU();
		break;
		
	case "license":
		menuMyBlog :: LICENSE_MENU();
		break;
		
	case "maintenance":
		menuMyBlog :: MAINTD_MENU();
		break;
		
	default :
		menuMyBlog :: ENTRIES_MENU();
		break;
}
?>
