<?php	
	class menuMyBlog{
	    function CONFIG_MENU() {
		mosMenuBar::startTable();
		mosMenuBar::save( 'savesettings', 'Save' );				
	    mosMenuBar::back();
	    mosMenuBar::spacer();
	    mosMenuBar::endTable();
	  }
		
	   function FILE_MENU() {
	    mosMenuBar::startTable();
	    mosMenuBar::save();
	    mosMenuBar::cancel();
	    mosMenuBar::spacer();
	    mosMenuBar::endTable();
	  }
	
	  function ABOUT_MENU() {
	    mosMenuBar::startTable();
	    mosMenuBar::back();
	    mosMenuBar::spacer();
	    mosMenuBar::endTable();
	  }
	  
	  function STATS_MENU() {
	    mosMenuBar::startTable();
	    mosMenuBar::back();
	    mosMenuBar::spacer();
	    mosMenuBar::endTable();
	  }
	  
	  function UPDATES_MENU() {
	    mosMenuBar::startTable();
	    mosMenuBar::back();
	    mosMenuBar::spacer();
	    mosMenuBar::endTable();
	  }
	  
	  function LICENSE_MENU() {
	    mosMenuBar::startTable();
	    mosMenuBar::back();
	    mosMenuBar::spacer();
	    mosMenuBar::endTable();
	  }
	  function MAINTD_MENU() {
	    mosMenuBar::startTable();
	    mosMenuBar::back();
	    mosMenuBar::spacer();
	    mosMenuBar::endTable();
	  }
	  
	  function DASHBOARD_MENU() {
	    mosMenuBar::startTable();
	    mosMenuBar::back();
	    mosMenuBar::spacer();
	    mosMenuBar::endTable();
	  }
	  
	  
	  function IMPORT_MENU() {
	    mosMenuBar::startTable();
	    mosMenuBar::back();
	    mosMenuBar::spacer();
	    mosMenuBar::endTable();
	  }
	  
	  function MENU_Default() {
	    mosMenuBar::startTable();
	    mosMenuBar::publish();
	    mosMenuBar::unpublish();
	    mosMenuBar::editList();
	    mosMenuBar::deleteList();
	    mosMenuBar::endTable();
	  }
	  
	  function TAGS_MENU(){
		mosMenuBar::startTable();
	    mosMenuBar::back();
	    mosMenuBar::spacer();
	    mosMenuBar::endTable();
	}
	  
	  function ENTRIES_MENU() {
	    mosMenuBar::startTable();
	    mosMenuBar::publish('publishEntries');
	    mosMenuBar::unpublish('unpublishEntries');
	    mosMenuBar::deleteList('','deleteEntries');
	    mosMenuBar::endTable();
	  }
	  
	  function BOTS_MENU() {
	    mosMenuBar::startTable();
	    mosMenuBar::publish('publishBots');
	    mosMenuBar::unpublish('unpublishBots');
	    # mosMenuBar::editList();
	    mosMenuBar::addNew('addBot');
		//mosMenuBar::deleteList('deleteBots');
	    mosMenuBar::endTable();
	  }
	  
	  function MAMBOTS_MENU() {
	    mosMenuBar::startTable();
	    mosMenuBar::publish('publishMambots');
	    mosMenuBar::unpublish('unpublishMambots');
	    # mosMenuBar::editList();
		//mosMenuBar::deleteList('deleteBots');
	    mosMenuBar::endTable();
	  }
	  
	  function ADDBOT_MENU(){
	  	mosMenuBar::startTable();
	    mosMenuBar::save('saveBot');
	    mosMenuBar::cancel('bots');
	    # mosMenuBar::editList();
	    mosMenuBar::endTable();
	  }
	  
	  function INSTALL_MENU(){
	  	mosMenuBar::startTable();
	    mosMenuBar::save('saveInstall', 'Save');
	    mosMenuBar::cancel('exitInstall', 'Exit Installation');
	    # mosMenuBar::editList();
	    mosMenuBar::endTable();
	  }
	}
