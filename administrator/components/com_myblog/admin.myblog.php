<?php
(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

global $sectionid, $database, $_MY_CONFIG, $section, $catid, $jax, $sections, $mainframe;
include_once(dirname(dirname(dirname(dirname(__FILE__)))) . '/components/libraries/cmslib/spframework.php');

$cms  =& cmsInstance('CMSCore');
$cms->load('helper', 'url');


require_once($cms->get_path('root').'/components/com_myblog/defines.myblog.php');

if(cmsVersion() == _CMS_JOOMLA10 || cmsVersion() == _CMS_MAMBO){
	include_once($cms->get_path('root') . '/includes/sef.php');
}elseif(cmsVersion() == _CMS_JOOMLA15){
	//include_once($cms->get_path('root') . '/plugins/system/sef.php');
}

if(!class_exists('AzrulJXCachedTemplate'))
	include_once($cms->get_path('plugins') . '/system/pc_includes/template.php');

if(!defined('JAX_SITE_ROOT'))
	require_once ($cms->get_path('plugins') . '/system/pc_includes/ajax.php');

require_once ($cms->get_path('root') . '/components/com_myblog/functions.myblog.php');
require_once ($cms->get_path('root') . "/administrator/components/com_myblog/config.myblog.php");
require_once ($cms->get_path('root') . "/administrator/components/com_myblog/functions.admin.php");


$_MY_CONFIG = new MYBLOG_Config();

$sectionid = $_MY_CONFIG->sectionid;
$catid = $_MY_CONFIG->catid;
$sections = $_MY_CONFIG->get('managedSections');

if ($sections == "")
	$sections = "-1";
	
$jax = new JAX($cms->get_path('plugin-live') . "/system/pc_includes");
$jax->setReqURI($cms->get_path('live') . "/administrator/index2.php");
if(@isset($_REQUEST['task']) && ($_REQUEST['task'] == 'azrul_ajax')){
	// Only include ajax file if needed
	require_once('ajax.myblog.php');
}
$jax->process();

$cid = cmsGetVar('cid', 0,'REQUEST');
$task = cmsGetVar('task', '', 'POST');
$title = '';

if (empty ($task)){
	$task = cmsGetVar('task' ,'comments', 'GET');
}


if ($task == "xajax") {
	showAjaxedAdmin();
} else {
	ob_start();
	showAjaxedAdmin($task);
	$panel = ob_get_contents();
	ob_end_clean();
	ob_start();

	$content = '';
	$title = '';
	switch ($task) {
		case "config" :
			if(cmsVersion() != _CMS_JOOMLA15)
				$title = 'Configuration';
			showConfig();
			break;

		case "savesettings" :
			saveConfig();
			break;

		case "license":
			if(cmsVersion() != _CMS_JOOMLA15)
				$title = 'License Agreement';
			showLicense();
			break;

		case "info" :
			showInfo();
			break;
		
		case "publish" :
		case "publishEntries" :
			publishBlog($cid, 1, $option);
			break;
		
		case "unpublish" :
		case "unpublishEntries" :
			publishBlog($cid, 0, $option);
			break;

		case "publishBots" :
			publishBots($cid, 1, $option);
			break;

		case "unpublishBots" :
			publishBots($cid, 0, $option);
			break;

		case "publishMambots" :
			publishMambots($cid, 1, $option);
			break;

		case "unpublishMambots" :
			publishMambots($cid, 0, $option);
			break;

		case "addBot" :
			addBot();
			break;

		case "saveBot" :
			saveBot();
			break;

		case "deleteBots" :
			deleteBots($cid, $option);
			break;

		case "frontpage" :
			frontpageBlog($cid, 1, $option);
			break;

		case "unfrontpage" :
			frontpageBlog($cid, 0, $option);
			break;

		case "category" :
			if(cmsVersion() != _CMS_JOOMLA15)
				$title = 'Manage Tags';
			showCategories();
			break;

		case "about" :
			showAbout();
			break;

// 		case "bots" :
// 
// 			showBots();
// 			break;

		case "orderup" :
		case "orderdown" :
			orderBot(intval($cid[0]), ($task == 'orderup' ? -1 : 1), $option, $client);
			break;

		case "install" :
			showInstallWizard();
			break;

		case "saveInstall" :
			saveInstall();
			break;

		case "exitInstall" :
			cmsRedirect("index2.php?option=com_myblog", "Installation complete.Thank you for using MyBlog!");
			break;
		
		case "remove":
		case "deleteEntries" :
			removeBlogs($cid);
			break;

		case "contentmambots" :
			if(cmsVersion() != _CMS_JOOMLA15)
				$title = 'Content mambots integration';
			showMambots();
			break;

		case "maintenance":
			if(cmsVersion() != _CMS_JOOMLA15)
				$title = 'Maintenance';
			showMaintenance();
			break;

		case "fixlinks":
			myFixLinks();
			cmsRedirect("index2.php?option=$option&task=maintenance", "$new_permalinks new permalinks added. $modified_permalinks permalinks modified.");
			break;
		case "clearcache":
		    myClearCache();
		    cmsRedirect('index2.php?option=com_myblog&task=maintenance', 'Cache cleared.');
		    break;
		case 'fixdashboardlinks':
			myFixDashboardLinks();
			cmsRedirect("index2.php?option=$option&task=maintenance", "My Blog dashboard link fixed.");
			break;

		case "fixIntrotext":
			fixIntrotext();
			break;
		case 'latestnews':
			/**
		     * Show latest news for My Blog
		     **/
		    if(cmsVersion() != _CMS_JOOMLA15)
		    	$title = "Latest updates";
			showLatestNews();
			break;
		case 'dashboard':
			/**
		     * Show dashboard
		     **/
			//showDashboard();
			showNewDashboard();
			break;
		case "blogs" :
		default :
			if(cmsVersion() != _CMS_JOOMLA15)
				$title = 'List blog entries';
			showBlogs();
			break;
	}

	$content = ob_get_contents();
	ob_end_clean();
	$content = str_replace(array('{CONTENT}', '{TITLE}'), array($content, $title), $panel);
	echo $content;
}


function _recreateTables() {
	global $database, $option;

	$db =& cmsInstance('CMSDb');

	$db->query("DROP TABLE `#__myblog_categories`");
	$db->query("CREATE TABLE IF NOT EXISTS `#__myblog_categories` ( `id` int(10) unsigned NOT NULL auto_increment,`name` varchar(50) NOT NULL default '',PRIMARY KEY (`id`) ) TYPE=MyISAM");
	$db->query("DROP TABLE `#__myblog_content_categories`");
	$db->query("CREATE TABLE IF NOT EXISTS `#__myblog_content_categories` ( `id` int(10) unsigned NOT NULL auto_increment,`contentid` int(10) unsigned NOT NULL default '0',`category` int(10) unsigned NOT NULL default '0',PRIMARY KEY (`id`) ) TYPE=MyISAM");
	$db->query("DROP TABLE `#__myblog_images`");
	$db->query("CREATE TABLE IF NOT EXISTS `#__myblog_images` ( `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,`filename` TEXT NOT NULL DEFAULT '',`contentid` INTEGER UNSIGNED NOT NULL DEFAULT 0,`user_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,PRIMARY KEY(`id`) ) TYPE=MYISAM");
	$db->query("DROP TABLE `#__myblog_permalinks`");
	$db->query("CREATE TABLE IF NOT EXISTS `#__myblog_permalinks` ( `contentid` INTEGER UNSIGNED NOT NULL,`permalink` TEXT NOT NULL DEFAULT '',PRIMARY KEY(`contentid`) ) TYPE=MYISAM");
	return;
}

/**
 *	Create sample data for My Blog if new installation
 */ 
function _importSampleData() {
	global $database, $option, $my, $sectionid;
	$cms    =& cmsInstance('CMSCore');
	$db		=& cmsInstance('CMSDb');

	$cms->load('libraries', 'user');
	$title		= mysql_real_escape_string('Welcome to MyBlog!');
	$content	= mysql_real_escape_string('<p>Hello,</p><p> Thank you for using <strong>MyBlog!</strong>,the premier blogging tool for the popular Content Management System-Joomla!.<strong>MyBlog!</strong> is a feature packed,AJAX enabled replacement for the Joomla! Blog component.</p><p>&nbsp;</p><div style="text-align:center"><img src="components/com_myblog/images/icon.png" border="0" alt="icon.png" hspace="4" vspace="4" width="199" height="60" /></div><p>&nbsp;</p><p>&nbsp;Among the features currently implemented are:</p><ul><li><strong>MyBlog! </strong>Dashboard</li></ul><blockquote> <ul><li>Quick search and linking of previous posts while writing a blog entry</li><li>Tags / Tagclouds</li><li> Easy image upload and browsing using MyBlog!&#39;s own image browser</li><li>SEF friendly permanent links for each blog entry</li><li> Single-click publishing/unpublishing</li><li>AJAXed page view of my blog entries </li></ul></blockquote> <ul><li><strong>MyBlog! </strong>Admin</li></ul><blockquote><ul><li>Publishing/posting permissions</li><li>3rd party MyBlog! addons support similar to Mambots</li><li><a href="http://www.azrul.com/product/joomla_comment_system.html">Jomcomment</a> integration</li><li>Moderate blogs,tags/categories</li></ul></blockquote><ul><li> <strong>MyBlog!</strong> Frontpage view</li></ul><blockquote><ul><li>Browse blogs by keyword,blogger,or tags</li><li>Simple and easy view of all blog entries / my blog entries <br /></li><li>Templating support</li><li><a href="http://del.icio.us">del.icio.us</a> &bull;<a href="http://www.digg.com/">digg</a> &bull;<a href="http://www.spurl.net">spurl</a> &bull;<a href="http://reddit.com/">reddit</a> &bull;<a href="http://www.furl.net">furl</a> social bookmarking support</li></ul></blockquote><ul><li> Future features:</li></ul><ul><li>RSS feeds</li><li>Trackbacks</li><li>More ready-made templates</li><li>Community-builder support</li><li>Archived view of posts</li><li> and more...! </li></ul><p>As part of our product improvement process,we would like you to submit any queries,suggestions,or comments regarding MyBlog! on our <a href="http://www.azrul.com/forum/index.php/board,6.0.html">forums at Azrul.com</a>.</p><p> Thank you!</p><p>-MyBlog Dev Team.</p><p>&nbsp;</p>');

	$strSQL	= "INSERT INTO #__content SET "
			. "created_by='{$cms->user->id}', "
			. "title='{$title}', "
			. "introtext='{$content}', "
			. "state='1', "
			. "created='2007-01-18 09:58:56', "
			. "modified='2007-01-18 10:01:34', "
			. "sectionid='{$sectionid}'";

	$db->query($strSQL);
	$insertid = $db->insertid();
	$db->query("INSERT into #__myblog_permalinks SET contentid='$insertid',permalink='welcome-to-myblog.html'");
	$categories = array (
	'gadgets',
	'sports',
	'myblog'
	);
	foreach ($categories as $cat) {
		$db->query("INSERT INTO #__myblog_categories SET name='$cat'");
	}
	$cid = $insertid;
	$db->query("SELECT id from #__myblog_categories WHERE name='myblog'");
	$catid = $db->get_valuet();

	$db->query("INSERT into #__myblog_content_categories SET contentid='$cid',category='$catid'");
	$db->query("INSERT into #__myblog_user SET user_id='{$cms->user->id}',description='Write something to describe your blog'");

	return "Sample data imported.";
}

/**	NOT USED
 *	Show Installation Wizard 
 */ 
function showInstallWizard() {
	$step = "";
	if (isset ($_GET['step']))
	$step = $_GET['step'];
	switch ($step) {
		case "menus" :
			showInstallMenus();
			break;
		case "settings" :
			showInstallSettings();
			break;
		case "data" :
			showInstallData();
			break;
		default :
			break;
	}
}

/**	NOT USED
 *	Show page to create menus
 */ 
function showInstallMenus() {
	global $database, $option;
	$db	=& cmsInstance('CMSDb');
	?> <form action="index2.php?option=com_myblog&task=install" method="POST" name="adminForm"> <table cellpadding="4" cellspacing="0" border="0" width="100%"> <tr> <td width="100%" class="sectionname">&nbsp;</td> </tr> </table> <table width="860px" border="0" cellspacing="0" cellpadding="0" class="mytable" width="860px"> <th colspan="3">Setup Menus</th> <tr> <td width="23%"><b>Menu Type</b></td> <td width="25%"><select name="menutype" id="menutype"> <?php $database->setQuery("SELECT distinct(menutype) FROM #__menu");$menutypes=$database->loadObjectList();foreach ($menutypes as $menu) { echo "<option value=\"$menu->menutype\">$menu->menutype</option>";} ?> </select></td> <td width="52%">&nbsp;</td> </tr> <tr> <td><strong>Name</strong></td> <td><input type="text" name="menuname" id="menuname" value="" size="40"/></td> <td>&nbsp;</td> </tr> <tr> <td><strong>Link to</strong></td> <td><select name="menulink" id="menulink"> <option value="dashboard">MyBlog Dashboard Home</option> <option value="write">Write a new MyBlog entry</option> <option value="viewall">View all MyBlog entries</option> <option value="viewuser">View user's MyBlog entries only</option> </select></td> <td>&nbsp;</td> </tr> <tr> <td><strong>Open in</strong></td> <td><select name="menutarget" id="menutarget"> <option value="0">Current Window</option> <option value="1">New Window</option> </select></td> <td>&nbsp;</td> </tr> <tr> <td><strong>Access Level</strong></td> <td> <?php

	$query = "SELECT id AS value,name AS text" . "\n FROM #__groups" . "\n ORDER BY id";
	$db->query($query);
	$groups = $db->get_object_list();
	if(cmsVersion() == _CMS_JOOMLA10 || cmsVersion() == _CMS_MAMBO){
		$access = mosHTML :: selectList($groups, 'menuaccess', 'class="inputbox" size="3"', 'value', 'text', "1");
	}elseif(cmsVersion() == _CMS_JOOMLA15){
		$access = JHTML::_('select.genericlist',   $groups, 'menuaccess', 'class="inputbox" size="1"','value', 'text',"1");
	}

	echo $access;
	?> </td> <td>&nbsp;</td> </tr> <tr> <td colspan="3">&nbsp;</td> </tr> </table> <input type="hidden" name="option" value="<?php echo $option;?>"> <input type="hidden" name="act" value="<?php ?>"> <input type="hidden" name="task" value="saveInstall"> <input type="hidden" name="step" value="menus"> <input type="hidden" name="boxchecked" value="0"> </form> <?php }

	function showInstallSettings() {
		ob_start();
		showConfig();
		$installContent=ob_get_contents();
		ob_end_clean();
		$installContent=str_replace("</form>","<input type=\"hidden\" name=\"step\" value=\"settings\" /></form>",$installContent);
		echo $installContent;
	}

	function showInstallData() {
		global $database,$option,$sectionid;
		$db	=& cmsInstance('CMSDb');
?> 
	<form action="index2.php?option=com_myblog&task=install" method="POST" name="adminForm"> 
	<table cellpadding="4" cellspacing="0" border="0" width="100%"> <tr> <td width="100%" class="sectionname">&nbsp;</td> </tr> 
	</table> 
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="adminlist"> <th colspan="3">Setup Data</th> 
		<?php $db->query("SELECT count(*) from #__content WHERE sectionid=$sectionid");
		$contentcount = $db->get_value();
		if ($contentcount > 0) { ?> <tr> <td width="23%"><b>Previous installation detected.<br/>Recreate tables?</b></td> <td width="25%">
		<input id="freshinstall0" class="inputbox" type="radio" checked="checked" value="no" name="freshinstall"/> 
		<label for="freshinstall0">No</label> 
		<input id="freshinstall1" class="inputbox" type="radio" value="yes" onclick="alert('Warning:All previous MyBlog content,image upload and category data will be deleted if you select yes.');" name="freshinstall"/> 
		<label for="freshinstall1">Yes</label></td> 
		<td width="52%">Note:All MyBlog image upload,category and content data tables will be lost if 'yes' is selected.</td> </tr> <?php } ?> 
		<tr> <td width="23%"><b>Import data from</b></td> <td width="25%"><select name="source" id="source"> 
		<option value="none">(none)</option> 
		<option value="sample">Sample Data</option> 
		<?php $db->query("SELECT name from #__components WHERE name='Mamblog'");
		if ($db->get_value()) echo '<option value="mamblog">Mamblog</option>';?> 
		<option value="joomla">Joomla!</option> </select></td> <td width="52%">&nbsp;</td> </tr> <tr> <td colspan="3">&nbsp;</td> </tr> </table> 
		<input type="hidden" name="option" value="<?php echo $option;?>"> <input type="hidden" name="act" value="<?php ?>"> 
		<input type="hidden" name="task" value="saveInstall"> <input type="hidden" name="step" value="data"> 
		<input type="hidden" name="boxchecked" value="0"> </form> <?php 
	}


	function showInstallPanel(){
	global $mainframe;
	
	$cms	=& cmsInstance('CMSCore');
?> 
	<link rel="stylesheet" type="text/css" href="<?php echo $cms->get_path('live');?>/components/com_myblog/css/niftyCorners.css"> 
	<script type="text/javascript" src="<?php echo $cms->get_path('live');?>/components/com_myblog/js/nifty.js"></script> 
	<script type="text/javascript"> window.onload=function(){ 
		if(!NiftyCheck())
		return;
		Rounded("div#sideNav","all","#FFF","#F8F8F8","border #ccc");
	}
	</script> 
	<div class="loading" id="loadingDiv" style="display:none;"> 
	<img src="<?php echo $cms->get_path('live');?>/components/com_myblog/images/busy.gif" width="16" height="16" align="absmiddle">&nbsp;Loading...</div> 
	<div style="background-color:#F8F8F8" id="sideNav"> 
	<table width="100%" border="0" cellspacing="0" cellpadding="0"> 
	<tr> <td scope="col"> 
	<div> <h3 align="center">Installation</h3> </div> 
	<div class="sideNavTitle">Steps</div> 
	<div class="sideNavContent"> 
	<p><a href="index2.php?option=com_myblog&task=install&step=menus">Create Menus</a></p> 
	<p><a href="index2.php?option=com_myblog&task=install&step=settings">General settings</a>
	</p> <p><a href="index2.php?option=com_myblog&task=install&step=data">Setup Data</a></p> </div> 
	<div class="sideNavTitle">About / Support </div> <div class="sideNavContent"> 
	<p><a href="index2.php?option=com_myblog&task=about">About My Blog </a></p> <p>License Information </p> 
	<p>Support</p> </div></td> </tr> </table> </div> 
	<p align="center">
	<?php $cms =& cmsInstance("CMSCore"); 
	require_once( $cms->get_path("root").'/includes/domit/xml_domit_lite_include.php' );
	$xmlDoc=new DOMIT_Lite_Document();$xmlDoc->resolveErrors( true );
	$myblog_version="n/a";if ($xmlDoc->loadXML( $cms->get_path("root")."/administrator/components/com_myblog/myblog.xml",false,true )) {
		$root=&$xmlDoc->documentElement;$element =&$root->getElementsByPath('version',1);$myblog_version =$element ? $element->getText():'';} echo "Version $myblog_version";?></p> <?php }


		function showAjaxedAdmin($task){
			global $database, $mainframe, $option, $jax;
			$cms		 =& cmsInstance('CMSCore');

			//Get the task and set the width of the admin table if the task is dashboard
			if(cmsVersion() == _CMS_JOOMLA10 || cmsVersion() == _CMS_MAMBO)
			$task   = mosGetParam($_GET,'task','');

			
			echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"" . $cms->get_path('live') . "/components/com_myblog/css/admin_style.css\" />";
?>
	<script src="<?php echo $cms->get_path('live');?>/includes/js/tabs/tabpane_mini.js" type="text/javascript"></script> 
	<?php echo $jax->getScript();?> 
	<style type="text/css">
		.loading { 
		font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;font-weight:bold;color:#000000;background-color:#FFFF99;padding:4
		ppublishBlog( $cid=null,$publish=1,$option )x 16px;
		/* position:absolute; **/
		top:0px;
		right:0px;
		} --> 
	</style> 
		<table width="100%" border="0" cellspacing="4" cellpadding="6" align="left">
		<tr> <td width="10%" valign="top" align="left">
		<?php if ($task=='install') showInstallPanel();
			else showSidePanel();?> 
		</td> 
		<td width="90%" valign="top" align="left">
				<div id="mainAdminContent">
				    <img src="<?php echo $cms->get_path('live'); ?>/administrator/components/com_myblog/logo.png" />
					<h1>{TITLE}</h1>
				<div>{CONTENT}</div>
			</td> 
		</tr> 
		</table> 
		<?php

		}

	/**
	*	Called via AJAX to view a blog entry from list of blog entries
	*/ 
	function viewEntry($contentid)
	{
		$db	=& cmsInstance('CMSDb');
		global $database, $sections, $mainframe;
		$db->query("SELECT c.*,p.permalink from #__content as c,#__myblog_permalinks as p WHERE c.id=p.contentid and c.id='$contentid' and c.sectionid in ($sections)");
		$row = $db->get_object_list();

		$objResponse = new JAXResponse();
		if ($row)
		{
			$row = $row[0];
			$content = '<td colspan="6">'. $row->fulltext . '</td>';

			# Add mosConfig_live_site to all relative URLs, since we are in /administrator instead of /
			$content = preg_replace("/(<\s*(a|img)\s+[^>]*(href|src)\s*=\s*[\"'])(?!http)([^\"'>]+)[\"'>]/i", "$1".$cms->get_path('live') ."/$4\"", $content);
			$row->introtext = preg_replace("/([\x80-\xFF])/e", "chr(0xC0|ord('\\1')>>6).chr(0x80|ord('\\1')&0x3F)", $row->introtext);
			$row->fulltext = preg_replace("/([\x80-\xFF])/e", "chr(0xC0|ord('\\1')>>6).chr(0x80|ord('\\1')&0x3F)", $row->fulltext);
			//$objResponse->addAssign("$contentid"."_content", "innerHTML", str_replace("&quot;", '"', $content));
			$objResponse->addScriptCall("eval","if (document.getElementById('$contentid"."_content').style.display=='none')
									document.getElementById('$contentid"."_content').style.display='';
								else
									document.getElementById('$contentid"."_content').style.display='none';
								
								var entryCell = document.createElement('td');
								entryCell.colSpan='6';
								entryCell.style.textAlign='left';
								entryCell.innerHTML = '".str_replace("&#39;", "\'", str_replace("&quot;", '\"', str_replace('\&#39;', "\\\&#39;", addslashes(str_replace(array("\r","\n"), array(" ", " "), $row->introtext . ($row->introtext!="" ? "<p>=======================</p>" : "")  . $row->fulltext)))))."';
								var emptyCell = document.createElement('td');
								var content_el = document.getElementById('$contentid"."_content');
								if (document.getElementById('$contentid"."_content').style.display!='none')
									document.getElementById('$contentid"."_content').appendChild(entryCell);
								if (document.getElementById('$contentid"."_content').style.display=='none' && content_el.firstChild)
									content_el.removeChild(content_el.firstChild);
								if (document.getElementById('$contentid"."_content').style.display=='none' && content_el.firstChild)
									content_el.removeChild(content_el.firstChild);");

		}
		$objResponse->sendResponse();

	}

	/**
	 * Publish selected blog entry
	 */ 
	function publishBlog($cid = null, $publish = 1, $option) {
		global $database, $mainframe;
		$cms	=& cmsInstance('CMSCore');
		$cms->load('helper', 'url');
		
		$db	=& cmsInstance('CMSDb');

		if (!is_array($cid) || count($cid) < 1) {
			$action = $publish ? 'publish' : 'unpublish';
			echo "<script> alert('Select an item to $action');window.history.go(-1);</script>\n";
			exit;
		}
		$cids = implode(',', $cid);
		$db->query("UPDATE #__content SET state='$publish' WHERE id IN ($cids)");

		$mosmsg = $publish ? 'Blog entries published' : 'Blog entries unpublished';
		$server_string = htmlspecialchars($_SERVER['QUERY_STRING'], ENT_QUOTES);
		$server_string = str_replace('&amp;', '&',$server_string); 				
		$server_string = preg_replace('/(&mosmsg=.*)/', '', $server_string);
		
		cmsRedirect("index2.php?" . $server_string. '&mosmsg='. urlencode($mosmsg));
	}

	/**
	 *	Publish selected myblog bots
	 */ 
	function publishBots($cid = null, $publish = 1, $option) {
		global $database;
		$cms	=& cmsInstance('CMSCore');
		$cms->load('helper', 'url');
		$db	=& cmsInstance('CMSDb');

		if (!is_array($cid) || count($cid) < 1) {
			$action = $publish ? 'publish' : 'unpublish';
			echo "<script> alert('Select an item to $action');window.history.go(-1);</script>\n";
			exit;
		}
		$cids = implode(',', $cid);
		$db->query("UPDATE #__myblog_bots SET published='$publish' WHERE id IN ($cids)");
		cmsRedirect("index2.php?option=$option&task=bots");
	}

	/**
	 *	Publish selected Mambots
	 */ 
	function publishMambots($cid = null, $publish = 1, $option) {
		global $database;
		$cms	=& cmsInstance('CMSCore');
		$cms->load('helper', 'url');
		$db	=& cmsInstance('CMSDb');

		if (!is_array($cid) || count($cid) < 1) {
			$action = $publish ? 'publish' : 'unpublish';
			echo "<script> alert('Select an item to $action');window.history.go(-1);</script>\n";
			exit;
		}
		$cids = implode(',', $cid);
		$db->query("UPDATE #__myblog_mambots SET my_published='$publish' WHERE mambot_id IN ($cids)");
		cmsRedirect("index2.php?option=$option&task=contentmambots");
	}
	
	/**
 	 *	Show List of tags
	 */
	function showCategories() {
		global $mainframe, $option, $_MY_CONFIG;
			
		$cms	=& cmsInstance('CMSCore');
		$cms->load('helper', 'url');
		$cms->load('model', 'tag', 'com_myblog');
		
		$db	=& cmsInstance('CMSDb');

		$db->query("SELECT * FROM #__myblog_categories ORDER BY name ASC");
		$rows = $db->get_object_list();
		
		$iso = cmsGetISO();
		$jq  = $cms->get_path('live') . '/administrator/components/com_myblog/js/jquery-1.2.6.pack.js';
?>
<script src="<?php echo $jq;?>" type="text/javascript"></script>
<script type="text/javascript">

jQuery.noConflict();
jQuery(document).ready(
	function() {
 		showEditLink();
 		showEditSlug();
 		bindDefaultLink();
	}
);

function bindDefaultLink(){
	jQuery('.defaultTag, .notDefaultTag').each(function(){
		jQuery(this).click(function(){
			setDefaultTag(jQuery(this));
		});
	});
}

function setDefaultTag(obj){
	var id	= obj.parent().parent().find('span.tagname').attr('orgid');
	
	var set	= '0';
	
	if(obj.hasClass('defaultTag')){
		obj.parent().append('<a href="javascript:void();" class="notDefaultTag"></a>');
		obj.remove();
	} else {
		obj.parent().append('<a href="javascript:void();" class="defaultTag"></a>');
		obj.remove();
		set	= '1';
	}
	jax.call('myblog', 'myxSetDefaultCategory', id, set);
	bindDefaultLink();

}

function showEditLink(){
	jQuery('#categoryTable tr span.editlink').each(function() {
		var orgId	= jQuery(this).attr('orgid');
		var orgVal	= jQuery(this).attr('orgval');

		if(jQuery('#categoryTable tr span.tagname input') != null){
	
			orgVal	= orgVal.replace(/\'/g,'\\\'');
			
			var edit	= '<a href="javascript:void(0);" onclick="editTag(\'' + orgId +'\',\'' + orgVal +'\')">Edit</a>';
			edit	+= '<span class="errornotice"></span>';
			jQuery(this).html(edit);
		}
	});
}

function showEditSlug(){
	jQuery('#categoryTable tr span.editslug').each(function() {
		var orgId	= jQuery(this).attr('orgid');
		var orgVal	= jQuery(this).attr('orgval');

		if(jQuery('#categoryTable tr span.slugname input') != null){
			var edit	= '<a href="javascript:void(0);" onclick="editSlug(\'' + orgId +'\',\'' + orgVal +'\')">Edit</a>';
			edit	+= '<span class="errornotice"></span>';
			jQuery(this).html(edit);
		}
	});
}

function editTag(id, value){

	// restore old values from 'orgval' attr
	jQuery('#categoryTable tr span.tagname input').each(function() {
		// Restore edit link
		showEditLink();

		jQuery(this).parent().html(jQuery(this).parent().attr('orgval'));
	});

	// Add the input, save & cancel button
	var html = '<input type="text" id="tag" value="' + value + '" size="30" />';
	html 	+= '<a href="javascript:void(0);" class="saveTag">Save</a> | ';
	html 	+= '<a href="javascript:void(0);" class="cancelSaveTag">Cancel</a>';
	jQuery('#row' + id + ' span span.tagname').html(html);
	jQuery('#row' + id + ' span.editlink').html('');
	// set focus to current input box
	jQuery('#tag').focus();
	
	// Bind save button
	jQuery('#categoryTable tr span.tagname a:first').unbind('click');
	jQuery('#categoryTable tr span.tagname a:first').click(function () {
		var newTag = jQuery(this).prev().val();
		jQuery(this).parent().attr('orgval', newTag);
			
		jQuery('#categoryTable tr span.tagname input').each(function() {
			jQuery(this).parent().html(jQuery(this).parent().attr('orgval'));
		});

		if(newTag == value){
			showEditLink();
			return;
		}
		jax.call('myblog','myxUpdateCategory',id, newTag, value);
		showEditLink();
    });

	// Bind the cancel button
	jQuery('#categoryTable tr span.tagname a:last').unbind('click');
	jQuery('#categoryTable tr span.tagname a:last').click(function () {
		jQuery('#categoryTable tr span.tagname input').each(function() {
			jQuery(this).parent().html(jQuery(this).parent().attr('orgval'));
		});
		showEditLink();
	});
}

function editSlug(id, value){
	
	// restore old values from 'orgval' attr
	jQuery('#categoryTable tr span.slugname input').each(function() {
		// Restore edit link
		showEditSlug();
		
		jQuery(this).parent().html(jQuery(this).parent().attr('orgval'));
	});

	// Add the input, save & cancel button
	var html = '<input type="text" id="slug" value="' + value + '" size="30" />';
	html 	+= '<a href="javascript:void(0);" class="saveTag">Save</a> | ';
	html 	+= '<a href="javascript:void(0);" class="cancelSaveTag">Cancel</a>';
	jQuery('#row' + id + ' span span.slugname').html(html);
	jQuery('#row' + id + ' span.editslug').html('');
	// set focus to current input box
	jQuery('#slug').focus();
	
	// Bind save button
	jQuery('#categoryTable tr span.slugname a:first').unbind('click');
	jQuery('#categoryTable tr span.slugname a:first').click(function () {
		var slug	= jQuery(this).prev().val();
		jQuery(this).parent().attr('orgval', slug);
		
		jQuery('#categoryTable tr span.slugname input').each(function() {
			var val	= jQuery(this).parent().attr('orgval');
			
			// We know we dont allow '
			val		= val.replace(/\'/g,'');
			jQuery(this).parent().html(val);
		});

		if(slug == value){
			showEditSlug();
			return;
		}
		jax.icall('myblog','myxUpdateSlug',id, slug, value);
		showEditSlug();
    });

	// Bind the cancel button
	jQuery('#categoryTable tr span.slugname a:last').unbind('click');
	jQuery('#categoryTable tr span.slugname a:last').click(function () {
		jQuery('#categoryTable tr span.slugname input').each(function() {
			jQuery(this).parent().html(jQuery(this).parent().attr('orgval'));
		});
		showEditSlug();
	});
}

function addTagRow(tagName, slug , rowId, hasDefault){
	var rowClass	= jQuery('#categoryTable tr:last').hasClass('row0') ? 'row1' : 'row0';

	var html = '<tr id="row' + rowId + '" class="' + rowClass + '">';

	slug	= slug.replace(/\'/g,'\\\'');
	slug	= slug.replace(/\"/g,'\\"');
	slug	= slug.replace(/\\/g,'\\\\');
	slug	= slug.replace(/\0/g,'\\0');
	
	if(hasDefault == '1'){
		html += '<td align="center"><a href="javascript:void(0);" class="notDefaultTag">X</a></td>';
	}
	html += '<td><span onclick="jax.icall(\'myblog\',\'myxDeleteCategory\',\'' + rowId + '\');" class="CommonTextButtonSmall">Delete</span></td>';
	html += '<td>';
	html += '	<span>';
	html += '		<span class="tagname" orgval="' + tagName + '" orgid="' + rowId + '">' + tagName + '</span>';
	html += '		<span class="editlink" orgval="' + tagName + '" orgid="' + rowId + '">'+ tagName;
	html += '		<a href="javascript:void(0);"></a>';
	html += '		</span>';
	html += '	</span>';
	html += '</td>';
	html += '<td>';
	html += '	<span>';
	html += '		<span class="slugname" orgval="' + slug + '" orgid="' + rowId + '">' + slug;
	html += '			<a href="javascript:void(0);"></a>';
	html += '		</span>';
	html += '		<span class="editslug" orgval="' + slug + '" orgid="' + rowId + '">'
	html += '			<a href="javascript:void(0);"></a>';
	html += '		</span>';
	html += '	</span>';
	html += '</td>';
	html += '</tr>';
	
	jQuery('#categoryTable').append(html);
	
 	showEditLink();
 	showEditSlug();
 	bindDefaultLink();
	
}
</script>
<div class="myInfo">
	Use this page to create / delete and edit tags which can then be assigned into your blog entries.<strong>Tag Slugs</strong> are actually
	an alternative text which will be used in the URL. This prevents My Blog from searching incorrect tags due to special encoded
	characters.
</div>
<h3 id="tagnotice"></h1>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="adminlist"> 
	<thead> 
		<tr> 
<?php if($_MY_CONFIG->get('allowDefaultTags')): ?>
			<th width="10%" style="text-align: center;">Default</th>
<?php endif; ?>
			<th width="10%" scope="col">Action</th>
			<th width="39%" scope="col">Tag</th> 
			<th width="39%">Slug</th>
		</tr>
	</thead>
	<tbody id="categoryTable">
<?php
		$i	= 0;
		
		foreach ($rows as $row){
			$tagObj = new MYTag();
			$tagObj->load($row->id);
 
?>
		<tr id="row<?php echo $row->id; ?>" class="row<?php echo $i;?>">
<?php if($_MY_CONFIG->get('allowDefaultTags')): ?>
			<td align="center">
			<?php if($row->default && $row->default == '1'){ ?>
				<a href="javascript:void(0);" class="defaultTag">X</a>
			<?php } else {?>
				<a href="javascript:void(0);" class="notDefaultTag">X</a>
			<?php } ?>
<?php endif; ?>
			</td>
			<td>
				<span onclick="jax.icall('myblog','myxDeleteCategory','<?php echo $row->id;?>');" class="CommonTextButtonSmall">Delete</span>
			</td>
			<td>
				<span>
					<span class="tagname" orgval="<?php echo $row->name; ?>" orgid="<?php echo $row->id;?>">
						<?php echo $row->name; ?>
					</span>
					
					<span class="editlink" orgval="<?php echo $row->name; ?>" orgid="<?php echo $row->id;?>">
						<a href="javascript:void(0);"></a>
					</span>
				</span>
			</td>
			<td>
				<span>
					<span class="slugname" orgval="<?php echo $tagObj->getSlug(); ?>" orgid="<?php echo $row->id;?>">
						<?php echo $tagObj->getSlug(); ?>
					</span>
					<span class="editslug" orgval="<?php echo $tagObj->getSlug(); ?>" orgid="<?php echo $row->id;?>">
						<a href="javascript:void(0);"></a>
					</span>
				</span>
			</td>
		</tr>
<?php
		$i	= ($i > 0) ? 0 : 1;
	} 
?>
	</tbody>
</table>
<p> 
<form action="" method="post" name="formNewCat" id="formNewCat" onsubmit="jax.icall('myblog','myxAddCategory',document.getElementById('newCat').value);return false;"> 
<input name="newCat" id="newCat" type="text" class="CommonTextButtonSmall" size="48" maxlength="48"> 
<span class="CommonTextButtonSmall" onclick="jax.icall('myblog','myxAddCategory',document.getElementById('newCat').value);">Add New Tag</span>
&nbsp;&nbsp;<span id="categoryerror" class="errorMsg"></span>
</form>
</p>
<?php
	}

		function fixIntrotext(){
			global $database, $_MY_CONFIG, $sections;
			$cms	=& cmsInstance('CMSCore');
			$cms->load('helper', 'url');
			$db	=& cmsInstance('CMSDb');

			$db->query("SELECT id,`fulltext` FROM #__content WHERE sectionid IN ($sections) and `introtext` = '' ");
			$rows = $db->get_object_list();

			$num_fixed = 0;

			if ($rows and count($rows)>0)
			{
				foreach ($rows as $row)
				{
					$id = $row->id;
					$fulltext = $row->fulltext;

					# get introtext (first X characters)
					$introtext = getIntrotext($fulltext, $_MY_CONFIG->get('introLength'), '{readmore}');

					# get fulltext (fulltext - introtext)
					$fulltext = substr($fulltext, strlen($introtext));

					$db->query("UPDATE #__content SET `introtext`='$introtext', `fulltext`='$fulltext' WHERE id='$id'");
					//if ($database->query())
					$num_fixed++;
				}
			}

			cmsRedirect("index2.php?option=com_myblog&task=maintenance", "$num_fixed entries fixed.");

		}

		function myFixLinks()
		{
			global $database, $_MY_CONFIG, $sections;
			$db	=& cmsInstance('CMSDb');

			# Fix content entries without permalinks
			$db->query("SELECT c.id,c.title,p.contentid FROM #__content as c LEFT OUTER JOIN #__myblog_permalinks as p ON ( c.id=p.contentid ) WHERE p.contentid IS NULL and c.sectionid IN ($sections)");
			$rows = $db->get_object_list();
			$new_permalinks = 0;
			if ($rows) {
				$new_permalinks = count($rows);
				foreach ($rows as $row) {
					// remove unwanted chars
					$title = $row->title;
					$link = myTitleToLink(trim($title . ".html"));
					//removes unwanted character from url including !.
					$link = preg_replace('/[!@#$%\^&*\(\)\+=\{\}\[\]|\\<">,\\/\^\*;:\?\'\\\]/', "", $link);

					$db->query("SELECT * from #__myblog_permalinks WHERE permalink='$link' and contentid!='$row->id'");
					$linkExists = $db->get_value();
					if ($linkExists) {
						// remove unwanted chars
						$link = myTitleToLink(trim($title));
						//$link = preg_replace('/[!@#$%\^&*\(\)\+=\{\}\[\]|\\<">,\\/\^\*;:\?\'\\\]/', "", $link);

						$plink = "$link-$uid.html";
						$db->query("SELECT contentid from #__myblog_permalinks WHERE permalink='$plink' and contentid!='$row->id'");
						$count = 0;
						while ($db->get_value()) {
							$count++;
							$plink = "$link-{$row->id}-$count.html";
							$db->query("SELECT contentid from #__myblog_permalinks WHERE permalink='$plink' and contentid!='$row->id'");
						}
						//$plink = urlencode($plink);
						$db->query("INSERT INTO #__myblog_permalinks SET permalink='$plink',contentid='$row->id'");
					} else {
						//$link = urlencode($link);
						$db->query("INSERT INTO #__myblog_permalinks SET permalink='$link',contentid='$row->id'");
					}
				}
			}


			# ensure all permalinks are  'safe'
			$db->query("SELECT contentid, permalink from #__myblog_permalinks");
			$permalinks = $db->get_object_list();
			$modified_permalinks = 0;

			if ($permalinks)
			{
				$modified_permalinks = count($permalinks);

				foreach ($permalinks as $permalink)
				{
					$uid = $permalink->contentid;
					$link = $permalink->permalink;
					$link = myTitleToLink(trim($link));

					$db->query("SELECT * from #__myblog_permalinks WHERE permalink='$link' and contentid!='$uid'");
					$linkExists = $db->get_value();

					if ($linkExists) { # if link exists, find a unique permalink

						// remove unwanted characters
						//$link = preg_replace('/[!@#$%\^&*\(\)\+=\{\}\[\]|\\<">,\\/\^\*;:\?\'\\\]/', "", $link);
						//$link = myTitleToLink(trim($title));

						if (substr($link, strlen($link)-5, 5)==".html")
						$link = substr($link, 0, strlen($link)-5);

						$plink = "$link-$uid.html";
						$db->query("SELECT contentid from #__myblog_permalinks WHERE permalink='$plink' and contentid!='$uid'");
						$count = 0;

						while ($db->get_value()) {
							$count++;
							$plink = "$link-$uid-$count.html";
							$db->query("SELECT contentid from #__myblog_permalinks WHERE permalink='$plink' and contentid!='$uid'");
						}

						//$plink = urlencode($plink);

						$db->query("UPDATE #__myblog_permalinks SET permalink='$plink' WHERE contentid='$uid'");
					} else {
						$db->query("UPDATE #__myblog_permalinks SET permalink='$link' WHERE contentid=$uid");
					}
				}
			}
		}

		function showMaintenance() {
			$cms	=& cmsInstance('CMSCore');
			include_once($cms->get_path('root') . '/administrator/components/com_myblog/admin.myblog.html.php');
			myAdminMaintenance();
	?>
	
	&nbsp;&nbsp;
	<?php
		}
		
		

		/**
		 * Show latest news from our RSS Feeds
		 */
		function showLatestNews(){
			$cms    =& cmsInstance('CMSCore');

			// Load our cmslib library lastrss
			$cms->load('libraries','lastrss');
			$rss = $cms->lastrss;
			$rss->cache_dir = $cms->get_path('root') . '/cache';
			$rss->cache_time = 120;
			$rss->date_format = 'M d, Y g:i:s A';
			$rss->CDATA = 'content';

			// Variables declarations that will be used in this function
			$url    = 'http://support.azrul.com/rss/index.php?_m=news&_a=view&group=default';

			$html	= "<div style=\"width:640px;\"><p>";

			if ($rs = $rss->get($url)){
				$rs = $rss->get($url);

				foreach ($rs['items'] as $item) {
					$html	.= "<div style=\"padding:8px;border-bottom:1px dotted #666666;\"><div style=\"font-weight:bold\"><a parent=\"_blank\"href=\"{$item['link']}\">{$item['title']}</a></div>"
					. "<div>{$item['pubDate']}</div><div>{$item['description']}</div></div>";
				}
				if ($rs['items_count'] <= 0)
				$html	.= "<li>Sorry, no items found in the RSS file :-(</li>";
			}else
			$html	.= "Sorry: It's not possible to reach RSS file $url\n<br />";

			$html	.= "</p></div>";

			echo $html;
		}

		/**
		 *	Shows Configuration sEttings page
		 */ 
		function showConfig() {
			global $database, $mainframe, $option;
			$cms    =& cmsInstance('CMSCore');
			$db		=& cmsInstance('CMSDb');
			require_once($cms->get_path('root') . "/administrator/components/com_myblog/config.myblog.php");

			$config = new MYBLOG_Config();

			echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"" .$cms->get_path('live') ."/components/com_myblog/css/admin_style.css\" />";

			# Get templates list
			if(cmsVersion() == _CMS_JOOMLA10 || cmsVersion() == _CMS_MAMBO){
				$file_list = mosReadDirectory($cms->get_path('root') . "/components/com_myblog/templates", "");
			}elseif(cmsVersion() == _CMS_JOOMLA15){
				$file_list = JFolder::folders($cms->get_path('root') . "/components/com_myblog/templates", "");
			}

			$t_list = array ();
			$temp_lists  = array();

			$filecount = 0;
			foreach ($file_list as $val) {
				if (!strstr($val, "svn") and !strstr($val, "admin") and !strstr($val, "_default")){
					if(cmsVersion() == _CMS_JOOMLA10 || cmsVersion() == _CMS_MAMBO){
						$t_list[] = mosHTML :: makeOption($val, $val);
					}elseif(cmsVersion() == _CMS_JOOMLA15){
						$t_list[] = JHTML::_('select.option',  $val, $val);
					}
					$temp_lists[$val]    = $val;
				}
			}

			if(cmsVersion() == _CMS_JOOMLA10 || cmsVersion() == _CMS_MAMBO){
				$templates = mosHTML :: selectList($t_list, 'template', 'class="inputbox" size="1"', 'value', 'text', $config->get('template'));
			}elseif(cmsVersion() == _CMS_JOOMLA15){
				$templates = JHTML::_('select.genericlist',   $t_list, 'template', 'class="inputbox" size="1"','value', 'text', $config->get('template'));
			}

			$db->query("SELECT id,title FROM #__sections ORDER BY title ASC");
			$rows = $db->get_object_list();
			$sect_rows = count($rows);

			$managedSections = "<select name=\"managedSections[]\" size=\"" . count($rows) . "\" multiple>";
			$sectionid = $config->get('postSection');
			$catid = $config->get('catid');
			$sectionsArray = explode(",", $config->get('managedSections'));

			array_walk($sectionsArray, "trim");
			#var_dump($sectionsArray);
			#Get the list of the sections
			$sect_lists	= array();

			#Get the selected section lists.
			$sel_sect_lists = array();

			#var_dump($rows);
			if ($rows) {
				foreach ($rows as $row) {
					$managedSections .= "<option value='$row->id' ";
					if (in_array($row->id, $sectionsArray)) {
						$managedSections .= "selected";
						$sel_sect_lists[]   = $row->id;
					}
					$managedSections .= ">$row->title</option>";
					$sect_lists[$row->id]   = $row->title;
				}
			}
			#var_dump($sel_sect_lists);
			$managedSections .= "</select>";
			$db->query("SELECT id,title FROM #__sections ORDER BY title ASC");
			$rows = $db->get_object_list();
			$sect_rows = count($rows);
			$postInSection = "<select name=\"postSection\">"; // Post entries in section
			$postSection = $config->get('postSection');

			#Get the section lists so that we can display it to user to
			#select which section to save new entries at.
			$save_sect_lists    = array();
			if ($rows) {
				foreach ($rows as $row) {
					$postInSection .= "<option value='$row->id' ";
					if ($postSection == $row->id)
					$postInSection .= "selected";
					$postInSection .= ">$row->title</option>";
					$save_sect_lists[$row->id]  = $row->title;
				}
			}
			$postInSection .= "</select>";
			if(cmsVersion() == _CMS_JOOMLA10 || cmsVersion() == _CMS_MAMBO){
				$lang_file_list = mosReadDirectory($cms->get_path('root') . "/components/com_myblog/language", "");
			}elseif(cmsVersion() == _CMS_JOOMLA15){
				$lang_file_list = JFolder::files($cms->get_path('root') . "/components/com_myblog/language", "");
			}

			$lang_list = array ();
			$lang_filecount = 0;
			$lang_lists = array();
			foreach ($lang_file_list as $val) {
				if (!strstr($val, "svn") and substr($val, strlen($val)-4, 4)==".php"){
					if(cmsVersion() == _CMS_JOOMLA10 || cmsVersion() == _CMS_MAMBO){
						$lang_list[] = mosHTML :: makeOption($val,substr($val, 0, strlen($val)-4));
					}elseif(cmsVersion() == _CMS_JOOMLA15){
						$lang_list[] = JHTML::_('select.option', $val,substr($val, 0, strlen($val)-4));
					}
					$lang_lists[$val] = substr($val,0,-4);
				}
			}

			if(cmsVersion() == _CMS_JOOMLA10 || cmsVersion() == _CMS_MAMBO){
				$lang_files = mosHTML :: selectList($lang_list, 'language', 'class="inputbox" size="1"', 'value', 'text', $config->get('language'));
			}elseif(cmsVersion() == _CMS_JOOMLA15){
				$lang_files = JHTML::_('select.genericlist',   $lang_list, 'language', 'class="inputbox" size="1"','value', 'text', $config->get('language'));
			}

			#Load the html data from config_template.php file
			include ($cms->get_path('root') . "/administrator/components/com_myblog/templates/config_template.php");
		}



	function showNewDashboard()
	{
?>
		<link href="<?php echo MY_COM_LIVE; ?>/css/azwindow.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo MY_COM_LIVE; ?>/css/style.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="<?php echo MY_COM_LIVE; ?>/css/ui.css" type="text/css" media="screen" />
		<script type="text/javascript" language="javascript" src="<?php echo MY_COM_LIVE; ?>/js/myblog.js"></script>
<?php
	}



		/**
 *  Show dashboard in the back end.
 **/
		function showDashboard(){
			$cms    =& cmsInstance('CMSCore');
			$site   = $cms->get_path('live');

			$poweredBy  = getPoweredByLink();
			//Check if there is id passed. If there is id it means we need to edit the blog
			$blogId	= cmsGetVar('id', '', 'GET');

			if(isset($blogId) && !empty($blogId)){
				$dbLink	= rtrim($cms->get_path('live') ,'/') . '/index2.php?option=com_myblog&task=write&keepThis=true&TB_iframe=true&no_html=1&id=' . $blogId;
			}
			else{
				$dbLink	= rtrim($cms->get_path('live') ,'/') . '/index2.php?option=com_myblog&task=write&keepThis=true&TB_iframe=true&no_html=1&id=0';
			}

			// top position should be lowe in J1.5 
			$winTop = (cmsVersion() != _CMS_JOOMLA15)? '140px':'180px';
			$winBottom = (cmsVersion() != _CMS_JOOMLA15)? '0':'40';
			
			$html   = '';
			$html   .= <<<SHOWHTML

<script type="text/javascript">//<![CDATA[

function azrulWindowSave(entryid){
}

// redirect to list view
function azrulWindowSaveClose(){
	window.location = '$site/administrator/index2.php?option=com_myblog&task=blogs';
}

function editWindowTitle(nt){
	jax.$('azrulEditorMessage').innerHTML = nt;
}

var yPos;
if (window.innerHeight != null)
{
	yPos = window.innerHeight;
} else if (document.documentElement && document.documentElement.clientHeight)
{
	yPos = document.documentElement.clientHeight;
} else
{
	yPos = document.body.clientHeight;
}
yPos=yPos-230;

//]]></script>
<style type="text/css">

div #azrulWindow{
	-moz-border-radius-bottomleft:7px;
	-moz-border-radius-bottomright:7px;
	-moz-border-radius-topleft:7px;
	-moz-border-radius-topright:7px;
	-moz-box-sizing:border-box;
	background-color:/*#F2F5F7;*/ #cdcdcd;
	border:1px solid /*#D0E4FD;*/ #898989;
	margin:20px 10px 30px;
	padding:2px 10px;
	width:805px;
	position: absolute;
}

div #azrulWindowContent{
	border-left: 1px solid #898989;
	border-right: 1px solid #898989;
}

div #azrulWindowClose{
	padding: 5px;
	border-bottom: 1px solid #898989;
}

div #azrulWindowTitle{
	border-bottom: 1px solid #898989;
	padding: 5px;
}

div #azrulWindowFooter{
	font-size: 10px;
	font-family: verdana;
	border-top: 1px solid #898989;
	padding: 4px;
	text-align: right;
}
</style>
<div id="azrulWindow" style="top:$winTop;width: 780px;">
	<table cellpadding="0" cellspacing="0" width="100%">
	    <tr>
	        <td id="azrulWindowTitle" width="95%">MyBlog Editor <span id="azrulEditorMessage"></span></td>
	        <td id="azrulWindowClose" width="5%"><a href="index2.php?option=com_myblog&task=blogs">Close</a></td>
		</tr>

		<tr>
		    <td colspan="2" id="azrulWindowContent">
                <iframe id="azrulContentFrame" src="$dbLink" frameborder="0" style="width:100%;" scrolling="no"></iframe>
		    </td>
		</tr>
	</table>
	<div id="azrulWindowFooter">$poweredBy</div>
</div>
<link href="MY_COM_LIVE/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="MY_COM_LIVE/css/ui.css" type="text/css" media="screen" />

<script type="text/javascript">//<![CDATA[
iframe = document.getElementById("azrulContentFrame");
yPos = yPos - $winBottom;
if(yPos < 480){
	yPos = 480;
}
iframe.style.height = (yPos) + 'px';


//]]></script>

SHOWHTML;

			echo $html;


		}

		/**
 *	Save configuration settings
 */ 
		function saveConfig() {
			global $database, $option, $sectionid, $mainframe;

			$cms	=& cmsInstance('CMSCore');
			$cms->load('helper', 'url');
			$db		=& cmsInstance('CMSDb');
			require_once($cms->get_path('root') . "/administrator/components/com_myblog/config.myblog.php");
			$config = new MYBLOG_Config();
			$note = "";

			# Disable technorati pings if xmlrpc extension not found
			if (!function_exists('xmlrpc_encode_request')) {
				$note = "Note:x-m-l RPC extension in PHP is disabled.Ping Technorati set to 0";
			}

			$config->save();
			$config = new MYBLOG_Config();
			$managedSections = $config->get('managedSections');
			$sections = $managedSections;

			# Fix content entries without permalinks
			$db->query("SELECT c.id,c.title,p.contentid FROM #__content as c LEFT OUTER JOIN #__myblog_permalinks as p ON ( c.id=p.contentid ) WHERE p.contentid IS NULL and c.sectionid IN ($sections)");
			$rows = $db->get_object_list();
			if ($rows) {
				foreach ($rows as $row) {
					// remove unwanted chars
					$title = $row->title;
					$link = myTitleToLink(trim($title . ".html"));
					//$link = preg_replace('/[!@#$%\^&*\(\)\+=\{\}\[\]|\\<">,\\/\^\*;:\?\'\\\]/', "", $link);

					$db->query("SELECT * from #__myblog_permalinks WHERE permalink='$link' and contentid!='$row->id'");
					$linkExists = $db->get_value();
					if ($linkExists) {
						// remove unwanted chars
						$link = myTitleToLink(trim($title));
						//$link = preg_replace('/[!@#$%\^&*\(\)\+=\{\}\[\]|\\<">,\\/\^\*;:\?\'\\\]/', "", $link);

						$plink = "$link-$uid.html";
						$db->query("SELECT contentid from #__myblog_permalinks WHERE permalink='$plink' and contentid!='$row->id'");
						$count = 0;
						while ($db->get_value()) {
							$count++;
							$plink = "$link-{$row->id}-$count.html";
							$db->query("SELECT contentid from #__myblog_permalinks WHERE permalink='$plink' and contentid!='$row->id'");
						}
						//$plink = urlencode($plink);
						$db->query("UPDATE #__myblog_permalinks SET permalink='$plink' WHERE `contentid`='$row->id'");
					} else {
						//$link = urlencode($link);
						$db->query("INSERT INTO #__myblog_permalinks SET permalink='$link',contentid='$row->id'");
					}
				}
			}
			$redirect = "index2.php?option=com_myblog&task=config";
			cmsRedirect($redirect, "Settings saved.$note");
		}

		/**
 *	Save My Blog bot
 */ 
		function saveBot() {
			global $database, $mainframe, $option;
			$cms	=& cmsInstance('CMSCore');
			$cms->load('helper', 'url');
			require_once($cms->get_path('root') . "/administrator/components/com_myblog/config.myblog.php");
			$published = $_POST['published'];
			$filename = $_POST['filename'];
			$folder = $_POST['folder'];
			$botName = $_POST['botName'];
			if ($published == "" or $filename == "" or $folder == "") {
				echo "<script> alert('" . $row->getError() . "');window.history.go(-1);</script>\n";
				exit ();
			}
			$db->query("SELECT * from #__myblog_bots WHERE folder='$folder' AND filename='$filename'");
			if ($db->get_value()) {
				cmsRedirect("index2.php?option=com_myblog&task=bots", "Bot already exists.Error adding bot.");
				return;
			}
			$db->query("SELECT max(ordering) from #__myblog_bots");
			$ordering = $db->get_value() + 1;
			$db->query("INSERT INTO #__myblog_bots SET name='$botName',published='$published',ordering='$ordering',folder='$folder',filename='$filename'");
			cmsRedirect("index2.php?option=com_myblog&task=bots", "Bot added.");
		}

		/**
		 *	Show My Blog backend's side panel
		 */ 
		function showSidePanel() {
			global $mainframe;
			
			$cms  =& cmsInstance('CMSCore');
			$jax = new JAX($cms->get_path('plugin-live') . "/system/pc_includes");
			$jax->setReqURI($cms->get_path('live') . "/administrator/index2.php");
			
			// Load dashboard stuffs
			showNewDashboard();
			
			// Try login user.
			$cms->load('libraries' , 'user');
			
			$strSQL	= "SELECT session_id FROM #__session WHERE `userid`={$cms->user->id}";
			$cms->db->query( $strSQL );
			$cms->load('libraries' , 'user');
			$userId		= $cms->user->id;
			$sessionId	= $cms->db->get_value();
			
			$link		= rtrim($cms->get_path('live') ,'/') . '/index2.php?option=com_myblog&task=write&no_html=1&id=0&session=' . $userId . ':' . $sessionId;

			if(cmsVersion() == _CMS_JOOMLA15)
				$link		= rtrim($cms->get_path('live') ,'/') . '/index.php?option=com_myblog&tmpl=component&task=write&no_html=1&id=0&session=' . $userId . ':' . $sessionId;
?> 
	<link rel="stylesheet" type="text/css" href="<?php echo $cms->get_path('live');?>/components/com_myblog/css/niftyCorners.css"> 
	<script type="text/javascript" src="<?php echo $cms->get_path('live');?>/components/com_myblog/js/nifty.js"></script> 
	<script type="text/javascript"> window.onload=function(){ if(!NiftyCheck()) return;Rounded("div#sideNav","all","#FFF","#F8F8F8","border #ccc");} 
	</script> <div class="loading" id="loadingDiv" style="display:none;"> 
	<img src="<?php echo $cms->get_path('live');?>/components/com_myblog/images/busy.gif" width="16" height="16" align="absmiddle">&nbsp;Loading...</div> 
	<div style="background-color:#F8F8F8" id="sideNav"> <table width="100%" border="0" cellspacing="0" cellpadding="0"> <tr> <td scope="col"> 
	<div> <h3 align="center">Admin Panel </h3> </div> <div class="sideNavTitle"><img hspace="2" style="vertical-align: middle;" src="components/com_myblog/images/Options_16x16.gif"/>Configuration</div> 
	<div class="sideNavContent"> <p><a href="index2.php?option=com_myblog&task=config">General Settings </a></p>
	 <p><a href="index2.php?option=com_myblog&task=category">Tags</a></p> 
	 <p><a href="index2.php?option=com_myblog&task=contentmambots">Content Mambots Integration</a>
	 </p> <p><a href="index2.php?option=com_myblog&task=maintenance">Maintenance</a></div> 
	 <div class="sideNavTitle"><img hspace="2" style="vertical-align: middle;" src="components/com_myblog/images/Documents_16x16.gif"/>Manage Blog Entries</div> <div class="sideNavContent"> 
	 <p>
		<a href="javascript:void(0);" onclick="myAzrulShowWindow('<?php echo $link; ?>');">Write New Entry</a>
	 </p>
	 <p><a href="index2.php?option=com_myblog&task=blogs&limitstart=0" >View all </a></p> 
	 <p><a href="index2.php?option=com_myblog&task=blogs&limitstart=0&publish=0" >View unpublished entries</a></p> 
	 </div> <div class="sideNavTitle"><img hspace="2" style="vertical-align: middle;" src="components/com_myblog/images/Information_16x16.gif"/>About / Support </div> <div class="sideNavContent"> 
	 <p><a href="index2.php?option=com_myblog&task=about">About My Blog </a></p> 
	<p>
		<a href="index2.php?option=com_myblog&task=latestnews">Check for latest news</a>
	</p>
	 <p><a href="index2.php?option=com_myblog&task=license">License Information</a> </p> 
	 <p>Support</p> </div></td> </tr> </table> </div> <p align="center">
	 <?php $cms =& cmsInstance("CMSCore"); 

	 require_once( $cms->get_path("root").'/includes/domit/xml_domit_lite_include.php' );
	 $xmlDoc=new DOMIT_Lite_Document();$xmlDoc->resolveErrors( true );
	 $myblog_version="n/a";

	 if ($xmlDoc->loadXML( $cms->get_path("root")."/administrator/components/com_myblog/myblog.xml",false,true )) {
	 	$root=&$xmlDoc->documentElement;
	 	$element =&$root->getElementsByPath('version',1);
	 	$myblog_version =$element ? $element->getText():'';
	 }
	echo "Version $myblog_version";?></p>

<?php

		}

/**
 *	View list of blog entries
 */ 
	function showBlogs() {
		global $mainframe, $_MY_CONFIG;

		$cms	= & cmsInstance('CMSCore');
		$cms->load('helper', 'url');
		
		$db		= $cms->db;
		
		require_once($cms->get_path('root') . "/components/com_myblog/functions.myblog.php");
		require_once($cms->get_path('root') . "/administrator/components/com_myblog/config.myblog.php");

		$_MY_CONFIG = new MYBLOG_Config();
		$limit =intval( $mainframe->getUserStateFromRequest( "viewlistlimit",'limit',$mainframe->getCfg('list_limit') ) );
		$limitstart =intval( $mainframe->getUserStateFromRequest( "view{com_myblog}limitstart",'limitstart',0 ) );
		$search = $mainframe->getUserStateFromRequest("search{com_myblog}", 'search', '');
		$search = $db->_escape(trim(strtolower($search)));
		
		
		
		//$limit = mosGetParam($_GET, 'limit', $_MY_CONFIG->get('numEntry'));
		$publish = "";
		if (isset ($_GET['publish']))
			$publish = intval($_GET['publish']);

		$where = array ();
		if ($search) {
			$where[] = "LOWER(comment) LIKE '%$search%'";
		}

		if (is_int($publish)){
			$where[] = "STATE=$publish";
		}
			
		$where[] = "sectionid IN (" . $_MY_CONFIG->get('managedSections') . ")";

		$db->query("SELECT count(*) FROM #__content" . (count($where) ? "\nWHERE " . implode(' AND ', $where) : ""));
		$total = $db->get_value();

		$pagination	= myPagination($total, $limitstart , $limit);
		
		$db->query("SELECT * FROM #__content" . (count($where) ? "\nWHERE " . implode(' AND ', $where) : "") . "\n ORDER BY id DESC " . "\nLIMIT $pagination->limitstart,$pagination->limit");
		$rows = $db->get_object_list();
		$total = count($rows);
		//$$limit = 10;

		
		
		// Try login user.
		$cms->load('libraries' , 'user');
		
		$strSQL	= "SELECT session_id FROM #__session WHERE `userid`={$cms->user->id}";
		$cms->db->query( $strSQL );
		$cms->load('libraries' , 'user');
		$userId		= $cms->user->id;
		$sessionId	= $cms->db->get_value();
?>
<script type="text/javascript" language="javascript">
function toggleEntry(id){
	var entry	= document.getElementById(id + '_content');
	
	if(entry.style.display == 'none')
		entry.style.display	= '';
	else
		entry.style.display = 'none';
}
</script>
<form action="index2.php?<?php echo htmlspecialchars($_SERVER['QUERY_STRING'],ENT_QUOTES); ?>" method="POST" name="adminForm">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="adminlist">
		<thead>
		<tr>
			<th width="2%" class="title">
				<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $rows );?>);" />
			</th>
			<th width="10%">Date</th>
			<th width="25%">Title</th>
			<th width="5%">Published</th>
			<th width="5%">Edit</th>
			<th width="30%">Tags</th>
			<th width="20%">Author</th>
		</tr>
		</thead>
		<tbody>
<?php
		$i=0;$n=1;
		if ($rows) {
			foreach($rows as $row){
				$task=$row->state ? 'unpublish':'publish';
				$img=$row->state ? 'publish_g.png':'publish_x.png';
				$isFp = $db->get_value();
				$taskFp=$isFp ? 'unfrontpage':'frontpage';
				$imgFp=$isFp ? 'tick.png':'publish_x.png';
?>
		<tr>
			<td>
				<input type='checkbox' id='cb<?php echo $i;?>' name='cid[]' value='<?php echo $row->id;?>' onclick='isChecked(this.checked);' />
			</td>
			<td>
			<?php
			if(cmsVersion() == _CMS_JOOMLA10 || cmsVersion() == _CMS_MAMBO){
				echo mosFormatDate($row->created, "%d %b %Y<br/>%H:%M:%S");
			}elseif(cmsVersion() == _CMS_JOOMLA15){
				echo JHTML::_('date', $row->created);
			}
			?>
			</td>
			<td>
				<a href="javascript:void(0);" onclick="toggleEntry('<?php echo $row->id; ?>');"><?php echo htmlspecialchars($row->title);?></a>
			</td>
			<td width="5%" align="center">
				<a href="javascript:void(0);" onclick="jax.icall('myblog','myxTogglePublishAdmin','<?php echo $row->id;?>');">
				<img id="pubImg<?php echo $row->id;?>" src="images/<?php echo $img;?>" hspace="8" width="12" height="12" border="0" alt="" /></a>
			</td>
			<td align="center">
				[&nbsp;<a href="javascript:void(0);" onclick="myAzrulShowWindow('<?php echo rtrim($cms->get_path('live') ,'/');?>/index2.php?option=com_myblog&task=write&no_html=1&id=<?php echo $row->id;?>&session=<?php echo $userId . ':' . $sessionId;?>');">Edit</a>&nbsp;]
			</td>
			<td><?php echo myCategoriesURLGet($row->id,false,false,false);?></td>
			<td align="center">
				<?php echo myUserGetName($row->created_by);?>
			</td>
		</tr>
		<tr id="<?php echo $row->id; ?>_content" style="display: none;">
			<td colspan="7"><?php echo $row->introtext . $row->fulltext; ?></td>
		</tr>
		<?php $i++;
			}
		}
		?>
		</tbody>
	<tfoot>
		<tr>
		<td colspan="7">
		<input type="hidden" name="option" value="com_myblog" />
		<input type="hidden" name="task" value="blogs" />
		<input type="hidden" name="boxchecked" value="0" />
		<div style="width:100%;"><?php echo $pagination->footer;?></div>
		</td>
		<tr>
	</tfoot>
	</table>
</form>
<?php
		}

	/**
	*	Delete selected blog entries
	*/ 
	function removeBlogs($cid){
		global $database, $mainframe;
		$cms	=& cmsInstance('CMSCore');
		$cms->load('helper', 'url');
		$db	=& cmsInstance('CMSDb');
	
		if ($cid and !empty($cid)) {
			foreach ($cid as $uid) {
				$db->query("DELETE FROM #__content WHERE id=$uid");
				$db->query("DELETE FROM #__myblog_permalinks WHERE contentid=$uid");
				$db->query("DELETE FROM #__myblog_images WHERE contentid=$uid");
				$db->query("DELETE FROM #__myblog_content_categories WHERE contentid=$uid");
			}
		}
	
		cmsRedirect($cms->get_path('live')."/administrator/index2.php?option=com_myblog&task=blogs&limitstart=0","Blog entries deleted.");
	}

		/**
 *	Show list of myblog bots
 */ 
		function showBots(){
			global $database, $mainframe, $option, $_MY_CONFIG;
			$cms =& cmsInstance("CMSCore");
			
			require_once($cms->get_path("root")."/components/com_myblog/functions.myblog.php");
			require_once($cms->get_path("root")."/administrator/components/com_myblog/config.myblog.php");
			$_MY_CONFIG=new MYBLOG_Config();$publish="";
			if (isset($_GET['publish'])) $publish= intval($_GET['publish']);
			
			$cms->db->query( "SELECT count(*) FROM #__myblog_bots");
			$total = $cms->db->get_value();
			$cms->db->query( "SELECT * FROM #__myblog_bots" ."\n ORDER BY ordering ASC ");
			$rows = $cms->db->get_object_list();
	$total = count($rows);?>
	<form action="index2.php?option=com_myblog&task=bots&publish=<?php echo $publish;?>" method="post" name="adminForm"> 
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="adminlist"> <tr> <th width="2%" class="title">
	<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $rows );?>);" /></th> 
	<th width="35%">&nbsp;Bot Name</th> <th width="20%">&nbsp;Folder</th> <th width="20%">&nbsp;Filename</th> <th width="5%">&nbsp;Published</th> 
	<th width="15%">&nbsp;&nbsp;Order</th> </tr> <?php $i=0;
	foreach($rows as $row){
		$task=$row->published ? 'unpublish':'publish';
		$img=$row->published ? 'publish_g.png':'publish_x.png';?> 
		<tr id='bot<?php echo $i ?>'> <td>
		<input type='checkbox' id='cb<?php echo $i;?>' name='cid[]' value='<?php echo $row->id;?>' onclick='isChecked(this.checked);' /></td> 
		<td>&nbsp;<?php echo $row->name;?></td> <td>&nbsp;<?php echo $row->folder;?></td> <td>&nbsp;<?php echo $row->filename;?></td> 
		<td align="right"><a href="javascript:void(0);" onclick="jax.icall('myblog','myxToggleBotPublish','<?php echo $row->id;?>');return false;">
		<img id="pubImg<?php echo $row->id;?>" src="images/<?php echo $img;?>" hspace="8" width="12" height="12" border="0" alt="" /></a></td> 
		<td>&nbsp;&nbsp;
		<?php 
		if ($i > 0) {
			echo '<a href="javascript:void(0);" onClick="jax.icall(\'myblog\',\'orderBot\',\''.$row->id.'\',\''.$i.'\',\'1\');" title="'.'Order Up'.'"> <img src="images/uparrow.png" width="12" height="12" border="0" alt="'.'Order Up'.'"> </a>';
		} else {
			echo '&nbsp;';} ?> <?php
			if ($i < $total-1 && $i>0) {
				echo '<a href="javascript:void(0);" onClick="jax.icall(\'myblog\',\'orderBot\',\''.$row->id.'\',\''.$i.'\',\'-1\');" title="'.'Order Down'.'"> <img src="images/downarrow.png" width="12" height="12" border="0" alt="'.'Order Down'.'"> </a>';
			} else {
				echo '&nbsp;';
			} ?> </td> </tr> 
	<?php $i++;
	} ?> 
	</table>
	 
		<input type="hidden" name="option" value="com_myblog" />
		<input type="hidden" name="task" value="bots" /> 
		<input type="hidden" name="boxchecked" value="0" /> 
		<input type="hidden" name="limitstart" value="<?php ?>"> <input type="hidden" name="limit" value="<?php ?>"> 
		</form></div></td> </tr> </table> 
		<?php } 

		function addBot(){
			global $mainframe, $option;
	$cms =& cmsInstance("CMSCore"); ?> 
	<p> <form action="index2.php?option=com_myblog&task=saveBot" method="post" name="adminForm"> 
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="adminlist"> <th colspan="3">Add New MyBlog Bot</th> 
	<tr> <td><strong>Bot Name</strong></td> <td><input type="text" id="botName" name="botName" size="40" /></td> <td>&nbsp;</td> </tr> 
	<tr> <td width="23%"><b>Folder</b></td> <td width="25%"><select name="folder" id="folder"> 
	<?php $curr_dir=$cms->get_path("root")."/components/com_myblog/plugins";
	if ($dir_handle=opendir($curr_dir)) {
		while (($file=readdir($dir_handle))!==false) {
			if (is_dir($curr_dir."/".$file) and $file!="." and $file!=".."){
				echo '<option value="'.$file.'">'.$file.'</option>\n';
			}
		}

		closedir($dir_handle);} ?> </select></td> <td width="52%">&nbsp;</td> </tr> <tr> 
		<td><strong>Filename</strong></td> <td><input type="text" id="filename" name="filename" size="30" />.php</td> 
		<td>&nbsp;</td> </tr> <tr> <td><strong>Publish</strong></td> <td>
		<?php echo mosHTML::yesnoRadioList( 'published','class="inputbox"',"0");?></td> <td>&nbsp;</td> </tr> <tr> <td colspan="3">&nbsp;</td> </tr> </table> 
	<input type="hidden" name="option" value="com_myblog">
	<input type="hidden" name="task" value="saveBot"> 
	<input type="hidden" name="boxchecked" value="0"> </form> </p>

<?php } function showAbout(){ ?> 
<table width="100%" border="0" cellspacing="0" cellpadding="4"> 
<tr> 
	<td>
	<p>About MyBlog for Joomla! </p> 
	<p>MyBlog brings the power of Ajax to Joomla CMS.</p> 
	<h3>Blog in style using MyBlog! </h3> 
	<p>Thank you for using My Blog - a WordPress-like blogging tool for Joomla! Developed by the Team at Pocketcraft Software that created JomComment,
	My Blog is designed to be an easy to use, neat, yet fully featured blogging component for Joomla!, replacing existing blogging solutions
	like Mamblog and Joomla!'s own content editor. </p>
	<p>Features include: <blockquote>
	- trackback<br/> 
	- RSS<br/> 
	- custom template system<br/> 
	- simple and easy to use blog editor with Myblog's own custom made image browser<br/> 
	- admin control over blogging<br/> 
	- tags<br/> 
	- Joomla content mambot integration<br/> 
	- blog posts search<br/> 
	- and much more!</blockquote>
	</p> 
	<p>
	Visit us at <a href="http://www.azrul.com">http://www.azrul.com</a> to find out more about other exciting Joomla! components we have to offer.
	</p>
	</td> </tr> </table> <?php 
}

/**
 * Show list of content mambots
 **/	 	
function showMambots(){
	global $mainframe, $_MY_CONFIG;
	
	require_once(MY_LIBRARY_PATH . '/plugins.class.php');
	
	$plugins	= new MYPlugins();
	$cms    	=& cmsInstance('CMSCore');
	$cms->load('helper', 'url');

	$limit		= intval( $mainframe->getUserStateFromRequest( "viewlistlimit",'limit',$mainframe->getCfg('list_limit') ) );
	$limitstart = intval( $mainframe->getUserStateFromRequest( "view{com_myblog}limitstart",'limitstart',0 ) );
	$publish	= cmsGetVar('publish','','GET');

	$plugins->init();
	$rows		= $plugins->get($limitstart, $limit);
	$total		= count($rows);
	
	$pagination	= myPagination($total, $limitstart, $limit);
?>
	<div>
		<span style="color:red;font-weight: bold;">NOTE:</span>
		Content Mambot integration is by default only enabled when you view a blog entry.<br />
		To enable Content Mambot integration on the list of blog entries, make sure 
		<b>'Use mambots on My Blog frontpage?'</b> settings in <b>General Settings->Layout</b> 
		is set to <b>'Yes'</b>.
	</div>
	<br />
	<form action="#" method="POST" name="adminForm">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="adminlist">
		<thead>
		<tr>
			<th width="2%" class="title">
				<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $rows );?>);" />
			</th>
			<th width="35%">&nbsp;Bot Name</th>
			<th width="5%">&nbsp;Published</th>
			<th width="55%">&nbsp;</th>
		</tr>
		</thead>
		<tbody>
<?php
	$i=0;

	foreach($rows as $row){
		$task			= $row->my_published ? 'unpublish' : 'publish';
		$img			= $row->my_published ? 'publish_g.png' : 'publish_x.png';
		$mambotsToKeep	= ',' . $row->mambot_id;
?> 
		<tr id="bot<?php echo $i;?>"> 
			<td>
				<input type='checkbox' id='cb<?php echo $i;?>' name='cid[]' value='<?php echo $row->mambot_id;?>' onclick='isChecked(this.checked);' />
			</td>
			<td>
				&nbsp;<?php echo $row->name;?>
			</td>
			<td align="center">
				<a href="javascript:void(0);" onclick="jax.call('myblog','myxToggleMambotPublish','<?php echo $row->mambot_id;?>');return false;">
					<img id="pubImg<?php echo $row->mambot_id;?>" src="images/<?php echo $img;?>" hspace="8" width="12" height="12" border="0" alt="" />
				</a>
			</td>
			<td>
				&nbsp;
			</td>
		</tr>
<?php 
		$i++;
	}
?>
	</tbody>
	<tr>
	<td colspan="4">
	<input type="hidden" name="option" value="com_myblog" />
	<input type="hidden" name="task" value="contentmambots" />
	<input type="hidden" name="boxchecked" value="0" />
	<br />
	<div style="width:100%;">
<?php echo $pagination->footer;?>
	</div>
	</td>
	</tr>
	</tfoot>
	</table>
	</form>
<?php 
		//$db->query("DELETE FROM #__myblog_mambots WHERE mambot_id NOT IN ($mambotsToKeep)");
}
?>

<?php

function showLicense() {
  ?>
      <table cellpadding="4" cellspacing="0" border="0" width="100%">
      <tr>
        <td>
        
  <H3>SOFTWARE LICENSE AND LIMITED WARRANTY </H3>
  <p>This is a legally binding agreement between you and <em>Pocketcraft Software</em>. By   installing and/or using this software, you are agreeing to become bound by the   terms of this agreement.</p>
  <p>If you do not agree to the terms of this agreement, do not use this software.   </p>
  <p><strong>GRANT OF LICENSE</strong>. <em>Pocketcraft Software</em> grants to you a non-exclusive   right to use this software program (hereinafter the "Software") in accordance   with the terms contained in this Agreement. You may use the Software on a single   computer. If you have purchased a site license, you may use the Software on the   number of websites defined by and in accordance with the site license.</p>
  <p><strong>UPGRADES</strong>. If you acquired this software as an upgrade of a previous   version, this Agreement replaces and supercedes any prior Agreements. You may   not continue to use any prior versions of the Software, and nor may you   distribute prior versions to other parties.</p>
  <p><strong>OWNERSHIP OF SOFTWARE</strong>. <em>Pocketcraft Software</em> retains the copyright, title,   and ownership of the Software and the written materials.</p>
  <p><strong>COPIES</strong>. You may make as many copies of the software as you wish, as   long as you guarantee that the software can only be used on one website (joomla installation) in any   one instance. You may not distribute copies of the Software or accompanying   written materials to others.</p>
  <p><strong>TRANSFERS</strong>. You may not transfer the Software to another person provided   that you have a written permission from <em>Pocketcraft Software</em> . You may not   transfer the Software from one website to another.  In no event may you transfer, assign, rent, lease, sell, or   otherwise dispose of the Software on a temporary basis.</p>
  <p><strong>TERMINATION</strong>. This Agreement is effective until terminated. This   Agreement will terminate automatically without notice from <em>Pocketcraft Software</em> if   you fail to comply with any provision of this Agreement. Upon termination you   shall destroy the written materials and all copies of the Software, including   modified copies, if any.</p>
  <p><strong>DISCLAIMER OF WARRANTY</strong>. <em>Pocketcraft Software</em> disclaims all other   warranties, express or implied, including, but not limited to, any implied   warranties of merchantability, fitness for a particular purpose and   noninfringement.</p>
  <p><strong>OTHER WARRANTIES EXCLUDED</strong>. <em>Pocketcraft Software</em> shall not be liable for   any direct, indirect, consequential, exemplary, punitive or incidental damages   arising from any cause even if <em>Pocketcraft Software</em> has been advised of the   possibility of such damages. Certain jurisdictions do not permit the limitation   or exclusion of incidental damages, so this limitation may not apply to you.</p>
  <p>In no event will <em>Pocketcraft Software</em> be liable for any amount greater than what   you actually paid for the Software. Should any other warranties be found to   exist, such warranties shall be limited in duration to 15 days following the   date you install the Software.</p>
  <p><strong>EXPORT LAWS</strong>. You agree that you will not export the Software or   documentation.</p>
  <p><strong>PROPERTY</strong>. This software, including its code, documentation,   appearance, structure, and organization is an exclusive product of the <em>Pocketcraft Software</em>, which retains the property rights to the software, its   copies, modifications, or merged parts.</p>
  <p>&nbsp;</p>

        </td>
      </tr>
      </table>
  <?php
}
