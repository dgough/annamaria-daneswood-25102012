<?php
/**
 * Some function, that should be accessible from the installer as well as the
 * main admin page 
 */ 

function myCheckOldJomComment(){
	$cms    =& cmsInstance('CMSCore');
	
	if(file_exists($cms->get_path('root') . '/components/com_jomcomment/cms/spframework.php'))
	    return true;

	return false;
}

/**
 * If there is a component that previosuly link to MyBlog, we need to update 
 * its component id  
 */ 
function myFixMenuLinks(){
	$cms    =& cmsInstance('CMSCore');
	$db		=& cmsInstance('CMSDb');
	
	$comid = $db->get_value("SELECT `id` FROM #__components WHERE `name`='My Blog'");
	$query = "UPDATE #__menu SET `componentid`='$comid' WHERE `link` LIKE 'index.php?option=com_myblog%'";
	$db->query($query);
}

/**
 * Fix link to the dashboard in the user menu.
 */		 		
function myFixDashboardLinks(){
	$cms    =& cmsInstance('CMSCore');
	$db		=& cmsInstance('CMSDb');
	
	$comid = $db->get_value("SELECT `id` FROM #__components WHERE `name`='My Blog'");
	
	// Make sure it is there
	$linkExist = $db->get_value("SELECT count(*) FROM #__menu 
		WHERE 
			`menutype`='usermenu' 
			AND `link` LIKE '%index.php?option=com_myblog%' 
			AND `type`='component' 
			AND `componentid`='$comid' ");
	
	// If no correct one is found, delete all myblog links in usermenu
	// and create a new one
	if(!$linkExist){
		$query = "DELETE FROM #__menu 
			WHERE
				`menutype`='usermenu' 
				AND `link` LIKE '%index.php?option=com_myblog%'";
		$db->query($query);

		$type       = 'components';
		$menuAlias	='';

		if(cmsVersion() == _CMS_JOOMLA15){
			// Only Joomla 1.5 has menu alias
			$menuAlias	= " `alias`='myblog-admin', ";
			$type       = "component";
		}

		$query = "INSERT INTO `#__menu`
			SET 
				`name`= 'My Blog Dashboard',
				{$menuAlias}
				`menutype`='usermenu',
				`link`='index.php?option=com_myblog&task=adminhome',
				`type`='{$type}',
				`access`='1',
				`published`='1',
				`componentid`='$comid'";
		
		
		$db->query($query);
	}
}

/** 
 * Fix myblog related tables 
 */ 
function myFixDBTables(){
	$db		=& cmsInstance('CMSDb');
	
	// Make sure the tags are unique
	$db->query( 'SHOW FIELDS FROM `#__myblog_categories`');
	$fields = $db->get_object_list();

	// Add missing fields in the table
	if(empty($fields[1]->Key)){
		$db->query("ALTER TABLE `#__myblog_categories` ADD UNIQUE (`name`)");
	}
}

/**
 * Extract the bundled file into
 */ 
function myExtractFiles(){
	// unpack all front-end files
    echo '<strong>Installing MyBlog components</strong><br/>';
    echo '<img src="images/tick.png"> Installing front-end components<br/>';
    myExtractArchive(SITE_ROOT_PATH . "/administrator/components/com_myblog/com.zip", 
		SITE_ROOT_PATH . "/components/com_myblog/");

	// Unpack backend files
    echo '<img src="images/tick.png"> Installing back-end components<br/><br/>';
    myExtractArchive(SITE_ROOT_PATH . "/components/com_myblog/admin.zip", 
		SITE_ROOT_PATH . "/administrator/components/com_myblog/");
}

/**
 * Extract zip files
 */ 
function myExtractArchive($src, $destDir){
	if(class_exists('JFactory') && defined('_JEXEC')){
		
		$destDir =  JPath::clean($destDir);
		$src 	=  JPath::clean($src);
		
		JArchive::extract($src, $destDir);
	} else {
		if(!class_exists('PclZip')){
		    #BC compatibilities.
	        include_once(SITE_ROOT_PATH. "/administrator/includes/pcl/pclzip.lib.php");
		}
		
		$archive = new PclZip($src);
		$list = $archive->extract(PCLZIP_OPT_PATH, $destDir);
	}
	
	return true;
}

function myMkdir($destDir){
	if(class_exists('JFactory') && defined('_JEXEC')){
		JFolder::create($destDir);
	} else {
		mkdir($destDir);
	}
	
	return true;
}

function myRmdir($destDir){
	if(class_exists('JFactory') && defined('_JEXEC')){
		$destDir =  JPath::clean($destDir);
		JFolder::delete($destDir);
	} else {
		@unlink($destDir);
	}
	
	return true;
}

function myDeleteDir($src){
	if(class_exists('JFactory') && defined('_JEXEC'))
		JFolder::delete($src);
	else
		deleteDir($src);
		
}

function myUnlink($src){
	if(class_exists('JFactory') && defined('_JEXEC'))
		JFile::delete($src);
	else
		@unlink($src);
}

