<?php

/**
* @copyright (C) 2006 by Azrul Rahim - All rights reserved!
* @license http://www.azrul.com Copyrighted Commercial Software
**/
# Don't allow direct linking
(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');
define("SITE_ROOT_PATH", dirname(dirname(dirname(dirname(__FILE__)))) );

function com_install() {
	global $mainframe, $_VERSION, $option, $my;
	ob_start();
	
	// Include the admin functions
	require_once (SITE_ROOT_PATH. "/administrator/components/com_myblog/functions.admin.php");
	installCmsLib();
	
	// Check if older Jom COmment is installed.
	if(myCheckOldJomComment()){
		echo '<h3 style="color: red;">Installation FAILS! Older version of Jom Comment detected!</h3>';
		echo "<p>Please update your version of Jom Comment before proceeding with the installation of My Blog!</p>";
		return;
	}

	
	if(!function_exists('cmsInstance')){
		echo "<p>Installation FAILS!. You need to make sure the Joomla root directory is writeable.</p>";
		echo "<p>We need the root folder to be writeable to allow us to install our custom libraries.</p>";
		return;
	}
	$cms    =& cmsInstance('CMSCore');
	$db		=& cmsInstance('CMSDb');
	$db->query("UPDATE #__components SET admin_menu_img='../administrator/components/com_myblog/images/myblog-icon.png' WHERE admin_menu_link='option=com_myblog'");

	
	
	// Load installation script
	include_once($cms->get_path('root') . '/administrator/components/com_myblog/install.sql.php');
	
	myFixDashboardLinks();
	myFixMenuLinks();
	myFixDBTables();
	
	// unpack all zipped files
	myExtractFiles();

?>

<p><img src="<?php $cms->get_path('live')?>/components/com_myblog/images/pageicon.png" alt="logo" /></p>
<p><strong>My Blog</strong><br/>
<code>
<br/>
<style>
.CommonTextButtonBig, .CommonTextButtonSmall {
background-color:#EEEEEE;
border-color:#CCCCCC rgb(153, 153, 153) rgb(153, 153, 153) rgb(204, 204, 204);
border-style:solid;
border-width:1px;
color:#333333;
display:-moz-inline-box;
font-family:Tahoma,Arial,Helvetica;
font-size:120%;
margin:1px;
padding:10px;
vertical-align:middle;
white-space:nowrap;
}
.CommonTextButtonSmall {
font-size:100%;
padding:2px 3px 3px 2px;
text-align:center;
}
input.CommonTextButtonBig {
color:#000000;
font-size:150%;
font-weight:bold;
text-align:left;
}
input.CommonTextButtonSmall {
text-align:left;
}
a.CommonTextButtonSmall {
cursor:pointer;
}
input.CommonTextButtonSmall:hover, input.CommonTextButtonSmall:active, input.CommonTextButtonSmall:focus, input.CommonTextButtonBig:hover, input.CommonTextButtonBig:active, input.CommonTextButtonBig:focus {
background-color:#FFFFFF;
}
.CommonTextButtonBig:hover, .CommonTextButtonSmall:hover {
background-color:#DDDDDD;
border-color:#CCCCCC rgb(153, 153, 153) rgb(153, 153, 153) rgb(204, 204, 204);
border-style:solid;
border-width:1px;
color:#333333;
text-decoration:none;
}

</style>
<?php


    echo '<img src="images/tick.png"> Creating database tables<br/><br/>';
    
    if(!class_exists('PclZip'))
		require_once($cms->get_path("root") . "/administrator/includes/pcl/pclzip.lib.php");

    $db->query("SELECT id from #__sections WHERE title='MyBlog'");
	$sectionid = $db->get_value();
	if ($sectionid){
		echo '<img src="images/tick.png"> Previous MyBlog section found. Republishing section.<br/><br/>';
		$db->query("UPDATE #__sections SET published = 1 WHERE id = $sectionid");
	} else {
		echo '<img src="images/tick.png"> Creating MyBlog section. <br/><br/>';
		$db->query("INSERT INTO #__sections SET published = 1, title='MyBlog', name='MyBlog', scope='content', count=1, ordering=1");
		$sectionid = $db->get_insert_id();
	}

	// Upgrade for feedburner
	$fields = $cms->db->getFields('#__myblog_user');
	
	if(!empty($fields)){
        if(!array_key_exists("feedburner", $fields)){
			// Add new 'feedburner' column
			$strSQL = "ALTER TABLE `#__myblog_user` ADD `feedburner` TEXT NOT NULL";
			$db->query($strSQL);
        }

        if(!array_key_exists("style", $fields)){
			// Add new 'feedburner' column
			$strSQL = "ALTER TABLE `#__myblog_user` ADD `style` TEXT NOT NULL";
			$db->query($strSQL);
        }
        
		if(!array_key_exists("title", $fields)){
			// Add new 'feedburner' column
			$strSQL = "ALTER TABLE `#__myblog_user` ADD `title` TEXT NOT NULL";
			$db->query($strSQL);
        }
	}

	// Upgrade myblog_categories table structure.
	$fields	= $cms->db->getFields('#__myblog_categories');
	
	if(!empty($fields)){
		if(!array_key_exists('default', $fields)){
			$strSQL	= 'ALTER TABLE `#__myblog_categories` ADD `default` TINYINT NOT NULL';
			$db->query($strSQL);
			
			$strSQL	= 'ALTER TABLE `#__myblog_categories` ADD INDEX ( `default` )';
			$db->query($strSQL);			
		}

		if(!array_key_exists('slug', $fields)){
			$strSQL	= 'ALTER TABLE `#__myblog_categories` ADD `slug` VARCHAR(255) NOT NULL';
			$db->query($strSQL);
		}
	}
	//ALTER TABLE `jos_myblog_categories` ADD `slug` VARCHAR( 255 ) NOT NULL AFTER `default` ;

    $db->query("SELECT id from #__categories WHERE title='MyBlog'");
	$catid = $db->get_value();
	if ($catid){
		$db->query("UPDATE #__categories SET published = 1 WHERE id = $catid");
	} else{
		$db->query("INSERT INTO #__categories SET published = 1, title='MyBlog', name='MyBlog', section=$sectionid, ordering=1");
		$catid = $db->get_insert_id();
	}
	
	

	// All Files has been unpacked, call CONFIG file once to initialise the default config
	include_once($cms->get_path("root") . "/administrator/components/com_myblog/config.myblog.php");
	$cfgObject = new MYBLOG_Config();

	echo '<strong>Creating default menu items</strong><br/>';
	echo '<img src="images/tick.png"> Creating User Menu item - MyBlog Dashboard<br/><br/>';
	echo '<strong>Installing System Mambots</strong><br/>';

    # check if azrul system mambot is installed (for ajax stuff)
	$src  = $cms->get_path("root") . "/components/com_myblog/azrul.zip";
    sysBotUpgrade($src);

	# Check if azvideo bot is installed
	$src    = $cms->get_path('root') . '/components/com_myblog/azvideobot.zip';
	installAzvideoBot($src);

	//echo '<img src="images/tick.png">Installing Social Bookmarking Bot for MyBlog<br/><br />';
	//$db->query("SELECT count(*) from #__myblog_bots WHERE filename='social_bookmarks' and folder='social_bookmark'");
	//if (!$db->get_value())
	//{
	//	$db->query("INSERT into #__myblog_bots SET name='Social Bookmarking', published='1', ordering='1', filename='social_bookmarks', folder='social_bookmark'");
	//}
	echo '<strong>Checking configuration settings</strong><br/>';

	$db->query("SELECT id FROM #__components WHERE `option`='com_jomcomment'");
	$jomcommentExists = $db->get_value();
	$jc_compat = -1;
	if ($jomcommentExists && file_exists($cms->get_path("root") . "/administrator/components/com_jomcomment/jomcomment.xml")){
		// check jomcomment version
	  	require_once( $cms->get_path("root") . '/includes/domit/xml_domit_lite_include.php' );
		// Read the file to see if it's a valid component XML file
		$xmlDoc = new DOMIT_Lite_Document();
		$xmlDoc->resolveErrors( true );

		if (!$xmlDoc->loadXML( $cms->get_path("root") . "/administrator/components/com_jomcomment/jomcomment.xml", false, true )) {
			//continue;
		}

		$root = &$xmlDoc->documentElement;
		$element 			= &$root->getElementsByPath('version', 1);
		$jc_version 		= $element ? $element->getText() : '';
		$jc_version = explode(" ", $jc_version);
		$jc_version = $jc_version[0];
		$jc_compat = strcmp($jc_version, "1.8");

		if ($root->getTagName() != 'mosinstall') {
			//continue;
		}
		if ($root->getAttribute( "type" ) != "component") {
			//continue;
		}
	}


	echo '<br/>';

	echo 'Installation completed. <br/> Thank you for using MyBlog! <br/>Click the button below to configure MyBlog.<br/><br/>';
	// echo 'Preliminary installation completed. <br/>
	//		Please click the button below to complete this installation.<br/><br/>';

	echo '<a class="CommonTextButtonBig" href="index2.php?option=com_myblog">Continue&nbsp;&nbsp;<img align="absmiddle" border="0" src="components/com_myblog/Forward_16x16.png" /></a>';
?>

<br/>
	</code>
  	<font class="small">&copy; Copyright 2006 by Azrul<br/>
  	This component is copyrighted commercial software. Distribution is prohibited.</font></p>
	<p><a href="http://www.azrul.com">www.azrul.com</a></p><br/><br/><br/><br/>


  <?php
	$content    = ob_get_contents();
	ob_end_clean();
	
	# after the installation, we need to delete the zip file leftover
	$installfiles = array('admin.zip', 'com.zip', 'azrul.zip','cmslib.zip', 'azvideobot.php');
	foreach($installfiles as $f){
		//if (file_exists($cms->get_path("root") . "/components/com_myblog/$f"))
		//		myUnlink(file_exists($cms->get_path("root") . "/components/com_myblog/$f"));
	}
	
	return $content;
	
}

/**
 * Create 2 menu items.
 *  1. in "main menu", called "Blog"
 *  2. in "user menu", called "My Blog Entries"
 *
 * If the menu already exist, do not re-create them, Just re-publish them
 */
function sysBotGetVersion(){

	$cms    =& cmsInstance('CMSCore');
	require_once( $cms->get_path("root") . '/includes/domit/xml_domit_lite_include.php' );

	$version = 0;

	$filename = ($cms->get_path("plugins") . '/system/azrul.system.xml');
	if(file_exists($filename)){
		// Read the file to see if it's a valid component XML file
		$xmlDoc = new DOMIT_Lite_Document();
		$xmlDoc->resolveErrors( true );

		if (!$xmlDoc->loadXML( $filename, false, true )) {
			//continue;
		}

		$root = &$xmlDoc->documentElement;

		if ($root->getTagName() != 'mosinstall') {
			//continue;
		}

		$element   = &$root->getElementsByPath('version', 1);
		$version   = $element ? $element->getText() : '';
	}

	return doubleval($version);
}

function sysBotUpgrade($src){
    $cms    =& cmsInstance('CMSCore');
    $installIt = false;

	$botTable   = (cmsVersion() == _CMS_JOOMLA15) ? 'plugins' : 'mambots';
	$botVersion = sysBotGetVersion();
    if($botVersion != 0 && $botVersion < 2.5){
    	require_once($cms->get_path('root') . "/administrator/includes/pcl/pclzip.lib.php");

    	@deleteDir($cms->get_path('plugin') . "/system/pc_includes");
		@unlink($cms->get_path('plugin') . "/system/azrul.system.php");
		@unlink($cms->get_path('plugin') . "/system/azrul.system.xml");
		$strSQL = "DELETE FROM #__{$botTable} WHERE element='azrul.system'";
	    $cms->db->query($strSQL);

	    $installIt = true;
	} else if($botVersion == 0){
		// No system bot detected, install it
		$installIt = true;
	}

	
	if($installIt){
		echo '<img src="images/tick.png"> Installing Azrul.com System mambots <br/>';		
		$list = myExtractArchive($src,$cms->get_path('plugins') . "/system/");
	    
	    $strSQL = "DELETE FROM #__{$botTable} WHERE element='azrul.system' OR `name`='Azrul.com System Mambot'";
	    $cms->db->query($strSQL);
	    
	    $strSQL = "INSERT INTO `#__{$botTable}` SET `name`='Azrul.com System Mambot', "
	            . "`element`='azrul.system', "
	            . "`folder`='system', "
	            . "`access`='0', "
	            . "`ordering`='1', "
	            . "`published`='1'";
		$cms->db->query($strSQL);
	    unset($archive);
	}
}

function deleteDir($dir){
    if (substr($dir, strlen($dir)-1, 1)== '/')
        $dir = substr($dir, strlen($dir)-1);
    //echo $dir;
    if (file_exists($dir) and $handle = opendir($dir)){
        while ($obj = readdir($handle)){
            if ($obj!= '.' && $obj!= '..'){
                if (is_dir($dir.$obj)){
                    if (!deleteDir($dir.$obj))
                        return false;
                }
                elseif (is_file($dir.$obj)){
                    if (!unlink($dir.$obj))
                        return false;
                }
            }
        }
        closedir($handle);
        if (!@rmdir($dir))
            return false;
        return true;
    }
    return false;
}

function upgradeCmsLib(){

	if(file_exists(SITE_ROOT_PATH . '/components/libraries'))
		@myRmdir(SITE_ROOT_PATH . '/components/libraries');
	
	// Install it now (from the cmslib.zip file)
	$libpath = SITE_ROOT_PATH . '/components/libraries';

	if (!file_exists($libpath)) myMkdir($libpath);
	if (!file_exists($libpath.'/cmslib')) myMkdir($libpath.'/cmslib');

	# CMS Compatibilities
	# spframework not included yet, cant use cmsVersion()
	$list = myExtractArchive(SITE_ROOT_PATH . '/components/com_myblog/cmslib.zip', $libpath.'/cmslib/');
	
	if($list){
		// Extract success, Include the library
		include_once (SITE_ROOT_PATH . '/components/libraries/cmslib/spframework.php');
	}
}

// Install the CMS Lib if required and load the library
function installCmsLib(){
	global $mainframe;

	// Test if version.php file exists in the cmslib folder. If it doesn't exists, we know that this might be an upgrade.
	if( file_exists( SITE_ROOT_PATH . '/components/libraries/cmslib/version.php') ){
		// This is an upgrade from 1.1 cmslib
		global $cmsVersion;
		include_once( SITE_ROOT_PATH . '/components/libraries/cmslib/version.php');
		
		if($cmsVersion >= 1.1){
			// Don't need to upgrade this since the cmslib version is higher.
			// Just include it.
			include_once( SITE_ROOT_PATH . '/components/libraries/cmslib/spframework.php');
		} else {
			upgradeCmsLib();
		}
	}else{
		// This is older versions before 1.1 cmslib exist
		upgradeCmsLib();
	}
}

/**
 * Install Azrul's videobot
 **/
function installAzvideoBot($zipSrc){
	$cms        =& cmsInstance('CMSCore');
	$exists		= false;
	$botTable   = (cmsVersion() == _CMS_JOOMLA15) ? 'plugins' : 'mambots';
	
	/**
	 * Check if existing video mambot exists
	 **/
	$strSQL = "SELECT * FROM #__{$botTable} WHERE `element`='azvideobot' AND `folder`='content'";
	$cms->db->query($strSQL);
	
	if($cms->db->get_value()){
		$exists = true;

		// Remove old video mambots
		if (file_exists($cms->get_path("plugins") . "/content/azvideobot.php"))
			myUnlink($cms->get_path("plugins") . "/content/azvideobot.php");
		if (file_exists($cms->get_path("plugins") . "/content/azvideobot.xml"))
			myUnlink($cms->get_path("plugins") . "/content/azvideobot.xml");
	}


    echo '<img src="images/tick.png"> Installing Azrul Videobot<br />';
    myExtractArchive($zipSrc ,$cms->get_path('plugins') . '/content/');
    if(!$exists){
		$query  	= "INSERT INTO #__{$botTable} SET `name`='Azrul Video Mambot', "
		        	. "`element`='azvideobot', `folder`='content', "
		        	. "`access`='0', `ordering`='0', `published`='1', `params`='height=350px
					  width=425px'";
		$cms->db->query($query);
	}
	unset($archive);

}/* End installAzvideoBot */

function showInstallWizard(){
	global $mainframe;
	$cms	=& cmsInstance('CMSCore');
	$db =& cmsInstance('CMSDb');
	?>
	<link href="<?php echo $cms->get_path('live');?>/components/com_myblog/css/admin_style.css" rel="stylesheet" type="text/css">
<table width="100%" border="0" cellspacing="4" cellpadding="4">
    <tr>
        <td><table width="100%" class="mytable" border="0" cellspacing="2" cellpadding="2">
                <tr>
                    <th>Step 1 : Create a link to the blog-writig page in the user's menu </th>
                </tr>
                <tr>
                    <td><blockquote>You need to create a link to your new blog component in one of your menu. </blockquote></td>
                </tr>
                <tr>
                    <td><blockquote>
                            <form name="form1">
                                <label for="select"></label>
                                <select name="menutype" id="menutype">
                                <?php
									$db->query("SELECT distinct(menutype) from #__menu");
									$menutypes = $db->get_object_list();
									foreach($menutypes as $menu)
									{
										echo "<option value=\"$menu->menutype\">$menu->menutype</option>";
									}
								?>
                                </select>

                                <label for="Submit"></label>
                                <input onclick = "var w;w=window.open('index2.php?option=com_menus&menutype=mainmenu&task=edit&type=url&link=hello', 'menu1');

												el.value='MyBlog Entries';
												el=w.document.getElementByName('link');
												el.setAttribute('value', 'index.php?option=com_myblog&task=startadmin');" type="button" name="button" value="Create the menu now." id="button1">
                            </form>
                        </blockquote></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>
            <table width="100%"  class="mytable"  border="0" cellspacing="2" cellpadding="2">
                <tr>
                    <th valign="middle">Step 2 : Create a link to the blog-writig page in the user's menu </th>
                </tr>
                <tr>
                    <td><blockquote>You need to create a link to your new blog component in one of your menu. </blockquote></td>
                </tr>
                <tr>
                    <td><blockquote>
                            <form name="form1" method="post" action="" target="_blank">
                                <label for="select"></label>
                                <select name="select" id="select">
                                    <?php
										$db->query("SELECT distinct(menutype) from #__menu");
										$menutypes = $db->get_object_list();
										foreach($menutypes as $menu)
										{
											echo "<option value=\"$menu->menutype\">$menu->menutype</option>";
										}
								?>
                                </select>
                                <label for="Submit"></label>
                                <input type="submit" name="Submit" value="Create the menu now." id="Submit">
                            </form>
                        </blockquote></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>
            <table width="100%" class="mytable"  border="0" cellspacing="2" cellpadding="2">
                <tr>
                    <th>Step 3 : Make sure all the links are published </th>
                </tr>
                <tr>
                    <td><blockquote>You need to create a link to your new blog component in one of your menu. </blockquote></td>
                </tr>
                <tr>
                    <td><blockquote>
                            <form name="form2" method="post" action="">
                                <label for="label"></label>
                                <input type="submit" name="Submit2" value="Publish all the menu items" id="label">
                            </form>
                        </blockquote></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table></td>
    </tr>
</table>
<?php

}
?>
