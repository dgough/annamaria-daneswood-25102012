<?php
(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');
define("SITE_ROOT_PATH", dirname(dirname(dirname(dirname(__FILE__)))) );

include_once (SITE_ROOT_PATH . '/components/libraries/cmslib/spframework.php');

function deleteDir($dir){
    if (substr($dir, strlen($dir)-1, 1)!= '/')
        $dir .= '/';
    
    if ($handle = opendir($dir)){
        while ($obj = readdir($handle)){
            if ($obj!= '.' && $obj!= '..'){
                if (is_dir($dir.$obj)){
                    if (!deleteDir($dir.$obj))
                        return false;
                }
                elseif (is_file($dir.$obj)){
                    if (!unlink($dir.$obj))
                        return false;
                }
            }
        }
        closedir($handle);
        if (!@rmdir($dir))
            return false;
        return true;
    }
    return false;
} 

function myDeleteDir($src){
	if(class_exists('JFactory') && defined('_JEXEC'))
		JFolder::delete($src);
	else
		deleteDir($src);
		
}

function myUnlink($src){
	if(class_exists('JFactory') && defined('_JEXEC'))
		JFile::delete($src);
	else
		@unlink($src);
}


function com_uninstall() {
	
    global $database, $mainframe;
	global $_VERSION;
	$cms    =&cmsInstance('CMSCore');
	# Empty the component folder
	myDeleteDir($cms->get_path("root") . "/components/com_myblog");

	# Empty admin folder
	# Won't work. Cannot delete this folder as this file is IN this folder	
	
	//deleteDir($cms->get_path("root") . "/administrator/components/com_myblog");
	
	# unpublish all My Blog menu Items. DO NOT delete them
	# UPDATE: No need to unpublish anything. This isn't a normal behavious for most
	# component. This annoys normal user	
		
	/*	
	$database->setQuery("UPDATE #__menu SET published='0' WHERE menutype='usermenu' and 
						link='index2.php?option=com_myblog&no_html=1&admin=1&task=adminhome'");
	$database->query();
	$database->setQuery("UPDATE #__menu SET published='0' WHERE menutype='mainmenu' and 
							link='index.php?option=com_myblog'");
	$database->query();
	$database->setQuery("UPDATE #__sections SET published='0' WHERE title='MyBlog'");
	$database->query();
	*/
	
	return true;    
}
?>
