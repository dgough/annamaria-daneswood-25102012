<?php
class menuMyBlog {
	function CONFIG_MENU() {
		JToolBarHelper::title( JText::_( 'MyBlog Configuration' ), 'config.png' );
		JToolBarHelper::save('savesettings');
		JToolBarHelper::back();
		JToolBarHelper::spacer();
	  
	}

	function FILE_MENU() {
		JToolBarHelper::save();
		JToolBarHelper::cancel();
		JToolBarHelper::spacer();
	}

	function ABOUT_MENU() {
		JToolBarHelper::title( JText::_( 'About MyBlog' ), 'systeminfo.png' );
		JToolBarHelper::back();
		JToolBarHelper::spacer();
	}
	
	function TAGS_MENU(){
		JToolBarHelper::title( JText::_( 'Tags Manager' ), 'categories.png' );
		JToolBarHelper::back();
		JToolBarHelper::spacer();
	}
	 
	function STATS_MENU() {
		JToolBarHelper::title( JText::_( 'Statistics' ), 'generic.png' );
		JToolBarHelper::back();
		JToolBarHelper::spacer();
	}
	
	function LICENSE_MENU() {
		JToolBarHelper::title( JText::_( 'License Information' ), 'systeminfo.png' );
		JToolBarHelper::back();
		JToolBarHelper::spacer();
	}
	
	function MAINTD_MENU() {
		JToolBarHelper::title( JText::_( 'Maintenance' ), 'systeminfo.png' );
		JToolBarHelper::back();
		JToolBarHelper::spacer();
	}
	
	function UPDATES_MENU(){
		JToolBarHelper::title( JText::_( 'Latest updates' ), 'systeminfo.png' );
		JToolBarHelper::back();
		JToolBarHelper::spacer();
	}
	
	function DASHBOARD_MENU() {
		JToolBarHelper::title( JText::_( 'Write blog entry' ), 'addedit.png' );
		JToolBarHelper::back();
		JToolBarHelper::spacer();
	}
	 
	 
	function MENU_Default() {
		JToolBarHelper::title( JText::_( 'MyBlog' ), 'addedit.png' );
		JToolBarHelper::publish();
		JToolBarHelper::unpublish();
		JToolBarHelper::editList();
		JToolBarHelper::deleteList();
	}
	 
	function ENTRIES_MENU() {
		JToolBarHelper::title( JText::_( 'Blog Entries' ), 'addedit.png' );
		JToolBarHelper::publish();
		JToolBarHelper::unpublish();
		JToolBarHelper::deleteList();
	}
	 
	function BOTS_MENU() {
		JToolBarHelper::title( JText::_( 'MyBlog Plugins' ), 'generic.png' );
		JToolBarHelper::publish();
		JToolBarHelper::unpublish();
		JToolBarHelper::addNewX();
	}
	 
	function MAMBOTS_MENU() {
	
		JToolBarHelper::title( JText::_( 'MyBlog Plugins' ), 'plugin.png' );
		JToolBarHelper::publish('publishMambots');
		JToolBarHelper::unpublish('unpublishMambots');
	}
	 
	function ADDBOT_MENU(){
		mosMenuBar::startTable();
		mosMenuBar::save('saveBot');
		mosMenuBar::cancel('bots');
		mosMenuBar::endTable();
	}
	 
	function INSTALL_MENU(){
		JToolBarHelper::save();
		JToolBarHelper::cancel();
	}
}
