<?php

//*******************************************************
//* SEF Service Map Component
//* http://fun.kubera.org
//* (C) Radoslaw Kubera
//* license http://www.gnu.org/copyleft/gpl.html GNU/GPL
//*******************************************************


/** ensure this file is being included by a parent file */
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

if (!($acl->acl_check( 'administration', 'edit', 'users', $my->usertype, 'components', 'all' ) | $acl->acl_check( 'administration', 'edit', 'users', $my->usertype, 'components', 'com_contact' ))) {
	mosRedirect( 'index2.php', _NOT_AUTH );
}



$task = mosGetParam($_REQUEST, 'task', ''); 
global $mosConfig_absolute_path,$database;
$adminPath = $mosConfig_absolute_path . '/administrator';

include_once ($mosConfig_absolute_path."/administrator/components/com_sefservicemap/sefservicemap.util.php");
include_once ( $mosConfig_absolute_path . '/includes/domit/xml_domit_lite_include.php' );

smGetComponentParams();
smSynchronize();
cache_clear_Check();

include_once ($adminPath."/components/com_sefservicemap/admin.sefservicemap.html.php");

if (file_exists($adminPath."/components/com_sefservicemap/language/".$mosConfig_lang.".php")) 
{ 
  include_once ($adminPath."/components/com_sefservicemap/language/".$mosConfig_lang.".php");
} 
else 
{
  include_once ($adminPath."/components/com_sefservicemap/language/english.php");
} 

require_once ($mosConfig_absolute_path.'/components/com_sefservicemap/version.php');

$id = mosGetParam( $_REQUEST, 'id',0);
$menu_id = mosGetParam( $_REQUEST, 'menu_id',0);
$title = mosGetParam( $_REQUEST, 'title',0);
$settings = mosGetParam( $_REQUEST, 'settings','');


foreach ($_REQUEST as $key=>$value)
{
  if (eregi('clear_',$key))
  {
    $task = 'clearcache';
    $id = str_replace('clear_','',$key);
  }
}

if (isset($_REQUEST['remove_all']))
{
   $option = 
   $param = $task;
   $task = 'removecache';
}

switch ($task) 
{
   case "removecache" : remove_cache($param);
   break;

   case "clearcache" : clear_cache($id);
   break;

   case "integrator" : edit_integrator($id,$menu_id,$title,$settings);
   break;

   case "saveint" : saveint($menu_id,$id,$settings);
   break;

   case "config": edit_config();
   break;

   case "config_integrators": edit_config_integrators();
   break;

   case "config_cache": edit_config_cache();
   break;

   case "config_css": edit_config_css();
   break;
 
   case "save" : save_config('config');
   break;

   case "savecache" : save_config('config_cache');
   break;

   case "savecss" : save_css();
   break;
  
   case "menu" : menu();
   break;

   case 'publish': changePubl( $id, 1);
   break;

   case 'unpublish': changePubl( $id, 0);
   break;

   case 'publishmenu': changeMenuPubl( $id, 1);
   break;

   case 'unpublishmenu': changeMenuPubl( $id, 0);
   break;

   case 'changeprocessorstate': ChangeProcessorState($id);
   break;

   case 'downmenu': down($id);
   break;

   case 'upmenu': up ($id);
   break;

   case 'about': about ();
   break;
   default: menu();
  
}


function remove_cache($param)
{
  global $mosConfig_absolute_path;
  $cache_dir = $mosConfig_absolute_path.'/components/com_sefservicemap/cache/';
  DeleteDirectory ($cache_dir);
  @mkdir ($cache_dir);
  mosRedirect("index2.php?option=com_sefservicemap&task=".$param);
}

function clear_cache($id)
{
  global $mosConfig_absolute_path;
  $cache_dir = $mosConfig_absolute_path.'/components/com_sefservicemap/cache/'.$id;
  DeleteDirectory ($cache_dir);
  mosRedirect("index2.php?option=com_sefservicemap&task=menu");
}

function about()
{
  $MyHTML_sefservicemap_admin = new HTML_sefservicemap_admin;
  $MyHTML_sefservicemap_admin->about();
}

function edit_config()
{

  $MyHTML_sefservicemap_admin = new HTML_sefservicemap_admin;
  global $mainframe;
  $params=$mainframe->ParamsArray;
  $MyHTML_sefservicemap_admin->edit_config($params);
}

function edit_config_css()
{
  $MyHTML_sefservicemap_admin = new HTML_sefservicemap_admin;
  global $mainframe;
  $params=$mainframe->ParamsArray;
   
  $MyHTML_sefservicemap_admin->edit_config_css($params);
}

function edit_config_cache()
{
  global $mainframe;
  $params=$mainframe->ParamsArray;
  $cache = 0;

  global $mosConfig_absolute_path;
  $cache_path = $mosConfig_absolute_path.'/components/com_sefservicemap/cache/';
  if (is_dir($cache_path))
  {
    $dir=opendir($cache_path);
    while ($file=readdir($dir))
    {
      if ($file!='.' && $file!='..')
      {
        $cache = 1;
      }
    }  
    closedir($dir);
  }

  $MyHTML_sefservicemap_admin = new HTML_sefservicemap_admin;
  $MyHTML_sefservicemap_admin->edit_config_cache($params,$cache);
}

function edit_config_integrators()
{
  global $mainframe;
  $params=$mainframe->ParamsArray;
  $MyHTML_sefservicemap_admin = new HTML_sefservicemap_admin;
  $MyHTML_sefservicemap_admin->edit_config_integrators($params);
}

function menu()
{
  global $mosConfig_absolute_path;
  $adminPath = $mosConfig_absolute_path . '/administrator';

  global $database;
  $database->setQuery("select *,name as title from #__sef_sm_menus order by ordering");
  $menus = $database->loadObjectList();
  $count=count($menus);

  $cache=0;
  $cache_path = $mosConfig_absolute_path.'/components/com_sefservicemap/cache/';
  if (is_dir($cache_path))
  {
    $dir=opendir($cache_path);
    while ($file=readdir($dir))
    {
      if ($file!='.' && $file!='..')
      {
        $cache = 1;
      }
    }  
    closedir($dir);
  }

  $MyHTML_sefservicemap_admin = new HTML_sefservicemap_admin;
  $MyHTML_sefservicemap_admin->show_all_menus($menus,$count,$cache);
}

function find_menu($parent,$menutype,$path,&$k,$title)
{
  global $mosConfig_absolute_path;
  if ($path!='') $path.='->';
  global $database;
  $query = "SELECT a.*,b.published AS menu_published,b.integrator AS menu_integrator,b.ordering AS menu_ordering, b.params AS menu_params"
  ."\n FROM #__menu AS a LEFT JOIN #__sef_sm_menu as b ON b.menu_id = a.id "
  ."\n WHERE a.menutype='$menutype' and a.published >='0' and a.parent='$parent' order by 'ordering'";
  $database->setQuery($query);
  $submenus = $database->loadObjectList();

  global $mosConfig_dbprefix;
  $plugtable=$mosConfig_dbprefix.'plugins';
  $alltables=$database->getTableList();
  if (!in_array($mosConfig_dbprefix.'plugins',$alltables)) $plugtable = $mosConfig_dbprefix.'mambots';

   foreach ($submenus as $submenu)
  {
    if ($submenu->menu_published==1)
    {
      $img='tick.png';
      $task='unpublish';
      $alt = "Published";
      $ppub = 1;
    }
    else
    {
      $img = 'publish_x.png';
      $task = "publish";
      $alt = "Unpublished";
      $ppub = 1;
    }
    
    if ($submenu->published==0)
    {
       $img= 'disabled.png';
       $ppub = 0;
    }
               
                     
        $ptask='changeprocessorstate';
        switch ($submenu->menu_integrator)
        {
          case 0: $palt = _Disabled;break;
          case 1: $palt = _Global;break;
          case 2: $palt = _Custom;break;
        }
    $processor = smGetParam($submenu->link,'option','');
    if ($processor=='content') $processor='com_content';

    $integrator='';
    $integrator->id =0;
    $integrator->element='';
    $query = "SELECT id,element from ".$plugtable." where folder='com_sefservicemap' and element='".$processor."_bot' and published='1'";
    $database->setQuery($query);
    $database->loadObject($integrator);
   
   if ($integrator->id==0)
    {
      $palt= _None;
      $proc =0;
    }
    else 
    {
      if ($submenu->menu_integrator==0) $proc=3;else $proc=$submenu->menu_integrator;
    }
    
    if ($ppub==0)
    {
      $pimg= 'disabled.png';
      $proc =0;
    }

    $MyHTML_sefservicemap_admin = new HTML_sefservicemap_admin;
    $MyHTML_sefservicemap_admin->show_menu($title,$submenu,$integrator,$k,$ppub,$path,$alt,$palt,$img,$task,$ptask,$proc);
    if ($k==0) $k=1; else $k=0;
    find_menu($submenu->id,$menutype,$path.$submenu->name,$k,$title);
  }
}

function edit_integrator ($id,$menu_id,$title,$settings)
{
    global $mainframe,$database,$mosConfig_absolute_path,$mosConfig_lang;

    global $mosConfig_dbprefix;
    $plugtable=$mosConfig_dbprefix.'plugins';
    $alltables=$database->getTableList();
    if (!in_array($mosConfig_dbprefix.'plugins',$alltables)) $plugtable = $mosConfig_dbprefix.'mambots';

    $row = new mosMambot($database);
    $row->load($id);
    global $database;
    if ($settings=='local') 
    {
      $query = "SELECT params FROM #__sef_sm_menu where menu_id='$menu_id'";
      $database->setQuery($query);
      $pars = $database->loadResult();
      $query = "SELECT name FROM #__menu where id='$menu_id'";
      $database->setQuery($query);
      $name = $database->loadResult();
    }
    else
    {
      $query = "SELECT name,params from ".$plugtable." where id='$id'";
      $database->setQuery($query);
      $database->loadObject($result);
      $title= $result->name;
      $pars = $result->params;
      $name=_Global_Settings;
    }
                
                  if (file_exists($mosConfig_absolute_path. '/mambots/'.$row->folder . '/' . $row->element .'.'.$mosConfig_lang))
                  {
                    $xmlfile = $mosConfig_absolute_path. '/mambots/'.$row->folder . '/' . $row->element .'.'.$mosConfig_lang;
                  }
                  else
                    $xmlfile = $mosConfig_absolute_path. '/plugins/'.$row->folder . '/' . $row->element .'.'.$mosConfig_lang;
                  
                if (!file_exists($xmlfile))  $xmlfile = $mosConfig_absolute_path. '/mambots/'.$row->folder . '/' . $row->element .".xml";
                if (!file_exists($xmlfile))  $xmlfile = $mosConfig_absolute_path. '/plugins/'.$row->folder . '/' . $row->element .".xml";

		if (file_exists( $xmlfile )) {
			$xmlDoc = new DOMIT_Lite_Document();
			$xmlDoc->resolveErrors( true );
			if (!$xmlDoc->loadXML( $xmlfile, false, true )) {
				continue;
			}

			$root = &$xmlDoc->documentElement;

			if ($root->getTagName() != 'mosinstall') {
				continue;
			}
			if ($root->getAttribute( "type" ) != "mambot") {
				continue;
			}

                        $element 			= &$root->getElementsByPath('name', 1);
                        $row->mambotname 	= $element ? $element->getText() : '';
                        if ($settings!='local')  $title =  $row->mambotname;

			$element 			= &$root->getElementsByPath('creationDate', 1);
			$row->creationdate 	= $element ? $element->getText() : '';

			$element 			= &$root->getElementsByPath('author', 1);
			$row->author 		= $element ? $element->getText() : '';

			$element 			= &$root->getElementsByPath('copyright', 1);
			$row->copyright 	= $element ? $element->getText() : '';

			$element 			= &$root->getElementsByPath('authorEmail', 1);
			$row->authorEmail 	= $element ? $element->getText() : '';

			$element 			= &$root->getElementsByPath('authorUrl', 1);
			$row->authorUrl 	= $element ? $element->getText() : '';

			$element 			= &$root->getElementsByPath('version', 1);
			$row->version 		= $element ? $element->getText() : '';
		}


     $params = new mosParameters( $pars, $xmlfile, 'mambot');
  
     $MyHTML_sefservicemap_admin = new HTML_sefservicemap_admin;
     $MyHTML_sefservicemap_admin->edit_integrator ($id,$menu_id,$title,$name,$row,$params,$settings);

}

function up_down($id,$count,$number)
{
  if ($number!=1)
  {
  ?>
  
    <a href='index2.php?option=com_sefservicemap&task=upmenu&id=<?php echo $id?>'><img src='images/uparrow.png' width="12" height="12" border="0" alt="Move Up"></a>
  <?php }?>
   </td>
   <td>
  <?php
  if ($number!=$count)
  {
  ?>
    <a href='index2.php?option=com_sefservicemap&task=downmenu&id=<?php echo $id?>'><img src='images/downarrow.png' width="12" height="12" border="0" alt="Move Up"></a>
  <?php }
}

function saveint($menu_id,$id,$settings)
{
  $params = mosGetParam( $_POST, 'params', '' );
	if (is_array( $params )) {
		$txt = array();
		foreach ( $params as $k=>$v) {
			$txt[] = "$k=$v";
		}
		$row = implode( "\n", $txt );
	}
  global $database;

  global $mosConfig_dbprefix;
  $plugtable=$mosConfig_dbprefix.'plugins';
  $alltables=$database->getTableList();
  if (!in_array($mosConfig_dbprefix.'plugins',$alltables)) $plugtable = $mosConfig_dbprefix.'mambots';

  if ($settings=='local')
  {
    $query = "UPDATE #__sef_sm_menu SET params='$row' where menu_id='$menu_id'";
  }
  else
  {
    $query = "UPDATE ".$plugtable." SET params='$row' where id='$id'";
  }
  $database->setQuery($query);
  $database->query();
  if ($menu_id) mosRedirect( "index2.php?option=com_sefservicemap&task=menu" );
  else  mosRedirect("index2.php?option=com_sefservicemap&task=config_integrators");
}

function down($id)
{
  global $database;

  $database->setQuery("SELECT id,ordering FROM #__sef_sm_menus WHERE id='$id'");
  $database->loadObject($act);

  $database->setQuery("SELECT id,ordering FROM #__sef_sm_menus WHERE ordering>$act->ordering order by ordering");
  $database->loadObject($next);
  
  $database->setQuery("UPDATE #__sef_sm_menus set ordering='$next->ordering' where id='$act->id'");
  $database->query();

  $database->setQuery("UPDATE #__sef_sm_menus set ordering='$act->ordering' where id='$next->id'");
  $database->query();

  mosRedirect( "index2.php?option=com_sefservicemap&task=menu" );
}

function up($id)
{
   global $database;

  $database->setQuery("SELECT id,ordering FROM #__sef_sm_menus WHERE id='$id'");
  $database->loadObject($act);

  $database->setQuery("SELECT id,ordering FROM #__sef_sm_menus WHERE ordering<$act->ordering order by ordering desc");
  $database->loadObject($next);
  
  $database->setQuery("UPDATE #__sef_sm_menus set ordering='$next->ordering' where id='$act->id'");
  $database->query();

  $database->setQuery("UPDATE #__sef_sm_menus set ordering='$act->ordering' where id='$next->id'");
  $database->query();

  mosRedirect( "index2.php?option=com_sefservicemap&task=menu" );

}


function changePubl( $id=null, $state=0) {
	global $database, $my;

	$query = "UPDATE #__sef_sm_menu"
	. "\n SET published = " . intval( $state )
	. "\n WHERE menu_id='$id'";

	$database->setQuery( $query );
	if (!$database->query()) {
		echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
		exit();
	}

	mosRedirect( "index2.php?option=com_sefservicemap&task=menu" );
}

function changeMenuPubl( $id=null, $state=0) {
	global $database, $my;

	$query = "UPDATE #__sef_sm_menus"
	. "\n SET published = " . intval( $state )
	. "\n WHERE id='$id'";

	$database->setQuery( $query );
	if (!$database->query()) {
		echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
		exit();
	}

	mosRedirect( "index2.php?option=com_sefservicemap&task=menu" );
}


function changeProcessorState($id=null)
{
	global $database;
        
	$query = "select integrator from #__sef_sm_menu"
	. "\n WHERE menu_id='$id'";
        $database->setQuery($query);
        $now = $database->loadResult();
       
        switch ($now)
        {
          case 0: $state=1;break; //use global settings
          case 1: $state=2;break; //use custom settings
          case 2: $state=0; break; //integrator disabled
        }

	$query = "UPDATE #__sef_sm_menu"
	. "\n SET integrator = " . intval( $state )
	. "\n WHERE menu_id='$id'";

	$database->setQuery( $query );
	if (!$database->query()) {
		echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
		exit();
	}

	mosRedirect( "index2.php?option=com_sefservicemap&task=menu" );

}

function save_config($from)
{ 
  $params = mosGetParam( $_POST, 'params', '' );
        $i=1;
        $config = array();
	if (is_array( $params )) {
		$txt = array();
		foreach ( $params as $k=>$v) {
                        $config[$i]->variable=$k;
                        $config[$i]->value=$v; 
                        $i++;
               
		}
	}
  global $database;
  for ($j=1; $j<$i;$j++)
  {
    $query = ( "select count(variable) from #__sef_sm_settings where variable='".$config[$j]->variable."'");
    $database->setQuery($query);
    $cnt = $database->loadResult();
    if ($cnt==0)
    {
      $query = ( "insert into #__sef_sm_settings set variable='".$config[$j]->variable."'");
      $database->setQuery($query);
      $database->query();
    }
    $query = ( "update #__sef_sm_settings set value='".$config[$j]->value."' where variable='".$config[$j]->variable."'");
    $database->setQuery($query);
    $database->query();
  }

  mosRedirect( "index2.php?option=com_sefservicemap&task=".$from);
}

function save_css()
{
  $css= mosGetParam($_REQUEST,'css','');
  global $mosConfig_absolute_path;
  $file = $mosConfig_absolute_path.'/components/com_sefservicemap/css/template_css.css';
  $config = @fopen($file,'wb');
  @fwrite($config,$css);
  @fclose($config); 
  mosRedirect("index2.php?option=com_sefservicemap&task=config_css");
}
?>