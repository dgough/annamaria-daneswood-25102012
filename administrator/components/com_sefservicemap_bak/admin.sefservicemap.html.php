<?php
//*******************************************************
//* SEF Service Map Component
//* http://fun.kubera.org
//* (C) Radoslaw Kubera
//* license http://www.gnu.org/copyleft/gpl.html GNU/GPL
//*******************************************************


/** ensure this file is being included by a parent file */
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

global $mosConfig_absolute_path;

class HTML_sefservicemap_admin 
{
  var $component_params;

  function HTML_sefservicemap_admin()
  {
    global $mainframe;
    $this->component_params=$mainframe->ParamsArray;
  }

  function about()
  {
    ?>
    <form action="index2.php" method="post" name="adminForm">
    <table class="adminheading">
      <tr>
        <th>
  	   SEF Service Map - <?php echo _SEF_Help ?>
        </th>
      </tr>
    </table>
    <table>
      <tr>
        <td>
          <?php
            global $mosConfig_absolute_path,$mosConfig_lang;
            $adminPath = $mosConfig_absolute_path . '/administrator';

            if (file_exists($adminPath."/components/com_sefservicemap/language/".$mosConfig_lang.".help.php")) 
            { 
              $f = file ($adminPath."/components/com_sefservicemap/language/".$mosConfig_lang.".help.php");
            } 
            else 
            {
                 $f = file ($adminPath."/components/com_sefservicemap/language/english.help.php");
            } 
            $txt = implode ("<br/>",$f); 
            echo $txt;
          ?>
        </td>
      </tr>
    </table>
    <input type="hidden" name="task" value="">
    <input type="hidden" name="option" value="com_sefservicemap" />
    </form>
    <?php
  }

  function show_menu($title,$submenu,$integrator,$k,$ppub,$path,$alt,$palt,$img,$task,$ptask,$proc)
  {
     global $mosConfig_absolute_path;
     ?>
     <tr class="<?php echo "row$k"; ?>">
      <td>
        <?php echo $submenu->id; ?>
      </td>
      <td>
        <?php if ($proc==0 || $proc==3 || $proc==1) if ($ppub==1) echo $path.$submenu->name; else echo '<i>'.$path.$submenu->name.'</i>';?>
        <?php if ($proc==2) {?><a href='index2.php?option=com_sefservicemap&task=integrator&settings=local&id=<?php echo $integrator->id?>&menu_id=<?php echo $submenu->id?>&title=<?php echo $title?>'><?php if ($ppub==1) echo $path.$submenu->name; else echo '<i>'.$path.$submenu->name.'</i>';?></a><?php }?>
      </td>
      <td>
      </td>
      <td>
      </td>
      <td align="center">
        <?php if ($ppub==1) {?><a href='index2.php?option=com_sefservicemap&task=<?php echo $task?>&id=<?php echo $submenu->id?>'><img src='images/<?php echo $img ?>' hspace='3' border='0' alt="<?php echo $alt ?>"></a><?php }?>
        <?php if ($ppub==0) {echo '<img src="images/'.$img.'" />';}?>
      </td>                       
      <td align="center">
        <?php if ($proc!=0) {?><a href='index2.php?option=com_sefservicemap&task=<?php echo $ptask?>&id=<?php echo $submenu->id?>'><?php echo $palt ?></a><?php }?>
        <?php if ($proc==0) {echo $palt;}?>
      </td>
      <td>
        <?php echo $submenu->link; ?>
      </td>
      <td align="center">
       <?php 
       $enabled = $this->component_params->get($integrator->element);
       if ($enabled)
       { global $lang;
         $cache_dir = $mosConfig_absolute_path.'/components/com_sefservicemap/cache/'.$submenu->id;
         if (is_dir($cache_dir))
         {
           echo '<input type="submit" '.$enabled.' class="button" name="clear_'.$submenu->id.'" value ="'._SEF_CLEAR_CACHE.'">';
         }
       } 
       ?>
      </td>
    </tr>
    <?php
  }
  

  function edit_config ($params)
  { 
    ?>
  <script language="javascript" type="text/javascript">
	    function submitbutton(pressbutton) {
	      var form = document.adminForm;
	      if (pressbutton == 'cancel') {
	        submitform( pressbutton );
	        return;
	      } else {
	        submitform( pressbutton );
	      }
	    }
	    </script>
    <?php
    if(is_callable('mosCommonHTML',"loadOverlib")) mosCommonHTML::loadOverlib();

    global $mosConfig_lang,$mosConfig_absolute_path,$database,$mosConfig_dbprefix;

    global $mosConfig_dbprefix;
    $plugtable=$mosConfig_dbprefix.'plugins';
    $alltables=$database->getTableList();
    if (!in_array($mosConfig_dbprefix.'plugins',$alltables)) $plugtable = $mosConfig_dbprefix.'mambots';

    $version = ' v.'._SEF_SM_VERSION;

    ?>
    <form action="index2.php" method="post" name="adminForm">
    <table class="adminheading">
      <tr>
        <th>
          SEF Service Map - <?php echo _SEF_Config ?>
        </th>
      </tr>
    </table>       
    <?php 
    global $mosConfig_absolute_path, $database,$mosConfig_cachetime;
    $k=0;
    ?>
    <table align = "left" width="100%" class="adminform">
      <tr>
        <td>
          <?php echo $params->render();?>
        </td>
      </tr>
      <tr align="center"><td align="center"><hr/><div class="small"><a href="http://fun.kubera.org/component/option,com_remository/Itemid,33/func,select/id,4/">SEF Service Map Component<?php echo $version?></a></div></td></tr><tr align="center"><td align="center"><div class="small">by Radoslaw Kubera</div></td></tr>        
    </table>
    <input type="hidden" name="task" value="config">
    <input type="hidden" name="option" value="com_sefservicemap" />
    </form>
    <?php
  }

  function edit_config_css ($params)
  { 
    ?>
  <script language="javascript" type="text/javascript">
	    function submitbutton(pressbutton) {
	      var form = document.adminForm;
	      if (pressbutton == 'cancel') {
	        submitform( pressbutton );
	        return;
	      } else {
	        submitform( pressbutton );
	      }
	    }
	    </script>
    <?php
    if(is_callable('mosCommonHTML',"loadOverlib")) mosCommonHTML::loadOverlib();

    global $mosConfig_lang,$mosConfig_absolute_path,$database,$mosConfig_dbprefix;

    global $mosConfig_dbprefix;
    $plugtable=$mosConfig_dbprefix.'plugins';
    $alltables=$database->getTableList();
    if (!in_array($mosConfig_dbprefix.'plugins',$alltables)) $plugtable = $mosConfig_dbprefix.'mambots';

    $version = ' v.'._SEF_SM_VERSION;

    ?>
    <form action="index2.php" method="post" name="adminForm">
    <table class="adminheading">
      <tr>
        <th>
          SEF Service Map - <?php echo _CSS_TEMPLATE ?>
        </th>
      </tr>
    </table>
         
    <?php 
    global $mosConfig_absolute_path, $database,$mosConfig_cachetime;
      
    $k=0;
    ?>
    <table width="100%" class="adminform">
      <tr align="center">
        <td align="center" width="100%">
          <div align="center">
            <textarea name="css" rows="30" cols="100"><?php include $mosConfig_absolute_path.'/components/com_sefservicemap/css/template_css.css' ?></textarea> 
          </div>
        </td>
      </tr>
      <tr align="center"><td align="center"><div class="small"><hr/><a href="http://fun.kubera.org/component/option,com_remository/Itemid,33/func,select/id,4/">SEF Service Map Component<?php echo $version?></a></div></td></tr><tr align="center"><td align="center"><div class="small">by Radoslaw Kubera</div></td></tr>
    </table>
    <input type="hidden" name="task" value="config">
    <input type="hidden" name="option" value="com_sefservicemap" />
    </form>
    <?php
  }



  function edit_config_integrators ($params)
  { 
    ?>
  <script language="javascript" type="text/javascript">
	    function submitbutton(pressbutton) {
	      var form = document.adminForm;
	      if (pressbutton == 'cancel') {
	        submitform( pressbutton );
	        return;
	      } else {
	        submitform( pressbutton );
	      }
	    }
	    </script>
    <?php
    if(is_callable('mosCommonHTML',"loadOverlib")) mosCommonHTML::loadOverlib();

    global $mosConfig_lang,$mosConfig_absolute_path,$database,$mosConfig_dbprefix;

    global $mosConfig_dbprefix;
    $plugtable=$mosConfig_dbprefix.'plugins';
    $alltables=$database->getTableList();
    if (!in_array($mosConfig_dbprefix.'plugins',$alltables)) $plugtable = $mosConfig_dbprefix.'mambots';

    $version = ' v.'._SEF_SM_VERSION;

    ?>
    <form action="index2.php" method="post" name="adminForm">
    <table class="adminheading">
      <tr>
        <th>
          SEF Service Map - <?php echo _Integrators_Info ?>
        </th>
      </tr>
    </table>     
    <?php 
    global $mosConfig_absolute_path, $database,$mosConfig_cachetime;
      
    $k=0;

    ?>
    <table align = "left" width="100%" class="adminform">
      <tr class="<?php echo "row$k"; ?>">
        <th width="30%"><b><?php echo _Global_Settings?></b></td>
        <th width="5%"><b><?php echo _Published?></b></td>
        <th width="15%"><b><?php echo _Access?></b></td>
        <th width="30%"><b><?php echo _Author?></b></td> 
        <th width="20%"><b><?php echo _Version?></b></td> 
      </tr>      
      <?php
      $query = "SELECT * from ".$plugtable." where folder='com_sefservicemap' and published>='0'";
      $database->setQuery($query);
      $rows = $database->loadObjectList();
      foreach ($rows as $row)
      {

        $k++; if ($k>1) $k=0;       
        $xmlfile = $mosConfig_absolute_path. '/mambots/'.$row->folder . '/' . $row->element .'.'.$mosConfig_lang;
        if (!file_exists($xmlfile))  $xmlfile = $mosConfig_absolute_path. '/mambots/'.$row->folder . '/' . $row->element .".xml";

        if (!file_exists($xmlfile))  $xmlfile = $mosConfig_absolute_path. '/plugins/'.$row->folder . '/' . $row->element .'.'.$mosConfig_lang;
        if (!file_exists($xmlfile))  $xmlfile = $mosConfig_absolute_path. '/plugins/'.$row->folder . '/' . $row->element .".xml";


        if (file_exists( $xmlfile )) 
        {
          $xmlDoc = new DOMIT_Lite_Document();
          $xmlDoc->resolveErrors( true );

          if (!$xmlDoc->loadXML( $xmlfile, false, true )) 
          {
            continue;
          }
          $root = &$xmlDoc->documentElement;
          if ($root->getTagName() != 'mosinstall') 
          {
            continue;
          }

          if ($root->getAttribute( "type" ) != "mambot") 
          {
            continue;
          }
          $element 			= &$root->getElementsByPath('name', 1);
          $row->mambotname 	= $element ? $element->getText() : '';
          $element 			= &$root->getElementsByPath('creationDate', 1);
          $row->creationdate 	= $element ? $element->getText() : '';
          $element 			= &$root->getElementsByPath('author', 1);
          $row->author 		= $element ? $element->getText() : '';
          $element 			= &$root->getElementsByPath('copyright', 1);
          $row->copyright 	= $element ? $element->getText() : '';
          $element 			= &$root->getElementsByPath('authorEmail', 1);
          $row->authorEmail 	= $element ? $element->getText() : '';
          $element 			= &$root->getElementsByPath('authorUrl', 1);
          $row->authorUrl 	= $element ? $element->getText() : '';
          $element 			= &$root->getElementsByPath('version', 1);
          $row->version 		= $element ? $element->getText() : '';
        }
        ?>
        <tr class="<?php echo "row$k"; ?>">
          <td><a href='index2.php?option=com_sefservicemap&task=integrator&settings=global&id=<?php echo $row->id?>'><?php echo $row->mambotname?></a></td>
          <td align="center">
            <?php if ($row->published==1) echo _yes; else echo _no;?>
          </td>
          <td>
            <?php
            $database->setQuery("select name from #__groups where id='$row->access'");
            $group = $database->loadResult();
            echo $group ?>
          </td>
          <td><?php echo $row->author?></td>
          <td><?php echo $row->version?></td>
        </tr>
        <?php
      }
      ?>    
      <tr align="center"><td colspan="5" align="center"><hr/><div class="small"><a href="http://fun.kubera.org/component/option,com_remository/Itemid,33/func,select/id,4/">SEF Service Map Component<?php echo $version?></a></div></td></tr><tr align="center"><td colspan="5" align="center"><div class="small">by Radoslaw Kubera</div></td></tr>        
    </table>
    <input type="hidden" name="task" value="config_integrators">
    <input type="hidden" name="option" value="com_sefservicemap" />
    </form>
    <?php
  }

  function edit_config_cache ($params,$cache)
  { 
    $k=1;
    ?>
  <script language="javascript" type="text/javascript">
	    function submitbutton(pressbutton) {
	      var form = document.adminForm;
	      if (pressbutton == 'cancel') {
	        submitform( pressbutton );
	        return;
	      } else {
	        submitform( pressbutton );
	      }
	    }
	    </script>
    <?php
    if(is_callable('mosCommonHTML',"loadOverlib")) mosCommonHTML::loadOverlib();

    global $mosConfig_lang,$mosConfig_absolute_path,$database,$mosConfig_dbprefix;

    global $mosConfig_dbprefix;
    $plugtable=$mosConfig_dbprefix.'plugins';
    $alltables=$database->getTableList();
    if (!in_array($mosConfig_dbprefix.'plugins',$alltables)) $plugtable = $mosConfig_dbprefix.'mambots';

    $version = ' v.'._SEF_SM_VERSION;

    ?>
    <form action="index2.php" method="post" name="adminForm"> 
    <table class="adminheading">
      <tr>
        <th>
          SEF Service Map - <?php echo _SEF_Cache ?>
        </th>
      </tr>
    </table>         
    <?php 
    global $mosConfig_absolute_path, $database,$mosConfig_cachetime;
      
    $query = "SELECT * from ".$plugtable." where folder='com_sefservicemap' and published>='0'";
    $database->setQuery($query);
    $rows = $database->loadObjectList();
    ?>
    <table align = "left" width="100%" class="adminform">
      <tr>
        <th><?php echo _SEF_INTEGERATOR?></th>
        <th><?php echo _SEF_CACHE_ENABLED?></th>
        <th><?php echo _SEF_CACHE_TIME?></th>
      </tr>
      <?php
      foreach ($rows as $row)
      {

        $xmlfile = $mosConfig_absolute_path. '/mambots/'.$row->folder . '/' . $row->element .'.'.$mosConfig_lang;
        if (!file_exists($xmlfile))  $xmlfile = $mosConfig_absolute_path. '/mambots/'.$row->folder . '/' . $row->element .".xml";

        if (!file_exists($xmlfile))  $xmlfile = $mosConfig_absolute_path. '/plugins/'.$row->folder . '/' . $row->element .'.'.$mosConfig_lang;
        if (!file_exists($xmlfile))  $xmlfile = $mosConfig_absolute_path. '/plugins/'.$row->folder . '/' . $row->element .".xml";

        if (file_exists( $xmlfile )) 
        {
          $xmlDoc = new DOMIT_Lite_Document();
          $xmlDoc->resolveErrors( true );
          if (!$xmlDoc->loadXML( $xmlfile, false, true )) 
          {
            continue;
          }
          $root = &$xmlDoc->documentElement;
          if ($root->getTagName() != 'mosinstall') 
          {
            continue;
          }
          if ($root->getAttribute( "type" ) != "mambot") 
          {
            continue;
          }
          $element 			= &$root->getElementsByPath('name', 1);
          $row->mambotname 	= $element ? $element->getText() : '';
          $element 			= &$root->getElementsByPath('creationDate', 1);
          $row->creationdate 	= $element ? $element->getText() : '';
          $element 			= &$root->getElementsByPath('author', 1);
          $row->author 		= $element ? $element->getText() : '';
          $element 			= &$root->getElementsByPath('copyright', 1);
          $row->copyright 	= $element ? $element->getText() : '';
          $element 			= &$root->getElementsByPath('authorEmail', 1);
          $row->authorEmail 	= $element ? $element->getText() : '';
          $element 			= &$root->getElementsByPath('authorUrl', 1);
          $row->authorUrl 	= $element ? $element->getText() : '';
          $element 			= &$root->getElementsByPath('version', 1);
          $row->version 		= $element ? $element->getText() : '';
        }
        echo '<tr class="row'.$k.'">';
        $k++; if ($k>1) $k=0;       
        echo '<td>'.$row->mambotname.'</td>';
        $enabled = $params->get($row->element,0);
        $value = $params->get($row->element.'_time',0);
        if (!$value) $value = $mosConfig_cachetime;
        ?> 
        <td>
          <select name ="params[<?php echo $row->element?>]" class="inputbox">
            <option value ="0" <?php if ($enabled==0) echo 'selected'; ?>><?php echo _SEF_NO?></option>
            <option value ="1" <?php if ($enabled==1) echo 'selected';?>><?php echo _SEF_ENABLED?></option>
          </select>
        </td>
        <td> 
          <input size=10" type="text" class = "inputbox" name = "params[<?php echo $row->element?>_time]" value="<?php echo $value?>"><?php echo _SEF_SECONDS?>
        </td>
        <?php
        echo '</tr>';
      }
      if ($cache) 
      { ?>
        <tr>
          <td>
            <input type="submit" class="button" name="remove_all" value ="<?php echo _SEF_CLEAR_CACHE_ALL ?>">
          </td>
        </tr>
       <?php 
      } ?>
      <tr align="center"><td colspan="3" align="center"><hr/><div class="small"><a href="http://fun.kubera.org/component/option,com_remository/Itemid,33/func,select/id,4/">SEF Service Map Component<?php echo $version?></a></div></td></tr><tr align="center"><td colspan="3" align="center"><div class="small">by Radoslaw Kubera</div></td></tr>
    </table>
    <input type="hidden" name="task" value="config_cache">
    <input type="hidden" name="option" value="com_sefservicemap" />
    </form>
    <?php
  }

  function edit_integrator ($id,$menu_id,$title,$name,$row,$params,$settings)
  {
    global $database;
  $version = ' v.'._SEF_SM_VERSION;

  ?>
  <script language="javascript" type="text/javascript">
	    function submitbutton(pressbutton) {
	      var form = document.adminForm;
	      if (pressbutton == 'cancel') {
	        submitform( pressbutton );
	        return;
	      } else {
	        submitform( pressbutton );
	      }
	    }
	    </script>
  <?php
  if(is_callable('mosCommonHTML',"loadOverlib")) mosCommonHTML::loadOverlib();
  ?>	
    <form action="index2.php" method="post" name="adminForm">
      <table class="adminheading">
        <tr>
          <th>SEF Service Map - <?php echo $title.' - '.$name ?></th>
        </tr>
      </table>
      <table align = "left" width="100%" class="adminform">
        <tr>
         <th class = "title"><?php echo _Parameters?></th>
         <th class = "title"><?php echo _Mambot_Integrator_Info?></th>
        </tr>
        <tr>
          <td width = "50%" valign = "top" align = "left">
            <table class="adminform">
              <tr>
                <td>
                <?php echo $params->render();?>
                </td>
              </tr>
            </table>
          </td>
          <td valign = "top">
            <table class="adminform">
              <tr>
		<td width="100" align="left">
					<?php echo _Mambot_Name?>:
					</td>
					<td>
					  <?php echo $row->mambotname; ?>
					</td>
				</tr>
				<tr>
					<td valign="top" align="left">
					<?php echo _Folder?>:
					</td>
					<td>
					<?php echo $row->folder; ?>
					</td>
				</tr>
				<tr>
					<td valign="top" align="left">
					<?php echo _Mambot_file?>:
					</td>
					<td>
					  <?php echo $row->element; ?>.php
					</td>
				</tr>
				<tr>
					<td valign="top" align="left">
					<?php echo _Access?>:
					</td>
					<td>
					<?php
                                        $database->setQuery("select name from #__groups where id='$row->access'");
                                        $group = $database->loadResult();
                                        echo $group ?>
					</td>
				</tr>
				<tr>
					<td valign="top">
					<?php echo _Published?>:
					</td>
					<td>
					<?php if ($row->published == 1) echo 'Yes'; else echo 'No' ?>
					</td>
				</tr>
				<tr>
					<td valign="top">
                                        <?php echo _Author?>:
					</td>
                                        <td>
                                        <?php echo $row->author ?>
                                        </td>
				</tr>
				<tr>
					<td valign="top">
					<?php echo _Version?>:
					</td>
					<td>
					<?php echo $row->version; ?>
					</td>
				</tr>
            </table>   
          </td>
        </tr>
        <tr>
          <th class = "title" colspan="2"></th>
        </tr>
        <tr><td colspan="2" valign="bottom" align="center"><div class="small"><a href="http://fun.kubera.org/component/option,com_remository/Itemid,33/func,select/id,4/">SEF Service Map Component<?php echo $version?></a></div></td></tr><tr><td colspan="2" align="center"><div class="small">by Radoslaw Kubera</div></td></tr>
      </table>   
      <input type="hidden" name="task" value="saveint">
      <input type="hidden" name="menu_id" value="<?php echo $menu_id?>">
      <input type="hidden" name="option" value="com_sefservicemap" />
      <input type="hidden" name="id" value="<?php echo $id?>" /> 
      <input type="hidden" name="settings" value="<?php echo $settings ?>" />  
    </form>
    <?php
  }

  function show_all_menus($menus,$countm,$cache)
  {
    $numberm=1;
   
    if(is_callable('mosCommonHTML',"loadOverlib")) mosCommonHTML::loadOverlib();


    ?>
    <script language="javascript" type="text/javascript">
	    function submitbutton(pressbutton) {
	      var form = document.adminForm;
	      if (pressbutton == 'cancel') {
	        submitform( pressbutton );
	        return;
	      } else {
	        submitform( pressbutton );
	      }
	    }
	    </script>

		<form action="index2.php" method="post" name="adminForm">
		<table class="adminheading">
		<tr>
			<th>
			SEF Service Map - <?php echo _SEF_Structure ?>
			</th>
                </tr>
                </table>
                <?php
                
                $k=0;               
                ?>
                <table class="adminlist">
                <tr>
                        <th width = "5%" class="title">
			<?php echo _ID?>
			</th>
			
		        <th width = "30%" class="title">
			<?php echo _Menu_name ?>
			</th>
                        <th colspan="2" width="1%" class="title" nowrap="true">
			<?php echo _Reorder?>
			</th>
			<th width="5%" class="title" nowrap="true">
			<?php echo _Published?>
			</th>
			<th width="2%" class="title" nowrap="true">
			<?php echo _Integrator?>
			</th>
                        <th width = "40%" class="title" nowrap="true">
			<?php echo _Link?>
			</th>

                        <th nowrap="true" align="center">
			<?php echo _SEF_Cache?>
			</th>

                </tr>
                <?php
                foreach ($menus as $menu)
                {
                   $pars = new mosParameters($menu->params);
                   $title = $menu->title;
                
                  if ($menu->published==1)
                  {
                    $img='tick.png';
                    $task='unpublishmenu';
                    $alt = "Published";
                    $proc =1;
                  }
                  else
                  {
                    $img = 'publish_x.png';
                    $task = "publishmenu";
                    $alt = "Unpublished";
                    $proc=1;
                  }  

                ?>
		<tr bgcolor="#E4E4E4">
                        <td>
			<b>ID</b>
			</td>
			
		        <td>
			  <b><?php if ($proc==1) echo "<a href='index2.php?option=com_menus&menutype=".$menu->title."'>".$menu->title."</a>"; else echo "<a href='index2.php?option=com_menus&menutype=".$menu->title."'><i>".$menu->title."</i></a>";?></b>
			</td>
                        <td>
                        <?php up_down ($menu->id, $countm,$numberm);$numberm++;?> 
                         </td>
			<td align="center">
                           <?php if ($proc==1) {?><a href='index2.php?option=com_sefservicemap&task=<?php echo $task?>&id=<?php echo $menu->id?>'><img src='images/<?php echo $img ?>' hspace='3' border='0' alt="<?php echo $alt ?>"></a><?php }?>
                           <?php if ($proc==0) {echo '<img src="images/'.$img.'" />';}?>
                        </td>
			<td>
			</td>
                        <td>
			</td>
                        <td>
			</td>

                </tr>
                 <?php


                find_menu(0,$menu->title,'',$k,$menu->title);

           
                 }
                 $version = ' v.'._SEF_SM_VERSION; 

                 ?>
                <tr>
                  <th colspan="7"></th>
                  <th align="right">
            <?php if ($cache) { ?>
                <input type="submit" class="button" name="remove_all" value ="<?php echo _SEF_CLEAR_CACHE_ALL ?>">
            <?php } ?>
                   </th>
                </tr>
                <tr><td colspan="8" valign="bottom" align="center"><div class="small"><a href="http://fun.kubera.org/component/option,com_remository/Itemid,33/func,select/id,4/">SEF Service Map Component<?php echo $version ?></a></div></td></tr><tr><td colspan="8" align="center"><div class="small">by Radoslaw Kubera</div></td></tr>
                </table>
                <input type="hidden" name="option" value="com_sefservicemap" />
                <input type="hidden" name="task" value="menu">
                </form>
                <?php



  }

}

?>