<?php
//*******************************************************
//* SEF Service Map Component
//* http://fun.kubera.org
//* (C) Radoslaw Kubera
//* license http://www.gnu.org/copyleft/gpl.html GNU/GPL
//*******************************************************


/** ensure this file is being included by a parent file */
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

class TOOLBAR_sefservicemap {

       function MENU()
       {
         $task = mosGetParam($_REQUEST,'task','config');
 	 mosMenuBar::startTable(); 
         if ($task =='config') mosMenuBar::save( 'save', 'Save' );
         if ($task =='integrator') mosMenuBar::save( 'saveint', 'Save' );
         if ($task =='config_cache') mosMenuBar::save( 'savecache', 'Save' );
         if ($task =='config_css') mosMenuBar::save( 'savecss', 'Save' );

	 mosMenuBar::custom( 'menu','edit.png','edit_f2.png', _SEF_Structure, false );
	 mosMenuBar::custom( 'config','edit.png','edit_f2.png', _SEF_Config, false );
	 mosMenuBar::custom( 'config_integrators','edit.png','edit_f2.png', _Integrators_Info, false );
	 mosMenuBar::custom( 'config_cache','edit.png','edit_f2.png', _SEF_Cache, false );
	 mosMenuBar::custom( 'config_css','edit.png','edit_f2.png',_CSS_TEMPLATE, false );
	 mosMenuBar::custom( 'about', 'help.png', 'help_f2.png', _SEF_Help, false );
         mosMenuBar::endTable();
       }

}

?>
