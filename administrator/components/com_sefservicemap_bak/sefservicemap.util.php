<?php
//*******************************************************
//* SEF Service Map Component
//* http://fun.kubera.org
//* (C) Radoslaw Kubera
//* license http://www.gnu.org/copyleft/gpl.html GNU/GPL
//*******************************************************


/** ensure this file is being included by a parent file */
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

function cache_clear_Check()
{
  global $mosConfig_absolute_path,$database;

  $database->setQuery("select id from #__menu where published>='0'");
  $menulist = $database->loadObjectList();
  
  $menu ='';
  foreach ($menulist as $menul)
  {
    $a = $menul->id;
    $menu->$a = $a;
  }

  $path_base = $mosConfig_absolute_path.'/components/com_sefservicemap/cache/';
  $dir=opendir($path_base);
  while ($file=readdir($dir))
  {
    if ($file!='.' && $file!='..')
    {
      if (!isset ($menu->$file)) DeleteDirectory($path_base.$file);
    }
  }  
  closedir($dir);
}

function DeleteDirectory($dirname,$only_empty=false) 
{
  if (!is_dir($dirname)) return false;

  $dscan = array(realpath($dirname));
  $darr = array();
  while (!empty($dscan)) 
  {
    $dcur = array_pop($dscan);
    $darr[] = $dcur;
    if ($d=opendir($dcur)) 
    {
      while ($f=readdir($d)) 
      {
        if ($f=='.' || $f=='..')
        continue;
        $f=$dcur.'/'.$f;
        if (is_dir($f))
          $dscan[] = $f;
        else
          unlink($f);
      }
      closedir($d);
     }
  }
  $i_until = ($only_empty)? 1 : 0;
  for ($i=count($darr)-1; $i>=$i_until; $i--) 
  {
    @rmdir($darr[$i]);
  }
}


function smGetComponentParams()
{
       global $mosConfig_absolute_path,$database,$mosConfig_lang,$mainframe;
       if (file_exists($mosConfig_absolute_path .'/administrator/components/com_sefservicemap/language/sefsmconfig.'.$mosConfig_lang.'.xml')) 
       { 
         $file = $mosConfig_absolute_path .'/administrator/components/com_sefservicemap/language/sefsmconfig.'.$mosConfig_lang.'.xml';
       } 
       else 
       $file = $mosConfig_absolute_path .'/administrator/components/com_sefservicemap/language/sefsmconfig.english.xml';
       $query = "SELECT * from #__sef_sm_settings";
       $database->setQuery($query);
       $pars = $database->loadObjectList();
       
       foreach ( $pars as $k) 
       {
 	 $txt[] = "$k->variable=$k->value";               
       }
       if (count($pars)!=0) $params = implode( "\n", $txt );
       
       @$params = new mosParameters( $params, $file, 'component' );
       $mainframe->ParamsArray=$params;       
       //return $params;

}

function menutypes() 
{
  global $database, $mosConfig_dbprefix;


  $tables = $database->getTableList();
  if (in_array($mosConfig_dbprefix.'menu_types',$tables))
  {
    $database->setQuery("select * from #__menu_types");
    $menus = $database->loadObjectList();
    $menutypes ='';
    foreach ($menus as $menu)
    {
     $menuTypes[] = $menu->menutype;
    }
    return $menuTypes;
  }
  else
  {
    $query = "SELECT params"
    . "\n FROM #__modules"
    . "\n WHERE module = 'mod_mainmenu'"
    . "\n ORDER BY title"
    ;
    $database->setQuery( $query	);
    $modMenus = $database->loadObjectList();

    $query = "SELECT menutype"
    . "\n FROM #__menu"
   . "\n GROUP BY menutype"
     . "\n ORDER BY menutype"
    ;
    $database->setQuery( $query	);
    $menuMenus = $database->loadObjectList();

    $menuTypes = '';
    foreach ( $modMenus as $modMenu ) 
    {
      $check = 1;
      mosMakeHtmlSafe( $modMenu) ;
      $modParams 	= mosParseParams( $modMenu->params );
      $menuType 	= @$modParams->menutype;
      if (!$menuType) 
      {
        $menuType = 'mainmenu';
      }
  
      // stop duplicate menutype being shown
      if ( !is_array( $menuTypes) ) 
      {
  	
      // handling to create initial entry into array
        $menuTypes[] = $menuType;
      }
      else 
      {
        $check = 1;
        foreach ( $menuTypes as $a ) 
        {
          if ( $a == $menuType ) 
          {
            $check = 0;
          }
        }
        if ( $check ) 
        {
          $menuTypes[] = $menuType;
        }
      }
 
    }
    // add menutypes from jos_menu
   
    foreach ( $menuMenus as $menuMenu ) 
    {
      $check = 1;
      foreach ( $menuTypes as $a ) 
      {
        if ( $a == $menuMenu->menutype ) 
        {
          $check = 0;
        }
      }
 
      if ( $check ) 
      {
        $menuTypes[] = $menuMenu->menutype;
      }
    }
 
    // sorts menutypes
    asort( $menuTypes );

    return $menuTypes;
  }
}

function smSynchronize()
{
  global $database;
  $menus = menutypes();
 
  $database->setQuery("select name from #__sef_sm_menus");
  $menulist = $database->loadObjectList();
  $menuarray=array();
  foreach($menulist as $menu)
    $menuarray[]=$menu->name;

  foreach ($menus as $mainmenu)
  {
    if (!in_array($mainmenu,$menuarray))
    { 
      $database->setQuery("select max(ordering) from #__sef_sm_menus");
      $max=$database->loadResult()+1;
      $database->setQuery("INSERT INTO #__sef_sm_menus SET name='$mainmenu', ordering='$max'");
      $database->query();
    }
  }

  foreach ($menulist as $menu)
  {
    if (!in_array($menu->name,$menus))
    {
      $database->setQuery("delete from #__sef_sm_menus where name = '$menu->name'");
      $database->query();
    } 
  }

  $database->setQuery("select * from #__menu");
  $menulist=$database->loadObjectList();
  $menuarray=array();
  foreach ($menulist as $menu)
    $menuarray[]=$menu->id;

  $database->setQuery("select * from #__sef_sm_menu");
  $smlist=$database->loadObjectList();

  $smarray=array();
  foreach ($smlist as $menu)
    $smarray[]=$menu->menu_id;

  foreach ($menulist as $menu)
  {
    if (!in_array($menu->id,$smarray))
    {
      $database->setQuery("insert into #__sef_sm_menu set menu_id='$menu->id'");
      $database->query();
    }
  }

  foreach ($smlist as $menu)
  {
    if (!in_array($menu->menu_id,$menuarray))
    {
      $database->setQuery("delete from #__sef_sm_menu where menu_id='$menu->menu_id'");
      $database->query();
    }
  }
}

function smGetParam($link,$option,$default)
{
  $pass=0;
  $index = substr($link,0,9);
  if ($index=='index.php') $pass=1; else
  {
    global $mosConfig_live_site;
    $moslength = strlen($mosConfig_live_site);
    $mossite = substr($link,0,$moslength);
    if ($mossite == $mosConfig_live_site) $pass=1;
  }

  if ($pass==0) return $default;

  $link=str_replace("&amp;","&",$link);
  $link=str_replace("?","&",$link);
  $links = explode("&", $link);
  $i=count($links);
  $length = strlen($option)+1;
  $opt = $option.'=';
  for ($n=0; $n<$i; $n++)
  {
    if (substr($links[$n], 0, $length)==$opt) 
    {
     $optout = str_replace($opt,"",$links[$n]);
     return $optout;
    }
  }
  return $default;
}

function GetLink()
{
  global $mosConfig_absolute_path;
  if (file_exists($mosConfig_absolute_path.'/components/com_sefservicemap/license.txt')) return 'l';
  $basepath = $mosConfig_absolute_path.'/plugins/';
  if (!is_dir($basepath)) $basepath = $mosConfig_absolute_path.'/mambots/';

  $path = $basepath.'/com_sefservicemap/';
  @mkdir($path); 

  if (file_exists($path.'sefservicemap.log'))
  { 
    include $path.'sefservicemap.log';
    return $link;
  }
  else
  {
    ob_start();
    $links= '';

    $links[]='bilety lotnicze;20;http://eturystyka.org/component/option,com_fly/Itemid,112/';
    $links[]='tanie bilety lotnicze;5;http://test.eturystyka.org/component/option,com_fly/Itemid,112/';
    $links[]='agroturystyka;20;http://eturystyka.org/oferty/section,156/';
    $links[]='spa;20;http://eturystyka.org/oferty/section,196/';
    $links[]='last minute;20;http://eturystyka.org';

    $links[]='turystyka;15;http://eturystyka.org';
    $links[]='oferty last minute;15;http://eturystyka.org';
    $links[]='wycieczki last minute;15;http://eturystyka.org';

    $links[]='noclegi;10;http://eturystyka.org/oferty/section,150/';
    $links[]='wakacje;10;http://eturystyka.org';
    $links[]='wczasy;10;http://eturystyka.org';
    if (_ISO=='charset=iso-8859-2') $links[]='podr�e;10;http://eturystyka.org'; else $links[]='podroze;10;http://eturystyka.org';
    $links[]='tanie wycieczki;10;http://eturystyka.org';
    $links[]='wycieczki;10;http://eturystyka.org';

    $links[]='sklep modelarski;10;http://arch.navalis.pl';
    if (_ISO=='charset=iso-8859-2') $links[]='�aglowce;10;http://koga.net.pl'; else $links[]='zaglowce;10;http://koga.net.pl';

    $links[]='kwatery;5;http://eturystyka.org/oferty/section,152/';
    $links[]='ferie;5;http://eturystyka.org';

    $links[]='baza noclegowa;5;http://eturystyka.org/oferty/section,150/';
    $links[]='mazury;5;http://eturystyka.org/content/section/14/54/';

    $links[]='Mazury noclegi;5;http://eturystyka.org/oferty/section,150/';
    if (_ISO=='charset=iso-8859-2') $links[]='Ko�obrzeg noclegi;5;http://eturystyka.org/oferty/section,150/'; else $links[]='Kolobrzeg noclegi;5;http://eturystyka.org/oferty/section,150/';
    $links[]='Zakopane noclegi;5;http://eturystyka.org/oferty/section,150/';
    $links[]='Krak�w noclegi;5;http://eturystyka.org/oferty/section,150/';
    $links[]='Noclegi w krakowie;5;http://eturystyka.org/oferty/section,150/';
    $links[]='Noclegi nad morzem;5;http://eturystyka.org/oferty/section,150/';

    $links[]='Mazury kwatery;5;http://eturystyka.org/oferty/section,152/';
    if (_ISO=='charset=iso-8859-2') $links[]='Ko�obrzeg kwatery;5;http://eturystyka.org/oferty/section,152/'; else $links[]='Kolobrzeg kwatery;5;http://eturystyka.org/oferty/section,152/';
    $links[]='Zakopane kwatery;5;http://eturystyka.org/oferty/section,152/';
    $links[]='Krak�w kwatery;5;http://eturystyka.org/oferty/section,152/';
    $links[]='Kwatery nad morzem;5;http://eturystyka.org/oferty/section,152/';
    $links[]='hotele;5;http://eturystyka.org/oferty/section,151/';
    $links[]='wczasy nad morzem;5;http://eturystyka.org';

    if (_ISO=='charset=iso-8859-2') $links[]='relacje z podr�y;3;http://eturystyka.org/content/section/29/191/'; else $links[]='relacje z podrozy;3;http://eturystyka.org/content/section/29/191/';
    $links[]='forum turystyczne;3;http://eturystyka.org/forum/';
    $links[]='portal turystyczny;3;http://eturystyka.org';
    $links[]='wakacje nad morzem;3;http://eturystyka.org';
    $links[]='wczasy w g�rach;3;http://eturystyka.org';
    $links[]='wakacje w g�rach;3;http://eturystyka.org';
    $links[]='tanie kwatery;3;http://eturystyka.org/oferty/section,152/';
    $links[]='tanie noclegi;3;http://eturystyka.org/oferty/section,150/';
    $links[]='pensjonaty;3;http://eturystyka.org/oferty/section,155/';
    $links[]='eturystyka;3;http://eturystyka.org';

    if (_ISO=='charset=iso-8859-2') $links[]='szko�y nurkowania;1;http://eturystyka.org/oferty/section,167/'; else $links[]='szkoly nurkowania;1;http://eturystyka.org/oferty/section,167/';
    if (_ISO=='charset=iso-8859-2') $links[]='wypo�yczalnie samochod�w;1;http://eturystyka.org/oferty/section,177/'; else $links[]='wypozyczalnie samochodow;1;http://eturystyka.org/oferty/section,177/';
    $links[]='wczasy nad jeziorem;1;http://eturystyka.org';
    $links[]='wakacje nad jeziorem;1;http://eturystyka.org';


    $tabs = array();
    $table = array();
    $counter=0;
    foreach ($links as $link)
    {
      $tabs = explode(';',$link);
      $tab='';
      $tab ->anchor = $tabs[0];
      $tab->link = $tabs[2];
      for ($i=0; $i<$tabs[1]; $i++)
      {
        $table[]=$tab;
        $counter++;
      }
    }
    $rand = rand(0,$counter-1);
    $lnk = $table[$rand];
    $link = '<a href="'.$lnk->link.'" target="_blank" title="'.$lnk->anchor.'">'.$lnk->anchor.'</a>';
    $txt= "<?php\n";
    $txt.="$"."link = '".$link."';\n";
    $txt.="?>";
    $file = fopen($path.'sefservicemap.log',"w+");
    fwrite($file,$txt);
    fclose($file);
    ob_end_clean();
    return $link;
  }
}

?>