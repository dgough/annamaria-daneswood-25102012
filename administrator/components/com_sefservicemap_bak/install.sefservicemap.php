<?php
//*******************************************************
//* SEF Service Map Component
//* http://fun.kubera.org
//* (C) Radoslaw Kubera
//* license http://www.gnu.org/copyleft/gpl.html GNU/GPL
//*******************************************************

/** ensure this file is being included by a parent file */
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );
global $mosConfig_absolute_path,$mosConfig_lang;
$adminPath = $mosConfig_absolute_path.'/administrator/';
include_once ($adminPath."/components/com_sefservicemap/admin.sefservicemap.html.php");

if (file_exists($adminPath."/components/com_sefservicemap/language/".$mosConfig_lang.".php")) 
{ 
  include_once ($adminPath."/components/com_sefservicemap/language/".$mosConfig_lang.".php");
} 
else 
{
  include_once ($adminPath."/components/com_sefservicemap/language/english.php");
} 
include_once ($mosConfig_absolute_path.'/components/com_sefservicemap/version.php');

function com_install()
{
  global $mosConfig_absolute_path,$mosConfig_lang;
  $adminPath = $mosConfig_absolute_path.'/administrator/';
?>
  <center>
  <table width="100%" border="0">
    <tr>
      <td>
        <strong>SEF Service Map Component v.<?php echo _SEF_SM_VERSION ?></strong><br/>
        <font class="small">by Radoslaw Kubera.</font>
      </td>
    </tr>
    <tr>
      <td>
      <?php

        if (file_exists($adminPath."/components/com_sefservicemap/language/".$mosConfig_lang.".help.php")) 
        { 
          $f = file ($adminPath."/components/com_sefservicemap/language/".$mosConfig_lang.".help.php");
        } 
        else 
        {
          $f = file ($adminPath."/components/com_sefservicemap/language/english.help.php");
        } 
        $txt = implode ("<br/>",$f); 
        echo $txt;
      ?>
      </td>
    </tr>
    <tr> 
      <td>
        <font color="green"><b>Installation finished.</b></font>
      </td>
    </tr>
   </table>
   </center>
<?php
  global $mosConfig_absolute_path;
  $adminpath = $mosConfig_absolute_path.'/administrator/components/com_sefservicemap/';
  $path = $mosConfig_absolute_path.'/components/com_sefservicemap/';
  @mkdir ($path.'cache/');
  rename ($adminpath.'sefsmconfig.english.xml',$adminpath.'language/sefsmconfig.english.xml');
  rename ($adminpath.'sefsmconfig.polish.xml',$adminpath.'language/sefsmconfig.polish.xml');
}

?>