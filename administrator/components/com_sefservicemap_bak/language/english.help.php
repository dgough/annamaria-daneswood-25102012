<u>1. General information</u>
Position your site in the Internet!
SEF Service Map is a component, which task is to create a complete sitemap. Beside being fully compatible with standard Joomla! components (articles, RSS headlines, links, contacts), SEF Service Map is an open component - it allows integration with other components like galleries, forums, catalogs and others, thanks to built-in integration mechanisms.

SEF Service Map does create not only a standard sitemap, but also sitemaps compatible with the sitemaps.org standards:
- XML map (e.g. for Google maps)
- TXT map (e.g. for Yahoo maps)

These are a few of the additional functions of this component:
- it uses its own cache, so it reduces server load;
- it allows individual settings of the integrators for each menu item;
- you can include/exclude any menu or menu element from the sitemap;
- creating shortened descriptions for the sitemap items;
- you can turn on/off the loading of icons;
- definable CSS styles;

Some of the components fully compatible with SEF Service Map:
- Fireboard,Joolmaboard, SMF Forum (joomlahacks.com bridge);
- DocMan, Remository;
- JCal PRo, ExtCalendar2;
- RSGallery2, Zoom Gallery;
- SOBI2;
- Virtuemart;
And others.

<u>2. Installation</u>
After you install the component, install 4 basic additional itegrators (plugins):
- Articles (com_content_bot.zip);
- Links (com_weblinks_bot.zip);
- RSS Headlines (com_newsfeeds_bot.zip);
- Contacts (com_contacts_bot.zip);
and publish them. The integrators are to be installed just as any other Joomla! plugins.

This is where you can find additional integrators for other components:
<a href="http://extensions.joomla.org/index.php?option=com_mtree&task=listcats&cat_id=1858&Itemid=35">JoomlaExtensions Directory</a>
<a href="http://fun.kubera.org/component/option,download/func,select/id,5/lang,iso-8859-1/">Fun With Joomla!</a>

If You are using OpenSEF, make small fix for them:
/components/com_sef/sef.php
line:
OR eregi("index2.php", $_SERVER['REQUEST_URI'])
replace with:
OR (eregi("index2.php", $_SERVER['REQUEST_URI']) && !eregi("com_sefservicemap",$_SERVER['REQUEST_URI']))

<u>3. License</u>
GNU/GPL

<u>4. Special Thanks</u>

- Jaros�aw Gawlowski - <a href="http://www.festivalinfo.pl"target="_blank">http://www.festivalinfo.pl</a> (small bug fixes);
- <a href="http://eturystyka.org" target="_blank">http://eturystyka.org</a> - for hosting and support for my projects;
- Bartek Kubera for translation;

<u>5. See also</u>
I recommend using my other component - JAccelerator - an advanced cache designed to hasten the loading time of the site and to reduce server load:
<a href="http://extensions.joomla.org/component/option,com_mtree/task,viewlink/link_id,2326/Itemid,35/">JAccelerator- JED</a>
and:
<a href="http://fun.kubera.org/content/view/20/45/lang,iso-8859-1/">JAccelerator - Fun With Joomla!</a>

<u>6. Contact</u>
Radoslaw Kubera <a mailto:radek@kubera.org>radek@kubera.org</a>
<a href="http://fun.kubera.org" target="_blank">Fun With Joomla!</a>