<?php
//*******************************************************
//* SEF Service Map Component
//* http://fun.kubera.org
//* (C) Radoslaw Kubera
//* license http://www.gnu.org/copyleft/gpl.html GNU/GPL
//*******************************************************


/** ensure this file is being included by a parent file */
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

DEFINE ('_SEF_Config','Konfiguracja');
DEFINE ('_SEF_Cache','Cache');

DEFINE ('_SEF_CACHE_TIME','Czas �ycia w cache');
DEFINE ('_SEF_CACHE_ENABLED','W��czony');
DEFINE ('_SEF_INTEGERATOR','Integrator');
DEFINE ('_SEF_ENABLED','W��czony');
DEFINE ('_SEF_NO','Nie');
DEFINE ('_SEF_SECONDS','Sekund');
DEFINE ('_SEF_CLEAR_CACHE','Oczy��');
DEFINE ('_SEF_CLEAR_CACHE_ALL','Oczy�� ca�o��');

DEFINE ('_Global_Settings','Ustawienia');
DEFINE ('_Integrators_Info','Integratory');
DEFINE ('_Mambot_Name','Nazwa Dodatku');
DEFINE ('_Published','Opublikowany');
DEFINE ('_Access','Dost�p');
DEFINE ('_Author','Autor');
DEFINE ('_Version','Wersja');
DEFINE ('_yes','Tak');
DEFINE ('_no','Nie');
DEFINE ('_Integrator_Zone','Strefa Integrator�w');

DEFINE ('_SEF_Structure','Struktura');
DEFINE ('_SEF_Help','Pomoc');
DEFINE ('_ID','ID');
DEFINE ('_Menu_name','Nazwa menu');
DEFINE ('_Reorder','Przesu�');
DEFINE ('_Integrator','Ustawienia Integratora');
DEFINE ('_Link','Link');
DEFINE ('_Disabled','Wy��czony');
DEFINE ('_Global','Globalne');
DEFINE ('_Custom','W�asne');
DEFINE ('_None','Brak');


DEFINE ('_Parameters','Parametry');
DEFINE ('_Mambot_Integrator_Info','Informacje o Integratorze');
DEFINE ('_Name','Nazwa');
DEFINE ('_Folder','Folder');
DEFINE ('_Mambot_file','Plik dodatku');

DEFINE ('_SITEMAP_XML','Mapa XML');
DEFINE ('_SITEMAP_TXT','Mapa TXT');

DEFINE ('_CSS_TEMPLATE','CSS');


?>