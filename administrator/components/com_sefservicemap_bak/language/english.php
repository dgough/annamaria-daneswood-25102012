<?php
//*******************************************************
//* SEF Service Map Component
//* http://fun.kubera.org
//* (C) Radoslaw Kubera
//* license http://www.gnu.org/copyleft/gpl.html GNU/GPL
//*******************************************************


/** ensure this file is being included by a parent file */
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

DEFINE ('_SEF_Config','Configuration');
DEFINE ('_SEF_Cache','Cache');

DEFINE ('_SEF_CACHE_TIME','Cache time');
DEFINE ('_SEF_CACHE_ENABLED','Enabled');
DEFINE ('_SEF_INTEGERATOR','Integrator');
DEFINE ('_SEF_ENABLED','Enabled');
DEFINE ('_SEF_NO','No');
DEFINE ('_SEF_SECONDS','Seconds');
DEFINE ('_SEF_CLEAR_CACHE','Clear');
DEFINE ('_SEF_CLEAR_CACHE_ALL','Clear all');

DEFINE ('_Global_Settings',' Settings');
DEFINE ('_Integrators_Info','Integrators');
DEFINE ('_Mambot_Name','Mambot Name');
DEFINE ('_Published','Published');
DEFINE ('_Access','Access');
DEFINE ('_Author','Author');
DEFINE ('_Version','Version');
DEFINE ('_yes','Yes');
DEFINE ('_no','No');
DEFINE ('_Integrator_Zone','Integrators Zone');

DEFINE ('_SEF_Structure','Structure');
DEFINE ('_SEF_Help','Help');
DEFINE ('_ID','ID');
DEFINE ('_Menu_name','Menu name');
DEFINE ('_Reorder','Reorder');
DEFINE ('_Integrator','Integrator Settigs');
DEFINE ('_Link','Link');
DEFINE ('_Disabled','Disabled');
DEFINE ('_Global','Global');
DEFINE ('_Custom','Custom');
DEFINE ('_None','None');

DEFINE ('_Parameters','Parameters');
DEFINE ('_Mambot_Integrator_Info','Mambot Integrator Info');
DEFINE ('_Folder','Folder');
DEFINE ('_Mambot_file','Mambot file');

DEFINE ('_SITEMAP_XML','XML Map');
DEFINE ('_SITEMAP_TXT','TXT Map');

DEFINE ('_CSS_TEMPLATE','CSS');

?>