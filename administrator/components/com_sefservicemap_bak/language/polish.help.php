<u>1. Inforamcje og�lne</u>
Wypozycjonuj sw�j serwis w Internecie!
SEF Service Map jest komponentem, kt�rego zadaniem jest stworzenie kompletnej mapy serwisu. Poza integracj� ze standardowymi komponentami Joomla! (artyku�y, nag��wki RSS, linki, kontakty) umo�liwia integracj� z innymi komponentami takimi jak galerie, fora, katalogi i inne, przy u�yciu wbudowanemu mechanizmowi integrator�w, co czyni SEF Service Map komponentem otwartym.

Komponent opr�cz standardowej mapy serwisu tworzy mapy zgodne ze standardem sitemaps.org:
- map� XML (np. dla Google saps)
- map� TXT (np. dla Yahoo maps)

Oto niedk�re, dodatkowe mo�liwo�ci komponentu:
- posiada w�asny cache, co zmniejsza obci��enie serwera;
- indywidualne ustawienia integrator�w dla ka�dej pozycji menu;
- mo�wiwo�� w��czenia/wy��czenia z mapy serwisu dowolnego menu lub pozycji z menu;
- tworzenie skr�conych opis�w do pozycji w mapie;
- mo�liwo�c w��czenia/wy��czenia wy�wietlania ikon;
- definiowane style CSS;

Niekt�re komponenty zitegrowane z SEF Service Map:
- Fireboard,Joolmaboard, SMF Forum (joomlahacks.com bridge);
- DocMan, Remository;
- JCal PRo, ExtCalendar2;
- RSGallery2, Zoom Gallery;
- SOBI2;
- Virtuemart;
I inne.

<u>2. Instalacja</u>
Po zainstalowaniu komponentu nale�y przeprowadzi� instalacj� 4 podstawowych dodatk�w (integrator�w):
- Artyku�y (com_content_bot.zip);
- Linki (com_weblinks_bot.zip);
- Nag��wki RSS (com_newsfeeds_bot.zip);
- Kontakty (com_contacts_bot.zip);
a nast�pnie je opublikowa�.
Integratory instaluje si� jak wszystkie standardowe dodatki Joomla!

Oto gdzie mo�na szuka� dodatkowych integrator�w do innych komponent�w:
<a href="http://extensions.joomla.org/index.php?option=com_mtree&task=listcats&cat_id=1858&Itemid=35">Joomla Extensions Directory</a>
<a href="http://fun.kubera.org/component/option,download/func,select/id,5/lang,iso-8859-2/">Fun With Joomla!</a>

Je�eli u�ywasz OpenSEF, zr�b ma�� poprawk�:
/components/com_sef/sef.php
linia:
OR eregi("index2.php", $_SERVER['REQUEST_URI'])
zamie� na:
OR (eregi("index2.php", $_SERVER['REQUEST_URI']) && !eregi("com_sefservicemap",$_SERVER['REQUEST_URI']))

<u>3. Licencja</u>
GNU/GPL

<u>4. Podzi�kowania</u>
- Jaros�aw Gaw�owski - <a href="http://www.festivalinfo.pl" target="_blank">http://www.festivalinfo.pl</a> (small bug fixes);
- <a href="http://eturystyka.org" target="_blank">http://eturystyka.org</a> - za hosting oraz wsparcie dla moich projekt�w;
- Bartek Kubera za t�umaczenie;

<u>5. Zobacz te�</u>
Polecam r�wnie� inny m�j komponent  - JAccelerator - zaawansowany cache, kt�rego zadaniem jest przy�pieszenie wy�wietlania strony oraz zmniejszenie obci��enia serwera:
<a href="http://extensions.joomla.org/component/option,com_mtree/task,viewlink/link_id,2326/Itemid,35/">JAccelerator - JED</a>
oraz:
<a href="http://fun.kubera.org/content/view/20/45/lang,iso-8859-2/">JAccelerator - Fun With Joomla!</a>

<u>6. Kontakt</u>
Radoslaw Kubera <a mailto:radek@kubera.org>radek@kubera.org</a>
<a href="http://fun.kubera.org" target="_blank">Fun With Joomla!</a>