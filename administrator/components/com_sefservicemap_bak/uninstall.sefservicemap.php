<?php
//*******************************************************
//* SEF Service Map Component
//* http://fun.kubera.org
//* (C) Radoslaw Kubera
//* license http://www.gnu.org/copyleft/gpl.html GNU/GPL
//*******************************************************

/** ensure this file is being included by a parent file */
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

function GetCompParams()
{
   global $database;
   $query = "SELECT * from #__sef_sm_settings";
   $database->setQuery($query);
   $pars = $database->loadObjectList();
       
   foreach ( $pars as $k) 
   {
     $txt[] = "$k->variable=$k->value";               
   }
   if (count($pars)!=0) $params = implode( "\n", $txt );
   $params = new mosParameters( $params);
   return $params;
}


function com_uninstall() 
{
  global $database;
  $params = GetCompParams();
  $keep_settings=intval($params->get('keep_settings','1'));
  if (!$keep_settings)
  {
    $database->setQuery("DROP TABLE #__sef_sm_menu");
    $database->query();
    $database->setQuery("DROP TABLE #__sef_sm_menus");
    $database->query();
    $database->setQuery("DROP TABLE #__sef_sm_settings");
    $database->query();
    echo "Settings removed.";
  }
  echo "SEF Service Map Component successfully uninstalled.";
}

?>