<?php
/**
* @version $Id: module.php 328 2005-10-02 15:39:51Z Jinx $
* @package Joomla
* @subpackage Installer
* @copyright Copyright (C) 2005 Open Source Matters. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );

// ensure user has access to this function
if ( !$acl->acl_check( 'administration', 'install', 'users', $my->usertype, $element . 's', 'all' ) ) {
	mosRedirect( 'index2.php', _NOT_AUTH );
}

require_once( $mainframe->getPath( 'installer_html', 'module' ) );

HTML_installer::showInstall( 'Install new Modules', $option, 'module', '', dirname(__FILE__) );
?>
<table class="content">
<?php
writableCell( 'media' );
writableCell( 'administrator/modules' );
writableCell( 'modules' );
?>
</table>
<?php
showInstalledModules( $option );

/**
* @param string The URL option
*/
function showInstalledModules( $_option ) {
	global $database, $mosConfig_absolute_path;

	$filter 		= mosGetParam( $_POST, 'filter', '' );
	$select[] 		= mosHTML::makeOption( '', 'All' );
	$select[] 		= mosHTML::makeOption( '0', 'Site Modules' );
	$select[] 		= mosHTML::makeOption( '1', 'Admin Modules' );
	$lists['filter'] = mosHTML::selectList( $select, 'filter', 'class="inputbox" size="1" onchange="document.adminForm.submit();"', 'value', 'text', $filter );
	if ( $filter == NULL ) {
		$and = '';
	} else if ( !$filter ) {
		$and = "\n AND client_id = 0";
	} else if ( $filter ) {
		$and = "\n AND client_id = 1";
	}

	$exclude = array('custom','mod_quickicon','mod_mosmsg','mod_toolbar','mod_pathway','mod_fullmenu','mod_online','mod_unread','mod_components','mod_banners','mod_sections','mod_archive','mod_latestnews','mod_rssfeed','mod_login','mod_mainmenu','mod_poll','mod_whosonline','mod_mostread','mod_newsflash','mod_related_items','mod_search','mod_random_image','mod_popular','mod_latest','mod_stats','mod_wrapper');
	
	$query = "SELECT 0 as id, module, client_id"
	. "\n FROM #__modules"
	. "\n WHERE module LIKE 'mod_%' AND iscore='0' AND client='0' AND module NOT IN ('".implode("','", $exclude)."')"
	. $and
	. "\n GROUP BY module, client_id"
	. "\n ORDER BY client_id, module"
	;
	$database->setQuery( $query );
	$rowsFront = $database->loadObjectList('module');	
	
	$query = "SELECT 0 as id, module, client_id"
	. "\n FROM #__modules"
	. "\n WHERE module LIKE 'mod_%' AND iscore='0' AND client='1' AND module NOT IN ('".implode("','", $exclude)."')"
	. $and
	. "\n GROUP BY module, client_id"
	. "\n ORDER BY client_id, module"
	;
	$database->setQuery( $query );
	$rowsAdmin = $database->loadObjectList('module');
	
	
	// list of directories
	$frontMods = mosReadDirectory( $mosConfig_absolute_path .'/modules' );	
	$adminMods = mosReadDirectory( $mosConfig_absolute_path .'/administrator/modules' );	
	$tmpRows = array();
	
	//Get front end modules xml files
	foreach($frontMods as $mod){
		
		$module = explode('.',$mod);
		if(isset($module[1]) && $module[1] == 'xml' && !in_array($module[0], $exclude)){
			$tmp = new stdClass();
			$tmp->id = 0;
			$tmp->client_id = 0;
			$tmp->module = $module[0];			
			$rowsFront[] = $tmp;
		}	
	}
	
	//Get front end modules xml files
	foreach($adminMods as $mod){
		
		$module = explode('.',$mod);
		if(isset($module[1]) && $module[1] == 'xml' && !in_array($module[0], $exclude)){
			$tmp = new stdClass();
			$tmp->id = 0;
			$tmp->client_id = 1;
			$tmp->module = $module[0];			
			$rowsAdmin[] = $tmp;
		}	
	}
	ksort($rowsFront);
	ksort($rowsAdmin);
	
	$rows = array_merge($rowsFront, $rowsAdmin);
		
	$n = count( $rows );
	for ($i = 0; $i < $n; $i++) {
		$row =& $rows[$i];

		// path to module directory
		if ($row->client_id == "1"){
			$moduleBaseDir	= mosPathName( mosPathName( $mosConfig_absolute_path ) . "administrator/modules" );
		} else {
			$moduleBaseDir	= mosPathName( mosPathName( $mosConfig_absolute_path ) . "modules" );
		}

		// xml file for module
		$xmlfile = $moduleBaseDir. "/" .$row->module .".xml";

		if (file_exists( $xmlfile )) {
			$xmlDoc = new DOMIT_Lite_Document();
			$xmlDoc->resolveErrors( true );
			if (!$xmlDoc->loadXML( $xmlfile, false, true )) {
				continue;
			}

			$root = &$xmlDoc->documentElement;

			if ($root->getTagName() != 'mosinstall') {
				continue;
			}
			if ($root->getAttribute( "type" ) != "module") {
				continue;
			}

			$element 			= &$root->getElementsByPath( 'creationDate', 1 );
			$row->creationdate 	= $element ? $element->getText() : '';

			$element 			= &$root->getElementsByPath( 'author', 1 );
			$row->author 		= $element ? $element->getText() : '';

			$element 			= &$root->getElementsByPath( 'copyright', 1 );
			$row->copyright 	= $element ? $element->getText() : '';

			$element 			= &$root->getElementsByPath( 'authorEmail', 1 );
			$row->authorEmail 	= $element ? $element->getText() : '';

			$element 			= &$root->getElementsByPath( 'authorUrl', 1 );
			$row->authorUrl 	= $element ? $element->getText() : '';

			$element 			= &$root->getElementsByPath( 'version', 1 );
			$row->version 		= $element ? $element->getText() : '';
		}
	}

	HTML_module::showInstalledModules( $rows, $_option, $xmlfile, $lists );
}
?>