<?php
/**
* @version $Id: admin.installer.html.php 5992 2006-12-13 00:18:18Z friesengeist $
* @package Joomla
* @subpackage Installer
* @copyright Copyright (C) 2005 Open Source Matters. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );

function writableCell( $folder ) {
	echo '<tr>';
	echo '<td class="item">' . $folder . '/</td>';
	echo '<td align="left">';
	echo is_writable( $GLOBALS['mosConfig_absolute_path'] . '/' . $folder ) ? '<b><font color="green">Writeable</font></b>' : '<b><font color="red">Unwriteable</font></b>' . '</td>';
	echo '</tr>';
}

/**
* @package Joomla
*/
class HTML_installer {
	
	function showInstall($title, $option, $element, $client = "", $p_startdir = "", $backLink="" ){
		
		?>
		<style type="text/css">		
		.content{
		margin: 0px 20px 20px;
		}
		</style>
		<table class="adminheading">
		<tr>
			<th class="install">
			<?php echo $title;?>
			</th>
			<td align="right" nowrap="nowrap">
			<?php echo $backLink;?>
			</td>
		</tr>
		</table>
		<table class="adminform installform">
		<tr>
			<th>
			Select Option
			</th>
		</tr>
		<tr>
		<td stlye='padding: 5px 0px'>
			<a class='install_link <?php echo $element == '' ? 'active' : '' ?>' href='index2.php?option=com_installer'>Install</a> | 
			<a class='install_link <?php echo $element == 'component' ? 'active' : '' ?>' href='index2.php?option=com_installer&element=component'>Components</a> | 
			<a class='install_link <?php echo $element == 'module' ? 'active' : '' ?>' href='index2.php?option=com_installer&element=module'>Modules</a> | 
			<a class='install_link <?php echo $element == 'mambot' ? 'active' : '' ?>' href='index2.php?option=com_installer&element=mambot'>Plugins</a> | 
			<a class='install_link <?php echo $element == 'language' ? 'active' : '' ?>'  href='index2.php?option=com_languages'>Languages</a> | 
			<a class='install_link <?php echo $element == 'template' && $client == '' ? 'active' : '' ?>'  href='index2.php?option=com_templates'>Site Templates</a> | 
			<a class='install_link <?php echo $element == 'template' && $client == 'admin' ? 'active' : '' ?>'  href='index2.php?option=com_templates&client=admin'>Admin Templates</a>
		</td>
		</tr>
		<tr>
			<th>
			&nbsp;
			</th>
		</tr>		
		</table>
		<?php
		
	}

	function showInstallForm( $title, $option, $element, $client = "", $p_startdir = "", $backLink="" ) {
		if (!defined( '_INSTALL_3PD_WARN' )) {
			define( '_INSTALL_3PD_WARN', 'Warning: Installing 3rd party extensions may compromise your server\'s security. Upgrading your installation will not update your 3rd party extensions.<br />For more information on keeping your site secure, please see the <a href="http://forum.joomla.org/index.php/board,267.0.html" target="_blank" style="color: blue; text-decoration: underline;">Security Forum</a>.' );
		}
		?>
		<script language="javascript" type="text/javascript">
		function submitbutton3(pressbutton) {
			var form = document.adminForm_dir;

			// do field validation
			if (form.userfile.value == ""){
				alert( "Please select a directory" );
			} else {
				form.submit();
			}
		}
		</script>
		<form enctype="multipart/form-data" action="index2.php" method="post" name="filename">
		<table class="adminheading">
		<tr>
			<th class="install">
			<?php echo $title;?>
			</th>
			<td align="right" nowrap="nowrap">
			<?php echo $backLink;?>
			</td>
		</tr>
		</table>

		<table class="adminform">
		<tr>
			<th>
			Upload Package File
			</th>
		</tr>
		<tr>
			<td align="left">
			Package File:
			<input class="text_area" name="userfile" type="file" size="70"/>
			<input class="button" type="submit" value="Upload File &amp; Install" />
			</td>
		</tr>
		</table>

		<input type="hidden" name="task" value="uploadfile"/>
		<input type="hidden" name="option" value="<?php echo $option;?>"/>
		<input type="hidden" name="element" value="<?php echo $element;?>"/>
		<input type="hidden" name="client" value="<?php echo $client;?>"/>
		<input type="hidden" name="<?php echo josSpoofValue(); ?>" value="1" />
		</form>
		<br />

		<form enctype="multipart/form-data" action="index2.php" method="post" name="adminForm_dir">
		<table class="adminform">
		<tr>
			<th>
			Install from directory
			</th>
		</tr>
		<tr>
			<td align="left">
			Install directory:&nbsp;
			<input type="text" name="userfile" class="text_area" size="65" value="<?php echo $p_startdir; ?>"/>&nbsp;
			<input type="button" class="button" value="Install" onclick="submitbutton3()" />
			</td>
		</tr>
		</table>

		<input type="hidden" name="task" value="installfromdir" />
		<input type="hidden" name="option" value="<?php echo $option;?>"/>
		<input type="hidden" name="element" value="<?php echo $element;?>"/>
		<input type="hidden" name="client" value="<?php echo $client;?>"/>
		<input type="hidden" name="<?php echo josSpoofValue(); ?>" value="1" />
		</form>
		<?php
	}

	/**
	* @param string
	* @param string
	* @param string
	* @param string
	*/
	function showInstallMessage( $message, $title, $url ) {
		global $PHP_SELF;
		?>
		<table class="adminheading">
		<tr>
			<th class="install">
			<?php echo $title; ?>
			</th>
		</tr>
		</table>

		<table class="adminform">
		<tr>
			<td align="left">
			<strong><?php echo $message; ?></strong>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center">
			[&nbsp;<a href="<?php echo $url;?>" style="font-size: 16px; font-weight: bold">Continue ...</a>&nbsp;]
			</td>
		</tr>
		</table>
		<?php
	}
	
	function showWarning(){
		
		if(mosGetParam($_REQUEST,'warning',0)){?>
		<div style="width: 95%; background-image: url(../includes/js/ThemeOffice/warning.png);" class='message'>
			<?php echo _INSTALL_3PD_WARN; ?>
		</div>
		<?php
		}
	}
}
?>