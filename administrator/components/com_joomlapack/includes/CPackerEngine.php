<?php
/**
* @package		JoomlaPack
* @subpackage	CUBE
* @copyright	Copyright (C) 2006-2008 JoomlaPack Developers. All rights reserved.
* @version		$Id$
* @license 		http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
* JoomlaPack is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
**/

// ensure this file is being included by a parent file - Joomla! 1.0.x and 1.5 compatible
(defined( '_VALID_MOS' ) || defined('_JEXEC')) or die( 'Direct Access to this location is not allowed.' );

// Archiver engines have to be referenced here in order for the wakeup to function
CJVAbstract::import('CZIPCreator');
CJVAbstract::import('CJPACreator');

/**
 * Packing engine. Takes care of putting gathered files (the file list) into
 * an archive.
 */
class CPackerEngine extends CJPEngineParts {
	/**
     * Full pathname to the archive file
     * @access private
     * @var string
     */
	var $_archiveFile;

	/**
	* Maximum fragment number
	* @access private
	* @var integer
	*/
	var $_maxFragment;

	/**
	* Current fragment number
	* @access private
	* @var integer
	*/
	var $_currentFragment;

	/**
	* Active file list descriptor
	* @access private
	* @var array
	*/
	var $_fileListDescriptor;

	/**
	* Total size of file lists
	* @access private
	* @var integer
	*/
	var $_totalBytes;

	/**
	* Total size processed so far
	* @access private
	* @var integer
	*/
	var $_currentBytes;
	
	/**
	 * The stored archive engine object
	 *
	 * @var object
	 */
	var $_archive;

	/**
	 * Public constructor of the class
	 *
	 * @return CPackerEngine
	 */
	function CPackerEngine(){
		$this->_DomainName = "Packing";
		CJPLogger::WriteLog(_JP_LOG_DEBUG, "CPackerEngine :: new instance");
	}
	
	/**
	 * Implements the _prepare() abstract method
	 *
	 */
	function _prepare()
	{
		$database = CJVAbstract::getDatabase();
		$configuration =& CConfiguration::getInstance();

		// Initialize internal variables
		$this->_currentFragment = 0;
		$this->_totalBytes = 0;
		$this->_currentBytes = 0;

		// Get maximum number of fragments
		$sql = "SELECT * FROM #__jp_packvars WHERE `key` like 'fragment%'";
		$database->setQuery( $sql );
		$database->query();
		$this->_maxFragment = $database->getNumRows();
		
		// Get total size of files to backup
		for( $i=1; $i <= $this->_maxFragment; $i++ ) {
			$serialized = CJPTables::ReadVar("fragment$i");
			$descriptor = unserialize($serialized);
			$this->_totalBytes += $descriptor['size'];
			unset($descriptor);
		}
		
		// Instanciate archiver
		$this->_createArchiverObject();

		$this->_isPrepared = true;

		CJPLogger::WriteLog(_JP_LOG_DEBUG, "CPackerEngine :: prepared");
	}
	
	/**
	 * Implements the _run() abstract method
	 */
	function _run()
	{
		// Well, this shouldn't really happen
		if ($this->_hasRan) {
			$this->_isRunning = false;
			return;
		}
		
		// Try to pack next fragment
		$this->_currentFragment++;
		if ($this->_currentFragment > $this->_maxFragment) {
			// We have just finished, as we ended up on one fragment past the end. Glue archive and return.
			$this->_isRunning = false;
			$this->_hasRan = true;
			$this->_fileListDescriptor['files'] = null;
		} else {
			CJPLogger::WriteLog(_JP_LOG_INFO, "Archiving fragment #" . $this->_currentFragment);
			$this->_isRunning = true;
			$this->_hasRan = false;
			// Load the current fragment
			$this->_fileListDescriptor = unserialize(CJPTables::ReadVar("fragment" . $this->_currentFragment));
			// Get paths to add / remove
			$pathsAddRemove = $this->_getPaths( $this->_fileListDescriptor['type'] );
			
			// BEGIN FIX 1.2 Stable -- The databases.ini is never written on disk
			switch($this->_fileListDescriptor['type'])
			{
				case 'virtsql':
					// Get the contents off the disk
					CJPLogger::WriteLog(_JP_LOG_DEBUG, "Adding databases.ini to archive");
					$databasesINI = CJPTables::ReadVar('databases_ini');
					$this->_archive->addVirtualFile( 'databases.ini', $pathsAddRemove['add'], $databasesINI );
					CJPLogger::WriteLog(_JP_LOG_DEBUG, "Finished adding databases.ini to archive");
					break;
					
				default:
					// Add files to archive
					CJPLogger::WriteLog(_JP_LOG_DEBUG, "Adding files to archive");
					$this->_archive->addFileList( $this->_fileListDescriptor['files'], $pathsAddRemove['remove'], $pathsAddRemove['add'] );
					CJPLogger::WriteLog(_JP_LOG_DEBUG, "Finished adding files to archive");
					break;
			}
			// END FIX 1.2 Stable
		}
	}
	
	/**
	 * Implements the _finalize() abstract method
	 *
	 */
	function _finalize()
	{
		CJPLogger::WriteLog(_JP_LOG_INFO, "Finalizing archive");
		$this->_archive->finalizeArchive();
		CJPLogger::WriteLog(_JP_LOG_DEBUG, "Archive is finalized");
		$this->_isFinished = true;
	}

	/**
	 * Creates the proper archiver object based on user selection
	 */
	function _createArchiverObject()
	{
		// Expand template name
		$configuration =& CConfiguration::getInstance(); 
		switch( $configuration->CompressionMethod )
		{
			case 'jpa':
				$extension = '.jpa';
				break;

			case 'zip':
			default:
				$extension = '.zip';
				break;
		}
		$this->_archiveFile = CJVAbstract::getExpandedTarName( $extension );
		CJPLogger::WriteLog(_JP_LOG_DEBUG, "Expanded archive file name: " . $this->_archiveFile);
		
		switch( $configuration->CompressionMethod )
		{
			case 'jpa':
				$this->_archive = new CJPACreator( $this->_archiveFile );
				break;

			case 'zip':
				$this->_archive = new CZIPCreator( $this->_archiveFile, $configuration->TempDirectory, false );
				break;
		}
	}
	
	/**
	* Returns the path to trim and the path to add to the fragment's files
	*/
	function _getPaths( $fragmentType ){
		$configuration =& CConfiguration::getInstance();
		
		$retArray = array();
		switch($fragmentType){
			case "site":
				$retArray['remove'] = CJVAbstract::TranslateWinPath( JPSiteRoot );
				$retArray['add'] = "";
				break;
			case "installation":
				$filePath = CJVAbstract::TranslateWinPath( $configuration->TempDirectory . "/installation/" );
				$retArray['remove'] = $filePath;
				$retArray['add'] = "installation";
				break;
			case "sql":
				$retArray['remove'] = CJVAbstract::TranslateWinPath( $configuration->TempDirectory );
				$retArray['add'] = "installation/sql";
				break;
			case "virtsql":
				// Virtual SQL -- Used to include the databases.ini in the backup set without writting it on disk
				$retArray['remove'] = '';
				$retArray['add'] = "installation/sql";
				break;
			// case "external":
			// TODO - Handle forcibly included directories (later versions will do that...)
		} // switch

		CJPLogger::WriteLog(_JP_LOG_DEBUG, "Fragment type is '$fragmentType'");
		CJPLogger::WriteLog(_JP_LOG_DEBUG, "  path to remove : " . $retArray['remove']);
		CJPLogger::WriteLog(_JP_LOG_DEBUG, "  path to add    : " . $retArray['add']);
		return $retArray;
	}
}
?>