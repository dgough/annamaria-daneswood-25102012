<?php
/**
* @package		JoomlaPack
* @subpackage	BaseClasses
* @copyright	Copyright (C) 2006-2008 JoomlaPack Developers. All rights reserved.
* @version		$Id$
* @license 		http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
* JoomlaPack is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
**/

// ensure this file is being included by a parent file - Joomla! 1.0.x and 1.5 compatible
(defined( '_VALID_MOS' ) || defined('_JEXEC')) or die( 'Direct Access to this location is not allowed.' );

/**
 * The installer deployment step engine. Extracts the selected installer archive to a
 * temporary directory and adds its files to the global file list.
 *
 */
class CInstallerDeploymentEngine extends CJPEngineParts {

	/**
     * Where we should put our copy of the installer
     * @access private
     * @var string
     */
	var $_targetDirectory;

	/**
     * The path to the compressed installer image
     * @access private
     * @var string
     */
	var $_tarName;

	/**
     * The list of installer files
     * @access private
     * @var array
     */
	var $_fileList;

	/**
	 * Public constructor
	 *
	 * @return CInstallerDeploymentEngine
	 */
	function CInstallerDeploymentEngine()
	{
		$this->_DomainName = "InstallerDeployment";
		CJPLogger::WriteLog(_JP_LOG_DEBUG, "CInstallerDeploymentEngine :: new instance");
	}
	
	/**
	 * Implements the _prepare method of the abstract class
	 */
	function _prepare()
	{
		CJPLogger::WriteLog(_JP_LOG_DEBUG, "CInstallerDeploymentEngine :: Begin _prepare()");
		$configuration = CConfiguration::getInstance();
		
		// Put default values from configuration
		$this->_targetDirectory = $configuration->TempDirectory;
		$this->_tarName = JPComponentRoot . "/installers/" . $configuration->AltInstaller->Package;
		CJPLogger::WriteLog(_JP_LOG_DEBUG, "Temporary Directory: {$this->_targetDirectory}");
		CJPLogger::WriteLog(_JP_LOG_DEBUG, "Installer Package: {$this->_tarName}");

		// Create the file list of the installer files
		CJPLogger::WriteLog(_JP_LOG_DEBUG, "CInstallerDeploymentEngine :: preparing file list");
		$filePath = $this->_targetDirectory . "/installation/";
		// Fix 1.2a3: Replaced slash handling with TranslateWinPath
		$filePath = CJVAbstract::TranslateWinPath($filePath);
		CJPLogger::WriteLog(_JP_LOG_DEBUG, "Installation Temp Folder: $filePath");

		$this->_fileList = array();
		$AltInstaller = $configuration->AltInstaller;
		foreach( $AltInstaller->fileList as $fileName ) {
			$this->_fileList[] = $filePath . $fileName;
		}

		CJPLogger::WriteLog(_JP_LOG_DEBUG, "CInstallerDeploymentEngine :: prepared");
		
		$this->_isPrepared = true;
	}

	/**
	 * Implements the _run method of the abstract class
	 */
	function _run()
	{
		if ($this->_hasRan) {
			CJPLogger::WriteLog(_JP_LOG_DEBUG, "CInstallerDeploymentEngine :: Already finished");
			$this->_isRunning = false;
			$this->_Step = "";
			$this->_Substep = "";
		} else {
			$this->_isRunning = true;
			$database = CJVAbstract::getDatabase();

			CJPLogger::WriteLog(_JP_LOG_DEBUG, "Extracting installer");

			// Include TAR file suport
			if (!defined('_JEXEC')) {
				CJPLogger::WriteLog(_JP_LOG_DEBUG, "Including Tar file support (1.0.x)");
				require_once( JPSiteRoot . "/includes/PEAR/PEAR.php" );
				require_once( JPSiteRoot . "/includes/Archive/Tar.php" );
			} else {
				CJPLogger::WriteLog(_JP_LOG_DEBUG, "Including Tar file support (1.5.x)");
				jimport('pear.archive_tar.Archive_Tar');
				//require_once( JPATH_ROOT.DS.'libraries'.DS.'pear'.DS.'archive_tar'.DS.'Archive_Tar.php' );
			}
			
			// Remove any leftover files
			CJPLogger::WriteLog(_JP_LOG_DEBUG, "Removing any old installation folders");
			$cube =& CCUBE::getInstance();
			$cube->_unlinkRecursive( $this->_targetDirectory . "/installation" );

			// Extract the installer image
			CJPLogger::WriteLog(_JP_LOG_DEBUG, "Extracting installer");
			$tar = new Archive_Tar( $this->_tarName, 'gz' );
			$tar->extract( $this->_targetDirectory );

			CJPLogger::WriteLog(_JP_LOG_DEBUG, "Creating fragment for installer");

			// Get the current node number in file list db table and start adding
			// to the next fragment
			$sql = "SELECT COUNT(*) FROM #__jp_packvars WHERE `key` like 'fragment%'";
			$database->setQuery( $sql );
			$database->query();
			$currentNode = $database->loadResult() + 1;

			// Add the installer files to the file list stored in db; all files
			// are added as 'Branch 2' entries, indicating a different subpath
			// from the rest of the filelist has to be removed from them.
			$fileList = array();
			$fragmentSize = 0;
			foreach($this->_fileList as $fileName){
				$filesize = filesize( $fileName );
				$fragmentSize += $filesize;
				$fileList[] = $fileName;
			}
			$fragmentDescriptor = array();
			$fragmentDescriptor['type'] = "installation"; // Other possible values are 'site', 'sql', 'external'
			$fragmentDescriptor['size'] = $fragmentSize;
			$fragmentDescriptor['files'] = $fileList;

			$serializedDescriptor = serialize($fragmentDescriptor);
			unset($fragmentDescriptor);
			unset($fileList);
			
			CJPTables::WriteVar( "fragment$currentNode", $serializedDescriptor );

			unset($serializedDescriptor);

			// Indicate we have finished
			$this->_hasRan = true;
			$this->_isRunning = false;

			CJPLogger::WriteLog(_JP_LOG_DEBUG, "Done creating fragment for installer");
		}
	}
	
	/**
	 * Implements the _finalize method of the abstract class
	 */
	function _finalize()
	{
		// Do nothing, really
		$this->_isFinished = true;
	}
}

?>