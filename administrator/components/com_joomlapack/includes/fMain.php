<?php
/**
* @package		JoomlaPack
* @copyright	Copyright (C) 2006-2008 JoomlaPack Developers. All rights reserved.
* @version		$Id$
* @license 		http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
* JoomlaPack is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
**/

// ensure this file is being included by a parent file - Joomla! 1.0.x and 1.5 compatible
(defined( '_VALID_MOS' ) || defined('_JEXEC')) or die( 'Direct Access to this location is not allowed.' );

class CMainPage
{
	/**
	 * Is the output directory writable?
	 *
	 * @var bool
	 */
	var $_isOutWritable;
	
	/**
	 * Is the temporary directory writable?
	 *
	 * @var bool
	 */
	var $_isTempWritable;
	
	/**
	 * Is the application ready to backup?
	 *
	 * @var bool
	 */
	var $_isStatusGood;
	
	/**
	 * Is the user using the default temp directory?
	 *
	 * @var bool
	 */
	var $_defaultDirs;
	
	/**
	 * Implements the Singleton pattern
	 *
	 * @return CMainPage
	 * @static
	 */
	function &getInstance()
	{
		static $instance;
		
		if( !is_object($instance) )
		{
			$instance = new CMainPage();
		}
		
		return $instance;
	}
	
	/**
	 * Constructor
	 *
	 * @return CMainPage
	 */
	function CMainPage()
	{
		$config =& CConfiguration::getInstance();
		$this->_isOutWritable	= $config->isOutputWriteable();	
		$this->_isTempWritable	= $config->isTempWriteable();
		$this->_isStatusGood	= $this->_isOutWritable && $this->_isTempWritable;
		$this->_defaultDirs		= (realpath($config->OutputDirectory) == realpath(JPComponentRoot . '/temp')) ||
								  realpath($config->TempDirectory) == realpath(JPComponentRoot . '/temp');
	}
	
	/**
	 * Displays the HTML for this page, directly outputting it to the browser (due to the use of tabs)
	 */
	function echoHTML()
	{
		// Load the translations
		$lang =& CLangManager::getInstance();
		
		// Make the Control Panel HTML
		$cpanel = new CJPControlPanelHTML();
		$cpanel->addItem( CJVAbstract::JPLink('config'), 'config', $lang->get('cpanel', 'config') );
		$cpanel->addItem( CJVAbstract::JPLink('def'), 'def', $lang->get('cpanel', 'def') );
		$cpanel->addItem( CJVAbstract::JPLink('sff'), 'sff', $lang->get('cpanel', 'sff') );
		$cpanel->addItem( CJVAbstract::JPLink('dbef'), 'dbef', $lang->get('cpanel', 'dbef') );
		$cpanel->addItem( CJVAbstract::JPLink('multidb'), 'multidb', $lang->get('cpanel', 'multidb') );
		$cpanel->addItem( CJVAbstract::JPLink('pack'), 'backup', $lang->get('cpanel', 'pack') );
		$cpanel->addItem( CJVAbstract::JPLink('backupadmin'), 'bufa', $lang->get('cpanel', 'buadmin') );
		$cpanel->addItem( CJVAbstract::JPLink('log'), 'log', $lang->get('cpanel', 'log') );
		$cpanel->addItem( CJVAbstract::JPLink('unlock'), 'reset', $lang->get('cpanel', 'unlock') );
		$cpanelHTML = $cpanel->getHTML();
		
		// Create the admin form
		echo CJPHTML::getAdminHeadingHTML( $lang->get('cpanel','home') );
		echo <<<ENDSNIPPET
		<table class="adminform">
			<tr>
				<td width="55%" valign="top">
					$cpanelHTML
				</td>
				<td width="45%" valign="top">
ENDSNIPPET;
		$this->_getTabsHTML();
		echo <<<ENDSNIPPET
				</td>
			</tr>
		</table>		
ENDSNIPPET;

	}
	
	/**
	 * Renders the JoomlaPack's overview tabbed pane
	 */
	function _getTabsHTML()
	{
		$lang =& CLangManager::getInstance();
		
		if( defined('_JEXEC') )
		{
			jimport('joomla.html.pane');
			$tabs  =& JPane::getInstance('sliders');
			echo $tabs->startPane('jpstatuspane');
		} else {
			$tabs = new mosTabs(1);
			$tabs->startPane('jpstatuspane');
		}
		
		if( defined('_JEXEC') )
		{
			echo $tabs->startPanel( $lang->get('main','overview'), 'jpstatusov' );
		} else {
			$tabs->startTab( $lang->get('main','overview'), 'jpstatusov' );		
		}

		echo '<p class="sanityCheck">' . $lang->get('main','status') . ': ';
		echo CJPHTML::colorizeWriteStatus( $this->_isStatusGood, true, 'appgood', 'appnotgood', 'main' ) . '</p>';
		
		// --- START --- Detect use of default temp directory
		if($this->_defaultDirs)
		{
			echo '<p class="sanityCheck">' . $lang->get('main','warning') . '<br/>';
			echo '<a href="http://www.joomlapack.net/option=com_content&id=75.html">'.$lang->get('main','warning_info') . '</a></p>';
		}
		// --- END ---

		if( defined('_JEXEC') )
		{
			echo $tabs->endPanel();
			echo $tabs->startPanel( $lang->get('main','details'), 'jpstatusdet' );
		} else {
			$tabs->endTab();		
			$tabs->startTab( $lang->get('main','details'), 'jpstatusdet' );
		}
		
		$item		= $lang->get('main', 'item');
		$status		= $lang->get('main', 'status');
		$tempDir	= $lang->get('common', 'tempdir');
		$tempStatus	= CJPHTML::colorizeWriteStatus( $this->_isTempWritable, true );
		$outDir		= $lang->get('common', 'outdir');
		$outStatus	= CJPHTML::colorizeWriteStatus( $this->_isOutWritable, true );
		$verCheckTitle	= $lang->get('common', 'version_check');
		$verHTML	= $this->checkAppStatusV(1);
		
		echo <<<ENDSNIPPET
			<table align="center" border="1" cellspacing="0" cellpadding="5" class="adminlist">
				<thead>
					<tr>
						<th class="title">$item</th>
						<th>$status</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>$tempDir</td>
						<td>$tempStatus</td>
					</tr>
					<tr>
						<td>$outDir</td>
						<td>$outStatus</td>
					</tr>
					<tr>
						<td>$verCheckTitle</td>
						<td>$verHTML</td>
					</tr>
				</tbody>
			</table>
ENDSNIPPET;
		if( defined('_JEXEC') )
		{
			echo $tabs->endPanel();
			echo $tabs->endPane();
		} else {
			$tabs->endTab();
			$tabs->endPane();		
		}
		
	}

	function checkAppStatusV ($app_status) {
		if ($app_status == 1) {
		$vcheck = urlencode(base64_encode(_JP_VERSION));
		//echo $vcheck;  //debug
		return "<script type=\"text/javascript\" src=\"http://www.joomlapack.net/version.php?vid=2&verx=$vcheck\"></script>";
		}
	}
		
}

/* Get last run date.
      $path = "docs/";
      // show the most recent file
      echo "Most recent file is: ".getNewestFN($path);
      // Returns the name of the newest file
      // (My_name YYYY-MM-DD HHMMSS.inf)
      function getNewestFN ($path) {
        // store all .inf names in array
        $p = opendir($path);
        while (false !== ($file = readdir($p))) {
          if (strstr($file,".zip"))
            $list[]=date("YmdHis ", filemtime($path.$file)).$path.$file;
        }
        // sort array descending
        rsort($list);
        // return newest file name   
        return $list[0];
      }
*/

?>
