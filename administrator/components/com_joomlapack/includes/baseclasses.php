<?php
/**
* @package		JoomlaPack
* @subpackage	BaseClasses
* @copyright	Copyright (C) 2006-2008 JoomlaPack Developers. All rights reserved.
* @version		$Id$
* @license 		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @since		1.1.2b
*
* JoomlaPack is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
**/

// Ensure this file is being included by a parent file - Joomla! 1.0.x and 1.5 compatible
(defined( '_VALID_MOS' ) || defined('_JEXEC')) or die( 'Direct Access to this location is not allowed.' );

/**
 * Base class for CUBE components (a.k.a. Engine Parts)
 * @abstract 
 */
class CJPEngineParts
{
	/**
	 * Indicates whether this part has finished its initialisation cycle
	 *
	 * @var boolean
	 * @access protected
	 */
	var $_isPrepared = false;
		
	/**
	 * Indicates whether this part has more work to do (it's in running state)
	 *
	 * @var boolean
	 * @access protected
	 */
	var $_isRunning = false;
	
	/**
	 * Indicates whether this part has finished its finalization cycle
	 *
	 * @var boolean
	 * @access protected
	 */
	var $_isFinished = false;

	/**
	 * Indicates whether this part has finished its run cycle
	 *
	 * @var boolean
	 * @access protected
	 */
	var $_hasRan = false;
	
	/**
	 * Indicates whether this part has prematurely stopped due to an error
	 *
	 * @var boolean
	 * @access protected
	 */
	var $_isError = false;
	
	/**
	 * Stores the last error message for this part
	 *
	 * @var boolean
	 * @access protected
	 */
	var $_errorMessage = "";
	
	/**
	 * The name of the engine part (a.k.a. Domain), used in return table
	 * generation.
	 *
	 * @var string
	 * @access protected
	 */
	var $_DomainName = "";
	
	/**
	 * The step this engine part is in. Used verbatim in return table and
	 * should be set by the code in the _run() method.
	 *
	 * @var string
	 * @access protected
	 */
	var $_Step = "";
	
	/**
	 * A more detailed description of the step this engine part is in. Used
	 * verbatim in return table and should be set by the code in the _run()
	 * method.
	 *
	 * @var string
	 * @access protected
	 */
	var $_Substep = "";
	
	/**
	 * Any configuration variables, in the form of an array. 
	 *
	 * @var array
	 * @access protected
	 */
	var $_parametersArray = array();
		
	/**
	 * Runs the preparation for this part. Should set _isPrepared
	 * to true
	 * @access protected
	 * @abstract 
	 */
	function _prepare()
	{
	}
	
	/**
	 * Runs the finalisation process for this part. Should set
	 * _isFinished to true.
	 * @access protected
	 * @abstract
	 */
	function _finalize()
	{	
	}
	
	/**
	 * Runs the main functionality loop for this part. Upon calling,
	 * should set the _isRunning to true. When it finished, should set
	 * the _hasRan to true. If an error is encountered, _isError should
	 * be set to true.
	 * @access protected
	 * @abstract
	 */
	function _run()
	{
		
	}
	
	/**
	 * The public interface to an engine part. This method takes care for
	 * calling the correct method in order to perform the initialisation -
	 * run - finalisation cycle of operation and return a proper CUBE
	 * reponse array.
	 * @return array A CUBE Return Array
	 * @access protected
	 * @final
	 */
	function tick()
	{
		// Call the right action method, depending on engine part state
		switch( $this->_getState() )
		{
			case "init":
				$this->_prepare();
				break;
			case "prepared":
				$this->_run();
				break;
			case "running":
				$this->_run();
				break;
			case "postrun":
				$this->_finalize();
				break;
		}
		
		// Send a Return Table back to the caller
		return $this->_makeReturnTable();
	}
	
	/**
	 * Sends any kind of setup information to the engine part. Using this,
	 * we avoid passing parameters to the constructor of the class. These
	 * parameters should be passed as an indexed array and should be taken
	 * into account during the preparation process only. This function will
	 * set the error flag if it's called after the engine part is prepared.
	 *
	 * @param array $parametersArray The parameters to be passed to the
	 * engine part.
	 * @access public
	 * @final 
	 */
	function setup( $parametersArray )
	{
		if( $this->_isPrepared )
		{
			$this->_isError = true;
			$this->_errorMessage = "Can't modify configuration after the preparation of " . $this->_DomainName;
		}
		else
		{
			$this->_parametersArray = $parametersArray;
		}
	}
	
	/**
	 * Returns the state of this engine part.
	 * 
	 * @return string The state of this engine part. It can be one of
	 * error, init, prepared, running, postrun, finished.
	 * @access protected
	 * @final
	 */
	function _getState()
	{
		if( $this->_isError )
		{
			return "error";		
		}
		
		if( !($this->_isPrepared) )
		{
			return "init";
		}
		
		if( !($this->_isFinished) && !($this->_isRunning) && !( $this->_hasRan ) && ($this->_isPrepared) )
		{
			return "prepared";
		}
				
		if ( !($this->_isFinished) && $this->_isRunning && !( $this->_hasRan ) )
		{
			return "running";		
		}

		if ( !($this->_isFinished) && !($this->_isRunning) && $this->_hasRan )
		{
			return "postrun";		
		}
		
		if ( $this->_isFinished )
		{
			return "finished";
		}		
	}
	
	/**
	 * Constructs a CUBE Return Table based on the engine part's state.
	 *
	 * @final
	 * @return array The CUBE Return Array for the current state
	 */
	function _makeReturnTable()
	{
		return array(
			'HasRun'	=> (!($this->_isFinished)),
			'Domain'	=> $this->_DomainName,
			'Step'		=> $this->_Step,
			'Substep'	=> $this->_Substep,
			'Error'		=> $this->_errorMessage
		);
	}
}

/**
 * Represent a class which returns a filter, inclusion or exclusion, for
 * any domain required (be it directories, database or mixed)
 * @abstract
 */
class CJPFilterAbstract
{
	/**
	 * A simple array of the folder names to exclude (absolute paths)
	 *
	 * @var array
	 * @access protected
	 */
	var $_folderFilters;
	
	/**
	 * A simple array of single files to be excluded (absolute paths)
	 * 
	 * @var array
	 * @access protected
	 */
	var $_singleFileFilters;
	
	/**
	 * A simple array of database table names to exclude
	 *
	 * @var array
	 * @access protected
	 */
	var $_databaseFilters;
	
	/**
	 * A simple array of folders to forcibly include in the backup (absolute paths)
	 *
	 * @var array
	 * @access protected
	 */
	var $_includeFolderFilters;

	/**
	 * Initializes the filters arrays, e.g. load settings of the database or a file
	 * @abstract
	 */
	function init()
	{
		
	}
	
	/**
	 * Gets the filters of the relevant category
	 *
	 * @param string $predicate It's "singlefile", "folder", "includefolder" or
	 * 				 "database", depending on the filter type you want to retreive
	 * @return array
	 * @final
	 */
	function getFilters( $predicate )
	{
		switch( $predicate )
		{
			case "singlefile": // Single File Filters
				return $this->_singleFileFilters;
				break;
			case "folder": // Directory Exclusion Filters
				return $this->_folderFilters;
				break;
			case "database": // Database Table Filters
				return $this->_databaseFilters;
				break;
			case "includefolder": // (reserved for future use)
				return $this->_includeFolderFilters;
				break;
			default:
				return array();
				break;
		}
	}
}

/**
 * Abstract class all archiver engines must implement
 * 
 * @abstract 
 */
class CJPArchiverEngineAbstract
{
	/**
	 * Adds a list of files into the archive, removing $removePath from the
	 * file names and adding $addPath to them.
	 *
	 * @param array $fileList A simple string array of filepaths to include
	 * @param string $removePath Paths to remove from the filepaths
	 * @param string $addPath Paths to add in front of the filepaths
	 * @abstract
	 */
	function addFileList( &$fileList, $removePath, $addPath )
	{
		
	}
	
	/**
	 * Makes whatever finalization is needed for the archive to be considered
	 * complete and usefull
	 * @abstract
	 */
	function finalizeArchive()
	{
		
	}
}
?>