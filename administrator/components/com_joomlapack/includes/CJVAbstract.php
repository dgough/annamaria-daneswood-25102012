<?php
/**
* @package		JoomlaPack
* @subpackage 	BaseClasses
* @copyright	Copyright (C) 2006-2008 JoomlaPack Developers. All rights reserved.
* @version		$Id$
* @license 		http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
* JoomlaPack is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* 
* Joomla! Version Abstraction Layer & JoomlaPack HTML Rendering
* 
* The purpose of this file is to provide static "black-box" methods to
* abstract the source-level differences between Joomla! 1.0.x and 1.5.x,
* as well as the common HTML rendering mechanisms which drive the GUI.
**/

// Ensure this file is being included by a parent file - Joomla! 1.0.x and 1.5 compatible
(defined( '_VALID_MOS' ) || defined('_JEXEC')) or die( 'Direct Access to this location is not allowed.' );

/**
 * Provides static methods to abstract source-level differences between
 * Joomla! 1.0.x (built upon the Mambo codebase and coding conventions)
 * and Joomla! 1.5.x (built upon Joomla! Framework 1.5.x).
 *
 */
class CJVAbstract
{
	
	/**
	 * Discourage creating objects from this class
	 *
	 * @return CJVAbstract
	 */
	function CJVAbstract()
	{
		die('CJVAbstract does not support object creation.');
	}
	
	/**
	 * Gets a parameter value from the $_REQUEST object
	 *
	 * @param string $paramName The parameter name
	 * @param string $defaultValue The default value (null if not specified)
	 * @return mixed The parameter value
	 */
	function getParam( $paramName, $defaultValue = null )
	{
		if( !defined('_JEXEC') ) {
			return mosGetParam( $_REQUEST, $paramName, $defaultValue );
		} else {
			return JRequest::getVar($paramName, $defaultValue);
		}
	}
	
	/**
	 * Returns the site's base URI
	 *
	 * @return string The site's URI, e.g. http://www.example.com
	 */
	function SiteURI()
	{
		$port = ( $_SERVER['SERVER_PORT'] == 80 ) ? '' : ":".$_SERVER['SERVER_PORT'];
		$root = $_SERVER['SERVER_NAME'] . $port . $_SERVER['PHP_SELF'];
		$root = str_replace('/administrator/', '/', $root);
		$upto = strpos( $root, "/index" );
		$root = substr( $root, 0, $upto );
		
		// Fix 1.2.b1 -- Detect and use HTTPS protocol is the page was called as https://....
		$https = !(empty($_SERVER['HTTPS']) || ($_SERVER['HTTPS'] == 'off'));
		$protocol = $https ? 'https://' : 'http://';
		
		return $protocol.$root;		
	}
	
	/**
	 * Returns a URI to a JoomlaPack function, defined by act and task
	 *
	 * @param string $act The JP action to perform 
	 * @param string $task The JP task (if any) associated to the action to perform
	 * @param bool $nohtml Set to true in order to suppress Joomla!'s regular HTML output
	 * @param string $miscOptions Any options to append to the URI
	 * 
	 * @return string
	 */
	function JPLink( $act, $task = "", $nohtml = false, $miscOptions = "" )
	{
		$link = CJVAbstract::SiteURI() . '/administrator/index2.php?';
		$link .= "option=" . CJVAbstract::getParam('option', 'com_joomlapack');
		$link .= ($act == "") ? "" : "&act=$act";
		$link .= ($task == "") ? "" : "&task=$task";
		$link .= $nohtml ? "&no_html=1" : "";
		$link .= ($miscOptions == "") ? "" : "&$miscOptions";
		
		return $link;
	}
	
	/**
	 * Makes a Windows path more UNIX-like, by turning backslashes to forward slashes
	 *
	 * @param string $p_path The path to transform
	 * @return string
	 */
	function TranslateWinPath( $p_path )
    {
		if (stristr(php_uname(), 'windows')){
			// Change potential windows directory separator
			if ((strpos($p_path, '\\') > 0) || (substr($p_path, 0, 1) == '\\')){
				$p_path = strtr($p_path, '\\', '/');
			}
		}
		return $p_path;
	}
	
	/**
	 * Returns Joomla!'s database object
	 *
	 * @return JDatabase
	 */
	function getDatabase()
	{
		if( defined('_JEXEC') )
		{
			$database 	= & JFactory::getDBO();
		} else {
			global $database;
		}
		return $database;
	}
	
	/**
	 * Imports a JoomlaPack file from the includes folder using require_once
	 *
	 * @param string $file Filename, without extension (e.g. 'CConfiguration')
	 * @param boolean $backend If true, uses backend includes directory. If false, uses frontend includes directory.
	 */
	function import( $file, $backend = true )
	{
		if( $backend ) {
			$base = JPComponentRoot . '/includes/';
		} else {
			$base = JPFrontendRoot . '/includes/';
		}
		
		require_once $base . $file . '.php';
	}
	
	/**
	 * Expands the archive's template name and returns an absolute path
	 *
	 * @param string $extension The extension to append, defaults to '.zip'
	 * @return string The absolute filename of the archive file requested
	 */
	function getExpandedTarName( $extension = '.zip' )
	{
		$configuration =& CConfiguration::getInstance();
		
		// Get the proper extension
		$templateName = $configuration->TarNameTemplate;
		CJPLogger::WriteLog(_JP_LOG_DEBUG, "Archive template name: $templateName");

		// Parse [DATE] tag
		$dateExpanded = strftime("%Y%m%d", time());
		$templateName = str_replace("[DATE]", $dateExpanded, $templateName);

		// Parse [TIME] tag
		$timeExpanded = strftime("%H%M%S", time());
		$templateName = str_replace("[TIME]", $timeExpanded, $templateName);

		// Parse [HOST] tag
		$templateName = str_replace("[HOST]", $_SERVER['SERVER_NAME'], $templateName);
		CJPLogger::WriteLog(_JP_LOG_DEBUG, "Expanded template name: $templateName");
		
		//add prefix for kickstart
		//$templateName = "jp_".$templateName;

		$path = $configuration->OutputDirectory . "/" . $templateName . $extension;
		CJPLogger::WriteLog(_JP_LOG_DEBUG, "Calculated archive absolute path: $path");
		
		// What the hell was I thinking when I had put realpath($path) in here?!?!?!
		return CJVAbstract::TranslateWinPath( $path );
	}
	
	function getDBPrefix()
	{
		if( !defined('_JEXEC') )
		{
			global $mosConfig_dbprefix;
			return $mosConfig_dbprefix;
		} else {
			$conf =& JFactory::getConfig();
			return $conf->getValue('config.dbprefix');
		}
	}

	/**
	 * Parse an INI file and return an associative array. Since PHP versions before 5.1 are
	 * bitches with regards to INI parsing, I use a PHP-only solution to overcome this
	 * obstacle.
	 *
	 * @param string $file The file to process
	 * @param bool $process_sections True to also process INI sections
	 * @return array An associative array of sections, keys and values
	 */
	function parse_ini_file( $file, $process_sections )
	{
		if( version_compare(PHP_VERSION, '5.1.0', '>=') )
		{
			return parse_ini_file($file, $process_sections);
		} else {
			return CJVAbstract::_parse_ini_file($file, $process_sections);
		}
	}
	
	/**
	 * A PHP based INI file parser.
	 * 
	 * Thanks to asohn ~at~ aircanopy ~dot~ net for posting this handy function on
	 * the parse_ini_file page on http://gr.php.net/parse_ini_file
	 * 
	 * @param string $file Filename to process
	 * @param bool $process_sections True to also process INI sections
	 * @return array An associative array of sections, keys and values
	 * @access private
	 */
	function _parse_ini_file($file, $process_sections = false)
	{
		  $process_sections = ($process_sections !== true) ? false : true;

		  $ini = file($file);
		  if (count($ini) == 0) {return array();}
		
		  $sections = array();
		  $values = array();
		  $result = array();
		  $globals = array();
		  $i = 0;
		  foreach ($ini as $line) {
		    $line = trim($line);
		    $line = str_replace("\t", " ", $line);
		
		    // Comments
		    if (!preg_match('/^[a-zA-Z0-9[]/', $line)) {continue;}
		
		    // Sections
		    if ($line{0} == '[') {
		      $tmp = explode(']', $line);
		      $sections[] = trim(substr($tmp[0], 1));
		      $i++;
		      continue;
		    }
		
		    // Key-value pair
		    list($key, $value) = explode('=', $line, 2);
		    $key = trim($key);
		    $value = trim($value);
		    if (strstr($value, ";")) {
		      $tmp = explode(';', $value);
		      if (count($tmp) == 2) {
		        if ((($value{0} != '"') && ($value{0} != "'")) ||
		            preg_match('/^".*"\s*;/', $value) || preg_match('/^".*;[^"]*$/', $value) ||
		            preg_match("/^'.*'\s*;/", $value) || preg_match("/^'.*;[^']*$/", $value) ){
		          $value = $tmp[0];
		        }
		      } else {
		        if ($value{0} == '"') {
		          $value = preg_replace('/^"(.*)".*/', '$1', $value);
		        } elseif ($value{0} == "'") {
		          $value = preg_replace("/^'(.*)'.*/", '$1', $value);
		        } else {
		          $value = $tmp[0];
		        }
		      }
		    }
		    $value = trim($value);
		    $value = trim($value, "'\"");
		
		    if ($i == 0) {
		      if (substr($line, -1, 2) == '[]') {
		        $globals[$key][] = $value;
		      } else {
		        $globals[$key] = $value;
		      }
		    } else {
		      if (substr($line, -1, 2) == '[]') {
		        $values[$i-1][$key][] = $value;
		      } else {
		        $values[$i-1][$key] = $value;
		      }
		    }
		  }
		
		  for($j = 0; $j < $i; $j++) {
		    if ($process_sections === true) {
		      $result[$sections[$j]] = $values[$j];
		    } else {
		      $result[] = $values[$j];
		    }
		  }
		
		  return $result + $globals;
	}
}

/**
 * Provides common HTML rendering features
 *
 */
class CJPHTML
{
	/**
	 * Returns an administration page header
	 *
	 * @param string $pageTitle The title of the page
	 * @return string
	 */
	function getAdminHeadingHTML( $pageTitle = null )
	{
		$lang =& CLangManager::getInstance();
		$myTitle = $lang->get('common', 'jptitle');
		if( is_null( $pageTitle ) ) {
			$pageTitle = $myTitle;  		
		}
		
		$homeButton = CJPHTML::getImageURI('home');
		$componentURI = CJVAbstract::JPLink('main');
		$homeAlt = $lang->get('cpanel','home');
		
		if( !defined('_JEXEC') )
		{
			// Joomla! 1.0.x-style headings
			$out = <<<ENDMARK
				<table class="adminheading">
					<thead>
						<tr>
							<th class="info" nowrap rowspan="2">
								$pageTitle
							</th>
							<th nowrap align="right" width="40" style="background: none;">
								<a href="$componentURI"><img src="$homeButton" border="0" width="32" height="32" alt="$homeAlt" title="$homeAlt" /></a>
							</th>
							</tr>
					</thead>
				</table>
	
ENDMARK;
		} else {
			// Joomla! 1.5.x-style headings
			$out = <<<ENDMARK
				<table class="adminheading" style="width:100%">
					<tr>
						<td nowrap><h2>$pageTitle</h2>
						<td nowrap align="right" width="40" style="text-align: right;">
							<a href="$componentURI"><img src="$homeButton" border="0" width="32" height="32" alt="$homeAlt" title="$homeAlt" /></a>
						</td>
					</tr>
				</table>
ENDMARK;
		}

		return $out;
	}
	
	/**
	 * Returns HTML for colored (red/green) display of writable status 
	 *
	 * @param mixed $CurrentStatus The current status of the item in question
	 * @param mixed $DesiredStatus The status considered good for the item in question
	 * @param string $goodKey The translation key for the string to display if the status is good
	 * @param string $errorKey The translation key for the string to display if the status is not good
	 * @param string $translationSection The translation section to look for the key in.
	 * @return string
	 */
	function colorizeWriteStatus( $CurrentStatus, $DesiredStatus, $goodKey = 'writable', $errorKey = 'unwritable', $translationSection = 'common' ) {
		global $JPLang;
	
		$isStatusGood = ($CurrentStatus === $DesiredStatus);
		$statusVerbal = $isStatusGood ? $JPLang[$translationSection][$goodKey] : $JPLang[$translationSection][$errorKey];
		$class = $isStatusGood ? 'statusok' : 'statusnotok';
		$image = $isStatusGood ? 'ok_small' : 'error_small';
		
		$imageURL = CJPHTML::getImageURI( $image );
		
		return '<span class="' . $class . '"><img src="' . $imageURL . '" border="0" width="16" height="16" />' . $statusVerbal . '</span>';
	}
	
	function getImageURI( $imageName )
	{
		return CJVAbstract::SiteURI() . '/administrator/components/' . CJVAbstract::getParam('option', 'com_joomlapack') . '/images/' . $imageName . '.png';
	}

}

/**
 * Provides a Control Panel rendering class
 *
 */
class CJPControlPanelHTML
{
	/**
	 * The array holding the control panel items
	 *
	 * @var array
	 */
	var $items = array();
	
	/**
	 * Public constructor
	 *
	 * @return CJPControlPanelHTML
	 */
	function CJPControlPanelHTML()
	{
		
	}
	
	/**
	 * Adds a control panel item to the collection
	 *
	 * @param string $link The URL to visit when clicked
	 * @param string $image The name of the image to display, minus the path and .png extensions
	 * @param string $text The label beneath the image
	 * @param string $onclick A JavaScript command to execute upon clicking (overrides the $link URL)
	 */
	function addItem( $link, $image, $text, $onclick = null )
	{
		$myItem = array(
			'link' => $link,
			'image' => $image,
			'text' => $text,
			'onclick' => $onclick
		);
		
		$this->items[] = $myItem;
	}
	
	function getHTML()
	{
		$out = '<div id="cpanel">';
		foreach( $this->items as $item )
		{
			$out .= $this->_getItemHTML( $item );			
		}
		$out .= "</div>";
		return $out;
	}
	
	function _getItemHTML( $item )
	{
		if( !is_null( $item['onclick'] ) )
		{
			$anchorTags = 'onclick="' . $item['onclick'] . '"';
		} else {
			$anchorTags = 'href="' . $item['link'] . '"';
		}
		
		$imageHTML = CJPHTML::getImageURI($item['image']);
		$imageHTML = '<img src="' . $imageHTML . '" border="0" width="32" height="32" />';
		
		$text = $item['text'];
		
		$out = <<<ENDOFHTML
			<div style="float:left;">
				<div class="icon">
					<a $anchorTags >$imageHTML<br/>
					<span>$text</span>
					</a>
				</div>
			</div>
ENDOFHTML;

		return $out;
	}
}
?>