<?php
/**
* @package		JoomlaPack
* @subpackage	CUBE
* @copyright	Copyright (C) 2006-2008 JoomlaPack Developers. All rights reserved.
* @version		$Id$
* @license 		http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
* JoomlaPack is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
**/

// ensure this file is being included by a parent file - Joomla! 1.0.x and 1.5 compatible
(defined( '_VALID_MOS' ) || defined('_JEXEC')) or die( 'Direct Access to this location is not allowed.' );

define( '_JPA_MAJOR', 1 ); // JPA Format major version number
define( '_JPA_MINOR', 0 ); // JPA Format minor version number

/**
 * JoomlaPack Archive creation class
 * 
 * JPA Format 1.0 implemented, minus BZip2 compression support
 */
class CJPACreator extends CZIPCreator
{
	/**
	 * How many files are contained in the archive
	 *
	 * @var integer
	 */
	var $_fileCount = 0;
	
	/**
	 * The total size of files contained in the archive as they are stored (it is smaller than the
	 * archive's file size due to the existence of header data)
	 *
	 * @var integer
	 */
	var $_compressedSize = 0;
	
	/**
	 * The total size of files contained in the archive when they are extracted to disk.
	 *
	 * @var integer
	 */
	var $_uncompressedSize = 0;
	
	/**
	 * Constructor
	 *
	 * @param string $archiveFileName The full pathname to the archive file
	 * @return CJPACreator
	 */
	function CJPACreator( $archiveFileName )
	{
		CJPLogger::WriteLog(_JP_LOG_DEBUG, "CJPACreator :: new instance - archive $archiveFileName");
		$this->_dataFileName = $archiveFileName;
		
		// Try to kill the archive if it exists
		CJPLogger::WriteLog(_JP_LOG_DEBUG, "CJPACreator :: Killing old archive");
		$fp = @fopen( $this->_dataFileName, "wb" );
		if (!($fp === false)) {
			@ftruncate( $fp,0 );
			@fclose( $fp );
		} else {
			@unlink( $this->_dataFileName );
			@touch( $this->_dataFileName );
		}
		
		// Update some internal values, overriding ZIP defaults
		$this->_ctrlDirHeader = "\x4A\x50\x41";	// Standard Header signature
		$this->_fileHeader = "\x4A\x50\x46";	// Entity Block signature
		 
		
		// Write the initial instance of the archive header
		$this->_writeArchiveHeader();
	}
	
    /**
     * Adds a single file to the ZIP archive (actually, updates Central Directory and Data File)
     * from data passed directly as a string. Currently used for databases.ini production
     *
     * @access public
     * @param string $fileName	Full pathname to file to be stored
     * @param string $addPath	Path to add
     * @param string $virtualContent	The data of the file
     * @return boolean True on success, false if file was skipped
     */
	function addVirtualFile( $fileName, $addPath, $virtualContent )
	{
		// Get real size before compression
		$fileSize = strlen($virtualContent);

		// Decide if we will compress
		$compressionMethod = function_exists("gzcompress") ? 1 : 0;

		$storedName = $this->_addRemovePaths( $fileName, '', $addPath );
		
        /* "Entity Description BLock" segment. */
        $unc_len = &$fileSize; // File size

		if ($compressionMethod == 1) {
			// Proceed with compression
			$zdata   = @gzcompress($virtualContent);
			if ($zdata === false) {
				// If compression fails, let it behave like no compression was available
				$c_len = &$unc_len;
				$compressionMethod = 0;
			} else {
				$zdata   = substr(substr($zdata, 0, strlen($zdata) - 4), 2);
				$c_len   = strlen($zdata);
			}			
		} else {
			$c_len = $unc_len;
		}

		$this->_compressedSize += $c_len; // Update global data
		$this->_uncompressedSize += $fileSize; // Update global data
		$this->_fileCount++;
		
		// Get file permissions
		$perms = 0777;
		
		// Calculate Entity Description Block length
		$blockLength = 21 + strlen($storedName) ;
		
        // Open data file for output
		$fp = @fopen( $this->_dataFileName, "ab");
		if ($fp === false)
			CJPLogger::WriteLog(_JP_LOG_ERROR, "Could not open archive file {$this->_dataFileName} for append!");
		$this->_fwrite( $fp, $this->_fileHeader ); // Entity Description Block header
		$this->_fwrite( $fp, pack('v', $blockLength) ); // Entity Description Block header length
		$this->_fwrite( $fp, pack('v', strlen($storedName) ) ); // Length of entity path
		$this->_fwrite( $fp, $storedName ); // Entity path
		$this->_fwrite( $fp, pack('C', 1 ) ); // Entity type
		$this->_fwrite( $fp, pack('C', $compressionMethod ) ); // Compression method
		$this->_fwrite( $fp, pack('V', $c_len ) ); // Compressed size
		$this->_fwrite( $fp, pack('V', $unc_len ) ); // Uncompressed size
		$this->_fwrite( $fp, pack('V', $perms ) ); // Entity permissions

		/* "File data" segment. */
		if ($compressionMethod == 1) {
			// Just dump the compressed data
			$this->_fwrite( $fp, $zdata );
			unset( $zdata );
		} else {
			$this->_fwrite( $fp, $virtualContent );
		}

		fclose( $fp );
		
		// ... and return TRUE = success
		return TRUE;
	}
	
	/**
	 * Updates the Standard Header with current information
	 */
	function finalizeArchive()
	{
		$this->_writeArchiveHeader();
	}
	
    /**
     * Adds a single file to the JPA archive.
     * It intelligently choses when to compress based on file size and available memory.
     *
     * @access private
     * @param string $fileName	Full pathname to file to be stored
     * @return boolean True on success, false if file was skipped
     */
	function _addFile( $fileName, $removePath, $addPath )
	{
		// See if it's a directory
		$isDir = is_dir($fileName);

		// Get real size before compression
		$fileSize = $isDir ? 0 : filesize($fileName);

		// Decide if we will compress
		if ($isDir) {
			$compressionMethod = 0; // don't compress directories...
		} else {
			// Do we have plenty of memory left?
			$memLimit = ini_get("memory_limit");
			if( is_numeric($memLimit) && ($memLimit < 0) ) $memLimit = ""; // 1.2a3 -- Rare case with memory_limit < 0, e.g. -1Mb!
			if (($memLimit == "") || ($fileSize >= _CZIPCREATOR_COMPRESSION_THRESHOLD)) {
				// No memory limit, or over 1Mb files => always compress up to 1Mb files (otherwise it times out)
				$compressionMethod = ($fileSize <= _CZIPCREATOR_COMPRESSION_THRESHOLD) ? 1 : 0;
			} elseif ( function_exists("memory_get_usage") ) {
				// PHP can report memory usage, see if there's enough available memory; Joomla! alone eats about 5-6Mb! This code is called on files <= 1Mb
				$memLimit = $this->_return_bytes( $memLimit );
				$availableRAM = $memLimit - memory_get_usage();
				$compressionMethod = (($availableRAM / 2.5) >= $fileSize) ? 1 : 0;
			} else {
				// PHP can't report memory usage, compress only files up to 512Kb (conservative approach) and hope it doesn't break
				$compressionMethod = ($fileSize <= 524288) ? 1 : 0;
			}
		}

		$compressionMethod = function_exists("gzcompress") ? $compressionMethod : 0;

		$storedName = $this->_addRemovePaths( $fileName, $removePath, $addPath );
		
        /* "Entity Description BLock" segment. */
        $unc_len = &$fileSize; // File size
        $storedName .= ($isDir) ? "/" : "";

		if ($compressionMethod == 1) {
			// Get uncompressed data
			if( function_exists("file_get_contents") && (_CZIPCREATOR_FORCE_FOPEN == false) ) {
				$udata = @file_get_contents( $fileName ); // PHP > 4.3.0 saves us the trouble
			} else {
				// Argh... the hard way!
				$udatafp = @fopen( $fileName, "rb" );
				if( !($udatafp === false) ) {
					$udata = "";
					while( !feof($udatafp) ) {
						$udata .= fread($udatafp, 524288);
					}
					fclose( $udatafp );
				} else {
					$udata = false;
				}
			}
			
			if ($udata === FALSE) {
				// Unreadable file, skip it.
				return false;
			} else {
				// Proceed with compression
				$zdata   = @gzcompress($udata);
				if ($zdata === false) {
					// If compression fails, let it behave like no compression was available
					$c_len = &$unc_len;
					$compressionMethod = 0;
				} else {
					unset( $udata );
					$zdata   = substr(substr($zdata, 0, strlen($zdata) - 4), 2);
					$c_len   = strlen($zdata);
				}
			}
		} else {
			$c_len = $unc_len;
		}

		$this->_compressedSize += $c_len; // Update global data
		$this->_uncompressedSize += $fileSize; // Update global data
		$this->_fileCount++;
		
		// Get file permissions
		$perms = @fileperms( $fileName );
		
		// Calculate Entity Description Block length
		$blockLength = 21 + strlen($storedName) ;
		
        // Open data file for output
		$fp = @fopen( $this->_dataFileName, "ab");
		if ($fp === false)
			CJPLogger::WriteLog(_JP_LOG_ERROR, "Could not open archive file {$this->_dataFileName} for append!");
		$this->_fwrite( $fp, $this->_fileHeader ); // Entity Description Block header
		$this->_fwrite( $fp, pack('v', $blockLength) ); // Entity Description Block header length
		$this->_fwrite( $fp, pack('v', strlen($storedName) ) ); // Length of entity path
		$this->_fwrite( $fp, $storedName ); // Entity path
		$this->_fwrite( $fp, pack('C', ($isDir ? 0 : 1) ) ); // Entity type
		$this->_fwrite( $fp, pack('C', $compressionMethod ) ); // Compression method
		$this->_fwrite( $fp, pack('V', $c_len ) ); // Compressed size
		$this->_fwrite( $fp, pack('V', $unc_len ) ); // Uncompressed size
		$this->_fwrite( $fp, pack('V', $perms ) ); // Entity permissions

		/* "File data" segment. */
		if ($compressionMethod == 1) {
			// Just dump the compressed data
			$this->_fwrite( $fp, $zdata );
			unset( $zdata );
		} elseif (!$isDir) {
			// Copy the file contents, ignore directories
			$zdatafp = @fopen( $fileName, "rb" );
			while( !feof($zdatafp) ) {
				$zdata = fread($zdatafp, 524288);
				$this->_fwrite( $fp, $zdata );
			}
			fclose( $zdatafp );
		}

		fclose( $fp );
		
		// ... and return TRUE = success
		return TRUE;
	}
	
	/**
	 * Outputs a Standard Header at the top of the file
	 *
	 */
	function _writeArchiveHeader()
	{
		$fp = @fopen( $this->_dataFileName, 'r+' );
		$this->_fwrite( $fp, $this->_ctrlDirHeader );					// ID string (JPA)
		$this->_fwrite( $fp, pack('v', 19) );							// Header length; fixed to 19 bytes
		$this->_fwrite( $fp, pack('C', _JPA_MAJOR ) );					// Major version
		$this->_fwrite( $fp, pack('C', _JPA_MINOR ) );					// Minor version
		$this->_fwrite( $fp, pack('V', $this->_fileCount ) );			// File count
		$this->_fwrite( $fp, pack('V', $this->_uncompressedSize ) );	// Size of files when extracted
		$this->_fwrite( $fp, pack('V', $this->_compressedSize ) );		// Size of files when stored
		@fclose( $fp );
	}
}
?>