<?php
/**
 * Database Table Exclusion Filter Class
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is distributed subject to the GNU General
 * Public Licence (GPL) version 2 or later.
 * http://www.gnu.org/copyleft/gpl.html
 * If you did not receive a copy of the GNU GPL and are unable to obtain it through the web,
 * please send a note to nikosdion@gmail.com so we can mail you a copy immediately.
 *
 * Visit www.JoomlaPack.net for more details.
 *
 * @package    JoomlaPack
 * @Author     Nicholas K. Dionysopoulos nikosdion@gmail.com
 * @copyright  2006-2007 Nicholas K. Dionysopoulos
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version    $Id$
 */

// ensure this file is being included by a parent file - Joomla! 1.0.x and 1.5 compatible
(defined( '_VALID_MOS' ) || defined('_JEXEC')) or die( 'Direct Access to this location is not allowed.' );


class CDBExclusionFilter extends CJPFilterAbstract
{
	/**
	 * Implements the abstract init method
	 *
	 */
	function init()
	{
		$database = CJVAbstract::getDatabase();
		
		// Initialize by loading any exisiting filters
		$sql = "SELECT * FROM #__jp_dbtf";
		$database->setQuery( $sql );
		$database->query();

		$tempFilters = $database->loadAssocList();
		
		// Initialize with existing filters
		$this->_databaseFilters = array();
		
		if (!is_null($tempFilters)) {
			foreach($tempFilters as $filter){
				$this->_databaseFilters[] = $filter['tablename'];
			}
		}
	}
	
	/**
	 * Modifies a table exclusion status
	 *
	 * @param string $table The table's abstract name, e.g. #__content for jos_content
	 * @param string $checked Set to TRUE, on or checked to enable filter. Anything else disables it.
	 * @return boolean Always true
	 */
	function modifyFilter( $table, $checked )
	{
		$db = CJVAbstract::getDatabase();
		
		// Delete any existing instances
		$sql = "DELETE FROM #__jp_dbtf WHERE `tablename` = '$table'";
		$db->setQuery($sql);
		$db->query();
		
		// Add a new record if $checked is true
		if( ($checked == 'on') || ($checked == 'checked') || ($checked === true) )
		{
			$sql = "INSERT INTO #__jp_dbtf (`tablename`) VALUES ('$table')";
			$db->setQuery($sql);
			$db->query();
		}
		
		return true;
	}

	/**
	 * Removes all filters
	 */
	function ResetDBFilters()
	{
		$db = CJVAbstract::getDatabase();
		$sql = "DELETE FROM #__jp_dbtf";
		$db->setQuery($sql);
		$db->query();
	}
	
	/**
	 * Adds all non-Joomla tables to the exclusion filters
	 */
	function ExcludeNonJoomla()
	{
		// Get all tables
		$db = CJVAbstract::getDatabase();
		$sql = "SHOW TABLES";
		$db->setQuery($sql);
		$tables = $db->loadRowList();

		// Get prefix
		global $mosConfig_dbprefix;
		$prefix = defined('_JEXEC') ? JApplication::getCfg('dbprefix') : $mosConfig_dbprefix;
		
		// Loop tables
		foreach( $tables as $row )
		{
			$table = $row[0];
			$abstractTable = str_replace($prefix, '#__', $table);
			if( $table == $abstractTable )
			{
				// Filter only non-Joomla tables
				$this->modifyFilter($abstractTable, true);
			}
		}
	}

}
?>