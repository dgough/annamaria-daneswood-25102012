<?php
/**
 * Single File Exclusion Filter Class
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is distributed subject to the GNU General
 * Public Licence (GPL) version 2 or later.
 * http://www.gnu.org/copyleft/gpl.html
 * If you did not receive a copy of the GNU GPL and are unable to obtain it through the web,
 * please send a note to nikosdion@gmail.com so we can mail you a copy immediately.
 *
 * Visit www.JoomlaPack.net for more details.
 *
 * @package    JoomlaPack
 * @Author     Nicholas K. Dionysopoulos nikosdion@gmail.com
 * @copyright  2006-2007 Nicholas K. Dionysopoulos
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version    $Id$
 */

// ensure this file is being included by a parent file - Joomla! 1.0.x and 1.5 compatible
(defined( '_VALID_MOS' ) || defined('_JEXEC')) or die( 'Direct Access to this location is not allowed.' );

class CSingleFileFilter extends CJPFilterAbstract {
	
	/**
	 * Loads the file filters off the database and stores them in the _singleFileFilters array
	 *
	 */
	function init()
	{
		$database = CJVAbstract::getDatabase();
		$configuration =& CConfiguration::getInstance();
		
		// Initialize by loading any exisiting filters
		$sql = "SELECT * FROM #__jp_sff";
		$database->setQuery( $sql );
		$database->query();

		$tempFilters = $database->loadAssocList();
				
		// Initialize with existing filters
		$this->_singleFileFilters = array();
		
		if (!is_null($tempFilters)) {
			foreach($tempFilters as $filter){
				$this->_singleFileFilters[] = CJVAbstract::TranslateWinPath($filter['file']);
			}
		}
	}
	
	/**
	 * Gets the contents of a directory and flags excluded files
	 *
	 * @param string $root The directory to scan
	 * @return array An associative array of associative arrays (use the code, Luke!)
	 */
	function getDirectory( $root )
	{
		// If there's no root directory specified, use the site's root
		$root = is_null($root) ? JPSiteRoot : $root ;
		
		$isSiteRoot = CJVAbstract::TranslateWinPath($root) == CJVAbstract::TranslateWinPath(JPSiteRoot);

		// Initialize filter list
		$this->init();
		
		// Initialize the two arrays to be returned
		$arDirs = array();
		$arFiles = array();
		
		// Get directory's contents
		CJVAbstract::import('CFSAbstraction');
		$FS = new CFSAbstraction();
		
		$allFilesAndDirs = $FS->getDirContents( $root );
		
		if (!($allFilesAndDirs === false)) {
			foreach($allFilesAndDirs as $fileDef) {
				$fileName = basename($fileDef['name']);
				switch( $fileDef['type'] )
				{
					case 'dir':
						if( $isSiteRoot && (($fileName == '.') || ($fileName == '..')) )
						{
							// Don't include . and .. for site's root							
						} else {
							if( $fileName != '.' ) {
								$arDirs[] = $fileName;	
							}						
						}
						break;
					case 'file':
						$excluded = is_array($this->_singleFileFilters) ? in_array( CJVAbstract::TranslateWinPath($root . DIRECTORY_SEPARATOR . $fileName) , $this->_singleFileFilters) : false;
						$arFiles[$fileName] = $excluded; 
						break;
				}
			}
		}
		
		sort($arDirs);
		$ret['folders'] = $arDirs;
		unset($arDirs);
		$ret['files'] = $arFiles;
		unset($arFiles);
		return $ret;
	}
	
	/**
	 * Modifies a filter
	 *
	 * @param string $root Folder where the file exists
	 * @param string $file The file for which the filter is about
	 * @param mixed $checked If set to on, yes or checked then the filter is activated, otherwise deactivated
	 */
	function modifyFilter($root, $file, $checked)
	{
		$database = CJVAbstract::getDatabase();
		
		$activate = ($checked == "on") || ($checked == "yes") || ($checked == "checked") ? true : false;
		
		$sql = "SELECT `sff_id` FROM #__jp_sff WHERE `file`=\"" . $database->getEscaped( CJVAbstract::TranslateWinPath($root . "/" . $file) ) . "\"";
		$database->setQuery( $sql );
		$database->query();
		$sff_id = $database->loadResult();
		
		if ($activate) {
			// Add the filter, if it doesn't exist
			if (is_null($sff_id)) {
				$sql = "INSERT INTO #__jp_sff (`file`) VALUES (\"" . $database->getEscaped(CJVAbstract::TranslateWinPath($root . "/" . $file) ) . "\")";
				$database->setQuery( $sql );
				$database->query();
			}
		} else {
			// Remove the filter, if it exists
			$sql = "DELETE FROM #__jp_sff WHERE `file` = \"" . $database->getEscaped(CJVAbstract::TranslateWinPath($root . "/" . $file) ) . "\"";
			$database->setQuery( $sql );
			$database->query();
		}
	}
}
?>