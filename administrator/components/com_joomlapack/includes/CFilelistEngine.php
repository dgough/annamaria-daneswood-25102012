<?php
/**
* @package		JoomlaPack
* @subpackage	CUBE
* @copyright	Copyright (C) 2006-2008 JoomlaPack Developers. All rights reserved.
* @version		$Id$
* @license 		http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
* JoomlaPack is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
**/

// ensure this file is being included by a parent file - Joomla! 1.0.x and 1.5 compatible
(defined( '_VALID_MOS' ) || defined('_JEXEC')) or die( 'Direct Access to this location is not allowed.' );

$config =& CConfiguration::getInstance();
define('JPMaxFragmentSize', $config->get('mnMaxFragmentSize'));	// Maximum bytes a fragment can have (default: 1Mb)
define('JPMaxFragmentFiles', $config->get('mnMaxFragmentFiles'));		// Maximum number of files a fragment can have (default: 50 files)

/**
 * Engine part to handles the file list creation
 */
class CFilelistEngine extends CJPEngineParts {
	/**
     * Directories to exclude
     * @access private
     * @var array
     */
	var $_ExcludeDirs;
	
	/**
	 * Files to exclude
	 *
	 * @var array
	 */
	var $_ExcludeFiles;

	/**
     * The next directory to scan
     * @access private
     * @var string
     */
	var $_nextDirectory;

	/**
     * The number of the current node (fragment)
     * @access private
     * @var integer
     */
	var $_currentNode;

	/**
     * The size of the current node (fragment) in bytes
     * @access private
     * @var integer
     */
	var $_currentNodeSize;

	/**
     * The list of branch nodes (directories not yet scanned for files)
     * @access private
     * @var array
     */
	var $_branchNodes;

	/**
     * Holds the list of files of the current fragment
     * @access private
     * @var array
     */
	var $_currentList;

	/**
	* Public constructor CFileListEngine
	* When the object is generated, it takes care of removing old entries and
	* initializing this task's algorithm
	*/
	function CFilelistEngine()
	{
		$this->_DomainName = "FileList";
		CJPLogger::WriteLog(_JP_LOG_DEBUG, "CFilelistEngine :: New instance");
	}
	
	/**
	 * Implements the _prepare() method of CJPEngineParts
	 * @access private
	 */
	function _prepare()
	{
		CJPLogger::WriteLog(_JP_LOG_DEBUG, "CFilelistEngine :: Starting prepare()");
		
		// Remove old entries from filelist database
		CJPTables::DeleteMultipleVars('fragment%');

		// Get the directory exclusion filters - this only needs to be done once
		$this->_createExcludeDirs();
		
		// FIX 1.1.0 $mosConfig_absolute_path may contain trailing slashes or backslashes incompatible with exclusion filters
		$this->_nextDirectory = realpath(JPSiteRoot); // Start scanning from Joomla! root
		$this->_currentNode = 1; // We start adding to the first fragment
		$this->_currentNodeSize = 0; // The size of this fragment is 0 bytes, as no files are added yet
		$this->_branchNodes = array();
		$this->_currentList = array();
		
		$this->_isPrepared = true;

		CJPLogger::WriteLog(_JP_LOG_DEBUG, "CFilelistEngine :: Finished prepare()");
	}

	/**
	* Scans the next directory if we have not finished. Implements the _run() method
	* of CJPEngineParts.
	* @access private
	*/
	function _run(){
		if ($this->_hasRan) {
			CJPLogger::WriteLog(_JP_LOG_DEBUG, "CFilelistEngine :: Already finished");
			$this->_isRunning = false;
			$this->_Step = "-";
			$this->_Substep = "";
		} else {
			// Process the next directory
			$this->_recurseDirectory( $this->_nextDirectory );

			// Get the next directory, or mark ourselves as finished
			$nextDir = $this->_getNextDirectory();
			if ($nextDir === false) {
				$this->_isRunning = false;
				$this->_hasRan = true;
			} else {
				$this->_nextDirectory = $nextDir;
			}

			$this->_Step = $nextDir;
			$this->_Substep = "";
		}
	}
	
	/**
	 * Finalization method. Implements the _finalize() methid of CJPEngineParts
	 * @access private
	 */
	function _finalize()
	{
		// 30.08.2007 : Add finally save fragment - start fix by dimon(www.izumrud.com.ua;webstudio@ukr.net)
		if ($this->_currentNodeSize > 0 ){
			CJPLogger::WriteLog(_JP_LOG_DEBUG, "Saving fragment #" . $this->_currentNode);
			// Save current fragment
			$this->_saveFragment();
			// Start a new fragment
			$this->_currentList = array();
			$this->_currentNode++;
			$this->_currentNodeSize = 0;
		}
		// 30.08.2007 --- end fix by dimon(www.izumrud.com.ua;webstudio@ukr.net)
		
		$this->_isFinished = true;
		
		CJPLogger::WriteLog(_JP_LOG_DEBUG, "CFilelistEngine :: Finalized");
	}

	function _recurseDirectory( $dirName ){
		CJPLogger::WriteLog(_JP_LOG_DEBUG, "Recursing into " . $dirName);

		CJVAbstract::import('CFSAbstraction');
		$FS = new CFSAbstraction();

		if (in_array( $dirName, $this->_ExcludeDirs )) {
			CJPLogger::WriteLog(_JP_LOG_INFO, "Skipping directory $dirName");
			return;
		}

		// Get the contents of the directory
		$fileList = $FS->getDirContents( $dirName );

		if (!is_readable($dirName)) {
			// A non-browsable directory
			CJPLogger::WriteLog(_JP_LOG_WARNING, "Unreadable directory $dirName. Check permissions.");
		}

		if ($fileList === false) {
			// A non-browsable directory; however, it seems that I never get FALSE reported here?!
			CJPLogger::WriteLog(_JP_LOG_WARNING, "Unreadable directory $dirName. Check permissions.");
		} else {
			// Initialize local processed files counter
			$processedFiles = 0;
			// Scan all directory entries
			foreach($fileList as $fileDescriptor) {
				switch($fileDescriptor['type']) {
					case "dir":
						// A new directory found. Mark it for recursion
						if (!( ( substr($fileDescriptor['name'], -1, 1) == "." ) || ( substr($fileDescriptor['name'], -1, 2) == ".." ) )) {
							$this->_branchNodes[] = $fileDescriptor['name'];
							$processedFiles++;
							CJPLogger::WriteLog(_JP_LOG_DEBUG, "Adding directory " . $fileDescriptor['name']);
						}
						break;
					case "file":
						// Just a file... process it.
						$processedFiles++;
						$filesize = $fileDescriptor['size'];
						$skipThisFile = is_array($this->_ExcludeFiles) ? in_array( $fileDescriptor['name'], $this->_ExcludeFiles ) : false;
						if ($skipThisFile) {
							CJPLogger::WriteLog(_JP_LOG_INFO, "Skipping file $dirName");
						} else {
							if (($this->_currentNodeSize + $filesize <= JPMaxFragmentSize) && (count($this->_currentList) < JPMaxFragmentFiles) ) {
								// It fits in the current fragment (see the JPMaxFragmentX constants)
								$this->_currentNodeSize += $filesize;
							} else {
								CJPLogger::WriteLog(_JP_LOG_DEBUG, "Saving fragment #" . $this->_currentNode);
								// Save current fragment
								$this->_saveFragment();
								// Start a new fragment
								$this->_currentList = array();
								$this->_currentNode++;
								$this->_currentNodeSize = 0;
							}
							$this->_currentList[] = $fileDescriptor['name'];
							//CJPLogger::WriteLog(_JP_LOG_DEBUG, "Added file " . $fileDescriptor['name'] . "(" . $fileDescriptor['size'] . " bytes )");
						}
						break;
					// All other types (links, character devices etc) are ignored.
				}
			}
			// Check for empty directories and add them to the list
			if ( $processedFiles == 0 ) {
				$this->_currentList[] = $dirName;
				CJPLogger::WriteLog(_JP_LOG_INFO, "Empty directory $dirName");
			}
		}
		CJPLogger::WriteLog(_JP_LOG_DEBUG, "Done recursing $dirName");
	}

	function _saveFragment(){
		$database = CJVAbstract::getDatabase();

		$fragmentDescriptor = array();
		$fragmentDescriptor['type'] = "site"; // Other possible values are 'installer', 'sql', 'external'
		$fragmentDescriptor['size'] = $this->_currentNodeSize;
		$fragmentDescriptor['files'] = $this->_currentList;

		$serializedDescriptor = serialize($fragmentDescriptor);
		unset($fragmentDescriptor);

		CJPTables::WriteVar( "fragment" . $this->_currentNode, $serializedDescriptor );

		unset($serializedDescriptor);
	}

	function _getNextDirectory(){
		if (count($this->_branchNodes) == 0) {
			return false;
		} else {
			return array_shift( $this->_branchNodes );
		}
	}

	/**
	* Returns the array of the exclusion filters
	* TODO: Probably I should pass a reference to the CDirExclusion object instead of this
	*/
	function _createExcludeDirs() {
		CJVAbstract::import('CFilterManager');

		$filterManager = new CFilterManager();
		$filterManager->init();
		$this->_ExcludeDirs = $filterManager->getFilters('folder');
		$this->_ExcludeFiles = $filterManager->getFilters('singlefile');

		CJPLogger::WriteLog(_JP_LOG_DEBUG, "Got directory exclusion filters");
		
		unset($filterManager);
	}

}
?>