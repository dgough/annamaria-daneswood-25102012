<?php
/**
* @package		JoomlaPack
* @subpackage	CUBE
* @copyright	Copyright (C) 2006-2008 JoomlaPack Developers. All rights reserved.
* @version		$Id$
* @license 		http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
* JoomlaPack is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
**/

// ensure this file is being included by a parent file - Joomla! 1.0.x and 1.5 compatible
(defined( '_VALID_MOS' ) || defined('_JEXEC')) or die( 'Direct Access to this location is not allowed.' );

CJVAbstract::import('CDBDump');
/**
 * Multiple database backup engine.
 */
class CDBBackupEngine extends CJPEngineParts
{
	/**
	 * A list of the databases to be packed
	 *
	 * @var array
	 */
	var $_databaseList = array();
	
	/**
	 * The current instance of CDBDump used to backup tables
	 *
	 * @var CDBDump
	 */
	var $_currentCDBDump = null;
	
	/**
	 * The current index of _databaseList
	 *
	 * @var integer
	 */
	var $_currentListIndex = null;
	
	/**
	 * Implements the constructor of the class
	 *
	 * @return CDBBackupEngine
	 */
	function CDBBackupEngine()
	{
		$this->_DomainName = "PackDB";
		CJPLogger::WriteLog(_JP_LOG_DEBUG, "CDBBackupEngine :: New instance");		
	}
	
	/**
	 * Implements the _prepare abstract method
	 *
	 */
	function _prepare()
	{
		CJPLogger::WriteLog(_JP_LOG_DEBUG, "CDBBackupEngine :: Preparing instance");
		$this->_getDatabaseList();
		$this->_currentListIndex = 0;
		$this->_isPrepared = true;
		$this->_hasRan = false;
	}
	
	/**
	 * Implements the _run() abstract method
	 */
	function _run()
	{
		if( $this->_hasRan )
		{
			CJPLogger::WriteLog(_JP_LOG_DEBUG, "CDBBackupEngine :: Already finished");
			$this->_isRunning = false;
			$this->_hasRan = true;
			$this->_Step = '';
			$this->_Substep = '';			
		} else {
			$this->_isRunning = true;
			$this->_hasRan = false;			
		}
		
		
		// Make sure we have a CDBDump instance loaded!
		if(is_null( $this->_currentCDBDump ))
		{
			CJPLogger::WriteLog(_JP_LOG_DEBUG, "CDBBackupEngine :: Iterating next database");
			// Create a new instance
			$this->_currentCDBDump = new CDBDump();
			// Configure the CDBDump instance
			$this->_currentCDBDump->setup( $this->_databaseList[$this->_currentListIndex] );
		}
		
		// Try to step the instance
		$retArray = $this->_currentCDBDump->tick();
		$this->_Step = $retArray['Step'];
		$this->_Substep = $retArray['Substep'];

		// Check if the instance has finished
		if(!$retArray['HasRun'])
		{
			// The instance has finished; go to the next entry in the list and dispose the old CDBDump instance
			$this->_currentListIndex++;
			$this->_currentCDBDump = null;
			
			// Are we past the end of the list?
			if($this->_currentListIndex > (count($this->_databaseList)-1) )
			{
				CJPLogger::WriteLog(_JP_LOG_DEBUG, "CDBBackupEngine :: No more databases left to iterate");
				$this->_hasRan = true;
				$this->_isRunning = false;
			}
		}
		
	}
	
	/**
	 * Implements the _finalize() abstract method
	 *
	 */
	function _finalize()
	{
		$this->_isFinished = true;
		
		// If we are in db backup mode, don't create a databases.ini
		$cube =& CCUBE::getInstance();
		if ($cube->_OnlyDBMode) {
			CJPLogger::WriteLog(_JP_LOG_DEBUG, "CDBBackupEngine :: Skipping databases.ini");
			return;
		}
		
		
		CJPLogger::WriteLog(_JP_LOG_DEBUG, "CDBBackupEngine :: Creating databases.ini");
		// Create a new string
		$databasesINI = '';
		
		// Loop through databases list
		foreach( $this->_databaseList as $definition )
		{
			// Joomla! core database comes with no parameters; we must retrieve them
			if( $definition['isJoomla'] )
			{
				CJPLogger::WriteLog(_JP_LOG_DEBUG, "CDBBackupEngine :: Adding Joomla definition");
				if( !defined('_JEXEC') )
				{
					// Joomla! 1.0 parameters
					global $mosConfig_host, $mosConfig_user, $mosConfig_password, $mosConfig_db, $mosConfig_dbprefix;
					$definition['host']     = $mosConfig_host;
					$definition['username'] = $mosConfig_user;
					$definition['password'] = $mosConfig_password;
					$definition['database'] = $mosConfig_db;
					$definition['prefix']   = $mosConfig_dbprefix;
					$definition['dumpFile'] = 'joomla.sql';
				} else {
					// Joomla! 1.5 parameters
					jimport('joomla.database.database');
					jimport( 'joomla.database.table' );
					
					$conf =& JFactory::getConfig();
					$definition['host']     = $conf->getValue('config.host');
					$definition['username'] = $conf->getValue('config.user');
					$definition['password'] = $conf->getValue('config.password');
					$definition['database'] = $conf->getValue('config.db');
					$definition['prefix']   = $conf->getValue('config.dbprefix');
					$definition['dumpFile'] = 'joomla.sql';
				}
			} else {
				CJPLogger::WriteLog(_JP_LOG_DEBUG, "CDBBackupEngine :: Adding extra database definition");
				$definition['prefix'] = '';
			}
			
			$section = basename($definition['dumpFile']);
			
			$databasesINI .= <<<ENDDEF
[$section]
dbname = "{$definition['database']}"
sqlfile = "{$definition['dumpFile']}"
dbhost = "{$definition['host']}"
dbuser = "{$definition['username']}"
dbpass = "{$definition['password']}"
prefix = "{$definition['prefix']}"

ENDDEF;
			
		}
		
		// BEGIN FIX 1.2 Stable -- databases.ini isn't written on disk
		CJPLogger::WriteLog(_JP_LOG_DEBUG, "CDBBackupEngine :: Writing databases.ini contents as a packvar entry");
		$configuration = CConfiguration::getInstance();
		CJPTables::WriteVar('databases_ini', $databasesINI );
		
		// Create the fragment
		$db = CJVAbstract::getDatabase();
		$fragmentDescriptor = array();
		$fragmentDescriptor['type'] = "virtsql";
		$fragmentDescriptor['size'] = strlen($databasesINI);
		$fragmentDescriptor['files'] = array( 'databases.ini' );
		
		$serializedDescriptor = serialize($fragmentDescriptor);

		$sql = 'SELECT COUNT(*) FROM #__jp_packvars WHERE `key` LIKE "fragment%"';
		$db->setQuery( $sql );
		$currentNode = $db->loadResult();
		$currentNode++;

		CJPTables::WriteVar("fragment$currentNode", $serializedDescriptor);

		CJPLogger::WriteLog(_JP_LOG_DEBUG, "Added new fragment with virtual databases.ini");
		// END FIX 1.2 Stable
		
	}
	
	/**
	 * Populates _databaseList with the list of databases in the settings
	 *
	 */
	function _getDatabaseList()
	{
		/*
		 * Logic:
		 * Add an entry for the Joomla! database
		 * If we are in DB Only mode, return
		 * Otherwise, itterate the configured databases and add them if and only if all settings are populated
		 */
		
		// Cleanup the _databaseList array
		$this->_databaseList = array();
		
		// Add a new record for the core Joomla! database
		$entry = array(
			'isJoomla' => true,
			'useFilters' => true,
			'host' => '',
			'port' => '',
			'username' => '',
			'password' => '',
			'database' => '',
			'dumpFile' => ''
		);
		
		$this->_databaseList[] = $entry;
		
		$cube =& CCUBE::getInstance();
		if ($cube->_OnlyDBMode) {
			CJPLogger::WriteLog(_JP_LOG_DEBUG, "CDBBackupEngine :: Skipping extra databases definitions");
			return;
		}
		
		$db = CJVAbstract::getDatabase();
		$sql = 'SELECT * FROM `#__jp_extradb`';
		$db->setQuery($sql);
		$extraDefs = $db->loadAssocList();
		if( count($extraDefs) > 0 )
		{
			foreach( $extraDefs as $def )
			{
				$entry = array(
					'isJoomla' => false,
					'useFilters' => false,
					'host' => $def['host'],
					'port' => $def['port'],
					'username' => $def['username'],
					'password' => $def['password'],
					'database' => $def['database'],
					'dumpFile' => $def['id'] . '-' . $def['database'] . '.sql'
				);
				$this->_databaseList[] = $entry;
			}
		}
	}
}

?>