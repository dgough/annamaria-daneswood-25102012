<?php
/**
* @package		JoomlaPack
* @copyright	Copyright (C) 2006-2008 JoomlaPack Developers. All rights reserved.
* @version		1.1.1b2
* @license 		http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
* JoomlaPack is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
**/

// ensure this file is being included by a parent file - Joomla! 1.0.x and 1.5 compatible
(defined( '_VALID_MOS' ) || defined('_JEXEC')) or die( 'Direct Access to this location is not allowed.' );

class CUnlockPage
{
	/**
	 * Sigleton
	 *
	 * @return CUnlockPage
	 */
	function &getInstance()
	{
		static $instance;
		
		if( !is_object($instance) )
		{
			$instance = new CUnlockPage();
		}
		
		return $instance;
	}
	
	function echoHTML()
	{
		CJVAbstract::import('CLangManager');
		$lang =& CLangManager::getInstance();
		
		$errorStack = array(); // Initialize an errors stack 
		
		// Cleanup locks
		CJVAbstract::import('CCUBE');
		CCUBE::_cleanup();
		
		// Test for the existence of a default temporary folder
		if( !is_dir(JPComponentRoot . '/temp') )
		{
			// Temp dir doesn't exist; try to create one
			if(! @mkdir( JPComponentRoot . '/temp' ) )
			{
				$errorStack[] = $lang->get('unlock', 'cantcreatedir');
			} else {
				// Try creating a deafult .htaccess
				$htaccess = <<<END
Order deny,allow
Deny from all
END;
				$fp = @fopen( JPComponentRoot . '/temp/.htaccess' );
				if( $fp === false )
				{
					$errorStack[] = $lang->get('unlock', 'cantcreatehtaccess');
				} else {
					@fputs( $fp, $htaccess );
					@fclose( $fp );
				}
			}
			
		}
		
		// Get some more HTML fragments
		echo CJPHTML::getAdminHeadingHTML( $lang->get('cpanel', 'unlock') );
		
		?>
		<p>
		<?php
			if( count($errorStack) == 0 ) {
				echo $lang->get('unlock','done');
			} else {
				foreach( $errorStack as $error )
				{
					echo "<p class=\"error\">$error</p>";
				}
			}
		?>
		</p>
		<?php
	}
}
?>