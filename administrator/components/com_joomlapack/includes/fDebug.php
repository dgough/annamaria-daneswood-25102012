<?php
/**
* @package		JoomlaPack
* @subpackage	BackendPages
* @copyright	Copyright (C) 2006-2008 JoomlaPack Developers. All rights reserved.
* @version		$Id$
* @license 		http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
* JoomlaPack is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
**/

// ensure this file is being included by a parent file - Joomla! 1.0.x and 1.5 compatible
(defined( '_VALID_MOS' ) || defined('_JEXEC')) or die( 'Direct Access to this location is not allowed.' );

/**
 * This page is used while developing for testing out portions of the JoomlaPack
 * code base. Upon reaching release status it is cleared.
 *
 */
class CTestPage
{
	/**
	 * Implements the Singleton pattern
	 *
	 * @return CTestPage
	 * @static
	 */
	function &getInstance()
	{
		static $instance;
		
		if( !is_object($instance) )
		{
			$instance = new CTestPage();
		}
		
		return $instance;
	}	

	function echoHTML()
	{
		CJVAbstract::import('CDBExclusionFilter');
		$test = new CDBExclusionFilter;
		$test->init();
		print_r($test->getFilters('database'));
	}
}

class CCUBE
{
	var $_OnlyDBMode = true;
	
	function &getInstance()
	{
		static $instance;
		
		if( !is_object($instance) )
		{
			$instance = new CCUBE();
		}
		
		return $instance;
	}	
	
}

?>