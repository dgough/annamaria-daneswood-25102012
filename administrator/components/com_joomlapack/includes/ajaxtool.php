<?php
/**
* @package		JoomlaPack
* @subpackage	AJAX
* @copyright	Copyright (C) 2006-2008 JoomlaPack Developers. All rights reserved.
* @version		$Id$
* @license 		http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
* JoomlaPack is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* 
* This file contains the PHP part of the AJAX interface
**/

// ensure this file is being included by a parent file - Joomla! 1.0.x and 1.5 compatible
(defined( '_VALID_MOS' ) || defined('_JEXEC')) or die( 'Direct Access to this location is not allowed.' );

sajax_init();
sajax_export(
	"tick", "ping", "dirSelectionHTML", "toggleDirFilter",
	"errorTrapReport", "deleteBackup", "toggleDBFilter", "DBSelectionHTML",
	"ResetDBEF", "FilterNonJoomlaTables", "testdatabase", "getDefaultOutputDirectory",
	"getDeafultTempDirectory", "getMultiDBList", "toggleMultiDBActive", "toggleFileFilter",
	"sffSelectionHTML"
);
sajax_handle_client_request();

// ===============================================================================================
// ===============================================================================================

/**
* Continues the procedure
* @param $forceStart boolean When set to true, forces the procedure to start over
*/
function tick( $forceStart = 0, $forceDBOnly = 0 ){
	CJVAbstract::import('CCUBE');

	if ( ($forceDBOnly > 0) && ($forceStart > 0) ) {
		$cube =& CCUBE::getInstance( true, true );
	} elseif ( $forceStart > 0 ) {
		$cube =& CCUBE::getInstance( true );
	} else {
		$cube =& CCUBE::getInstance();
	}
	
	$ret = $cube->tick();
	$cube->save();
	CJPLogger::WriteLog(_JP_LOG_DEBUG, "<-- Finished AJAX Call -->");

	return $ret;
}

/**
* Simple PING server for debugging purposes
* @return integer Always 1
*/
function ping()
{
	return 1;
}

/**
* JPSetErrorReporting will reset error reporting to only syntax and parse errors,
* storing the old value for later use. It'll also try to set an infinite time
* limit on the script. All this is required to avoid PHP messing output meant
* for AJAX client-side parsing which caused notorious timeouts.
* @deprecated 
*/
function JPSetErrorReporting(){
	global $JP_Error_Reporting;

	$JP_Error_Reporting = @error_reporting(E_ERROR | E_PARSE);
	#$JP_Error_Reporting = error_reporting(E_WARNING | E_ERROR | E_PARSE);
	@set_time_limit(0);
}

/**
* JPRestoreErrorReporing will restore error reporting. It'll also try to clear
* (erase) the output buffer, so that the script can send back only the intended
* result. All this is required to avoid PHP messing output meant for AJAX
* client-side parsing which caused notorious timeouts.
* @deprecated 
*/
function JPRestoreErrorReporing(){
	global $JP_Error_Reporting;

	@error_reporting($JP_Error_Reporting);
	@ob_clean();
}

function dirSelectionHTML( $root ){
	CJVAbstract::import('fDirExclusion');

	JPSetErrorReporting();
	
	$page =& CDirExclusionPage::getInstance();
	$out = $page->getDirSelectionHTML( $root );

	JPRestoreErrorReporing();

	return $out;
}

function toggleDirFilter( $root, $dir, $checked ){
	CJVAbstract::import('CDirExclusionFilter');

	JPSetErrorReporting();
	
	$def = new CDirExclusionFilter();
	$def->modifyFilter($root, $dir, $checked);
	
	JPRestoreErrorReporing();
	
	return 1;
}

function errorTrapReport( $badData ){
	JPSetErrorReporting();
	CJPLogger::WriteLog(_JP_LOG_ERROR, "Last operation failed. Server response:");
	CJPLogger::WriteLog(_JP_LOG_ERROR, htmlspecialchars($badData));
	JPRestoreErrorReporing();
	
	return 1;
}

function getLock()
{
	CJVAbstract::import('CJPTables');
	return CJPTables::ReadVar('CUBELock');
}

function deleteBackup( $filename )
{
	CJVAbstract::import('fBUAdmin');
	$page =& CBackupAdminPage::getInstance();
	return $page->deleteFile( $filename );
}

function toggleDBFilter( $myTable, $checked )
{
	CJVAbstract::import('CDBExclusionFilter');
	
	JPSetErrorReporting();
	
	$dbef = new CDBExclusionFilter();
	$dbef->modifyFilter($myTable, $checked);
	
	JPRestoreErrorReporing();
	
	return 1;
}

function DBSelectionHTML()
{
	CJVAbstract::import('fdbef');
	
	JPSetErrorReporting();
	
	$page =& CDatabaseExclusionPage::getInstance();
	$out = $page->getTablesHTML();
	
	JPRestoreErrorReporing();

	return $out;
}

function ResetDBEF()
{
	CJVAbstract::import('CDBExclusionFilter');
	
	JPSetErrorReporting();
	
	$dbef = new CDBExclusionFilter();
	$dbef->ResetDBFilters();
	
	JPRestoreErrorReporing();
	
	return 1;
}

function FilterNonJoomlaTables()
{
	CJVAbstract::import('CDBExclusionFilter');
	
	JPSetErrorReporting();
	
	$dbef = new CDBExclusionFilter();
	$dbef->ExcludeNonJoomla();
	
	JPRestoreErrorReporing();
	
	return 1;
}

function testdatabase($host, $port, $user, $pass, $database)
{
	CJVAbstract::import('fMultiDB');
	
	JPSetErrorReporting();
	
	$multidb =& CMultiDBPage::getInstance();
	$result = $multidb->testConnection($host, $port, $user, $pass, $database);
	
	JPRestoreErrorReporing();
	
	return $result;
}

function getDefaultOutputDirectory()
{
	return JPComponentRoot . '/temp';
}

function getDeafultTempDirectory()
{
	return JPComponentRoot . '/temp';
}

function getMultiDBList()
{
	CJVAbstract::import('fMultiDB');
	
	JPSetErrorReporting();
	
	$multidb =& CMultiDBPage::getInstance();
	$result = $multidb->getMultiDBList();
	
	JPRestoreErrorReporing();
	
	return $result;
}

function toggleMultiDBActive($id)
{
	CJVAbstract::import('fMultiDB');
	
	JPSetErrorReporting();
	
	$multidb =& CMultiDBPage::getInstance();
	$result = $multidb->toggleActive($id);
	
	JPRestoreErrorReporing();
	
	return $result;
}

function toggleFileFilter($myRoot, $myFile, $sCheckStatus)
{
	CJVAbstract::import('CSingleFileFilter');
	
	JPSetErrorReporting();
	
	$sff = new CSingleFileFilter();
	$sff->modifyFilter($myRoot, $myFile, $sCheckStatus);
	
	JPRestoreErrorReporing();
	
	return 1;
}

function sffSelectionHTML( $myRoot )
{
	CJVAbstract::import('fFileExclusion');
	
	JPSetErrorReporting();
	
	$page =& CFileExclusionPage::getInstance();
	$out = $page->getFileSelectionHTML( $myRoot );
	
	JPRestoreErrorReporing();
	
	return $out;
}
?>