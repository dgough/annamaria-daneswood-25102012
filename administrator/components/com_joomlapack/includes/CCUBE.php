<?php
/**
* @package		JoomlaPack
* @subpackage	CUBE
* @copyright	Copyright (C) 2006-2008 JoomlaPack Developers. All rights reserved.
* @version		$Id$
* @license 		http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
* JoomlaPack is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
**/

// ensure this file is being included by a parent file - Joomla! 1.0.x and 1.5 compatible
(defined( '_VALID_MOS' ) || defined('_JEXEC')) or die( 'Direct Access to this location is not allowed.' );

// Include the JoomlaPack database variable storage utility class and CUBE engine classes
CJVAbstract::import('CJPTables');
CJVAbstract::import('CFilelistEngine');
CJVAbstract::import('CInstallerDeploymentEngine');
CJVAbstract::import('CDBBackupEngine');
CJVAbstract::import('CPackerEngine');
CJVAbstract::import('CFilterManager');

/**
 * The backup engine exposed interface class. Takes care for all the backup steps!
 *
 */
class CCUBE {
	/**
	 * Current domain of operation
	 *
	 * @var string
	 * @access private
	 */
	var $_currentDomain;

	/**
	 * Current step
	 *
	 * @var string
	 * @access private
	 */
	var $_currentStep;

	/**
	 * Current substep
	 *
	 * @var string
	 * @access private
	 */
	var $_currentSubstep;

	/**
	 * Current engine object executing work
	 *
	 * @var CJPEngineParts
	 * @access private
	 */
	var $_currentObject;

	/**
	 * Indicates if we are done
	 *
	 * @var boolean
	 * @access private
	 */
	var $_isFinished;

	/**
	 * The current error, if any
	 *
	 * @var string
	 * @access private
	 */
	var $_Error;

	/**
	 * Should we backup only the database contents and nothing more?
	 *
	 * @var boolean
	 * @access private
	 */
	var $_OnlyDBMode;
	
	/**
	 * An instance of the CFilterManager
	 *
	 * @var CFilterManager
	 * @access public
	 */
	var $FilterManager;

	/**
	 * A static method implementing the Singleton pattern, persisting in between sessions
	 *
	 * @param boolean $forceCreate Forcibly create a new instance of the object
	 * @param boolean $OnlyDBMode Create an object that only runs on DB mode.
	 * @return CCUBE
	 * @static
	 */
	function &getInstance( $forceCreate = false, $OnlyDBMode = false )
	{
		static $instance;
		
		if( is_object($instance) && (!$forceCreate) )
		{
			CJPLogger::WriteLog(_JP_LOG_DEBUG, "CCUBE::getInstance - Object in memory (OK)");
			// An object already exists in memory. Return this.
			return $instance;
		} else {
			if( $forceCreate )
			{
				// If we are forced to create, we fool the code below by making it think that the CUBE
				// doesn't exist in the database.
				CJPLogger::WriteLog(_JP_LOG_DEBUG, "CCUBE::getInstance - Forced creation, removing CUBEObject");
				$count = -1;
				CJPTables::DeleteVar('CUBEObject');
			} else {
				// Otherwise, look into the database for a stored CUBE object
				$count = CJPTables::CountVar('CUBEObject');
			}
			
			if ($count != 1)
			{
				// No stored object, we are forced to create a fresh object or something really
				// odd is going on with MySQL!
				CJPLogger::WriteLog(_JP_LOG_DEBUG, "CCUBE::getInstance - Not found in database, creating new instance");
				$instance = new CCUBE( $OnlyDBMode );
				return $instance;
			} else {
				// Load from db
				CJPLogger::WriteLog(_JP_LOG_DEBUG, "CCUBE::getInstance - Loading from database");
				$serialized = CJPTables::ReadVar('CUBEObject');
				$instance = unserialize( $serialized );
				return $instance;
			}
		}
	}

	/**
	 * Creates a new instance of the CUBE object and empties the temporary
	 * database tables
	 *
	 * @param boolean $OnlyDBMode Should we backup only the database contents?
	 * @return CCUBE
	 */
	function CCUBE( $OnlyDBMode = false )
	{
		$configuration =& CConfiguration::getInstance();
		
		$this->_OnlyDBMode = $OnlyDBMode;

		// Remove old entries from 'packvars' table
		CJPTables::DeleteMultipleVars('%CUBE%');

		// Initialize internal variables
		$this->_currentDomain = "init";		// Current domain of operation
		$this->_currentObject = null;		// Nullify current object
		$this->_isFinished = false;
		$this->_Error = false;
		
		$this->FilterManager = new CFilterManager();

		// Create a lock time stamp
		CJPTables::WriteVar( 'CUBELock', time() );

		CJPLogger::ResetLog();
		CJPLogger::WriteLog(_JP_LOG_INFO, "--------------------------------------------------------------------------------");
		CJPLogger::WriteLog(_JP_LOG_INFO, "JoomlaPack");
		CJPLogger::WriteLog(_JP_LOG_INFO, "Your one for all backup solution");
		CJPLogger::WriteLog(_JP_LOG_INFO, "--------------------------------------------------------------------------------");
		// PHP configuration variables are tried to be logged only for debug log levels
		if ($configuration->logLevel >= 3) {
			CJPLogger::WriteLog(_JP_LOG_INFO, "--- PHP Configuration Values ---" );
			CJPLogger::WriteLog(_JP_LOG_INFO, "PHP Version        :" . phpversion() );
			CJPLogger::WriteLog(_JP_LOG_INFO, "OS Version         :" . php_uname('s') );
			CJPLogger::WriteLog(_JP_LOG_INFO, "Safe mode          :" . ini_get("safe_mode") );
			CJPLogger::WriteLog(_JP_LOG_INFO, "Display errors     :" . ini_get("display_errors") );
			CJPLogger::WriteLog(_JP_LOG_INFO, "Disabled functions :" . ini_get("disable_functions") );
			CJPLogger::WriteLog(_JP_LOG_INFO, "open_basedir restr.:" . ini_get('open_basedir') );
			CJPLogger::WriteLog(_JP_LOG_INFO, "Max. exec. time    :" . ini_get("max_execution_time") );
			CJPLogger::WriteLog(_JP_LOG_INFO, "Memory limit       :" . ini_get("memory_limit") );
			if(function_exists("memory_get_usage"))
				CJPLogger::WriteLog(_JP_LOG_INFO, "Current mem. usage :" . memory_get_usage() );
			if(function_exists("gzcompress")) {
				CJPLogger::WriteLog(_JP_LOG_INFO, "GZIP Compression   : available (good)" );
			} else {
				CJPLogger::WriteLog(_JP_LOG_INFO, "GZIP Compression   : n/a (no compression)" );
			}
			CJPLogger::WriteLog(_JP_LOG_INFO, "--------------------------------------------------------------------------------");
		}
		
		// Reset errors.csv
		@unlink($configuration->OutputDirectory .  "/errors.csv");		

		//@unlink( $JPConfiguration->OutputDirectory . '/errors.csv' );  //$JPConfiguration not defined here
		
		if ($this->_OnlyDBMode) {
			CJPLogger::WriteLog(_JP_LOG_INFO, "JoomlaPack is starting a new database backup");
		} else {
			CJPLogger::WriteLog(_JP_LOG_INFO, "JoomlaPack is starting a new full site backup");
		}
	}

	/**
	 * The public interface of CCUBE, tick() does a single chunk of processing and returns a
	 * CUBE Return Array.
	 *
	 * @return array
	 */
	function tick(){
		if (!$this->_isFinished)
		{
			CJPTables::WriteVar( 'CUBELock', time() ); // Update lock timestamp
			switch( $this->_runAlgorithm() ){
				case 0:
					// more work to do, return OK
					CJPLogger::WriteLog(_JP_LOG_DEBUG, "CUBE :: More work required in domain '" . $this->_currentDomain);
					break;
				case 1:
					// Engine part finished
					CJPLogger::WriteLog(_JP_LOG_DEBUG, "CUBE :: Domain '" . $this->_currentDomain . "' has finished");
					$this->_getNextObject();
					if ($this->_currentDomain == "finale") {
						// We have finished the whole process.
						$this->_cleanup();
						CJPLogger::WriteLog(_JP_LOG_DEBUG, "CUBE :: Just finished");
					}
					break;
				case 2:
					CJPLogger::WriteLog(_JP_LOG_DEBUG, "CUBE :: Error occured in domain '" . $this->_currentDomain);
					// An error occured...
					$this->_cleanup();
					break;
			} // switch
			return $this->_makeCUBEArray();
		}
	}

	/**
	* Post work clean-up of files & database
	* @access private
	*/
	function _cleanup()
	{
		$database = CJVAbstract::getDatabase();
		$configuration =& CConfiguration::getInstance();

		CJPLogger::WriteLog(_JP_LOG_INFO, "Cleaning up");
		// Define which entries to keep in #__jp_packvars
		$keepInDB = array();

		// Clean installation files
		// ---------------------------------------------------------------------
		CCUBE::_unlinkRecursive($configuration->TempDirectory . "/installation");
		// Clean db backup files
		$folderPath = $configuration->TempDirectory;
		if ($configuration->AltInstaller->SQLDumpMode == "split") {
			$file1 = $folderPath . "/joomla.sql";
			$file2 = $folderPath . "/sample_data.sql";
		} else {
			$file1 = $folderPath . "/" .$configuration->AltInstaller->BaseDump;
			$file2 = $folderPath . "/" . $configuration->AltInstaller->SampleDump;
		}
		
		$file3 = $folderPath . '/databases.ini';

		// Make sure we don't accidentaly remove the whole temp directory!
		if ($file1 != $folderPath . '/') CCUBE::_unlinkRecursive($file1);
		if ($file2 != $folderPath . '/') CCUBE::_unlinkRecursive($file2);
		if ($file3 != $folderPath . '/') CCUBE::_unlinkRecursive($file3);

		// Clean database (also removes lock file)
		// ---------------------------------------------------------------------
		$sql = "SELECT `key` FROM #__jp_packvars";
		$database->setQuery( $sql );
		$keys = $database->loadResultArray();

		foreach($keys as $key){
			if (!in_array( $key, $keepInDB )) {
				CJPTables::DeleteVar( $key );
			}
		}

		unset( $keys );
	}

	/**
	* Recursively deletes file inside a directory
	* @param string $dirName Directory to delete
	* @access private
	* @static
	*/
	function _unlinkRecursive( $dirName ) {
		// Fix 1.1.1: require_once now refers an absolute path to avoid problems with open_basedir restrictions (by nicholas)
		CJVAbstract::import('CFSAbstraction');
		$FS = new CFSAbstraction();

		CJPLogger::WriteLog(_JP_LOG_DEBUG, "Recursively unlinking $dirName");

		if (is_file( $dirName )) {
			CJPLogger::WriteLog(_JP_LOG_DEBUG, "Unlinking $dirName - <b>THIS IS A FILE, NOT A DIR</b>");
			@unlink( $dirName );
		} elseif (is_dir( $dirName )) {
			// Get the contents of the directory
			$fileList = $FS->getDirContents( $dirName );
			if ($fileList === false) {
				// A non-browsable directory.
				CJPLogger::WriteLog(_JP_LOG_WARNING, "Can't delete directory $dirName. Check permissions.");
			} else {
				foreach($fileList as $fileDescriptor) {
					switch($fileDescriptor['type']) {
						case "dir":
							$myDir = $fileDescriptor['name'];
							if( (substr($myDir, -1, 2) == '..') || (substr($myDir, -1, 1) == '.') )
							{
								// Do nothing for '.' and '..' special dirs. 
							} else {
								CCUBE::_unlinkRecursive( $fileDescriptor['name'] );	
							}							
							break;
						case "file":
							//@unlink( $dirName . "/" . $fileDescriptor['name'] );
							@unlink( $fileDescriptor['name'] );
							break;
						// All other types (links, character devices etc) are ignored.
					}
				}
				@rmdir( $dirName );
			}
		}
	}

	/**
	* Single step algorithm. Runs the tick() function of the $_currentObject
	* until it finishes or produces an error, then returns the result array.
	* @return integer 1 if we finished correctly, 2 if error occured.
	* @access private
	*/
	function _algoSingleStep()
	{
		CJPLogger::WriteLog(_JP_LOG_DEBUG, "Single Stepping");
		$finished = false;
		$error = false;

		while( (!$finished) ){
			$result = $this->_currentObject->tick();
			$this->_currentDomain = $result['Domain'];
			$this->_currentStep = $result['Step'];
			$this->_currentSubstep = $result['Substep'];
			$error = false;
			if(isset($result['Error'])) $error = !($result['Error'] == "");//joostina pach
			$finished = $error ? true : !($result['HasRun']);
		} // while

		if (!$error) {
			CJPLogger::WriteLog(_JP_LOG_DEBUG, "Successful Fast algorithm on " . $this->_currentDomain);
		} else {
			CJPLogger::WriteLog(_JP_LOG_ERROR, "Failed Fast algorithm on " . $this->_currentDomain);
			CJPLogger::WriteLog(_JP_LOG_ERROR, $result['Error']);
		}
		$this->_Error = $error ? $result['Error'] : "";
		return $error ? 2 : 1;
	}

	/**
	* Multi-step algorithm. Runs the tick() function of the $_currentObject once
	* and returns.
	* @return integer 0 if more work is to be done, 1 if we finished correctly,
	* 2 if error eccured.
	* @access private
	*/
	function _algoMultiStep()
	{
		CJPLogger::WriteLog(_JP_LOG_DEBUG, "Multiple Stepping");
		$error = false;

		$result = $this->_currentObject->tick();
		$this->_currentDomain = $result['Domain'];
		$this->_currentStep = $result['Step'];
		$this->_currentSubstep = $result['Substep'];
		$error = false;
		if(isset($result['Error'])) $error = !($result['Error'] == "");//joostina pach
		$finished = $error ? true : !($result['HasRun']);

		if (!$error) {
			CJPLogger::WriteLog(_JP_LOG_DEBUG, "Successful Slow algorithm on " . $this->_currentDomain);
		} else {
			CJPLogger::WriteLog(_JP_LOG_ERROR, "Failed Slow algorithm on " . $this->_currentDomain);
			CJPLogger::WriteLog(_JP_LOG_ERROR, $result['Error']);
		}
		$this->_Error = $error ? $result['Error'] : "";
		return $error ? 2 : ( $finished ? 1 : 0 );
	}

	/**
	* Smart step algorithm. Runs the tick() function until we have consumed 75%
	* of the maximum_execution_time (minus 1 seconds) within this procedure. If
	* the available time is less than 1 seconds, it defaults to multi-step.
	* @return integer 0 if more work is to be done, 1 if we finished correctly,
	* 2 if error eccured.
	* @access private
	*/
	function _algoSmartStep()
	{
		CJPLogger::WriteLog(_JP_LOG_DEBUG, "Smart Stepping");

		// Get the maximum execution time
		$maxExecTime = ini_get("maximum_execution_time");
		$startTime = $this->_microtime_float();
		if ( ($maxExecTime == "") || ($maxExecTime == 0) ) {
			// If we have no time limit, set a hard limit of about 10 seconds
			// (safe for Apache and IIS timeouts, verbose enough for users)
			$maxExecTime = 14;
		}

		if ( $maxExecTime <= 1.75 ) {
			// If the available time is less than the trigger value, switch to
			// multi-step
			return $this->_algoMultiStep();
		} else {
			// All checks passes, this is a SmartStep-enabled case
			$maxRunTime = ($maxExecTime - 1) * 0.75;
			$runTime = 0;
			$finished = false;
			$error = false;

			// Loop until time's up, we're done or an error occured
			while( ($runTime <= $maxRunTime) && (!$finished) && (!$error) ){
				$result = $this->_currentObject->tick();
				$this->_currentDomain = $result['Domain'];
				$this->_currentStep = $result['Step'];
				$this->_currentSubstep = $result['Substep'];
				$error = false;
				if(isset($result['Error'])) $error = !($result['Error'] == "");
				$finished = $error ? true : !($result['HasRun']);

				$endTime = $this->_microtime_float();
				$runTime = $endTime - $startTime;
			} // while

			// Return the result
			if (!$error) {
				CJPLogger::WriteLog(_JP_LOG_DEBUG, "Successful Smart algorithm on " . $this->_currentDomain);
			} else {
				CJPLogger::WriteLog(_JP_LOG_ERROR, "Failed Smart algorithm on " . $this->_currentDomain);
				CJPLogger::WriteLog(_JP_LOG_ERROR, $result['Error']);
			}
			$this->_Error = $error ? $result['Error'] : "";
			return $error ? 2 : ( $finished ? 1 : 0 );
		}
	}

	/**
	* Runs the user-selected algorithm for the current engine
	* @return integer 0 if more work is to be done, 1 if we finished correctly,
	* 2 if error eccured.
	* @access private
	*/
	function _runAlgorithm(){
		$algo = $this->_selectAlgorithm();
		CJPLogger::WriteLog(_JP_LOG_DEBUG, "I have chosen $algo algorithm for " . $this->_currentDomain);

		switch( $algo ){
			case "single":
				// Single-step algorithm - fast but leads to timeouts in medium / big sites
				return $this->_algoSingleStep();
				break;
			case "multi":
				// Multi-step algorithm - slow but most compatible
				return $this->_algoMultiStep();
				break;
			case "smart":
				// SmartStep algorithm - best compromise between speed and compatibility
				return $this->_algoSmartStep();
				break;
			default:
				// No algorithm (null algorithm) for "init" and "finale" domains. Always returns success.
				//return $this->_isFinished ? 1 : 0;
				return 1;
		} // switch
	}

	/**
	* Selects the algorithm to use based on the current domain
	* @return string The algorithm to use
	* @access private
	*/
	function _selectAlgorithm(){
		$configuration =& CConfiguration::getInstance();
		
		switch( $this->_currentDomain )
		{
			case "init":
			case "finale":
				return "(null)";
				break;
			case "FileList":
				return $this->_OnlyDBMode ? "(null)" : $configuration->fileListAlgorithm;
				break;
			case "PackDB":
				return $configuration->dbAlgorithm;
				break;
			case "Packing":
				return $this->_OnlyDBMode ? "(null)" :$configuration->packAlgorithm;
				break;
			case "InstallerDeployment":
				return $this->_OnlyDBMode ? "(null)" :"single";
				break;
		}
	}

	/**
	* Returns the current microtime as a float
	* @return float Current microtime
	* @access private
	*/
	function _microtime_float()
	{
	    list($usec, $sec) = explode(" ", microtime());
	    return ((float)$usec + (float)$sec);
	}

	/**
	* Creates the next engine object based on the current execution domain
	* @return integer 0 = success, 1 = all done, 2 = error
	* @access private
	*/
	function _getNextObject(){
		// Kill existing object
		$this->_currentObject = null;
		// Try to figure out what object to spawn next
		switch( $this->_currentDomain )
		{
			case "init":
				// Next domain : Filelist creation
				CJPLogger::WriteLog(_JP_LOG_DEBUG, "Next domain --> Filelist");
				$this->_currentObject = new CFilelistEngine();
				$this->_currentDomain = "FileList";
				return 0;
				break;
			case "FileList":
				// Next domain : Installer Deployment
				CJPLogger::WriteLog(_JP_LOG_DEBUG, "Next domain --> Installer Deployment");
				$this->_currentObject = new CInstallerDeploymentEngine();
				$this->_currentDomain = "InstallerDeployment";
				return 0;
				break;
			case "InstallerDeployment":
				CJPLogger::WriteLog(_JP_LOG_DEBUG, "Next domain --> Database backup");
				// Next domain : Database backup
				$this->_currentObject = new CDBBackupEngine();
				$this->_currentDomain = "PackDB";
				return 0;
				break;
			case "PackDB":
				CJPLogger::WriteLog(_JP_LOG_DEBUG, "Next domain --> Packing");
				// Next domain : File packing
				$this->_currentObject = new CPackerEngine();
				$this->_currentDomain = "Packing";
				return 0;
				break;
			case "Packing":
				CJPLogger::WriteLog(_JP_LOG_DEBUG, "Next domain --> finale");
				// Next domain : none (done)
				$this->_currentDomain = "finale";
				return 1;
				break;
			case "finale":
			default:
				CJPLogger::WriteLog(_JP_LOG_DEBUG, "Next domain not applicable; already on 'finale'");
				return 1;
				break;
		}
	}

	/**
	* Creates the CUBE return array
	* @return array A CUBE return array with timestamp data
	* @access private
	*/
	function _makeCUBEArray(){
		$ret['HasRun'] = $this->_isFinished ? 0 : 1;
		$ret['Domain'] = $this->_currentDomain;
		$ret['Step'] = htmlentities( $this->_currentStep );
		$ret['Substep'] = htmlentities( $this->_currentSubstep );
		$ret['Error'] = htmlentities( $this->_Error );
		//$ret['Timestamp'] = $this->_microtime_float();
		return $ret;
	}
	
	/**
	 * Saves the current instance to the database 
	 */
	function save()
	{
		$serialized = serialize( $this );
		CJPTables::WriteVar( 'CUBEObject', $serialized );
		unset( $serialized );
	}
	
	function getCUBEArray()
	{
		return $this->_makeCUBEArray();
	}
}

/**
 * Timeout error handler
 */
function deadOnTimeOut()
{
	if( connection_status() >= 2 ) {
		CJPLogger::WriteLog(_JP_LOG_ERROR, "JoomlaPack has timed out. Please read the documentation.");
	}
}
register_shutdown_function("deadOnTimeOut");
?>