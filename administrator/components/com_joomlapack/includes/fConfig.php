<?php
/**
* @package		JoomlaPack
* @subpackage	BackendPages
* @copyright	Copyright (C) 2006-2008 JoomlaPack Developers. All rights reserved.
* @version		$Id$
* @license 		http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
* JoomlaPack is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
**/

// ensure this file is being included by a parent file - Joomla! 1.0.x and 1.5 compatible
(defined( '_VALID_MOS' ) || defined('_JEXEC')) or die( 'Direct Access to this location is not allowed.' );

// Require AJAX support
CJVAbstract::import('sajax');
CJVAbstract::import('ajaxtool');

/**
 * Configuration screen
 *
 */
class CConfigPage
{
	
	/**
	 * Implements the Singleton pattern
	 *
	 * @return CConfigPage
	 * @static
	 */
	function &getInstance()
	{
		static $instance;
		
		if( !is_object($instance) )
		{
			$instance = new CConfigPage();
		}
		
		return $instance;
	}
	
	/**
	 * Displays the HTML for this page, directly outputting it to the browser
	 */
	function echoHTML()
	{
		$lang =& CLangManager::getInstance();
		$configuration =& CConfiguration::getInstance();
		
		// Show top header
		echo CJPHTML::getAdminHeadingHTML( $lang->get('cpanel', 'config') );
		
		echo "<p align=\"center\">" . $lang->get('config', 'filestatus') . ' ' . CJPHTML::colorizeWriteStatus( $configuration->isConfigurationWriteable(), true ) . "</p>";
		
		$this->echoAJAXJS();
		
		?>
		<form action="index2.php" method="post" name="adminForm">
			<input type="hidden" name="option" value="<?php echo CJVAbstract::getParam('option','com_joomlapack') ; ?>" />
			<input type="hidden" name="act" value="config" />
			<input type="hidden" name="task" value="" />
			
			<table border="0" cellpadding="0" cellspacing="0" width="95%" class="adminform">
			<tr><td>
		<?php
			if( !defined('_JEXEC') ) {
				$tabs = new mosTabs(1);
				$tabs->startPane(1);
				$tabs->startTab( $lang->get('config','basic_options'), 'jpconfigbasic' );
			} else {
				jimport('joomla.html.pane');
				$tabs =& JPane::getInstance('sliders');
				echo $tabs->startPane('jpconfig');
				echo $tabs->startPanel( $lang->get('config','basic_options'), 'jpconfigbasic' );
			}
		?>
			<table cellpadding="4" cellspacing="0" border="0" width="95%" class="adminform">
				<tr align="center" valign="middle">
					<th width="20%">&nbsp;</th>
					<th width="20%"><?php echo $lang->get('config', 'option'); ?></th>
					<th width="60%"><?php echo $lang->get('config', 'cursettings'); ?></th>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><?php echo $lang->get('common', 'outdir'); ?></td>
					<td><input type="text" name="outdir" id="outdir" size="40" value="<?php echo $configuration->OutputDirectory; ?>" />
					<input type="button" value="<?php echo $lang->get('config', 'defaultdir'); ?>" onclick="getDefaultOutputDirectory();" />
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><?php echo $lang->get('common', 'tempdir'); ?></td>
					<td><input type="text" name="tempdir" id="tempdir" size="40" value="<?php echo $configuration->TempDirectory; ?>" />
					<input type="button" value="<?php echo $lang->get('config', 'defaultdir'); ?>" onclick="getDefaultTempDirectory();" />
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><?php echo $lang->get('config', 'tarname'); ?></td>
					<td><input type="text" name="tarname" size="40" value="<?php echo $configuration->TarNameTemplate;?>" /></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><?php echo $lang->get('config', 'loglevel'); ?></td>
					<td><?php $this->outputLogLevel( $configuration->logLevel ); // @todo Use common function ?></td>
				</tr>
			</table>
		<?php
			if( !defined('_JEXEC') ) {
				$tabs->endTab();
				$tabs->startTab( $lang->get('config','advanced'), 'jpconfigadvanced' );
			} else {
				echo $tabs->endPanel();
				echo $tabs->startPanel( $lang->get('config','advanced'), 'jpconfigadvanced' );
			}
			
		?>
			<table cellpadding="4" cellspacing="0" border="0" width="95%" class="adminform">	
				<tr align="center" valign="middle">
					<th width="20%">&nbsp;</th>
					<th width="20%"><?php echo $lang->get('config','option'); ?></th>
					<th width="60%"><?php echo $lang->get('config','cursettings'); ?></th>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><?php echo $lang->get('config','sqlcompat'); ?></td>
					<td><?php $this->outputSQLCompat( $configuration->MySQLCompat ); ?></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><?php echo $lang->get('config','fla_label'); ?></td>
					<td><?php $this->AlgorithmChooser( $configuration->fileListAlgorithm, "fileListAlgorithm" ); ?></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><?php echo $lang->get('config','dba_label'); ?></td>
					<td><?php $this->AlgorithmChooser( $configuration->dbAlgorithm, "dbAlgorithm" ); ?></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><?php echo $lang->get('config','pa_label') ?></td>
					<td><?php $this->AlgorithmChooser( $configuration->packAlgorithm, "packAlgorithm" ); ?></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><?php echo $lang->get('config','compress'); ?></td>
					<td><?php $this->outputCompressionMethodChooser( $configuration->CompressionMethod ); ?></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><?php echo $lang->get('config','altinstaller'); ?></td>
					<td><?php $this->AltInstallerChooser( $configuration->InstallerPackage ); ?></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><?php echo $lang->get('config','backupMethod') ?></td>
					<td><?php $this->backupMethodChooser( $configuration->backupMethod ); ?>
				</tr>
			</table>	
		<?php
			if( !defined('_JEXEC') ) {
				$tabs->endTab();
				$tabs->startTab( $lang->get('config','frontend'), 'jpconfigfrontend' );
			} else {
				echo $tabs->endPanel();
				echo $tabs->startPanel( $lang->get('config','frontend'), 'jpconfigfrontend' );
			}
		?>
			<table cellpadding="4" cellspacing="0" border="0" width="95%" class="adminform">	
				<tr align="center" valign="middle">
					<th width="20%">&nbsp;</th>
					<th width="20%"><?php echo $lang->get('config','option'); ?></th>
					<th width="60%"><?php echo $lang->get('config','cursettings'); ?></th>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><?php echo $lang->get('config','enablefeb'); ?></td>
					<td><input name="enableFrontend" type="checkbox" <?php echo ($configuration->enableFrontend ) ? 'checked' : ''; ?> /></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><?php echo $lang->get('config','secretprompt'); ?></td>
					<td><input name="secretWord" type="text" size="30" maxlength="30" value="<?php echo $configuration->secretWord; ?>" /></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td><?php echo $lang->get('config','secretinfo1'); ?><br />
					<?php echo $this->makeFEBURL(); ?>
					</td>
				</tr>
			</table>
		<?php
			if( !defined('_JEXEC') ) {
				$tabs->endTab();
				$tabs->startTab( $lang->get('config','magic'), 'jpconfigmagic' );
			} else {
				echo $tabs->endPanel();
				echo $tabs->startPanel( $lang->get('config','magic'), 'jpconfigmagic' );
			}
		?>
			<table cellpadding="4" cellspacing="0" border="0" width="95%" class="adminform">	
				<tr align="center" valign="middle">
					<th width="20%">&nbsp;</th>
					<th width="20%"><?php echo $lang->get('config','option'); ?></th>
					<th width="60%"><?php echo $lang->get('config','cursettings'); ?></th>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><?php echo $lang->get('config','mnRowsPerStep'); ?></td>
					<td><input name="mnRowsPerStep" type="text" value="<?php echo $configuration->mnRowsPerStep; ?>" /></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><?php echo $lang->get('config','mnMaxFragmentSize'); ?></td>
					<td><input name="mnMaxFragmentSize" type="text" value="<?php echo $configuration->mnMaxFragmentSize; ?>" /></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><?php echo $lang->get('config','mnMaxFragmentFiles'); ?></td>
					<td><input name="mnMaxFragmentFiles" type="text" value="<?php echo $configuration->mnMaxFragmentFiles; ?>" /></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><?php echo $lang->get('config','mnZIPForceOpen'); ?></td>
					<td><input name="mnZIPForceOpen" type="checkbox" <?php echo ($configuration->mnZIPForceOpen ) ? 'checked' : ''; ?> /></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><?php echo $lang->get('config','mnZIPCompressionThreshold'); ?></td>
					<td><input name="mnZIPCompressionThreshold" type="text" value="<?php echo $configuration->mnZIPCompressionThreshold; ?>" /></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><?php echo $lang->get('config','mnZIPDirReadChunk'); ?></td>
					<td><input name="mnZIPDirReadChunk" type="text" value="<?php echo $configuration->mnZIPDirReadChunk; ?>" /></td>
				</tr>
				
			</table>
		<?php
			if( !defined('_JEXEC') ) {
				$tabs->endTab();
				$tabs->endPane();
			} else {
				echo $tabs->endPanel();
				echo $tabs->endPane();
			}
		?>
			</td></tr></table>
			</form>
		<?php
	}
	
	/**
	 * Displays an SQL compatibility option combobox
	 *
	 * @param string $sqlcompat
	 */
	function outputSQLCompat( $sqlcompat ) {
		$lang =& CLangManager::getInstance();
		
		$options = array();
		
		if( !defined('_JEXEC') )
		{
			$options[] = mosHTML::makeOption('compat', $lang->get('config','compat') );
			$options[] = mosHTML::makeOption('default', $lang->get('config','default') );
			
			echo mosHTML::selectList( $options, 'sqlcompat', '', 'value', 'text', $sqlcompat );
		} else {
			$options[] = JHTML::_('select.option', 'compat', $lang->get('config','compat') );
			$options[] = JHTML::_('select.option', 'default', $lang->get('config','default') );
			
			echo JHTML::_('select.genericlist', $options, 'sqlcompat', '', 'value', 'text', $sqlcompat );
		}
	}
	
	/**
	 * Displays a combobox for selecting archive type
	 *
	 * @param string $compressionMethod
	 */
	function outputCompressionMethodChooser( $compressionMethod ) {
		$lang =& CLangManager::getInstance();
		
		$options = array();
		
		if( !defined('_JEXEC') )
		{
			$options[] = mosHTML::makeOption('zip', $lang->get('config','zip') );
			$options[] = mosHTML::makeOption('jpa', $lang->get('config','jpa') );
			
			echo mosHTML::selectList( $options, 'compress', '', 'value', 'text', $compressionMethod );
		} else {
			$options[] = JHTML::_('select.option', 'zip', $lang->get('config','zip') );
			$options[] = JHTML::_('select.option', 'jpa', $lang->get('config','jpa') );
			
			echo JHTML::_('select.genericlist', $options, 'compress', '', 'value', 'text', $compressionMethod );
		}
	}
	
	/**
	 * Outputs a packing algorithm combobox
	 *
	 * @param string $strOption Current selection
	 * @param string $strName The name of the <select> tag
	 */
	function AlgorithmChooser( $strOption, $strName ) {
		$lang =& CLangManager::getInstance();
		
		$options = array();
		
		if( !defined('_JEXEC') )
		{
			$options[] = mosHTML::makeOption('single', $lang->get('config','single') );
			$options[] = mosHTML::makeOption('smart', $lang->get('config','smart') );
			$options[] = mosHTML::makeOption('multi', $lang->get('config','multi') );
			
			echo mosHTML::selectList( $options, $strName, '', 'value', 'text', $strOption );
		} else {
			$options[] = JHTML::_('select.option', 'single', $lang->get('config','single') );
			$options[] = JHTML::_('select.option', 'smart', $lang->get('config','smart') );
			$options[] = JHTML::_('select.option', 'multi', $lang->get('config','multi') );
			
			echo JHTML::_('select.genericlist', $options, $strName, '', 'value', 'text', $strOption );
		}	
	}
	
	/**
	 * Displays an installer selection combobox
	 *
	 * @param string $strOption Selected installer's key
	 */
	function AltInstallerChooser( $strOption ) {
		$configuration =& CConfiguration::getInstance();
		
		$altInstallers = $configuration->AltInstaller->loadAllDefinitions();
		
		if( !defined('_JEXEC') )
		{
			foreach ($altInstallers as $altInstaller) {
				$options[] = mosHTML::makeOption($altInstaller['meta'], $altInstaller['name'] );
			}
			
			echo mosHTML::selectList( $options, 'altInstaller', '', 'value', 'text', $strOption );
		} else {
			foreach ($altInstallers as $altInstaller) {
				$options[] = JHTML::_('select.option', $altInstaller['meta'], $altInstaller['name'] );
			}
			
			echo JHTML::_('select.genericlist', $options, 'altInstaller', '', 'value', 'text', $strOption );
		}
	}
	
	/**
	 * Displays a logging level combobox
	 *
	 * @param string $strOption Selected log level
	 */
	function outputLogLevel( $strOption ) {
		$lang =& CLangManager::getInstance();
		
		$options = array();
		
		if( !defined('_JEXEC') )
		{
			$options[] = mosHTML::makeOption('1', $lang->get('config','llerror') );
			$options[] = mosHTML::makeOption('2', $lang->get('config','llwarning') );
			$options[] = mosHTML::makeOption('3', $lang->get('config','llinfo') );
			$options[] = mosHTML::makeOption('4', $lang->get('config','lldebug') );
			$options[] = mosHTML::makeOption('0', $lang->get('config','llnone') );
			
			echo mosHTML::selectList( $options, 'logLevel', '', 'value', 'text', $strOption );
		} else {
			$options[] = JHTML::_('select.option', '1', $lang->get('config','llerror') );
			$options[] = JHTML::_('select.option', '2', $lang->get('config','llwarning') );
			$options[] = JHTML::_('select.option', '3', $lang->get('config','llinfo') );
			$options[] = JHTML::_('select.option', '4', $lang->get('config','lldebug') );
			$options[] = JHTML::_('select.option', '0', $lang->get('config','llnone') );
			
			echo JHTML::_('select.genericlist', $options, 'logLevel', '', 'value', 'text', $strOption );
		}
	}
	
	function makeFEBURL()
	{
		$out = "<tt>";
		if( defined('_JEXEC') )
		{
			$out .= substr_replace(JURI::root(), '', -1, 1);
		} else {
			global $mosConfig_live_site;
			$out .= $mosConfig_live_site;
		}
		$out .= '/index2.php?option=com_joomlapack&act=fullbackup&key=<b>secret_key</b>&no_html=1';
		$out .= "</tt>";
		return $out;
	}

	function echoAJAXJS()
	{
		echo <<<ENDFRAGMENT
		<script language="JavaScript" type="text/javascript">
				/*
				 * (S)AJAX Library code
				 */
ENDFRAGMENT;
		sajax_show_javascript();
		echo <<<ENDFRAGMENT
		
				function getDefaultOutputDirectory()
				{
					x_getDefaultOutputDirectory( getDefaultOutputDirectory_cb )
				}
				
				function getDefaultOutputDirectory_cb( myRet )
				{
					document.getElementById("outdir").value = myRet;
				}

				function getDefaultTempDirectory()
				{
					x_getDeafultTempDirectory( getDeafultTempDirectory_cb )
				}
				
				function getDeafultTempDirectory_cb( myRet )
				{
					document.getElementById("tempdir").value = myRet;
				}
				
		</script>
ENDFRAGMENT;
	}

	function backupMethodChooser( $activeMethod )
	{
		$lang =& CLangManager::getInstance();
		
		$options = array();
		
		if(!defined('_JEXEC'))
		{
			$options[] = mosHTML::makeOption('ajax', $lang->get('config','methodajax') );
			$options[] = mosHTML::makeOption('redirect', $lang->get('config','methodjsredirect') );
			
			echo mosHTML::selectList( $options, 'backupMethod', '', 'value', 'text', $activeMethod);
		} else {
			$options[] = JHTML::_('select.option', 'ajax', $lang->get('config','methodajax') );
			$options[] = JHTML::_('select.option', 'redirect', $lang->get('config','methodjsredirect') );
			
			echo JHTML::_( 'select.genericlist', $options, 'backupMethod', '', 'value', 'text', $activeMethod);
		}
	}
}
?>