<?php
/**
* @package		JoomlaPack
* @subpackage	BackendPages
* @copyright	Copyright (C) 2006-2008 JoomlaPack Developers. All rights reserved.
* @version		$Id$
* @license 		http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
* JoomlaPack is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
**/

// ensure this file is being included by a parent file - Joomla! 1.0.x and 1.5 compatible
(defined( '_VALID_MOS' ) || defined('_JEXEC')) or die( 'Direct Access to this location is not allowed.' );

CJVAbstract::import('sajax');
CJVAbstract::import('ajaxtool');

/**
 * Multiple (extra) databases management page
 * 
 * Allows the user to define extra databases to be dumped, as part of the full site backup process.
 * This makes possible to backup a site consisting of a Joomla! installation along with other scripts
 * which might use their own databases.
 *
 */
class CMultiDBPage
{
	/**
	 * Implements the Singleton pattern
	 *
	 * @return CMultiDBPage
	 * @static
	 */
	function &getInstance()
	{
		static $instance;
		
		if( !is_object($instance) )
		{
			$instance = new CMultiDBPage();
		}
		
		return $instance;
	}
	
	/**
	 * Displays the HTML for this page.
	 * 
	 * Depending on the task, it will call the appropriate private methods to handle and display
	 * the data
	 *
	 */
	function echoHTML()
	{
		$task = CJVAbstract::getParam('task', 'view');
		switch( $task )
		{
			case 'edit':
			case 'new':
				$this->_echoEditHTML();
				break;
			case 'save':
				$this->_saveFromPOST();
				$this->_echoViewHTML();
				break;
			case 'delete':
				$this->_deleteFromPOST();
				$this->_echoViewHTML();
				break;
			case 'view':
			default:
				$this->_echoViewHTML();
				break;
		}
	}
	
	/**
	 * Displays a list of all extra database definitions
	 *
	 */
	function _echoViewHTML()
	{
		$lang =& CLangManager::getInstance();		
		$this->_echoJavaScript(); // Get JavaScript for AJAX calls
		echo CJPHTML::getAdminHeadingHTML( $lang->get('cpanel', 'multidb') );
		
		?>
		<form action="index2.php" method="post" name="adminForm">
			<input type="hidden" name="option" value="<?php echo CJVAbstract::getParam('option','com_joomlapack') ; ?>" />
			<input type="hidden" name="act" value="multidb" />
			<input type="hidden" name="task" id="task" value="" />
			<input type="hidden" name="id" id="id" value="" />
		</form>

		<div id="multidblist">
			&nbsp;
		</div>
		
		<script type="text/javascript">
			do_getMultiDBList();
		</script>
		<?php
	}
	
	/**
	 * Displays the edit form of an existing or newly created record 
	 *
	 */
	function _echoEditHTML()
	{
		// Get the task and find the item's id
		$task = CJVAbstract::getParam('task');
		$id = CJVAbstract::getParam('id', null);
		$id = ($task == 'new') ? null : (is_numeric($id) ? $id : null);
		
		// If it is not a new record, try to fetch it. If fetch is imposible, resort to new record
		if( !is_null($id) )
		{
			$row = $this->_getRow($id);
			if( count($row) == 0 )
			{
				$id = null;
				$row = array();
			}
		} else {
			$row = array();
		}
		
		// Start outputting the page
		$lang =& CLangManager::getInstance();		
		$this->_echoJavaScript(); // Get JavaScript for AJAX calls
		echo CJPHTML::getAdminHeadingHTML( $lang->get('cpanel', 'multidb') );

		?>
		<form action="index2.php" method="post" name="adminForm">
			<input type="hidden" name="option" value="<?php echo CJVAbstract::getParam('option','com_joomlapack') ; ?>" />
			<input type="hidden" name="act" value="multidb" />
			<input type="hidden" name="task" value="" />
			<input type="hidden" name="id" value="<?php echo is_null($id) ? '' : $id ?>" />
			<table cellpadding="4" cellspacing="0" border="0" width="95%" class="adminform">
				<tr>
					<td><?php echo $lang->get('multidb', 'host') ?></td>
					<td><input type="text" name="host" id="host" value="<?php echo $row['host']; ?>" /></td>					
				</tr>
				<tr>
					<td><?php echo $lang->get('multidb', 'port') ?></td>
					<td><input type="text" name="port" id="port" value="<?php echo $row['port']; ?>" /></td>					
				</tr>
				<tr>
					<td><?php echo $lang->get('multidb', 'username') ?></td>
					<td><input type="text" name="username" id="username" value="<?php echo $row['username']; ?>" /></td>					
				</tr>
				<tr>
					<td><?php echo $lang->get('multidb', 'password') ?></td>
					<td><input type="text" name="password" id="password" value="<?php echo $row['password']; ?>" /></td>					
				</tr>
				<tr>
					<td><?php echo $lang->get('multidb', 'database') ?></td>
					<td><input type="text" name="database" id="database" value="<?php echo $row['database']; ?>" /></td>					
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>
					<input type="button" value="<?php echo $lang->get('multidb','testdb'); ?>" onclick="testdb();" />
					</td>
				</tr>
			</table>
		<?php
	}
	
	/**
	 * Handles data POSTed to the form and updates the database. If no error occured, it will
	 * reroute to list view of all records.
	 *
	 */
	function _saveFromPOST()
	{
		$id			= CJVAbstract::getParam('id', '');
		$host		= CJVAbstract::getParam('host','');
		$port		= CJVAbstract::getParam('port','');
		$user		= CJVAbstract::getParam('username','');
		$database	= CJVAbstract::getParam('database','');
		$pass		= CJVAbstract::getParam('password','');
		$active		= CJVAbstract::getParam('active', 'on');
		
		$active = ($active == 'on') || ($active === true) || ($active == 'checked');
		$new = is_null($id) || ($id == '');
		
		if( $new )
		{
			$this->addRow($host,$port,$user,$pass,$database);
		} else {
			$this->modifyRow($id,$host,$port,$user,$pass,$database,$active);
		}
	}
	
	/**
	 * Deletes a selected row
	 *
	 */
	function _deleteFromPOST()
	{
		$id			= CJVAbstract::getParam('id', '');
		
		$db = CJVAbstract::getDatabase();
		$sql = "DELETE FROM `#__jp_extradb` WHERE `id` = '$id'";
		$db->setQuery( $sql );
		$db->query();
	}
	
	/**
	 * Adds a new row to the database
	 *
	 * @param string $host MySQL server's hostname
	 * @param string $port MySQL server's port
	 * @param string $user MySQL server's username
	 * @param string $pass MySQL server's password
	 * @param string $database MySQL database
	 */
	function addRow($host, $port, $user, $pass, $database)
	{
		$db = CJVAbstract::getDatabase();
		
		$sql = "INSERT INTO #__jp_extradb (`host`, `port`, `username`, `password`, `database`, `active`) VALUES ('$host', '$port', '$user', '$pass', '$database', '-1')";
		$db->setQuery( $sql );
		$db->query();
	}
	
	/**
	 * Modifies an exisiting row
	 *
	 * @param integer $id The ID of the row to update
	 * @param string $host MySQL server's hostname
	 * @param string $port MySQL server's port
	 * @param string $user MySQL server's username
	 * @param string $pass MySQL server's password
	 * @param string $database MySQL database
	 * @param boolean $active True to set the record active
	 */
	function modifyRow($id, $host, $port, $user, $pass, $database, $active)
	{
		$db = CJVAbstract::getDatabase();

		$active = $active ? '-1' : '0';
		$sql = "UPDATE #__jp_extradb SET `host` = '$host', `port` = '$port', `username` = '$user', `password` = '$pass', `database` = '$database', `active` = '$active' WHERE `id` = '$id'";
		$db->setQuery( $sql );
		$db->query();
	}
	
	/**
	 * Toggles the active status of a record
	 *
	 * @param integer $id The row id (`id` column)
	 * @return boolean True on success
	 */
	function toggleActive( $id )
	{
		if( !is_numeric($id) ) return false;
		
		$db = CJVAbstract::getDatabase();
		
		$sql = "SELECT `active` FROM `#__jp_extradb` WHERE `id` = '$id'";
		$db->setQuery($sql);
		$active = $db->loadResult();
		
		if( is_null($active) ) return false;
		
		// Invert active status
		$active = ($active == 0);
		$active = $active ? '-1' : '0';
		
		$sql = "UPDATE `#__jp_extradb` SET `active` = '$active' WHERE `id` = '$id'";
		$db->setQuery($sql);
		$db->query();
		
		return true;
	}
	
	/**
	 * Tests connection to a MySQL database using the provided settings
	 *
	 * @param string $host MySQL server's hostname
	 * @param string $port MySQL server's port
	 * @param string $user MySQL server's username
	 * @param string $pass MySQL server's password
	 * @param string $database MySQL database
	 * @return boolean|string True on success, error description on failure
	 */
	function testConnection($host, $port, $user, $pass, $database)
	{
		$host = $host . ($port != '' ? ":$port" : '');
		
		if( !defined('_JEXEC') )
		{
			$database = new database($host, $user, $pass, $database, '', false);
			if( $database->getErrorNum() > 0) return false;
		} else {
			jimport('joomla.database.database');
			jimport( 'joomla.database.table' );
			$conf =& JFactory::getConfig();
			$driver 	= $conf->getValue('config.dbtype');
			$options	= array ( 'driver' => $driver, 'host' => $host, 'user' => $user, 'password' => $pass, 'database' => $database, 'prefix' => '' );
			
			$database =& JDatabase::getInstance( $options );
			
			if ( JError::isError($database) ) return false;
			if ($database->getErrorNum() > 0) return false;
		}
		
		return true;
	}
	
	/**
	 * Returns a list of associative arrays representing the database records
	 *
	 * @return array
	 */
	function _getRowList()
	{
		$db = CJVAbstract::getDatabase();
		
		$sql = 'SELECT * FROM `#__jp_extradb`';
		$db->setQuery($sql);
		return $db->loadAssocList();
	}
	
	/**
	 * Loads a record into an associative array
	 *
	 * @param integer $id The ID of the requested row
	 * @return array
	 */
	function _getRow($id)
	{
		$db = CJVAbstract::getDatabase();
		
		$sql = 'SELECT * FROM `#__jp_extradb` WHERE id = ' . $id;
		$db->setQuery($sql);
		return $db->loadAssoc();
	}
	
	/**
	 * Outputs the JavaScript required for (S)AJAX to work
	 *
	 */
	function _echoJavaScript()
	{
		$lang =& CLangManager::getInstance();
?>
		<script type="text/javascript">
		<?php sajax_show_javascript(); ?>
		
		function testdb()
		{
			var host = document.getElementById("host").value;
			var port = document.getElementById("port").value;
			var username = document.getElementById("username").value;
			var password = document.getElementById("password").value;
			var database = document.getElementById("database").value;

			x_testdatabase( host, port, username, password, database, testdb_cb ); 
		}
		
		function testdb_cb( myRet )
		{
			if( myRet == true )
			{
				alert('<?php echo $lang->get('multidb','testok'); ?>');
			} else {
				alert('<?php echo $lang->get('multidb','testnotok'); ?>');
			}
		}
		
		function do_getMultiDBList()
		{
			x_getMultiDBList( do_getMultiDBList_cb );
		}
		
		function do_getMultiDBList_cb( myRet )
		{
			document.getElementById("multidblist").innerHTML = myRet;
		}
		
		function ToggleActive( id )
		{
			x_toggleMultiDBActive( id, ToggleActive_cb );
		}
		
		function ToggleActive_cb( myRet )
		{
			do_getMultiDBList();
		}
		
		function editRow( id )
		{
			document.getElementById("id").value = id;
			submitbutton('edit');
		}
		
		function deleteRow( id )
		{
			document.getElementById("id").value = id;
			submitbutton('delete');
		}
		 
		</script>
<?php
	}

	/**
	 * Returns the admin list of multidb definitions
	 *
	 * @return string The HTML of the table
	 */
	function getMultiDBList()
	{
		CJVAbstract::import('CLangManager');
		$lang =& CLangManager::getInstance();
		
		$lang_active = $lang->get('multidb','active');
		$lang_host = $lang->get('multidb','host');
		$lang_database = $lang->get('multidb','database');
		
		$out = '';
		$out .= <<<END
			<table class="adminlist">
			<thead>
			<tr>
				<th width="5">#</th>
				<th class="title" width="100px">$lang_active</th>
				<th class="title">$lang_host</th>
				<th class="title">$lang_database</th>
				<th width="80" align="right"></th>
				<th width="80" align="right"></th>
			</tr>
			</thead>
			<tbody>
END;
		$allRows = $this->_getRowList();
		if( count($allRows) > 0 )
		{
			foreach( $allRows as $row )
			{
				$checked = $row['active'] ? " checked = \"true\" " : "";
				
				$row_id = $row['id'];
				$row_host = $row['host'];
				$row_database = $row['database'];
				$lang_edit = $lang->get('multidb','edit');
				$lang_delete = $lang->get('multidb','delete');
				
				$out .= <<<ENDTHISROW
			<tr>
				<td>$row_id</td>
				<td><input type="checkbox" $checked onclick="ToggleActive($row_id);" /></td>
				<td>$row_host</td>
				<td>$row_database</td>
				<td><a href="javascript:editRow($row_id);">$lang_edit</a></td>
				<td><a href="javascript:deleteRow($row_id);">$lang_delete</a></td>
			</tr>
ENDTHISROW;
			}
		} else {
			$out .= '<tr><td colspan="6">' . $lang->get('multidb', 'norecords') . '</td></tr>';
		}
		
		$out .= <<<END
			</tbody>
		</table>
END;
		
		return $out;
	}
}
?>