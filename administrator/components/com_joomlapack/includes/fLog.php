<?php

/**
* @package		JoomlaPack
* @subpackage	BackendPages
* @copyright	Copyright (C) 2006-2008 JoomlaPack Developers. All rights reserved.
* @version		$Id$
* @license 		http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
* JoomlaPack is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
**/

// ensure this file is being included by a parent file - Joomla! 1.0.x and 1.5 compatible
(defined( '_VALID_MOS' ) || defined('_JEXEC')) or die( 'Direct Access to this location is not allowed.' );

class CLogViewerPage
{
	/**
	 * Implements the Singleton pattern
	 *
	 * @return CLogViewerPage
	 * @static
	 */
	function &getInstance()
	{
		static $instance;
		
		if( !is_object($instance) )
		{
			$instance = new CLogViewerPage();
		}
		
		return $instance;
	}
	
	/**
	 * Displays the HTML for this page
	 * 
	 */
	function echoHTML()
	{
		$lang =& CLangManager::getInstance();
		$configuration =& CConfiguration::getInstance();
		
		$option = CJVAbstract::getParam('option','com_joomlapack');
		
		// Show top header
		echo CJPHTML::getAdminHeadingHTML( $lang->get('log', 'logbanner') );
		
		echo "<p><a href=\"index2.php?option=$option&act=dllog&no_html=1\">". $lang->get('log','downloadtext') ."</a></p>";
		
		echo '<div style="text-align: left; padding: 0.5em; background-color: #EEEEFE; border: thin solid black; margin: 0.5em; font-family: Courier New, monospace; font-size: medium;">';
		CJPLogger::VisualizeLogDirect();
		echo '</div>';
	}
}
?>