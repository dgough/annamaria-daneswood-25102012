<?php
/**
* @version $Id: admin.banners.php 4556 2006-08-18 18:29:18Z stingrey $
* @package Joomla
* @subpackage Banners
* @copyright Copyright (C) 2005 Open Source Matters. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );

// ensure user has access to this function
if (!($acl->acl_check( 'administration', 'edit', 'users', $my->usertype, 'components', 'all' )| $acl->acl_check( 'administration', 'edit', 'users', $my->usertype, 'components', 'com_bannersmanager' ))) {
	mosRedirect( 'index2.php', _NOT_AUTH );
}

require_once( $mainframe->getPath( 'admin_html' ) );
require_once( $mainframe->getPath( 'class' ) );
if (file_exists($mosConfig_absolute_path .'/components/'.$option.'/lang/lang_' . $mosConfig_lang . '.php'))
	include_once( $mosConfig_absolute_path .'/components/'.$option.'/lang/lang_' . $mosConfig_lang . '.php' );
else
	include_once( $mosConfig_absolute_path .'/components/'.$option.'/lang/lang_english.php' );

$cid = mosGetParam( $_POST, 'cid', array(0) );
mosArrayToInts( $cid );

if (!is_array( $cid )) {
	$cid = array(0);
}

switch($act)
{
	case "group":
		switch ($task) {
			case 'new':
				editGroup( null, $option );
				break;
		
			case 'save':
				saveGroup( $task );
				break;
		
			case 'edit':
				if ((isset($id))&&($id != 0)&&($id != ""))
					editGroup( $id, $option );
				else
					editGroup( $cid[0], $option );
				break;
		
			case 'remove':
				removeGroup( $cid );
				break;
		
			default:
				viewGroups( $option );
				break;
		}
		break;
		
	case "client":
		switch ($task) {
			case 'new':
				editClient( null, $option );
				break;
		
			case 'save':
				saveClient( $task );
				break;
		
			case 'edit':
				if ((isset($id))&&($id != 0)&&($id != ""))
					editClient( $id, $option );
				else
					editClient( $cid[0], $option );
				break;
		
			case 'remove':
				removeClient( $cid );
				break;
		
			default:
				viewClients( $option );
				break;
		}
		break;
		
	case "banner":
	default: 
		switch ($task) {
			case 'new':
				editBanner( null, $option );
				break;
		
			case 'save':
			case 'apply':
			case 'resetClicks':
			case 'resetImps':
				saveBanner( $task );
				break;
		
			case 'edit':
				if ((isset($id))&&($id != 0)&&($id != ""))
					editBanner( $id, $option );
				else
					editBanner( $cid[0], $option );
				break;
		
			case 'remove':
				removeBanner( $cid );
				break;
		
			case 'publish':
				publishBanner( $cid,1 );
				break;
		
			case 'unpublish':
				publishBanner( $cid, 0 );
				break;
				
			case 'orderup':
			orderBanner( intval( $cid[0]), -1, $option );
			break;
	
			case 'orderdown':
			orderBanner( intval( $cid[0] ), 1, $option );
			break;
			
			case 'saveorder':
			saveOrder( $cid ,$option);
			break;
			
			case 'importbanners':
			importBanners($option);
		
			default:
				viewBanners( $option );
				break;
		}
		
}

function viewGroups( $option ) {
	global $database, $mainframe, $mosConfig_list_limit;
		
	$limit 				= intval( $mainframe->getUserStateFromRequest( "viewlistlimit", 'limit', $mosConfig_list_limit ) );
	$limitstart 		= intval( $mainframe->getUserStateFromRequest( "view{$option}limitstart", 'limitstart', 0 ) );
	
	// get the total number of records
	$query = "SELECT COUNT(*)"
	. "\n FROM #__bannersmanager_group"
	;
	$database->setQuery( $query );
	$total = $database->loadResult();

	require_once( $GLOBALS['mosConfig_absolute_path'] . '/administrator/includes/pageNavigation.php' );
	$pageNav = new mosPageNav( $total, $limitstart, $limit );
	$query = "SELECT *"
	. "\n FROM `#__bannersmanager_group`"
	. "\n WHERE 1 "
	;
	
	$database->setQuery( $query,$pageNav->limitstart, $pageNav->limit);
	$rows = $database->loadObjectList();
	
	$query = "SELECT m.title,m.position,m.params,m.id"
	. "\n FROM #__modules AS m "
	. "\n WHERE m.module = 'mod_bannersmanager'"
	;
	$database->setQuery( $query );
	$mods = $database->loadObjectList();

	HTML_bannersmanager::showGroups( $rows, $mods, $pageNav, $option );
}

function editGroup( $id, $option ) {
	global $database, $my;
	$lists = array();
	$row = new bannersManagerGroup($database);
	if (isset($id))
		$row->load( (int)$id );
		
	HTML_bannersmanager::groupForm( $row, $option );
}

function saveGroup( $task ) {
	global $database,$mosConfig_absolute_path;

	$row = new bannersManagerGroup($database);

	$msg = 'Saved Group info';
	if (!$row->bind( $_POST )) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
		
	if (!$row->store()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	
	mosRedirect( 'index2.php?option=com_bannersmanager&act=group', $msg );
}

function removeGroup( $cid ) {
	global $database,$mosConfig_absolute_path;
	
	if (count( $cid )) {
		$cids = implode( ',', $cid );
			
		$query = "SELECT count(*) FROM #__bannersmanager_banner"
		. "\n WHERE groupid IN ( $cids )";
		$database->setQuery( $query );
		$total = $database->loadResult();
		if ($total > 0)
		{
			mosRedirect( 'index2.php?option=com_bannersmanager&act=group', BANNERSMANAGER_CANNOT_DELETE_GROUP );
		}
		
		$query = "DELETE FROM #__bannersmanager_group"
		. "\n WHERE id IN ( $cids )"
		;
		$database->setQuery( $query );
		if (!$database->query()) {
			echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
		}
	}
	mosRedirect( 'index2.php?option=com_bannersmanager&act=group' );
}

function viewClients( $option ) {
	global $database, $mainframe, $mosConfig_list_limit;
		
	$limit 				= intval( $mainframe->getUserStateFromRequest( "viewlistlimit", 'limit', $mosConfig_list_limit ) );
	$limitstart 		= intval( $mainframe->getUserStateFromRequest( "view{$option}limitstart", 'limitstart', 0 ) );
	
	// get the total number of records
	$query = "SELECT COUNT(*)"
	. "\n FROM #__bannersmanager_client"
	;
	$database->setQuery( $query );
	$total = $database->loadResult();

	require_once( $GLOBALS['mosConfig_absolute_path'] . '/administrator/includes/pageNavigation.php' );
	$pageNav = new mosPageNav( $total, $limitstart, $limit );
	$query = "SELECT c.*,count(*) as num_of_banners, b.id as not_empty "
	. "\n FROM `#__bannersmanager_client` as c"
	. "\n LEFT JOIN `#__bannersmanager_banner` as b ON b.clientid = c.id"
	. "\n WHERE 1 "
	. "\n GROUP BY c.id"
	;
	
	$database->setQuery( $query,$pageNav->limitstart, $pageNav->limit);
	$rows = $database->loadObjectList();

	HTML_bannersmanager::showClients( $rows, $pageNav, $option );
}

function editClient( $id, $option ) {
	global $database, $my;
	$lists = array();
	$row = new bannersManagerClient($database);
	if (isset($id))
		$row->load( (int)$id );
		
	HTML_bannersmanager::clientForm( $row, $option );
}

function saveClient( $task ) {
	global $database,$mosConfig_absolute_path;

	$row = new bannersManagerClient($database);

	$msg = 'Saved Client info';
	if (!$row->bind( $_POST )) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
		
	if (!$row->store()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	
	mosRedirect( 'index2.php?option=com_bannersmanager&act=client', $msg );
}

function removeClient( $cid ) {
	global $database,$mosConfig_absolute_path;
	
	if (count( $cid )) {
		$cids = implode( ',', $cid );
			
		$query = "SELECT count(*) FROM #__bannersmanager_banner"
		. "\n WHERE clientid IN ( $cids )";
		$database->setQuery( $query );
		$total = $database->loadResult();
		if ($total > 0)
		{
			mosRedirect( 'index2.php?option=com_bannersmanager&act=client', BANNERSMANAGER_CANNOT_DELETE_CLIENT );
		}
		
		$query = "DELETE FROM #__bannersmanager_client"
		. "\n WHERE id IN ( $cids )"
		;
		$database->setQuery( $query );
		if (!$database->query()) {
			echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
		}
	}
	mosRedirect( 'index2.php?option=com_bannersmanager&act=client' );
}

function viewBanners( $option ) {
	global $database, $mainframe, $mosConfig_list_limit;

	$groupid = mosGetParam( $_REQUEST, 'groupid', 0 );
	$clientid = mosGetParam( $_REQUEST, 'clientid', 0 );		
	$limit 				= intval( $mainframe->getUserStateFromRequest( "viewlistlimit", 'limit', $mosConfig_list_limit ) );
	$limitstart 		= intval( $mainframe->getUserStateFromRequest( "view{$option}limitstart", 'limitstart', 0 ) );
	
	$where = "1";
	
	if ($groupid  != 0)
		$where .= " AND b.groupid = $groupid";
	
	if ($clientid  != 0)
		$where .= " AND b.clientid = $clientid";
	
	// get the total number of records
	$query = "SELECT COUNT(*)"
	. "\n FROM #__bannersmanager_banner AS b WHERE $where"
	;
	$database->setQuery( $query );
	$total = $database->loadResult();

	require_once( $GLOBALS['mosConfig_absolute_path'] . '/administrator/includes/pageNavigation.php' );
	$pageNav = new mosPageNav( $total, $limitstart, $limit );

	$query = "SELECT b.*,g.name as groupname,c.name as clientname,g.id as groupid,c.id as clientid "
	. "\n FROM #__bannersmanager_banner AS b "
	. "\n LEFT JOIN #__bannersmanager_group as g ON b.groupid = g.id"
	. "\n LEFT JOIN #__bannersmanager_client as c ON b.clientid = c.id"
	. "\n WHERE $where ORDER BY b.ordering"
	;
	$database->setQuery( $query, $pageNav->limitstart, $pageNav->limit );
	$rows = $database->loadObjectList();
	
	$database->setQuery("SELECT g.name, g.id FROM #__bannersmanager_group as g WHERE 1");
	$groups = $database -> loadObjectList();
	if ($database -> getErrorNum()) {
		echo $database -> stderr();
		return false;
	}
	
	$groupoptions[] = mosHTML::makeOption( 0, BANNERSMANAGER_ALL_GROUPS );
	foreach($groups as $group) {
		$groupoptions[] = mosHTML::makeOption( $group->id, $group->name );
	}
  	$groupselect = mosHTML::selectList( $groupoptions, 'groupid', 'class="inputbox" size="1" onchange="document.adminForm.submit();"' , 'value', 'text', $groupid );
  	
  	$database->setQuery("SELECT c.name, c.id FROM #__bannersmanager_client as c WHERE 1");
	$clients = $database -> loadObjectList();
	if ($database -> getErrorNum()) {
		echo $database -> stderr();
		return false;
	}
	
	$clientoptions[] = mosHTML::makeOption( 0, BANNERSMANAGER_ALL_CLIENTS );
	foreach($clients as $client) {
		$clientoptions[] = mosHTML::makeOption( $client->id, $client->name );
	}
  	$clientselect = mosHTML::selectList( $clientoptions, 'clientid', 'class="inputbox" size="1" onchange="document.adminForm.submit();"' , 'value', 'text', $clientid );
  	
  	
  	
	HTML_bannersmanager::showBanners( $rows,$groupselect,$clientselect, $pageNav, $option );
}

function editBanner( $bannerid, $option ) {
	global $database, $my,$_MAMBOTS;
	$lists = array();
	$row = new bannersManagerBanner($database);
	if (isset($bannerid))
		$row->load( (int)$bannerid );
		
	// make the select list for the image positions
	$yesno[] = mosHTML::makeOption( '0', _CMN_NO );
  	$yesno[] = mosHTML::makeOption( '1', _CMN_YES );
  	$lists['published'] = mosHTML::selectList( $yesno, 'published', 'class="inputbox" size="1"' , 'value', 'text', $row->published );
  	
  	$query ="SELECT * FROM #__bannersmanager_group WHERE 1";
	$database->setQuery( $query );
	$results = $database->loadObjectList('name');
	if (count($results) == 0)
	{
		mosRedirect( 'index2.php?option=com_bannersmanager&act=group', BANNERSMANAGER_CREATE_GROUP_FIRST);
	}
	else
	{
		foreach($results as $gp)
		{
			$groups[] = mosHTML::makeOption( $gp->id, $gp->name );
		}
		$lists['groups'] = mosHTML::selectList( $groups, 'groupid', 'class="inputbox" size="1"' , 'value', 'text', $row->groupid );
	}
	
	$query ="SELECT * FROM #__bannersmanager_client WHERE 1";
	$database->setQuery( $query );
	$results = $database->loadObjectList('name');
	if (count($results) == 0)
	{
		mosRedirect( 'index2.php?option=com_bannersmanager&act=client', BANNERSMANAGER_CREATE_CLIENT_FIRST);
	}
	else
	{
		foreach($results as $cl)
		{
			$clients[] = mosHTML::makeOption( $cl->id, $cl->name );
		}
		$lists['clients'] = mosHTML::selectList( $clients, 'clientid', 'class="inputbox" size="1"' , 'value', 'text', $row->clientid );
	}
  	
  	$bannertype[] = mosHTML::makeOption( '0', BANNERSMANAGER_IMAGE );
  	$bannertype[] = mosHTML::makeOption( '1', BANNERSMANAGER_CUSTOM );
  	$lists['bannerType'] = mosHTML::selectList( $bannertype, 'type', 'class="inputbox" size="1" onchange="selType(this.options[this.selectedIndex].value);"' , 'value', 'text', $row->type );
  	
  	$target[] = mosHTML::makeOption( '_blank', BANNERSMANAGER_TARGET_NEW );
  	$target[] = mosHTML::makeOption( '_self', BANNERSMANAGER_TARGET_SELF );  	
  	$target[] = mosHTML::makeOption( '_parent', BANNERSMANAGER_TARGET_PARENT );
  	$lists['clickTarget'] = mosHTML::selectList( $target, 'target', 'class="inputbox" size="1"' , 'value', 'text', $row->target );
  	
  	
  	$menuids = preg_split('/,/', $row->menuid, -1, PREG_SPLIT_NO_EMPTY);
  	$lookup = array();
  	foreach($menuids as $menuid)
  	{
  		$temp = new stdClass();
		$temp->value = $menuid;
		$lookup[] = $temp;
  	}
	$lists['selections'] 	= mosAdminMenus::MenuLinks( $lookup, 1, 1,0);
  		
	if (!isset($lookup)) {
		$lookup = array( mosHTML::makeOption( 0, 'All' ) );
	}
	
	/*** Create flash options lists ***/
	$wmode = array();
	$wmode[] = mosHTML::makeOption( 'window' );
	$wmode[] = mosHTML::makeOption( 'opaque' );
	$wmode[] = mosHTML::makeOption( 'transparent' );
	$lists['wmode'] = mosHTML::selectList( $wmode, 'wmode', 'class="inputbox" size="1"' , 'value', 'text', $row->wmode );
	
	$values = array();
	if ((isset($row->id))&&($row->id != 0))
	{
		$query = "SELECT * FROM #__bannersmanager_extension WHERE bannerid = ".$row->id;
		$database->setQuery( $query );
		$results = $database->loadObjectList('name');
	}	
	$_MAMBOTS->loadBotGroup( 'bannersmanager' );
	$extensions = $_MAMBOTS->trigger( 'onGetInfo', null, false );
	for($i = 0,$tot = count($extensions);$i < $tot;$i++) 
	{
		if ((isset($bannerid))&&(isset($results[$extensions[$i]->option])))
		{
			$options = preg_split('/,/', $results[$extensions[$i]->option]->value , -1, PREG_SPLIT_NO_EMPTY);
			$extensions[$i]->values = array();
			foreach($options as $opt)
			{
				$temp = new stdClass();
				$temp->value = $opt;
				$extensions[$i]->values[] = $temp;
			}
		}
		else
		{
			$extensions[$i]->values = array();
		}
	}
	
	HTML_bannersmanager::bannerForm( $row, $lists, $extensions, $option );
}

function saveBanner( $task ) {
	global $database,$mosConfig_absolute_path,$_MAMBOTS;

	$row = new bannersManagerBanner($database);
	$impunlimited = mosGetParam( $_POST, 'impunlimited', "" );
	$clicksunlimited = mosGetParam( $_POST, 'clicksunlimited', "" );

	$msg = 'Saved Banner info';
	$redirect_url = 'index2.php?option=com_bannersmanager&act=banner';
	
	if (!$row->bind( $_POST )) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

	// Resets clicks when `Reset Clicks` button is used instead of `Save` button
	if ( $task == 'resetClicks' ) {
		$row->clicks = 0;
		$msg = 'Reset Banner clicks';
		$redirect_url .= '&act=banner&task=edit&id='.$row->id;
	}
	
	// Resets clicks when `Reset Clicks` button is used instead of `Save` button
	if ( $task == 'resetImps' ) {
		$row->impmade = 0;
		$msg = 'Reset Banner impression';
		$redirect_url .= '&act=banner&task=edit&id='.$row->id;
	}
	
	if($task == 'apply'){
		$redirect_url .= '&act=banner&task=edit&id='.$row->id;
	}
	
	if ($impunlimited == "on")
	{
		$row->implimit = 0;
	}
	
	if ($clicksunlimited == "on")
	{
		$row->clickslimit = 0;
	}
	
	$menuid = mosGetParam( $_POST, "selections", array() );
	$row->menuid = ",".implode(',', $menuid).",";
		
	if (!$row->store()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	
	if (!isset($row->id)||($row->id == 0)){
		$row->id = $database->insertid();
	} 
	
	$query = "DELETE FROM #__bannersmanager_extension"
	. "\n WHERE `bannerid` = ".$row->id;
	$database->setQuery( $query );
	$database->query();
	if ($database -> getErrorNum()) {
		echo $database -> stderr();
		return false;
	}
	
	// image1 upload
	if (isset($_FILES["picture"]) and !$_FILES["picture"]['error'] ) {
		@unlink($mosConfig_absolute_path."/images/banners/".$row->imageurl);
		@move_uploaded_file($_FILES["picture"]["tmp_name"], $mosConfig_absolute_path."/images/banners/b".$row->id."_".$_FILES["picture"]["name"]);
		mosChmod($mosConfig_absolute_path."/images/banners/b".$row->id."_".$_FILES["picture"]["name"],644);
		$query = "UPDATE #__bannersmanager_banner"
		. "\n SET `imageurl` = 'b".$row->id."_".$_FILES["picture"]["name"]."'"
		. "\n WHERE id=".$row->id;
		$database->setQuery( $query );
		$database->query();
		if ($database -> getErrorNum()) {
			echo $database -> stderr();
			return false;
		}	
	}	
	
	$_MAMBOTS->loadBotGroup( 'bannersmanager' );
	$extensions = $_MAMBOTS->trigger( 'onSave', null, false );
	
	foreach($extensions as $ext) {
		$valuesArray = mosGetParam( $_POST, $ext, array() );
		$value = ",".implode(',', $valuesArray).",";
	
		$query = "INSERT INTO #__bannersmanager_extension ( `name` , `bannerid` , `value` ) VALUES ('$ext','".$row->id."','$value')";
		$database->setQuery( $query );
		if (!$database->query()) {
			echo $database->getErrorMsg();
			return false;
		}
	}
	mosRedirect( $redirect_url, $msg );
}

function publishBanner( $cid, $publish=1 ) {
	global $database, $my;

	if (!is_array( $cid ) || count( $cid ) < 1) {
		$action = $publish ? 'publish' : 'unpublish';
		echo "<script> alert('Select an item to $action'); window.history.go(-1);</script>\n";
		exit();
	}

	$cids = implode( ',', $cid );

	$query = "UPDATE #__bannersmanager_banner"
	. "\n SET published = " . intval( $publish )
	. "\n WHERE id IN ( $cids )"
	;
	$database->setQuery( $query );
	if (!$database->query()) {
		echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
		exit();
	}
	mosRedirect( 'index2.php?option=com_bannersmanager&act=banner' );
}

function removeBanner( $cid ) {
	global $database,$mosConfig_absolute_path;
	
	if (count( $cid )) {
		$cids = implode( ',', $cid );
		
		$query = "SELECT imageurl FROM #__bannersmanager_banner"
		. "\n WHERE id IN ( $cids )"
		;
		$database->setQuery( $query );
		$rows = $database->loadObjectList();
		foreach($rows as $row)
		{
			@unlink($mosConfig_absolute_path."/images/banners/".$row->imageurl);
		}
		
		$query = "DELETE FROM #__bannersmanager_banner"
		. "\n WHERE id IN ( $cids )"
		;
		$database->setQuery( $query );
		if (!$database->query()) {
			echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
		}
	}
	mosRedirect( 'index2.php?option=com_bannersmanager&act=banner' );
}

function saveOrder( &$tid,$option ) {
	global $database;

	$total		= count( $tid );
	$order 		= mosGetParam( $_POST, 'order', array(0) );
	$row 		= new bannersManagerBanner( $database );

	// update ordering values
	for( $i=0; $i < $total; $i++ ) {
		$row->load( $tid[$i] );
		if ($row->ordering != $order[$i]) {
			$row->ordering = $order[$i];
			if (!$row->store()) {
				echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
				exit();
			} // if
		} // if
	} // for

	// clean any existing cache files
	mosCache::cleanCache( $option );
	
	mosRedirect("index2.php?option=$option&act=banner", "");
} // saveOrder

/**
* Moves the order of a record
* @param integer The increment to reorder by
*/
function orderBanner( $uid, $inc, $option ) {
	global $database;

	$row = new bannersManagerBanner( $database );
	$row->load( $uid );
	$row->move( $inc );

	// clean any existing cache files
	mosCache::cleanCache( $option );

	mosRedirect("index2.php?option=$option&act=banner", "");
}

function importBanners($option) {
	global $database;
	
	$query = "SELECT * FROM #__banner WHERE 1";
	$database->setQuery( $query );
	$rows = $database->loadObjectList();
	
	$import_query = "INSERT INTO `#__bannersmanager_banner` (`name`,`clientid`,`groupid`, `type`,`imageurl`, `clickurl`,`custombannercode`,`impmade`, `implimit`, `menuid`, `clicks`, `clickslimit`, `ordering`, `published`) ";
	$i =0;
	foreach($rows as $row)
	{
		if ($i == 0)
			$import_query .= " VALUES ";
		else
			$import_query .= " , ";
		
		if ((isset($row->custombannercode))&&($row->custombannercode != "")){
			$import_query .= " ('$row->name','$row->cid',1,1,'$row->imageurl','$row->clickurl','$row->custombannercode','$row->impmade','$row->imptotal',',0,','$row->clicks','0','0','$row->showBanner') ";
		}
		else {
			$import_query .= " ('$row->name','$row->cid',1,0,'$row->imageurl','$row->clickurl','$row->custombannercode','$row->impmade','$row->imptotal',',0,','$row->clicks','0','0','$row->showBanner') ";
		}
		$i++;
	}
	$database->setQuery( $import_query );
	if (!$database->query()) {
		echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
	}
	
	$query = "SELECT * FROM #__bannerclient WHERE 1";
	$database->setQuery( $query );
	$rows = $database->loadObjectList();
	
	$import_query = "INSERT IGNORE INTO `#__bannersmanager_client` (`id`,`name`,`description`,`email`) ";
	$i =0;
	foreach($rows as $row)
	{
		if ($i == 0)
			$import_query .= " VALUES ";
		else
			$import_query .= " , ";
		
		$import_query .= " ('$row->cid','$row->name','$row->contact','$row->email') ";
		$i++;
	}
	$database->setQuery( $import_query );
	if (!$database->query()) {
		echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
	}
	
	mosRedirect("index2.php?option=$option&act=banner", "");
}
?>