<?php

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );

class HTML_bannersmanager {

	function menu($option) {
		echo '<div align="center">';
		echo '<a href="index2.php?option='.$option.'&amp;act=banner">'.BANNERSMANAGER_BANNERS.'</a>';
		echo ' | ';
		echo '<a href="index2.php?option='.$option.'&amp;act=client">'.BANNERSMANAGER_CLIENTS.'</a>';
		echo ' | ';
		echo '<a href="index2.php?option='.$option.'&amp;act=group">'.BANNERSMANAGER_GROUPS.'</a>';
		echo '</div>';
	}
	
	function showBanners( &$rows, $groupselect,$clientselect,&$pageNav, $option ) {
		global $my;

		mosCommonHTML::loadOverlib();
		?>
		<?php HTML_bannersmanager::menu($option);?>
		<form action="index2.php" method="post" name="adminForm">
		<table class="adminheading">
		<tr>
			<th>
			<?php 
			echo BANNERSMANAGER_BANNERS_MANAGER; 
			?>
			</th>
		</tr>
		</table>
		
		<div align="right">
		<?php echo $clientselect; ?>
		<?php echo $groupselect; ?>
		</div>
		<table class="adminlist">
		<tr>
			<th width="20">
			#
			</th>
			<th width="20">
			<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $rows ); ?>);" />
			</th>
			<th align="left" nowrap>
			<?php echo BANNERSMANAGER_BANNER_NAME; ?>
			</th>
			<th align="left" nowrap>
			<?php echo BANNERSMANAGER_CLIENT; ?>
			</th>
			<th align="left" nowrap>
			<?php echo BANNERSMANAGER_GROUP; ?>
			</th>
			<th width="10%" nowrap>
			<?php echo _CMN_PUBLISHED; ?>
			</th>
			<th colspan="2" align="center" width="5%">
			<?php echo BANNERSMANAGER_REORDER; ?>
			</th>
			<th width="3%">
			<?php echo BANNERSMANAGER_ORDER; ?>
			</th>
			<th width="3%">
			<a href="javascript: saveorder( <?php echo count( $rows ) -1; ?> )"><img src="images/filesave.png" border="0" width="16" height="16" /></a>
			</th>
			<th width="11%" nowrap>
			<?php echo BANNERSMANAGER_IMPRESSION_MADE; ?>
			</th>
			<th width="11%" nowrap>
			<?php echo BANNERSMANAGER_IMPRESSION_LEFT; ?>
			</th>
			<th width="8%">
			<?php echo BANNERSMANAGER_CLICKS; ?>
			</th>
			<th width="8%" nowrap>
			<?php echo BANNERSMANAGER_PERCENT_CLICKS; ?>
			</th>
		</tr>
		<?php
		$k = 0;
		for ($i=0, $n=count( $rows ); $i < $n; $i++) {
			$row = &$rows[$i];
			$link = 'index2.php?option='.$option.'&amp;task=edit&amp;act=banner&amp;hidemainmenu=1&amp;id='. $row->id;
			$linkclient = 'index2.php?option='.$option.'&amp;task=edit&amp;act=client&amp;hidemainmenu=1&amp;id='. $row->clientid;
			$linkgroup = 'index2.php?option='.$option.'&amp;task=edit&amp;act=group&amp;hidemainmenu=1&amp;id='. $row->groupid;

			$impleft 	= $row->implimit - $row->impmade;
			if( $impleft < 0 ) {
				$impleft 	= "unlimited";
			}

			if ( $row->impmade != 0 ) {
				$percentClicks = substr(100 * $row->clicks/$row->impmade, 0, 5);
			} else {
				$percentClicks = 0;
			}

			$task 	= $row->published ? 'unpublish' : 'publish';
			$img 	= $row->published ? 'publish_g.png' : 'publish_x.png';
			$alt 	= $row->published ? 'Published' : 'Unpublished';

			$checked 	= mosHTML::idBox( $i,$row->id );
			?>
			<tr class="<?php echo "row$k"; ?>">
				<td align="center">
				<?php echo $pageNav->rowNumber( $i ); ?>
				</td>
				<td align="center">
				<?php echo $checked; ?>
				</td>
				<td align="left">
					<a href="<?php echo $link; ?>">
					<?php echo $row->name; ?>
					</a>
				</td>
				<td align="left">
				<?php if (isset($row->clientname))
				{
						echo "<a href='$linkclient'>";
						echo $row->clientname;
						echo "</a>";
				}
				?>
				</td>
				<td align="left">
				<?php if (isset($row->groupname))
				{
						echo "<a href='$linkgroup'>";
						echo $row->groupname;
						echo "</a>";
				}
				?>
				</td>
				<td align="center">
				<a href="javascript: void(0);" onclick="return listItemTask('cb<?php echo $i;?>','<?php echo $task;?>')">
				<img src="images/<?php echo $img;?>" width="12" height="12" border="0" alt="<?php echo $alt; ?>" />
				</a>
				</td>
				<td align="right">
				<?php echo $pageNav->orderUpIcon( $i); ?>
				</td>
				<td align="left">
				<?php echo $pageNav->orderDownIcon( $i, count( $rows )); ?>
				</td>
				<td align="center" colspan="2">
				<input type="text" name="order[]" size="5" value="<?php echo $row->ordering; ?>" class="text_area" style="text-align: center" />
				</td>
				<td align="center">
				<?php echo $row->impmade;?>
				</td>
				<td align="center">
				<?php echo $impleft;?>
				</td>
				<td align="center">
				<?php echo $row->clicks;?>
				</td>
				<td align="center">
				<?php echo $percentClicks;?>
				</td>
			</tr>
			<?php
			$k = 1 - $k;
		}
		?>
		</table>
		<?php echo $pageNav->getListFooter(); ?>

		<input type="hidden" name="option" value="<?php echo $option; ?>" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="act" value="banner" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="hidemainmenu" value="0" />
		</form>
		<?php
	}

	function bannerForm( &$_row, &$lists,$extensions, $option ) {
		global $mosConfig_absolute_path,$mosConfig_live_site;
		
		mosMakeHtmlSafe( $_row, ENT_QUOTES, 'custombannercode' );
		?>
		<script language="javascript" type="text/javascript">
		<!--
		function submitbutton(pressbutton) {
			var form = document.adminForm;
			// do field validation
			
			if (form.name.value == "") {
				alert( "<?php echo BANNERSMANAGER_PROVIDE_BANNER_NAME; ?>" );
			}
			else if (getSelectedValue('adminForm','type') == 0) {

				if ((form.already_upload.value != 1)&&(form.picture.value == "")) {
					alert( "<?php echo BANNERSMANAGER_PROVIDE_FILE; ?>" );
				}
				else {
					submitform( pressbutton );
				}
			}
			else if (getSelectedValue('adminForm','type') == 1) {
				if (form.custombannercode.value == "") {
					alert( "<?php echo BANNERSMANAGER_PROVIDE_CUSTOM_CODE; ?>" );
				}
				else {
					submitform( pressbutton );
				}
			}

		}
		 function getObject(obj) {
			var strObj;
			if (document.all) {
			  strObj = document.all.item(obj);
			} else if (document.getElementById) {
			  strObj = document.getElementById(obj);
			}
			return strObj;
		  }
  
		 function selType(sType) {
			var elem;
			switch (sType) {
			  case '0':
				elem=getObject('divcustombanner');
				elem.style.visibility = 'hidden';
				elem.style.display = 'none';
				elem=getObject('divimagebanner');
				elem.style.visibility = 'visible';
				elem.style.display = 'block';
				break;
			  case '1':
				elem=getObject('divimagebanner');
				elem.style.visibility = 'hidden';
				elem.style.display = 'none';
				elem=getObject('divcustombanner');
				elem.style.visibility = 'visible';
				elem.style.display = 'block';
				break;
			}
		}
		
		function checkBoxChange(obj) {
			var elem;
			switch (obj.name) {
			  case 'impunlimited':
				elem=getObject('implimit');
				if (obj.checked == 1)
					elem.readOnly = true;
				else
					elem.readOnly = false;
				break;
			  case 'clicksunlimited':
				elem=getObject('clickslimit');
				if (obj.checked == 1)
					elem.readOnly = true;
				else
					elem.readOnly = false;
				break;
			}
		}
		
		function initUnLimited(implimit,clickslimit) {
			var elem;
			elem=getObject('implimit');
			if (implimit == 1)
				elem.readOnly = true;
			else
				elem.readOnly = false;
			elem=getObject('clickslimit');
			if (clickslimit == 1)
				elem.readOnly = true;
			else
				elem.readOnly = false;
		}
		
		//-->
		</script>
		<?php HTML_bannersmanager::menu($option);?>
		<form action="index2.php" method="post" name="adminForm" enctype="multipart/form-data">
		<table class="adminheading">
		<tr>
			<th>
			<?php echo BANNERSMANAGER_BANNER_TITLE; ?>
			<small>
			<?php echo $_row->id ? _E_EDIT : _CMN_NEW;?>
			</small>
			</th>
		</tr>
		</table>
		
		<?php
		$configtabs = new mosTabs( 0 );
		$configtabs->startPane( "config" );
		$configtabs->startTab(BANNERSMANAGER_BANNER_DETAILS,"details-page");
		?>
		<table class="adminform" width="100%">
		<tr>
			<th colspan="2" width="100%">
			<?php echo BANNERSMANAGER_BANNER_DETAILS; ?>
			</th>
		</tr>		
		<tr>
			<td width="20%">
			<?php echo BANNERSMANAGER_BANNER_NAME; ?>
			</td>
			<td width="80%">
			<input class="inputbox" type="text" name="name" value="<?php echo $_row->name;?>" />
			</td>
		</tr>
		<tr>
			<td width="20%">
			<?php echo BANNERSMANAGER_CLIENT; ?>
			</td>
			<td width="80%">
			<?php  echo $lists['clients']; ?>
			</td>
		</tr>
		<tr>
			<td width="20%">
			<?php echo BANNERSMANAGER_GROUP; ?>
			</td>
			<td width="80%">
			<?php  echo $lists['groups']; ?>
			</td>
		</tr>

		<tr>
			<td>
			<?php echo BANNERSMANAGER_IMPRESSIONS_PURCHASED; ?>
			</td>
			<?php
			$impunlimited = '';
			if ($_row->implimit == 0) {
				$impunlimited = 'checked="checked"';
				$_row->implimit = '';
			}
			?>
			<td>
			<input class="inputbox" type="text" id="implimit" name="implimit" size="12" maxlength="11" value="<?php echo $_row->implimit;?>" />
			&nbsp;&nbsp;&nbsp;&nbsp;
			<?php echo BANNERSMANAGER_UNLIMITED; ?><input type="checkbox" name="impunlimited" onclick="checkBoxChange(this);" <?php echo $impunlimited;?> />
			</td>
		</tr>
		<tr>
			<td>
			<?php echo BANNERSMANAGER_CLICKS_PURCHASED; ?>
			</td>
			<?php
			$clicksunlimited = '';
			if ($_row->clickslimit == 0) {
				$clicksunlimited = 'checked="checked"';
				$_row->clickslimit = '';
			}
			?>
			<td>
			<input class="inputbox" type="text" id="clickslimit" name="clickslimit" size="12" maxlength="11" value="<?php echo $_row->clickslimit;?>" />
			&nbsp;&nbsp;&nbsp;&nbsp;
			<?php echo BANNERSMANAGER_UNLIMITED; ?><input type="checkbox" name="clicksunlimited" onclick="checkBoxChange(this);" <?php echo $clicksunlimited;?> />
			</td>
		</tr>
		<tr>
			<td>
			<?php echo BANNERSMANAGER_SHOW_BANNER; ?>
			</td>
			<td>
			<?php echo $lists['published']; ?>
			</td>
		</tr>
		<tr >
			<td valign="top" align="right">
			<?php echo BANNERSMANAGER_CLICKS; ?>
			</td>
			<td colspan="2">
			<?php echo $_row->clicks;?>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<input name="reset_hits" type="button" class="button" value="Reset Clicks" onclick="submitbutton('resetClicks');" />
			</td>
		</tr>
		<tr >
			<td valign="top" align="right">
			Impression
			</td>
			<td colspan="2">
			<?php echo $_row->impmade;?>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<input name="reset_imps" type="button" class="button" value="Reset Impressions" onclick="submitbutton('resetImps');" />
			</td>
		</tr>
		<tr>
			<td>
			<?php echo BANNERSMANAGER_BANNER_TYPE; ?>
			</td>
			<td>
			<?php echo $lists['bannerType']; ?>
			</td>
		</tr>		
		</table>
		<div id="divcustombanner">
		<table class="adminform" width="100%">
		<tr>
			<td valign="top">
			<?php echo BANNERSMANAGER_CUSTOM_BANNER_CODE; ?>
			</td>
			<td>
			<textarea class="inputbox" cols="70" rows="5" name="custombannercode"><?php echo $_row->custombannercode;?></textarea>
			</td>
		</tr>
		</table>
		</div>
		<div id="divimagebanner">
		<table class="adminform" width="100%">
		<tr>
			<td width="20%">
			<?php echo BANNERSMANAGER_BANNER_WIDTH; ?>
			</td>
			<td width="80%">
			<input class="inputbox" type="text" name="width" size='6' value="<?php echo $_row->width;?>" /> <?php echo BANNERSMANAGER_BANNER_PX; ?>
			</td>
		</tr>	
		<tr>
			<td>
			<?php echo BANNERSMANAGER_BANNER_HEIGHT; ?>
			</td>
			<td>
			<input class="inputbox" type="text" name="height" size='6' value="<?php echo $_row->height;?>" /> <?php echo BANNERSMANAGER_BANNER_PX; ?>
			</td>
		</tr>
		<tr>
			<td>
			<?php echo BANNERSMANAGER_BANNER_BORDER; ?>
			</td>
			<td>
			<input class="inputbox" type="text" name="border" size='6' value="<?php echo $_row->border;?>" /> <?php echo BANNERSMANAGER_BANNER_PX; ?>
			</td>
		</tr>
		<tr>
			<td colspan='2'>
			<?php echo BANNERSMANAGER_BANNER_FLASH; ?></td>
		</tr>
		<tr>
			<td>
			<?php echo BANNERSMANAGER_BANNER_WMODE; ?>
			</td>
			<td>
			<?php  echo $lists['wmode']; ?>
			</td>
		</tr>
		<tr>
			<td>
			<?php echo BANNERSMANAGER_BANNER_PLAY; ?>
			</td>
			<td>
			<?php echo mosHTML::yesnoRadioList( 'play', 'class="inputbox"', $_row->play ? 1 : 0 ); ?>
			</td>
		</tr>	
		<tr>
			<td>
			<?php echo BANNERSMANAGER_BANNER_MENU; ?>
			</td>
			<td>
			<?php echo mosHTML::yesnoRadioList( 'menu', 'class="inputbox"', $_row->menu ? 1 : 0 ); ?>
			</td>
		</tr>	
		<tr>
			<td>
			<?php echo BANNERSMANAGER_BANNER_LOOP; ?>
			</td>
			<td>
			<?php echo mosHTML::yesnoRadioList( 'loop', 'class="inputbox"', $_row->loop ? 1 : 0 ); ?>
			</td>
		</tr>		
		<tr>
			<td>
			<?php echo BANNERSMANAGER_BANNER_BGCOLOR; ?>
			</td>
			<td>
			<input class="inputbox" type="text" name="bgcolor" size='10' value="<?php echo $_row->bgcolor;?>" />
			</td>
		</tr>			
		<tr>
			<td>
			<?php echo BANNERSMANAGER_CLICK_URL; ?>
			</td>
			<td>
			<input class="inputbox" type="text" name="clickurl" size="100" maxlength="200" value="<?php echo $_row->clickurl;?>" />
			</td>
		</tr>
		<tr>
			<td>
			<?php echo BANNERSMANAGER_CLICK_URL_TARGET; ?>
			</td>
			<td>
			<?php echo $lists['clickTarget']; ?>
			</td>
		</tr>		
		<tr>
			<td>
			<?php echo BANNERSMANAGER_FILE_URL; ?>
			</td>
			<td>
			<input type="file" name="picture"/>
			</td>
		</tr>
		<tr>
			<td>
			</td>
			<td>
			<?php
			if (isset($_row->imageurl)&& ($_row->imageurl!= ""))
			{
				$pic = $mosConfig_absolute_path."/images/banners/".$_row->imageurl;
				$ext = substr(strrchr($pic, '.'), 1);

				if (file_exists($pic)) 
				{
					if(strtolower($ext) == 'swf'){
						$width = $_row->width ? "width='$_row->width'" : '';
						$height = $_row->height ? "height='$_row->height'" : '';				
						
						echo "<object classid='clsid:d27cdb6e-ae6d-11cf-96b8-444553540000' codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0' $width $height> "
							 ."<param name='movie' value='$mosConfig_live_site/images/banners/$_row->imageurl' />\n"
							 ."<param name='quality' value='high' />\n"
							 .(!$_row->loop ? 	"<param name='loop' value='false' />\n" : '')
							 .(!$_row->menu ? 	"<param name='menu' value='false' />\n" : '')
							 .($_row->wmode ? 	"<param name='wmode' value='$_row->wmode' />" : '')
							 .($_row->bgcolor ? "<param name='bgcolor' value='$_row->bgcolor' />" : '')
												
							."<embed src='$mosConfig_live_site/images/banners/$_row->imageurl' quality='high' name='mymoviename' type='application/x-shockwave-flash' pluginspage='http://www.macromedia.com/go/getflashplayer' $width $height "
							.(!$_row->loop ? 	'loop="false" ' : '')
							.(!$_row->menu ? 	'menu="false" ' : '')
							.($_row->wmode ? 	"wmode='$_row->wmode' " : '')
							.($_row->bgcolor ? 	"bgcolor='$_row->bgcolor' " : '')							
							."></embed> 
						</object>";
						
					}else{
						echo '<img src="'.$mosConfig_live_site.'/images/banners/'.$_row->imageurl.'"/>';
						echo "<input type='hidden' name='imageurl' value='".$_row->imageurl."' />";
					}
					echo "<input type='hidden' name='already_upload' value='1' />";
				}
				else
				{
					echo "<input type='hidden' name='already_upload' value='0' />";
				}
			}
			else {
				echo "<input type='hidden' name='already_upload' value='0' />";
			}
			?>
			</td>
		</tr>
		</table>
		</div>
		<?php   
		$configtabs->endTab();
		$configtabs->startTab(BANNERSMANAGER_RESTRICTION_PAGES,"itemid-page");
		?>		
		<table width="100%" class="adminform">
		<tr>
			<th>
			<?php echo BANNERSMANAGER_RESTRICTION_PAGES; ?>
			</th>
		</tr>
		<tr>
			<td>
			<?php echo BANNERSMANAGER_MENU_ITEMS_SELECTION; ?> 
			<br />
			<?php echo $lists['selections']; ?>
			</td>
		</tr>
		</table>		
		<?php   
		$configtabs->endTab();
		?>		
		<?php
		foreach($extensions as $extension) {
		$configtabs->startTab($extension->name,$extension->option."-page");
		?>
		<table width="100%" class="adminform">
		<tr>
			<th>
			<?php echo $extension->name; ?>
			</th>
		</tr>
		</table>
		<table width="100%" class="adminform">
		<tr>
			<td>
			<?php echo $extension->description; ?>
			<br />
			<?php echo mosHTML::selectList( $extension->list, $extension->option.'[]', 'class="inputbox" size="30" multiple="multiple"' , 'value', 'text',$extension->values ); ?>
			</td>
		</tr>
		</table>
		<?php 
		$configtabs->endTab();
		} 
		$configtabs->endPane();
		?>
		

		<input type="hidden" name="option" value="<?php echo $option; ?>" />
		<input type="hidden" name="id" value="<?php echo $_row->id; ?>" />
		<input type="hidden" name="clicks" value="<?php echo $_row->clicks; ?>" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="act" value="banner" />
		<input type="hidden" name="impmade" value="<?php echo $_row->impmade; ?>" />
		</form>
		<?php
		echo "<script type=\"text/javascript\"> selType('".$_row->type."'); </script>";	
		echo "<script type=\"text/javascript\"> initUnLimited(".!$_row->implimit.",".!$_row->clickslimit."); </script>";	
		
	}
	
	function showGroups( $rows, $mods,&$pageNav, $option ) {
		global $my,$mosConfig_live_site;

		mosCommonHTML::loadOverlib();
		?>
		<?php HTML_bannersmanager::menu($option);?>
		<form action="index2.php" method="post" name="adminForm">
		<table class="adminheading">
		<tr>
			<th>
			<?php echo BANNERSMANAGER_GROUPS_MANAGER; ?>
			</th>
		</tr>
		</table>
		
		<table class="adminlist">
		<tr>
			<th width="20">
			#
			</th>
			<th width="20">
			<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $rows ); ?>);" />
			</th>
			<th align="left" width="20" nowrap>
			<?php echo BANNERSMANAGER_GROUP_ID; ?>
			</th>
			<th align="left" width="40%" nowrap>
			<?php echo BANNERSMANAGER_GROUP_NAME; ?>
			</th>
			<th align="left" width="40%" nowrap>
			<?php echo BANNERSMANAGER_MODULE; ?>
			</th>
		</tr>
		<?php 
		$k = 0;
		for ($i=0, $n=count( $rows ); $i < $n; $i++) {
			$row = &$rows[$i];
			$link = 'index2.php?option='.$option.'&amp;task=edit&amp;act=group&amp;hidemainmenu=1&amp;id='. $row->id;
			
			$checked 	= mosHTML::idBox( $i,$row->id );
			?>
			<tr class="<?php echo "row$k"; ?>">
				<td width="20" align="center">
				&nbsp;
				</td>
				<td width="20" align="center">
				<?php echo $checked; ?>
				</td>
				<td width="20" align="center">
				<?php echo $row->id; ?>
				</td>
				<td width="40%" align="left">
					<a href="<?php echo $link; ?>" title="Edit Banner">
					<?php echo $row->name; ?>
					</a>
				</td>
				<td width="40%" align="left">
					<?php 
					$index=0;
					if(isset($mods)) {
					foreach($mods as $mod) {
						if ($index > 0)
							echo "<br/>";
						if (strpos($mod->params, "banner_gid=".$row->id."\n") === false)
						{
						}
						else
						{
							$link_mod = $mosConfig_live_site."/administrator/index2.php?option=com_modules&client=&task=editA&hidemainmenu=1&id=".$mod->id;
							echo "<a href='$link_mod'>".$mod->title."</a> (".$mod->position.")";
						}
						$index++;
					} 
					}
					?>
				</td>
			</tr>
			<?php
			$k = 1 - $k;
		}
		?>
		</table>
		<?php echo $pageNav->getListFooter(); ?>

		<input type="hidden" name="option" value="<?php echo $option; ?>" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="act" value="group" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="hidemainmenu" value="0" />
		</form>
		<?php 
	}

	function groupForm( &$_row, $option ) {
		global $mosConfig_absolute_path,$mosConfig_live_site;
		?>
		<?php HTML_bannersmanager::menu($option);?>
		<form action="index2.php" method="post" name="adminForm">
		<table class="adminheading">
		<tr>
			<th>
			<?php echo BANNERSMANAGER_GROUP_TITLE; ?>
			<small>
			<?php echo $_row->id ? _E_EDIT : _CMN_NEW;?>
			</small>
			</th>
		</tr>
		</table>
		
		<table class="adminform" width="100%">
		<tr>
			<th colspan="2" width="100%">
			<?php echo BANNERSMANAGER_GROUP_DETAILS; ?>
			</th>
		</tr>		
		<tr>
			<td width="20%">
			<?php echo BANNERSMANAGER_GROUP_NAME; ?>
			</td>
			<td width="80%">
			<input class="inputbox" type="text" name="name" value="<?php echo $_row->name;?>" />
			</td>
		</tr>
		
		<input type="hidden" name="option" value="<?php echo $option; ?>" />
		<input type="hidden" name="id" value="<?php echo $_row->id; ?>" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="act" value="group" />
		</form>	
		<?php 	
	}
	
	function showClients( $rows,&$pageNav, $option ) {
		global $my,$mosConfig_live_site;

		mosCommonHTML::loadOverlib();
		?>
		<?php HTML_bannersmanager::menu($option);?>
		<form action="index2.php" method="post" name="adminForm">
		<table class="adminheading">
		<tr>
			<th>
			<?php echo BANNERSMANAGER_CLIENTS_MANAGER; ?>
			</th>
		</tr>
		</table>
		
		<table class="adminlist">
		<tr>
			<th width="20">
			#
			</th>
			<th width="20">
			<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $rows ); ?>);" />
			</th>
			<th align="left" width="45%" nowrap>
			<?php echo BANNERSMANAGER_CLIENT_NAME; ?>
			</th>
			<th align="left" width="45%" nowrap>
			<?php echo BANNERSMANAGER_CLIENT_NB_BANNERS; ?>
			</th>
		</tr>
		<?php 
		$k = 0;
		for ($i=0, $n=count( $rows ); $i < $n; $i++) {
			$row = &$rows[$i];
			$link = 'index2.php?option='.$option.'&amp;task=edit&amp;act=client&amp;hidemainmenu=1&amp;id='. $row->id;
			
			$checked 	= mosHTML::idBox( $i,$row->id );
			?>
			<tr class="<?php echo "row$k"; ?>">
				<td width="20" align="center">
				<?php echo "".$i + 1; ?>
				</td>
				<td width="20" align="center">
				<?php echo $checked; ?>
				</td>
				<td width="45%" align="left">
				<a href="<?php echo $link; ?>" title="Edit Client">
				<?php echo $row->name; ?>
				</a>
				</td>
				<td width="45%" align="left">
					<?php 
					if (!isset($row->not_empty))
						$row->num_of_banners = 0;
					echo $row->num_of_banners;
					?>
				</td>
			</tr>
			<?php
			$k = 1 - $k;
		}
		?>
		</table>
		<?php echo $pageNav->getListFooter(); ?>

		<input type="hidden" name="option" value="<?php echo $option; ?>" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="act" value="client" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="hidemainmenu" value="0" />
		</form>
		<?php 
	}

	function clientForm( &$_row, $option ) {
		global $mosConfig_absolute_path,$mosConfig_live_site;
		?>
		<?php HTML_bannersmanager::menu($option);?>
		<form action="index2.php" method="post" name="adminForm">
		<table class="adminheading">
		<tr>
			<th>
			<?php echo BANNERSMANAGER_CLIENT_TITLE; ?>
			<small>
			<?php echo $_row->id ? _E_EDIT : _CMN_NEW;?>
			</small>
			</th>
		</tr>
		</table>
		
		<table class="adminform" width="100%">
		<tr>
			<th colspan="2" width="100%">
			<?php echo BANNERSMANAGER_CLIENT_DETAILS; ?>
			</th>
		</tr>		
		<tr>
			<td width="20%">
			<?php echo BANNERSMANAGER_CLIENT_NAME; ?>
			</td>
			<td width="80%">
			<input class="inputbox" type="text" name="name" value="<?php echo $_row->name;?>" />
			</td>
		</tr>
			<td width="20%">
			<?php echo BANNERSMANAGER_CONTACT; ?>
			</td>
			<td width="80%">
			<input class="inputbox" type="text" name="contact" value="<?php echo $_row->contact;?>" />
			</td>
		</tr>				
		<tr>
			<td width="20%">
			<?php echo BANNERSMANAGER_EMAIL; ?>
			</td>
			<td width="80%">
			<input class="inputbox" type="text" name="email" value="<?php echo $_row->email;?>" />
			</td>
		</tr>
		<tr>
			<td width="20%">
			<?php echo BANNERSMANAGER_DESCRIPTION; ?>
			</td>
			<td width="80%">
			<textarea name="description" cols='50' rows='20' wrap='VIRTUAL'><?php echo $_row->description;?></textarea>
			</td>
		</tr>	
		
		<input type="hidden" name="option" value="<?php echo $option; ?>" />
		<input type="hidden" name="id" value="<?php echo $_row->id; ?>" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="act" value="client" />
		</form>	
		<?php 	
	}
}
?>