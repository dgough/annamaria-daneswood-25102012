<?php
//
// Copyright (C) 2006 Thomas Papin
// http://www.gnu.org/copyleft/gpl.html GNU/GPL

// This file is part of the AdsManager Component,
// a Joomla! Classifieds Component by Thomas Papin
// Email: thomas.papin@free.fr
//
// no direct access
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

function com_install()
{
	global $mosConfig_lang,$database;
	
	$query = "SELECT COUNT(*)"
	. "\n FROM #__bannersmanager_group"
	;
	$database->setQuery( $query );
	$total = $database->loadResult();
	
	if ($total == 0)
	{
		$database->setQuery("INSERT IGNORE INTO `#__bannersmanager_group` VALUES (1, 'Vertical Module');");
		$result = $database->query();
	
		$database->setQuery("INSERT IGNORE INTO `#__bannersmanager_group` VALUES (2, 'Horizontal Module');");
		$result = $database->query();
		
		$database->setQuery("INSERT IGNORE INTO `#__bannersmanager_group` VALUES (3, 'Inner Module');");
		$result = $database->query();
	}
	
	$query = "SELECT COUNT(*)"
	. "\n FROM #__bannersmanager_client"
	;
	$database->setQuery( $query );
	$total = $database->loadResult();
	
	if ($total == 0)
	{
		$database->setQuery("INSERT IGNORE INTO `#__bannersmanager_client` VALUES (1, 'Client 1','Description','client1@aol.com');");
		$result = $database->query();
	
		$database->setQuery("INSERT IGNORE INTO `#__bannersmanager_client` VALUES (2, 'Client 2','Description','client2@gmail.com');");
		$result = $database->query();
		
		$database->setQuery("INSERT IGNORE INTO `#__bannersmanager_client` VALUES (3, 'Client 3','Description','client3@msn.com');");
		$result = $database->query();
	}
	
	//Update to 1.0.1
    $database->setQuery("SELECT clientid FROM #__bannersmanager_banner WHERE 1");
    $database->loadObjectList();
	if ($database->getErrorNum()) {
		$database->setQuery("ALTER TABLE #__bannersmanager_banner ADD `clientid` INT DEFAULT '1' NOT NULL AFTER `groupid`");
		$result = $database->query();
    }
?>

<center>
<table width="100%" border="0">
   <tr>
      <td>
      Thank you for using BannersManager (joomprod.com)<br/>
      You can make a donation to the author via Paypal<br/>
      <div align="center">
		<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
		<input type="hidden" name="cmd" value="_xclick">
		<input type="hidden" name="business" value="r_caroline08@yahoo.fr">
		<input type="hidden" name="item_name" value="JoomPROD">
		<input type="hidden" name="item_number" value="don1">
		<input type="hidden" name="no_shipping" value="2">
		<input type="hidden" name="no_note" value="1">
		<input type="hidden" name="currency_code" value="EUR">
		<input type="hidden" name="tax" value="0">
		<? if ($mosConfig_lang == "french") {  ?>
		<input type="hidden" name="lc" value="FR">
		<? } else { ?>
		<input type="hidden" name="lc" value="EN">
		<? } ?>
		<input type="hidden" name="bn" value="PP-DonationsBF">
		<input type="image" src="https://www.paypal.com/fr_FR/i/btn/x-click-but04.gif" border="0" name="submit" alt="Effectuez vos paiements via PayPal : une solution rapide, gratuite et s�curis�e">
		<? if ($mosConfig_lang == "french") { ?>
		<img alt="" border="0" src="https://www.paypal.com/fr_FR/i/scr/pixel.gif" width="1" height="1">
		<? } else { ?>
		<img alt="" border="0" src="https://www.paypal.com/en_EN/i/scr/pixel.gif" width="1" height="1">
		<? } ?>
		</form>	
	</div>
	 <p>
		<em>Thomas PAPIN - thomas.papin@free.fr</em>
	 </p>
      </td>
      <td>
         <p>
            <br>
            <br>
            <br>
         </p>
      </td>
   </tr>
</table>
</center>


<?php
}
?>