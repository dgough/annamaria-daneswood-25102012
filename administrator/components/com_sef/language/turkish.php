<?php
/**
 * SEF module for Joomla!
 *
 * @author      $Author: michal $
 * @copyright   ARTIO s.r.o., http://www.artio.cz
 * @package     JoomSEF
 * @version     $Name$, ($Revision: 4994 $, $Date: 2005-11-03 20:50:05 +0100 (??t, 03 XI 2005) $)
 */

// Security check to ensure this file is being included by a parent file.
if (!defined('_VALID_MOS')) die('Direct Access to this location is not allowed.');

// admin interface
define('_COM_SEF_CONFIG', 'ARTIO JoomSEF<br/>Yapřlandřrma');
define('_COM_SEF_CONFIGDESC', 'ARTIO JoomSEF ayarlarřnřz.');
define('_COM_SEF_HELP', 'ARTIO JoomSEF<br/>Destek');
define('_COM_SEF_HELPDESC', 'ARTIO JoomSEF destek?');
define('_COM_SEF_INFO', 'ARTIO JoomSEF<br/>Kurulum Dosyalarř');
define('_COM_SEF_INFODESC', 'ARTIO JoomSEF kurulumu hakkřnda detaylř bilgi');
define('_COM_SEF_CHANGELOG', 'ARTIO JoomSEF<br/>Changelog');
define('_COM_SEF_CHANGELOGDESC', 'View ARTIO JoomSEF Changelog');
define('_COM_SEF_VIEWURL', 'G÷ster/DŘzenle<br/>SEF Adresleri');
define('_COM_SEF_VIEWURLDESC', 'SEF adreslerini g÷ster ve dŘzenle');
define('_COM_SEF_VIEW404', 'G÷ster/DŘzenle<br/>404 Logs');
define('_COM_SEF_VIEW404DESC', 'G÷ster/DŘzenle 404 Logs');
define('_COM_SEF_VIEWCUSTOM', 'View/Edit<br/>Redirects');
define('_COM_SEF_VIEWCUSTOMDESC', 'View/Edit Custom Redirects');
define('_COM_SEF_SUPPORT', 'Destek<br/>Web sitesi');
define('_COM_SEF_SUPPORTDESC', 'ARTIO JoomSEF destek sayfasřna ba­lanřr (Yeni Pencerede)');
define('_COM_SEF_BACK', 'Back to ARTIO JoomSEF Control Panel');
define('_COM_SEF_PURGEURL', 'Sil<br/>SEF Urls');
define('_COM_SEF_PURGEURLDESC', 'TŘm sef sayfalarřnř sil');
define('_COM_SEF_PURGE404', 'Sil<br/>404 Logs');
define('_COM_SEF_PURGE404DESC', 'Sil 404 Logs');
define('_COM_SEF_PURGECUSTOM', 'Sil<br/>Redirects');
define('_COM_SEF_PURGECUSTOMDESC', 'Sil Custom Redirects');
define('_COM_SEF_WARNDELETE', 'DŢKKAT!!!<br/>Sayfalar silinecek ');
define('_COM_SEF_RECORD', ' sayfa');
define('_COM_SEF_RECORDS', ' sayfalar');
define('_COM_SEF_NORECORDS', 'Kayřtlř yok.');
define('_COM_SEF_PROCEED', ' Proceed ');
define('_COM_SEF_OK', ' Tamam ');
define('_COM_SEF_SUCCESSPURGE', 'Kayřtlar ba¦arřyla temizledi');
define('_PREVIEW_CLOSE', 'Bu pencereyi kapat');
define('_COM_SEF_EMPTYURL', 'URL redirection hazřrlamalřsřn.');
define('_COM_SEF_NOLEADSLASH', 'The should be NO LEADING SLASH on the New SEF URL');
define('_COM_SEF_BADURL', 'Url ba¦lamayan eski Non-SEF indeks.Php.');
define('_COM_SEF_URLEXIST', 'Bu URL zaten veri tabanřnřn išinde var!');
define('_COM_SEF_SHOW0', 'SEF Urls G÷ster');
define('_COM_SEF_SHOW1', '404 Log G÷ster');
define('_COM_SEF_SHOW2', 'Custom Redirects G÷ster');
define('_COM_SEF_INVALID_URL', 'INVALID URL: this link requires a valid Itemid, but one was not found.<br/>SOLUTION: Create a menuitem for this item. You do not have to publish it, just create it.');
define('_COM_SEF_DEF_404_MSG', '<h1>404: Hata</h1><h4>-zgŘnŘz, Aradř­řnřz sayfa bulunamadř</h4>');
define('_COM_SEF_SELECT_DELETE', 'Silmek išin bir parša seš.');
define('_COM_SEF_ASC', ' (asc) ');
define('_COM_SEF_DESC', ' (desc) ');
define('_COM_SEF_WRITEABLE', ' <b><font color="green">Yazřlabilir</font></b>');
define('_COM_SEF_UNWRITEABLE', ' <b><font color="red">Yazřlmaz</font></b>');
define('_COM_SEF_USING_DEFAULT', ' <b><font color="red">Varsayřlan de­erleri kullan</font></b>');
define('_COM_SEF_DISABLED',"<p class='error'>Not: SEF support in Joomla/Mambo is currently disabled. To use SEF, please enable it from the <a href='"
.$GLOBALS['mosConfig_live_site']."/administrator/index2.php?option=com_config'>Global Configuration</a> SEO page.</p>");
define('_COM_SEF_TITLE_CONFIG', 'ARTIO JoomSEF Ayarlarř');
define('_COM_SEF_TITLE_BASIC', 'Basit ayarlar');
define('_COM_SEF_ENABLED', 'Ţzin ver');
define('_COM_SEF_TT_ENABLED', 'If set to No the default SEF for Joomla/Mambo will be used');
define('_COM_SEF_DEF_404_PAGE', 'Varsayřlan 404 sayfasř');
define('_COM_SEF_REPLACE_CHAR', 'Replacement karekter');
define('_COM_SEF_TT_REPLACE_CHAR', 'Replacement karekterler');
define('_COM_SEF_PAGEREP_CHAR', 'Bo¦luk karekterleri');
define('_COM_SEF_TT_PAGEREP_CHAR', 'Bo¦luk ve benzeri durumlarda kullanřlacak karekter - genel olarak (-) kullanřlřr.');
define('_COM_SEF_STRIP_CHAR', 'Kullanřlmayacak karekterler');
define('_COM_SEF_TT_STRIP_CHAR', 'Bu karekterler kullanřlsa bile sayfa uzantřlarřnda g÷zŘkmeyecektir. |');
define('_COM_SEF_FRIENDTRIM_CHAR', 'Trim friendly characters');
define('_COM_SEF_TT_FRIENDTRIM_CHAR', 'Characters to trim from around the URL, separate with |');
define('_COM_SEF_USE_ALIAS', 'Kullanřlacak ba¦lřk');
define('_COM_SEF_TT_USE_ALIAS', 'Kullanřlacak ba¦lřk tŘrŘnŘ sešiniz.');
define('_COM_SEF_SUFFIX', 'Dosya son eki');
define('_COM_SEF_TT_SUFFIX', 'Extension to use for \\\'files\\\'. Leave blank to disable. A common entry here is \\\'html\\\'.');
define('_COM_SEF_ADDFILE', 'Varsayřlan index dosyalarř');
define('_COM_SEF_TT_ADDFILE', 'File name to place after a blank url / when no file exists.  Useful for bots that crawl your site looking for a specific file in that place but returns a 404 because there is none there.');
define('_COM_SEF_PAGETEXT', 'Sayfa Yazřsř');
define('_COM_SEF_TT_PAGETEXT', 'Text to append to url for multiple pages. Use %s to insert page number, by default it will be at end. If a suffix is defined, it will be added to the end of this string.');
define('_COM_SEF_LOWER', 'All lowercase');
define('_COM_SEF_TT_LOWER', 'Convert all characters to lowercase characters in the URL', 'All lowercase');
define('_COM_SEF_SHOW_SECT', 'B÷lŘmleri g÷ster');
define('_COM_SEF_TT_SHOW_SECT', 'Url b÷lŘmleri g÷sterilsin mi ?');
define('_COM_SEF_HIDE_CAT', 'Kategorileri sakla');
define('_COM_SEF_TT_HIDE_CAT', 'Set to yes to exclude the category name in url');
define('_COM_SEF_404PAGE', '404 sayfasř');
define('_COM_SEF_TT_404PAGE', 'Static content page to use for 404 Not Found errors (published state does not matter)');
//Removed 1.2.5: define('_COM_SEF_TITLE_ADV', 'Advanced Component Configuration');
define('_COM_SEF_TT_ADV', '<b>use default handler</b><br/>process normally, if an SEF Advanced extension is present it will be used instead. <br/><b>nocache</b><br/>do not store in DB and create old style SEF URLs<br/><b>skip</b><br/>do not make SEF urls for this component<br/>');
define('_COM_SEF_TT_ADV4', 'Ţleriki sešenekler ');
define('_COM_SEF_TITLE_MANAGER', 'ARTIO JoomSEF URL Y÷netimi');
define('_COM_SEF_VIEWMODE', 'G÷sterMode:');
define('_COM_SEF_SORTBY', 'Sort by:');
define('_COM_SEF_HITS', 'Hit');
define('_COM_SEF_DATEADD', 'Tarih Ekle');
define('_COM_SEF_SEFURL', 'SEF Url');
define('_COM_SEF_URL', 'Url');
define('_COM_SEF_REALURL', 'Geršek Url');
define('_COM_SEF_EDIT', 'DŘzenle');
define('_COM_SEF_ADD', 'Ekle');
define('_COM_SEF_NEWURL', 'Yeni SEF URL');
define('_COM_SEF_TT_NEWURL', 'Only relative redirection from the Joomla/Mambo root <i>without</i> a the leading slash');
define('_COM_SEF_OLDURL', 'Eski Non-SEF Url');
define('_COM_SEF_TT_OLDURL', 'Ba¦langřšta index.php');
define('_COM_SEF_SAVEAS', 'Direk y÷nlendirmeyi kaydet');
define('_COM_SEF_TITLE_SUPPORT', 'ARTIO JoomSEF Destek');
define('_COM_SEF_HELPVIA', '<b>Yardřmkanallarř:</b>');
define('_COM_SEF_PAGES', 'Resmi ŘrŘn sayfasř.');
define('_COM_SEF_FORUM', 'ARTIO Destek Formu');
define('_COM_SEF_HELPDESK', 'ARTIO Helpdesk (payed)');
define('_COM_SEF_TITLE_PURGE', 'ARTIO JoomSEF Purge Veritabanř');
define('_COM_SEF_USE_DEFAULT', '(Genel handler)');
define('_COM_SEF_NOCACHE', 'Nocache');
define('_COM_SEF_SKIP', 'Geš');
define('_COM_SEF_INSTALLED_VERS', 'Kurulmu¦ versiyon.:');
define('_COM_SEF_COPYRIGHT', 'Copyright');
define('_COM_SEF_LICENSE', 'Lisans');
define('_COM_SEF_SUPPORT_JOOMSEF', 'Destek');
define('_COM_SEF_CONFIG_UPDATED', 'Yapřlandřrmayř gŘncelle¦tir');
define('_COM_SEF_WRITE_ERROR', 'Hata. Yapřlandřrma yazřlmadř');
define('_COM_SEF_NOACCESS', 'Unable to access');
define('_COM_SEF_FATAL_ERROR_HEADERS', 'FATAL ERRPR: HEADER ALREADY SENT');
define('_COM_SEF_UPLOAD_OK', 'File was successfully uploaded');
define('_COM_SEF_ERROR_IMPORT', 'Error while importing:');
define('_COM_SEF_INVALID_SQL', 'INVALID DATA IN SQL FILE :');
define('_COM_SEF_NO_UNLINK', 'Unable to remove uploaded file from media directory');
define('_COM_SEF_IMPORT_OK', 'Custom URLS were successfully imported!');
define('_COM_SEF_WRITE_FAILED', 'Unable to write uploaded file to media directory');
define('_COM_SEF_EXPORT_FAILED', 'EXPORT FAILED!!!');
define('_COM_SEF_IMPORT_EXPORT', 'Import/Export Custom URLS');
define('_COM_SEF_SELECT_FILE', 'Please select a file first');
define('_COM_SEF_IMPORT', 'Import Custom URLS');
define('_COM_SEF_EXPORT', 'Backup Custom URLS');

// component interface
define('_COM_SEF_NOREAD', 'FATAL ERROR: Unable to read file ');
define('_COM_SEF_CHK_PERMS', 'Please check your file permissions and ensure that that this file can be read.');
define('_COM_SEF_DEBUG_DATA_DUMP', 'DEBUG DATA DUMP COMPLETE: Page Load Terminated');
define('_COM_SEF_STRANGE', 'Something strange has occured. This should not happen<br />');

// temporary
define('_COM_SEF_FULL_TITLE', 'Tam Ba¦lřk');
define('_COM_SEF_TITLE_ALIAS', 'Di­er Adřyla');
define('_COM_SEF_SHOW_CAT', 'Kategorileri G÷ster');

// new 1.2.5
// Advanced configuration
define('_COM_SEF_TITLE_ADV_CONF', 'Di­er  Yapřlandřrmalar');
define('_COM_SEF_REPLACEMENTS', 'De­i¦tirilecek karekterler');
define('_COM_SEF_TT_REPLACEMENTS', 'LŘtfen de­i¦tirilmesini istedi­iniz harfleri yazřnřz.');
// JoomFish configuration
define('_COM_SEF_JOOMFISH_CONF', 'JoomFish Yapřlandřrma');
//define('_COM_SEF_JF_LANG_TO_PATH', 'Dil patika?');
//define('_COM_SEF_TT_JF_LANG_TO_PATH', 'Sets whether different versions of language contents differ by path or page suffix.');
define('_COM_SEF_JF_ALWAYS_USE_LANG', 'Always use language?');
define('_COM_SEF_TT_JF_ALWAYS_USE_LANG', 'Always include language code in the generated URL.');
define('_COM_SEF_JF_TRANSLATE', 'Translate URLs?');
define('_COM_SEF_TT_JF_TRANSLATE', 'Use JoomFish to translate SEF URLs titles.');
// Component configuration
define('_COM_SEF_TITLE_COM_CONF', 'Bile¦en Yapřlandřrmasř');

// new 1.3.0
// Admin section URL filters
define('_COM_SEF_FILTERSEF', 'SEF adreslerini sŘz:');
define('_COM_SEF_FILTERSEF_HLP', 'To filter shown URLs by SEF URL, enter part of it into this field and hit ENTER.');
define('_COM_SEF_FILTER404', 'Adresleri sŘz:');
define('_COM_SEF_FILTERREAL', 'Geršek adresleri sŘz:');
define('_COM_SEF_FILTERREAL_HLP', 'To filter shown URLs by original URL, enter part of it into this field and hit ENTER.');
define('_COM_SEF_FILTERCOMPONENT', 'Bile¦enler:');
define('_COM_SEF_FILTERCOMPONENTALL', '(Hepsi)');

// Upgrade texts
define('_COM_SEF_TITLE_UPGRADE', 'ARTIO JoomSEF Y÷netimi GŘncelle¦tir');
define('_COM_SEF_UPGRADE', 'GŘncelle¦tir');
define('_COM_SEF_TITLE_NEWVERSION', 'Son versiyon:');
define('_COM_SEF_UPGRADEPACKAGE_HEADER', 'YŘkleme paket dosyalarř');
define('_COM_SEF_UPGRADEPACKAGE_LABEL', 'Paket dosyalarř:');
define('_COM_SEF_UPGRADEPACKAGE_SUBMIT', 'GŘncelle¦tirme dosyalarř &amp; Upgrade');
define('_COM_SEF_UPGRADESERVER_HEADER', 'Upgrade From ARTIO Server');
define('_COM_SEF_UPGRADESERVER_LINKTEXT', 'Upgrade From ARTIO Server');
define('_COM_SEF_UPGRADESERVER_LINKTITLE', 'Upgrade From ARTIO Server');
define('_COM_SEF_UPGRADE_BADPACKAGE', 'This package does not contain any upgrade informations.');
define('_COM_SEF_UPGRADE_BADSOURCE', 'Source not recognized.');
define('_COM_SEF_UPGRADE_CONNECTIONFAILED', 'Could not connect to the server.');
define('_COM_SEF_UPGRADE_SERVERUNAVAILABLE', 'Server not available.');

define('_COM_SEF_CANT_UPGRADE', 'Cannot upgrade.<br />Either your JoomSEF version is up to date or its upgrade is no longer supported.');
define('_COM_SEF_UPGRADE_INVALIDOPERATION', 'Invalid upgrade operation: ');
define('_COM_SEF_UPGRADE_INVALIDFILE', 'Invalid upgrade file: ');
define('_COM_SEF_UPGRADE_SQLERROR', 'Unable to execute SQL query: ');

define('_COM_SEF_URL_TTL', 'ADRESLER');
define('_COM_SEF_META_TTL', 'Meta Veri (optional)');
define('_COM_SEF_METATITLE', 'Ba¦lřk:');
define('_COM_SEF_META_INFO', 'Please note that JoomSEF Mambot must be installed and published for meta tag functionality to be working.<br />You may also wish to deactivate Joomla! standard meta tag generation in your site global configuration.');
define('_COM_SEF_META_TIP', 'JoomSEF MetaBot Notice');
define('_COM_SEF_METADESCRIPTION', 'Meta Ašřklama:');
define('_COM_SEF_METAKEYWORDS', 'Meta Anahtar:');
define('_COM_SEF_METALANG', 'Meta Dili:');
define('_COM_SEF_METAROBOTS', 'Meta Robot:');
define('_COM_SEF_METAGOOGLE', 'Meta Googlebot:');
define('_COM_SEF_TITLE_INFO', 'ARTIO JoomSEF Dosyalarř');

define('_COM_SEF_INSTALL_EXT', 'Kur');
define('_COM_SEF_UNINSTALL_EXT', 'Kaldřr');
define('_COM_SEF_TITLE_INSTALL_EXT', 'SEF eklentilerini kur');
define('_COM_SEF_TITLE_UNINSTALL_EXT', 'SEF eklentilerini kaldřr');
define('_COM_SEF_TITLE_INSTALL_NEW_EXT', 'SEF eklentilerini kaldřr');
define('_COM_SEF_INSTALLED_EXTS', 'Sef Eklentileri Kuruldu');
define('_COM_SEF_EXTS_INFO', 'Only those SEF extensions that can be uninstalled are displayed - some core extensions cannot be removed.');
define('_COM_SEF_EXT', 'SEF Eklentileri');
define('_COM_SEF_AUTHOR', 'Sahip');
define('_COM_SEF_VERSION', 'Versiyon');
define('_COM_SEF_DATE', 'Tarih');
define('_COM_SEF_AUTHOR_EMAIL', '-retici Email');
define('_COM_SEF_AUHTOR_URL', '-retici URL');
define('_COM_SEF_NONE_EXTS', 'There are no uninstallable SEF extensions installed.');

// new 1.3.3
define('_COM_SEF_SHOW3', 'TŘm y÷nlendirmeleri g÷ster');
define('_COM_SEF_PURGE_URLS', 'Do you wish to purge automatically created URLs?\n\nNote: You may wish to delete existing auto-created URLs if you have reconfigured the way they should look. This will NOT delete any URLs stored as Custom.');

// new 1.4.0
define('_COM_SEF_EXCLUDE_SOURCE', 'Exclude source info (Itemid)');
define('_COM_SEF_TT_EXCLUDE_SOURCE', 'This will exclude information about link source (Itemid) from URL.<br />This may prevent duplicate URLs, but probably will limit your Joomla! functionality.');
define('_COM_SEF_REAPPEND_SOURCE', 'Reappend source (Itemid)');
define('_COM_SEF_TT_REAPPEND_SOURCE', 'This setting reappends the Itemid to the SEF URL as query parameter.');
define('_COM_SEF_APPEND_NONSEF', 'Append non-SEF variables to URL');
define('_COM_SEF_TT_APPEND_NONSEF', 'Excludes often changing variables from SEF URL and appends them as non-SEF query.<br />This will decrease database usage and also prevent duplicate URLs in some extensions.');

define('_COM_SEF_JF_LANG_PLACEMENT', 'Language integration');
define('_COM_SEF_TT_JF_LANG_PLACEMENT', 'Where to add language constant in the generated URLs. Case <b>do not add</b> should be used only when path translation is active.');
define('_COM_SEF_LANG_PATH_TXT', 'include in path');
define('_COM_SEF_LANG_SUFFIX_TXT', 'add as suffix');
define('_COM_SEF_LANG_NONE_TXT', 'do not add');

define('_COM_SEF_UPLOAD_ERROR', 'ERROR UPLOADING FILE');
define('_COM_SEF_UPTODATE', 'Your JoomSEF is up to date.');

// new 1.5.0
define('_COM_SEF_RECORD_404', 'Record 404 page hits?');
define('_COM_SEF_TT_RECORD_404', 'Should we store 404 page hits to DB? Disabling this will descrease the number of SQL queries performed by JoomSEF, however you will not be able to see hits to noexisting pages at your site.');
define('_COM_SEF_TRANSIT_SLASH', 'Be tolerant to trailing slash?');
define('_COM_SEF_TT_TRANSIT_SLASH', 'Do we accept both URLs that do or do not end with trainling slash valid?');

// new 2.0.0
define('_COM_SEF_LANG_DOMAIN_TXT', 'use different domains');
define('_COM_SEF_JF_DOMAIN', 'Domain configuration');
define('_COM_SEF_TT_JF_DOMAIN', 'Define the live site for each language (without the trailing slash).');
define('_COM_SEF_ALT_DOMAIN', 'Alternative live sites');
define('_COM_SEF_TT_ALT_DOMAIN', 'List of alternative live site domains (and paths) (different than your site domain in configuration), that JoomSEF should also accept (use e.g. when your SSL-secured domain is different than the original one or on special configurations). More entries need to be separated by comma.');

define('_COM_SEF_INSTALLED_PATCHES', 'Installed SEF Patches');
define('_COM_SEF_PATCHES_INFO', "You can manage SEF Patches using standard Joomla! Mambot system. Don't forget to publish those patches you want to use.");
define('_COM_SEF_PATCH', 'SEF Patch');
define('_COM_SEF_NONE_PATCHES', 'There are no SEF Patches installed.');

define('_COM_SEF_EDIT_EXT', 'DŘzenle');
define('_COM_SEF_TITLE_EDIT_EXT', 'SEF eklentilerini dŘzenle');
define('_COM_SEF_ADV_HANDLING', 'Handling');
define('_COM_SEF_ADV_TITLE', 'Custom title');
define('_COM_SEF_TT_ADV_TITLE', 'Overrides the default menu title in URL. Leave blank for default behaviour.');
define('_COM_SEF_DELETE_FILTER', 'Delete All Filtered');
define('_COM_SEF_TITLE_DELETE_FILTER', 'Deletes all URLs matching the filters.');
define('_COM_SEF_DELETE_FILTER_QUESTION', 'Are you sure you want to delete all the URLs matching selected filters? (All pages will be deleted.)');
define('_COM_SEF_IGNORE_SOURCE', 'Ignore multiple sources (Itemids)');
define('_COM_SEF_TT_IGNORE_SOURCE', 'When selected, only one URL will be generated for every page, even when there is more than one Itemid pointing to it.');
define('_COM_SEF_USE_CACHE', 'cache Kullan?');
define('_COM_SEF_TT_USE_CACHE', 'If set to Yes, URLs will be saved to cache so less queries will be made to database.');
define('_COM_SEF_CACHE_SIZE', 'Maximum cache boyutu:');
define('_COM_SEF_TT_CACHE_SIZE', 'How many URLs can be saved in cache.');
define('_COM_SEF_CACHE_MINHITS', 'Minimum cache hit:');
define('_COM_SEF_TT_CACHE_MINHITS', 'How many hits must URL have to be saved in cache.');
define('_COM_SEF_CLEAN_CACHE', 'Temizle Cache');
define('_COM_SEF_TITLE_CLEAN_CACHE', 'Cleans the cache of URLs.');
define('_COM_SEF_CLEAN_CACHE_QUESTION', 'Cleaning the cache is recommended every time you change the setting of the cache or you edit any of your custom URLs. Are you sure you want to clean the cache?');

define('_COM_SEF_EXTUPGRADE_TITLE', 'SEF Eklentileri');
define('_COM_SEF_NOTAVAILABLE', 'Not available');
define('_COM_SEF_PARAMETERS', 'Parametere');
define('_COM_SEF_DESCRIPTION', 'Ašřklama');
define('_COM_SEF_NAME', 'Ţsim');
define('_COM_SEF_CACHE_CONF', 'Cache Yapřlandřrma');
define('_COM_SEF_ITEMID', 'Itemid');
define('_COM_SEF_TT_ITEMID', 'Menu item associated with this URL.');

define('_COM_SEF_NONSEFREDIRECT', 'Redirect nonSEF URLs to SEF?');
define('_COM_SEF_TT_NONSEFREDIRECT', 'When someone types nonSEF URL in his browser he will be redirected to its SEF equivalent with Moved Permanently header.');

define('_COM_SEF_USEMOVED', 'Use Moved Permanently redirection table?');
define('_COM_SEF_TT_USEMOVED', 'When you change the SEF url, it can be saved to redirection table and will remain working with Moved Permanently header.');
define('_COM_SEF_USEMOVEDASK', 'Ask before saving URL to Moved Permanently table?');
define('_COM_SEF_TT_USEMOVEDASK', 'If set to No, URL will be saved automatically anytime you change it.');
define('_COM_SEF_VIEW301DESC', 'View/Edit Moved Permanently Redirects');
define('_COM_SEF_VIEW301', 'View/Edit 301 Urls');
define('_COM_SEF_PURGE301DESC', 'Purge Moved Permanently Redirects');
define('_COM_SEF_PURGE301', 'Purge 301 Urls');

define('_COM_SEF_301OLDURL', 'Moved from URL');
define('_COM_SEF_301NEWURL', 'Moved to URL');
define('_COM_SEF_TT_301OLDURL', 'Bu Url Y÷nlendirmede');
define('_COM_SEF_TT_301NEWURL', 'Bu Url\'yi y÷nlendir.');

define('_COM_SEF_DAYS', 'Sonra Kullan');
define('_COM_SEF_FILTEROLD_HLP', 'To filter shown URLs by Moved from URL, enter part of it into this field and hit ENTER.');
define('_COM_SEF_FILTERNEW_HLP', 'To filter shown URLs by Moved to URL, enter part of it into this field and hit ENTER.');
define('_COM_SEF_FILTEROLD', 'URL křmřldatřlan filtre.:');
define('_COM_SEF_FILTERNEW', 'Filter Moved to URL:');
define('_COM_SEF_FILTERDAY', 'Not used for (days):');
define('_COM_SEF_NEVER', 'Never');

define('_COM_SEF_CACHECLEANED', 'Cache Temizlendi');
define('_COM_SEF_CONFIRM301', 'Your SEF link has changed. Do you wish to save the old one to Moved Permanently redirection table so it will be still working?');

define('_COM_SEF_DISABLENEWSEF', 'Yeni SEF Url\'ler?');
define('_COM_SEF_TT_DISABLENEWSEF', 'If set to yes, no new URLs will be generated and only those already in database will be used.');
define('_COM_SEF_DONTSHOWTITLE', 'MenŘ Ba¦lřk URL');
define('_COM_SEF_TT_DONTSHOWTITLE', 'If checked, the menu title will not be present in URL at all (except the direct link to component).');
define('_COM_SEF_SHOW4', 'Anasayfa linklerini g÷ster');
define('_COM_SEF_REINSTALL', 'You have uploaded the package with same version as your current JoomSEF, reinstall instead of upgrade has been initiated.');

define('_COM_SEF_DONTREMOVESID', 'Do not remove SID from SEF URL?');
define('_COM_SEF_TT_DONTREMOVESID', 'If set to yes, the sid variable will not be removed from SEF URL. This may help some components to work properly, but also can create duplicates with some others.');

// new 2.2.9
define('_COM_SEF_IMPORT_URLS', 'Import URLs');
define('_COM_SEF_EXPORT_SELECTED', 'Export Selected');
define('_COM_SEF_EXPORT_FILTERED', 'Export Filtered');
define('_COM_SEF_404ITEMID', 'ItemID for Default 404 Page');
define('_COM_SEF_TT_404ITEMID', 'Set your own ItemID for Default 404 Page. Leave blank to not use ItemID. You can find ItemID in Menu Manager.');

// new 2.3.1
define('_COM_SEF_CREATE_301', 'Create 301');
define('_COM_SEF_USEINDEX', 'Use index for sections and categories');
define('_COM_SEF_TT_USEINDEX', 'If set to Yes, the default index file will be appended to links to sections and categories.');
define('_COM_SEF_CHECKJUNKURLS', 'Check variables validity?');
define('_COM_SEF_TT_CHECKJUNKURLS', 'If set to yes, the option, task and id variables will be checked for validity.');
define('_COM_SEF_CACHE_FLOCK', 'Use standard file locking?');
define('_COM_SEF_TT_CACHE_FLOCK', 'Set to No if you experience problems with corrupted cache file.');

// 2.3.2
define('_COM_SEF_CUSTOMNONSEF', 'Custom non-SEF variables');
define('_COM_SEF_TT_CUSTOMNONSEF', 'Here you can specify custom non-SEF variables separated by semicolon globally for every URL.');
define('_COM_SEF_COMMON_PARAMETERS', 'Common Parameters');
define('_COM_SEF_EXT_PARAMETERS', 'Extension Parameters');

?>
