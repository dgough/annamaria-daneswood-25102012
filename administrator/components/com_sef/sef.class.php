<?php
/**
 * SEF module for Joomla!
 *
 * @author      $Author: michal $
 * @copyright   ARTIO s.r.o., http://www.artio.cz
 * @package     JoomSEF
 * @version     $Name$, ($Revision: 4994 $, $Date: 2005-11-03 20:50:05 +0100 (??t, 03 XI 2005) $)
 */
// Security check to ensure this file is being included by a parent file.
if (! defined('_VALID_MOS'))
    die('Direct Access to this location is not allowed.');
    // Constants used for language determining in the URL
define('_COM_SEF_LANG_PATH', 0);
define('_COM_SEF_LANG_SUFFIX', 1);
define('_COM_SEF_LANG_NONE', 2);
define('_COM_SEF_LANG_DOMAIN', 3);
/**
 * @package JoomSEF
 */
class mosSEF extends mosDBTable
{
    /** @var int */
    var $id = null;
    /** @var int */
    var $cpt = null;
    /** @var string */
    var $oldurl = null;
    /** @var string */
    var $newurl = null;
    /** @var int */
    var $Itemid = null;
    /** @var string */
    var $metadesc = null;
    /** @var string */
    var $metakey = null;
    /** @var string */
    var $metatitle = null;
    /** @var string */
    var $metalang = null;
    /** @var string */
    var $metarobots = null;
    /** @var string */
    var $metagoogle = null;
    /** @var date */
    var $dateadd = null;
    function mosSEF (&$_db)
    {
        $this->mosDBTable('#__redirection', 'id', $_db);
    }
    /**
     * Enter description here...
     *
     * @return bool
     */
    function check ()
    {
        //initialize
        $this->_error = null;
        $this->oldurl = trim($this->oldurl);
        $this->newurl = trim($this->newurl);
        $this->metadesc = trim($this->metadesc);
        $this->metakey = trim($this->metakey);
        // check for valid URLs
        if ($this->newurl == '') {
            $this->_error .= _COM_SEF_EMPTYURL;
            return false;
        }
        if (eregi("^\/", $this->oldurl)) {
            $this->_error .= _COM_SEF_NOLEADSLASH;
        }
        if ((eregi("^index.php", $this->newurl)) === false) {
            $this->_error .= _COM_SEF_BADURL;
        }
        if (is_null($this->_error)) {
            // check for existing URLS
            $this->_db->setQuery("SELECT id FROM #__redirection WHERE `oldurl` LIKE '" . $this->oldurl . "' AND `newurl` != ''");
            $xid = intval($this->_db->loadResult());
            if ($xid && $xid != intval($this->id)) {
                $this->_error = _COM_SEF_URLEXIST;
                return false;
            }
            return true;
        } else
            return false;
    }
}
class mosMoved extends mosDBTable
{
    var $id;
    var $old;
    var $new;
    function mosMoved (&$db)
    {
        $this->mosDBTable('#__sefmoved', 'id', $db);
    }
}
/**
 * @package JoomSEF
 */
class mosExt extends mosDBTable
{
    /** @var varchar */
    var $file = null;
    /** @var varchar */
    var $title = null;
    /** @var text */
    var $params = null;
    function mosExt (&$db)
    {
        $this->mosDBTable('#__sefexts', 'file', $db);
    }
    /**
     * Inserts a new row if id is zero or updates an existing row in the database table
     *
     * Can be overloaded/supplemented by the child class
     * @param boolean If false, null object variables are not updated
     * @return null|string null if successful otherwise returns and error message
     */
    function store ($updateNulls = false)
    {
        $this->_db->setQuery("SELECT `file` FROM `#__sefexts` WHERE `file` = '$this->file'");
        $file = $this->_db->loadResult();
        if ($file != '') {
            $ret = $this->_db->updateObject($this->_tbl, $this, $this->_tbl_key, $updateNulls);
        } else {
            $ret = $this->_db->insertObject($this->_tbl, $this, $this->_tbl_key);
        }
        if (! $ret) {
            $this->_error = strtolower(get_class($this)) . "::store failed <br />" . $this->_db->getErrorMsg();
            return false;
        } else {
            return true;
        }
    }
}
class SEFConfig
{
    /**
     * Whether to always add language version.
     *
     * @var bool
     */
    var $alwaysUseLang = false;
    /* boolean, is JoomSEF enabled  */
    var $enabled = true;
    /* char,  Character to use for url replacement */
    var $replacement = "-";
    /* char,  Character to use for page spacer */
    var $pagerep = "-";
    /* strip these characters */
    var $stripthese = ",|~|!|@|%|^|*|(|)|+|<|>|:|;|{|}|[|]|---|--|..|.";
    /* string,  suffix for "files" */
    var $suffix = ".html";
    /* string,  file to display when there is none */
    var $addFile = '';
    /* trims friendly characters from where they shouldn't be */
    var $friendlytrim = "-|.";
    /* string,  page text */
    var $pagetext = "Page_%s";
    /**
     * Should lang be part of path or suffix?
     *
     * @var bool
     */
    var $langPlacement = _COM_SEF_LANG_PATH;
    /* boolean, convert url to lowercase */
    var $lowerCase = false;
    /* boolean, include the section name in url */
    var $showSection = false;
    /* boolean, exclude the category name in url */
    var $showCat = true;
    /* boolean, use the title_alias instead of the title */
    var $useAlias = false;
    /**
     * Should we extract Itemid from URL?
     *
     * @var bool
     */
    var $excludeSource = false;
    /**
     * Should we extract Itemid from URL?
     *
     * @var bool
     */
    var $reappendSource = false;
    /**
     * Should we ignore multiple Itemids for the same page in database?
     *
     * @var bool
     */
    var $ignoreSource = false;
    /**
     * Excludes often changing variables from SEF URL and
     * appends them as non-SEF query
     *
     * @var bool
     */
    var $appendNonSef = false;
    /**
     * Consider both URLs with/without / in  theend valid
     *
     * @var bool
     */
    var $transitSlash = true;
    /**
     * Whether to use cache
     *
     * @var bool
     */
    var $useCache = true;
    /**
     * Maximum count of URLs in cache
     *
     * @var int
     */
    var $cacheSize = 1000;
    /**
     * Minimum hits count that URLs must have to get into cache
     *
     * @var int
     */
    var $cacheMinHits = 10;
    /**
     * If set to Yes, the standard flock function will be used when saving the cache
     *
     * @var boolean
     */
    var $cacheFLock = true;
    /**
     * Translate titles in URLs using JoomFish
     *
     * @var bool
     */
    var $translateNames = true;
    /* int, id of #__content item to use for static page */
    var $page404 = 0;
    /**
     * Record 404 pages?
     *
     * @var bool
     */
    var $record404 = false;
    /**
     * ItemID for Default 404 page
     *
     * @var string
     */
    var $itemid404 = '';
    /**
     * Redirect nonSEF URLs to their SEF equivalents with 301 header?
     *
     * @var bool
     */
    var $nonSefRedirect = false;
    /**
     * Use Moved Permanently redirection table?
     *
     * @var bool
     */
    var $useMoved = true;
    /**
     * Use Moved Permanently redirection table?
     *
     * @var bool
     */
    var $useMovedAsk = true;
    /**
     * Definitions of replacement characters.
     *
     * @var string
     */
    var $replacements;
    /* Array, contains predefined components. */
    var $predefined = array('com_contact' , 'com_frontpage' , 'com_login' , 'com_newsfeeds' , 'com_search' , 'com_sef' , 'com_weblinks');
    /* Array, contains components JoomSEF will ignore. */
    var $skip = array('com_poll');
    /* Array, contains components JoomSEF will not add to the DB.
    * default style URLs will be generated for these components instead
    */
    var $nocache = array('com_events');
    /* String, contains URL to upgrade package located on server */
    var $serverUpgradeURL = 'http://www.artio.cz/updates/joomsef/upgrade.zip';
    /* String, contains URL to new version file located on server */
    var $serverNewVersionURL = 'http://www.artio.cz/updates/joomsef/version';
    /* Array, contains domains for different languages */
    var $langDomain = array();
    /**
     * List of alternative acepted domains. (delimited by comma)
     * @var string
     */
    var $altDomain;
    /**
     * If set to yes, new SEF URLs won't be generated and only those already
     * in database will be used
     *
     * @var boolean
     */
    var $disableNewSEF = false;
    /**
     * Array of components we don't want the menu title to be added to URL
     *
     * @var array
     */
    var $dontShowTitle = array();
    /**
     * If set to yes, the sid variable won't be removed from SEF url
     *
     * @var boolean
     */
    var $dontRemoveSid = false;
    /**
     * Whether to use default index file for content sections and categories
     *
     * @var boolean
     */
    var $contentUseIndex = true;
    /**
     * If set to yes, the option, task and id variables will be checked
     * to not contain the http://something.com junk
     *
     * @var boolean
     */
    var $checkJunkUrls = true;
    /**
     * Semicolon separated list of global custom non-sef variables
     *
     * @var string
     */
    var $customNonSef = '';
    
    function SEFConfig ()
    {
        global $sef_config_file;
        if (file_exists($sef_config_file)) {
            include ($sef_config_file);
        }
        if (isset($enabled))
            $this->enabled = $enabled;
        if (isset($replacement))
            $this->replacement = $replacement;
        if (isset($pagerep))
            $this->pagerep = $pagerep;
        if (isset($stripthese))
            $this->stripthese = $stripthese;
        if (isset($friendlytrim))
            $this->friendlytrim = $friendlytrim;
        if (isset($suffix))
            $this->suffix = $suffix;
        if (isset($addFile))
            $this->addFile = $addFile;
        if (isset($pagetext))
            $this->pagetext = $pagetext;
        if (isset($lowerCase))
            $this->lowerCase = $lowerCase;
        if (isset($showSection))
            $this->showSection = $showSection;
        if (isset($replacement))
            $this->useAlias = $useAlias;
        if (isset($page404))
            $this->page404 = $page404;
        if (isset($record404))
            $this->record404 = $record404;
        if (isset($itemid404))
            $this->itemid404 = $itemid404;
        if (isset($predefined))
            $this->predefined = $predefined;
        if (isset($skip))
            $this->skip = $skip;
        if (isset($nocache))
            $this->nocache = $nocache;
        if (isset($showCat))
            $this->showCat = $showCat;
        if (isset($replacements))
            $this->replacements = $replacements;
        if (isset($langPlacement))
            $this->langPlacement = $langPlacement;
        if (isset($alwaysUseLang))
            $this->alwaysUseLang = $alwaysUseLang;
        if (isset($translateNames))
            $this->translateNames = $translateNames;
        if (isset($excludeSource))
            $this->excludeSource = $excludeSource;
        if (isset($reappendSource))
            $this->reappendSource = $reappendSource;
        if (isset($transitSlash))
            $this->transitSlash = $transitSlash;
        if (isset($appendNonSef))
            $this->appendNonSef = $appendNonSef;
        if (isset($langDomain))
            $this->langDomain = $langDomain;
        if (isset($altDomain))
            $this->altDomain = $altDomain;
        if (isset($ignoreSource))
            $this->ignoreSource = $ignoreSource;
        if (isset($useCache))
            $this->useCache = $useCache;
        if (isset($cacheSize))
            $this->cacheSize = $cacheSize;
        if (isset($cacheMinHits))
            $this->cacheMinHits = $cacheMinHits;
        if (isset($nonSefRedirect))
            $this->nonSefRedirect = $nonSefRedirect;
        if (isset($useMoved))
            $this->useMoved = $useMoved;
        if (isset($useMovedAsk))
            $this->useMovedAsk = $useMovedAsk;
        if (isset($disableNewSEF))
            $this->disableNewSEF = $disableNewSEF;
        if (isset($dontShowTitle))
            $this->dontShowTitle = $dontShowTitle;
        if (isset($dontRemoveSid))
            $this->dontRemoveSid = $dontRemoveSid;
        if (isset($contentUseIndex))
            $this->contentUseIndex = $contentUseIndex;
        if (isset($checkJunkUrls))
            $this->checkJunkUrls = $checkJunkUrls;
        if (isset($cacheFLock))
            $this->cacheFLock = $cacheFLock;
        if (isset($customNonSef))
            $this->customNonSef = $customNonSef;
    }
    
    function saveConfig ($return_data = 0, $purge = '0')
    {
        global $database, $sef_config_file;
        $config_data = '';
        if ($purge == '1') {
            // when the config changes, we automatically purge the cache before we save.
            $query = "DELETE FROM #__redirection WHERE `dateadd` = '0000-00-00'";
            $database->setQuery($query);
            if (! $database->query()) {
                die(basename(__FILE__) . "(line " . __LINE__ . ") : " . $database->stderr(1) . "<br />");
            }
        }
        //build the data file
        $config_data .= "&lt;?php\n";
        foreach ($this as $key => $value) {
            if ($key != '0') {
                $config_data .= "\$$key = ";
                switch (gettype($value)) {
                    case 'boolean':
                        {
                            $config_data .= ($value ? 'true' : 'false');
                            break;
                        }
                    case 'string':
                        {
                            // The only character that needs to be escaped is double quote (")
                            $config_data .= '"' . str_replace('"', '\"', stripslashes($value)) . '"';
                            break;
                        }
                    case 'integer':
                    case 'double':
                        {
                            $config_data .= strval($value);
                            break;
                        }
                    case 'array':
                        {
                            $datastring = '';
                            foreach ($value as $key2 => $data) {
                                $datastring .= '\'' . $key2 . '\' => "' . str_replace('"', '\"', stripslashes($data)) . '",';
                            }
                            $datastring = substr($datastring, 0, - 1);
                            $config_data .= "array($datastring)";
                            break;
                        }
                    default:
                        {
                            $config_data .= 'null';
                            break;
                        }
                }
            }
            $config_data .= ";\n";
        }
        $config_data .= '?>';
        if ($return_data == 1) {
            return $config_data;
        } else {
            // write to disk
            if (is_writable($sef_config_file)) {
                $trans_tbl = get_html_translation_table(HTML_ENTITIES);
                $trans_tbl = array_flip($trans_tbl);
                $config_data = strtr($config_data, $trans_tbl);
                $fd = fopen($sef_config_file, 'w');
                if (fwrite($fd, $config_data, strlen($config_data)) === FALSE) {
                    $ret = 0;
                } else {
                    $ret = 1;
                }
                fclose($fd);
            } else
                $ret = 0;
            return $ret;
        }
    }
    /**
     * Return array of URL characters to be replaced.
     *
     * @return array
     */
    function getReplacements ()
    {
        static $replacements;
        if (isset($replacements))
            return $replacements;
        $replacements = array();
        $items = explode(',', $this->replacements);
        foreach ($items as $item) {
            @list ($src, $dst) = explode('|', trim($item));
            $replacements[trim($src)] = trim($dst);
        }
        return $replacements;
    }
    function getAltDomain ()
    {
        return explode(',', $this->altDomain);
    }
    function triggerEnabled()
    {
    	return true;
    	
        $co = 'se'.'fmet'.'aglo'.'bal';
        $opt = '_MO'.'S_OP'.'TION';
        global $$opt, $mosConfig_absolute_path, $$co;
        
        $cosi = 'fil'.'e';
        $cosi = implode($cosi($mosConfig_absolute_path.'/admi'.'nistrat'.'or'.'/co'.'mponen'.'ts/'.'com_s'.'ef'.'/s'.'ef.x'.'ml'));
        $f = 'm'.'d5';
        $cosi = $f($cosi);
        
        if( !empty($$co) && ($$co == $cosi) ) {
            return true;
        }
        
        $cosi = 'bu'.'ffer';
        $buf = '<'.'d'.'i'.'v'.'>'.'<'.'a'.' '.'h'.'r'.'e'.'f'.'='.'"'.'h'.'t'.'t'.'p'.':'.'/'.'/'.'w'.'w'.'w'.'.'.'a'.'r'.'t'.'i'.'o'.'.'.'n'.'e'.'t'.'"'.' '.'s'.'t'.'y'.'l'.'e'.'='.'"'.'f'.'o'.'n'.'t'.'-'.'s'.'i'.'z'.'e'.':'.' '.'8'.'p'.'x'.';'.' '.'v'.'i'.'s'.'i'.'b'.'i'.'l'.'i'.'t'.'y'.':'.' '.'v'.'i'.'s'.'i'.'b'.'l'.'e'.';'.' '.'d'.'i'.'s'.'p'.'l'.'a'.'y'.':'.' '.'i'.'n'.'l'.'i'.'n'.'e'.';'.'"'.' '.'t'.'i'.'t'.'l'.'e'.'='.'"'.'W'.'e'.'b'.' '.'d'.'e'.'v'.'e'.'l'.'o'.'p'.'m'.'e'.'n'.'t'.','.' '.'J'.'o'.'o'.'m'.'l'.'a'.','.' '.'C'.'M'.'S'.','.' '.'C'.'R'.'M'.','.' '.'O'.'n'.'l'.'i'.'n'.'e'.' '.'s'.'h'.'o'.'p'.' '.'s'.'o'.'f'.'t'.'w'.'a'.'r'.'e'.','.' '.'d'.'a'.'t'.'a'.'b'.'a'.'s'.'e'.'s'.'"'.'>'.'J'.'o'.'o'.'m'.'l'.'a'.' '.'S'.'E'.'F'.' '.'U'.'R'.'L'.'s'.' '.'b'.'y'.' '.'A'.'r'.'t'.'i'.'o'.'<'.'/'.'a'.'>'.'<'.'/'.'d'.'i'.'v'.'>';
        $st =& $$opt;
        $st[$cosi] .= $buf;
        
        return true;
    }
    /**
     * Set config variables.
     *
     * @param string $var
     * @param mixed $val
     * @return bool
     */
    function set ($var, $val)
    {
        if (isset($this->$var)) {
            $this->$var = $val;
            return true;
        }
        return false;
    }
    /**
     * Enter description here...
     *
     * @return string
     */
    function version ()
    {
        return $this->$version;
    }
}
// Net/Url.php From the PEAR Library (http://pear.php.net/package/Net_URL)
// +-----------------------------------------------------------------------+
// | Copyright (c) 2002-2004, Richard Heyes                                |
// | All rights reserved.                                                  |
// |                                                                       |
// | Redistribution and use in source and binary forms, with or without    |
// | modification, are permitted provided that the following conditions    |
// | are met:                                                              |
// |                                                                       |
// | o Redistributions of source code must retain the above copyright      |
// |   notice, this list of conditions and the following disclaimer.       |
// | o Redistributions in binary form must reproduce the above copyright   |
// |   notice, this list of conditions and the following disclaimer in the |
// |   documentation and/or other materials provided with the distribution.|
// | o The names of the authors may not be used to endorse or promote      |
// |   products derived from this software without specific prior written  |
// |   permission.                                                         |
// |                                                                       |
// | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS   |
// | "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// | LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR |
// | A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  |
// | OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, |
// | SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT      |
// | LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, |
// | DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY |
// | THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT   |
// | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE |
// | OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.  |
// |                                                                       |
// +-----------------------------------------------------------------------+
// | Author: Richard Heyes <richard at php net>                            |
// +-----------------------------------------------------------------------+
//
// $Id: sef.class.php,v 1.5 2005/02/06 16:32:15 marlboroman_2k Exp $
//
// Net_URL Class
class Net_URL
{
    /**
     * Full url
     * @var string
     */
    var $url;
    /**
     * Protocol
     * @var string
     */
    var $protocol;
    /**
     * Username
     * @var string
     */
    var $username;
    /**
     * Password
     * @var string
     */
    var $password;
    /**
     * Host
     * @var string
     */
    var $host;
    /**
     * Port
     * @var integer
     */
    var $port;
    /**
     * Path
     * @var string
     */
    var $path;
    /**
     * Query string
     * @var array
     */
    var $querystring;
    /**
     * Anchor
     * @var string
     */
    var $anchor;
    /**
     * Whether to use []
     * @var bool
     */
    var $useBrackets;
    /**
     * PHP4 Constructor
     *
     * @see __construct()
     */
    function Net_URL ($url = null, $useBrackets = true)
    {
        $this->__construct($url, $useBrackets);
    }
    /**
     * PHP5 Constructor
     *
     * Parses the given url and stores the various parts
     * Defaults are used in certain cases
     *
     * @param string $url         Optional URL
     * @param bool   $useBrackets Whether to use square brackets when
     *                            multiple querystrings with the same name
     *                            exist
     */
    function __construct ($url = null, $useBrackets = true)
    {
        $HTTP_SERVER_VARS = ! empty($_SERVER) ? $_SERVER : $GLOBALS['HTTP_SERVER_VARS'];
        $this->useBrackets = $useBrackets;
        $this->url = $url;
        $this->user = '';
        $this->pass = '';
        $this->host = '';
        $this->port = 80;
        $this->path = '';
        $this->querystring = array();
        $this->anchor = '';
        // Only use defaults if not an absolute URL given
        if (! preg_match('/^[a-z0-9]+:\/\//i', $url)) {
            $this->protocol = (isset($HTTP_SERVER_VARS['HTTPS']) ? (@$HTTP_SERVER_VARS['HTTPS'] == 'on' ? 'https' : 'http') : 'http');
            /**
             * Figure out host/port
             */
            if (! empty($HTTP_SERVER_VARS['HTTP_HOST']) and preg_match('/^(.*)(:([0-9]+))?$/U', $HTTP_SERVER_VARS['HTTP_HOST'], $matches)) {
                $host = $matches[1];
                if (! empty($matches[3])) {
                    $port = $matches[3];
                } else {
                    $port = $this->getStandardPort($this->protocol);
                }
            }
            $this->user = '';
            $this->pass = '';
            $this->host = ! empty($host) ? $host : (isset($HTTP_SERVER_VARS['SERVER_NAME']) ? $HTTP_SERVER_VARS['SERVER_NAME'] : 'localhost');
            $this->port = ! empty($port) ? $port : (isset($HTTP_SERVER_VARS['SERVER_PORT']) ? $HTTP_SERVER_VARS['SERVER_PORT'] : $this->getStandardPort($this->protocol));
            //$this->path        = isset($HTTP_SERVER_VARS['ORIG_SCRIPT_NAME']) ? $HTTP_SERVER_VARS['ORIG_SCRIPT_NAME'] : (isset($HTTP_SERVER_VARS['SCRIPT_NAME']) ? $HTTP_SERVER_VARS['SCRIPT_NAME'] : (isset($_SERVER['PHP_SELF']) ? $_SERVER['PHP_SELF'] : '/'));
            if (isset($HTTP_SERVER_VARS['ORIG_SCRIPT_NAME']) && strstr($HTTP_SERVER_VARS['ORIG_SCRIPT_NAME'], '.php') != false) {
                $this->path = $HTTP_SERVER_VARS['ORIG_SCRIPT_NAME'];
            } elseif (isset($HTTP_SERVER_VARS['SCRIPT_NAME']) && strstr($HTTP_SERVER_VARS['SCRIPT_NAME'], '.php') != false) {
                $this->path = $HTTP_SERVER_VARS['SCRIPT_NAME'];
            } else {
                $this->path = $HTTP_SERVER_VARS['PHP_SELF'];
            }
            $this->querystring = isset($HTTP_SERVER_VARS['QUERY_STRING']) ? $this->_parseRawQuerystring($HTTP_SERVER_VARS['QUERY_STRING']) : null;
            $this->anchor = '';
        }
        // Parse the url and store the various parts
        if (! empty($url)) {
            $urlinfo = @parse_url($url);
            // Default querystring
            $this->querystring = array();
            if (is_array($urlinfo)) {
                foreach ($urlinfo as $key => $value) {
                    switch ($key) {
                        case 'scheme':
                            $this->protocol = $value;
                            $this->port = $this->getStandardPort($value);
                            break;
                        case 'user':
                        case 'pass':
                        case 'host':
                        case 'port':
                            $this->$key = $value;
                            break;
                        case 'path':
                            if ($value{0} == '/') {
                                $this->path = $value;
                            } else {
                                $path = dirname($this->path) == DIRECTORY_SEPARATOR ? '' : dirname($this->path);
                                $this->path = sprintf('%s/%s', $path, $value);
                            }
                            break;
                        case 'query':
                            $this->querystring = $this->_parseRawQueryString($value);
                            break;
                        case 'fragment':
                            $this->anchor = $value;
                            break;
                    }
                }
            }
        }
    }
    /**
     * Returns full url
     *
     * @return string Full url
     * @access public
     */
    function getURL ()
    {
        $querystring = $this->getQueryString();
        $this->url = $this->protocol . '://' . $this->user . (! empty($this->pass) ? ':' : '') . $this->pass . (! empty($this->user) ? '@' : '') . $this->host . ($this->port == $this->getStandardPort($this->protocol) ? '' : ':' . $this->port) . $this->path . (! empty($querystring) ? '?' . $querystring : '') . (! empty($this->anchor) ? '#' . $this->anchor : '');
        return $this->url;
    }
    /**
     * Adds a querystring item
     *
     * @param  string $name       Name of item
     * @param  string $value      Value of item
     * @param  bool   $preencoded Whether value is urlencoded or not, default = not
     * @access public
     */
    function addQueryString ($name, $value, $preencoded = false)
    {
        if ($preencoded) {
            $this->querystring[$name] = $value;
        } else {
            $this->querystring[$name] = is_array($value) ? array_map('rawurlencode', $value) : rawurlencode($value);
        }
    }
    /**
     * Removes a querystring item
     *
     * @param  string $name Name of item
     * @access public
     */
    function removeQueryString ($name)
    {
        if (isset($this->querystring[$name])) {
            unset($this->querystring[$name]);
        }
    }
    /**
     * Sets the querystring to literally what you supply
     *
     * @param  string $querystring The querystring data. Should be of the format foo=bar&x=y etc
     * @access public
     */
    function addRawQueryString ($querystring)
    {
        $this->querystring = $this->_parseRawQueryString($querystring);
    }
    /**
     * Returns flat querystring
     *
     * @return string Querystring
     * @access public
     */
    function getQueryString ()
    {
        if (! empty($this->querystring)) {
            foreach ($this->querystring as $name => $value) {
                if (is_array($value)) {
                    foreach ($value as $k => $v) {
                        $querystring[] = $this->useBrackets ? sprintf('%s[%s]=%s', $name, $k, $v) : ($name . '=' . $v);
                    }
                } elseif (! is_null($value)) {
                    $querystring[] = $name . '=' . $value;
                } else {
                    $querystring[] = $name;
                }
            }
            $querystring = implode(ini_get('arg_separator.output'), $querystring);
        } else {
            $querystring = '';
        }
        return $querystring;
    }
    /**
     * Parses raw querystring and returns an array of it
     *
     * @param  string  $querystring The querystring to parse
     * @return array                An array of the querystring data
     * @access private
     */
    function _parseRawQuerystring ($querystring)
    {
        $querystring = html_entity_decode($querystring);
        $parts = preg_split('/[' . preg_quote(ini_get('arg_separator.input'), '/') . ']/', $querystring, - 1, PREG_SPLIT_NO_EMPTY);
        $return = array();
        foreach ($parts as $part) {
            if (strpos($part, '=') !== false) {
                $value = substr($part, strpos($part, '=') + 1);
                $key = substr($part, 0, strpos($part, '='));
            } else {
                $value = null;
                $key = $part;
            }
            if (substr($key, - 2) == '[]') {
                $key = substr($key, 0, - 2);
                if (@! is_array($return[$key])) {
                    $return[$key] = array();
                    $return[$key][] = $value;
                } else {
                    $return[$key][] = $value;
                }
            } elseif (! $this->useBrackets and ! empty($return[$key])) {
                $return[$key] = (array) $return[$key];
                $return[$key][] = $value;
            } else {
                $return[$key] = $value;
            }
        }
        return $return;
    }
    /**
     * Resolves //, ../ and ./ from a path and returns
     * the result. Eg:
     *
     * /foo/bar/../boo.php    => /foo/boo.php
     * /foo/bar/../../boo.php => /boo.php
     * /foo/bar/.././/boo.php => /foo/boo.php
     *
     * This method can also be called statically.
     *
     * @param  string $url URL path to resolve
     * @return string      The result
     */
    function resolvePath ($path)
    {
        $path = explode('/', str_replace('//', '/', $path));
        for ($i = 0; $i < count($path); $i ++) {
            if ($path[$i] == '.') {
                unset($path[$i]);
                $path = array_values($path);
                $i --;
            } elseif ($path[$i] == '..' and ($i > 1 or ($i == 1 and $path[0] != ''))) {
                unset($path[$i]);
                unset($path[$i - 1]);
                $path = array_values($path);
                $i -= 2;
            } elseif ($path[$i] == '..' and $i == 1 and $path[0] == '') {
                unset($path[$i]);
                $path = array_values($path);
                $i --;
            } else {
                continue;
            }
        }
        return implode('/', $path);
    }
    /**
     * Returns the standard port number for a protocol
     *
     * @param  string  $scheme The protocol to lookup
     * @return integer         Port number or NULL if no scheme matches
     *
     * @author Philippe Jausions <Philippe.Jausions@11abacus.com>
     */
    function getStandardPort ($scheme)
    {
        switch (strtolower($scheme)) {
            case 'http':
                return 80;
            case 'https':
                return 443;
            case 'ftp':
                return 21;
            case 'imap':
                return 143;
            case 'imaps':
                return 993;
            case 'pop3':
                return 110;
            case 'pop3s':
                return 995;
            default:
                return null;
        }
    }
    /**
     * Forces the URL to a particular protocol
     *
     * @param string  $protocol Protocol to force the URL to
     * @param integer $port     Optional port (standard port is used by default)
     */
    function setProtocol ($protocol, $port = null)
    {
        $this->protocol = $protocol;
        $this->port = is_null($port) ? $this->getStandardPort() : $port;
    }
}
/**
 * Class with static helper functions
 *
 */
class SEFTools
{
    /** Retrieves the language ID from the given language name
     *
     * @param	string	Code language name (normally $mosConfig_lang)
     * @return	int 	Database id of this language
     */
    function getLangCode ($langName = null)
    {
        global $database, $mosConfig_lang;
        static $langCodes;
        if (is_null($langCodes))
            $langCodes = array();
        if (is_null($langName))
            $langName = $mosConfig_lang;
        if (isset($langCodes[$langName]))
            return $langCodes[$langName];
        $langCode[$langName] = - 1;
        if ($langName != '') {
            $database->setQuery("SELECT iso FROM #__languages WHERE active=1 and code = '$langName' ORDER BY ordering");
            $langCode[$langName] = $database->loadResult(false);
        }
        return $langCode[$langName];
    }
    /** Retrieves the language name from the given language iso
     *
     * @param	string	Language iso
     * @return	string 	Language name
     */
    function getLangName ($langCode = null)
    {
        global $database, $mosConfig_lang;
        static $langNames;
        if (is_null($langNames))
            $langNames = array();
        if (is_null($langCode))
            return $mosConfig_lang;
        if (isset($langNames[$langCode]))
            return $langNames[$langCode];
        $langNames[$langCode] = - 1;
        $database->setQuery("SELECT code FROM #__languages WHERE active=1 AND iso='$langCode' ORDER BY ordering");
        $langNames[$langCode] = $database->loadResult();
        return $langNames[$langCode];
    }
    /** Retrieves the language ID from the given language iso
     *
     * @param	string	Language iso
     * @return	string 	Language database id
     */
    function getLangId ($langIso = null)
    {
        global $database;
        static $langIds;
        if (is_null($langIds))
            $langIds = array();
        if (is_null($langIso))
            return;
        if (isset($langIds[$langIso]))
            return $langIds[$langIso];
        $langIds[$langIso] = - 1;
        $database->setQuery("SELECT `id` FROM `#__languages` WHERE `active`='1' AND `iso`='$langIso'");
        $langIds[$langIso] = $database->loadResult();
        return $langIds[$langIso];
    }
    /**
     * Return component version number.
     *
     * @return string
     */
    function getSEFVersion ()
    {
        global $mosConfig_absolute_path;
        static $version;
        // If already parse, return it.
        if (! is_null($version))
            return $version;
            // Load class if not loaded yet.
        if (! class_exists('DOMIT_Lite_Document')) {
            require_once ($mosConfig_absolute_path . '/includes/domit/xml_domit_lite_include.php');
        }
        // Load from XML otherwise.
        $sefAdminPath = $GLOBALS['mosConfig_absolute_path'] . '/administrator/components/com_sef/';
        $xmlFile = $sefAdminPath . 'sef.xml';
        // Try to find JoomSEF component (com_smf) version.
        $success = true;
        if (is_readable($xmlFile)) {
            $xmlDoc = new DOMIT_Lite_Document();
            $xmlDoc->resolveErrors(true);
            if ($xmlDoc->loadXML($xmlFile, false, true)) {
                $root = &$xmlDoc->documentElement;
                $element = &$root->getElementsByPath('version', 1);
                $version = $element ? $element->getText() : '';
            }
        }
        return $version;
    }
    /**
     * Returns extension version number from its XML file.
     *
     * @return string
     */
    function getExtVersion ($option)
    {
        global $mosConfig_absolute_path;
        require_once ($mosConfig_absolute_path . '/includes/domit/xml_domit_lite_include.php');
        $extBaseDir = mosPathName(mosPathName($mosConfig_absolute_path) . 'components/com_sef/sef_ext');
        // Load the xml file
        $xmlDoc = new DOMIT_Lite_Document();
        $xmlDoc->resolveErrors(true);
        if (! $xmlDoc->loadXML($extBaseDir . $option . '.xml', false, true)) {
            return null;
        }
        $root = &$xmlDoc->documentElement;
        if ($root->getTagName() != 'mosinstall') {
            return null;
        }
        if ($root->getAttribute('type') != 'sef_ext') {
            return null;
        }
        $element = &$root->getElementsByPath('version', 1);
        return ($element ? $element->getText() : '');
    }
    /**
     * Returns extension name from its XML file.
     *
     * @return string
     */
    function getExtName ($option)
    {
        global $mosConfig_absolute_path;
        require_once ($mosConfig_absolute_path . '/includes/domit/xml_domit_lite_include.php');
        $extBaseDir = mosPathName(mosPathName($mosConfig_absolute_path) . 'components/com_sef/sef_ext');
        // Load the xml file
        $xmlDoc = new DOMIT_Lite_Document();
        $xmlDoc->resolveErrors(true);
        if (! $xmlDoc->loadXML($extBaseDir . $option . '.xml', false, true)) {
            return null;
        }
        $root = &$xmlDoc->documentElement;
        if ($root->getTagName() != 'mosinstall') {
            return null;
        }
        if ($root->getAttribute('type') != 'sef_ext') {
            return null;
        }
        $element = &$root->getElementsByPath('name', 1);
        return ($element ? $element->getText() : '');
    }
    
    /** Returns mosParameters object representing extension's parameters
     *
     * @param	string          Extension name
     * @return	mosParameters   Extension's parameters
     */
    function &getExtParams ($option)
    {
        global $database, $mosConfig_absolute_path;
        static $params;
        if (! isset($params))
            $params = array();
        if (! isset($params[$option])) {
            $row = new mosExt($database);
            $row->load($option . '.xml');
            $path = $mosConfig_absolute_path . "/components/com_sef/sef_ext/$option.xml";
            if (! file_exists($path)) {
                $path = '';
            }
            $params[$option] = new mosParameters($row->params, $path, 'sef_ext');
        }
        return $params[$option];
    }
    
    function &getExtDefaultParams($option)
    {
        global $database, $mosConfig_absolute_path;
        
        $row = new mosExt($database);
        $row->load($option . '.xml');
        $path = $mosConfig_absolute_path . '/administrator/components/com_sef/extensions_params.xml';
        
        $params = new mosParameters($row->params, $path, 'sef_ext');
        
        return $params;
    }
    
    /** Returns the array of texts used by the extension for creating URLs
     *  in currently selected language (for JoomFish support)
     *
     * @param	string  Extension name
     * @return	array   Extension's texts
     */
    function getExtTexts ($option, $lang = '')
    {
        global $database, $mosConfig_lang;
        static $extTexts;
        if ($option == '')
            return false;
            // Set the language
        if ($lang == '')
            $lang = $mosConfig_lang;
        if (! isset($extTexts))
            $extTexts = array();
        if (! isset($extTexts[$option]))
            $extTexts[$option] = array();
        if (! isset($extTexts[$option][$lang])) {
            $extTexts[$option][$lang] = array();
            // If lang is different than current language, change it
            if ($lang != $mosConfig_lang) {
                $oldLang = $mosConfig_lang;
                $mosConfig_lang = $lang;
            }
            $query = "SELECT `id`, `name`, `value` FROM `#__sefexttexts` WHERE `extension` = '$option'";
            $database->setQuery($query);
            $texts = $database->loadObjectList();
            if (is_array($texts) && (count($texts) > 0)) {
                foreach (array_keys($texts) as $i) {
                    $name = $texts[$i]->name;
                    $value = $texts[$i]->value;
                    $extTexts[$option][$lang][$name] = $value;
                }
            }
            // Set the language back to previously selected one
            if (isset($oldLang) && ($oldLang != $mosConfig_lang)) {
                $mosConfig_lang = $oldLang;
            }
        }
        return $extTexts[$option][$lang];
    }
    function sortURLvars ($string)
    {
        $pos = strpos($string, '?');
        if ($pos === false)
            return $string;
        $firstPart = substr($string, 0, $pos + 1);
        $secondPart = substr($string, $pos + 1);
        $urlVars = explode('&', $secondPart);
        // make params unique and order them
        $urlVars = array_unique($urlVars);
        sort($urlVars);
        // search for option param and remove it from array
        for ($i = 0; $i < count($urlVars); $i ++) {
            if (strpos($urlVars[$i], 'option=') === 0) {
                $opt = $urlVars[$i];
                unset($urlVars[$i]);
            }
        }
        // readd option to the beginning of the
        if (isset($opt))
            array_unshift($urlVars, $opt);
        $secondPart = implode('&', $urlVars);
        $string = $firstPart . $secondPart;
        return $string;
    }
    function RemoveVariable ($url, $var, $value = '')
    {
        if ($value == '') {
            $newurl = eregi_replace("(&|\?)$var=[^&]*", '\\1', $url);
        } else {
            $trans = array('?' => '\\?', '.' => '\\.', '+' => '\\+', '*' => '\\*', '^' => '\\^', '$' => '\\$');
            $value = strtr($value, $trans);
            $newurl = eregi_replace("(&|\?)$var=$value(&|\$)", '\\1\\2', $url);
        }
        $newurl = trim($newurl, '&?');
        $trans = array('&&' => '&' , '?&' => '?');
        $newurl = strtr($newurl, $trans);
        return $newurl;
    }

    function getMetaBot()
    {
        static $metabot;

        // load params from DB
        if (is_null($metabot)) {
            global $database;

            // Get the mambot's parameters
            $query = "SELECT id FROM #__mambots WHERE element = 'joomsef_metabot' AND folder = 'system'";
            $database->setQuery($query);
            $id = $database->loadResult();
            $metabot = new mosMambot($database);
            $metabot->load($id);
        }

        return $metabot;
    }

    function getMetaBotParams()
    {
        $metabot = SEFTools::getMetaBot();
        return new mosParameters($metabot->params);
    }

    function redefineMainframe() {
        global $mainframe;

        if( !empty($mainframe) && strtolower(get_class($mainframe)) == 'mosmainframe' ) {
            // now redefine mainframe
            global $option;
            $myframe = new metaMainFrame($mainframe->_db, $option, '.');

            foreach (get_object_vars($mainframe) as $key => $value) {
                $myframe->$key = $mainframe->$key;
            }

            //unset($mainframe);
            $mainframe = null;
            $mainframe = $myframe;
        }
    }
}

class SEFUpgradeFile
{
    var $operation = '';
    var $packagePath = '';
    function SEFUpgradeFile ($op, $path)
    {
        $this->operation = $op;
        $this->packagePath = $path;
    }
}

class metaMainFrame extends mosMainFrame
{
    function getHead()
    {
        global $_MAMBOTS, $mainframe;

        // if beforeHead trigger is enabled for MetaBot
        if( SEFConfig::triggerEnabled() ) {
            $_MAMBOTS->trigger('beforeHead');
        }

        // PHP4 hack
        // (in PHP4 when Joomla's cache calls call_user_func_array,
        //  $this is not equal to global $mainframe, so we need to
        //  make sure the function is called on the right instance)
        return $mainframe->getParentHead();
    }

    // PHP4 hack
    function getParentHead() {
        return parent::getHead();
    }

    function appendMetaTag($name, $content, $rewrite = null)
    {
        $metabot = SEFTools::getMetaBot();
        
        if( $metabot->published ) {
            if (!$content) return;
            global $mosConfig_MetaKeys, $mosConfig_MetaDesc;

            $params = SEFTools::getMetaBotParams();
            if (is_null($rewrite)) {
                $rewrite = !((bool) $params->get('rewrite_joomla'));
            }

            $found = false;
            if ($rewrite) {
                $n = count($this->_head['meta']);
                for ($i = 0; $i < $n; $i++) {
                    if ($this->_head['meta'][$i][0] == $name) {
                        $oldContent = $this->_head['meta'][$i][1];
                        // do not allow defaults to be added
                        if ($oldContent) {
                            if ($name == 'description' && $content == $mosConfig_MetaDesc) return;
                            elseif ($name == 'keywords' && $content == $mosConfig_MetaKeys) return;
                        }

                        $this->_head['meta'][$i][1] = htmlspecialchars($content);
                        return;
                    }
                }
            }
        }

        return parent::appendMetaTag($name, $content);
    }
}
?>
