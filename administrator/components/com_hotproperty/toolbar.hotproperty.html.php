<?php
// $Id: toolbar.hotproperty.html.php
/**
* Hot Property toolbar HTML
*
* @package Hot Property 0.9
* @copyright (C) 2004 Lee Cher Yeong
* @url http://www.Mosets.com/
* @author Lee Cher Yeong <cy@mosets.com>
**/
// ensure this file is being included by a parent file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

class menuhotproperty {

	# Company menus
	
	function COMPANY_NEW() {
		mosMenuBar::startTable();
		mosMenuBar::save('savecompany');
		mosMenuBar::cancel('cancelcompany');
		mosMenuBar::spacer();
		mosMenuBar::endTable();
	}
	
	function COMPANY_EDIT() {
		mosMenuBar::startTable();
		mosMenuBar::save('savecompany');
		mosMenuBar::cancel('cancelcompany');
		mosMenuBar::spacer();
		mosMenuBar::endTable();
	}
	
	function COMPANY_LIST() {
		mosMenuBar::startTable();
		mosMenuBar::addNew( 'newcompany' );
		mosMenuBar::editList( 'editcompany' );
		mosMenuBar::deleteList( '', 'removecompany' );
		mosMenuBar::spacer();
		mosMenuBar::endTable();
	}

	# Agent menus
	
	function AGENT_NEW() {
		mosMenuBar::startTable();
		mosMenuBar::save('saveagent');
		mosMenuBar::cancel('cancelagent');
		mosMenuBar::spacer();
		mosMenuBar::endTable();
	}
	
	function AGENT_EDIT() {
		mosMenuBar::startTable();
		mosMenuBar::save('saveagent');
		mosMenuBar::cancel('cancelagent');
		mosMenuBar::spacer();
		mosMenuBar::endTable();
	}
	
	function AGENT_LIST() {
		mosMenuBar::startTable();
		mosMenuBar::addNew( 'newagent' );
		mosMenuBar::editList( 'editagent' );
		mosMenuBar::deleteList( '', 'removeagent' );
		mosMenuBar::spacer();
		mosMenuBar::endTable();
	}

	# Property menus
	
	function PROPERTY_NEW() {
		mosMenuBar::startTable();
		mosMenuBar::save('saveproperty');
		mosMenuBar::cancel('cancelproperty');
		mosMenuBar::spacer();
		mosMenuBar::endTable();
	}
	
	function PROPERTY_EDIT() {
		mosMenuBar::startTable();
		mosMenuBar::save('saveproperty');
		mosMenuBar::cancel('cancelproperty');
		mosMenuBar::spacer();
		mosMenuBar::endTable();
	}
	
	function PROPERTY_LIST() {
		mosMenuBar::startTable();
		mosMenuBar::publishList('publish_property');
		mosMenuBar::unpublishList('unpublish_property');
		mosMenuBar::archiveList( 'archive_property', 'Archive' );
		mosMenuBar::divider();
		mosMenuBar::addNew( 'newproperty' );
		mosMenuBar::editList( 'editproperty' );
		mosMenuBar::deleteList( '', 'removeproperty' );
		mosMenuBar::spacer();
		mosMenuBar::endTable();
	}

	function PROPERTY_ARCHIVED_LIST() {
		mosMenuBar::startTable();
		mosMenuBar::unarchiveList( 'unarchive_property', 'Unarchive' );
		mosMenuBar::divider();
		mosMenuBar::deleteList( '', 'removeproperty' );
		mosMenuBar::endTable();
	}
	# Property Type menus
	
	function PROP_TYPE_NEW() {
		mosMenuBar::startTable();
		mosMenuBar::save('saveprop_type');
		mosMenuBar::cancel('cancelprop_type');
		mosMenuBar::spacer();
		mosMenuBar::endTable();
	}
	
	function PROP_TYPE_EDIT() {
		mosMenuBar::startTable();
		mosMenuBar::save('saveprop_type');
		mosMenuBar::cancel('cancelprop_type');
		mosMenuBar::spacer();
		mosMenuBar::endTable();
	}
	
	function PROP_TYPE_LIST() {
		mosMenuBar::startTable();
		mosMenuBar::publishList('publish_prop_type');
		mosMenuBar::unpublishList('unpublish_prop_type');
		mosMenuBar::divider();
		mosMenuBar::addNew( 'newprop_type' );
		mosMenuBar::editList( 'editprop_type' );
		mosMenuBar::deleteList( '', 'removeprop_type' );
		mosMenuBar::spacer();
		mosMenuBar::endTable();
	}

	# Extra Fields menus
	
	function PROP_EF_NEW() {
		mosMenuBar::startTable();
		mosMenuBar::save('saveprop_ef');
		mosMenuBar::cancel('cancelprop_ef');
		mosMenuBar::spacer();
		mosMenuBar::endTable();
	}
	
	function PROP_EF_EDIT() {
		mosMenuBar::startTable();
		mosMenuBar::save('saveprop_ef');
		mosMenuBar::cancel('cancelprop_ef');
		mosMenuBar::spacer();
		mosMenuBar::endTable();
	}
	
	function PROP_EF_LIST() {
		mosMenuBar::startTable();
		mosMenuBar::publishList('publish_prop_ef');
		mosMenuBar::unpublishList('unpublish_prop_ef');
		mosMenuBar::divider();
		mosMenuBar::addNew( 'newprop_ef' );
		mosMenuBar::editList( 'editprop_ef' );
		mosMenuBar::deleteList( '', 'removeprop_ef' );
		mosMenuBar::spacer();
		mosMenuBar::endTable();
	}

	# Featured Menu

	function PROP_FEATURED_LIST() {
		mosMenuBar::startTable();
		mosMenuBar::publishList('publish_featured');
		mosMenuBar::unpublishList('unpublish_featured');
		mosMenuBar::divider();
		mosMenuBar::custom('remove_featured','delete.png','delete_f2.png','Remove', true);
		mosMenuBar::spacer();
		mosMenuBar::endTable();
	}

	# Report Menu
	function REPORT_MENU() {
		mosMenuBar::startTable();
		mosMenuBar::custom('doreport','edit.png','edit_f2.png','Generate Report', false);
		mosMenuBar::spacer();
		mosMenuBar::endTable();
	}

	function DOREPORT_MENU() {
		mosMenuBar::startTable();
		mosMenuBar::custom('report','new.png','new_f2.png','New Report', false);
		mosMenuBar::spacer();
		mosMenuBar::endTable();
	}

	function DOREPORT_EXCEL_MENU() {
		mosMenuBar::startTable();
		mosMenuBar::custom('report','new.png','new_f2.png','New Report', false);
		mosMenuBar::spacer();
		mosMenuBar::endTable();
	}

	# License Menu
	function LICENSE_MENU() {
		mosMenuBar::startTable();
		mosMenuBar::back();
		mosMenuBar::spacer();
		mosMenuBar::endTable();
	}

	# Config Menu
	function CONFIG_MENU() {
		mosMenuBar::startTable();
		mosMenuBar::save('saveConfig');
		mosMenuBar::back();
		mosMenuBar::spacer();
		mosMenuBar::endTable();
	}

}


?>