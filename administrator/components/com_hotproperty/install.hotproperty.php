<?php

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

require_once( 'components/com_hotproperty/admin.hotproperty.class.php' );

function com_install() {

	# Perform fresh install
	return new_install();

}

function new_install() {
	global $database;

	$msg = '<table width="100%" border="0" cellpadding="8" cellspacing="0"><tr width="160"><td align="center" valign="top"><img width="153" height="103" src="../components/com_hotproperty/img/hp_logo.png" alt="Hot Property" /></td>';
	$msg .= '<td width="100%" align="left" valign="top"><center><h3>Hot Property v'.hp_version.'</h3><h4>A Joomla! Listing Solutions (Real Estates, Auto, Books, Cds etc.)</h4><font class="small">&copy; Copyright 2004-2006 by Mosets Consulting. <a href="http://www.mosets.com/">http://www.mosets.com/</a><br/></font></center><br />';
	$msg .= "<fieldset style=\"border: 1px dashed #C0C0C0;\"><legend>Details</legend>";

	# Change Admin Icon to Mosets icon
	$database->setQuery("UPDATE #__components SET admin_menu_img='../components/com_hotproperty/img/favicon.png' WHERE admin_menu_link='option=com_hotproperty'");
	$database->query();

	# Add new ACL
	$hpacl = new gacl_api;
	
	if ($groupid = $hpacl->add_group("Agent",$hpacl->get_group_id("Registered"))) {
		$msg .= "<font color=#339900>OK</font> &nbsp;  Agent ACL created<br />";
	}

	# Assigning default config
	$row = new hpConfig();
	$row->imgdir_standard='/components/com_hotproperty/img/std/';
	$row->imgdir_thumb='/components/com_hotproperty/img/thb/';
	$row->imgdir_original='/components/com_hotproperty/img/ori/';
	$row->imgdir_agent='/components/com_hotproperty/img/agent/';
	$row->imgdir_company='/components/com_hotproperty/img/company/';
	$row->img_noimage_thumb='noimage.png';
	$row->img_noimage_standard='noimage.png';
	$row->imgsize_standard='400';
	$row->imgsize_thumb='120';
	$row->imgsize_agent='100';
	$row->imgsize_company='240';
	$row->quality_photo='80';
	$row->quality_agent='80';
	$row->quality_company='80';
	$row->img_connector='_';
	$row->img_method='gd2';
	$row->img_netpbmpath='';
	$row->img_impath='';
	$row->img_saveoriginal='0';
	$row->link_open_newwin='0';
	$row->show_thumb='1';
	$row->show_moreinfo='1';
	$row->show_pdficon='1';
	$row->show_printicon='1';
	$row->show_emailicon='1';
	$row->default_agent='';
	$row->default_company='1';
	$row->use_companyagent='1';
	$row->auto_approve='1';
	$row->css='default';
	$row->currency='�';
	$row->thousand_sep='1';
	$row->thousand_string=' ';
	$row->dec_point='2';
	$row->dec_string=',';
	$row->language='english';
	$row->use_advsearch='1';
	$row->use_diplaynum='1';
	$row->use_sort_name='1';
	$row->use_sort_agent='';
	$row->use_sort_price='1';
	$row->use_sort_suburb='';
	$row->use_sort_state='';
	$row->use_sort_country='';
	$row->use_sort_type='';
	$row->use_sort_modified='';
	$row->use_sort_hits='';
	$row->default_order='name';
	$row->default_order2='asc';
	$row->default_limit='10';
	$row->default_limit_agent='10';
	$row->default_limit_co='10';
	$row->default_limit_search='10';
	$row->default_limit_featured='10';
	$row->fp_featured_count='3';
	$row->fp_show_featured='1';
	$row->fp_show_search='1';
	$row->fp_show_empty='1';
	$row->fp_show_nbr='1';
	$row->fp_depth_count='2';
	$row->fp_depth_calculous='1';
	$row->log_search='1';
	$row->agent_groupid='31';
	$row->agent_usertype='Agent';
	$row->show_agentdetails='1';
	$row->show_enquiryform='1';
	$row->show_guide='1';
	# End of config

	# Write Configuration to file
	$config = "<?php\n";
	$config .= $row->getVarText();
	$config .= "?>";

	if ($fp = fopen("components/com_hotproperty/config.hotproperty.php", "w")) {
		fputs($fp, $config, strlen($config));
		fclose ($fp);
		$msg .= "<font color='green'>OK</font> &nbsp; Updated configuration file<br />";
	} else {
		$msg = "<font color=red>FAILED</font> &nbsp; An Error Has Occurred! Unable to open config file to write!<br />";
	}

	global $mosConfig_absolute_path;
	
	$msg .= '<br />';
	$msg .= (is_writable( $mosConfig_absolute_path.$row->imgdir_standard ) ? '<b><font color="green">Writeable</font></b>' : '<b><font color="red">Unwriteable</font></b>');
	$msg .= ' &nbsp;'.$mosConfig_absolute_path.$row->imgdir_standard . '<br />';

	$msg .= (is_writable( $mosConfig_absolute_path.$row->imgdir_thumb ) ? '<b><font color="green">Writeable</font></b>' : '<b><font color="red">Unwriteable</font></b>');
	$msg .= ' &nbsp;'.$mosConfig_absolute_path.$row->imgdir_thumb . '<br />';

	$msg .= (is_writable( $mosConfig_absolute_path.$row->imgdir_original ) ? '<b><font color="green">Writeable</font></b>' : '<b><font color="red">Unwriteable</font></b>');
	$msg .= ' &nbsp;'.$mosConfig_absolute_path.$row->imgdir_original . '<br />';

	$msg .= (is_writable( $mosConfig_absolute_path.$row->imgdir_agent ) ? '<b><font color="green">Writeable</font></b>' : '<b><font color="red">Unwriteable</font></b>');
	$msg .= ' &nbsp;'.$mosConfig_absolute_path.$row->imgdir_agent . '<br />';

	$msg .= (is_writable( $mosConfig_absolute_path.$row->imgdir_company ) ? '<b><font color="green">Writeable</font></b>' : '<b><font color="red">Unwriteable</font></b>');
	$msg .= ' &nbsp;'.$mosConfig_absolute_path.$row->imgdir_company . '<br />';


	$msg .= "<br /><font color='green'>OK &nbsp; Hot Property Installed Successfully!</font></fieldset>";
	$msg .= "<p /><a href=\"index2.php?option=com_hotproperty\">Run Hot Property now!</a>";
	$msg .='<br /><br /></td></tr></table>';
	return $msg ;
} 
?>
