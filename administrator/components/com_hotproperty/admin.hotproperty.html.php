<?php
// $Id: admin.hotproperty.html.php
/**
* Hot Property admin HTML
*
* @package Hot Property 0.9
* @copyright (C) 2004 Lee Cher Yeong
* @url http://www.Mosets.com/
* @author Lee Cher Yeong <cy@mosets.com>
**/

// ensure this file is being included by a parent file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

require("components/com_hotproperty/classes/oohforms.inc");

class HTML_hotproperty {

	function print_style() {
	?>
	<style type="text/css">
		a.hp_menu {
			font-weight: bold;
			text-decoration: none;
		}
		a.hp_menu:hover {
			font-weight: bold;
			text-decoration: underline;
		}
		a.hp_menu_selected {
			font-weight: bold;
			color: #515151;
			text-decoration: none;
			font-size: 12px;
		}
		a.hp_menu_selected:hover {
			text-decoration: underline;
			font-weight: bold;
			color: #515151;
			font-size: 12px;
		}

		fieldset.report {
			background: inherit;
			border: 1px solid #C0C0C0;
			text-align: left;
			padding: 10px;
			margin-bottom: 10px;
		}

		fieldset.report legend {
			text-align: left;
			font-weight: bold;
		}

	</style>
	<?php
	}

	function print_startmenu( $task ) {
		global $hp_show_guide, $hp_use_companyagent, $my;
		global $mosConfig_caching;

		HTML_hotproperty::print_style();
	?>
	<table cellpadding="3" cellspacing="0" border="0" width="100%">
	<tr>
		<td align="left" valign="top" width="160" height="0">
			<center><img width="153" height="103" src="../components/com_hotproperty/img/hp_logo.png" alt="Hot Property" hspace="0" vspace="5" /></center>

			<table cellpadding="4" cellspacing="0" border="0" width="160" height="100%" align="left" class="adminlist">
				<tr><th>Properties</th></tr>

				<tr><td><b>&middot;</b>&nbsp;<a class="hp_menu<?php echo ($task=="listproperty" || $task=="editproperty" || $task=="") ? "_selected": ""; ?>" href="index2.php?option=com_hotproperty&task=listproperty">View Properties</a></td></tr>
				<tr><td><b>&middot;</b>&nbsp;<a class="hp_menu<?php echo ($task=="listprop_type" || $task=="editprop_type" || $task=="newprop_type") ? "_selected": ""; ?>" href="index2.php?option=com_hotproperty&task=listprop_type">Manage Types</a></tr>
				<tr><td><b>&middot;</b>&nbsp;<a class="hp_menu<?php echo ($task=="featured") ? "_selected": ""; ?>" href="index2.php?option=com_hotproperty&task=featured">Manage Featured</a></td></tr>
				<tr><td><b>&middot;</b>&nbsp;<a class="hp_menu<?php echo ($task=="listprop_ef" || $task=="editprop_ef") ? "_selected": ""; ?>" href="index2.php?option=com_hotproperty&task=listprop_ef">Extra Fields</a></tr>
				<tr><td><b>&middot;</b>&nbsp;<a class="hp_menu<?php echo ($task=="listarchived") ? "_selected": ""; ?>" href="index2.php?option=com_hotproperty&task=listarchived">Archived Properties</a></td></tr>
				<tr><td><b>+</b>&nbsp;<a class="hp_menu<?php echo ($task=="newproperty") ? "_selected": ""; ?>" href="index2.php?option=com_hotproperty&task=newproperty">Add Property</a></td></tr>
				<?php if($my->gid > 22){ ?>
					<?php if ($hp_use_companyagent == '1') { ?>
					<tr><th>Companies</th></tr>
					<tr><td><b>&middot;</b>&nbsp;<a class="hp_menu<?php echo ($task=="listcompany" || $task=="editcompany") ? "_selected": ""; ?>" href="index2.php?option=com_hotproperty&task=listcompany"> View Companies</a></td></tr>
					<tr><td><b>+</b>&nbsp;<a class="hp_menu<?php echo ($task=="newcompany") ? "_selected": ""; ?>" href="index2.php?option=com_hotproperty&task=newcompany">Add Company</a></td></tr>
	
					<tr><th>Agents</th></tr>
					<tr><td><b>&middot;</b>&nbsp;<a class="hp_menu<?php echo ($task=="listagent" || $task=="editagent") ? "_selected": ""; ?>" href="index2.php?option=com_hotproperty&task=listagent"> View Agents</a></td></tr>
					<tr><td><b>+</b>&nbsp;<a class="hp_menu<?php echo ($task=="newagent") ? "_selected": ""; ?>" href="index2.php?option=com_hotproperty&task=newagent">Add Agent</a></td></tr>
					<?php } ?>
                    
                    <tr><th>Season Admin</th></tr>
					<tr><td><b>&middot;</b>&nbsp;<a class="hp_menu" href="index2.php?pshop_mode=admin&page=season.season_list&option=com_virtuemart">Seasons</a></td></tr>
                    <tr><td><b>+</b>&nbsp;<a class="hp_menu" href="index2.php?pshop_mode=admin&page=season.season_form&option=com_virtuemart">Add Seasons</a></td></tr>
                    <tr><td><b>&middot;</b>&nbsp;<a class="hp_menu" href="index2.php?pshop_mode=admin&page=season.season_rate_list&option=com_virtuemart">Season Pricing</a></td></tr>
                    <tr><td><b>+</b>&nbsp;<a class="hp_menu" href="index2.php?pshop_mode=admin&page=season.season_rate_form&option=com_virtuemart">Add Season Pricing</a></td></tr>
                    
					<tr><th>Statistics</th></tr>
					<tr><td><b>&middot;</b>&nbsp;<a class="hp_menu<?php echo ($task=="log_searches") ? "_selected": ""; ?>" href="index2.php?option=com_hotproperty&task=log_searches">Log Searches</a></td></tr>
	
					<tr><th>More...</th></tr>
					<tr><td><b>&middot;</b>&nbsp;<a class="hp_menu<?php echo ($task=="config") ? "_selected": ""; ?>" href="index2.php?option=com_hotproperty&task=config">Configuration</a></td></tr>
					<tr><td><b>&middot;</b>&nbsp;<a class="hp_menu<?php echo ($task=="report") ? "_selected": ""; ?>" href="index2.php?option=com_hotproperty&task=report">Generate Report</a></td></tr>
					<?php if ($mosConfig_caching) { ?>
					<tr><td><b>&middot;</b>&nbsp;<a class="hp_menu<?php echo ($task=="clean_cache") ? "_selected": ""; ?>" href="index2.php?option=com_hotproperty&task=clean_cache">Clean Cache</a></td></tr>
					<?php } ?>
					<?php /* <tr><td><b>�</b>&nbsp;<a class="hp_menu<?php echo ($task=="about") ? "_selected": ""; ?>" href="index2.php?option=com_hotproperty&task=about">About Hot Property</a></td></tr> */ ?>
				<?php } ?>
			</table>
		</td>
		<td valign="top">
		<?php
		if ($hp_show_guide == 1) {
			include("components/com_hotproperty/guide.hotproperty.php");
		}
	}

	function print_endmenu() {	?>
		</td>
		</tr>
	</table>
	<?php
	}

 /***********
 * Company  *
 ***********/

	function listcompany( &$rows, &$pageNav, $option ) {
?>
	<form action="index2.php" method="POST" name="adminForm">
	<table cellpadding="4" cellspacing="0" border="0" width="100%">
		<tr>

      <td width="100%" align="left"><span class="sectionname">Company Manager</span></td>
			<td nowrap>Display #</td>
			<td> <?php echo $pageNav->writeLimitBox(); ?> </td>
		</tr>
	</table>
	<table cellpadding="4" cellspacing="0" border="0" width="100%" align="right" class="adminlist">
		<tr>
			<th width="20">#</th>
			<th width="20"><input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $rows ); ?>);" /></th>
			<th align="left" nowrap>Company Name</th>
			<th nowrap>Num. of Agents</th>
			<th nowrap>Num. of Properties</th>
			<th nowrap>Total hits</th>
		</tr>
<?php
		$k = 0;
		for ($i=0, $n=count( $rows ); $i < $n; $i++) {
			$row = &$rows[$i];
?>
		<tr class="<?php echo "row$k"; ?>">
			<td width="20" align="center"><?php echo $i+$pageNav->limitstart+1;?></td>
			<td width="20">
				<input type="checkbox" id="cb<?php echo $i;?>" name="id[]" value="<?php echo $row->id; ?>" onClick="isChecked(this.checked);" />
			</td>
			<td width="78%" align="left"><a href="#edit" onclick="return listItemTask('cb<?php echo $i; ?>','editcompany')"><?php echo $row->name; ?></a></td>
			<td width="11%" align="center"><?php echo $row->agents;?></td>
			<td width="11%" align="center"><?php echo $row->properties;?></td>
			<td width="11%" align="center"><?php echo ($row->hits) ? $row->hits : "0";?></td>
<?php		$k = 1 - $k; ?>
		</tr>
<?php	}
?>
		<tr>
			<th align="center" colspan="6"> <?php echo $pageNav->writePagesLinks(); ?></th>
		</tr>
		<tr>
			<td align="center" colspan="6"> <?php echo $pageNav->writePagesCounter(); ?></td>
		</tr>
	</table>

	<input type="hidden" name="option" value="<?php echo $option; ?>">
	<input type="hidden" name="task" value="listcompany">
	<input type="hidden" name="boxchecked" value="0">
	</form>
<?php
	}

	function editcompany( &$row, $option ) {
		global $mosConfig_live_site, $mosConfig_absolute_path, $hp_imgdir_company;
		global $mosConfig_editor;

		//mosMakeHtmlSafe( $row, ENT_QUOTES, 'desc' );
		mosMakeHtmlSafe( $row );

		//include_once( $mosConfig_absolute_path . "/editor/editor.php" );
		//initEditor();
?>
	<script language="javascript">
	<!--
		function submitbutton(pressbutton) {
			var form = document.adminForm;
			if (pressbutton == 'cancelcompany') {
				submitform( pressbutton );
				return;
			}
			// do field validation
			if (form.name.value == "") {
				alert( "Please fill in the Company Name." );
			} else {
				<?php getEditorContents( 'editor1', 'desc' ) ; ?>
				submitform( pressbutton );
			}
		}
	//-->
	</script>
	<table cellpadding="4" cellspacing="0" border="0" width="100%">
		<tr>
			<td width="100%"><span class="sectionname"><?php echo $row->id ? 'Edit' : 'Add';?> Company</span></td>
		</tr>
	</table>
	<form action="index2.php" method="POST" name="adminForm" enctype="multipart/form-data">
	<table cellpadding="4" cellspacing="1" border="0" width="100%" class="adminform">
		<tr>
			<td width="10%" align="left">Company Name:</td>
			<td align="left"><input class="text_area" type="text" name="name" size="30" valign="top" value="<?php echo $row->name; ?>"></td>
			<td width="200" rowspan="10">
			<?php
				if ($row->photo != "") {
				?>
					<img style="border: 5px solid #c0c0c0;" alt="<?php echo $row->name; ?>" src="<?php echo $mosConfig_live_site.$hp_imgdir_company.$row->photo; ?>">
					<br />
					<input type="checkbox" name="remove_photo" value="1"> Remove this photo
				<?php
				}
				else {
					echo "No Photo";
				}
			?>
			</td>
		</tr>
		<tr>
			<td align="left">Address:</td>
			<td align="left"><input class="text_area" type="text" name="address" size="30" maxlength="100" value="<?php echo $row->address; ?>"></td>
		</tr>
		<tr>
			<td align="left">Suburb:</td>
			<td align="left"><input class="text_area" type="text" name="suburb" size="15" maxlength="50" value="<?php echo $row->suburb; ?>"></td>
		</tr>
		<tr>
			<td align="left">State:</td>
			<td align="left"><input class="text_area" type="text" name="state" size="15" maxlength="50" value="<?php echo $row->state; ?>"></td>
		</tr>
		<tr>
			<td align="left">Country:</td>
			<td align="left"><input class="text_area" type="text" name="country" size="15" maxlength="50" value="<?php echo $row->country; ?>"></td>
		</tr>
		<tr>
			<td align="left">Postcode:</td>
			<td align="left"><input class="text_area" type="text" name="postcode" size="6" maxlength="10" value="<?php echo $row->postcode; ?>"></td>
		</tr>
		<tr>
			<td align="left">Telephone:</td>
			<td align="left"><input class="text_area" type="text" name="telephone" size="10" maxlength="20" value="<?php echo $row->telephone; ?>"></td>
		</tr>
		<tr>
			<td align="left">Fax:</td>
			<td align="left"><input class="text_area" type="text" name="fax" size="10" maxlength="20" value="<?php echo $row->fax; ?>"></td>
		</tr>
		<tr>
			<td align="left">E-mail:</td>
			<td align="left"><input class="text_area" type="text" name="email" size="20" value="<?php echo $row->email; ?>"></td>
		</tr>
		<tr>
			<td align="left">Website:</td>
			<td align="left"><input class="text_area" type="text" name="website" size="30" value="<?php echo (!empty($row->website)) ? $row->website : "http://"; ?>"></td>
		</tr>
		<tr>
			<td align="left"><?php echo ($row->photo == "") ? 'Add' : 'Change';?> Photo:</td>
			<td align="left"><input class="text_area" type="file" name="photo" size="30"></td>
		</tr>
		<tr>
			<td align="left" valign="top">Description:</td>
			<td align="left">
				<?php
				// parameters : areaname, content, hidden field, width, height, rows, cols
				editorArea( 'editor1',  $row->desc, 'desc', '500', '200', '50', '5' ) ; ?>
			</td>
		</tr>
	</table>
	<input type="hidden" name="option" value="<?php echo $option; ?>">
	<input type="hidden" name="id" value="<?php echo $row->id; ?>">
	<input type="hidden" name="task" value="">
	</form>
<?php }

 /************
 *   Agent   *
 ************/

	function listagent( &$rows, &$pageNav, $option ) {
?>
	<form action="index2.php" method="POST" name="adminForm">
	<table cellpadding="4" cellspacing="0" border="0" width="100%">
		<tr>

      <td width="100%" align="left"><span class="sectionname">Agent Manager</span></td>
			<td nowrap>Display #</td>
			<td> <?php echo $pageNav->writeLimitBox(); ?> </td>
		</tr>
	</table>
	<table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
		<tr>
			<th width="20">#</th>
			<th width="20"><input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $rows ); ?>);" /></th>
			<th align="left" nowrap>Agent Name</th>
			<th align="left" nowrap>Company</th>
			<th align="left" nowrap>Login User</th>
			<th nowrap>Num. of Properties</th>
			<th nowrap>Total hits</th>
		</tr>
<?php
		$k = 0;
		for ($i=0, $n=count( $rows ); $i < $n; $i++) {
			$row = &$rows[$i];
?>
		<tr class="<?php echo "row$k"; ?>">
			<td width="20" align="center"><?php echo $i+$pageNav->limitstart+1;?></td>
			<td width="20">
				<input type="checkbox" id="cb<?php echo $i;?>" name="id[]" value="<?php echo $row->id; ?>" onClick="isChecked(this.checked);" />
			</td>
			<td width="39%" align="left"><a href="#edit" onclick="return listItemTask('cb<?php echo $i; ?>','editagent')"><?php echo $row->name; ?></a></td>
			<td width="25%" align="left"><?php echo $row->company;?></td>
			<td width="25%" align="left"><?php
					if (!empty($row->mosUsername)) echo $row->mosName." (".$row->mosUsername.")";
					else echo "No Login";
			?></td>
			<td width="11%" align="center"><?php echo $row->properties;?></td>
			<td width="11%" align="center"><?php echo ($row->hits) ? $row->hits : "0";?></td>
<?php		$k = 1 - $k; ?>
		</tr>
<?php	}
?>
		<tr>
			<th align="center" colspan="7"> <?php echo $pageNav->writePagesLinks(); ?></th>
		</tr>
		<tr>
			<td align="center" colspan="7"> <?php echo $pageNav->writePagesCounter(); ?></td>
		</tr>
	</table>

	<input type="hidden" name="option" value="<?php echo $option; ?>">
	<input type="hidden" name="task" value="listagent">
	<input type="hidden" name="boxchecked" value="0">
	</form>
<?php
	}

	function editagent( &$row, &$lists, $option ) {
		global $mosConfig_live_site, $mosConfig_absolute_path, $hp_imgdir_agent;
		global $mosConfig_editor;

		//mosMakeHtmlSafe( $row, ENT_QUOTES, 'desc' );
		mosMakeHtmlSafe( $row );

		//include_once( $mosConfig_absolute_path . "/editor/editor.php" );
		//initEditor();
?>
	<script language="javascript">
	<!--
		function submitbutton(pressbutton) {
			var form = document.adminForm;
			if (pressbutton == 'cancelagent') {
				submitform( pressbutton );
				return;
			}
			// do field validation
			if (form.name.value == "") {
				alert( "Please fill in the Agent Name." );
			} else if (form.email.value == "") {
				alert( "Please fill in the E-mail." );
			} else if (form.company.value == "0") {
				alert( "Please assign a company." );
			} else {
				<?php getEditorContents( 'editor1', 'desc' ) ; ?>
				submitform( pressbutton );
			}
		}
	//-->
	</script>
	<table cellpadding="4" cellspacing="0" border="0" width="100%">
		<tr>
			<td width="100%"><span class="sectionname"><?php echo $row->id ? 'Edit' : 'Add';?> Agent</span></td>
		</tr>
	</table>
	<form action="index2.php" method="POST" name="adminForm" enctype="multipart/form-data">
	<table cellpadding="4" cellspacing="1" border="0" width="100%" class="adminform">
		<tr>
			<td align="left" width="12%">Agent Name:</td>
			<td align="left"><input class="text_area" type="text" name="name" size="30" valign="top" value="<?php echo $row->name; ?>"></td>
			<td width="200" rowspan="6">
			<?php
				if ($row->photo != "") {
				?>
					<img style="border: 5px solid #c0c0c0;" alt="<?php echo $row->name; ?>" src="<?php echo $mosConfig_live_site.$hp_imgdir_agent.$row->photo; ?>">
					<br />
					<input type="checkbox" name="remove_photo" value="1"> Remove this photo
				<?php
				}
				else {
					echo "No Photo";
				}
			?>
			</td>
		</tr>
		<tr>
			<td align="left">Company:</td>
			<td align="left"><?php echo $lists["company"]; ?></td>
		</tr>
		<tr>
			<td align="left">Login User:</td>
			<td align="left"><?php echo $lists["logins"]; ?></td>
		</tr>
		<tr>
			<td align="left">E-Mail:</td>
			<td align="left"><input class="text_area" type="text" name="email" size="30" maxlength="255" value="<?php echo $row->email; ?>"></td>
		</tr>
		<tr>
			<td align="left">Mobile Number:</td>
			<td align="left"><input class="text_area" type="text" name="mobile" size="12" maxlength="20" value="<?php echo $row->mobile; ?>"></td>
		</tr>
		<tr>
			<td align="left"><?php echo ($row->photo == "") ? 'Add' : 'Change';?> Photo:</td>
			<td align="left"><input class="text_area" type="file" name="photo" size="30"></td>
		</tr>
		<tr>
			<td align="left" valign="top">Description:</td>
			<td align="left"><?php editorArea( 'editor1',  $row->desc, 'desc', '500', '200', '50', '5' ) ; ?></td>
		</tr>
		<tr>
			<td align="left">Need Approval?</td>
			<td align="left"><?php echo $lists["approval"]; ?></td>
		</tr>

	</table>
	<input type="hidden" name="option" value="<?php echo $option; ?>">
	<input type="hidden" name="id" value="<?php echo $row->id; ?>">
	<input type="hidden" name="task" value="">
	</form>
<?php }

 /************
 * Property  *
 ************/

	function listproperty( &$rows, &$pageNav, $option, $search, $list ) {
		global $hp_use_companyagent;
?>
	<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
	<script language="Javascript" src="../includes/js/overlib_mini.js"></script>
	<form action="index2.php" method="POST" name="adminForm">
	<table cellpadding="4" cellspacing="0" border="0" width="100%">
		<tr>

      <td width="100%" align="left"><span class="sectionname">Property Manager</span></td>
			<td nowrap>Display #</td>
			<td> <?php echo $pageNav->writeLimitBox(); ?> </td>
      <td>Search:</td>
      <td>
        <input type="text" name="search" value="<?php echo $search;?>" class="text_area" onChange="document.adminForm.submit();" />
      </td>
      <td align="left">
        <?php echo $list["types"];?> <?php echo ($hp_use_companyagent == '1') ? '&nbsp; '.$list["agents"]: '';?>
      </td>
      <td>Order:</td>
      <td align="left"><?php echo $list["order"];?></td>
		</tr>
	</table>
	<table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
		<tr>
			<th width="20">#</th>
			<th width="20"><input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $rows ); ?>);" /></th>
			<th align="left" nowrap>Property</th>
			<?php if ($hp_use_companyagent == '1') { ?><th nowrap align="left">Agent</th><?php } ?>
			<th nowrap align="left">Type</th>
			<th nowrap align="left">Location</th>
			<th nowrap>Featured</th>
			<th nowrap>Published</th>
			<th nowrap>Hits</th>
		</tr>
<?php
		$k = 0;
		for ($i=0, $n=count( $rows ); $i < $n; $i++) {
			$row = &$rows[$i];
?>
		<tr class="<?php echo "row$k"; ?>">
			<td width="20" align="center"><?php echo $i+$pageNav->limitstart+1;?></td>
			<td width="20">
				<input type="checkbox" id="cb<?php echo $i;?>" name="id[]" value="<?php echo $row->id; ?>" onClick="isChecked(this.checked);" />
			</td>
			<td width="38%" align="left"><?php echo ($row->photos) ? "<img src=\"../components/com_hotproperty/img/photo.png\" width=\"14\" height=\"13\">": "<img src=\"images/blank.png\" width=\"14\" height=\"13\">"; ?> <a href="#edit" onclick="return listItemTask('cb<?php echo $i; ?>','editproperty')"><?php echo $row->name; ?></a></td>
			<?php if ($hp_use_companyagent == '1') { ?><td width="20%" align="left"><?php echo $row->agent;?></td><?php } ?>
			<td width="12%" align="left"><?php echo $row->type;?></td>
			<td width="20%" align="left"><?php echo ($row->suburb <> "" && $row->state <> "") ? $row->suburb.", ".$row->state : $row->suburb.$row->state;?></td>
			<?php if ($row->approved)  { ?>
			<td>
			<?php
				$task = $row->featured ? 'unfeature_property' : 'feature_property';
				$img = $row->featured ? 'tick.png' : 'publish_x.png';
			?>
			<a href="#ToggleFeatured" onClick="return listItemTask('cb<?php echo $i;?>','<?php echo $task; ?>')"><img border="0" src="images/<?php echo $img; ?>"></a>


</td>

     <?php
			$now = date( "Y-m-d h:i:s" );
			if ($now <= $row->publish_up && $row->published == "1") {
				$img = 'publish_y.png';
			} else if (($now <= $row->publish_down || $row->publish_down == "0000-00-00 00:00:00") && $row->published == "1") {
				$img = 'publish_g.png';
			} else if ($now > $row->publish_down && $row->published == "1") {
				$img = 'publish_r.png';
			} elseif ($row->published == "0") {
				$img = "publish_x.png";
			}
			$times = '';
			if (isset($row->publish_up)) {
				if ($row->publish_up == '0000-00-00 00:00:00') {
					$times .= "<tr><td>Start: Always</td></tr>";
				} else {
					$times .= "<tr><td>Start: $row->publish_up</td></tr>";
				}
			}
			if (isset($row->publish_down)) {
				if ($row->publish_down == '0000-00-00 00:00:00') {
					$times .= "<tr><td>Finish: No Expiry</td></tr>";
				} else {
					$times .= "<tr><td>Finish: $row->publish_down</td></tr>";
				}
			}
			if ($times) {
?>
        <td align="center"><a href="javascript: void(0);" onMouseOver="return overlib('<table border=0 width=100% height=100%><?php echo $times; ?></table>', CAPTION, 'Publish Information', BELOW, RIGHT);" onMouseOut="return nd();" onClick="return listItemTask('cb<?php echo $i;?>','<?php echo $row->published ? "unpublish_property" : "publish_property";?>')"><img src="images/<?php echo $img;?>" border="0" alt="" /></a>
        <?php
			}
	  ?>
      </td>
			<?php } else { ?>
				<td colspan="2" align="center"><a href="#Approve" onClick="return listItemTask('cb<?php echo $i;?>','approve_property')">[Approve]</a></td>
			<?php } ?>
			<td align="center"><?php echo ($row->hits) ? $row->hits : "0";?></td>
<?php		$k = 1 - $k; ?>
		</tr>
<?php	}
?>
		<tr>
			<th align="center" colspan="9"> <?php echo $pageNav->writePagesLinks(); ?></th>
		</tr>
		<tr>
			<td align="center" colspan="9"> <?php echo $pageNav->writePagesCounter(); ?></td>
		</tr>
	</table>

	<input type="hidden" name="option" value="<?php echo $option; ?>">
	<input type="hidden" name="task" value="listproperty">
	<input type="hidden" name="boxchecked" value="0">
	</form>
	<br />
	<table cellspacing="0" cellpadding="4" border="0" align="center">
		<tr align="center">
			<td> <img src="../components/com_hotproperty/img/photo.png" width="14" height="13" border="0" alt="Photo">
			</td>
			<td> Contain Photo(s) |</td>
			<td> <img src="images/publish_y.png" border=0 alt="Pending" />
			</td>
			<td> Published, but is <u>Pending</u> |</td>
			<td> <img src="images/publish_g.png" border=0 alt="Visible" />
			</td>
			<td> Published and is <u>Current</u> |</td>
			<td> <img src="images/publish_r.png" border=0 alt="Finished" />
			</td>
			<td> Published, but has <u>Expired</u> |</td>
			<td> <img src="images/publish_x.png" border=0 alt="Finished" />
			</td>
			<td> Not Published </td>
		</tr>
		<tr>
		<td colspan="8" align="center">Click on icon to toggle state.</td>
		</tr>
	</table>
<?php
	}

	function editproperty( &$row, &$lists, &$core_fields, &$extra_fields, &$extra_fields_data, $num_of_images, &$extensions, $option, $activetab ) {
		global $hp_use_companyagent, $hp_default_agent, $hp_auto_approve;
		global $mosConfig_editor, $mosConfig_absolute_path, $database;

		mosMakeHtmlSafe( $row );
		$tabs = new mosTabs(0);
?>
	<link rel="stylesheet" type="text/css" media="all" href="../includes/js/calendar/calendar-mos.css" title="green" />
	<script type="text/javascript" src="../includes/js/calendar/calendar.js"></script>
	<script type="text/javascript" src="../includes/js/calendar/lang/calendar-en.js"></script>
	<script language="javascript">
	<!--
		function submitbutton(pressbutton) {
			var form = document.adminForm;
			if (pressbutton == 'cancelproperty') {
				submitform( pressbutton );
				return;
			}
			// do field validation
			if (form.name.value == "") {
				alert( "Please fill in the Property Name." );
				form.type.value = currentType; //on remet le type � sa valeur d'origine, d�finie tout en bas du formulaire
				form.name.focus(); //et on donne le focus au nom c'est pas con
			} else if(form.type.value == "0") {
				alert( "Please enter the Property Type." );
				form.type.focus();
			} else if(form.agent.value == "0") {
				alert( "Please assign an Agent." );
				form.type.value = currentType;
				form.agent.focus();
			} else if(isNaN(form.price.value)) {
				alert( "Please enter a valid price." );
			} else {
				<?php getEditorContents( 'editor1', 'intro_text' ) ; ?>
				<?php getEditorContents( 'editor2', 'full_text' ) ; ?>
				submitform( pressbutton );
			}
		}

	//-->
	</script>
	<table cellpadding="4" cellspacing="0" border="0" width="100%">
		<tr>
			<td width="100%" align="left"><span class="sectionname"><?php echo $row->id ? 'Edit' : 'Add';?> Property</span></td>
		</tr>
	</table>

	<form action="index2.php" method="POST" name="adminForm">

	<?php if (!$row->id) { ?>
		<div align="right"><a class="toolbar" href="javascript:submitbutton('saveproperty_image_new');"><img src="images/save.png" alt="Save" border="0" align="middle" />&nbsp;Save & Insert Photos >></a></div>
	<?php } ?>

	<?php
	$tabs->startPane("content-pane");
	$tabs->startTab("Properties","properties-page");
	?>
	<table cellpadding="4" cellspacing="1" border="0" width="100%" class="adminform">
		<tr><td align="left" colspan="3" style="border-top: 1px solid #C0C0C0; border-bottom: 1px solid #C0C0C0; background-color: #FFFFFF"><span class="error" style="float:left;">NOTICE: </span><div style="padding-left:46px;">Only <span style="color:green">PUBLISHED</span> and <span style="color:green">deeply RELATED</span> CORE extra-fields to the type of that property, are listed below.<br /> Striked elements are required* but no-related ones so they won't be displayed in front-end.</div></td></tr>
	<?php
		$i = 0;
		
		# New OOH Forms
		$f = new form;
		
		foreach($core_fields AS $core_field) {

			#on charge l'objet $core_field_O:
			$core_field_O=new mosHPPropEF($database);
			$core_field_O->load($core_field->id);

			$rel=$core_field_O->isRelToType($row->type);

			$i++;
			# Show Field if it is published ET s'il est li� � la cat�gorie de la prop
			# 'Type' & Agent field will always appear ET 'name' puisqu'� d�faut de ne pas �tre d�publiable, peut en revanche tr�s bien n'est pas li� => donc on l'ajoute d'office car il est n�cessaire � la validation du formulaire!
			if ($core_field->published == '1' && $rel >0 || $core_field->name == 'name' || $core_field->name == 'type' || $core_field->name == 'agent' ) {
			switch($core_field->name) {
				case 'name':?>
		<tr>
			<td align="left" width="10%"><?php echo $core_field->caption; ?>*:</td>
			<td align="left"><input class="text_area" type="text" name="name" size="30" valign="top" value="<?php echo $row->name; ?>"></td>
			<td align="middle" valign="top" rowspan="13">
		    <table cellpadding="3" cellspacing="0" border="0" width="200" class="adminform">
              <tr>
                <th colspan="2">Property Info</th>
              </tr>
							<tr>
								<td align="left" valign="top"><b>Hits:</b></td>
								<td align="left"><?php echo $row->hits;?></td>
							</tr>
							<tr>
								<td align="left" valign="top"><b>State:</b></td>
								<td align="left"><?php echo ($row->published) ? "Published" : "Unpublished";?></td>
							</tr>
							<tr>
								<td align="left" valign="top"><b>Last Modified:</b></td>
								<td align="left"><?php echo $row->modified;?></td>
							</tr>
							<tr>
								<td align="left" valign="top"><b>Created:</b></td>
								<td align="left"><?php echo $row->created;?></td>
							</tr>
				</table>
			</td>
		</tr><?php
					break;
				case 'agent':
					if ($hp_use_companyagent == '1') {
	?><tr>
			<td align="left"><?php echo $rel ? $core_field->caption : '<strike>'.$core_field->caption.'</strike>' ?>*:</td>
			<td align="left"><?php echo $lists["agent"]; ?></td>
		</tr><?php
					}
					break;
				case 'type':
	?><tr>
			<td align="left"><?php echo $rel ? $core_field->caption : '<strike>'.$core_field->caption.'</strike>' ?>*:</td>
			<td align="left"><?php echo $lists["prop_type"]; ?>
  </td>
		</tr><?php
					break;
				case 'address':
	?><tr>
			<td align="left"><?php echo $core_field->caption; ?>:</td>
			<td align="left"><input class="text_area" type="text" name="address" size="30" maxlength="100" value="<?php echo $row->address; ?>"></td>
		</tr><?php
					break;
				case 'suburb':
	?><tr>
			<td align="left"><?php echo $core_field->caption; ?>:</td>
			<td align="left"><input class="text_area" type="text" name="suburb" size="15" maxlength="50" value="<?php echo $row->suburb; ?>"></td>
		</tr><?php
					break;
				case 'state':
	?><tr>
			<td align="left"><?php echo $core_field->caption; ?>:</td>
			<td align="left">
			<?php
			if ($core_field->field_type == "selectlist" || $core_field->field_type == "selectmultiple") {

					# --- Drop Down Menu & Multiple Select List ----------------------------------
					$values = explode("|",$core_field->field_elements);

					# - Generate arrays of values and labels
					$values_labels = array();

					$values_labels[] = array("label"=>'', "value"=>'');
					foreach($values AS $value) {
						$values_labels[] = array("label"=>trim($value), "value"=>trim($value));
					}
					$f->add_element(array("type"=>($core_field->field_type == "selectlist") ? "select" : "select multiple",
	                             "name"=>$core_field->name,
	                             "options"=>$values_labels,
	                             "size"=>($core_field->field_type == "selectlist") ? 1 : $core_field->size,
	                             "value"=>($core_field->field_type == "selectlist") ? $row->state : $values,
															 "extrahtml"=>"class=\"text_area\""));
					$f->show_element($core_field->name);

				}else{

					# --- Text Box ----------------------------------
					$f->add_element(array("type"=>"text",
																"name"=>$core_field->name,
																"value"=>$row->state,
																"size"=>$core_field->size,
																"extrahtml"=>"class=\"text_area\""));
					$f->show_element($core_field->name);
				}
				
			?></td>
		</tr><?php
					break;
				case 'country':
	?><tr>
			<td align="left"><?php echo $core_field->caption; ?>:</td>
			<td align="left"><input class="text_area" type="text" name="country" size="15" maxlength="50" value="<?php echo $row->country; ?>"></td>
		</tr><?php
					break;
				case 'postcode':
	?><tr>
			<td align="left"><?php echo $core_field->caption; ?>:</td>
			<td align="left"><input class="text_area" type="text" name="postcode" size="6" maxlength="10" value="<?php echo $row->postcode; ?>"></td>
		</tr><?php
					break;
				case 'price':
	?><tr>
			<td align="left"><?php echo $core_field->caption; ?>:</td>
			<td align="left"><input class="text_area" type="text" name="price" size="15" maxlength="20" value="<?php echo $row->price; ?>"></td>
		</tr><?php
					break;
				case 'featured':
	?><tr>
			<td align="left" valign="top"><?php echo $core_field->caption; ?>:</td>
			<td align="left"><input type="checkbox" name="featured" value="1"<?php echo ($row->featured) ? " checked":"";?>></td>
		</tr><?php
					break;
				case 'intro_text':
	?><tr>
			<td align="left" valign="top" colspan="2"><?php echo $core_field->caption; ?>:</td>
		</tr>
		<tr>
			<td align="left" colspan="2">
				<?php
				editorArea( 'editor1',  $row->intro_text, 'intro_text', '500', '200', '50', '5' ) ; ?>
			</td>
		</tr><?php
					break;
				case 'full_text':
	?><tr>
			<td align="left" valign="top" colspan="2"><?php echo $core_field->caption; ?>:</td>
		</tr>
		<tr>
			<td align="left" colspan="2">
				<?php
				editorArea( 'editor2',  $row->full_text, 'full_text', '500', '200', '50', '5' ) ; ?>
			</td>
		</tr><?php
					break;
				case 'notes':
	?><tr>
			<td align="left" valign="top" colspan="2"><?php echo $core_field->caption; ?>:</td>
		</tr>
		<tr>
			<td align="left" colspan="2">
					<textarea name="note" class="text_area" rows="5" cols="50"><?php echo $row->note; ?></textarea>
				<br /><em>* Note are hidden to end-user</em>
			</td>
		</tr><?php
					break;
			} // End Switch
			} // End if published
		} // End of foreach
?></table><?php
		if ($hp_use_companyagent <> '1') {
?><input type="hidden" name="agent" value="<?php echo ($row->agent) ? $row->agent : $hp_default_agent; ?>" /><?php
		}
		$price_ef=new mosHPPropEF($database);
		$price_ef->load($core_fields['price']->id);
		# En plus du cas o� l'ef price n'est pas publi�, on v�rifie s'il n'est pas li�:
		if ($core_fields['price']->published == '0' || !$price_ef->isRelToType($row->type)) {
?><input type="hidden" name="price" value="0" /><?php
		}
		#pour conserver l'attribut featured m�me s'il n'est pas related:
		$featured_ef=new mosHPPropEF($database);
		$featured_ef->load($core_fields['featured']->id);
		if (!$featured_ef->isRelToType($row->type)) {
?><input type="hidden" name="featured" value="<?php echo $row->featured; ?>" /><?php
		}
?>
		<?php	
		$tabs->endTab();

		if ($row->id) { // On cherche � s'assurer que le type a �t� s�lectionner avant d'afficher l'onglet 'extrafields'
		$tabs->startTab("Extra Fields","extrafields-page");
		?>

		<table cellpadding="5" cellspacing="0" border="0" width="100%" class="adminform">
			<tr><td align="left" colspan="3" style="border-top: 1px solid #C0C0C0; border-bottom: 1px solid #C0C0C0; background-color: #FFFFFF"><span class="error">NOTICE: </span>Only <span style="color:green">PUBLISHED</span> and <span style="color:green">deeply RELATED</span> extra-fields to the type of that property, are listed below.</td></tr>
		<?php

		$s=0;
		# Generate all published fields
		for ($i=0, $n=count( $extra_fields ); $i < $n ; $i++) {
			$extra_field  = $extra_fields[$i];

			$extra_field_O=new mosHPPropEF($database);
			$extra_field_O->load($extra_field->id);

			if($extra_field_O->isRelToType($row->type))
			{
				$s++;

				$extra_field->name = "ef_".$extra_field->name;
				echo '<tr><td width="120" align="left" valign="top">'.$extra_field->caption.': </td><td align="left">';

				if ( !array_key_exists(substr($extra_field->name,3),$extra_fields_data) ) {
					$extra_fields_data[substr($extra_field->name,3)]->value = '';
				}

				if ($extra_field->field_type == "text") {

					# --- Text Box ----------------------------------
					$f->add_element(array("type"=>"text",
																"name"=>$extra_field->name,
																"value"=>( ( array_key_exists(substr($extra_field->name,3),$extra_fields_data)) ? $extra_fields_data[substr($extra_field->name,3)]->value : ''),
																"size"=>$extra_field->size,
																"extrahtml"=>"class=\"text_area\""));
					echo $extra_field->prefix_text;
					$f->show_element($extra_field->name);
					echo $extra_field->append_text;

				} elseif ($extra_field->field_type == "link") {

					# --- Web Link ----------------------------------

					$f->add_element(array("type"=>"text",
																"name"=>$extra_field->name,
																"value"=>$extra_fields_data[substr($extra_field->name,3)]->value,
																"size"=>$extra_field->size,
																"extrahtml"=>"class=\"text_area\""));
					echo $extra_field->prefix_text;
					$f->show_element($extra_field->name);
					echo $extra_field->append_text;
					echo "<br /><i>ie: Google | http://www.google.com/</i>";

				} elseif ($extra_field->field_type == "multitext") {

					# --- Text Area ----------------------------------
					$f->add_element(array("type"=>"textarea",
																"name"=>$extra_field->name,
																"value"=>$extra_fields_data[substr($extra_field->name,3)]->value,
		                            "rows"=>$extra_field->size,
			                          "cols"=>60,
																"extrahtml"=>"class=\"text_area\""));
					echo $extra_field->prefix_text;
					$f->show_element($extra_field->name);
					echo $extra_field->append_text;

				} elseif ($extra_field->field_type == "wysiwyg") {

					$value = $extra_fields_data[substr($extra_field->name,3)]->value;
					
					echo $extra_field->prefix_text;		
					editorArea( $extra_field->name,  $value ? $value: '', $extra_field->name, 500, 200, '70', '15' );					
					echo $extra_field->append_text;

				} elseif ($extra_field->field_type == "selectlist" || $extra_field->field_type == "selectmultiple") {

					# --- Drop Down Menu & Multiple Select List ----------------------------------
					$values = explode("|",$extra_field->field_elements);

					if ( empty($extra_fields_data[substr($extra_field->name,3)]->value) ) {
						$extra_fields_data[substr($extra_field->name,3)]->value = '';
					}

					# - Generate arrays of values and labels
					$values_labels = array();

					$values_labels[] = array("label"=>'', "value"=>'');
					foreach($values AS $value) {
						$values_labels[] = array("label"=>trim($value), "value"=>trim($value));
					}
					$f->add_element(array("type"=>($extra_field->field_type == "selectlist") ? "select" : "select multiple",
	                             "name"=>$extra_field->name,
	                             "options"=>$values_labels,
	                             "size"=>($extra_field->field_type == "selectlist") ? 1 : $extra_field->size,
	                             "value"=>($extra_field->field_type == "selectlist") ? $extra_fields_data[substr($extra_field->name,3)]->value : explode("|",$extra_fields_data[substr($extra_field->name,3)]->value),
															 "extrahtml"=>"class=\"text_area\""));
					$f->show_element($extra_field->name);

				} elseif ($extra_field->field_type == "checkbox") {

					# --- Checkboxes ----------------------------------
					$values = explode("|",$extra_field->field_elements);

					# - Generate arrays of values and labels
					$j = 0;
					$f->add_element(array("type"=>"checkbox",
																"name"=>$extra_field->name,
																"multiple"=>1,
																"value"=>explode("|",$extra_fields_data[substr($extra_field->name,3)]->value)));
					foreach($values AS $value) {
						$f->show_element($extra_field->name,trim($value));
						echo "&nbsp;".$value."<br />";
					}
				} elseif ($extra_field->field_type == "radiobutton") {

					# --- Radio Buttons ----------------------------------
					$values = explode("|",$extra_field->field_elements);

					# - Generate arrays of values and labels
					$j = 0;
					$f->add_element(array("type"=>"radio",
															 "name"=>$extra_field->name,
															 "value"=>trim($extra_fields_data[substr($extra_field->name,3)]->value)));
					foreach($values AS $value) {
						$f->show_element($extra_field->name,trim($value));
						echo "&nbsp;".$value."<br />";
					}
				} else {
					echo $extra_field->field_type;
				}
				echo '</td></tr>';
			}
		} // FIN du if(isRelToType)

		if ( count( $extra_fields ) <= 0 || $s==0 ) {
			echo '<tr><td align="center"><br /><a href="index2.php?option=com_hotproperty&task=listprop_ef">' . 'There are no published Extra Fields' . '</a><br /><br /></td></tr>';
		}

		?>
    </table>

	<?php
	$tabs->endTab();
	} // FIN du if($row->id) qui de la m�me mani�re que pour l'onglet 'Photos' s'assure que la propri�t� existe(et donc qu'un type � �t� sp�cifi�') avant de l'afficher

	if ($row->id) {
		$tabs->startTab("Photos","photos-page");
	?>
	<table cellpadding="5" cellspacing="0" border="0" width="100%" class="adminform">
		<tr>
			<td align="center">
			<iframe src="index3.php?option=com_hotproperty&task=listprop_image&property=<?php echo $row->id; ?>" id="imgManager" width="100%" height="<?php echo (($num_of_images * 110)+294); ?>" marginwidth="0" marginheight="0" align="top" scrolling="auto" frameborder="0" hspace="0" vspace="0" background="white"> </iframe>
			</td>
		</tr>
	</table>
	<?php
		$tabs->endTab();
	} ?>

	<?php
	$tabs->startTab("Publishing","publishing-page");
	?>

	<table cellpadding="5" cellspacing="0" border="0" width="100%" class="adminform">
		<tr>
			<td valign="top" align="right">Override Created Date </td>
			<td align="left"><input class="text_area" type="text" name="created" id="created" size="25" maxlength="19" value="<?php echo $row->created; ?>" />
				<input name="reset" type="reset" class="button" onClick="return showCalendar('created', 'y-mm-dd');" value="..."></td>
		</tr>
		<tr>
			<td width="20%" align="right">Start Publishing:</td>
			<td width="80%" align="left"> <input class="text_area" type="text" name="publish_up" id="publish_up" size="25" maxlength="19" value="<?php echo $row->publish_up; ?>" />
				<input type="reset" class="button" value="..." onClick="return showCalendar('publish_up', 'y-mm-dd');">
			</td>
		</tr>
		<tr>
			<td width="20%" align="right">Finish Publishing:</td>
			<td width="80%" align="left"> <input class="text_area" type="text" name="publish_down" id="publish_down" size="25" maxlength="19" value="<?php echo $row->publish_down; ?>" />
				<input type="reset" class="button" value="..." onClick="return showCalendar('publish_down', 'y-mm-dd');">
			</td>
		</tr>
		<tr>
			<td width="20%" align="right">Approve:</td>
			<td width="80%" align="left"> <input type="checkbox" name="approved" value="1"<?php echo ($row->approved == '1' || $hp_auto_approve == '1') ? " checked" : "";?>>
			</td>
		</tr>
		<tr>
			<td width="20%" align="right">Published:</td>
			<td width="80%" align="left"> <input type="checkbox" name="published" value="1"<?php echo ($row->published == '1') ? " checked" : "";?>>
			</td>
		</tr>
		<tr>
			<td colspan="2">Meta Description:
			<br /><textarea class="text_area" cols="30" rows="3" style="width:300px; height:50px" name="metadesc" width="500"><?php echo str_replace('&','&amp;',$row->metadesc); ?></textarea>
			</td>
		</tr>
			<tr>
			<td colspan="2">Meta Keywords:
			<br /><textarea class="text_area" cols="30" rows="3" style="width:300px; height:50px" name="metakey" width="500"><?php echo str_replace('&','&amp;',$row->metakey); ?></textarea>
			</td>
		</tr>

	</table>
	<?php
	$tabs->endTab();
	?>

	<?php
		if (count($extensions) > 0) {
			$j = 5;
			foreach($extensions AS $ext) {
				$tabs->startTab($ext->name,$ext->option."-page");
			?>
    <table cellpadding="5" cellspacing="0" border="0" width="100%" class="adminform">
	    <tr>
        <td align="center">
				<iframe src="index3.php?option=<?php echo $ext->option; ?>&task=ext_main&property_id=<?php echo $row->id; ?>" name="<?php echo $ext->option; ?>" id="<?php echo $ext->option; ?>" width="100%" height="<?php echo ((3 * 110)+294); ?>" marginwidth="0" marginheight="0" align="top" scrolling="auto" frameborder="0" hspace="0" vspace="0" background="white"></iframe>
				</td>
      </tr>
    </table>
		<?php
			$tabs->endTab();
			}
		}
	$tabs->endPane();

	echo "<script type=\"text/javascript\">\n";
  echo "  tabPane1.setSelectedIndex( ".$activetab.");";
	echo "</script>";
	?>

	<input type="hidden" name="option" value="<?php echo $option; ?>">
	<input type="hidden" name="id" value="<?php echo $row->id; ?>">
	<input type="hidden" name="task" value="">
	</form>
	<script language="javascript">
	<!--
		var currentType = document.adminForm.type.value;
	-->
	</script>
	<?php
	}

	function listarchived( &$rows, &$pageNav, $option, $search, $list ) {
		global $hp_use_companyagent;
?>
	<form action="index2.php" method="POST" name="adminForm">
	<table cellpadding="4" cellspacing="0" border="0" width="100%">
		<tr>
      <td width="100%" align="left"><span class="sectionname">Archived Properties</span></td>
			<td nowrap>Display #</td>
			<td> <?php echo $pageNav->writeLimitBox(); ?> </td>
      <td>Search:</td>
      <td>
        <input type="text" name="search" value="<?php echo $search;?>" class="text_area" onChange="document.adminForm.submit();" />
      </td>
      <td align="left">
        <?php echo $list["types"];?>
      </td>
		</tr>
	</table>
	<table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
		<tr>
			<th width="20">#</th>
			<th width="20"><input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $rows ); ?>);" /></th>
			<th align="left" nowrap>Property</th>
			<?php if ($hp_use_companyagent) { ?><th nowrap align="left">Agent</th><?php } ?>
			<th nowrap align="left">Type</th>
		</tr>
<?php
		$k = 0;
		for ($i=0, $n=count( $rows ); $i < $n; $i++) {
			$row = &$rows[$i];
?>
		<tr class="<?php echo "row$k"; ?>">
			<td width="20" align="center"><?php echo $i+$pageNav->limitstart+1;?></td>
			<td width="20">
				<input type="checkbox" id="cb<?php echo $i;?>" name="id[]" value="<?php echo $row->id; ?>" onClick="isChecked(this.checked);" />
			</td>
			<td width="<?php echo ($hp_use_companyagent) ? '68' : '88'; ?>%" align="left"><?php echo $row->name; ?></td>
			<?php if ($hp_use_companyagent) { ?><td width="20%" align="left"><?php echo $row->agent;?></td><?php } ?>
			<td width="12%" align="left"><?php echo $row->type;?></td>
		</tr>
		<?php		$k = 1 - $k; ?>
<?php	}
?>
		<tr>
			<th align="center" colspan="<?php echo ($hp_use_companyagent) ? '5' : '4'; ?>"> <?php echo $pageNav->writePagesLinks(); ?></th>
		</tr>
		<tr>
			<td align="center" colspan="<?php echo ($hp_use_companyagent) ? '5' : '4'; ?>"> <?php echo $pageNav->writePagesCounter(); ?></td>
		</tr>
	</table>

	<input type="hidden" name="option" value="<?php echo $option; ?>">
	<input type="hidden" name="task" value="listarchived">
	<input type="hidden" name="boxchecked" value="0">
	</form>

<?php
	}

 /*******************
 * Image Management *
 *******************/
	function listprop_starthtml_image($msg='') {
	?>
		<?php if ($msg <> '') { ?>
		<table cellpadding="5" cellspacing="0" border="0" width="100%" style="background-color:#ffffff; border: 3px solid #e5e5e5;">
			<tr>
				<td colspan="2"><?php echo $msg; ?></td>
			</tr>
		</table>
		<?php } ?>
		<div style="background-color:#f5f5f5">
		<?php
	}

	function listprop_endhtml_image() {
		?>
		</div>
		<?php
	}

	function listprop_uploadnew_image($property) {
	?>
		<form action="index3.php?option=com_hotproperty&task=listprop_upload_image&property=<?php echo $property; ?>" method="post" enctype="multipart/form-data">
		<table cellpadding="5" cellspacing="0" border="0" width="100%" style="background-color:#e5e5e5">
			<tr>
				<td colspan="2"><strong>Upload New Photo /Photos in bulk<strong></td>
			</tr>
		</table>
		<table cellpadding="5" cellspacing="0" border="0" width="100%">
			<tr>
				<td width="10%"><strong>Title (editable):</strong></td>
				<td width="90%"><input type="text" name="title" size="30" class="text_area" /></td>
			</tr>
			<tr>
				<td valign="top"><strong>Description (editable):</strong></td>
				<td><textarea name="desc" value="<?php echo $row->desc; ?>" class="text_area" rows="6" cols="50"></textarea></td>
			</tr>
			<tr>
				<td valign="top"><strong>Photo (or ZIP file of photos):</strong></td>
				<td><input class="text_area" type="file" name="image" /></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" class="button" value="Upload Photos" /></td>
			</tr>
		</table>
		</form>
	<?php
	}

	function listprop_edit_image($row, $option) {
		global $mosConfig_live_site, $hp_imgdir_standard;
		?>
		<form action="index3.php?option=com_hotproperty&task=listprop_upload_image&property=<?php echo $row->property; ?>" method="post" enctype="multipart/form-data">
		<table cellpadding="5" cellspacing="0" border="0" width="100%" style="background-color:#e5e5e5">
			<tr>
				<td colspan="2"><strong>Edit Photos<strong></td>
			</tr>
		</table>
		<p />
		<center><img style="border: 5px solid #c0c0c0;" alt="<?php echo $row->title; ?>" src="<?php echo $mosConfig_live_site.$hp_imgdir_standard.$row->standard; ?>"></center>
		<p />
		<table cellpadding="5" cellspacing="0" border="0" width="100%">
			<tr>
				<td width="240"><strong>Title:</strong></td>
				<td width="85%"><input type="text" name="title" size="30" class="text_area" value="<?php echo $row->title; ?>" /></td>
			</tr>
			<tr>
				<td valign="top"><strong>Description:</strong></td>
				<td><textarea name="desc" class="text_area" rows="6" cols="50"><?php echo $row->desc; ?></textarea></td>
			</tr>
			<tr style="display:none;">
				<td valign="top"><strong>Change Photo:</strong></td>
				<td><input class="text_area" type="file" name="image" /></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" class="button" value="Save Changes" />&nbsp;<input type="button" class="button" value="Delete this photo" onclick="javascript:if (confirm('Are you sure you want to delete selected photo? ')){ 	document.location.href='index3.php?option=com_hotproperty&task=listprop_delete_image&property=<?php echo $row->property; ?>&id=<?php echo $row->id; ?>';}" />&nbsp;<input type="button" class="button" value="Cancel" onclick="document.location.href='index3.php?option=com_hotproperty&task=listprop_image&property=<?php echo $row->property; ?>'" /></td>
			</tr>
		</table>
		<input type="hidden" name="id" value="<?php echo $row->id; ?>" />
		</form>
		<?php
	}

	function listprop_image(&$rows, $property, $option, $msg='') {
		global $mosConfig_live_site, $hp_imgdir_thumb, $hp_img_noimage_thumb;
	?>
		<p />
		<table cellpadding="5" cellspacing="0" border="0" width="100%" bgcolor="#f5f5f5">
			<tr style="background-color:#e5e5e5;">
				<td align="center">#</td>
				<td colspan="4" width="100%" align="left"><strong>Assigned Photos</strong></td>
			</tr>
		<?php
			$i=0;
			$n=count($rows);
			if ($n <= 0) echo '<tr><td colspan="4" align="center"><br />No image assigned to this property yet.</td></tr>';
			foreach($rows AS $row) {	?>
				<tr style="<?php echo (($i%2)==0) ? "background-color: #f5f5f5;":"background-color: #ffffff;" ?>">
					<td width="20" rowspan="3" align="center"><?php echo $i+1; ?></td>
					<td rowspan="3" align="center"><a href="index3.php?option=com_hotproperty&task=listprop_edit_image&property=<?php echo $property; ?>&id=<?php echo $row->id; ?>"><img alt="Click to edit this photo" style="border: 1px solid #d5d5d5;" align="left" hspace="6" src="<?php
						if (!empty($row->thumb)) {
							echo $mosConfig_live_site.$hp_imgdir_thumb.$row->thumb;
						} else {
							echo $mosConfig_live_site.$hp_imgdir_thumb.$hp_img_noimage_thumb;
						}
					?>"></a></td>
					<td width="90%"><strong><?php echo $row->title; ?></strong></td>
					<td width="60">

			<?php		if (($i > 0 ) && $row->property == @$rows[$i-1]->property) { ?>
        <a href="index3.php?option=com_hotproperty&task=orderup_images&id=<?php echo $row->id; ?>">
        <img src="images/uparrow.png" border="0" alt="Move Up">
        </a>
        <?php		} else { echo "&nbsp;"; } ?>
					</td>
					<td width="60">
			<?php		if (($i < $n-1 ) && $row->property == @$rows[$i+1]->property) { ?>
        <a href="index3.php?option=com_hotproperty&task=orderdown_images&id=<?php echo $row->id; ?>">
        <img src="images/downarrow.png" border="0" alt="Move Down">
        </a>
        <?php		} else { echo "&nbsp;"; } ?>
					</td>
				</tr>
				<tr style="<?php echo (($i%2)==0) ? "background-color: #f5f5f5;":"background-color: #ffffff;" ?>">
					<td height="50" valign="top" colspan="3"><?php echo $row->desc; ?></td>
				</tr>
				<tr height="10" style="<?php echo (($i%2)==0) ? "background-color: #f5f5f5;":"background-color: #ffffff;" ?>">
					<td valign="top" colspan="3"><a href="javascript:if (confirm('Are you sure you want to delete selected photo? ')){ 	document.location.href='index3.php?option=com_hotproperty&task=listprop_delete_image&property=<?php echo $property; ?>&id=<?php echo $row->id; ?>';}">[Delete this photo]</a></td>
				</tr>
				<?php
				$i++;
			}
		?>
		</table>
	<?php
 }

 /****************
 * Property Type *
 ****************/

	function listprop_type( &$rows, &$pageNav, $option ) {
		global $database, $mosConfig_live_site;
?>
	<form action="index2.php" method="POST" name="adminForm">
	<table cellpadding="4" cellspacing="0" border="0" width="100%">
		<tr>
      <td width="100%" align="left"><span class="sectionname">Property Type Manager</span></td>
			<td nowrap>Display #</td>
			<td> <?php echo $pageNav->writeLimitBox(); ?> </td>
		</tr>
	</table>
	<table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
		<tr>
			<th width="20">#</th>
			<th width="20"><input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $rows ); ?>);" /></th>
			<th align="left" nowrap>Property Type Name</th>
			<th nowrap>Num. of Properties</th>
			<th nowrap>Published</th>
			<th nowrap colspan="2">Reorder</th>
		</tr>
<?php
		$k = 0;
		for ($i=0, $n=count( $rows ); $i < $n; $i++) {
			$row = &$rows[$i];

			# On charge l'id du parent:
			$database->setQuery("SELECT id FROM #__hp_prop_types WHERE id=$row->parent");
			$parent=$database->loadResult();

			# On cherche le premier et le dernier item du niveau courant(pour pouvoir le conparer et afficher ou non les fl�ches de re-ordering)
			$database->setQuery("SELECT MIN(ordering) FROM #__hp_prop_types WHERE parent=$parent");
			$premier=$database->loadResult();
			$database->setQuery("SELECT MAX(ordering) FROM #__hp_prop_types WHERE parent=$parent");
			$dernier=$database->loadResult();
?>
		<tr class="<?php echo "row$k"; ?>">
			<td width="20" align="center"><?php echo $i+$pageNav->limitstart+1;?></td>
			<td width="20">
				<input type="checkbox" id="cb<?php echo $i;?>" name="id[]" value="<?php echo $row->id; ?>" onClick="isChecked(this.checked);" />
			</td>
			<td width="75%" align="left"><a href="#edit" onclick="return listItemTask('cb<?php echo $i; ?>','editprop_type')"><?php echo $row->treename; ?></a></td>
			<td width="25%" align="center"><?php echo ($row->properties) ? $row->properties : "0";?></td>
			<?php
				$task = $row->published ? 'unpublish_prop_type' : 'publish_prop_type';

				# pour afficher une icone gris�e sur les cat�gories d�sactiv�es hierarchiquement:
				if($row->published)
				{
					$type_O = new mosHPPropTypes($database);
					$type_O->load($row->id);

					$pt_test=false;
					foreach($type_O->parentsType() as $pt)
					{
						if(!$pt->published)
						{
							$pt_test=true;
							break;
						}
					}

					if($pt_test)
						$img=$mosConfig_live_site.'/components/com_hotproperty/img/publish_g2.png';
					else
						$img='images/publish_g.png';
				}
				else
				{
					$img='images/publish_x.png';
				}
			?>
			<td align="center"><a href="javascript: void(0);" onClick="return listItemTask('cb<?php echo $i;?>','<?php echo $task;?>')"><img src="<?php echo $img;?>" border="0" alt="" /></a></td>
      <td>
			<?php
			// Si c'est le premier, on affiche pas la fl�che du haut:
			if (($i > 0 || ($i+$pageNav->limitstart > 0)) && $row->ordering!=$premier ) { ?>
        <a href="#reorder" onClick="return listItemTask('cb<?php echo $i;?>','orderup_type')">
        <img src="images/uparrow.png" border="0" alt="Move Up">
        </a>
        <?php		} else { echo "&nbsp;"; } ?>
      </td>
      <td>
			<?php
			// Si c'est le dernier, on affiche pas la fl�che du bas:
			if (($i < $n-1 || $i+$pageNav->limitstart < $pageNav->total-1) && $row->ordering!=$dernier ) { ?>
        <a href="#reorder" onClick="return listItemTask('cb<?php echo $i;?>','orderdown_type')">
        <img src="images/downarrow.png" border="0" alt="Move Down">
        </a>
        <?php		} else { echo "&nbsp;"; } ?>
      </td>
			<?php		$k = 1 - $k; ?>
		</tr>
<?php	}
?>
		<tr>
			<th align="center" colspan="7"> <?php echo $pageNav->writePagesLinks(); ?></th>
		</tr>
		<tr>
			<td align="center" colspan="7"> <?php echo $pageNav->writePagesCounter(); ?></td>
		</tr>
	</table>

	<input type="hidden" name="option" value="<?php echo $option; ?>" />
	<input type="hidden" name="task" value="listprop_type" />
	<input type="hidden" name="boxchecked" value="0" />
	</form>
<?php
	}

	function editprop_type( &$row, &$lists, $option ) {
		global $database, $mosConfig_absolute_path, $mosConfig_editor;

		//mosMakeHtmlSafe( $row, ENT_QUOTES, 'desc' );
		mosMakeHtmlSafe( $row );

		//include_once( $mosConfig_absolute_path . "/editor/editor.php" );
		//initEditor();

?>

	<script language="javascript">
	<!--
		function submitbutton(pressbutton) {
			var form = document.adminForm;
			if (pressbutton == 'cancelprop_type') {
				submitform( pressbutton );
				return;
			}
			// do field validation
			if (form.name.value == "") {
				alert( "Please fill in the Property Type Name." );
			} else {
				<?php getEditorContents( 'editor1', 'desc' ) ; ?>
				submitform( pressbutton );
			}
		}
	//-->
	</script>
	<table cellpadding="4" cellspacing="0" border="0" width="100%">
		<tr>
			<td width="100%"><span class="sectionname"><?php echo $row->id ? 'Edit' : 'Add';?> Property Type</span></td>
		</tr>
	</table>
	<form action="index2.php" method="POST" name="adminForm">
	<table cellpadding="4" cellspacing="1" border="0" width="100%" class="adminform">
		<tr>
			<td align="left" width="10%">Property Type Name:</td>
			<td align="left"><input class="text_area" type="text" name="name" size="30" valign="top" value="<?php echo $row->name; ?>"></td>
		</tr>
		<tr>
			<td align="left" width="10%">Parent Property Type:</td>
			<td align="left">
				<?php
					$query = "SELECT * FROM #__hp_prop_types" . ( isset($row->id) ? " WHERE id != 0 AND id!=$row->id" : "" ); //la cat�gorie courante ne peut etre la parente ce qui justifie la condition WHERE qui est conditionnelle dans le cas ou la cat�gorie n'existe pas encore(cr�ation)
					$database->setQuery( $query );
					$src_list = $database->loadObjectList();
					$preload = array();
					$preload[] = mosHTML::makeOption( '0', '/root' );
					$selected = array();
					$selected[] = mosHTML::makeOption( $row->parent ); //s�lectionne par d�faut dans la liste l'item d'id
					echo mosHTML::treeSelectList( &$src_list, -1, $preload, 'parent',
				  	               'class="inputbox" size="1"', 'value', 'text', $selected );
				?>
			</td>
		</tr>
		<tr>
			<td align="left" valign="top">Description:</td>
			<td align="left"><?php editorArea( 'editor1',  $row->desc ? $row->desc : "", 'desc', 500, 200, '70', '15' ) ; ?></td>
		</tr>
	</table>
	<input type="hidden" name="option" value="<?php echo $option; ?>">
	<input type="hidden" name="id" value="<?php echo $row->id; ?>">
	<input type="hidden" name="lft" value="<?php echo $row->lft; ?>">
	<input type="hidden" name="rgt" value="<?php echo $row->rgt; ?>">
	<input type="hidden" name="task" value="">
	</form>
<?php }

 /****************
 * Extra Fields  *
 ****************/

	function listprop_ef( &$rows, &$lists, &$pageNav, $option ) {
		global $database;
?>
	<form action="index2.php" method="POST" name="adminForm">
	<table cellpadding="4" cellspacing="0" border="0" width="100%">
		<tr>
      <td width="100%" align="left"><span class="sectionname">Extra Fields Manager</span></td>
			<td nowrap>Show <?php echo $lists["show"];?></td>
			<td nowrap>Display #</td>
			<td> <?php echo $pageNav->writeLimitBox(); ?> </td>
		</tr>
	</table>
	<table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
		<tr>
			<th width="20">#</th>
			<th width="20"><input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $rows ); ?>);" /></th>
			<th width="30%" align="left" nowrap>Caption</th>
			<th width="20%" align="left" nowrap>Field Name</th>
			<th width="15%" align="left" nowrap>Type</th>
			<th width="10%" align="center" nowrap>Related Types</th>
			<th width="50" align="left" nowrap>Featured</th>
			<th width="50" align="left" nowrap>Listing</th>
			<th width="50" align="left" nowrap>Searchable</th>
			<th width="10%" nowrap>Published</th>
			<th width="10%" colspan="2" nowrap>Reorder</th>
		</tr>
<?php
		$k = 0;
		for ($i=0, $n=count( $rows ); $i < $n; $i++) {
			$row = &$rows[$i];
?>
		<tr class="<?php echo "row$k"; ?>">
			<td align="center"><?php echo $i+$pageNav->limitstart+1;?></td>
			<td>
				<input type="checkbox" id="cb<?php echo $i;?>" name="id[]" value="<?php echo $row->id; ?>" onClick="isChecked(this.checked);" />
			</td>
			<td align="left">
				<a href="#edit" onclick="return listItemTask('cb<?php echo $i; ?>','editprop_ef')"><?php echo $row->caption; ?></a>
			</td>
			<td align="left"><?php echo $row->name;?></td>
			<td align="left"><?php echo ($row->iscore) ? "<b>CORE</b>" : $row->field_type;?></td>
			<td align="center">
				<?php
					$ef=new mosHPPropEF($database);
					$ef->load($row->id);
					$reltypes=$ef->listRelTypes();

					$liste="";
					for($j=0, $p=count( $reltypes ); $j < $p; $j++)
					{
						$liste .= $reltypes[$j]->name . (( $j != ($p-1) ) ? ",&nbsp;" : "");
					};

					switch($p)
					{
						case 0:
							echo "None";
							break;
						case 1:
							if($liste=='/root')
								echo 'All';
							else
								echo '<span style="font-style:italic;">'.$liste.'</span>';
							break;
						default:
							echo '<span title="'.$liste.'" style="border-bottom:thin dotted;cursor:help;">Varies</span>';
							break;
					}
				?>
			</td>
			<?php
				if ($row->hidden) {
			?>
			<td align="center" colspan="2"><strong>HIDDEN</strong></td>
			<?php
				} else {
			?>
			<td align="center"><?php
					if ($row->name <> 'name') {
						if ($row->featured == "1") {
						?>
							<a href="#ToggleFeatured" onClick="return listItemTask('cb<?php echo $i;?>','togglefeatured_ef')"><img border="0" src="images/tick.png"></a>
						<?php
						} else { ?>
							<a href="#ToggleFeatured" onClick="return listItemTask('cb<?php echo $i;?>','togglefeatured_ef')"><img border="0"  src="images/publish_x.png"></a>
						<?php
							}
					} else { echo "<img src=\"images/tick.png\">"; } ?>
			</td>
			<td align="center"><?php
					if ($row->name <> 'name') {
						if ($row->listing) { ?>
							<a href="#ToggleListing" onClick="return listItemTask('cb<?php echo $i;?>','togglelisting_ef')"><img border="0" src="images/tick.png"></a>
							<?php } else { ?>
								<a href="#ToggleListing" onClick="return listItemTask('cb<?php echo $i;?>','togglelisting_ef')"><img border="0"  src="images/publish_x.png"></a>
							<?php
						}
					} else {echo "<img src=\"images/tick.png\">";} ?>
			</td>
			<?php
				} // End of Hidden
			?>
			<td align="center"><?php
					if ($row->search) { ?>
						<img border="0" src="images/tick.png">
						<?php } else { ?>
							<img border="0"  src="images/publish_x.png">
						<?php
					}
					?>
			</td>
			<?php
				$task = $row->published ? 'unpublish_prop_ef' : 'publish_prop_ef';
				$img = $row->published ? 'publish_g.png' : 'publish_x.png';
			?>
			<td align="center">
				<?php if ($row->name <> 'name') { ?>
				<a href="javascript: void(0);" onClick="return listItemTask('cb<?php echo $i;?>','<?php echo $task;?>')"><img src="images/<?php echo $img;?>" border="0" alt="" /></a>
				<?php } else {echo "<img src=\"images/publish_g.png\">";} ?>
			</td>
      <td>
			<?php		if (($i > 0 || ($i+$pageNav->limitstart > 0)) && $row->iscore == @$rows[$i-1]->iscore) { ?>
        <a href="#reorder" onClick="return listItemTask('cb<?php echo $i;?>','orderup_ef')">
        <img src="images/uparrow.png" border="0" alt="Move Up">
        </a>
        <?php		} else { echo "&nbsp;"; } ?>
      </td>
      <td>
			<?php		if (($i < $n-1 || $i+$pageNav->limitstart < $pageNav->total-1) && $row->iscore == @$rows[$i+1]->iscore) { ?>
        <a href="#reorder" onClick="return listItemTask('cb<?php echo $i;?>','orderdown_ef')">
        <img src="images/downarrow.png" border="0" alt="Move Down">
        </a>
        <?php		} else { echo "&nbsp;"; } ?>
      </td>
			<?php		$k = 1 - $k; ?>
		</tr>
<?php	}
?>
		<tr>
			<th align="center" colspan="12"> <?php echo $pageNav->writePagesLinks(); ?></th>
		</tr>
		<tr>
			<td align="center" colspan="12"> <?php echo $pageNav->writePagesCounter(); ?></td>
		</tr>
	</table>

	<input type="hidden" name="option" value="<?php echo $option; ?>">
	<input type="hidden" name="task" value="listprop_ef">
	<input type="hidden" name="boxchecked" value="0">
	</form>
<?php
	}

	function editprop_ef( &$row, &$lists, $option, &$types, &$selected ) {
		global $ef_types_need_elements, $database;
		//mosMakeHtmlSafe( $row, ENT_QUOTES, 'desc' );
		mosMakeHtmlSafe( $row );

?>
	<script language="javascript">
	<!--
		function submitbutton(pressbutton) {
			var form = document.adminForm;
			if (pressbutton == 'cancelprop_ef') {
				submitform( pressbutton );
				return;
			}
			// do field validation
			if (form.caption.value == "") {
				alert( "Please fill in the Field's caption." );
			} else if (form.iscore.value == "0") {
				if (form.name.value == "") {
					alert( "Please fill in the Field's name." );
				} else if (form.field_type.value == "0" ) {
					alert( "Please fill in the Field's field type." );
				} else if ( ( form.field_type.value == "checkbox" || form.field_type.value == "selectlist" || form.field_type.value == "selectmultiple" || form.field_type.value == "radiobutton" ) && 0 ) {
						alert( "Please fill in the Field Elements." );
				} else if (form.search.checked) {
					if (form.search_caption.value == "") {
						alert( "Please fill in the Search Caption." );
					}	else {
						submitform( pressbutton );
					}
				} else {
						submitform( pressbutton );
				}
			} else if (form.search.checked) {
					if (form.search_caption.value == "") {
						alert( "Please fill in the Search Caption." );
					}	else {
						submitform( pressbutton );
					}
			} else {
				submitform( pressbutton );
			}
		}
		
		function addRowToTable()
		{
			var tbl = document.getElementById('field_element_table');
			var lastRow = tbl.rows.length;
			// if there's no header row in the table, then iteration = lastRow + 1
			var iteration = lastRow;
			var row = tbl.insertRow(lastRow);
			row.id = ('el_' + iteration);
			
			// left cell
			var cellLeft = row.insertCell(0);
			var el = document.createElement('input');
			el.type = 'text';
			el.name = 'field_elements[]';
			el.size = 30;
			cellLeft.appendChild(el);
			
			// right cell
			var cellRight = row.insertCell(1);
			var el2 = document.createElement('input');
			el2.type = 'button';
			el2.value = 'Delete';
			el2.onclick = function(){removeRowFromTable(this)};
			cellRight.appendChild(el2);		
		}
		
		function removeRowFromTable(button)
		{
			if(confirm('Are you sure you wish to delete this value?')){
				var row = button.parentNode.parentNode;			
				var tbl = document.getElementById('field_element_table');
				tbl.deleteRow(row.rowIndex);
			}
		}
		
	//-->
	</script>

	<table cellpadding="4" cellspacing="0" border="0" width="100%">
		<tr>
			<td width="100%"><span class="sectionname"><?php echo $row->id ? 'Edit' : 'Add';?> <?php echo $row->iscore ? 'Core' : 'Extra';?> Fields</span></td>
		</tr>
	</table>
	<form action="index2.php" method="POST" name="adminForm">
	<table cellpadding="4" cellspacing="1" border="0" width="100%" class="adminform">
		<tr valign="top">
			<td align="left" width="13%">Caption:</td>
			<td align="left"><input class="text_area" type="text" name="caption" size="30" valign="top" value="<?php echo $row->caption; ?>"></td>
			<td align="middle" valign="top" rowspan="9" width="180">
			<?php
			# By default, 'name' field is shown in all listing (featured, listing, view)
			if ($row->name <> 'name')	 {
			?>
			<!--la liste des cat�gories associ�es � l'extra-field-->
			<table cellpadding="3" cellspacing="0" border="0" class="adminform">
              <tr>
                <th colspan="2">Types</th>
              </tr>
              <tr>
              	<td>
              		<?php
					$preload = array();
					$preload[] = mosHTML::makeOption( '0', "All" );
					$preload[] = mosHTML::makeOption( '-999', '----' );
					$preload[] = mosHTML::makeOption( '-1', "None" );
					$preload[] = mosHTML::makeOption( '-999', '----' );
					echo mosHTML::treeSelectList( &$types, 0, $preload, 'reltypes[]',
				  	               'class="inputbox" size="15" multiple="multiple"', 'value', 'text', $selected );
				  	?>
              	</td>
              </tr>
            </table>
            <!--FIN de la liste des cat�gories associ�es � l'extra-field-->
		    <table cellpadding="3" cellspacing="0" border="0" class="adminform">
              <tr>
                <th colspan="2">Options</th>
              </tr>
							<tr>
								<td align="left" width="80%" valign="top">Show in Featured:</td>
								<td align="left" width="20%"><input type="checkbox" name="featured" value="1"<?php echo ($row->featured) ? " checked":"";?>></td>
							</tr>
							<tr>
								<td align="left" valign="top">Show in Listing:</td>
								<td align="left"><input type="checkbox" name="listing" value="1"<?php echo ($row->listing) ? " checked":"";?>></td>
							</tr>
							<tr>
								<td align="left" valign="top">Hide Caption:</td>
								<td align="left"><input type="checkbox" name="hideCaption" value="1"<?php echo ($row->hideCaption) ? " checked":"";?>></td>
							</tr>
							<tr>
								<td align="left" valign="top">Hidden:</td>
								<td align="left"><input type="checkbox" name="hidden" value="1"<?php echo ($row->hidden) ? " checked":"";?>></td>
							</tr>
							<tr>
								<td align="left" valign="top">Published:</td>
								<td align="left"><input type="checkbox" name="published" value="1"<?php echo ($row->published) ? " checked":"";?>></td>
							</tr>
				</table>
				<p />
				<?php } // End of <> name
				# Searchable Field ?>
		    <table cellpadding="3" cellspacing="0" border="0" width="180" class="adminform">
              <tr>
                <th><input type="checkbox" name="search" value="1"<?php echo ($row->search) ? " checked":"";?>> Searchable Field</th>
              </tr>
							<tr><td align="left">Caption:</td></tr>
							<tr><td align="left"><input class="text_area" type="text" name="search_caption" size="21" value="<?php echo $row->search_caption; ?>"></td></tr>
							<tr><td align="left">Type:</td></tr>
							<tr><td align="left"><?php echo $lists['search_types']; ?></td></tr>
				</table>
			</td>

		</tr>
		<?php
		# Allow changing of field name, type and elements for non-core fields ONLY
		if(!$row->iscore) { ?>
		<tr valign="top">
			<td align="left" width="10%">Field Name:</td>
			<td align="left"><input class="text_area" type="text" name="name" size="30" valign="top" value="<?php echo $row->name; ?>"></td>
		</tr>
		<?php } ?>
		<tr valign="top">
			<td align="left" width="10%">Field Type:</td>
			<td align="left"><?php echo $lists["field_types"]; ?></td>
		</tr>
		<tr valign="top">
			<td align="left" width="10%" valign="top">Field Elements:</td>
				<td align="left">
				<?php 
				$elements = explode('|',$row->field_elements); 
				if(!count($elements)) $elements[] = '';
				?>				
				<table width="100%" id='field_element_table' style='display: <?php echo in_array($row->field_type, $ef_types_need_elements) ? 'block' : 'none' ?>'>
					<tr>
						<td>Click "Add Value" to create an option</td>
						<td>
							<input type='button' value='Add Value' onclick='addRowToTable()' />
						</td>
					</tr>
					<?php foreach($elements as $el){
						echo "<tr>
								<td>
									<input type='text' value='$el' size='30' name='field_elements[]' />
								</td>
								<td><input type='button' value='Delete' onclick='removeRowFromTable(this)' /></td>
							</tr>";
					}?>		
				</table>		
			</td>
		</tr>
		<tr valign="top">
			<td align="left" width="10%">Default Value:</td>
			<td align="left"><input class="text_area" type="text" name="default_value" size="30" valign="top" value="<?php echo $row->default_value; ?>"></td>
		</tr>
		<?php
		
		# Allow append_text and prefix_text if this is a non-core field OR core price field.
		if(!$row->iscore || $row->name == 'price' || 1) { ?>
		<tr>
			<td align="left" width="10%" valign="top">Prefix Text:</td>
			<td align="left"><input class="text_area" type="text" name="prefix_text" size="30" valign="top" value="<?php echo $row->prefix_text; ?>"<?php if( $row->field_type <> "text" && $row->field_type <> "multitext" && $row->name <> 'price' && $row->field_type <> "link") echo " disabled style=\"background-color:#F5F5F5\""; ?>>
      <br /><em>Text to display before the value. ie: USD</em></td>
		</tr>
		<tr valign="top">
			<td align="left" width="10%" valign="top">Append Text:</td>
			<td align="left"><input class="text_area" type="text" name="append_text" size="30" valign="top" value="<?php echo $row->append_text; ?>"<?php if( $row->field_type <> "text" && $row->field_type <> "multitext" && $row->name <> 'price' && $row->field_type <> "link") echo " disabled style=\"background-color:#F5F5F5\""; ?>>
      <br /><em>Text to display after the value. ie: metres, square feets</em></td>
		</tr>
		<?php } ?>
		<?php
		# Allow changing of size non-core fields ONLY
		if(!$row->iscore || 1) { ?>
		<tr valign="top">
			<td align="left" width="10%">Size:</td>
			<td align="left"><input class="text_area" type="text" name="size" size="3" valign="top" value="<?php echo $row->size; ?>"></td>
		</tr>
		<?php } ?>
		<tr valign="top">
			<td align="left" width="10%">Ordering:</td>
			<td align="left"><?php
				if ($row->id)	echo $lists["order"];
				else echo "New items default to the last place";
			?></td>
		</tr>
	</table>
	<input type="hidden" name="option" value="<?php echo $option; ?>">
	<input type="hidden" name="id" value="<?php echo $row->id; ?>">
	<input type="hidden" name="iscore" value="<?php echo $row->iscore; ?>">
	<input type="hidden" name="task" value="">
	</form>
<?php }

 /************
 * Featured  *
 ************/

	function listfeatured( &$rows, &$pageNav, $option ) {
?>
	<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
	<script language="Javascript" src="../includes/js/overlib_mini.js"></script>
	<form action="index2.php" method="POST" name="adminForm">
	<table cellpadding="4" cellspacing="0" border="0" width="100%">
		<tr>

      <td width="100%" align="left"><span class="sectionname">Featured Items</span></td>
			<td nowrap>Display #</td>
			<td> <?php echo $pageNav->writeLimitBox(); ?> </td>
		</tr>
	</table>
	<table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
		<tr>
			<th width="20">#</th>
			<th width="20"><input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $rows ); ?>);" /></th>
			<th width="80%" align="left" nowrap>Property</th>
			<th nowrap align="left">Type</th>
			<th nowrap>Published</th>
			<th colspan="2" nowrap>Reorder</th>

		</tr>
<?php
		$k = 0;
		for ($i=0, $n=count( $rows ); $i < $n; $i++) {
			$row = &$rows[$i];
?>
		<tr class="<?php echo "row$k"; ?>">
			<td width="20" align="center"><?php echo $i+$pageNav->limitstart+1;?></td>
			<td width="20">
				<input type="checkbox" id="cb<?php echo $i;?>" name="id[]" value="<?php echo $row->id; ?>" onClick="isChecked(this.checked);" />
			</td>
			<td width="38%" align="left"><?php echo $row->name; ?></td>
			<td width="12%" align="left"><?php echo $row->type;?></td>
     <?php
			$now = date( "Y-m-d h:i:s" );
			if ($now <= $row->publish_up && $row->published == "1") {
				$img = 'publish_y.png';
			} else if (($now <= $row->publish_down || $row->publish_down == "0000-00-00 00:00:00") && $row->published == "1") {
				$img = 'publish_g.png';
			} else if ($now > $row->publish_down && $row->published == "1") {
				$img = 'publish_r.png';
			} elseif ($row->published == "0") {
				$img = "publish_x.png";
			}
			$times = '';
			if (isset($row->publish_up)) {
				if ($row->publish_up == '0000-00-00 00:00:00') {
					$times .= "<tr><td>Start: Always</td></tr>";
				} else {
					$times .= "<tr><td>Start: $row->publish_up</td></tr>";
				}
			}
			if (isset($row->publish_down)) {
				if ($row->publish_down == '0000-00-00 00:00:00') {
					$times .= "<tr><td>Finish: No Expiry</td></tr>";
				} else {
					$times .= "<tr><td>Finish: $row->publish_down</td></tr>";
				}
			}
			if ($times) {
?>
        <td align="center"><a href="javascript: void(0);" onMouseOver="return overlib('<table border=0 width=100% height=100%><?php echo $times; ?></table>', CAPTION, 'Publish Information', BELOW, RIGHT);" onMouseOut="return nd();" onClick="return listItemTask('cb<?php echo $i;?>','<?php echo $row->published ? "unpublish_featured" : "publish_featured";?>')"><img src="images/<?php echo $img;?>" border="0" alt="" /></a>
        <?php
			}
	  ?>
     </td>
     <td width="30">
			<?php		if ($i > 0 || ($i+$pageNav->limitstart > 0)) { ?>
        <a href="#reorder" onClick="return listItemTask('cb<?php echo $i;?>','orderup_featured')">
        <img src="images/uparrow.png" border="0" alt="Move Up">
        </a>
        <?php		} else { echo "&nbsp;"; } ?>
      </td>
      <td width="30">
			<?php		if ($i < $n-1 || $i+$pageNav->limitstart < $pageNav->total-1) { ?>
        <a href="#reorder" onClick="return listItemTask('cb<?php echo $i;?>','orderdown_featured')">
        <img src="images/downarrow.png" border="0" alt="Move Down">
        </a>
        <?php		} else { echo "&nbsp;"; } ?>
      </td>

<?php		$k = 1 - $k; ?>
		</tr>
<?php	}
?>
		<tr>
			<th align="center" colspan="9"> <?php echo $pageNav->writePagesLinks(); ?></th>
		</tr>
		<tr>
			<td align="center" colspan="9"> <?php echo $pageNav->writePagesCounter(); ?></td>
		</tr>
	</table>

	<input type="hidden" name="option" value="<?php echo $option; ?>">
	<input type="hidden" name="task" value="featured">
	<input type="hidden" name="boxchecked" value="0">
	</form>
	<br />
	<table cellspacing="0" cellpadding="4" border="0" align="center">
		<tr align="center">
			<td> <img src="images/publish_y.png" border=0 alt="Pending" />
			</td>
			<td> Published, but is <u>Pending</u> |</td>
			<td> <img src="images/publish_g.png" border=0 alt="Visible" />
			</td>
			<td> Published and is <u>Current</u> |</td>
			<td> <img src="images/publish_r.png" border=0 alt="Finished" />
			</td>
			<td> Published, but has <u>Expired</u> |</td>
			<td> <img src="images/publish_x.png" border=0 alt="Finished" />
			</td>
			<td> Not Published </td>
		</tr>
		<tr>
		<td colspan="8" align="center">Click on icon to toggle state.</td>
		</tr>
	</table>
<?php
	}

/*****************
 * Log Searches  *
 ****************/

	function log_searches( &$rows, &$pageNav, $option ) {
?>
	<form action="index2.php" method="POST" name="adminForm">
	<table cellpadding="4" cellspacing="0" border="0" width="100%">
		<tr>

      <td width="100%" align="left"><span class="sectionname">Search Text's Log</span></td>
			<td nowrap>Display #</td>
			<td> <?php echo $pageNav->writeLimitBox(); ?> </td>
		</tr>
	</table>
	<table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
		<tr>
			<th width="40" align="right">#</th>
			<th width="80%" align="left" nowrap>Search Text</th>
			<th width="120" nowrap align="center">Times Requested</th>
		</tr>
<?php
		$k = 0;
		for ($i=0, $n=count( $rows ); $i < $n; $i++) {
			$row = &$rows[$i];
?>
		<tr class="<?php echo "row$k"; ?>">
			<td width="40" align="right"><?php echo $i+$pageNav->limitstart+1;?></td>
			<td width="80%" align="left"><?php echo $row->search_term; ?></td>
			<td width="12s0" align="center"><?php echo $row->hits;?></td>
			<?php		$k = 1 - $k; ?>
		</tr>
<?php	}
?>
		<tr>
			<th align="center" colspan="3"> <?php echo $pageNav->writePagesLinks(); ?></th>
		</tr>
		<tr>
			<td align="center" colspan="3"> <?php echo $pageNav->writePagesCounter(); ?></td>
		</tr>
	</table>

	<input type="hidden" name="option" value="<?php echo $option; ?>">
	<input type="hidden" name="task" value="log_searches">
	<input type="hidden" name="boxchecked" value="0">
	</form>
<?php
	}

/**************
 * Reporting  *
 **************/
	function report( $fields, $option ) {
	?>
   <script type="text/javascript" language="javascript">
		function submitbutton( pressbutton ) {
			var form = document.adminForm;

			// do field validation

			var elts      = document.adminForm.elements['fields[]'];
			var elts_cnt  = (typeof(elts.length) != 'undefined')
										? elts.length
										: 0;

			temp = false;
			for (var i = 0; i < elts_cnt; i++) {
					if (elts[i].checked == true) temp = true;
			}

			if (temp == true) {
				submitform( pressbutton );
			} else {
				alert('Please select at least one field.');
			}
		}

		function setCheckboxes(the_form, do_check)
		{
				var elts      = document.forms[the_form].elements['fields[]'];
				var elts_cnt  = (typeof(elts.length) != 'undefined')
											? elts.length
											: 0;

				if (elts_cnt) {
						for (var i = 0; i < elts_cnt; i++) {
								elts[i].checked = do_check;
						} // end for
				} else {
						elts.checked        = do_check;
				} // end if... else

				return true;
		} // end of the 'setCheckboxes()' function
	 </script>
<form action="index2.php" method="POST" name="adminForm">
<table cellpadding="4" cellspacing="0" border="0" width="100%">
	<tr><td colspan="2" align="left"><span class="sectionname">Report</span></td></tr>
  <tr valign="top">
		<td width="50%">
			<fieldset class="report">
				<legend>Fields</legend>
				<table cellpadding="1" cellspacing="0" border="0" width="100%">
				<?php
					$i = 0;
					foreach ($fields AS $field) {
						if ($i%2 == 0) echo "<tr>";
						echo "<td><input type='checkbox' name='fields[]' value='".$field->name."' /> ".$field->caption. "</td>";
						if ($i%2 == 1) echo "</tr>";
						$i++;
					}
				?>
				</table>
				<br />
				<a href="#" onclick="setCheckboxes('adminForm', true); return false;">Select All</a> / <a href="#" onclick="setCheckboxes('adminForm', false); return false;">Unselect All</a>
			</fieldset>
		</td>
		<td>
			<fieldset class="report">
				<legend>Report Options</legend>

				<input type="radio" name="show_all" value="1" checked /> Show all
				<br />
				<input type="radio" name="show_all" value="0" /> Show only published property
				<p />
				<input type="checkbox" name="nowrap" value="1" /> No wrapping text<br />
				<input type="checkbox" name="show_fulltext" value="1" /> Show full text
			</fieldset>
			<br />
			<input type="hidden" name="option" value="<?php echo $option; ?>" />
			<input type="hidden" name="task" value="doreport" />
		</td>
	</tr>
</table>
</form>
	<?php
	}

	function doreport( $fields, $fields2, $rows, $pageNav, $report_options, $option ) {
	?>
	<form action="index2.php" method="POST" name="adminForm">
	<table cellpadding="4" cellspacing="0" border="0" width="100%">
		<tr>
      <td width="100%" align="left"><span class="sectionname">Report View</span></td>
			<td nowrap>Display #</td>
			<td> <?php echo $pageNav->writeLimitBox(); ?> </td>
		</tr>
		<tr>
			<td colspan="3" align="left">
			<a href="index3.php?option=<?php echo $option; ?>&task=doreport_print&nowrap=<?php echo $report_options['nowrap']; ?>&show_fulltext=<?php echo $report_options['show_fulltext']; ?>" target="_blank">Print</a> | <a href="index2.php?option=<?php echo $option; ?>&task=doreport_excel">Convert All to CSV for Ms Excel</a>
			</td>
		</tr>
	</table>

	<table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
		<tr>
			<th width="20">#</th>
			<?php foreach($fields2 AS $field) { ?>
				<th align="left" nowrap><?php echo $field->caption; ?></th>
			<?php } ?>
		</tr>
<?php
		$k = 0;
		for ($i=0, $n=count( $rows ); $i < $n; $i++) {
			$row = &$rows[$i];
?>
		<tr class="<?php echo "row$k"; ?>">
			<td width="20" align="center"><?php echo $i+$pageNav->limitstart+1;?></td>
			<?php
			$j = 0;
			foreach($row AS $r) {
				if ($j > 0)	{
					if (strlen($r) > 50 && $report_options['show_fulltext'] <> '1') {
						$r = substr($r, 0, 50) . '...';
					}
					//echo '<td align="left" nowrap>';
					echo '<td align="left"'.( ($report_options['nowrap'] == '1') ? 'nowrap ' : '').'>';
					echo (($r <> '') ? htmlspecialchars($r) : '&nbsp;');
					echo '</td>';
				}
				$j++;
			} ?>
<?php		$k = 1 - $k; ?>
		</tr>
<?php	}
?>
		<tr>
			<th align="center" colspan="<?php echo (count($fields2) +1); ?>"> <?php echo $pageNav->writePagesLinks(); ?></th>
		</tr>
		<tr>
			<td align="center" colspan="<?php echo (count($fields2) +1); ?>"> <?php echo $pageNav->writePagesCounter(); ?></td>
		</tr>
	</table>
	<?php
			foreach($fields AS $f)	{
				echo "<input type='hidden' name='fields[]' value='$f' /> \n";
			}
	?>
	<input type="hidden" name="option" value="<?php echo $option; ?>">
	<input type="hidden" name="task" value="doreport">
	<input type="hidden" name="show_fulltext" value="<?php echo $report_options['show_fulltext']; ?>">
	<input type="hidden" name="nowrap" value="<?php echo $report_options['nowrap']; ?>">
	<input type="hidden" name="boxchecked" value="0">
	</form>
	<?php
	}

	function doreport_print( $fields, $rows, $report_options, $option ) {
	?>
	<a href="#" onclick="javascript:window.print(); return false" title="Print">[Print]</a>
	&nbsp;
	<a href="#" onclick="javascript:window.close(); return false" title="Print">[Close]</a>

	<p />
	<table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
		<tr>
			<th width="20">#</th>
			<?php foreach($fields AS $field) { ?>
				<th align="left" nowrap><?php echo $field; ?></th>
			<?php } ?>
		</tr>
<?php
		for ($i=0, $n=count( $rows ); $i < $n; $i++) {
			$row = &$rows[$i];
?>
		<tr class="<?php echo "row1"; ?>">
			<td width="20" align="center"><?php echo $i+1;?></td>
			<?php
			$j = 0;
			foreach($row AS $r) {
				if ($j > 0)	{
					if (strlen($r) > 50 && $report_options['show_fulltext'] <> 1) {
						$r = substr($r, 0, 50) . '...'.$report_options['show_fulltext'];
					}
					//echo '<td align="left" nowrap>';
					echo '<td align="left"'.( ($report_options['nowrap'] == 1) ? 'nowrap ' : '').'>';
					echo (($r <> '') ? htmlspecialchars($r) : '&nbsp;');
					echo '</td>';
				}
				$j++;
			} ?>
		</tr>
<?php	}
?>
	</table>
	<?php
	}

	function doreport_excel( $header, $data, $option ) {
	?>
	<script language="Javascript">
	<!--
	/*
	Select and Copy form element script- By Dynamicdrive.com
	For full source, Terms of service, and 100s DTHML scripts
	Visit http://www.dynamicdrive.com
	*/

	//specify whether contents should be auto copied to clipboard (memory)
	//Applies only to IE 4+
	//0=no, 1=yes
	var copytoclip=1

	function HighlightAll(theField) {
	var tempval=eval("document."+theField)
	tempval.focus()
	tempval.select()
	if (document.all&&copytoclip==1){
	therange=tempval.createTextRange()
	therange.execCommand("Copy")
	window.status="Contents highlighted and copied to clipboard!"
	setTimeout("window.status=''",1800)
	}
	}
	//-->
	</script>

	<table cellpadding="4" cellspacing="0" border="0" width="100%">
		<tr><td width="100%" align="left"><span class="sectionname">Report View</span></td></tr>
	</table>

	<center>
	<form action="index2.php" method="POST" name="adminForm">
	<table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
		<tr><th>CSV Data for Ms Excel</th></tr>
		<tr class="row0">
			<td>
			<p />
			<a href="javascript:HighlightAll('adminForm.csv_excel')">Select All</a>
			<p />
			<textarea name="csv_excel" rows="30" cols="80"><?php
				echo $header;
				echo $data;
			?></textarea>
			<p />
			<a href="javascript:HighlightAll('adminForm.csv_excel')">Select All</a>
			</td>
	</table>
	<input type="hidden" name="option" value="<?php echo $option; ?>" />
	<input type="hidden" name="task" value="doreport" />
	</form>
	</center>
	<?php
	}

/*****************
 * Configuration *
 ****************/
	function showConfig( $row, $lists, $imageLibs, $captions, $option ) {

		// Initialize Tabs
		$tabs = new mosTabs(0);
	?>
<script language="javascript" type="text/javascript">
	function submitbutton(pressbutton) {
		var form = document.adminForm;
		if (form.use_companyagent.value == "0" && form.default_agent.value == "0") {
				alert('Please choose a Default Agent');
		} else if (pressbutton == 'saveConfig') {
				submitform( pressbutton );
		} else {
			document.location.href = 'index2.php';
		}
	}
</script>

<table cellpadding="4" cellspacing="0" border="0" width="100%">
  <tr>
    <td width="100%" class="sectionname">Hot Property Configuration: <span class="componentheading">Configuration file is :
      <?php echo is_writable( 'components/com_hotproperty/config.hotproperty.php' ) ? '<b><font color="green">Writeable</font></b>' : '<b><font color="red">Unwriteable</font></b>' ?>
</span></td>
  </tr>
</table>

<form action="index2.php" method="POST" name="adminForm">
	<?php
	$tabs->startPane("content-pane");
	$tabs->startTab("Main Config","mainconfig-page");
	?>
	<table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminform">
		<tr>
			<td align="left" width="185">CSS File:</td>
			<td align="left" width="78%"><?php echo $lists['css']; ?></td>
		</tr>
		<tr>
			<td align="left">Language:</td>
			<td align="left"><?php echo $lists['lang']; ?></td>
		</tr>
		<tr>
			<td align="left">Log Searches:</td>
			<td align="left"><?php echo mosHTML::yesnoSelectList('log_search','class="text_area"',$row->log_search); ?></td>
		</tr>
		<tr><td align="left" colspan="2" style="border-top: 1px solid #C0C0C0; border-bottom: 1px solid #C0C0C0; background-color: #FFFFFF"><span class="error">WARNING: </span>Auto-approving all new property will allow Agents to submit and published <u>ANY</u> property to the website without getting approval from you. </td></tr>
		<tr>
			<td align="left">Auto approve all new property:</td>
			<td align="left"><?php echo mosHTML::yesnoSelectList('auto_approve','class="text_area"',$row->auto_approve); ?></td>
		</tr>
		<tr>
			<td align="left">Show Agent's Detail in Property Page:</td>
			<td align="left"><?php echo mosHTML::yesnoSelectList('show_agentdetails','class="text_area"',$row->show_agentdetails); ?></td>
		</tr>
		<tr>
			<td align="left">Show Enquiry Form in Property Page:</td>
			<td align="left"><?php echo mosHTML::yesnoSelectList('show_enquiryform','class="text_area"',$row->show_enquiryform); ?></td>
		</tr>
		<tr>
			<td align="left">Open Web Link in Seperate Window:</td>
			<td align="left"><?php echo mosHTML::yesnoSelectList('link_open_newwin','class="text_area"',$row->link_open_newwin); ?></td>
		</tr>
		<tr>
			<td align="left">Show 'More Information' in Property's Details:</td>
			<td align="left"><?php echo mosHTML::yesnoSelectList('show_moreinfo','class="text_area"',$row->show_moreinfo); ?></td>
		</tr>
		<tr>
			<td align="left">Show PDF Icon:</td>
			<td align="left"><?php echo mosHTML::yesnoSelectList('show_pdficon','class="text_area"',$row->show_pdficon); ?></td>
		</tr>
		<tr>
			<td align="left">Show Print Icon:</td>
			<td align="left"><?php echo mosHTML::yesnoSelectList('show_printicon','class="text_area"',$row->show_printicon); ?></td>
		</tr>
		<tr>
			<td align="left">Show E-mail Icon:</td>
			<td align="left"><?php echo mosHTML::yesnoSelectList('show_emailicon','class="text_area"',$row->show_emailicon); ?></td>
		</tr>
		<tr>
			<td align="left">Use Advanced Search:</td>
			<td align="left"><?php echo mosHTML::yesnoSelectList('use_advsearch','class="text_area"',$row->use_advsearch); ?></td>
		</tr>
		<tr><th colspan="2">Company & Agent</th></tr>
		<tr><td colspan="2" align="left" style="border-bottom: 1px solid #C0C0C0; background-color: #FFFFFF">Turning this Off will disable the use of 'Company' and 'Agent' feature. However, <u>you will need to create at least ONE Company and ONE Agent</u>. This is to allow you to use the agent's email address as the destination for enquiries and feedback form. You can switch back to use this feature again in the future.</td></tr>
		<tr>
			<td align="left">Use 'Company' & 'Agent' feature:</td>
			<td align="left"><?php echo mosHTML::yesnoSelectList('use_companyagent','class="text_area" onchange="javascript:if (document.forms[0].use_companyagent.options[selectedIndex].value==\'1\') { document.forms[0].default_agent.disabled=true;	 } else { document.forms[0].default_agent.disabled=false; }"',$row->use_companyagent); ?></td>
		</tr>
		<tr>
			<td align="left">Default Agent:</td>
			<td align="left"><?php echo $lists['default_agent']; ?></td>
		</tr>
		<tr><td colspan="2" align="left" style="border-top: 1px solid #C0C0C0; border-bottom: 1px solid #C0C0C0; background-color: #FFFFFF">This option is only applicable if you use Hot Property's User Registration hack (HP Hack #1). This hack allow you to auto assign new registered member as 'Agent' group. All new Agent will be assigned to the company specified below. Administrator can always re assign them to other company in the future.</td></tr>
		<tr>
			<td align="left">Default Company:</td>
			<td align="left"><?php echo $lists['default_company']; ?></td>
		</tr>
		<tr><th colspan="2">Currency format</th></tr>
		<tr>
			<td align="left">Currency:</td>
			<td align="left"><input class="text_area" type="text" name="currency" size="8" value="<?php echo $row->currency; ?>"></td>
		</tr>
		<tr>
			<td align="left">Use 1000 seperator (,):</td>
			<td align="left"><?php echo mosHTML::yesnoSelectList('thousand_sep','class="text_area"',$row->thousand_sep); ?></td>
		</tr>
		<tr>
			<td align="left">Thousand Seperator:</td>
			<td align="left"><input class="text_area" type="text" name="thousand_string" size="8" value="<?php echo $row->thousand_string; ?>"></td>
		</tr>
		<tr>
			<td align="left">Decimal Places:</td>
			<td align="left"><?php
        $dec_point[] = mosHTML::makeOption( '0', '0' );
        $dec_point[] = mosHTML::makeOption( '2', '2' );
        $dec_points = mosHTML::selectList( $dec_point, 'dec_point', 'class="text_area" size="1"', 'value', 'text', $row->dec_point );
        echo $dec_points;
      ?></td>
		</tr>
		<tr>
			<td align="left">Decimal Seperator:</td>
			<td align="left"><input class="text_area" type="text" name="dec_string" size="8" value="<?php echo $row->dec_string; ?>"></td>
		</tr>
    </table>
		<?php
		$tabs->endTab();
		$tabs->startTab("Prop. Listing","proplisting-page");
		?>
    <table cellpadding="2" cellspacing="4" border="0" width="100%" class="adminform">

		<tr>
			<td align="left" width="180">Default Ordering:</td>
			<td align="left" width="84%"><?php echo $lists['order']; ?> in <?php echo $lists['order2']; ?> order.</td>
		</tr>
		<tr>
			<td align="left">Allow user to set Display #:</td>
			<td align="left"><?php echo mosHTML::yesnoSelectList('use_diplaynum','class="text_area"',$row->use_diplaynum); ?></td>
		</tr>
		<tr>
			<td align="left">Show thumbnails:</td>
			<td align="left"><?php echo mosHTML::yesnoSelectList('show_thumb','class="text_area"',$row->show_thumb); ?></td>
		</tr>
		<tr><td colspan="2" align="left" style="border-top: 1px solid #C0C0C0; border-bottom: 1px solid #C0C0C0; background-color: #FFFFFF">You can use a particular sorting option ONLY if this field is displayed in "Listing".</td>
		</tr>
		<tr>
			<td align="left" valign="top">Sorting Options Available:</br></td>
			<td align="left">
				<table cellpadding="0" cellspacing="0" border="0" width="70%">
					<tr>
						<td width="33%"><input type="checkbox" name="use_sort_name" value="1"<?php echo ($row->use_sort_name) ? " checked" : ""; ?> /> <?php echo $captions['name']->caption; ?></td>
						<td width="33%"><input type="checkbox" name="use_sort_agent" value="1"<?php echo ($row->use_sort_agent) ? " checked" : ""; ?> /> <?php echo $captions['agent']->caption; ?></td>
						<td width="33%"><input type="checkbox" name="use_sort_price" value="1"<?php echo ($row->use_sort_price) ? " checked" : ""; ?> /> <?php echo $captions['price']->caption; ?></td>
					</tr>
					<tr>
						<td width="33%"><input type="checkbox" name="use_sort_suburb" value="1"<?php echo ($row->use_sort_suburb) ? " checked" : ""; ?> /> <?php echo $captions['suburb']->caption; ?></td>
						<td width="33%"><input type="checkbox" name="use_sort_state" value="1"<?php echo ($row->use_sort_state) ? " checked" : ""; ?> /> <?php echo $captions['state']->caption; ?></td>
						<td width="33%"><input type="checkbox" name="use_sort_country" value="1"<?php echo ($row->use_sort_country) ? " checked" : ""; ?> /> <?php echo $captions['country']->caption; ?></td>
					</tr>
					<tr>
						<td width="33%"><input type="checkbox" name="use_sort_type" value="1"<?php echo ($row->use_sort_type) ? " checked" : ""; ?> /> <?php echo $captions['type']->caption; ?></td>
						<td width="33%"><input type="checkbox" name="use_sort_modified" value="1"<?php echo ($row->use_sort_modified) ? " checked" : ""; ?> /> <?php echo $captions['modified']->caption; ?></td>
						<td width="33%"><input type="checkbox" name="use_sort_hits" value="1"<?php echo ($row->use_sort_hits) ? " checked" : ""; ?> /> <?php echo $captions['hits']->caption; ?></td>
					</tr>
				</table>
			</td>
		</tr>

		<tr><th colspan="2">Front Page</th></tr>
		<tr>
			<td align="left">Number of Featured Item in Frontpage:</td>
			<td align="left"><input class="text_area" type="text" name="fp_featured_count" size="5" value="<?php echo $row->fp_featured_count; ?>"></td>
		</tr>
		<tr>
			<td align="left">Show Featured Property:</td>
			<td align="left"><?php echo mosHTML::yesnoSelectList('fp_show_featured','class="text_area"',$row->fp_show_featured); ?></td>
		</tr>
		<tr>
			<td align="left">Show Search Form:</td>
			<td align="left"><?php echo mosHTML::yesnoSelectList('fp_show_search','class="text_area"',$row->fp_show_search); ?></td>
		</tr>
		<tr>
			<td align="left">Show Empty Types:</td>
			<td align="left"><?php echo mosHTML::yesnoSelectList('fp_show_empty','class="text_area"',$row->fp_show_empty); ?></td>
		</tr>
		<tr>
			<td align="left">Show Number of properties:</td>
			<td align="left"><?php echo mosHTML::yesnoSelectList('fp_show_nbr','class="text_area"',$row->fp_show_nbr); ?></td>
		</tr>
		<tr>
			<td align="left">Depth Level Limit:</td>
			<td align="left"><input class="text_area" type="text" name="fp_depth_count" size="5" value="<?php echo $row->fp_depth_count; ?>"><?php echo mosHTML::yesnoSelectList('fp_depth_calculous','class="text_area"',$row->fp_depth_calculous); ?>From depth limit, include properties in depth</td>
		</tr>

		<tr><th colspan="3">Items to show per page</th></tr>
		<tr>
			<td align="left">Default:</td>
			<td align="left"><input class="text_area" type="text" name="default_limit" size="5" value="<?php echo $row->default_limit; ?>"></td>
		</tr>
		<tr>
			<td align="left">Agent Page:</td>
			<td align="left"><input class="text_area" type="text" name="default_limit_agent" size="5" value="<?php echo $row->default_limit_agent; ?>"></td>
		</tr>
		<tr>
			<td align="left">Company Page:</td>
			<td align="left"><input class="text_area" type="text" name="default_limit_co" size="5" value="<?php echo $row->default_limit_co; ?>"></td>
		</tr>
		<tr>
			<td align="left">Search Page:</td>
			<td align="left"><input class="text_area" type="text" name="default_limit_search" size="5" value="<?php echo $row->default_limit_search; ?>"></td>
		</tr>
		<tr>
			<td align="left">Featured Page:</td>
			<td align="left"><input class="text_area" type="text" name="default_limit_featured" size="5" value="<?php echo $row->default_limit_featured; ?>"></td>
		</tr>

    </table>
		<?php
		$tabs->endTab();
		$tabs->startTab("Images","imagesettings-page");
		?>
    <table cellpadding="2" cellspacing="4" border="0" width="100%" class="adminform">

		<tr>
			<td align="left" width="160" valign="top">Thumbnail Creator:</td>
			<td align="left" width="85%"><?php
				if(!empty($imageLibs['gd1'])) { $thumbcreator[] = mosHTML::makeOption( 'gd1', 'GD Library '.$imageLibs['gd1'] ); }
        $thumbcreator[] = mosHTML::makeOption( 'gd2', 'GD2 Library '.$imageLibs['gd2'] );
				$thumbcreator[] = mosHTML::makeOption( 'netpbm', (isset($imageLibs['netpbm'])) ? $imageLibs['netpbm'] : "Netpbm" );
        $thumbcreator[] = mosHTML::makeOption( 'imagemagick', (isset($imageLibs['imagemagick'])) ? $imageLibs['imagemagick'] : "Imagemagick" );

				$thumbcreation = mosHTML::selectList( $thumbcreator, 'img_method', 'class="text_area" size="4"', 'value', 'text', $row->img_method );
        echo $thumbcreation;
      ?></td>
		</tr>
		<tr><td colspan="2" align="left" style="border-top: 1px solid #C0C0C0; border-bottom: 1px solid #C0C0C0; background-color: #FFFFFF">You may leave the path below blank. Unless you have problem using any of the libraries, you may enter the path provided by your host for Netpbm or Imagemagick libraries.</td>
		</tr>
		<tr>
			<td align="left" width="185">Netpbm path:</td>
			<td align="left"><input class="text_area" type="text" name="img_netpbmpath" size="35" value="<?php echo $row->img_netpbmpath; ?>">&nbsp; ie: /home/site/home/netpbm/</td>
		</tr>
		<tr>
			<td align="left" width="185">Imagemagick path:</td>
			<td align="left"><input class="text_area" type="text" name="img_impath" size="35" value="<?php echo $row->img_impath; ?>">&nbsp; ie: /usr/lib/php/</td>
		</tr>
		<tr><th colspan="3">Photos' Size</th></tr>
		<tr>
			<td align="left" width="185">Zoom:</td>
			<td align="left"><input class="text_area" type="text" name="imgsize_zoom" size="5" value="<?php echo $row->imgsize_zoom; ?>"> px</td>
		</tr>
		<tr>
			<td align="left" width="185">Standard:</td>
			<td align="left"><input class="text_area" type="text" name="imgsize_standard" size="5" value="<?php echo $row->imgsize_standard; ?>"> px</td>
		</tr>
		<tr>
			<td align="left" width="185">Thumb:</td>
			<td align="left"><input class="text_area" type="text" name="imgsize_thumb" size="5" value="<?php echo $row->imgsize_thumb; ?>"> px</td>
		</tr>
		<tr>
			<td align="left" width="185">Agent:</td>
			<td align="left"><input class="text_area" type="text" name="imgsize_agent" size="5" value="<?php echo $row->imgsize_agent; ?>"> px</td>
		</tr>
		<tr>
			<td align="left" width="185">Company:</td>
			<td align="left"><input class="text_area" type="text" name="imgsize_company" size="5" value="<?php echo $row->imgsize_company; ?>"> px</td>
		</tr>
		<tr><th colspan="3">Photos' Quality</th></tr>
		<tr>
			<td align="left" width="185">Property:</td>
			<td align="left"><input class="text_area" type="text" name="quality_photo" size="5" value="<?php echo $row->quality_photo; ?>"></td>
		</tr>
		<tr>
			<td align="left" width="185">Agent:</td>
			<td align="left"><input class="text_area" type="text" name="quality_agent" size="5" value="<?php echo $row->quality_agent; ?>"></td>
		</tr>
		<tr>
			<td align="left" width="185">Company:</td>
			<td align="left"><input class="text_area" type="text" name="quality_company" size="5" value="<?php echo $row->quality_company; ?>"></td>
		</tr>
	</table>
	<?php
	$tabs->endTab();
	$tabs->endPane();
	?>
  <input type="hidden" name="agent_usertype" value="<?php echo $row->agent_usertype; ?>">
  <input type="hidden" name="agent_groupid" value="<?php echo $row->agent_groupid; ?>">
  <input type="hidden" name="show_guide" value="<?php echo $row->show_guide; ?>">
  <input type="hidden" name="version" value="<?php echo $row->version; ?>">
  <input type="hidden" name="option" value="<?php echo $option; ?>">
  <input type="hidden" name="task" value="">
</form>
	<?php
	}

/**********************
 * About Hot Property *
 **********************/
	function showAbout() {
	?>
	<center><img width="153" height="103" src="../components/com_hotproperty/img/hp_logo.png" alt="Hot Property"></center>
	<br />
	<table width="50%" border="0" align="center" cellpadding="2" cellspacing="0" class="adminform">
		<tr>
			<th colspan="2">Hot Property</th>
		</tr>
		<tr>
			<td width="20%" align="right">Version:</td>
			<td width="80%" align="left"><?php echo hp_version; ?></td>
		</tr>
		<tr>
			<td width="20%" align="right">Website:</td>
			<td width="80%" align="left"><a target="_blank" href="http://www.mosets.com/">http://www.mosets.com/</a></td>
		</tr>
		<tr>
			<td width="20%" align="right">Author:</td>
			<td width="80%" align="left">CY Lee</td>
		</tr>
		<tr>
			<td width="20%" align="right">E-mail:</td>
			<td width="80%" align="left"><a href="mailto:hotproperty@mosets.com">hotproperty@mosets.com</a></td>
		</tr>
		<tr>
			<td width="20%" align="right">License:</td>
			<td width="80%" align="left"><a href="index2.php?option=com_hotproperty&task=license">License Agreement</a></td>
		</tr>
		<tr>
			<td colspan="2" align="left"><strong>Credits</strong></td>
		</tr>
		<tr>
			<td colspan="2" align="left">
				<ul>
				<li>Alexandru Bulat</a> of <a href="http://pxdesign.com/" target="_blank">Px Design Solutions</a></li>
				<li><a href="http://www.joomla.org/content/blogcategory/43/85/" target="_blank">Developers</a> of <a href="http://www.joomla.org/" target="_blank">Joomla</a></li>
				</ul>
			</td>
		</tr>

	</table>
	<br />
	<center>Lee Cher Yeong &copy; 2004-2006 All rights reserved. Hot Property is a Proprietary software.<br />Please read the <a href="index2.php?option=com_hotproperty&task=license">license agreement</a> before using this product</center>
	<?php
	}
}
?>
