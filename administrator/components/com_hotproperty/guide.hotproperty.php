<?php
// $Id: guide.hotproperty.php
/**
* Hot Property guide
*
* @package Hot Property 0.9
* @copyright (C) 2004 Lee Cher Yeong
* @url http://www.Mosets.com/
* @author Lee Cher Yeong <cy@mosets.com>
**/

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

global $database, $my;

# Get number of property types
$database->setQuery( "SELECT count(*) FROM #__hp_prop_types" );
$total["types"] = $database->loadResult();

# Get number of companies
$database->setQuery( "SELECT count(*) FROM #__hp_companies" );
$total["companies"] = $database->loadResult();

# Get number of agents
$database->setQuery( "SELECT count(*) FROM #__hp_agents" );
$total["agents"] = $database->loadResult();

#Get number of published types
if ($total["types"] > 0) {
	$database->setQuery( "SELECT count(*) FROM #__hp_prop_types WHERE published=1" );
	$published["types"] = $database->loadResult();
} else {
	$published["types"] = 0;
}

# Get number of properties
$database->setQuery( "SELECT count(*) FROM #__hp_properties" );
$total["properties"] = $database->loadResult();

/*********
 * STEP 1 - Adding Types, Companies and Agents
 *********/
if ($total["types"] <= 0 || $total["agents"] <= 0 || $total["companies"] <= 0) {
?>
		<center>
		<fieldset class="menubackgr" style="padding: 10px; text-align: left"><legend><strong>Property Guide</strong></legend>
		Hi <?php echo $my->username; ?>!
		<p />
		Thank you for choosing Hot Property. This is Hot Property's Guide and we hope to guide you in a simple and easy way to start using Hot Property in 4 simple steps! You may take your time setting up and exploring Hot Property in each step. Once you have completed all the tasks for each steps, you will be moved to the next step.
		<p />
		<strong><u>STEP 1</u></strong><br />
		Ok, let's start the guide. Right now, you may not be able to insert new property yet. You must do the following:
		<ol style="margin-left: 25px; padding: 0px;">
			<?php if ($total["types"] <= 0) { ?> <li><a href="index2.php?option=com_hotproperty&task=newprop_type">Create one property type</a></li><?php } ?>
			<?php if ($total["companies"] <= 0) { ?> <li><a href="index2.php?option=com_hotproperty&task=newcompany">Create one company</a></li><?php } ?>
			<?php if ($total["agents"] <= 0) { ?> <li><a href="index2.php?option=com_hotproperty&task=newagent">Create one agent</a></li><?php } ?>
		</ol>
		</fieldset>
		</center>
<?php
/*********
 * STEP 2 - Publish Types
 *********/
} elseif ($published["types"] <= 0 && $total["types"] > 0) {
?>
		<center>
		<fieldset class="menubackgr" style="padding: 10px; text-align: left"><legend><strong>Hot Property's Guide</strong></legend>
		<strong><u>STEP 2</u></strong><br />
		You do not have any published property in your website because there is no <a href="index2.php?option=com_hotproperty&task=listprop_type">published type</a>. Please note that in order to show a property in your website, you must publish the property <i>AND</i> its type.
		<ul style="margin-left: 25px; padding: 0px;">
			<li><a href="index2.php?option=com_hotproperty&task=listprop_type">Go to Manage Types and publish at least one property type.</a></li>
		</ul>
		</fieldset>
		</center>
<?php
/*********
 * STEP 3 - Setting up the configuration and adding the first property.
 *********/
} elseif ($total["properties"] <= 0) {
?>
		<center>
		<fieldset class="menubackgr" style="padding: 10px; text-align: left"><legend><strong>Hot Property's Guide</strong></legend>
		<strong><u>STEP 3</u></strong><br />
		Now that you have set up all the nescessary item, you may want to head over to the <a href="index2.php?option=com_hotproperty&task=config">configuration page</a> to configure some of the available settings. ie: Image library, Thumnbnail size etc. After that you may <a href="index2.php?option=com_hotproperty&task=newproperty">start adding your properties</a>!
		</fieldset>
		</center>
<?php
/*********
 * STEP 4 - DONE. More Information
 *********/
} elseif ($total["properties"] == 0) {
?>
		<center>
		<fieldset class="menubackgr" style="padding: 10px; text-align: left"><legend><strong>Hot Property's Guide</strong></legend>
		<strong><u>STEP 4</u></strong><br />
		Great! You have successfully added your first property with Hot Property(HP)! Keep in mind that HP has a built-in approval system and therefore you must manually approve every property before you publish it. If it is not approved, you will see an '[Approve]' link in the Property Manager(View Properties). Click on the link to approve the property.
		<p />
		Hot Property Guide will end here (it will dissapear after you add another property). If you have any questions while using Hot Property, head over to <a href="http://www.mosets.com/">Mosets.com</a> or send an e-mail listed on the <a href="index2.php?option=com_hotproperty&task=about">About page</a>.
		<p />
		Thank you for using Hot Property!
		</fieldset>
		</center>
<?php
}

?>