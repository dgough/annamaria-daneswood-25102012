<?php
// $Id: toolbar.hotproperty.php
/**
* Hot Property toolbar
*
* @package Hot Property 0.9
* @copyright (C) 2004 Lee Cher Yeong
* @url http://www.Mosets.com/
* @author Lee Cher Yeong <cy@mosets.com>
**/

// ensure this file is being included by a parent file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

require_once( $mainframe->getPath( 'toolbar_html' ) );
require_once( $mainframe->getPath( 'toolbar_default' ) );

switch ($task) {

	# Company Events
	case "newcompany":
		menuhotproperty::COMPANY_NEW();
		break;
	case "editcompany":
		menuhotproperty::COMPANY_EDIT();
		break;
	case "listcompany":
		menuhotproperty::COMPANY_LIST();
		break;

	# Agent Events
	case "newagent":
		menuhotproperty::AGENT_NEW();
		break;
	case "editagent":
		menuhotproperty::AGENT_EDIT();
		break;
	case "listagent":
		menuhotproperty::AGENT_LIST();
		break;

	# Property Events
	case "newproperty":
		menuhotproperty::PROPERTY_NEW();
		break;
	case "editproperty":
		menuhotproperty::PROPERTY_EDIT();
		break;
	case "listarchived":
		menuhotproperty::PROPERTY_ARCHIVED_LIST();
		break;
	case "listproperty":
		menuhotproperty::PROPERTY_LIST();
		break;

	# Property Type Events
	case "newprop_type":
		menuhotproperty::PROP_TYPE_NEW();
		break;
	case "editprop_type":
		menuhotproperty::PROP_TYPE_EDIT();
		break;
	case "listprop_type":
		menuhotproperty::PROP_TYPE_LIST();
		break;

	# Extra Fields Events
	case "newprop_ef":
		menuhotproperty::PROP_EF_NEW();
		break;
	case "editprop_ef":
		menuhotproperty::PROP_EF_EDIT();
		break;
	case "listprop_ef":
		menuhotproperty::PROP_EF_LIST();
		break;

	# Featured Events
	case "featured":
		menuhotproperty::PROP_FEATURED_LIST();
		break;
	# Config Event
	case "config":
		menuhotproperty::CONFIG_MENU();
		break;

	# Statistics
	case "log_searches":
		break;

	# Report
	case "report":
		menuhotproperty::REPORT_MENU();		
		break;
	case "doreport":
		menuhotproperty::DOREPORT_MENU();		
		break;
	case "doreport_excel":
		menuhotproperty::DOREPORT_EXCEL_MENU();	
		break;

	# About
	case "about":
		break;
	case "license":
		menuhotproperty::LICENSE_MENU();	
		break;

	# List property Events
	default:
		menuhotproperty::PROPERTY_LIST();
		break;

}
?>