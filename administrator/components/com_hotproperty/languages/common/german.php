<?php

# Main
DEFINE ('_HP_COM_TITLE','Immobilienliste');
DEFINE ('_HP_MORE','Mehr…');
DEFINE ('_HP_MOREFEATURED','Noch mehr Immobilienangebote…');

DEFINE ('_HP_COMPANY','Firma');
DEFINE ('_HP_CONTACTNUMBER','Kontakt-Nummer');
DEFINE ('_HP_ADDRE&#223;','Adre&#223;e');
DEFINE ('_HP_PROPERTY','Immobilie');
DEFINE ('_HP_PROPERTIES','Immobilien');
DEFINE ('_HP_ARROW','&gt;');
DEFINE ('_HP_SEARCH','Suche');
DEFINE ('_HP_SEARCH_ADV','Erweiterte Suche');
DEFINE ('_HP_IN','in');
DEFINE ('_HP_PHOTO', 'Foto');
DEFINE( '_HP_EXTRAFIELDS', 'Zusatzfelder');
DEFINE( '_HP_PHOTOS', 'Fotos');
DEFINE( '_HP_PUBLISHING', 'Herausgabe');
DEFINE ('_HP_TYPE', 'Type');
DEFINE ('_HP_SUBURB', 'Vorort');
DEFINE ('_HP_STATE', 'Staat');
DEFINE ('_HP_COUNTRY', 'Land');
DEFINE ('_HP_POSTCODE', 'Postleitzahl');
DEFINE ('_HP_PRICE', 'Preis');
DEFINE ('_HP_FEATURED', 'Angeboten');
DEFINE ('_HP_MOVEUP', 'Nach oben');
DEFINE ('_HP_MOVEDOWN', 'Nach unten');
DEFINE ('_HP_ANYTIME','Jederzeit');
DEFINE ('_HP_R&#223;FEED', 'Kriegen Sie die neuesten Immobilien direkt auf Ihren Desktop');
DEFINE ('_HP_COM_PATHWAY_MAIN', 'Hauptseite');
DEFINE ('_HP_RESET', 'Zur&#220;cksetzen');

# Sort
DEFINE ('_HP_SORT_BY','Sortiert nach:');
DEFINE ('_HP_SORT_LOWEST_PRICE','Preis: G&#220;nstig bis Hoch');
DEFINE ('_HP_SORT_HIGHEST_PRICE','Preis: Hoch bis G&#220;nstig');
DEFINE ('_HP_SORT_PRICE','Preis');
DEFINE ('_HP_SORT_AZ','AZ');
DEFINE ('_HP_SORT_A_Z','A-Z');
DEFINE ('_HP_SORT_Z_A','Z-A');
DEFINE ('_HP_SORT_ASC','&lt;&lt;');
DEFINE ('_HP_SORT_DESC','&gt;&gt;');

# Featured
DEFINE ('_HP_FEATURED_EMPTY','Keine Immobilien im Angebot');
DEFINE ('_HP_FEATURED_TITLE','Angeboten');

# Types
DEFINE ('_HP_TYPES_TITLE','Immobilien Typen');

#View
DEFINE ('_HP_VIEW_AGENT_TITLE','Agent');
DEFINE ('_HP_VIEW_FEATURES_TITLE','Mehr Information');
DEFINE ('_HP_VIEW_AGENT_CONTACT','Anfrage senden / Einen Termin machen');
DEFINE ('_HP_SENDENQUIRY','Anfrage senden');

# Search
DEFINE ('_HP_SEARCH_TITLE','Suche');
DEFINE ('_HP_SEARCH_ADV_TITLE','Erweiterte Suche');
DEFINE ('_HP_SEARCH_RESULT_TITLE','Suchresultate');
DEFINE ('_HP_SEARCH_TEXT','Immobiliensuche:');
DEFINE ('_HP_SEARCH_SELECTAGENT', 'Agent w&#196;hlen');
DEFINE ('_HP_SEARCH_SELECTCOMPANY', 'Firma w&#196;hlen');
DEFINE ('_HP_SEARCH_FROM','Von ');
DEFINE ('_HP_SEARCH_TO','Bis ');
DEFINE ('_HP_SEARCH_ALLTYPES','Alle Typen');
DEFINE ('_HP_SEARCH_ALLAGENTS','Alle Agenten');
DEFINE ('_HP_SEARCH_ALLCOMPANIES', 'Alle Firmen');
DEFINE ('_HP_SEARCH_EXACTLY', 'Genau');
DEFINE ('_HP_SEARCH_MORE_THAN', 'Mehr als');
DEFINE ('_HP_SEARCH_LE&#223;_THAN', 'Weniger als');
DEFINE ('_HP_SEARCH_NOSEARCH','Sie haben keine Suche eingegeben. Bitte Suche eingeben und wieder probieren.');
DEFINE ('_HP_SEARCH_NOTVALID','Sie haben keine g&#220;ltige Suche eingegeben. Bitte Suche eingeben und wieder probieren.');
DEFINE ('_HP_SEARCH_NORESULT','Ihre Suche brachte keine Ergebni&#223;e. Bitte wieder probieren, oder werfen Sie einen Blick auf die Immobilienangebote unten.');
DEFINE ('_HP_SEARCH_TRYAGAIN', 'Wieder probieren');
DEFINE ('_HP_ADVANCED_SEARCH','Erweiterte Suche');

DEFINE ('_HP_SEARCH_BY_NAME', 'Suche nach Namen');

DEFINE ('_HP_SEARCH_HEADING', 'Immobiliensuche');
DEFINE ('_HP_SEARCH_SEARCHED_FOR', 'Sie suchten nach:');
DEFINE ('_HP_SEARCH_MORE_LINK', 'Mehr');
DEFINE ('_HP_SEARCH_PRICE_BAND', 'Preiskla&#223;e');

# Agent
DEFINE ('_HP_PROPBYAGENT','Immobilien angeboten durch ');
DEFINE ('_HP_AGENT_SENDEMAIL','Senden Sie eine Mail an diesen Agent');
DEFINE ('_HP_AGENT_COMPANY','Firma');
DEFINE ('_HP_AGENT_MOBILE','Handy');
DEFINE ('_HP_AGENT_ERROR_EMPTY','Kein Agent');

# Company
DEFINE ('_HP_CO_TITLE','Firma');
DEFINE ('_HP_CO_SENDEMAIL','Senden Sie eine Mail an diese Firma');
DEFINE ('_HP_CO_CONTACT','Anfrage / Feedback senden');
DEFINE ('_HP_CO_WEBSITE', 'Website:');

#Property
DEFINE ('_HP_PROP_ERROR_EMPTY','Keine Immobilie vorhanden');
DEFINE ('_HP_SELECT_PROP_TYPE','Immobilien Type w&#196;hlen');


DEFINE ('_HP_PROP_RETURN','Zur&#220;ck zu den Angeboten');
DEFINE ('_HP_PROP_ENLARGE_PHOTO','[ Foto vergr&#214;&#223;ern ]');
DEFINE ('_HP_PROP_KEY_FEATURES','Haupteigenschaften');


DEFINE ('_HP_PROP_TAB_GENERAL_INFO','Allgemeine Information');
DEFINE ('_HP_PROP_TAB_LIVING_AREA','Wohnfl&#196;che');
DEFINE ('_HP_PROP_TAB_KITCHEN','K&#220;che');
DEFINE ('_HP_PROP_TAB_BEDROOM','Schlafzimmer');
DEFINE ('_HP_PROP_TAB_OUTSIDE','Drau&#223;en');
DEFINE ('_HP_PROP_TAB_MAP','Plan');
DEFINE ('_HP_PROP_TAB_BATHROOM','Badezimmer');
DEFINE ('_HP_PROP_TAB_VIDEO', 'Video');
DEFINE ('_HP_PROP_TAB_COMMENTS', 'Bewertungen');

# Contact
DEFINE ('_HP_CONTACT_ERR_COMPLETE','Bitte Formular ausf&#220;llen vor dem Absenden.');
DEFINE ('_HP_CONTACT_ERR_ONECONTACT','Bitte zumindest eine Kontaktm&#214;glichkeit geben (Mail/Kontaktnummer), so da&#223; unserer Agent Sie direkt kontaktieren kann.');
DEFINE ('_HP_CONTACT_ERR_VALIDEMAIL','Bitte eine g&#220;ltige Mail Adre&#223;e eingeben.');
DEFINE ('_HP_CONTACT_THANK','Danke f&#220;r Ihre Anfrage. Unserer Agent wird Sie so bald wie m&#214;glich kontaktieren.');
DEFINE ('_HP_CONTACT_ENQUIRY_TEXT','Dies ist eine Anfrage von ');
DEFINE ('_HP_CONTACT_ENQUIRY_TEXT2',"------\n\n Immobilieninfo bei der Adre&#223;e unten:");
DEFINE ('_HP_CONTACT_ENQUIRY_SUBJECTP','Immobilienanfrage - ');
DEFINE ('_HP_CONTACT_ENQUIRY_SUBJECTA','Agentenanfrage');
DEFINE ('_HP_CONTACT_ENQUIRY_SUBJECTC','Firmenanfrage');

# Agent Menu
DEFINE ('_HP_MENU_MANAGEPROPERTY', 'Ansicht meiner Immobilien');
DEFINE ('_HP_MENU_MANAGEPROPERTY2', 'Verwaltung meiner Immobilien');
DEFINE ('_HP_MENU_NOTAVAILABLE', 'Nicht vorhanden');
DEFINE ('_HP_MENU_EDITAGENT' , 'Mein Agentprofil');
DEFINE ('_HP_MENU_ADDPROPERTY' , 'Immobilie hinzuf&#220;gen'); 

# Edit Agent
DEFINE ('_HP_EDITAGENT', 'Mein Agentprofil');
DEFINE ('_HP_AGENT_NAME', 'Agentenname: ');
DEFINE ('_HP_AGENT_REMOVEPHOTO', 'Foto entfernen');
DEFINE ('_HP_AGENT_NOPHOTO','Kein Foto');
DEFINE ('_HP_AGENT_MSG_01', 'Ihr Agentenprofil wurde erfolgreich gespeichert!');

# Edit Property
DEFINE ('_HP_EDITPROP_SAVEINSERTPHOTO', 'Fotos speichern & einf&#220;gen >>');
DEFINE ('_HP_EDITPROP_PROPERTYNAME', 'Immobilienname:');
DEFINE ('_HP_EDITPROP_INTROTEXT', 'Introtext');
DEFINE ('_HP_EDITPROP_FULLTEXT', 'Volltext');
DEFINE ('_HP_EDITPROP_NOTES', 'Anmerkungen');
DEFINE ('_HP_EDITPROP_NOTES_MSG', '* Anmerkungen werden vom Endbenutzer verborgen');
DEFINE ('_HP_EDITPROP_MSG_01', 'Ihre Immobilie wurde erfolgreich gespeichert!');
DEFINE ('_HP_EDITPROP_MSG_02', 'Warten auf Genehmigung');
DEFINE ('_HP_EDITPROP_MSG_03', 'Ihre Se&#223;ion ist abgelaufen. Bitte wieder einloggen bevor Sie eine neue Immobilie einreichen.');
DEFINE ('_HP_EDITPROP_NOPUBLISHEDEF', 'Keine zus&#196;tzlichen Felder Ver&#214;ffentlicht');

# Image Management
DEFINE ('_HP_IMGEDIT_TITLE', 'Foto bearbeiten');
DEFINE ('_HP_IMGEDIT_CHANGEPHOTO', 'Foto &#196;ndern :');
DEFINE ('_HP_IMGEDIT_A&#223;IGNEDPHOTOS', 'Zugewiesene Fotos');

DEFINE ('_HP_IMGUPLOAD_TITLE', 'Neues Foto uploaden');
DEFINE ('_HP_IMGUPLOAD_PHOTO', 'Foto:');
DEFINE ('_HP_IMGUPLOAD_SUBMIT', 'Foto uploaden');
DEFINE ('_HP_IMGUPLOAD_ERR_01', 'Bitte sicherstellen da&#223; Sie ein Bild angegeben haben vor dem Uploaden.');
DEFINE ('_HP_IMGUPLOAD_ERR_02', 'Bitte sicherstellen da&#223; Sie ein g&#220;ltiges Bild angegeben haben vor dem Uploaden.');
DEFINE ('_HP_IMGUPLOAD_ERR_03', 'Nur Dateientypen gif, png, jpg oder bmp kann man uploaden.');
DEFINE ('_HP_IMGUPLOAD_ERR_04', 'Eine Datei mit dem gleichen Namen ist bereits vorhanden. Bitte umbenennen und wieder uploaden.');
DEFINE ('_HP_IMGUPLOAD_ERR_05', 'Fehler beim L&#214;schen des alten Fotos');
DEFINE ('_HP_IMGUPLOAD_ERR_06', 'Fehler beim L&#214;schen des originalen \ alten Fotos');
DEFINE ('_HP_IMGUPLOAD_ERR_07', 'Fehler beim L&#214;schen des normalen \ alten Fotos');
DEFINE ('_HP_IMGUPLOAD_ERR_08', 'Fehler beim L&#214;schen des Thumbnails \ alten Fotos');
DEFINE ('_HP_IMGUPLOAD_ERR_09', 'Fehler beim L&#214;schen des Fotos');

DEFINE ('_HP_IMGEDIT_MSG_01', 'Sind Sie sicher da&#223; Sie das ausgew&#196;hlte Foto l&#214;schen wollen?');
DEFINE ('_HP_IMGEDIT_MSG_02', 'Noch kein Bild f&#220;r diese Immbolie.');
DEFINE ('_HP_IMGEDIT_MSG_03', 'Klicken um das Foto zu bearbeiten');

DEFINE ('_HP_IMGEDIT_BTN_DELPHOTO', 'Foto l&#214;schen');
DEFINE ('_HP_IMGEDIT_BTN_DELPHOTO2', '[Foto l&#214;schen]');
?>
