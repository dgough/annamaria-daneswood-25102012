<?php
// $Id: admin.hotproperty.php
/**
* Hot Property admin 
*
* @package Hot Property 0.9
* @copyright (C) 2004 Lee Cher Yeong
* @url http://www.Mosets.com/
* @author Lee Cher Yeong <cy@mosets.com>
**/

# ensure this file is being included by a parent file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

require_once( $mainframe->getPath( 'admin_html' ) );
require_once( 'components/com_hotproperty/admin.hotproperty.class.php' );
require_once( 'components/com_hotproperty/function.hotproperty.php' );
require_once( 'components/com_hotproperty/config.hotproperty.php' );

error_reporting(E_ALL);
ini_set('display_errors','On');

# Generate list of Extra Field's type
$ef_types = array 
(
  '0' => 'Select Field Types',
  'text' => 'One Line Textbox',
  'multitext' => 'Multi Line Textbox',
  'wysiwyg' => 'WYSIWYG',
  'selectlist' => 'Drop Down Menu',
  'selectmultiple' => 'Multiple Select List',
  'checkbox' => 'Checkbox',
  'radiobutton' => 'Radio Button',
  'link' => 'Web Link'
);

//	/* 
$search_types = array
(
	'0'			=> 'Default',
	'range_1'	=> 'Less/More Than X',
	'range_2'	=> 'From X to Y'
//	'text' => 'One Line Textbox',
//  'selectlist' => 'Drop Down Menu',
//  'checkbox' => 'Checkbox',
//	'radiobutton' => 'Radio Button',
); 
//	*/

$ef_types_need_elements = array('selectlist', 'selectmultiple', 'checkbox', 'radiobutton');

# Get IDs
$cid = mosGetParam( $_REQUEST, 'id', array(0) );

# This var get the same value as $cid BUT we assume this is strictly integer than array
$property = intval(mosGetParam( $_REQUEST, 'id', 0 ));

if (!is_array( $cid )) {
	$cid = array(0);
}

if (substr($task,-5) == "image" || $task == "doreport_print") {
	HTML_hotproperty::listprop_starthtml_image();
//}	elseif($task == "doreport_excel") {
	// do nothing
} else {
	HTML_hotproperty::print_startmenu( $task );
}

switch ($task) {

	#-------------------------
	# Company Events
	#-------------------------
	case "newcompany":
		editcompany(0,$option);
		break;
	case "editcompany":
		editcompany($cid[0],$option);
		break;
	case "savecompany":
		mosCache::cleanCache( 'com_hotproperty' );
		savecompany($option);
		break;
	case "removecompany":
		mosCache::cleanCache( 'com_hotproperty' );
		removecompany($cid,$option);
		break;
	case "cancelcompany":
		cancelcompany($option);
		break;
	case "listcompany":
		listcompany($option);
		break;

	#-------------------------
	# Agent Events
	#-------------------------
	case "newagent":
		editagent(0,$option);
		break;
	case "editagent":
		editagent($cid[0],$option);
		break;
	case "saveagent":
		mosCache::cleanCache( 'com_hotproperty' );
		saveagent($option);
		break;
	case "removeagent":
		mosCache::cleanCache( 'com_hotproperty' );
		removeagent($cid,$option);
		break;
	case "cancelagent":
		cancelagent($option);
		break;
	case "listagent":
		listagent($option);
		break;

	#-------------------------
	# Property Events
	#-------------------------
	case "newproperty":
		editproperty(0,$option);
		break;
	case "editproperty":
		if (is_array($cid) && $cid[0])	editproperty($cid[0],$option, '0' );
		else editproperty(mosGetParam( $_REQUEST, 'id', 0 ),$option, mosGetParam( $_REQUEST, 'tab', '0' )); // In use when user creates new property and proceed to save and insert the first image.
		break;
	case "saveproperty":
		mosCache::cleanCache( 'com_hotproperty' );
		saveproperty($option);
		break;
	# Pour rafra�chir les extra-fields � l'update du type de la propri�t�:
	case "updateproperty":
		mosCache::cleanCache( 'com_hotproperty' );
		saveproperty($option, '0');
		break;
	case "saveproperty_image_new":
		# Save and Edit again - For new record
		mosCache::cleanCache( 'com_hotproperty' );
		saveproperty($option, 'newimage');
		break;
	case "publish_property":
		mosCache::cleanCache( 'com_hotproperty' );
		publish_property($cid, 1, $option);
		break;
	case "unpublish_property":
		mosCache::cleanCache( 'com_hotproperty' );
		publish_property($cid, 0, $option);
		break;
	case "archive_property":
		mosCache::cleanCache( 'com_hotproperty' );
		publish_property($cid, -1, $option);
		break;
	case "unarchive_property":
		mosCache::cleanCache( 'com_hotproperty' );
		publish_property($cid, 0, $option);
		break;
	case "feature_property":
		mosCache::cleanCache( 'com_hotproperty' );
		feature_property($cid, 1, $option);
		break;
	case "unfeature_property":
		mosCache::cleanCache( 'com_hotproperty' );
		feature_property($cid, 0, $option);
		break;
	case "removeproperty":
		mosCache::cleanCache( 'com_hotproperty' );
		removeproperty($cid,$option);
		break;
	case "approve_property":
		mosCache::cleanCache( 'com_hotproperty' );
		approve_property($cid, 1, $option);
		break;
	case "cancelproperty":
		cancelproperty($option);
		break;
	case "listarchived":
		listarchived($option);
		break;
	case "listproperty":
		listproperty($option);
		break;

	#-------------------------
	# Image Management - under Property event
	#-------------------------
	case "listprop_image":
		listprop_showupload_image();
		break;
	case "listprop_upload_image":
		mosCache::cleanCache( 'com_hotproperty' );
		listprop_upload_image();
		break;
	case "listprop_edit_image":
		listprop_edit_image();
		break;
	case "listprop_delete_image":
		mosCache::cleanCache( 'com_hotproperty' );
		listprop_delete_image();
		break;
	case "orderup_images":
		mosCache::cleanCache( 'com_hotproperty' );
		listprop_order_image(-1, $option);
		break;
	case "orderdown_images":
		mosCache::cleanCache( 'com_hotproperty' );
		listprop_order_image(1, $option);
		break;
	case "listprop_edit_images":
		listprop_edit_images($option);
		break;

	#-------------------------
	# Property Type Events
	#-------------------------
	case "newprop_type":
		editprop_type(-1,$option);
		break;
	case "editprop_type":
		editprop_type($cid[0],$option);
		break;
	case "saveprop_type":
		mosCache::cleanCache( 'com_hotproperty' );
		saveprop_type($option);
		break;
	case "publish_prop_type":
		mosCache::cleanCache( 'com_hotproperty' );
		publish_prop_type($cid, 1, $option);
		break;
	case "unpublish_prop_type":
		mosCache::cleanCache( 'com_hotproperty' );
		publish_prop_type($cid, 0, $option);
		break;
	case "removeprop_type":
		mosCache::cleanCache( 'com_hotproperty' );
		removeprop_type($cid,$option);
		break;
	case "cancelprop_type":
		cancelprop_type($option);
		break;
	case "orderup_type":
		mosCache::cleanCache( 'com_hotproperty' );
		order_type( $cid[0], -1, $option );
		break;
	case "orderdown_type":
		mosCache::cleanCache( 'com_hotproperty' );
		order_type( $cid[0], 1, $option );
		break;
	case "listprop_type":
		listprop_type($option);
		break;

	#-------------------------
	# Extra Field(ef) Events
	#-------------------------
	case "newprop_ef":
		editprop_ef(0,$option);
		break;
	case "editprop_ef":
		editprop_ef($cid[0],$option);
		break;
	case "saveprop_ef":
		mosCache::cleanCache( 'com_hotproperty' );
		saveprop_ef($option);
		break;
	case "publish_prop_ef":
		mosCache::cleanCache( 'com_hotproperty' );
		publish_prop_ef($cid, 1, $option);
		break;
	case "unpublish_prop_ef":
		mosCache::cleanCache( 'com_hotproperty' );
		publish_prop_ef($cid, 0, $option);
		break;
	case "togglefeatured_ef":
		mosCache::cleanCache( 'com_hotproperty' );
		togglefeatured_ef($cid[0], $option);
		break;
	case "togglelisting_ef":
		mosCache::cleanCache( 'com_hotproperty' );
		togglelisting_ef($cid[0], $option);
		break;
	case "removeprop_ef":
		mosCache::cleanCache( 'com_hotproperty' );
		removeprop_ef($cid,$option);
		break;
	case "cancelprop_ef":
		cancelprop_ef($option);
		break;
	case "orderup_ef":
		mosCache::cleanCache( 'com_hotproperty' );
		order_ef( $cid[0], -1, $option );
		break;
	case "orderdown_ef":
		mosCache::cleanCache( 'com_hotproperty' );
		order_ef( $cid[0], 1, $option );
		break;
	case "listprop_ef":
		listprop_ef($option);
		break;

	#-------------------------
	# Featured Items
	#-------------------------
	case "featured":
		listfeatured( $option );
		break;
	case "orderup_featured":
		mosCache::cleanCache( 'com_hotproperty' );
		order_featured( $cid[0], -1, $option );
		break;
	case "orderdown_featured":
		mosCache::cleanCache( 'com_hotproperty' );
		order_featured( $cid[0], 1, $option );
		break;
	case "publish_featured":
		mosCache::cleanCache( 'com_hotproperty' );
		publish_featured($cid, 1, $option);
		break;
	case "unpublish_featured":
		mosCache::cleanCache( 'com_hotproperty' );
		publish_featured($cid, 0, $option);
		break;
	case "remove_featured":
		mosCache::cleanCache( 'com_hotproperty' );
		remove_featured($cid, $option);
		break;

	#-------------------------
	# Log Searches
	#-------------------------
	case "log_searches":
		log_searches($option);
		break;

	#-------------------------
	# Reporting
	#-------------------------
	case "report":
		report($option);
		break;
	case "doreport":
		doreport($option);
		break;
	case "doreport_print":
		doreport_print($option);
		break;
	case "doreport_excel":
		doreport_excel($option);
		break;

	#-------------------------
	# Configuration
	#-------------------------
	case "config":
		editConfig($option);
		break;
	case "saveConfig":
		mosCache::cleanCache( 'com_hotproperty' );
		saveConfig($option);
		break;

	#-------------------------
	# About Hot Property
	#-------------------------
	case "about":
		HTML_hotproperty::showAbout();
		break;
	case "license":
		include( "license.hotproperty.php" );
		break;

	#-------------------------
	# Cache
	#-------------------------
	case "clean_cache":
		mosCache::cleanCache( 'com_hotproperty' );
		mosRedirect( "index2.php?option=$option", "Cache cleaned" );
		break;

	#-------------------------
	# Default
	#-------------------------
	default:
		listproperty($option);

}

if (substr($task,-5) == "image") {
	listprop_image($option);
	HTML_hotproperty::listprop_endhtml_image();
} elseif ($task == "doreport_print") {
	HTML_hotproperty::listprop_endhtml_image();
//} elseif ($task == "doreport_excel") {
	// do nothing
}	else {
	HTML_hotproperty::print_endmenu();
}

/***********
 * Company *
 ***********/

function listcompany( $option ) {
	global $database, $mainframe;

	$limit = $mainframe->getUserStateFromRequest( "viewlistlimit", 'limit', 10 );
	$limitstart = $mainframe->getUserStateFromRequest( "viewcli{$option}limitstart", 'limitstart', 0 );

	# get the total number of records
	$database->setQuery( "SELECT count(*) FROM #__hp_companies" );
	$total = $database->loadResult();
	echo $database->getErrorMsg();

	require_once("includes/pageNavigation.php");
	$pageNav = new mosPageNav( $total, $limitstart, $limit );

	$sql = "SELECT c.*, COUNT(DISTINCT a.id) as agents, count(DISTINCT p.id) as properties, sum(p.hits) as hits"
		. "\nFROM #__hp_companies AS c"
		. "\nLEFT JOIN #__hp_agents AS a ON c.id=a.company"
		. "\nLEFT JOIN #__hp_properties AS p ON a.id=p.agent"
		. "\nGROUP BY c.id"
		. "\nORDER BY c.name ASC"
		. "\nLIMIT $pageNav->limitstart,$pageNav->limit";

	$database->setQuery($sql);

	if(!$result = $database->query()) {
		echo $database->stderr();
		return false;
	}
	$rows = $database->loadObjectList();

	HTML_hotproperty::listcompany( $rows, $pageNav, $option );
}

function editcompany( $id, $option ) {
	global $database;
	$row = new mosHPCompanies($database);
	$row->load($id);

	if (!$id) {
		# do stuff for new record
		$row->featured = 0;
		$row->hits = 0;
	}
	HTML_hotproperty::editcompany( $row, $option );
}

function savecompany( $option ) {
	global $database, $mosConfig_absolute_path;
	global $hp_img_method, $hp_imgdir_company, $hp_imgsize_company, $hp_quality_company;

	$remove_photo = mosGetParam( $_REQUEST, 'remove_photo', 0 );

	# Prepare database
	$row = new mosHPCompanies( $database );
	if (!$row->bind( $_POST )) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

	# Website
	if ($row->website == "http://") $row->website = "";

	# Check if this company has previously uploaded any of its photo. 
	if($row->id > 0) {
		$sql="SELECT photo FROM #__hp_companies WHERE id='".$row->id."'";
		$database->setQUery($sql);
		$old_photo = $database->loadResult();
	}

	# Verifiy photo 
	if ($_FILES['photo']['error'] == 0) {

		# Check directory is writable
		if(!is_writable($mosConfig_absolute_path.$hp_imgdir_company)) { 
				echo "<script> alert('The company image directory is not writable. Please chmod this directory to 755 before uploading new images: ".$mosConfig_absolute_path.$hp_imgdir_company."'); window.history.go(-1); </script>\n";
				exit();
		}

		# Verify image is supplied 
		if(!isset($_FILES['photo']) || !is_array($_FILES['photo'])) {
			echo "<script> alert('Please make sure you specified an image before uploading.'); window.history.go(-1); </script>\n";
			exit();
		}

		# Verify the file is uploaded
		if (!is_uploaded_file($_FILES['photo']['tmp_name'])) {
			echo "<script> alert('Please make sure you specified a valid image before uploading.'); window.history.go(-1); </script>\n";
			exit();
		} 

		# Verify this is an acceptable image file (.gif, .jpg, .png, .bmp)
		if ((strcasecmp(substr($_FILES['photo']['name'],-4),".gif")) && (strcasecmp(substr($_FILES['photo']['name'],-4),".jpg")) && (strcasecmp(substr($_FILES['photo']['name'],-4),".png")) && (strcasecmp(substr($_FILES['photo']['name'],-4),".bmp")) ) {
			echo "<script> alert('Only files of type gif, png, jpg or bmp can be uploaded.'); window.history.go(-1); </script>\n";
			exit();
		}

		# No Duplicates!
		if (file_exists($mosConfig_absolute_path.$hp_imgdir_company.$_FILES['photo']['name'])) {
			echo "<script> alert('There already exists a file with the same name. Please rename and upload again.'); window.history.go(-1); </script>\n";
			exit();
		}

//		if(copy ($_FILES['photo']['tmp_name'], $mosConfig_absolute_path.$hp_imgdir_company.$_FILES['photo']['name'])) {
		if( move_uploaded_file($_FILES['photo']['tmp_name'], $mosConfig_absolute_path.'/media/'.basename($_FILES['photo']['tmp_name'])) ) {

			$src_file = $mosConfig_absolute_path.'/media/'.basename($_FILES['photo']['tmp_name']);

			# Generate the Standard image
			if(resize_image($src_file, $mosConfig_absolute_path.$hp_imgdir_company.$_FILES['photo']['name'], $hp_imgsize_company, $hp_img_method, $hp_quality_company)) {
				$row->photo = $_FILES['photo']['name'];
				# SUCCESS!
				# Now delete his previous photo
				if ($old_photo != "") {
					if(!unlink($mosConfig_absolute_path.$hp_imgdir_company.$old_photo)) {
						echo "<script> alert('Error while deleting old photo'); window.history.go(-1); </script>\n";
						exit();
					}
				}
			} 
			unlink($src_file);
		} 
	}

	# Remove current photo - No photo for this company!
	if ($remove_photo) {
		$row->photo = '';
		if(!unlink($mosConfig_absolute_path.$hp_imgdir_company.$old_photo)) {
				echo "<script> alert('Error while deleting old photo'); window.history.go(-1); </script>\n";
				exit();
		}
	}

	if (!$row->store()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

	mosRedirect( "index2.php?option=$option&task=listcompany" );
}

function cancelcompany( $option ) {
	mosRedirect( "index2.php?option=$option&task=listcompany" );
}

function removecompany( $id, $option ) {
	global $database;
	global $mosConfig_absolute_path, $hp_imgdir_company;

	for ($i = 0; $i < count($id); $i++) {
		$query = "SELECT COUNT(id) FROM #__hp_agents WHERE company='".$id[$i]."'";
		$database->setQuery($query);

		if(($count = $database->loadResult()) == null) {
			echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
		}

		if ($count != 0) {
			# Do not allow removal if there's any agent under this company
			mosRedirect( "index2.php?option=$option&task=listcompany","Cannot delete this company, as they still have agents working for them." );
		} else {
			# Start removal process
			$query = "SELECT photo FROM #__hp_companies WHERE id='".$id[$i]."'";
			$database->setQuery($query);
			$old_photo=$database->loadResult();

			# If this company has a photo, delete it
			if ($old_photo != "") {
				if(!unlink($mosConfig_absolute_path.$hp_imgdir_company.$old_photo)) {
					echo "<script> alert('Error while deleting old photo'); window.history.go(-1); </script>\n";
					exit();
				}
			}
			
			# Remove company from database
			$query="DELETE FROM #__hp_companies WHERE `id`='".$id[$i]."'";
			$database->setQuery($query);
			$database->query();
		}
	}
	mosRedirect("index2.php?option=$option&task=listcompany");
}

/***********
 * Agent *
 ***********/

function listagent( $option ) {
	global $database, $mainframe;

	$limit = $mainframe->getUserStateFromRequest( "viewlistlimit", 'limit', 10 );
	$limitstart = $mainframe->getUserStateFromRequest( "viewcli{$option}limitstart", 'limitstart', 0 );

	# get the total number of records
	$database->setQuery( "SELECT count(*) FROM #__hp_agents" );
	$total = $database->loadResult();
	echo $database->getErrorMsg();

	require_once("includes/pageNavigation.php");
	$pageNav = new mosPageNav( $total, $limitstart, $limit );

	$sql = "SELECT a.*, c.name AS company, count(p.id) AS properties, sum(p.hits) AS hits, u.name AS mosName, u.username AS mosUsername"
		. "\nFROM #__hp_agents AS a"
		. "\nLEFT JOIN #__hp_companies AS c ON c.id=a.company"
		. "\nLEFT JOIN #__hp_properties AS p ON a.id=p.agent"
		. "\nLEFT JOIN #__users AS u ON u.id=a.user"
		. "\nGROUP BY a.id"
		. "\nORDER BY a.name ASC"
		. "\nLIMIT $pageNav->limitstart,$pageNav->limit";

	$database->setQuery($sql);

	if(!$result = $database->query()) {
		echo $database->stderr();
		return false;
	}
	$rows = $database->loadObjectList();

	HTML_hotproperty::listagent( $rows, $pageNav, $option );
}

function editagent( $id, $option ) {
	global $database, $hp_agent_groupid;
	$row = new mosHPAgents($database);
	$row->load($id);

	# Load MOS Username Logins
  $logins[] = mosHTML::makeOption( '0', 'Select Login User' );
  $logins[] = mosHTML::makeOption( '0', 'No Login' );
	
	# Get agents from mos_users
	$database->setQuery("SELECT id AS value, name AS text FROM #__users"
		.	"\nWHERE gid=".$hp_agent_groupid);
	$logins = array_merge($logins, $database->loadObjectList());
	$lists["logins"] = mosHTML::selectList( $logins, 'user', 'class="inputbox" size="1"', 'value', 'text', $row->user );

  # Get the list of companies
  $companies[] = mosHTML::makeOption( '0', 'Select Company' );
  $database->setQuery( "SELECT id AS value, name AS text FROM #__hp_companies"
    . "\nORDER BY name" );
  $companies = array_merge( $companies, $database->loadObjectList() );
  $lists["company"] = mosHTML::selectList( $companies, 'company', 'class="inputbox" size="1"', 'value', 'text', $row->company );
	
	# Yes/No select list for "Need Approval"
	$lists["approval"] = mosHTML::yesnoSelectList("need_approval", 'class="inputbox"', $row->need_approval);

	if (!$id) {
		# do stuff for new record
		$row->featured = 0;
		$row->hits = 0;
	}
	HTML_hotproperty::editagent( $row, $lists, $option );
}

function saveagent( $option ) {
	global $database, $mosConfig_absolute_path;
	global $hp_img_method, $hp_imgdir_agent, $hp_imgsize_agent, $hp_quality_agent;

	$remove_photo = mosGetParam( $_REQUEST, 'remove_photo', 0 );

	# Prepare database
	$row = new mosHPAgents( $database );
	if (!$row->bind( $_POST )) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

	# Check if the login user has already been taken by another agent
	if (!empty($row->user)) {
		$database->setQuery("SELECT id FROM #__hp_agents WHERE user=".$row->user." AND id <> '".$row->id."'");
		if($database->loadResult()) {
			echo "<script> alert('There is already an agent using the same login. Please choose another Login User.'); window.history.go(-1); </script>\n";
			exit();
		}
	}

	# Check if this agent has previously uploaded any of his photo. 
	if($row->id > 0) {
		$sql="SELECT photo FROM #__hp_agents WHERE id='".$row->id."'";
		$database->setQUery($sql);
		$old_photo = $database->loadResult();
	}

	# Verifiy photo 
	if ($_FILES['photo']['error'] == 0) {

		# Check directory is writable
		if(!is_writable($mosConfig_absolute_path.$hp_imgdir_agent)) { 
				echo "<script> alert('The agent image directory is not writable. Please chmod this directory to 755 before uploading new images: ".$mosConfig_absolute_path.$hp_imgdir_agent."'); window.history.go(-1); </script>\n";
				exit();
		}

		# Verify image is supplied 
		if(!isset($_FILES['photo']) || !is_array($_FILES['photo'])) {
			echo "<script> alert('Please make sure you specified an image before uploading.'); window.history.go(-1); </script>\n";
			exit();
		}

		# Verify the file is uploaded
		if (!is_uploaded_file($_FILES['photo']['tmp_name'])) {
			echo "<script> alert('Please make sure you specified a valid image before uploading.'); window.history.go(-1); </script>\n";
			exit();
		} 

		# Verify this is an acceptable image file (.gif, .jpg, .png, .bmp)
		if ((strcasecmp(substr($_FILES['photo']['name'],-4),".gif")) && (strcasecmp(substr($_FILES['photo']['name'],-4),".jpg")) && (strcasecmp(substr($_FILES['photo']['name'],-4),".png")) && (strcasecmp(substr($_FILES['photo']['name'],-4),".bmp")) ) {
			echo "<script> alert('Only files of type gif, png, jpg or bmp can be uploaded.'); window.history.go(-1); </script>\n";
			exit();
		}

		# No Duplicates!
		if (file_exists($mosConfig_absolute_path.$hp_imgdir_agent.$_FILES['photo']['name'])) {
			echo "<script> alert('There already exists a file with the same name. Please rename and upload again.'); window.history.go(-1); </script>\n";
			exit();
		}

//		if(copy ($_FILES['photo']['tmp_name'], $mosConfig_absolute_path.$hp_imgdir_agent.$_FILES['photo']['name'])) {
		if( move_uploaded_file($_FILES['photo']['tmp_name'], $mosConfig_absolute_path.'/media/'.basename($_FILES['photo']['tmp_name'])) ) {

			$src_file = $mosConfig_absolute_path.'/media/'.basename($_FILES['photo']['tmp_name']);

			# Generate the Standard image
			if(resize_image($src_file, $mosConfig_absolute_path.$hp_imgdir_agent.$_FILES['photo']['name'], $hp_imgsize_agent, $hp_img_method, $hp_quality_agent)) {
				$row->photo = $_FILES['photo']['name'];
				# SUCCESS!
				# Now delete his previous photo
				if ($old_photo != "") {
					if(!unlink($mosConfig_absolute_path.$hp_imgdir_agent.$old_photo)) {
						echo "<script> alert('Error while deleting old photo'); window.history.go(-1); </script>\n";
						exit();
					}
				}
			} 
			unlink( $src_file );
		}
	}

	# Remove current photo - No photo for this agent!
	if ($remove_photo) {
		$row->photo = '';
		if(!unlink($mosConfig_absolute_path.$hp_imgdir_agent.$old_photo)) {
				echo "<script> alert('Error while deleting old photo'); window.history.go(-1); </script>\n";
				exit();
		}
	}

	if (!$row->store()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

	mosRedirect( "index2.php?option=$option&task=listagent" );
}

function cancelagent( $option ) {
	mosRedirect( "index2.php?option=$option&task=listagent" );
}

function removeagent( $id, $option ) {
	global $database;
	global $mosConfig_absolute_path, $hp_imgdir_agent;

	for ($i = 0; $i < count($id); $i++) {
		$query = "SELECT COUNT(id) FROM #__hp_properties WHERE agent='".$id[$i]."'";
		$database->setQuery($query);

		if(($count = $database->loadResult()) == null) {
			echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
		}

		if ($count != 0) {
			# Do not allow removal if this agent has properties
			mosRedirect( "index2.php?option=$option&task=listagent",
			"Cannot delete this agent, as he/she still has properties." );
		} else {
			# Start removal process
			$query = "SELECT photo FROM #__hp_agents WHERE id='".$id[$i]."'";
			$database->setQuery($query);
			$old_photo=$database->loadResult();

			# If this company has a photo, delete it
			if ($old_photo != "") {
				if(!unlink($mosConfig_absolute_path.$hp_imgdir_agent.$old_photo)) {
					echo "<script> alert('Error while deleting old photo'); window.history.go(-1); </script>\n";
					exit();
				}
			}
			# Remove from database
			$query="DELETE FROM #__hp_agents WHERE `id`='".$id[$i]."'";
			$database->setQuery($query);
			$database->query();
		}
	}
	mosRedirect("index2.php?option=$option&task=listagent");
}

/************
 * Property *
 ***********/

function listproperty( $option ) {
	global $database, $mainframe;

	$limit = $mainframe->getUserStateFromRequest( "viewlistlimit", 'limit', 15 );
	$limitstart = $mainframe->getUserStateFromRequest( "viewcli{$option}limitstart", 'limitstart', 0 );

  # Prepare category and search choices
  $type = intval( mosGetParam( $_POST, 'type', 0 ) );
  $agent = intval( mosGetParam( $_POST, 'agent', 0 ) );
  $search = trim( strtolower( mosGetParam( $_POST, 'search', '' ) ) );

	# Get the ordering
  $order = trim( strtolower( mosGetParam( $_POST, 'order', 'p.name' ) ) );

  $where = array();
  $where[] = "p.published<>'-1'";
	if ($type > 0) {
	$type_O=new mosHPPropTypes($database);
	$type_O->load($type);
    $where[] = "t.lft>='$type_O->lft'";
    $where[] = "t.rgt<='$type_O->rgt'";
  }
	if ($agent > 0) {
    $where[] = "p.agent='$agent'";
  }
  if ($search) {
    $where[] = "LOWER(p.name) LIKE '%$search%'";
  }

	# get the total number of records
	$database->setQuery( "SELECT count(*) FROM #__hp_properties AS p" 
		.	(count( $where ) ? "\nWHERE " . implode( ' AND ', $where ) : "") );
	$total = $database->loadResult();
//	echo $database->getErrorMsg();

	# Page Navigation
	require_once("includes/pageNavigation.php");
	$pageNav = new mosPageNav( $total, $limitstart, $limit );

  # Construct list of categories
  $preload=array();
  $preload[] = mosHTML::makeOption( '0', 'Select Type' );
  $database->setQuery( "SELECT * FROM #__hp_prop_types"
    . "\nWHERE id<>0"
    . "\nORDER BY ordering" );
  $types = $database->loadObjectList();
  $list["types"] = mosHTML::treeSelectList( &$types, -1, $preload, 'type', 'class="inputbox" size="1" onchange="document.adminForm.submit();"', 'value', 'text', $type );

  # Construct list of agents
  $agents[] = mosHTML::makeOption( '0', 'Select Agent' );
  $agents[] = mosHTML::makeOption( '-1', '- All Agentss' );
  $database->setQuery( "SELECT id AS value, name AS text FROM #__hp_agents"
    . "\nORDER BY name DESC" );
  $agents = array_merge( $agents, $database->loadObjectList() );
  $list["agents"] = mosHTML::selectList( $agents, 'agent', 'class="inputbox" size="1" onchange="document.adminForm.submit();"', 'value', 'text', $agent );

	# Construct the ordering options
	$orders[] = mosHTML::makeOption( 'p.name', 'A-Z' );
	$orders[] = mosHTML::makeOption( 'a.name', 'Agent' );
	$orders[] = mosHTML::makeOption( 't.name', 'Type' );
	$orders[] = mosHTML::makeOption( 'p.modified', 'Modified' );
	$orders[] = mosHTML::makeOption( 'p.created', 'Newest' );
	$orders[] = mosHTML::makeOption( 'p.featured', 'Featured' );
	$orders[] = mosHTML::makeOption( 'p.published', 'Unpublished' );
	$orders[] = mosHTML::makeOption( 'p.approved', 'Need Approval' );
	$orders[] = mosHTML::makeOption( 'p.suburb', 'Suburb' );
	$orders[] = mosHTML::makeOption( 'p.state', 'State' );
	$orders[] = mosHTML::makeOption( 'p.hits', 'Hits' );
  $list["order"] = mosHTML::selectList( $orders, 'order', 'class="inputbox" size="1" onchange="document.adminForm.submit();"', 'value', 'text', $order );

	# Get ALL properties
	$sql = "SELECT p.*, COUNT(DISTINCT photo.id) AS photos, c.name AS company, a.name AS agent, t.name AS type"
		. "\nFROM #__hp_properties AS p"
		. "\nLEFT JOIN #__hp_agents AS a ON a.id=p.agent"
		. "\nLEFT JOIN #__hp_companies AS c ON c.id=a.company"
		. "\nLEFT JOIN #__hp_prop_types AS t ON t.id=p.type"
		.	"\nLEFT JOIN #__hp_photos AS photo ON photo.property=p.id"
		.	(count( $where ) ? "\nWHERE " . implode( ' AND ', $where ) : "")
			.	"\nGROUP BY p.id"
		. "\nORDER BY p.approved ASC, ".$order." ".(($order == "p.hits" || $order == "p.created" || $order == "p.modified" || $order == "p.featured") ? "DESC" : "ASC") // default to ASC, unless for 'hits', which we sort highest hits first
		. "\nLIMIT $pageNav->limitstart,$pageNav->limit";

	$database->setQuery($sql);

	if(!$result = $database->query()) {
		echo $database->stderr();
		return false;
	}
	$rows = $database->loadObjectList();

	HTML_hotproperty::listproperty( $rows, $pageNav, $option, $search, $list );
}

function editproperty( $id, $option, $activetab='0' ) {
	global $database;

	# Load Properties's MAIN info
	$row = new mosHPProperties($database);
	$row->load($id);
	
	#on charge la cat�gorie de la prop:
	$type=new mosHPPropTypes($database);
	$type->load($row->type);

	# Load Properties's Extra Fields CORE ELEMENTS
	$sql = "SELECT ef.*"
		. "\nFROM #__hp_prop_ef AS ef"
		. "\nWHERE iscore='1'"
		. "\nORDER BY ef.ordering ASC";

	$database->setQuery($sql);
	
	if(!$result = $database->query()) {
		echo $database->stderr();
		return false;
	}
	$core_fields = $database->loadObjectList('name');
	# END: Load Properties's Extra Fields CORE Elements

	# Load Properties's Extra Fields ELEMENTS
	$sql = "SELECT ef.*"
		. "\nFROM #__hp_prop_ef AS ef"
		. "\nWHERE iscore='0' AND published='1'"
		. "\nORDER BY ef.ordering ASC";

	$database->setQuery($sql);
	
	if(!$result = $database->query()) {
		echo $database->stderr();
		return false;
	}
	$extra_fields = $database->loadObjectList();

	# END: Load Properties's Extra Fields Elements

	# Load Properties's Extra Fields DATA (aka Properties2)
	$sql = "SELECT ef.name AS name, p.value"
		. "\nFROM #__hp_properties2 AS p"
		. "\nLEFT JOIN #__hp_prop_ef AS ef ON ef.id=p.field"
		. "\nWHERE p.property='$id'";
	$database->setQuery($sql);
	if(!$result = $database->query()) {
		echo $database->stderr();
		return false;
	}
	$extra_fields_data = $database->loadObjectList('name');

	# END: Load Properties's Extra Fields DATA

	# Get number of images - so that we can estimate the height for iframe, to remove the vertical scrollbar
	$sql = "SELECT COUNT(*) AS images"
		. "\nFROM #__hp_photos"
		. "\nWHERE property='$id'";
	$database->setQuery($sql);
	if(!$result = $database->query()) {
		echo $database->stderr();
		return false;
	}
	$num_of_images = $database->loadResult();

  # Get the list of agent (companies)
  $agents[] = mosHTML::makeOption( '0', 'Select Agent' );
  $database->setQuery( "SELECT a.id AS id, a.name AS name, c.name AS company FROM #__hp_agents AS a"
		. "\nLEFT JOIN #__hp_companies AS c ON c.id=a.company"
    . "\nORDER BY a.name ASC, c.name ASC" );
	$row_agents = $database->loadObjectList();
	foreach($row_agents AS $row_agent) {
	  $agents[] = mosHTML::makeOption( $row_agent->id, $row_agent->name." (".$row_agent->company.")" );
	}
	$lists["agent"] = mosHTML::selectList( $agents, 'agent', 'class="inputbox" size="1"', 'value', 'text', ((count($agents) <= 2 && $id == 0) ? $row_agent->id : $row->agent ) );

  # Get the list of property types
  $query = "SELECT * FROM #__hp_prop_types WHERE id!=0"; //On exclut /root que l'on ajoute manuellement
  $database->setQuery( $query );
  $src_list = $database->loadObjectList();
  $preload = array();
  $preload[] = mosHTML::makeOption( '0', 'Select Property Type' );
  $selected = array();
  $selected[] = mosHTML::makeOption( $row->type ); //S�lectionne par d�faut dans la liste le type courant de la propri�t�
  $lists["prop_type"] = mosHTML::treeSelectList( &$src_list, -1, $preload, 'type', 'class="inputbox" size="1" onchange="javascript:submitbutton(\'updateproperty\')"', 'value', 'text', $selected );	

	if ($id == 0) {
		# Assign default value for new data
		$row->featured = 0;
		$row->hits = 0;
		$row->publish_up = date( "Y-m-d", time() );
		$row->publish_down = "Never";

		#Assign for properties2 data
		for ($i=0, $n=count( $extra_fields ); $i < $n; $i++) {
			$extra_field  = $extra_fields[$i];
			if ($extra_field->default_value <> "") {
				$extra_fields_data[$extra_field->name]->value = $extra_field->default_value;
			}
		}
	} else {
		if (trim( $row->publish_down ) == "0000-00-00 00:00:00") {
			$row->publish_down = "Never";
		}
	}

	# Check HP Extension
	$extensions = getHPExtensions();

	HTML_hotproperty::editproperty( $row, $lists, $core_fields, $extra_fields, $extra_fields_data, $num_of_images, $extensions, $option, $activetab );
}

function getHPExtensions() {
	global $database;

	$database->setQuery("SELECT `name`, `option` FROM #__components WHERE `option` LIKE 'com_hp_%' && `option` <> 'com_hpmulticats' && parent=0");
	$extensions = $database->loadObjectList();

	return $extensions;
}

function saveproperty( $option, $task='' ) {
	global $database;

	$row = new mosHPProperties( $database );
	if (!$row->bind( $_POST )) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

	if ($row->id < 1) {
		$row->created = $row->created ? $row->created : date( "Y-m-d H:i:s" );
	} else {
		$row->modified = date( "Y-m-d H:i:s" );
		$row->created = $row->created ? $row->created : date( "Y-m-d H:i:s" );
	}

	# If never fill it with all zeros
	if (trim( $row->publish_down ) == "Never") {
		$row->publish_down = "0000-00-00 00:00:00";
	}

	if ($row->approved <> "1") {
		$row->approved = 0;
	}

	if ($row->published <> "1") {
		$row->published = 0;
	}
	
	if (!$row->store()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

	# Store ef_ elements to our database #__hp_properties2
/*
 * This is just wasteful so ive removed it
 * 
 *
	# Erase Previous Records, make way for the new data
	$sql="DELETE FROM #__hp_properties2"
		. "\nWHERE property='".$row->id."'";
	$database->setQuery($sql);
	if (!$database->query()) {
		echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
		exit();
	}
*/
	# Assign Properties2 data (Extra Fields)
	foreach($_POST AS $k => $v) {
		if (substr($k,0,3) == "ef_") {

			# -- Get the prop_ef id first
			$sql="SELECT id FROM #__hp_prop_ef WHERE name='".substr($k,3)."'";
			$database->setQuery($sql);
			if (!$database->query()) {
				echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
				exit();
			}
			$prop_ef_id = $database->loadResult();
			
			$sql = "SELECT id FROM #__hp_properties2 WHERE property = '$row->id' AND field = '$prop_ef_id'";
			$database->setQuery($sql);
			$prop_ef_val_id = (int) $database->loadResult();

			# -- Now add the row
			$sql = "REPLACE INTO #__hp_properties2"
				. "\n(`id`,`property`, `field`, `value`)"
				. "\nVALUES ('$prop_ef_val_id','".$row->id."', '".$prop_ef_id."', '".((is_array($v)) ? implode("|",$v) : $v)."')";
			$database->setQuery($sql);

			//echo $database->getQuery()."<br />";
			if (!$database->query()) {
				echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
				exit();
			}

		} // End if
	} // End of foreach
	
	# Erase any field data for this property that are not published
	$sql="DELETE FROM #__hp_properties2 WHERE property='".$row->id."' AND field NOT IN (SELECT id FROM #__hp_prop_ef)";
	$database->setQuery($sql);
	if (!$database->query()) {
		echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
		exit();
	}

	# Manage Featured Ordering
	$featured = new mosHPFeatured($database);
	if ($row->featured) {
		if (!$featured->load( $row->id )) {
			// new entry
			$database->setQuery( "INSERT INTO #__hp_featured VALUES ('$row->id','1')" );
			if (!$database->query()) {
				echo "<script> alert('".$database->stderr()."');</script>\n";
				exit();
			}
			$featured->ordering = 1;
		}
	} else {
		if (!$featured->delete( $row->id )) {
			$msg .= $featured->stderr();
		}
		$featured->ordering = 0;
	}
	$featured->updateOrder();

	if ($task == '') mosRedirect( "index2.php?option=$option&task=listproperty" );
	else mosRedirect( "index2.php?option=$option&task=editproperty&id=$row->id&tab=$task" );

}

function feature_property( $id, $feature=0 ,$option )
{
	global $database, $my;

	if (!is_array( $id ) || count( $id ) < 1) {
		$action = $feature ? 'feature' : 'unfeature';
		echo "<script> alert('Select an item to $action'); window.history.go(-1);</script>\n";
		exit;
	}

	$ids = implode( ',', $id );

	$database->setQuery( "UPDATE #__hp_properties SET featured='$feature'"
	. "\nWHERE id IN ($ids)"
	);
	if (!$database->query()) {
		echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
		exit();
	}

	# Update Featured table
	$fp = new mosHPFeatured( $database );
	foreach ($id as $id_one) {
		// toggles go to first place
		if ($fp->load( $id_one )) {
			if (!$fp->delete( $id_one )) {
				$msg .= $fp->stderr();
			}
			$fp->ordering = 0;
		} else {
			// new entry
			$database->setQuery( "INSERT INTO #__hp_featured VALUES ('$id_one','1')" );
			if (!$database->query()) {
				echo "<script> alert('".$database->stderr()."');</script>\n";
				exit();
			}
			$fp->ordering = 1;
		}
		$fp->updateOrder();
	}

	mosRedirect( "index2.php?option=$option&task=listproperty" );
}

function approve_property( $id, $approve=1 ,$option )
{
	global $database, $my;

	if (!is_array( $id ) || count( $id ) < 1) {
		$action = $feature ? 'approve' : 'unapprove';
		echo "<script> alert('Select an item to $action'); window.history.go(-1);</script>\n";
		exit;
	}

	$ids = implode( ',', $id );

	$database->setQuery( "UPDATE #__hp_properties SET approved='$approve'"
	. "\nWHERE id IN ($ids)"
	);
	if (!$database->query()) {
		echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
		exit();
	}

	mosRedirect( "index2.php?option=$option&task=listproperty" );
}

/***
 * Publish (1) / Unpublish (0) / Archive (-1)/ Unarchive properties (0)
 ***/
function publish_property( $id, $publish=1 ,$option )
{
	global $database, $my;

	if (!is_array( $id ) || count( $id ) < 1) {
		$action = $publish == 1 ? 'publish' : ($publish == -1 ? 'archive' : 'unpublish');
		echo "<script> alert('Select an item to $action'); window.history.go(-1);</script>\n";
		exit;
	}

	$ids = implode( ',', $id );

	$database->setQuery( "UPDATE #__hp_properties SET published='$publish'"
	. "\nWHERE id IN ($ids)"
	);
	if (!$database->query()) {
		echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
		exit();
	}

	# If action is Arhive, Unfeature the property.
	if ($publish == -1) {
		$database->setQuery( "UPDATE #__hp_properties SET featured='0'"
		. "\nWHERE id IN ($ids)"
		);
		if (!$database->query()) {
			echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
			exit();
		}

		# Update Featured table - Remove property
		$fp = new mosHPFeatured( $database );
		foreach ($id as $id_one) {
			// toggles go to first place
			if ($fp->load( $id_one )) {
				if (!$fp->delete( $id_one )) {
					$msg .= $fp->stderr();
				}
				$fp->ordering = 0;
			} 
			$fp->updateOrder();
		} // End of foreach
	}

	# End of Unfeature

	mosRedirect( "index2.php?option=$option&task=listproperty" );

}


function cancelproperty( $option ) {
	mosRedirect( "index2.php?option=$option&task=listproperty" );
}

function removeproperty( $id, $option ) {
	global $database;
	global $mosConfig_absolute_path, $hp_imgdir_original, $hp_imgdir_thumb, $hp_imgdir_standard;

	for ($i = 0; $i < count($id); $i++) {

		# Get the photos for this property
		$sql="SELECT original, standard, thumb FROM #__hp_photos WHERE property='".$id[$i]."'";
		$database->setQUery($sql);
		$old_photos = $database->loadObjectList();

		if (!empty($old_photos)) {
			foreach($old_photos AS $old_photo) {
				if (!empty($old_photo->original) && file_exists($mosConfig_absolute_path.$hp_imgdir_original.$old_photo->original)) {
					if(!unlink($mosConfig_absolute_path.$hp_imgdir_original.$old_photo->original)) {
						echo "<script> alert('Error while deleting Original's old photo'); window.history.go(-1); </script>\n";
						exit();
					}
				}
				if (!empty($old_photo->standard) && file_exists($mosConfig_absolute_path.$hp_imgdir_original.$old_photo->standard)) {
					if(!unlink($mosConfig_absolute_path.$hp_imgdir_standard.$old_photo->standard)) {
						echo "<script> alert('Error while deleting Standard's old photo'); window.history.go(-1); </script>\n";
						exit();
					}
				}
				if (!empty($old_photo->thumb) && file_exists($mosConfig_absolute_path.$hp_imgdir_original.$old_photo->thumb)) {
					if(!unlink($mosConfig_absolute_path.$hp_imgdir_thumb.$old_photo->thumb)) {
						echo "<script> alert('Error while deleting Thumbnail's old photo'); window.history.go(-1); </script>\n";
						exit();
					}
				}
			}
		} // End of Empty

		$query="DELETE FROM #__hp_properties WHERE `id`='".$id[$i]."'";
		$database->setQuery($query);
		$database->query();

		$query="DELETE FROM #__hp_properties2 WHERE `property`='".$id[$i]."'";
		$database->setQuery($query);
		$database->query();
	}
	mosRedirect("index2.php?option=$option&task=listproperty");
}

function listarchived( $option ) {
	global $database, $mainframe;

	$limit = $mainframe->getUserStateFromRequest( "viewlistlimit", 'limit', 10 );
	$limitstart = $mainframe->getUserStateFromRequest( "viewcli{$option}limitstart", 'limitstart', 0 );

  # Prepare category and search choices
  $type = intval( mosGetParam( $_POST, 'type', 0 ) );
  $search = trim( strtolower( mosGetParam( $_POST, 'search', '' ) ) );

	# Get the ordering
  $order = trim( strtolower( mosGetParam( $_POST, 'order', 'p.name' ) ) );

  $where = array();
  $where[] = "p.published='-1'";
	if ($type > 0) {
    $where[] = "p.type='$type'";
  }
  if ($search) {
    $where[] = "LOWER(p.name) LIKE '%$search%'";
  }

	# get the total number of records
	$database->setQuery( "SELECT count(*) FROM #__hp_properties AS p" 
		.	(count( $where ) ? "\nWHERE " . implode( ' AND ', $where ) : "") );
	$total = $database->loadResult();
	echo $database->getErrorMsg();

	# Page Navigation
	require_once("includes/pageNavigation.php");
	$pageNav = new mosPageNav( $total, $limitstart, $limit );

  # Get the list of categories
  $types[] = mosHTML::makeOption( '0', 'Select Type' );
  $types[] = mosHTML::makeOption( '-1', '- All Types' );
  $database->setQuery( "SELECT id AS value, name AS text FROM #__hp_prop_types"
    . "\nORDER BY ordering" );
  $types = array_merge( $types, $database->loadObjectList() );
  $list["types"] = mosHTML::selectList( $types, 'type', 'class="inputbox" size="1" onchange="document.adminForm.submit();"', 'value', 'text', $type );

	# Get ALL properties
	$sql = "SELECT p.*, c.name AS company, a.name AS agent, t.name AS type"
		. "\nFROM #__hp_properties AS p"
		. "\nLEFT JOIN #__hp_agents AS a ON a.id=p.agent"
		. "\nLEFT JOIN #__hp_companies AS c ON c.id=a.company"
		. "\nLEFT JOIN #__hp_prop_types AS t ON t.id=p.type"
		.	(count( $where ) ? "\nWHERE " . implode( ' AND ', $where ) : "")
		. "\nORDER BY ".$order." ".(($order == "p.hits") ? "DESC" : "ASC") // default to ASC, unless for 'hits', which we sort highest hits first
		. "\nLIMIT $pageNav->limitstart,$pageNav->limit";

	$database->setQuery($sql);

	if(!$result = $database->query()) {
		echo $database->stderr();
		return false;
	}
	$rows = $database->loadObjectList();

	HTML_hotproperty::listarchived( $rows, $pageNav, $option, $search, $list );
}

/********************
 * Image Management *
 *******************/

function listprop_image($option, $msg='') {
	global $database;

	$property = mosGetParam( $_REQUEST, 'property', array(0) );

	$sql = "SELECT p.*"
		. "\nFROM #__hp_photos AS p"
		. "\nWHERE property='".$property."'"
		. "\nORDER BY ordering ASC";

	$database->setQuery($sql);

	if(!$result = $database->query()) {
		echo $database->stderr();
		return false;
	}
	$rows = $database->loadObjectList();

	HTML_hotproperty::listprop_image($rows, $property, $option, $msg);
}

function listprop_showupload_image() {
	$property = mosGetParam( $_REQUEST, 'property', 0 );
	HTML_hotproperty::listprop_uploadnew_image($property);
}




function listprop_upload_image() {
	global $mosConfig_absolute_path, $database;
	global $hp_imgdir_standard, $hp_imgdir_thumb, $hp_imgdir_original, $hp_imgdir_zoom;
	global $hp_img_connector, $hp_img_method, $hp_imgsize_standard, $hp_imgsize_thumb;
	global $hp_img_saveoriginal, $hp_quality_photo, $hp_imgsize_zoom;

	$property = mosGetParam( $_REQUEST, 'property', 0 ); // Property ID
	$id = mosGetParam( $_POST, 'id', 0 ); // Image ID 
	$option = mosGetParam( $_REQUEST, 'option', array(0) );
	
	# Does our directory allow writing for public?
	if ($_FILES['image']['tmp_name'] != '') {
		if(!is_writable($mosConfig_absolute_path.$hp_imgdir_standard)) { 
				echo "<script> alert('The standard image directory is not writable. Please chmod this directory to 755 before uploading new images: ".$mosConfig_absolute_path.$hp_imgdir_standard."'); window.history.go(-1); </script>\n";
				exit();
		}

		if(!is_writable($mosConfig_absolute_path.$hp_imgdir_thumb)) { 
				echo "<script> alert('The thumb image directory is not writable. Please chmod this directory to 755 before uploading new images: ".$mosConfig_absolute_path.$hp_imgdir_thumb."'); window.history.go(-1); </script>\n";
				exit();
		}

		if ($hp_img_saveoriginal) {
			if(!is_writable($mosConfig_absolute_path.$hp_imgdir_original)) { 
				echo "<script> alert('The original image directory is not writable. Please chmod this directory to 755 before uploading new images: ".$mosConfig_absolute_path.$hp_imgdir_original."'); window.history.go(-1); </script>\n";
				exit();		}
		}
	}
	
	//if($_FILES['image']['tmp_name'] != ""){
		$ext1 = explode(".", ($_FILES['image']['name']));
		$ext = $ext1[1];
	if($ext == "zip"){
		
	 $zip = new ZipArchive;
     $res = $zip->open($_FILES['image']['tmp_name']);
     if ($res === TRUE) {
		 $dirName = $_FILES['image']['name']."/";
		 $zip->extractTo($dirName);
         $zip->close();
		 $zipName1 = explode(".", ($_FILES['image']['name']));
		 $zipName = $zipName1[0];		 
     }
	 
	$zip = zip_open($_FILES['image']['tmp_name']);
	while($each_image = zip_read($zip)){
		
		$_FILES['image']['tmp_name'] = zip_entry_name($each_image);
		$_FILES['image']['name'] = zip_entry_name($each_image);
		$actualFile = explode("/", $_FILES['image']['name']);
		$_FILES['image']['name'] = $actualFile[1];
		$_FILES['image']['size'] = zip_entry_filesize($each_image);

	# If this image has no ID, it means this property is attaching NEW photo - Verify image
	# Id this is an existing photo AND has attached a file - Verify image
	if ($id == 0 || ($id != 0 && $_FILES['image']['tmp_name'] != '') ) {
		# Verify image is supplied 
		if(!isset($_FILES['image']) || !is_array($_FILES['image'])) {
			echo "<script> alert('Please make sure you specified an image before uploading.'); window.history.go(-1); </script>\n";
			exit();
		}

/*
		# Verify the file is uploaded
		if (!is_uploaded_file($_FILES['image']['tmp_name'])) {
			echo "<script> alert('Please make sure you specified a valid image before uploading.'); window.history.go(-1); </script>\n";
			exit();
		} 
*/

		# Verify this is an acceptable image file (.gif, .jpg, .png, .bmp)
		if ((strcasecmp(substr($_FILES['image']['name'],-4),".gif")) && (strcasecmp(substr($_FILES['image']['name'],-4),".jpg")) && (strcasecmp(substr($_FILES['image']['name'],-4),".png")) && (strcasecmp(substr($_FILES['image']['name'],-4),".bmp")) ) {
			echo "<script> alert('Only files of type gif, png, jpg or bmp can be uploaded.'); window.history.go(-1); </script>\n";
			exit();
		}

		# No Duplicates! - Check original, thumb and standard directory
		if (file_exists($mosConfig_absolute_path.$hp_imgdir_original.$property.$hp_img_connector.$_FILES['image']['name']) || file_exists($mosConfig_absolute_path.$hp_imgdir_standard.$property.$hp_img_connector.$_FILES['image']['name']) || file_exists($mosConfig_absolute_path.$hp_imgdir_thumb.$property.$hp_img_connector.$_FILES['image']['name'])) {
			echo "<script> alert('There already exists a file with the same name. Please rename and upload again.'); window.history.go(-1); </script>\n";
			exit();
		} 
	} 

	# This is an existing photo - He's editing it. 
	if ($id != 0) { 
		# Store the old photo name for later use - to delete if he's uploading any new one
		$sql="SELECT original, standard, thumb FROM #__hp_photos WHERE id='".$id."' LIMIT 1";
		$database->setQUery($sql);
		$old_photos = $database->loadObjectList();
	}

	# Prepare the database
	$row = new mosHPPhotos( $database );
	$row->property = $property;
	
$testDir = ($dirName.$_FILES['image']['tmp_name']);	
	
	# Copy & generate image if (i) New photo (ii) Changing existing photo
	if (empty($id) || ($id > 0 && $_FILES['image']['tmp_name'] != '') ) {
	
	  # Allow saving of original image. Disabled by default
		if ($hp_img_saveoriginal) {
			if(copy(($mosConfig_absolute_path."/administrator/".$testDir), $mosConfig_absolute_path.$hp_imgdir_original.$property.$hp_img_connector.$_FILES['image']['name'])) {
				//echo "Original File: SUCCESS<br />";
				$row->original = $property.$hp_img_connector.$_FILES['image']['name'];
			}
		}
		
		# Generate image if GD Libraries are available
		if ($hp_img_method <> "none") {


			# Move the uploaded file to Mambo /media directory - Safe Mode copatibility
			if ( copy(($mosConfig_absolute_path."/administrator/".$testDir), $mosConfig_absolute_path.'/media/'.basename($_FILES['image']['tmp_name'])) ) {
				$src_file = $mosConfig_absolute_path.'/media/'.basename($_FILES['image']['tmp_name']);
			} else {
				echo "ERROR: Can not move uploaded file!";
			}
						
			# Generate the Zoom image
			if(resize_image($src_file, $mosConfig_absolute_path.$hp_imgdir_zoom.$property.$hp_img_connector.$_FILES['image']['name'], $hp_imgsize_zoom, $hp_img_method, $hp_quality_photo)) {
				//				echo "Standard File generate: SUCCESS<br />";
				$row->zoom = $property.$hp_img_connector.$_FILES['image']['name'];
			} else {
								echo "Zoom File generate: FAILED<br />";
			}

			# Generate the Standard image
			if(resize_image($src_file, $mosConfig_absolute_path.$hp_imgdir_standard.$property.$hp_img_connector.$_FILES['image']['name'], $hp_imgsize_standard, $hp_img_method, $hp_quality_photo)) {
				//				echo "Standard File generate: SUCCESS<br />";
				$row->standard = $property.$hp_img_connector.$_FILES['image']['name'];
			} else {
								echo "Standard File generate: FAILED<br />";
			}

			# Generate the Thumb image
			if(resize_image($src_file, $mosConfig_absolute_path.$hp_imgdir_thumb.$property.$hp_img_connector.$_FILES['image']['name'], $hp_imgsize_thumb, $hp_img_method, $hp_quality_photo)) {
				//			echo "Thumb File generate: SUCCESS<br />";
				$row->thumb = $property.$hp_img_connector.$_FILES['image']['name'];
			} else {
								echo "Thumb File generate: FAILED<br />";
			}
	
			# Remove the temporary file in /media
			unlink( $src_file );

		}  else {
			
			
			# If user does not have GD Libraries, we use the Original photo as the Standard Photo.
			if(copy (($mosConfig_absolute_path."/administrator/".$testDir), $mosConfig_absolute_path.$hp_imgdir_standard.$property.$hp_img_connector.$_FILES['image']['name'])) {
				//echo "Standard File: SUCCESS<br />";
				$row->standard = $property.$hp_img_connector.$_FILES['image']['name'];
			}
		}// End of generate Libraries

		# New image generation is done, if this image has any old photo, delete it
		if (!empty($old_photos)) {
			foreach($old_photos AS $old_photo) {
				if (!empty($old_photo->original)) {
					if(!unlink($mosConfig_absolute_path.$hp_imgdir_original.$old_photo->original)) {
						echo "<script> alert('Error while deleting Original's old photo'); window.history.go(-1); </script>\n";
						exit();
					}
				}
				if (!empty($old_photo->standard)) {
					if(!unlink($mosConfig_absolute_path.$hp_imgdir_standard.$old_photo->standard)) {
						echo "<script> alert('Error while deleting Standard's old photo'); window.history.go(-1); </script>\n";
						exit();
					}
				}
				if (!empty($old_photo->thumb)) {
					if(!unlink($mosConfig_absolute_path.$hp_imgdir_thumb.$old_photo->thumb)) {
						echo "<script> alert('Error while deleting Thumbnail's old photo'); window.history.go(-1); </script>\n";
						exit();
					}
				}
			}
		} // End of Empty

	} // End of IF

	# Get ready and insert to databse
	if (!$row->bind( $_POST )) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

//	$row->title = htmlspecialchars($row->title, ENT_QUOTES);
//	$row->desc = htmlspecialchars($row->desc, ENT_QUOTES);

	if($row->id < 1) { # Do for new record
		$row->ordering = 9999;
		if ($row->title == "") $row->title = $_FILES['image']['name'];
	}
	
//		echo "Title: ".$row->title;
	if (!$row->store()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	} 
	$row->updateOrder('property='.$row->property);

	# Show message upon success upload
	$msg = "Upload Success! - <b>".$_FILES['image']['name']."</b> (".round(intval($_FILES['image']['size']/1024))." Kb)";
	
	} //end while loop for each image in zip file
	}	//end files conditional statement
	
	else{
		
		$dirName = "";
		
		if ($id == 0 || ($id != 0 && $_FILES['image']['tmp_name'] != '') ) {
		# Verify image is supplied 
		if(!isset($_FILES['image']) || !is_array($_FILES['image'])) {
			echo "<script> alert('Please make sure you specified an image before uploading.'); window.history.go(-1); </script>\n";
			exit();
		}

/*
		# Verify the file is uploaded
		if (!is_uploaded_file($_FILES['image']['tmp_name'])) {
			echo "<script> alert('Please make sure you specified a valid image before uploading.'); window.history.go(-1); </script>\n";
			exit();
		} 
*/

		# Verify this is an acceptable image file (.gif, .jpg, .png, .bmp)
		if ((strcasecmp(substr($_FILES['image']['name'],-4),".gif")) && (strcasecmp(substr($_FILES['image']['name'],-4),".jpg")) && (strcasecmp(substr($_FILES['image']['name'],-4),".png")) && (strcasecmp(substr($_FILES['image']['name'],-4),".bmp")) ) {
			echo "<script> alert('Only files of type gif, png, jpg or bmp can be uploaded.'); window.history.go(-1); </script>\n";
			exit();
		}

		# No Duplicates! - Check original, thumb and standard directory
		if (file_exists($mosConfig_absolute_path.$hp_imgdir_original.$property.$hp_img_connector.$_FILES['image']['name']) || file_exists($mosConfig_absolute_path.$hp_imgdir_standard.$property.$hp_img_connector.$_FILES['image']['name']) || file_exists($mosConfig_absolute_path.$hp_imgdir_thumb.$property.$hp_img_connector.$_FILES['image']['name'])) {
			echo "<script> alert('There already exists a file with the same name. Please rename and upload again.'); window.history.go(-1); </script>\n";
			exit();
		} 
	} 

	# This is an existing photo - He's editing it. 
	if ($id != 0) { 
		# Store the old photo name for later use - to delete if he's uploading any new one
		$sql="SELECT original, standard, thumb FROM #__hp_photos WHERE id='".$id."' LIMIT 1";
		$database->setQUery($sql);
		$old_photos = $database->loadObjectList();
	}

	# Prepare the database
	$row = new mosHPPhotos( $database );
	$row->property = $property;
	
$testDir = ($dirName.$_FILES['image']['tmp_name']);	
	
	
	# Copy & generate image if (i) New photo (ii) Changing existing photo
	if (empty($id) || ($id > 0 && $_FILES['image']['tmp_name'] != '') ) {
	
	  # Allow saving of original image. Disabled by default
		if ($hp_img_saveoriginal) {
			if(copy(($_FILES['image']['tmp_name']), $mosConfig_absolute_path.$hp_imgdir_original.$property.$hp_img_connector.$_FILES['image']['name'])) {
				//echo "Original File: SUCCESS<br />";
				$row->original = $property.$hp_img_connector.$_FILES['image']['name'];
			}
		}
		
		# Generate image if GD Libraries are available
		if ($hp_img_method <> "none") {


			# Move the uploaded file to Mambo /media directory - Safe Mode copatibility
			if ( copy(($_FILES['image']['tmp_name']), $mosConfig_absolute_path.'/media/'.basename($_FILES['image']['tmp_name'])) ) {
				$src_file = $mosConfig_absolute_path.'/media/'.basename($_FILES['image']['tmp_name']);
				
			} else {
				echo "ERROR: Can not move uploaded file!";
			}
						
			# Generate the Zoom image
			if(resize_image($src_file, $mosConfig_absolute_path.$hp_imgdir_zoom.$property.$hp_img_connector.$_FILES['image']['name'], $hp_imgsize_zoom, $hp_img_method, $hp_quality_photo)) {
				//				echo "Standard File generate: SUCCESS<br />";
				$row->zoom = $property.$hp_img_connector.$_FILES['image']['name'];
			} else {
								echo "Zoom File generate: FAILED<br />";
			}

			# Generate the Standard image
			if(resize_image($src_file, $mosConfig_absolute_path.$hp_imgdir_standard.$property.$hp_img_connector.$_FILES['image']['name'], $hp_imgsize_standard, $hp_img_method, $hp_quality_photo)) {
				//				echo "Standard File generate: SUCCESS<br />";
				$row->standard = $property.$hp_img_connector.$_FILES['image']['name'];
			} else {
								echo "Standard File generate: FAILED<br />";
			}

			# Generate the Thumb image
			if(resize_image($src_file, $mosConfig_absolute_path.$hp_imgdir_thumb.$property.$hp_img_connector.$_FILES['image']['name'], $hp_imgsize_thumb, $hp_img_method, $hp_quality_photo)) {
				//			echo "Thumb File generate: SUCCESS<br />";
				$row->thumb = $property.$hp_img_connector.$_FILES['image']['name'];
			} else {
								echo "Thumb File generate: FAILED<br />";
			}
	
			# Remove the temporary file in /media
			unlink( $src_file );

		}  else {
			# If user does not have GD Libraries, we use the Original photo as the Standard Photo.
			if(copy (($mosConfig_absolute_path."/administrator/".$testDir), $mosConfig_absolute_path.$hp_imgdir_standard.$property.$hp_img_connector.$_FILES['image']['name'])) {
				//echo "Standard File: SUCCESS<br />";
				$row->standard = $property.$hp_img_connector.$_FILES['image']['name'];
			}
		}// End of generate Libraries

		# New image generation is done, if this image has any old photo, delete it
		if (!empty($old_photos)) {
			foreach($old_photos AS $old_photo) {
				if (!empty($old_photo->original)) {
					if(!unlink($mosConfig_absolute_path.$hp_imgdir_original.$old_photo->original)) {
						echo "<script> alert('Error while deleting Original's old photo'); window.history.go(-1); </script>\n";
						exit();
					}
				}
				if (!empty($old_photo->standard)) {
					if(!unlink($mosConfig_absolute_path.$hp_imgdir_standard.$old_photo->standard)) {
						echo "<script> alert('Error while deleting Standard's old photo'); window.history.go(-1); </script>\n";
						exit();
					}
				}
				if (!empty($old_photo->thumb)) {
					if(!unlink($mosConfig_absolute_path.$hp_imgdir_thumb.$old_photo->thumb)) {
						echo "<script> alert('Error while deleting Thumbnail's old photo'); window.history.go(-1); </script>\n";
						exit();
					}
				}
			}
		} // End of Empty

	} // End of IF

	# Get ready and insert to databse
	if (!$row->bind( $_POST )) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

//	$row->title = htmlspecialchars($row->title, ENT_QUOTES);
//	$row->desc = htmlspecialchars($row->desc, ENT_QUOTES);

	if($row->id < 1) { # Do for new record
		$row->ordering = 9999;
		if ($row->title == "") $row->title = $_FILES['image']['name'];
	}
	
//		echo "Title: ".$row->title;
	if (!$row->store()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	} 
	$row->updateOrder('property='.$row->property);

	# Show message upon success upload
	$msg = "Upload Success! - <b>".$_FILES['image']['name']."</b> (".round(intval($_FILES['image']['size']/1024))." Kb)";
	
		}
		
	listprop_showupload_image();
}






function listprop_edit_image() {
	global $database;

	$id = mosGetParam( $_REQUEST, 'id', 0 );

  $row = new mosHPPhotos( $database );
	$row->load( $id );
	
	if(!(isset($option))){
		$option="";
		}
	
	HTML_hotproperty::listprop_edit_image($row, $option);
}

function listprop_delete_image() {
	global $database;
	global $mosConfig_absolute_path, $hp_imgdir_original, $hp_imgdir_thumb, $hp_imgdir_standard;

	$id = mosGetParam( $_REQUEST, 'id', 0 ); // Image ID

	# Get the filename from the DB to delete
	$sql="SELECT original, thumb, standard FROM #__hp_photos WHERE `id`='".$id."' LIMIT 1";
	$database->setQuery($sql);
	$old_photos = $database->loadObjectList();

	# Remove from database
	$sql="DELETE FROM #__hp_photos WHERE `id`='".$id."'";
	$database->setQuery($sql);
	$database->query();
	
	# Delete the old photos
	if (!empty($old_photos)) {
		foreach($old_photos AS $old_photo) {
			if (!empty($old_photo->original)) {
				if(!unlink($mosConfig_absolute_path.$hp_imgdir_original.$old_photo->original)) {
					echo "<script> alert('Error while deleting photo'); window.history.go(-1); </script>\n";
					exit();
				}
			}
			if (!empty($old_photo->standard)) {
				if(!unlink($mosConfig_absolute_path.$hp_imgdir_standard.$old_photo->standard)) {
					echo "<script> alert('Error while deleting photo'); window.history.go(-1); </script>\n";
					exit();
				}
			}
			if (!empty($old_photo->thumb)) {
				if(!unlink($mosConfig_absolute_path.$hp_imgdir_thumb.$old_photo->thumb)) {
					echo "<script> alert('Error while deleting photo'); window.history.go(-1); </script>\n";
					exit();
				}
			}
		}
	} // End of Empty

	listprop_showupload_image();
}

function listprop_order_image($inc, $option) {
  global $database;

	$id = mosGetParam( $_REQUEST, 'id', 0 );

  # oop database connector
  $row = new mosHPPhotos( $database );
	$row->load( $id );
	$row->move( $inc, "property='$row->property'" );
	
	mosRedirect( "index3.php?option=$option&task=listprop_image&property=$row->property" );
}

function listprop_edit_images($option) {
  global $database;

	$id = mosGetParam( $_REQUEST, 'id', 0 );

	$row = new mosHPPhotos( $database );
	$row->load( $id );
	
	
}

/*****************
 * Property Type *
 ****************/

function listprop_type( $option ) {
	global $database, $mainframe;

	$limit = $mainframe->getUserStateFromRequest( "viewlistlimit", 'limit', 10 );
	$limitstart = $mainframe->getUserStateFromRequest( "viewcli{$option}limitstart", 'limitstart', 0 );

	# get the total number of records
	$database->setQuery( "SELECT count(*) FROM #__hp_prop_types" );
	$total = $database->loadResult();
	echo $database->getErrorMsg();

	require_once("includes/pageNavigation.php");
	$pageNav = new mosPageNav( $total, $limitstart, $limit );
	
	$sql = "SELECT t.*, COUNT(p.id) AS properties"
		. "\nFROM #__hp_prop_types AS t"
		. "\nLEFT JOIN #__hp_properties AS p ON (p.type=t.id AND p.published <> '-1')"
		. "\nGROUP BY t.id"
		. "\nORDER BY t.ordering ASC";

	$database->setQuery($sql);

	if(!$result = $database->query()) {
		echo $database->stderr();
		return false;
	}
	$rows = $database->loadObjectList();

	# Pour une liste indent�e des cat�gories:
	// establish the hierarchy of the hp_proptypes
	$children = array();
	// first pass - collect children
	foreach ($rows as $v ) {
		$pt = $v->parent;
		$list = @$children[$pt] ? $children[$pt] : array();
		array_push( $list, $v );
		$children[$pt] = $list;
	}
	// second pass - get an indent list of the items
	$list = mosTreeRecurse( 0, '', array(), $children ); // 0 permet de commencer la liste apr�s /root
	
	$total = count( $list );

	require_once( $GLOBALS['mosConfig_absolute_path'] . '/administrator/includes/pageNavigation.php' );
	$pageNav = new mosPageNav( $total, $limitstart, $limit  );

	// slice out elements based on limits
	$list = array_slice( $list, $pageNav->limitstart, $pageNav->limit );
	# FIN de la liste indent�e

	HTML_hotproperty::listprop_type( $list, $pageNav, $option );
}

function editprop_type( $id, $option ) {
	global $database;
	$row = new mosHPPropTypes($database);
	$row->load($id);

	HTML_hotproperty::editprop_type( $row, $lists, $option );
}

function saveprop_type( $option ) {
	global $database;

	$row = new mosHPPropTypes( $database );

	if (!$row->bind( $_POST )) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

	# Put new item to last
	if($row->id < 1) $row->ordering = 9999;

	# est-ce qu'il s'agit d'une nouvelle cat�gorie? (ou est-on seulement en train d'en modifier une)
	if($row->id == 0)
	{
		$new=true;
	}
	else
	{
		# On enregistre l'ancien parent pour pouvoir le comparer au nouveau et savoir si l'on doit moveUpdater :)
		$old_parent = new mosHPPropTypes( $database );
		$old_parent->load($row->id);
	}

	if (!$row->store()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

	# met � jour les bornes:
	if($new){ // pour la cr�ation d'une cat�gorie
		$row->addType();
	} else {
		if($old_parent->parent != $row->parent) // pour le d�placement d'une cat�gorie
			$row->moveType($row->parent);
	}
	
	# Update order
	$row->updateOrder();
	mosRedirect( "index2.php?option=$option&task=listprop_type" );
}

function order_type( $id, $inc, $option ) {
  global $database;

  # oop database connector
  $row = new mosHPPropTypes( $database );
	$row->load( $id );
	$row->move( $inc);
	
	mosRedirect( "index2.php?option=$option&task=listprop_type" );
}

function cancelprop_type( $option ) {
	mosRedirect( "index2.php?option=$option&task=listprop_type" );
}

function publish_prop_type( $id, $publish=1 ,$option ) {
	global $database, $my;

	if (!is_array( $id ) || count( $id ) < 1) {
		$action = $publish ? 'publish' : 'unpublish';
		echo "<script> alert('Select an item to $action'); window.history.go(-1);</script>\n";
		exit;
	}

	$ids = implode( ',', $id );

	$database->setQuery( "UPDATE #__hp_prop_types SET published='$publish'"
	. "\nWHERE id IN ($ids)"
	);
	if (!$database->query()) {
		echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
		exit();
	}

	mosRedirect( "index2.php?option=$option&task=listprop_type" );
}		

function removeprop_type( $ids, $option ) {
	global $database;

	$msg="";

	foreach(array_reverse($ids) as $id) {

		$row = new mosHPPropTypes($database);
		$row->load($id);
		
		# On s'assure que la cat�gorie ne contient r�cursivement plus de propri�t�
		if($row->countProps_R() != 0)
			mosRedirect( "index2.php?option=$option&task=listprop_type",
			"Cannot delete this property type as they recursively contain properties" );
	
		# On alerte si c'est un noeud:
		if( $row->rgt - $row->lft > 1 )
			$msg .= "$row->name, ";
		else
			$row->deleteType();

	}
	
	if(!empty($msg))
		$msg=$msg."ha(s/ve)n't been deleted as they contain children types. To delete them with, please all select them too.'";
	
	mosRedirect("index2.php?option=$option&task=listprop_type",	$msg);
}

/*****************
 * Extra Fields  *
 ****************/

function listprop_ef( $option ) {
	global $database, $mainframe, $ef_types;

	$limit = $mainframe->getUserStateFromRequest( "viewlistlimit", 'limit', 10 );
	$limitstart = $mainframe->getUserStateFromRequest( "viewcli{$option}limitstart", 'limitstart', 0 );

  # Retrieve show value (show core, non-core or ALL)
  $show_val = intval( mosGetParam( $_POST, 'show', -1 ) );

	# Setup the WHERE parameters
	$where = array();
  if ($show_val >= 0) $where[] = "iscore='$show_val'";

	# get the total number of records
	$database->setQuery( "SELECT count(*) FROM #__hp_prop_ef".(count( $where ) ? "\nWHERE " . implode( ' AND ', $where ) : "") );
	$total = $database->loadResult();
	echo $database->getErrorMsg();

  $show = array();
  $show[] = mosHTML::makeOption( '-1', 'ALL' );
  $show[] = mosHTML::makeOption( '1', 'Core fields only' );
  $show[] = mosHTML::makeOption( '0', 'Non-core fields only' );
  $lists["show"] = mosHTML::selectList( $show, 'show', 'class="inputbox" size="1" onchange="document.adminForm.submit();"', 'value', 'text', $show_val );

	require_once("includes/pageNavigation.php");
	$pageNav = new mosPageNav( $total, $limitstart, $limit );

	$sql = "SELECT ef.*"
		. "\nFROM #__hp_prop_ef AS ef"
		. (count( $where ) ? "\nWHERE " . implode( ' AND ', $where ) : "")
		. "\nORDER BY ef.iscore ASC, ef.ordering ASC"
		. "\nLIMIT $pageNav->limitstart,$pageNav->limit";

	$database->setQuery($sql);

	if(!$result = $database->query()) {
		echo $database->stderr();
		return false;
	}
	$rows = $database->loadObjectList();

	# Convert field type to more friendlier format
	$i = 0;
	foreach($rows AS $row) {
		if ( !empty($row->field_type) ) {
			$rows[$i]->field_type = $ef_types[$row->field_type];
		}
		$i++;
	}

	HTML_hotproperty::listprop_ef( $rows, $lists, $pageNav, $option );
}

function editprop_ef( $id, $option ) {
	global $database, $ef_types, $search_types;
	$row = new mosHPPropEF($database);
	$row->load($id);

	# Assign values for new ID
	if (!$id) {
		$row->published = 0;
		$row->iscore = 0;
		$row->hidden = 0;
		$row->listing = 0;
		$row->featured = 0;
	}
	
	# Generate Search Types
	foreach($search_types AS $search_type_key => $search_type_value) {
		$search_types2[] = mosHTML::makeOption( $search_type_key, $search_type_value );
	}
	$lists["search_types"] = mosHTML::selectList( $search_types2, 'search_type', 'class="inputbox" size="1"'.(($row->field_type <> 'text') ? ' disabled style=\"background-color:#F5F5F5\"' : ''),	'value', 'text', $row->search_type );

  # Generate the Field Types
	foreach($ef_types AS $ef_type_key => $ef_type_value) {
		$field_types[] = mosHTML::makeOption( $ef_type_key, $ef_type_value );
	}
	$lists["field_types"] = mosHTML::selectList( $field_types, 'field_type', 'class="inputbox" size="1" onchange="javascript:
ftype = document.forms[0].field_type.options[selectedIndex].value;
	if (ftype==\'selectlist\' || ftype==\'selectmultiple\' || ftype==\'checkbox\' || ftype==\'radiobutton\') {document.getElementById(\'field_element_table\').style.display = \'block\';	document.forms[0].prefix_text.disabled=true;	document.forms[0].append_text.disabled=true; document.forms[0].prefix_text.style.backgroundColor=\'#f5f5f5\'; document.forms[0].append_text.style.backgroundColor=\'#f5f5f5\';} 
else {document.getElementById(\'field_element_table\').style.display = \'none\'; 	document.forms[0].prefix_text.disabled=false;	document.forms[0].append_text.disabled=false;	document.forms[0].prefix_text.style.backgroundColor=\'#FFFFFF\'; document.forms[0].append_text.style.backgroundColor=\'#FFFFFF\'; 
}
if (ftype==\'text\') {
	document.forms[0].search_type.disabled=false;	
	document.forms[0].search_type.style.backgroundColor=\'#FFFFFF\'; 
} else {
	document.forms[0].search_type.value=\'0\';
	document.forms[0].search_type.disabled=true;	
	document.forms[0].search_type.style.backgroundColor=\'#f5f5f5\'; 
}"', 'value', 'text', $row->field_type );

	# make order list
	$orders = mosGetOrderingList( "SELECT ordering AS value, caption AS text"
		. "\nFROM #__hp_prop_ef"
		. "\nWHERE iscore='".$row->iscore."'"
		. "\nORDER BY ordering"
	);
	$lists["order"] = mosHTML::selectList( $orders, 'ordering', 'class="inputbox" size="1"',
	'value', 'text', intval( $row->ordering ) );
	
	# La liste des types:
	$database->setQuery("SELECT * FROM #__hp_prop_types");
	$types=$database->loadObjectList();
	
	# s�lection des types d�j� associ�s:
	if ( $id ) { // S'il s'agit d'un extra-field existant
		$query = "SELECT type AS value"
		. "\nFROM #__hp_prop_ef2"
		. "\nWHERE id = " . (int) $row->id;
		$database->setQuery( $query );
		$selected = $database->loadObjectList();
		if(empty($selected))
			$selected = array( mosHTML::makeOption( -1, 'None' ) );
	} else { // Si c'est un nouvel extra-field
		# Par d�faut on s�lectionne 'All'
		$selected = array( mosHTML::makeOption( 0, _HP_SEARCH_ALLTYPES ) );
	}
	
	HTML_hotproperty::editprop_ef( $row, $lists, $option, $types, $selected );
}

function saveprop_ef( $option ) {
	global $database;

	$row = new mosHPPropEF( $database );
	
	//Clean up field elements array
	//$_POST['field_elements'] = implode('|',mosGetParam($_POST,'field_elements',''));
	
	if (!$row->bind( $_POST )) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

	# Validate field's name
	if (substr_count($row->name," ") > 0) {
		echo "<script> alert('Please do not enter spaces in the field name.'); window.history.go(-1); </script>\n";
		exit();
	}

	# Validate field name contactins alphanumeric, _ and - only.
	if ( !eregi( "^[a-zA-Z0-9_-]+$", $row->name ) && $row->iscore == 0 ) {
		echo "<script> alert('Please enter alphabets, numbers, dash(-) and underscore(_) only for field name.'); window.history.go(-1); </script>\n";
		exit();
	}

	# Validate that this is not a duplicate field name
	$database->setQuery("SELECT * FROM #__hp_prop_ef WHERE name='".$row->name."' && id<>'".$row->id."'");
	$database->query();
	if($database->getNumRows() > 0) {
		echo "<script> alert('You seems to have another Extra Field with the same name. Please rename and try again.'); window.history.go(-1); </script>\n";
		exit();
	}

	# Successively remove '|' at the start and end to eliminate blank options
	while (substr($row->field_elements, -1) == '|') {
		$row->field_elements = substr($row->field_elements, 0, -1);
	}
	while (substr($row->field_elements, 0, 1) == '|') {
		$row->field_elements = substr($row->field_elements, 1);
	}

	# Clean up Field Elements's data. Remove spaces around '|' so that it is used correctly in SET COLUMN in MySQL
	//$tmp_fe_array = explode('|',$row->field_elements);
	$tmp_fe_array2 = array();
	//unset($row->field_elements);
	foreach($row->field_elements AS $tmp_fe) {
		# Detect usage of comma.
		//if (strrpos($tmp_fe,',') == FALSE) 
		//{
		if($tmp_fe){
			$tmp_fe_array2[] = htmlspecialchars(trim($tmp_fe), ENT_QUOTES);
		}
			//$tmp_fe_array2[] = trim($tmp_fe);

		//} else {
		//	echo "<script> alert('Commas are not allowed in field elements. Please remove the comma and try again.'); window.history.go(-1); </script>\n";
		//	exit();
		//}
	}
	$row->field_elements = implode('|',$tmp_fe_array2);
	
	# 'Name' field is always 'published', 'featured', and 'listing'
	if ($row->id == '1') {
		$row->published = '1';
		$row->featured = '1';
		$row->listing = '1';
	}

	# Put new item to last
	if($row->id <= 0) $row->ordering = 9999;

	if (!$row->store()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	$row->updateOrder("iscore='$row->iscore'");

	# On choppe la liste des types s�lectionn�s(->cf: admin.modules.php l.271):
	$reltypes 	= josGetArrayInts( 'reltypes' );
	/*echo "<pre>";
	echo "reltypes=";
	print_r($reltypes);
	echo "</pre>";*/

	# On  efface les anciennes associations de l'extra-field consid�r�:
	$row->deleteRelTypes();
	
	# � partir du moment o� 'All' appara�t dans les types s�lectionn�s, on se contente d'associer l'ef � la cat�gorie d'id 0 (/root)
	if ( in_array( '0', $reltypes ) ) {
		$row->addRelType(0);
	# ou qu'� partir du moment ou 'None' ou que '----' appara�t seul
	} elseif ( in_array( '-1', $reltypes ) || ( count($reltypes)==1 && in_array( '-999', $reltypes ) ) ) {
		// on fait rien, tout a d�j� �t� supprim�
	} else {
		# Pour chaque type s�lectionn� dans la liste, on l'ajoute � la table #__hp_prop_ef2:
		foreach ($reltypes as $reltype){
			if($reltype != "-999") // On prend pas les '----'
			{
				// nouvelle association:
				$row->addRelType($reltype);
			}
		}
	}
	
	mosRedirect( "index2.php?option=$option&task=listprop_ef" );
}

function cancelprop_ef( $option ) {
	mosRedirect( "index2.php?option=$option&task=listprop_ef" );
}

function removeprop_ef( $id, $option ) {
	global $database;

	for ($i = 0; $i < count($id); $i++) {
		$query = "SELECT iscore FROM #__hp_prop_ef WHERE id='".$id[$i]."' LIMIT 1";
		$database->setQuery($query);

		if(($iscore = $database->loadResult()) == null) {
			echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
		}

		if ($iscore == 1) {
			mosRedirect( "index2.php?option=$option&task=listprop_ef",
			"Cannot delete core property's field" );
		} else {
			# Delete the main fields data
			$query="DELETE FROM #__hp_prop_ef WHERE `id`='".$id[$i]."'";
			$database->setQuery($query);
			$database->query();

			# Delete the data associated with this field
			$query="DELETE FROM #__hp_properties2 WHERE `field`='".$id[$i]."'";
			$database->setQuery($query);
			$database->query();
		}
	}
	mosRedirect("index2.php?option=$option&task=listprop_ef");
}

function togglefeatured_ef($id, $option) {
	global $database;
	$row = new mosHPPropEF( $database );
	$row->toggleFeatured($id);

	mosRedirect( "index2.php?option=$option&task=listprop_ef" );
}

function togglelisting_ef($id, $option) {
	global $database;
	$row = new mosHPPropEF( $database );
	$row->toggleListing($id);

	mosRedirect( "index2.php?option=$option&task=listprop_ef" );
}

function publish_prop_ef( $id, $publish=1 ,$option ) {
	global $database, $my;

	if (!is_array( $id ) || count( $id ) < 1) {
		$action = $publish ? 'publish' : 'unpublish';
		echo "<script> alert('Select an item to $action'); window.history.go(-1);</script>\n";
		exit;
	}

	$ids = implode( ',', $id );

	$database->setQuery( "UPDATE #__hp_prop_ef SET published='$publish'"
	. "\nWHERE id IN ($ids)"
	);
	if (!$database->query()) {
		echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
		exit();
	}

	mosRedirect( "index2.php?option=$option&task=listprop_ef" );
}

function order_ef( $id, $inc, $option ) {
  global $database;

	# oop database connector
	$row = new mosHPPropEF( $database );
	$row->load( $id );
	$row->move( $inc, "iscore='$row->iscore'" );

	mosRedirect( "index2.php?option=$option&task=listprop_ef" );
}

/******************
 * Featured Item  *
 *****************/
function listfeatured( $option ) {
	global $database, $mainframe;

	$limit = $mainframe->getUserStateFromRequest( "viewlistlimit", 'limit', 10 );
	$limitstart = $mainframe->getUserStateFromRequest( "viewcli{$option}limitstart", 'limitstart', 0 );

	# get the total number of records
	$database->setQuery( "SELECT count(*) FROM #__hp_featured" );
	$total = $database->loadResult();
	echo $database->getErrorMsg();

	require_once("includes/pageNavigation.php");
	$pageNav = new mosPageNav( $total, $limitstart, $limit );

	$sql = "SELECT p.*, c.name AS company, a.name AS agent, t.name AS type"
		. "\nFROM #__hp_properties AS p"
		. "\nLEFT JOIN #__hp_agents AS a ON a.id=p.agent"
		. "\nLEFT JOIN #__hp_companies AS c ON c.id=a.company"
		. "\nLEFT JOIN #__hp_prop_types AS t ON t.id=p.type"
		. "\nLEFT JOIN #__hp_featured AS f ON f.property=p.id"
		. "\nWHERE p.featured=1"
		. "\nORDER BY f.ordering ASC"
		. "\nLIMIT $pageNav->limitstart,$pageNav->limit";

	$database->setQuery($sql);

	if(!$result = $database->query()) {
		echo $database->stderr();
		return false;
	}
	$rows = $database->loadObjectList();

	HTML_hotproperty::listfeatured( $rows, $pageNav, $option );
}

function order_featured( $id, $inc, $option ) {
  global $database;

  # oop database connector
  $row = new mosHPFeatured( $database );
	$row->load( $id );
	$row->move( $inc);
	
	mosRedirect( "index2.php?option=$option&task=featured" );
}

function publish_featured( $id, $publish=1 ,$option )
{
	global $database, $my;

	if (!is_array( $id ) || count( $id ) < 1) {
		$action = $publish ? 'publish' : 'unpublish';
		echo "<script> alert('Select an item to $action'); window.history.go(-1);</script>\n";
		exit;
	}

	$ids = implode( ',', $id );

	$database->setQuery( "UPDATE #__hp_properties SET published='$publish'"
	. "\nWHERE id IN ($ids)"
	);
	if (!$database->query()) {
		echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
		exit();
	}

	mosRedirect( "index2.php?option=$option&task=featured" );

}

function remove_featured($id, $option) {
	global $database;

	# Update property table
	$row = new mosHPFeatured( $database );
	$row->toggleFeatured($id);

	# Update Featured table
	if ($feature) {
		foreach ($id AS $id_one) {
			$fp = new mosHPFeatured($database);
			if (!$fp->load( $id_one )) {
				// new entry
				$database->setQuery( "INSERT INTO #__hp_featured VALUES ('$id_one','1')" );
				if (!$database->query()) {
					echo "<script> alert('".$database->stderr()."');</script>\n";
					exit();
				}
				$fp->ordering = 1;
			}
			$fp->updateOrder();
		} // End foreach
	} else {
		foreach ($id AS $id_one) {
			$fp = new mosHPFeatured($database);
			if (!$fp->delete( $id_one )) {
				$msg .= $fp->stderr();
			}
			$fp->ordering = 0;
			$fp->updateOrder();
		} // End foreach
	}

	mosRedirect( "index2.php?option=$option&task=featured" );
}

/*****************
 * Log Searches  *
 ****************/

function log_searches($option) {
	global $database, $mainframe;

	# Pagination
	$database->setQuery("SELECT COUNT(*) FROM #__hp_log_searches");
	$total = $database->loadResult();
	$limit = $mainframe->getUserStateFromRequest( "viewlistlimit", 'limit', 10 );
	$limitstart = $mainframe->getUserStateFromRequest( "viewcli{$option}limitstart", 'limitstart', 0 );
	require_once("includes/pageNavigation.php");
	$pageNav = new mosPageNav( $total, $limitstart, $limit );

	# Load search text's gog
	$database->setQuery("SELECT * FROM #__hp_log_searches ORDER BY hits DESC LIMIT ".$limitstart.", ".$limit);
	$rows = $database->loadObjectList();

	HTML_hotproperty::log_searches( $rows, $pageNav, $option );
}

/*****************
 *   Reporting   *
 ****************/

function report($option) {
	global $database;

	$sql = "SELECT name, caption, iscore, size, field_elements, field_type, search_caption, search_type "
		. "\n FROM #__hp_prop_ef"
		.	"\n ORDER BY iscore DESC, ordering ASC";
	$database->setQuery($sql);
	$fields = $database->loadObjectList();

	HTML_hotproperty::report( $fields, $option );
}

function doreport($option) {
	global $database, $mainframe;

	$fields = mosGetParam( $_REQUEST, 'fields');
	$show_all = mosGetParam( $_REQUEST, 'show_all', '1');
	$report_options['nowrap'] = mosGetParam( $_REQUEST, 'nowrap', '0');
	$report_options['show_fulltext'] = mosGetParam( $_REQUEST, 'show_fulltext', '0');

	# Gather the requested fields' attributes (iscore, name, caption)
	$sql = "SELECT name, caption, iscore "
		. "\n FROM #__hp_prop_ef"
		.	"\n WHERE name='" . ((count($fields) > 1) ? implode("' OR name='", $fields) : $fields[0])."'"
		.	"\n ORDER BY iscore DESC, ordering ASC";
	$database->setQuery($sql);
	$fields2 = $database->loadObjectList();	

	# Drop temp2 table
	$database->setQuery("DROP TABLE IF EXISTS #__hp_temp2");
	$database->query();

	# Create Table
	$sql = "CREATE TABLE #__hp_temp2 ("
		."\n id INT(11) NOT NULL, ";

	foreach ($fields2 AS $field2) {
		$sql .= "\n".$field2->name." MEDIUMTEXT NOT NULL, ";
	}
	$sql .= "\nPRIMARY KEY  (id)"
		. "\n) TYPE=MyISAM ;";

	$database->setQUery($sql);
	if (!$database->query()) 
	{
		echo $database->getErrorMsg();
	}

	# Populating core field's data
	$sql2 = "INSERT INTO #__hp_temp2 (`id`";

	foreach ($fields2 AS $col) {
		if ($col->iscore) {
			$sql2 .= ", `".$col->name."`";
		}
	}
	$sql2 .= ") \n SELECT p.id";
	foreach ($fields2 AS $col) {
		if ($col->iscore) {
			if ($col->name == 'company') 
			{
				$sql2 .= ", c.name AS company";
			} elseif ($col->name == 'agent') {
				$sql2 .= ", a.name AS agent";
			} elseif ($col->name == 'type') {
				$sql2 .= ", t.name AS type";
			} else {
				if ($col->name == 'notes') {
					$sql2 .= ", p.note";  // Inconsistency in Hidden Note name. Fix.
				} else {
					$sql2 .= ", p.".$col->name;
				}
			}
		}
	}

	$sql2 .= "\n FROM (#__hp_properties AS p, #__hp_companies AS c)"
		. "\n LEFT JOIN #__hp_prop_types AS t ON p.type = t.id"
		. "\n LEFT JOIN #__hp_agents AS a ON p.agent = a.id"
		.	"\n WHERE a.company=c.id";

	if ($show_all == '0') {
		$sql2 .=  "\n AND p.published='1'"
			. "\n	AND (publish_up = '0000-00-00 00:00:00' OR publish_up <= NOW())"
			. "\n	AND (publish_down = '0000-00-00 00:00:00' OR publish_down >= NOW())";
	}

	$database->setQuery($sql2);
	if (!$database->query()) {
		echo $database->getErrorMsg();
	}

	# Populating non-core field's data
	$sql3 = "SELECT e.name, e.field_type, p.property, p.field, p.value FROM (#__hp_properties2 AS p, #__hp_prop_ef AS e)"
		.	"\nWHERE e.id=p.field";

	$database->setQuery($sql3);
	$rows3 = $database->loadObjectList();
	
	foreach ($rows3 AS $row3) {
		if ( in_array($row3->name,$fields) ) {
			$sql4 = "UPDATE #__hp_temp2 SET ".$row3->name."='".$row3->value."' WHERE id='".$row3->property."'";
			$database->setQuery($sql4);
			$database->query();
		}
	}

	# Page Navigation
	$limit = $mainframe->getUserStateFromRequest( "viewlistlimit", 'limit', 25 );
	$limitstart = $mainframe->getUserStateFromRequest( "viewcli{$option}limitstart", 'limitstart', 0 );

	$database->setQuery( "SELECT count(*) FROM #__hp_temp2" );
	$total = $database->loadResult();
	echo $database->getErrorMsg();

	require_once("includes/pageNavigation.php");
	$pageNav = new mosPageNav( $total, $limitstart, $limit );

	# Get the meat
//	$sql5 = "SELECT * FROM #__hp_temp2 ORDER BY id LIMIT $limitstart, $limit";
	$sql5 = "SELECT * FROM #__hp_temp2 ORDER BY ".((in_array('name', $fields)) ? 'name' : 'id' )." LIMIT $limitstart, $limit";
	$database->setQuery($sql5);
	$rows = $database->loadObjectList();

	HTML_hotproperty::doreport( $fields, $fields2, $rows, $pageNav, $report_options, $option );
}

function doreport_print($option) {
	global $database;

	$report_options['nowrap'] = intval( mosGetParam( $_REQUEST, 'nowrap', '0') );
	$report_options['show_fulltext'] = intval( mosGetParam( $_REQUEST, 'show_fulltext', '0') );

	# Retrieve the caption
	$sql = "DESCRIBE #__hp_temp2";
	$database->setQuery($sql);
	$fields = $database->loadObjectList('Field');

	foreach( $fields AS $field ) {
		if ($field->Field <> 'id') {
			$sql = "SELECT caption FROM #__hp_prop_ef WHERE name = '".$field->Field."' LIMIT 1";
			$database->setQuery($sql);
			$fields2[$field->Field] = $database->loadResult();
		}
	}

	# Get the meat
	$sql = "SELECT * FROM #__hp_temp2 ORDER BY ".((array_search('Name', $fields2)) ? 'name' : 'id' );
	$database->setQuery($sql);
	$rows = $database->loadObjectList();

	HTML_hotproperty::doreport_print( $fields2, $rows, $report_options, $option );
}

function doreport_excel($option) {
	global $database;
	
	$seperator = ',';
	# Retrieve the caption
	$sql = "DESCRIBE #__hp_temp2";
	$database->setQuery($sql);
	$fields = $database->loadObjectList('Field');

	foreach( $fields AS $field ) {
		if ($field->Field <> 'id') {
			$sql = "SELECT caption FROM #__hp_prop_ef WHERE name = '".$field->Field."' LIMIT 1";
			$database->setQuery($sql);
			$fields2[$field->Field] = $database->loadResult();
		}
	}

	# Get the meat
	$sql = "SELECT * FROM #__hp_temp2 ORDER BY ".((array_search('Name', $fields2)) ? 'name' : 'id' );
	$database->setQuery($sql);
	$rows = $database->loadObjectList();
	
	# Create the .CSV
	$header = '';
	foreach ($fields2 AS $field) {
			$header .= $field.$seperator;
	}
	$header .= "\n";

	foreach($rows AS $row) {
		$line = '';
		$j = 0;
		foreach($row as $value){
			if ($j > 0) {
				if(!isset($value) || $value == ""){
					//$value = $seperator;
				}else{
					$value = str_replace('"', '""', $value);
					$value = '"' . $value . '"'; // . $seperator;
				}
				$line .= $value;
//				if ($j <= count($row)) {
					$line .= $seperator;
//				}
			}
			$j++;
		}
		$data .= trim($line)."\n";
	}
	# this line is needed because returns embedded in the data have "\r"
	# and this looks like a "box character" in Excel
		$data = str_replace("\r", "", $data);

//	echo $header ."<br />";
//	echo nl2br($data);

	HTML_hotproperty::doreport_excel( $header, $data, $option );
}

/*****************
 * Configuration *
 ****************/

function editConfig( $option ) {
  global $database;

	$row = new hpConfig();
	$row->bindGlobals();

  $configfile = "components/com_hotproperty/config.hotproperty.php";
  @chmod ($configfile, 0766);
  $permission = is_writable($configfile);
  if (!$permission) {
     echo "<center><h1><font color=red>Warning...</FONT></h1><BR>";
     echo "<B>Your config file is /administrator/$configfile</b><BR>";
     echo "<B>You need to chmod this to 766 in order for the config to be updated</B></center><BR><BR>";
  }

	if ( !is_numeric($row->fp_featured_count) ) $row->fp_featured_count = 0;

	# Detect Image Libraries available
	$imageLibs=array();
	$imageLibs=detect_ImageLibs();

	# compile list of the languages
	$langs = array();
	$menuitems = array();

	if ($handle=opendir("../components/com_hotproperty/language/")) {
		$i=0;
		while ($file = readdir($handle)) {
			if (!strcasecmp(substr($file,-4),".php") && $file <> "." && $file <> ".." && strcasecmp(substr($file,-11),".ignore.php")) {
				$langs[] = mosHTML::makeOption( substr($file,0,-4) );
			}
		}
	}

	# sort list of languages
	sort($langs);
	reset($langs);

	# compile list of CSS files
	$css = array();
	$menuitems = array();

	if ($handle=opendir("../components/com_hotproperty/css/")) {
		$i=0;
		while ($file = readdir($handle)) {
			if (!strcasecmp(substr($file,-4),".css") && $file <> "." && $file <> "..") {
				$css[] = mosHTML::makeOption( substr($file,0,-4) );
			}
		}
	}

	# sort list of CSS files
	sort($css);
	reset($css);

  # Get the list of agent (companies)
  $agents[] = mosHTML::makeOption( '0', 'Select Agent' );
  $database->setQuery( "SELECT a.id AS id, a.name AS name, c.name AS company FROM #__hp_agents AS a"
		. "\nLEFT JOIN #__hp_companies AS c ON c.id=a.company"
    . "\nORDER BY a.name ASC, c.name ASC" );
	$row_agents = $database->loadObjectList();
	foreach($row_agents AS $row_agent) {
	  $agents[] = mosHTML::makeOption( $row_agent->id, $row_agent->name." (".$row_agent->company.")" );
	}

	# Get the list of companies
	$companies[] = mosHTML::makeOption( '0', 'Select Company' );
  $database->setQuery( "SELECT c.id AS id, c.name AS name FROM #__hp_companies AS c"
    . "\nORDER BY c.name ASC" );
	$row_companies = $database->loadObjectList();
	foreach($row_companies AS $row_company) {
	  $companies[] = mosHTML::makeOption( $row_company->id, $row_company->name);
	}


	# compile list of ordering
	$order = array();
	$order[] = mosHTML::makeOption( "price", "Price" );
	$order[] = mosHTML::makeOption( "name", "Name" );

	$order2 = array();
	$order2[] = mosHTML::makeOption( "desc", "Descending");
	$order2[] = mosHTML::makeOption( "asc", "Ascending");

	# build the html select list

	$lists['lang'] = mosHTML::selectList( $langs, 'language', 'class="inputbox" size="1"',
	'value', 'text', $row->language );
	$lists['css'] = mosHTML::selectList( $css, 'css', 'class="inputbox" size="1"',
	'value', 'text', $row->css );
	$lists['order'] = mosHTML::selectList( $order, 'default_order', 'class="inputbox" size="1"',
	'value', 'text', $row->default_order );
	$lists['order2'] = mosHTML::selectList( $order2, 'default_order2', 'class="inputbox" size="1"',
	'value', 'text', $row->default_order2 );
    $lists['default_agent'] = mosHTML::selectList( $agents, 'default_agent', 'class="inputbox" size="1" '.(($row->use_companyagent == '1') ? 'disabled ' : ''), 'value', 'text', $row->default_agent );
	$lists['default_company'] = mosHTML::selectList( $companies, 'default_company', 'class="inputbox" size="1" ', 'value', 'text', $row->default_company );

	# Get the caption names for all the available sorting options
	$sql = "SELECT caption, name FROM #__hp_prop_ef WHERE iscore='1'";
	$database->setQuery($sql);
	$captions = $database->loadObjectList('name');

  HTML_hotproperty::showConfig( $row, $lists, $imageLibs, $captions, $option );
}

function detect_ImageLibs() {
	$imageLibs=array();

	# Initialization - To allow Windows machine to do proper detection
	$shell_cmd='';
	if(substr(PHP_OS, 0, 3) == 'WIN') $shell_cmd = getenv( "COMSPEC" ) . " /C ";
	unset($output);

	# Detect Imagemagick
	@exec($shell_cmd.'convert -version',  $output, $status);
	if(!$status){
			if(preg_match("/imagemagick[ \t]+([0-9\.]+)/i",$output[0],$matches))
				 $imageLibs['imagemagick'] = $matches[0];
	}

	#Detect Netpbm
	unset($output);
	@exec($shell_cmd. 'jpegtopnm -version 2>&1',  $output, $status);
	if(!$status){
			if(preg_match("/netpbm[ \t]+([0-9\.]+)/i",$output[0],$matches))
				 $imageLibs['netpbm'] = $matches[0];
	}

	#Detect GD1/GD2
	$GDfuncList = get_extension_funcs('gd');

	ob_start();
	@phpinfo(INFO_MODULES);
	$output=ob_get_contents();
	ob_end_clean();
	$matches[1]='';
	if(preg_match("/GD Version[ \t]*(<[^>]+>[ \t]*)+([^<>]+)/s",$output,$matches)){
			$gdversion = $matches[2];
	}

	if( $GDfuncList ){
	 if( trim($gdversion) == '1.6.2 or higher' ) {
			$imageLibs['gd1'] = $gdversion;
	 } else {
			$imageLibs['gd2'] = $gdversion;
	 }
	}
	/*if( $GDfuncList ){
	 if( in_array('imagegd2',$GDfuncList) )
			$imageLibs['gd2'] = $gdversion;
	 else
			$imageLibs['gd1'] = $gdversion;
	}*/

	return $imageLibs;
}

function saveConfig($option) {
	$row = new hpConfig();
	if (!$row->bind( $_POST )) {
		mosRedirect( "index2.php", $row->getError() );
	}

	$config = "<?php\n";
	$config .= $row->getVarText();
	$config .= "?>";

	if ($fp = fopen("components/com_hotproperty/config.hotproperty.php", "w")) {
		fputs($fp, $config, strlen($config));
		fclose ($fp);
		mosRedirect( "index2.php?option=$option&task=config", "The configuration details have been updated!" );
	} else {
		mosRedirect( "index2.php?option=$option&task=config", "An Error Has Occurred! Unable to open config file to write!" );
	}
}

?>
