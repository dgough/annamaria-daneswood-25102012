<?php
// $Id: admin.hotproperty.class.php
/**
* Hot Property Main Class
*
* @package Hot Property 0.9
* @copyright (C) 2004 Lee Cher Yeong
* @url http://www.Mosets.com/
* @author Lee Cher Yeong <cy@mosets.com>
**/

// ensure this file is being included by a parent file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );
define ('hp_version',"0.98");

/**
* Companies table class
*/
class mosHPCompanies extends mosDBTable {
	var $id=null;
	var $name=null;
	var $address=null;
	var $suburb=null;
	var $state=null;
	var $country=null;
	var $postcode=null;
	var $telephone=null;
	var $fax=null;
	var $email=null;
	var $website=null;
	var $photo=null;
	var $desc=null;

	function mosHPCompanies( &$db ) {
		$this->mosDBTable( '#__hp_companies', 'id', $db );
	}
}

/**
* Agents table class
*/
class mosHPAgents extends mosDBTable {
	var $id=null;
	var $user=null;
	var $name=null;
	var $mobile=null;
	var $email=null;
	var $photo=null;
	var $company=null;
	var $desc=null;
	var $need_approval=null;

	function mosHPAgents( &$db ) {
		$this->mosDBTable( '#__hp_agents', 'id', $db );
	}
}

/**
* Properties table class
*/
class mosHPProperties extends mosDBTable {
	var $id=null;
	var $name=null;
	var $agent=null;
	var $type=null;
	var $address=null;
	var $suburb=null;
	var $state=null;
	var $country=null;
	var $postcode=null;
	var $price=0;
	var $note=null;
	var $intro_text=null;
	var $full_text=null;
	var $featured=0;
	var $published=null;
	var $publish_up=null;
	var $publish_down=null;
	var $created=null;
	var $modified=null;
	var $approved=null;
	var $metakey=null;
	var $metadesc=null;
	var $hits=null;

	function mosHPProperties( &$db ) {
		$this->mosDBTable( '#__hp_properties', 'id', $db );
	}
}

/**
* Featured table class
*/
class mosHPFeatured extends mosDBTable {
	var $property=null;
	var $ordering=null;

	function mosHPFeatured( &$db ) {
		$this->mosDBTable( '#__hp_featured', 'property', $db );
	}

	function toggleFeatured( $ids ) {
		foreach ($ids AS $id) {
			# Get current state
			$this->_db->setQuery("SELECT featured FROM #__hp_properties WHERE id='".$id."'");
			if($this->_db->loadResult() == 0) {
				$this->_db->setQuery("UPDATE #__hp_properties SET featured=1 WHERE id='".$id."'");
			} else {
				$this->_db->setQuery("UPDATE #__hp_properties SET featured=0 WHERE id='".$id."'");
			}
			$this->_db->query();
		}
	}

}

/**
* Property Types table class
*/
class mosHPPropTypes extends mosDBTable {
	var $id=null;
	var $parent=null;  // id parent
	var $name=null;
	var $desc=null;
	var $published=null;
	var $ordering=null;
	var $lft=null;  // Borne gauche
	var $rgt=null;  // Borne droite

	function mosHPPropTypes( &$db ) {
		$this->mosDBTable( '#__hp_prop_types', 'id', $db );
	}
	
	/**
	* Ajout d'une cat�gorie
	*   -> http://sqlpro.developpez.com/cours/arborescence/#L2.11
	* ---------------------------------------------------------------
	* [@param] $included=true/false : cat�gorie incluse ou non
	*/
	function addType() {
		global $database;
		
		# On charge le p�re:
		$parent = new mosHPPropTypes($database);
		$parent->load($this->parent);
		
		# Tous les �l�ments � droite du p�re(lui compris) sont incr�ment�s de 2 pour laisser la place � la nouvelle cat�gorie(cf. http://sqlpro.developpez.com/cours/arborescence/images/SQLtree3.gif)
		$this->_db->setQuery("UPDATE #__hp_prop_types SET lft = lft+2 WHERE lft >= $parent->rgt");
		if (!$this->_db->query()) {
				echo "<script> alert('".$this->_db->getErrorMsg()."'); window.history.go(-1); </script>\n";
				return false;
		}
		$this->_db->setQuery("UPDATE #__hp_prop_types SET rgt = rgt+2 WHERE rgt >= $parent->rgt");
		if (!$this->_db->query()) {
				echo "<script> alert('".$this->_db->getErrorMsg()."'); window.history.go(-1); </script>\n";
				return false;
		}

		# On ins�re la cat�gorie � droite
		$this->_db->setQuery("UPDATE #__hp_prop_types SET lft = ".$parent->rgt.", rgt = ".($parent->rgt+1)." WHERE id = $this->id");
		if (!$this->_db->query()) {
				echo "<script> alert('".$this->_db->getErrorMsg()."'); window.history.go(-1); </script>\n";
				return false;
		}
		$this->lft = $parent->rgt;
		$this->rgt = $parent->rgt+1;

		return true;
	}
	
	/**
	* Retourne une liste d'objets de tous les parents
	*   -> http://sqlpro.developpez.com/cours/arborescence/#L2.7
	* ---------------------------------------------------------------
	* [@param] $included=true/false : cat�gorie incluse ou non
	* [@param] $order="ASC"/"DESC" : trier la liste par 'lft' croissant/d�croissant
	*/
	function parentsType($included=true, $order=null)
	{	
		if(isset($order))
			$order=" ORDER BY lft ".$order;
		($included) ? $query="SELECT * FROM #__hp_prop_types WHERE lft <= $this->lft AND rgt >= $this->rgt".$order
		            : $query="SELECT * FROM #__hp_prop_types WHERE lft < $this->lft AND rgt > $this->rgt".$order;
		$this->_db->setQuery($query);
		if (!$this->_db->query())
		{
				echo "<script> alert('".$this->_db->getErrorMsg()."'); window.history.go(-1); </script>\n";
				return false;
		}
		return $this->_db->loadObjectList();
	}
	
	/**
	* Retourne une liste d'objets de toutes les cat�gories directement enfants
	* ---------------------------------------------------------------
	* [@param] $published=1/0 : pr�cision optionnelle quant � l'�tat(publi�/non-publi�) des cat�gories
	*/
	function childrenType($published=null)
	{	
		if(isset($published))
			$where=" AND published=".$published;
		$query = "SELECT * FROM #__hp_prop_types WHERE parent=".$this->id."".$where;
		$this->_db->setQuery($query);
		if (!$this->_db->query())
		{
				echo "<script> alert('".$this->_db->getErrorMsg()."'); window.history.go(-1); </script>\n";
				return false;
		}
		return $this->_db->loadObjectList();
	}
	
	/**
	* Retourne une liste d'objets de toutes les cat�gories enfants
	*   -> http://sqlpro.developpez.com/cours/arborescence/#L2.5
	* ---------------------------------------------------------------
	* [@param] $included=true/false : cat�gorie incluse ou non
	* [@param] $published=1/0 : pr�cision optionnelle quant � l'�tat(publi�/non-publi�) des cat�gories
	*/
	function childrenType_R($included=true, $published=null)
	{	
		if (isset($published))
			$where=" AND published=".$published;
		
		($included) ? $query="SELECT * FROM #__hp_prop_types WHERE lft >= $this->lft AND rgt <= $this->rgt".$where
		            : $query="SELECT * FROM #__hp_prop_types WHERE lft > $this->lft AND rgt < $this->rgt".$where;
		$this->_db->setQuery($query);
		if (!$this->_db->query())
		{
				echo "<script> alert('".$this->_db->getErrorMsg()."'); window.history.go(-1); </script>\n";
				return false;
		}
		return $this->_db->loadObjectList();
	}
		
	/**
	* Nombre de cat�gories directement enfants
	* ---------------------------------------------------------------
	* [@param] $published=1/0 : pr�cision optionnelle quant � l'�tat(publi�/non-publi�) des cat�gories
	*/
	function countChildrenType($published=null)
	{
		return count($this->childrenType($published));
	}
	
	/**
	* Nombre de cat�gories enfants(en R�cursif)
	* ---------------------------------------------------------------
	* [@param] $included=true/false : cat�gorie incluse ou non
	* [@param] $published=1/0 : pr�cision optionnelle quant � l'�tat(publi�/non-publi�) des cat�gories
	*/
	function countChildrenType_R($included=true, $published=null)
	{
		return count($this->childrenType_R($included, $published));
	}
	
	/**
	* Nombre de propri�t�s
	* ---------------------------------------------------------------
	* [@param] $published=1/0 : pr�cision optionnelle quant � l'�tat(publi�/non-publi�) des propri�t�s
	*/
	function countProps($published=null)
	{
		if(isset($published))
			$where=" AND published=".$published;
		$this->_db->setQuery("SELECT COUNT(id) FROM #__hp_properties WHERE type=$this->id".$where);
		if (!$this->_db->query())
		{
			echo "<script> alert('".$this->_db->getErrorMsg()."'); window.history.go(-1); </script>\n";
			return false;
		}
		
		return $this->_db->loadResult();
	}
	
	/**
	* Nombre de propri�t�s en R�cursif
	* ---------------------------------------------------------------
	* [@param] $included=true/false : cat�gorie incluse ou non
	* [@param] $published=1/0 : pr�cision optionnelle quant � l'�tat(publi�/non-publi�) des propri�t�s
	* [@param] $forced=true/false : forcer le comptage des sous-propri�t�s au travers d'une cat�gorie qui n'est pas dans le m�me �tat que $published(bon ca n'a pas tellement de sens de forcer le comptage les propri�t�s d�publi�es au travers des cat�gories publi�es mais bon...l'inverse n'est pas vrai, et puis ca fait style d'expliquer des choses qui n'ont pas de sens :p)
	*/
	function countProps_R($included=true, $published=null, $forced=true)
	{
		global $database;
		
		$s=0;
		foreach($this->childrenType_R($included, $published) as $child)
		{
			$row = new mosHPPropTypes($database);
			$row->load($child->id);
			
			if(!$forced && isset($published))
			{	# Afin de ne pas comptabiliser les sous-propri�t�s d'une cat�gorie (!$published)�e...
				$court_circuit=false;
				foreach($row->parentsType() as $parent)
				{
					# ...On remonte la hi�rarchie, voir si aucun de ses parents ne serait (!$published)�
					if($parent->published != $published)
					{
						$court_circuit=true;
						break;
					}
				}
				if(!$court_circuit)
					$s += $row->countProps($published);
			}
			else
			{
				$s += $row->countProps($published);
			}
				
		}
		return $s;
	}

	/**
	* Suppression d'une cat�gorie
	*   -> http://sqlpro.developpez.com/cours/arborescence/#L2.12
	* ---------------------------------------------------------------
	*/
	function deleteType()
	{
		# On s'assure qu'il s'agit d'une feuille:
		if( $this->rgt - $this->lft <= 1 )
		{
			# On la supprime:
			$this->_db->setQuery("DELETE FROM #__hp_prop_types WHERE lft = $this->lft");
			if (!$this->_db->query())
			{
				echo "<script> alert('".$this->_db->getErrorMsg()."'); window.history.go(-1); </script>\n";
				return false;
			}
			
			# On re-num�rote la partie droite de l'arbre:
			$this->_db->setQuery("UPDATE #__hp_prop_types SET lft = lft-2 WHERE lft >= $this->lft");
			if (!$this->_db->query())
			{
				echo "<script> alert('".$this->_db->getErrorMsg()."'); window.history.go(-1); </script>\n";
				return false;
			}
			$this->_db->setQuery("UPDATE #__hp_prop_types SET rgt = rgt-2 WHERE rgt >= $this->rgt");
			if (!$this->_db->query())
			{
				echo "<script> alert('".$this->_db->getErrorMsg()."'); window.history.go(-1); </script>\n";
				return false;
			}
			
			# On supprime les associations li�es � cette cat�gorie:
			$this->_db->setQuery("DELETE FROM #__hp_prop_ef2 WHERE type = $this->id");
			if (!$this->_db->query())
			{
				echo "<script> alert('".$this->_db->getErrorMsg()."'); window.history.go(-1); </script>\n";
				return false;
			}
			
			return true;
		}
		else
		{
			return false;	
		}
	}
	
	/***
	* D�placement d'une cat�gorie vers le parent d'id $cid
	*/
	function moveType($cid)
	{
		global $database;
		
		$new_parent = new mosHPPropTypes( $database );	
		$new_parent->load($cid);
		
		$nbr_subcats=count($this->childrenType_R());
		
		#la largeur du bloc de cat�gories � d�placer:
		$inc=$new_parent->rgt - $this->lft;
		
		# D�placement vers la droite
		if ( $this->rgt < $new_parent->rgt ) {
	
			# En gros: on fait de la place!
			# Toutes les cat�gories � droite du nouveau p�re(lui compris) sont incr�ment�es de 2 fois le nombre de sous cat�gories de la cat�gorie mouvante pour leur faire la place
			$this->_db->setQuery("UPDATE #__hp_prop_types SET lft = lft+".(2*$nbr_subcats)." WHERE lft >= $new_parent->rgt");
			$this->_db->query();
			$this->_db->setQuery("UPDATE #__hp_prop_types SET rgt = rgt+".(2*$nbr_subcats)." WHERE rgt >= $new_parent->rgt");
			$this->_db->query();
					
			# En gros: on d�place!
			# on les translate de $inc vers la droite, pour qu'ils soient tout a droite du nouveau parent
			$this->_db->setQuery( "UPDATE #__hp_prop_types SET lft = lft + $inc, rgt = rgt + $inc WHERE lft >= $this->lft AND rgt <= $this->rgt" );
			$this->_db->query();
				
			# En gros on rebouche!
			# on comble le vide cr�� par le d�placement
			$this->_db->setQuery("UPDATE #__hp_prop_types SET lft = lft-".(2*$nbr_subcats)." WHERE lft >= $this->lft");
			$this->_db->query();
			$this->_db->setQuery("UPDATE #__hp_prop_types SET rgt = rgt-".(2*$nbr_subcats)." WHERE rgt >= $this->rgt");
			$this->_db->query();
			
		}
		# D�placement vers la gauche
		else
		{
			# On fait de la place:
			$this->_db->setQuery("UPDATE #__hp_prop_types SET lft = lft+".(2*$nbr_subcats)." WHERE lft >= $new_parent->rgt");
			$this->_db->query();
			$this->_db->setQuery("UPDATE #__hp_prop_types SET rgt = rgt+".(2*$nbr_subcats)." WHERE rgt >= $new_parent->rgt");
			$this->_db->query();
			
			# On d�place:
			$this->_db->setQuery( "UPDATE #__hp_prop_types SET lft = lft +($inc - ".(2*$nbr_subcats)."), rgt = rgt +($inc - ".(2*$nbr_subcats).") WHERE lft >= ($this->lft + ".(2*$nbr_subcats).") AND rgt <= ($this->rgt + ".(2*$nbr_subcats).")" );
			$this->_db->query();
			
			# On rebouche:
			$this->_db->setQuery("UPDATE #__hp_prop_types SET lft = lft-".(2*$nbr_subcats)." WHERE lft >= $this->lft + ".(2*$nbr_subcats));
			$this->_db->query();
			$this->_db->setQuery("UPDATE #__hp_prop_types SET rgt = rgt-".(2*$nbr_subcats)." WHERE rgt >= $this->rgt + ".(2*$nbr_subcats) );
			$this->_db->query();
		}
	}
}

/**
* Extra Fields table class
*/
class mosHPPropEF extends mosDBTable {
	var $id=null;
	var $field_type=null;
	var $name=null;
	var $caption=null;
	var $default_value=null;
	var $size=null;
	var $field_elements=null;
	var $prefix_text=null;
	var $append_text=null;
	var $hidden=0;
	var $ordering=null;
	var $published=0;
	var $featured=0;
	var $listing=0;
	var $hideCaption=0;
	var $iscore=0;
	var $search=0;
	var $search_caption=null;
	var $search_type=null;

	function mosHPPropEF( &$db ) {
		$this->mosDBTable( '#__hp_prop_ef', 'id', $db );
	}

	function toggleFeatured( $id ) {
		# Get current state
		$this->_db->setQuery("SELECT featured FROM #__hp_prop_ef WHERE id='".$id."'");
		if($this->_db->loadResult() == 0) {
			$this->_db->setQuery("UPDATE #__hp_prop_ef SET featured=1 WHERE id='".$id."'");
		} else {
			$this->_db->setQuery("UPDATE #__hp_prop_ef SET featured=0 WHERE id='".$id."'");
		}
		if($this->_db->query()) return true;
		else return false;
	}

	function toggleListing( $id ) {
		# Get current state
		$this->_db->setQuery("SELECT listing FROM #__hp_prop_ef WHERE id='".$id."'");
		if($this->_db->loadResult() == 0) {
			$this->_db->setQuery("UPDATE #__hp_prop_ef SET listing=1 WHERE id='".$id."'");
		} else {
			$this->_db->setQuery("UPDATE #__hp_prop_ef SET listing=0 WHERE id='".$id."'");
		}
		if($this->_db->query()) return true;
		else return false;
	}
	
	/**
	 * Retourne une liste des types associ�s
	 */
	function listRelTypes()
	{
		global $database;
		
		$this->_db->setQuery("SELECT * FROM #__hp_prop_ef2 AS ef2"
		. "\nLEFT JOIN #__hp_prop_types AS t ON t.id=ef2.type"
		. "\nWHERE ef2.id=$this->id"
		. "\nORDER BY t.lft ASC"
		);
		if (!$this->_db->query())
		{
			echo "<script> alert('".$this->_db->getErrorMsg()."'); window.history.go(-1); </script>\n";
			return false;
		}
		
		return $this->_db->loadObjectList();
	}
	
	/**
	 * Supprime tous les types associ�s
	 */
	function deleteRelTypes()
	{
		global $database;
		
		$this->_db->setQuery("DELETE FROM #__hp_prop_ef2"
		. "\n WHERE id = $this->id"
		);
		if (!$this->_db->query())
		{
			echo "<script> alert('".$this->_db->getErrorMsg()."'); window.history.go(-1); </script>\n";
			return false;
		}
		
		return true;
	}
	
	/**
	 * Ajoute une association avec un type
	 * @param integer : L'id du type � ajouter � la liste des associations
	 */
	function addRelType($type)
	{
		global $database;
		
		$this->_db->setQuery("INSERT INTO #__hp_prop_ef2"
		. "\n SET id = $this->id, type = $type"
		);
		if (!$this->_db->query())
		{
			echo "<script> alert('".$this->_db->getErrorMsg()."'); window.history.go(-1); </script>\n";
			return false;
		}
		
		return true;
	}
	
	/**
	 * Teste si l'EF est associ�(en profondeur) � une cat�gorie donn�e
	 * @param integer $type : L'id de la cat�gorie � consid�rer pour le test
	 */
	function isRelToType($type)
	{
		global $database;
		
		if(!empty($type))
		{
			$type_O=new mosHPPropTypes($database);
			$type_O->load($type);
			
			$query = "SELECT * FROM #__hp_prop_ef2 AS ef2"
			. "\nLEFT JOIN #__hp_prop_types AS t ON t.id=ef2.type"
			. "\nWHERE ef2.id = $this->id AND ((t.lft <= $type_O->lft AND t.rgt >= $type_O->rgt) OR ef2.type = 0)"
			;
			//echo "query for [".$this->name."(id:".$this->id.")]=".$query."<br>";
			$this->_db->setQuery($query);
			if (!$this->_db->query())
			{
				echo "<script> alert('".$this->_db->getErrorMsg()."'); window.history.go(-1); </script>\n";
				return false;
			}
			$subRelTypes=$database->loadObjectList();
			/*echo "<pre>";
			echo "subRelTypes";
			print_r($subRelTypes);
			echo "</pre>";*/
			if(count($subRelTypes))
				return true;
			else
				return false;
		}
		else
		{
			return false;
		}
		
	}
}

/**
 * une table pour enregistrer les relations des EF avec les types
 */
class mosHPPropEF2 extends mosDBTable {
	var $id=null;
	var $type=null;
	
	function mosHPPropEF2( &$db ) {
		$this->mosDBTable( '#__hp_prop_ef2', 'id', $db );
	}
	

}

/**
* Extra Fields Data (aka Properties2) table class
*/
class mosHPProperties2 extends mosDBTable {
	var $id=null;
	var $property=null;
	var $field=null;
	var $value=null;

	function mosHPProperties2( &$db ) {
		$this->mosDBTable( '#__hp_properties2', 'id', $db );
	}
}

/**
* Photos Table
*/
class mosHPPhotos extends mosDBTable {
	var $id=null;
	var $property=null;
	var $original=null;
	var $zoom=null;
	var $standard=null;
	var $thumb=null;
	var $title=null;
	var $desc=null;
	var $ordering=null;

	function mosHPPhotos( &$db ) {
		$this->mosDBTable( '#__hp_photos', 'id', $db );
	}
}

/**
* Configuration
*/
class hpConfig extends mosDBTable {
	var $imgdir_zoom='/components/com_hotproperty/img/zoom/';
	var $imgdir_standard='/components/com_hotproperty/img/std/';
	var $imgdir_thumb='/components/com_hotproperty/img/thb/';
	var $imgdir_original='/components/com_hotproperty/img/ori/';
	var $imgdir_agent='/components/com_hotproperty/img/agent/';
	var $imgdir_company='/components/com_hotproperty/img/company/';
	var $img_noimage_thumb='noimage.png';
	var $img_noimage_standard='noimage.png';
	var $imgsize_zoom=null;
	var $imgsize_standard=null;
	var $imgsize_thumb=null;
	var $imgsize_agent=null;
	var $imgsize_company=null;
	var $quality_photo=null;
	var $quality_agent=null;
	var $quality_company=null;
	var $img_connector='_';
	var $img_method=null;
	var $img_netpbmpath=null;
	var $img_impath=null;
	var $img_saveoriginal=0;
	var $link_open_newwin=null;
	var $show_thumb=null;
	var $show_moreinfo=null;
	var $show_pdficon=null;
	var $show_printicon=null;
	var $show_emailicon=null;
	var $default_agent=null;
	var $default_company=null;
	var $use_companyagent=null;
	var $use_advsearch=null;
	var $use_diplaynum=null;
	var $use_sort_name=null;
	var $use_sort_agent=null;
	var $use_sort_price=null;
	var $use_sort_suburb=null;
	var $use_sort_state=null;
	var $use_sort_country=null;
	var $use_sort_type=null;
	var $use_sort_modified=null;
	var $use_sort_hits=null;
	var $default_order = null;
	var $default_order2 = null;
	var $default_limit = null;
	var $default_limit_agent=null;
	var $default_limit_co=null;
	var $default_limit_search=null;
	var $default_limit_featured=null;
	var $fp_featured_count=null;
	var $fp_show_featured=null;
	var $fp_show_search=null;
	var $fp_show_empty=null;
	var $fp_show_nbr=null;
	var $fp_depth_count=null;
	var $fp_depth_calculous=null;
	var $log_search=null;
	var $agent_groupid=null;
	var $agent_usertype=null;
	var $show_agentdetails=null;
	var $show_enquiryform=null;
	var $show_guide=null;
	var $auto_approve=null;
	var $css=null;
	var $currency=null;
	var $thousand_sep=null;
	var $thousand_string=null;
	var $dec_point=null;
	var $dec_string=null;

	var $language=null;
	var $_alias=null;

	function hpConfig() {
		$this->_alias = array(
		'imgdir_zoom'=>'hp_imgdir_zoom',
		'imgdir_standard'=>'hp_imgdir_standard',
		'imgdir_thumb'=>'hp_imgdir_thumb',
		'imgdir_original'=>'hp_imgdir_original',
		'imgdir_agent'=>'hp_imgdir_agent',
		'imgdir_company'=>'hp_imgdir_company',
		'img_noimage_thumb'=>'hp_img_noimage_thumb',
		'img_noimage_standard'=>'hp_img_noimage_standard',
		'imgsize_zoom'=>'hp_imgsize_zoom',
		'imgsize_standard'=>'hp_imgsize_standard',
		'imgsize_thumb'=>'hp_imgsize_thumb',
		'imgsize_agent'=>'hp_imgsize_agent',
		'imgsize_company'=>'hp_imgsize_company',
		'quality_photo'=>'hp_quality_photo',
		'quality_agent'=>'hp_quality_agent',
		'quality_company'=>'hp_quality_company',
		'img_connector'=>'hp_img_connector',
		'img_method'=>'hp_img_method',
		'img_netpbmpath'=>'hp_img_netpbmpath',
		'img_impath'=>'hp_img_impath',
		'img_saveoriginal'=>'hp_img_saveoriginal',
		'link_open_newwin'=>'hp_link_open_newwin',
		'show_thumb'=>'hp_show_thumb',
		'show_moreinfo'=>'hp_show_moreinfo',
		'show_pdficon'=>'hp_show_pdficon',
		'show_printicon'=>'hp_show_printicon',
		'show_emailicon'=>'hp_show_emailicon',
		'default_agent'=>'hp_default_agent',
		'default_company'=>'hp_default_company',
		'use_companyagent'=>'hp_use_companyagent',
		'auto_approve'=>'hp_auto_approve',
		'css'=>'hp_css',
		'currency'=>'hp_currency',
		'thousand_sep'=>'hp_thousand_sep',
		'thousand_string'=>'hp_thousand_string',
		'dec_point'=>'hp_dec_point',
		'dec_string'=>'hp_dec_string',
		'language'=>'hp_language',
		'use_advsearch'=>'hp_use_advsearch',
		'use_diplaynum'=>'hp_use_diplaynum',
		'use_sort_name'=>'hp_use_sort_name',
		'use_sort_agent'=>'hp_use_sort_agent',
		'use_sort_price'=>'hp_use_sort_price',
		'use_sort_suburb'=>'hp_use_sort_suburb',
		'use_sort_state'=>'hp_use_sort_state',
		'use_sort_country'=>'hp_use_sort_country',
		'use_sort_type'=>'hp_use_sort_type',
		'use_sort_modified'=>'hp_use_sort_modified',
		'use_sort_hits'=>'hp_use_sort_hits',
		'default_order'=>'hp_default_order',
		'default_order2'=>'hp_default_order2',
		'default_limit'=>'hp_default_limit',
		'default_limit_agent'=>'hp_default_limit_agent',
		'default_limit_co'=>'hp_default_limit_co',
		'default_limit_search'=>'hp_default_limit_search',
		'default_limit_featured'=>'hp_default_limit_featured',
		'fp_featured_count'=>'hp_fp_featured_count',
		'fp_show_featured'=>'hp_fp_show_featured',
		'fp_show_search'=>'hp_fp_show_search',
		'fp_show_empty'=>'hp_fp_show_empty',
		'fp_show_nbr'=>'hp_fp_show_nbr',
		'fp_depth_count'=>'hp_fp_depth_count',
		'fp_depth_calculous'=>'hp_fp_depth_calculous',
		'log_search'=>'hp_log_search',
		'agent_groupid'=>'hp_agent_groupid',
		'agent_usertype'=>'hp_agent_usertype',
		'show_agentdetails'=>'hp_show_agentdetails',
		'show_enquiryform'=>'hp_show_enquiryform',
		'show_guide'=>'hp_show_guide'
		);
	}

	function getVarText() {
		$txt = '';
		foreach ($this->_alias as $k=>$v) {
			$txt .= "\$$v='".addslashes( $this->$k )."';\n";
		}
		return $txt;
	}

	function bindGlobals() {
		foreach ($this->_alias as $k=>$v) {
			if(isset($GLOBALS[$v]))
			$this->$k = $GLOBALS[$v];
			else
			$this->$k = "";
		}
	}
}

?>