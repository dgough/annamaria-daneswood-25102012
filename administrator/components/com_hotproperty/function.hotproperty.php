<?php

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

function resize_image($src_file, $dest_file, $new_size, $method, $dest_qual)
{
  $imagetype = array( 1 => 'GIF', 2 => 'JPG', 3 => 'PNG', 4 => 'SWF', 5 => 'PSD', 6 => 'BMP', 7 => 'TIFF', 8 => 'TIFF', 9 => 'JPC', 10 => 'JP2', 11 => 'JPX', 12 => 'JB2', 13 => 'SWC', 14 => 'IFF');
  $imginfo = getimagesize($src_file);

  if ($imginfo == null) die("ERROR: Source file not found!");

  $imginfo[2] = $imagetype[$imginfo[2]];

  # GD can only handle JPG & PNG images
  if ($imginfo[2] != 'JPG' && $imginfo[2] != 'PNG' && ($method == 'gd1' || $method == 'gd2')) die("ERROR: GD can only handle JPG and PNG files!");

  # height/width
  $srcWidth = $imginfo[0];
  $srcHeight = $imginfo[1];

//  echo "Creating thumbnail from $imginfo[2], $imginfo[0] x $imginfo[1]...<br>";

  $ratio = max($srcWidth, $srcHeight) / $new_size;
  $ratio = max($ratio, 1.0);
  $destWidth = (int)($srcWidth / $ratio);
  $destHeight = (int)($srcHeight / $ratio);

  # Method for thumbnails creation
  switch ($method) {

  case "gd1" :
    if (!function_exists('imagecreatefromjpeg')) {
        die('GD image library not installed!');
    }
    if ($imginfo[2] == 'JPG')
      $src_img = imagecreatefromjpeg($src_file);
    else
      $src_img = imagecreatefrompng($src_file);
    if (!$src_img){
      $ERROR = $lang_errors['invalid_image'];
      return false;
    }
    $dst_img = imagecreate($destWidth, $destHeight);
    imagecopyresized($dst_img, $src_img, 0, 0, 0, 0, $destWidth, (int)$destHeight, $srcWidth, $srcHeight);
    imagejpeg($dst_img, $dest_file, $dest_qual);
    imagedestroy($src_img);
    imagedestroy($dst_img);
    break;

  case "gd2" :
		$gdinfo = gd_info();
		if( empty($gdinfo['GD Version']) ) {
        die('GD2 image library not installed!');
		}

    if ($imginfo[2] == 'JPG')
      $src_img = imagecreatefromjpeg($src_file);
    else
      $src_img = imagecreatefrompng($src_file);
    if (!$src_img){
      $ERROR = $lang_errors['invalid_image'];
      return false;
    }
    $dst_img = imagecreatetruecolor($destWidth, $destHeight);
    imagecopyresampled($dst_img, $src_img, 0, 0, 0, 0, $destWidth, (int)$destHeight, $srcWidth, $srcHeight);
    imagejpeg($dst_img, $dest_file, $dest_qual);
    imagedestroy($src_img);
    imagedestroy($dst_img);
    break;
	case "netpbm":
		global $hp_img_netpbmpath; 
		if($hp_img_netpbmpath) { 
			if(!is_dir($hp_img_netpbmpath))	{ 
					echo "NetPbm path not correct";
					die; 
				} 
			} 
		 if ($imginfo[2] == 'PNG') { 
				$cmd = $hp_img_netpbmpath . "pngtopnm $src_file | " . $hp_img_netpbmpath . "pnmscale -xysize $destWidth $destHeight | " . $hp_img_netpbmpath . "pnmtopng > '$dest_file'" ; 
			}	else if ($imginfo[2] == 'JPG')	{ 
				$cmd = $hp_img_netpbmpath . "jpegtopnm $src_file | " . $hp_img_netpbmpath . "pnmscale -xysize $destWidth $destHeight | " . $hp_img_netpbmpath . "ppmtojpeg -quality=$dest_qual > '$dest_file'" ;
			}	else if ($imginfo[2] == 'GIF') { 
				$cmd = $hp_img_netpbmpath . "giftopnm $src_file | " . $hp_img_netpbmpath . "pnmscale -xysize $destWidth $destHeight | " . $hp_img_netpbmpath . "ppmquant 256 | " . $hp_img_netpbmpath . "ppmtogif > '$dest_file'" ; 
			}//end if.. 

		@exec($cmd); 
		break;
	case "imagemagick":
		global $hp_img_impath, $mosConfig_absolute_path;

		$tmp_name = substr(strrchr($dest_file, "/"), 1);
		copy($src_file, $mosConfig_absolute_path.'/media/'.$tmp_name);
		$uploadfile = $mosConfig_absolute_path.'/media/'.$tmp_name;
		$cmd = $hp_img_impath."convert -resize $destWidth $uploadfile '$dest_file'";
		@exec($cmd);
		unlink($uploadfile);

		break;
  }

  # Set mode of uploaded picture
  chmod($dest_file, octdec('755'));

  # We check that the image is valid
  $imginfo = getimagesize($dest_file);
  if ($imginfo == null){
    return false;
  } else {
    return true;
  }
}

?>