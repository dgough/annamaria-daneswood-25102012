<?php

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

function com_uninstall()
{
	global $database;

	$msg = '<table width="100%" border="0" cellpadding="8" cellspacing="0"><tr>';
	$msg .= '<td width="100%" align="left" valign="top"><center><h3>Hot Property</h3><h4>A Mambo Open Source Listing Solutions (Real Estates, Auto, Books, Cds etc.)</h4><font class="small">&copy; Copyright 2004 by Lee Cher Yeong. <a href="http://www.mosets.com/">http://www.mosets.com/</a><br/></font></center><br />';

	$msg .= "<fieldset style=\"border: 1px dashed #C0C0C0;\"><legend>Details</legend>";

	$hpacl = new gacl_api;

	# Reassign Agent user to Registered user
	$database->setQuery("UPDATE #__users SET gid='".$hpacl->get_group_id("Registered")."' WHERE gid='".$hpacl->get_group_id("Agent")."'");
	if ($database->query()) {
		$msg .= "<font color=#339900>OK</font> &nbsp; All Agent users are converted to Registered user<br />";
	}

	# Update the #__core_acl_groups_aro_map table
	$database->setQuery( "UPDATE #__core_acl_groups_aro_map"
		. "\nSET group_id = '".$hpacl->get_group_id("Registered")."'"
		. "\nWHERE group_id = '".$hpacl->get_group_id("Agent")."'"
	);
	$database->query() or die( $database->stderr() );

	# Remove ACL
	$hpacl = new gacl_api;
	
	if($hpacl->del_group($hpacl->get_group_id("Agent"))) {
		$msg .= "<font color=#339900>OK</font> &nbsp; Agent ACL deleted<br />";
	} else {
		$msg .= "<font color=red>FAILED</font> &nbsp; Agent ACL not deleted<br />";
	}

	$msg .= "<font color=#339900>OK</font> &nbsp; Hot Property Uninstalled Successfully</fieldset>";
	$msg .='<br /><br /></td></tr></table>';

	return $msg;
	/*
	
	*/
}
?>
