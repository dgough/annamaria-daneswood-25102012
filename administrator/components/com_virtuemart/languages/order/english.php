<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
/**
*
* @version $Id: english.php 1071 2007-12-03 08:42:28Z thepisu $
* @package VirtueMart
* @subpackage languages
* @copyright Copyright (C) 2004-2007 soeren - All rights reserved.
* @translator soeren
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
global $VM_LANG;
$langvars = array (
	'CHARSET' => 'ISO-8859-1',
	'PHPSHOP_ORDER_PRINT_PAYMENT_LOG_LBL' => 'Payment Log',
	'PHPSHOP_ORDER_PRINT_SHIPPING_PRICE_LBL' => 'Shipping Price',
	'PHPSHOP_ORDER_STATUS_LIST_CODE' => 'Booking Status Code',
	'PHPSHOP_ORDER_STATUS_LIST_NAME' => 'Booking Status Name',
	'PHPSHOP_ORDER_STATUS_FORM_LBL' => 'Booking Status',
	'PHPSHOP_ORDER_STATUS_FORM_CODE' => 'Booking Status Code',
	'PHPSHOP_ORDER_STATUS_FORM_NAME' => 'Booking Status Name',
	'PHPSHOP_ORDER_STATUS_FORM_LIST_ORDER' => 'Booking List',
	'PHPSHOP_COMMENT' => 'Comment',
	'PHPSHOP_ORDER_LIST_NOTIFY' => 'Notify Customer?',
	'PHPSHOP_ORDER_LIST_NOTIFY_ERR' => 'Please change the Booking Status first!',
	'PHPSHOP_ORDER_HISTORY_INCLUDE_COMMENT' => 'Include this comment?',
	'PHPSHOP_ORDER_HISTORY_DATE_ADDED' => 'Date Added',
	'PHPSHOP_ORDER_HISTORY_CUSTOMER_NOTIFIED' => 'Customer Notified?',
	'PHPSHOP_ORDER_STATUS_CHANGE' => 'Booking Status Change',
	'PHPSHOP_ORDER_LIST_PRINT_LABEL' => 'Print Label',
	'PHPSHOP_ORDER_LIST_VOID_LABEL' => 'Void Label',
	'PHPSHOP_ORDER_LIST_TRACK' => 'Track',
	'VM_DOWNLOAD_STATS' => 'DOWNLOAD STATS',
	'VM_DOWNLOAD_NOTHING_LEFT' => 'no downloads remaining',
	'VM_DOWNLOAD_REENABLE' => 'Re-Enable Download',
	'VM_DOWNLOAD_REMAINING_DOWNLOADS' => 'Remaining Downloads',
	'VM_DOWNLOAD_RESEND_ID' => 'Resend Download ID',
	'VM_EXPIRY' => 'Expiry',
	'VM_UPDATE_STATUS' => 'Update Status',
	'VM_ORDER_LABEL_ORDERID_NOTVALID' => 'Please provide a valid, numeric, Booking ID, not "{booking_id}"',
	'VM_ORDER_LABEL_NOTFOUND' => 'Booking record not found in shipping label database.',
	'VM_ORDER_LABEL_NEVERGENERATED' => 'Label has not been generated yet',
	'VM_ORDER_LABEL_CLASSCANNOT' => 'Class {ship_class} cannot get label images, why are we here?',
	'VM_ORDER_LABEL_SHIPPINGLABEL_LBL' => 'Shipping Label',
	'VM_ORDER_LABEL_SIGNATURENEVER' => 'Signature was never retrieved',
	'VM_ORDER_LABEL_TRACK_TITLE' => 'Track',
	'VM_ORDER_LABEL_VOID_TITLE' => 'Void Label',
	'VM_ORDER_LABEL_VOIDED_MSG' => 'Label for waybill {tracking_number} has been voided.',
	'VM_ORDER_PRINT_PO_IPADDRESS' => 'IP-ADDRESS',
	'VM_ORDER_STATUS_ICON_ALT' => 'Status Icon',
	'VM_ORDER_PAYMENT_CCV_CODE' => 'CVV Code',
	'VM_ORDER_NOTFOUND' => 'Booking not found! It may have been deleted.',
	'VM_ORDER_ARRIVAL' => 'Arrival',
	'VM_ORDER_DEPARTURE' => 'Departure',
	'VM_ORDER_PEOPLE' => 'People',
	'VM_ORDER_CURRENT_PRICE' => 'Current Price',
	'VM_ORDER_MODIFY_PRICE' => 'Modify Price',
	
	
	'VM_ORDER_OVERVIEW_HEADING' => 'Booking Overview',
	'VM_ORDER_HISTORY_HEADING' => 'Booking History',
	'VM_ORDER_BILLING_HEADING' => 'Billing Information',
	'VM_ORDER_PAYMENT_HEADING' => 'Payment Information',	
	'VM_ORDER_UPDATE_HEADING' => 'Update Status',
	'VM_ORDER_MODIFY_HEADING' => 'Modify Booking',
	'VM_ORDER_ADD_HEADING' => 'Add Booking',
	
	'VM_ORDER_OVERVIEW_TIP' => 'Here you can view all the basic information about this booking, for more detailed information please use the tabs above.',
	'VM_ORDER_HISTORY_TIP' => 'This page displays the order history for every time the booking has been changed.',
	'VM_ORDER_BILLING_TIP' => 'The customers billing information is listed below, to change this, please use the modify booking tab',
	'VM_ORDER_UPDATE_TIP' => 'This provides a quick and easy method to enable you to change the status of the booking only. You can include a comment and send a notice to the customer that the booking status has been changed.',
	'VM_ORDER_ADD_TIP' => 'Use this page to add a new booking. Select the property and user account, select your dates and use the "get price" button to get live pricing from the server, or alternatively enter your own custom price.',
	'VM_ORDER_MODIFY_TIP' => 'Use this page to modify an existing booking. You may change your dates and use the "get price" button to get live pricing from the server, or alternatively enter your own custom price. You must check the "Update" checkbox for your changes to be saved.',
	
	'VM_ORDER_MODIFY_USER' => 'User',
	'VM_ORDER_MODIFY_PROPERTY' => 'Property',
	'VM_ORDER_MODIFY_UPDATE' => 'Update Booking',
	'VM_ORDER_MODIFY_UPDATE_NOTE' => '(this must be ticked to update the booking details)',
	'VM_ORDER_MODIFY_USER_NOTE' => '(If no user is selected, a user will be created with the details below)',
	'VM_ORDER_MODIFY_IGNORE_VALIDATION' => 'Ignore Validation',
	'VM_ORDER_MODIFY_VALIDATION_NOTE' => '(uncheck to validate the number of guests/min nights/max nights/changeover days)',
	'VM_ORDER_MODIFY_NOTIFY_HTML' => 'Send full HTML order email ',
	'VM_ORDER_MODIFY_PRICE' => 'Modify Price',
	'VM_ORDER_MODIFY_UPDATE' => 'Update',
	'VM_ORDER_MODIFY_USE_MOD' => 'Use Modified Price',
	'VM_ORDER_MODIFY_USERNOTE' => 'Please note: Modifying the information below when updating a booking will not update their central record, only the information assocaited with this order.',
	
	'VM_ORDER_MODIFY_GET_PRICE' => 'Get Live Price',
	'VM_ORDER_MODIFY_GET_PRICE_TIP' => 'Clicking the "Get Live Price" button will return the price of the booking for the date range you have selected by using the pricing information stored on the server',
	'VM_ORDER_CHECKIN_TIME' => '<strong>Checkin Time:</strong> 4pm',
	'VM_ORDER_CHECKOUT_TIME' => '<strong>Checkout Time:</strong> 10am',
	'VM_ORDER_KEYCODE' => 'Keycode:',
	
	
	
	//order.details
	
	
); $VM_LANG->initModule( 'order', $langvars );
?>