<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direkter Zugang zu '.basename(__FILE__).' ist nicht gestattet.' ); 
/**
*
* @version $Id: english.php 1071 2007-12-03 08:42:28Z thepisu $
* @package VirtueMart
* @subpackage languages
* @copyright Copyright (C) 2004-2007 soeren - All rights reserved.
* @translator soeren
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
global $VM_LANG;
$langvars = array (
	'CHARSET' => 'ISO-8859-1',
	'PHPSHOP_ORDER_PRINT_PAYMENT_LOG_LBL' => 'Zahlungslog',
	'PHPSHOP_ORDER_PRINT_SHIPPING_PRICE_LBL' => 'Versandpreis',
	'PHPSHOP_ORDER_STATUS_LIST_CODE' => 'Buchungsstatus-Kode',
	'PHPSHOP_ORDER_STATUS_LIST_NAME' => 'Buchungsstatus-Name',
	'PHPSHOP_ORDER_STATUS_FORM_LBL' => 'Buchungsstatus',
	'PHPSHOP_ORDER_STATUS_FORM_CODE' => 'Buchungsstatus-Kode',
	'PHPSHOP_ORDER_STATUS_FORM_NAME' => 'Buchungsstatus-Name',
	'PHPSHOP_ORDER_STATUS_FORM_LIST_ORDER' => 'Buchungsliste',
	'PHPSHOP_COMMENT' => 'Kommentar',
	'PHPSHOP_ORDER_LIST_NOTIFY' => 'Kunden benachrichtigen?',
	'PHPSHOP_ORDER_LIST_NOTIFY_ERR' => 'Bitte erst den Buchungsstatus &#228;ndern!',
	'PHPSHOP_ORDER_HISTORY_INCLUDE_COMMENT' => 'Kommentar beif&#252;gen?',
	'PHPSHOP_ORDER_HISTORY_DATE_ADDED' => 'Datum hinzugef&#252;gt',
	'PHPSHOP_ORDER_HISTORY_CUSTOMER_NOTIFIED' => 'Kunde benachrichtigt?',
	'PHPSHOP_ORDER_STATUS_CHANGE' => 'Buchungsstatus&#228;nderung',
	'PHPSHOP_ORDER_LIST_PRINT_LABEL' => 'Etikett drucken',
	'PHPSHOP_ORDER_LIST_VOID_LABEL' => 'Etikett ung&#252;ltig ',
	'PHPSHOP_ORDER_LIST_TRACK' => 'Verfolgen',
	'VM_DOWNLOAD_STATS' => 'DOWNLOAD STATS',
	'VM_DOWNLOAD_NOTHING_LEFT' => 'keine weiteren Downloads',
	'VM_DOWNLOAD_REENABLE' => 'Download wieder aktivieren',
	'VM_DOWNLOAD_REMAINING_DOWNLOADS' => 'Verbleibende Downloads',
	'VM_DOWNLOAD_RESEND_ID' => 'Download- erneut sendenID',
	'VM_EXPIRY' => 'Verfall',
	'VM_UPDATE_STATUS' => 'Status aktualisieren',
	'VM_ORDER_LABEL_ORDERID_NOTVALID' => 'Bitte geben Sie ein g&#252;ltiges, numerisches, Buchungs-ID ein, nicht "{booking_id}"',
	'VM_ORDER_LABEL_NOTFOUND' => 'Buchungsdatensatz nicht in der Versandetikett-Datenbank gefunden.',
	'VM_ORDER_LABEL_NEVERGENERATED' => 'Etikett wurde noch nicht erstellt',
	'VM_ORDER_LABEL_CLASSCANNOT' => 'Klasse {ship_class} kann keine Etikett-Bilder bekommen, wieso sind wir hier?',
	'VM_ORDER_LABEL_SHIPPINGLABEL_LBL' => 'Versandetikett',
	'VM_ORDER_LABEL_SIGNATURENEVER' => 'Unterschrift wurde nie abgerufen',
	'VM_ORDER_LABEL_TRACK_TITLE' => 'Verfolgen',
	'VM_ORDER_LABEL_VOID_TITLE' => 'Etikett ung&#252;ltig',
	'VM_ORDER_LABEL_VOIDED_MSG' => 'Etikett f&#252;r den Frachtbrief {tracking_number} wurde storniert.',
	'VM_ORDER_PRINT_PO_IPADDRESS' => 'IP-ADDRESS',
	'VM_ORDER_STATUS_ICON_ALT' => 'Statussymbol',
	'VM_ORDER_PAYMENT_CCV_CODE' => 'CVV Kode',
	'VM_ORDER_NOTFOUND' => 'Buchung nicht gefunden! Es wurde m&#246;glicherweise gel&#246;scht.',
	'VM_ORDER_ARRIVAL' => 'Ankunft',
	'VM_ORDER_DEPARTURE' => 'Abreise',
	'VM_ORDER_PEOPLE' => 'Personen',
	'VM_ORDER_CURRENT_PRICE' => 'Aktueller Preis',
	'VM_ORDER_MODIFY_PRICE' => 'Preis ver&#228;ndern',
	'VM_ORDER_OVERVIEW_HEADING' => 'Buchungs&#252;bersicht',
	'VM_ORDER_HISTORY_HEADING' => 'Buchungsvergangenheit',
	'VM_ORDER_BILLING_HEADING' => 'Rechnungsinformation',
	'VM_ORDER_PAYMENT_HEADING' => 'Zahlungsinformation',	
	'VM_ORDER_UPDATE_HEADING' => 'Status aktualisieren',
	'VM_ORDER_MODIFY_HEADING' => 'Buchung ver&#228;ndern',
	'VM_ORDER_ADD_HEADING' => 'Buchung hinzuf&#252;gen',
	'VM_ORDER_OVERVIEW_TIP' => 'Hier k&#246;nnen Sie alle Grundinformationen &#252;ber diese Buchung sehen, f&#252;r mehr detaillierte Information wenden Sie sich bitte an die Tabulatoren oben.',
	'VM_ORDER_HISTORY_TIP' => 'Diese Seite zeigt die Bestellungsvergangenheit, f&#252;r jeden Zeitpunkt wo die Buchung ge&#228;ndert wurde.',
	'VM_ORDER_BILLING_TIP' => 'Die Kunden-Rechnungsinformationen ist unten aufgef&#252;hrt, um dies zu &#228;ndern, benutzen Sie bitte den Buchung-&#228;ndern Tabulator',
	'VM_ORDER_UPDATE_TIP' => 'Dies bietet eine schnelle und einfache Methode an, um nur den Buchungsstatus zu &#228;ndern. Sie k&#246;nnen einen Kommentar beif&#252;gen und eine Mitteilung an den Kunden senden daß der Buchungsstatus ver&#228;ndert wurde.',
	'VM_ORDER_ADD_TIP' => 'Auf dieser Seite k&#246;nnen Sie eine neue Buchung hinzuzuf&#252;gen. W&#228;hlen Sie die Immobilie und das Nutzerkonto, w&#228;hlen Sie Ihre Reisedaten und verwenden Sie den "Preis bekommen" Knopf, um Live-Preise vom Server zu erhalten, oder geben Sie Ihren eigenen nutzerdefinierten Preis ein.',
	'VM_ORDER_MODIFY_TIP' => 'Auf dieser Seite k&#246;nnen Sie eine bestehende Buchung &#228;ndern. Sie k&#246;nnen Ihre Daten &#228;ndern und den "Preis bekommen" Knopf gebrauchen um Live-Preise vom Server zu erhalten, oder geben Sie Ihren eigenen nutzerdefinierten Preis ein. Sie m&#252;ssen die "Update" Checkbox aktivieren damit Ihre &#228;nderungen gespeichert werden.',
	'VM_ORDER_MODIFY_USER' => 'Nutzer',
	'VM_ORDER_MODIFY_PROPERTY' => 'Immobilie',
	'VM_ORDER_MODIFY_UPDATE' => 'Buchung aktualisieren',
	'VM_ORDER_MODIFY_UPDATE_NOTE' => '(dies muss angekreuzt sein, um die Buchungsdetails zu aktualisieren)',
	'VM_ORDER_MODIFY_USER_NOTE' => '(wenn kein Nutzer ausgew&#228;hlt ist, wird ein Nutzer erzeugt mit den Details unten)',
	'VM_ORDER_MODIFY_IGNORE_VALIDATION' => '&#252;berpr&#252;fung ignorieren',
	'VM_ORDER_MODIFY_VALIDATION_NOTE' => '(uncheckem um die Anzahl der G&#228;ste / min N&#228;chte / max N&#228;chte / Wechseltage zu &#252;berpr&#252;fen)',
	'VM_ORDER_MODIFY_NOTIFY_HTML' => 'Vollst&#228;ndige HTML-Mail-Bestellung senden',
	'VM_ORDER_MODIFY_PRICE' => 'Preis ver&#228;ndern',
	'VM_ORDER_MODIFY_UPDATE' => 'Aktualisieren',
	'VM_ORDER_MODIFY_USE_MOD' => 'Ver&#228;nderten Preis verwenden',
	'VM_ORDER_MODIFY_USERNOTE' => 'Bitte beachten: Ver&#228;nderung der folgenden Information bei der Aktualisierung einer Buchung wird nicht den zentralen Datensatz aktualisieren, nur die Information die mit diesem Auftrag verbunden ist.',
	'VM_ORDER_MODIFY_GET_PRICE' => 'Live-Preis bekommen',
	'VM_ORDER_MODIFY_GET_PRICE_TIP' => 'Clicken des "Live-Preis bekommen" Knopfes wird den Buchungspreis zur&#252;ckstellen f&#252;r den Datumsbereich den Sie ausgew&#228;hlt haben, mit der Verwendung der Preisinformation die auf dem Server gespeichert ist.',
	'VM_ORDER_CHECKIN_TIME' => '<strong>Einbuchungszeit:</strong> 4pm',
	'VM_ORDER_CHECKOUT_TIME' => '<strong>Ausbuchungszeit:</strong> 10am',
	'VM_ORDER_KEYCODE' => 'Schl&#252;sselkode:',
	
	//order.details

); $VM_LANG->initModule( 'order', $langvars );
?>