<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direkter Zugang zu '.basename(__FILE__).' ist nicht gestattet.' ); 
/**
*
* @version $Id: english.php 1071 2007-12-03 08:42:28Z thepisu $
* @package VirtueMart
* @subpackage languages
* @copyright Copyright (C) 2004-2007 soeren - All rights reserved.
* @translator soeren
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
global $VM_LANG;
$langvars = array (
	'CHARSET' => 'ISO-8859-1',
	'PHPSHOP_USER_FORM_FIRST_NAME' => 'Vorname',
	'PHPSHOP_USER_FORM_LAST_NAME' => 'Nachname',
	'PHPSHOP_USER_FORM_MIDDLE_NAME' => 'Weitere Vornamen',
	'PHPSHOP_USER_FORM_COMPANY_NAME' => 'Firmenname',
	'PHPSHOP_USER_FORM_ADDRE&#223;_1' => 'Adre&#223;e 1',
	'PHPSHOP_USER_FORM_ADDRE&#223;_2' => 'Adre&#223;e 2',
	'PHPSHOP_USER_FORM_CITY' => 'Stadt',
	'PHPSHOP_USER_FORM_STATE' => 'Staat/Provinz/Gebiet',
	'PHPSHOP_USER_FORM_ZIP' => 'ZIP / Postleitzahl',
	'PHPSHOP_USER_FORM_COUNTRY' => 'Land',
	'PHPSHOP_USER_FORM_PHONE' => 'Telefon',
	'PHPSHOP_USER_FORM_PHONE2' => 'Handy',
	'PHPSHOP_USER_FORM_FAX' => 'Fax',
	'PHPSHOP_I&#223;HIP_LIST_PUBLISH_LBL' => 'Aktiv',
	'PHPSHOP_I&#223;HIP_FORM_UPDATE_LBL' => 'Versandart konfigurieren',
	'PHPSHOP_STORE_FORM_FULL_IMAGE' => 'Volles Bild',
	'PHPSHOP_STORE_FORM_UPLOAD' => 'Bild hochladen',
	'PHPSHOP_STORE_FORM_STORE_NAME' => 'Ladenname',
	'PHPSHOP_STORE_FORM_COMPANY_NAME' => 'Laden Firmenname',
	'PHPSHOP_STORE_FORM_ADDRE&#223;_1' => 'Adre&#223;e 1',
	'PHPSHOP_STORE_FORM_ADDRE&#223;_2' => 'Adre&#223;e 2',
	'PHPSHOP_STORE_FORM_CITY' => 'Stadt',
	'PHPSHOP_STORE_FORM_STATE' => 'Staat/Provinz/Gebiet',
	'PHPSHOP_STORE_FORM_COUNTRY' => 'Land',
	'PHPSHOP_STORE_FORM_ZIP' => 'ZIP / Postleizahl',
	'PHPSHOP_STORE_FORM_CURRENCY' => 'W&#228;hrung',
	'PHPSHOP_STORE_FORM_LAST_NAME' => 'Nachname',
	'PHPSHOP_STORE_FORM_FIRST_NAME' => 'Vorname',
	'PHPSHOP_STORE_FORM_MIDDLE_NAME' => 'Weitere Vornamen',
	'PHPSHOP_STORE_FORM_TITLE' => 'Titel',
	'PHPSHOP_STORE_FORM_PHONE_1' => 'Telefon 1',
	'PHPSHOP_STORE_FORM_PHONE_2' => 'Telefon 2',
	'PHPSHOP_STORE_FORM_DESCRIPTION' => 'Beschreibung',
	'PHPSHOP_PAYMENT_METHOD_LIST_LBL' => 'Zahlungsmethodenliste',
	'PHPSHOP_PAYMENT_METHOD_LIST_NAME' => 'Name',
	'PHPSHOP_PAYMENT_METHOD_LIST_CODE' => 'Kode',
	'PHPSHOP_PAYMENT_METHOD_LIST_SHOPPER_GROUP' => 'K&#228;ufer-Gruppe',
	'PHPSHOP_PAYMENT_METHOD_LIST_ENABLE_PROCE&#223;OR' => 'Zahlungsmethode',
	'PHPSHOP_PAYMENT_METHOD_FORM_LBL' => 'Zahlungsmethodenformular',
	'PHPSHOP_PAYMENT_METHOD_FORM_NAME' => 'Zahlungsmethodenname',
	'PHPSHOP_PAYMENT_METHOD_FORM_SHOPPER_GROUP' => 'K&#228;ufer-Gruppe',
	'PHPSHOP_PAYMENT_METHOD_FORM_DISCOUNT' => 'Rabatt',
	'PHPSHOP_PAYMENT_METHOD_FORM_CODE' => 'Kode',
	'PHPSHOP_PAYMENT_METHOD_FORM_LIST_ORDER' => 'Buchungsliste',
	'PHPSHOP_PAYMENT_METHOD_FORM_ENABLE_PROCE&#223;OR' => 'Zahlungsmethode',
	'PHPSHOP_PAYMENT_FORM_CC' => 'Kreditkarte',
	'PHPSHOP_PAYMENT_FORM_USE_PP' => 'Zahlungsproze&#223;or verwenden',
	'PHPSHOP_PAYMENT_FORM_BANK_DEBIT' => 'Banklastschrift',
	'PHPSHOP_PAYMENT_FORM_AO' => 'Nur Adre&#223;e / Barzahlung bei Lieferung',
	'PHPSHOP_STATISTIC_STATISTICS' => 'Statistiken',
	'PHPSHOP_STATISTIC_CUSTOMERS' => 'Kunden',
	'PHPSHOP_STATISTIC_ACTIVE_PRODUCTS' => 'Aktive Produkte',
	'PHPSHOP_STATISTIC_INACTIVE_PRODUCTS' => 'Inaktive Produkte',
	'PHPSHOP_STATISTIC_NEW_ORDERS' => 'Neue Buchungen',
	'PHPSHOP_STATISTIC_NEW_CUSTOMERS' => 'Neue Kunden',
	'PHPSHOP_CREDITCARD_NAME' => 'Kreditkartenname',
	'PHPSHOP_CREDITCARD_CODE' => 'Kreditkarte - Kurzer Kode',
	'PHPSHOP_YOUR_STORE' => 'Ihr Laden',
	'PHPSHOP_CONTROL_PANEL' => 'Bedienfeld',
	'PHPSHOP_CHANGE_PA&#223;KEY_FORM' => 'Anzeigen / &#228;ndern des Pa&#223;worts / Transaktions-Aktuelle Transaktion KeyKey',
	'PHPSHOP_TYPE_PA&#223;WORD' => 'Bitte geben Sie Ihr Nutzer-Pa&#223;wort ein',
	'PHPSHOP_CURRENT_TRANSACTION_KEY' => 'Aktueller Transaktions-Key',
	'PHPSHOP_CHANGE_PA&#223;KEY_SUCCE&#223;' => 'Der Transaktions-Key wurde erfolgreich ge&#228;ndert.',
	'VM_PAYMENT_CLA&#223;_NAME' => 'Zahlungskla&#223;enname',
	'VM_PAYMENT_CLA&#223;_NAME_TIP' => '(z.B. <strong>ps_netbanx</strong>) :<br />
		default: ps_payment<br />
		<em>ps_payment w&#228;hlen wenn Sie unsicher sind, was zu w&#228;hlen!</em>',
	'VM_PAYMENT_EXTRAINFO' => 'Zahlung Zus&#228;tzliche Information',
	'VM_PAYMENT_EXTRAINFO_TIP' => 'Wird auf der Buchungsbest&#228;tigun&#223;eite gezeigt. Kann sein: HTML Formularkode von Ihrem Zahlungsdienstleistungsanbieters, Hinweise an den Kunden usw.',
	'VM_PAYMENT_ACCEPTED_CREDITCARDS' => 'Akzeptierte Kreditkartentypen',
	'VM_PAYMENT_METHOD_DISCOUNT_TIP' => 'Um den Rabatt zu einer Geb&#252;hr zu ver&#228;ndern, verwenden Sie einen negativen Wert hier (Beispiel: <strong>-2.00</strong>).',
	'VM_PAYMENT_METHOD_DISCOUNT_MAX_AMOUNT' => 'Maximaler Rabattbetrag',
	'VM_PAYMENT_METHOD_DISCOUNT_MIN_AMOUNT' => 'Minimaler Rabattbetrag',
	'VM_PAYMENT_FORM_FORMBASED' => 'HTML-Formular basiert (z.B. PayPal)',
	'VM_ORDER_EXPORT_MODULE_LIST_LBL' => 'Modulliste exportieren',
	'VM_ORDER_EXPORT_MODULE_LIST_NAME' => 'Name',
	'VM_ORDER_EXPORT_MODULE_LIST_DESC' => 'Beschreibung',
	'VM_STORE_FORM_ACCEPTED_CURRENCIES' => 'Liste der akzeptierten W&#228;hrungen',
	'VM_STORE_FORM_ACCEPTED_CURRENCIES_TIP' => 'Diese Liste definiert alle W&#228;hrungen die Sie akzeptieren Sie, wenn jemand sich etwas in Ihrem Gesch&#228;ft kauft. <strong>Anmerkung:</strong> Alle W&#228;hrungen hier gew&#228;hlt k&#246;nnen bei der Ka&#223;e gebraucht werden! Wenn Sie das nicht wollen, w&#228;hlen Sie einfach die W&#228;hrung Ihres Landes (=default).',
	'VM_EXPORT_MODULE_FORM_LBL' => 'Modulenformular exportieren',
	'VM_EXPORT_MODULE_FORM_NAME' => 'Modulenname exportieren',
	'VM_EXPORT_MODULE_FORM_DESC' => 'Beschreibung',
	'VM_EXPORT_CLA&#223;_NAME' => 'Kla&#223;enname exportieren',
	'VM_EXPORT_CLA&#223;_NAME_TIP' => '(e.g. <strong>ps_bookings_csv</strong>) :<br /> default: ps_xmlexport<br /> <i>Leer la&#223;en, wenn Sie unsicher sind was Sie eingeben sollen!</i>',
	'VM_EXPORT_CONFIG' => 'Zus&#228;tzliche Konfigurationen exportieren',
	'VM_EXPORT_CONFIG_TIP' => 'Export-Konfiguration f&#252;r benutzerdefinierte Export-Module definieren oder zus&#228;tzliche Konfiguration definieren. Kode mu&#223; g&#252;ltiger PHP-Kode sein.',
	'VM_SHIPPING_MODULE_LIST_NAME' => 'Name',
	'VM_SHIPPING_MODULE_LIST_E_VERSION' => 'Version',
	'VM_SHIPPING_MODULE_LIST_HEADER_AUTHOR' => 'Autor',
	'PHPSHOP_STORE_ADDRE&#223;_FORMAT' => 'Ladenadre&#223;en-Format',
	'PHPSHOP_STORE_ADDRE&#223;_FORMAT_TIP' => 'Sie k&#246;nnen die folgenden Platzhalter hier verwenden',
	'PHPSHOP_STORE_DATE_FORMAT' => 'Ladendatumsformat',
	'VM_PAYMENT_METHOD_ID_NOT_PROVIDED' => 'Fehler: Zahlungsmethoden-ID wurde nicht angegeben.',
	'VM_SHIPPING_MODULE_CONFIG_LBL' => 'Versandmodul-Konfiguration',
	'VM_SHIPPING_MODULE_CLA&#223;ERROR' => 'Konnte nicht Kla&#223;e instanzieren {shipping_module}'
); $VM_LANG->initModule( 'Laden', $langvars );
?>
