<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direkter Zugang zu'.basename(__FILE__).' ist nicht gestattet.' ); 
/**
*
* @package VirtueMart
* @subpackage languages
* @copyright Copyright (C) 2004-2008 soeren - All rights reserved.
* @translator soeren
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
global $VM_LANG;
$langvars = array (
	'CHARSET' => 'ISO-8859-1',
	'PHPSHOP_BROWSE_LBL' => 'Browsen',
	'PHPSHOP_FLYPAGE_LBL' => 'Produktdetails',
	'PHPSHOP_PRODUCT_FORM_EDIT_PRODUCT' => 'Dieses Produkt bearbeiten',
	'PHPSHOP_DOWNLOADS_START' => 'Download starten',
	'PHPSHOP_DOWNLOADS_INFO' => 'Bitte Download-ID, den Sie in derMail bekommen haben, eingeben und \'Download starten\ klicken\'.',
	'PHPSHOP_WAITING_LIST_ME&#223;AGE' => 'Bitte geben Sie Ihre Mail-Adre&#223;e ein, um benachrichtigt zu werden wenn dieses Produkt im Vorrat ist. 
		Wir werden diese Mail-Adre&#223;e nicht teilen, vermieten, verkaufen oder f&#252;r irgendeinen anderen Zweck verwenden als Ihnen zu sagen, wenn das Produkt wieder auf Lager ist.<br /><br />Danke!',
	'PHPSHOP_WAITING_LIST_THANKS' => 'Danke f&#252;rs warten! <br />Wir werden Sie wi&#223;en la&#223;en, sobald wir unser Inventar bekommen.',
	'PHPSHOP_WAITING_LIST_NOTIFY_ME' => 'Notify Me!',
	'PHPSHOP_SEARCH_ALL_CATEGORIES' => 'Alle Kategorien durchsuchen',
	'PHPSHOP_SEARCH_ALL_PRODINFO' => 'Alle Produktinformationen durchsuchen',
	'PHPSHOP_SEARCH_PRODNAME' => 'Nur Produktname',
	'PHPSHOP_SEARCH_MANU_VENDOR' => 'Nur Hersteller / Lieferant',
	'PHPSHOP_SEARCH_DESCRIPTION' => 'Nur Produktbeschreibung',
	'PHPSHOP_SEARCH_AND' => 'und',
	'PHPSHOP_SEARCH_NOT' => 'nicht',
	'PHPSHOP_SEARCH_TEXT1' => 'Mit der ersten Dropdownliste-Liste k&#246;nnen Sie eine Kategorie w&#228;hlen, um Ihre Suche zu begrenzen. 
        Mit der ersten Dropdownliste-Liste k&#246;nnen Sie Ihre Suche bregrenzen auf eine bestimmte Art von Produktdetails (z.B. Name). 
        Nachdem Sie diese ausgew&#228;hlt haben (oder den Default ALLE gela&#223;en haben), Schl&#252;&#223;elwort f&#252;r die Suche eingeben. ',
	'PHPSHOP_SEARCH_TEXT2' => ' Sie k&#246;nnen Ihre Suche weiter verfeinern durch Angabe eines zweiten Schl&#252;&#223;elwortes und der Auswahl des UND oder NICHT-Operators. 
        Auswahl von UND bedeutet da&#223; beide W&#246;rter vorhanden sein m&#252;&#223;en damit das Produkt angezeigt wird. 
        Auswahl von NICHT bedeutet da&#223; das Produkt nur angezeigt wird wenn das erste Schl&#252;&#223;elwort vorhanden ist, 
        und nicht das zweite.',
	'PHPSHOP_CONTINUE_SHOPPING' => 'Weiter einkaufen',
	'PHPSHOP_AVAILABLE_IMAGES' => 'Verf&#252;gbare Bilder f&#252;r',
	'PHPSHOP_BACK_TO_DETAILS' => 'Zur&#252;ck zu Produktdetails',
	'PHPSHOP_IMAGE_NOT_FOUND' => 'Bild nicht gefunden!',
	'PHPSHOP_PARAMETER_SEARCH_TEXT1' => 'Wollen Sie Produkte nach technischen Parametern finden?<BR>Sie k&#246;nnen irgendein vorbereitetes Formular gebrauchen:',
	'PHPSHOP_PARAMETER_SEARCH_NO_PRODUCT_TYPE' => 'Es tut mir leid. Es gibt keine Kategorie f&#252;r die Suche.',
	'PHPSHOP_PARAMETER_SEARCH_BAD_PRODUCT_TYPE' => 'Es tut mir leid. Es gibt keinen ver&#246;ffentlichten Produkt-Typ mit diesem Namen.',
	'PHPSHOP_PARAMETER_SEARCH_IS_LIKE' => 'Ist wie',
	'PHPSHOP_PARAMETER_SEARCH_IS_NOT_LIKE' => 'Ist NICHT wie',
	'PHPSHOP_PARAMETER_SEARCH_FULLTEXT' => 'Volltextsuche',
	'PHPSHOP_PARAMETER_SEARCH_FIND_IN_SET_ALL' => 'Alle ausgew&#228;hlten',
	'PHPSHOP_PARAMETER_SEARCH_FIND_IN_SET_ANY' => 'Jede gew&#228;hlten',
	'PHPSHOP_PARAMETER_SEARCH_RESET_FORM' => 'Formular zur&#252;ckstellen',
	'PHPSHOP_PRODUCT_NOT_FOUND' => 'Leider wurde das angeforderte Produkt nicht gefunden!',
	'PHPSHOP_PRODUCT_PACKAGING1' => 'Nummer {unit}s in der Verpackung:',
	'PHPSHOP_PRODUCT_PACKAGING2' => 'Nummer {unit}s in der Box:',
	'PHPSHOP_CART_PRICE_PER_UNIT' => 'Preis pro Einheit',
	'VM_PRODUCT_ENQUIRY_LBL' => 'Stellen Sie eine Frage &#252;ber dieses Produkt',
	'VM_RECOMMEND_FORM_LBL' => 'Artikel an einen Freund empfehlen',
	'PHPSHOP_EMPTY_YOUR_CART' => 'Warenkorb leeren',
	'VM_RETURN_TO_PRODUCT' => 'Zur&#252;ck zum Produkt',
	'EMPTY_CATEGORY' => 'Diese Kategorie ist momentan leer.',
	'ENQUIRY' => 'Anfrage',
	'NAME_PROMPT' => 'Namen eingeben',
	'EMAIL_PROMPT' => 'Mail-Adre&#223;e',
	'ME&#223;AGE_PROMPT' => 'Nachricht eingeben',
	'SEND_BUTTON' => 'Senden',
	'THANK_ME&#223;AGE' => 'Danke f&#252;r Ihre Anfrage. Wir werden Sie so bald wie m&#246;glich kontaktieren.',
	'PROMPT_CLOSE' => 'Schlie&#223;en',
	'VM_RECOVER_CART_REPLACE' => 'Warenkorb mit gespeichertem Warenkorb ersetzen',
	'VM_RECOVER_CART_MERGE' => 'Gespeicherten Warenkorb zum aktuellen Warenkorb hinzuf&#252;gen',
	'VM_RECOVER_CART_DELETE' => 'Gespeicherten Warenkorb l&#246;schen ',
	'VM_EMPTY_YOUR_CART_TIP' => 'Wagen leeren',
	'VM_SAVED_CART_TITLE' => 'Gespeicherten Warenkorb',
	'VM_SAVED_CART_RETURN' => 'Zur&#252;ck'
); $VM_LANG->initModule( 'einkaufen', $langvars );
?>