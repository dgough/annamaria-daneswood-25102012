<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direkter Zugang zu '.basename(__FILE__).' ist nicht gestattet.' ); 
/**
*
* @version $Id: english.php 1071 2007-12-03 08:42:28Z thepisu $
* @package VirtueMart
* @subpackage languages
* @copyright Copyright � 2004-2007 soeren - All rights reserved.
* @translator soeren
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
global $VM_LANG;
$langvars = array (
	'CHARSET' => 'ISO-8859-1',
	'PHPSHOP_RB_INDIVIDUAL' => 'Individuelle Produkt-Angebote',
	'PHPSHOP_RB_SALE_TITLE' => 'Verkaufsberichte', // not used?
	'PHPSHOP_RB_SALES_PAGE_TITLE' => 'Verkaufst&#228;tigkeit im &#252;berblick', // not used?
	'PHPSHOP_RB_INTERVAL_TITLE' => 'Interval stellen',
	'PHPSHOP_RB_INTERVAL_MONTHLY_TITLE' => 'Monatlich',
	'PHPSHOP_RB_INTERVAL_WEEKLY_TITLE' => 'W&#246;chentlich',
	'PHPSHOP_RB_INTERVAL_DAILY_TITLE' => 'T&#228;glich',
	'PHPSHOP_RB_THISMONTH_BUTTON' => 'Dieser Monat',
	'PHPSHOP_RB_LASTMONTH_BUTTON' => 'Letzter Monat',
	'PHPSHOP_RB_LAST60_BUTTON' => 'Letzten 60 Tage',
	'PHPSHOP_RB_LAST90_BUTTON' => 'Letzten 90 Tage',
	'PHPSHOP_RB_START_DATE_TITLE' => 'Starten am',
	'PHPSHOP_RB_END_DATE_TITLE' => 'Enden am',
	'PHPSHOP_RB_SHOW_SEL_RANGE' => 'Zeige diesen gew&#228;hlten Bereich',
	'PHPSHOP_RB_REPORT_FOR' => 'Bericht f&#252;r ',
	'PHPSHOP_RB_DATE' => 'Datum',
	'PHPSHOP_RB_ORDERS' => 'Bestellungen',
	'PHPSHOP_RB_TOTAL_ITEMS' => 'Totale Artikel verkauft',
	'PHPSHOP_RB_REVENUE' => 'Umsatz',
	'PHPSHOP_RB_PRODLIST' => 'Produktliste'
); $VM_LANG->initModule( 'reportbasic', $langvars );
?>