<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die('Direkter Zugang zu'.basename(__FILE__).' ist nicht gestattet.' ); 
/**
*
* @version $Id: english.php 1071 2007-12-03 08:42:28Z thepisu $
* @package VirtueMart
* @subpackage languages
* @copyright Copyright � 2004-2007 soeren - All rights reserved.
* @translator soeren
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
global $VM_LANG;
$langvars = array (
	'CHARSET' => 'ISO-8859-1',
	'PHPSHOP_ADMIN_CFG_PRICES_INCLUDE_TAX' => 'Preise inklusiv Mehrwertsteuer zeigen?',
	'PHPSHOP_ADMIN_CFG_PRICES_INCLUDE_TAX_EXPLAIN' => 'Setzt die Flagge, ob die K&#228;ufer Preise inklusiv oder exklusiv Mehrwertsteuer sehen.',
	'PHPSHOP_SHOPPER_FORM_ADDRE&#223;_LABEL' => 'Adre&#223;enspitzname',
	'PHPSHOP_SHOPPER_GROUP_LIST_LBL' => 'K&#228;ufergruppenliste',
	'PHPSHOP_SHOPPER_GROUP_LIST_NAME' => 'Gruppenname',
	'PHPSHOP_SHOPPER_GROUP_LIST_DESCRIPTION' => 'Gruppenbeschreibung',
	'PHPSHOP_SHOPPER_GROUP_FORM_LBL' => 'K&#228;ufergruppen-Formular',
	'PHPSHOP_SHOPPER_GROUP_FORM_NAME' => 'Gruppenname',
	'PHPSHOP_SHOPPER_GROUP_FORM_DESC' => 'Gruppenbeschreibung',
	'PHPSHOP_SHOPPER_GROUP_FORM_DISCOUNT' => 'Rabatt f&#252;r Standard-K&#228;ufergruppe (in %en)',
	'PHPSHOP_SHOPPER_GROUP_FORM_DISCOUNT_TIP' => 'Ein positiver Betrag X bedeutet: Wenn das Produkt keinen Preis an DIESE K&#228;ufergruppe zugeordnet hat, wird der Standardpreis vermindert mit X %. Ein negativer Betrag bewirkt das Gegenteil'
); $VM_LANG->initModule( 'K&#228;ufer', $langvars );
?>