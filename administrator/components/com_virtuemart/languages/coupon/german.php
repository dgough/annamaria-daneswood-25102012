<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direkter Zugang zu '.basename(__FILE__).' ist nicht gestattet.' ); 
/**
*
* @version $Id: english.php 1071 2007-12-03 08:42:28Z thepisu $
* @package VirtueMart
* @subpackage languages
* @copyright Copyright � 2004-2007 soeren - All rights reserved.
* @translator soeren
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
global $VM_LANG;
$langvars = array (
	'CHARSET' => 'ISO-8859-1',
	'PHPSHOP_COUPON_EDIT_HEADER' => 'Gutschein Update',
	'PHPSHOP_COUPON_CODE_HEADER' => 'Kode',
	'PHPSHOP_COUPON_PERCENT_TOTAL' => 'Prozent oder Total',
	'PHPSHOP_COUPON_TYPE' => 'Gutschein Typ',
	'PHPSHOP_COUPON_TYPE_TOOLTIP' => 'Ein Geschenkgutschein wird gel�scht, nachdem er f�r die Abzinsung einer Buchung verwendet wurde. Ein permanenter Gutschein kann so oft verwendent werden wie der Kunde will.',
	'PHPSHOP_COUPON_TYPE_GIFT' => 'Geschenkgutschein',
	'PHPSHOP_COUPON_TYPE_PERMANENT' => 'Permanenter Gutschein',
	'PHPSHOP_COUPON_VALUE_HEADER' => 'Wert',
	'PHPSHOP_COUPON_PERCENT' => 'Prozent',
	'PHPSHOP_COUPON_TOTAL' => 'Total'
); $VM_LANG->initModule( 'Gutschein', $langvars );
?>