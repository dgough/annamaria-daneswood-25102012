<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direkter Zugang zu '.basename(__FILE__).' ist nicht gestattet.' ); 
/**
*
* @version $Id: english.php 1.0 2008-06-06 12:47:00 GMT Organic Development $
* @package VirtueMart
* @subpackage languages
* @copyright Copyright © 2004-2007 soeren - All rights reserved.
* @translator Organic Development
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
global $VM_LANG;
$langvars = array (
	
	'VM_CAL_YOUR_PRICES' => 'Ihre Preise',
	'VM_CAL_SELECT_DATE_RANGE' => 'Bitte w&#228;hlen Sie Ihre Zeitr&#228;ume aus dem oberen Kalender.',
	'VM_CAL_FULL_PRICE' => 'Voller Preis',
	'VM_CAL_DISCOUNTS' => 'Rabatte',
	'VM_CAL_DISCOUNTS_OFF_SEASON' => 'Nebensaison Rabatt',
	'VM_CAL_ONLINE_PRICE' => 'Ihr Online Preis',
	'VM_CAL_CHARGES' => 'Geb&#252;hren',
	'VM_CAL_ARRIVAL' => 'Ankunftsdatum',
	'VM_CAL_DEPARTURE' => 'Abfahrtsdatum',
	'VM_CAL_TODAYS_DATE' => 'Heutiges Datum',
	'VM_CAL_AVL_DATE' => 'Verf&#252;gbares Datum',
	'VM_CAL_UNAVL_DATE' => 'Datum nicht verf&#252;gbar',
	'VM_CAL_SELECTED_DATE' => 'Ausgew&#228;hlte Reisedaten',
	'VM_CAL_RETURN_LISTINGS' => 'Zur&#252;ck zu den Anzeigen',
	'VM_CAL_BOOK_NOW' => 'Jetzt buchen',
	'VM_CAL_PRINT' => 'Preise drucken',
	'VM_CAL_CLEAR' => 'Form löschen',
	'VM_CAL_GET_PRICE' => 'Preis erhalten',
);
$VM_LANG->initModule( 'calendar', $langvars );
?>
