<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
/**
*
* @version $Id: english.php 1.0 2008-06-06 12:47:00 GMT Organic Development $
* @package VirtueMart
* @subpackage languages
* @copyright Copyright (C) 2004-2007 soeren - All rights reserved.
* @translator Organic Development
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
global $VM_LANG;
$langvars = array (
	
	'VM_CAL_YOUR_PRICES' => 'Your Prices',
	'VM_CAL_SELECT_DATE_RANGE' => 'Please select your date ranges from the calendar above to obtain your pricing.',
	'VM_CAL_FULL_PRICE' => 'Full price',
	'VM_CAL_DISCOUNTS' => 'Discounts',
	'VM_CAL_DISCOUNTS_OFF_SEASON' => 'Off Season Discount',
	'VM_CAL_ONLINE_PRICE' => 'Your Online Price',
	'VM_CAL_CHARGES' => 'Charges',
	'VM_CAL_ARRIVAL' => 'Arrival Date',
	'VM_CAL_DEPARTURE' => 'Departure Date',
	'VM_CAL_TODAYS_DATE' => 'Todays Date',
	'VM_CAL_AVL_DATE' => 'Available Date',
	'VM_CAL_UNAVL_DATE' => 'Unavailable Date',
	'VM_CAL_SELECTED_DATE' => 'Selected Dates',
	'VM_CAL_RETURN_LISTINGS' => 'Return to the listings',
	'VM_CAL_BOOK_NOW' => 'Book Now',
	'VM_CAL_PRINT' => 'Print',
	'VM_CAL_CLEAR' => 'Clear',
	'VM_CAL_GET_PRICE' => 'Get Price',

	
	
	
	
);
$VM_LANG->initModule( 'calendar', $langvars );
?>