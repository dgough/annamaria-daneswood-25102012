<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
/**
*
* @version $Id: english.php 1.0 2008-06-06 12:47:00 GMT Organic Development $
* @package VirtueMart
* @subpackage languages
* @copyright Copyright (C) 2004-2007 soeren - All rights reserved.
* @translator Organic Development
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
global $VM_LANG;
$langvars = array (
	
	'VM_CAL_YOUR_PRICES' => 'Ihre Preise',
	'VM_CAL_SELECT_DATE_RANGE' => 'Bitte wählen Sie Ihre Termine links auf dem Kalender aus.',
	'VM_CAL_FULL_PRICE' => 'voller Preis',
	'VM_CAL_DISCOUNTS' => 'Rabatte',
	'VM_CAL_DISCOUNTS_OFF_SEASON' => 'Rabatt / Nebensaison',
	'VM_CAL_ONLINE_PRICE' => 'Ihr Online-Preis',
	'VM_CAL_CHARGES' => 'Kosten',
	'VM_CAL_ARRIVAL' => 'Datum der Ankunft',
	'VM_CAL_DEPARTURE' => 'Datum der Abreise',
	'VM_CAL_TODAYS_DATE' => 'heutiges Datum',
	'VM_CAL_AVL_DATE' => 'verfügbarer Termin',
	'VM_CAL_UNAVL_DATE' => 'nicht-verfügbarer Termin',
	'VM_CAL_SELECTED_DATE' => 'ausgewählte Termine',
	'VM_CAL_RETURN_LISTINGS' => 'zurück zur Liste',
	'VM_CAL_BOOK_NOW' => 'Jetzt buchen',
	'VM_CAL_CLEAR' => 'löschen',
	'VM_CAL_GET_PRICE' => 'Preis kriegen',
	'DAY' => 'Tag',
	'MONTH' => 'Monat',
	'YEAR' => 'Jahr'	
);
$VM_LANG->initModule( 'calendar', $langvars );
?>