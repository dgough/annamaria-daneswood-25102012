<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direkter Zugang zu '.basename(__FILE__).' ist nicht gestattet.' ); 
/**
*
* @version $Id: english.php 1071 2007-12-03 08:42:28Z thepisu $
* @package VirtueMart
* @subpackage languages
* @copyright Copyright � 2004-2007 soeren - All rights reserved.
* @translator soeren
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
global $VM_LANG;
$langvars = array (
	'CHARSET' => 'ISO-8859-1',
	'PHPSHOP_CARRIER_LIST_LBL' => 'Versenderliste',
	'PHPSHOP_RATE_LIST_LBL' => 'Versandkosten Liste',
	'PHPSHOP_CARRIER_LIST_NAME_LBL' => 'Name',
	'PHPSHOP_CARRIER_LIST_ORDER_LBL' => 'Buchungsliste',
	'PHPSHOP_CARRIER_FORM_LBL' => 'Versender bearbeiten / erstellen',
	'PHPSHOP_RATE_FORM_LBL' => 'Eine Versandskosten-Flatrate erstellen/ bearbeiten',
	'PHPSHOP_RATE_FORM_NAME' => 'Versandpreis Beschreibung',
	'PHPSHOP_RATE_FORM_CARRIER' => 'Versender',
	'PHPSHOP_RATE_FORM_COUNTRY' => 'Land',
	'PHPSHOP_RATE_FORM_ZIP_START' => 'ZIP Reichweite Anfang',
	'PHPSHOP_RATE_FORM_ZIP_END' => 'ZIP Reichweite Ende',
	'PHPSHOP_RATE_FORM_WEIGHT_START' => 'Geringstes Gewicht',
	'PHPSHOP_RATE_FORM_WEIGHT_END' => 'H&#246;chstes Gewicht',
	'PHPSHOP_RATE_FORM_PACKAGE_FEE' => 'Ihre Paket Geb&#220;hr',
	'PHPSHOP_RATE_FORM_CURRENCY' => 'W&#228;hrung',
	'PHPSHOP_RATE_FORM_LIST_ORDER' => 'Buchungsliste',
	'PHPSHOP_SHIPPING_RATE_LIST_CARRIER_LBL' => 'Versender',
	'PHPSHOP_SHIPPING_RATE_LIST_RATE_NAME' => 'Versandpreis Beschreibung',
	'PHPSHOP_SHIPPING_RATE_LIST_RATE_WSTART' => 'Gewicht von �',
	'PHPSHOP_SHIPPING_RATE_LIST_RATE_WEND' => '� bis',
	'PHPSHOP_CARRIER_FORM_NAME' => 'Versender Unternehmen',
	'PHPSHOP_CARRIER_FORM_LIST_ORDER' => 'Buchungsliste'
); $VM_LANG->initModule( 'shipping', $langvars );
?>