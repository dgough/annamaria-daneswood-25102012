<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direkter Zugang zu '.basename(__FILE__).' ist nicht gestattet.' ); 
/**
*
* @version $Id: english.php 1071 2007-12-03 08:42:28Z thepisu $
* @package VirtueMart
* @subpackage languages
* @copyright Copyright © 2004-2007 soeren - All rights reserved.
* @translator soeren
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
global $VM_LANG;
$langvars = array (
	'CHARSET' => 'ISO-8859-1',
	'VM_HELP_YOURVERSION' => 'Ihre {Produkt} Version',
	'VM_HELP_ABOUT' => '<span style="font-weight: bold;">
		VirtueMart</span> ist die komplette Open Source E-Commerce L&#214;sung für Mambo und Joomla!. 
		Es ist eine Anwendung die mit einem Komponenten kommt, und mit mehr als 8 Modulen und Mambots / Plugins.
		Es hat seine Wurzeln in einem Warenkorb-Skript namens "phpShop" (Authors: Edikon Corp. & the <a href="http://www.virtuemart.org/" target="_blank">phpShop</a> community).',
	'VM_HELP_LICENSE_DESC' => 'VirtueMart ist lizensiert unter der <a href="{licenseurl}" target="_blank">{licensename} Lizenz</a>.',
	'VM_HELP_TEAM' => 'Ein kleines Team von Entwicklern helfen bei der Weiterentwicklung dieser Warenkorb-Skript.',
	'VM_HELP_PROJECTLEADER' => 'Projektleiter',
	'VM_HELP_HOMEPAGE' => 'Startseite',
	'VM_HELP_DONATION_DESC' => 'Wie w&#228;r\'s mit einer kleinen Spende an das VirtueMart Projekt? Das wird uns helfen um die Arbeit an diesem Komponenten weiterzuführen und neue Funktionen zu erschaffen.',
	'VM_HELP_DONATION_BUTTON_ALT' => 'Zahlen Sie mit PayPal - es ist schnell, kostenlos und sicher!'
); $VM_LANG->initModule( 'help', $langvars );
?>