<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direkter Zugang zu '.basename(__FILE__).' ist nicht gestattet.' ); 
/**
*
* @version $Id: english.php 1071 2007-12-03 08:42:28Z thepisu $
* @package VirtueMart
* @subpackage languages
* @copyright Copyright © 2004-2007 soeren - All rights reserved.
* @translator soeren
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
global $VM_LANG;
$langvars = array (
	'CHARSET' => 'ISO-8859-1',
	'PHPSHOP_TAX_LIST_LBL' => 'Steuersatz-Liste',
	'PHPSHOP_TAX_LIST_STATE' => 'Mehrwertsteuer Staat oder Gebiet',
	'PHPSHOP_TAX_LIST_COUNTRY' => 'Mehrwertsteuer Land',
	'PHPSHOP_TAX_FORM_LBL' => 'Steuerinformation zuf&#252;gen',
	'PHPSHOP_TAX_FORM_STATE' => 'Mehrwertsteuer Staat oder Gebiet',
	'PHPSHOP_TAX_FORM_COUNTRY' => 'Mehrwertsteuer Land',
	'PHPSHOP_TAX_FORM_RATE' => 'Steuersatz (f&#252;r 16% => 0.16 einf&#252;llen)'
); $VM_LANG->initModule( 'Steuern', $langvars );
?>