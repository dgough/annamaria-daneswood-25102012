<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direkter Zugang zu '.basename(__FILE__).' ist nicht gestattet.' ); 
/**
*
* @version $Id: english.php 1071 2007-12-03 08:42:28Z thepisu $
* @package VirtueMart
* @subpackage languages
* @copyright Copyright © 2004-2007 soeren - All rights reserved.
* @translator soeren
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
global $VM_LANG;
$langvars = array (
	'CHARSET' => 'ISO-8859-1',
	'PHPSHOP_VENDOR_LIST_LBL' => 'H&#228;ndlerliste',
	'PHPSHOP_VENDOR_LIST_ADMIN' => 'Admin',
	'PHPSHOP_VENDOR_FORM_LBL' => 'Information hinzufügen',
	'PHPSHOP_VENDOR_FORM_CONTACT_LBL' => 'Kontakt-Information',
	'PHPSHOP_VENDOR_FORM_FULL_IMAGE' => 'Volles Bild',
	'PHPSHOP_VENDOR_FORM_UPLOAD' => 'Bild hochladen',
	'PHPSHOP_VENDOR_FORM_STORE_NAME' => 'H&#228;ndler Ladenname',
	'PHPSHOP_VENDOR_FORM_COMPANY_NAME' => 'H&#228;ndler Firmenname',
	'PHPSHOP_VENDOR_FORM_ADDRESS_1' => 'Adresse 1',
	'PHPSHOP_VENDOR_FORM_ADDRESS_2' => 'Adresse 2',
	'PHPSHOP_VENDOR_FORM_CITY' => 'Stadt',
	'PHPSHOP_VENDOR_FORM_STATE' => 'Staat/Provinz/Gebiet',
	'PHPSHOP_VENDOR_FORM_COUNTRY' => 'Land',
	'PHPSHOP_VENDOR_FORM_ZIP' => 'ZIP / Postleitzahl',
	'PHPSHOP_VENDOR_FORM_PHONE' => 'Telefon',
	'PHPSHOP_VENDOR_FORM_CURRENCY' => 'W&#228;hrung',
	'PHPSHOP_VENDOR_FORM_CATEGORY' => 'H&#228;ndler Kategorie',
	'PHPSHOP_VENDOR_FORM_LAST_NAME' => 'Nachname',
	'PHPSHOP_VENDOR_FORM_FIRST_NAME' => 'Vorname',
	'PHPSHOP_VENDOR_FORM_MIDDLE_NAME' => 'Weitere Vornamen',
	'PHPSHOP_VENDOR_FORM_TITLE' => 'Titel',
	'PHPSHOP_VENDOR_FORM_PHONE_1' => 'Telefon 1',
	'PHPSHOP_VENDOR_FORM_PHONE_2' => 'Telefon 2',
	'PHPSHOP_VENDOR_FORM_FAX' => 'Fax',
	'PHPSHOP_VENDOR_FORM_EMAIL' => 'Mail',
	'PHPSHOP_VENDOR_FORM_IMAGE_PATH' => 'Bildpfad',
	'PHPSHOP_VENDOR_FORM_DESCRIPTION' => 'Beschreibung',
	'PHPSHOP_VENDOR_CAT_LIST_LBL' => 'H&#228;ndler Kategorienliste',
	'PHPSHOP_VENDOR_CAT_NAME' => 'Kategorienname',
	'PHPSHOP_VENDOR_CAT_DESCRIPTION' => 'Kategorienbeschreibung',
	'PHPSHOP_VENDOR_CAT_VENDORS' => 'H&#228;ndler',
	'PHPSHOP_VENDOR_CAT_FORM_LBL' => 'H&#228;ndler Kategorienformular',
	'PHPSHOP_VENDOR_CAT_FORM_INFO_LBL' => 'Kategorieninformation',
	'PHPSHOP_VENDOR_CAT_FORM_NAME' => 'Kategorienname',
	'PHPSHOP_VENDOR_CAT_FORM_DESCRIPTION' => 'Kategorienbeschreibung'
); $VM_LANG->initModule( 'H&#228;ndler', $langvars );
?>