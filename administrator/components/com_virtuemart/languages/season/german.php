<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direkter Zugang zu '.basename(__FILE__).' ist nicht gestattet.' ); 
/**
*
* @version $Id: english.php 1.0 2008-06-06 12:47:00 GMT Organic Development $
* @package VirtueMart
* @subpackage languages
* @copyright Copyright (C) 2004-2007 soeren - All rights reserved.
* @translator Organic Development
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
global $VM_LANG;
$langvars = array (
	'VM_BK_NAV_HEADING' => 'Saison Preise',
	'VM_BK_NAV_LIST_LABEL' => 'Saison Preisliste',
	'VM_BK_NAV_ADD_LABEL' => 'Saison Preise beif&#220;gen',
	'VM_BK_LIST_HEADING' => 'Saison Preisliste',
	'VM_BK_LIST_LBL' => 'Saison',
	'VM_BK_LIST_PROPERTY' => 'Immobilie',
	'VM_BK_LIST_STARTDATE' => 'Startdatum',
	'VM_BK_LIST_ENDDATE' => 'Enddatum',
	'VM_BK_LIST_RATE' => '&#220;bernachtungspreis',
	'VM_BK_LIST_RATE_WEEKLY' => 'Wochenpreis',
	'VM_BK_LIST_DISCOUNT' => 'Rabatt',
	'VM_BK_FORM_LBL' => 'Saison-Etikett',
	'VM_BK_FORM_PROPERTY' => 'Zugeordnete Immobilie',
	'VM_BK_FORM_STARTDATE' => 'Beginndatum der Saison',
	'VM_BK_FORM_ENDDATE' => 'Enddatum der Saison',
	'VM_BK_FORM_RATE' => 'Enddatum der Saison',
	'VM_BK_FORM_MINPEOPLE' => 'Mindestanzahl Personen',
	'VM_BK_FORM_MAXPEOPLE' => 'Maximalanzahl Personen',
	'VM_BK_FORM_MINSTAY' => 'Mindestaufenthalt (N&#228;chte)',
	'VM_BK_FORM_CHANGEOVER' => 'Wechseltag',
	'VM_BK_SEASION_ADDED' => 'Saison erfolgreich hinzugef&#220;gt',
	'VM_BK_SEASON_ADD_FAILED' => 'Saison Hinzuf&#220;gung gescheitert',
	'VM_BK_SEASON_UPDATED' => 'Saison erfolgreich aktualisiert',
	'VM_BK_SEASON_UPDATE_FAILED' => 'Saison-Update gescheitert',	
	'VM_BK_SEASON_ERR_UPDATE_SELECT' => 'Kann Saison nicht finden',
	'VM_BK_SEASON_START' => 'Startdatum',
	'VM_BK_SEASON_END' => 'Enddatum',
	'VM_BK_SEASON_MIN_PPL_FORCE' => 'Min Personen erzwingen',
	'VM_BK_SEASON_MIN_PPL' => 'Mindestanzahl Personen',
	'VM_BK_SEASON_MAX_PPL' => 'Maximalanzahl Personen',
	'VM_BK_SEASON_MIN_STAY' => 'Mindestaufenthalt (N&#228;chte)',
	'VM_BK_SEASON_MAX_STAY' => 'Maximalaufenthalt (N&#228;chte)',
	'VM_BK_SEASON_LONG_DAYS' => 'Langzeiturlaubstage (Tage um f&#220;r l&#228;ngeren Aufenthalt Rabatt zu qualifizieren)',
	'VM_BK_SEASON_LONG_RATE' => 'Langzeiturlaub Preis (Abzinsungssatz f&#220;r Langzeiturlaub pro Tag)',
	'VM_BK_SEASON_CHANGEOVER' => 'Wechseltag (geben Sie einen Wechseltag f&#220;r die Immobilie ein)',
	'VM_BK_SEASON_FCHANGEOVER' => 'Wechseltag erzwingen (um Wechseltage zu erzwingen wird Buchungen zwingen um an Wechseltagen zu starten und zu enden)',
	'VM_BK_SEASON_RATE' => 'Saison &#220;bernachtungspreis',
	'VM_BK_SEASON_RATE_WEEKLY' => 'Saison Wochenpreis',
	'VM_BK_SEASON_DISCOUNT' => 'Saisonsrabatt (in %en)',
	'VM_BK_SEASON_DAYS' => array('Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag','Sonntag'),
	'VM_BK_TT_CHANGEOVER' => 'Eine Zahl um den Wochentag darzustellen (z.B. 1 = Montag)',
	'VM_BK_TT_MINPEOPLE' => 'Die Mindestanzahl Personen denen erlaubt wird in diesem Saisonbereich und f&#220;r diese Immobilie zu buchen',
	'VM_BK_TT_MAXPEOPLE' => 'Die Maximalanzahl Personen denen erlaubt wird in diesem Saisonbereich und f&#220;r diese Immobilie zu buchen'
);
$VM_LANG->initModule( 'season', $langvars );
?>