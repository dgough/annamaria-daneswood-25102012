<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
/**
*
* @version $Id: english.php 1.0 2008-06-06 12:47:00 GMT Organic Development $
* @package VirtueMart
* @subpackage languages
* @copyright Copyright (C) 2004-2007 soeren - All rights reserved.
* @translator Organic Development
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
global $VM_LANG;
$langvars = array (
	'VM_BK_NAV_HEADING' => 'Season Pricing',
	'VM_BK_NAV_LIST_LABEL' => 'Season Pricing List',
	'VM_BK_NAV_ADD_LABEL' => 'Add Season Pricing',
	
	'VM_BK_LIST_HEADING' => 'Season Pricing List',
	'VM_BK_LIST_LBL' => 'Season',
	'VM_BK_LIST_PROPERTY' => 'Property',
	'VM_BK_LIST_STARTDATE' => 'Start Date',
	'VM_BK_LIST_ENDDATE' => 'End Date',
	'VM_BK_LIST_RATE' => 'Nightly Rate',
	'VM_BK_LIST_RATE_WEEKLY' => 'Weekly Rate',
	'VM_BK_LIST_DISCOUNT' => 'Discount',
	
	'VM_BK_FORM_LBL' => 'Season label',
	'VM_BK_FORM_PROPERTY' => 'Associated property',
	'VM_BK_FORM_STARTDATE' => 'Start date of season',
	'VM_BK_FORM_ENDDATE' => 'End date of season',
	'VM_BK_FORM_RATE' => 'End date of season',
	'VM_BK_FORM_MINPEOPLE' => 'Minimum no. of people',
	'VM_BK_FORM_MAXPEOPLE' => 'Maximum no. of people',
	'VM_BK_FORM_MINSTAY' => 'Minimum stay (nights)',
	'VM_BK_FORM_CHANGEOVER' => 'Changeover day',
	'VM_BK_SEASION_ADDED' => 'Season Added Successfully',
	'VM_BK_SEASON_ADD_FAILED' => 'Season add failed',
	'VM_BK_SEASON_UPDATED' => 'Season Updated Successfully',
	'VM_BK_SEASON_UPDATE_FAILED' => 'Season update failed',	
	'VM_BK_SEASON_ERR_UPDATE_SELECT' => 'Unable to find Season',
	'VM_BK_SEASON_START' => 'Start Date',
	'VM_BK_SEASON_END' => 'End Date',
	'VM_BK_SEASON_MIN_PPL_FORCE' => 'Force Min People',
	'VM_BK_SEASON_MIN_PPL' => 'Min People',
	'VM_BK_SEASON_MAX_PPL' => 'Max People',
	'VM_BK_SEASON_MIN_STAY' => 'Min Stay (in nights)',
	'VM_BK_SEASON_MAX_STAY' => 'Max Stay (in nights)',
	'VM_BK_SEASON_LONG_DAYS' => 'Long Stay Days (days to qualify for long stay discount)',
	'VM_BK_SEASON_LONG_RATE' => 'Long Stay Rate (discount rate for long stays per day)',
	'VM_BK_SEASON_CHANGEOVER' => 'Changeover Day (enter a change over day for the property)',
	'VM_BK_SEASON_FCHANGEOVER' => 'Force Changeover Day (forcing changeover days will force bookings to start and end on changeover days)',
	'VM_BK_SEASON_RATE' => 'Season Nightly Rate',
	'VM_BK_SEASON_RATE_WEEKLY' => 'Season Weekly Rate',
	'VM_BK_SEASON_DISCOUNT' => 'Season Discount (in %)',
	'VM_BK_SEASON_DAYS' => array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'),
	
	'VM_BK_TT_CHANGEOVER' => 'A number to represent the day of the week (e.g. 1 = monday)',
	'VM_BK_TT_MINPEOPLE' => 'The Minimum no. of people that are allowed to book in this season range and for this property',
	'VM_BK_TT_MAXPEOPLE' => 'The Maximum no. of people that are allowed to book in this season range and for this property'
);
$VM_LANG->initModule( 'season', $langvars );
?>