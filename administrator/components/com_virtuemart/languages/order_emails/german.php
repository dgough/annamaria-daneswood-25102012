<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direkter Zugang zu '.basename(__FILE__).' ist nicht gestattet.' ); 
/**
*
* @version $Id: english.php 1071 2007-12-03 08:42:28Z thepisu $
* @package VirtueMart
* @subpackage languages
* @copyright Copyright © 2004-2007 soeren - All rights reserved.
* @translator soeren
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
global $VM_LANG;
$langvars = array (
	'CHARSET' => 'ISO-8859-1',
	'VM_ORDER_EMAIL_GO_SITE' => 'Zur Anna Maria Website',

	//resevation email
	'VM_ORDER_EMAIL_DEAR' => 'Liebe/r ',
	'VM_ORDER_EMAIL_CONFIRMATION' => 'RESERVIERUNGSBESTÄTIGUNG VOR IHRER GEPLANTEN ANKUNFT  - ES IST IHRE VERANTWORTUNG, IHRE RESERVIERUNGSDETAILS GRÜNDLICH ZU LESEN. BITTE DRUCKEN SIE ALLE INFORMATION AUF IHRER PERSÖNLICHEN WEBSEITE AUS (EINSCHLIEßLICH DER SCHLÜSSELBOX-KODE DIE NÖTIG IST, UM DAS GEBÄUDE BETRETEN – DIE SCHLÜSSELBOX IST NEBEN DER VORDERTÜR), DIESE MAIL, DEN LAGEPLAN UND BRINGEN SIE DIESE MIT',
	'VM_ORDER_EMAIL_ON_OR_AFTER' => '4pm oder später',
	'VM_ORDER_EMAIL_GUEST_NAME' => 'Gast Name:',
	'VM_ORDER_EMAIL_GUEST_DETAILS' => 'Gastdetails',
	'VM_ORDER_EMAIL_GUEST_NAME' => 'Gast Name:',
	'VM_ORDER_EMAIL_GUEST_ADDRESS' => 'Gast-Adresse:',
	'VM_ORDER_EMAIL_GUEST_TELEPHONE' => 'Gasttelefon:',
	'VM_ORDER_EMAIL_GUEST_EMAIL' => 'Gast Mail-Adresse:',
	'VM_ORDER_EMAIL_WEBSITE' => 'Ihre persönliche Webseite',
	'VM_ORDER_EMAIL_DETAILS' => 'Immobiliendetails',
	'VM_ORDER_EMAIL_PROPERTY_NAME' => 'Immobilienname:',
	'VM_ORDER_EMAIL_PROPERTY_ADDRESS' => 'Immobilienadresse:',
	'VM_ORDER_EMAIL_PROPERTY_PHONE' => 'Immobilien Telefonnummer:',
	'VM_ORDER_EMAIL_RESERVATION' => 'Reservierungsdetails',
	'VM_ORDER_EMAIL_ARRIVAL_DATE' => 'Ankunftsdatum:',
	'VM_ORDER_EMAIL_DEPARTURE_DATE' => 'Abreisedatum:',
	'VM_ORDER_EMAIL_NUMBER_PEOPLE' => 'Anzahl der Personen:',
	'VM_ORDER_EMAIL_BALANCE_OUTSTANDING' => 'Restschuld:',
	'VM_ORDER_EMAIL_CHECK_IN' => 'Check-In:',
	'VM_ORDER_EMAIL_' => '4pm oder später',
	'VM_ORDER_EMAIL_CHECK_OUT' => 'Check-Out:',
	'VM_ORDER_EMAIL_LATEST_DEPARTURE' => 'Spätestens 10am am Tag der Abreise',
	'VM_ORDER_EMAIL_KEY_PICKUP' => 'Schlüsselübergabe:',
	'VM_ORDER_EMAIL_PLEASE_BRING_MAP' => 'Bitte drucken Sie die Karte mit Richtungen zu Ihrer Immobilie aus',
	'VM_ORDER_EMAIL_CLICK_FOR_MORE' => 'Hier klicken für den Immobilien Lageplan',

	//post-vacation-email
	'VM_ORDER_EMAIL_WE_MISS_YOU' => '<p>Wir vermissen Sie<BR>
  <BR>
  Vielen Dank daß Sie Anna Maria Vacations gewählt haben. Wir hoffen daß Sie Ihren Aufenthalt in %s genossen haben, und daß der gesamte Prozess von der Auswahl zum Bleiben einfach und stressfrei war. <BR>
  <BR>
 Wir freuen uns über jeden Feedback von Ihnen, so wie, wie fanden Sie die Web-Site, war der Buchungsprozess einfach, hat die Immobilie Ihren Erwartungen entsprochen? Wir freuen uns über konstruktive Kritik, aber wir lieben positive Kommentare. <BR>
  <BR>
  Wenn Sie Freunde oder Familie als neue Mieter empfehlen, zahlen wir Ihnen $100 zu auf Ihren nächsten Urlaub, für jede volle Buchungswoche die Sie erhalten. Bitte mailen Sie uns mit den Details.<BR>
  <BR>
  Als vorheriger Mieter, sind Sie sowieso zu $100 pro Woche Ermäßigung berechtigt, bis zu einem Maximum von $200, wonach unser langfristiger Rabatt eintritt.<BR>
  <BR>
  Wir danken Ihnen wiederum daß Sie Anna Maria Vacations gewählt haben und freuen uns auf Ihre baldige Rückkehr in unser kleines Stück Paradies. &nbsp;<BR>
  <BR>
  <BR>
  Von dem gesamten &nbsp;Anna Maria Vacations Personal<BR>
</p>',
	'VM_ORDER_EMAIL_PAYMENT_IS_DUE' => 'Bezahlung erfolgt für Bestellung:',
	'VM_ORDER_EMAIL_PLEASE_SEE_DETAILS' => '. Bitte Sehen Sie die unteren Details.
Wenn Sie Ihre Zahlung bereits gemacht haben, bitte ignorieren Sie diese Mail.',
	'VM_ORDER_EMAIL_PROPERTY' => 'Immobilie:',
	//'VM_ORDER_EMAIL_ARRIVAL_DATE' => 'Ankunftsdatum:',
	//'VM_ORDER_EMAIL_DEPARTURE_DATE' => 'Abreisedatum:',
	'VM_ORDER_EMAIL_NUMBER_OF_GUESTS' => 'Anzahl der Gäste:',
	'VM_ORDER_EMAIL_NOTES_EMAIL' => '
	<p>
	Anmerkungen:<br /><br />
	Bitte wieder die<a href=\'%s\'> Allgemeinen Geschäftsbedingungen</a>veröffentlicht auf unserer Website lesen z.B. Anzahl der Gäste, Haustiere, usw. <br />
	Siehe auch Häufig Gestellte Fragen auf unserer Website, die praktisch alle Fragen bezüglich der Immobilien beantworten die je durch irgendeinen Gast gefragt wurden.<br /> 
	Für spezifische Fragen über Ihre Immobilie, bitte gehen Sie zu "Alle Immobilien" auf unserer <a href=\'%s\'>Startseite</a> und wählen Sie die Information für Ihre reservierte Ferienwohnung.<br />
	Wir stellen keine Strandtücher zur Verfügung. Wir bieten aber Handtücher und Bettwäsche.<br />
	Anzahl der Gäste vereinbart darf nicht überschritten werden <br />
	Lokale Telefonnummern um uns im Notfall zu kontaktieren während Ihres Aufenthaltes<br /> 
	<br />
	Verwaltung: Büro 941-778-4178,  anrufen zwischen 8am und 5pm<br /> 
	Instandhaltung / Reparatur oder Haushaltung: 941-713-0284 <br />
	Schwimmbad & Pflege: 941-301-1183 <br />
	<br />
	<br />
	Genießen Sie Ihren Besuch auf der wunderschönen Anna Maria Insel an der Golfküste Floridas<br /> 
	<br />   
	Reservierungsabteilung,<br />',
	//payment_email
	'VM_ORDER_EMAIL_DEAR' => 'Liebe/r ',
	'VM_ORDER_EMAIL_PAYMENT_DUE' => 'Bezahlung erfolgt für Bestellung:',
	'VM_ORDER_EMAIL_SEE_DETAILS' => 'Bitte Sehen Sie die unteren Details',
	'VM_ORDER_EMAIL_IF_YOU_HAVE_PAID' => 'Wenn Sie Ihre Zahlung bereits gemacht haben, bitte ignorieren Sie diese Mail.',
	//'VM_ORDER_EMAIL_PROPERTY' => 'Immobilie',
	//'VM_ORDER_EMAIL_ARRIVAL_DATE' => 'Ankunftsdatum',
	//'VM_ORDER_EMAIL_DEPARTURE_DATE' => 'Abreisedatum',
	//'VM_ORDER_EMAIL_NUMBER_GUESTS' => 'Anzahl der Gäste',
	'VM_ORDER_EMAIL_ALMOST_DUE' => 'Fälliger Betrag:',
		'VM_ORDER_EMAIL_ADDRESS' => '',
	'VM_ORDER_EMAIL_OUSTANDING_BALANCE' => '
Bitte besuchen Sie %s um Ihre Restbeträge zu zahlen.
Bitte sehen Sie Ihre persönliche Webseite %s für Ihre Reservierungsinformation.
Dies ist eine computergenerierte Zahlungsanforderung.  Am heutigen Tag scheint es daß wir noch nicht die Bezahlung für Ihren nächsten Urlaub erhalten haben.
Hinweis: für Last-Minute-Reservierungen ist Ihre volle Zahlung nun sofort fällig.
Wenn wir Ihre Zahlung innerhalb der nächsten 10 Tage nicht erhalten haben, wird die Immobilie wieder auf dem Internet zur Verfügung gestellt. Beachten Sie jedoch, daß Sie unter unseren AGBs noch immer verantwortlich sind für die vollständige Bezahlung, es sei denn wir können die Immobilie wieder vermieten. In diesem Fall werden wir die Gelder von Ihrem Konto abziehen und Sie werden verantwortlich für den Fehlbetrag sein, ',

//payment_email.html.tpl.php
	'VM_ORDER_EMAIL_OUSTANDING_BALANCE_FORMATTED' => '<p>Bitte besuchen Sie <a href="%s">Ihr Zahlungsgebiet</a> um Ihre Restbeträge zu zahlen.</p>
			<p>Bitte sehen Sie Ihre <a href="%s">Persönliche Webseite</a> für Ihre Reservierungsinformation.</p>
			<p>Dies ist eine computergenerierte Zahlungsanforderung.  Am heutigen Tag scheint es daß wir noch nicht die Bezahlung für Ihren nächsten Urlaub erhalten haben.</p>
			<p>Hinweis: für Last-Minute-Reservierungen ist Ihre volle Zahlung nun sofort fällig.</p>
			<p style="font-size: 90%">Wenn wir Ihre Zahlung innerhalb der nächsten 10 Tage nicht erhalten haben, wird die Immobilie wieder auf dem Internet zur Verfügung gestellt.  Beachten Sie jedoch, daß Sie unter unseren AGBs noch immer verantwortlich sind für die vollständige Bezahlung, es sei denn wir können die Immobilie wieder vermieten. In diesem Fall werden wir die Gelder von Ihrem Konto abziehen und Sie werden verantwortlich für den Fehlbetrag sein. </p>',

//confirmation
	'VM_ORDER_EMAIL_YOUR_PERSONAL_WEB' => 'Ihre Persönliche Webseite-Details',
	'VM_ORDER_EMAIL_YOUR_PERSONAL_WEB_LOGIN' => 'Sie können in Ihre persönliche Webseite einloggen mit dem Nutzernamen und Passwort, die Ihnen in einer separaten Mail zugesendet wurden',
	'VM_ORDER_EMAIL_BOOKING_CONFIRRM' => 'Vielen Dank für Ihre Buchung. Bitte folgen Sie den Link unten, um Ihre aktuellen und früheren Buchungen zu sehen. Hier finden Sie zusätzliche Buchungsinformation, einschließlich einer Karte, den Schlüssel-Kode um die Immobilie zu betreten, und Sie können Ihre persönlichen Kontodaten aktualisieren.',
	'VM_ORDER_EMAIL_YOUR_USERNAME' => 'Ihr Nutzername ist',
	'VM_ORDER_EMAIL_PASSWORD_EMAILED' => ' und Ihr Passwort wurde Ihnen per Mail gesendet, nachdem Sie beigetreten sind.',

//maintenance_email

	'VM_ORDER_EMAIL_DEAR_NAME' => 'Liebe/r [NAME],',
	'VM_ORDER_EMAIL_WEEKLY_SCHEDULE' => 'Finden Sie den wöchentlichen Checkout Zeitplan für [DATE_FROM] bis [DATE_TO]',
	'VM_ORDER_EMAIL_PROPERTY_B' => 'Immobilie',
	'VM_ORDER_EMAIL_CHECK_IN_OUT' => 'Check-out/Check-in',
	'VM_ORDER_EMAIL_THNAKS' => 'Danke',

	
); $VM_LANG->initModule( 'order_email', $langvars );
?>