<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
/**
*
* @version $Id: english.php 1071 2007-12-03 08:42:28Z thepisu $
* @package VirtueMart
* @subpackage languages
* @copyright Copyright (C) 2004-2007 soeren - All rights reserved.
* @translator soeren
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
global $VM_LANG;
$langvars = array (
	'CHARSET' => 'ISO-8859-1',

	'VM_ORDER_EMAIL_GO_SITE' => 'Go to Anna Maria web site',
	
	//resevation email
	'VM_ORDER_EMAIL_DEAR' => 'Dear ',
	'VM_ORDER_EMAIL_CONFIRMATION' => 'CONFIRMATION OF YOUR RESERVATION PRIOR TO YOUR SCHEDULED ARRIVAL  - IT IS YOUR RESPONSIBILITY TO READ ALL YOUR RESERVATION DETAILS THOROUGHLY. PLEASE PRINT ALL INFORMATION ON YOUR PERSONAL WEB PAGE. PLEASE BRING THIS EMAIL AND LOCATION MAP WITH YOU. PLEASE MAKE SURE YOU RETRIEVE YOUR KEY BOX CODE NEEDED TO ENTER THE PROPERTY. THE KEY BOX IS LOCATED BESIDE THE FRONT DOOR.',
	'VM_ORDER_EMAIL_ON_OR_AFTER' => 'On or after 4pm',
	'VM_ORDER_EMAIL_GUEST_NAME' => 'Guest Name:',
	'VM_ORDER_EMAIL_GUEST_DETAILS' => 'Guest Details',
	'VM_ORDER_EMAIL_GUEST_NAME' => 'Guest Name:',
	'VM_ORDER_EMAIL_GUEST_ADDRESS' => 'Guest Address:',
	'VM_ORDER_EMAIL_GUEST_TELEPHONE' => 'Guest Telephone:',
	'VM_ORDER_EMAIL_GUEST_EMAIL' => 'Guest email:',
	'VM_ORDER_EMAIL_WEBSITE' => 'Your Personal Web Page',
	'VM_ORDER_EMAIL_DETAILS' => 'Property Details',
	'VM_ORDER_EMAIL_PROPERTY_NAME' => 'Property Name:',
	'VM_ORDER_EMAIL_PROPERTY_ADDRESS' => 'Property Address:',
	'VM_ORDER_EMAIL_PROPERTY_PHONE' => 'Property Phone No:',
	'VM_ORDER_EMAIL_RESERVATION' => 'Reservation Details',
	'VM_ORDER_EMAIL_ARRIVAL_DATE' => 'Arrival Date:',
	'VM_ORDER_EMAIL_DEPARTURE_DATE' => 'Departure Date:',
	'VM_ORDER_EMAIL_NUMBER_PEOPLE' => 'Number of persons:',
	'VM_ORDER_EMAIL_BALANCE_OUTSTANDING' => 'Balance outstanding:',
	'VM_ORDER_EMAIL_CHECK_IN' => 'Check In:',
	'VM_ORDER_EMAIL_' => 'On or after 4pm',
	'VM_ORDER_EMAIL_CHECK_OUT' => 'Check Out:',
	'VM_ORDER_EMAIL_LATEST_DEPARTURE' => '10 am latest on date of departure',
	'VM_ORDER_EMAIL_KEY_PICKUP' => 'Key Pick Up:',
	'VM_ORDER_EMAIL_PLEASE_BRING_MAP' => 'Please print the map with directions to your property 	',
	'VM_ORDER_EMAIL_CLICK_FOR_MORE' => 'Click here for property locations map',

	//post-vacation-email
	/* 'VM_ORDER_EMAIL_WE_MISS_YOU' => '<p>We Miss You Guys<BR>
  <BR>
  Many thanks for choosing Anna Maria Vacations. We trust   you enjoyed your stay in %s, and that the whole process from   choosing to staying was easy and stress free. <BR>
  <BR>
  We welcome any feedback   you may have such as, how did you find the web site, was the booking process   easy, did the property come up to your expectations? We welcome constructive   criticisms but we love positive comments. <BR>
  <BR>
  If you recommend any friends   or family as a new renter we will pay you $100 towards your next vacation for   every full week\'s booking you obtain. Please email us with the details.<BR>
  <BR>
  In   any event, as a previous renter, you are entitled to $100 per week discount up   to a maximum of $200 after which our long term discount kicks in.<BR>
  <BR>
  Again   we thank you for choosing Anna Maria Vacations and look forward to welcoming you   back to our little bit of paradise. &nbsp;<BR>
  <BR>
  <BR>
  From all the staff at &nbsp;Anna   Maria Vacations<BR>
</p>', */

	'VM_ORDER_EMAIL_WE_MISS_YOU' => '<p>We Miss You Guys!<BR>
	<BR>
	We at Anna Maria Vacations really appreciate you choosing us as your rental property provider.  We trust you enjoyed your stay at %s and that your experience with us was a positive one. <BR>
	<BR>
	We are a luxury property provider and as such, we strive to provide a truly great experience for our guests.  We are always looking for ways to improve and we value any feedback you can give us pertaining to our web site, the booking process, the condition of the property or any other important information.<BR>
	<BR>
	We would be very grateful if you would enter in a review of the property on our website <a href="http://www.annamaria.com">www.annamaria.com</a>.  Go to the specific property you stayed at and click on the blue review tab.  You will have to enter your username and password for your account to write the review.   Also, please <a href="http://www.facebook.com/pages/Anna-Maria-Vacations/122505126188">click here</a> to like us on our Facebook page.<BR>
	<BR>
	Remember, as a returning guest you will receive our returning renter discount of $100 per 6 nights, up to a $200 total.  In addition, you can earn another $100 per week discount (max $200) for referring a friend that books with us.<BR>
	<BR>
	
	Again, we are very grateful for you choosing Anna Maria Vacations and we look forward to welcoming you back to the island again real soon.<BR>
	<BR>
	<BR>
	Warm Regards,<BR><BR>
	All the Anna Maria Vacations Staff<BR>
</p>',
	

	'VM_ORDER_EMAIL_PAYMENT_IS_DUE' => 'Payment is due for order:',
	'VM_ORDER_EMAIL_PLEASE_SEE_DETAILS' => '. Please see the details below.
If you have already made your payment, please disregard this email.',
	'VM_ORDER_EMAIL_PROPERTY' => 'Property:',
	//'VM_ORDER_EMAIL_ARRIVAL_DATE' => 'Arrival Date:',
	//'VM_ORDER_EMAIL_DEPARTURE_DATE' => 'Departure Date:',
	'VM_ORDER_EMAIL_NUMBER_OF_GUESTS' => 'Number of guests:',
	
	
	
	'VM_ORDER_EMAIL_NOTES_EMAIL' => '
	<p>Thank you for booking with Anna Maria Vacations.  Below is your reservation and deposit payment confirmation.  Any balance due will need to be paid within 90 days prior to your arrival.  All of your payment reminders will be emailed to the email address provided at the time of booking.  Please note there will be a $65 admin fee charged with your order. If paying by check please make your check payable to Anna Maria Vacations.</p>
	<p><strong>Mail your check to:</strong><br />
	Anna Maria Vacations<br />
	5702 Marina Dr. Unit 8 <br />
	Holmes Beach, Fl. 34217</p>
	<p>You can also pay the balance via a bank wire transfer but will need to contact our office for the necessary information to make this form of payment.</p>  
	<p>Check in time is 4:00 we will strictly adhere to this time.  We want to provide each of our guests with the best property available and to do this we need adequate time to clean the property.  Our cleaning staff greatly appreciates your understanding in this matter.  We are more than happy to give you recommendations for places to eat or shop.</p>
	<p>Upon your arrival you can go directly to your vacation property and enter the 4 digit key code provided at the time of booking.  The key box will be located beside the front door or close by and your key will be in the key box.  The code will not open the door you will need to retrieve the key from the box.</p> 
	<p>Our properties are supplied with all bath and bed linens.  Beach towels are not supplied but are often there, upon arrival if they are not supplied you can rent them from the company listed below or purchase them cheaply from several  stores on the island. The kitchens are fully stocked with your basic needs including but not limited to dishes, cups, glasses, pots and pans, utensils, coffee maker, blender and toaster.  Units are equipped with hair dryers.  The paper products provided to start you out will be one roll of paper towels and two rolls of toilet paper per bathroom.  You will be responsible to resupply as needed.  There are supermarkets and drug stores on the island.</p>
	<p>If you would like to rent bicycles, beach chairs, wagons, cribs, strollers or other supplies, please contact Fun and More Rentals.  Also, if you would like a starter pack of food and drinks they can supply  your property with these items before your arrival. Their toll free number is 866-931-9840 or view their website at www.funandmorerentals.com for a complete list of offerings.</p>
	<p>We do have some properties that allow pets but we have more that do not so be sure to check with us before bringing a pet.  Our full pet policy is located on the website.</p>
	<p>Please note all of our properties are non-smoking.  Also, please see Commonly Asked Questions published on the website that answers most questions guests have asked.</p>
	<p>Please adhere to the 10 a.m. check out time on the day of your departure.  Our cleaning staff have very tight schedules on these days.</p> 
	<p>If you need any assistance while on the island you may contact our property manager or maintenance manager at the below numbers.</p>
	<table cellspacing="0">
		<tr>
			<td>Property Manager:&nbsp;&nbsp;</td>
			<td>Chris Troyer&nbsp;&nbsp;</td>
			<td>(941) 932-6803</td>
		</tr>
	</table>
	<p>We thank you again for booking with Anna Maria Vacations and hope you enjoy your visit to beautiful Anna Maria Island.</p>',

	//payment_email
	'VM_ORDER_EMAIL_DEAR' => 'Dear ',
	'VM_ORDER_EMAIL_PAYMENT_DUE' => 'Payment is due for order:',
	'VM_ORDER_EMAIL_SEE_DETAILS' => 'Please see the details below',
	'VM_ORDER_EMAIL_IF_YOU_HAVE_PAID' => 'If you have already made your payment, please disregard this email.',
	//'VM_ORDER_EMAIL_PROPERTY' => 'Property',
	//'VM_ORDER_EMAIL_ARRIVAL_DATE' => 'Arrival Date',
	//'VM_ORDER_EMAIL_DEPARTURE_DATE' => 'Departure Date',
	//'VM_ORDER_EMAIL_NUMBER_GUESTS' => 'Number of guests',
	'VM_ORDER_EMAIL_ALMOST_DUE' => 'Amount due now:',
		'VM_ORDER_EMAIL_ADDRESS' => '',
	'VM_ORDER_EMAIL_OUSTANDING_BALANCE' => '

Please visit %s to pay your outstanding balances.
Please see your Personal Web Page %s for your reservation information.
This is a computer-generated payment Request.  As of todays date it appears we have not yet received payment for your upcoming vacation.
Note: for last minute reservations your full payment is now immediately due.
If we have not received your payment within the next 10 days the property will be made available on the Internet for re-renting.  However, please note that under our Terms & Conditions you are still be responsible for the full payment unless we can re-rent the property, in which case we will deduct any monies from your account and you will be responsible any shortfall.',

//payment_email.html.tpl.php
	'VM_ORDER_EMAIL_OUSTANDING_BALANCE_FORMATTED' => '<p>Please visit <a href="%s">your payment area</a> to pay your outstanding balances</p>
			<p>Please see your <a href="%s">Personal Web Page</a> for your reservation information.</p>
			<p>This is a computer-generated payment Request.  As of todays date it appears we have not yet received payment for your upcoming vacation.</p>
			<p>Note: for last minute reservations your full payment is now immediately due.</p>
			<p style="font-size: 90%">If we have not received your payment within the next 10 days the property will be made available on the Internet for re-renting.  However, please note that under our Terms & Conditions you are still be responsible for the full payment unless we can re-rent the property, in which case we will deduct any monies from your account and you will be responsible any shortfall. </p>',

			
//confirmation
	'VM_ORDER_EMAIL_YOUR_PERSONAL_WEB' => 'Your Personal Web Page Details',
	'VM_ORDER_EMAIL_YOUR_PERSONAL_WEB_LOGIN' => 'You can login to your personal web page with the username and password that have been emailed to you in a separate email',
	'VM_ORDER_EMAIL_BOOKING_CONFIRRM' => 'Thank you for booking with us, please follow the link below to view your current and previous bookings. You will find additional booking information including a map and you will be able to update your personal account information.',
	'VM_ORDER_EMAIL_YOUR_USERNAME' => 'Your username is',
	'VM_ORDER_EMAIL_PASSWORD_EMAILED' => ' and your password was emailed to you when you joined.',


//maintenance_email

    'VM_ORDER_EMAIL_DEAR_NAME' => 'Dear [NAME],',
	'VM_ORDER_EMAIL_WEEKLY_SCHEDULE' => 'Please find the weekly checkout schedule for [DATE_FROM] to [DATE_TO]',
	'VM_ORDER_EMAIL_PROPERTY_B' => 'Property',
	'VM_ORDER_EMAIL_CHECK_IN_OUT' => 'Check-out/Check-in',
	'VM_ORDER_EMAIL_THNAKS' => 'Thanks',
	
	
	
	
	
); $VM_LANG->initModule( 'order_email', $langvars );
?>
