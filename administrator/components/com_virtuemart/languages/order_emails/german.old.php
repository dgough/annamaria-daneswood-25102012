<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
/**
*
* @version $Id: english.php 1071 2007-12-03 08:42:28Z thepisu $
* @package VirtueMart
* @subpackage languages
* @copyright Copyright (C) 2004-2007 soeren - All rights reserved.
* @translator soeren
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
global $VM_LANG;
$langvars = array (
	'CHARSET' => 'ISO-8859-1',

	
	'VM_ORDER_EMAIL_GO_SITE' => 'Besuchen Sie die Website von Anna Maria',
	
	

	
	
	//resevation email
	'VM_ORDER_EMAIL_DEAR' => 'Sehr geehrte(r)',
	'VM_ORDER_EMAIL_CONFIRMATION' => 'BESTÄTIGUNG IHRER RESERVIERUNG VOR IHRER GEPLANTEN ANKUNFT - ES LIEGT IN IHRER VERANTWORTUNG, ALLE EINZELHEITEN IHRER RESERVIERUNG SORGFÄLTIG ZU ÜBERPRÜFEN. BITTE DRUCKEN SIE ALLE INFORMATIONEN AUF IHRER PERSÖNLICHEN WEBPAGE SOWIE DIESE E-MAIL UND DIE STANDORTKARTE AUS UND BRINGEN SIE DEN AUSDRUCK MIT',
	'VM_ORDER_EMAIL_ON_OR_AFTER' => 'Ab 16 Uhr',
	'VM_ORDER_EMAIL_GUEST_NAME' => 'Name des Gastes:',
	'VM_ORDER_EMAIL_GUEST_DETAILS' => 'persönliche Angaben des Gastes',
	'VM_ORDER_EMAIL_GUEST_NAME' => 'Name des Gastes:',
	'VM_ORDER_EMAIL_GUEST_ADDRESS' => 'Adresse des Gastes:',
	'VM_ORDER_EMAIL_GUEST_TELEPHONE' => 'Telefon-Nr. des Gastes:',
	'VM_ORDER_EMAIL_GUEST_EMAIL' => 'E-Mail-Adresse des Gastes:',
	'VM_ORDER_EMAIL_WEBSITE' => 'Ihre persönliche Webpage',
	'VM_ORDER_EMAIL_DETAILS' => 'Angaben zur Ferienwohnung/Villa',
	'VM_ORDER_EMAIL_PROPERTY_NAME' => 'Name der Ferienwohnung/Villa:',
	'VM_ORDER_EMAIL_PROPERTY_ADDRESS' => 'Adresse der Ferienwohnung/Villa:',
	'VM_ORDER_EMAIL_PROPERTY_PHONE' => 'Telefon-Nr. der Ferienwohnung/Villa:',
	'VM_ORDER_EMAIL_RESERVATION' => 'Einzelheiten der Reservierung',
	'VM_ORDER_EMAIL_ARRIVAL_DATE' => 'Datum der Ankunft:',
	'VM_ORDER_EMAIL_DEPARTURE_DATE' => 'Datum der Abreise:',
	'VM_ORDER_EMAIL_NUMBER_PEOPLE' => 'Anzahl der Personen:',
	'VM_ORDER_EMAIL_BALANCE_OUTSTANDING' => 'ausstehende Restzahlung:',
	'VM_ORDER_EMAIL_CHECK_IN' => 'Einchecken:',
	'VM_ORDER_EMAIL_' => 'Ab 16 Uhr',
	'VM_ORDER_EMAIL_CHECK_OUT' => 'Auschecken:',
	'VM_ORDER_EMAIL_LATEST_DEPARTURE' => 'spätestens 10 Uhr vormittags am Tag der Abreise',
	'VM_ORDER_EMAIL_KEY_PICKUP' => 'Schlüssel abholen:',
	'VM_ORDER_EMAIL_PLEASE_BRING_MAP' => 'Bitte drucken Sie die Standortkarte mit der Wegbeschreibung zu Ihrer Ferienwohnung/Villa aus',
	'VM_ORDER_EMAIL_CLICK_FOR_MORE' => 'Zu den Standortkarten der Ferienwohnungen/Villen hier anklicken',

	//post-vacation-email
	'VM_ORDER_EMAIL_WE_MISS_YOU' => '<p>Wir vermissen Euch<BR>
  <BR>
  Vielen Dank, dass Sie sich für Anna Maria Vacations entschieden haben. Wir hoffen, dass Sie einen schönen Aufenthalt in <?php echo $property->name ?> hatten und dass für Sie alles - von der Auswahl Ihres Feriendomizils bis zu Ihrem Aufenthalt - problemlos und stressfrei verlaufen ist. <BR>
  <BR>
  Wir würden uns über jede Stellungnahme von Ihnen freuen, zum Beispiel wie Ihnen unsere Website gefallen hat, ob Sie den Buchungsvorgang einfach fanden, oder ob die ausgewählte Ferienwohnung oder Villa Ihre Erwartungen erfüllt hat. Wir freuen uns über konstruktive Kritik, aber wir lieben auch positive Kommentare. <BR>
  <BR>
  Wenn Sie unsere Ferienvermietung an Freunde oder Familienmitglieder weiterempfehlen, schreiben wir Ihnen für Ihren nächsten Urlaub $100,- für jede volle Woche gut, die Sie bei uns buchen. Bitte informieren Sie uns per E-Mail über die Details.<BR>
  <BR>
  In jedem Fall steht Ihnen als ehemaliger Mieter ein Rabatt von $100,- pro Woche bis zu einem Höchstbetrag von $200,- zu. Danach setzt unser Rabatt für längere Aufenthalte ein.<BR>
  <BR>
  Nochmals vielen Dank, dass Sie sich für Anna Maria Vacations entschieden haben. Wir würden uns freuen, Sie wieder auf unserer kleinen Paradiesinsel begrüßen zu dürfen. &nbsp;<BR>
  <BR>
  <BR>
  Herzliche Grüße von allen Mitarbeitern bei &nbsp;Anna   Maria Vacations<BR>
</p>',
	

	'VM_ORDER_EMAIL_PAYMENT_IS_DUE' => 'Der Zahlungsauftrag ist fällig:',
	'VM_ORDER_EMAIL_PLEASE_SEE_DETAILS' => '. Bitte beachten Sie die nachfolgenden Details.
Wenn Sie die Zahlung schon geleistet haben, betrachten Sie diese E-Mail bitte als gegenstandslos.',
	'VM_ORDER_EMAIL_PROPERTY' => 'Ferienwohnung/Villa:',
	//'VM_ORDER_EMAIL_ARRIVAL_DATE' => 'Datum der Ankunft:',
	//'VM_ORDER_EMAIL_DEPARTURE_DATE' => 'Datum der Abreise:',
	'VM_ORDER_EMAIL_NUMBER_OF_GUESTS' => 'Anzahl der Gäste:',
	
	
	
	'VM_ORDER_EMAIL_NOTES_EMAIL' => '
	<p>
	Anmerkungen:<br /><br />
	Bitte lesen nochmals die Hinweise unter<a href=\'%s\'> Allgemeine Geschäftsbedingungen</a> auf unserer Website, z.B. über die Anzahl der Gäste, Haustiere, etc. <br />
	Beachten Sie auch die Seite „Häufig gestellte Fragen“ auf unserer Website. Hier wird praktisch jede Frage beantwortet, die jemals von Gästen über unsere Ferienwohnungen/Villen gestellt worden ist.<br /> 
	Für Fragen, die eine bestimmte Ferienwohnung oder Villa betreffen, gehen Sie bitte nach „Alle Ferienwohnungen und Villen auf einen Blick“ auf unserer <a href=\'%s\'>Home Page</a> und wählen Sie die allgemeinen Informationen über die von Ihnen reservierte Ferienwohnung oder Villa aus.<br />
	Wir stellen Badezimmerhandtücher und Bettwäsche für Sie bereit. Strandhandtücher werden von uns nicht gestellt.<br />
	Die vereinbarte Anzahl von Gästen darf nicht überschritten werden.<br />
	Benutzen Sie bitte nachfolgende Telefonnummern, um uns in Notfällen während Ihres Aufenthaltes anzurufen.<br /> 
	<br />
	Verwaltung: Büro 941-778-4178,  zwischen 8.00 und 17.00 Uhr <br /> 
	Instandhaltung/Reparaturen oder Haushaltsführung: 941-713-0284 <br />
	Pool & Wartungsarbeiten: 941-301-1183 <br />
	<br />
	<br />
	Genießen Sie Ihren Aufenthalt auf der schönen Insel Anna Maria an der Golfküste Floridas.<br /> 
	<br />   
	Reservierungsabteilung,<br />',

	//payment_email
	'VM_ORDER_EMAIL_DEAR' => 'Sehr geehrte(r)',
	'VM_ORDER_EMAIL_PAYMENT_DUE' => 'Der Zahlungsauftrag ist fällig:',
	'VM_ORDER_EMAIL_SEE_DETAILS' => 'Bitte beachten Sie die nachfolgenden Details',
	'VM_ORDER_EMAIL_IF_YOU_HAVE_PAID' => 'Wenn Sie die Zahlung schon geleistet haben, betrachten Sie diese E-Mail bitte als gegenstandslos.',
	//'VM_ORDER_EMAIL_PROPERTY' => 'Ferienwohnung oder Villa',
	//'VM_ORDER_EMAIL_ARRIVAL_DATE' => 'Datum der Ankunft',
	//'VM_ORDER_EMAIL_DEPARTURE_DATE' => 'Datum der Abreise',
	//'VM_ORDER_EMAIL_NUMBER_GUESTS' => 'Anzahl der Gäste',
	'VM_ORDER_EMAIL_ALMOST_DUE' => 'Jetzt fälliger Betrag:',
		'VM_ORDER_EMAIL_ADDRESS' => '',
	'VM_ORDER_EMAIL_OUSTANDING_BALANCE' => '

Bitte gehen Sie auf %s , um Ihre ausstehenden Zahlungen zu leisten.
Auf Ihrer persönlichen Webpage finden Sie alle Informationen zu Ihrer Reservierung.
Diese Zahlungsaufforderung wurde maschinell erstellt. 
Die Zahlung für Ihren bevorstehenden Urlaub ist bis zum heutigen Datum noch nicht bei uns eingegangen.
Bitte beachten Sie: Für Last-Minute-Reservierungen ist die Zahlung in voller Höhe nunmehr sofort fällig.
Sollte Ihre Zahlung nicht innerhalb der nächsten 10 Tage bei uns eingehen, wird die Ferienwohnung/Villa im Internet erneut zur Vermietung angeboten.
Beachten Sie jedoch, dass Sie gemäß unserer Allgemeinen Geschäftsbedingungen weiterhin zur Zahlung in voller Höhe verpflichtet sind, es sein denn, die Ferienwohnung/Villa kann erneut vermietet werden. In diesem Fall wird Ihr Konto mit jeglichen Fehlbeträgen belastet und Sie werden für eventuelle Mindereinnahmen haftbar gemacht.',

//payment_email.html.tpl.php
	'VM_ORDER_EMAIL_OUSTANDING_BALANCE_FORMATTED' => '<p>Bitte gehen Sie auf <a href="%s">das Eingabefeld für Ihre Zahlungen</a> um Ihre ausstehenden Zahlungen zu leisten</p>
			<p>Auf Ihrer<a href="%s">persönlichen Webpage</a> finden Sie alle Informationen zu Ihrer Reservierung.</p>
			<p> Diese Zahlungsaufforderung wurde maschinell erstellt. Die Zahlung für Ihren bevorstehenden Urlaub ist bis zum heutigen Datum noch nicht bei uns eingegangen.</p>
			<p>Bitte beachten Sie: Für Last-Minute-Reservierungen ist die Zahlung in voller Höhe sofort fällig.</p>
			<p style="font-size: 90%"> Sollte Ihre Zahlung nicht innerhalb der nächsten 10 Tage bei uns eingehen, wird die Ferienwohnung/Villa im Internet erneut zur Vermietung angeboten.
Beachten Sie jedoch, dass Sie gemäß unserer Allgemeinen Geschäftsbedingungen weiterhin zur Zahlung in voller Höhe verpflichtet sind, es sein denn, die Ferienwohnung/Villa kann erneut vermietet werden. In diesem Fall wird Ihr Konto mit jeglichen Fehlbeträgen belastet und Sie werden für eventuelle Mindereinnahmen haftbar gemacht.</p>',

			
//confirmation
	'VM_ORDER_EMAIL_YOUR_PERSONAL_WEB' => 'Die Details Ihrer persönlichen Webpabe',
	'VM_ORDER_EMAIL_BOOKING_CONFIRRM' => 'Vielen Dank für Ihre Buchung bei uns. Bitte folgen Sie dem nachfolgenden Link, um Ihre gegenwärtigen und früheren Buchungen zu überprüfen. Hier finden Sie auch weitere Informationen zu Ihrer Buchung, einschließlich einer Standortkarte und dem Schlüsselcode, mit dem Sie Zutritt zu Ihrer Ferienwohnung/Villa haben. Hier können Sie auch Ihre persönlichen Kontoinformationen auf den neusten Stand bringen.',
	'VM_ORDER_EMAIL_YOUR_USERNAME' => 'Ihr Benutzername ist',
	'VM_ORDER_EMAIL_PASSWORD_EMAILED' => 'Ihr Passwort wurde Ihnen bereits per E-Mail zugesandt.',


//maintenance_email

    'VM_ORDER_EMAIL_DEAR_NAME' => 'Sehr geehrte(r) [NAME],',
	'VM_ORDER_EMAIL_WEEKLY_SCHEDULE' => 'Anbei finden Sie unseren wöchentlichen Checkout-Plan für den Zeitraum von [DATE_FROM] bis [DATE_TO]',
	'VM_ORDER_EMAIL_PROPERTY_B' => 'Ferienwohnung/Villa',
	'VM_ORDER_EMAIL_CHECK_IN_OUT' => 'Check-out/Check-in',
	'VM_ORDER_EMAIL_THNAKS' => 'Vielen Dank',
	
	
	
	
	
); $VM_LANG->initModule( 'order_email', $langvars );
?>