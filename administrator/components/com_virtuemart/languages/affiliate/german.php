<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direkter Zugang zu '.basename(__FILE__).' ist nicht gestattet.' ); 
/**
*
* @version $Id: english.php 1071 2007-12-03 08:42:28Z thepisu $
* @package VirtueMart
* @subpackage languages
* @copyright Copyright (C) 2004-2007 soeren - All rights reserved.
* @translator soeren
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
global $VM_LANG;
$langvars = array (
	'CHARSET' => 'ISO-8859-1',
	'PHPSHOP_USER_FORM_EMAIL' => 'Mail',
	'PHPSHOP_SHOPPER_LIST_LBL' => 'Einkäufer-Liste',
	'PHPSHOP_SHOPPER_FORM_BILLTO_LBL' => 'Rechnungsinformation',
	'PHPSHOP_SHOPPER_FORM_USERNAME' => 'Nutzername',
	'PHPSHOP_AFFILIATE_MOD' => 'Partner-Verwaltung',
	'PHPSHOP_AFFILIATE_LIST_LBL' => 'Partner-Liste',
	'PHPSHOP_AFFILIATE_LIST_AFFILIATE_NAME' => 'Partner-Name',
	'PHPSHOP_AFFILIATE_LIST_AFFILIATE_ACTIVE' => 'Aktiv',
	'PHPSHOP_AFFILIATE_LIST_RATE' => 'Rate',
	'PHPSHOP_AFFILIATE_LIST_MONTH_TOTAL' => 'Monatstotal',
	'PHPSHOP_AFFILIATE_LIST_MONTH_COMMISSION' => 'Monatskommission',
	'PHPSHOP_AFFILIATE_LIST_ORDERS' => 'Bestellungsliste',
	'PHPSHOP_AFFILIATE_EMAIL_WHO' => 'Wen zu mailen (* = ALLE)',
	'PHPSHOP_AFFILIATE_EMAIL_CONTENT' => 'Ihre Mail-Adresse',
	'PHPSHOP_AFFILIATE_EMAIL_SUBJECT' => 'Das Thema',
	'PHPSHOP_AFFILIATE_EMAIL_STATS' => 'Aktuelle Statistiken einschließen',
	'PHPSHOP_AFFILIATE_FORM_RATE' => 'Kommissionsrate (Prozent)',
	'PHPSHOP_AFFILIATE_FORM_ACTIVE' => 'Aktiv?',
	'VM_AFFILIATE_SHOWINGDETAILS_FOR' => 'Details zeigen für',
	'VM_AFFILIATE_LISTORDERS' => 'Bestellungen listen',
	'VM_AFFILIATE_MONTH' => 'Monat',
	'VM_AFFILIATE_CHANGEVIEW' => 'Ansicht ändern',
	'VM_AFFILIATE_ORDERSUMMARY_LBL' => 'Bestellungsübersicht',
	'VM_AFFILIATE_ORDERLIST_ORDERREF' => 'Bestellungshinweis',
	'VM_AFFILIATE_ORDERLIST_DATEORDERED' => 'Bestelldatum',
	'VM_AFFILIATE_ORDERLIST_ORDERTOTAL' => 'Gesamtbestellung',
	'VM_AFFILIATE_ORDERLIST_COMMISSION' => 'Kommission (Rate)',
	'VM_AFFILIATE_ORDERLIST_ORDERSTATUS' => 'Bestellstatus'
); $VM_LANG->initModule( 'affiliate', $langvars );
?>