<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direkter Zugang zu '.basename(__FILE__).' ist nicht gestattet.' ); 
/**
*
* @package VirtueMart
* @subpackage languages
* @copyright Copyright © 2004-2008 soeren - All rights reserved.
* @translator soeren
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
global $VM_LANG;
$langvars = array (
	'CHARSET' => 'ISO-8859-1',
	'PHPSHOP_NO_CUSTOMER' => 'Sie sind nocht nicht ein registrierter Kunde. Bitte geben Sie Ihre Zahlungsinformationen.',
	'PHPSHOP_THANKYOU' => 'Danke f&#252;r Ihre Buchung.',
	'PHPSHOP_EMAIL_SENDTO' => 'Eine Best&#228;tigungsmail wurde gesendet an',
	'PHPSHOP_CHECKOUT_NEXT' => 'Weiter',
	'PHPSHOP_CHECKOUT_CONF_BILLINFO' => 'Rechnungsinformation',
	'PHPSHOP_CHECKOUT_CONF_COMPANY' => 'Gesellschaft',
	'PHPSHOP_CHECKOUT_CONF_NAME' => 'Name',
	'PHPSHOP_CHECKOUT_CONF_ADDRESS' => 'Adresse',
	'PHPSHOP_CHECKOUT_CONF_EMAIL' => 'Mail',
	'PHPSHOP_CHECKOUT_CONF_SHIPINFO' => 'Versandinformation',
	'PHPSHOP_CHECKOUT_CONF_SHIPINFO_COMPANY' => 'Gesellschaft',
	'PHPSHOP_CHECKOUT_CONF_SHIPINFO_NAME' => 'Name',
	'PHPSHOP_CHECKOUT_CONF_SHIPINFO_ADDRESS' => 'Adresse',
	'PHPSHOP_CHECKOUT_CONF_SHIPINFO_PHONE' => 'Telefon',
	'PHPSHOP_CHECKOUT_CONF_SHIPINFO_FAX' => 'Fax',
	'PHPSHOP_CHECKOUT_CONF_PAYINFO_METHOD' => 'Bezahlungsmethode',
	'PHPSHOP_CHECKOUT_CONF_PAYINFO_REQINFO' => 'benötigte Infomation wenn Kreditkartenzahlung gew&#228;hlt wird',
	'PHPSHOP_PAYPAL_THANKYOU' => 'Danke f&#252;r Ihre Bezahlung. 
        Die Transaktion war erfolgreich. Sie erhalten eine Best&#228;tigungsmail f&#252;r die Transaktion mit PayPal. 
        Sie können jetzt weiter oder einloggen bei <a href=http://www.paypal.com>www.paypal.com</a> um die Transaktionsdetails zu sehen.',
	'PHPSHOP_PAYPAL_ERROR' => 'Ein Fehler ist aufgetreten w&#228;hrend der Bearbeitung Ihrer Transaktion. Der Status Ihrer Bestellung konnte nicht aktualisiert werden.',
	'PHPSHOP_THANKYOU_SUCCESS' => 'Ihre Buchung war erfolgreich!',
	'VM_CHECKOUT_TITLE_TAG' => 'Booking: Step %s of %s',
	'VM_CHECKOUT_ORDERIDNOTSET' => 'Buchungs-ID nicht festgelegt oder leer!',
	'VM_CHECKOUT_FAILURE' => 'Failure',
	'VM_CHECKOUT_SUCCESS' => 'Erfolg',
	'VM_CHECKOUT_PAGE_GATEWAY_EXPLAIN_1' => 'Diese Seite befindet sich auf der Webshop Website.',
	'VM_CHECKOUT_PAGE_GATEWAY_EXPLAIN_2' => 'Das Gateway hat die Seite aud der Website ausgef&#252;hrt, und zeigt das Ergebnis SSL-verschl&#252;sselt.',
	
'VM_CHECKOUT_CCV_CODE' => 'Kreditkarten Best&#228;tigungscode',
	'VM_CHECKOUT_CCV_CODE_TIPTITLE' => 'Was ist der Kreditkarten Best&#228;tigungscode?',
	'VM_CHECKOUT_MD5_FAILED' => 'MD5 &#252;berpr&#252;fung fehlgeschlagen',
	'VM_CHECKOUT_ORDERNOTFOUND' => 'Buchung nicht gefunden',
	'VM_CHECKOUT_ECHEQUE' => 'F&#252;r eine elektronische Version eines Schecks, siehe Beispiel unten.',
	'VM_CHECKOUT_ECHEQUE_DETAILS' => 'Sie finden Ihre Bankdaten auf Ihrem Kontoauszug. Bitte geben Sie Ihre 9-stellige Bankleitzahl in das Feld oben ein.',
	
'VM_CHECKOUT_CREDITCARD' => 'Bitte geben Sie die Kreditkartennummer ohne Leerzeichen ein.',
	'PHPSHOP_EPAY_PAYMENT_CARDTYPE' => 'Die Zahlung ist erstellt von %s <img
src="/components/com_virtuemart/shop_image/ps_image/epay_images/%s"
border="0">',

	//checkout.itransact
	'VM_CHECKOUT_THANK_YOU' => 'Danke f&#252;r Ihre Zahlung, unser System hat angemeldet, daß die Zahlung erfolgreich war.',
	
); $VM_LANG->initModule( 'checkout', $langvars );
?>