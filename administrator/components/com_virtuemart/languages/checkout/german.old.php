<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
/**
*
* @package VirtueMart
* @subpackage languages
* @copyright Copyright (C) 2004-2008 soeren - All rights reserved.
* @translator soeren
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
global $VM_LANG;
$langvars = array (
	'CHARSET' => 'ISO-8859-1',
	'PHPSHOP_NO_CUSTOMER' => 'Sie sind noch nicht als Kunde registriert. Bitte geben Sie Ihre Rechnungsadresse an.',
	'PHPSHOP_THANKYOU' => 'Danke für Ihre Bestellung.',
	'PHPSHOP_EMAIL_SENDTO' => 'Eine Bestätigungs-E-Mail wurde gesendet an',
	'PHPSHOP_CHECKOUT_NEXT' => 'Weiter',
	'PHPSHOP_CHECKOUT_CONF_BILLINFO' => 'Rechnungsinformationen',
	'PHPSHOP_CHECKOUT_CONF_COMPANY' => 'Firma',
	'PHPSHOP_CHECKOUT_CONF_NAME' => 'Name',
	'PHPSHOP_CHECKOUT_CONF_ADDRESS' => 'Adresse',
	'PHPSHOP_CHECKOUT_CONF_EMAIL' => 'E-Mail',
	'PHPSHOP_CHECKOUT_CONF_SHIPINFO' => 'Lieferinformationen',
	'PHPSHOP_CHECKOUT_CONF_SHIPINFO_COMPANY' => 'Firma',
	'PHPSHOP_CHECKOUT_CONF_SHIPINFO_NAME' => 'Name',
	'PHPSHOP_CHECKOUT_CONF_SHIPINFO_ADDRESS' => 'Adresse',
	'PHPSHOP_CHECKOUT_CONF_SHIPINFO_PHONE' => 'Telefon',
	'PHPSHOP_CHECKOUT_CONF_SHIPINFO_FAX' => 'Fax',
	'PHPSHOP_CHECKOUT_CONF_PAYINFO_METHOD' => 'Bezahlung per',
	'PHPSHOP_CHECKOUT_CONF_PAYINFO_REQINFO' => 'nur nötig wenn Zahlung per Kreditkarte gewählt wird.',
	'PHPSHOP_PAYPAL_THANKYOU' => 'Vielen Dank für Ihre Zahlung. 
        Ihre Transaktion wurde abgeschlossen. Sie erhalten eine Bestätigungs-E-Mail von PayPal für die Transaktion. 
        Sie können nun fortfahren oder sich unter <a href=http://www.paypal.com>www.paypal.com</a> in Ihr Konto einloggen, um die Details der Transaktion zu sehen.',
	'PHPSHOP_PAYPAL_ERROR' => 'Achtung, bei der Transaktion ist möglicherweise ein Fehler aufgetreten. Der Status der Bestellung
        konnte nicht aktualisiert werden.',
	'PHPSHOP_THANKYOU_SUCCESS' => 'Ihre Buchung wurde erfolgreich ausgeführt!',
	'VM_CHECKOUT_TITLE_TAG' => 'Checkout: Schritt %s von %s',
	'VM_CHECKOUT_ORDERIDNOTSET' => 'Buchungs-ID nicht eingerichtet oder leer!',
	'VM_CHECKOUT_FAILURE' => 'Fehler',
	'VM_CHECKOUT_SUCCESS' => 'Erfolg',
	'VM_CHECKOUT_PAGE_GATEWAY_EXPLAIN_1' => 'Diese Seite befindet sich auf der Website des Webshops.',
	'VM_CHECKOUT_PAGE_GATEWAY_EXPLAIN_2' => 'Das Gateway führt die Seite auf der Website aus und zeigt das Ergebnis SSL Encrypted.',
	'VM_CHECKOUT_CCV_CODE' => 'Kreditkarten-Validierungscode',
	'VM_CHECKOUT_CCV_CODE_TIPTITLE' => 'Wie lautet der Kreditkarten-Validierungscode?',
	'VM_CHECKOUT_MD5_FAILED' => 'MD5 Check fehlgeschlagen',
	'VM_CHECKOUT_ORDERNOTFOUND' => 'Bestellung nicht gefunden',
	'PHPSHOP_EPAY_PAYMENT_CARDTYPE' => 'Die Zahlung wird erstellt von %s <img
src="/components/com_virtuemart/shop_image/ps_image/epay_images/%s"
border="0">'
); $VM_LANG->initModule( 'checkout', $langvars );
?>