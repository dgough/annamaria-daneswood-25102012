<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
/*
*
* @version $Id: english.php 1.0 2008-06-06 12:47:00 GMT Organic Development $
* @package VirtueMart
* @subpackage languages
* @copyright Copyright (C) 2004-2007 soeren - All rights reserved.
* @translator Organic Development
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
global $VM_LANG;
$langvars = array ( 
	
	'VM_BK_EMAIL_PROPERTY' => 'Ferienwohnung/Villa',
	'VM_BK_EMAIL_ARRIVE' => 'Datum der Ankunft',
	'VM_BK_EMAIL_DEPART' => 'Datum der Abreise',
	'VM_BK_EMAIL_NIGHTS' => 'Gesamtzahl der Übernachtungen',
	'VM_BK_EMAIL_SUBTOTAL' => 'Zwischensumme',
	'VM_BK_EMAIL_DISCOUNT' => 'Rabatt',
	'VM_BK_EMAIL_TOTAL' => 'Gesamtsumme',
	'VM_BK_EMAIL_YOUR_BOOKING' => 'Ihre Buchung',
	
	'VM_BOOKING_HEADING' => 'Buchungsbericht',
	'VM_BOOKING_INTRO' => '<p>Vielen Dank für Ihr Interesse an einer Buchung bei uns. Wir führen Sie nun durch den Buchungsvorgang.
</p><p>Bitte folgen Sie auf dem Bildschirm dem Vorgang in drei Schritten, um die Details der von Ihnen gewünschten Buchung zu bestätigen.</p>',
	'VM_BOOKING_LEGEND1' => 'Ihr provisorischer Buchungsbericht',
	'VM_BOOKING_LEGEND2' => 'Ihre Buchung ändern / aktualisieren',
	'VM_BOOKING_LEGEND3' => 'Extras',
	'VM_BOOKING_LEGEND4' => 'Ihr abschließender Buchungsbericht',
	
	'VM_BOOKING_EXTRASTEXT' => 'Extras sind bei Abschluss der grundlegenden Buchungsinformationen auswählbar',
	
	'VM_BOOKING_TIP1' => 'Bitte überprüfen Sie die Angaben in Ihrem provisorischen Buchungsbericht. Bitte beachten Sie: Die Mietkosten sind ohne Steuern.',
	'VM_BOOKING_TIP2' => 'Nutzen Sie diesen Abschnitt, um die Termine Ihrer Buchung zu ändern oder zusätzliche, von Ihnen gewünschte Optionen hinzuzufügen, einschließlich zusätzliche Personen. Klicken Sie auf UPDATE um Ihren provisorischen Buchungsbericht auf den neusten Stand zu bringen.',
	'VM_BOOKING_TIP3' => 'Bitte wählen Sie Extras aus, die Sie Ihrer Buchung hinzufügen wollen.',
	'VM_BOOKING_TIP4' => 'Dieser Abschnitt zeigt den endgültigen Gesamtpreis Ihrer Buchung einschließlich aufgeschlüsselter Kosten, zur Anwendung kommender Rabatte und Ihrer Gesamtkosten einschließlich Steuern. Klicken Sie auf JETZT BUCHEN um Ihre Buchung zu bestätigen und fortzusetzen. ',
	
	'VM_BOOKING_UPDATE' => ' Aktualisieren ',
	'VM_BOOKING_BOOKNOW' => ' Jetzt buchen ',
	
	//VM_BOOKING_TOATAL
	'VM_BOOKING_FULL_PRICE' => 'voller Preis',
	'VM_BOOKING_SEASON_DISCOUNT' => 'Rabatt Nebensaison',
	'VM_BOOKING_TOTAL' => 'Gesamtrabatt',
	'VM_BOOKING_EXTRAS' => 'Extras',
	'VM_BOOKING_COST_OF_RETAIL' => 'Mietkosten',
	'VM_BOOKING_TAX_AT' => 'Steuern (bei',
	'VM_BOOKING_DAMAGE_PROTECTION' => 'Schutz vor Beschädigungen',

	//quick_price_toatal.tpl.php
	'VM_BOOKING_WAS' => 'war:',
	'VM_BOOKING_NOW' => 'jetzt:',
	'VM_BOOKING_SAVE' => 'Sie sparen:',
	'VM_BOOKING_DATE_FROM' => 'Datum ab',
	'VM_BOOKING_DATE_FROM' => 'Datum bis',
	'VM_BOOKING_PEOPLE' => 'Personen',
	'VM_BOOKING_PETS' => 'Haustiere?',
	'VM_BOOKING_OWNER' => 'Besitzer (freie) Buchung?',
	'VM_BOOKING_YES' => 'Ja',
	'VM_BOOKING_NO' => 'Nein',
	
	//form.original.tpl.php
	'VM_BOOKING_DATE_FROM' => 'Datum von',
	'VM_BOOKING_DATE_TO' => 'Datum bis',
	'VM_BOOKING_ARRIVAL_AIRPORT' => 'Ankunft Flughafen',
	'VM_BOOKING_DEPT_AIRPORT' => 'Abreise Flughafen',
	'VM_BOOKING_ARRIVAL_FLIGHT' => 'Ankunft Flug',
	'VM_BOOKING_DEPT_FLIGHT' => 'Abreise Flug',
	
	'VM_BOOKING_PEOPLE' => 'Personen',
	'VM_BOOKING_OWNER' => 'Besitzer (freie)<br /> Buchung?',
	
	//wrapper.tpl.php
	'VM_BOOKING_PAYMENTS' => 'Zahlungen',
	
	
	'VM_BOOKING_TERMS' => '<span style="color: red">Payments</span><br />	
		Zum Zeitpunkt Ihrer Reservierung wird Ihre Kreditkarte mit einer nicht zurückzahlbaren Anzahlung von $250,- belastet. (Dieser Betrag ist Teil Ihrer Gesamtkosten). Weitere Zahlungen können per eCheck (bevorzugt), wobei eine Gebühr von {echeque} anfällt, oder Kreditkarte (Gebühr {creditcard}) geleistet werden.  25% der Gesamtsumme sind zahlbar innerhalb von 7 Tagen ab Buchung.
		Der Rest der Gesamtsumme muss 90 Tage vor Ihrer Ankunft eingehen. Alle Zahlen sind an Anna Maria Vacations zu entrichten.
		Der Kurs des US Dollar hat Vorrang. Wechselkursschwankungen werden berechnet oder gutgeschrieben. 
		<br />
		<span style="color: red">Damage Protection</span><br />	
		Für jede Reservierung ist eine Sicherheitsleistung/ Schadensschutzversicherung in Höhe von 38$ abzuschließen, die Unfallschäden bis zu $3 000,- abdeckt. Bitte beachten Sie, dass diese Versicherung keine fahrlässig verursachten Schäden oder extra Reinigungskosten abdeckt, die anfallen, wenn die Ferienwohnung oder Villa in verunreinigtem Zustand hinterlassen wird.
		<br />
		<span style="color: red">Cancelations</span><br />
		Rückerstattungen erfolgen unter dem Vorbehalt, dass Anna Maria Vacations in der Lage ist, die Unterkunft erneut zu vermieten. 
		Es ist ratsam, eine Reiseversicherung abzuschließen; siehe: <a href="http://www.vacationrentalinsurance.com" target="_blank">Vacation Rental Insurance</a>',
	'VM_BOOKING_PLAESE_NOTE' => '! Bitte beachten Sie, dass Ihre Buchung nicht ausgeführt wird, bevor Sie Ihre Anfangszahlung von $250,- geleistet und Ihre Bestätigungs-E-mail erhalten haben.',

	//Offers from back end
	'VM_BOOKING_OFFERS_CLEANING_FEE' => 'Reinigungsgebühr',
	'VM_BOOKING_OFFERS_MINIMUM' => 'Mindest-Zuschlagsgebühr auf die Buchung',
	'VM_BOOKING_OFFERS_SHORT_BREAKS' => 'Kurzaufenthalte',
	//'VM_BOOKING_OFFERS_GAP_FILLER' => 'Terminlücken-Rabatt',
	'VM_BOOKING_OFFERS_GAP_FILLER' => 'Sonderangebote/freie Termine',
	'VM_BOOKING_OFFERS_LONG_STAY' => 'Längere Aufenthalte',
	'VM_BOOKING_OFFERS_ONLINE_DISCOUNT' => 'Online-Rabatt',
	'VM_BOOKING_OFFERS_LONG_STAY' => 'Rabatt für längere Aufenthalte',
	'VM_BOOKING_OFFERS_RETURNING_CUST' => 'Stammkunden-Rabatt',
	
	'VM_BOOKING_FULL_GAP' => 'Gebucht werden muss der gesamte Zwischenraum zwischen Buchungen'
	
);
$VM_LANG->initModule( 'booking', $langvars );
?>