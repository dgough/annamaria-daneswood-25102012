<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
/*
*
* @version $Id: english.php 1.0 2008-06-06 12:47:00 GMT Organic Development $
* @package VirtueMart
* @subpackage languages
* @copyright Copyright (C) 2004-2007 soeren - All rights reserved.
* @translator Organic Development
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
global $VM_LANG;
$langvars = array ( 
	
	'VM_BK_EMAIL_PROPERTY' => 'Property',
	'VM_BK_EMAIL_ARRIVE' => 'Arrival Date',
	'VM_BK_EMAIL_DEPART' => 'Departure Date',
	'VM_BK_EMAIL_NIGHTS' => 'Total Nights',
	'VM_BK_EMAIL_SUBTOTAL' => 'Subtotal',
	'VM_BK_EMAIL_DISCOUNT' => 'Discount',
	'VM_BK_EMAIL_TOTAL' => 'Total',
	'VM_BK_EMAIL_YOUR_BOOKING' => 'Your Booking',
	
	'VM_BOOKING_HEADING' => 'Booking Summary',
	'VM_BOOKING_INTRO' => '<p>Thank you for your interest in booking with us. We will now take you through the booking process. </p><p>Please follow the three step process on this screen to confirm the details of your desired booking. </p>',
	'VM_BOOKING_LEGEND1' => 'Your Provisional booking summary',
	'VM_BOOKING_LEGEND2' => 'Amend / Update your booking',
	'VM_BOOKING_LEGEND3' => 'Extras',
	'VM_BOOKING_LEGEND4' => 'Your Final Booking summary',
	
	'VM_BOOKING_EXTRASTEXT' => 'Extras will be selectable upon completion of the primary booking information',
	
	'VM_BOOKING_TIP1' => 'Please check your provisional booking summary details are correct. Note: the cost of rental is excluding tax. ',
	'VM_BOOKING_TIP2' => 'Use this section to change the dates of your booking or add any additional options you may require including additional persons. Clicking UPDATE will update your provisional booking summary.',
	'VM_BOOKING_TIP3' => 'Please select any extras that you wish to add to your booking.',
	'VM_BOOKING_TIP4' => 'This section shows the final total price of your booking including itemised costs, discounts applied and your total cost including taxes. Click BOOK NOW to confirm and continue your booking. ',
	
	'VM_BOOKING_UPDATE' => ' Update ',
	'VM_BOOKING_BOOKNOW' => ' Book Now ',
	
	//VM_BOOKING_TOATAL
	'VM_BOOKING_FULL_PRICE' => 'Full Price',
	'VM_BOOKING_SEASON_DISCOUNT' => 'Off Season Discount',
	'VM_BOOKING_TOTAL' => 'Total Discount',
	'VM_BOOKING_EXTRAS' => 'Extras',
	'VM_BOOKING_COST_OF_RETAIL' => 'Cost of Rental',
	'VM_BOOKING_TAX_AT' => 'Tax (at',
	'VM_BOOKING_DAMAGE_PROTECTION' => 'Damage Protection',

	//quick_price_toatal.tpl.php
	'VM_BOOKING_WAS' => 'Was:',
	'VM_BOOKING_NOW' => 'Now:',
	'VM_BOOKING_SAVE' => 'You save:',
	'VM_BOOKING_DATE_FROM' => 'Date from',
	'VM_BOOKING_DATE_FROM' => 'Date to',
	'VM_BOOKING_PEOPLE' => 'People',
	'VM_BOOKING_PETS' => 'Pets?',
	'VM_BOOKING_OWNER' => 'Owner (free) booking?',
	'VM_BOOKING_YES' => 'Yes',
	'VM_BOOKING_NO' => 'No',
	
	//form.original.tpl.php
	'VM_BOOKING_DATE_FROM' => 'Date from',
	'VM_BOOKING_DATE_TO' => 'Date to',
	'VM_BOOKING_ARRIVAL_AIRPORT' => 'Arrival airport',
	'VM_BOOKING_DEPT_AIRPORT' => 'Dept airport',
	'VM_BOOKING_ARRIVAL_FLIGHT' => 'Arrival flight',
	'VM_BOOKING_DEPT_FLIGHT' => 'Dept flight',
	
	'VM_BOOKING_PEOPLE' => 'people',
	'VM_BOOKING_OWNER' => 'Owner (free)<br /> booking?',
	
	//wrapper.tpl.php
	'VM_BOOKING_PAYMENTS' => 'Payments',
	
	
	//Remoced Credit Card (fee {creditcard}).
	'VM_BOOKING_TERMS' => '<span style="color: red">Payments</span><br />	
		An initial $250 non refundable deposit is charged to your credit card at the time 
		of your reservation (this is part of your total cost). Any remaining balances can 
		be paid by credit card, E-check or personal check (as long as outside of 10 days 
		of stay for personal check). 25% is payable within 7 days of making the booking.
		Final Balance due must be received by 90 days prior to arrival. 
		All funds should made payable to Anna Maria Vacations.
		The US dollar rate prevails. Exchange rate differences will be debited or credited 
		<br />
		<span style="color: red">Damage Protection</span><br />	
		All reservations incur a $48 security deposit damage protection insurance, this 
		covers up to $3,000 of accidental damage. Please note this does not cover 
		careless damage or extra cleaning if the property is left in a dirty condition
		<br />
		<span style="color: red">Cancelations</span><br />
		Refunds are subject to Anna Maria Vacation\'s ability to re-rent the accommodation. 
		It is advisable to take out travel insurance see : <a href="http://www.vacationrentalinsurance.com" target="_blank">Vacation Rental Insurance</a>',

	//Offers from back end
	'VM_BOOKING_OFFERS_CLEANING_FEE' => 'Cleaning Fee',
	'VM_BOOKING_OFFERS_MINIMUM' => 'Minimum Booking Surcharge',
	'VM_BOOKING_OFFERS_SHORT_BREAKS' => 'Short Breaks',
	'VM_BOOKING_OFFERS_GAP_FILLER' => 'Gap Filler Discount',
	'VM_BOOKING_OFFERS_LONG_STAY' => 'Long Stay Discount',
	'VM_BOOKING_OFFERS_ONLINE_DISCOUNT' => 'Online Discount',
	'VM_BOOKING_OFFERS_LONG_STAY' => 'Long Stay Discount',
	'VM_BOOKING_OFFERS_RETURNING_CUST' => 'Returning Customers Discount',
	
	'VM_BOOKING_FULL_GAP' => 'You must book the entire gap between bookings',
	
	// Organic mod
	'BEDROOM' => 'Bedroom',
	'BEDROOMS' => 'Bedrooms'
	
);
$VM_LANG->initModule( 'booking', $langvars );
?>