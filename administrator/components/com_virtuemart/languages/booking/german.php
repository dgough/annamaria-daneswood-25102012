<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direkter Zugang zu '.basename(__FILE__).' ist nicht gestattet.' ); 
/*
*
* @version $Id: english.php 1.0 2008-06-06 12:47:00 GMT Organic Development $
* @package VirtueMart
* @subpackage languages
* @copyright Copyright © 2004-2007 soeren - All rights reserved.
* @translator Organic Development
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
global $VM_LANG;
$langvars = array ( 
	
	'VM_BK_EMAIL_PROPERTY' => 'Immobilie',
	'VM_BK_EMAIL_ARRIVE' => 'Ankunftsdatum',
	'VM_BK_EMAIL_DEPART' => 'Abfahrtsdatum',
	'VM_BK_EMAIL_NIGHTS' => 'Gesamtanzahl N&#196;chte',
	'VM_BK_EMAIL_SUBTOTAL' => 'Subtotal',
	'VM_BK_EMAIL_DISCOUNT' => 'Rabatt',
	'VM_BK_EMAIL_TOTAL' => 'Total',
	'VM_BK_EMAIL_YOUR_BOOKING' => 'Ihre Buchung',
	
	'VM_BOOKING_HEADING' => 'Buchungs&#252;bersicht',
	'VM_BOOKING_INTRO' => '<p>Danke da&#223; Sie mit uns buchen wollen. Wir f&#252;hren Sie jetzt durch den Buchungsproze&#223;. </p><p>Bitte folgen Sie den dreistufigen Proze&#223; auf dem Bildschirm um die Details Ihrer gew&#252;nschten Buchung zu best&#196;tigen. </p>',
	'VM_BOOKING_LEGEND1' => 'Ihre vorl&#196;ufige Buchungs&#252;bersicht',
	'VM_BOOKING_LEGEND2' => '&#196;nderung / Aktualisierung Ihrer Buchung',
	'VM_BOOKING_LEGEND3' => 'Extras',
	'VM_BOOKING_LEGEND4' => 'Ihre finale Buchungs&#252;bersicht',
	
	'VM_BOOKING_EXTRASTEXT' => 'Extras werden nach Abschlu&#223; der prim&#196;ren Buchungsinformationen ausw&#196;hlbar sein',
	
	'VM_BOOKING_TIP1' => 'Bitte &#252;berpr&#252;fen Sie Ihre vorl&#196;ufige Buchungs&#252;bersicht. Hinweis: der Mietpreis ist exklusiv Mehrwertsteuer. ',
	'VM_BOOKING_TIP2' => 'In dieser Sektion k&#214;nnen Sie das Buchungsdatum &#196;ndern oder zus&#196;tzliche Optionen hinzuf&#252;gen, einschlie&#223;lich zus&#196;tzlicher Personen. UPDATE klicken um Ihre vorl&#196;ufige Buchungs&#252;bersicht zu aktualisieren.',
	'VM_BOOKING_TIP3' => 'Bitte w&#196;hlen Sie alle Extras, die Sie zu Ihrer Buchung hinzuf&#252;gen m&#214;chten.',
	'VM_BOOKING_TIP4' => 'Diese Sektion zeigt den endg&#252;ltigen Gesamtpreis Ihrer Buchung, einschlie&#223;lich detaillierte Kosten, zutreffenden Rabatt und Ihre Gesamtkosten inklusive Steuern. Auf JETZT BUCHEN klicken um Ihre Buchung zu best&#196;tigen und fortzusetzen. ',
	
	'VM_BOOKING_UPDATE' => ' Update ',
	'VM_BOOKING_BOOKNOW' => ' Jetzt buchen ',
	
	//VM_BOOKING_TOATAL
	'VM_BOOKING_FULL_PRICE' => 'Vollpreis',
	'VM_BOOKING_SEASON_DISCOUNT' => 'Nebensaison Rabatt',
	'VM_BOOKING_TOTAL' => 'Gesamt Rabatt',
	'VM_BOOKING_EXTRAS' => 'Extras',
	'VM_BOOKING_COST_OF_RETAIL' => 'Mietkosten',
	'VM_BOOKING_TAX_AT' => 'Mehrwertsteuern (bei',
	'VM_BOOKING_DAMAGE_PROTECTION' => 'Besch&#196;digung&#223;chutz',

	//quick_price_toatal.tpl.php
	'VM_BOOKING_WAS' => 'War:',
	'VM_BOOKING_NOW' => 'Jetzt:',
	'VM_BOOKING_SAVE' => 'Sie sparen:',
	'VM_BOOKING_DATE_FROM' => 'Datum ab',
	'VM_BOOKING_DATE_FROM' => 'Datum bis',
	'VM_BOOKING_PEOPLE' => 'Personen',
	'VM_BOOKING_PETS' => 'Haustiere?',
	'VM_BOOKING_OWNER' => 'Besitzer (kostenlose) Buchung?',
	'VM_BOOKING_YES' => 'Ja',
	'VM_BOOKING_NO' => 'Nein',
	
	//form.original.tpl.php
	'VM_BOOKING_DATE_FROM' => 'Datum ab',
	'VM_BOOKING_DATE_TO' => 'Datum bis',
	'VM_BOOKING_ARRIVAL_AIRPORT' => 'Ankunft Flughafen',
	'VM_BOOKING_DEPT_AIRPORT' => 'Abflug Flughafen',
	'VM_BOOKING_ARRIVAL_FLIGHT' => 'Ankunft Flug',
	'VM_BOOKING_DEPT_FLIGHT' => 'R&#252;ckflug',
	
	'VM_BOOKING_PEOPLE' => 'Personen',
	'VM_BOOKING_OWNER' => 'Besitzer (kostenlose)<br /> Buchung?',
	
	//wrapper.tpl.php
	'VM_BOOKING_PAYMENTS' => 'Zahlungen',
	
	
	'VM_BOOKING_TERMS' => '<span style="color: red">Payments</span><br />	
		Eine nicht r&#252;ckzahlbare Anzahlung von $250 wird Ihre Kreditkarte zum Zeitpunkt der Reservierung berechnet (dies ist Teil Ihrer Gesamtkosten). 
		Au&#223;tehende Zahlungen k&#214;nnen per E-Scheck gemacht werden (bevorzugt) verursacht eine Geb&#252;hr in H&#214;he von {echeque}. Kreditkarte (Geb&#252;hr {creditcard}).  25% ist f&#196;llig innerhalb 7 Tagen nach der Buchung.
		Restbetrag mu&#223; sp&#196;testens 90 Tage vor der Anreise empfangen werden.
		Alle Gelder sind zahlbar an Anna Maria Vacations.
		Der US-Dollar-Kurs &#252;berwiegt. Wechselkursdifferenzen werden belastet bzw. gutgeschrieben.
		<br />
		<span style="color: red">Besch&#196;digung&#223;chutz</span><br />	
		Alle Buchungen entstehen eine $38 Kaution Schaden-Versicherung. Dies 
		deckt bis zu $3000 Unfallschaden. Bitte beachten: dies umfa&#223;t nicht
		nachl&#196;&#223;igen Schaden oder zus&#196;tzliche Reinigung wenn die Immobilie in einem schmutzigen Zustand verla&#223;en wird.
		<br />
		<span style="color: red">Annullierungen</span><br />
		R&#252;ckerstattungen unterliegen Anna Maria Vacations F&#196;higkeit die Immobilie neu zu vermieten. 
		Es ist ratsam, Reiseversicherung abzuschlie&#223;en. Siehe: <a href="http://www.vacationrentalinsurance.com" target="_blank">Vacation Rental Insurance</a>',
	'VM_BOOKING_PLAESE_NOTE' => '! Bitte beachten: Ihre Buchung wird nicht eingereicht bis Sie die erste $250 Kaution bezahlt und Ihre Best&#196;tigungsmail erhalten haben.',

	//Offers from back end
	'VM_BOOKING_OFFERS_CLEANING_FEE' => 'Reinigungsgeb&#252;hr',
	'VM_BOOKING_OFFERS_MINIMUM' => 'Minimaler Buchungszuschlag',
	'VM_BOOKING_OFFERS_SHORT_BREAKS' => 'Kurzurlaub',
	'VM_BOOKING_OFFERS_GAP_FILLER' => 'L&#252;ckenf&#252;ller Rabatt',
	'VM_BOOKING_OFFERS_LONG_STAY' => 'Langzeitaufenthalt Rabatt',
	'VM_BOOKING_OFFERS_ONLINE_DISCOUNT' => 'Online Rabatt',
	'VM_BOOKING_OFFERS_LONG_STAY' => 'Langzeitaufenthalt Rabatt',
	'VM_BOOKING_OFFERS_RETURNING_CUST' => 'Wiederkehrender Kunden Rabatt',
	
	'VM_BOOKING_FULL_GAP' => 'You must book the entire gap between bookings',
	
	// Organic mod
	'BEDROOM' => 'Schlafzimmer',
	'BEDROOMS' => 'Schlafzimmer'
	
);
$VM_LANG->initModule( 'booking', $langvars );
?>