<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
/**
*
* @version $Id: english.php 1071 2007-12-03 08:42:28Z thepisu $
* @package VirtueMart
* @subpackage languages
* @copyright Copyright (C) 2004-2007 soeren - All rights reserved.
* @translator soeren
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
global $VM_LANG;
$langvars = array (
	'PHPSHOP_THANKYOU' => 'Thank you for choosing to pay your stage payment',
	'PHPSHOP_THANKYOU_SUCCESS' => 'There may now be some additional steps below depending upon the payment method you selected.',
	'PHPSHOP_EMAIL_SENDTO' => 'A confirmation email has been sent to',
	
	'CHARSET' => 'ISO-8859-1',
	'PHPSHOP_ACC_CUSTOMER_ACCOUNT' => 'Customer Account:',
	'PHPSHOP_ACC_UPD_BILL' => 'Here you can update your billing information.',
	'PHPSHOP_ACC_UPD_SHIP' => 'Here you can add and maintain shipping addresses.',
	'PHPSHOP_ACC_ACCOUNT_INFO' => 'Account Information',
	'PHPSHOP_ACC_SHIP_INFO' => 'Shipping Information',
	'PHPSHOP_DOWNLOADS_CLICK' => 'Click on Product Name to Download File(s).',
	'PHPSHOP_DOWNLOADS_EXPIRED' => 'You have already downloaded the file(s) the maximum number of times, or the download period has expired.',
	
	//account.index.tpl
	'VM_ACCOUNT_NO_PERMISSION' => 'You appear not to have placed any bookings before. Account information will be available once bookings are placed.',
	'VM_ACCOUNT_FOOR_INFO' => 'For your information:',
	//'VM_ACCOUNT_TIP_WELCOME' => 'Welcome to your personal booking page. Here you can review your bookings, make payments and edit you personal account information.  Please choose the appropriate option from below.',
	'VM_ACCOUNT_TIP_ACCOUNT' => 'Please click on the Account information link above to view and edit your personal account information, including name, email, password and contact details.',
	'VM_ACCOUNT_TIP_BOOKINGS' => 'To view your booking including payment details, directions and key access click above or on the links below',
    //'PHPSHOP_ORDER_PRINT_PO_LBL'
	
	//account.orderz
	'VM_ACCOUNT_BOOKING_SUMMARY' => 'Here is your booking summary. To pay your outstanding balance, please click on the Pay Now button below the invoice. Ensure that you print out a copy of your booking details for your trip.',
	'VM_ACCOUNT_DEPOSIT_INFO' => '<b>Please note:</b>&nbsp;The initial $250 deposit must be paid via credit card or e-check to secure your booking. Payment by any other method will be declined and your booking will <b>NOT </b>be confirmed.<br />Credit card payments incur a fee of {creditcard}, e-check incurs a fee of {echeque} per check',	
	'VM_ACCOUNT_PAYMENTS_CLEARED' => 'Thank you, all payments have been submitted and cleared',
	

	//ps_payments
	'VM_ACCOUNT_PLEASE_NOTE' => '<strong>Please note:</strong> a payment of %s is due',
	
	
	//srgsre
	
	'VM_ACCOUNT_TIP_WELCOME' => 'Welcome to your personal booking page. Here you can review your bookings, make payments and edit you personal account information. Please choose the appropriate option from below.',
	
	//echo sprintf($VM_LANG->_('VM_ORDER_EMAIL_NOTES_EMAIL'), echo $terms_url, echo URL );
	// echo $VM_LANG->_('VM_ACCOUNT_BOOKINGS_LISTED') 
	
	'VM_ACCOUNT_PREFERRED_PAYMENT' => 'Please select your preferred payment method',
	'VM_ACCOUNT_PAY_NOW' => 'Pay now',
	'VM_ACCOUNT_RECIVED_PAYMENTS' => 'Below is a list of the payments we have received for your booking and the remaining payments still to pay.',
	'VM_ACCOUNT_SELECT_STAGE' => 'You must select at least 1 payment stage',
	'VM_ACCOUNT_NOT_UPDATE' => 'Could not update the selected payment stage. Please ensure you have entered the correct details.',
	'VM_ACCOUNT_PAYMENT_CODE' => 'Beginning to parse the payment extra info code...',
	'VM_ACCOUNT_CODE_ERROR' => 'Error: The code of the payment method',
	'VM_ACCOUNT_PARSE_ERROR' => 'contains a Parse Error!<br />Please correct that first',
	'VM_ACCOUNT_CHECK_DETAILS' => 'Please click on the Account information link above to view and edit your personal account information, including name, email, password and contact details.',
	'VM_ACCOUNT_BOOKINGS_LISTED' => 'To view your booking including payment details, directions and key access click above or on the links below',
	
	//account.billing
	
	'VM_ACCOUNT_CHECK_INFO' => 'Use this page to  edit your personal account information, including name, email, password and billing details. To save your information click the black save button. You must click this to save your changes. To return to the menu without making any changes, please click the green back button.',
	
	); $VM_LANG->initModule( 'account', $langvars );
?>