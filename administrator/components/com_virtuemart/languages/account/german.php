<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direkter Zugang zu'.basename(__FILE__).' ist nicht erlaubt.' ); 
/**
*
* @version $Id: english.php 1071 2007-12-03 08:42:28Z thepisu $
* @package VirtueMart
* @subpackage languages
* @copyright Copyright (C) 2004-2007 soeren - All rights reserved.
* @translator soeren
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
global $VM_LANG;
$langvars = array (
	'PHPSHOP_THANKYOU' => 'Vielen Dank daß Sie diese Phase zahlen',
	'PHPSHOP_THANKYOU_SUCCESS' => 'Möglicherweise gibt es jetzt einige weitere Schritte unten, abhängig von Ihrer Zahlungsmethode.',
	'PHPSHOP_EMAIL_SENDTO' => 'Eine Bestätigungsmail wurde gesendet',
	
	'CHARSET' => 'ISO-8859-1',
	'PHPSHOP_ACC_CUSTOMER_ACCOUNT' => 'Kundenkonto:',
	'PHPSHOP_ACC_UPD_BILL' => 'Zahlungsinformationen aktualisieren.',
	'PHPSHOP_ACC_UPD_SHIP' => 'Lieferadressen hinzuzufügen und erhalten.',
	'PHPSHOP_ACC_ACCOUNT_INFO' => 'Kontoinformation',
	'PHPSHOP_ACC_SHIP_INFO' => 'Lieferungsinformation',
	'PHPSHOP_DOWNLOADS_CLICK' => 'Auf den Produktnamen klicken um Dateien herunterzuladen.',
	'PHPSHOP_DOWNLOADS_EXPIRED' => 'Sie haben bereits die Dateien maximal heruntergeladen, oder der Download ist abgelaufen.',
	
	//account.index.tpl
	'VM_ACCOUNT_NO_PERMISSION' => 'Es scheint daß Sie noch nie zuvor eine Buchung gemacht haben. Kontoinformation wird verfügbar sein sobald Buchungen platziert sind.',
	'VM_ACCOUNT_FOOR_INFO' => 'Zu Ihrer Information:',
	//'VM_ACCOUNT_TIP_WELCOME' => 'Willkommen zu Ihrer persönlichen Buchungsseite. Hier können Sie Ihre Buchungen überprüfen, Zahlungen machen und Ihre persönliche Kontoinformation bearbeiten.  Bitte wählen Sie die entsprechende Option unten.',
	'VM_ACCOUNT_TIP_ACCOUNT' => 'Bitte auf den Kontoinformationenlink oben klicken, um Ihre persönliche Kontoinformationen zu sehen und zu bearbeiten, einschließlich Name, Mail-Adresse, Passwort und Kontaktdaten.',
	'VM_ACCOUNT_TIP_BOOKINGS' => 'Um Ihre Buchung,  einschließlich Zahlungsdetails, Richtungen  und Schlüssel-Zugang, zu sehen, oben klicken oder auf die unteren Links',
    //'PHPSHOP_ORDER_PRINT_PO_LBL'
	
	//account.orderz
	'VM_ACCOUNT_BOOKING_SUMMARY' => 'Hier ist Ihre Reservierungsübersicht. Um Ihren Restbetrag zu bezahlen, klicken Sie bitte auf den Jetzt Zahlen Knopf unterhalb der Rechnung. Bitte drucken Sie jetzt eine Kopie Ihrer Buchung-Details für Ihre Reise.',
	'VM_ACCOUNT_DEPOSIT_INFO' => '<b>Bitte beachten:</b>&nbsp;Die erste $250 Anzahlung muss per Kreditkarte bezahlt werden um Ihre Buchung zu sichern. Zahlung durch irgendeine andere Methode wird abgelehnt und Ihre Buchung wird <b>NICHT</b> bestätigt.<br />Kreditkartenzahlungen verursachen eine Gebühr von {creditcard}, e-check verursacht eine Gebühr von {echeque} per Scheck',	
	'VM_ACCOUNT_PAYMENTS_CLEARED' => 'Danke, alle Zahlungen wurden eingereicht und geklärt',
	

	//ps_payments
	'VM_ACCOUNT_PLEASE_NOTE' => '<strong>Bitte beachten:</strong> eine Zahlung von %s ist fällig',
	
	
	//srgsre
	
	'VM_ACCOUNT_TIP_WELCOME' => 'Willkommen zu Ihrer persönlichen Buchungsseite. Hier können Sie Ihre Buchungen überprüfen, Zahlungen machen und Ihre persönliche Kontoinformation bearbeiten. Bitte wählen Sie die entsprechende Option unten.',
	
	//echo sprintf($VM_LANG->_('VM_ORDER_EMAIL_NOTES_EMAIL'), echo $terms_url, echo URL );
	// echo $VM_LANG->_('VM_ACCOUNT_BOOKINGS_LISTED') 
	
	'VM_ACCOUNT_PREFERRED_PAYMENT' => 'Bitte wählen Sie Ihre bevorzugte Zahlungsmethode',
	'VM_ACCOUNT_PAY_NOW' => 'Jetzt zahlen',
	'VM_ACCOUNT_RECIVED_PAYMENTS' => 'Unten ist eine Liste der Zahlungen die wir für Ihre Buchung erhalten haben und die ausstehenden Zahlungen.',
	'VM_ACCOUNT_SELECT_STAGE' => 'Sie müssen mindestens eine Zahlungsphase wählen',
	'VM_ACCOUNT_NOT_UPDATE' => 'Konnte die gewählte Zahlungsphase nicht aktualisieren. Bitte vergewissern Sie sich ob Sie die richtigen Daten eingegeben haben.',
	'VM_ACCOUNT_PAYMENT_CODE' => 'Anfang der Analyse des Zahlungs-Extra Info Kode...',
	'VM_ACCOUNT_CODE_ERROR' => 'Fehler: Kode der Zahlungsmethode',
	'VM_ACCOUNT_PARSE_ERROR' => 'enthält einen Analysenfehler!<br />Bitte erst korrigieren',
	'VM_ACCOUNT_CHECK_DETAILS' => 'Bitte auf den Kontoinformationenlink oben klicken um Ihre persönliche Kontoinformationen zu sehen und zu bearbeiten, einschließlich Name, Mail-Adresse, Passwort und Kontaktdaten.',
	'VM_ACCOUNT_BOOKINGS_LISTED' => 'Um Ihre Buchung,  einschließlich Zahlungsdetails, Richtungen und Schlüssel-Zugang, zu sehen, oben klicken oder auf die unteren Links',
	
	//account.billing
	
	'VM_ACCOUNT_CHECK_INFO' => 'Auf dieser Seite können Sie Ihre persönliche Kontoinformation bearbeiten, einschließlich Name, Mail-Adresse, Passwort und Rechnungsdaten. Um Ihre Information zu speichern, auf den schwarzen Speicher-Knopf klicken. Sie müssen klicken um Ihre Änderungen zu speichern. Um ohne irgendwelche Änderungen zum Menü zurückzukehren, bitte den grünen Zurück-Knopf klicken.',
	
	); $VM_LANG->initModule( 'account', $langvars );
?>