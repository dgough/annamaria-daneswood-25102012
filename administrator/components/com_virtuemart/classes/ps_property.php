<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );
/**
*
* @version $Id: ps_product.php 1475 2008-07-16 17:35:35Z soeren_nb $
* @package VirtueMart
* @subpackage classes
* @copyright Copyright (C) 2004-2008 soeren - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/

/**
 * The class is is used to manage product repository.
 * @package virtuemart
 * @author pablo, jep, gday, soeren
 * 
 */
class ps_property extends vmAbstractObject {
	
	function canManage(){
		global $vmuser;
		if(!in_array($vmuser->gid, array(23,24,25,31))){
			$GLOBALS['vmLogger']->err( 'You do not have permission to manage seasons.' );
			return false;
		}
		return true;
	}
	
	
	function getAllowedProperties(){
		global $vmuser, $db;
		
		//If user is an owner, only select their properties
		if(ps_property::canManage()){
			$where = '';
			if($vmuser->gid == 31) $where = "AND a.user = '$my->id'";
			
			$db->query("SELECT p.id from #__hp_properties AS p, jos_hp_agents as a WHERE a.id = p.agent $where");
			$ids = array();
			while($db->next_record()){
				$ids[] = $db->f("id");
			}
			
			return $ids;
		}else{			
			return false;
		}
	}
    
}  // ENd of CLASS ps_property

?>