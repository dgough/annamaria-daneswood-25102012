<?php
if( ! defined( '_VALID_MOS' ) && ! defined( '_JEXEC' ) )
die( 'Direct Access to ' . basename( __FILE__ ) . ' is not allowed.' ) ;
/**
 *
 * @version $Id: ps_tax.php 1317 2008-03-17 22:54:14Z thepisu $
 * @package VirtueMart
 * @subpackage classes
 * @copyright Copyright (C) 2004-2008 soeren - All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
 *
 * http://virtuemart.net
 */


class ps_season extends vmAbstractObject  {

	var $key = 'season_id';
	var $key_rate = 'rate_id';
	var $_table_name = '#__{vm}_seasons';
	var $_table_name_rate = '#__{vm}_seasons_rates';
	var $ps_booking;
	
	//Set the dateFrom dateTo and label SQL statements
	//This is needed as we need to detect if the rate links to as season 
	//or if the date info is held with the rate
	var $sqlDateFrom = 'IF(r.season_id, s.dateFrom, r.dateFrom)';
	var $sqlDateTo = 'IF(r.season_id, s.dateTo, r.dateTo)';
	var $sqlLabel = 'IF(r.season_id, s.label, r.label)';
	var $sqlDateLabel;

	function __construct() {
		$this->ps_booking = new ps_booking();
		$this->sqlDateLabel = "$this->sqlDateFrom AS dateFrom, $this->sqlDateTo AS dateTo, $this->sqlLabel AS label";		
	}
	
	
	
	function validate_add_season( &$d ){		
		global $vmLogger, $VM_LANG;

		list($syear,$smonth,$sday) = explode('-',$d['dateFrom']);
		if (!checkdate($smonth,$sday,$syear)) {
			$vmLogger->err( 'The given start date is invalid, please enter a valid start date for the season' ) ;
			return false;
		}

		list($eyear,$emonth,$eday) = explode('-',$d['dateTo']);
		if (!checkdate($emonth,$eday,$eyear)) {
			$vmLogger->err( 'The given end date is invalid, please enter a valid end date for the season' ) ;
			return false;
		}
		return true;
	}
	
	
	function validate_update_season( &$d ){
		global $vmLogger, $VM_LANG;

		if(!$d["season_id"]) {
			$vmLogger->err( $VM_LANG->_('VM_BK_SEASON_ERR_UPDATE_SELECT') ) ;
			return false ;
		}

		list($syear,$smonth,$sday) = explode('-',$d['dateFrom']);
		if (!checkdate($smonth,$sday,$syear)) {
			$vmLogger->err( 'The given start date is invalid, please enter a valid start date for the season' ) ;
			return false;
		}

		list($eyear,$emonth,$eday) = explode('-',$d['dateTo']);
		if (!checkdate($emonth,$eday,$eyear)) {
			$vmLogger->err( 'The given end date is invalid, please enter a valid end date for the season' ) ;
			return false;
		}
		return true;
	}
	
	
	function validate_delete_season( &$d ){
		global $vmLogger, $VM_LANG;
		
		if( ! $d["season_id"] ) {
			$vmLogger->err( 'Unable to find season to delete. No season id specified' ) ;
			return false ;
		}
		/*
		$db = new ps_DB();
		$db->query("SELECT count(season_id) AS seasons FROM #__{vm}_seasons_rates WHERE season_id = ".$d["season_id"]);
		$db->num_rows();
		if($db->f('seasons')){
			$vmLogger->err( 'This season has season rates associated with it. Please delete the associated rates before removing the season.' ) ;
			return false ;
		}*/

		return true;
	}
	
	
	function add_season( &$d, $show_logger = 1 ){
		
		global $VM_LANG, $modulename;
		$modulename = 'booking';
		$db = new ps_DB( ) ;

		if( ! $this->validate_add_season( $d ) ) {
			return false;
		}

		$fields = array(
			'dateFrom' => vmget( $d, 'dateFrom' ),
			'dateTo' => vmget( $d, 'dateTo' ),
			'label' => vmget( $d, 'label' )
		);

		$db->buildQuery('INSERT', $this->_table_name, $fields );

		if( !$db->query() ) {
			$GLOBALS['vmLogger']->err($VM_LANG->_('VM_BK_SEASON_ADD_FAILED'));
			return false;	
		}
		
		$_REQUEST['season_id'] = $db->last_insert_id() ;
		if($show_logger) $GLOBALS['vmLogger']->info($VM_LANG->_('VM_BK_SEASION_ADDED'));
		return true;
			
	}
	
	
	function update_season( &$d, $show_logger = 1 ){
		
		global $VM_LANG, $modulename, $pagePermissionsOK;
		$modulename = 'booking';
		$db = new ps_DB( ) ;

		if( ! $this->validate_update_season( $d ) ) {
			return false;
		}

		$fields = array(
			'season_id' => vmget( $d, 'season_id' ),
			'dateFrom' => vmget( $d, 'dateFrom' ),
			'dateTo' => vmget( $d, 'dateTo' ),
			'label' => vmget( $d, 'label' )
		);

		$db->buildQuery('UPDATE', $this->_table_name, $fields, 'WHERE season_id='.$d['season_id']);

		if( !$db->query() ) {
			$GLOBALS['vmLogger']->err($VM_LANG->_('VM_BK_SEASON_UPDATE_FAILED'));
			return false;
		}
		
		if($show_logger) $GLOBALS['vmLogger']->info($VM_LANG->_('VM_BK_SEASON_UPDATED'));
		return true;		
		
	}
	
	
	function delete_season( &$d ){
		
		if( ! $this->validate_delete_season( $d ) ) {
			return False ;
		}

		$record_id = $d["season_id"] ;

		if( is_array( $record_id ) ) {
			foreach( $record_id as $record ) {
				if( ! $this->delete_record_season( $record, $d ) )
				return false ;
			}
			return true;
		} else {
			return $this->delete_record_season( $record_id, $d ) ;
		}
	}


	/**
	 * Deletes one season record.
	 */
	function delete_record_season( $record_id, &$d ) {
		global $db ;

		$q = "DELETE FROM $this->_table_name WHERE season_id = $record_id";
		if(!$db->query( $q )){
			$GLOBALS['vmLogger']->err('Unable to delete season');
			return false;
		}else{
			$GLOBALS['vmLogger']->info('Season deleted successfully.');
			
			if(!$db->query("DELETE FROM #__{vm}_seasons_rates WHERE season_id = ".$d["season_id"])){
				$GLOBALS['vmLogger']->err( 'Unable to delete associated season pricing' ) ;
			}			
			return true;	
		}		
	}

	
	
	
	/**
	 * Validates the input values before adding an item
	 *
	 * @param arry $d The _REQUEST array
	 * @return boolean True on success, false on failure
	 */
	function validate_add_rate( &$d ) {		
		global $vmLogger, $VM_LANG;
		
		if (!is_numeric($d['rate']) || $d['rate']=='') {
			$vmLogger->err( 'The given rate is either empty or isn\'t a number. Please enter a valid rate for the season' ) ;
			return false;
		}
		
		//Check if we are in season independant mode.
		//If we are and no season has been selected, validate dates
		if(defined('VB_SEASON_INDEPENDENT') && VB_SEASON_INDEPENDENT == 1){
			
			if($d['season_id'] == ''){				
				if(!$this->validate_add_season( $d )){
					return false;
				}				
			}
		}
		
				
		if($d['property_id'] == ''){
			$vmLogger->err( 'Please select a property.' ) ;
			return false;
		}
		
		//Remove all non-numeric characters
		$d['discount'] = ereg_replace('[^0-9]+',"",$d['discount']);
		if($d['discount'] > 100) $d['discount'] = 100;
		if($d['discount'] < 0) $d['discount'] = 0;

		return true;
	}
	
	/**
	 * Validates the input values before updating an item
	 *
	 * @param arry $d The _REQUEST array
	 * @return boolean True on success, false on failure
	 */
	function validate_update_rate( &$d ) {
		global $vmLogger, $VM_LANG;
		
		if (!is_numeric($d['rate']) || $d['rate']=='') {
			$vmLogger->err( 'The given rate is either empty or isn\'t a number. Please enter a valid rate for the season' ) ;
			return false;
		}
		
		//Check if we are in season independant mode.
		//If we are and no season has been selected, validate dates
		if(defined('VB_SEASON_INDEPENDENT') && VB_SEASON_INDEPENDENT == 1){

			if($d['season_id'] == ''){				
				if(!$this->validate_add_season( $d )){
					return false;
				}				
			}
		}
		
		if($d['property_id'] == ''){
			$vmLogger->err( 'Please select a property.' ) ;
			return false;
		}
		
		//Remove all non-numeric characters
		$d['discount'] = ereg_replace('[^0-9]+',"",$d['discount']);
		if($d['discount'] > 100) $d['discount'] = 100;
		if($d['discount'] < 0) $d['discount'] = 0;
		
		return true;
	}
	
	/**
	 * Validates the input values before deleting an item
	 *
	 * @param arry $d The _REQUEST array
	 * @return boolean True on success, false on failure
	 */
	function validate_delete_rate( &$d ) {
		global $vmLogger, $VM_LANG;

		if( ! $d["rate_id"] ) {
			$vmLogger->err( 'Unable to find rate to delete. No rate id specified' ) ;
			return false ;
		}

		return true;
	}

	/**
	 * Creates a new season record
	 *
	 * @param arry $d The _REQUEST array
	 * @return boolean True on success, false on failure
	 */
	function add_rate( &$d, $update = 0 ) {
		global $VM_LANG, $modulename;
		$modulename = 'booking';
		$db = new ps_DB( ) ;

		if( ! $this->validate_add_rate( $d ) ) {
			return false;
		}

		$fields = array(
			'property_id' => vmget( $d, 'property_id' ),
			'season_id' => vmget( $d, 'season_id' ),
			'rate' => vmget( $d, 'rate' ),
			'currency' => vmget( $d, 'currency' ),
			'per_person_pricing' => vmget( $d, 'per_person_pricing' ),
			'min_people_force' => vmget( $d, 'min_people_force' ),
			'min_people' => vmget( $d, 'min_people' ),
			'max_people' => vmget( $d, 'max_people' ),
			'min_stay' => vmget( $d, 'min_stay' ),
			'max_stay' => vmget( $d, 'max_stay' ),
			'long_stay_days' => vmget( $d, 'long_stay_days' ),
			'long_stay_rate' => vmget( $d, 'long_stay_rate' ),
			'changeover_day' => vmget( $d, 'changeover_day' ),
			'changeover_force' => vmget( $d, 'changeover_force' ),
			'discount' =>  vmget( $d, 'discount' )
		);
				
		if(!vmget( $d, 'season_id')){
			$fields['dateFrom'] = vmget( $d, 'dateFrom' );
			$fields['dateTo'] = vmget( $d, 'dateTo' );
			$fields['label'] = vmget( $d, 'label' );			
			$fields['season_id'] = 0;			
		}else{
			$fields['dateFrom'] = '';
			$fields['dateTo'] = '';
			$fields['label'] = '';			
		}

		//Update tax info for property
		$tax_id = vmget( $d, 'tax_rate_id' );
		if($tax_id){
			$db->buildQuery('REPLACE', '#__{vm}_property_tax', array('tax_rate_id'=>$tax_id,'property_id'=>vmget( $d, 'property_id' )));
			if(!$db->query()){
				$GLOBALS['vmLogger']->err('Unable to update property tax information');
				return false;
			}
		}	
		
		if($update){
			$action = 'UPDATE';
			$where = ' WHERE rate_id = '.vmGet($d,'rate_id');
		}else{
			$action = 'INSERT';
			$where = '';	
		}

		$db->buildQuery($action, $this->_table_name_rate, $fields, $where );	
		
		if( !$db->query() ) {
			$GLOBALS['vmLogger']->err($VM_LANG->_('VM_BK_SEASON_ADD_FAILED'));
			return false;		
		}
		
		$_REQUEST['rate_id'] = $action == 'INSERT' ? $db->last_insert_id() : $_REQUEST['rate_id'];			
		
		//Update the discount information
		if(!$this->updateDiscounts($d, $_REQUEST['rate_id'])) return false;
		
		if(!$update) $GLOBALS['vmLogger']->info($VM_LANG->_('VM_BK_SEASION_ADDED'));		
		else $GLOBALS['vmLogger']->info('Seasonn rate updated successfully.');		
		return true;		
	}

	/**
	 * Updates a season record
	 * @author pablo
	 *
	 * @param arry $d The _REQUEST array
	 * @return boolean True on success, false on failure
	*/
	function update_rate( &$d ) {
		global $VM_LANG, $modulename, $pagePermissionsOK;
		$modulename = 'booking';
		$db = new ps_DB( ) ;
		
		

		if( ! $this->validate_update_rate( $d ) ) {
			return false;
		}
		
		return $this->add_rate($d, 1);
	}


	/**
	 * Controller for Deleting Records.
	 */
	function delete_rate( &$d ) {

		if( ! $this->validate_delete_rate( $d ) ) {
			return False ;
		}
		
		//Update the discount information
		if(!$this->deleteDiscounts($d['rate_id'])) return false;

		$record_id = $d["rate_id"] ;
		if( is_array( $record_id ) ) {
			foreach( $record_id as $record ) {
				if( ! $this->delete_record_rate( $record, $d ) )
				return false ;
			}
			return true ;
		} else {
			return $this->delete_record_rate( $record_id, $d ) ;
		}
	}


	/**
	 * Deletes one season record.
	 */
	function delete_record_rate( $record_id, &$d ) {
		global $db ;

		$q = "DELETE FROM $this->_table_name_rate WHERE rate_id=".(int)$record_id;
		$q .= " LIMIT 1" ;
		if(!$db->query( $q )){
			$GLOBALS['vmLogger']->err('Unable to delete season rate');
			return false;
		}else{
			$GLOBALS['vmLogger']->info('Season rate deleted successfully');
			return true;
		}		
	}

	
	/**
	 * 
	 */
	function updateDiscounts($d, $rate_id){
		
		$db = new ps_DB();
		
		//Do discount info
		$discount_params = vmGet($d,'discount_params',array());

		foreach(vmGet($d,'discount_enabled',array()) AS $id => $enabled){
				
			if($enabled != -1){
				//discount_params[1][weeks]
				$paramsArr = vmGet($discount_params , $id , array() );
				
				//Gather parameters
				$params = '';
				if (is_array( $paramsArr )) {
					$txt = array();
					foreach ($paramsArr as $k=>$v) {
						$txt[] = "$k=$v";
					}
					if( is_callable(array('mosParameters', 'textareaHandling'))) {
						$params = mosParameters::textareaHandling( $txt );
					}
					else {
						
						$total = count( $txt );
						for( $i=0; $i < $total; $i++ ) {
							if ( strstr( $txt[$i], "\n" ) ) {
								$txt[$i] = str_replace( "\n", '<br />', $txt[$i] );
							}
						}	
						$params = implode( "\n", $txt );				
					}			
				}
				
				$discount_fields = array(
					'season_rate_id' => $rate_id,
					'discount_id' => $id,
					'params' => $params,
					'enabled' => $enabled
				);
				//Add to database
				$db->buildQuery('REPLACE', '#__{vm}_seasons_discounts', $discount_fields );	
				if( !$db->query() ) {
					$GLOBALS['vmLogger']->err('Could not update discount method with id: '.$id);
					return false;		
				}
			}else{
				if(!$db->query("DELETE FROM #__{vm}_seasons_discounts WHERE season_rate_id = $rate_id AND discount_id = $id")){
					$GLOBALS['vmLogger']->err('Could not delete discount method with id: '.$id);
					return false;	
				}
			}
		}	
		return true;
	}
	
	
	/**
	 * 
	 */
	function deleteDiscounts($rate_id){
		
		$db = new ps_DB();
		return $db->query('DELETE FROM #__{vm}_seasons_discounts WHERE season_rate_id = '.$rate_id);
		
	}
	
	
	/**
	 * Returns the number of seasons from x date to y date
	 * Get all season info for given date within range
	 * If no season rate for given property, then get general season rate.
	 * If no general season rate then log error
	 */
	function getPrice() {
		global $vmLogger;
		
		$db = new ps_DB();
		
		list($syear,$smonth,$sday) = explode('-',$this->datefrom);
		list($syear,$smonth,$sday) = explode('-',$this->datefrom);
		
		if (preg_match('/\d{4}\-\d{2}\-\d{2}/',$this->datefrom) && preg_match('/\d{4}\-\d{2}\-\d{2}/',$this->dateto)) {
			
			$days = (strtotime($this->datefrom) - strtotime($this->dateto)) / (60 * 60 * 24);
			
			foreach ($days as $day) {
				$db->query("SELECT propertyid,rate FROM $this->_table_name WHERE date_add('".$this->datefrom."' INTERVAL ".$day." DAY) between `dateFrom` and `dateTo`");
				
				if ($db->num_rows()==1) {
					
					$season_info = $db->loadAssocList();
					$price += $season_info['rate'];
					
				} elseif ($db->num_rows()>1) {
					
					$db->loadObjectList($seasons);
					
					foreach ($seasons as $season) {
						if ($season->propertyid==$this->propertyid) {
							$price = (int)$season->price;
							break;
						}
						$price = (int)$season->price;
					}
					
				} else {
					$vmLogger->err( 'Could not select a valid season entry for the given date ') ;
				}
			}
		}		

	}
	
	
	
	function getSeasons($id = '', $prop_id = '', $dateFrom = '', $dateTo = '', $where = array(), $order = '', $edit = 0){
		
		global $my, $limitstart, $limit, $vmuser;
		
		$db = new ps_db();	
		
		if($prop_id){
			if(!is_array($prop_id)){
				$prop_ids = array($prop_id);
			}else{
				$prop_ids = $prop_id;
			}
		}else{
			$prop_ids = array();
		}

		//Check if we are getting the list of season to edit them
		if($edit){
			//If user is an owner, only select their properties
			if($vmuser->gid == 31){
				//Get the properties that this user can manage
				$db->query("SELECT p.id FROM #__hp_properties AS p, #__hp_agents AS a WHERE a.id = p.agent AND a.user = '$my->id'");
				$ids = $db->loadResultArray();
	
				//Check if the supplied prop id is in the allowed results
				if(in_array($prop_id,$ids)){
					$prop_ids = array($prop_id);
					
				//If not, check that there were returned results for this user
				}else if(count($ids) > 0){
					$prop_ids = $ids;		
					
				//Else check that the user isnt trying to access someone elses season		
				}else if(!in_array($prop_id,$ids)){
					$GLOBALS['vmLogger']->err( 'You can only edit property seasons that you manage.' );
					return;
				}
			//Check if the user is admin
			}else if(in_array(strtolower($vmuser->gid), array(23,24,25))){
				$prop_ids = $prop_ids;
				
			//Otherwise throw error that they dont have permissions
			}else{
				$GLOBALS['vmLogger']->err( 'You do not have permission to manage this season.' );
				return false;
			}
		}

		//Compile where array
		if($id !== '') $where[] = "r.rate_id=$id";
		
		if(count($prop_ids)) $where[] = 'p.id IN ('.implode(',',$prop_ids).')';
		
		if($dateFrom && $dateTo) $where[] = "('$dateFrom' BETWEEN $this->sql AND $this->sqlDateTo OR $dateTo' BETWEEN $this->sqlDateFrom AND $this->sqlDateTo)";
		else if ($dateFrom) $where[] = "('$dateFrom' BETWEEN $this->sqlDateFrom AND $this->sqlDateTo)";
		else if ($dateTo) $where[] = "('$dateTo' BETWEEN $this->sqlDateFrom AND $this->sqlDateTo)";
		
		$q = "SELECT r.*, p.id AS prop_id, p.name AS prop_name, 
			$this->sqlDateLabel,
			t.tax_rate_id
			FROM $this->_table_name_rate AS r
	  		LEFT JOIN $this->_table_name AS s ON s.season_id = r.season_id		
	  		LEFT JOIN #__{vm}_property_tax AS t ON t.property_id = r.property_id	  		
	  		LEFT JOIN #__hp_properties AS p ON r.property_id = p.id".	  		
			(count($where) ? ' WHERE '.implode(' AND ', $where) : '').
			($order ? ' ORDER BY '.$order : ' ORDER BY p.name, dateFrom').
			($limit ? " LIMIT $limitstart, " . $limit : '');
			
		$db->query($q);
				
		return (count($db->num_rows()) > 0 ? $db : false);			
	}
	
	
	/**
	 * Function to return a single season
	 *
	 * @param unknown_type $id
	 * @param unknown_type $prop_id
	 * @param unknown_type $dateFrom
	 * @param unknown_type $dateTo
	 * @param unknown_type $where
	 * @param unknown_type $noEdit
	 * @return unknown
	 */
	function getSeason($id = '', $prop_id = '', $dateFrom = '', $dateTo = '', $where = array(), $order = '', $noEdit = 0){
		return $this->getSeasons($id, $prop_id, $dateFrom, $dateTo, $where, $order, $noEdit);
	}
	
	
	
	function getSeasonBounds($prop_id){
		
		//get the min and max values for a property
		$db = new ps_DB;		
		$q = "SELECT *, $this->sqlDateLabel
			FROM $this->_table_name_rate AS r
	  		LEFT JOIN $this->_table_name AS s ON s.season_id = r.season_id
	  		WHERE $this->sqlDateTo > now() AND r.property_id = $prop_id
	  		ORDER BY r.rate * IF(r.per_person_pricing, r.min_people, 1) ASC LIMIT 0,1";	
		
		//Get the minimum value from the seasons table
		$db->query($q);		
		$min = $db->get_row();
		
		$q = "SELECT *, $this->sqlDateLabel
			FROM $this->_table_name_rate AS r
	  		LEFT JOIN $this->_table_name AS s ON s.season_id = r.season_id
	  		WHERE $this->sqlDateTo > now() AND r.property_id = $prop_id
	  		ORDER BY r.rate * IF(r.per_person_pricing, r.min_people, 1) DESC LIMIT 0,1";	
		
		//Get the maximum value from the seasons table
		$db->query($q);		
		$max = $db->get_row();	
	
		//If we found a price return 
		if($min && $max){
			return array('minimum'=>$min,'maximum'=>$max);
		}else{
			return false;
		}
	}
	
	
	
	/**
	 * This function retrieves the most current discount for a particular property
	 *
	 * @param $prop_id
	 * @return array or false
	 */
	
	function getSeasonDiscount($prop_id, $date = ''){
		
		$date = $date ? $date : date('Y-m-d');
		$season = $this->getSeasons('',$prop_id,$date,'',array('r.discount != ""'), "dateFrom");
			
		if(!$season) return false;
		
		if($season->num_rows()){			
			return array('discount'=>$season->f('discount'),
						'dateFrom'=>$season->f('dateFrom'),
						'dateTo'=>$season->f('dateTo')
						);				
		}else{
			return false;
		}	
	}
}
?>