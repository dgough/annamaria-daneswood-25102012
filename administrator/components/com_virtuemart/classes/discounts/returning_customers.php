<?php

/**
 * Enter description here...
 *
 */
class returning_customers{
	
	function totalDiscount($total, $nights, $dateFrom, $dateTo, $ps_booking){
		global $my, $mosConfig_absolute_path;
		
		$params = $this->params;
		$discount_amount = $params->get('discount_amount',0); 
		$max_weeks = $params->get('weeks',0); 
		
		if($my->id){
			//Get previous bookings number
			$db = new ps_DB();
			$db->query("SELECT count(order_id) AS `orders` FROM #__{vm}_orders WHERE user_id = '$my->id'");
			$db->next_record();
			
			if($db->f('orders') > 0){
				
				$weeks = floor($nights / 7);
				$total = (($weeks > $max_weeks) ? $max_weeks : $weeks) * $discount_amount;
				
				return $total;
			}else{
				return 0;
			}
		}else{
			return 0;
		}
	}		
}
?>