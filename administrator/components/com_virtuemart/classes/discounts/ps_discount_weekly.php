<?php

class ps_discount_weekly{
				
	function totalDiscount($total, $nights, $dateFrom, $dateTo, $ps_booking){
		
		$params = $this->params;
		$start_amount = $params->get('start_amount',0);
		$min_weeks = $params->get('min_weeks',0);
		$max_weeks = $params->get('max_weeks',0);
		$additional_weeks_value = $params->get('additional_weeks',0);
			
		if($start_amount && $min_weeks){
				
			$weeks = floor($nights / 7);
			$return = 0;
			
			if($weeks >= $min_weeks){
				//Add initial ammount
				$return += $start_amount;

				//Calculate how many additional weeks there are
				$additional_weeks = $weeks - $min_weeks;				
				if($additional_weeks > 0){
					
					//If the total number of weeks is above the max weeks, multiply by max weeks offset, otherwise additional weeks
					$return += ($weeks > $max_weeks ? ($max_weeks - $min_weeks) : $additional_weeks) * $additional_weeks_value;					
				}
			}
		}
		return $return;
	}		
}
?>