<?php

/**
 * Enter description here...
 *
 */
class ps_discount_shortbreaks{
	
	var $valid = 0;
	var $processed = 0;

	function process(&$ps_booking){
		$db = new ps_DB();		
		
		//Reset validity
		$this->valid = 0;
		$this->processed = 1;
		$min_nights = (int)$this->params->get('min_nights');
		
		if($min_nights > 0){				
			if(strtotime("+$min_nights days", strtotime($ps_booking->dateFrom)) > strtotime($ps_booking->dateTo)){
				$ps_booking->minNights = $min_nights;
				return;
			}
		}
				
		//Checks if the booking starts on the same day another finishes
		$db->query("SELECT * FROM #__{vm}_order_booking AS b
					LEFT JOIN #__{vm}_orders AS o ON o.order_id = b.order_id
					WHERE 
					o.order_status IN ('C') AND 
					b.property_id = $ps_booking->property_id AND
					b.departure <= '$ps_booking->dateFrom'
					ORDER BY b.departure DESC LIMIT 0,1");
		$db->next_record();
		if($db->f('departure') == $ps_booking->dateFrom){
			$this->valid = 1;
			return;
		}
				
		//Checks if the booking ends on the same day another starts
		$db->query("SELECT * FROM #__{vm}_order_booking AS b
					LEFT JOIN #__{vm}_orders AS o ON o.order_id = b.order_id
					WHERE 
					o.order_status IN ('C') AND 
					b.property_id = $ps_booking->property_id AND						
					b.arrival >= '$ps_booking->dateTo'
					ORDER BY b.arrival ASC LIMIT 0,1");
		$db->next_record();			
		if($db->f('arrival') == $ps_booking->dateTo){
			$this->valid = 1;
			return;
		}
	}
		
	function nightlyDiscount($season_id, $season, $date, $day_no, $total, $nights, $dateFrom, $dateTo, &$ps_booking){
		
		//Do some processing to work out if booking starts/ends on the same day as another booking
		if(!$this->processed) $this->process($ps_booking);
		
		if($this->valid){

			$ps_booking->validation['MIN_NIGHTS'] = 0;	
			
			//No discount, only remove the nights validation
			return 0;
		}			
	}
}
?>