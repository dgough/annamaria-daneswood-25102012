<?php

/**
 * Gap filler discount module:
 * Allows you to offer 
 *
 */
class ps_discount_gapfiller{
	
	var $isGap = false;
	var $starts = 0;
	var $ends = 0;
	var $arrival = '';
	var $departure = '';
	var $valid = 0;
	var $processed = 0;
	var $currSeason = 0;
	var $nights;
	var $full_gap;
	var $partial_gap;
	var $force_full_gap;
	
	
	function process(&$ps_booking){
		global $VM_LANG;
		
		$db = new ps_DB();		
		
		//Reset validity
		$this->valid = 0;
		$this->arrival = '';
		$this->departure = '';
		
		$this->nights = intval(trim($this->params->get('nights',7)));
		$this->full_gap = intval(trim($this->params->get('full_discount')));
		$this->partial_gap = intval(trim($this->params->get('partial_discount')));
		$this->force_full_gap = intval(trim($this->params->get('force_full_gap', 1)));
		
		if(!$this->full_gap && !$this->partial_gap){
			$this->processed = 1;
			return;
		}

		//Check that the booked period is less than the minimum number of nights
		if(!(round((strtotime($ps_booking->dateTo) - strtotime($ps_booking->dateFrom)) / 86400) < $this->nights)){
			$this->processed = 1;	
			return;
		}		
		
		//Checks if the booking starts on the same day another finishes
		$db->query("SELECT * FROM #__{vm}_order_booking AS b
					LEFT JOIN #__{vm}_orders AS o ON o.order_id = b.order_id
					WHERE 
					o.order_status IN ('C') AND 
					b.property_id = $ps_booking->property_id AND
					b.departure <= '$ps_booking->dateFrom'
					ORDER BY b.departure DESC LIMIT 0,1");
		$db->next_record();
		
		$this->arrival = $db->f('departure');
		$this->starts = $this->arrival == $ps_booking->dateFrom;
		
		//Checks if the booking ends on the same day another starts
		$db->query("SELECT * FROM #__{vm}_order_booking AS b
					LEFT JOIN #__{vm}_orders AS o ON o.order_id = b.order_id
					WHERE 
					o.order_status IN ('C') AND 
					b.property_id = $ps_booking->property_id AND						
					b.arrival >= '$ps_booking->dateTo'
					ORDER BY b.arrival ASC LIMIT 0,1");
		$db->next_record();			
		$this->departure = $db->f('arrival');
		$this->ends = $this->departure == $ps_booking->dateTo;
		
		//Check that the gap is less than the minimum number of nights
		if(!(round((strtotime($this->departure) - strtotime($this->arrival)) / 86400) < $this->nights)){
			$this->processed = 1;	
			return;
		}
		
		//set gap flag
		$this->isGap = true;

		//Validate based on if full/partial discount is set
		if(
			($this->full_gap && $this->starts && $this->ends)
			||
			(!$this->force_full_gap && $this->partial_gap && ($this->starts || $this->ends))
		){
			$this->valid = 1;
		}else{
			$this->valid = 0;
		}		
				
		$this->processed = 1;
	}
		
	function nightlyDiscount($season_id, $season, $date, $day_no, $total, $nights, $dateFrom, $dateTo, &$ps_booking){
		global $VM_LANG;
		
		//Do some processing to work out if booking starts/ends on the same day as another booking
		if(!$this->processed || $this->currSeason != $season_id) $this->process($ps_booking);
		$this->currSeason = $season_id;
		
		if($this->valid){
			
			$ps_booking->validation['MIN_NIGHTS'] = 0;	
					
			//Full gap booking
			if($this->starts && $this->ends && $this->full_gap){
			
				return $total * ($this->full_gap / 100);
				
			//Partial booking
			}else if($this->partial_gap){
								
				return $total * ($this->partial_gap / 100);	
			}
		}else{
			if($this->isGap && $this->force_full_gap && (($this->starts && !$this->ends) || (!$this->starts && $this->ends)))
			{
				static $alerted;
				if(!$alerted){
					$alerted = 1;
					$ps_booking->validation['MIN_NIGHTS'] = 1;
					$GLOBALS['vmLogger']->err( $VM_LANG->_('VM_BOOKING_FULL_GAP') );
					$ps_booking->display_errors = false;
				}
			}
		}			
	}
}
?>