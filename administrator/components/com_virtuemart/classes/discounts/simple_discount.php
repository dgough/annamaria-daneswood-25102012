<?php
/**
 * Simple percentage or value discount
 *
 */
class simple_discount{
	
	function totalDiscount($total, $nights, $dateFrom, $dateTo, &$ps_booking){
		
		if($this->params->get('discount_type') == 1){	
			return $this->params->get('discount_amount');
		}
		
		return $total * ($this->params->get('discount_amount') / 100);
		
	}		
}
?>