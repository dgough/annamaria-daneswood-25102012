<?php

/**
 * Enter description here...
 *
 */
class mid_week_full_week{
		
	var $nights;
	var $unixFrom;
	var $unixTo;
	var $mid_week_days = array();
	var $mid_week_discount;
	var $full_week_discount;
	var $discount_weeks = array();
	
	function nightlyDiscount($season_id, $season, $date, $day_no, $total, $nights, $dateFrom, $dateTo, $ps_booking){
		
		if(!$this->unixFrom){
			$this->unixFrom = strtotime($dateFrom);
			$this->unixTo = strtotime($dateTo);
			$this->mid_week_days = explode(',',$this->params->get('mid_week_days'));
			$this->mid_week_discount = $this->params->get('mid_week_discount');
			$this->full_week_discount = $this->params->get('full_week_discount');
			
			if($this->full_week_discount){
				
				//Work out how many full weeks there are
				$weeks = floor($nights/7);
				
				$from = $this->unixFrom;
				$to = $this->unixFrom += ($weeks * 7 * 86400);
				
				for($i = $from; $i < $to; $i += 86400){
					$this->discount_weeks[] = date('Y-m-d',$i);
				}
				$this->unixFrom = $to;				
			}
		}
		//Get today as a unix timestamp		
		$day = strtotime($date);
		$discount_total = 0;

		//Check if today falls in the weekly discount period
		if(in_array($date, $this->discount_weeks)){
			$discount_total += ($this->full_week_discount / 100) * $total;	

		//Check if today is outside the weekly discount period but in the mid week discount
		}else if($day >= $this->unixFrom){						
			$dayNo = date('N',$day);
			if(in_array($dayNo,$this->mid_week_days)){
				$discount_total += ($this->mid_week_discount / 100) * $total;
			}
		}	
		//Return discount		
		return $discount_total;
	}	
}
?>