<?php
if( ! defined( '_VALID_MOS' ) && ! defined( '_JEXEC' ) )
die( 'Direct Access to ' . basename( __FILE__ ) . ' is not allowed.' ) ;


class ps_discount extends vmAbstractObject  {

	var $key = 'discount_id';
	var $_table_name = '#__{vm}_discounts';
	var $objects = array();
	var $objects_default = array();
	var $curSeason = 0;
	
	function __construct(&$ps_booking) {
				
		//Reset the object holder
		$this->objects = array();
		
		$db = new ps_DB();
		
		//Get rate specific to this property if any
		$q = "	SELECT * FROM $this->_table_name 
				ORDER BY `ordering`";		
		$db->query($q);
		
		while($db->next_record()){
		
			//Include the discount module file
			$file = $db->f('file');
			$class = $db->f('class');
			$params = new vmParameters( $db->f('params'), CLASSPATH."discounts/$file.xml", 'discount' );
		
			//Include class if not already included
			if(!class_exists($db->f('class'))) require_once(CLASSPATH."discounts/$file.php");
			$obj = new $class($ps_booking);	
			$obj->id = $db->f('discount_id');	
			$obj->name = $db->f('label');	
			$obj->order = $db->f('ordering');	
			$obj->params = $params;	
			$obj->file = $file;
			$obj->published = $db->f('published');	
			$obj->published_season = $db->f('published_season');	
			
			$this->objects[$db->f('discount_id')] = $obj;
		}
		$this->objects_default = $this->objects;
	}
	
	
	
	/**
	 * This function loops through all the discount 
	 * modules and gets the associated discount 
	 * value for the passed date and session id
	 * 
	 */
	function getNightlyDiscounts($season_id, $season, $date, $day_no, $total, $nights, $dateFrom, $dateTo, &$ps_booking){			
		
		//Get the season specific discounts
		$this->checkSeasonDiscounts($season_id);

		$return = array();
		foreach($this->objects as &$discount){			
			if($discount->published && method_exists($discount, 'nightlyDiscount')){
				$tmp = new stdClass();
				$tmp->id = $discount->id;
				$tmp->name = $discount->name;
				$tmp->value = $discount->nightlyDiscount($season_id, $season, $date, $day_no, $total, $nights, $dateFrom, $dateTo, $ps_booking);
				$return[$discount->order] = $tmp;
				
				//Set the new total value, this allows stacking of discounts
				$total = $total - $tmp->value;
			}
		}
		ksort($return);
		return $return;
	}
	
	
	
	/**
	 * This function loops through all the discount 
	 * modules and gets the associated discount value 
	 * 
	 */
	function getTotalDiscounts($total, $nights, $dateFrom, $dateTo, &$ps_booking){			

		//Reeset the discount methods incase the nightly discount changed them	
		$this->objects = $this->objects_default;

		$return = array();
		foreach($this->objects as &$discount){		
			if($discount->published && method_exists($discount, 'totalDiscount')){
				$tmp = new stdClass();
				$tmp->id = $discount->id;
				$tmp->name = $discount->name;
				$tmp->value = $discount->totalDiscount($total, $nights, $dateFrom, $dateTo, $ps_booking);				
				$return[$discount->order] = $tmp;
				
				//Set the new total value, this allows stacking of discounts
				$total = $total - $tmp->value;
			}
		}
		ksort($return);	
		return $return;
	}
		
	
	/**
	 * This function retrieves the discounts that are specific to a particular season
	 * It has the option override the default parameter for a discount method or disable one
	 */
	function checkSeasonDiscounts($season_id){

		if($this->curSeason != $season_id){
			$db = new ps_DB();
			$db->query("SELECT * FROM #__{vm}_seasons_discounts WHERE season_rate_id = $season_id AND (enabled = 1 OR enabled = 0)");
			while($db->next_record()){
			
				$id = $db->f('discount_id');
				if($this->objects[$id]->published_season){
					$this->objects[$id]->published = $db->f('enabled');
					$this->objects[$id]->params = new vmParameters( $db->f('params'), CLASSPATH."discounts/".$this->objects[$id]->file.".xml", 'discount' );
				}
			}
			$this->curSeason = $season_id;	
		}	
	}
	
	
	/**
	 * Gets the description from an xml file
	 *
	 * @param unknown_type $xmlFile
	 * @return unknown
	 */
	function getDiscountDesc($xmlFile = ''){
		global $mosConfig_absolute_path;
		
		if($xmlFile){
			if($raw = file_get_contents($xmlFile)){
				$xml = new SimpleXMLElement($raw);	
				return $xml->description;
			}			
		}
		return '';		
	}
	

	/**
	 * Lists all available files from the /classes directory
	 *
	 * @param string $name
	 * @param string $preselected
	 * @return string
	 */
	function list_discounts( $name, $preselected ) {
		global $mosConfig_absolute_path;
		$classes = vmReadDirectory( CLASSPATH.'discounts/', '.php', false, true );
		$array = array();
		foreach ($classes as $class ) {
			if( is_dir( $class ) ) continue;
			$classname = basename( $class, '.php' );
			if( $classname != 'ps_main' && $classname != 'ps_ini' ) {
				$array[$classname] = $classname;
			}
		}
		return ps_html::selectList( $name, $preselected, $array, 1, '', 'id="'.$name.'"' );
	}
	
	
	
	/**
	 * Return a drop down list of the other discount methods
	 */
	function list_order( $selected_order = 0 ) {

		$db = new ps_DB;

		$q  = "SELECT ordering,discount_id,label FROM $this->_table_name ORDER BY ordering ASC";
		$db->query( $q );
		
		$html = "<select class=\"inputbox\" name=\"ordering\">\n";
		while( $db->next_record() ) {
			if( $selected_order == $db->f("ordering") ) {
				$selected = "selected=\"selected\"";
			}
			else {
				$selected = "";
			}
			$html .= "<option value=\"".$db->f("ordering")."\" $selected>"
			.$db->f("ordering").'.'.$db->f("label")
			."</option>\n";
		}
		$html .= "</select>\n";
		return $html;		
	}
	
	
	/**
	 * This function kind of overrides the changeOrdering function with the abstractObject class,
	 * this allows us to send to it exactly what we want and bypasses the validation that it would
	 * normally use.
	 * 
	 */
	function changeOrder( $d ){
		
		return $this->changeOrdering( $this->_table_name, 'ordering', 'discount_id', 'label', 1 );
		
	}
	
	
	
	
	/**
	 * Discount Saving / Updating / Deleting routines
	 */
	
	/**
	 * Validates the input values before adding/updating an item
	 *
	 * @param arry $d The _REQUEST array
	 * @return boolean True on success, false on failure
	 */
	function validateAdd( &$d ){
		global $vmLogger, $VM_LANG;
		
		if(!vmGet($d,'label')){
			$vmLogger->err( 'You must specify a label for this discount method.' ) ;
			return false;
		}
		if(!vmGet($d,'file')){
			$vmLogger->err( 'You must specify a file for this discount method.' ) ;
			return false;
		}
		$path = CLASSPATH.'discounts/';
		$file = vmGet($d,'file');
		if(!file_exists($path.$file.'.php')){
			$vmLogger->err( "The file $file does not exist. Please make sure it exists in $path" );
			return false;
		}
		return true;		
	}
	
	/**
	 * Validation - calls the add validation method
	 */
	
	function validateUpdate( &$d ){
		return $this->validateAdd( $d );
	}
	
	/**
	 * Validates the input values before deleting an item
	 *
	 * @param arry $d The _REQUEST array
	 * @return boolean True on success, false on failure
	 */
	function validate_delete( $d ) {
		global $vmLogger, $VM_LANG;
		
		if( ! $d["discount_id"] ) {
			$vmLogger->err( 'No discount id specified, unable to delete.' ) ;
			return False ;
		}
		
		return True ;
	
	}
	
	
	function add( &$d, $update = 0 ){
		global $VM_LANG, $vmLogger;
		
		if(!$this->validateAdd($d)){
			return false;
		}
		
		$db = new ps_DB();
		
		$params = vmGet( $d, 'params', '' );
		if (is_array( $params )) {
			$txt = array();
			foreach ($params as $k=>$v) {
				$txt[] = "$k=$v";
			}
			if( is_callable(array('mosParameters', 'textareaHandling'))) {
				$d['params'] = mosParameters::textareaHandling( $txt );
			}
			else {
				
				$total = count( $txt );
				for( $i=0; $i < $total; $i++ ) {
					if ( strstr( $txt[$i], "\n" ) ) {
						$txt[$i] = str_replace( "\n", '<br />', $txt[$i] );
					}
				}
				$d['params'] = implode( "\n", $txt );
		
			}
		}
		
		$fields = array(
			'published' => vmget( $d, 'published', 0 ),
			'published_season' => vmget( $d, 'published_season', 0 ),
			'label' => vmget( $d, 'label' ),
			'file' => vmget( $d, 'file' ),
			'class' => vmget( $d, 'file' ),
			'params' => vmget( $d, 'params' ),
			'ordering' => vmget( $d, 'ordering' )
		);

		
		if($update){
			$action = 'UPDATE';
			$where = ' WHERE discount_id = '.$d['discount_id'];
		}else{
			$action = 'INSERT';
			$where = '';
			
			// add a new record			
			$sql="SELECT MAX(ordering) as max FROM $this->_table_name";
			$db->query($sql); 
			$db->next_record();
			$fields['ordering'] = $db->f('max')+1;			
		}
		$db->buildQuery($action, $this->_table_name, $fields, $where );

		if( !$db->query()) {
			$update ? $vmLogger->err('Failed to update discount method.') : $vmLogger->err('Failed to add discount method.');
			return false;
		}	

		$vmLogger->info('Discount method saved successfully.');
		$_REQUEST['discount_id'] = $update ? $d['discount_id'] : $db->last_insert_id();
		
		
		/* Moved UP in the list order */
		$dbu = new ps_DB();
		
		if( intval($d['ordering']) < intval($d['currentpos']) && $d['currentpos'] != '') {

			$q = "SELECT discount_id FROM $this->_table_name ";
			$q .= "WHERE ordering < '" . intval($d["currentpos"]) . "'";
			$q .= "AND ordering >= '" . intval($d["ordering"]) . "' AND discount_id != ".$_REQUEST['discount_id'];
			$db->query( $q );

			while( $db->next_record() ) {
				$dbu->query("UPDATE $this->_table_name SET ordering=ordering+1 WHERE discount_id='".$db->f("discount_id")."'");
			}
		}
		/* Moved DOWN in the list order */
		else if( intval($d['ordering']) > intval($d['currentpos']) && $d['currentpos'] != ''){

			$q = "SELECT discount_id FROM $this->_table_name ";
			$q .= "WHERE ordering > '" . intval($d["currentpos"]) . "'";
			$q .= "AND ordering <= '" . intval($d["ordering"]) . "' AND discount_id != ".$_REQUEST['discount_id'];
			$db->query( $q );
			
			while( $db->next_record() ) {
				$dbu->query("UPDATE $this->_table_name SET ordering=ordering-1 WHERE discount_id='".$db->f("discount_id")."'");
			}
		}
		
		
		return true;		
	}
	
	
	function update( &$d ){
		return $this->add( $d, 1 );
	}
	
	
	/**
	 * Controller for Deleting Records.
	 */
	function delete( &$d ) {
		
		if( ! $this->validate_delete( $d ) ) {
			return False ;
		}
		
		$record_id = $d["discount_id"] ;
		
		if( is_array( $record_id ) ) {
			foreach( $record_id as $record ) {
				if( ! $this->delete_record( $record, $d ) )
					return false ;
			}
			return true ;
		} else {
			return $this->delete_record( $record_id, $d ) ;
		}
	}
	/**
	 * Deletes one tax record.
	 */
	function delete_record( $record_id, &$d ) {
		global $db, $vmLogger;
					
		$q = "DELETE FROM $this->_table_name WHERE $this->key=$record_id";
		if($db->query( $q )){
			
			//Remove from season discounts
			$db->query('DELETE FROM #__{vm}_seasons_discounts WHERE discount_id = '.$record_id);
			
			$vmLogger->info("Discount method: $record_id deleted successfully");
		}else{
			$vmLogger->err('Failed to delete discount method.');
		}
	}
}
?>