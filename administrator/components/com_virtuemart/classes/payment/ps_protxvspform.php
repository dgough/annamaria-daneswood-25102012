<?php
// *************************************************************************
// *                                                                       *
// * Ecom Solution Protx VSP Form Payments Module for Virtuemart           *
// * Copyright (c) 2007 Ecom Solution LTD. All Rights Reserved             *
// * Release Date: 09.08.2007                                              *
// * Version 1.1                                                           *
// *                                                                       *
// *************************************************************************
// *                                                                       *
// * Email: support@ecommerce-host.co.uk                                   *
// * Website: http://www.e-commerce-solution.co.uk                         *
// *                                                                       *
// *************************************************************************
// *                                                                       *
// * This software is furnished under a license and may be used and copied *
// * only  in  accordance  with  the  terms  of such  license and with the *
// * inclusion of the above copyright notice.  This software  or any other *
// * copies thereof may not be provided or otherwise made available to any *
// * other person.  No title to and  ownership of the  software is  hereby *
// * transferred.                                                          *
// *                                                                       *
// * You may not reverse  engineer, decompile, defeat  license  encryption *
// * mechanisms, or  disassemble this software product or software product *
// * license.  Ecom Solution  may  terminate  this license  if you fail to *
// * comply with any of the terms and conditions set forth in our end user *
// * license agreement (EULA).  In such event,  licensee  agrees to return *
// * licensor  or destroy  all copies of software  upon termination of the *
// * license.                                                              *
// *                                                                       *
// *                                                                       *
// *************************************************************************

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

class ps_protxvspform {

    var $classname = "ps_protxvspform";
    var $payment_code = "PXFM";
    
    /**
    * Show all configuration parameters for this payment method
    * @returns boolean False when the Payment method has no configration
    */
    function show_configuration() {
        global $VM_LANG;
        global $mosConfig_live_site;
        $db = new ps_DB();
          
        /** Read current Configuration ***/
        include_once(CLASSPATH ."payment/".$this->classname.".cfg.php");
    ?> <img src="<?php echo $mosConfig_live_site; ?>/components/com_protxvspform/logo.gif"
  align="absmiddle" border="0" hspace="10"> &nbsp;&nbsp;&nbsp;&nbsp; <a href="index2.php?option=com_protxvspform">Protx 
VSP Forms Payment Module</a> by Ecom Solution &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; 
<img src="<?php echo $mosConfig_live_site; ?>/components/com_protxvspform/ecom-solution.gif"
  align="absmiddle" border="0" hspace="20" width="150" height="50"> 
<table>
  <tr valign="top"> 
    <td><b>General</b></td>
    <td colspan="3">These settings apply to all transactions</td>
  </tr>

  <tr valign="top" align="left"> 
    <td><b><strong>Licence</strong></b></td>
    <td> 
      <!-- get the local root -->
      <?php global $mosConfig_absolute_path; ?>
      <?php require_once($mosConfig_absolute_path . '/administrator/components/com_protxvspform/licencecheck.e.php'); ?>
      <!-- show status link -->
      <script language="JavaScript" src="<?php echo($mosConfig_live_site);  ?>/administrator/components/com_protxvspform/licencewin.js" type="text/javascript"></script>
      <a href="javascript:EL_open_Manager('com_protxvspform');"><b> 
      <?php echo $licence->EL_description; ?>
      </b></a> </td>
    <td>&nbsp;</td>
    <td>Enter your Ecom Solution licence key/download Id </td>
  </tr>
  <tr> 
    <td><b><strong>Protx Vendor </strong></b></td>
    <td> 
      <input type="text" name="PXFM_VENDOR" class="inputbox" value="<?php  echo PXFM_VENDOR ?>" />
    </td>
    <td>&nbsp;</td>
    <td> Merchant/Vendor name e.g. testvendor</td>
  </tr>
  <tr> 
    <td><b><strong>Currency </strong></b></td>
    <td> 
      <input type="text" name="PXFM_CURRENCY" class="inputbox" value="<?php  echo PXFM_CURRENCY ?>" />
    </td>
    <td></td>
    <td> </td>
  </tr>
  <tr> 
    <td><b><strong>Description </strong></b></td>
    <td> 
      <input type="text" name="PXFM_DESC" class="inputbox" value="<?php  echo PXFM_DESC ?>" />
    </td>
    <td>&nbsp;</td>
    <td> Order Description </td>
  </tr>
  <tr> 
    <td><strong> Apply AVS2 </strong></td>
    <td> 
      <select name="PXFM_ApplyAVSCV2" class="inputbox" >
        <option <?php  if (PXFM_ApplyAVSCV2 == '1') echo "\"selected\""; ?> value="1"> 
        YES </option>
        <option <?php  if (PXFM_ApplyAVSCV2 == '0') echo "\"selected\""; ?> value="0"> 
        NO </option>
      </select>
    </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td><strong> Apply 3DSecure </strong></td>
    <td> 
      <select name="PXFM_Apply3DSecure" class="inputbox" >
        <option <?php  if (PXFM_Apply3DSecure == '1') echo "\"selected\""; ?> value="1"> 
        YES </option>
        <option <?php  if (PXFM_Apply3DSecure == '0') echo "\"selected\""; ?> value="0"> 
        NO </option>
      </select>
    </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td><b>Mode</b></td>
    <td colspan="3">The Protx Key must correspond to the mode</td>
  </tr>
  <tr> 
    <td><strong> Test Mode </strong></td>
    <td> 
      <select name="PXFM_TEST" class="inputbox" >
        <option <?php  if (PXFM_TEST == 'SIM') echo "selected=\"selected\""; ?> value="SIM"> 
        Simulator </option>
        <option <?php  if (PXFM_TEST == 'TEST') echo " selected "; ?> value="TEST"> 
        Test </option>
        <option <?php  if (PXFM_TEST == 'LIVE') echo " selected "; ?> value="LIVE"> 
        Live </option>
      </select>
    </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td><b><strong>Protx Key </strong></b></td>
    <td> 
      <input type="text" name="PXFM_PASS" class="inputbox" value="<?php  echo PXFM_PASS ?>" />
    </td>
    <td>&nbsp;</td>
    <td> Merchant/Vendor encryption key, e.g. GgpGnGNNtn4dIp3i </td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp; </td>
  </tr>
  <tr> 
    <td><b>Advanced</b></td>
    <td colspan="3">You shouldn't need to change these settings</td>
  </tr>
  <tr> 
    <td><b><strong>Simulator URL</strong></b></td>
    <td> 
      <input type="text" name="PXFM_URL_SIM" class="inputbox" value="<?php  echo PXFM_URL_SIM ?>" />
    </td>
    <td>&nbsp;</td>
    <td>&nbsp; </td>
  </tr>
  <tr> 
    <td><b><strong>Test URL </strong></b></td>
    <td> 
      <input type="text" name="PXFM_URL_TEST" class="inputbox" value="<?php  echo PXFM_URL_TEST ?>" />
    </td>
    <td>&nbsp;</td>
    <td>&nbsp; </td>
  </tr>
  <tr> 
    <td><b><strong>Live URL </strong></b></td>
    <td> 
      <input type="text" name="PXFM_URL_LIVE" class="inputbox" value="<?php  echo PXFM_URL_LIVE ?>" />
    </td>
    <td>&nbsp;</td>
    <td>&nbsp; </td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<br>

<?php
    }
    
    function has_configuration() {
      // return false if there's no configuration
      return true;
   }
   
  /**
	* Returns the "is_writeable" status of the configuration file
	* @param void
	* @returns boolean True when the configuration file is writeable, false when not
	*/
   function configfile_writeable() {
      return is_writeable( CLASSPATH."payment/".$this->classname.".cfg.php" );
   }
   
  /**
	* Returns the "is_readable" status of the configuration file
	* @param void
	* @returns boolean True when the configuration file is writeable, false when not
	*/

   function configfile_readable() {
      return is_readable( CLASSPATH."payment/".$this->classname.".cfg.php" );
   }
   
  /**
	* Writes the configuration file for this payment method
	* @param array An array of objects
	* @returns boolean True when writing was successful
	*/
   function write_configuration( &$d ) {
      
      $my_config_array = array(
        "PXFM_VENDOR" =>    $d['PXFM_VENDOR'], 
        "PXFM_PASS" =>   $d['PXFM_PASS'], 
        "PXFM_CURRENCY" =>   $d['PXFM_CURRENCY'], 
        "PXFM_DESC" =>   $d['PXFM_DESC'], 
        "PXFM_TEST" =>   $d['PXFM_TEST'], 
        "PXFM_URL_SIM" =>   $d['PXFM_URL_SIM'], 
        "PXFM_URL_TEST" =>   $d['PXFM_URL_TEST'], 
        "PXFM_URL_LIVE" =>   $d['PXFM_URL_LIVE'],   
        "PXFM_ApplyAVSCV2" =>   $d['PXFM_ApplyAVSCV2'],    
        "PXFM_Apply3DSecure" =>   $d['PXFM_Apply3DSecure']                       
        );
        
      $config = "<?php\n";
      $config .= "defined('_VALID_MOS') or die('Direct Access to this location is not allowed.'); \n\n";
      foreach( $my_config_array as $key => $value ) {
        $config .= "define ('$key', '$value');\n";
      } 
      $config .= "?>";
  
      if ($fp = fopen(CLASSPATH ."payment/".$this->classname.".cfg.php", "w")) {
          fputs($fp, $config, strlen($config));
          fclose ($fp);
          return true;
     }
     else
        return false;
   }
   
  /**************************************************************************
  ** name: process_payment()
  ** returns: 
  ***************************************************************************/
   function process_payment($order_number, $order_total, &$d) {
      global $mosConfig_mailfrom;

      $err_level = error_reporting(E_ALL);
      //$err_handler = set_error_handler("option_explicit");

      global $vmLogger;
      global $mosConfig_Site_name, $mosConfig_live_site;
      $database = new ps_DB;
      
      /*** Get the Configuration File  ***/
      require_once(CLASSPATH ."payment/".$this->classname.".cfg.php");      
    
  	  // Get user email
	  $dbe = new ps_DB;
	  $qe = "SELECT email FROM #__users WHERE id='".$d["user_id"]."'";
	  $dbe->query($qe);
	  $dbe->next_record();
	  $email = $dbe->f("email");
      $CustomerName1 = $dbe->f("name"); 
  
  	  // Get user billing information - not yet used
	  $dbbt = new ps_DB;
	  $qt = "SELECT * FROM #__{vm}_user_info WHERE user_id=".$d["user_id"]." AND address_type='BT'";
	  $dbbt->query($qt);
	  $dbbt->next_record();
	  $user_info_id = $dbbt->f("user_info_id");
	  if( $user_info_id != $d["ship_to_info_id"]) {
	  	// Get user billing information
	  	$dbst =& new ps_DB;
	  	$qt = "SELECT * FROM #__{vm}_user_info WHERE user_info_id='".$d["ship_to_info_id"]."' AND address_type='ST'";
	  	$dbst->query($qt);
	  	$dbst->next_record();
	  }
	  else {
	  	$dbst = $dbbt;
	  }

      // max 100
      $CustomerName = 
      		substr($dbbt->f("title"), 0, 10) . " " . 
      		substr($dbbt->f("first_name"), 0, 40) . " " . 
      		substr($dbbt->f("last_name"), 0, 40) . "";  


      $data["BillingAddress"] = 
      		substr($dbbt->f("address_1"), 0, 60) . " \n" . 
      		substr($dbbt->f("city"), 0, 40) . " \n" . 
      		substr($dbbt->f("state"), 0, 40) . " \n" .  
      		substr(country_code_3_name($dbbt->f("country")), 0, 60);
      
      $data["BillingPostCode"] = substr($dbbt->f("zip"), 0, 20);
      
      $data["DeliveryAddress"] = 
      		substr($dbst->f("address_1"), 0, 60) . " \n" . 
      		substr($dbst->f("city"), 0, 40) . " \n" . 
      		substr($dbst->f("state"), 0, 40) . " \n" .  
      		substr(country_code_3_name($dbst->f("country")), 0, 60);
      $data["DeliveryPostCode"] = substr($dbst->f("zip"), 0, 20);
      
	  		  	
      switch (PXFM_TEST ) {
        case 'TEST': DEFINE ('PXFM_URL', PXFM_URL_TEST); break;
        case 'LIVE': DEFINE ('PXFM_URL', PXFM_URL_LIVE); break;
        default: DEFINE ('PXFM_URL', PXFM_URL_SIM); break;
      }
 
      DEFINE ('PXFM_NOTIFY', $mosConfig_live_site.'/administrator/components/com_virtuemart/protxvspform_notify.php');
      
      // crypt the stuff
      $amount =  $order_total;
      //on the order detail page
      if(empty($amount)) $amount = $dbo->f("order_total");       
	  $amount = sprintf ("%01.2f", $amount);
        
      $stuff = "VendorTxCode=" . substr($order_number, 0, 32) ."&"; 
      $stuff .= "Amount=$amount&";
      $stuff .= "Currency=" . PXFM_CURRENCY. "&";
      $stuff .= "Description=" . PXFM_DESC . "&";
      $stuff .= "SuccessURL=" . PXFM_NOTIFY . "&";
      $stuff .= "FailureURL=" . PXFM_NOTIFY . "&";
      
      // optional; 
      $stuff .= "VendorEmail=" . $mosConfig_mailfrom  . "&";    
      $stuff .= "CustomerEMail=" . $email  . "&";     
      $stuff .= "ApplyAVSCV2=" . PXFM_ApplyAVSCV2 . "&";
      $stuff .= "Apply3DSecure=" . PXFM_Apply3DSecure . "&";   
      $stuff .= "CustomerName=" . $CustomerName  . "&";   
      $stuff .= "BillingAddress=" . $data["BillingAddress"]  . "&";    
      $stuff .= "BillingPostCode=" . $data["BillingPostCode"]  . "&";    
      $stuff .= "DeliveryAddress=" . $data["DeliveryAddress"]  . "&";    
      $stuff .= "DeliveryPostCode=" . $data["DeliveryPostCode"]  . "";    
 
      //$data["BillingAddress"] 

      $crypt = base64_encode(SimpleXor($stuff,PXFM_PASS));
      DEFINE ('PXFM_CRYPT', $crypt);      
             
      // set back error-handling 
      error_reporting($err_level);
      //restore_error_handler();  

      // show the form... extra payment info
      return true;
      //return false;
    }
   
}


// functions 
//__________________________________________________________
      

function simpleXor($InString, $Key) {
  // Initialise key array
  $KeyList = array();
  // Initialise out variable
  $output = "";
  
  // Convert $Key into array of ASCII values
  for($i = 0; $i < strlen($Key); $i++){
    $KeyList[$i] = ord(substr($Key, $i, 1));
  }

  // Step through string a character at a time
  for($i = 0; $i < strlen($InString); $i++) {
    // Get ASCII code from string, get ASCII code from key (loop through with MOD), XOR the two, get the character from the result
    // % is MOD (modulus), ^ is XOR
    $output.= chr(ord(substr($InString, $i, 1)) ^ ($KeyList[$i % strlen($Key)]));
  }

  // Return the result
  return $output;
}

function getToken($thisString) {

  // List the possible tokens
  $Tokens = array(
    "Status",
    "StatusDetail",
    "VendorTxCode",
    "VPSTxId",
    "TxAuthNo",
    "Amount",
    "AVSCV2", 
    "AddressResult", 
    "PostCodeResult", 
    "CV2Result", 
    "GiftAid", 
    "3DSecureStatus", 
    "CAVV" );

  // Initialise arrays
  $output = array();
  $resultArray = array();
  
  // Get the next token in the sequence
  for ($i = count($Tokens)-1; $i >= 0 ; $i--){
    // Find the position in the string
    $start = strpos($thisString, $Tokens[$i]);
    // If it's present
    if ($start !== false){
      // Record position and token name
      $resultArray[$i]->start = $start;
      $resultArray[$i]->token = $Tokens[$i];
    }
  }
  
  // Sort in order of position
  sort($resultArray);

  // Go through the result array, getting the token values
  for ($i = 0; $i<count($resultArray); $i++){
    // Get the start point of the value
    $valueStart = $resultArray[$i]->start + strlen($resultArray[$i]->token) + 1;
    // Get the length of the value
    if ($i==(count($resultArray)-1)) {
      $output[$resultArray[$i]->token] = substr($thisString, $valueStart);
    } else {
      $valueLength = $resultArray[$i+1]->start - $resultArray[$i]->start - strlen($resultArray[$i]->token) - 2;
      $output[$resultArray[$i]->token] = substr($thisString, $valueStart, $valueLength);
    }      

  }

  // Return the ouput array
  return $output;
}
 

//***************************************************


// get country code name from code 3
function country_code_3_name($cd3) {
	global $database;
	$q  = "SELECT country_name FROM `#__vm_country` WHERE ( country_3_code = '" . $cd3 . "')";
	$database->setQuery( $q );
	$res = $database->loadResult();
	return $res;
}
?>
