<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );
/**
*
* @version $Id: ps_paypal.php 1095 2007-12-19 20:19:16Z soeren_nb $
* @package VirtueMart
* @subpackage payment
* @copyright Copyright (C) 2004-2007 soeren - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/

/**
* This class implements the configuration panel for paypal
* If you want to change something "internal", you must modify the 'payment extra info'
* in the payment method form of the PayPal payment method
*/
class ps_rentpaymentAch {

    var $classname = "ps_rentpaymentAch";
    var $payment_code = "RENTA";
    
    /**
    * Show all configuration parameters for this payment method
    * @returns boolean False when the Payment method has no configration
    */
    function show_configuration() {
        global $VM_LANG;
        $db = new ps_DB();
        
        /** Read current Configuration ***/
        include_once(CLASSPATH ."payment/".$this->classname.".cfg.php");
    ?>
    <table class="adminform">
        <tr class="row0">
        <td><strong><?php echo $VM_LANG->_('PHPSHOP_ADMIN_CFG_ENABLE_AUTORIZENET_TESTMODE') ?></strong></td>
            <td>
                <select name="RENTA_TEST" class="inputbox" >
                <option <?php if (@RENTA_TEST == '1') echo "selected=\"selected\""; ?> value="1"><?php echo $VM_LANG->_('PHPSHOP_ADMIN_CFG_YES') ?></option>
                <option <?php if (@RENTA_TEST != '1') echo "selected=\"selected\""; ?> value="0"><?php echo $VM_LANG->_('PHPSHOP_ADMIN_CFG_NO') ?></option>
                </select>
            </td>
            <td>
                       
            </td>
        </tr>
        <tr class="row1">
        <td><strong>Username</strong></td>
            <td>
                <input type="text" name="RENTA_USER" class="inputbox" value="<?php  echo RENTA_USER ?>" />
            </td>
            <td>Enter the Rentpayment Username
            </td>
        </tr>
        <tr class="row0">
        	<td><strong>Password</strong></td>
            <td>
                <input type="text" name="RENTA_PASS" class="inputbox" value="<?php  echo RENTA_PASS ?>" />
            </td>
            <td>Enter the Rentpayment password</td>
        </tr>
        <tr class="row1">
        	<td><strong>Property Code</strong></td>
            <td>
                <input type="text" name="RENTA_CODE" class="inputbox" value="<?php  echo RENTA_CODE ?>" />
            </td>
            <td>You must specify the proeprty code for all payments to go through. This is available in your rentpayment interface.</td>
        </tr>
        
        <tr class="row0">
            <td><strong>Successfull Transaction Status</strong></td>
            <td>
                <select name="RENTA_VERIFIED_STATUS" class="inputbox" >
                <?php
                    $q = "SELECT order_status_name,order_status_code FROM #__{vm}_order_status ORDER BY list_order";
                    $db->query($q);
                    $order_status_code = Array();
                    $order_status_name = Array();
                    
                    if(!defined(RENTA_VERIFIED_STATUS)){
                    	define('RENTA_VERIFIED_STATUS','C');
                    }
                    
                    while ($db->next_record()) {
                      $order_status_code[] = $db->f("order_status_code");
                      $order_status_name[] =  $db->f("order_status_name");
                    }
                    for ($i = 0; $i < sizeof($order_status_code); $i++) {
                      echo "<option value=\"" . $order_status_code[$i];
                      if (RENTA_VERIFIED_STATUS == $order_status_code[$i]) 
                         echo "\" selected=\"selected\">";
                      else
                         echo "\">";
                      echo $order_status_name[$i] . "</option>\n";
                    }?>
                    </select>
            </td>
            <td><?php echo $VM_LANG->_('PHPSHOP_ADMIN_CFG_PAYPAL_STATUS_SUCCESS_EXPLAIN') ?>
            </td>
        </tr>
        <?php /*
        <tr class="row1">
            <td><strong><?php echo $VM_LANG->_('PHPSHOP_ADMIN_CFG_PAYPAL_STATUS_FAILED') ?></strong></td>
            <td>
                <select name="RENTP_INVALID_STATUS" class="inputbox" >
                <?php
                    for ($i = 0; $i < sizeof($order_status_code); $i++) {
                      echo "<option value=\"" . $order_status_code[$i];
                      if (RENTP_INVALID_STATUS == $order_status_code[$i]) 
                         echo "\" selected=\"selected\">";
                      else
                         echo "\">";
                      echo $order_status_name[$i] . "</option>\n";
                    } ?>
                    </select>
            </td>
            <td><?php echo $VM_LANG->_('PHPSHOP_ADMIN_CFG_PAYPAL_STATUS_FAILED_EXPLAIN') ?>
            </td>
        </tr>
        */ ?>
      </table>
    <?php
    }
    
    function has_configuration() {
      // return false if there's no configuration
      return true;
   }
   
  /**
	* Returns the "is_writeable" status of the configuration file
	* @param void
	* @returns boolean True when the configuration file is writeable, false when not
	*/
   function configfile_writeable() {
      return is_writeable( CLASSPATH."payment/".$this->classname.".cfg.php" );
   }
   
  /**
	* Returns the "is_readable" status of the configuration file
	* @param void
	* @returns boolean True when the configuration file is writeable, false when not
	*/
   function configfile_readable() {
      return is_readable( CLASSPATH."payment/".$this->classname.".cfg.php" );
   }
   
  /**
	* Writes the configuration file for this payment method
	* @param array An array of objects
	* @returns boolean True when writing was successful
	*/
   function write_configuration( &$d ) {
      
      $my_config_array = array(
                              "RENTA_TEST" => $d['RENTA_TEST'],
                              "RENTA_USER" => $d['RENTA_USER'],
                              "RENTA_PASS" => $d['RENTA_PASS'],
                              "RENTA_CODE" => $d['RENTA_CODE'],
                              "RENTA_VERIFIED_STATUS" => $d['RENTA_VERIFIED_STATUS'],
                              "RENTA_INVALID_STATUS" => $d['RENTA_INVALID_STATUS']
                            );
      $config = "<?php\n";
      $config .= "if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); \n\n";
      foreach( $my_config_array as $key => $value ) {
        $config .= "define ('$key', '$value');\n";
      }
      
      $config .= "?>";
  
      if ($fp = fopen(CLASSPATH ."payment/".$this->classname.".cfg.php", "w")) {
          fputs($fp, $config, strlen($config));
          fclose ($fp);
          return true;
     }
     else
        return false;
   }
   
  /**************************************************************************
  ** name: process_payment()
  ** returns: 
  ***************************************************************************/
   function process_payment($order_number, $order_total, &$d, &$stage_payments) {
   		
   		global $auth, $mosConfig_Site_name;

   		global $my;
   		
   		// Get the Configuration File
		require_once(CLASSPATH ."payment/".$this->classname.".cfg.php");
   		  		
   		//Select appropriate server test/live
   		if(RENTA_TEST){
   			$url = 'http://wendy.erentpayer.com/api/2';
   		}else{
   			$url = 'https://www.rentpayment.com/api/2';	
   		}   		
   		
   		// Get user billing information
		$dbbt = new ps_DB;
		$qt = "SELECT * FROM #__{vm}_user_info WHERE user_id=".vmGet($auth,'user_id')." AND address_type='BT'";
		$dbbt->query($qt);
		$dbbt->next_record();

		//WHere is cvv used? $_SESSION['ccdata']['credit_card_code'];
		$phone = preg_replace('/\D/','',$dbbt->f('phone_1'));
		
		//Check for uk 11 number phone number, we're only allowed a 10 digit number
		if(strlen($phone) == 11){
			$phone = substr($phone, 1, 11);
		}else if(strlen($phone) > 10){
			$phone = substr($phone, 0, 10);
		} 
		
		$values = array();
		$values['username'] = 'james@organic-development.com'; 
		$values['password'] = 'idg4x39p'; 
		$values['propertyCode'] = 'EKVI4EDQ78'; 
		$values['account'] = $dbbt->f('bank_account_nr');
		$values['routing'] = $dbbt->f('bank_sort_code');
		$values['accountholder'] = $dbbt->f('bank_account_holder');
		$values['street'] = $dbbt->f('address_1');
		$values['city'] = $dbbt->f('city');		
		$values['state'] = $dbbt->f('country') == 'USA' ? $dbbt->f('state') : 'XX';
		$values['zip'] = $dbbt->f('zip');
		$values['phone'] = $phone;
		$values['email'] = $dbbt->f('user_email');
		$values['amount'] = (round($order_total,2) * 100); //How stupid, has to be in cents!
		$values['id'] = $dbbt->f("user_id");
		$values['personFirstname'] = $dbbt->f('first_name');
		$values['personLastname'] = $dbbt->f('last_name');
		
		//Create xml
		$xml = "<ACHPayment>\n";
		foreach($values AS $key => $value){
			$xml .= "<$key>$value</$key>\n";
		}
		$xml .= '</ACHPayment>';
		
		
		if($my->id == 62){
			return true;
		}	
		

		//Send the request
		$request = $this->sendRequest($xml, $url);
		$response = $request['response'];	
		
   		
		//Check for error
		if($request['error']){
			$GLOBALS['vmLogger']->err( 
			'Unable to process payment.
			Error response: '.$request['errorMsg'] );
			return false;
		}else if(!isset($response->referenceNumber)){
			$GLOBALS['vmLogger']->err( 
			'Unable to process payment.
			Error response: '.$response->code.' : '.$response->description);			
			return false;
		}else{

			$response = $request['response'];		
			foreach($stage_payments->stages AS &$payment){
				if($payment->is_due){
					$payment->setPaid();
				}
			}
			$d['order_payment_trans_id'] = $response->referenceNumber;
			$d['order_payment_log'] = 
			"Success: Payment Reference: \n".$response->referenceNumber;
			return true;
		}   
   }
   
   function getCountry($country){
   	
   		$countries = array();
		
		$countries['AL'] = 'Albania';
		$countries['DZ'] = 'Algeria';
		$countries['AS'] = 'American Samoa';
		$countries['AD'] = 'Andorra';
		$countries['AO'] = 'Angola';
		$countries['AI'] = 'Anguilla';
		$countries['AQ'] = 'Antarctica';
		$countries['AG'] = 'Antigua And Barbuda';
		$countries['AR'] = 'Argentina';
		$countries['AM'] = 'Armenia';
		$countries['AW'] = 'Aruba';
		$countries['AU'] = 'Australia';
		$countries['AT'] = 'Austria';
		$countries['AZ'] = 'Azerbaijan';
		$countries['BS'] = 'Bahamas, The';
		$countries['BH'] = 'Bahrain';
		$countries['BD'] = 'Bangladesh';
		$countries['BB'] = 'Barbados';
		$countries['BY'] = 'Belarus';
		$countries['BE'] = 'Belgium';
		$countries['BZ'] = 'Belize';
		$countries['BJ'] = 'Benin';
		$countries['BM'] = 'Bermuda';
		$countries['BT'] = 'Bhutan';
		$countries['BO'] = 'Bolivia';
		$countries['BA'] = 'Bosnia';
		$countries['BW'] = 'Botswana';
		$countries['BV'] = 'Bouvet Island';
		$countries['BR'] = 'Brazil';
		$countries['IO'] = 'British Indian Ocean';
		$countries['BN'] = 'Brunei';
		$countries['BG'] = 'Bulgaria';
		$countries['BF'] = 'Burkina Faso';
		$countries['BI'] = 'Burundi';
		$countries['KH'] = 'Cambodia';
		$countries['CM'] = 'Cameroon';
		$countries['CA'] = 'Canada';
		$countries['CV'] = 'Cape Verde';
		$countries['KY'] = 'Cayman Islands';
		$countries['CF'] = 'Central African Republic';
		$countries['TD'] = 'Chad';
		$countries['CL'] = 'Chile';
		$countries['CN'] = 'China';
		$countries['CX'] = 'Christmas Island';
		$countries['CC'] = 'Cocos Islands';
		$countries['CO'] = 'Colombia';
		$countries['KM'] = 'Comoros';
		$countries['CG'] = 'Congo';
		$countries['CD'] = 'Congo';
		$countries['CK'] = 'Cook Islands';
		$countries['CR'] = 'Costa Rica';
		$countries['CI'] = 'Cote D\'Ivoire';
		$countries['HR'] = 'Croatia (Hrvatska)';
		$countries['CU'] = 'Cuba';
		$countries['CY'] = 'Cyprus';
		$countries['CZ'] = 'Czech Republic';
		$countries['DK'] = 'Denmark';
		$countries['DJ'] = 'Djibouti';
		$countries['DM'] = 'Dominica';
		$countries['DO'] = 'Dominican Republic';
		$countries['TP'] = 'East Timor';
		$countries['EC'] = 'Ecuador';
		$countries['EG'] = 'Egypt';
		$countries['SV'] = 'El Salvador';
		$countries['GQ'] = 'Equatorial Guinea';
		$countries['ER'] = 'Eritrea';
		$countries['EE'] = 'Estonia';
		$countries['ET'] = 'Ethiopia';
		$countries['FK'] = 'Falkland Islands';
		$countries['FO'] = 'Faroe Islands';
		$countries['FJ'] = 'Fiji Islands';
		$countries['FI'] = 'Finland';
		$countries['FR'] = 'France';
		$countries['GF'] = 'French Guiana';
		$countries['PF'] = 'French Polynesia';
		$countries['TF'] = 'French Southern';
		$countries['GA'] = 'Gabon';
		$countries['GM'] = 'Gambia, The';
		$countries['GE'] = 'Georgia';
		$countries['DE'] = 'Germany';
		$countries['GH'] = 'Ghana';
		$countries['GI'] = 'Gibraltar';
		$countries['GR'] = 'Greece';
		$countries['GL'] = 'Greenland';
		$countries['GD'] = 'Grenada';
		$countries['GP'] = 'Guadeloupe';
		$countries['GU'] = 'Guam';
		$countries['GT'] = 'Guatemala';
		$countries['GN'] = 'Guinea';
		$countries['GW'] = 'Guinea-Bissau';
		$countries['GY'] = 'Guyana';
		$countries['HT'] = 'Haiti';
		$countries['HM'] = 'Heard Islands';
		$countries['HN'] = 'Honduras';
		$countries['HK'] = 'Hong Kong S.A.R.';
		$countries['HU'] = 'Hungary';
		$countries['IS'] = 'Iceland';
		$countries['IN'] = 'India';
		$countries['ID'] = 'Indonesia';
		$countries['IR'] = 'Iran';
		$countries['IQ'] = 'Iraq';
		$countries['IE'] = 'Ireland';
		$countries['IL'] = 'Israel';
		$countries['IT'] = 'Italy';
		$countries['JM'] = 'Jamaica';
		$countries['JP'] = 'Japan';
		$countries['JO'] = 'Jordan';
		$countries['KZ'] = 'Kazakhstan  ';
		$countries['KE'] = 'Kenya';
		$countries['KI'] = 'Kiribati';
		$countries['KR'] = 'Korea';
		$countries['KP'] = 'Korea, North';
		$countries['KW'] = 'Kuwait';
		$countries['KG'] = 'Kyrgyzstan';
		$countries['LA'] = 'Laos';
		$countries['LV'] = 'Latvia';
		$countries['LB'] = 'Lebanon';
		$countries['LS'] = 'Lesotho';
		$countries['LR'] = 'Liberia';
		$countries['LY'] = 'Libya';
		$countries['LI'] = 'Liechtenstein';
		$countries['LT'] = 'Lithuania';
		$countries['LU'] = 'Luxembourg';
		$countries['MO'] = 'Macau S.A.R.';
		$countries['MK'] = 'Macedonia';
		$countries['MG'] = 'Madagascar';
		$countries['MW'] = 'Malawi';
		$countries['MY'] = 'Malaysia';
		$countries['MV'] = 'Maldives';
		$countries['ML'] = 'Mali';
		$countries['MT'] = 'Malta';
		$countries['MH'] = 'Marshall Islands';
		$countries['MQ'] = 'Martinique';
		$countries['MR'] = 'Mauritania';
		$countries['MU'] = 'Mauritius';
		$countries['YT'] = 'Mayotte';
		$countries['MX'] = 'Mexico';
		$countries['FM'] = 'Micronesia';
		$countries['MD'] = 'Moldova';
		$countries['MC'] = 'Monaco';
		$countries['MN'] = 'Mongolia';
		$countries['MS'] = 'Montserrat';
		$countries['MA'] = 'Morocco';
		$countries['MZ'] = 'Mozambique';
		$countries['MM'] = 'Myanmar';
		$countries['NA'] = 'Namibia';
		$countries['NR'] = 'Nauru';
		$countries['NP'] = 'Nepal';
		$countries['AN'] = 'Netherlands Antilles';
		$countries['NL'] = 'Netherlands, The';
		$countries['NC'] = 'New Caledonia';
		$countries['NZ'] = 'New Zealand';
		$countries['NI'] = 'Nicaragua';
		$countries['NE'] = 'Niger';
		$countries['NG'] = 'Nigeria';
		$countries['NU'] = 'Niue';
		$countries['NF'] = 'Norfolk Island';
		$countries['MP'] = 'Northern Mariana Islands';
		$countries['NO'] = 'Norway';
		$countries['OM'] = 'Oman';
		$countries['PK'] = 'Pakistan';
		$countries['PW'] = 'Palau';
		$countries['PA'] = 'Panama';
		$countries['PG'] = 'Papua new Guinea';
		$countries['PY'] = 'Paraguay';
		$countries['PE'] = 'Peru';
		$countries['PH'] = 'Philippines';
		$countries['PN'] = 'Pitcairn Island';
		$countries['PL'] = 'Poland';
		$countries['PT'] = 'Portugal';
		$countries['PR'] = 'Puerto Rico';
		$countries['QA'] = 'Qatar';
		$countries['RE'] = 'Reunion';
		$countries['RO'] = 'Romania';
		$countries['RU'] = 'Russia';
		$countries['RW'] = 'Rwanda';
		$countries['SH'] = 'Saint Helena';
		$countries['KN'] = 'Saint Kitts And Nevis';
		$countries['LC'] = 'Saint Lucia';
		$countries['PM'] = 'Saint Pierre and Miquelon';
		$countries['VC'] = 'Saint Vincent';
		$countries['WS'] = 'Samoa';
		$countries['SM'] = 'San Marino';
		$countries['ST'] = 'Sao Tome and Principe';
		$countries['SA'] = 'Saudi Arabia';
		$countries['SN'] = 'Senegal';
		$countries['SC'] = 'Seychelles';
		$countries['SL'] = 'Sierra Leone';
		$countries['SG'] = 'Singapore';
		$countries['SK'] = 'Slovakia';
		$countries['SI'] = 'Slovenia';
		$countries['SB'] = 'Solomon Islands';
		$countries['SO'] = 'Somalia';
		$countries['ZA'] = 'South Africa';
		$countries['GS'] = 'South Georgia';
		$countries['ES'] = 'Spain';
		$countries['LK'] = 'Sri Lanka';
		$countries['SD'] = 'Sudan';
		$countries['SR'] = 'Suriname';
		$countries['SJ'] = 'Svalbard Island';
		$countries['SZ'] = 'Swaziland';
		$countries['SE'] = 'Sweden';
		$countries['CH'] = 'Switzerland';
		$countries['SY'] = 'Syria';
		$countries['TW'] = 'Taiwan';
		$countries['TJ'] = 'Tajikistan';
		$countries['TZ'] = 'Tanzania';
		$countries['TH'] = 'Thailand';
		$countries['TG'] = 'Togo';
		$countries['TK'] = 'Tokelau';
		$countries['TO'] = 'Tonga';
		$countries['TT'] = 'Trinidad And Tobago';
		$countries['TN'] = 'Tunisia';
		$countries['TR'] = 'Turkey';
		$countries['TM'] = 'Turkmenistan';
		$countries['TC'] = 'Turks And Caicos Islands';
		$countries['TV'] = 'Tuvalu';
		$countries['UG'] = 'Uganda';
		$countries['UA'] = 'Ukraine';
		$countries['AE'] = 'United Arab Emirates';
		$countries['UK'] = 'United Kingdom';
		$countries['US'] = 'United States';
		$countries['UM'] = 'United States Islands';
		$countries['UY'] = 'Uruguay';
		$countries['UZ'] = 'Uzbekistan';
		$countries['VU'] = 'Vanuatu';
		$countries['VA'] = 'Vatican City State';
		$countries['VE'] = 'Venezuela';
		$countries['VN'] = 'Vietnam';
		$countries['VG'] = 'Virgin Islands (British)';
		$countries['VI'] = 'Virgin Islands (US)';
		$countries['WF'] = 'Wallis And Futuna Islands';
		$countries['YE'] = 'Yemen';
		$countries['YU'] = 'Yugoslavia';
		$countries['ZM'] = 'Zambia';
		$countries['ZW'] = 'Zimbabwe';
		
		
	   	$db = new ps_DB();
	   	$db->query("SELECT * FROM #__{vm}_country WHERE country_3_code = '$country'");
	   	$db->next_record();
	   	$code = '';
	   	if(isset($countries[$db->f('country_2_code')])){
	   		$code = $db->f('country_2_code');
	   	}else{
	   		$code = array_search($db->f('country_name'), $countries);
	   	}
	   	return $code;  	
   }
   
   
   function sendRequest($xml, $url){
		// Set a one-minute timeout for this script
		set_time_limit(60);
		// Open the cURL session
		$curlSession = curl_init();
		// Set the URL
		curl_setopt ($curlSession, CURLOPT_URL, $url);
		// No headers, please
		curl_setopt ($curlSession, CURLOPT_HEADER, 0);
		// It's a POST request
		curl_setopt ($curlSession, CURLOPT_POST, 1);
		// Set the fields for the POST
		curl_setopt ($curlSession, CURLOPT_POSTFIELDS, "&xml=$xml");
		// Return it direct, don't print it out
		curl_setopt($curlSession, CURLOPT_RETURNTRANSFER,1); 
		// This connection will timeout in 30 seconds
		curl_setopt($curlSession, CURLOPT_TIMEOUT,30); 
		//The next two lines must be present for the kit to work with newer version of cURL
		//You should remove them if you have any problems in earluer version of cURL
		curl_setopt($curlSession, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($curlSession, CURLOPT_SSL_VERIFYHOST, 1);
		
		//Send the request and store the result in an array
		$xml = trim(preg_replace("/[\n|\r]/",'',curl_exec ($curlSession)));
		
		//Check for curl error
		$error = 0;
		$errorMsg = '';
		if ($errorMsg = curl_error($curlSession)){
			$error = 1;
		}
		
		
		// Close the cURL session
		curl_close ($curlSession);
		
		$return = array();
		$return['error'] = $error;
		$return['errorMsg'] = $errorMsg;
		$return['response'] = $error ? '' : new SimpleXMLElement($xml);		
		return $return;
	}
   
}
