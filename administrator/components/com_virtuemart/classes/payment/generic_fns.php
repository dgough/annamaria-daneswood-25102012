<?php
// *************************************************************************
// *                                                                       *
// * Ecom Solution Generic Functions for Payments Modules                  *
// * Copyright (c) 2007 Ecom Solution LTD. All Rights Reserved             *
// * Email: techsupport@e-commerce-solution.co.uk                          *
// * Website: http://www.e-commerce-solution.co.uk                         *
// * Script Name: generic_fns.php                                          *
// * Release Date: 01.03.2008                                              *
// * Version 1.2                                                           *
// *                                                                       *
// *************************************************************************
// *                                                                       *
// * This software is furnished under a license and may be used and copied *
// * only  in  accordance  with  the  terms  of such  license and with the *
// * inclusion of the above copyright notice.  This software  or any other *
// * copies thereof may not be provided or otherwise made available to any *
// * other person.  No title to and  ownership of the  software is  hereby *
// * transferred.                                                          *
// *                                                                       *
// * You may not reverse  engineer, decompile, defeat  license  encryption *
// * mechanisms, or  disassemble this software product or software product *
// * license.  Ecom Solution  may  terminate  this license  if you fail to *
// * comply with any of the terms and conditions set forth in our end user *
// * license agreement (EULA).  In such event,  licensee  agrees to return *
// * licensor  or destroy  all copies of software  upon termination of the *
// * license.                                                              *
// *                                                                       *
// *                                                                       *
// *************************************************************************
// 

// GENERIC FUNCTIONS
global $mosConfig_live_site;
$img_0 = '<img border="0" hspace="4" src="' . $mosConfig_live_site.'/images/blank.png" width="12" height="12" align="absmiddle">'; 
$img_ok = '<img border="0" hspace="4" src="' . $mosConfig_live_site.'/images/tick.png" width="12" height="12" align="absmiddle">'; 
$img_no = '<img border="0" hspace="4" src="' . $mosConfig_live_site.'/images/publish_x.png" width="12" height="12" align="absmiddle">'; 
$img_edit = '<img title="Edit" border="0" hspace="2" src="' . $mosConfig_live_site.'/images/M_images/edit.png" width="16" height="16" align="absmiddle">'; 
$img_new = '<img title="New" border="0" hspace="2" src="' . $mosConfig_live_site.'/images/M_images/new.png" width="16" height="16" align="absmiddle">'; 
$img_web = '<img title="Domains" border="0" hspace="2" src="' . $mosConfig_live_site.'/images/M_images/weblink.png" width="16" height="16" align="absmiddle">'; 
$img_eml = '<img title="Email" border="0" hspace="2" src="' . $mosConfig_live_site.'/images/M_images/emailButton.png" width="16" height="16" align="absmiddle">'; 
$img_mnu = '<img border="0" hspace="8" vspace="5" src="' . $mosConfig_live_site . '/administrator/images/expandall.png" align="absmiddle" width="13" height="13">';
$img_copy = '<img title="Copy" border="0" hspace="2" src="' . $mosConfig_live_site . '/components/com_virtuemart/shop_image/ps_image/copy_f2.gif" align="absmiddle" width="19" height="19">';
$img_info = '<img title="Info" border="0" hspace="2" src="' . $mosConfig_live_site . '/components/com_virtuemart/shop_image/ps_image/log_info.png" align="absmiddle" width="22" height="22">';
$img_file = '<img border="0" src="' . $mosConfig_live_site . '/administrator/images/download_f2.png" align="absmiddle" width="32" height="32">';

function showYesNoImg($bln) { 
	global $img_ok, $img_no;
	if ($bln == '1') 
	  $img = $img_ok;
	else
	  $img = $img_no;
	echo $img;
} 

//***************************************************

// function used as an error handler 
function option_explicit($errno, $errstr, $errfile, $errline) {
  print "<br /><b>Fatal error</b>: $errstr in <b>$errfile</b>";
  print " on line <b>$errline</b><br />";	 
  exit();
}

function on_error_resume_next($errno, $errstr, $errfile, $errline) {
  global $myerr;
  $myerr = "$errstr Line: $errline";
}

// revert error handlng to default 
function default_handler($errno, $errstr, $errfile, $errline) {
  return false;
}

function error_display($errno, $errstr, $errfile, $errline) {
  print "<br />Error: $errstr ";	
}

//***************************************************

// is express checkout enabled?
function is_payment_enabled($payment_method_code) {
  global $database;
  $q = "SELECT payment_enabled FROM #__vm_payment_method WHERE payment_method_code='$payment_method_code'"; 
  $database->setQuery( $q );
  $rows = $database->loadObjectList();
  $ret = '';
  if ($rows) {
    $row = $rows[0];
    $ret = $row->payment_enabled; 
  }
  return $ret; 
}

//***************************************************

function get_payment_method_id_from_code($payment_method_code) {
  global $database;
  $q = "SELECT * FROM #__vm_payment_method WHERE payment_method_code='$payment_method_code'"; 
  $database->setQuery( $q );
  $rows = $database->loadObjectList();
  
  $ret = 0;
  if($rows) {
    $row = $rows[0];
    $ret = $row->payment_method_id; 
  }
  return $ret; 
}


//***************************************************

// request 
function get_req($v){
  return Trim(get_query($v) . post($v));
}

//***************************************************

// request querystring
function get_query($var){
  $val = '';
  if (isset($_GET[$var])) {
     $val = $_GET[$var];
  } 
  if (isset($_GET[strtoupper($var)])) {
     $val = $_GET[strtoupper($var)];
  } 
  if (isset($_GET[strtolower($var)])) {
     $val = $_GET[strtolower($var)];
  } 
  return Trim($val);
}

//***************************************************

// request post
function post($var){
  $val = '';
  $chk_length = 0;
  $possible_array ='';

  // was it a get request?
  if (strtoupper($_SERVER['REQUEST_METHOD']) != 'GET') {
        
      if (isset($_POST[$var])) {
        $possible_array = $_POST[$var];
        $chk_length = count($possible_array);
      }

      // is it an arrayc?
      if (is_array($possible_array)) {

        // construct comma delimited string
        for ($i = 0; $i < $chk_length; $i++) {

          // add a comma if not first item
          if (Trim($val) != '') $val .= ',';
          $val .= $possible_array[$i];
        } 

      } else {
        // is not array 
        $val = $possible_array;
      }
      
  } // get
  return Trim($val);
}


//***************************************************

// strip chars
function removechars($s) {
  $r = '';
  for ($i=0;$i<strlen($s);$i++) {
    $c = substr($s, $i, 1);
    if ((is_numeric($c)) || ($c == '.'))
      $r .= $c;
  }
  return $r;
}


//***************************************************

function server($var){
  $val = '';
  if (isset($_SERVER[$var])) {
     $val = $_SERVER[$var];
   }
  return Trim($val);
}

//***************************************************

function setsession($ky, $val) {
  $_SESSION[$ky] = $val;
}  

//***************************************************

// session
function session($var){
  $val = '';
  if (isset($_SESSION[$var])) {
     $val = $_SESSION[$var];
   }
  if (isset($_SESSION[strtoupper($var)])) {
     $val = $_SESSION[strtoupper($var)];
  } 
  return Trim($val);
}

//***************************************************

// file_get_contents replacement
// file_get_contents is 4.0.0 - 4.3.0 and 5.0.0 +

function file_get_contents1($f) {
  if (file_exists($f)) {  
    return implode('', file($f));
  } else {
    // ("File does not exist!");
  }
}

//***************************************************

// write string to file

function write2file($doc, $outputstring ){
  write2fileMode($doc, $outputstring, "w");
}

//***************************************************


// write string to file with Mode

function write2fileMode($doc, $outputstring, $for_mode) {
  if ($doc == '') return;
  
  // Set file for opening
  $fp = fopen($doc, $for_mode);
  // Check if can write to file
  if (!$fp) {
     print "Could not open desired file for writing";
  } else {
     fwrite($fp, $outputstring);
  }
  // Close the written file
  fclose($fp);    
}


//***************************************************

// get a value from array
function getArrayValue($prms, $var){
  $prms_lower = array_change_key_case($prms);
  $val = '';
  if (isset($prms_lower [ strtolower($var) ] )) {
    $val = $prms_lower[strtolower($var)];
  } 
  return Trim($val);
}

//***************************************************

function KeyPairtoArray($str) {
	$result = $str;
 	$ary = array();
	while(strlen($result)){
		// name
		$keypos= strpos($result,'=');
		$keyval = substr($result,0,$keypos);
		// value
		$valuepos = strpos($result,'&') ? strpos($result,'&'): strlen($result);
		$valval = substr($result,$keypos+1,$valuepos-$keypos-1);
		// decoding the respose
		$ary[$keyval] = $valval;
		$result = substr($result,$valuepos+1,strlen($result));
	}
	return $ary;
}

//***************************************************

function ccsession($var){
  $val = '';
  if (isset($_SESSION['ccdata'][$var])) {
     $val = $_SESSION['ccdata'][$var];
   }
  return Trim($val);
}

//***************************************************

// get country name from code  3
function country_3_name($cd3) {
	global $database; 
	$q  = "SELECT country_name FROM `#__vm_country` WHERE ( country_3_code = '" . $cd3 . "')";
	$database->setQuery( $q );
	$res = $database->loadResult();
	return $res;
}

//***************************************************

// get country code 3 from 2
function country_code_2_3($cd) {
	global $database;
	$q  = "SELECT country_3_code FROM `#__vm_country` WHERE ( country_2_code = '" . $cd . "')";
	$database->setQuery( $q );
	$res = $database->loadResult();
	return $res;
}

//***************************************************

// get country code 2 from 3
function country_code_3_2($cd) {
	global $database; 
	$q  = "SELECT country_2_code FROM `#__vm_country` WHERE ( country_3_code = '" . $cd . "')";
	$database->setQuery( $q );
	$res = $database->loadResult();
	return $res;
}

//***************************************************


function myCopy( $srcfolder, $strdestfolder, $aryfile){

  global $mosConfig_absolute_path;
  $Installer = new mosInstaller();
    
  $copy_str = '';
  $file = array($aryfile);
  
  $src = $mosConfig_absolute_path . $srcfolder .'/';
  $strdest1 = $mosConfig_absolute_path . $strdestfolder;
  
  // already exists?
  if (file_exists($strdest1 . $file[0])){    
    // copy to backup folder
    $Installer->copyFiles( $strdest1, $src . 'backup/', $file, true ); 
      $copy_str .= 'File backed up: ';   
      $copy_str .= $strdestfolder . $file[0];  
      $copy_str .= "\n";    
  } 
  
  if (file_exists($src)){  
    // copy to folder
    $Installer->copyFiles($src, $strdest1, $file, true );
    
    if (file_exists($strdest1 . $file[0])){    
      $copy_str .= 'File copied to: ';   
      $copy_str .= $strdestfolder . $file[0]; 
      $copy_str .= "\n";    
    } else {
      $copy_str .= 'File could not be copied to: '; 
      $copy_str .= $strdest1 . $file[0];   
      $copy_str .= "\n";    
    }
    
  } else {
    $copy_str .= 'Source file does not exist: '; 
    $copy_str .= $srcfolder . $file[0];   
    $copy_str .= "\n";    
  }
  return $copy_str;  
}

// curl fn
function doCurlhttp($method, $url, $port, $vars) {
  if (trim($method) == '') return;
  if (trim($url) == '') return;

  $ch = curl_init();
  if($ch) {
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_PORT, $port);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_USERAGENT, "EcomSolutionHTTPRequest");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.txt');
    curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie.txt');
    if ($method == 'POST') {
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);
    }
    $data = curl_exec($ch);
    curl_close($ch);
    if ($data) { 
        return $data;
    } else {
        return curl_error($ch);
    }
  }
}

function str_to_xml_params( $res_array ){
   // is it xml?
  $xml_parser = xml_parser_create();
  xml_parse_into_struct($xml_parser, $res_array, $vals, $index);
  xml_parser_free($xml_parser);

  // put into arry
  $params = array();
  $i = 0;
  foreach ($vals as $xml_elem) {
    if ($xml_elem['type'] == 'complete') {
        $params[trim($xml_elem['tag'])] = isset($xml_elem['value'])? trim($xml_elem['value']): '';
    } 
  }
  
   return $params;
}

// remove trailing slash 

function remove_trailing_slash($p) {
if (substr($p, -1, 1) == '/')  
  $p = substr($p, 0, -1); 
return $p;
}


// convert array
// to string key=value&key2=test
function ArraytoKeyPair($ary) {
  $kr ='';
  foreach($ary as $k=>$v)
    $kr .=  '&' . $k . '=' . urlencode($v);
    //$kr .=  '&' . $k . '=' . trim($v);
  return substr($kr, 1);
}

function getInput() {
$q = isset($_SERVER['QUERY_STRING']) ? trim($_SERVER['QUERY_STRING']) : '';
if ($q == '')
  return getRawPost();
else
  return $q;
}

function getRawPost() {
  $output = '';
  $fp = fopen("php://input", "r");  

  // while content exists, keep retrieving in chunks
  while( !feof( $fp ) ) {
        $output .= fgets( $fp, 1024);
  }
  // close the socket connection
  fclose( $fp);

  // return the response
  return $output;
}

function CheckAccess($Allowed) {
  global $your_ip;
  
  $access_ok = 0;
  foreach ($Allowed as $fld=>$value) {
    $value = str_replace('.*', '', $value);
    if (strpos($your_ip, $value) !== false ){
      $access_ok = $value;
    }
  }
     
  // return the ip if found otherwise 0
  return $access_ok;  
}

function show_db_err() {
	global $database; 
	if ($database->getErrorNum()) {
		echo $database->getQuery();
		echo $database->stderr();
		return false;
	}
}

function options_from_query($q, $s) {
	global $database;
	$str = '';

	$database->setQuery( $q ); 
	//echo($database->getQuery( ) ); 
	$res = $database->loadObjectList();
	show_db_err();
		
    if(is_array($res)) {
	foreach($res as $i=>$j)  {
	  $str .= '<option ';
	  // must be a better way than this to show first 2 props of stdClass?
	  $z = 0;
	  foreach($j as $k=>$v)  {
	    if ($z==0) {
	      if($v == $s) $str .= ' selected ';
	      $str .= ' value="' . $v . '">';		  
		} else  {
	      $str .= $v . ' ';	
		}
		$z++;
	  }
	  $str .= ' </option>';	  
	  	 
	}
	}
	return $str;
}

function ShortenString($s, $c)	 {
    $ret = '';
	$ret = substr($s, 0, $c);
    if (strlen($s) > $c) $ret .= "...";
    return $ret;
}

function defconn(){
  global $mosConfig_host, $mosConfig_user, $mosConfig_password, $mosConfig_db;
  $joomla_link=mysql_connect($mosConfig_host, $mosConfig_user, $mosConfig_password);
  mysql_select_db($mosConfig_db, $joomla_link);
}
	
// is this mysql server version x?	
function is_mysqlv($x) {
	$mysqlversion = mysql_get_server_info();
    $mysqlv = substr($mysqlversion, 0, strpos($mysqlversion, "-"));
	return (substr($mysqlv, 0, 1) == $x);
}


function list_order_status($s){
  $q = "SELECT order_status_code, order_status_name FROM #__vm_order_status ORDER BY list_order";
  return options_from_query($q, $s);
}	

function getVirtueMartVersion() { 
	global $mosConfig_absolute_path;
	$vm_version = '';	
	$vm_vsn_file = $mosConfig_absolute_path.'/administrator/components/com_virtuemart/version.php';
	$vm = file_exists( $vm_vsn_file );  
	if ($vm) {
		require_once($vm_vsn_file); 
		$vm_version = $VMVERSION->RELEASE . " " . $VMVERSION->DEV_STATUS . " [". $VMVERSION->CODENAME . "]";
	}
    return $vm_version;
}
	
function ShowVersionInfoPanel() {
$str = 'For: ' .  THIS_VERSION_FOR . '<br>
&copy; ' .  THIS_COPYRIGHT . '<br>
Email: ' .  THIS_SUPPORT_EMAIL . '<br>
Release Date: ' .  THIS_VERSION_DATE . '<br>
Version: ' .  THIS_VERSION . '<br>';
return $str; 
} 

// get an vm count from db
function get_vm_count_tbl($tbl)  {
	global $database;
	$sql = "SELECT COUNT(*) FROM " . $tbl . "; ";
	$database->setQuery( $sql );
	return $database->loadResult();
} 
?>