<?php

global $calendarCount;

if(!$calendarCount) $calendarCount = 0;

class ps_calendar {
	
	var $calendarCount;
	var $ajax = 0;	
	var $onLoad = 0;
	var $monthFormat = 1; 
	var $reference = '';
	var $dateFrom = '';
	var $dateTo = '';
	var $popup = 0;
	var $prop_id = 0;
	
	function __construct($reference = '',$prop_id, $popup = 0){
		global $mosConfig_absolute_path, $calendarCount, $my, $option, $sess, $VM_LANG;

		require_once(dirname(__FILE__)."/../../../../components/com_virtuemart/virtuemart_parser.php");		
		$VM_COMPONENT_NAME = VM_COMPONENT_NAME;
		if(!$reference) $reference = "vb_cal_$calendarCount";
		$this->reference 	= $reference;	
		$this->prop_id 		= $prop_id;	
		$this->popup 		= $popup;
		$this->calendarCount = $calendarCount;
		$calendarCount++;		
		$VM_LANG->load('calendar');
	}
	
	
	function setScripts($addToHead = 0){
		global $vm_mainframe, $option, $mosConfig_live_site, $mainframe;
		
		if(!defined('_VM_CALENDAR_CALLED')){
			if( $addToHead ) :
				$mainframe->addCustomHeadTag( vmCommonHTML::scriptTag( "$mosConfig_live_site/components/com_virtuemart/js/calendar/dhtmlSuite-common.js") );
				$mainframe->addCustomHeadTag( vmCommonHTML::scriptTag( "$mosConfig_live_site/components/com_virtuemart/js/calendar/dhtmlSuite-calendar.js") );
				$mainframe->addCustomHeadTag( vmCommonHTML::scriptTag( "$mosConfig_live_site/components/com_virtuemart/js/calendar/calendar_initialise.js") );			
				$mainframe->addCustomHeadTag( vmCommonHTML::linkTag(VM_THEMEURL."templates/calendar/css/calendar.css" ));
			
			else :
				echo vmCommonHTML::scriptTag( "$mosConfig_live_site/components/com_virtuemart/js/calendar/dhtmlSuite-common.js");
				echo vmCommonHTML::scriptTag( "$mosConfig_live_site/components/com_virtuemart/js/calendar/dhtmlSuite-calendar.js");
				echo vmCommonHTML::scriptTag( "$mosConfig_live_site/components/com_virtuemart/js/calendar/calendar_initialise.js");			
				$mainframe->addCustomHeadTag( vmCommonHTML::linkTag(VM_THEMEURL."templates/calendar/css/calendar.css" ));
			
			endif;
			
			define('_VM_CALENDAR_CALLED',1);
		}		
		//if($addToHead) $vm_mainframe->close();
	}
	
	function getAjaxCalendar($bookable = 1){

		$ps_booking = new ps_booking();
		//Restore booking session
		$ps_booking->restoreSession();
		
		if(!$this->reference) $this->reference = "vm_cal_instance_$this->calendarCount";
		$this->setScripts(1);	
		$this->ajax = 1;
		
		//Read the dates from the request array if set
		$this->dateFrom 	= vmGet($_REQUEST,'dateFrom');
		$this->dateFrom 	= is_array($this->dateFrom) ? $this->dateFrom['y'].'-'.$this->dateFrom['m'].'-'.$this->dateFrom['d'] : $this->dateFrom;		
		$this->dateTo 		= vmGet($_REQUEST,'dateTo');
		$this->dateTo 		= is_array($this->dateTo) ? $this->dateTo['y'].'-'.$this->dateTo['m'].'-'.$this->dateTo['d'] : $this->dateTo;
		
		//Set the from and to dates
		$this->dateFrom 	= $this->dateFrom != '' ? $this->dateFrom : ($ps_booking->dateFrom != '' ? $ps_booking->dateFrom : vmGet($_REQUEST,'dateFrom'));
		$this->dateTo 		= $this->dateTo != '' ? $this->dateTo : ($ps_booking->dateTo != '' ? $ps_booking->dateTo : vmGet($_REQUEST,'dateFrom'));	
		
		$tpl = vmTemplate::getInstance();	
		$tpl->set_vars(
					array(  'dateFrom' => $this->dateFrom,
							'dateTo' => $this->dateTo,
							'calendar' => $this,
							'popup' => $this->popup,
							'property_id' => $this->prop_id,
							'reference' => $this->reference,
							'bookable' => $bookable
						)
					);
		return $tpl->fetch('calendar/calendar.ajax.tpl.php');
	}
	
	
		
	function renderInput($name, $value = '', $initialDate = '', $showIcon = 1){
		
		if(is_array($value)){
			$value = sprintf('%s-%s-%s',$value['y'],$value['m'],$value['d']);
		}
		$date = strtotime($value);			
		if(!$date){
			$date = '';
		}
		
		$return = '';	
		$return .= ps_calendar::list_days($name, $date);
		$return .= ps_calendar::list_month($name, $date, '');  
		$return .= ps_calendar::list_year($name, $date, '', date('Y')+2);
		if($showIcon) $return .=  "<img style='cursor: pointer' id='{$this->reference}-$name-icon' class='$name' src='".VM_THEMEURL."templates/calendar/images/icon.png' alt='Open Calendar' />";	
		
		return $return;
	}
	

	function getCalendar(){
		global $mosConfig_lang, $database;
		
		//Get the furthest date from the seasons table
		$db = new ps_DB();
		$db->query("SELECT IF(s.season_id, s.dateTo, r.dateTo) AS date
					FROM #__{vm}_seasons_rates AS r
					LEFT JOIN #__{vm}_seasons AS s ON s.season_id = r.season_id ".
					($this->prop_id ? "WHERE r.property_id = '$this->prop_id' " : '').
					"ORDER BY date DESC 
					LIMIT 1");				
		$db->next_record();		
		$date = $db->f('date');
		
		$database->setQuery("SELECT iso FROM #__languages WHERE shortcode = '$mosConfig_lang'");
		$lang = $database->loadResult();
		$lang = $lang ? $lang : 'en';
		
		//Create end date
		$endDate = $date ? strtotime($date) : strtotime('+2 years');
		$jsEndDate = sprintf('{year: %s, month: %s, day: %s}',date('Y',$endDate),date('m',$endDate),date('d',$endDate));
				
		if(!$this->popup) echo "<div id='{$this->reference}_cal'></div>";
		?>
		<script type="text/javascript">			
		//<!--
		<?php
		
		// Organic Mod: Check if logged in to back end, set $isAdmin to true to stop date popup on booking page
		$isAdmin = false;
		if( strpos($_SERVER['REQUEST_URI'], 'administrator/') ) $isAdmin = true;
		
		if($this->popup){
			?>			
			var <?php echo $this->reference ?>_popup;		
			window.addEvent('<?php echo $this->onLoad ? 'load' : 'domready' ?>', init_<?php echo $this->reference ?>PopupCal);
			
			function init_<?php echo $this->reference ?>PopupCal() {
				
				//Remove calendar if it already exists
				if(!<?php echo $this->reference ?>_popup){
				
					//Initialise calendar
					<?php echo $this->reference ?>_popup = new vbCalendar(
					{
						reference: '<?php echo $this->reference ?>',
						popup: 1, 
						endDate: <?php echo $jsEndDate ?>,
						prop_id: '<?php echo $this->prop_id ?>',
						language: '<?php echo $lang ?>'
						<?php // Organic Mod: this is here to stop the calendar popping up when viewing a booking overview
						if ( $isAdmin ) echo ',preUpdate: false';
						// end if?>
					})
				}else{
					<?php echo $this->reference ?>_popup.initialize();
				}
				<?php echo $this->getInvalidDateRanges($this->reference.'_popup'); ?>
			}			
			
		<?php
		} else {
		?>
			var <?php echo $this->reference ?>_inline;
			window.addEvent('<?php echo $this->onLoad ? 'load' : 'domready' ?>', init_<?php echo $this->reference ?>Cal);
			
			function init_<?php echo $this->reference ?>Cal() {
				
				//Remove calendar if it already exists
				if(!<?php echo $this->reference ?>_inline){
				
					//Initialise calendar
					<?php echo $this->reference ?>_inline = new vbCalendar(
					{
						reference: '<?php echo $this->reference ?>',
						popup: 0, 
						endDate: <?php echo $jsEndDate ?>,
						inlineRef:'<?php echo $this->reference ?>_cal',
						prop_id: '<?php echo $this->prop_id ?>',
						language: '<?php echo $lang ?>'
						<?php // Organic Mod: this is here to stop the calendar popping up when viewing a booking overview
						if ( $isAdmin ) echo ',preUpdate: false';
						// end if?>
					})
				}else{
					<?php echo $this->reference ?>_inline.initialize();
				}
				<?php echo $this->getInvalidDateRanges($this->reference.'_inline'); ?>
			}						
        <?php
		}
		?>		
		//-->
		</script>
		<?php
		
	}	

	
	function getInvalidDateRanges($ref){
		
		$return = '';
		if($this->prop_id){
			
			$db = new ps_DB();
			
			$q = "SELECT booking.arrival, booking.departure FROM #__{vm}_order_booking AS booking 
				LEFT JOIN #__{vm}_orders AS `order` ON order.order_id = booking.order_id
				WHERE (`arrival` >= now() OR `departure` >= now())
				AND booking.property_id = '$this->prop_id'
				AND order.order_status IN ('C')";
			$db->query($q);
			
			while($db->next_record()){
				$this->dateFrom = strtotime($db->f('arrival'));
				$this->dateTo = strtotime($db->f('departure'));
				$d1 = date('d',$this->dateFrom);
				$m1 = date('m',$this->dateFrom);
				$y1 = date('Y',$this->dateFrom);
				$d2 = date('d',$this->dateTo);
				$m2 = date('m',$this->dateTo);
				$y2 = date('Y',$this->dateTo);
				
				$return .= "$ref.addInvalidDateRange({year: $y1,month: $m1,day: $d1},{year: $y2,month: $m2,day: $d2});\n";
			}	
			$return .= "$ref.updateCalendar();\n";
		}	
		return $return;		
	}
	
	
	/**
	 * Creates an drop-down list with numbers from 1 to 31 or of the selected range
	 *
	 * @param string $list_name The name of the select element
	 * @param string $selected_item The pre-selected value
	 */
	function list_days($list_name,$selected_item='', $start=null, $end=null, $extras = '') {
		global $VM_LANG, $mosConfig_lang;
		$start = $start ? $start : 1;
		$end = $end ? $end : $start + 30;
		$list = array(''=>$VM_LANG->_("DAY"));

		for ($i=$start; $i<=$end; $i++) {
			$k = (strlen($i) == 1) ? "0$i" : $i;
			$list[$k] = $i;
		}
		
		if($selected_item){
			$selected_item = date('d',$selected_item);
		}else{
			$selected_item = '';
		}
		
		$extras .= " id='$this->reference-$list_name-d'";
		$list_name .= "[d]";
		return ps_html::selectList($list_name, $selected_item, $list,1,'',$extras);
	}
	
	
	/**
	 * Creates a Drop-Down List for the 12 months in a year
	 *
	 * @param string $list_name The name for the select element
	 * @param string $selected_item The pre-selected value
	 * 
	 */
	function list_month($list_name, $selected_item="", $extras = '') {
		global $VM_LANG;
		
		switch($this->monthFormat){
			
			default:
			case 0:
			$list = array(''=> $VM_LANG->_("MONTH"),
			"01" => '01',
			"02" => '02',
			"03" => '03',
			"04" => '04',
			"05" => '05',
			"06" => '06',
			"07" => '07',
			"08" => '08',
			"09" => '09',
			"10" => '10',
			"11" => '11',
			"12" => '12');
			break;
			
			case 1:
			$list = array(''=>$VM_LANG->_("MONTH"),
			"01" => $VM_LANG->_('JAN'),
			"02" => $VM_LANG->_('FEB'),
			"03" => $VM_LANG->_('MAR'),
			"04" => $VM_LANG->_('APR'),
			"05" => $VM_LANG->_('MAY'),
			"06" => $VM_LANG->_('JUN'),
			"07" => $VM_LANG->_('JUL'),
			"08" => $VM_LANG->_('AUG'),
			"09" => $VM_LANG->_('SEP'),
			"10" => $VM_LANG->_('OCT'),
			"11" => $VM_LANG->_('NOV'),
			"12" => $VM_LANG->_('DEC'));
			break;	
				
			case 2:
			$list = array(''=>$VM_LANG->_("MONTH"),
			"01" => $VM_LANG->_('JAN_SHORT'),
			"02" => $VM_LANG->_('FEB_SHORT'),
			"03" => $VM_LANG->_('MAR_SHORT'),
			"04" => $VM_LANG->_('APR_SHORT'),
			"05" => $VM_LANG->_('MAY_SHORT'),
			"06" => $VM_LANG->_('JUN_SHORT'),
			"07" => $VM_LANG->_('JUL_SHORT'),
			"08" => $VM_LANG->_('AUG_SHORT'),
			"09" => $VM_LANG->_('SEP_SHORT'),
			"10" => $VM_LANG->_('OCT_SHORT'),
			"11" => $VM_LANG->_('NOV_SHORT'),
			"12" => $VM_LANG->_('DEC_SHORT'));
			break;	
			
		}

		if($selected_item){
			$selected_item = date('m',$selected_item);
		}else{
			$selected_item = '';
		}
		
		$extras .= " id='$this->reference-$list_name-m'";
		$list_name .= "[m]";
		return ps_html::selectList($list_name, $selected_item, $list,1,'',$extras);
	}

	/**
	 * Creates an drop-down list with years of the selected range or of the next 7 years
	 *
	 * @param string $list_name The name of the select element
	 * @param string $selected_item The pre-selected value
	 */
	function list_year($list_name,$selected_item='', $start=null, $end=null, $extras = '') {
		global $VM_LANG;
		
		if($selected_item){
			$selected_item = date('Y',$selected_item);
		}else{
			$selected_item = '';
		}
		
		$from = !empty($selected_item) ? $selected_item : null;		
		$start = !empty($start) ? $start : (!empty($from) ? $from : date('Y'));
		$end = !empty($end) ? $end : $start + 7;
		
		$list = array('' => $VM_LANG->_("YEAR"));
		for ($i=$start; $i<=$end; $i++) {
			$list[$i] = $i;
		}		
		
		$extras .= " id='$this->reference-$list_name-y'";
		$list_name .= "[y]";
		return ps_html::selectList($list_name, $selected_item, $list,1,'',$extras);
		
	}
}
?>