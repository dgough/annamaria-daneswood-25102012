<?php
if( ! defined( '_VALID_MOS' ) && ! defined( '_JEXEC' ) )
die( 'Direct Access to ' . basename( __FILE__ ) . ' is not allowed.' ) ;


/**
 * The stage payment object
 *
 */
class ps_stage_payments{

	var $payment_id;
	var $order_id;
	var $payment_method_id;
	var $payment_amount;
	var $payment_due;
	var $payment_submitted;
	var $payment_cleared;
	var $payment_stage;	
	var $due;					
	var $is_due;	
		
	function __construct($due, $amount, $stage, $submitted = '0000-00-00', $cleared = '0000-00-00'){
		
		$amount = $amount <= 0 ? 0 : $amount;
		$this->due = $due;		
		$this->payment_due = date('Y-m-d',$due);
		$this->payment_amount = $amount;
		$this->payment_stage = $stage;
		$this->payment_submitted = !$submitted || $submitted == '0000-00-00' ? '' : $submitted;
		$this->payment_cleared = !$cleared || $cleared == '0000-00-00' ? '' : $cleared;
		$this->is_due = $due <= strtotime(date('Y-m-d')) && !$this->payment_cleared;
		if(!$amount) $this->setPaid();
	}
	
	function setDue($date){
		$this->due = $date;
		$this->payment_due = date('Y-m-d',$this->due);
		$this->is_due = true;
	}
	
	function setPaid(){
		$this->payment_submitted = $this->payment_cleared = date('Y-m-d H:i:s');		
	}
}
?>