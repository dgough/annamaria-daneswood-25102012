<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );

// ps maintainance - a tool for allowing pool cleaners / cleaners enter the cost of their services against a property

class ps_maintenance {

	var $_table_name = '#__{vm}_maintenance';
	
	function updateUserProperties($d) {
		global $my, $vmLogger, $perm, $VM_LANG;
		$db = new ps_DB();
		
		$db->query("SELECT * from #__{vm}_maintenance_props WHERE user_id = ".vmGet($d,'user_id'));
		if($db->num_rows()){
			$action = 'UPDATE';
			$where = ' WHERE user_id = '.vmGet($d,'user_id');
		}else{
			$action = 'INSERT';
			$where = '';
		}
		
		$fields = array('user_id' => vmGet($d,'user_id'),
						'properties' => implode(',',$d['properties'])
						);
		
		$db->buildQuery($action, '#__{vm}_maintenance_props', $fields, $where);
		
		if ($db->query()==false) {
			$vmLogger->err('Could not add the associated maintenance properties. Please ensure you have selected at least 1 property.');
			return false;
		}	
		return true;	
	}
	
	
	function validate_add(&$d) {
		global $vmLogger;
		
		$success = true;
		if(!vmGet($d, 'property_id')){
			$vmLogger->err('Property specified. You must specify a property.');
			$success = false;
		}
		if(!vmGet($d, 'user_id')){
			$vmLogger->err('No user id specified. You must specify a user ID.');
			$success = false;
		}
		if(!vmGet($d, 'worked_from')){
			$vmLogger->err('No from date specified. Please enter a worked from date.');
			$success = false;
		}
		if(!vmGet($d, 'worked_to')){
			$vmLogger->err('No to date specified. Please enter a worked to date.');
			$success = false;
		}
		if(!vmGet($d, 'type_id')){
			$vmLogger->err('No maintenance type selected. Please select a maintenance type.');
			$success = false;
		}
		if(!$this->checkDate(vmGet($d, 'worked_from'))){
			$vmLogger->err('Please enter a valid worked from date.');
			$success = false;
		}
		if(!$this->checkDate(vmGet($d, 'worked_to'))){
			$vmLogger->err('Please enter a valid worked to date.');
			$success = false;
		}
		if(!is_numeric(vmGet($d, 'hours'))){
			$vmLogger->err('Please enter valid hours.');
			$success = false;
		}
		if(!is_numeric(vmGet($d, 'minutes'))){
			$vmLogger->err('Please enter valid minutes.');
			$success = false;
		}
		if(!is_numeric(vmGet($d, 'amount'))){
			$vmLogger->err('Please enter a valid amount.');
			$success = false;
		}		
		return $success;	
	}
	
	function validate_update(&$d) {
		global $vmLogger;	

		if(!vmGet($d, 'maintenance_id')){
			$vmLogger->err('No maintenance ID specified, can not update.');
			return false;
		}		
		return $this->validate_add($d);
	}
	
	function checkDate(&$date){
		
		list($year,$month,$day) = explode('-',$date);
		return checkdate($month, $day, $year);		
	}
	
	
	function add(&$d) {
		global $my, $vmLogger, $perm, $VM_LANG, $vmuser;
		
		if (!$this->validate_add($d)) {
			return false;
		}
		
		$db = new ps_DB();
		
		$fields = array('property_id' => vmGet($d,'property_id'),
						'user_id' => vmGet($d,'user_id'),
						'type' => vmGet($d,'type_id'),
						'worked_from' => vmGet($d,'worked_from'),
						'worked_to' => vmGet($d,'worked_to'),
						'hours' => vmGet($d,'hours'),
						'minutes' => vmGet($d,'minutes'),
						'amount' => vmGet($d,'amount'),
						'comments' => $_REQUEST['comments'],
						'approved' => (vmGet($d,'approved',0) == 1 ? date('Y-m-d') : vmGet($d,'approved')),
						'paid' => (vmGet($d,'paid',0) ? date('Y-m-d') : 0)
						);
		
		$db->buildQuery('INSERT', $this->_table_name, $fields,array('comments'));
		
		if ($db->query()==false) {
			$vmLogger->err('Could not add maintenance, please check your form.');
			return false;
		}
		
		$d['user_id'] = $_REQUEST['user_id'] = $db->last_insert_id();
		
$recordID = $d['user_id'];

if($_FILES['file']['name'] != ""){

$name=$_FILES['file']['name'];
$fileNameParts = explode( ".", $name);
$fileExtension = end( $fileNameParts );
$fileExtension = strtolower( $fileExtension );
$name = $fileNameParts[0] . "." . $fileNameParts[1];

$tmpname=$_FILES['file']['tmp_name'];
$dest=$name;
	if($fileExtension=='exe' || $fileExtension=='zip' || $fileExtension=='gzip' || $fileExtension=='rar')
	{
	}
	else
	{
	move_uploaded_file($tmpname,$dest);
	mysql_query("INSERT INTO supDocs (maint_id, document) VALUES('".$recordID."' ,'".$dest."')")or die(mysql_error());
	}

}

		/**
		 * Send email to the owner of the property
		 */
		$d['new'] = 1;		
		if(!$this->sendMail($d)){
			$vmLogger->err('Could not send email to owner.');
			return false;
		}
		
		return true;
	}	
	
	function update(&$d) {
		global $my, $vmLogger, $perm, $VM_LANG, $vmuser;

		if (!$this->validate_update($d)) {
			return false;
		}
		
		$db = new ps_DB();

		//Compile fields to update
		$fields = array('property_id' => vmGet($d,'property_id'),
						'user_id' => vmGet($d,'user_id'),
						'type' => vmGet($d,'type_id'),
						'worked_from' => vmGet($d,'worked_from'),
						'worked_to' => vmGet($d,'worked_to'),
						'hours' => vmGet($d,'hours'),
						'minutes' => vmGet($d,'minutes'),
						'amount' => vmGet($d,'amount'),
						'comments' => $_REQUEST['comments'],
						'approved' => (vmGet($d,'approved',0) == 1 ? date('Y-m-d') : vmGet($d,'approved')),
						'paid' => vmGet($d,'paid')
						);
		
		$property_id = vmGet($d,'property_id');
		$user_id = vmGet($d,'user_id');
		$type = vmGet($d,'type_id');
		$worked_from = vmGet($d,'worked_from');
		$worked_to = vmGet($d,'worked_to');
		$hours = vmGet($d,'hours');
		$minutes = vmGet($d,'minutes');
		$amount = vmGet($d,'amount');
		$comments = $_REQUEST['comments'];
		$approved = (vmGet($d,'approved',0) == 1 ? date('Y-m-d') : vmGet($d,'approved'));
		$paid = vmGet($d,'paid');
		$maintenance_id = vmGet($d,'maintenance_id');
						
		//Update the DB table
		//$db->buildQuery('UPDATE', $this->_table_name, $fields, 'WHERE maintenance_id ='.vmGet($d,'maintenance_id',0),array('comments')));

		mysql_query("UPDATE jos_vm_maintenance SET property_id='".$property_id."', user_id='".$user_id."', type='".$type_id."', worked_from='".$worked_from."', worked_to='".$worked_to."', hours='".$hours."', minutes='".$minutes."', amount='".$amount."', comments='".$comments."', approved='".$approved."', paid='".$paid."' WHERE maintenance_id ='".$maintenance_id."' ")or die(mysql_error());

if ($db->query()==false) {
			$vmLogger->err('Could not update maintenance, please check your form.');
			return false;
		}

if($_FILES['file']['name'] != ""){

$recordID = $maintenance_id;
$name=$_FILES['file']['name'];
$fileNameParts = explode( ".", $name);
$fileExtension = end( $fileNameParts );
$fileExtension = strtolower( $fileExtension );
$name = $fileNameParts[0] . "." . $fileNameParts[1];

$tmpname=$_FILES['file']['tmp_name'];
$dest=$name;
	if($fileExtension=='exe' || $fileExtension=='zip' || $fileExtension=='gzip' || $fileExtension=='rar')
	{
	}
	else
	{
	move_uploaded_file($tmpname,"../documents/".$dest);
	mysql_query("INSERT INTO supDocs (maint_id, document) VALUES('".$recordID."' ,'".$dest."')")or die(mysql_error());
		}
}

		/**
		 * Send email to the owner of the property
		 */
		$d['new'] = 0;		
		if(!$this->sendMail($d)){
			$vmLogger->err('Could not send email to owner.');
			return false;
		}
		
		return true;
	}
			
	
	
	function validate_delete(&$d){
		
		if(!vmGet($d,'maintenance_id')){
			$GLOBALS['vmLogger']->err('Please specify a maintenance id.');
			return false;
		}
		return true;
	}
	
	
	/**
	* Controller for Deleting Records.
	*/
	function delete(&$d) {

		$record_id = vmGet($d,"maintenance_id");
		if(!$this->validate_delete($d)) return;
		
		if( is_array( $record_id)) {
			foreach( $record_id as $record) {
				if( !$this->delete_record( $record, $d ))
				return false;
			}
			return true;
		}
		else {
			return $this->delete_record( $record_id, $d );
		}
	}
	/**
	* Deletes one Record.
	*/
	function delete_record( $record_id, &$d ) {
		global $db;
		$record_id = intval( $record_id );
		$q = "DELETE from #__{vm}_maintenance where maintenance_id='$record_id'";
		$db->query($q);
		return True;
	}
	
	
	function sendMail(&$d){
		global $mosConfig_sitename, $sess;
		
		/**
		 * Setup the data to be passed to the email
		 */
		
		/**
		 * Get the property name
		 */
		$db = new ps_DB();
		$db->query("SELECT name FROM #__hp_properties WHERE id = ".vmGet($d,'property_id'));
		$db->next_record();
		$property = $db->f('name');
		
		/**
		 * Get the maintenance type
		 */
		$db = new ps_DB();
		$db->query("SELECT maintenance_type FROM #__{vm}_maintenance_types WHERE maintenance_type_id = ".vmGet($d,'type'));
		$db->next_record();
		$type = $db->f('maintenance_type');
		
		/**
		 * Get the owner info
		 */
		$db = new ps_DB();
		$db->query("SELECT u.* FROM jos_users AS u
					LEFT JOIN jos_hp_agents AS a ON u.id = a.user
					LEFT JOIN jos_hp_properties AS p ON a.id = p.agent
					WHERE p.id = ".vmGet($d,'property_id'));
		$db->next_record();
		$owner_name = $db->f('name');
		$owner_email = $db->f('email');
		
		//If there is no owner email then we just return
		if(!$owner_email) return true;
				
		$id = vmGet($d,'maintenance_id');
		$update = vmGet($d,'updated');
		$d['property'] = $property;
		$d['type'] = $type;
		$d['owner'] = $owner_name;
		$d['amount'] = $GLOBALS['CURRENCY_DISPLAY']->getFullValue(vmGet($d,'amount'));
		$d['approved'] = ($d['approved'] == '' || $d['approved'] == '0000-00-00') ? 'X' : $d['approved'];
		$d['paid'] = (!$d['paid'] == '' || $d['paid'] == '0000-00-00') ? 'X' : $d['paid'];		
		$d['sitename'] = $mosConfig_sitename;		
		$d['url'] = str_replace(URL.'administrator/',URL,$sess->url(URL."index.php?page=maintenance.maintenance_form&maintenance_id=$id"));		
		
		/**
		 * Compile message data
		 */
		$subject = $d['new'] ? sprintf("A maintenance sheet has been submitted for : %s",$property) : sprintf("A maintenance sheet has been updated for : %s",$property);
		$msg = 
"

Dear [OWNER]


 A maintenance sheet has been submitted for [PROPERTY]:


Type:
[TYPE]

Worked From:
[WORKED_FROM]

Worked To:
[WORKED_TO]

Time: [HOURS]h [MINUTES]m

Comments:		

[COMMENTS]

---------------------------
Total:
[AMOUNT]
---------------------------

Approved:
[APPROVED]

Paid:
[PAID]


You can log in to view and approve maintenance at:
[URL]


Kind Regards

[SITENAME]

";
				
		/**
		 * Loop through all submitted vars and update email.
		 */
		foreach($d as $k => $v){			
			$k = strtoupper($k);
			$msg = str_replace("[$k]",$v,$msg);		
		}

		/**
		 * Get the vendor info
		 */
		$db = new ps_DB();
		$db->query("SELECT vendor_name, contact_email FROM #__{vm}_vendor WHERE vendor_id = 1");
		$db->next_record();
		
		/**
		 * Send the email
		 */		
		return vmMail($db->f('contact_email'), $db->f('vendor_name'), $owner_email, $subject, $msg);		
	}
	
	function getMaintenance($prop_id, $vbDateFrom,$vbDateTo) {
		$db = new ps_DB();
		global $my, $vmuser;
		
		$q = 'SELECT p.id, p.name, sum(m.amount) AS total from #__hp_properties AS p ';
		$q.= 'LEFT JOIN #__hp_agents AS a on p.agent=a.id ';
		$q.= 'LEFT JOIN #__users AS u on a.user=u.id ';
		$q.= "LEFT JOIN $this->_table_name AS m on p.id=m.property_id ";
		$q.= "WHERE m.worked_to BETWEEN '$vbDateFrom' AND '$vbDateTo' AND ";
		$q.= 'm.approved != "" AND m.approved != "0000-00-00" ';
		$q .= $prop_id ? "AND p.id = $prop_id " : (!in_array($vmuser->gid,array(23,24,25)) ? 'and u.id='.$my->id.' ' : '');
		$q.= 'group by p.id';
		
		$db->setQuery($q);
		
		return $db->loadObjectList();
	}
	
}

?>