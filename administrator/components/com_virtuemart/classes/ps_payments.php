<?php 
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );
/**
*
* @version $Id: ps_payment_method.php 1273 2008-02-26 11:37:02Z thepisu $
* @package VirtueMart
* @subpackage classes
* @copyright Copyright (C) 2004-2007 soeren - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
require_once( CLASSPATH . "ps_checkout.php" );
require_once( CLASSPATH . "pageNavigation.class.php" );
require_once( CLASSPATH . "htmlTools.class.php" );
require_once( CLASSPATH . "ps_stage_payments.php" );

class ps_payments extends vmAbstractObject {

	var $db;
	var $listObj;
	
	function __construct($pageNav = ''){
		$pageNav = $pageNav ? $pageNav : new vmPageNav( 0,0,0);
		$this->listObj = new listFactory( $pageNav );
	}
	
	
	
	function getStagePayments($total = 0, $order_id = 0, $payment_ids = array(), &$ps_booking = null, $payment_method_id = 0){
		
		//Create the payment stage object that holds the results
		$payment_stages = new stdClass();
		$payment_stages->amount_due = 0;	
		$payment_stages->stages_due = array();
		$payment_stages->stages = array();	

		//Are we loading existing payment stages or creating new ones
		if($order_id){
			
			$db = $this->getPayments(array('op.order_id = '.$order_id),'op.payment_id');
			if($db){
				while($db->next_record()){
					$tmp = new ps_stage_payments(strtotime($db->f('payment_due')), $db->f('payment_amount'), $db->f('payment_stage'), $db->f('payment_submitted'), $db->f('payment_cleared'));
					$tmp->order_id = $db->f('order_id');
					$tmp->payment_id = $db->f('payment_id');
					$tmp->payment_method_id = $db->f('payment_method_id');
					$tmp->payment_method_name = $db->f('payment_method_name');
					if(count($payment_ids)) $tmp->is_due = in_array($db->f('payment_id'), $payment_ids);
					if($tmp->is_due){
						$payment_stages->amount_due += $tmp->payment_amount;
						$payment_stages->stages_due[] = $tmp->payment_stage;
					}
					$payment_stages->stages[] = $tmp;
				}
			}			
		}else{
			
			/**
			 * Get the number of payment stages
			 */
			$stage_payments = defined('VB_STAGE_PAY_NUM') ? VB_STAGE_PAY_NUM : 0;
			
			//Check if we are taking stage payments
			$enable_stage_payments = ($stage_payments > 0 && defined('VB_STAGE_PAYMENTS') && !$ps_booking->payInFull) ? VB_STAGE_PAYMENTS : 0;		
			
			
			/**
			 * Get some dates used below
			 */
			$dateBooked = $ps_booking->unixdateBooked;
			$dateFrom = $ps_booking->unixdateFrom;
			$today = strtotime(date("Y-m-d"));
			
			
			/**
			 * Get the balance details from config
			 * Work out if the balance is due
			 */
			
			$balance_days = (defined('VB_STAGE_PAY_DAYS_FINAL') ? VB_STAGE_PAY_DAYS_FINAL : 0) * (24 * 60 * 60);
			$balance_from = defined('VB_STAGE_PAY_FROM_FINAL') ? VB_STAGE_PAY_FROM_FINAL : 0;
			if($balance_from == 0){				
				$balance_due = $dateBooked + $balance_days;					
			}else{
				$balance_due = $dateFrom - $balance_days;				
			}	
				
			if($today >= $balance_due){
				$balance_is_due = 1;
			}else{
				$balance_is_due = 0;
			}
			
			
			/**
			 * Check if we are accepting stage payments, or 
			 * If balance is due or 
			 * The total is 0, 
			 * return the whole amount as one stage
			 */
			if(!$enable_stage_payments || $balance_is_due || $total <= 0){
				$payment_stages->amount_due = $total;
				$payment_stages->stages_due[] = 0;
				$payment_stages->stages[0] = new ps_stage_payments($today,$total,0);
			}else{
		
				
				/**
				 * Loop through the payment stages
				 */
				$balance = $total;
				$stages = array('due'=>0);			
				for($i = 0; $i < $stage_payments; $i++){
						
					if($balance > 0){
						$amount = constant("VB_STAGE_PAY$i");
						$type = constant("VB_STAGE_PAY_TYPE$i");
						$days = constant("VB_STAGE_PAY_DAYS$i") * (24 * 60 * 60);
						$from = constant("VB_STAGE_PAY_FROM$i");
						
						//If we are calculating from the BOOKING date, set due date accordingly
						if($from == 0){				
							$due = $dateBooked + $days;												
						}else{
							//If we are calculating from the ARRIVAL date, set due date accordingly
							$due = $dateFrom - $days;
						}
						
						//Check that the due date does not exceed the balance due date
						if($due > $balance_due) $due = $balance_due;
						
						//Calculate the stage amount
						//Are we doing a percentage, if so, calculate
						//the percentage amount
						if($type == 0){
							//Percentage
							$amount = (($amount * $balance) / 100);					
						}	
		
						//Check that this stage hasnt made the balance negative
						if($amount < 0){
							$amount = $balance;
						}
						
						//Check the stage is greater than 0
						if($amount > 0){
							
							//Create stage payment object					
							$payment_stages->stages[$i] = new ps_stage_payments($due, $amount, $i);
							
							//Check if this stage payment is due 
							if($payment_stages->stages[$i]->is_due){
								$payment_stages->stages_due[] = $i;
								$payment_stages->amount_due += $amount;
							}						
			
							//Subtract this off the balance
							$balance -= $amount;
						}
					}					
				}
		
				/**
				 * Check the balance is greater than 0
				 */		
				$balance = $balance < 0 ? 0 : $balance;		
						
				if($balance > 0){	
					//Create balance stage payment object
					$payment_stages->stages[$i] = new ps_stage_payments($balance_due, $balance, $i);		
				}	
			}
		}
		
		//Subtract the payment discount		
		$ps_checkout = new ps_checkout();				
		$payment_stages->amount_due = $payment_stages->amount_due - $ps_checkout->get_payment_discount( $payment_method_id, $payment_stages->amount_due );		
		
		return $payment_stages;
	}
	
	
		
	function getPayments($where = array(), $order= '', $keyword = ''){
		
		$q = $this->getQuery($where, $order, $keyword);
		
		$this->db = new ps_DB();
		$this->db->query($q);

		if($this->db->num_rows() == 0) return false;
		
		return $this->db;
	}	
	
	
	function getQuery($where = array(), $order = '', $keyword = ''){
		global $limitstart, $limit;
		
		if (!isset($limitstart)) $limitstart = 0;
		
		if (!isset($limit)) $limit = 100;
		
		if($limit > 100) $limit = 100;
		
		if($keyword){
			$where[] = "(
						CONCAT(ui.first_name,' ',ui.last_name) like '%$keyword%' OR
						prop.name like '%$keyword%' OR
						pm.payment_method_name like '%$keyword%' OR
						op.payment_stage like '%$keyword%' OR
						op.order_id like '%$keyword%'
						)";
		}
		
		$order = $order ? $order : 'op.payment_due DESC'; 
		$q= "SELECT op.*, pm.payment_method_name, prop.name AS property, CONCAT(ui.first_name,' ',ui.last_name) AS user
			FROM #__{vm}_order_payment AS op
			LEFT JOIN #__{vm}_order_user_info AS ui ON ui.order_id = op.order_id
			LEFT JOIN #__{vm}_payment_method AS pm ON pm.payment_method_id = op.payment_method_id
			RIGHT JOIN #__{vm}_order_booking AS bk ON bk.order_id = op.order_id 
			LEFT JOIN #__hp_properties AS prop ON bk.property_id = prop.id"
			.(count($where) ? ' WHERE '.implode(' AND ',$where) : '')
			." ORDER BY $order"
			." LIMIT $limitstart, $limit";
		return $q;
	}
	
	
	function getPaymentHeader(){
		// these are the columns in the table
		$columns = Array(  	'Booking' => '',
							'Property' => '',
							'User' => '',
							'Stage' => '',
							'Amount' => '',
							'Method' => '',								
							'Due' => '',
							'Date Sent' => '',
							'Received' => ''
						);
		
		$this->listObj->writeTableHeader( $columns );
		return count($columns);;
	}
	
	function renderPayments($rowClass = '', $return = ''){
		global $sess, $CURRENCY_DISPLAY;
		
		$i = 0;

		while($this->db->next_record()){		
			$link_start = '<a href="'.$sess->url($_SERVER['PHP_SELF'].'?page=order.admin_payment_form&payment_id='.$this->db->f('payment_id')).($return ? "&return=".urlencode($return) : '').'">';
			$link_end = '</a>';
			$this->listObj->newRow( $rowClass ? $rowClass.(int)$i : '' );			
			$this->listObj->addCell( $link_start.sprintf("%06d", $this->db->f("order_id")).$link_end );
			$this->listObj->addCell( $link_start.$this->db->f('property').$link_end );
			$this->listObj->addCell( $this->db->f("user") ? $this->db->f("user") : '&nbsp;' );
			$this->listObj->addCell( $this->db->f('payment_stage') == 0 ? 'Deposit' : $this->db->f('payment_stage'));
			$this->listObj->addCell( $CURRENCY_DISPLAY->getFullValue($this->db->f('payment_amount')), 'style="text-align: right"' );
			$this->listObj->addCell( $this->db->f('payment_method_name') ? $this->db->f('payment_method_name') : '&nbsp;' );
			$this->listObj->addCell( $this->db->f('payment_due') );
			$this->listObj->addCell( !($this->db->f('payment_submitted') == '0000-00-00' || $this->db->f('payment_submitted') == '') ? $this->db->f('payment_submitted') : vmCommonHTML::getYesNoIcon( 0, '', 'Not yet received' ));
			$this->listObj->addCell( !($this->db->f('payment_cleared') == '0000-00-00' || $this->db->f('payment_cleared') == '') ? $this->db->f('payment_cleared') : vmCommonHTML::getYesNoIcon( 0, '', 'Not yet cleared' ));
			$i = !$i;		
		}
		
	}
	
		
	function getCustomerPayments($order_id){
		global $ps_booking, $modulename, $CURRENCY_DISPLAY, $ps_checkout;
					
		include_once(CLASSPATH.'ps_checkout.php');
		$ps_checkout = new ps_checkout();

		//Load mootools
		vmCommonHTML::loadMooTools();
		?>
		<style type="text/css">
		table.adminlist{
		text-align: center;
		}
		table.adminlist table td{
			border-bottom: 0px;
		}
		table.adminlist tr.due{
		font-weight: bold;
		}
		</style>
		<script type="text/javascript">
		//<!--
		
		window.addEvent('domready', function(){

			if($('submitForm')){
				$('submitForm').addEvent('click',function(e){
					
					e = new Event(e);
			
					var hasStage = 0;
					var stages = $('vmMainPage').getElements('input[name^=payment_id]');
					stages.each(function(el){		
					   if(el.getProperty('type') == 'checkbox' && el.checked){
					   		hasStage = 1;
					   }else if(el.getProperty('type') == 'hidden' && el.getValue() != 0){
					   		hasStage = 1;
					   }
					});
					if(!hasStage){
						alert('You must select at least 1 payment stage');
						e.stop();
						return false;
					}
					
				})
			}
		})
		//-->
		</script>
		Below is a list of the payments we have received for your booking and the remaining payments still to pay.
		<br />		
		<?php
			
		$ps_booking->loadBookingData($order_id);		
		
		// Create the List Object with page navigation
		$listObj = new listFactory( );
		
		// print out the search field and a list heading
		$listObj->writeSearchHeader('', '', $modulename, '');
		
		// start the list table
		$listObj->startTable();
		
		// these are the columns in the table
		$columns = Array(  	'Payment Stage' => '',
							'Payment Amount' => '',
							'Payment Method' => '',								
							'Payment Due' => '',
							'Date Sent' => '',
							'Received' => '',	
							'Pay Now' => ''						
						);
		
		$colcount = count($columns);
		
		$listObj->writeTableHeader( $columns );
		
		$today = strtotime(date('Y-m-d'));
		
		$paid = 0;
		
		$due_today = 0;
		$still_to_pay = 0;
		
		$payments = $this->getStagePayments(0, $order_id);
		$num_stages = count($payments->stages);
		
		foreach($payments->stages AS $payment){
				
			$payment_id = $payment->payment_id;
			
			$amount = 		$payment->payment_amount;
			$method = 		$payment->payment_method_name;
			$due = 			$payment->payment_due;
			$due_unix = 	$payment->due;
			$submitted = 	$payment->payment_submitted == '' || $payment->payment_submitted == '0000-00-00' ? 0 : $payment->payment_submitted;
			$cleared = 		$payment->payment_cleared == '' || $payment->payment_cleared == '0000-00-00' ? 0 : $payment->payment_cleared;
			$stage = 		$payment->payment_stage;
			$is_due = 		$payment->is_due;
			$is_balance = 	($stage == ($num_stages - 1));
			$still_to_pay = !$submitted ? 1 : $still_to_pay;
			
		
			if(!$cleared && $is_due){
				$checked = 'checked="checked"';
			}else{
				$checked = '';
			}		
			if($cleared){
				$payNow = '&nbsp;';
			}else{
				$payNow = "<input type='checkbox' name='payment_id[]' value='$payment_id' $checked />";
			}
			if(!$cleared) $due_today += $amount;
			
			if($stage == 0){
				$payment_stage = "Deposit";
			}else if($is_balance){
				$payment_stage = "Final payment";
			}else{
				$payment_stage = "Stage $stage";
			}
			
			$listObj->newRow( $is_due && !$cleared ? 'due' : '');	
			$listObj->addCell( $payment_stage , 'style="text-align: left"');
			$listObj->addCell( $CURRENCY_DISPLAY->getFullValue($amount), 'style="text-align: right"' );
			$listObj->addCell( ($method ? $method : '&nbsp;') );			
			$listObj->addCell( $due );
			$listObj->addCell( ($submitted ? $submitted : vmCommonHTML::getYesNoIcon( 0 ) ) );
			$listObj->addCell( ($cleared ? $cleared : vmCommonHTML::getYesNoIcon( 0 ) ) );		
			$listObj->addCell( $payNow );	
			if($cleared){
				$paid++;
			}
		}
		
		$listObj->newRow();
		$listObj->addCell( '&nbsp;', "colspan='$colcount'" );
		
		//Get the payment methods
		$listObj->newRow();
		$isDue = ($due_today > 0) ? '<div class="payment_due"><strong>Please note:</strong> a payment of '.$CURRENCY_DISPLAY->getFullValue($due_today).' is due</div>' : '';
		$listObj->addCell( $isDue, "style='text-align: center' colspan='$colcount'" );
		
		//Check if the user has any mroe to pay
		if($num_stages == $paid){
			
			$listObj->newRow();
			$listObj->addCell( '<h4>Thank you, all payments have been submitted and cleared</h4>', "colspan='$colcount' style='text-align: center'" );	
		}else{
			//Payment method heading
			$listObj->newRow();
			$listObj->addCell( '<br /><h4>Please select your preferred payment method</h4>', "colspan='$colcount'" );
			
			//Get the payment methods
			$listObj->newRow();
			$listObj->addCell( $this->list_payment_methods(), "colspan='$colcount' style='text-align: left'" );
			
			//Output the pay now button
			$listObj->newRow();
			$listObj->addCell( '<input type="submit" id="submitForm" value="Pay Now" style="width: 150px" />', "style='text-align: center' colspan='$colcount'" );			
		}
		
		$listObj->writeTable();
		$listObj->endTable();
		$listObj->writeFooter( '', "&page=account.payments_confirm&order_id=$order_id" );

	}
	
	
	/**
	 * Lists the payment methods of all available payment modules
	 *
	 * @param int $payment_method_id
	 */
	function list_payment_methods( $payment_method_id=0 ) {
		global $order_total, $sess, $VM_CHECKOUT_MODULES, $VB_STAGE_PAY_METHODS, $page;
		$ps_vendor_id = $_SESSION['ps_vendor_id'];
		$auth = $_SESSION['auth'];
		
		$ship_to_info_id = vmGet( $_REQUEST, 'ship_to_info_id' );
		$shipping_rate_id = vmGet( $_REQUEST, 'shipping_rate_id' );
		
        require_once(CLASSPATH . 'ps_payment_method.php');
        $ps_payment_method = new ps_payment_method;
		require_once( CLASSPATH. 'ps_creditcard.php' );
	    $ps_creditcard = new ps_creditcard();
	    
	    /**
		 * Only show the payment stages relating to multi-stage payments
		 * if we are on the order.payment_list page
		 * */
		if(isset($VB_STAGE_PAY_METHODS) && count($VB_STAGE_PAY_METHODS)){
			$stage_pay_methods = $VB_STAGE_PAY_METHODS;
		}else{
			$stage_pay_methods = array();
		}
	    
		// Do we have Credit Card Payments?
		$db_cc  = new ps_DB;
		$q = "SELECT * from #__{vm}_payment_method,#__{vm}_shopper_group WHERE ";
		$q .= "#__{vm}_payment_method.shopper_group_id=#__{vm}_shopper_group.shopper_group_id ";
		$q .= "AND (#__{vm}_payment_method.shopper_group_id='".$auth['shopper_group_id']."' ";
		$q .= "OR #__{vm}_shopper_group.default='1') ";
		$q .= "AND (enable_processor='' OR enable_processor='Y') ";
		$q .= (count($stage_pay_methods) ? "AND payment_method_id IN (".implode(',',$stage_pay_methods).") " : "AND payment_enabled='Y' ");
		$q .= "AND #__{vm}_payment_method.vendor_id='$ps_vendor_id' ";
		$q .= " ORDER BY list_order";
		$db_cc->query($q);
		
		if ($db_cc->num_rows()) {
		    $cc_payments=true;
		}
		else {
		    $cc_payments=false;
		}
		$count = 0;
		$db_nocc  = new ps_DB;
		$q = "SELECT * from #__{vm}_payment_method,#__{vm}_shopper_group WHERE ";
		$q .= "#__{vm}_payment_method.shopper_group_id=#__{vm}_shopper_group.shopper_group_id ";
		$q .= "AND (#__{vm}_payment_method.shopper_group_id='".$auth['shopper_group_id']."' ";
		$q .= "OR #__{vm}_shopper_group.default='1') ";
		$q .= "AND (enable_processor='B' OR enable_processor='N' OR enable_processor='P') ";
		$q .= (count($stage_pay_methods) ? "AND payment_method_id IN (".implode(',',$stage_pay_methods).") " : "AND payment_enabled='Y' ");
		$q .= "AND #__{vm}_payment_method.vendor_id='$ps_vendor_id' ";
		$q .= " ORDER BY list_order";
		$db_nocc->query($q);
		if ($db_nocc->next_record()) {
		    $nocc_payments=true;
		    $first_payment_method_id = $db_nocc->f("payment_method_id");
		    $count = $db_nocc->num_rows();
		    $db_nocc->reset();
		}
		else {
		    $nocc_payments=false;
		}

		$theme = new $GLOBALS['VM_THEMECLASS']();
		$theme->set_vars(array('db_nocc' => $db_nocc,
								'db_cc' => $db_cc,
								'nocc_payments' => $nocc_payments,
								'payment_method_id' => $payment_method_id,
								'first_payment_method_id' => $first_payment_method_id,
								'count' => $count,
								'cc_payments' => $cc_payments,
								'ps_creditcard' => $ps_creditcard,
								'ps_payment_method' => $ps_payment_method
						 	)
						 );
						 
		return $theme->fetch( 'checkout/list_payment_methods.tpl.php');			
	}
	
	function add(&$d) {
		return $this->update($d, 1);
	}
	
	function validate_add(&$d){
		return true;
	}
	
	function validate_update($d) {
		if(!vmGet($d,'payment_id',0)){
			$GLOBALS['vmLogger']->err('You must supply a payment id.');
			return false;
		}
		return true;
	}
	
	function update(&$d, $new = 0) {
		global $order_total, $sess, $VM_CHECKOUT_MODULES, $VB_STAGE_PAY_METHODS, $vmLogger;
		
		if (!$new && !$this->validate_update($d)) {
			return false;
		}else if($new && !$this->validate_add($d)){
			return false;
		}
		
		$db = new ps_DB();
				
		$fields = array('payment_amount' => vmGet($d,'payment_amount'),
						'payment_due' => vmGet($d,'payment_due'),
						'payment_submitted' => vmGet($d,'payment_submitted')!='' ? vmGet($d,'payment_submitted') : '',
						'payment_cleared' => vmGet($d,'payment_cleared') !='' ? vmGet($d,'payment_cleared') : '',					
						'payment_method_id' => vmGet($d,'payment_method_id'),					
					 );
					 
		if(vmGet($d,'order_id')) $fields['order_id'] = vmGet($d,'order_id');
		
		if($new){
			$db->buildQuery('INSERT', '#__{vm}_order_payment', $fields);
		}else{
			$db->buildQuery('UPDATE', '#__{vm}_order_payment', $fields, 'where `payment_id`='.vmGet($d,'payment_id',0));
		}
		
		if ($db->query()==false) {
			$vmLogger->err('Could not update the selected payment stage. Please ensure you have entered the correct details.');
			return false;
		}	
		return true;
	}
}

?>
