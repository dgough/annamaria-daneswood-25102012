<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );
/**
* This file is called after the order has been placed by the customer
*
* @version $Id: checkout.thankyou.php 1364 2008-04-09 16:44:28Z soeren_nb $
* @package VirtueMart
* @subpackage html
* @copyright Copyright (C) 2004-2008 soeren - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/

mm_showMyFileName( __FILE__ );

require_once(CLASSPATH.'ps_product.php');
$ps_product= new ps_product;
$Itemid = $sess->getShopItemid();

global $vendor_currency, $user, $ps_booking, $auth, $VM_LANG, $page;


// Order_id is returned by checkoutComplete function
$order_id = $db->getEscaped(vmGet($vars, 'order_id' ) );

//Load the payment stages that were creates in ps_checkout::add()
$payment_stages = vmGet($_SESSION,'payment_stages',false);
if(!$payment_stages  OR !is_object($payment_stages)){
	$vmLogger->crit( 'There was a problem fetching your stage payments. Please contact us to resolve this issue. Your order id is:'.$order_id );
	return false;
}


$print = vmRequest::getInt('print', 0);

/** Retrieve User Email **/
$q  = "SELECT * FROM `#__{vm}_order_user_info` WHERE `order_id`='$order_id' AND `address_type`='BT'";
$db->query( $q );
$db->next_record();
$old_user = '';
if( !empty( $user ) && is_object($user)) {
	$old_user = $user;
}
$user = $db->record[0];
$dbbt = $db->_clone( $db );

$user->email = $db->f("user_email");

/** Retrieve Order & Payment Info **/
$db = new ps_DB;
$q  = "SELECT * FROM #__{vm}_orders AS o
LEFT JOIN #__{vm}_order_payment AS p ON p.order_id = o.order_id
LEFT JOIN #__{vm}_payment_method AS pm ON pm.payment_method_id = p.payment_method_id
WHERE o.order_id='$order_id' ".(count($payment_stages->stages_due) ? "AND p.payment_stage IN (".implode(',',$payment_stages->stages_due).")" : '').' LIMIT 0,1';
$db->query($q);


/** Get booking data **/
$dbbk = new ps_DB();
$q = "SELECT b.booking_dateFrom, b.booking_dateTo, p.name FROM #__{vm}_order_booking AS b, #__hp_properties as p WHERE b.booking_order_id = '".$db->f("order_id")."' AND b.booking_property_id = p.id";
$dbbk->query($q);

//Get stage payment info
$order_due = isset($payment_stages->amount_due) ? $payment_stages->amount_due : $db->f('order_total');

//Set the total with respect to the stage payments
$db->record[0]->order_total = $order_due;

$tpl = new $GLOBALS['VM_THEMECLASS']();
$tpl->set( 'order_id', $order_id );
$tpl->set( 'payment_stages', $payment_stages );
$tpl->set( 'ps_product', $ps_product );
$tpl->set( 'vendor_currency', $vendor_currency );
$tpl->set( 'user', $user );
$tpl->set( 'dbbt', $dbbt );
$tpl->set( 'db', $db );
$tpl->set( 'order_title' , $VM_LANG->_('PHPSHOP_ORDER_PRINT_PO_NUMBER').": ". $db->f("order_id") ." from " . $dbbk->f("booking_dateFrom") ." to " . $dbbk->f("booking_dateFrom") ." : at : " . $dbbk->f("name"));

echo $tpl->fetch( "pages/$page.tpl.php" );


if( !empty($old_user) && is_object($old_user)) {
	$user = $old_user;
}
?>