<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );
/**
*
* @version $Id: order.order_print.php 1354 2008-04-05 19:54:07Z gregdev $
* @package VirtueMart
* @subpackage html
* @copyright Copyright (C) 2004-2008 soeren - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
mm_showMyFileName( __FILE__ );
global $ps_order_status;
ini_set('display_errors','On');
error_reporting(E_ALL);
//Load mootools
vmCommonHTML::loadMooTools();

require_once(CLASSPATH.'ps_calendar.php');
require_once(CLASSPATH.'ps_product.php');
$ps_product =& new ps_product;

require_once(CLASSPATH.'ps_order_status.php');
require_once(CLASSPATH.'ps_checkout.php');
require_once(CLASSPATH.'ps_shopper_group.php');

$order_id = vmGet($_REQUEST,'order_id');

/**
 * Check if we are loading an order or creating a new one
 */
if(!$order_id){

	mm_showMyFileName( __FILE__ );
	global $ps_order_status, $vmuser;
	
	$where = '';
	if(!in_array($vmuser->gid,array(23,24,25))){
		$where = "WHERE u.id = $vmuser->id";
	}
	$dbu = new ps_DB();
	$dbu->setQuery("SELECT u.id, IF((ui.first_name != '' AND ui.last_name != ''), CONCAT(ui.first_name,' ',ui.last_name), name) as name
					FROM #__users as u
					LEFT JOIN #__{vm}_user_info AS ui ON ui.user_id = u.id
					$where
					ORDER BY IF(ui.last_name != '', ui.last_name , name)");
	$users = $dbu->loadObjectList();
	
	$dbprop = new ps_DB();
	$dbprop->setQuery("SELECT id,CONCAT(name, IF(published, '', ' - (unpublished)')) AS name FROM #__hp_properties WHERE id IN (".implode(',',ps_order_booking::getAllowedProperties()).") ORDER BY name");
    $allproperties = $dbprop->loadObjectList();
			
	require_once(PAGEPATH.'order.booking_modify.php');
	

}else{

	
	$dbc = new ps_DB;
	$q = "SELECT * FROM #__{vm}_orders WHERE order_id='$order_id'";
	$db->query($q);
	if( !$db->next_record() ) {
		
		 echo $GLOBALS['vmLogger']->err($VM_LANG->_('VM_ORDER_NOTFOUND'));
		 
	}else{
	  	
		echo ps_order::order_print_navigation( $order_id, 'order.booking_form' );
	  
	  $q = "SELECT * FROM #__{vm}_order_history WHERE order_id='$order_id' ORDER BY order_status_history_id ASC";
	  $dbc->query( $q );
	  $order_events = $dbc->record;
	  ?>
	<div id="vb-details-wrapper">
		<?php
			$tab = new vmTabPanel( 1, 1, "orderstatuspanel");
			$tab->startPane( "order_change_pane" );
			$tab->startTab($VM_LANG->_('VM_ORDER_OVERVIEW_HEADING'), "booking_overview" );
		?>
	    <h3><?php echo $VM_LANG->_('VM_ORDER_OVERVIEW_HEADING') ?></h3>
	    <?php if($VM_LANG->_('VM_ORDER_OVERVIEW_TIP')){ ?>
	    <div class="tip">
	    	<?php echo $VM_LANG->_('VM_ORDER_OVERVIEW_TIP') ?>
	    </div>
	    <?php } ?>
	  <table class="adminlist">
			  <tr class="sectiontableheader"> 
				<th colspan="2"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_PO_LBL') ?></th>
			  </tr>
			  <tr> 
				<td><strong><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_PO_NUMBER') ?>:</strong></td>
				<td><?php printf("%08d", $db->f("order_id"));?></td>
			  </tr>
			  <tr> 
				<td><strong><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_PO_DATE') ?>:</strong></td>
				<td><?php echo vmFormatDate( $db->f("cdate")+$mosConfig_offset);?></td>
			  </tr>
			  <tr> 
				<td><strong><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_PO_STATUS') ?>:</strong></td>
				<td><?php echo ps_order_status::getOrderStatusName($db->f("order_status")) ?></td>
			  </tr>
			  <?php if(defined('_VM_IS_BACKEND')){ ?>
			  <tr>
				  <td><strong><?php echo $VM_LANG->_('VM_ORDER_PRINT_PO_IPADDRESS') ?>:</strong></td>
				  <td><?php $db->p("ip_address"); ?></td>
			  </tr>
			  <?php } ?>
			  <?php 
			  if( PSHOP_COUPONS_ENABLE == '1') { ?>
			  <tr>
				  <td><strong><?php echo $VM_LANG->_('PHPSHOP_COUPON_COUPON_HEADER') ?>:</strong></td>
				  <td><?php if( $db->f("coupon_code") ) $db->p("coupon_code"); else echo '-'; ?></td>
			  </tr>
			  <?php 
				} ?>
		</table>
	
	<table class="adminlist" width="100%" id="your_booking">
		<?php
			/*** Organic Mod ***/
		    /* Display booking info here */
		    
			$dbb = new ps_DB();
			$dbb->query("SELECT b.*, p.id as prop_id, p.name AS property FROM #__{vm}_order_booking AS b, #__hp_properties AS p WHERE b.property_id = p.id AND b.order_id = '$order_id'");
			if($dbb->num_rows() > 0){
				
				$GLOBALS['VM_LANG']->load('booking');
			  	global $modulename;  	
			  	$oldmodulename = $modulename;
			  	$modulename = 'booking';
			  	?>
			  <tr>
			  	<td colspan="7" class='sectiontableheader'>
			  		<?php echo $VM_LANG->_('VM_BK_EMAIL_YOUR_BOOKING') ?>
			  	</td>
			  </tr>
				
			    <tr align="left">
					<th class="title"><?php echo $VM_LANG->_('VM_BK_EMAIL_PROPERTY') ?></th>
			        <th class="title"><?php echo $VM_LANG->_('VM_BK_EMAIL_ARRIVE') ?></th>
			        <th class="title"><?php echo $VM_LANG->_('VM_BK_EMAIL_DEPART') ?></th>
					<th class="title"><?php echo $VM_LANG->_('VM_BK_EMAIL_NIGHTS') ?></th>
					<th class="title"><?php //echo $VM_LANG->_('VM_BK_EMAIL_SUBTOTAL') ?></th>
					<th class="title"><?php echo $VM_LANG->_('VM_BK_EMAIL_SUBTOTAL') ?></th>
					<th class="title"><?php echo $VM_LANG->_('VM_BK_EMAIL_TOTAL') ?></th>
			    </tr>
			    <?php
				$subtotal = $CURRENCY_DISPLAY->getFullValue($dbb->f('subtotal'), '', $db->f('order_currency'));
				$total = $CURRENCY_DISPLAY->getFullValue($dbb->f('total'), '', $db->f('order_currency'));
				?>
				<tr>
					<td><?php echo $dbb->f('property') ?></td>
					<td><?php echo $dbb->f('arrival') ?></td>
					<td><?php echo $dbb->f('departure') ?></td>
					<td><?php echo round(((strtotime($dbb->f('departure')) - strtotime($dbb->f('arrival'))) / 86400)) ?></td>
					<td><?php //echo $subtotal ?></td>
					<td><?php echo $subtotal ?></td>
					<td><?php echo $total ?></td>
				</tr>
				
			  <?php 
			  $modulename = $oldmodulename;
			  } 
					
			  $dbt = new ps_DB;
			  $qt  = "SELECT order_item_id, product_quantity,order_item_name,order_item_sku,product_id,product_item_price,product_final_price, product_attribute, order_status
						FROM `#__{vm}_order_item`
						WHERE #__{vm}_order_item.order_id='$order_id' ";
			  $dbt->query($qt);
			  $i = 0;
			  $dbd = new ps_DB();
			  if($dbd->num_rows() > 0){
			 	 ?>
				  <tr>
					<th class="title" width="5%"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_QUANTITY') ?></th>
					<th class="title" width="32%"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_NAME') ?></th>
					<th class="title" width="9%"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_SKU') ?></th>
					<th class="title" width="10%"><?php //echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_PO_STATUS') ?></th>
					<th class="title" width="12%"><?php echo $VM_LANG->_('PHPSHOP_PRODUCT_FORM_PRICE_NET') ?></th>
					<th class="title" width="12%"><?php echo $VM_LANG->_('PHPSHOP_PRODUCT_FORM_PRICE_GROSS') ?></th>
					<th class="title" width="19%"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_TOTAL') ?></th>
				  </tr>
				  <?php
		
				  while ($dbt->next_record()){
					if ($i++ % 2) {
						$bgcolor='row0';
					} else {
						$bgcolor='row1';
					}
					$t = $dbt->f("product_quantity") * $dbt->f("product_final_price");
					// Check if it's a downloadable product
					$downloadable = false;
					$files = array();
					$dbd->query('SELECT product_id, attribute_name
									FROM `#__{vm}_product_attribute`
									WHERE product_id='.$dbt->f('product_id').' AND attribute_name=\'download\'' );
					if( $dbd->next_record() ) {
						$downloadable = true;
						$dbd->query('SELECT product_id, end_date, download_max, download_id, file_name
									FROM `#__{vm}_product_download`
									WHERE product_id='.$dbt->f('product_id').' AND order_id=\''.$order_id .'\'' );
						while( $dbd->next_record() ) {
							$files[] = $dbd->get_row();
						}
					}
				  ?>
				  <tr class="<?php echo $bgcolor; ?>" valign="top"> 
					<td width="5%"> <?php $dbt->p("product_quantity") ?></td>
					<td width="32%"><?php $dbt->p("order_item_name"); 
					  echo "<br /><span style=\"font-size: smaller;\">" . ps_product::getDescriptionWithTax($dbt->f("product_attribute")) . "</span>"; 
					  if( $downloadable ) {
					  	echo '<br /><br />
					  			<div style="font-weight:bold;">'.$VM_LANG->_('VM_DOWNLOAD_STATS') .'</div>';
					  	if( empty( $files )) {
					  		echo '<em>- '.$VM_LANG->_('VM_DOWNLOAD_NOTHING_LEFT') .' -</em>';
					  		$enable_download_function = $ps_function->get_function('insertDownloadsForProduct');
					  		if( $perm->check( $enable_download_function['perms'] ) ) {
					  			echo '<form action="'.$_SERVER['PHP_SELF'].'" method="post">				  		
					  			<input type="hidden" name="page" value="'.$page.'" />
					  			<input type="hidden" name="order_id" value="'.$order_id.'" />
					  			<input type="hidden" name="product_id" value="'.$dbt->f('product_id').'" />
					  			<input type="hidden" name="user_id" value="'.$dbt->f('user_id').'" />
					  			<input type="hidden" name="func" value="insertDownloadsForProduct" />
								<input type="hidden" name="vmtoken" value="'. vmSpoofValue($sess->getSessionId()) .'" />
					  			<input type="hidden" name="option" value="'.$option.'" />
					  			<input class="button" type="submit" name="submit" value="'.$VM_LANG->_('VM_DOWNLOAD_REENABLE').'" />
					  			</form>';
					  		}
					  	} else {
					  		foreach( $files as $file ) {
					  			echo '<em>'
					  					.'<a href="'.$sess->url( $_SERVER['PHP_SELF'].'?page=product.file_form&amp;product_id='.$dbt->f('product_id').'&amp;file_id='.$db->f("file_id")).'&amp;no_menu='.@$_REQUEST['no_menu'].'" title="'.$VM_LANG->_('PHPSHOP_MANUFACTURER_LIST_ADMIN').'">'
					  					.$file->file_name.'</a></em><br />';
					  			echo '<ul>';
					  			echo '<li>'.$VM_LANG->_('VM_DOWNLOAD_REMAINING_DOWNLOADS') .': '.$file->download_max.'</li>';
					  			if( $file->end_date > 0 ) {
					  				echo '<li>'.$VM_LANG->_('VM_EXPIRY').': '.vmFormatDate( $file->end_date + $mosConfig_offset ).'</li>';
					  			}
					  			echo '</ul>';
					  			echo '<form action="'.$_SERVER['PHP_SELF'].'" method="post">				  		
					  			<input type="hidden" name="order_id" value="'.$order_id.'" />
					  			<input type="hidden" name="page" value="'.$page.'" />
					  			<input type="hidden" name="func" value="mailDownloadId" />
								<input type="hidden" name="vmtoken" value="'. vmSpoofValue($sess->getSessionId()) .'" />
					  			<input type="hidden" name="option" value="'.$option.'" />
					  			<input class="button" type="submit" name="submit" value="'.$VM_LANG->_('VM_DOWNLOAD_RESEND_ID').'" />
					  			</form>';
					  		}
					  		
					  	}
					  }
					  ?>
					</td>
					<td width="9%"><?php  $dbt->p("order_item_sku") ?>&nbsp;</td>
									
					<td width="12%" align="right"><?php 
						echo $GLOBALS['CURRENCY_DISPLAY']->getFullValue($dbt->f("product_item_price"), 5, $db->f('order_currency'));  
						?></td>
					<td width="12%" align="right"><?php echo $GLOBALS['CURRENCY_DISPLAY']->getFullValue($dbt->f("product_final_price"), '', $db->f('order_currency'));  ?></td>
					<td width="19%" align="right"><?php echo $GLOBALS['CURRENCY_DISPLAY']->getFullValue($t, '', $db->f('order_currency')); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
				  </tr>
				  <?php 
				  } 
			  }
			  ?> 
			  <tr> 
				<td colspan="7">&nbsp; </td>
			  </tr>
			  
			  <?php if($db->f("order_subtotal") != $db->f("order_total")){ ?>
			  <tr> 	
				<td colspan="6" align="right"><strong> <?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_SUBTOTAL') ?>: </strong></td>
				<td width="19%" align="right"><?php echo $GLOBALS['CURRENCY_DISPLAY']->getFullValue($db->f("order_subtotal"), '', $db->f('order_currency')); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			  </tr>
			  <?php } ?>
	 		 <?php
			  /* COUPON DISCOUNT */
			$coupon_discount = $db->f("coupon_discount");
			
	  
			if( PAYMENT_DISCOUNT_BEFORE == '1') {
			  if ($db->f("order_discount") != 0) {
	  		?>
			  <tr>
				 <td width="5%">&nbsp;</td>
				<td width="42%">&nbsp;</td>
				<td colspan="4" align="right"><strong><?php 
				  if( $db->f("order_discount") > 0)
					echo $VM_LANG->_('PHPSHOP_PAYMENT_METHOD_LIST_DISCOUNT');
				  else
					echo $VM_LANG->_('PHPSHOP_FEE');
					?>:</strong></td>
				<td width="19%" align="right"><?php
					  if ($db->f("order_discount") > 0 )
					 echo "-" . $GLOBALS['CURRENCY_DISPLAY']->getFullValue(abs($db->f("order_discount")), '', $db->f('order_currency'));
				elseif ($db->f("order_discount") < 0 )
					 echo "+" . $GLOBALS['CURRENCY_DISPLAY']->getFullValue(abs($db->f("order_discount")), '', $db->f('order_currency')); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				  </td>
			  </tr>
			  
			  <?php 
			  } 
			  if( $coupon_discount > 0 || $coupon_discount < 0) {
				?>
			  <tr>
				<td colspan="6" align="right"><?php echo $VM_LANG->_('PHPSHOP_COUPON_DISCOUNT') ?>:
				</td> 
				<td align="right"><?php
				  echo "- ".$GLOBALS['CURRENCY_DISPLAY']->getFullValue( $coupon_discount, '', $db->f('order_currency') ); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
			  </tr>
			  <?php
			  }
			}
			global $PSHOP_SHIPPING_MODULES;
			//var_dump($PSHOP_SHIPPING_MODULES);
			
			if($db->f("order_tax") > 0){ ?>
			  <tr>
				<td colspan="6" align="right"><strong><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_TOTAL_TAX') ?>: </strong></td>
				<td width="19%" align="right"><?php echo $GLOBALS['CURRENCY_DISPLAY']->getFullValue($db->f("order_tax"), '', $db->f('order_currency')) ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			  </tr>
	  <?php	}
	  
			if( PAYMENT_DISCOUNT_BEFORE != '1') {
			  if ($db->f("order_discount") != 0) {
	  ?>
			  <tr> 
				<td colspan="6" align="right"><strong><?php 
				  if( $db->f("order_discount") > 0)
					echo $VM_LANG->_('PHPSHOP_PAYMENT_METHOD_LIST_DISCOUNT');
				  else
					echo $VM_LANG->_('PHPSHOP_FEE');
					?>:</strong></td>
				<td width="19%" align="right"><?php
					  if ($db->f("order_discount") > 0 )
					 echo "-" . $GLOBALS['CURRENCY_DISPLAY']->getFullValue(abs($db->f("order_discount")), '', $db->f('order_currency'));
				elseif ($db->f("order_discount") < 0 )
					 echo "+" . $GLOBALS['CURRENCY_DISPLAY']->getFullValue(abs($db->f("order_discount")), '', $db->f('order_currency')); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				  </td>
			  </tr>
			  
			  <?php 
			  } 
			  if( $coupon_discount > 0 || $coupon_discount < 0) {
	  ?>
			  <tr> 
				<td colspan="6" align="right"><strong><?php echo $VM_LANG->_('PHPSHOP_COUPON_DISCOUNT') ?>:</strong>
				</td> 
				<td align="right"><?php
				  echo "- ".$GLOBALS['CURRENCY_DISPLAY']->getFullValue( $coupon_discount, '', $db->f('order_currency') ); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
			  </tr>
			  <?php
			  }
			}
	  ?>
	  		<tr>
				<td colspan="6" align="right"><strong>Security Deposit: </strong></td>
				<td width="19%" align="right"><strong><?php echo $GLOBALS['CURRENCY_DISPLAY']->getFullValue($dbb->f('insurance'), '', $db->f('order_currency')); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td>
			  </tr>
			  <tr>
				<td colspan="6" align="right"><strong><?php echo $VM_LANG->_('PHPSHOP_CART_TOTAL') ?>: </strong></td>
				<td width="19%" align="right"><strong><?php echo $GLOBALS['CURRENCY_DISPLAY']->getFullValue($db->f("order_total"), '', $db->f('order_currency')); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td>
			  </tr>
			  <tr>
				<td colspan="6">&nbsp;</td>
				<td width="19%"><?php echo ps_checkout::show_tax_details( $db->f('order_tax_details'), $db->f('order_currency') ); ?></td>
			  </tr>
		</table>
		
		 <table width="100%">
			<tr class="sectiontableheader"> 
				
			  <?php if( $db->f("ship_method_id") ) { ?>
			  <td valign="top">
				<table class="adminlist">
				  <tr>
					<th><?php 
							$details = explode( "|", $db->f("ship_method_id"));
							echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_SHIPPING_LBL') ?>
					</th>
				  </tr>
				  <tr> 
					<td width="50%">
					  <strong><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_SHIPPING_CARRIER_LBL') ?>: </strong>
						<?php  echo $details[1]; ?>&nbsp;</td>
	  
				  <tr>
					<td width="50%">
					  <strong><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_SHIPPING_MODE_LBL') ?>: </strong>
					  <?php echo $details[2]; ?></td>
					
				  </tr>
				  <tr>
					<td width="50%">
					  <strong><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_SHIPPING_PRICE_LBL') ?>: </strong>
					<?php echo $GLOBALS['CURRENCY_DISPLAY']->getFullValue($details[3], '', $db->f('order_currency')); ?>
					</td>
				  </tr>
				</table>
			  </td>
			  <?php 
			  } ?>
		</tr>
		<tr>
			  <!-- Customer Note -->
			  <td valign="top">
				<table class="adminlist">
				  <tr>
					<th><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_CUSTOMER_NOTE') ?></th>
				  </tr>
				  <tr>
				  <td valign="top" width="50%" rowspan="4"><?php
					  if( $db->f("customer_note") ) {
						echo nl2br( $db->f("customer_note") );
					  }
					  else
						echo " ./. ";
						  
					  ?>&nbsp;
					</td>
				  </tr>
				</table>
			  </td>
		</tr>
		<tr>
			  <?php
			    $dbpm =& new ps_DB;
				$q  = "SELECT * FROM #__{vm}_payment_method, #__{vm}_order_payment WHERE #__{vm}_order_f.order_id='$order_id' ";
				$q .= "AND #__{vm}_payment_method.payment_method_id=#__{vm}_order_payment.payment_method_id";
				$dbpm->query($q);
				$dbpm->next_record();
			   
				// DECODE Account Number
				$dbaccount =& new ps_DB;
			    $q = "SELECT ".VM_DECRYPT_FUNCTION."(order_payment_number,'".ENCODE_KEY."') 
					AS account_number, order_payment_code FROM #__{vm}_order_payment  
					WHERE order_id='".$order_id."'";
				$dbaccount->query($q);
				$dbaccount->next_record();
				?>
			  <!-- Payment Information -->
			  <td valign="top">
				<table class="adminlist">
				  <tr class="sectiontableheader"> 
					<th width="13%"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_PAYMENT_LBL') ?></th>
					<th width="40%"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_ACCOUNT_NAME') ?></th>
					<th width="30%"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_ACCOUNT_NUMBER'); ?></th>
					<th width="17%"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_EXPIRE_DATE') ?></th>
				  </tr>
				  <tr> 
					<td width="13%"><?php $dbpm->p("payment_method_name");?> </td>
					<td width="40%"><?php $dbpm->p("order_payment_name");?></td>
					<td width="30%"><?php 
					echo ps_checkout::asterisk_pad( $dbaccount->f("account_number"), 4, true );
					if( $dbaccount->f('order_payment_code')) {
						echo '<br/>(' . $VM_LANG->_('VM_ORDER_PAYMENT_CCV_CODE') . ': '.$dbaccount->f('order_payment_code').') ';
					}
					?></td>
					<td width="17%"><?php echo vmFormatDate( $dbpm->f("order_payment_expire"), '%b-%Y'); ?></td>
				  </tr>
				  <tr> 
				  <tr class="sectiontableheader"> 
					<th colspan="4"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_PAYMENT_LOG_LBL') ?></th>
				  </tr>
				  <tr> 
					<td colspan="4"><?php if($dbpm->f("order_payment_log")) echo $dbpm->f("order_payment_log"); else echo "./."; ?></td>
				  </tr>
				</table>
			  </td>
			  
			</tr>
		  </table>
	
	<?php
	$tab->endTab();
	
	$tab->startTab( $VM_LANG->_('VM_ORDER_HISTORY_HEADING'), "order_history_page" );
		
		function __unserialize($sObject) {   
		    $__ret =preg_replace('!s:(\d+):"(.*?)";!e', "'s:'.strlen('$2').':\"$2\";'", $sObject );		   
		    return unserialize($__ret);		   
		}
	?>
    	<h3><?php echo $VM_LANG->_('VM_ORDER_HISTORY_HEADING') ?></h3>
    	<?php if($VM_LANG->_('VM_ORDER_HISTORY_TIP')){ ?>
        <div class="tip">
            <?php echo $VM_LANG->_('VM_ORDER_HISTORY_TIP') ?>
        </div>
        <?php } ?>
		<table class="adminlist" width="100%">
		 <tr>
		  <th><?php echo $VM_LANG->_('PHPSHOP_ORDER_HISTORY_DATE_ADDED') ?></th>
		  <th><?php echo $VM_LANG->_('PHPSHOP_ORDER_HISTORY_CUSTOMER_NOTIFIED') ?></th>
		  <th><?php echo $VM_LANG->_('PHPSHOP_ORDER_LIST_STATUS') ?></th>
		  <th><?php echo $VM_LANG->_('PHPSHOP_COMMENT') ?></th>
		 </tr>
		 <?php 
		 foreach( $order_events as $order_event ) {

		  echo "<tr>";
		  echo "<td>".$order_event->date_added."</td>\n";
		  echo "<td align=\"center\"><img alt=\"" . $VM_LANG->_('VM_ORDER_STATUS_ICON_ALT') ."\" src=\"$mosConfig_live_site/administrator/images/";
		  echo $order_event->customer_notified == 1 ? 'tick.png' : 'publish_x.png';
		  
		  echo "\" border=\"0\" align=\"absmiddle\" /></td>\n";
		  echo "<td>".$order_event->order_status_code."</td>\n";
		  echo "<td>".$order_event->comments."</td>\n";
		  echo "</tr>\n";
		  ?>
          <tr>
          	<td colspan="4">
          		<?php           		
          		if(!is_null($order_event->booking_serialized) &&  is_string($order_event->booking_serialized)){
          			//(__unserialize($order_event->booking_serialized))>
				}else{
					echo '&nbsp;';
				}
				?>
				</td>
          </tr>
          <?php		  
		 }
		 ?>
		</table>
		<?php		
		$tab->endTab();
		
			
		$tab->startTab($VM_LANG->_('VM_ORDER_BILLING_HEADING'), "billing_info" ); 
		?>
	    
		<h3><?php echo $VM_LANG->_('VM_ORDER_BILLING_HEADING') ?></h3>
		<?php if($VM_LANG->_('VM_ORDER_BILLING_TIP')){ ?>
		<div class="tip">
	    	<?php echo $VM_LANG->_('VM_ORDER_BILLING_TIP') ?>
	    </div>
	    <?php } ?>
	  <?php
		$user_id = $db->f("user_id");
		$dbt = new ps_DB;
		$qt = "SELECT * from #__{vm}_order_user_info WHERE user_id='$user_id' AND order_id='$order_id' ORDER BY address_type ASC";
		$dbt->query($qt);
		$dbt->next_record();
	
		require_once( CLASSPATH . 'ps_userfield.php' );
		$userfields = ps_userfield::getUserFields('registration', false, '', true, true );
		$shippingfields = ps_userfield::getUserFields('shipping', false, '', true, true );
	   ?>
	   
	   <!-- bill to information -->
			<table width="100%" class='adminlist'>
				<tr>
					<th colspan="2" style='text-align: left'>
						Customer Billing Details
					</th>
				</tr>
			<?php 
			$z=0;
			foreach( $userfields as $field ) {
				if( $field->name == 'email') $field->name = 'user_email';
				?>
			  <tr class="row<?php echo $z ?>">
				<td>&nbsp;<?php echo $VM_LANG->_($field->title) ? $VM_LANG->_($field->title) : $field->title ?> :</td>
				<td><?php
					switch($field->name) {
			          	case 'country':
			          		require_once(CLASSPATH.'ps_country.php');
			          		$country = new ps_country();
			          		$dbc = $country->get_country_by_code($dbt->f($field->name));
		          			if( $dbc !== false ) echo $dbc->f('country_name').' ';
			          		break;
			          	default: 
	
			          		echo $dbt->f($field->name).' &nbsp;';
			          		break;
			          }
			          ?>
				</td>
			  </tr>
			  <?php
			  	$z = !$z;
				}
			   ?>
			</table>
	        
	<?php 
	$tab->endTab();
		
	if(@VB_STAGE_PAYMENTS){
		$tab->startTab($VM_LANG->_('VM_ORDER_PAYMENT_HEADING'), "payment_info" ); 
		?>
		<h3><?php echo $VM_LANG->_('VM_ORDER_PAYMENT_HEADING') ?></h3>
		<?php if($VM_LANG->_('VM_ORDER_PAYMENT_TIP')){ ?>
		<div class='tip'>
			<?php echo $VM_LANG->_('VM_ORDER_PAYMENT_TIP') ?>
		</div>
		<?php }
		
		require_once(CLASSPATH.'ps_payments.php');
		$ps_payments = new ps_payments();
		//$ps_payments->listObj->writeSearchHeader('', '', $modulename, '');
		$ps_payments->listObj->startTable();
		$ps_payments->getPaymentHeader();
		$ps_payments->getPayments(array("op.order_id = '$order_id'"),'op.payment_due ASC, op.payment_stage ASC');
		$ps_payments->renderPayments('',$page);
		$ps_payments->listObj->writeTable();
		$ps_payments->listObj->endTable();
		//$ps_payments->listObj->writeFooter( '', "&page=order.booking_form" );
				
		$tab->endTab();
	}
	$tab->startTab( $VM_LANG->_('VM_ORDER_UPDATE_HEADING'), "booking_status" );
	?>
	<h3><?php echo $VM_LANG->_('VM_ORDER_UPDATE_HEADING') ?></h3>
	<?php if($VM_LANG->_('VM_ORDER_UPDATE_TIP')){ ?>
    <div class="tip">
    	<?php echo $VM_LANG->_('VM_ORDER_UPDATE_TIP') ?>
    </div>
    <?php } ?>
		<form method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>">
		
		<table class="adminlist">	
		  <td><?php echo "<strong>".$VM_LANG->_('PHPSHOP_ORDER_PRINT_PO_STATUS') .": </strong>"; ?> </td>
          <td colspan="2">
          	<?php $ps_order_status->list_order_status($db->f("order_status")); ?>
			<input type="submit" class="button" name="Submit" value="<?php echo $VM_LANG->_('PHPSHOP_UPDATE') ?>" />
			<input type="hidden" name="func" value="orderStatusSet" />
			<input type="hidden" name="vmtoken" value="<?php echo vmSpoofValue($sess->getSessionId()) ?>" />
			<input type="hidden" name="option" value="com_virtuemart" />
			<input type="hidden" name="current_order_status" value="<?php $db->p("order_status") ?>" />
			<input type="hidden" name="order_id" value="<?php echo $order_id ?>" />
			<input type="hidden" name="page" value="<?php echo $page ?>" />
          </td>
		 </tr>
		 <tr>
		  <td valign="top"><?php echo "<strong>".$VM_LANG->_('PHPSHOP_COMMENT') .": </strong>"; ?>
		  </td>
		  <td>
			<textarea name="order_comment" rows="4" cols="35"></textarea>
		  </td>
		  <td>
			<input type="checkbox" name="notify_customer" id="notify_customer" checked="checked" value="Y" /> 
				<label for="notify_customer"><?php echo $VM_LANG->_('PHPSHOP_ORDER_LIST_NOTIFY') ?></label>
			<br/>
			<input type="checkbox" name="include_comment" id="include_comment" checked="checked" value="Y" /> 
				<label for="include_comment"><?php echo $VM_LANG->_('PHPSHOP_ORDER_HISTORY_INCLUDE_COMMENT') ?></label>
		  </td>
		 </tr>
		</table>
		</form>
	<?php
	$tab->endTab();
	
	$tab->startTab( $VM_LANG->_('VM_ORDER_MODIFY_HEADING'), "modify_order" );
			
	$cid = vmRequest::getVar( 'cid', array(0), '', 'array' );
	
	require_once(PAGEPATH.'order.booking_modify.php');		
	
	$tab->endTab();		
	
	$tab->endPane();
	?>
	</div>
	<?php	
	}

}
?>