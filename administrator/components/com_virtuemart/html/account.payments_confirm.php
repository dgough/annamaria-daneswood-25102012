<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );
/*
 * @version $Id: account.payments_confirm.php 1122 2008-01-07 14:52:31Z thepisu $
 * @package VirtueMart
 * @subpackage html
 */
mm_showMyFileName(__FILE__);

require_once( CLASSPATH . "htmlTools.class.php" );
require_once( CLASSPATH . "ps_payment_method.php" );
require_once( CLASSPATH . "ps_checkout.php" );
require_once( CLASSPATH . "ps_payments.php" );


global $sess, $ps_booking, $CURRENCY_DISPLAY, $modulename, $keyword, $page;

$ps_payments = new ps_payments();
$order_id 			= vmGet($_REQUEST, 'order_id');
$payment_method_id 	= vmGet($_REQUEST, 'payment_method_id');
$payment_ids 		= vmGet($_REQUEST, 'payment_id', array());
$_SESSION['payment_ids'] = $payment_ids;

//Check if a payment method has been selected
if(!count($payment_ids)) vmRedirect($sess->url(SECUREURL.basename($_SERVER['PHP_SELF'])."?page=account.payments&order_id=$order_id"), "Please select at least 1 stage to pay.");

// Save credit card data
// The User has choosen a payment method
$_SESSION['ccdata']['order_payment_name'] = vmGet($_REQUEST,'order_payment_name');
// VISA, AMEX, DISCOVER....
$_SESSION['ccdata']['creditcard_code'] = vmGet($_REQUEST,'creditcard_code');
$_SESSION['ccdata']['order_payment_number'] = vmGet($_REQUEST,'order_payment_number');
$_SESSION['ccdata']['order_payment_expire_month'] = vmGet($_REQUEST,'order_payment_expire_month');
$_SESSION['ccdata']['order_payment_expire_year'] = vmGet($_REQUEST,'order_payment_expire_year');
// 3-digit Security Code (CVV)
$_SESSION['ccdata']['credit_card_code'] = vmGet($_REQUEST,'credit_card_code');
$_SESSION["ccdata"]["order_payment_expire"] = @mktime(0,0,0,$_SESSION["ccdata"]["order_payment_expire_month"], 1,$_SESSION["ccdata"]["order_payment_expire_year"]);

if (!ps_checkout::validate_payment_method($_REQUEST, false)) { //Change false to true to Let the user play with the VISA Testnumber
	return false;
}	


//Get the chosen payment method name
$payment_method = ps_payment_method::get_field($payment_method_id, "payment_method_name");

// Create the List Object with page navigation
$listObj = new listFactory( );

// print out the search field and a list heading
$listObj->writeSearchHeader('You have selected to pay the following payments:', '', $modulename, '');

// start the list table
$listObj->startTable();

// these are the columns in the table
$columns = Array(  	'Payment Stage' => '',
					'Payment Amount' => '',
					'Payment Method' => '',								
					'Payment Due' => '',
				);
	
$listObj->writeTableHeader( $columns );

$total = 0;
$colcount = count($columns);

$payment_stages = $ps_payments->getStagePayments(0, $order_id, $payment_ids);
$total_stages = count($payment_stages->stages);
foreach($payment_stages->stages AS $payment){
	
	if($payment->is_due){
		$stage = $payment->payment_stage;
		if($stage == 0 && $total_stages > 1){
			$stage_label = "Deposit";
		}else if($stage == $total_stages-1){
			$stage_label = "Final payment";
		}else{
			$stage_label = "Stage $stage";
		}
		
		$listObj->newRow();	
		$listObj->addCell( $stage_label, 'style="text-align: center"' );
		$listObj->addCell( $CURRENCY_DISPLAY->getFullValue($payment->payment_amount), 'style="text-align: center"' );
		$listObj->addCell( $payment_method, 'style="text-align: center"' );			
		$listObj->addCell( $payment->payment_due, 'style="text-align: center"' );
	}
}

$listObj->newRow();
$listObj->addCell( '&nbsp;', "colspan='$colcount'" );

$listObj->newRow();
$listObj->addCell( sprintf("Please confirm that you wish to pay <strong>%s</strong> by %s today.",$CURRENCY_DISPLAY->getFullValue($payment_stages->amount_due), $payment_method), "style='text-align: center' colspan='$colcount'" );
	
$listObj->newRow();
$listObj->addCell( '&nbsp;', "colspan='$colcount'" );

$listObj->newRow();
$listObj->addCell( '<input type="submit" id="submitForm" value="Confirm" style="width: 150px" />', "style='text-align: center' colspan='$colcount'" );

$listObj->writeTable();
$listObj->endTable();

$hidden = "&page=account.payments_thankyou&order_id=$order_id&payment_method_id=$payment_method_id";	
$listObj->writeFooter( $keyword, $hidden );

	
?>