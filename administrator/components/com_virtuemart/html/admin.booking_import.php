<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );

global $psdb;

$psdb = new ps_DB();

function updateBooking($row) {
	global $psdb;
	
	//$order_number = $row->user_id.'_'.md5($row->rec);
	$total_gross = $row->price;
	
	$total_net = ($row->price / 1.105);
	$total_tax = ($row->price - ($row->price / 1.105));
	$state_tax = ($row->price - ($row->price / 1.06));
	$resort_tax = ($row->price - ($row->price / 1.045));
	
	$start = $row->start;
	$end = $row->end;
	
	$created = strtotime($row->rectime);
	
	$concat = $row->rec.'|'.$row->price.'|'.$order_number."\n";
	
	if (!$psdb->query("insert INTO #__{vm}_order_booking set 
				order_id='".$row->rec."',
				property_id='".$row->prop_id."',
				people='".$row->people."',
				arrival='".$start."',
				departure='".$end."',
				total='".$total_gross."',
				subtotal='".$total_net."',
				original='".$total_gross."',
				tax_total='".$total_tax."',
				tax_state='".$state_tax."',
				tax_resort='".$resort_tax."'
				")) {
		file_put_contents(dirname(__FILE__).'/failed_bookings.txt',$concat,FILE_APPEND);					
	} else {
		echo 'Done : '.$row->rec.'<br />';
	}
	
}

$db = new ps_DB();
$db->setQuery("select 
				tu.user_id, 
				ui.user_info_id, 
				tp.prop_id, 
				tb.* 
				from temp_bookings tb 
				left join temp_users tu on tb.custid=tu.rec 
				left join jos_users u on tu.user_id=u.id 
				left join jos_vm_user_info ui on u.id=ui.user_id 
				left join temp_props tp on tb.name=tp.old_property 
				where tp.prop_id != 0 AND tp.imported = 0 and tp.prop_id is not null");

$rows = $db->loadObjectList();

foreach ($rows as $row) {
	updateBooking($row);
	
}


?>