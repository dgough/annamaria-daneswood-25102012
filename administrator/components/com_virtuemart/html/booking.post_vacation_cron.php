<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 

/**
 * VirtueBook: Cron script to send emails after a vacation
 */
$daysPriorToArriaal = -3;

//Set the subject
$subject = "Thank-you for taking your vacation with Anna Maria Vacations";

/*******/
$date = date('Y-m-d',(time()+($daysPriorToArriaal * 86400)));
echo "Fetching bookings that start on: $date";

global $sess, $vmLogger, $VM_LANG;
$VM_LANG->load('order_emails');

/**
 * Get vendor information
 */
$dbv = new ps_DB;
$qt = "SELECT * from #__{vm}_vendor ";
/* Need to decide on vendor_id <=> order relationship */
$qt .= "WHERE vendor_id = '".vmGet( $_SESSION, 'ps_vendor_id', 1 )."'";
$dbv->query($qt);
$dbv->next_record();

$vendor_email = $dbv->f("contact_email");
$vendor_name = $dbv->f("vendor_name");

$imagefile = pathinfo($dbv->f("vendor_full_image"));
$extension = $imagefile['extension'] == "jpg" ? "jpeg" : "jpeg";

$EmbeddedImages = array();
$EmbeddedImages[] = array(	'path' => IMAGEPATH."vendor/".$dbv->f("vendor_full_image"),
					'name' => "vendor_image", 
					'filename' => $dbv->f("vendor_full_image"),
					'encoding' => "base64",
					'mimetype' => "image/".$extension );

/**
 * Get the bookings
 */
$db = new ps_DB();
$db->query("SELECT b.* FROM #__vm_orders AS o
LEFT JOIN #__vm_order_booking as b on b.order_id = o.order_id
WHERE b.departure = '$date' AND o.order_status = 'C'");

$dbp = new ps_DB();

echo '<br />Found '.$db->num_rows().' records<br />';

//Loop through bookings
while($db->next_record()){
	
	$booking = $db->get_row();
	$order_id = $booking->order_id;
	
	//Get the property details
	$dbp->query("SELECT * FROM #__hp_properties WHERE id = $booking->property_id");
	$dbp->next_record();
	$property = $dbp->get_row();
	
	//Get the customer information
	$dbp->query("SELECT * FROM #__vm_order_user_info	WHERE order_id = $order_id");
	$dbp->next_record();
	$user = $dbp->get_row();
	$user->name = ($user->title ? $user->title : '')."$user->last_name"; 
	$customer_email = $user->user_email;
	$customer_name = $user->name;
	//$customer_email = "rob@organic-development.com";
	
	/**
	 * Get the template file
	 */
	$tpl = vmTemplate::getInstance();
	$tpl->set_vars(array('property' => $property,
						'customer' => $user,									
						'subject' => $subject									
					));
	$mail_AltBody = $tpl->fetch('order_emails/post_vacation_email.html.tpl.php');

	$mail_Body = '';
	
 	$success = vmMail( $vendor_email, $vendor_name, $customer_email, $subject, $mail_AltBody, $mail_Body, true, null, null, $EmbeddedImages);
 	vmMail( $customer_email, $customer_name, $vendor_email, $subject, $mail_AltBody, $mail_Body, true, null, null, $EmbeddedImages);
	if(!$success){		
		$vmLogger->err( 'Something went wrong while sending the post-vacation email to '.$db->f('first_name').' '.$db->f('last_name').' at '.$customer_email );
	}else{
		$vmLogger->info( 'Success: sending the post-vacation email to '.$db->f('first_name').' '.$db->f('last_name').' at '.$customer_email );
	}
}
exit();
?>