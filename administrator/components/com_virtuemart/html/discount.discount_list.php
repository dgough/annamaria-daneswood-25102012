<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
/**
*
* @version $Id: tax.tax_list.php 1099 2007-12-21 12:46:46Z soeren_nb $
* @package VirtueMart
* @subpackage html
* @copyright Copyright (C) 2004-2007 soeren - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
mm_showMyFileName( __FILE__ );

error_reporting(E_ALL);
ini_set('display_errors','On');

require_once( CLASSPATH . "pageNavigation.class.php" );
require_once( CLASSPATH . "htmlTools.class.php" );

$q = "";
$list  = "SELECT * FROM #__{vm}_discounts ORDER BY ordering";
$count = "SELECT count(*) as num_rows FROM #__{vm}_discounts"; 
$list .= $q . " LIMIT $limitstart, " . $limit;
$count .= $q;   

$db->query($count);
$db->next_record();
$num_rows = $db->f("num_rows");
  
$db->query($list);

// Create the Page Navigation
$pageNav = new vmPageNav( $num_rows, $limitstart, $limit );

// Create the List Object with page navigation
$listObj = new listFactory( $pageNav );

// print out the search field and a list heading
$listObj->writeSearchHeader('Discounts', '', $modulename, "discount_list");

// start the list table
$listObj->startTable();

// these are the columns in the table
$columns = Array(  "#" => "width=\"20\"", 
					"<input type=\"checkbox\" name=\"toggle\" value=\"\" onclick=\"checkAll(".count($db->record) .")\" />" => "width=\"20\"",
					'Discount Name' => '',
					$VM_LANG->_('VM_FIELDMANAGER_REORDER')=> "width=\"5%\"",
					vmCommonHTML::getSaveOrderButton( (count($db->record)), 'changeOrder' ) => 'width="8%"',
					$VM_LANG->_('E_REMOVE') => "width=\"5%\""
				);
$listObj->writeTableHeader( $columns );

$i = 0;
while ($db->next_record()) {

	$listObj->newRow();
	
	// The row number
	$listObj->addCell( $pageNav->rowNumber( $i ) );
	
	// The Checkbox
	$listObj->addCell( vmCommonHTML::idBox( $i, $db->f("discount_id"), false, "discount_id" ) );
	
	$url = $_SERVER['PHP_SELF'] . "?page=$modulename.discount_form&discount_id=". $db->f("discount_id");
	$tmp_cell = "<a href=\"" . $sess->url($url) . "\">". $db->f("label"). "</a>";
	$listObj->addCell( $tmp_cell );

	$tmp_cell = "<div align=\"center\">"
	. $pageNav->orderUpIcon( $i, $i > 0, "orderup", $VM_LANG->_('CMN_ORDER_UP'), $page, "changeOrder" )
	. "\n&nbsp;"
	. $pageNav->orderDownIcon( $i, $db->num_rows(), $i-1 <= $db->num_rows(), 'orderdown', $VM_LANG->_('CMN_ORDER_DOWN'), $page, "changeOrder" )
	. "</div>";
	$listObj->addCell( $tmp_cell );

	$listObj->addCell( vmCommonHTML::getOrderingField( $db->f('ordering') ) );
	
	$listObj->addCell( $ps_html->deleteButton( "discount_id", $db->f("discount_id"), "deleteDiscount", $keyword, $limitstart ) );

	$i++;
}
$listObj->writeTable();

$listObj->endTable();

$listObj->writeFooter( $keyword );
?>