<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 

mm_showMyFileName( __FILE__ );

global $VM_LANG, $modulename, $vm_mainframe, $sess;

require_once( CLASSPATH . "htmlTools.class.php" );
require_once(CLASSPATH.'ps_order_booking.php');
$ps_order_booking = new ps_order_booking();

/**
 * Add some default styling for the calendar
 */

$styles = "
table.adminlist td,
table.adminlist th{
text-align: center;
padding: 2px !important;
font-weight: bold;
}

table.adminlist td.property{
font-weight: bold;
text-align: left;
border-right: 1px solid #ccc;
}

table.adminlist td.property div{
width: 120px;
}

.vb_availability_calendar td.booked,
.vb_availability_calendar ul.vb_key li.booked span{
background-color: #4EA9C4 !important;
}

.vb_availability_calendar td.arrival,
.vb_availability_calendar ul.vb_key li.arrival span{
background: url(".VM_THEMEURL."templates/calendar/images/book_start.gif) no-repeat center center !important;
}

.vb_availability_calendar td.departure,
.vb_availability_calendar ul.vb_key li.departure span{
background: url(".VM_THEMEURL."templates/calendar/images/book_end.gif) no-repeat center center !important;
}

.vb_availability_calendar td.overlap{
background-image: url(".VM_THEMEURL."images/slash.gif) !important;
background-position: center !important;
background-repeat: no-repeat !important;
}

td.search_form{
display: none;
}

#date_select{
text-align: right;
margin-top: -40px;
float: right;
}

.vb_availability_calendar ul.vb_key{
list-style: none;
padding: 5px 0px;
margin: 0px;
}

.vb_availability_calendar ul.vb_key li{
float: left;
padding: 3px;
}

.vb_availability_calendar ul.vb_key li.key{
font-weight: bold;
}

.vb_availability_calendar ul.vb_key li.clear{
clear: left;
float: none;
}

.vb_availability_calendar ul.vb_key li span{
display: block;
float: left;
width: 10px;
height: 10px;
vertical-align: middle;
border: 1px solid #ccc;
margin: 2px;
}


";

$vm_mainframe->addStyleDeclaration($styles);

//Get the date
$date = vmGet($_REQUEST,'date',date('Y-m'));


/**
 * Create the month drop downs
 */
$month = date('m');
$year = date('Y');
$months = array();
$current = '';
for($i = 0; $i < 24; $i++){
	
	if($month == 13){
		$month = 01;
		$year++;
	}
	if(strlen($month) == 1) $month = 0 . $month;	
	$short_month = strtoupper(date('M',strtotime("$year-$month-01")));
	$month_str = $VM_LANG->_($short_month);	
	$months["$year-$month"] = $month_str.' - '.$year;	
	if($date == "$year-$month"){
		$current = $month_str.' - '.$year;
	}
	$month++;
}


/**
 * Get the properties
 */
$type = vmGet($_REQUEST,'type');
$dbp = new ps_DB();
$dbp->setQuery("SELECT p.id, p.name, p2.value FROM #__hp_properties p
			LEFT JOIN #__hp_properties2 p2 ON p2.property = p.id
			WHERE p.published = 1
			AND p2.field = 22 "
			.($type ? " AND p.type = $type" : '')
			." ORDER BY p2.value, p.name");
$properties = $dbp->loadObjectList('id');


//var_dump($dbp->_database->_sql);
/**
 * Get the roder between 2 dates
 */
$from = "$date-01";

$unixFrom = strtotime($from.' 00:00:00');
$to = date('Y-m-t',$unixFrom+86400);
$unixTo = strtotime($to);
$orders = $ps_order_booking->getOrders('C',$from,$to,'',1)->loadObjectList();



foreach($orders as $order){
	
	$propid = $order->property_id;
	if(isset($properties[$propid])){
		if(!isset($properties[$propid]->orders)) $properties[$propid]->orders = array();
		
		
		$checkdate = $order->arrival;
		while($checkdate <= $order->departure){
						
			$unixNow = strtotime($checkdate);
			$now = date('Y-m-d',$unixNow);
			$tmp = new stdClass();
			$tmp->starts = $now == $order->arrival;
			$tmp->ends = $now == $order->departure;
			$tmp->date = $now;
			
			//Check if another booking has set data for "today"
			if(isset($properties[$propid]->orders[$now])){
				$old = $properties[$propid]->orders[$now];
				if($old->starts) $tmp->starts = 1;
				if($old->ends) $tmp->ends = 1;
			}
			//Store booking data
			$properties[$propid]->orders[$now] = $tmp;	
			
			//Increment date
			$checkdate = date('Y-m-d',strtotime('+1 day',$unixNow));			
		}
	}	
}

echo '<div class="vb_availability_calendar">';

// Create the List Object with page navigation
$listObj = new listFactory();

$pagetitle = JText::_('_AVAILABILITY_CALENDAR_TITLE');

// print out the search field and a list heading
$listObj->writeSearchHeader($pagetitle.' '.$current, '', $modulename, "availability_calendar");

//Output the month drop down
echo ps_html::selectList( 'date', $date, $months, 1, '', 'id="date_select" onchange="document.adminForm.submit()"' );


echo '
<ul class="vb_key">
	<li class="key">Key: </li>
	<li class="available"><span></span>Available</li>
	<li class="booked"><span></span>Booked</li>
	<li class="arrival"><span></span>Checkin: 4pm</li>
	<li class="departure"><span></span>Checkout: 10am</li>
	<li class="clear"></li>
</ul>';

// start the list table
$listObj->startTable();

// these are the columns in the table
$columns = array('Property'=>'style="border-bottom: 1px solid #ccc; text-align: left;"');
for($i = date('d',$unixFrom); $i <= date('d',$unixTo);  $i += 1){
	$i = strlen($i) == 1 ? 0 . $i : $i;
	$columns[$i] = '';	
}
$listObj->writeTableHeader( $columns );

foreach($properties as $prop){
	$cols = ( ( strtotime( $to ) - strtotime( $from ) ) / 86400 ) + 1;
	if( $prop->value != $num_rooms ) :
		$listObj->newRow();
		// Add the rooms title, 6 rooms or more gets appended with a '+'
		// $listObj->addCell( $prop->value . ( $prop->value >= 6 ? '+' : '' ) . ' ' . $VM_LANG->_( 'BEDROOM' . ( $prop->value > 1 ? 'S' : '' ) ), 'class="property"' );
		$listObj->addCell( $prop->value . ' ' . $VM_LANG->_( 'BEDROOM' . ( $prop->value > 1 ? 'S' : '' ) ), 'class="property"' );
		$listObj->addCell( '', 'colspan="' . $cols . '"' );
		$num_rooms = $prop->value;
	endif;
	
	$listObj->newRow();
	
	$url = sefRelToAbs('index.php?option=com_hotproperty&task=view&id='.$prop->id);
	// The property
	$listObj->addCell( "<div><a href='$url'>$prop->name</a></div>", 'class="property"' );
	
	//Loop through the days
	$checkdate = $from;
	while($checkdate <= $to){
		
		$unixNow = strtotime($checkdate);
		$todayDate = date('d',$unixNow);
		if(isset($prop->orders) && array_key_exists($checkdate,$prop->orders)){		
			$class = '';
			$today = $prop->orders[$checkdate];
			
			//Check if we have an overlap or start/end of booking
			if($today->ends && $today->starts){
				$class = 'overlap';
			}
			else if($today->starts)
			{				
				$class = 'arrival';
			}
			else if($today->ends)
			{				
				$class = 'departure';								
			}
			$listObj->addCell( $todayDate , "class='booked $class'" );
			
		}else{		
			$listObj->addCell( $todayDate );
		}
		//Increment date
		$checkdate = date('Y-m-d',strtotime('+1 day',$unixNow));
	}
	
}

$listObj->writeTable();

$listObj->endTable();

$listObj->writeFooter( '' );

echo '</div>';
?>
