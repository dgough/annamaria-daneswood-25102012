<?php

if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );

require_once(CLASSPATH.'ps_user.php');
$ps_user = new ps_user();

$db = new ps_DB();
$db->query("SELECT * FROM zOldUsers");

while($db->next_record()){

	//Country mapping
	switch(trim($db->f('country'))){
	
		case 'United Kingdom':
		case 'England. UK':
		case 'England':
		case 'Great Britain':
		case 'Ireland':
		case 'Manchester':
		case 'Northern Ireland':
		case 'Scotland':
		case 'U.K.':
		case 'UK':
		case 'United Kingdom':
		case 'United Kingdon':
		case 'Wales, UK':
			$ctry = "GBR";
			break;
	
		case 'Belgium':
				$ctry = 'BEL';
				break;
		case 'Bermuda':
				$ctry = 'BMU';
				break;
				
		case 'CA':
		case 'Canada':
				$ctry = 'CAN';
				break;
		case 'Erie':
				$ctry = 'IRL';
				break;
		case 'Germany':
				$ctry = 'DEU';
				break;
	
		case 'Switzerland':
				$ctry = 'CHE';
				break;
	
		case 'Netherlands':
		case 'The Netherlands':
				$ctry = 'NLD';
				break;
	
		case 'IL':		
		case 'america':
		case 'U.S.':
		case 'U.S.A.':
		case 'United States':
		case 'US':
		case 'USA':
				$ctry = 'USA';
				break;
				
		default:
		case '':
				$ctry = '';
			break;
	}

	if(strlen(trim($db->f('state'))) == 2){
		$state = trim($db->f('state'));
	}else{
		$state = '';
	}

	$d = array();
	$d["name"] = $db->f('fname').' '.$db->f('lname');
	$d["username"] = strtolower(preg_replace("/[^a-z\d]/i",'',$db->f('fname').$db->f('lname')));
	$d["email"] = $db->f('email');
	$d["password"] = $db->f('password');
	$d["password2"] = $db->f('password');
	$d["gid"] = "18";
	$d["block"] = "0";
	$d["params"] = '';
	$d["id"] = "0";
	$d["cid"] = array(0);
	$d["contact_id"] = "";
	$d["sendEmail"] = "0";
	$d["vendor_id"] = "1";
	$d["perms"] = "shopper";
	$d["customer_number"] = "";
	$d["shopper_group_id"] = "5";
	$d["company"] = "";
	$d["title"] = "";
	$d["first_name"] = $db->f('fname');
	$d["last_name"] = $db->f('lname');
	$d["middle_name"] = "";
	$d["address_1"] = $db->f('address');
	$d["address_2"] = '';
	$d["city"] = $db->f('city');
	$d["zip"] = $db->f('zipcode');
	$d["country"] = $ctry;
	$d["state"] = $state;
	$d["phone_1"] = $db->f('phond');
	$d["phone_2"] = $db->f('phone');
	$d["fax"] = $db->f('phoneC');
	$d["address_type"] = "BT";
	$d["address_type_name"] = "-default-";
	$d["user_id"] = "0";
	$d["vmtoken"] = vmSpoofValue($GLOBALS['sess']->getSessionId());
	$d["func"] = "userAdd";
	$d["page"] = "admin.user_import";
	$d["task"] = "save";
	$d["option"] = "com_virtuemart";
	$d["no_menu"] = "0";
	$d["no_toolbar"] = "0";
	$d["only_page"] = "0";
	$d["pshop_admin"] = "admin";
	$d["admin_login"] = "b3JnYW5pY3w1NDhmZDQwNjg5M2IwNzljNDIyMDgxY2JjMGJlOThjZnwxYTY0ZjBhYjZiMmMzMjBmNTIzMDRjOGYyMjIyZjdkNg==";
	$d["vmLayout"] = "standard";
	$d["cod"] = "1.12.13.16.17.19.20.22.23.25.26.29.30.32.43.45.47.52.54.56.58.59";
	$d["user_info_id"] = "";
	$_REQUEST = $d;
	$_POST = $d;
	$ps_user->add($d);
}



?>