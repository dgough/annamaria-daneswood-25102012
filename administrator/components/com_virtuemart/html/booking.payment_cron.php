<?php

/**
 * VirtueBook: Cron script to send emails to users when they have payments due
 */

global $ps_booking, $sess, $vmLogger, $VM_LANG;

$VM_LANG->load('order_emails');

error_reporting(E_ALL);
ini_set('display_errors','On');

/**
 * Get vendor information
 */
$dbv = new ps_DB;
$qt = "SELECT * from #__{vm}_vendor ";
/* Need to decide on vendor_id <=> order relationship */
$qt .= "WHERE vendor_id = '".vmGet( $_SESSION, 'ps_vendor_id', 1 )."'";
$dbv->query($qt);
$dbv->next_record();

$vendor_email = $dbv->f("contact_email");
$vendor_name = $dbv->f("vendor_name");

$imagefile = pathinfo($dbv->f("vendor_full_image"));
$extension = $imagefile['extension'] == "jpg" ? "jpeg" : "jpeg";

$EmbeddedImages = array();
$EmbeddedImages[] = array(	'path' => IMAGEPATH."vendor/".$dbv->f("vendor_full_image"),
					'name' => "vendor_image", 
					'filename' => $dbv->f("vendor_full_image"),
					'encoding' => "base64",
					'mimetype' => "image/".$extension );


/**
 * Get outstanding payments
 */
$db = new ps_DB();
$q = "
SELECT p.*, ui.*, b.*, o.*, prop.name as property, prop.*, round(sum(p.payment_amount), 2) AS total_due, round(o.order_total, 2) AS order_total 
FROM #__{vm}_order_payment AS p
LEFT JOIN #__{vm}_orders AS o ON o.order_id = p.order_id
LEFT JOIN #__{vm}_order_booking AS b ON o.order_id = b.order_id
LEFT JOIN #__{vm}_user_info AS ui ON ui.user_id = o.user_id
LEFT JOIN #__hp_properties AS prop ON prop.id = b.property_id
WHERE p.payment_due <= now() AND (p.payment_cleared = '' OR ISNULL(p.payment_cleared) OR p.payment_cleared = '0000-00-00')
AND p.payment_amount > 0
AND o.user_id != ''
AND b.order_id != ''
AND o.order_status = 'C'
AND b.arrival >= now()
GROUP BY o.order_id
ORDER BY o.order_id ASC";

$db->query($q);


while($db->next_record()){

	$customer_email = $db->f('user_email');
	$customer_name = $db->f('first_name').' '.$db->f('last_name');

	if($customer_email){
		$subject = sprintf('%s - Payment due for order %s', $vendor_name, $db->f('order_id'));
		
		$tpl = vmTemplate::getInstance();
		$tpl->set_vars(array('customer' => ($db->f('title') ? $db->f('title') : '').$db->f('first_name').' '.$db->f('last_name'),
							'property' => $db->f('property')."\n".$db->f('address')."\n".$db->f('suburb')."\n".$db->f('state')."\n".$db->f('country')."\n".$db->f('postcode')."\n",
							'arrival' => $db->f('arrival'),			
							'departure' => $db->f('departure'),
							'people' => $db->f('people'),
							'amount' => $GLOBALS['CURRENCY_DISPLAY']->getFullValue( $db->f('total_due') ),
							'payment_url' => $sess->url( "index.php?page=order.payment_list&order_id=".$db->f("order_id") ),
							'order_url' => $sess->url( "index.php?page=account.index" ),
							'order_id' => $db->f('order_id'),
							'subject' => $subject,
							'db' => $db
						));
	
		$mail_Body = $tpl->fetch('order_emails/payment_email.tpl.php');
		$mail_AltBody = $tpl->fetch('order_emails/payment_email.html.tpl.php');
		
		if(@ORDER_MAIL_HTML){
		 	$success = vmMail( $vendor_email, $vendor_name, $customer_email, $subject, $mail_AltBody, $mail_Body, true, null, null, $EmbeddedImages);
		 	vmMail( $customer_email, $customer_name, $vendor_email, $subject, $mail_AltBody, $mail_Body, true, null, null, $EmbeddedImages);
		}else{
		 	$success = vmMail( $vendor_email, $vendor_name, $customer_email, $subject, $mail_Body );
		 	vmMail( $customer_email, $customer_name, $vendor_email, $subject, $mail_Body );
		}
		
		if(!$success){		
			$vmLogger->err( 'Something went wrong while sending the payment email to '.$db->f('first_name').' '.$db->f('last_name').' at '.$customer_email );
		}else{
			$vmLogger->info( 'Email successfully sent to: '.$db->f('first_name').' '.$db->f('last_name').' at '.$customer_email );
		}
	}
}

exit('Finished');
?>