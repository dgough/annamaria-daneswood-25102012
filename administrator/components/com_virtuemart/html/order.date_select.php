<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 

global $option, $vbDateFrom, $vbDateTo, $vbViewAll,$page, $keyword, $mainframe, $sess;

if(!isset($mainframe->_userstate)){
	$mainframe->_userstate = array();
}

//Get the date from the session / request
$property_id = $mainframe->getUserStateFromRequest($option.'property', 'property_id','');
$vbDateFrom = $mainframe->getUserStateFromRequest($option.'vbDateFrom', 'vbDateFrom',date('Y-m-01'));
$vbDateTo = $mainframe->getUserStateFromRequest($option.'vbDateTo', 'vbDateTo',date('Y-m-t'));
$vbViewAll = vmGet($_REQUEST,'viewAll') != '';
if($vbViewAll){
	$mainframe->setUserState($option.'vbDateFrom', null);
	$mainframe->setUserState($option.'vbDateTo', null);
	$_REQUEST['vbDateFrom'] = $vbDateFrom = null;
	$_REQUEST['vbDateTo'] = $vbDateTo = null;
}

if($page == 'order.booking_overview'){
	$vbDateFrom = date('Y-m-01',strtotime($vbDateFrom));
	$vbDateTo = date('Y-m-t',strtotime($vbDateFrom));
}

//Get unix date from and to
$vbDateFromUnix = strtotime($vbDateFrom);
$vbDateToUnix = strtotime($vbDateTo);

if($keyword){
	$vbDateFrom = '';
	$vbDateTo = '';
}

$year = date('Y',strtotime($vbDateFrom));
$month = date('m',strtotime($vbDateFrom));
$show = vmGet($_REQUEST,'show');
?>
<div id="date-range-select">
	<form action="<?php echo vmGet($_SERVER,'PHP_SELF',''); ?>" method="get">
	<input type='hidden' name='option' value="<?php echo $option ?>" />
	<input type='hidden' name='page' value="<?php echo $page ?>" />
	<input type='hidden' name='keyword' value="<?php echo $keyword ?>" />
	<input type='hidden' name='property_id' value="<?php echo $property_id ?>" />
	<input type='hidden' name='limit' value="9999" />
	<input type='hidden' name='limitstart' value="0" />
	
<?php
echo vmCommonHTML::ScriptTag( $mosConfig_live_site .'/includes/js/calendar/calendar.js');
if( class_exists( 'JConfig' ) ) {
// in Joomla 1.5, the name of calendar lang file is changed...
	echo vmCommonHTML::ScriptTag( $mosConfig_live_site .'/includes/js/calendar/lang/calendar-en-GB.js');
} else {
	echo vmCommonHTML::ScriptTag( $mosConfig_live_site .'/includes/js/calendar/lang/calendar-en.js');
}
echo vmCommonHTML::linkTag( $mosConfig_live_site .'/includes/js/calendar/calendar-mos.css');
?>
	<?php if($page != 'order.booking_overview'){ ?>
	<div style="text-align :right">
		<label>Select Date Range : </label>
		<span><input class="inputbox" type="text" name="vbDateFrom" id="vbDateFrom" size="10" maxlength="19" value="<?php echo $vbDateFrom; ?>" /> <input name="reset" type="reset" class="button" onClick="return showCalendar('vbDateFrom', 'y-mm-dd');" value="..." /></span>
		<span><input class="inputbox" type="text" name="vbDateTo" id="vbDateTo" size="10" maxlength="19" value="<?php echo $vbDateTo; ?>" /> <input name="reset" type="reset" class="button" onClick="return showCalendar('vbDateTo', 'y-mm-dd');" value="..." /></span>
        <span><input type="submit" value=" Go " class="button"  /></span>
    </div>
    <?php } ?>  	
    </form>
</div>
<?php if($vbDateFrom && $vbDateTo){ ?>
<div id="list-years">
<?php
	$yr_tmp_url = '<a href="'.vmget($_SERVER,'PHP_SELF').'?option=com_virtuemart&page='.$page.'&vbDateFrom=%s&vbDateTo=%s&keyword='.$keyword.'&show='.$show.'&property_id='.$property_id.'">%s</a>';
		
	$yr_list = new listFactory();
	$yr_list->startTable();
	
	$lastYear = date('Y',$vbDateFromUnix)-1;
	$thisYear = date('Y',$vbDateFromUnix);
	$nextYear = date('Y',$vbDateFromUnix) + 1;
	
	$yr_columns = array(sprintf($yr_tmp_url,date("$lastYear-m-d",$vbDateFromUnix),date("$lastYear-m-d",$vbDateToUnix),$lastYear) => 'width="30%" style="text-align:center"',
						sprintf($yr_tmp_url,date("$thisYear-m-d",$vbDateFromUnix),date("$thisYear-m-d",$vbDateToUnix),$thisYear) => 'width="30%" style="text-align:center"',
						sprintf($yr_tmp_url,date("$nextYear-m-d",$vbDateFromUnix),date("$nextYear-m-d",$vbDateToUnix),$nextYear) => 'width="30%" style="text-align:center"',
						);
	$yr_list->writeTableHeader( $yr_columns );
	$yr_list->writeTable();
	$yr_list->endTable();
?>
</div>
<div id="list-months">
<?php 
	$mn_tmp_url = '<a href="'.vmget($_SERVER,'PHP_SELF').'?option=com_virtuemart&page='.$page.'&vbDateFrom=%s&vbDateTo=%s&keyword='.$keyword.'&show='.$show.'&property_id='.$property_id.'">%s</a>';
	
	$mn_list = new listFactory();
	$mn_list->startTable();
	$mn_columns = array(sprintf($mn_tmp_url,"$thisYear-01-01","$thisYear-01-31",'Jan') => '',
						sprintf($mn_tmp_url,"$thisYear-02-01","$thisYear-02-28",'Feb') => '',
						sprintf($mn_tmp_url,"$thisYear-03-01","$thisYear-03-31",'Mar') => '',
						sprintf($mn_tmp_url,"$thisYear-04-01","$thisYear-04-30",'Apr') => '',
						sprintf($mn_tmp_url,"$thisYear-05-01","$thisYear-05-31",'May') => '',
						sprintf($mn_tmp_url,"$thisYear-06-01","$thisYear-06-30",'Jun') => '',
						sprintf($mn_tmp_url,"$thisYear-07-01","$thisYear-07-31",'Jul') => '',
						sprintf($mn_tmp_url,"$thisYear-08-01","$thisYear-08-31",'Aug') => '',
						sprintf($mn_tmp_url,"$thisYear-09-01","$thisYear-09-30",'Sep') => '',
						sprintf($mn_tmp_url,"$thisYear-10-01","$thisYear-10-31",'Oct') => '',
						sprintf($mn_tmp_url,"$thisYear-11-01","$thisYear-11-30",'Nov') => '',
						sprintf($mn_tmp_url,"$thisYear-12-01","$thisYear-12-31",'Dec') => '');						
	$mn_list->writeTableHeader( $mn_columns );
	$mn_list->writeTable();
	$mn_list->endTable();
?>
</div>
<?php } ?>
<form action='<?php echo vmGet($_SERVER,'PHP_SELF') ?>' method="get" id='current-date-range' style="text-align: center; padding: 5px; font-size: 120%">
<a style='float: right' href='<?php echo vmGet($_SERVER,'PHP_SELF')."?option=com_virtuemart&page=$page&vbDateFrom=".date('Y-m-01',strtotime('+1 months',$vbDateFromUnix)).'&vbDateTo='.date('Y-m-t',strtotime('+1 months',$vbDateFromUnix)).'&property_id='.$property_id ?>'> >>> </a>
<a style='float: left' href='<?php echo vmGet($_SERVER,'PHP_SELF')."?option=com_virtuemart&page=$page&vbDateFrom=".date('Y-m-01',strtotime('-1 months',$vbDateFromUnix)).'&vbDateTo='.date('Y-m-t',strtotime('-1 months',$vbDateFromUnix)).'&property_id='.$property_id ?>'> <<< </a>

	<?php if($vbDateFrom && $vbDateTo){ ?>
		<input type="submit" name="viewAll" value="View All" /><br /><br />
		You are currently viewing: <strong><?php echo $vbDateFrom ?></strong> to <strong><?php echo $vbDateTo ?></strong>
	<?php }else{
		?>
		<input type="submit" value="Back this month" /><br /><br />
		You are currently viewing: <strong>All Bookings</strong>
		<?php
	} ?>
	<input type="hidden" name='vbDateFrom' value="<?php echo date('Y-m-01') ?>" />
	<input type="hidden" name='vbDateTo' value="<?php echo date('Y-m-t') ?>" />
	<input type="hidden" name='property_id' value="<?php echo $property_id ?>" />
	<input type="hidden" name='option' value="com_virtuemart" />
	<input type="hidden" name='page' value="<?php echo $page ?>" />
</div>
</form>
<br style='clear: both'/>