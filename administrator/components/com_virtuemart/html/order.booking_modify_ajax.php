<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 

mm_showMyFileName( __FILE__ );

global $page, $ps_order_status, $my, $mosConfig_live_site, $mosConfig_absolute_path, $ps_booking;

require_once(CLASSPATH.'connectionTools.class.php');

$task 	= vmget($_REQUEST,'task','');
$action = vmget($_REQUEST,'action','');

if ($action=='ajax') {
	
	switch($task) {
		case 'price':
									
			$dateFrom 	= vmGet( $_REQUEST, "dateFrom", 0);
			$dateTo		= vmGet( $_REQUEST, "dateTo", 0);
			$propertyid = vmGet( $_REQUEST, "propertyid", 0);
			
			$priceinfo = $ps_booking->getPrice(0,0,1);
			$numnights = $ps_booking->getNights();

			if ($ps_booking->status==1) {
				produceXML($ps_booking->status, $ps_booking->getMessages(0), $priceinfo['total'], $priceinfo['subtotal'], $priceinfo['tax']);
			} else { // got season price info back which is only a price from and to
				produceXML(0, $ps_booking->getMessages(0), 0);
			}
					
			
		break;
	}
		
} else {
	
}

function produceXML($status = 0, $message = '', $total = '', $subtotal = '', $tax = ''){
	
	$xml = '<xml>';
	$xml .= "<status><![CDATA[$status]]></status>\n";
	$xml .= "<message><![CDATA[$message]]></message>\n";
	$xml .= "<total><![CDATA[$total]]></total>\n";
	$xml .= "<subtotal><![CDATA[$subtotal]]></subtotal>\n";
	$xml .= "<tax><![CDATA[$tax]]></tax>\n";
	$xml .= '</xml>';
	
	//Send XML
	vmConnector::sendHeaderAndContent(200,$xml,'text/xml');
	exit();
}

?>