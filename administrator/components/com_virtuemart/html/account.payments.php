<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );
/*
 * @version $Id: order.label_image.php 1122 2008-01-07 14:52:31Z thepisu $
 * @package VirtueMart
 * @subpackage html
 */
mm_showMyFileName(__FILE__);

require_once( CLASSPATH . "htmlTools.class.php" );
require_once( CLASSPATH . "ps_payments.php" );

global $ps_booking;

$order_id = vmget($_REQUEST, 'order_id', '');
$ps_payments = new ps_payments();
if(!$order_id){
	$GLOBALS['vmLogger']->err("No booking id specified.");
	return false;
}
?>
<h3><?php echo 'Booking Payments' ?></h3>
<?php  echo 'Welcome '. $auth["first_name"] . " " . $auth["last_name"] ?>
<br />
<br />
<?php
$ps_payments->getCustomerPayments($order_id);
?>