<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );
/**
*
* @version $Id: order.order_print.php 1354 2008-04-05 19:54:07Z gregdev $
* @package VirtueMart
* @subpackage html
* @copyright Copyright (C) 2004-2008 soeren - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
mm_showMyFileName( __FILE__ );

global $limitstart, $limit, $modulename, $keyword, $vmuser, $sess;

require_once( CLASSPATH . "pageNavigation.class.php" );
require_once( CLASSPATH . "htmlTools.class.php" );
require_once( CLASSPATH . "ps_order_booking.php" );

//Get the users from the DB
$db = new ps_db();
$db->query("SELECT u.id AS user_id, IF(CONCAT(ui.first_name,ui.last_name) = '', u.name, CONCAT(ui.first_name,' ',ui.last_name)) AS user, ui.company , sum(m.amount) as total, ((sum(m.hours) * 60)+sum(m.minutes))/60 as time 
			FROM #__{vm}_user_info AS ui
			LEFT JOIN #__{vm}_maintenance AS m ON m.user_id = ui.user_id 
			LEFT JOIN #__users AS u ON u.id = ui.user_id 
			WHERE perms = 'maintenance'
			GROUP BY ui.user_id
			ORDER BY ui.last_name");

//Get then number of bookings
$num_rows = $db->num_rows();


// Create the Page Navigation
$pageNav = new vmPageNav( $num_rows, $limitstart, $limit );

// Create the List Object with page navigation
$listObj = new listFactory( $pageNav );

// print out the search field and a list heading
$listObj->writeSearchHeader('Maintenance People', VM_THEMEURL.'images/administration/dashboard/orders.png', $modulename, '');

$listObj->startTable();

$columns = array(
			"#" => "width=\"20\"", 	
			'Name'=>'',
			'Company'=>'',
			'Total time (hours)'=>'',
			'Total Billed'=>''
			);


$listObj->writeTableHeader( $columns );

$i = 0;
$maintenanceTotal = 0;
while($db->next_record()){
	
	$listObj->newRow();
	$id = $db->f('user_id');
	$url = '<a href="'.$sess->url($_SERVER['PHP_SELF']."?page=maintenance.user_form&user_id=$id").'">%s</a>';
	
	// The row number
	$listObj->addCell( sprintf($url,$pageNav->rowNumber( $i )) );
	
	//Maintenance person
	$listObj->addCell( sprintf($url,$db->f('user')) );
	
	//Company
	$listObj->addCell( $db->f('company') );
	
	//Total time
	$listObj->addCell( $db->f('time') );
	
	//Total billed
	$listObj->addCell( $GLOBALS['CURRENCY_DISPLAY']->getFullValue($db->f('total')) );
	
	$i++;
}

$listObj->writeTable();

$listObj->endTable();

echo '<input type="hidden" name="toggle" value="0" onclick="" />';

$listObj->writeFooter( $keyword, "&vbDateFrom=$vbDateFrom&vbDateTo=$vbDateTo&show=$show" );

?>