<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
/**
*
* @version $Id: order.order_list.php 1227 2008-02-08 12:09:50Z soeren_nb $
* @package VirtueMart
* @subpackage html
* @copyright Copyright (C) 2004-2007 soeren - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
mm_showMyFileName( __FILE__ );
global $page, $ps_order_status, $my;

$show = vmGet( $_REQUEST, "show", "" );
$form_code = "";
require_once( CLASSPATH . "pageNavigation.class.php" );
require_once( CLASSPATH . "htmlTools.class.php" );

//If user is an owner, only select their properties
$prop_ids = array();
if($my->usertype == 'Agent' && !in_array($my->gid, array(23,24,25))){
	$db->query("SELECT p.id from jos_hp_properties AS p, jos_hp_agents as a WHERE a.id = p.agent AND a.user = '$my->id'");
	while($db->next_record()){
		$prop_ids[] = $db->f("id");
	}
	$q .= "AND booking.booking_property_id IN(".implode(',',$prop_ids).")";
}else if(!in_array(strtolower($my->usertype), array('super administrator','administrator','manager'))){
	$GLOBALS['vmLogger']->err( 'You do not have permission to manage bookings.' );
	return;
}

if($prop_id && empty($prop_ids)) $prop_ids[] = $prop_id;

$list  = "SELECT #__{vm}_orders.order_id,order_status, #__{vm}_orders.cdate,#__{vm}_orders.mdate,order_total,order_currency,#__{vm}_orders.user_id,";
$list .= "first_name, last_name, booking.*, p.name as property 
		FROM #__{vm}_orders, #__{vm}_order_user_info , #__{vm}_bk_order AS booking 
		LEFT JOIN #__hp_properties AS p ON p.id = booking.booking_property_id "; //Organic ADD
$list .= "WHERE #__{vm}_orders.order_id = booking.booking_order_id AND ";//Organic ADD
$count = "SELECT count(*) as num_rows FROM #__{vm}_orders, #__{vm}_order_user_info WHERE ";
$q = "address_type = 'BT' AND ";
if (!empty($keyword)) {
        $q  .= "(#__{vm}_orders.order_id LIKE '%$keyword%' ";
        $q .= "OR #__{vm}_orders.order_status LIKE '%$keyword%' ";
        $q .= "OR first_name LIKE '%$keyword%' ";
        $q .= "OR last_name LIKE '%$keyword%' ";
		$q .= "OR CONCAT(`first_name`, ' ', `last_name`) LIKE '%$keyword%' ";
        $q .= ") AND ";
}
if (!empty($show)) {
	$q .= "order_status = '$show' AND ";
}
$q .= "(#__{vm}_orders.order_id=#__{vm}_order_user_info.order_id) ";
$q .= "AND #__{vm}_orders.vendor_id='".$_SESSION['ps_vendor_id']."' ";
$q .= (count($prop_ids) > 0) ? "AND booking.booking_property_id IN(".implode(',',$prop_ids).")" : ''; /* Organic add, only show agents bookings */
$q .= "ORDER BY #__{vm}_orders.cdate DESC ";
$list .= $q . " LIMIT $limitstart, " . $limit;
$count .= $q;   

$db->query($count);
$db->next_record();
$num_rows = $db->f("num_rows");
  
// Create the Page Navigation
$pageNav = new vmPageNav( $num_rows, $limitstart, $limit );

// Create the List Object with page navigation
$listObj = new listFactory( $pageNav );

// print out the search field and a list heading
$listObj->writeSearchHeader($VM_LANG->_('PHPSHOP_ORDER_LIST_LBL'), VM_THEMEURL.'images/administration/dashboard/orders.png', $modulename, "order_list");

//Check access rights
$canDelete = 0;
if($my->gid > 22 && $my->gid < 26){
	$canDelete = 1;
}

?>
<div align="center">
<?php
$navi_db = new ps_DB;
$q = "SELECT order_status_code, order_status_name ";
$q .= "FROM #__{vm}_order_status WHERE vendor_id = '$ps_vendor_id'";
$navi_db->query($q);
while ($navi_db->next_record()) {  ?> 
  <a href="<?php $sess->purl($_SERVER['PHP_SELF']."?page=$modulename.order_list&show=".$navi_db->f("order_status_code")) ?>">
  <b><?php echo $navi_db->f("order_status_name")?></b></a>
      | 
<?php 
} 
?>
    <a href="<?php $sess->purl($_SERVER['PHP_SELF']."?page=$modulename.order_list&show=")?>"><b>
    <?php echo $VM_LANG->_('PHPSHOP_ALL') ?></b></a>
</div>
<br />
<?php 

$listObj->startTable();

// these are the columns in the table
$columns = Array(  "#" => "width=\"20\"", 					
					$VM_LANG->_('PHPSHOP_ORDER_LIST_ID') => '',
					$VM_LANG->_('PHPSHOP_ORDER_PRINT_NAME') => '',
					'Property' => '',
					//$VM_LANG->_('PHPSHOP_ORDER_LIST_PRINT_LABEL') => '',
					//$VM_LANG->_('PHPSHOP_ORDER_LIST_TRACK') => '',
					//$VM_LANG->_('PHPSHOP_ORDER_LIST_VOID_LABEL') => '',
					//$VM_LANG->_('PHPSHOP_CHECK_OUT_THANK_YOU_PRINT_VIEW') => '',
					$VM_LANG->_('PHPSHOP_ORDER_LIST_CDATE') => '',
					//$VM_LANG->_('PHPSHOP_ORDER_LIST_MDATE') => '',
					'Arrival Date' => '',
					'Departure Date' => '',
					$VM_LANG->_('PHPSHOP_ORDER_LIST_STATUS') => '',
					//$VM_LANG->_('PHPSHOP_UPDATE') => 'width="116px"',
					$VM_LANG->_('PHPSHOP_ORDER_LIST_TOTAL') => 'width="60px"'
					
				);
				
if($canDelete){
	$columns[$VM_LANG->_('E_REMOVE')] = "width=\"5%\"";
}
$listObj->writeTableHeader( $columns );
// so we can determine if shipping labels can be printed
$dbl =& new ps_DB;

$db->query($list);

$i = 0;
while ($db->next_record()) {
    
	$listObj->newRow();
	
	// The row number
	$listObj->addCell( $pageNav->rowNumber( $i ) );
	
	// The Checkbox
	#$listObj->addCell( vmCommonHTML::idBox( $i, $db->f("order_id"), false, "order_id" ));
	
	$url = $_SERVER['PHP_SELF']."?page=$modulename.booking_details&limitstart=$limitstart&keyword=".urlencode($keyword)."&order_id=". $db->f("order_id");
	$tmp_cell = "<a href=\"" . $sess->url($url) . "\">".sprintf("%08d", $db->f("order_id"))."</a><br />";
	$listObj->addCell( $tmp_cell );
	
	$tmp_cell = $db->f('first_name').' '.$db->f('last_name');
	if( $perm->check('admin') && defined('_VM_IS_BACKEND')) {
		$url = $_SERVER['PHP_SELF']."?page=admin.user_form&amp;user_id=". $db->f("user_id");
		$tmp_cell = ($my->gid > 23 && $my->gid < 26) ? '<a href="'.$sess->url( $url ).'">'.$tmp_cell.'</a>' : $tmp_cell; //Organic mod: make user name uneditable for frontend users
	}
	
	$listObj->addCell( $tmp_cell );
	
	//Property name
	$listObj->addCell( $db->f('property'));
	
	// Creation Date
	$listObj->addCell(strftime("%d-%b-%y %H:%M", $db->f("cdate")));
	// Last Modified Date
    //$listObj->addCell( strftime("%d-%b-%y %H:%M", $db->f("mdate")));
    
	//Arrival date
    $listObj->addCell(strftime('%d-%b-%y',strtotime($db->f('booking_dateFrom'))));
    
    //Departure date
    $listObj->addCell(strftime('%d-%b-%y',strtotime($db->f('booking_dateTo'))));
    
    //Status
    $listObj->addCell($db->f("order_status"));
    
	$listObj->addCell( $GLOBALS['CURRENCY_DISPLAY']->getFullValue($db->f("order_total"), '', $db->f('order_currency')));
	// Change Order Status form
	$form_code .= '<form style="float:left;" method="post" action="'. $_SERVER['PHP_SELF'] .'" name="adminForm'. $i .'">';
	$form_code .= $ps_order_status->getOrderStatusList($db->f("order_status"), "style=\"visibility:hidden;\" onchange=\"document.adminForm$i.changed.value='1'\"");
	$form_code .= '<input type="hidden" class="inputbox" name="notify_customer" value="N" />
		<input type="hidden" name="page" value="order.order_list" />
		<input type="hidden" name="func" value="orderStatusSet" />
		<input type="hidden" name="vmtoken" value="'. vmSpoofValue($sess->getSessionId()) .'" />
		<input type="hidden" name="changed" value="0" />
		<input type="hidden" name="option" value="com_virtuemart" />
		<input type="hidden" name="order_id" value="'. $db->f("order_id") .'" />
		<input type="hidden" name="current_order_status" value="'. $db->f("order_status").'" />
		</form>';
	
    // Delete Order Button
    if($canDelete){
		$listObj->addCell( $ps_html->deleteButton( "order_id", $db->f("order_id"), "orderDelete", $keyword, $limitstart ) );
    }

	$i++; 
}
$listObj->writeTable();
#"<input type=\"hidden\" name=\"toggle\" value=\"\" onclick=\"checkAll(".$num_rows.")\" />" => "width=\"20\"",

$listObj->endTable();

$listObj->writeFooter( $keyword, "&show=$show&toggle=0" );

echo $form_code;
?>