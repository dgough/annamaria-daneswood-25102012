<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 

/**
*
* @version $Id: tax.tax_list.php 1099 2007-12-21 12:46:46Z soeren_nb $
* @package VirtueMart
* @subpackage html
* @copyright Copyright (C) 2004-2007 soeren - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
mm_showMyFileName( __FILE__ );
/*
error_reporting(E_ALL);
ini_set('display_errors','On');
*/


require_once( CLASSPATH . "ps_product_attribute.php");
require_once( CLASSPATH . "ps_product.php" );

$ps_booking->process(0);
$ps_booking->updateSession();

//Check if we have a property, if not, end here
if(!isset($ps_booking->property_data)) return;

$task = vmget($_REQUEST,'task','');
$action = vmget($_REQUEST,'action','');
$_SESSION['total_counter'] = 0;

$outputProperty = outputProperty($ps_booking);
$outputForm = outputForm($ps_booking);
$outputExtras = outputExtras($ps_booking);
$outputTotalTop = outputTotal($ps_booking);
$outputTotalBottom = outputTotal($ps_booking);

//Check if we are returning an AJAX response or normal html

if($action == 'ajax'){
	//Return ajax response
	switch($task){
		
		case 'main_form':
			produceXML($ps_booking->status, $ps_booking->getMessages(), $outputForm, $outputExtras, $outputTotalTop, $outputTotalBottom);
			break;
			
		case 'extras_form':
			produceXML($ps_booking->status, $ps_booking->getMessages(), $outputForm, $outputExtras, $outputTotalTop, $outputTotalBottom);
			break;
			
		case '':
		default:
			//Output main form here
			produceXML(0, 'Please supply an AJAX task');
			break;
	}

}else{
	//Return normal HTML

		$tpl = vmTemplate::getInstance();
		$tpl->set_vars(array('outputProperty' => $outputProperty,
							'outputForm' => $outputForm,
							'enableExtras' => (isset($ps_booking->extrasObjects) && count($ps_booking->extrasObjects) > 0),
							'outputExtras' => $outputExtras,
							'outputTotalTop' => $outputTotalTop,
							'outputTotalBottom' => $outputTotalBottom,
							'status' => $ps_booking->status
						)
					);
		
		echo $tpl->fetch('booking/booking.wrapper.tpl.php');
}

function outputProperty($ps_booking) {

	global $sess;
	
	include(dirname(__FILE__).'/../../com_hotproperty/config.hotproperty.php');
	
	$prop = $ps_booking->property_data;
	
	$tpl = vmTemplate::getInstance();
	$tpl->set_vars(array(
						'prop_id' => $prop->id,
						'prop_thumb' => $hp_imgdir_thumb.$prop->thumb,
						'prop_thumb_width' => $hp_imgsize_thumb,
						'prop_name' => $prop->name,
						'prop_url' => $sess->url('option=com_hotproperty&amp;task=view&amp;id='.$prop->id),
						'prop_desc' => $prop->intro_text,
						'prop' => $prop	
	));
	
	return $tpl->fetch('booking/booking.property.tpl.php');
}

function outputForm($ps_booking) {

	include(CLASSPATH.'ps_calendar.php');
	
	for ($i=1; $i < 21; $i++) {
		$people_list[$i] = $i;
	}
	
	$dateFrom = $ps_booking->dateFrom != '' ? $ps_booking->dateFrom : date('Y-m-d');
	$dateTo = $ps_booking->dateTo != '' ? $ps_booking->dateTo : date('Y-m-d',time()+86400);
	$tpl = vmTemplate::getInstance();
	$tpl->set_vars(array('propertyid' => $ps_booking->property_id,
						'dateFrom' => $dateFrom,
						'dateTo' => $dateTo,
						'arrivalAirport' => $ps_booking->arrivalAirport,
						'departureAirport' => $ps_booking->departureAirport,
						'arrivalFlightNo' => $ps_booking->arrivalFlightNo,
						'departureFlightNo' => $ps_booking->departureFlightNo,
						'people' => $ps_booking->people,
						'people_list' => $people_list,
						'isAdmin' => $ps_booking->admin,
						'isFree' => $ps_booking->free,
						'allowsPets' => $ps_booking->property_data->allows_pets,
						'petCost' => (defined('VB_PET_RATE') ? VB_PET_RATE : 0),
						'pets' => $ps_booking->hasPets
					)
				);
	
	return $tpl->fetch('booking/booking.form.tpl.php');
}

function outputExtras($ps_booking) {

	$ps_product_attribute = new ps_product_attribute();
	$extras = $ps_booking->getExtras();
	
	$extras_html = '';
	
	foreach ($extras as $extra) {
		$tpl = vmTemplate::getInstance();
		$tpl->set_vars(array('image' => ps_product::image_tag( $extra->product_thumb_image, 'class="browseProductImage" border="0" title="'.$extra->product_name.'" alt="'.$extra->product_name .'"'),
							'product_id' => $extra->product_id,
							'product_name' => $extra->product_name,
							'product_s_desc' => $extra->product_s_desc,
							'product_quantity' => $extra->product_quantity,
							'product_price' => $extra->price
						));
		$extras_html .= $tpl->fetch('booking/booking.extras_item.tpl.php');
	}
	
	$tpl = vmTemplate::getInstance();
	$tpl->set_vars(array('extras' => $extras_html));
	
	return $tpl->fetch('booking/booking.extras.tpl.php');
}

function outputTotal($ps_booking) {
	
	$tpl = vmTemplate::getInstance();
	$tpl->set_vars(array('total' => $ps_booking->total + $ps_booking->extras,
						'subtotal' => $ps_booking->subtotal + $ps_booking->extras,
						'original' => $ps_booking->original,						
						'discount_total' => $ps_booking->discount,
						'discounts' => $ps_booking->discounts,
						'extras' => $ps_booking->extras,
						'tax' => $ps_booking->tax,
						'tax_rate' => $ps_booking->tax_rate,
						'max' => $ps_booking->maxPrice,
						'security' => $ps_booking->securityDeposit,
						'cleaning' => $ps_booking->cleaning,
						'booking' => $ps_booking,
						'counter' => vmGet($_SESSION,'total_counter',0),
						
					));
	//The total outpuit is displayed twice, this allows us to display different things
	$_SESSION['total_counter']++;					
	
	return $tpl->fetch('booking/booking.total.tpl.php');
}

function produceXML($status = 0, $message = '', $form = '', $extras = '', $totaltop = '', $totalbottom = ''){
	
	$message_clean = str_replace("\n",'',str_replace("\r",'',strip_tags($message)));
	$xml = '<xml>';
	$xml .= "<status><![CDATA[$status]]></status>\n";
	$xml .= "<message><![CDATA[$message]]></message>\n";
	$xml .= "<message_clean><![CDATA[$message_clean]]></message_clean>\n";
	$xml .= "<form><![CDATA[$form]]></form>\n";
	$xml .= "<extras><![CDATA[$extras]]></extras>\n";
	$xml .= "<total_top><![CDATA[$totaltop]]></total_top>\n";
	$xml .= "<total_bottom><![CDATA[$totalbottom]]></total_bottom>\n";
	$xml .= '</xml>';
	
	//Send XML
	vmConnector::sendHeaderAndContent(200,$xml,'text/xml');	
	exit();
}


?>