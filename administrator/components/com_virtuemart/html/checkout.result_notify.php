<?php
// *************************************************************************
// *                                                                       *
// * Ecom Solution Payments Module for Virtuemart                          *
// * Copyright (c) 2007 Ecom Solution LTD. All Rights Reserved             *
// * Release Date: 09.08.2007                                              *
// * Version 1.1                                                           *
// *                                                                       *
// *************************************************************************
// *                                                                       *
// * Email: support@ecommerce-host.co.uk                                   *
// * Website: http://www.e-commerce-solution.co.uk                         *
// *                                                                       *
// *************************************************************************
// *                                                                       *
// * This software is furnished under a license and may be used and copied *
// * only  in  accordance  with  the  terms  of such  license and with the *
// * inclusion of the above copyright notice.  This software  or any other *
// * copies thereof may not be provided or otherwise made available to any *
// * other person.  No title to and  ownership of the  software is  hereby *
// * transferred.                                                          *
// *                                                                       *
// * You may not reverse  engineer, decompile, defeat  license  encryption *
// * mechanisms, or  disassemble this software product or software product *
// * license.  Ecom Solution  may  terminate  this license  if you fail to *
// * comply with any of the terms and conditions set forth in our end user *
// * license agreement (EULA).  In such event,  licensee  agrees to return *
// * licensor  or destroy  all copies of software  upon termination of the *
// * license.                                                              *
// *                                                                       *
// *                                                                       *
// *************************************************************************
// *  
// * this is a generic payment processor return page 
// *  
// *  

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

mm_showMyFileName( __FILE__ );

if( !isset( $_REQUEST["order_id"] ) || empty( $_REQUEST["order_id"] )) {
	echo "Order ID is not set or empty!";
}
else {

    $order_id = JRequest::getVar('order_id');
    $order_number = JRequest::getVar('order_number');
    $desc = JRequest::getVar('desc'); 

	//$order_id = intval( mosgetparam( $_REQUEST, "order_id" ));
	//$order_number = trim( mosgetparam( $_REQUEST, "order_number" ));
	//$desc =  mosgetparam( $_REQUEST, "desc" );

	$q = "SELECT order_status FROM #__{vm}_orders WHERE ";
	$q .= "#__{vm}_orders.user_id= " . $auth["user_id"] . " ";
	$q .= "AND #__{vm}_orders.order_id=$order_id ";
	$q .= "AND #__{vm}_orders.order_number LIKE '$order_number' ";

	$db->query($q);
	if ($db->next_record()) {
		$order_status = $db->f("order_status");
		if($order_status == 'C') {
		?> 
 
		<h2> Thank you for your order </h2>  
 
	<?php } else { ?>

		<span class="message">
			 Your order could not be completed.
		 </span>    
        <?php } ?>
        
<p><b>
 Your Order ID is: <?php echo $order_id ?>
</b></p>  
   
<p><b>Result: 
  <?php print $desc ; ?>
  </b> 
  <br />
     <p><a href="index.php?option=com_virtuemart&page=account.order_details&order_id=<?php echo $order_id ?>">
     <?php echo  $VM_LANG->_('PHPSHOP_ORDER_LINK') ?></a>
     </p>
    <?php
	}
	else {
		echo "Order not found!";
	}
}
?>
