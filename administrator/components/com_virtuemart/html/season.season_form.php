<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
/**
*
* @version $Id: tax.tax_form.php 1095 2007-12-19 20:19:16Z soeren_nb $
* @package VirtueMart
* @subpackage html
* @copyright Copyright (C) 2004-2007 soeren - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
error_reporting(E_ALL);
ini_set('display_errors','On');
*/

mm_showMyFileName( __FILE__ );

global $my;

//If user is an owner, only select their properties
if(!in_array(strtolower($my->usertype), array('super administrator','administrator','manager','agent'))){
	$GLOBALS['vmLogger']->err( 'You do not have permission to manage season pricing.' );
	return;
}

echo vmCommonHTML::scriptTag( $mosConfig_live_site .'/includes/js/calendar/calendar.js');
if( class_exists( 'JConfig' ) ) {
	// in Joomla 1.5, the name of calendar lang file is changed...
	echo vmCommonHTML::scriptTag( $mosConfig_live_site .'/includes/js/calendar/lang/calendar-en-GB.js');
} else {
	echo vmCommonHTML::scriptTag( $mosConfig_live_site .'/includes/js/calendar/lang/calendar-en.js');
}
echo vmCommonHTML::linkTag( $mosConfig_live_site .'/includes/js/calendar/calendar-mos.css');

?>
<style type="text/css">
div.header{
padding: 0px;
line-height: auto;
}
</style>
<?php

//First create the object and let it print a form heading
$formObj = &new formFactory( $VM_LANG->_('VM_BK_NAV_HEADING') );
//Then Start the form
$formObj->startForm();

$season_id = vmGet( $_REQUEST, 'season_id');
$option = empty($option) ? vmGet( $_REQUEST, 'option', 'com_virtuemart'):$option;

# Get property info
$hpq = 'SELECT id,name FROM #__hp_properties';
$prop_id = vmGet($_REQUEST,'property_id','');
$hpq .= ($prop_id ? " WHERE id = $prop_id" : '');
$db->query($hpq);
$props = $db->loadObjectList();
  
if (!empty($season_id)) {
 
  $q = "SELECT * FROM #__{vm}_seasons WHERE season_id=$season_id";
  $db->query($q);
  $db->next_record();
}

vmCommonHTML::loadMooTools();

?><br />
<script type="text/javascript">
//<!--

//Nightly/weekly calculation
window.addEvent('domready',function(){
	$('rate_night').addEvent('keyup',function(){		
		$('rate_week').value = $('rate_night').value * 7;		
	})
	
	$('rate_week').addEvent('keyup',function(){		
		$('rate_night_hide').value = $('rate_week').value / 7;		
		$('rate_night').value = roundNumber($('rate_week').value / 7, 2);		
	})
});

function roundNumber(num, dec) {
	var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
	return result;
}
//-->
</script>
<table class="adminform">
    <tr> 
      <td width="330px"><b><?php echo $VM_LANG->_('VM_BK_NAV_HEADING') ?></b></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><?php echo $VM_LANG->_('VM_BK_FORM_LBL') ?>:</td>
      <td>
      	<input type="text" class="inputbox" name="label" value="<?php echo $db->sf('label'); ?>" size="16" />
      </td>
    </tr>
    <tr>
    	<td><?php echo $VM_LANG->_('VM_BK_SEASON_START') ?></td>
        <td><input class="inputbox" type="text" name="dateFrom" id="dateFrom" size="20" maxlength="19" value="<?php echo $db->sf("dateFrom"); ?>" />
        <input name="reset" type="reset" class="button" onClick="return showCalendar('dateFrom', 'y-mm-dd');" value="..." /></td>
    </tr>
    <tr>
    	<td><?php echo $VM_LANG->_('VM_BK_SEASON_END') ?></td>
        <td><input class="inputbox" type="text" name="dateTo" id="dateTo" size="20" maxlength="19" value="<?php echo $db->sf("dateTo"); ?>" />
        <input name="reset" type="reset" class="button" onClick="return showCalendar('dateTo', 'y-mm-dd');" value="..." /></td>
    </tr>
</table>
<?php
// Add necessary hidden fields
$formObj->hiddenField( 'season_id', $season_id );
if($prop_id) $formObj->hiddenField( 'property_id', ($season_id ? $db->f('property_id') : $prop_id) );

$funcname = !empty($season_id) ? "updateseason" : "addseason";

// Write your form with mixed tags and text fields
// and finally close the form:
$formObj->finishForm( $funcname, $modulename.'.season_list', $option );
?>