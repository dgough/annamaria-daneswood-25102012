<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
/**
*
* @version $Id: order.order_list.php 1227 2008-02-08 12:09:50Z soeren_nb $
* @package VirtueMart
* @subpackage html
* @copyright Copyright (C) 2004-2007 soeren - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
mm_showMyFileName( __FILE__ );
global $page, $ps_order_status, $my, $mosConfig_live_site, $mosConfig_absolute_path, $ps_booking, $vbDateFrom, $vbDateTo, $mainframe, $vmuser, $perm, $vm_mainframe, $limitstart, $limit, $my;

//Add admin stylesheet for frontend usage
$vm_mainframe->addStyleSheet( VM_THEMEURL .'admin.styles.css' );

//Get the date from the session / request
$vbDateFrom = $mainframe->getUserStateFromRequest($option.'vbDateFrom', 'vbDateFrom', date('Y-m-01'));
$vbDateTo = $mainframe->getUserStateFromRequest($option.'vbDateTo', 'vbDateTo', date('Y-m-t'));
$show = vmGet( $_REQUEST, "show", "" );
$property_id = vmGet( $_REQUEST, "property_id", "" );

require_once( CLASSPATH . "pageNavigation.class.php" );
require_once( CLASSPATH . "htmlTools.class.php" );
require_once( CLASSPATH . "ps_maintenance.php" );
require_once( CLASSPATH . "ps_html.php" );
require_once( CLASSPATH . "ps_order_booking.php" );

$ps_order_booking = new ps_order_booking();

if(!$ps_order_booking->validateOwner()){
	return false;
}

//Include the date filter
require_once(PAGEPATH.'order.date_select.php');


//Get then number of bookings
$num_rows = $ps_order_booking->countOrders($show, $vbDateFrom, $vbDateTo, $property_id);
  
// Create the Page Navigation
$pageNav = new vmPageNav( $num_rows, $limitstart, $limit );


// Create the List Object with page navigation
$listObj = new listFactory( $pageNav );

// print out the search field and a list heading
$listObj->writeSearchHeader($VM_LANG->_('PHPSHOP_ORDER_LIST_LBL'), VM_THEMEURL.'images/administration/dashboard/orders.png', $modulename, "booking_list");

//Check access rights
$isAdmin = 0;
if(in_array($vmuser->gid,array(23,24,25))){
	$isAdmin=true;
} else {
	$isAdmin=false;
}


//Include the status and property filter
require_once(PAGEPATH.'order.status_select.php');

$listObj->startTable();

// these are the columns in the table
$columns = array("#" => "width=\"20\"", 					
				$VM_LANG->_('PHPSHOP_ORDER_LIST_ID') => '',
				$VM_LANG->_('PHPSHOP_ORDER_PRINT_NAME') => '',
				'Property' => '',
				
				$VM_LANG->_('PHPSHOP_ORDER_LIST_CDATE') => '',
				
				'Arrival Date' => '',
				'Departure Date' => '',
				
				$VM_LANG->_('PHPSHOP_ORDER_LIST_STATUS') => '',
								
				$VM_LANG->_('PHPSHOP_ORDER_LIST_SUBTOTAL') => 'width="60px"'
				);

if ($isAdmin) {
	$columns[$VM_LANG->_('PHPSHOP_ORDER_LIST_TOTAL')] = '';
	$columns[$VM_LANG->_('E_REMOVE')] = "width=\"5%\"";
}
$listObj->writeTableHeader( $columns );

$total=0;

//Get the main set of orders
$db = $ps_order_booking->getOrders($show, $vbDateFrom, $vbDateTo, $property_id, 0, true);

$i = 0;
while ($db->next_record()) {
    
	$listObj->newRow();
	
	// The row number
	$listObj->addCell( $pageNav->rowNumber( $i ) );
	
	// The Checkbox
	#$listObj->addCell( vmCommonHTML::idBox( $i, $db->f("order_id"), false, "order_id" ));
	
	$tmp_cell = sprintf("%06d", $db->f("order_id"));
	if($isAdmin || constant('VB_VIEW_ORDER')==1) {
		$url = $_SERVER['PHP_SELF']."?page=$modulename.booking_form&return=$page&limitstart=$limitstart&keyword=".urlencode($keyword)."&order_id=". $db->f("order_id");
		$tmp_cell = "<a href=\"" . $sess->url($url) . "\">".$tmp_cell."</a><br />";
	}
	
	$listObj->addCell( $tmp_cell );
	
	$tmp_cell = $db->f('first_name').' '.$db->f('last_name');
	
	if($isAdmin) {
		$url = $_SERVER['PHP_SELF']."?page=admin.user_form&amp;user_id=". $db->f("user_id");
		$tmp_cell = '<a href="'.$sess->url( $url ).'">'.$tmp_cell.'</a>';
	}
	
	$listObj->addCell( $tmp_cell );
	
	//Property name
	$listObj->addCell($db->f('property'));
	
	// Creation Date
	$listObj->addCell(strftime("%d-%b-%y %H:%M", $db->f("cdate")));
	
	// Last Modified Date
    //$listObj->addCell( strftime("%d-%b-%y %H:%M", $db->f("mdate")));
    
	//Arrival date
    $listObj->addCell(strftime('%d-%b-%y',strtotime($db->f('arrival'))));
    
    //Departure date
    $listObj->addCell(strftime('%d-%b-%y',strtotime($db->f('departure'))));
    
    //Status
	$listObj->addCell($list_status[$db->f("order_status")]);
	
	//$subtotal = round($ps_booking->getBookingMinusCommission($db->f("order_subtotal")),2);
	$subtotal = round( $db->f("order_subtotal") - $db->f("cleaning"),2);
	if ($subtotal < 0) $subtotal = 0;
	
	$order_total = $db->f("order_total");

	//Add to total if the booking ends in the current period
	if($db->f("ends_now") && $db->f("order_status") == 'C' OR (!$vbDateFrom && !$vbDateTo)) $total += $subtotal;
	
	$listObj->addCell( $GLOBALS['CURRENCY_DISPLAY']->getFullValue( $subtotal , 2, $db->f('order_currency') ), 'style="text-align: right"');
	
	if($isAdmin){
		
		//Total - only for admins
	    $listObj->addCell( $GLOBALS['CURRENCY_DISPLAY']->getFullValue( $order_total , 2, $db->f('order_currency') ), 'style="text-align: right"' );		
		
		
	    // Delete Order Button    
		$listObj->addCell( $ps_html->deleteButton( "order_id", $db->f("order_id"), "orderDelete", $keyword, $limitstart ) );
    }
	
	$i++;
}

$colspan = count($columns)-1;

$listObj->newRow();
$listObj->addCell('<hr />','colspan="'.($colspan+1).'"');

//Output subtotal
$listObj->newRow();
$listObj->addCell($VM_LANG->_('PHPSHOP_ORDER_LIST_SUBTOTAL'), "colspan='$colspan' style='text-align: right; font-weight: bold'");
$listObj->addCell($GLOBALS['CURRENCY_DISPLAY']->getFullValue($total), 'style="text-align: right"');


//Note
$comission = $total - $ps_booking->getBookingMinusCommission($total);
$listObj->newRow();
$listObj->addCell( 'Comission', "colspan='$colspan' style='text-align: right; font-weight: bold'");
$listObj->addCell('-'.$GLOBALS['CURRENCY_DISPLAY']->getFullValue( $comission), 'style="text-align: right"');


//Output Maintenance
if(defined('VB_OWNER_SHOW_MAINT') && VB_OWNER_SHOW_MAINT){
	$listObj->newRow();
	$listObj->addCell('Maintenance Fees', "colspan='$colspan' style='text-align: right; font-weight: bold'");
	$maintfees = 0;
	$ps_maintenance = new ps_maintenance();
	$maintenance = $ps_maintenance->getMaintenance($property_id, $vbDateFrom,$vbDateTo);
	foreach ($maintenance as $m) {
		$maintfees += floatval($m->total);
	}
	$listObj->addCell('-'.$GLOBALS['CURRENCY_DISPLAY']->getFullValue($maintfees), 'style="text-align: right"');
}


//Note
$listObj->newRow();
$listObj->addCell($VM_LANG->_('PHPSHOP_ORDER_LIST_TOTAL'), "colspan='$colspan' style='text-align: right; font-weight: bold'");
$listObj->addCell( $GLOBALS['CURRENCY_DISPLAY']->getFullValue($total - $maintfees - $comission ), 'style="text-align: right"');

//Output total
if(defined(VB_OWNER_SHOW_MAINT) && VB_OWNER_SHOW_MAINT){
	$listObj->newRow();
	$listObj->addCell("Please note: The above totals apply to bookings and maintenance work ending between $vbDateFrom and $vbDateTo", "colspan='".($colspan+1)."' style='text-align: right; font-weight: bold'");
}

$listObj->writeTable();
echo '<input type="hidden" name="toggle" value="1" onclick="return" />';

$listObj->endTable();

$listObj->writeFooter( $keyword, "&show=$show" );

?>

