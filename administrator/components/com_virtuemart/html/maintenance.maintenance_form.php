<?php
mm_showMyFileName( __FILE__ );

require_once( CLASSPATH . "ps_maintenance.php" );
require_once( CLASSPATH . "htmlTools.class.php" );

global $vmuser, $my, $perm, $vm_mainframe;

//Add admin stylesheet for frontend usage
$vm_mainframe->addStyleSheet( VM_THEMEURL .'admin.styles.css' );

//Get the maintenance item id
$maintenance_id = vmGet($_REQUEST,'maintenance_id',0);


if(!$perm->check( "maintenance,admin,storeadmin") && !$maintenance_id){
	$GLOBALS['vmLogger']->err( 'Only maintenance people can add maintenance.' );
	return;
}

//Load Mootools
vmCommonHtml::loadmootools();

/**
 * Retrieve this maintenance sheet
 */

$dbm = new ps_DB();
$dbm->query("SELECT m.*, p.name AS property, u.name AS user, t.maintenance_type AS type, t.maintenance_type_id AS type_id
			FROM #__{vm}_maintenance AS m 
			LEFT JOIN #__users AS u ON u.id = m.user_id
			LEFT JOIN #__{vm}_maintenance_types AS t ON t.maintenance_type_id = m.type
			LEFT JOIN #__hp_properties AS p on p.id = m.property_id 			
			WHERE m.maintenance_id = '$maintenance_id'");
$dbm->next_record();

/**
 * Retrieve this maintenance persons properties
 */

if($perm->check( "maintenance,admin,storeadmin")){
	if($perm->check( "maintenance")){
		$user_id = $my->id;
		$canApprove = 0;
	}else{
		$user_id = vmGet($_REQUEST,'user_id',$dbm->f('user_id'));	
		$canApprove = 1;
	}	
	$canEdit = 1;
}else{
	$user_id = $dbm->f('user_id');
	$canEdit = 0;
	$canApprove = 1;
}

$dbp = new ps_DB();
$dbp->query("SELECT properties FROM #__{vm}_maintenance_props WHERE user_id = '$user_id'");
$props = $dbp->loadResult();

/**
 * Get the intro text
 */
$db = new ps_DB();
$db->query('SELECT concat(`introtext`,`fulltext`) as text FROM #__content WHERE id = 39');
echo $db->f('text') ? '<div style="clear:both" class="info">'.$db->f('text').'</div>' : '';

/**
 * Add calendar code
 */
echo vmCommonHTML::scriptTag( $mosConfig_live_site .'/includes/js/calendar/calendar.js');
if( class_exists( 'JConfig' ) ) {
	// in Joomla 1.5, the name of calendar lang file is changed...
	echo vmCommonHTML::scriptTag( $mosConfig_live_site .'/includes/js/calendar/lang/calendar-en-GB.js');
} else {
	echo vmCommonHTML::scriptTag( $mosConfig_live_site .'/includes/js/calendar/lang/calendar-en.js');
}
echo vmCommonHTML::linkTag( $mosConfig_live_site .'/includes/js/calendar/calendar-mos.css');



/**
 * Create property drop down list
 */

$db = new ps_DB();
$q = "SELECT id,name FROM #__hp_properties WHERE id IN ($props) ORDER BY name";
$db->query($q);
$props = array(0=>'-- Select Property --');
while($db->next_record()){
	$props[$db->f('id')] = $db->f('name');
}
$properties = ps_html::selectList('property_id', $dbm->f('property_id'), $props);


/**
 * Create maintenance type drop down list
 */

$db = new ps_DB();
$q = "SELECT * FROM #__{vm}_maintenance_types ORDER BY ordering";
$db->query($q);
$types = array();
while($db->next_record()){
	$types[$db->f('maintenance_type_id')] = $db->f('maintenance_type');
}
$types = ps_html::selectList('type_id', $dbm->f('type'), $types);



/**
 * Create maintenance man drop down list
 */
$maintenance_people = '';
if(in_array($vmuser->gid,array(23,24,25))){
	$q = "SELECT u.id,u.name FROM #__users AS u LEFT JOIN #__{vm}_user_info AS ui ON u.id = ui.user_id WHERE ui.perms = 'maintenance' ORDER BY name";
	$db->query($q);
	$users = array(0=>'-- Select User --');
	while($db->next_record()){
		$users[$db->f('id')] = $db->f('name');
	}
	$maintenance_people = ps_html::selectList('user_id', $user_id, $users,1,'id="user_id"');
}

$formFactory = new formFactory($maintenance_id ? 'Update Maintenance Cost' : 'Add Maintenance Cost');
$formFactory->startForm();

?>
<script type="text/javascript">
//<!--

window.addEvent('domready',function(){
	if($('user_id')){
		$('user_id').addEvent('change',function(){
			<?php
			$url = str_replace('&amp;','&',$sess->url($_SERVER['PHP_SELF']."?page=maintenance.maintenance_form&maintenance_id=$maintenance_id"));
			$url .= strstr($url,'?') ? '&' : '?';
			?>
			document.location = "<?php echo $url ?>user_id="+this.value;
		})
	}
})
//-->
</script>

<fieldset class="vb-fieldset">
	<legend>Maintenance Time and Costs</legend>
	<?php if($canApprove){ ?>
	<div class="row">
    	<label>Maintenance Person:</label> <span><?php echo $canEdit ? $maintenance_people : $dbm->f('user'); ?></span>
    </div>
    <?php } ?>
    <div class="row">
    	<label>Property:</label> <span><?php echo $canEdit ? $properties : $dbm->f('property'); ?></span>
    </div>
    <div class="row">
    	<label>Maintenance Type:</label> <span><?php echo $canEdit ? $types : $dbm->f('type'); ?></span>
    </div>
    <div class="row">
    	<label>Worked From:</label>
    	<?php 
    	if($canEdit){
    		?>
    		<span>
    			<input class="inputbox" type="text" name="worked_from" id="workedfrom" size="20" maxlength="19" value="<?php echo $dbm->sf('worked_from') ?>" />
    			<input name="reset" type="reset" class="button" onClick="return showCalendar('workedfrom', 'y-mm-dd');" value="..." style="vertical-align:middle" />
    		</span>
    		<?php
    	}else{
    		echo $dbm->f('worked_from');
    	}?>        
    </div>
    <div class="row">
    	<label>Worked To:</label>
    	<?php 
    	if($canEdit){
    		?>
    		<span>
    			<input class="inputbox" type="text" name="worked_to" id="workedto" size="20" maxlength="19" value="<?php echo $dbm->sf('worked_to') ?>" />
    			<input name="reset" type="reset" class="button" onClick="return showCalendar('workedto', 'y-mm-dd');" value="..." style="vertical-align:middle" />
    		</span>
    		<?php
    	}else{
    		echo $dbm->f('worked_to');
    	}?> 
    </div>
    <div class="row">
    	<label>Hours : Minutes</label> 
    	<?php 
    	if($canEdit){
    		?>
    		<span><input type="text" name="hours" size="3" value="<?php echo $dbm->sf('hours'); ?>" /> : <input type="text" size="3" name="minutes" value="<?php echo $dbm->sf('minutes'); ?>" /></span>
    		<?php
    	}else{
    		echo $dbm->f('hours').' : '.$dbm->f('minutes');
    	}?>
    </div>
    <div class="row">    
    	<label>Amount:</label> 
    	<?php 
    	if($canEdit){
    		?><span><input type="text" name="amount" size="5" value="<?php echo intval($dbm->sf('amount')); ?>" /></span><?php
    	}else{
    		echo $GLOBALS['CURRENCY_DISPLAY']->getFullValue( $dbm->f('amount') );
    	}?>
    </div>
    <div class="row">    
    	<label>Comments:</label>
    	<span style="float: left"><?php 
    	if($canEdit){
    		?><textarea name="comments" rows="5" cols="40"><?php echo $dbm->sf('comments'); ?></textarea><?php
		}else{
			echo nl2br($dbm->f('comments'));
		}?>&nbsp;</span>
		<div style="clear: left"></div>
    </div>
    <?php if ($canApprove) { 
    	
    	$approved = $dbm->sf('approved') == '' || $dbm->sf('approved') == '0000-00-00' ? '' : $dbm->sf('approved');
    	$paid = $dbm->sf('paid') == '' || $dbm->sf('paid') == '0000-00-00' ? '' : $dbm->sf('paid');
    	?>
	<div class="row">
    	<label>Approved:</label>
    	<span>
    		<?php if($canEdit){ ?>
    			<input class="inputbox" type="text" name="approved" id="approved" size="20" maxlength="19" value="<?php echo $approved ?>" />
    			<input name="reset" type="reset" class="button" onClick="return showCalendar('approved', 'y-mm-dd');" value="..." style="vertical-align:middle" />
    		<?php }else{ ?>
    			<input class="inputbox" type="radio" name="approved" id="approved" value="1" <?php echo $approved ? 'selected="selected"' : '' ?> /> Yes <br />
    			<input class="inputbox" type="radio" name="approved" id="approved" value="0" <?php echo !$approved ? 'selected="selected"' : '' ?> /> No
    		<?php } ?>
    	</span>      
    </div>
    <div class="row">
    	<label>Paid:</label> 
    	<span>
    		<?php if($canEdit && $canApprove){ ?>
    			<input class="inputbox" type="text" name="paid" id="paid" size="20" maxlength="19" value="<?php echo $paid ?>" />
    			<input name="reset" type="reset" class="button" onClick="return showCalendar('paid', 'y-mm-dd');" value="..." style="vertical-align:middle" />
    		<?php }else{
    			echo $paid ? $paid : vmCommonHTML::getYesNoIcon( 0, '', 'Not Paid' );
    		} ?>&nbsp;
    	</span> 
        
        <div class="row">
        	<label>Supporting Docs</label> <br/>
            <span>
            <?php 
			$mainID = $_GET['maintenance_id'];
			$docs = mysql_query("select * from supDocs WHERE maint_id='".$mainID."'")or die(mysql_error());
				  while($docRow = mysql_fetch_array($docs)){
					  echo "<a href='http://46.32.250.110/~annamari/documents/";		//MUST REMOVE
					  echo $docRow['document'];
					  echo "' target='_blank'>";
					  echo $docRow['document'];
					  echo "</a><br/>";
					 } 
					  ?>
                      </span><br/>
            <span><input type="file" name="file" /></span>
        </div>
             
    <?php }else{ ?>
    	<input type="hidden" name="approved" value="1" />
	<?php } ?>
	<div class="row">
    	<label>&nbsp; </label> <span><input type="submit" value=" <?php echo $canApprove ? 'Submit' : 'Submit Time and Costs' ?> " /></span>
    </div>
</fieldset>
<?php
//End form
$formFactory->hiddenField('maintenance_id' , $maintenance_id ? $maintenance_id : 0);
if(!$canApprove){
	$formFactory->hiddenField('user_id' , $my->id);
}
if(!$canEdit){
	$formFactory->hiddenField('property_id' , $dbm->f('property_id'));
	$formFactory->hiddenField('user_id' , $dbm->f('user_id'));
	$formFactory->hiddenField('type' , $dbm->f('type'));
	$formFactory->hiddenField('worked_from' , $dbm->f('worked_from'));
	$formFactory->hiddenField('worked_to' , $dbm->f('worked_to'));
	$formFactory->hiddenField('hours' , $dbm->f('hours'));
	$formFactory->hiddenField('minutes' , $dbm->f('minutes'));
	$formFactory->hiddenField('amount' , $dbm->f('amount'));
	$formFactory->hiddenField('comments' , $dbm->f('comments'));
}
$formFactory->finishForm($maintenance_id ? 'updateMaintenance' : 'addMaintenance','maintenance.maintenance_list');
?>