<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );
/**
*
* @version $Id: order.stage_payments_form.php 1354 2008-04-05 19:54:07Z  $
* @package VirtueMart
* @subpackage html
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
*/
mm_showMyFileName( __FILE__ );
global $ps_order_status,$VB_STAGE_PAY_METHODS;

error_reporting(E_ALL);
ini_set('display_errors','On');

require_once(CLASSPATH.'ps_calendar.php');
require_once(CLASSPATH.'htmlTools.class.php');
	
echo vmCommonHTML::ScriptTag( $mosConfig_live_site .'/includes/js/calendar/calendar.js');
if( class_exists( 'JConfig' ) ) {
// in Joomla 1.5, the name of calendar lang file is changed...
	echo vmCommonHTML::ScriptTag( $mosConfig_live_site .'/includes/js/calendar/lang/calendar-en-GB.js');
} else {
	echo vmCommonHTML::ScriptTag( $mosConfig_live_site .'/includes/js/calendar/lang/calendar-en.js');
}
echo vmCommonHTML::linkTag( $mosConfig_live_site .'/includes/js/calendar/calendar-mos.css');

/**
 * Get the payment methods available
 */
$dbp = new ps_DB;
$q = "SELECT payment_method_id AS id,payment_method_name AS name FROM #__{vm}_payment_method WHERE payment_enabled = 'Y' "
	.(isset($VB_STAGE_PAY_METHODS) && count($VB_STAGE_PAY_METHODS) ? ' OR payment_method_id IN ('.implode(',',$VB_STAGE_PAY_METHODS).')' : '')
	.' ORDER BY list_order';	
$dbp->query($q);
$payment_methods = array();
while($dbp->next_record()){
	$payment_methods[] = $dbp->get_row();
}

$payment_id = vmGet($_REQUEST,'payment_id',0);

$db = new ps_DB;
$q = "SELECT op.*, p.name, ui.first_name, ui.last_name 
	FROM #__{vm}_order_payment op 
	LEFT JOIN #__{vm}_order_booking bko on op.order_id=bko.order_id 
	LEFT JOIN #__hp_properties p on bko.property_id=p.id 
	LEFT JOIN #__{vm}_orders o on o.order_id=op.order_id 
	LEFT JOIN #__{vm}_order_user_info ui on o.user_id=ui.user_id 
	WHERE op.payment_id=$payment_id";
$db->query($q);
$db->next_record();	

if(!$db->num_rows() && $payment_id){
	$GLOBALS['vmLogger']->err( 'Unable to find payment.' );
	return;
}

$formFactory =  new formFactory('Payment Details');
$formFactory->startForm();
?>
<style type="text/css">
div.header{
padding: 0px;
}
</style>

<fieldset class="vb-fieldset">
    <legend>Stage Payment <?php echo $db->sf('payment_stage'); ?> - <?php echo $db->sf('name'); ?> - <?php echo $db->sf('first_name').' '.$db->sf('last_name'); ?></legend>
    <div class="row">
        <label>Booking</label>
        <span><input class="inputbox" type="text" name="order_id" value="<?php echo $db->sf('order_id'); ?>" /></span>
    </div>
    <div class="row">
        <label>Amount</label>
        <span><input class="inputbox" type="text" name="payment_amount" value="<?php echo round($db->sf('payment_amount'),2); ?>" /></span>
    </div>
    <div class="row">
        <label>Payment Method</label>
        <span><?php echo vmCommonHTML::selectList($payment_methods, 'payment_method_id', '', 'id', 'name', $db->sf('payment_method_id')) ?></span>
    </div>
    <div class="row">
        <label>Payment Due</label>
        <span><input class="inputbox" type="text" name="payment_due" id="payment_due" size="10" maxlength="19" value="<?php echo $db->sf('payment_due'); ?>" /> <input name="reset" type="reset" class="button" onClick="return showCalendar('payment_due', 'y-mm-dd');" value="..." /></span>
    </div>
    <div class="row">
        <label>Payment Received</label>
        <span><input class="inputbox" type="text" name="payment_submitted" id="payment_submitted" size="10" maxlength="19" value="<?php echo $db->sf('payment_submitted'); ?>" /> <input name="reset" type="reset" class="button" onClick="return showCalendar('payment_submitted', 'y-mm-dd');" value="..." /></span>
    </div>
    <div class="row">
        <label>Payment Cleared</label>
        <span><input class="inputbox" type="text" name="payment_cleared" id="payment_cleared" size="10" maxlength="19" value="<?php echo $db->sf('payment_cleared'); ?>" /> <input name="reset" type="reset" class="button" onClick="return showCalendar('payment_cleared', 'y-mm-dd');" value="..." /></span>
    </div>
</fieldset>

<?php 
$return = vmGet($_REQUEST,'return','order.admin_payment_list');

$formFactory->hiddenField('return',$return);
$formFactory->hiddenField('payment_id',$payment_id);
$formFactory->hiddenField('order_id',$db->f('order_id'));
$formFactory->finishForm($payment_id ? 'orderUpdatePayment' : 'orderAddPayment' ,$return);