<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );
/**
*
* @version $Id: order.order_print.php 1354 2008-04-05 19:54:07Z gregdev $
* @package VirtueMart
* @subpackage html
* @copyright Copyright (C) 2004-2008 soeren - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
mm_showMyFileName( __FILE__ );

global $vbDateFrom, $vbDateTo, $limitstart, $limit, $modulename, $keyword, $vmuser, $sess, $vm_mainframe, $perm;

//Add admin stylesheet for frontend usage
$vm_mainframe->addStyleSheet( VM_THEMEURL .'admin.styles.css' );

require_once( CLASSPATH . "pageNavigation.class.php" );
require_once( CLASSPATH . "htmlTools.class.php" );
require_once( CLASSPATH . "ps_order_booking.php" );

$ps_order_booking = new ps_order_booking();

//Get seom values from the request array
$property_id = vmGet($_REQUEST,'property_id');
$user_id = $perm->check("maintenance") ? $my->id : vmGet($_REQUEST,'user_id');

//Get the property ids that this user can manage
$property_ids = $ps_order_booking->getAllowedProperties();

if($property_id && !in_array($property_id, $property_ids)){
	$GLOBALS['vmLogger']->err( 'You do not have permission to manage maintenance for this property.' );
	return;
}
if(empty($property_ids)){
	$GLOBALS['vmLogger']->err( 'You do not have permission to manage maintenance.' );
	return;
}

/**
 * Get the intro text
 */
$db = new ps_DB();
$db->query('SELECT concat(`introtext`,`fulltext`) as text FROM #__content WHERE id = 38');
echo $db->f('text') ? '<div style="clear:both" class="info">'.$db->f('text').'</div>' : '';


//Include the date filter
require_once(PAGEPATH.'order.date_select.php');

/**
 * Get the maintenance for this period/property/man
 */
$dbm = new ps_DB();
$where = array(" m.worked_to BETWEEN '$vbDateFrom' AND '$vbDateTo'");
if($property_id || count($property_ids))	$where[] = " m.property_id IN (".($property_id ? $property_id : implode(',',$property_ids)).")";
if($user_id)								$where[] = " m.user_id = '$user_id'";


$q = "SELECT m.*, IF(CONCAT(ui.first_name,ui.last_name) = '', u.name, CONCAT(ui.first_name,' ',ui.last_name)) AS user, p.name AS property, t.maintenance_type AS type FROM #__{vm}_maintenance AS m
	LEFT JOIN #__{vm}_maintenance_types AS t ON t.maintenance_type_id = m.type
	LEFT JOIN #__{vm}_user_info AS ui ON ui.user_id = m.user_id
	LEFT JOIN #__hp_properties AS p ON p.id = m.property_id
	LEFT JOIN #__users AS u ON u.id = m.user_id"
	.' WHERE '.implode(' AND ',$where)	
	." ORDER BY m.worked_to";

$dbm->query($q);


//Get then number of bookings
$num_rows = $dbm->num_rows();

// Create the Page Navigation
$pageNav = new vmPageNav( $num_rows, $limitstart, $limit );

// Create the List Object with page navigation
$listObj = new listFactory( $pageNav );

// print out the search field and a list heading
$listObj->writeSearchHeader('Maintenance Overview', VM_THEMEURL.'images/administration/dashboard/orders.png', $modulename, 'maintenance_list');


?>
<div style='float: right'>
<?php

/**
 * Create property drop down list
 */

$db = new ps_DB();
$q = "SELECT id,name FROM #__hp_properties WHERE id IN (".implode(',',$property_ids).") ORDER BY name";
$db->query($q);
$props = array(0=>'-- Select Property --');
while($db->next_record()){
	$props[$db->f('id')] = $db->f('name');
}
echo ps_html::selectList('property_id', $property_id, $props,1,'', "onchange='document.adminForm.submit()'");


/**
 * Create maintenance man drop down list
 */
if(in_array($vmuser->gid,array(23,24,25))){
	$q = "SELECT u.id,u.name FROM #__users AS u LEFT JOIN #__{vm}_user_info AS ui ON u.id = ui.user_id WHERE ui.perms = 'maintenance' ORDER BY name";
	$db->query($q);
	$users = array(0=>'-- Select Personnel --');
	while($db->next_record()){
		$users[$db->f('id')] = $db->f('name');
	}
	echo ps_html::selectList('user_id', $user_id, $users,1,'', "onchange='document.adminForm.submit()'");
}
?>
</div>
<br style="clear:right" />
<br style="clear:right" />
<?php

$listObj->startTable();

$columns = array(
			"#" => "width=\"20\"", 	
			'Person'=>'',
			'Type'=>'',
			'Property'=>'',
			'Worked From'=>'',
			'Worked To'=>'',
			'Time (h:m)'=>'style="text-align: center"',
			'Total'=>'style="text-align: right"',
			'Approved'=> 'style="text-align: center"',
			'Paid'=> 'style="text-align: center"'
			);

if($perm->check('admin,storeadmin')) $columns['Delete'] = 'style="text-align: center"';

$listObj->writeTableHeader( $columns );

$i = 0;
$maintenanceTotal = 0;
while($dbm->next_record()){
	
	$listObj->newRow();
	$id = $dbm->f('maintenance_id');
	$url = '<a href="'.$sess->url($_SERVER['PHP_SELF']."?page=maintenance.maintenance_form&maintenance_id=$id&").'">%s</a>';
	
	// The row number
	$listObj->addCell( sprintf($url,$pageNav->rowNumber( $i )) );
	
	//Maintenance person
	$listObj->addCell( sprintf($url,$dbm->f('user')) );
	
	//Maintenance type
	$listObj->addCell( $dbm->f('type') ? $dbm->f('type') : '&nbsp;' );
	
	//Property
	$listObj->addCell( $dbm->f('property') );
	
	//Worked From
	$listObj->addCell( $dbm->f('worked_from') );
	
	//Worked To
	$listObj->addCell( $dbm->f('worked_to') );
	
	//Time
	$listObj->addCell( sprintf("%02d", $dbm->f('hours')).'h:'.sprintf("%02d", $dbm->f('minutes')).'m', 'style="text-align: center"' );
	
	//Total
	$total = $dbm->f('amount');
	$maintenanceTotal += $total;
	$listObj->addCell( $GLOBALS['CURRENCY_DISPLAY']->getFullValue( $total ), 'style="text-align: right"');
	
	//Approved
	$listObj->addCell( !($dbm->f('approved') == '0000-00-00' || $dbm->f('approved') == '') ? $dbm->f('approved') : vmCommonHTML::getYesNoIcon( 0, '', 'Not Approved' ), 'style="text-align: center"');
	
	//Paid
	$listObj->addCell( !($dbm->f('paid') == '0000-00-00' || $dbm->f('paid') == '') ? $dbm->f('paid') : vmCommonHTML::getYesNoIcon( 0, '', 'Not Paid' ), 'style="text-align: center"');
	
	//Delete maintenance
	if($perm->check('admin,storeadmin')) $listObj->addCell( $ps_html->deleteButton( "maintenance_id", $dbm->f("maintenance_id"), "deleteMaintenance", $keyword, $limitstart ) , 'style="text-align: center"');
	
	$i++;
}

$listObj->newRow();
$listObj->addCell( '<strong>Maintenance Total</strong>', 'style="text-align: right" colspan="7"' );
$listObj->addCell( $GLOBALS['CURRENCY_DISPLAY']->getFullValue( $maintenanceTotal ), 'style="text-align: right"');
$listObj->addCell( '&nbsp;', 'colspan="'.(count($columns) - 8).'"' );


$listObj->writeTable();

$listObj->endTable();

echo '<input type="hidden" name="toggle" value="0" onclick="" />';

$listObj->writeFooter( $keyword, "&vbDateFrom=$vbDateFrom&vbDateTo=$vbDateTo&show=$show" );

?>