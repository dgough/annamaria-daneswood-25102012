<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );
/*
 * @version $Id: account.label_image.php 1122 2008-01-07 14:52:31Z thepisu $
 * @package VirtueMart
 * @subpackage html
 */
mm_showMyFileName(__FILE__);

require_once( CLASSPATH . "htmlTools.class.php" );
require_once( CLASSPATH . "ps_payment_method.php" );
require_once( CLASSPATH . "ps_payments.php" );
require_once( CLASSPATH . "ps_checkout.php" );
require_once( CLASSPATH . "ps_payments.php" );

global $ps_booking, $sess, $vmInputFilter;

$order_id = 			vmGet($_REQUEST,'order_id');
$payment_method_id = 	vmGet($_REQUEST,'payment_method_id');
$payment_ids =			vmGet($_SESSION,'payment_ids');
$d = 					$_REQUEST;


//Get the payment info
$null = null;
$ps_payments = new ps_payments();
$payment_stages = $ps_payments->getStagePayments(0, $order_id, $payment_ids, $null, $payment_method_id);
$_SESSION['payment_stages'] = $payment_stages;

// calculate the unix timestamp for the specified expiration date
// default the day to the 1st & clean the number
$expire_timestamp = @mktime(0,0,0,$_SESSION["ccdata"]["order_payment_expire_month"], 1,$_SESSION["ccdata"]["order_payment_expire_year"]);
$_SESSION["ccdata"]["order_payment_expire"] = $expire_timestamp;
$payment_number = ereg_replace(" |-", "", @$_SESSION['ccdata']['order_payment_number']);
$d["order_payment_code"] = @$_SESSION['ccdata']['credit_card_code'];		
		
/**
 * Compile some order information
 */
//User info
$q  = "SELECT * FROM #__{vm}_order_user_info AS uo
LEFT JOIN #__{vm}_user_info AS ui ON ui.user_id = uo.user_id
WHERE uo.order_id='$order_id'";
$db->query( $q.' AND uo.address_type="ST"' );
if(!$db->num_rows()){
	$db->query( $q.' AND uo.address_type="BT"' );
}
$db->next_record();
$d["ship_to_info_id"] = $db->f('user_info_id');
$d["user_id"] = $db->f('user_id');


// Check to see if Payment Class File exists
$ps_payment_method = new ps_payment_method();
$payment_class = $ps_payment_method->get_field($payment_method_id, "payment_class");
$enable_processor = $ps_payment_method->get_field($payment_method_id, "enable_processor");

if (file_exists(CLASSPATH . "payment/$payment_class.php") && $payment_stages->amount_due > 0) {
	if( !class_exists( $payment_class )) {
		include_once( CLASSPATH. "payment/$payment_class.php" );
	}

	$db = new ps_DB();
	$db->query("SELECT order_number FROM #__{vm}_orders WHERE order_id = $order_id");
	$order_number = $db->f('order_number');
	$_PAYMENT = new $payment_class();		
	
	if (!$_PAYMENT->process_payment($order_number, $payment_stages->amount_due, $d, $payment_stages)) {
		$vmLogger->err( $VM_LANG->_('PHPSHOP_PAYMENT_ERROR',false)." ($payment_class)" );
		return false;			
	}
}

		
/**
* UPDATE the Order payment info 
*/

foreach($payment_stages->stages as $payment){		
						
	
	//Check if the payment is due
	if($payment->is_due){
		
		// Payment number is encrypted using mySQL encryption functions.
		$fields = array();		
		$specialfield = array();	
		$fields['payment_submitted'] = $payment->payment_submitted;
		$fields['payment_cleared'] = $payment->payment_cleared;
		$fields['payment_method_id'] = $payment_method_id;
		$fields['order_payment_log'] = @$d["order_payment_log"];
		$fields['order_payment_trans_id'] = $vmInputFilter->safeSQL( @$d["order_payment_trans_id"] );
		if( !empty( $payment_number ) && VM_STORE_CREDITCARD_DATA == '1' ) {
			// Store Credit Card Information only if the Store Owner has decided to do so
			$fields['order_payment_code'] = $d["order_payment_code"];
			$fields['order_payment_expire'] = @$_SESSION["ccdata"]["order_payment_expire"];
			$fields['order_payment_name'] = @$_SESSION["ccdata"]["order_payment_name"];
			$fields['order_payment_number'] = VM_ENCRYPT_FUNCTION."( '$payment_number','" . ENCODE_KEY . "')";
			$specialfield = array('order_payment_number');
		} 
		
		//Add to DB
		$db->buildQuery( 'UPDATE', '#__{vm}_order_payment', $fields, " WHERE payment_id = '$payment->payment_id'", $specialfield );
		if(!$db->query()){
			echo $db->getErrorMsg();
		}
	}	
}

//Setup a vars array, used in checkout.thankyou
$vars['order_id'] = $order_id;
//Include the thankyou page
include(PAGEPATH.'checkout.thankyou.php');

//Clear session
$_SESSION['ccdata']['order_payment_name'] = '';
$_SESSION['ccdata']['creditcard_code'] = '';
$_SESSION['ccdata']['order_payment_number'] = '';
$_SESSION['ccdata']['order_payment_expire_month'] = '';
$_SESSION['ccdata']['order_payment_expire_year'] = '';
$_SESSION['ccdata']['credit_card_code'] = '';

?>