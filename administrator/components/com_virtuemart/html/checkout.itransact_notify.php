<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );
/*
 * @version $Id: order.label_image.php 1122 2008-01-07 14:52:31Z thepisu $
 * @package VirtueMart
 * @subpackage html
 */
mm_showMyFileName(__FILE__);

global $sess, $ps_booking, $CURRENCY_DISPLAY, $modulename, $keyword,$VB_STAGE_PAY_METHODS,$vmLogger;

require_once(CLASSPATH.'ps_checkout.php');

$order_id = vmget($_POST,'order_id','');
$payment_stages = explode(',',vmget($_POST,'stages',''));
$err = vmget($_POST,'err','');
$die = vmget($_POST,'die','');
$xid = vmget($_POST,'xid','');
$signature = vmget($_POST,'signature','');

if($err || $die){
	$vmLogger->err("There was an error with the transaction, the error message was: $err $die");
	return false;
}
if(!$order_id){
	$vmLogger->err('No order ID specified, unable to continue');
	return false;
}
if(!$xid){
	$vmLogger->err('No transaction ID specified, unable to continue');
	return false;
}

$db = new ps_DB();

/*
* Check to see if there's a semi-colon. If not, then no payment stage is being passed back; in which case the order is invalid
* Even when payment stages aren't turned on, at least 1 payment should be stored in #__vm_order_payment
*/
$success = true;
if ($order_id) {
	
	
	$db->query("SELECT order_status FROM #__{vm}_orders WHERE order_id = $order_id");
	$db->next_record();
	if($db->f('order_status') == 'P'){
		$ps_checkout = new ps_checkout();
		$ps_checkout->email_receipt($order_id);
	}
	
	$fields = array('order_status' => 'C'); // C == confirmed
		
	$db->buildQuery('UPDATE', '#__{vm}_orders', $fields, ' WHERE `order_id`='.$order_id);
	
	if (!$db->query()) {
		$vmLogger->err('Could not update the order information.');
		return false;
	}	
	
	if (count($payment_stages)) {				
		/*
		* update the payment stages
		* If there's a comma, then more than stage is being paid for; explode them and loop through
		* otherwise simply update the related payment stage
		*/				
		
		foreach ($payment_stages as $stage) {
			if($stage){
				$fields2 = array(
								'payment_cleared'=>date('Y-m-d'),
								'order_payment_number'=>$xid,
								'order_payment_log'=>$signature,
								);			
				$db->buildQuery('UPDATE', '#__{vm}_order_payment', $fields2, ' WHERE `order_id`='.$order_id.' AND `payment_stage`='.$stage);
				
				if (!$db->query()) {
					$vmLogger->err('Could not update payment stage '.$stage);
					$success = false;
				}
			}
		}					
	}
}
if($success){
	$db->query("SELECT * FROM #__{vm}_order_user_info WHERE order_id = $order_id");
	$db->next_record();
	$tpl = new $GLOBALS['VM_THEMECLASS']();
	$tpl->set( 'order_id', $order_id );
	$tpl->set( 'user', $db->get_row() );
	echo $tpl->fetch( "pages/checkout.notify_thankyou.tpl.php" );
}

?>