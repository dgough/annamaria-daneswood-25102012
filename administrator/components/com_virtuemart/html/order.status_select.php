<?php if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 

global $page, $vbDateFrom, $vbDateTo, $keyword;
require_once(CLASSPATH.'ps_order_booking.php');
$show = vmGet($_REQUEST,'show');
vmCommonHTML::loadMooTools();
?>
<script type='text/javascript'>
window.addEvent('domready',function(){
	$('property_id').addEvent('change',function(){
		window.location = '<?php echo $_SERVER['PHP_SELF']."?option=com_virtuemart&page=$page&keyword=$keyword&show=$show&property_id="?>'+$('property_id').value;
	});
});
</script>
<div style="float: right">
	<?php
	$props = ps_order_booking::getAllowedProperties();
	if(count($props) > 1){
		$q = "SELECT * FROM #__hp_properties WHERE published = 1 AND id IN (".implode(',',$props).") ORDER BY name";
		$dbp = new ps_DB();
		$dbp->query($q);
		$props = array(0=>'-- Select Property --');
		while($dbp->next_record()){
			$props[$dbp->f('id')] = $dbp->f('name');
		}
		echo ps_html::selectList('property_id', $property_id, $props,1,'', "id='property_id'");
	}
	?>
</div>
<div align="center">
<?php

$navi_db = new ps_DB;
$q = "SELECT order_status_code, order_status_name ";
$q .= "FROM #__{vm}_order_status WHERE vendor_id = '$ps_vendor_id'";
$navi_db->query($q);
$list_status = array();
while ($navi_db->next_record()) {  ?> 

	<?php $list_status[$navi_db->f("order_status_code")] = $navi_db->f("order_status_name") ?>
  	<a href="<?php $sess->purl($_SERVER['PHP_SELF']."?page=$page&show=".$navi_db->f("order_status_code")."&keyword=$keyword") ?>" class='status_<?php echo strtolower($navi_db->f("order_status_name")) ?>'><b><?php echo $navi_db->f("order_status_name")?></b></a> | <?php 
} 
?>
    <a href="<?php $sess->purl($_SERVER['PHP_SELF']."?page=$page&keyword=$keyword&show=")?>" class='status_all'><b>
    <?php echo $VM_LANG->_('PHPSHOP_ALL') ?></b></a>
</div>
<br style='clear: both'/>