<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
/**
*
* @version $Id: tax.tax_list.php 1099 2007-12-21 12:46:46Z soeren_nb $
* @package VirtueMart
* @subpackage html
* @copyright Copyright (C) 2004-2007 soeren - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
mm_showMyFileName( __FILE__ );

global $no_menu, $my;

require_once( CLASSPATH . "pageNavigation.class.php" );
require_once( CLASSPATH . "htmlTools.class.php" );

$prop_id = vmGet($_REQUEST,'property_id','');
$prop_ids = array();

//If user is an owner, only select their properties
if($my->usertype == 'Agent'){
	$db->query("SELECT p.id from jos_hp_properties AS p, jos_hp_agents as a WHERE a.id = p.agent AND a.user = '$my->id'");
	$ids = array();
	while($db->next_record()){
		$ids[] = $db->f("id");
	}
	if(in_array($prop_id,$ids)){
		$prop_ids = array($prop_id);
	}else{
		$GLOBALS['vmLogger']->err( 'You can only edit property seasons that you manage.' );
		return;
	}
}else if(!in_array(strtolower($my->usertype), array('super administrator','administrator','manager'))){
	$GLOBALS['vmLogger']->err( 'You do not have permission to manage bookings.' );
	return;
}

?>
<style type="text/css">
div.header{
padding: 0px;
line-height: auto;
}
</style>
<?php

if($prop_id && empty($prop_ids)) $prop_ids[] = $prop_id;

$list  = "SELECT s.* ";
$count = "SELECT count(*) as num_rows ";
$from = "FROM #__{vm}_seasons AS s ";
$list .= $from . " ORDER BY s.dateFrom LIMIT $limitstart, " . $limit;
$count .= $from;

$db->query($count);
$db->next_record();
$num_rows = $db->f("num_rows");

// Create the Page Navigation
$pageNav = new vmPageNav( $num_rows, $limitstart, $limit );

// Create the List Object with page navigation
$listObj = new listFactory( $pageNav );

// print out the search field and a list heading
$listObj->writeSearchHeader($VM_LANG->_('VM_BK_LIST_HEADING'), '', $modulename, "season_list");

// start the list table
$listObj->startTable();

// these are the columns in the table
$columns = Array(  "#" => 'width="20" class="sectiontableheader" ', 
					"<input type=\"checkbox\" name=\"toggle\" value=\"\" onclick=\"checkAll(".$num_rows.")\" />" => 'class="sectiontableheader" width="20"',
					$VM_LANG->_('VM_BK_LIST_LBL') => 'class="sectiontableheader" width="50%"',
					$VM_LANG->_('VM_BK_LIST_STARTDATE') => 'class="sectiontableheader" width="20%"',
					$VM_LANG->_('VM_BK_LIST_ENDDATE') => 'class="sectiontableheader" width="20%"',
					$VM_LANG->_('E_REMOVE') => 'class="sectiontableheader" width="50px"'
				);

$listObj->writeTableHeader( $columns );

$db->query($list);
$i = 0;
$tmp_prop='';

while ($db->next_record()) {
	
	$listObj->newRow();
	
	// The row number
	$listObj->addCell( $pageNav->rowNumber( $i ) );
	
	// The Checkbox
	$listObj->addCell( vmCommonHTML::idBox( $i, $db->f("season_id"), false, "season_id" ) );
	
	$url = $_SERVER['PHP_SELF'] . "?page=$modulename.season_form&no_menu=$no_menu&property_id=$prop_id&season_id=". $db->f("season_id");
	$tmp_cell = "<a href=\"" . $sess->url($url) . "\">". $db->f("label"). "</a>";
	$listObj->addCell( $tmp_cell );
	
	$listObj->addCell( date('jS F Y', strtotime($db->f('dateFrom'))) );
	$listObj->addCell( date('jS F Y', strtotime($db->f('dateTo'))) );
	
	$listObj->addCell( $ps_html->deleteButton( "season_id", $db->f("season_id"), "deleteSeason", $keyword, $limitstart, "&property_id=$prop_id" ) );
	
	$i++;
}

$listObj->writeTable();
$listObj->endTable();
$listObj->writeFooter( $keyword, "&property_id=$prop_id" );
?>