<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
/**
*
* @version $Id: order.order_list.php 1227 2008-02-08 12:09:50Z soeren_nb $
* @package VirtueMart
* @subpackage html
* @copyright Copyright (C) 2004-2007 soeren - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
mm_showMyFileName( __FILE__ );
global $page, $ps_order_status, $my, $mosConfig_live_site, $mosConfig_absolute_path, $CURRENCY_DISPLAY, $vbDateFrom, $vbDateTo, $vmuser, $perm, $vm_mainframe;

//Add admin stylesheet for frontend usage
$vm_mainframe->addStyleSheet( VM_THEMEURL .'admin.styles.css' );

//Get the date from the session / request
$vbDateFrom = $mainframe->getUserStateFromRequest($option.'vbDateFrom', 'vbDateFrom', date('Y-m-01'));
$vbDateTo = $mainframe->getUserStateFromRequest($option.'vbDateTo', 'vbDateTo', date('Y-m-t'));

$month = vmGet($_REQUEST,'vbMonth');
$year = vmGet($_REQUEST,'vbYear');
if($month && $year){
	$_REQUEST['vbDateFrom'] = "$year-$month-01";
	$_REQUEST['vbDateTo'] = "$year-$month-".cal_days_in_month(CAL_GREGORIAN, $month, $year);
	$vbDateFrom = $mainframe->getUserStateFromRequest($option.'vbDateFrom', 'vbDateFrom', date('Y-m-01'));
	$vbDateTo = $mainframe->getUserStateFromRequest($option.'vbDateTo', 'vbDateTo', date('Y-m-t'));	
}

$show = vmGet( $_REQUEST, "show", "" );
$property_id = vmGet( $_REQUEST, "property_id", "" );
$form_code = "";

if(vmGet($_REQUEST,'func')){
	$property_id = '';
}

require_once( CLASSPATH . "pageNavigation.class.php" );
require_once( CLASSPATH . "htmlTools.class.php" );
require_once( CLASSPATH . "ps_html.php" );
require_once( CLASSPATH . "ps_order_booking.php" );

$ps_order_booking = new ps_order_booking();

if(!$ps_order_booking->validateOwner()){
	return false;
}

//Include the date filter
require_once(PAGEPATH.'order.date_select.php');

//Get the properties
$q = "SELECT * FROM #__hp_properties WHERE "
	.($property_id ? " id = $property_id AND " : '')
	."published ORDER BY name";
$dbp = new ps_DB();
$dbp->query($q);
$properties = array();
while($dbp->next_record()){
	$properties[$dbp->f('id')] = $dbp->f('name');
}

//Get then number of bookings
$num_rows = $ps_order_booking->countOrders($show, $vbDateFrom, $vbDateTo, $property_id);

//Get the main set of orders
$db = $ps_order_booking->getOrders($show, $vbDateFrom, $vbDateTo, $property_id);

$orders = array();
while($db->next_record()){
	
	if(!isset($orders[$db->f('property_id')])) $orders[$db->f('booking_property_id')] = array();
	$orders[$db->f('property_id')][] = $db->get_row();
	
}
  
// Create the Page Navigation
$pageNav = new vmPageNav( $num_rows, $limitstart, $limit );

// Create the List Object with page navigation
$listObj = new listFactory( $pageNav );

// print out the search field and a list heading
$listObj->writeSearchHeader('Booking Overview', VM_THEMEURL.'images/administration/dashboard/orders.png', $modulename, "booking_list");

$imgPath = IMAGEURL.'booking_board/';
?>
<style type="text/css">

table.adminlist th.title{
text-align: center;
}

<?php
$pending = '#efd296';
$confirmed = '#cfe99d';
$cancelled = '#bcbcbc';
$refunded = '#ef9996';
?>

.status_pending{border-bottom: 4px solid <?php echo $pending ?>}
.status_confirmed{border-bottom: 4px solid <?php echo $confirmed ?>}
.status_cancelled{border-bottom: 4px solid <?php echo $cancelled ?>}
.status_refunded{border-bottom: 4px solid <?php echo $refunded ?>}

.booking.pending{background-color: <?php echo $pending ?>}
.booking.confirmed{background-color: <?php echo $confirmed ?>}
.booking.cancelled{background-color: <?php echo $cancelled ?>}
.booking.refunded{background-color: <?php echo $refunded ?>}

.inMonth{
padding-left: 0px !important;
padding-right: 0px !important;
}

.arrival,
.departure{
background-repeat:no-repeat; 
background-position: center;
vertical-align: top; 
padding: 0px !important;
height: 100%;
}

.overlap{
height: 100%;
background-repeat:no-repeat; 
background-position: center;
}

.arrival.pending,
.overlap_pending{background-image: url('<?php echo $imgPath ?>pending_start.gif')}
.arrival.confirmed,
.overlap_confirmed{background-image: url('<?php echo $imgPath ?>confirmed_start.gif')}
.arrival.cancelled,
.overlap_cancelled{background-image: url('<?php echo $imgPath ?>cancelled_start.gif')}
.arrival.refunded,
.overlap_refunded{background-image: url('<?php echo $imgPath ?>refunded_start.gif')}

.departure.pending{background-image: url('<?php echo $imgPath ?>pending_end.gif')}
.departure.confirmed{background-image: url('<?php echo $imgPath ?>confirmed_end.gif')}
.departure.cancelled{background-image: url('<?php echo $imgPath ?>cancelled_end.gif')}
.departure.refunded{background-image: url('<?php echo $imgPath ?>refunded_end.gif')}


.overlap.pending.overlap_pending,
.overlap.confirmed.overlap_confirmed,
.overlap.cancelled.overlap_cancelled,
.overlap.refunded.overlap_refunded{background-image: url('<?php echo $imgPath ?>overlap.gif')}

table.adminlist tr{height: 100%;}
.arrival div.oneNight{position: relative; width: 100%; height: 100%;  margin-top: 4px}
.arrival a{position: absolute; right: -3px; top: 0px;}

td.prop_pending div{border-bottom: 4px solid <?php echo $pending ?>}
td.prop_confirmed div{border-bottom: 4px solid <?php echo $confirmed ?>}
td.prop_cancelled div{border-bottom: 4px solid <?php echo $cancelled ?>}
td.prop_refunded div{border-bottom: 4px solid <?php echo $refunded ?>}
</style>

<?php
//Include the status and property filter
require_once(PAGEPATH.'order.status_select.php');

//Start the table output
$listObj->startTable();

// these are the columns in the table
$prop_width = 100;
$columns = array('Property'=>'');

$fromDate = strtotime($vbDateFrom);
$toDate = strtotime($vbDateTo);

$totalDays = (($toDate - $fromDate) / 86400) + 1;

//Stores the unix time stamp for each day of this period
$unixDays = array();

for($i = $fromDate; $i <= $toDate; $i+=86400 ){
		
	$day = date('d', $i);
	$month = date('m', $i);
	$year = date('Y', $i);
	$fullDate = date('Y-m-d', $i);
	$month_last_day = cal_days_in_month(CAL_GREGORIAN, $month, $year);
	
	$today = "<span title='$day/$month/$year'>".(($month_last_day == $day || $day == 1) ? "$day/$month": $day).'</span>';
	if(!isset($unixDays[$i])) $unixDays[$i] = $fullDate;
	$columns[$today] = 'width="'. floor(((100) / $totalDays)) .'%"';	
	
}

$isAdmin = 0;
if(in_array($vmuser->gid,array(23,24,25))){
	$isAdmin = 1;
}

$listObj->writeTableHeader( $columns );

$order_status = array();

foreach($properties as $id => $prop_name) {
	
	$prop_name = "<div>$prop_name</div>";
	$prop_orders = vmget($orders, $id, array());
    
	
	if(!count($prop_orders)){
		$listObj->newRow();
	
		// The row number
		$listObj->addCell( $prop_name );
		
		$listObj->addCell( '&nbsp;', "colspan='$totalDays'" );
		
	}else{
		
		/**
		 * Create an array that holds the order status used for thie property
		 */
		$order_status_exists = array();
	    foreach($prop_orders as $order){
	    	if(!in_array($order->order_status,$order_status_exists)) $order_status_exists[] = $order->order_status;
	    }
		
	    foreach($list_status AS $_status => $status_label){
	    	
	    	if(in_array($_status, $order_status_exists)){
	    		
				$listObj->newRow( );
			
				// The row number
				$listObj->addCell( $prop_name, "class='prop_".strtolower($status_label)."'");
				
				$day_offset = 0;
				$last_booking = $fromDate - 86400;
				foreach($prop_orders as $order_id => $order){
					
					if($order->order_status == $_status){
					
						$dateFrom = strtotime($order->arrival);
						$dateTo = strtotime($order->departure);
						$nights = ($dateTo - $dateFrom) / 86400;						
						
						$status = strtolower($list_status[$order->order_status]);
						$class = "class='booking booking_$status'";	
						
						$name = "$order->first_name $order->last_name";	
						
						$link = ($isAdmin || constant('VB_VIEW_ORDER')==1) ? $_SERVER['PHP_SELF']."?option=com_virtuemart&page=order.booking_form&return=$page&order_id=$order->order_id" : '';
						
						$arrive = date("d/m/Y", $dateFrom);
						$depart = date("d/m/Y", $dateTo);
						$popup = "<table>
									<tr>
										<td><strong>Name:</strong></td>
										<td>".$name."</td>
									</tr>
									<tr>
										<td><strong>Arrival:</strong></td>
										<td>$arrive</td>
									</tr>
									<tr>
										<td><strong>Departure:</strong></td>
										<td>$depart</td>
									</tr>
									<tr>
										<td><strong>Subtotal:</strong></td>							 
										<td>".$CURRENCY_DISPLAY->getFullValue($order->order_subtotal)."</td>
									</tr>
									<tr>
										<td><strong>Total:</strong></td>							 
										<td>".$CURRENCY_DISPLAY->getFullValue($order->order_total)."</td>
									</tr>
									<tr>
										<td><strong>Status:</strong></td>							 
										<td>$status</td>
									</tr>
								</table>";
						$popup = str_replace("\r", "", $popup);
						$popup = str_replace("\n", "", $popup);
						
						$popup_title = 'Booking Info';				
						
						//These flags are used to work out if we need to be adding a start/end slash
						$startsPrevMonth = 0;
						$endsNextMonth = 0;
						$length = $nights;
						
						if($dateFrom < $fromDate && $dateTo > $toDate){
							$startsPrevMonth = 1;
							$endsNextMonth = 1;
						}else if($dateFrom < $fromDate && $dateTo <= $toDate){
							$startsPrevMonth = 1;
							$length--;
						}else if($dateFrom >= $fromDate && $dateTo > $toDate){
							$endsNextMonth = 1;
							$length--;
						}else{
							$length = $length--;
						}
						
						//Check if the booking is too short output the whole name
						//If so, just output an icon
						$truncate = 0;
						if($length <= 2){
							$name = '';	
						}else{
							$title_length = (3 * $length);
							//Check if we have a really long name that we need to truncate
							if($title_length < strlen($name)){					
								$name = substr($name, 0, $title_length).'...';		
								$truncate = 1;			
							}				
						}			
						
						//Create booking tooltip, if its been truncated, add the hiding div elements
						$divWidth = $nights * 17;
						$tooltip = mosToolTip( $popup, $popup_title, '', 'tooltip.png', $name, $link, 1 );										
						$tooltip = $truncate ? '<div style="width: '.$divWidth.'px; overflow: hidden"><div style="width: 500px">'.$tooltip.'</div></div>' : $tooltip;										
						
						//Check if this booking overlaps the end of another booking
						$isOverlap = array_key_exists($order->arrival,$listObj->cells[$listObj->y]);
						if($isOverlap) $overlap = 'class="booking '.$order_status[$order->property_id][$order->arrival].' overlap overlap_'.$status.'"';
						
						//First we check if the booking spans the entire month
						if($startsPrevMonth && $endsNextMonth){
							
							$colspan = $totalDays;
							$listObj->addCell( $tooltip, "colspan='$colspan' class='booking spanMonth $status'" );
						
						//Now check if the arrival date starts in the previous month
						}else if($startsPrevMonth){
							
							$colspan = ($dateTo - $fromDate) / 86400;	
							if($colspan > 0)
							$listObj->addCell( $tooltip, "colspan='$colspan' class='booking startInPrevMonth $status'" );
							
							$listObj->x = $order->departure;
							$listObj->addCell( '&nbsp;', "class='departure $status'" );
							
						//Now check if the departure date is in the next month
						}else if($endsNextMonth){
											
							//Calculate the colspan
							$colspan = ((($toDate - $dateFrom) + 86400) / 86400) - 1;
							
							//Add some padding cells so that the booking starts on the correct date
							$padding = (($dateFrom - $last_booking) / 86400) - 1;
							if($padding > 0){
								$listObj->addCell('&nbsp;', "colspan='$padding'" );
							}
							
							//Check if there is an existing booking that ends on today
							if($isOverlap){					
								$listObj->cells[$listObj->y][$order->arrival]['attributes'] = $overlap;
							}else{
								if($colspan > 1){
									$listObj->addCell( '&nbsp;', "class='arrival $status'" );	
								}else{
									$listObj->addCell( '<div class="oneNight">'.$tooltip.'</div>', "class='arrival endInNextMonth $status'" );	
								}					
							}
							
							$day_offset = $dayTo;
							
							if($colspan > 0){
								$listObj->addCell( $tooltip, "colspan='$colspan' class='booking $status'" );
							}
						
						//Now check if the arrival and departure dates are both in this month	
						}else{
							
							$colspan = $nights - 1;													
							
							//Add some padding cells so that the booking starts on the correct date
							$padding = (($dateFrom - $last_booking) / 86400) - 1;
							if($padding > 0){
								$listObj->addCell('&nbsp;', "colspan='$padding'" );
							}
							
							$day_offset += $padding + $nights;
							
							//Check if there is an existing booking that ends on today
							if(array_key_exists($order->arrival,$listObj->cells[$listObj->y])){					
								$listObj->cells[$listObj->y][$order->arrival]['attributes'] = $overlap;
							}else{
								if($nights > 1){
									$listObj->addCell( '&nbsp;', "class='arrival $status'" );	
								}else{
									$listObj->addCell( '<div class="oneNight">'.$tooltip.'</div>', "class='arrival $status'" );	
								}					
							}
								
							if($nights > 1){			
								$listObj->addCell( $tooltip, "colspan='$colspan' class='booking inMonth $status'" );
							}
							
							$listObj->x = $order->departure;
							$listObj->addCell( '&nbsp;', "class='departure $status'" );				
							
						}
						$last_booking = $dateTo;
						
						//Save this orders end date and status so it can be used on crossover days
						if(!isset($order_status[$order_id])){
							$order_status[$order->property_id] = array();
						}			
						$order_status[$order->property_id][$order->departure] = $status;			
					}
				}
				
				$rest_of_month = ($toDate - $last_booking) / 86400;
				if($rest_of_month > 0){
				
					$listObj->addCell('&nbsp;', "colspan='$rest_of_month'" );
				}
		    }
		}
	}
}

$listObj->newRow();
$listObj->addCell( "<img src='".$imgPath."blank.gif' width='120px' height='1px' />");
$listObj->addCell( '', "colspan='". (count($columns)-1) ."'");

$listObj->writeTable();

$listObj->endTable();

$listObj->writeFooter( $keyword, "&vbDateFrom=$vbDateFrom&vbDateTo=$vbDateTo&show=$show" );


function getToolTip(){
	//These flags are used to work out if we need to be adding a start/end slash
	$startsPrevMonth = 0;
	$endsNextMonth = 0;
	$length = $nights;
	
	if($dateFrom < $fromDate && $dateTo > $toDate){
		$startsPrevMonth = 1;
		$endsNextMonth = 1;
	}else if($dateFrom < $fromDate && $dateTo <= $toDate){
		$startsPrevMonth = 1;
		$length--;
	}else if($dateFrom >= $fromDate && $dateTo > $toDate){
		$endsNextMonth = 1;
		$length--;
	}else{
		$length = $length--;
	}
	
	//Check if the booking is too short output the whole name
	//If so, just output an icon
	$truncate = 0;
	if($length <= 2){
		$name = '';	
	}else{
		$title_length = (3 * $length);
		//Check if we have a really long name that we need to truncate
		if($title_length < strlen($name)){					
			$name = substr($name, 0, $title_length).'...';		
			$truncate = 1;			
		}				
	}			
	
	//Create booking tooltip, if its been truncated, add the hiding div elements
	$divWidth = $nights * 17;
	$tooltip = mosToolTip( $popup, $popup_title, '', 'tooltip.png', $name, $link, 1 );										
	$tooltip = $truncate ? '<div style="width: '.$divWidth.'px; overflow: hidden"><div style="width: 500px">'.$tooltip.'</div></div>' : $tooltip;	
}
?>