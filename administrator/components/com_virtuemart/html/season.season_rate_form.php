<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
/**
*
* @version $Id: tax.tax_form.php 1095 2007-12-19 20:19:16Z soeren_nb $
* @package VirtueMart
* @subpackage html
* @copyright Copyright (C) 2004-2007 soeren - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
error_reporting(E_ALL);
ini_set('display_errors','On');
*/

mm_showMyFileName( __FILE__ );

global $vmuser, $option;

require_once( CLASSPATH . "ps_season.php" );
require_once( CLASSPATH . "ps_property.php" );
mosCommonHTML::loadOverlib();

//Check the user can manage properties
$prop_ids = ps_property::getAllowedProperties();
if($prop_ids === false) return;

$prop_id	= vmGet($_REQUEST,'property_id','');
$mode 		= vmGet($_REQUEST,'mode','');
$hp_ext 	= $mode == 'hp_ext';
$rate_id 	= vmGet( $_REQUEST, 'rate_id', 0);


echo vmCommonHTML::scriptTag( $mosConfig_live_site .'/includes/js/calendar/calendar.js');
if( class_exists( 'JConfig' ) ) {
	// in Joomla 1.5, the name of calendar lang file is changed...
	echo vmCommonHTML::scriptTag( $mosConfig_live_site .'/includes/js/calendar/lang/calendar-en-GB.js');
} else {
	echo vmCommonHTML::scriptTag( $mosConfig_live_site .'/includes/js/calendar/lang/calendar-en.js');
}
echo vmCommonHTML::linkTag( $mosConfig_live_site .'/includes/js/calendar/calendar-mos.css');



// Get the season data
$ps_seaon = new ps_season();
$season = $ps_seaon->getSeason($rate_id);



//First create the object and let it print a form heading
$formObj = &new formFactory( $VM_LANG->_('VM_BK_NAV_HEADING') );
//Then Start the form
$formObj->startForm();


/**
 * Get list of tax Values
 */
$q = "SELECT `tax_rate_id`, `tax_rate` as tax_rate, 
	`tax_resort` as tax_resort, 
	(`tax_rate` + `tax_resort`) as tax_total  
	FROM `#__{vm}_tax_rate` ORDER BY 
	`tax_rate` DESC, 
	`tax_rate_id` ASC";

$db->query( $q ) ;

$ratesArr[0] = $VM_LANG->_( 'PHPSHOP_INFO_MSG_VAT_ZERO_LBL' ) ;

$tax_rates = Array( ) ;
$tax_rates[0] = 0;

$current_tax_rate = 0;
while( $db->next_record() ) {
	if($season->sf('tax_rate_id') == $db->f( "tax_rate_id" )) $current_tax_rate = $db->f( "tax_total" );
	$tax_rates[$db->f( "tax_rate_id" )] = $db->f( "tax_total" ) ;
	$ratesArr[$db->f( "tax_rate_id" )] = "(" . $db->f( "tax_rate" ) * 100 . "%)".(($db->f( "tax_resort" ) > 0) ? " + (" . $db->f( "tax_resort" ) * 100 . "%) = (". $db->f( "tax_total" ) * 100 ." %)" : '');
}
$tax_rates_display = ps_html::selectList( 'tax_rate_id', $season->sf('tax_rate_id'), $ratesArr, 1, '', 'id="tax_rate_id"');


/*
 * Get property info
 **/
$hpq = 'SELECT id,name FROM #__hp_properties WHERE id IN ('.implode(',', $prop_ids).') ORDER BY name';
$db->query($hpq);

$props = $db->loadObjectList();
$show_props = in_array($vmuser->gid, array(23,24,25));


/**
 * Get property tax
 */
$prop_tax_id = $season->f('property_id') ? $season->f('property_id') : vmGet($_REQUEST,'property_id','');
$hpq = 'SELECT tax_rate_id FROM #__{vm}_property_tax WHERE property_id = '.$prop_tax_id;
$hpq .= ($prop_tax_id ? " WHERE property_id = $prop_tax_id" : '');
$db->query($hpq);
$db->next_record();
$tax_rate_id = $db->f('tax_rate-id');


/**
 * Get the season info for drop down list and JS
 */
$q = "SELECT season_id, label, dateFrom, dateTo FROM #__{vm}_seasons ORDER BY dateFrom DESC";
$dbs = new ps_db();
$dbs->query($q);
$seasons = $db->loadObjectList();


/**
 * Loop through season and assemble JS and set drop down label
 */
$js_seasons = '';
foreach($seasons as &$s){
	
	$js_seasons .="
		sLabels[$s->season_id] = '$s->label';
		sDateFrom[$s->season_id] = '$s->dateFrom';
		sDateTo[$s->season_id] = '$s->dateTo';	
		";	
	
	$s->label = "$s->label : $s->dateFrom - $s->dateTo";
}


vmCommonHTML::loadMooTools();

?><br />

<style type="text/css">
div.header{
padding: 0px;
line-height: auto;
}
table.pricing td{
padding: 2px 5px 0px 0px;
}

h3.toggler{
color: #333;
background: #eee;
border: 1px solid #aaa;
cursor: pointer;
padding: 3px;
margin: 3px 0px;
}

h3.heading{
margin: 5px 5px 10px;
color: #333;
}
</style>
<script type="text/javascript">
//<!--

var current_tax = <?php echo $current_tax_rate ?>;

//Nightly/weekly calculation
window.addEvent('domready',function(){
	
	//Set on change events for nightly/weekly rate changes
	$('rate_night').addEvent('keyup',nightNetChange)
	$('rate_night_ppl').addEvent('keyup',nightNetChange)
	$('rate_night_gross').addEvent('keyup',nightGrossChange)
	$('rate_night_gross_ppl').addEvent('keyup',nightGrossChange)
	$('rate_week').addEvent('keyup',weekNetChange)
	$('rate_week_ppl').addEvent('keyup',weekNetChange)
	$('rate_week_gross').addEvent('keyup',weekGrossChange)
	$('rate_week_gross_ppl').addEvent('keyup',weekGrossChange)	
	$('per_person_pricing').addEvent('change',updatePrices)
	$('min_people').addEvent('keyup',setMinPeople)
	if($('tax_rate_id')) $('tax_rate_id').addEvent('change',taxChange)
	
	//Store tax rates
	var tax_rates = new Array();
	<?php
	foreach( $tax_rates as $tax_rate_id => $tax_rate ) {
		echo "tax_rates[\"$tax_rate_id\"] = $tax_rate;\n";
	}
	?>
	
	var perPerson = 1;
	//Update all fields
	function updatePrices(){

		var min_ppl = $('min_people').value;
		//Update the nightly rates
		$('rate_night').value = roundNumber($('rate_night_hide').value, 3);
		$('rate_night_ppl').value = roundNumber($('rate_night_hide').value * min_ppl, 3);
		$('rate_night_gross').value = roundNumber($('rate_night_hide').value * (1 + current_tax), 3);				
		$('rate_night_gross_ppl').value = roundNumber($('rate_night_hide').value * min_ppl * (1 + current_tax), 3);				
		
		//Set the weekly net and gross values
		$('rate_week').value = roundNumber($('rate_night_hide').value * 7, 3);		
		$('rate_week_ppl').value = roundNumber($('rate_night_hide').value * min_ppl * 7, 3);		
		$('rate_week_gross').value = roundNumber($('rate_night_hide').value * 7 * (1 + current_tax), 3);		
		$('rate_week_gross_ppl').value = roundNumber($('rate_night_hide').value * min_ppl * 7 * (1 + current_tax), 3);	
		$('rate_night_ppl_hide').value = $('rate_night_hide').value * $('min_people').value;	
		
		setPersonGross();
	}
	
	function validateInput(e){
		e = new Event(e); 
        var key = e.key;      
        return ( key.match(/^\d+$/) );
	}
	//Nightly net change
	function nightNetChange(e){	

		if(this.value.match(/\.$/) || this.value.match(/\.0$/)) return;
		
		var setMinPpl = this.getProperty('id') == 'rate_night_ppl';
		//Set the nightly rate
		$('rate_night_hide').value = setMinPpl ? $('rate_night_ppl').value / $('min_people').value : $('rate_night').value;	
		//Update all the other fields
		updatePrices();
	}
	//Nightly gross change
	function nightGrossChange(e){
		 	
		if(this.value.match(/\.$/) || this.value.match(/\.0$/)) return;
		
		var setMinPpl = this.getProperty('id') == 'rate_night_gross_ppl';
		//Set the nightly rate
		$('rate_night_hide').value = setMinPpl ? $('rate_night_gross_ppl').value / $('min_people').value / (1 + current_tax) : $('rate_night_gross').value / (1 + current_tax);
		//Update all the other fields
		updatePrices();
	}
	//Weekly net change
	function weekNetChange(e){
		
		if(this.value.match(/\.$/) || this.value.match(/\.0$/)) return;
		
		var setMinPpl = this.getProperty('id') == 'rate_week_ppl';	
		//Set the nightly rate
		$('rate_night_hide').value = setMinPpl ? $('rate_week_ppl').value / $('min_people').value / 7 : $('rate_week').value / 7;	
		//Update all the other fields
		updatePrices();
	}
	//Weekly gross change
	function weekGrossChange(e){
		
		if(this.value.match(/\.$/) || this.value.match(/\.0$/)) return;
		
		var setMinPpl = this.getProperty('id') == 'rate_week_gross_ppl';
		//Set the nightly rate
		$('rate_night_hide').value = setMinPpl ? $('rate_week_gross_ppl').value / $('min_people').value / (1 + current_tax) / 7 : $('rate_week_gross').value / (1 + current_tax) / 7;	
		//Update all the other fields
		updatePrices();	
	}
	//Updates the fields if the tax id changes
	function taxChange(){
		//Set the new tax rate
		current_tax = tax_rates[$('tax_rate_id').value];
		//Update all the other fields
		updatePrices();
	}
	//Sets the per person flag
	function setPersonGross(){
		perPerson = $('per_person_pricing').value;
		$ES('td.per_person').each(checkPersonGross);
		$ES('td.gross').each(checkPersonGross);
		
		if(perPerson == 1){
			$('min_ppl_affect').setStyle('display','');
		}else{
			$('min_ppl_affect').setStyle('display','none');
		}
		
	}	
	
	//SEt the elements display based on if per person or gross is on
	function checkPersonGross(el){			
		var show = 0;

		if(el.hasClass('per_person') && el.hasClass('gross') && perPerson == 1 && current_tax > 0){
			show = 1;
		}else if(el.hasClass('per_person') && perPerson == 1 && !el.hasClass('gross')){
			show = 1;
		}else if(el.hasClass('gross') && current_tax > 0 && !el.hasClass('per_person')){
			show = 1;
		}
		if(show){
			el.setStyle('display','');
		}else{
			el.setStyle('display','none');
		}
	}
	
	//Set the if min people has been changed
	function setMinPeople(){
		
		if($('min_ppl_affect_night').checked && perPerson == 1){
			$('rate_night_hide').value = $('rate_night_ppl_hide').value / $('min_people').value;
		}
		updatePrices();
	}
	
	//Set the prices
	updatePrices();
	
	
	//Store the original values for the season
	var origLabel = '';
	var origDateFrom = '';
	var origDateTo = '';
	var origSet = 0;
	
	//Setup label and date holder
	var sLabels = new Array();
	var sDateFrom = new Array();
	var sDateTo = new Array();
	<?php echo $js_seasons ?>
	
	//Season selection functions
	if($('season_id')){
		checkSeason();
		$('season_id').addEvent('change',function(){
			checkSeason();			
		})
	}	

	//Disable or enable the label and date fields
	function checkSeason(){	
		var season = $('season_id').getValue();	
		if(season == ''){

			//Capture the original values
			if(origSet == 0){
				origLabel = $('label').value;
				origDateFrom = $('dateFrom').value;
				origDateTo = $('dateTo').value;	
				origSet = 1;		
			}
			
			$('label').removeProperty('disabled').value = origLabel;			
			$('dateFrom').removeProperty('disabled').value = origDateFrom;
			$('dateTo').removeProperty('disabled').value = origDateTo;
			$('dateFromButton').removeProperty('disabled');
			$('dateToButton').removeProperty('disabled');
		}else{
			$('label').setProperty('disabled','disabled').value = sLabels[season];			
			$('dateFrom').setProperty('disabled','disabled').value = sDateFrom[season];
			$('dateTo').setProperty('disabled','disabled').value = sDateTo[season];
			$('dateFromButton').setProperty('disabled','disabled');
			$('dateToButton').setProperty('disabled','disabled');
		}
	}
		

	//Advanced options sldiers
	$$('h3.toggler').each(function(h3){
		
		var block = h3.getNext();		
		var fx = new Fx.Slide(block,{duration: 300});	
		fx.hide();			
		h3.addEvent('click', function(){
			fx.toggle();
		});	
	});
})

function roundNumber(num, dec) {
	if(num.toString().match(/[\.|\.0]$/)) return num
	var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
	return result;
}
//-->
</script>
<table class="adminform">
	<tr>
		<td width="55%" valign="top">
		<table class="adminform">
		    <tr> 
		      <td width="30%"><b><?php echo $VM_LANG->_('VM_BK_NAV_HEADING') ?></b></td>
		      <td>&nbsp;</td>
		      <td width="70%">&nbsp;</td>
		    </tr>
		     <?php if(!$hp_ext){?>
		    <tr>      
		      <td><?php echo $VM_LANG->_('VM_BK_FORM_PROPERTY') ?>: </td>  
		      <td>&nbsp;</td>    
		      <td><?php
			  	echo vmCommonHTML::selectList($props,'property_id','','id','name', $season->sf('property_id'));
		        ?>
		      </td>
		    </tr>
		    <?php } ?>
		    <?php if(count($tax_rates) > 1){ ?>
		    <tr>
		      <td><?php echo 'Select Tax Rate' ?>:</td>
		      <td><?php echo mosToolTip('Chaning the tax rate for this season will affect all other seasons for this property') ?></td>
		      <td>
		      	<?php
		      	echo $tax_rates_display;
		        ?>
		        <script type="text/javascript">
		        
				function getTaxRate() {
					var selected_value = document.adminForm.product_tax_id.selectedIndex;
					var parameterVal = document.adminForm.product_tax_id[selected_value].value;
				
					if ( (parameterVal > 0) && (tax_rates[parameterVal] > 0) ) {
						return tax_rates[parameterVal];
					} else {
						return 0;
					}
				}
		        </script>
		      </td>
		    </tr>
		    <?php } ?>
		    <?php if(defined('VB_SEASON_INDEPENDENT') && VB_SEASON_INDEPENDENT == 1){ ?>
		    <tr>
		      <td><?php echo $VM_LANG->_('VM_BK_FORM_LBL') ?>:</td>
		      <td><?php echo mosToolTip('Select a pre-defined season, these can be setup through the season administration panel.') ?></td>
		      <td>
		      	<?php echo vmCommonHTML::selectList($seasons,'season_id','','season_id','label', $season->sf('season_id')); ?>
		      </td>
		    </tr>
		    <?php } ?>
		    <tr>
		      <td><?php echo $VM_LANG->_('VM_BK_FORM_LBL') ?>:</td>
		      <td><?php echo mosToolTip('Enter the label for this season rate.') ?></td>
		      <td>
		      	<input type="text" class="inputbox" id='label' name='label' value="<?php echo $season->sf('label'); ?>" size="20" />	
		      </td>
		    </tr>
		    <tr>
		    	<td><?php echo $VM_LANG->_('VM_BK_SEASON_START') ?></td>
		    	<td><?php echo mosToolTip('Enter the season start date') ?></td>
		        <td><input class="inputbox" type="text" name="dateFrom" id="dateFrom" size="20" maxlength="19" value="<?php echo $season->sf('dateFrom'); ?>" />
		        <input id="dateFromButton" name="reset" type="reset" class="button" onClick="return showCalendar('dateFrom', 'y-mm-dd');" value="..." /></td>
		    </tr>
		    <tr>
		    	<td><?php echo $VM_LANG->_('VM_BK_SEASON_END') ?></td>
		    	<td><?php echo mosToolTip('Enter the season end date') ?></td>
		        <td><input class="inputbox" type="text" name="dateTo" id="dateTo" size="20" maxlength="19" value="<?php echo $season->sf('dateTo'); ?>" />
		        <input id="dateToButton" name="reset" type="reset" class="button" onClick="return showCalendar('dateTo', 'y-mm-dd');" value="..." /></td>
		    </tr>
		    <tr>
		      <td><?php echo 'Pricing Calculated:' ?>:</td>
		      <td><?php echo mosToolTip('How do you wish to calculate pricing for this season, per person or per property? If per person pricing is used, the min people option below will affect the price. If per property is used, the price below will not be affected by the number of people.') ?></td>
		      <td>
		      	<?php
		      		$options = array(1=>'Per Person',0=>'Per Property');
		      		$default_pricing = defined('VB_PER_PERSON_PRICING') ? VB_PER_PERSON_PRICING : 1;      		
		      		$selected = $season->f('per_person_pricing') !== null ? $season->f('per_person_pricing') : $default_pricing;
		      		echo ps_html::selectList('per_person_pricing',$selected ,$options,1,0,'id="per_person_pricing"');
		      	?>
		      </td>
		    </tr>
		    <tr>
		      <td><?php echo $VM_LANG->_('VM_BK_SEASON_MIN_PPL_FORCE') ?>:</td>
		      <td><?php echo mosToolTip('Do you wish to force the user to book the minimum number of people. If not, then the per person price for the property will be multiplied by the min people figure below.') ?></td>
		      <td>
		      	<input type="checkbox" class="inputbox" value='1' name="min_people_force" <?php echo $season->sf('min_people_force') ? 'checked="checked"' : '' ?> />	
		      </td>
		    </tr>
		    <tr id='min_ppl_affect'>
		      <td><?php echo 'Chaning min people affects:' ?>:</td>
		      <td><?php echo mosToolTip('If you change the minimum people, how should this affect the price?<br />') ?></td>
		      <td>
		      	<input type='radio' id='min_ppl_affect_night' name='min_ppl_affect' checked="checked"> <label for='min_ppl_affect_night'>The per person price - Keep the total the same</label><br />
		      	<input type='radio' id='min_ppl_affect_total' name='min_ppl_affect'> <label for='min_ppl_affect_total'>The total price - Keep the per person price the same</label>
		      </td>
		    </tr>
		    <tr>
		      <td><?php echo $VM_LANG->_('VM_BK_SEASON_MIN_PPL') ?>:</td>
		      <td><?php echo mosToolTip('Please enter the minimum number of people for the property. Leave blank for none') ?></td>
		      <td>
		      	<input type="text" class="inputbox" name="min_people" id="min_people" value="<?php echo $season->sf('min_people'); ?>" size="16" />	
		      </td>
		    </tr>
		    <tr>
		      <td><?php echo $VM_LANG->_('VM_BK_SEASON_MAX_PPL') ?>:</td>
		      <td><?php echo mosToolTip('Please enter the maximum number of people for the property. Note that if the user tries to book over this number of people, they will receive an error. Leave blank for none') ?></td>
		      <td>
		      	<input type="text" class="inputbox" name="max_people" value="<?php echo $season->sf('max_people'); ?>" size="16" />	
		      </td>
		    </tr>
		    <tr>
		      <td><?php echo $VM_LANG->_('VM_BK_SEASON_RATE') ?>:</td>
		      <td><?php echo mosToolTip('Enter the season rate. You can enter a figure in any of the boxes to and the system will calculate the appropriate nightly/weekly net/gross rate. The net/gross total figures represent the price the user will pay if you have a minimum people value set and pricing is calculated per person.') ?></td>
		      <td>     
		      	<table class='pricing' cellspacing="0">
		      		<tr>
		      			<td>Net</td>      			
		      			<td class='gross'>Gross</td>
		      			<td class='per_person'>Net Total <?php echo mosToolTip('The net total represents the nightly/weekly net rate * the minimum number of people') ?></td>
		      			<td class='gross per_person'>Gross Total <?php echo mosToolTip('The net total represents the nightly/weekly gross rate * the minimum number of people') ?></td>
		      			<td></td>
		      		</tr>
		      		<tr>
		      			<td><input type="text" class="inputbox" id='rate_night' value="<?php echo round($season->sf('rate'), 3); ?>" size="10" />	
		      				<input type="hidden" id='rate_night_hide' name="rate" value="<?php echo $season->sf('rate'); ?>" />	
		      			</td>      			
		      			<td class='gross'>
		      				<input type="text" class="inputbox" id='rate_night_gross' value="<?php echo round($season->sf('rate') * (1+$current_tax_rate), 3); ?>" size="10" />
		      			</td> 
		      			<td class='per_person'>
		      				<input type="text" class="inputbox" id='rate_night_ppl' value="<?php echo round($season->sf('rate') * $season->sf('min_people'), 3); ?>" size="10" />	
		      				<input type="hidden" id='rate_night_ppl_hide' value="<?php echo $season->sf('rate') * $season->sf('min_people'); ?>" />		
		      			</td>     			
		      			<td class='gross per_person'>
		      				<input type="text" class="inputbox" id='rate_night_gross_ppl' value="<?php echo round($season->sf('rate') * $season->sf('min_people') * (1+$current_tax_rate), 3); ?>" size="10" />
		      			</td>
		      			<td>
		      				Per Night
		      			</td>
		      		</tr>
		      	</table> 	      	
		      </td>
		    </tr>
		    <tr>
		      <td><?php echo $VM_LANG->_('VM_BK_SEASON_RATE_WEEKLY') ?>:</td>
		      <td>&nbsp;</td>
		      <td>     
		      	<table class='pricing' cellspacing="0">
		      		<tr>
		      			<td>
		      				<input type="text" class="inputbox" id='rate_week' value="<?php echo round($season->sf('rate') * 7, 3); ?>" size="10" />	
		      			</td>      			
		      			<td class='gross'>
		      				<input type="text" class="inputbox" id='rate_week_gross' value="<?php echo round($season->sf('rate') * 7 * (1+$current_tax_rate), 3); ?>" size="10" />
		      			</td>
		      			<td class='per_person'>
		      				<input type="text" class="inputbox" id='rate_week_ppl' value="<?php echo round($season->sf('rate') * $season->sf('min_people') * 7, 3); ?>" size="10" />	
		      			</td>
		      			<td class='gross per_person'>
		      				<input type="text" class="inputbox" id='rate_week_gross_ppl' value="<?php echo round($season->sf('rate') * $season->sf('min_people') * 7 * (1+$current_tax_rate), 3); ?>" size="10" />
		      			</td>
		      			<td>
		      				Per Week
		      			</td>
		      		</tr>
		      	</table> 	      	
		      </td>
		      
		      <td>
		      	
		      </td>
		    </tr>    
		</table>
		</td>
		<td width="45%" valign="top">
			<div id="vb_advanced">
				<h3 class='toggler'>Advanced Options</h3>
				<div class='toggler'>
					<table class="adminform">
						<tr>
					      <td><?php echo $VM_LANG->_('VM_BK_SEASON_DISCOUNT') ?>:</td>
					      <td><?php echo mosToolTip('Enter a simple discount for this season') ?></td>
					      <td>
					      	<input type="text" class="inputbox" name='discount' value="<?php echo $season->sf('discount') ?>" size="5" /> %	
					      </td>
					    </tr>      
					    <tr>
					      <td><?php echo $VM_LANG->_('VM_BK_SEASON_MIN_STAY') ?>:</td>
					      <td><?php echo mosToolTip('Enter the minimum number of nights that the customer must book for. Leave blank for none') ?></td>
					      <td>
					      	<input type="text" class="inputbox" name="min_stay" value="<?php echo $season->sf('min_stay'); ?>" size="16" />	
					      </td>
					    </tr>
					    <tr>
					      <td><?php echo $VM_LANG->_('VM_BK_SEASON_MAX_STAY') ?>:</td>
					      <td><?php echo mosToolTip('Enter the maximum number of nights the customer can book for. Leave blank for none') ?></td>
					      <td>
					      	<input type="text" class="inputbox" name="max_stay" value="<?php echo $season->sf('max_stay'); ?>" size="16" />	
					      </td>
					    </tr>
					    <tr>
					      <td><?php echo $VM_LANG->_('VM_BK_SEASON_LONG_DAYS') ?>:</td>
					      <td><?php echo mosToolTip('Enter the number of days that a booking must reach to qualify for a long stay discount. Leave blank for none') ?></td>
					      <td>
					      	<input type="text" class="inputbox" name="long_stay_days" value="<?php echo $season->sf('long_stay_days'); ?>" size="16" />	
					      </td>
					    </tr>
					    <tr>
					      <td><?php echo $VM_LANG->_('VM_BK_SEASON_LONG_RATE') ?>:</td>
					      <td><?php echo mosToolTip('Enter the discount rate in % for nights above the long stay days discount') ?></td>
					      <td>
					      	<input type="text" class="inputbox" name="long_stay_rate" value="<?php echo $season->sf('long_stay_rate'); ?>" size="16" />	
					      </td>
					    </tr>
					    <tr>
					      <td><?php echo $VM_LANG->_('VM_BK_SEASON_CHANGEOVER') ?>:</td>
					      <td><?php echo mosToolTip('Enter a change over day for the property. Leave blank for none') ?></td>
					      <td>
					      	<?php 
					      	$arr = array();
					      	foreach($VM_LANG->modules['season']['VM_BK_SEASON_DAYS'] as $k => $v){
					      		$arr[] = vmCommonHTML::makeOption($k+1,$v);
					      	}      	
					      	echo vmCommonHTML::selectList($arr,'changeover_day','','value','text',$season->sf('changeover_day')) ?>
					      </td>
					    </tr>
					    <tr>
					      <td><?php echo $VM_LANG->_('VM_BK_SEASON_FCHANGEOVER') ?>:</td>
					      <td><?php echo mosToolTip('If you wish to force the user to book from and to changeover days select this option.') ?></td>
					      <td>
					      	<input type="checkbox" class="inputbox" name="changeover_force" <?php echo $season->sf('changeover_force') ? 'checked="checked"' : '' ?> value='1' />
					      </td>
					    </tr>
					</table>
				</div>				
				<?php
				include(dirname(__FILE__).'/season.discount_methods.php');
				?>
			</div>
		</td>
	</tr>
</table>

<?php

// Add necessary hidden fields


if($hp_ext){
	$formObj->hiddenField( 'property_id', ($rate_id ? $db->f('property_id') : $prop_id) );
	/*Organic Mod: Hard coded for now while i test e-cheque */
	/*$formObj->hiddenField( 'property_id', 48 );*/
	$formObj->hiddenField( 'mode', 'hp_ext' );
}

$funcname = !empty($rate_id) ? "updateRate" : "addRate";

$formObj->hiddenField( 'rate_id', $rate_id );

if(!defined('VB_SEASON_INDEPENDENT') || VB_SEASON_INDEPENDENT == 0) $formObj->hiddenField( 'season_id', 0 );

// Write your form with mixed tags and text fields
// and finally close the form:
$formObj->finishForm( $funcname, vmGet($_REQUEST,'return',"$modulename.season_rate_list"), $option );

?>