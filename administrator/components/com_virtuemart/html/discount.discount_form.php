<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
/**
*
* @version $Id: tax.tax_form.php 1095 2007-12-19 20:19:16Z soeren_nb $
* @package VirtueMart
* @subpackage html
* @copyright Copyright (C) 2004-2007 soeren - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
mm_showMyFileName( __FILE__ );

require_once(CLASSPATH.'ps_discount.php');

$ps_discount = new ps_discount();

//First create the object and let it print a form heading
$formObj = &new formFactory( $VM_LANG->_('PHPSHOP_TAX_FORM_LBL') );
//Then Start the form
$formObj->startForm();

$discount_id= vmGet( $_REQUEST, 'discount_id');
$option = empty($option)?vmGet( $_REQUEST, 'option', 'com_virtuemart'):$option;

if (!empty($discount_id)) {
  $q = "SELECT * FROM #__{vm}_discounts WHERE discount_id='$discount_id'"; 
  $db->query($q);  
  $db->next_record();
}
?><br />
<style type="text/css">
.paramlist{
	width: 450px;
}
</style>
<table class="adminform">
    <tr> 
      <td width="250px"><b><?php echo 'Discount: '.($discount_id ? $db->f('label') : 'New') ?></b></td>
      <td>&nbsp;</td>
    </tr>
    <?php if($discount_id){ ?>
    <tr> 
      <td align="right" >Description:</td>
      <td>
        <?php echo ps_discount::getDiscountDesc(CLASSPATH."discounts/".$db->f('file').".xml"); ?>
      </td>
    </tr>
    <?php } ?>
    <tr> 
      <td align="right" >Enabled:</td>
      <td>
        <input type='checkbox' name='published' value="1" <?php echo ($discount_id ? $db->f('published') : 1) ? 'checked="checked"' : '' ?>  />
      </td>
    </tr>
    <tr> 
      <td align="right" >Show in season pricing:</td>
      <td>
        <input type='checkbox' name='published_season' value="1" <?php echo ($discount_id ? $db->f('published_season') : 0) ? 'checked="checked"' : '' ?>  />
      </td>
    </tr>
    <tr> 
      <td align="right" ><?php echo 'Label'//$VM_LANG->_('PHPSHOP_TAX_FORM_COUNTRY') ?>:</td>
      <td>
        <input type='text' name='label' size='50' value="<?php echo $discount_id ? $db->f('label') : '' ?>"  />
      </td>
    </tr>
    <tr> 
      <td align="right" ><?php echo 'File'//$VM_LANG->_('PHPSHOP_TAX_FORM_STATE') ?>:</td>
      <td><?php 
        echo $ps_discount->list_discounts('file', ($discount_id ? $db->sf('file') : ''));
        ?>
      </td>
    </tr>
    <tr> 
      <td align="right" ><?php echo 'Class'//$VM_LANG->_('PHPSHOP_TAX_FORM_RATE') ?>:</td>
      <td> 
        <?php echo $discount_id ? $db->f('class').'()' : '' ?>
      </td>
    </tr>
    <tr>
      <td align="right"><?php echo 'Ordering'//$VM_LANG->_('PHPSHOP_MODULE_LIST_ORDER') ?>:</td>
      <td valign="top"><?php 
      if($discount_id){
        echo $ps_discount->list_order( $db->f("ordering") );
        echo "<input type='hidden' name='currentpos' value='".$db->f("ordering")."' />";
      }else{
      	 echo 'Ordering will be available once you have saved the discount.';
      	 echo "<input type='hidden' name='ordering' value='' />";
      	 echo "<input type='hidden' name='currentpos' value='' />";
      }
      ?>
      </td>
    </tr>
    <tr> 
      <td align="right" ><?php echo '---- Parameters ----';//$VM_LANG->_('PHPSHOP_TAX_FORM_RESORT') ?></td>
      <td> 
        <?php 
        if($discount_id){
        	$paramfile = CLASSPATH."discounts/".$db->f('file').".xml";
        	if(!file_exists($paramfile)) {      	
			 	echo "Could'nt find the XML file, there are no parameters for this discount";
        	}
        }else{
        	echo 'Parameters will be available once you have saved the discount.';
        }?>        
      </td>
    </tr>
    <?php if(isset($paramfile) && file_exists($paramfile)) {  
    		echo '<tr><td colspan="2">';    	     	
        	$params = new vmParameters( $db->f('params'), $paramfile, 'discount' );
        	echo $params->render();	
        	echo '</td></tr>';
    } ?>   
    <tr align="center">
      <td colspan="2" >&nbsp;</td>
    </tr>
</table>
<?php

// Add necessary hidden fields
$formObj->hiddenField( 'discount_id', $discount_id );

$funcname = !empty($discount_id) ? "updateDiscount" : "addDiscount";

// Write your form with mixed tags and text fields
// and finally close the form:
$formObj->finishForm( $funcname, $modulename.'.discount_list', $option );
?>