<?php

/**
 * VirtueBook: Cron script to send emails to maintenance people containing the checkins/outs
 */


global $ps_booking, $sess, $vmLogger, $VM_LANG;
$VM_LANG->load('order_emails');

$offset = vmGet($_REQUEST,'offset',3);
$days = vmGet($_REQUEST, 'days', 7);

/**
 * Get vendor information
 */
$dbv = new ps_DB;
$qt = "SELECT * from #__{vm}_vendor ";
/* Need to decide on vendor_id <=> order relationship */
$qt .= "WHERE vendor_id = '".vmGet( $_SESSION, 'ps_vendor_id', 1 )."'";
$dbv->query($qt);
$dbv->next_record();

$vendor_email = $dbv->f("contact_email");
$vendor_name = $dbv->f("vendor_name");

$imagefile = pathinfo($dbv->f("vendor_full_image"));
$extension = $imagefile['extension'] == "jpg" ? "jpeg" : "jpeg";

$EmbeddedImages = array();
$EmbeddedImages[] = array(	'path' => IMAGEPATH."vendor/".$dbv->f("vendor_full_image"),
					'name' => "vendor_image", 
					'filename' => $dbv->f("vendor_full_image"),
					'encoding' => "base64",
					'mimetype' => "image/".$extension );


/**
 * Get the properties
 */
$properties = array();

$dbp = new ps_DB();
$q = "
SELECT * FROM #__hp_properties AS p WHERE p.published = 1
ORDER BY p.name";
$dbp->query($q);
$db = new ps_db();
while($dbp->next_record()){
	$tmp = $dbp->get_row();
	$tmp->arrival = array();
	$tmp->departure = array();
	$properties[$tmp->id] = $tmp;
}


/**
 * Get the bookings
 */
$today = strtotime(date('Y-m-d'));
$fromDateUnix = strtotime( ($offset > 0 ? '+' : '')."$offset days", $today);
$toDateUnix = strtotime("+$days days", $fromDateUnix);
$fromDate = date('Y-m-d',$fromDateUnix);
$toDate = date('Y-m-d',$toDateUnix);

$db = new ps_DB();
$q = "
SELECT * FROM #__{vm}_orders AS o
LEFT JOIN #__{vm}_order_booking AS b on b.order_id = o.order_id
WHERE (b.arrival BETWEEN '$fromDate' AND '$toDate' OR b.departure BETWEEN '$fromDate' AND '$toDate')
AND o.order_status = 'C'
ORDER BY b.arrival";
$db->query($q);

$checkin = array();
$checkout = array();
while($db->next_record()){
	$pid = $db->f('property_id');
	$properties[$pid]->arrival[strtotime($db->f('arrival'))] = $db->get_row();	
	$properties[$pid]->departure[strtotime($db->f('departure'))] = $db->get_row();	
}


/**
 * Get the template file
 */
$tpl = vmTemplate::getInstance();
$tpl->set_vars(array('properties' => $properties,
					'fromDate' => $fromDate,
					'toDate' => $toDate,			
					'fromDateUnix' => $fromDateUnix,
					'toDateUnix' => $toDateUnix
				));
$mail_Body = $tpl->fetch('order_emails/maintenance_email.html.tpl.php');
//$mail_AltBody = $tpl->fetch('order_emails/maintenance_email.tpl.php');

		
$q = "SELECT ui.first_name, ui.last_name, ui.user_email FROM #__{vm}_user_info AS ui LEFT JOIN #__users AS u ON u.id = ui.user_id WHERE ui.perms = 'maintenance' AND u.block = 0";
$db->query($q);
		
$toDate = date('Y-m-d',$toDateUnix - 86400);
$subject = sprintf('%s - Weekly checkout schedule for %s - %s', $vendor_name, $fromDate, $toDate);	

$mail_Body = str_replace('[DATE_FROM]',$fromDate,$mail_Body);
$mail_Body = str_replace('[DATE_TO]',$toDate,$mail_Body);
$mail_Body = str_replace('[FROM]',ps_vendor::formatted_store_address(1),$mail_Body);

while($db->next_record()){

	$email = $db->f('user_email');
	$name = str_replace('&amp;',$db->f('first_name').' '.$db->f('last_name'), '');
	if($email){
		
		/**
		 * Send to customer
		 */
		$body = str_replace('[SUBJECT]',$subject,$mail_Body);
		$body = str_replace('[NAME]',$name,$body);		

		$success = vmMail( $vendor_email, $vendor_name, $email, $subject, $body, '', true, null, null, $EmbeddedImages);	
		if(!$success){		
			$vmLogger->err( 'Something went wrong while sending the email to '.$db->f('first_name').' '.$db->f('last_name').' at '.$email );
		}else{
			$vmLogger->info( 'Success: Email sent to  '.$db->f('first_name').' '.$db->f('last_name').' at '.$email );
		}
	}
}

/**
 * Send to vendor
 */
$body = str_replace('[SUBJECT]',$subject,$mail_Body);
$body = str_replace('[NAME]',$vendor_name,$body);
$body = str_replace('[DATE_FROM]',$fromDate,$body);
$body = str_replace('[DATE_TO]',$toDate,$body);
$body = str_replace('[FROM]',ps_vendor::formatted_store_address(1),$body);
if(!vmMail( $vendor_email, $vendor_name, $vendor_email, $subject, $body, '', true, null, null, $EmbeddedImages)){
	$vmLogger->err( 'Something went wrong while sending the email to '.$vendor_name.' at '.$vendor_email );	
}else{
	$vmLogger->info( 'Success: Email sent to  '.$vendor_name.' '.$vendor_name.' at '.$vendor_email );
}
exit('Finished');
?>