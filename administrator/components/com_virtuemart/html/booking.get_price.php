<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
/**
*
* @version $Id: tax.tax_list.php 1099 2007-12-21 12:46:46Z soeren_nb $
* @package VirtueMart
* @subpackage html
* @copyright Copyright (C) 2004-2007 soeren - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
mm_showMyFileName( __FILE__ );

$ps_booking->process(0);

produceXML($ps_booking);

function produceXML($ps_booking){
	global $CURRENCY_DISPLAY;
	
	
	$tpl = vmTemplate::getInstance();
	$tpl->set_vars(array(
						'booking' => $ps_booking,
						'status' => $ps_booking->status,
						'message' => $ps_booking->getMessages(0),
						'total' => $ps_booking->total	
					));
	
	$xml = $tpl->fetch('calendar/xml.response.tpl.php');
	
	//Send XML
	vmConnector::sendHeaderAndContent(200,$xml,'text/xml');	
	exit();
}

?>