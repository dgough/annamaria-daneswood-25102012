<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 

/**
 * VirtueBook: Cron script to send emails to customers prior to arrival
 */
$daysPriorToArriaal = 10;

ob_start();

//Set the subject
$subject = "Confirmation of your reservation prior to your scheduled arrival";

/*******/
$date = date('Y-m-d',(time()+($daysPriorToArriaal * 86400)));
echo "Fetching bookings that start on: $date";

global $sess, $vmLogger, $VM_LANG;
$VM_LANG->load('order_emails');

/**
 * Get vendor information
 */
$dbv = new ps_DB;
$qt = "SELECT * from #__{vm}_vendor ";
/* Need to decide on vendor_id <=> order relationship */
$qt .= "WHERE vendor_id = '1'";
$dbv->query($qt);
$dbv->next_record();

$vendor_email = $dbv->f("contact_email");
$vendor_name = $dbv->f("vendor_name");

$imagefile = pathinfo($dbv->f("vendor_full_image"));
$extension = $imagefile['extension'] == "jpg" ? "jpeg" : "jpeg";

$EmbeddedImages = array();
$EmbeddedImages[] = array(	'path' => IMAGEPATH."vendor/".$dbv->f("vendor_full_image"),
					'name' => "vendor_image", 
					'filename' => $dbv->f("vendor_full_image"),
					'encoding' => "base64",
					'mimetype' => "image/".$extension );



/**
 * Get the bookings
 */
$db = new ps_DB();
$db->query("SELECT b.* FROM #__vm_orders AS o
LEFT JOIN #__vm_order_booking as b on b.order_id = o.order_id
WHERE b.arrival = '$date' AND o.order_status = 'C'");

$dbp = new ps_DB();

echo '<br />Found '.$db->num_rows().' records<br />';

//Loop through bookings
while($db->next_record()){

	$booking = $db->get_row();
	$order_id = $booking->order_id;
	
	//Get the property details
	$dbp->query("SELECT p.*, ef1.value as latitude, ef2.value as longitude FROM #__hp_properties as p 
				 LEFT JOIN #__hp_properties2 as ef1 on p.id = ef1.property AND ef1.field = 40
				 LEFT JOIN #__hp_properties2 as ef2 on p.id = ef2.property AND ef2.field = 41
				 WHERE p.id = $booking->property_id");	

	$dbp->next_record();
	$property = $dbp->get_row();
	$property->address_array = array();
	if($property->address) 	$property->address_array[] = $property->address;
	if($property->suburb) 	$property->address_array[] = $property->suburb;	
	if($property->state) 	$property->address_array[] = $property->state;
	if($property->country) 	$property->address_array[] = $property->country;
	if($property->postcode) $property->address_array[] = $property->postcode;
	
	
	//Get the payment balance
	$dbp->query("SELECT sum(payment_amount) AS payment_due FROM #__vm_order_payment 
				WHERE order_id = $order_id AND (isnull(payment_cleared) OR payment_cleared = '' OR payment_cleared = '0000-00-00')");
	$dbp->next_record();
	$booking->balance = $GLOBALS['CURRENCY_DISPLAY']->getFullValue($dbp->f('payment_due'));
	
	//Get the payment balance
	$dbp->query("SELECT * FROM #__vm_order_user_info	WHERE order_id = $order_id");
	$dbp->next_record();
	$user = $dbp->get_row();
	$user->name = ($user->title ? $user->title : '')." $user->first_name $user->last_name"; 
	$user->address_array = array();
	if($user->address_1) 	$user->address_array[] = $user->address_1;
	if($user->address_2) 	$user->address_array[] = $user->address_2;
	if($user->city) 		$user->address_array[] = $user->city;
	if($user->state) 		$user->address_array[] = $user->state;
	if($user->zip) 			$user->address_array[] = $user->zip;
	$customer_email = $user->user_email;
	
	/**
	 * Get the template file
	 */
	$tpl = vmTemplate::getInstance();
	$tpl->set_vars(array('property' => $property,
						'booking' => $booking,
						'customer' => $user,					
						'subject' => $subject,						
						'account_url' => $sess->url( SECUREURL."index.php?page=account.index", true ),
						'terms_url' => $sess->url( SECUREURL."index.php?option=com_virtuemart&page=shop.tos&pop=1", true ),					
						'vendor_name' => $vendor_name,					
						'vendor_email' => $vendor_email						
					));

	$mail_AltBody = $tpl->fetch('order_emails/reservation_email.html.tpl.php');

	$mail_Body = '';
	
 	$success = vmMail( $vendor_email, $vendor_name, $customer_email, $subject, $mail_AltBody, $mail_Body, true, null, null, $EmbeddedImages);
	
 	vmMail( $customer_email, $customer_name, $vendor_email, $subject, $mail_AltBody, $mail_Body, true, null, null, $EmbeddedImages);
	if(!$success){		
		$vmLogger->err( 'Something went wrong while sending the resurvation email to '.$db->f('first_name').' '.$db->f('last_name').' at '.$customer_email );
	}else{
		$vmLogger->info( 'Success: sending the resurvation email to '.$db->f('first_name').' '.$db->f('last_name').' at '.$customer_email );
	}
}

ob_end_flush();
exit();
?>