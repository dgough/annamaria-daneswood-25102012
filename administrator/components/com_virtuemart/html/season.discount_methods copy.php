<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
/**
*
* @version $Id: tax.tax_form.php 1095 2007-12-19 20:19:16Z soeren_nb $
* @package VirtueMart
* @subpackage html
* @copyright Copyright (C) 2004-2007 soeren - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
error_reporting(E_ALL);
ini_set('display_errors','On');
*/

mm_showMyFileName( __FILE__ );

require_once(CLASSPATH.'ps_discount.php');

$db = new ps_DB();
$dbp = new ps_DB();
$db->query('SELECT * FROM #__{vm}_discounts WHERE published_season = 1 ORDER BY ordering');
if($db->num_rows()){
	echo "<h3 class='heading'>Discount Options</h3>";
}

while($db->next_record()){
	
	$discount_id = $db->f('discount_id');
	$paramfile = CLASSPATH."discounts/".$db->f('file').".xml";
	
	//Get the season specific discount params
	$dbp->query("SELECT * FROM #__{vm}_seasons_discounts WHERE discount_id = $discount_id AND season_rate_id = $rate_id");
	$dbp->next_record();
	
	$enabled = $dbp->num_rows() == 0 ? -1 : $dbp->f('enabled'); 
	
	echo sprintf("<h3 class='toggler'>%s</h3><div class='toggler'>",$db->f('label'));
	
	echo '<table width="100%" class="paramlist">';
	
	echo '<tr><td colspan="2">'.ps_discount::getDiscountDesc($paramfile).'</td></tr>';
	
	echo '<tr><td width="40%" valign="top" align="right">Enabled</td>';
	echo '<td>';
	
	echo "<input type='radio' name='discount_enabled[$discount_id]' value='-1' ".($enabled == -1 ? 'checked="checked"' : '').' /> Use Global';
	echo "<input type='radio' name='discount_enabled[$discount_id]' value='1' ".($enabled == 1 ? 'checked="checked"' : '').' /> Enabled';
	echo "<input type='radio' name='discount_enabled[$discount_id]' value='0' ".($enabled == 0 ? 'checked="checked"' : '').' /> Disabled';
	
	echo '</td></tr></table>';
	
	
	if(file_exists($paramfile)) {      	
				
	 	$params = new vmParameters( (trim($dbp->f('params')) ? $dbp->f('params') : $db->f('params')), $paramfile, 'discount' );
    	echo $params->render('discount_params['.$db->f('discount_id').']');	
	}
	
	echo '</div>';
}


?>