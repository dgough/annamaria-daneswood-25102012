<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 

mm_showMyFileName( __FILE__ );

global $VM_LANG, $modulename, $vm_mainframe, $sess, $ps_booking, $CURRENCY_DISPLAY, $vmLogger;

require_once( CLASSPATH . "htmlTools.class.php" );
require_once(CLASSPATH.'ps_order_booking.php');
$ps_order_booking = new ps_order_booking();
$ps_booking->display_errors = 0;
?>
<style type="text/css">

.month{
padding: 2px;
font-weight: bold;
}

.total{
text-align: right;
font-weight: bold;
}

</style>

<?php
$db = new ps_DB();
$dbd = new ps_DB();
$dbs = new ps_DB();

$dbd->query('SELECT * FROM #__{vm}_discounts WHERE `class`="ps_discount_gapfiller" AND `published` = 1');

if(!$dbd->num_rows()){
	$vmLogger->err('The Gap Filler discount module appears to be disabled or doesn\'t exist. Please install or enable.');
	return;
}

$paramfile = CLASSPATH."discounts/".$dbd->f('file').".xml";
$params = new vmParameters( $dbd->f('params'), $paramfile, 'discount' );

$db->setQuery("SELECT id, name FROM #__hp_properties WHERE published = 1");
$props = $db->loadObjectList('id');

$db->query("
SELECT b.*, p.id, p.name FROM #__{vm}_order_booking AS b 
LEFT JOIN #__hp_properties AS p ON p.id = b.property_id
LEFT JOIN #__hp_properties2 AS p2 ON p2.property = p.id AND p2.field = 55
LEFT JOIN #__{vm}_orders AS o ON o.order_id = b.order_id
WHERE departure >= now()
AND o.order_status IN ('C')
AND (p2.value != 'Yes' || ISNULL(p2.value))
ORDER BY p.name, arrival");

$months = array();
$currentProp = 0;
$prevDate = '';
$ps_booking_tmp = clone( $ps_booking );

while($db->next_record()){
	
	
	if($currentProp != $db->f('property_id')) $prevDate = '';
	$currentProp = $db->f('property_id');
	
	//Check that we have a previous date to check against
	if($prevDate){
		
		$arrival = strtotime($db->f('arrival'));
		//Check if the gap is 7 days or less
		$days = (round($arrival - $prevDate) / 86400);
		if($days < $params->get('nights') && $days > 0){
			
			$month = strtotime(date('Y-m-01',$arrival));
			$ps_booking_tmp->dateFrom = date('Y-m-d', $prevDate);
			$ps_booking_tmp->dateTo = date('Y-m-d', $arrival);
			$ps_booking_tmp->people = 1;			
			$price = $ps_booking_tmp->getPrice($currentProp, 0);
			
			//Check if this seasons is excluded
			$seasons = array_keys($ps_booking_tmp->seasons);
			$enabled = 0;
			
			// Organic mod: we are now allowing all short stays to be available if only gap-filler option is enabled
			$dbs->query(sprintf(
						'SELECT count(*) != 2 AS enabled 
						FROM #__{vm}_seasons_discounts AS s 
						LEFT JOIN jos_vm_discounts AS d ON d.discount_id = s.discount_id 
						WHERE s.season_rate_id IN (%s) 
						AND s.enabled = 0 
						AND (d.class = "ps_discount_shortbreaks" OR d.class = "ps_discount_gapfiller" )', implode(',',$seasons)
			) );
			
			$dbs->next_record();
			$enabled = $dbs->f('enabled');
			
			if($enabled){
				if(!isset($months[$month])) $months[$month] = array();
				$months[$month][] = array(
					'arrival' => $prevDate,
					'departure' => $arrival,
					'property_id' => $currentProp,
					'property_name' => $db->f('name'),
					'total' => $price
					);
			}
		}
		
	}
	$prevDate = strtotime($db->f('departure'));
}

//sort into month order
ksort($months);

// Create the List Object with page navigation
$listObj = new listFactory();

// print out the search field and a list heading
$listObj->writeSearchHeader( $VM_LANG->_('VM_GAPFILLER_TITLE'), '', $modulename, "");

// start the list table
$listObj->startTable();

//Write column headings
$listObj->writeTableHeader( array( $VM_LANG->_('VM_MONTH') => '',$VM_LANG->_('VM_PROPERTY')=>'',$VM_LANG->_('VM_ARRIVAL')=>'',$VM_LANG->_('VM_DEPARTURE')=>'',$VM_LANG->_('VM_TOTAL')=>'',$VM_LANG->_('VM_BOOK')=>'') );

foreach($months AS $month => $values){
	
	$listObj->newRow('month');
	
	// The month
	$listObj->addCell( strftime('%B  %Y',$month), 'class="month"' );
	
	//Spacer
	$listObj->addCell( '&nbsp;', 'colspan="5"' );
		
	foreach($values AS $value){
		$listObj->newRow();
		
		//Create the link
		$url = $_SERVER['PHP_SELF'] . "?page=booking.index";		
		$query = sprintf("property_id={$value['property_id']}&people=1&dateFrom[d]=%s&dateFrom[m]=%s&dateFrom[y]=%s&dateTo[d]=%s&dateTo[m]=%s&dateTo[y]=%s",
					date('d',$value['arrival']),
					date('m',$value['arrival']),
					date('Y',$value['arrival']),
					date('d',$value['departure']),
					date('m',$value['departure']),
					date('Y',$value['departure']));
		
		$url = $sess->url($url);
		$url .= (strstr($url,'?') ? '&amp;' : '?').$query;
		$linkStart = "<a href='$url' title='Click To Book'>";
		$linkEnd = "</a>";
		
		// The month
		$listObj->addCell( '&nbsp;', 'class="month"' );
		
		//The property
		$listObj->addCell( $linkStart. $props[$value['property_id']]->name.$linkEnd, 'class="property"' );
		
		//Arrival date
		$listObj->addCell( strftime('%d %B %Y',$value['arrival']), 'class="dates"' );
		
		//Departure
		$listObj->addCell( strftime('%d %B %Y',$value['departure']), 'class="dates"' );
		
		//The total
		$listObj->addCell( $CURRENCY_DISPLAY->getFullValue($CURRENCY->convert($value['total']['subtotal'])), 'class="total"' );
		
		//Book now
		$listObj->addCell( $linkStart. $VM_LANG->_('VM_BOOK_NOW').$linkEnd , 'class="total"' );
	
	}
	
}

$listObj->writeTable();

$listObj->endTable();

$listObj->writeFooter( '' );

?>
