<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );
/*
 * @version $Id: order.label_image.php 1122 2008-01-07 14:52:31Z thepisu $
 * @package VirtueMart
 * @subpackage html
 */
error_reporting(E_ALL);
ini_set('display_errors','On');
ini_set('memory_limit','60M');

mm_showMyFileName(__FILE__);

require_once( CLASSPATH . "pageNavigation.class.php" );
require_once( CLASSPATH . "htmlTools.class.php" );
require_once( CLASSPATH . "ps_payments.php" );

global $ps_booking, $limitstart, $limit;

$order_id = vmget($_REQUEST, 'order_id', '');

//Load mootools
vmCommonHTML::loadMooTools();

$db_total = new ps_DB();
$db_total->query("SELECT count(*) AS num_rows FROM #__{vm}_order_payment");
$db_total->next_record();

?>
<style type="text/css">
table.adminlist table td{
	border-bottom: 0px;
}

tr.heading{font-weight: bold}
tr.clearing{background-color: #EF7777 !important; color: #fff;}
tr.clearing0{background-color: #FFDFE0 !important}
tr.clearing1{background-color: #FFEFEF !important}
</style>
<h3><?php echo 'Booking Payments' ?></h3>

<?php

$num_rows = $db_total->f('num_rows');

// Create the Page Navigation
$pageNav = new vmPageNav( $keyword ? '' : $num_rows, $limitstart, $limit );

// Create the Payment class that creates a new List Object with page navigation
$ps_payments = new ps_payments($pageNav);

// print out the search field and a list heading
$ps_payments->listObj->writeSearchHeader('', '', $modulename, 'admin_payment_list');

// start the list table
$ps_payments->listObj->startTable();

$colcount = $ps_payments->getPaymentHeader();

$where = array("(op.payment_submitted != '' AND op.payment_submitted != '0000-00-00')","(op.payment_cleared = '000-00-00' OR isnull(op.payment_cleared) OR op.payment_cleared = '')");
if($ps_payments->getPayments($where, false, $keyword)){
	
	$ps_payments->listObj->newRow();	
	$ps_payments->listObj->addCell( '&nbsp;', "colspan='$colcount'" );
	
	$ps_payments->listObj->newRow( 'clearing heading');	
	$ps_payments->listObj->addCell( 'Payments waiting to be cleared.', "colspan='$colcount'" );
	
	$ps_payments->listObj->newRow();	
	$ps_payments->listObj->addCell( '&nbsp;', "colspan='$colcount'" );
	
	$ps_payments->renderPayments('clearing');
	
}

$ps_payments->listObj->newRow();	
$ps_payments->listObj->addCell( '&nbsp;', "colspan='$colcount'" );

$ps_payments->listObj->newRow('heading');	
$ps_payments->listObj->addCell( 'All payments', "colspan='$colcount'" );

$ps_payments->listObj->newRow();	
$ps_payments->listObj->addCell( '&nbsp;', "colspan='$colcount'" );

$ps_payments->getPayments(array(), false, $keyword);
$ps_payments->renderPayments();

$ps_payments->listObj->writeTable();
$ps_payments->listObj->endTable();
echo '<input type="hidden" name="toggle" value="1" onclick="return" />';

$ps_payments->listObj->writeFooter( $keyword, "&order_id=$order_id" );

?>