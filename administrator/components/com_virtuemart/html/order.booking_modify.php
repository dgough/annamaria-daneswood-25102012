<?php 
if(!defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );
	
	error_reporting(0);

	include_once(CLASSPATH.'htmlTools.class.php');

	global $vmuser, $perm, $mosConfig_live_site, $ps_booking;
	
	$order_id = isset($order_id) ? $order_id : vmGet($_REQUEST,'order_id');
	$property_id = $order_id ? $dbb->f('property_id') : vmGet($_REQUEST,'property_id');
	$user_id = vmGet($_REQUEST,'user_id',0);
	
	//Only allow owner etc to select themselves or a new user
	$dbu = new ps_DB();	
	if(!in_array($vmuser->gid,array(23,24,25))){
		$user_id = !isset($_REQUEST['user_id']) ? $vmuser->id : ($user_id == 0 ? 0 : $vmuser->id);
	}
	$q = "SELECT * FROM #__users AS u LEFT JOIN #__{vm}_user_info AS ui ON id=user_id ";
	$q .= "WHERE id=".($order_id ? $db->f('user_id') : $user_id );
	$q .= " AND (address_type='BT' OR address_type IS NULL ) ";
	
	$dbu->query($q);
	
	$dbu->next_record();
	
	$prop = $property_id ? vmGet($_REQUEST,'property_id') : ($order_id ? $dbb->f('property_id') : 0);
	$ps_calendar =& new ps_calendar('vm_booking',$prop,1);
	$ps_calendar->onLoad = 1;
	$ps_calendar->setScripts();
	$ps_calendar->getCalendar();
	
	$r_dateFrom = vmGet($_REQUEST,'dateFrom');
	$r_dateTo = vmGet($_REQUEST,'dateTo');
	if($r_dateFrom && $r_dateTo){
		$dateFrom = $r_dateFrom['y'].'-'.$r_dateFrom['m'].'-'.$r_dateFrom['d'];
		$dateTo = $r_dateTo['y'].'-'.$r_dateTo['m'].'-'.$r_dateTo['d'];		
	}else{
		$dateFrom = !$order_id ? date('Y-m-d') : $dbb->f('arrival');	
		$dateTo = !$order_id ? date('Y-m-d') : $dbb->f('departure');	
	}
	
?>
	<script type="text/javascript">
    //<!--
	window.addEvent('<?php echo !$order_id ? 'domready' : 'load' ?>', function() {
		/*
		* For updating the price : 
		*/	
			
		$('get_price').addEvent('click', function(e) {			
			
			var agree = <?php echo $order_id ? 'confirm("Are you sure you wish to modify the price for the new dates that you have entered?")' : 1 ?>;
			
			if (agree) {

				var url = '<?php echo $mosConfig_live_site ?>'+$('modify_booking_form').getProperty('action');
				
				var req = new Ajax(url, {				
					data: ($('modify_booking_form').toQueryString())+'&no_html=1&no_toolbar=1&only_page=1&no_menu=1&action=ajax&task=price&page=order.booking_modify_ajax&func=',
					method:'post',
					onComplete: function (responseText, responseXML) {
						
						var status = getText(responseXML.getElementsByTagName('status').item(0));
						var message = getText(responseXML.getElementsByTagName('message').item(0));
						var total = getText(responseXML.getElementsByTagName('total').item(0));
						var subtotal = getText(responseXML.getElementsByTagName('subtotal').item(0));
						var tax = getText(responseXML.getElementsByTagName('tax').item(0));
						
						if (status == 1){
							$('new_total').value = round(total,2);
							$('new_subtotal').value = round(subtotal,2);
							$('new_tax').value = round(tax,2);
						}else{
							alert(message);							
						}
					}
				}).request();				
			} //end if	
			
			new Event(e).stop();		
		});
		
		if($('update_booking')){			
			$('<?php echo $ps_calendar->reference ?>-dateFrom-d').addEvent('change',checkUpdateBooking)
			$('<?php echo $ps_calendar->reference ?>-dateFrom-m').addEvent('change',checkUpdateBooking)
			$('<?php echo $ps_calendar->reference ?>-dateFrom-y').addEvent('change',checkUpdateBooking)
			$('<?php echo $ps_calendar->reference ?>-dateTo-d').addEvent('change',checkUpdateBooking)
			$('<?php echo $ps_calendar->reference ?>-dateTo-m').addEvent('change',checkUpdateBooking)
			$('<?php echo $ps_calendar->reference ?>-dateTo-y').addEvent('change',checkUpdateBooking)
			$('new_total').addEvent('keyup',checkUpdateBooking)		
			$('get_price').addEvent('click',checkUpdateBooking)					
		}
		
		function checkUpdateBooking(){
			if($('update_booking')){
				$('update_booking').checked = 'checked';
			}
		}
		
		function getText(node) {
			if(typeof(node.text) == 'undefined') {
				return node.textContent;
			} else {
				return node.text;
			}
		}
		
		function round(num, dec) {
			var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
			return result;
		}
		
		
		/**
		* Function for handling chaning of property
		*/
		if($('property_id')){
			$('property_id').addEvent('change',reloadPage)		
		}
		if($('user_id')){
			$('user_id').addEvent('change',reloadPage)		
		}
		function reloadPage(){
				window.location = '<?php echo vmGet($_SERVER,'PHP_SELF','index2.php').'?pshop_mode=admin&page='.$page.'&option=com_virtuemart&'; ?>user_id=' + $('user_id').value + '&property_id=' + $('property_id').value;	
		}		
		<?php
		/**
		 * Get property tax
		 */
		$dbt = new ps_DB();
		$dbt->query("SELECT (t.tax_rate + t.tax_resort) as tax FROM #__{vm}_tax_rate AS t
					LEFT JOIN #__{vm}_property_tax AS p ON p.tax_rate_id = t.tax_rate_id
					WHERE p.property_id = $property_id");
		$dbt->next_record();
		?>
		
		var tax = 1 + <?php echo $dbt->f('tax') ?  $dbt->f('tax') : '0' ?>;
		$('new_total').addEvent('keyup',function(){			
			$('new_subtotal').value = round(($('new_total').value / tax),2);
			$('new_tax').value = round(($('new_total').value - $('new_subtotal').value),2);	
			$('use_new_price').setProperty('checked','checked');
		})
		
		$('new_subtotal').addEvent('keyup',function(){		
			$('new_total').value = round(($('new_subtotal').value * tax),2);
			$('new_tax').value = round(($('new_total').value - $('new_subtotal').value),2);		
			$('use_new_price').setProperty('checked','checked');	
		})
			
		if($('make_free')){
			$('make_free').addEvent('change', function(){
				if(this.getProperty('checked')) {
					$('new_subtotal').setProperty('value',0)
					$('new_subtotal').fireEvent('keyup')
					$('new_subtotal').setProperty('disabled','disabled')
					$('new_total').setProperty('disabled','disabled')
				}else{
					$('new_subtotal').removeProperty('disabled')
					$('new_total').removeProperty('disabled')
				}
			})
		}
	});
	
	//-->    
    </script>    
	<h3><?php echo $order_id ? $VM_LANG->_('VM_ORDER_MODIFY_HEADING') : $VM_LANG->_('VM_ORDER_ADD_HEADING') ?></h3>
	<?php if($order_id && $VM_LANG->_('VM_ORDER_MODIFY_TIP')){ ?>
		<div class='tip'>
			<?php echo $VM_LANG->_('VM_ORDER_MODIFY_TIP') ?>
		</div>
	<?php }else if(!$order_id && $VM_LANG->_('VM_ORDER_ADD_TIP')){?>
		<div class='tip'>
			<?php echo $VM_LANG->_('VM_ORDER_ADD_TIP') ?>
		</div>
	<?php } ?>
    <?php
    $formFactory = new formFactory();
    
    $formFactory->startForm('adminForm',' id="modify_booking_form"');

	?>
	<fieldset class="vb-fieldset">    	
        <legend><?php echo $VM_LANG->_('VM_ORDER_LEGEND_ADMIN') ?></legend>
        <?php if($order_id){ ?>
        <div class="row update_booking">
        	<label for="update_booking"><?php echo $VM_LANG->_('VM_ORDER_MODIFY_UPDATE') ?></label>          
          	<span>
          		<input type="checkbox" name="update_booking" id="update_booking" value="1" /> <?php echo $VM_LANG->_('VM_ORDER_MODIFY_UPDATE_NOTE') ?>
          	</span>
         </div>
         <?php }else{ ?>
         	<div class="row user_select">
                <label><?php echo $VM_LANG->_('VM_ORDER_MODIFY_USER') ?> : </label> <span><?php echo vmCommonHTML::selectList($users,'user_id','id="user_id"','id','name',$user_id); ?> <?php echo $VM_LANG->_('VM_ORDER_MODIFY_USER_NOTE') ?></span>
            </div>
            <div class="row property_select">
                <label><?php echo $VM_LANG->_('VM_ORDER_MODIFY_PROPERTY') ?> : </label> <span><?php echo vmCommonHTML::selectList($allproperties,'property_id','id="property_id"','id','name',$property_id); ?></span>
            </div>
         <?php } ?>
         <div class="row ignore_validation">
        	<label><?php echo $VM_LANG->_('VM_ORDER_MODIFY_IGNORE_VALIDATION') ?></label>          
          	<span>
          		<input type="checkbox" name="ignore" value="1" checked='checked' /> <?php echo $VM_LANG->_('VM_ORDER_MODIFY_VALIDATION_NOTE') ?>
          	</span>
         </div>
         <div class="row customer_notify">
        	<label style='height: 50px'><?php echo $VM_LANG->_('PHPSHOP_ORDER_LIST_NOTIFY') ?></label>          
          	<span>
          		<input type="radio" name="notify_customer" value="N" checked='checked' /> <?php echo $VM_LANG->_('PHPSHOP_ADMIN_CFG_NO')?> <br />			
          		<input type="radio" name="notify_customer" value="HTML" /> <?php echo $VM_LANG->_('PHPSHOP_ADMIN_CFG_YES')?> - <?php echo $VM_LANG->_('VM_ORDER_MODIFY_NOTIFY_HTML') ?>
          	</span>
         </div>
    </fieldset> 
	    <fieldset class="vb-fieldset">
	    	<legend><?php echo $VM_LANG->_('VM_ORDER_LEGEND_BOOKING') ?></legend>
	        
	    	<div class="row booking_arrival">
	    	    <label><?php echo $VM_LANG->_('VM_ORDER_ARRIVAL') ?></label> <span><?php echo $ps_calendar->renderInput('dateFrom',$dateFrom) ?></span>
	    	</div>
	        <div class="row booking_departure">
	    	    <label><?php echo $VM_LANG->_('VM_ORDER_DEPARTURE') ?></label> <span><?php echo $ps_calendar->renderInput('dateTo',$dateTo,'dateFrom') ?></span>
	    	</div>	    	
	    	<div class="row booking_people">
	    	    <label><?php echo $VM_LANG->_('VM_ORDER_PEOPLE') ?></label> 
	    	    <span><?php for ($i=1; $i < 21; $i++) {
						$people_list[$i] = $i;
						}
						echo ps_html::dropdown_display('people',!$order_id ? 0 : $dbb->f('people'), $people_list,1,'');
				?> </span>
	    	</div>
		    <?php
			if ($order_id) {
			?>
		        <div class="row current_price">
		    	    <label><?php echo $VM_LANG->_('VM_ORDER_CURRENT_PRICE') ?></label> <span><?php echo $CURRENCY_DISPLAY->getFullValue($db->f('order_total'), '', $db->f('order_currency')); ?></span>
		    	</div>
		    <?php
		   	}
		    ?>
		 
		 <div class="row security_deposit">
		 	<label>Security Deposit</label><span><input type="text" id="security_deposit" name="security_deposit" size="6" value="<?php echo $order_id ? $dbb->f('insurance') : $ps_booking->securityDeposit; ?>" /></span>
		 </div>
		 
    	 <div class="row modify_price">
    	    <label><?php echo $VM_LANG->_('VM_ORDER_MODIFY_PRICE') ?></label>   	    
    	    <table>
    	    	<tr>
    	    		<th class='price_total'><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_TOTAL') ?></th>
    	    		<th class='price_subtotal'><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_SUBTOTAL') ?></th>
    	    		<th class='price_tax'><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_TAX_TOTAL') ?></th>
    	    		<th class='price_get'><?php echo $VM_LANG->_('VM_ORDER_MODIFY_UPDATE') ?></th>
    	    		<th class='price_use_modified'><?php echo $VM_LANG->_('VM_ORDER_MODIFY_USE_MOD') ?></th>
    	    		<th class='price_free'>Free booking?</th>
    	    	</tr>
    	    	<tr>
    	    		<td class='price_total'>
    	    			<input type="text" id="new_total" name="new_total" size="6" value="<?php echo !$order_id ? 0 : round($db->f('order_total'),2) ?>" />
    	    		</td>
    	    		<td class='price_subtotal'>
    	    			<input type="text" id="new_subtotal" name="new_subtotal" size="6" value="<?php echo !$order_id ? 0 : round($db->f('order_subtotal')) ?>" />
    	    		</td>
    	    		<td class='price_tax'>
    	    			<input type="text" id="new_tax" size="6" disabled="disabled" value="<?php echo !$order_id ? 0 : round($db->f('order_tax')) ?>" />
    	    		</td>
    	    		<td class='price_get'>
    	    			<input type="button" id="get_price" value=" <?php echo $VM_LANG->_('VM_ORDER_MODIFY_GET_PRICE') ?> " />
    	    			<?php echo vmToolTip($VM_LANG->_('VM_ORDER_MODIFY_GET_PRICE_TIP'))  ?>
    	    		</td>
    	    		<td align="center" class='price_use_modified'><input type="checkbox" name="use_new_price" id='use_new_price' value="1" /></td>
    	    		<td align="center" class='price_free'><input type="checkbox" name="make_free" id='make_free' value="1" /></td>
    	    	</tr>
    	    </table>
    	</div>
   
        
    </fieldset><br />
    
    <fieldset class="vb-fieldset">
    	
        <legend><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_PO_STATUS') ?></legend>
        <div class="row booking_status_select">
        	<label><?php echo "<strong>".$VM_LANG->_('PHPSHOP_ORDER_PRINT_PO_STATUS') .": </strong>"; ?> </label>
          	<span>
          	<?php $ps_order_status->list_order_status(!$order_id ? 0 : $db->f("order_status")); ?>
				<input type="hidden" name="vmtoken" value="<?php echo vmSpoofValue($sess->getSessionId()) ?>" />
				<input type="hidden" name="current_order_status" value="<?php !$order_id ? '' : $db->p("order_status") ?>" />
				<input type="hidden" name="order_id" value="<?php echo $order_id ?>" />
          	</span>
        </div>
        <div class="row booking_comment">
			<label>
				<?php echo "<strong>".$VM_LANG->_('PHPSHOP_COMMENT') .": </strong>"; ?>
			</label>
		  <span>
			<textarea name="order_comment" rows="4" cols="35"></textarea>
		  </span>
		</div>
		<div class='row booking_comment_include'>
			<label for="include_comment"><?php echo $VM_LANG->_('PHPSHOP_ORDER_HISTORY_INCLUDE_COMMENT') ?></label>
			<span><input type="checkbox" name="include_comment" id="include_comment" checked="checked" value="Y" /></span>
		 </div>
    </fieldset><br />
    
    <?php if(($order_id || !$order_id && $user_id ) && $VM_LANG->_('VM_ORDER_MODIFY_USERNOTE')){ ?>
    <div style='text-align: center; padding: 5px'>
    	<?php echo $VM_LANG->_('VM_ORDER_MODIFY_USERNOTE') ?>
    </div>
    <?php } ?>
    <?php
	
    $GLOBALS['VM_LANG']->load('admin');		
	$oldmodulename = $modulename;
	$modulename = 'admin';
		
	global $mosConfig_allowUserRegistration, $mosConfig_useractivation;
	require_once( CLASSPATH . "ps_userfield.php" );
	require_once( CLASSPATH . "htmlTools.class.php" );

	$missing = vmGet( $_REQUEST, "missing", "" );
	
	if (!empty( $missing )) {
		echo "<script type=\"text/javascript\">alert('".$VM_LANG->_('CONTACT_FORM_NC',false)."'); </script>\n";
	}
	
	if ($mosConfig_allowUserRegistration == "0") {
		$GLOBALS['vm_mainframe']->errorAlert('User registration is disabled, it must be enabled in order to proceed.');
		return;
	}
	
	if( vmIsJoomla( '1.5' ) ) {
		// Set the validation value
		$validate = JUtility::getToken();
	} else {
		$validate =  function_exists( 'josspoofvalue' ) ? josSpoofValue(1) : vmSpoofValue(1);
	}
	
	$fields = ps_userfield::getUserFields($order_id ? 'account' : 'registration', false, '', false );
	// Read-only fields on registration don't make sense.
	foreach( $fields as $field ) $field->readonly = 0;
		
	$skip_fields = array('agreed','delimiter_sendregistration');
	if($order_id) $skip_fields = array_merge($skip_fields,array( 'username', 'password', 'password2' ));
			
	// This is the part that prints out ALL registration fields!
	ps_userfield::listUserFields( $fields, $skip_fields, $dbu, false);
	
	$modulename = $oldmodulename;
	
	if (!$order_id) {
		//shopper perms by default - although will need to check that user_info not already populated
		$formFactory->hiddenField('perms','shopper');
		$formFactory->hiddenField('vendor_id','1');
		$formFactory->hiddenField('shopper_group_id','');

	} else {
		$sgu = ps_shopper_group::get_shoppergroup_by_id($dbu->f("user_id"));

		$formFactory->hiddenField('perms',$dbu->f('perms'));
		$formFactory->hiddenField('customer_number',ps_shopper_group::get_customer_num($dbu->f("user_id")));
		$formFactory->hiddenField('vendor_id',$dbu->f('vendor_id'));
		$formFactory->hiddenField('shopper_group_id',$sgu["shopper_group_id"]);
		$formFactory->hiddenField('property_id',$dbb->f('property_id'));
		$formFactory->hiddenField('order_id',$order_id);	
	}		
	$formFactory->hiddenField('useractivation',$mosConfig_useractivation);	
	$formFactory->hiddenField('Itemid',$sess->getShopItemid());	

$return = vmGet($_REQUEST,'return','order.booking_form');
$formFactory->finishForm($order_id ? 'orderUpdate' : 'orderAdd', $return);
?>