<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
/**
*
* @version $Id: reportbasic.index.php 1099 2007-12-21 12:46:46Z soeren_nb $
* @package VirtueMart
* @subpackage html
* @copyright Copyright (C) 2004-2007 soeren - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
mm_showMyFileName( __FILE__ );

global $CURRENCY_DISPLAY;

$nh_report = new nh_report();
$show_products = vmGet( $_REQUEST, "show_products" );
$interval = vmGet( $_REQUEST, "interval", "byMonth" );

foreach (array ('thisMonth', 'lastMonth', 'last60', 'last90', 'sbmt') as $button_name) {
	$$button_name = vmGet( $_REQUEST, $button_name );
}

$selected_begin["day"] = $sday = vmGet( $_REQUEST, "sday", 1 );
$selected_begin["month"] = $smonth = vmGet( $_REQUEST, "smonth", date("m"));
$selected_begin["year"] = $syear = vmGet( $_REQUEST, "syear", date("Y"));

$selected_end["day"] = $eday = vmGet( $_REQUEST, "eday", date("d") );
$selected_end["month"] = $emonth = vmGet( $_REQUEST, "emonth", date("m"));
$selected_end["year"] = $eyear = vmGet( $_REQUEST, "eyear", date("Y"));

$i=0;

?>
<!-- BEGIN body -->
&nbsp;&nbsp;&nbsp;<img src="<?php echo VM_THEMEURL ?>images/administration/dashboard/report.png" border="0" />&nbsp;&nbsp;&nbsp;
<span class="sectionname"><?php echo $VM_LANG->_('PHPSHOP_REPORTBASIC_MOD') ?> : Money Cleared</span><br /><br />
    <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
    <input type="hidden" name="page" value="reportbasic.payment_cleared" />
    <input type="hidden" name="option" value="com_virtuemart" />
	<input type="hidden" name="pshop_mode" value="admin" />
    <table class="adminform" width="100%" border="0" cellspacing="0" cellpadding="1">
        <tr>
          <td colspan="2">
            <hr noshade="noshade" size="2" color="#000000" />
          </td>
        </tr>
        <tr>
          <td><?php echo $VM_LANG->_('PHPSHOP_RB_INTERVAL_TITLE'); ?></td>

          <td><input type="radio" id="byMonth" name="interval" value="byMonth" <?php if($interval=="byMonth") echo "checked='checked'" ?> />
          <label for="byMonth"><?php echo $VM_LANG->_('PHPSHOP_RB_INTERVAL_MONTHLY_TITLE') ?></label> &nbsp; &nbsp; 
          <input type="radio" name="interval" id="byWeek" value="byWeek" <?php if($interval=="byWeek") echo "checked='checked'" ?> />
          <label for="byWeek"><?php echo $VM_LANG->_('PHPSHOP_RB_INTERVAL_WEEKLY_TITLE'); ?></label> &nbsp; &nbsp;
          <input type="radio" name="interval" id="byDay" value="byDay" <?php if($interval=="byDay") echo "checked='checked'" ?> />
          <label for="byDay"><?php echo $VM_LANG->_('PHPSHOP_RB_INTERVAL_DAILY_TITLE'); ?></label> &nbsp; &nbsp;
          <input type="radio" name="interval" id="byAll" value="byAll" <?php if($interval=="byAll") echo "checked='checked'" ?> />
          <label for="byAll">All</label> &nbsp; &nbsp;
		  <input type="radio" name="interval" id="byOccupancy" value="byOccupancy" <?php if($interval=="byOccupancy") echo "checked='checked'" ?> />
          <label for="byOccupancy">Occupancy</label> &nbsp; &nbsp;
          </td>
        </tr>

        <tr>
          <td colspan="2">
            <hr noshade="noshade" size="2" color="#000000" />
          </td>
        </tr>

        <tr>
          <td><?php echo $VM_LANG->_('PHPSHOP_SHOW') ?></td>

          <td>
          <input type="submit" class="button" name="thisMonth" value="<?php echo $VM_LANG->_('PHPSHOP_RB_THISMONTH_BUTTON'); ?>" /> &nbsp; 
          <input type="submit" class="button" name="lastMonth" value="<?php echo $VM_LANG->_('PHPSHOP_RB_LASTMONTH_BUTTON'); ?>" /> &nbsp; 
          <input type="submit" class="button" name="last60" value="<?php echo $VM_LANG->_('PHPSHOP_RB_LAST60_BUTTON'); ?>" /> &nbsp;
          <input type="submit" class="button" name="last90" value="<?php echo $VM_LANG->_('PHPSHOP_RB_LAST90_BUTTON'); ?>" />
          </td>
        </tr>

        <tr>
          <td colspan="2">
            <hr noshade="noshade" size="2" color="#000000" />
          </td>
        </tr>

        <tr valign="top">
          <td width="100"><?php echo $VM_LANG->_('PHPSHOP_RB_START_DATE_TITLE'); ?></td>

          <td><?php
          $nh_report->make_date_popups("s", $selected_begin );
          ?></td>
        </tr>

        <tr>
          <td width="100"><?php echo $VM_LANG->_('PHPSHOP_RB_END_DATE_TITLE'); ?></td>

          <td><?php $nh_report->make_date_popups("e", $selected_end ); ?></td>
        </tr>

        <tr>
          <td>&nbsp;</td>

          <td><input type="submit" class="button" name="sbmt" value="<?php echo $VM_LANG->_('PHPSHOP_RB_SHOW_SEL_RANGE') ?>" /> </td>
        </tr>
      </table>
    </form>
<!-- begin output of report -->
<?php 
 /* assemble start date */
 if (isset($thisMonth)) {
   $start_date = date('Y-m-01');
   $end_date = date('Y-m-d');
 }
 else if (isset($lastMonth)) {
   $start_date = date('Y-m-01',strtotime('-1 month'));
   $end_date = date('Y-m-t',strtotime('-1 month'));
 }
 else if (isset($last60)) {
   $start_date = date('Y-m-01',strtotime('-60 days'));
   $end_date = date('Y-m-d');
 }
 else if(isset ($last90)) {
   $start_date = date('Y-m-01',strtotime('-90 days'));
   $end_date = date('Y-m-d');
 }
 elseif (isset($sbmt)) {
   /* start and end dates should have been given, assign accordingly */
   $start_max_day = date("j",mktime(0,0,0,$smonth+1,0,$syear));
   if (! (intval($sday) <= $start_max_day)) {
     $sday = $start_max_day;
   }
   $start_date = $syear.'-'.$smonth.'-'.$sday;

   $end_max_day = date("j",mktime(0,0,0,intval($smonth)+1,0,$syear));
   if (! (intval($eday) <= $end_max_day)) {
     $eday = $end_max_day;
   }
   $end_date = $eyear.'-'.$emonth.'-'.$eday;
 }
 else {
 /* nothing was sent to the page, so create default inputs */
   $start_date = date('Y-m-01');
   $end_date = date('Y-m-d');
   $interval = "byMonth";
 }
 
$total_days = ((strtotime($end_date) - strtotime($start_date)) / (60 * 60 * 24)) + 1;

//echo $total_days;

$query_date_line = "";
 /* get the interval and set the date line for the query */
 switch ($interval) {
    case 'byMonth':
     $query_date_line .= "MONTH(payment_cleared) as month_number, ";
	 $query_group_line = "GROUP BY month_number";
	 $query_order_line = "ORDER BY a.name ASC, month_number ASC, payment_cleared ASC";
     break;
   case 'byWeek':
     $query_date_line .= "WEEK(payment_cleared) as week_number, ";
     $query_group_line = "GROUP BY week_number";
	 $query_order_line = "ORDER BY a.name ASC, week_number ASC, payment_cleared ASC";
     break;
   case 'byDay':
   /* query for days */
     $query_group_line = "GROUP BY payment_cleared";
	 $query_order_line = "ORDER BY a.name ASC, payment_cleared ASC";
     break;
   case 'byOccupancy':
   	 $query_group_line = "GROUP BY ob.property_id";
	 $query_order_line = "ORDER BY ob.property_id";
	 break;
   case 'byAll':
   default:
     $query_group_line = '';
	 $query_order_line = "ORDER BY a.name ASC, payment_cleared ASC";
    break;
  }
  
  /* better way of setting up query */
  $q  = "SELECT ";
  $u  = $q;
  
  if ($interval != 'byOccupancy') {
  	$query_between_line = "WHERE payment_cleared BETWEEN '" . $start_date . "' AND '" . $end_date . "' ";
  } else {
  	$query_between_line = "WHERE (
							(ob.arrival BETWEEN '".$start_date."' AND '".$end_date."' OR ob.departure BETWEEN '".$start_date."' AND '".$end_date."')
							OR (ob.arrival < '".$start_date."' AND ob.departure > '".$start_date."')
							OR (ob.arrival < '".$end_date."' AND ob.departure > '".$end_date."')
							)";
  }
  if ($query_date_line) {
    $q .= $query_date_line;
  }
  $q .= "p.name AS `property_name`, a.name, payment_cleared, ";
  if($interval == 'byAll') {
  	$q .= "(payment.order_id) as order_id, ";
	$q .= "(payment.payment_amount) as gross_inc_insurance, ";
	$q .= "(IF(payment.payment_stage = 0, payment.payment_amount - ob.insurance, payment.payment_amount)) AS gross, "; //Remove the insurance from the first stage payment
	$q .= "ROUND(ob.tax_total / ob.subtotal, 5) AS tax, ";
	$q .= "ROUND(ob.tax_resort / ob.subtotal, 5) AS resort_tax, ";
	$q .= "ROUND(ob.tax_state / ob.subtotal, 5) AS state_tax ";
  } else {
  	$q .= "COUNT(payment.payment_id) as payments, ";
	$q .= "SUM(payment.payment_amount) as gross_inc_insurance, ";
	$q .= "SUM(IF(payment.payment_stage = 0, payment.payment_amount - ob.insurance, payment.payment_amount)) AS gross, "; //Remove the insurance from the first stage payment
	$q .= "ROUND(ob.tax_total / ob.subtotal, 5) AS tax, ";
	$q .= "ROUND(ob.tax_resort / ob.subtotal, 5) AS resort_tax, ";
	$q .= "ROUND(ob.tax_state / ob.subtotal, 5) AS state_tax ";
  }
  
  if ($interval == 'byOccupancy') {
  	$q .= ", SUM(IF(payment.payment_stage = 0, DATEDIFF(IF(ob.departure>'".$end_date."', '".$end_date."', ob.departure), IF(ob.arrival<'".$start_date."', '".$start_date."', ob.arrival)), NULL)) + 1 AS occ_days ";
  }
  
  $q .= "FROM ( #__{vm}_order_payment AS `payment`, #__{vm}_order_booking AS `ob`, jos_hp_properties AS `p`, jos_hp_agents AS `a` ) ";
  $q .= "LEFT JOIN #__{vm}_orders AS o ON payment.order_id = o.order_id ";  
  $q .= $query_between_line;
  $q .= "AND payment.order_id = ob.order_id ";
  $q .= "AND ob.property_id = p.id ";
  $q .= "AND p.agent = a.id";
  $q .= " AND payment.payment_amount > 0";
  $q .= ' AND o.order_status = "C" ';
  if ($query_group_line) {
    $q .= $query_group_line . ", p.id ";
  }
  
  if ( $query_order_line ) {
  	$q .= $query_order_line;
  }

/* setup the db and query */
  $db = new ps_DB;
  $db->query($q);
  
  //echo $db->_database->_sql;
  
  $orders_total = 0;
  $gross_total = 0;
  $net_total = 0;
  $tax_total = 0;
  $state_tax_total = 0;
  $resort_tax_total = 0;
  $total_occupancy = 0;
 ?>
    <h4><?php 
    echo $VM_LANG->_('PHPSHOP_RB_REPORT_FOR') ." ";
    echo $start_date." --&gt; ". $end_date; 
    ?></h4>
	<span>Please Note: The report below does NOT include the insurance fee which is deduced off the report</span>
    <table class="adminlist">
      <tr>
        <th>Property / Owner</th>
		<?php if ($interval != 'byOccupancy') { ?>
			<th>Cleared <?php echo $VM_LANG->_('PHPSHOP_RB_DATE') ?></th>
	        <th><?php if($interval != 'byAll'){ ?><?php echo 'Payments' ?><?php } ?></th>
        <?php } else { ?>
			<th>Occupancy Rate</th>
		<?php } ?>
        <th><?php echo 'State Tax '.$VM_LANG->_('PHPSHOP_RB_REVENUE') ?></th>
        <th><?php echo 'Resort Tax '.$VM_LANG->_('PHPSHOP_RB_REVENUE') ?></th>
        <th><?php echo 'Total Tax '.$VM_LANG->_('PHPSHOP_RB_REVENUE') ?></th>
        <th><?php echo 'Net '.$VM_LANG->_('PHPSHOP_RB_REVENUE') ?></th>
        <th><?php echo 'Gross '.$VM_LANG->_('PHPSHOP_RB_REVENUE') ?></th>        
      </tr>
<?php
	$total_rows = 0;
  while ($db->next_record()) {
    
	$total_rows++;
	
    if ($i++ % 2) {
      $bgcolor='row0';
    }
    else {
      $bgcolor='row1';
    }
	$gross = $db->f("gross");
	$tax_rate = $db->f("tax");
	$tax_resort = $db->f("resort_tax");
	$tax_state = $db->f("state_tax");
	
	$net = $gross / (1+$tax_rate);
	$tax = $net * $tax_rate;
	$resort = $net * $tax_resort;
	$state = $net * $tax_state;
	
    $orders_total += $db->f("payments");
    $gross_total += $gross;
    $net_total += $net;
    $tax_total += $tax;
    $state_tax_total += $state;
    $resort_tax_total += $resort;
	
	$tax_rate *= 100;
	$tax_resort *= 100;
	$tax_state *= 100;
	
	if ($interval == 'byOccupancy') {
		$total_occupancy += $db->f("occ_days");
	}
	
     ?> 
	
	<?php if ( $db->f( "name" ) != $prev_name ) { ?>
	<tr class="row0"><td colspan="<?php echo ($interval == 'byOccupancy' ? '7' : '8'); ?>"><strong><?php echo $db->f( "name" ); ?></strong></td></tr>
	<?php } ?>
	
	<?php $prev_name = $db->f( "name" ); ?>
	
    <tr class="row1"> 
	  <td><?php $db->p("property_name"); ?></td>
      <?php if ($interval != 'byOccupancy') { ?>
	  	  <td><?php $db->p("payment_cleared"); ?></td>
		  <td style="text-align: right"><?php if($interval != 'byAll'){ ?><?php $db->p("payments"); ?><?php } ?></td>
	  <?php } else { ?>
	  	  <td style="text-align: right"><?php $db->p("occ_days"); ?>/<?php echo $total_days; ?> days (<?php echo round(($db->f("occ_days") / $total_days) * 100, 1); ?>%)</td>
	  <?php } ?>
	  <td style="text-align: right"><?php echo $CURRENCY_DISPLAY->getFullValue($state, 2)." ($tax_state%)"; ?></td>
      <td style="text-align: right"><?php echo $CURRENCY_DISPLAY->getFullValue($resort, 2)." ($tax_resort%)"; ?></td>
      <td style="text-align: right"><?php echo $CURRENCY_DISPLAY->getFullValue($tax, 2)." ($tax_rate%)"; ?></td>
      <td style="text-align: right"><?php echo $CURRENCY_DISPLAY->getFullValue($net, 2); ?></td>
      <td style="text-align: right"><?php echo $CURRENCY_DISPLAY->getFullValue($gross, 2); ?></td>
    </tr> 
<?php } ?>
  <tr><td colspan="<?php echo ($interval == 'byOccupancy' ? '7' : '8'); ?>"><hr /></td></tr>
   <tr style="font-weight: bold">
    	<?php if($interval != 'byOccupancy'){ ?><td>&nbsp;</td><?php } ?>
		<td><strong>Total:</strong></td>    	
    	<td style="text-align: right"><?php echo ($interval == 'byOccupancy' ? round((($total_occupancy / $total_rows) / $total_days) * 100, 1) . '%' : $orders_total); ?></td>    	    	
    	<td style="text-align: right"><?php echo $CURRENCY_DISPLAY->getFullValue($state_tax_total) ?></td>
    	<td style="text-align: right"><?php echo $CURRENCY_DISPLAY->getFullValue($resort_tax_total) ?></td>
    	<td style="text-align: right"><?php echo $CURRENCY_DISPLAY->getFullValue($tax_total) ?></td>
		<td style="text-align: right"><?php echo $CURRENCY_DISPLAY->getFullValue($net_total) ?></td>
    	<td style="text-align: right"><?php echo $CURRENCY_DISPLAY->getFullValue($gross_total) ?></td>
    </tr>
  </table>
        
<!-- end output of report -->
<!-- END body -->
