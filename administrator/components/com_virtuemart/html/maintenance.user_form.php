<?php
mm_showMyFileName( __FILE__ );

require_once( CLASSPATH . "ps_maintenance.php" );
require_once( CLASSPATH . "htmlTools.class.php" );

global $vmuser, $my, $perm;

//Get the maintenance item id
$user_id = vmGet($_REQUEST,'user_id',0);

if(!$perm->check( "admin,storeadmin") && !$maintenance_id){
	$GLOBALS['vmLogger']->err( 'Only administrators can add maintenance people' );
	return false;
}

$db = new ps_DB();
$db->query("SELECT u.user_id, CONCAT(u.first_name,' ',u.last_name) AS user, m.properties 
			FROM #__{vm}_user_info AS u 
			LEFT JOIN #__{vm}_maintenance_props AS m ON m.user_id = u.user_id 
			WHERE u.user_id = $user_id");

if(!$db->num_rows()){
	$GLOBALS['vmLogger']->err( 'This user is not a maintenance person, please set their permission level to "maintenance"' );
	return false;
}
$db->next_record();

/**
 * Get this users properties
 */
$dbp = new ps_DB();
$dbp->query('SELECT id,name FROM #__hp_properties ORDER BY name');
$user_props = array();

foreach(explode(',',$db->f('properties')) as $p){
	$tmp = new stdClass();
	$tmp->id = $p;
	$user_props[] = $tmp;
}

$props = $dbp->loadObjectList();
$properties = vmCommonHTML::checkboxListTable($props, 'properties[]', '', 'id','name', $user_props, 4);

$formFactory = new formFactory('Maintenance Person Properties');
$formFactory->startForm()
?>
<style type="text/css">

fieldset.vb-fieldset table td{
padding: 3px;
}

fieldset.vb-fieldset table label{
	clear:none !important;
	display:inline !important;
	float:none !important;
	font-weight:normal !important;
	width:auto !important;
	padding:0 10px 0 5px;
}
</style>
<fieldset class="vb-fieldset">
	<legend><?php $db->p('user') ?> Associated Properties</legend>
	<div class="row">
		Select the properties this user can manage<br /><br />	
		<?php
			echo $properties;
		?>
	</div>
</fieldset>
<?php
//End form
$formFactory->hiddenField('user_id' , $user_id);
$formFactory->finishForm('updateUser','maintenance.user_overview');
?>