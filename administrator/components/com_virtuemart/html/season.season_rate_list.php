<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
/**
*
* @version $Id: tax.tax_list.php 1099 2007-12-21 12:46:46Z soeren_nb $
* @package VirtueMart
* @subpackage html
* @copyright Copyright (C) 2004-2007 soeren - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
mm_showMyFileName( __FILE__ );

global $no_menu, $my, $limitstart, $limit, $vmuser, $mainframe;

require_once( CLASSPATH . "pageNavigation.class.php" );
require_once( CLASSPATH . "htmlTools.class.php" );
require_once( CLASSPATH . "ps_season.php" );
require_once( CLASSPATH . "ps_order_booking.php" );
require_once( CLASSPATH . "ps_property.php" );

$prop_id	= vmGet($_REQUEST,'property_id');
$mode 		= vmGet($_REQUEST,'mode','');
$hp_ext 	= $mode == 'hp_ext';

$filter_property_id = $mainframe->getUserStateFromRequest($option.'filter_property_id', 'filter_property_id','');

//Get the property id's
$prop_ids = ps_property::getAllowedProperties();

if(!$prop_ids){
	$GLOBALS['vmLogger']->err( 'You do not have permission to manage seasons.' );
	return;
}
//If user is an owner, only select their properties
else if($vmuser->gid == 31){
	if(in_array($prop_id,$prop_ids)){
		$prop_ids = array($prop_id);
	}else{
		$GLOBALS['vmLogger']->err( 'You can only edit season rates that you manage.' );
		return;
	}
}

//IF we're in a hp_extention make sure wefilter to only the selected property
if($hp_ext){
	$prop_ids = array(vmGet($_REQUEST,'property_id'));
}
//If we're filtering, limit to only the filtered property id
else if($filter_property_id && in_array($filter_property_id, $prop_ids)){
	$prop_ids = array($filter_property_id);
}

?>
<style type="text/css">
div.header{
padding: 0px;
line-height: auto;
}
</style>
<?php

//if($prop_id && empty($prop_ids)) $prop_ids[] = $prop_id;

$ps_season = new ps_season();

$seasons = $ps_season->getSeasons('', $prop_ids);

// Create the Page Navigation
$count = "SELECT count(*) as num_rows FROM #__{vm}_seasons_rates AS s ".(count($prop_ids) ? "WHERE property_id IN (".implode(',',$prop_ids).')' : '');

$db->query($count);
$db->next_record();
$num_rows = $db->f("num_rows");


$pageNav = new vmPageNav( $num_rows, $limitstart, $limit );

// Create the List Object with page navigation
$listObj = new listFactory( $pageNav );

// print out the search field and a list heading
$listObj->writeSearchHeader('Season Rates', '', $modulename, "season_rate_list");

?>
<div style="float: right">
	<?php	
		if(!$hp_ext) echo ps_order_booking::getPropertiesList($filter_property_id, 'filter_property_id', "onchange='document.adminForm.submit()'");
	?><br /><br />
</div>
<div style='clear: both'></div>
<?php

// start the list table
$listObj->startTable();

// these are the columns in the table
$columns = Array(  "#" => 'width="20" class="sectiontableheader" ', 
					"<input type=\"checkbox\" name=\"toggle\" value=\"\" onclick=\"checkAll(".count($seasons).")\" />" => 'class="sectiontableheader" width="20"',
					$VM_LANG->_('VM_BK_LIST_LBL') => 'class="sectiontableheader" width="30%"',
					$VM_LANG->_('VM_BK_LIST_RATE') => 'class="sectiontableheader" width="8%"',
					$VM_LANG->_('VM_BK_LIST_RATE_WEEKLY') => 'class="sectiontableheader" width="8%"',
					'Weekly Rate Total' => 'class="sectiontableheader" width="8%"',
					$VM_LANG->_('VM_BK_LIST_DISCOUNT') => 'class="sectiontableheader" width="8%"',
					$VM_LANG->_('VM_BK_LIST_STARTDATE') => 'class="sectiontableheader" width="8%"',
					$VM_LANG->_('VM_BK_LIST_ENDDATE') => 'class="sectiontableheader" width="8%"',
					$VM_LANG->_('VM_BK_LIST_PROPERTY') => 'class="sectiontableheader" width="8%"',
					$VM_LANG->_('E_REMOVE') => 'class="sectiontableheader" width="50px"'
				);

$listObj->writeTableHeader( $columns );

$i = 0;
$tmp_prop = '';

while($seasons->next_record()){
	
	if ( $tmp_prop !== $seasons->f('prop_id') ) {
		$listObj->newRow();
		$listObj->addCell( '<strong>'.($seasons->f('prop_name') =='' ? 'All Properties' : $seasons->f('prop_name')).'</strong>' ,'colspan="'.count($columns).'"' );
		$tmp_prop=$seasons->f('prop_id');
	}
	
	$listObj->newRow();
	
	// The row number
	$listObj->addCell( $pageNav->rowNumber( $i ) );
	
	// The Checkbox
	$listObj->addCell( vmCommonHTML::idBox( $i, $seasons->f('rate_id'), false, "rate_id" ) );
	
	$url = $_SERVER['PHP_SELF'] . "?page=$modulename.season_rate_form&no_menu=$no_menu&mode=$mode&rate_id=". $seasons->f('rate_id');
	
	$tmp_cell = "<a href=\"" . $sess->url($url,0,1,1) . "\">". $seasons->f('label'). "</a>";
	$listObj->addCell( $tmp_cell );
	
	$listObj->addCell( round($seasons->f('rate'), 2) );
	
	$listObj->addCell( round($seasons->f('rate') * 7, 2));
	
	$listObj->addCell( round($seasons->f('rate') * ($seasons->f('min_people') ? $seasons->f('min_people') : 1)  * 7, 2));
	
	$listObj->addCell( round($seasons->f('discount'), 2).' %' );
	
	$listObj->addCell( date('jS F Y', strtotime($seasons->f('dateFrom'))) );
	$listObj->addCell( date('jS F Y', strtotime($seasons->f('dateTo'))) );
	
	$listObj->addCell( $seasons->f('prop_name'));
	
	$listObj->addCell( $ps_html->deleteButton( "rate_id", $seasons->f('rate_id'), "deleteRate", $keyword, $limitstart, "&property_id=$prop_id" ) );
	
	$i++;
}

$listObj->writeTable();
$listObj->endTable();
$listObj->writeFooter( $keyword, $hp_ext ? "&property_id=$prop_id&mode=hp_ext" : '' );
?>