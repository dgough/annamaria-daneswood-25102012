<?php
// hp_vb_seasons.php
/**
* Hot Property Pricing Bridge
*
* @package Hot Property 0.9
* @copyright (C) 2008 Organic Development
* @url http://www.organic-development.com/
* @author Organic Development <grow@organic-development.com>
**/

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

global $option, $mosConfig_live_site, $mosConfig_absolute_path, $mainframe, $option;

# Backend classes

if (mosGetParam($_REQUEST,'property_id') != '') {
	
	$index = $mainframe->_isAdmin ? 'index3.php' : 'index2.php';
	
	mosRedirect($index.'?option=com_virtuemart&page=season.season_rate_list&pshop_mode=shopper&no_menu=1&mode=hp_ext&property_id='.mosGetParam($_REQUEST,'property_id'));
	
} else {
	echo '<center><br /><h2>Unavailable</h2>Please save the property first before setting up your property pricing option</center>';
}

?>