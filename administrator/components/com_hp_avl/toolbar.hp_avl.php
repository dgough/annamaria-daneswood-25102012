<?php
// $Id: toolbar.hp_avl.php
/**
* Hot Property Availability Addon - Toolbar
*
* @package Hot Property 0.9
* @copyright (C) 2004 Lee Cher Yeong
* @url http://www.Mosets.com/
* @author Lee Cher Yeong <cy@mosets.com>
**/

// ensure this file is being included by a parent file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

require_once( $mainframe->getPath( 'toolbar_html' ) );
require_once( $mainframe->getPath( 'toolbar_default' ) );

switch ($task) {

	# Availability Type Events
	case "newType":
	case "editType":
		menu_hp_avl::EDIT_TYPE();
		break;
	case "config":
		menu_hp_avl::CONFIG();
		break;
	case "about":
		break;
	case "viewTypes":
	default:
		menu_hp_avl::VIEW_TYPES();
		break;

}
?>