<?php
// $Id: toolbar.hp_avl.html.php
/**
* Hot Property Availability Addon - Toolbar HTML
*
* @package Hot Property 0.9
* @copyright (C) 2004 Lee Cher Yeong
* @url http://www.Mosets.com/
* @author Lee Cher Yeong <cy@mosets.com>
**/
// ensure this file is being included by a parent file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

class menu_hp_avl {

	# Availability Type menus
	
	function EDIT_TYPE() {
		mosMenuBar::startTable();
		mosMenuBar::save('saveType');
		mosMenuBar::cancel('cancelType');
		mosMenuBar::spacer();
		mosMenuBar::endTable();
	}
	
	function VIEW_TYPES() {
		mosMenuBar::startTable();
		mosMenuBar::addNew( 'newType' );
		mosMenuBar::editList( 'editType' );
		mosMenuBar::deleteList( '', 'removeType' );
		mosMenuBar::spacer();
		mosMenuBar::endTable();
	}

	# Availability CONFIG menu

	function CONFIG() {
		mosMenuBar::startTable();
		mosMenuBar::save('saveConfig');
		mosMenuBar::cancel('cancelConfig');
		mosMenuBar::spacer();
		mosMenuBar::endTable();
	}
}


?>