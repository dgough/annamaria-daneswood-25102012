<?php
// $Id: admin.hp_avl.class.php
/**
* Hot Property Availability Extension - Main Class
*
* @package Hot Property 0.9
* @copyright (C) 2004 Lee Cher Yeong
* @url http://www.Mosets.com/
* @author Lee Cher Yeong <cy@mosets.com>
**/

// ensure this file is being included by a parent file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

define ('hp_avl_version',"1.1");

/**
* Availability Types table class
*/
class hp_avl_types extends mosDBTable {

	var $avl_type_id=null;
	var $name=null;
	var $desc=null;
	var $font_color=null;
	var $font_background=null;
	var $ordering=null;

	function hp_avl_types( &$db ) {
		$this->mosDBTable( '#__hp_avl_types', 'avl_type_id', $db );
	}
}

/**
* Availability Main table class
*/
class hp_avl extends mosDBTable {

	var $avl_id=null;
	var $avl_type_id=null;
	var $property_id=null;
	var $date=null;

	function hp_avl( &$db ) {
		$this->mosDBTable( '#__hp_avl', 'avl_id', $db );
	}

	function clear_avl($property_id, $month, $year) {
		global $database;

		$database->setQuery("DELETE FROM #__hp_avl WHERE property_id = '".$property_id."' AND MONTH(`date`) = '".$month."' AND YEAR(`date`) = '".$year."'");
		if (!$database->query()) {
			echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
			exit();
		}
	}

	function add_avl($property_id, $date, $avl_type_id) {
		global $database;

		$database->setQuery("INSERT INTO #__hp_avl "
			. "\n(avl_type_id, property_id, date) "
			.	"\nVALUES ('".$avl_type_id."', '".$property_id."', '".$date."')");
		if (!$database->query()) {
			echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
			exit();
		}
	}

}

class backend_Calendar extends Calendar {

    function getCalendarLink($month, $year)
    {
        return "index3.php?option=com_hp_avl&task=ext_viewmonth&month=$month&year=$year&property_id=$this->property_id";
    }

    function getMonthLink($month, $year)
    {
        return "index3.php?option=com_hp_avl&task=ext_viewmonth&month=$month&year=$year&property_id=$this->property_id";
    }

	function getYearLink($month, $year)
	{
			return "index3.php?option=com_hp_avl&task=ext_viewyear&year=$year&returnmonth=$month&property_id=$this->property_id";
	}

}

// PHP Calendar Class Version 1.4 (5th March 2001)
//  
// Copyright David Wilkinson 2000 - 2001. All Rights reserved.
// 
// This software may be used, modified and distributed freely
// providing this copyright notice remains intact at the head 
// of the file.
//
// This software is freeware. The author accepts no liability for
// any loss or damages whatsoever incurred directly or indirectly 
// from the use of this script. The author of this software makes 
// no claims as to its fitness for any purpose whatsoever. If you 
// wish to use this software you should first satisfy yourself that 
// it meets your requirements.
//
// URL:   http://www.cascade.org.uk/software/php/calendar/
// Email: davidw@cascade.org.uk


class Calendar
{
    
		/*
				Availability Types
		*/
		var $avl_types;
		var $avl;
		var $property_id;
		var $edit; // If 1, getYearHTML will output select box
    /*
        Constructor for the Calendar class
    */
    function Calendar()
    {
			$this->edit = 1;
    }

    /*
        Get the array of strings used to label the days of the week. This array contains seven 
        elements, one for each day of the week. The first entry in this array represents Sunday. 
    */
    function getDayNames()
    {
        return $this->dayNames;
    }
    

    /*
        Set the array of strings used to label the days of the week. This array must contain seven 
        elements, one for each day of the week. The first entry in this array represents Sunday. 
    */
    function setDayNames($names)
    {
        $this->dayNames = $names;
    }
    
    /*
        Get the array of strings used to label the months of the year. This array contains twelve 
        elements, one for each month of the year. The first entry in this array represents January. 
    */
    function getMonthNames()
    {
        return $this->monthNames;
    }
    
    /*
        Set the array of strings used to label the months of the year. This array must contain twelve 
        elements, one for each month of the year. The first entry in this array represents January. 
    */
    function setMonthNames($names)
    {
        $this->monthNames = $names;
    }
    
    
    
    /* 
        Gets the start day of the week. This is the day that appears in the first column
        of the calendar. Sunday = 0.
    */
      function getStartDay()
    {
        return $this->startDay;
    }
    
    /* 
        Sets the start day of the week. This is the day that appears in the first column
        of the calendar. Sunday = 0.
    */
    function setStartDay($day)
    {
        $this->startDay = $day;
    }
    
    
    /* 
        Gets the start month of the year. This is the month that appears first in the year
        view. January = 1.
    */
    function getStartMonth()
    {
        return $this->startMonth;
    }
    
    /* 
        Sets the start month of the year. This is the month that appears first in the year
        view. January = 1.
    */
    function setStartMonth($month)
    {
        $this->startMonth = $month;
    }
    
    
    /*
        Return the URL to link to in order to display a calendar for a given month/year.
        You must override this method if you want to activate the "forward" and "back" 
        feature of the calendar.
        
        Note: If you return an empty string from this function, no navigation link will
        be displayed. This is the default behaviour.
        
        If the calendar is being displayed in "year" view, $month will be set to zero.
    */
    function getCalendarLink($month, $year)
    {
        return "";
    }
    
    /*
        Return the URL to link to  for a given date.
        You must override this method if you want to activate the date linking
        feature of the calendar.
        
        Note: If you return an empty string from this function, no navigation link will
        be displayed. This is the default behaviour.
    */
    function getDateLink($month, $year)
    {
        return "";
    }

		function getMonthLink($month, $year)
    {
        return "";
    }

		function getYearLink($month, $year)
		{
				return "";
		}
    /*
        Return the HTML for the current month
    */
    function getCurrentMonthView()
    {
        $d = getdate(time());
        return $this->getMonthView($d["mon"], $d["year"]);
    }
    

    /*
        Return the HTML for the current year
    */
    function getCurrentYearView()
    {
        $d = getdate(time());
        return $this->getYearView($d["year"]);
    }
    
    
    /*
        Return the HTML for a specified month
    */
    function getMonthView($month, $year)
    {
        return $this->getMonthHTML($month, $year);
    }
    

    /*
        Return the HTML for a specified year
    */
    function getYearView($year)
    {
        return $this->getYearHTML($year);
    }
    
    
    
    /********************************************************************************
    
        The rest are private methods. No user-servicable parts inside.
        
        You shouldn't need to call any of these functions directly.
        
    *********************************************************************************/


    /*
        Calculate the number of days in a month, taking into account leap years.
    */
    function getDaysInMonth($month, $year)
    {
        if ($month < 1 || $month > 12)
        {
            return 0;
        }
   
        $d = $this->daysInMonth[$month - 1];
   
        if ($month == 2)
        {
            // Check for leap year
            // Forget the 4000 rule, I doubt I'll be around then...
        
            if ($year%4 == 0)
            {
                if ($year%100 == 0)
                {
                    if ($year%400 == 0)
                    {
                        $d = 29;
                    }
                }
                else
                {
                    $d = 29;
                }
            }
        }
    
        return $d;
    }


    /*
        Generate the HTML for a given month
    */
    function getMonthHTML($m, $y, $showYear = 1)
    {
      $s = "";

      $a = $this->adjustDate($m, $y);
      $month = $a[0];
      $year = $a[1];
        
    	$daysInMonth = $this->getDaysInMonth($month, $year);
    	$date = getdate(mktime(12, 0, 0, $month, 1, $year));
    	
    	$first = $date["wday"];
    	$monthName = $this->monthNames[$month - 1];
    	
    	$prev = $this->adjustDate($month - 1, $year);
    	$next = $this->adjustDate($month + 1, $year);
    	
    	if ($showYear == 1)
    	{
    	    $prevMonth = $this->getCalendarLink($prev[0], $prev[1]);
    	    $nextMonth = $this->getCalendarLink($next[0], $next[1]);
    	}
    	else
    	{
    	    $prevMonth = "";
    	    $nextMonth = "";
    	}
    	
    	$header = $monthName . (($showYear > 0) ? " " . $year : "");

    	$s .= "<table class=\"month calendar\" style='margin: 0px auto'>\n";
    	$s .= "<tr>\n";
    	$s .= "<td align=\"center\" valign=\"top\">" . (($prevMonth == "") ? "&nbsp;" : "<a href=\"$prevMonth\">&lt;&lt;</a>")  . "</td>\n";
    	$s .= "<td align=\"center\" valign=\"top\" class=\"calendarHeader\" colspan=\"5\">";
			$link = $this->getMonthLink($month, $year);
			if ($link <> '') {
				$s .= "<a href=\"".$link."\">".$header."</a>";	
			} else {
				$s .= $header;
			}
			$s .= "</td>\n"; 
    	$s .= "<td align=\"center\" valign=\"top\">" . (($nextMonth == "") ? "&nbsp;" : "<a href=\"$nextMonth\">&gt;&gt;</a>")  . "</td>\n";
    	$s .= "</tr>\n";
    	
    	$s .= "<tr>\n";
    	$s .= "<td align=\"center\" valign=\"top\" class=\"calendarHeader\">" . $this->dayNames[($this->startDay)%7] . "</td>\n";
    	$s .= "<td align=\"center\" valign=\"top\" class=\"calendarHeader\">" . $this->dayNames[($this->startDay+1)%7] . "</td>\n";
    	$s .= "<td align=\"center\" valign=\"top\" class=\"calendarHeader\">" . $this->dayNames[($this->startDay+2)%7] . "</td>\n";
    	$s .= "<td align=\"center\" valign=\"top\" class=\"calendarHeader\">" . $this->dayNames[($this->startDay+3)%7] . "</td>\n";
    	$s .= "<td align=\"center\" valign=\"top\" class=\"calendarHeader\">" . $this->dayNames[($this->startDay+4)%7] . "</td>\n";
    	$s .= "<td align=\"center\" valign=\"top\" class=\"calendarHeader\">" . $this->dayNames[($this->startDay+5)%7] . "</td>\n";
    	$s .= "<td align=\"center\" valign=\"top\" class=\"calendarHeader\">" . $this->dayNames[($this->startDay+6)%7] . "</td>\n";
    	$s .= "</tr>\n";
    	
    	// We need to work out what date to start at so that the first appears in the correct column
    	$d = $this->startDay + 1 - $first;
    	while ($d > 1)
    	{
    	    $d -= 7;
    	}

        // Make sure we know when today is, so that we can use a different CSS style
        $today = getdate(time());
    	
    	while ($d <= $daysInMonth)
    	{
    	    $s .= "<tr>\n";       
    	    
    	    for ($i = 0; $i < 7; $i++)
    	    {
        	    $class = ($year == $today["year"] && $month == $today["mon"] && $d == $today["mday"]) ? "calendarToday" : "calendarDate";
        	    $ddate = date('Y-m-d', strtotime($d.' '.date('F',mktime(0, 0, 0, $m, 1, 2004)).' '.$y) );
        	    
        	    if(isset($this->avl[$ddate])){
					$day = $this->avl[$ddate];
					
					$class .= " $day->class";
				}
					
				$s .= "<td ";

				/*
				// Display different colors for Availability types in ViewYear -> edit = 0
				if ($this->edit == 0 && $d > 0 && $d <= $daysInMonth) {
															
					//foreach($this->avl_types AS $avl_type) {
					//	$ddate = date('Y-m-d', strtotime($d.' '.date('F',mktime(0, 0, 0, $m, 1, 2004)).' '.$y) );
						// Assign class
					//	if (isset($this->avl[$ddate]) && $this->avl[$ddate]->avl_type_id == $avl_type->avl_type_id) {
					//		$s .= "style=\"background-color:".$avl_type->font_background."; color:".$avl_type->font_color."\" ";
					//	} else {
							
					//	}
					//}					
					
				} else {
        			//$s .= "<td ";
					if ($d > 0 && $d <= $daysInMonth) {
						//TODO: Assign color codes. Use javascript to change value in real time
					} else {

					}
				}
				*/
				
				$s .= "class=\"$class\" align=\"right\" valign=\"top\">";

    	        if ($d > 0 && $d <= $daysInMonth)
    	        {
    	            $link = $this->getDateLink($d, $month, $year);
    	            $s .= (($link == "") ? $d : "<a href=\"$link\">$d</a>");
									/*if ($this->edit <> 0) {
										$s .= '<br />';
										$s .= '<select name="day'.$d.'" class="inputbox2" size="1">';
										$s .= '<option value="0"> </option>';
										foreach($this->avl_types AS $avl_type) {
											$s .= '<option value="'.$avl_type->avl_type_id.'" ';
											$ddate = date('Y-m-d', strtotime($d.' '.date('F',mktime(0, 0, 0, $m, 1, 2004)).' '.$y) );
											if (isset($this->avl[$ddate]) && $this->avl[$ddate]->avl_type_id == $avl_type->avl_type_id) {
												$s .= 'selected ';
											}
											$s .= 'style="background-color:'. $avl_type->font_background.';">';
											$s .= $avl_type->name.'</option>';
										}
										$s .= '</select>';
									}*/
    	        }
    	        else
    	        {
    	            $s .= "&nbsp;";
    	        }
      	        $s .= "</td>\n";       
        	    $d++;
    	    }
    	    $s .= "</tr>\n";    
    	}
    	
    	$s .= "</table>\n";
    	
    	return $s;  	
    }
    
    
    /*
        Generate the HTML for a given year
    */
    function getYearHTML($year)
    {
      $s = "";
//    	$prev = $this->getCalendarLink(1, $year - 1);
//    	$next = $this->getCalendarLink(1, $year + 1);
    	$prev = $this->getYearLink(1, $year - 1);
    	$next = $this->getYearLink(1, $year + 1);
    	
        $s .= "<table class=\"calendar\" border=\"0\" style='margin: 0px auto'>\n";
        $s .= "<tr>";
    	$s .= "<td align=\"center\" valign=\"top\" align=\"left\">" . (($prev == "") ? "&nbsp;" : "<a href=\"$prev\">&lt;&lt;</a>")  . "</td>\n";
        $s .= "<td class=\"calendarHeader\" valign=\"top\" align=\"center\"><h2>" . (($this->startMonth > 1) ? $year . " - " . ($year + 1) : $year) ."</h2></td>\n";
    	$s .= "<td align=\"center\" valign=\"top\" align=\"right\">" . (($next == "") ? "&nbsp;" : "<a href=\"$next\">&gt;&gt;</a>")  . "</td>\n";
        $s .= "</tr>\n";
        $s .= "<tr>";
        $s .= "<td class=\"month_cell\" valign=\"top\">" . $this->getMonthHTML(0 + $this->startMonth, $year, 0) ."</td>\n";
        $s .= "<td class=\"month_cell\" valign=\"top\">" . $this->getMonthHTML(1 + $this->startMonth, $year, 0) ."</td>\n";
        $s .= "<td class=\"month_cell\" valign=\"top\">" . $this->getMonthHTML(2 + $this->startMonth, $year, 0) ."</td>\n";
        $s .= "</tr>\n";
        $s .= "<tr>\n";
        $s .= "<td class=\"month_cell\" valign=\"top\">" . $this->getMonthHTML(3 + $this->startMonth, $year, 0) ."</td>\n";
        $s .= "<td class=\"month_cell\" valign=\"top\">" . $this->getMonthHTML(4 + $this->startMonth, $year, 0) ."</td>\n";
        $s .= "<td class=\"month_cell\" valign=\"top\">" . $this->getMonthHTML(5 + $this->startMonth, $year, 0) ."</td>\n";
        $s .= "</tr>\n";
        $s .= "<tr>\n";
        $s .= "<td class=\"month_cell\" valign=\"top\">" . $this->getMonthHTML(6 + $this->startMonth, $year, 0) ."</td>\n";
        $s .= "<td class=\"month_cell\" valign=\"top\">" . $this->getMonthHTML(7 + $this->startMonth, $year, 0) ."</td>\n";
        $s .= "<td class=\"month_cell\" valign=\"top\">" . $this->getMonthHTML(8 + $this->startMonth, $year, 0) ."</td>\n";
        $s .= "</tr>\n";
        $s .= "<tr>\n";
        $s .= "<td class=\"month_cell\" valign=\"top\">" . $this->getMonthHTML(9 + $this->startMonth, $year, 0) ."</td>\n";
        $s .= "<td class=\"month_cell\" valign=\"top\">" . $this->getMonthHTML(10 + $this->startMonth, $year, 0) ."</td>\n";
        $s .= "<td class=\"month_cell\" valign=\"top\">" . $this->getMonthHTML(11 + $this->startMonth, $year, 0) ."</td>\n";
        $s .= "</tr>\n";
        $s .= "</table>\n";
        
        return $s;
    }
    
	    
	    
	function setUnavailable($property_id, $year, $month = ''){
		global $database;
		
		$database->setQuery("SELECT booking.arrival, booking.departure, booking.property_id, order.order_status, status.order_status_name
							FROM #__vm_order_booking as `booking`, #__vm_orders as `order`, #__vm_order_status as status
							WHERE booking.property_id = '$property_id' 
							AND order.order_id = booking.order_id
							AND status.order_status_code = order.order_status
							AND (YEAR(booking.arrival) = '$year' OR YEAR(booking.departure) = '$year')".
							($month != '' ? "AND (MONTH(booking.arrival) = '$month' OR MONTH(booking.departure) = '$month')" : '')
							);	
		$bookings = $database->loadObjectList('arrival');
	
		$status = array();
		foreach($bookings as $booking){
			$dateFrom = strtotime($booking->arrival);
			$dateTo = strtotime($booking->departure);
			if(!in_array($booking->order_status_name,$status)) $status[] = $booking->order_status_name;
			
			for($i = $dateFrom; $i <= $dateTo; $i+=86400){
				
				//Set "today"
				$day = date('Y-m-d',$i);
	
				//Check if we already have an entry for that day
				if(!array_key_exists($day,$this->avl)){
				
					$this->avl[$day] = new stdClass();
					$this->avl[$day]->avl_type_id = $booking->order_status == 'P' ? 1 : 2;
					$this->avl[$day]->property_id = $booking->property_id;
					$this->avl[$day]->status = $booking->order_status;
					$this->avl[$day]->status_name = $booking->order_status_name;
					$this->avl[$day]->class = strtolower($booking->order_status_name.($i == $dateFrom ? '-start changeover' : '').($i == $dateTo ? '-end changeover' : ''));
					$this->avl[$day]->crossover = 0;				
					
				}else{

					$old_status = $this->avl[$day]->status_name;
					
					$this->avl[$day] = new stdClass();
					$this->avl[$day]->avl_type_id = $booking->order_status == 'P' ? 1 : 2;
					$this->avl[$day]->property_id = $booking->property_id;
					$this->avl[$day]->status = $booking->order_status;
					$this->avl[$day]->status_name = $booking->order_status_name;
					$this->avl[$day]->class = strtolower($old_status.' '.$booking->order_status_name.'-start changeover'.($old_status == $booking->order_status_name ? '-stripe' : ''));
					$this->avl[$day]->crossover = 1;			
					
				}
			}	
		}
		return $status;
	}


    /*
        Adjust dates to allow months > 12 and < 0. Just adjust the years appropriately.
        e.g. Month 14 of the year 2001 is actually month 2 of year 2002.
    */
    function adjustDate($month, $year)
    {
        $a = array();  
        $a[0] = $month;
        $a[1] = $year;
        
        while ($a[0] > 12)
        {
            $a[0] -= 12;
            $a[1]++;
        }
        
        while ($a[0] <= 0)
        {
            $a[0] += 12;
            $a[1]--;
        }
        
        return $a;
    }

    /* 
        The start day of the week. This is the day that appears in the first column
        of the calendar. Sunday = 0.
    */
    var $startDay = 0;

    /* 
        The start month of the year. This is the month that appears in the first slot
        of the calendar in the year view. January = 1.
    */
    var $startMonth = 1;

    /*
        The labels to display for the days of the week. The first entry in this array
        represents Sunday.
    */
    var $dayNames = array("S", "M", "T", "W", "T", "F", "S");
    
    /*
        The labels to display for the months of the year. The first entry in this array
        represents January.
    */
    var $monthNames = array("January", "February", "March", "April", "May", "June",
                            "July", "August", "September", "October", "November", "December");
                            
                            
    /*
        The number of days in each month. You're unlikely to want to change this...
        The first entry in this array represents January.
    */
    var $daysInMonth = array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

}

?>