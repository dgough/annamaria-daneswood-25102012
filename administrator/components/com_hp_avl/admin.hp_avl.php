<?php
// $Id: admin.hp_avl.php
/**
* Hot Property Availability Extension - Admin 
*
* @package Hot Property 0.9
* @copyright (C) 2004 Lee Cher Yeong
* @url http://www.Mosets.com/
* @author Lee Cher Yeong <cy@mosets.com>
**/

# ensure this file is being included by a parent file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

require_once( 'components/com_hp_avl/admin.hp_avl.class.php' );
require_once( $mainframe->getPath( 'admin_html' ) );

# Check if Hot Property is installed
// TO DO
//require_once( 'components/com_hotproperty/config.hotproperty.php' );

$id = mosGetParam( $_REQUEST, 'id', array(0) );
if (!is_array( $id )) {
	$id = array(0);
}

switch ($task) {
	# Extension Event
	case "ext_main":
		ext_viewyear( $option );
		break;
	case "ext_viewmonth":
		ext_main( $option );
		break;
	case "ext_viewyear":
		ext_viewyear( $option );
		break;
	case "ext_save":
		ext_save( $option );
		break;

	# Type Event
	case "newType":
		editType( 0, $option );
		break;
	case "editType":
		editType( $id[0], $option );
		break;
	case "saveType":
		saveType( $option );
		break;
	case "orderup_type":
		orderType( $id[0], -1, $option );
		break;
	case "orderdown_type":
		orderType( $id[0], 1, $option );
		break;
	case "removeType":
		removeType( $id, $option );
		break;
	case "cancelType":
		cancelType( $option );
		break;

	# Config
	case "config":
		config( $option );
		break;
	case "saveConfig":
		saveConfig( $option );
		break;

	# About
	case "about":
		HTML_hp_avl::showAbout();
		break;

	case "viewTypes":
	default:
		viewTypes( $option );
}

function ext_main( $option ) {
	global $database;

	$today = getdate();
	$year = intval( mosGetParam( $_REQUEST, 'year', 0 ) );
	$month = intval( mosGetParam( $_REQUEST, 'month', 0 ) );
	$property_id = intval( mosGetParam( $_REQUEST, 'property_id', 0 ) );
	
	if ($property_id == 0) {
	
		HTML_hp_avl::ext_error( "<center><br /><h2>Unavailable</h2>Please save the property first before setting up availability's option</center>" );		
		//exit();

	} else {

		if ($year == 0 && $month == 0) {
			$year = intval( mosGetParam( $_POST, 'year', $today['year'] ) );
			$month = intval( mosGetParam( $_POST, 'month', $today['mon'] ) );
		}

		if ($property_id == 0)	 {
			$property_id = intval( mosGetParam( $_POST, 'property_id', 0 ) );
		}

		$cal = new backend_Calendar();
		$cal->property_id = $property_id;

		/*
		# Get Availability Type
		$database->setQuery('SELECT * FROM #__hp_avl_types');
		$avl_types = $database->loadObjectList('avl_type_id');
		$cal->avl_types = $avl_types;

		# Retrieve assigned Availability
		$database->setQuery("SELECT avl_type_id, date FROM #__hp_avl WHERE property_id = '".$property_id."' AND MONTH(`date`) = '".$month."' AND YEAR(`date`) = '".$year."'");
		$cal->avl = $database->loadObjectList('date');
		*/
		
		# Retrieve assigned Availability
		$status = $cal->setUnavailable($property_id, $year, $month);
		
		$html['calendar'] = $cal->getMonthHTML( $month, $year );

		HTML_hp_avl::ext_main( $html, $avl_types, $month, $year, $property_id, $option );

	}
}

function ext_save( $option ) {
	
	$year = intval( mosGetParam( $_REQUEST, 'year', $today['year'] ) );
	$month = intval( mosGetParam( $_REQUEST, 'month', $today['mon'] ) );
	$property_id = intval( mosGetParam( $_POST, 'property_id', 0 ) );
	
	$row = new hp_avl($database);

	$row->clear_avl($property_id, $month, $year);

	foreach($_POST AS $k => $v) {
		if (substr($k,0,3) == "day" && $v > 0) {
			$date = date('Y-m-d', strtotime(substr($k,3,2).' '.date('F',mktime(0, 0, 0, $month, 1, 2004)).' '.$year));
			$row->add_avl($property_id, $date, $v);
		}
	}
	mosRedirect( "index3.php?option=$option&task=ext_viewmonth&month=$month&year=$year&property_id=$property_id", "Calendar has been saved"  );
  
}

function ext_viewyear( $option ) {
	global $database;

	$today = getdate();

	$year = intval( mosGetParam( $_REQUEST, 'year', $today['year'] ) );
	$property_id = intval( mosGetParam( $_REQUEST, 'property_id', 0 ) );

	if ($property_id == 0) {
		HTML_hp_avl::ext_error( "<center><br /><h2>Unavailable</h2>Please save the property first before setting up availability's option</center>" );		
		exit();
	}

	$cal = new backend_Calendar();
	$cal->edit = 0;
	$cal->property_id = $property_id;

	# Get Availability Type
	/*
	$database->setQuery('SELECT * FROM #__hp_avl_types');
	$avl_types = $database->loadObjectList('avl_type_id');
	$cal->avl_types = $avl_types;
	

	# Retrieve assigned Availability
	$database->setQuery("SELECT avl_type_id, date FROM #__hp_avl WHERE property_id = '".$property_id."' AND MONTH(`date`) = '".$month."' AND YEAR(`date`) = '".$year."'");
	$cal->avl = $database->loadObjectList('date');
	*/	
	
	# Retrieve assigned Availability
	$status = $cal->setUnavailable($property_id, $year);

	$html = $cal->getYearHTML( $year );

	HTML_hp_avl::ext_viewyear( $html, $option );
}

function viewTypes($option) {
	global $database, $mainframe;

	$limit = $mainframe->getUserStateFromRequest( "viewlistlimit", 'limit', 10 );
	$limitstart = $mainframe->getUserStateFromRequest( "viewcli{$option}limitstart", 'limitstart', 0 );

	# get the total number of records
	$database->setQuery( "SELECT count(*) FROM #__hp_avl_types" );
	$total = $database->loadResult();
	echo $database->getErrorMsg();

	require_once("includes/pageNavigation.php");
	$pageNav = new mosPageNav( $total, $limitstart, $limit );
	
	$sql = "SELECT *"
		. "\nFROM #__hp_avl_types"
		. "\nORDER BY ordering ASC"
		. "\nLIMIT $pageNav->limitstart,$pageNav->limit";

	$database->setQuery($sql);

	if(!$result = $database->query()) {
		echo $database->stderr();
		return false;
	}
	$rows = $database->loadObjectList();

	HTML_hp_avl::viewTypes( $rows, $pageNav, $option );
}

function editType($id, $option) {
	global $database;

	$row = new hp_avl_types($database);
	$row->load($id);

	HTML_hp_avl::editType( $row, $option );
}

function saveType( $option ) {
	global $database;

	$row = new hp_avl_types( $database );

	if (!$row->bind( $_POST )) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

	# Put new item to last
	if($row->avl_type_id < 1) $row->ordering = 9999;

	if (!$row->store()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

	# Update order
	$row->updateOrder();
	mosRedirect( "index2.php?option=$option&task=viewTypes");
}

function orderType( $id, $inc, $option ) {
  global $database;

  # oop database connector
  $row = new hp_avl_types( $database );
	$row->load( $id );
	$row->move( $inc);
	
	mosRedirect( "index2.php?option=$option&task=viewTypes" );
}

function removeType( $id, $option ) {
	global $database;

	for ($i = 0; $i < count($id); $i++) {
			# Clear all Availability of this type
			$query="DELETE FROM #__hp_avl WHERE `avl_type_id`='".$id[$i]."'";
			$database->setQuery($query);
			$database->query();
			
			# Delete the Availability Type
			$query="DELETE FROM #__hp_avl_types WHERE `avl_type_id`='".$id[$i]."'";
			$database->setQuery($query);
			$database->query();
	}
	mosRedirect("index2.php?option=$option&task=viewTypes");
}

function cancelType( $option ) {
	mosRedirect( "index2.php?option=$option&task=viewTypes" );
}

function config( $option ) {
	global $database;

	$database->setQuery( "SELECT * FROM #__hp_avl_cfg" );
	$data = $database->loadObjectList( 'name' );
	
	$lists = array();

	$lists['delete_prev_month']  = mosHTML::yesnoSelectList( "vars[delete_prev_month]", 'class="inputbox" size="1"', @$data['delete_prev_month']->value );

	HTML_hp_avl::editConfig( $lists, $data, $option );

}

function saveConfig( $option ) {
	global $database;

	$vars = mosGetParam( $_POST, 'vars', array() );
	foreach ($vars as $k=>$v) {
		$v = $database->getEscaped( $v );
		$database->setQuery( "UPDATE #__hp_avl_cfg SET value = '".$v."' WHERE name = '".$k."'");
		$database->query();
	}
	mosRedirect("index2.php?option=$option&task=config", "Configurations have been saved.");
}

?>