<?php
// $Id: admin.hp_avl.html.php
/**
* Hot Property Availability Extension - Admin HTML
*
* @package Hot Property 0.9
* @copyright (C) 2004 Lee Cher Yeong
* @url http://www.Mosets.com/
* @author Lee Cher Yeong <cy@mosets.com>
**/

// ensure this file is being included by a parent file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

require("components/com_hotproperty/classes/oohforms.inc");

class HTML_hp_avl {

	/***
	* Availability
	*/
	function ext_printcss() {
		global $mosConfig_live_site;
	
		?>
		<link href="<?php echo $mosConfig_live_site ?>/components/com_hp_avl/css/hp_avl.css" rel="stylesheet" type="text/css">
		<?php			
	}
	
	function ext_main( &$html, &$avl_types, $month, $year, $property_id, $option ) {
		HTML_hp_avl::ext_printcss();
	?>
	<body style="background-color:#f5f5f5;">
	<form action="index3.php" method="POST" name="adminForm">
	<div style='text-align: center'>
	<a href="index3.php?option=<?php echo $option; ?>&task=ext_viewyear&year=<?php echo $year; ?>&returnmonth=<?php echo $month; ?>&property_id=<?php echo $property_id; ?>"><?php echo $year; ?></a>
	<p />
	<?php
		echo $html['calendar'];
	?>
	<p />
	<input type="hidden" name="option" value="<?php echo $option; ?>" />
	<input type="hidden" name="task" value="ext_save" />
	<input type="hidden" name="property_id" value="<?php echo $property_id; ?>" />
	<input type="hidden" name="month" value="<?php echo $month; ?>" />
	<input type="hidden" name="year" value="<?php echo $year; ?>" />
	<input type="submit" value="Apply Changes" class="button" />
	</form>
	</div>
	<p />
	<table cellpadding="5" cellspacing="0" border="0" style="border: 1px solid #C0C0C0">
	<?php	foreach($avl_types AS $avl_type) {	?>
	<tr>
		<td style="background-color:<?php echo $avl_type->font_background; ?>; color:<?php echo $avl_type->font_color; ?>"><?php echo $avl_type->name; ?></td>
		<td><?php echo $avl_type->desc; ?></td>
	</tr>
	<?php	}	?>
	</table>
	<?php
	}

	function ext_viewyear( $html, $option ) {
		HTML_hp_avl::ext_printcss();

	?>
	<div style='text-align: center'>
	<?php
		echo $html;
	?>
	</div>
	<?php
	}

	function ext_error( $html ) {
	?>
	<body style="background-color:#f5f5f5; 	color : #333333;	font-family : Arial, Helvetica, sans-serif;	font-size : 11px;">
	<?php
	echo $html;
	}

	/***
	* Managing Availability Types
	*/
	function viewTypes( $rows, $pageNav, $option ) {
	?>
	<form action="index2.php" method="POST" name="adminForm">
		<table cellpadding="4" cellspacing="0" border="0" width="100%">
			<tr>
				
	      <td width="100%" align="left"><span class="sectionname">Availability Type</span></td>
				<td nowrap>Display #</td>
				<td> <?php echo $pageNav->writeLimitBox(); ?> </td>
			</tr>
		</table>
		<table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
			<tr>
				<th width="20">#</th>
				<th width="20"><input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $rows ); ?>);" /></th>
				<th align="left" nowrap>Type Name</th>
				<th align="left" nowrap>Description</th>
				<th nowrap colspan="2">Reorder</th>
			</tr>
			<?php
					$k = 0;
					for ($i=0, $n=count( $rows ); $i < $n; $i++) {
						$row = &$rows[$i];
			?>
				<tr class="<?php echo "row$k"; ?>">
					<td width="20" align="center"><?php echo $i+$pageNav->limitstart+1;?></td>
					<td width="20">
						<input type="checkbox" id="cb<?php echo $i;?>" name="id[]" value="<?php echo $row->avl_type_id; ?>" onClick="isChecked(this.checked);" />
					</td>
					<td width="15%" align="left"><a href="#edit" onclick="return listItemTask('cb<?php echo $i; ?>','editType')"><?php echo $row->name; ?></a></td>
					<td align="left" width="80%"><?php echo $row->desc; ?></td>
		      <td>
					<?php		if ($i > 0 || ($i+$pageNav->limitstart > 0)) { ?>
		        <a href="#reorder" onClick="return listItemTask('cb<?php echo $i;?>','orderup_type')">
		        <img src="images/uparrow.png" width="12" height="12" border="0" alt="Move Up">
		        </a>
		        <?php		} else { echo "&nbsp;"; } ?>
		      </td>
		      <td>
					<?php		if ($i < $n-1 || $i+$pageNav->limitstart < $pageNav->total-1) { ?>
		        <a href="#reorder" onClick="return listItemTask('cb<?php echo $i;?>','orderdown_type')">
		        <img src="images/downarrow.png" width="12" height="12" border="0" alt="Move Down">
		        </a>
		        <?php		} else { echo "&nbsp;"; } ?>
		      </td>
		
					<?php		$k = 1 - $k; ?>
				</tr>
		<?php	}
		?>
		<tr>
			<th align="center" colspan="7"> <?php echo $pageNav->writePagesLinks(); ?></th>
		</tr>
		<tr>
			<td align="center" colspan="7"> <?php echo $pageNav->writePagesCounter(); ?></td>
		</tr>
	</table>

	<input type="hidden" name="option" value="<?php echo $option; ?>">
	<input type="hidden" name="task" value="viewTypes">
	<input type="hidden" name="boxchecked" value="0">
	</form>
	<?php
	}

	function editType( &$row, $option ) {
		mosMakeHtmlSafe( $row, ENT_QUOTES, 'desc' );
		?>
		<script language="javascript">
		<!--
			function submitbutton(pressbutton) {
				var form = document.adminForm;
				if (pressbutton == 'cancelType') {
					submitform( pressbutton );
					return;
				}
				// do field validation
				if (form.name.value == "") {
					alert( "Please fill in the Availability Type Name." );
				} else {
					<?php getEditorContents( 'editor1', 'desc' ) ; ?>
					submitform( pressbutton );
				}
			}
		//-->
		</script>
		<table cellpadding="4" cellspacing="0" border="0" width="100%">
			<tr>
				<td width="100%"><span class="sectionname"><?php echo $row->avl_type_id ? 'Edit' : 'Add';?> Availability Type</span></td>
			</tr>
		</table>
		<form action="index2.php" method="POST" name="adminForm">
		<table cellpadding="4" cellspacing="1" border="0" width="100%" class="adminform">
			<tr>
				<td align="left" width="10%">Name:</td>
				<td align="left"><input class="inputbox" type="text" name="name" size="30" valign="top" value="<?php echo $row->name; ?>"></td>
			</tr>
			<tr>
				<td align="left" valign="top">Description:</td>
				<td align="left"><?php editorArea( 'editor1',  $row->desc ? $row->desc : "", 'desc', 500, 200, '70', '15' ) ; ?></td>
			</tr>
			<tr>
				<td align="left" width="10%">Font Color:</td>
				<td align="left"><input class="inputbox" type="text" name="font_color" size="10" valign="top" value="<?php echo $row->font_color; ?>"></td>
			</tr>
			<tr>
				<td align="left" width="10%">Font Background:</td>
				<td align="left"><input class="inputbox" type="text" name="font_background" size="10" valign="top" value="<?php echo $row->font_background; ?>"></td>
			</tr>
		</table>
		<input type="hidden" name="option" value="<?php echo $option; ?>">
		<input type="hidden" name="avl_type_id" value="<?php echo $row->avl_type_id; ?>">
		<input type="hidden" name="task" value="saveType">
		</form>
		<?php 
	}

	function editConfig( &$lists, &$data, $option) {

		// Initialize Tabs
		$tabs = new mosTabs(0);
		?>
		<table cellpadding="4" cellspacing="0" border="0" width="100%">
		  <tr>
		    <td width="100%" class="sectionname">Availability Configuration</span></td>
		  </tr>
		</table>
		<script language="javascript" type="text/javascript">
			function submitbutton(pressbutton) {
				var form = document.adminForm;
					if (pressbutton == 'saveConfig') {
						if (confirm ("Are you sure?")) {
							submitform( pressbutton );
						}
					} else {
						document.location.href = 'index2.php?option=<?php echo $option;?>';
					}
			}
		</script>
		<form action="index2.php" method="POST" name="adminForm">
		
		<?php
			$tabs->startPane("content-pane");
			$tabs->startTab("General","general-page");
			?>
		
			<table cellpadding="2" cellspacing="4" border="0" width="100%" class="adminform">
				<?php foreach ($data AS $name => $value) { ?>
					<tr height="40">
						<td width="25%"><?php echo $data[$name]->label; ?></td>
						<td align="left"><?php echo $lists[$name]; ?></td>
						<td align="left" class="error"><?php echo $data[$name]->desc; ?></td>
					</tr>
				<?php } ?>
			</table>
		
			<?php
			$tabs->endTab();
			$tabs->endPane();
			?>
		
		  <input type="hidden" name="option" value="<?php echo $option; ?>">
		  <input type="hidden" name="task" value="saveConfig">
		</form>
		<script language="javascript" type="text/javascript">
			dhtml.cycleTab('tab1');
		</script>
	<?php }

	/********************************
	* About Availability Extension *
	*******************************/
	function showAbout() {
		global $database;
	?>
	<div style='text-align: center'><img width="153" height="103" src="../components/com_hotproperty/img/hp_logo.png" alt="Hot Property"></div>
	<br />
	<table width="50%" border="0" align="center" cellpadding="2" cellspacing="0" class="adminform">
		<tr>
			<th colspan="2"><h3>Hot Property</h3></th>
		</tr>
		<tr>
			<td align="center" colspan="2">Hot Property Component is <?php
				$database->setQuery("SELECT COUNT(*) FROM #__components WHERE `option`='com_hotproperty'");
				$installed = $database->loadResult();
				if ($installed > 0) {
					echo "<b>installed</b>";
				} else {
					echo "<span class=\"error\">not installed</span>";
					echo "<br /><span class=\"error\">Please install Hot Property component before using this Extension.</span>";
				}
			?></td>
		</tr>
		<tr>
			<td width="20%" align="right">Extension:</td>
			<td width="80%" align="left">Availability</td>
		</tr>
		<tr>
			<td width="20%" align="right">Version:</td>
			<td width="80%" align="left"><?php echo hp_avl_version; ?></td>
		</tr>
		<tr>
			<td width="20%" align="right">Description:</td>
			<td width="80%" align="left">Availability Component is an Extension for Hot Property. It allows Hot Property users to display Availability information to visitors.</td>
		</tr>
		<tr>
			<td width="20%" align="right">Website:</td>
			<td width="80%" align="left"><a target="_blank" href="http://www.mosets.com/">http://www.mosets.com/</a></td>
		</tr>
		<tr>
			<td width="20%" align="right">Author:</td>
			<td width="80%" align="left">CY Lee</td>
		</tr>
		<tr>
			<td width="20%" align="right">E-mail:</td>
			<td width="80%" align="left"><a href="mailto:hotproperty@mosets.com">hotproperty@mosets.com</a></td>
		</tr>
		<tr>
			<td width="20%" align="right">License:</td>
			<td width="80%" align="left"><a href="index2.php?option=com_hotproperty&task=license">License Agreement</a></td>
		</tr>
	</table>
	<br />
	<div style='text-align: center'>Lee Cher Yeong &copy; 2004 All rights reserved. Hot Property is a Proprietary software.<br />Please read the <a href="index2.php?option=com_hotproperty&task=license">license agreement</a> before using this product</div>
	<?php
	}

}

?>
