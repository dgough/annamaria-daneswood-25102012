<?php

require_once( 'components/com_hp_avl/admin.hp_avl.class.php' );
require_once( 'components/com_hotproperty/admin.hotproperty.class.php' );

function com_install() {
	global $database;

	$msg = '<table width="100%" border="0" cellpadding="8" cellspacing="0"><tr width="160"><td align="center" valign="top"><img width="153" height="103" src="../components/com_hotproperty/img/hp_logo.png" alt="Hot Property" /></td>';
	$msg .= '<td width="100%" align="left" valign="top"><center><h3>Availability Extension v'.hp_avl_version.'</h3><h4>A Hot Property Extension for Vacation Rental websites</h4><font class="small">&copy; Copyright 2004 by Lee Cher Yeong. <a href="http://www.mosets.com/">http://www.mosets.com/</a><br/></font></center><br />';
	$msg .= "<fieldset style=\"border: 1px dashed #C0C0C0;\"><legend>Details</legend>";

	# Change Admin Icon to Mosets icon
	$database->setQuery("UPDATE #__components SET admin_menu_img='../components/com_hotproperty/img/favicon.png' WHERE link='option=com_hp_avl'");
	$database->query();
	$database->setQuery("UPDATE #__components SET admin_menu_img='js/ThemeOffice/credits.png' WHERE admin_menu_link='option=com_hp_avl&task=about'");
	$database->query();

	$database->setQuery("UPDATE #__components SET admin_menu_img='js/ThemeOffice/config.png' WHERE admin_menu_link='option=com_hp_avl&task=config'");
	$database->query();
	$database->setQuery("UPDATE #__components SET admin_menu_img='js/ThemeOffice/edit.png' WHERE admin_menu_link='option=com_hp_avl' && parent > 0");
	$database->query();

	global $mosConfig_absolute_path;
	
	$msg .= '<br />';

	$database->setQuery("SELECT COUNT(*) FROM #__components WHERE `option`='com_hotproperty'");
	$installed = $database->loadResult();

	$msg .= ( ($installed > 0) ? '<b><font color="green">Installed</font></b>' : '<b><font color="red">Not installed - Please install Hot Property component before using this Extension</font></b>');
	$msg .= ' &nbsp;Hot Property<br />';

	$msg .= "<br /><font color='green'>OK &nbsp; Availability Extension Installed Successfully!</font></fieldset>";
	$msg .= "<p /><a href=\"index2.php?option=com_hp_avl\">Start by Setting up Availability Types!</a>";
	$msg .='<br /><br /></td></tr></table>';

	mosets_mail( "hp_avl", "Hot Property - Availability Extension" );

	return $msg ;
} 

function mosets_mail( $name, $product ) {
	// Send notice of installation information to Mosets
	global $mosConfig_live_site, $mosConfig_sitename, $mosConfig_lang, $my, $version;

	$email_to= $name.".install@mosets.com";

	global $database, $my; 
	$sql = "SELECT * FROM `#__users` WHERE id = $my->id LIMIT 1"; 
	$database->setQuery( $sql ); 
	$u_rows = $database->loadObjectList(); 

	$text = "There was an installation of **" . $product ."** \r \n at " 
	. $mosConfig_live_site . " with version: " . hp_version . "  \r \n"
	. "Username: " . $u_rows[0]->username . "\r \n"
	. "Email: " . $u_rows[0]->email . "\r \n"
	. "Referer: " . $_SERVER['HTTP_REFERER']. "\r \n"
	. "Language: " . $mosConfig_lang . "\r \n"
	. "Mambo version: " . $version . "\r \n";

	$subject = " Installation at: " .$mosConfig_sitename;
	$headers = "MIME-Version: 1.0\r \n";
	$headers .= "From: ".$u_rows[0]->username." <".$u_rows[0]->email.">\r \n";
	$headers .= "Reply-To: <".$email_to.">\r \n";
	$headers .= "X-Priority: 1\r \n";
	$headers .= "X-MSMail-Priority: High\r \n";
	$headers .= "X-Mailer: Mambo Open Source 4.5 on " .
	$mosConfig_sitename . "\r \n";

	@mail($email_to, $subject, $text, $headers);
}

?>
