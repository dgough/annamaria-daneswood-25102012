<?php
/**
* @version $Id: admin.content.html.php 6070 2006-12-20 02:09:09Z robs $
* @package Joomla
* @subpackage Content
* @copyright Copyright (C) 2005 Open Source Matters. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );

/**
* @package Joomla
* @subpackage Content
*/
class HTML_content {

	/**
	* Writes a list of the content items
	* @param array An array of content objects
	*/
	function showContent( &$rows, $section, &$lists, $search, $pageNav, $all=NULL, $redirect ) {
		global $my, $acl, $database, $mosConfig_offset;

		mosCommonHTML::loadOverlib();
		?>
		<form action="index2.php?option=com_content" method="post" name="adminForm">

		<table class="adminheading">
			<tr>
				<th class="edit" nowrap="nowrap">
				<?php
				if ( $all ) {
					?>
					Content Items Manager <small><small>[ Section: All ]</small></small>
					<?php
				} else {
					?>
					Content Items Manager <small><small>[ Section: <?php echo $section->title; ?> ]</small></small>
					<?php
				}
				?>
				</th>
				<td align='right'>
					<table width='400px'>
						<tr>
							<?php
							if ( $all ) {
								?>
								<td align="right" valign="top" width='33%'>
								<?php echo $lists['sectionid'];?>
								</td>
								<?php
							}
							?>
							<td align="center" valign="top" width='33%'>
							<?php echo $lists['catid'];?>
							</td>
							<td align='left' valign="top" width='33%'>
							<?php echo $lists['authorid'];?>
							</td>
						</tr>
						<tr>
							<td align="right" width='33%'>&nbsp;</td>
							<td align="right" width='33%'>
							Filter:
							</td>
							<td width='33%'>
							<input type="text" name="search" value="<?php echo htmlspecialchars( $search );?>" class="text_area" onChange="document.adminForm.submit();" />
							</td>
						</tr>
					</table>		
				</td>
			</tr>
		</table>

		<table class="adminlist">
		<tr>
			<th width="5">
			#
			</th>
			<th width="5">
			<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $rows ); ?>);" />
			</th>
			<th class="title">
			Title
			</th>
			<th width="5%">
			Published
			</th>
			<th nowrap="nowrap" width="5%">
			Front Page
			</th>
			<th colspan="2" align="center" width="5%">
			Reorder
			</th>
			<th width="2%">
			Order
			</th>
			<th width="1%">
			<a href="javascript: saveorder( <?php echo count( $rows )-1; ?> )"><img src="images/filesave.png" border="0" width="16" height="16" alt="Save Order" /></a>
			</th>
			<th >
			Access
			</th>
			<th width="2%">
			ID
			</th>
			<?php
			if ( $all ) {
				?>
				<th align="left">
				Section
				</th>
				<?php
			}
			?>
			<th align="left">
			Category
			</th>
			<th align="left">
			Author
			</th>
			<th align="center" width="10">
			Date
			</th>
		  </tr>
		<?php
		$k = 0;
		$nullDate = $database->getNullDate();
		$sectionid = '';
		$categoryid = '';
		for ($i=0, $n=count( $rows ); $i < $n; $i++) {
			$row = &$rows[$i];

			mosMakeHtmlSafe($row);

			$link 	= 'index2.php?option=com_content&sectionid='. $redirect .'&task=edit&hidemainmenu=1&id='. $row->id;

			$row->sect_link 	= 'index2.php?option=com_sections&task=editA&hidemainmenu=1&id='. $row->sectionid;
			$row->cat_link 		= 'index2.php?option=com_categories&task=editA&hidemainmenu=1&id='. $row->catid;

			$now = _CURRENT_SERVER_TIME;
			if ( $now <= $row->publish_up && $row->state == 1 ) {
			// Published
				$img = 'publish_y.png';
				$alt = 'Published';
			} else if ( ( $now <= $row->publish_down || $row->publish_down == $nullDate ) && $row->state == 1 ) {
			// Pending
				$img = 'publish_g.png';
				$alt = 'Published';
			} else if ( $now > $row->publish_down && $row->state == 1 ) {
			// Expired
				$img = 'publish_r.png';
				$alt = 'Expired';
			} elseif ( $row->state == 0 ) {
			// Unpublished
				$img = 'publish_x.png';
				$alt = 'Unpublished';
			}

			// correct times to include server offset info
			$row->publish_up 	= mosFormatDate( $row->publish_up, _CURRENT_SERVER_TIME_FORMAT );
			if (trim( $row->publish_down ) == $nullDate || trim( $row->publish_down ) == '' || trim( $row->publish_down ) == '-' ) {
				$row->publish_down = 'Never';
			}
			$row->publish_down 	= mosFormatDate( $row->publish_down, _CURRENT_SERVER_TIME_FORMAT );

			$times = '';
			if ($row->publish_up == $nullDate) {
				$times .= "<tr><td>Start: Always</td></tr>";
			} else {
				$times .= "<tr><td>Start: $row->publish_up</td></tr>";
			}
			if ($row->publish_down == $nullDate || $row->publish_down == 'Never') {
				$times .= "<tr><td>Finish: No Expiry</td></tr>";
			} else {
				$times .= "<tr><td>Finish: $row->publish_down</td></tr>";
			}

			if ( $acl->acl_check( 'administration', 'manage', 'users', $my->usertype, 'components', 'com_users' ) ) {
				if ( $row->created_by_alias ) {
					$author = $row->created_by_alias;
				} else {
					$linkA 	= 'index2.php?option=com_users&task=editA&hidemainmenu=1&id='. $row->created_by;
					$author = '<a href="'. $linkA .'" title="Edit User">'. $row->author .'</a>';
				}
			} else {
				if ( $row->created_by_alias ) {
					$author = $row->created_by_alias;
				} else {
					$author = $row->author;
				}
			}

			$date = mosFormatDate( $row->created, '%x' );

			$access 	= mosCommonHTML::AccessProcessing( $row, $i );
			$checked 	= mosCommonHTML::CheckedOutProcessing( $row, $i );
			
			if($row->sectionid !== $sectionid){
				?><tr class="<?php echo "row$k"; ?>" style='font-weight: bold'><td colspan="15"><?php		
				echo ($row->sectionid == 0) ? 'Uncategorized' : $row->section_name;
				?></td></tr><?php
				$sectionid = $row->sectionid;
				$k = (int)!$k;
			}
			if($row->catid != $categoryid && $row->sectionid != 0){
				?><tr class="<?php echo "row$k"; ?>" style='font-weight: bold'><td>&nbsp;</td><td colspan="43"><?php		
				echo $row->name;
				?></td></tr><?php
				$categoryid = $row->catid;
				$k = (int)!$k;
			}
			
			?>
			<tr class="<?php echo "row$k"; ?>">
				<td>
				<?php echo $pageNav->rowNumber( $i ); ?>
				</td>
				<td align="center">
				<?php echo $checked; ?>
				</td>
				<td>
				<?php
				if ( $row->checked_out && ( $row->checked_out != $my->id )) {
					echo $row->title;
				} else {
					?>
					<a href="<?php echo $link; ?>" title="Edit Content">
					<?php echo $row->title; ?>
					</a>
					<?php
				}
				?>
				</td>
				<?php
				if ( $times ) {
					?>
					<td align="center" width='50px'>
					<a href="javascript: void(0);" onMouseOver="return overlib('<table><?php echo $times; ?></table>', CAPTION, 'Publish Information', BELOW, RIGHT);" onMouseOut="return nd();" onclick="return listItemTask('cb<?php echo $i;?>','<?php echo $row->state ? "unpublish" : "publish";?>')">
					<img src="images/<?php echo $img;?>"  border="0" alt="<?php echo $alt; ?>" />
					</a>
					</td>
					<?php
				}
				?>
				<td align="center" width='50px'>
				<a href="javascript: void(0);" onclick="return listItemTask('cb<?php echo $i;?>','toggle_frontpage')">
				<img src="images/<?php echo ( $row->frontpage ) ? 'tick.png' : 'publish_x.png';?>"  border="0" alt="<?php echo ( $row->frontpage ) ? 'Yes' : 'No';?>" />
				</a>
				</td>
				<td align="right" width='25px'>
				<?php echo $pageNav->orderUpIcon( $i, ($row->catid == @$rows[$i-1]->catid) ); ?>
				</td>
				<td align="left" width='25px'>
				<?php echo $pageNav->orderDownIcon( $i, $n, ($row->catid == @$rows[$i+1]->catid) ); ?>
				</td>
				<td align="center" colspan="2" width='100px'>
				<input type="text" name="order[]" size="5" value="<?php echo $row->ordering; ?>" class="text_area" style="text-align: center" />
				</td>
				<td align="center" width='50px'>
				<?php echo $access;?>
				</td>
				<td align="left" width='50px'>
				<?php echo $row->id; ?>
				</td>
				<?php
				if ( $all ) {
					?>
					<td align="left" width='150px'>
					<?php if($row->sectionid == 0){ ?>
					Uncategorized
					<?php }else{ ?>
					<a href="<?php echo $row->sect_link; ?>" title="Edit Section">
					<?php echo $row->section_name; ?>
					</a>
					<?php } ?>
					</td>
					<?php
				}
				?>
				<td align="left" width='150px'>
				<a href="<?php echo $row->cat_link; ?>" title="Edit Category">
				<?php echo $row->name; ?>
				</a>
				</td>
				<td align="left" width='150px'>
				<?php echo $author; ?>
				</td>
				<td align="left" width='50px'>
				<?php echo $date; ?>
				</td>
			</tr>
			<?php
			$k = 1 - $k;
		}
		?>
		</table>

		<?php echo $pageNav->getListFooter(); ?>
		<?php mosCommonHTML::ContentLegend(); ?>

		<input type="hidden" name="option" value="com_content" />
		<input type="hidden" name="sectionid" value="<?php //echo $section->id;?>" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="hidemainmenu" value="0" />
		<input type="hidden" name="redirect" value="<?php echo $redirect;?>" />
		<input type="hidden" name="<?php echo josSpoofValue(); ?>" value="1" />
		</form>
		<?php
	}


	/**
	* Writes a list of the content items
	* @param array An array of content objects
	*/
	function showArchive( &$rows, $section, &$lists, $search, $pageNav, $option, $all=NULL, $redirect ) {
		global $my, $acl;

		?>
		<script language="javascript" type="text/javascript">
		function submitbutton(pressbutton) {
			if (pressbutton == 'remove') {
				if (document.adminForm.boxchecked.value == 0) {
					alert('Please make a selection from the list to send to Trash');
				} else if ( confirm('Are you sure you want to Trash the selected items? \nThis will not permanently delete the items.')) {
					submitform('remove');
				}
			} else {
				submitform(pressbutton);
			}
		}
		</script>
		<form action="index2.php" method="post" name="adminForm">

		<table class="adminheading">
		<tr>
			<th class="edit" rowspan="2">
			<?php
			if ( $all ) {
				?>
				Archive Manager <small><small>[ Section: All ]</small></small>
				<?php
			} else {
				?>
				Archive Manager <small><small>[ Section: <?php echo $section->title; ?> ]</small></small>
				<?php
			}
			?>
			</th>
			<?php
			if ( $all ) {
				?>
				<td align="right" rowspan="2" valign="top">
				<?php echo $lists['sectionid'];?>
				</td>
				<?php
			}
			?>
			<td align="right" valign="top">
			<?php echo $lists['catid'];?>
			</td>
			<td valign="top">
			<?php echo $lists['authorid'];?>
			</td>
		</tr>
		<tr>
			<td align="right">
			Filter:
			</td>
			<td>
			<input type="text" name="search" value="<?php echo htmlspecialchars( $search );?>" class="text_area" onChange="document.adminForm.submit();" />
			</td>
		</tr>
		</table>

		<table class="adminlist">
		<tr>
			<th width="5">
			#
			</th>
			<th width="20">
			<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $rows ); ?>);" />
			</th>
			<th class="title">
			Title
			</th>
			<th width="2%">
			Order
			</th>
			<th width="1%">
			<a href="javascript: saveorder( <?php echo count( $rows )-1; ?> )"><img src="images/filesave.png" border="0" width="16" height="16" alt="Save Order" /></a>
			</th>
			<th width="15%" align="left">
			Category
			</th>
			<th width="15%" align="left">
			Author
			</th>
			<th align="center" width="10">
			Date
			</th>
		</tr>
		<?php
		$k = 0;
		for ($i=0, $n=count( $rows ); $i < $n; $i++) {
			$row = &$rows[$i];

			$row->cat_link 	= 'index2.php?option=com_categories&task=editA&hidemainmenu=1&id='. $row->catid;

			if ( $acl->acl_check( 'administration', 'manage', 'users', $my->usertype, 'components', 'com_users' ) ) {
				if ( $row->created_by_alias ) {
					$author = $row->created_by_alias;
				} else {
					$linkA 	= 'index2.php?option=com_users&task=editA&hidemainmenu=1&id='. $row->created_by;
					$author = '<a href="'. $linkA .'" title="Edit User">'. $row->author .'</a>';
				}
			} else {
				if ( $row->created_by_alias ) {
					$author = $row->created_by_alias;
				} else {
					$author = $row->author;
				}
			}

			$date = mosFormatDate( $row->created, '%x' );
			?>
			<tr class="<?php echo "row$k"; ?>">
				<td>
				<?php echo $pageNav->rowNumber( $i ); ?>
				</td>
				<td width="20">
				<?php echo mosHTML::idBox( $i, $row->id ); ?>
				</td>
				<td>
				<?php echo $row->title; ?>
				</td>
				<td align="center" colspan="2">
				<input type="text" name="order[]" size="5" value="<?php echo $row->ordering; ?>" class="text_area" style="text-align: center" />
				</td>
				<td>
				<a href="<?php echo $row->cat_link; ?>" title="Edit Category">
				<?php echo $row->name; ?>
				</a>
				</td>
				<td>
				<?php echo $author; ?>
				</td>
				<td>
				<?php echo $date; ?>
				</td>
			</tr>
			<?php
			$k = 1 - $k;
		}
		?>
		</table>

		<?php echo $pageNav->getListFooter(); ?>

		<input type="hidden" name="option" value="<?php echo $option;?>" />
		<input type="hidden" name="sectionid" value="<?php echo $section->id;?>" />
		<input type="hidden" name="task" value="showarchive" />
		<input type="hidden" name="returntask" value="showarchive" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="hidemainmenu" value="0" />
		<input type="hidden" name="redirect" value="<?php echo $redirect;?>" />
		<input type="hidden" name="<?php echo josSpoofValue(); ?>" value="1" />
		</form>
		<?php
	}


	/**
	* Writes the edit form for new and existing content item
	*
	* A new record is defined when <var>$row</var> is passed with the <var>id</var>
	* property set to 0.
	* @param mosContent The category object
	* @param string The html for the groups select list
	*/
	function editContent( &$row, $section, &$lists, &$sectioncategories, &$images, &$params, $option, $redirect, &$menus ) {
		global $database,$mosConfig_live_site, $mainframe;
		$publishing_tab = 1;
		$images_tab = 1;
		$parameters_tab = 1;
		$meta_tab = 1;
		$menu_tab = 0;

		mosMakeHtmlSafe( $row );

		$nullDate 		= $database->getNullDate();
		$create_date 	= null;

		if ( $row->created != $nullDate ) {
			$create_date 	= mosFormatDate( $row->created, '%A, %d %B %Y %H:%M', '0' );
		}
		$mod_date = null;
		if ( $row->modified != $nullDate ) {
			$mod_date 		= mosFormatDate( $row->modified, '%A, %d %B %Y %H:%M', '0' );
		}

		$tabs = new mosTabs(1);

		// used to hide "Reset Hits" when hits = 0
		if ( !$row->hits ) {
			$visibility = "style='display: none; visibility: hidden;'";
		} else {
			$visibility = "";
		}

		mosCommonHTML::loadOverlib();
		mosCommonHTML::loadCalendar();
		?>
		<script language="javascript" type="text/javascript">
		<!--
		var sectioncategories = new Array;
		<?php
		$i = 0;
		foreach ($sectioncategories as $k=>$items) {
			foreach ($items as $v) {
				echo "sectioncategories[".$i++."] = new Array( '$k','".addslashes( $v->id )."','".addslashes( $v->name )."' );\t";
			}
		}
		?>

		var folderimages = new Array;
		<?php
		$i = 0;
		foreach ($images as $k=>$items) {
			foreach ($items as $v) {
				echo "folderimages[".$i++."] = new Array( '$k','".addslashes( ampReplace( $v->value ) )."','".addslashes( ampReplace( $v->text ) )."' );\t";
			}
		}
		?>

		function submitbutton(pressbutton) {
			var form = document.adminForm;

			if ( pressbutton == 'menulink' ) {
				if ( form.menuselect.value == "" ) {
					alert( "Please select a Menu" );
					return;
				} else if ( form.link_name.value == "" ) {
					alert( "Please enter a Name for this menu item" );
					return;
				}
			}

			if (pressbutton == 'cancel') {
				submitform( pressbutton );
				return;
			}
			// assemble the images back into one field
			<?php if($tab_images){ ?>
			var temp = new Array;
			for (var i=0, n=form.imagelist.options.length; i < n; i++) {
				temp[i] = form.imagelist.options[i].value;
			}
			form.images.value = temp.join( '\n' );
			<?php } ?>
			// do field validation
			if (form.title.value == ""){
				alert( "Content item must have a title" );
			} else if (form.sectionid.value == "-1"){
				alert( "You must select a Section." );
			} else if (form.catid.value == "-1" && form.sectionid.value > 0){
				alert( "You must select a Category." );
 			} else if (form.catid.value == "" && form.sectionid.value > 0){
 				alert( "You must select a Category." );
			} else {
				<?php getEditorContents( 'editor1', 'introtext' ) ; ?>
				<?php getEditorContents( 'editor2', 'fulltext' ) ; ?>
				submitform( pressbutton );
			}
			
		}
		
		function insertReadmore(editor) {
				var content = tinyMCE.getContent();
				//Set the focus onto tinymce
				tinyMCE.selectedInstance.contentWindow.focus();
				if (content.match(/<hr id="system-readmore" \/>/)) {
					alert('There is already a Read more... link that has been inserted. Only one such link is permitted.');
					return false;
				} else {					
					tinyMCE.execCommand('mceInsertContent',false,'<hr id="system-readmore" />')
				}
			}

		//-->
		</script>

		<form action="index2.php" method="post" name="adminForm">
		<table class="adminheading">
		<tr>
			<th class="edit">
			Content Item:
			<small>
			<?php echo $row->id ? 'Edit' : 'New';?>
			</small>
			<?php
			if ( $row->id ) {
				?>
				<small><small>
				[ Section: <?php echo $section; ?> ]
				</small></small>
				<?php
			}
			?>
			</th>
		</tr>
		</table>

		<table cellspacing="0" cellpadding="0" width="100%">
		<tr>
			<td width="60%" valign="top">
				<table width="100%" class="adminform">
				<tr>
					<td width="100%">
						<table cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr>
							<th colspan="4">
							Item Details
							</th>
						</tr>
						<tr>
							<td>
							Title:
							</td>
							<td>
							<input class="text_area" type="text" name="title" size="30" maxlength="100" value="<?php echo $row->title; ?>" />
							</td>
							<td>
							Section:
							</td>
							<td>
							<?php echo $lists['sectionid']; ?>
							</td>
						</tr>
						<tr>
							<td>
							Title Alias:
							</td>
							<td>
							<input name="title_alias" type="text" class="text_area" id="title_alias" value="<?php echo $row->title_alias; ?>" size="30" maxlength="100" />
							</td>
							<td>
							Category:
							</td>
							<td>
							<?php echo $lists['catid']; ?>
							</td>
						</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td width="100%">
					Intro Text: (required)
					<br /><?php
					// parameters : areaname, content, hidden field, width, height, rows, cols
					editorArea( 'editor1', $row->introtext, 'introtext', '100%;', '350', '75', '20' ) ; ?>
					<img src='<?php echo $mosConfig_live_site?>/mambots/editors-xtd/mosreadmore.gif' onclick="insertReadmore('introtext');return false;" />
					</td>
				</tr>
				<?php /*
				<tr>
					<td width="100%">
					Main Text: (optional)
					<br /><?php
					// parameters : areaname, content, hidden field, width, height, rows, cols
					editorArea( 'editor2', $row->fulltext, 'fulltext', '100%;', '400', '75', '30' ) ; ?>
					</td>
				</tr>
				*/ ?>
				</table>
			</td>
			<td valign="top" width="40%">
				<?php
			$tabs->startPane("content-pane");
			if($publishing_tab){
				$tabs->startTab("Publishing","publish-page");
				?>
				<table class="adminform">
				<tr>
					<th colspan="2">
					Publishing Info
					</th>
				</tr>
				<tr>
					<td valign="top" align="right" width="120">
					Show on Front Page:
					</td>
					<td>
					<input type="checkbox" name="frontpage" value="1" <?php echo $row->frontpage ? 'checked="checked"' : ''; ?> />
					</td>
				</tr>
				<tr>
					<td valign="top" align="right">
					Published:
					</td>
					<td>
					<input type="checkbox" name="published" value="1" <?php echo $row->state ? 'checked="checked"' : ''; ?> />
					</td>
				</tr>
				<tr>
					<td valign="top" align="right">
					Access Level:
					</td>
					<td>
					<?php echo $lists['access']; ?>
					</td>
				</tr>
				<tr>
					<td valign="top" align="right">
					Author Alias:
					</td>
					<td>
					<input type="text" name="created_by_alias" size="30" maxlength="100" value="<?php echo $row->created_by_alias; ?>" class="text_area" />
					</td>
				</tr>
				<tr>
					<td valign="top" align="right">
					Change Creator:
					</td>
					<td>
					<?php echo $lists['created_by']; ?>
					</td>
				</tr>
				<tr>
					<td valign="top" align="right">Ordering:</td>
					<td>
					<?php echo $lists['ordering']; ?>
					</td>
				</tr>
				<tr>
					<td valign="top" align="right">
					Override Created Date
					</td>
					<td>
					<input class="text_area" type="text" name="created" id="created" size="25" maxlength="19" value="<?php echo $row->created; ?>" />
					<input name="reset" type="reset" class="button" onclick="return showCalendar('created', 'y-mm-dd');" value="..." />
					</td>
				</tr>
				<tr>
					<td valign="top" align="right">
					Start Publishing:
					</td>
					<td>
					<input class="text_area" type="text" name="publish_up" id="publish_up" size="25" maxlength="19" value="<?php echo $row->publish_up; ?>" />
					<input type="reset" class="button" value="..." onclick="return showCalendar('publish_up', 'y-mm-dd');" />
					</td>
				</tr>
				<tr>
					<td valign="top" align="right">
					Finish Publishing:
					</td>
					<td>
					<input class="text_area" type="text" name="publish_down" id="publish_down" size="25" maxlength="19" value="<?php echo $row->publish_down; ?>" />
					<input type="reset" class="button" value="..." onclick="return showCalendar('publish_down', 'y-mm-dd');" />
					</td>
				</tr>
				</table>
				<br />
				<table class="adminform">
				<?php
				if ( $row->id ) {
					?>
					<tr>
						<td>
						<strong>Content ID:</strong>
						</td>
						<td>
						<?php echo $row->id; ?>
						</td>
					</tr>
					<?php
				}
				?>
				<tr>
					<td width="120" valign="top" align="right">
					<strong>State:</strong>
					</td>
					<td>
					<?php echo $row->state > 0 ? 'Published' : ($row->state < 0 ? 'Archived' : 'Draft Unpublished');?>
					</td>
				</tr>
				<tr >
					<td valign="top" align="right">
					<strong>
					Hits
					</strong>:
					</td>
					<td>
					<?php echo $row->hits;?>
					<div <?php echo $visibility; ?>>
					<input name="reset_hits" type="button" class="button" value="Reset Hit Count" onclick="submitbutton('resethits');" />
					</div>
					</td>
				</tr>
				<tr>
					<td valign="top" align="right">
					<strong>
					Revised
					</strong>:
					</td>
					<td>
					<?php echo $row->version;?> times
					</td>
				</tr>
				<tr>
					<td valign="top" align="right">
					<strong>
					Created
					</strong>
					</td>
					<td>
						<?php
						if ( !$create_date ) {
							?>
							New document
							<?php
						} else {
							echo $create_date;
						}
						?>
					</td>
				</tr>
				<tr>
					<td valign="top" align="right">
					<strong>
					Last Modified
					</strong>
					</td>
					<td>
						<?php
						if ( !$mod_date ) {
							?>
							Not modified
							<?php
						} else {
							echo $mod_date;
							?>
							<br />
							<?php
							echo $row->modifier;
						}
						?>
					</td>
				</tr>
				<tr>
					<td>
					<strong>URL:</strong>
					</td>
					<td>
					<?php echo "index.php?option=com_content&task=view&id=".$row->id; ?>
					</td>
				</table>
				<?php
				$tabs->endTab();
			}
			if($images_tab){
				$tabs->startTab("Icon","images-page");
				?>
				<script type='text/javascript'>
				//<!--
				
				function setIcon(){

					var imagelist = getIcon();
					
					var folderIndex = document.forms[0].folders.selectedIndex;
					var folder = document.forms[0].folders[folderIndex].value;
					
					document.getElementById('icon').value = folder+imagelist;
					document.getElementById('view_icon').src = '<?php echo $mosConfig_live_site.'/images/stories/' ?>'+imagelist;					
				}
				
				function removeIcon(){
					document.getElementById('icon').value = '';
					document.getElementById('view_icon').src = '../images/M_images/blank.png';		
				}
				
				function getIcon(){
					var imageIndex = document.getElementById('imagefiles').selectedIndex;
					var imagelist = document.getElementById('imagefiles')[imageIndex].value;
					
					return imagelist;
				}
				
				//-->				
				</script>
				<table class="adminform" width="100%">
				<tr>
					<th colspan="2">
						Icon Control
					</th>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%">
						<tr height='115px'>
							<td valign="top">
								Sub-folder: 
								<br />
								<br />
								<?php echo $lists['folders'];?>
							</td>
							<td valign="top">
								<div align="center">
									Gallery Images:
									<br />
									<br />
									<?php echo $lists['imagefiles'];?>
								</div>
							</td>
							<td valign="top">
							<div align="center">
								Preview Icon:
								<br />
								<br />									
								<img name="view_imagefiles" src="../images/M_images/blank.png" alt="Sample Image" />
							</div>
							</td>
							<?php /*
							<td width="2%">
								<input class="button" type="button" value=">>" onclick="setIcon()" title="Add" />
								<br />
								<input class="button" type="button" value="<<" onclick="removeIcon()" title="Remove" />
							</td>
							*/ ?>
							<td valign="top">
								<div align="center">
									Active Icon:
									<br />
									<br />
									<?php if($row->images != ''){ ?>
										<img id='view_icon' src="<?php echo "$mosConfig_live_site/images/stories/$row->images" ?>" alt="Active Image"  />
									<?php }else{ ?>
										<img id='view_icon' src="../images/M_images/blank.png" alt="Active Image"  />
									<?php } ?>
									<?php /*<input type='hidden' id='icon' name='images' value='<?php echo $row->images ?>' />*/ ?>
									<?php //echo $lists['imagelist'];?>
								</div>
							</td>
						
						</tr>
						</table>
						
					</td>
				</tr>
				
				</table>
				<?php
				$tabs->endTab();
			}
			if($parameters_tab){
				$tabs->startTab("Parameters","params-page");
				?>
				<table class="adminform">
				<tr>
					<th colspan="2">
					Parameter Control
					</th>
				</tr>
				<tr>
					<td>
					* These Parameters only control what you see when you click to view an item fully *
					<br /><br />
					</td>
				</tr>
				<tr>
					<td>
					<?php echo $params->render();?>
					</td>
				</tr>
				</table>
				<?php
				$tabs->endTab();
			}if($meta_tab){
				$tabs->startTab("Meta Info","metadata-page");
				?>
				<table class="adminform">
				<tr>
					<th colspan="2">
					Meta Data
					</th>
				</tr>
				<tr>
					<td>
					Description:
					<br />
					<textarea class="text_area" cols="30" rows="3" style="width: 350px; height: 50px" name="metadesc"><?php echo str_replace('&','&amp;',$row->metadesc); ?></textarea>
					</td>
				</tr>
					<tr>
					<td>
					Keywords:
					<br />
					<textarea class="text_area" cols="30" rows="3" style="width: 350px; height: 50px" name="metakey"><?php echo str_replace('&','&amp;',$row->metakey); ?></textarea>
					</td>
				</tr>
				<tr>
					<td>
					<input type="button" class="button" value="Add Sect/Cat/Title" onclick="f=document.adminForm;f.metakey.value=document.adminForm.sectionid.options[document.adminForm.sectionid.selectedIndex].text+', '+getSelectedText('adminForm','catid')+', '+f.title.value+f.metakey.value;" />
					</td>
				</tr>
				</table>
				<?php
				$tabs->endTab();
			}
			if($menu_tab){
				$tabs->startTab("Link to Menu","link-page");
				?>
				<table class="adminform">
				<tr>
					<th colspan="2">
					Link to Menu
					</th>
				</tr>
				<tr>
					<td colspan="2">
					This will create a 'Link - Content Item' in the menu you select
					<br /><br />
					</td>
				</tr>
				<tr>
					<td valign="top" width="90">
					Select a Menu
					</td>
					<td>
					<?php echo $lists['menuselect']; ?>
					</td>
				</tr>
				<tr>
					<td valign="top" width="90">
					Menu Item Name
					</td>
					<td>
					<input type="text" name="link_name" class="inputbox" value="" size="30" />
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td>
					<input name="menu_link" type="button" class="button" value="Link to Menu" onclick="submitbutton('menulink');" />
					</td>
				</tr>
				<tr>
					<th colspan="2">
					Existing Menu Links
					</th>
				</tr>
				<?php
				if ( $menus == NULL ) {
					?>
					<tr>
						<td colspan="2">
						None
						</td>
					</tr>
					<?php
				} else {
					mosCommonHTML::menuLinksContent( $menus );
				}
				?>
				<tr>
					<td colspan="2">
					</td>
				</tr>
				</table>
				<?php
				$tabs->endTab();
			}
				$tabs->endPane();
				?>
			</td>
		</tr>
		</table>

		<input type="hidden" name="id" value="<?php echo $row->id; ?>" />
		<input type="hidden" name="version" value="<?php echo $row->version; ?>" />
		<input type="hidden" name="mask" value="0" />
		<input type="hidden" name="option" value="<?php echo $option;?>" />
		<input type="hidden" name="redirect" value="<?php echo $redirect;?>" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="hidemainmenu" value="0" />
		<input type="hidden" name="<?php echo josSpoofValue(); ?>" value="1" />
		</form>
		<?php

	}


	/**
	* Form to select Section/Category to move item(s) to
	* @param array An array of selected objects
	* @param int The current section we are looking at
	* @param array The list of sections and categories to move to
	*/
	function moveSection( $cid, $sectCatList, $option, $sectionid, $items ) {
		?>
		<script language="javascript" type="text/javascript">
		function submitbutton(pressbutton) {
			var form = document.adminForm;
			if (pressbutton == 'cancel') {
				submitform( pressbutton );
				return;
			}

			// do field validation
			if (!getSelectedValue( 'adminForm', 'sectcat' )) {
				alert( "Please select something" );
			} else {
				submitform( pressbutton );
			}
		}
		</script>

		<form action="index2.php" method="post" name="adminForm">
		<br />
		<table class="adminheading">
		<tr>
			<th class="edit">
			Move Items
			</th>
		</tr>
		</table>

		<br />
		<table class="adminform">
		<tr>
			<td align="left" valign="top" width="40%">
			<strong>Move to Section/Category:</strong>
			<br />
			<?php echo $sectCatList; ?>
			<br /><br />
			</td>
			<td align="left" valign="top">
			<strong>Items being Moved:</strong>
			<br />
			<?php
			echo "<ol>";
			foreach ( $items as $item ) {
				echo "<li>". $item->title ."</li>";
			}
			echo "</ol>";
			?>
			</td>
		</tr>
		</table>
		<br /><br />

		<input type="hidden" name="option" value="<?php echo $option;?>" />
		<input type="hidden" name="sectionid" value="<?php echo $sectionid; ?>" />
		<input type="hidden" name="task" value="" />
		<?php
		foreach ($cid as $id) {
			echo "\n<input type=\"hidden\" name=\"cid[]\" value=\"$id\" />";
		}
		?>
		<input type="hidden" name="<?php echo josSpoofValue(); ?>" value="1" />
		</form>
		<?php
	}



	/**
	* Form to select Section/Category to copys item(s) to
	*/
	function copySection( $option, $cid, $sectCatList, $sectionid, $items  ) {
		?>
		<script language="javascript" type="text/javascript">
		function submitbutton(pressbutton) {
			var form = document.adminForm;
			if (pressbutton == 'cancel') {
				submitform( pressbutton );
				return;
			}

			// do field validation
			if (!getSelectedValue( 'adminForm', 'sectcat' )) {
				alert( "Please select a Section/Category to copy the items to" );
			} else {
				submitform( pressbutton );
			}
		}
		</script>
		<form action="index2.php" method="post" name="adminForm">
		<br />
		<table class="adminheading">
		<tr>
			<th class="edit">
			Copy Content Items
			</th>
		</tr>
		</table>

		<br />
		<table class="adminform">
		<tr>
			<td align="left" valign="top" width="40%">
			<strong>Copy to Section/Category:</strong>
			<br />
			<?php echo $sectCatList; ?>
			<br /><br />
			</td>
			<td align="left" valign="top">
			<strong>Items being copied:</strong>
			<br />
			<?php
			echo "<ol>";
			foreach ( $items as $item ) {
				echo "<li>". $item->title ."</li>";
			}
			echo "</ol>";
			?>
			</td>
		</tr>
		</table>
		<br /><br />

		<input type="hidden" name="option" value="<?php echo $option;?>" />
		<input type="hidden" name="sectionid" value="<?php echo $sectionid; ?>" />
		<input type="hidden" name="task" value="" />
		<?php
		foreach ($cid as $id) {
			echo "\n<input type=\"hidden\" name=\"cid[]\" value=\"$id\" />";
		}
		?>
		<input type="hidden" name="<?php echo josSpoofValue(); ?>" value="1" />
		</form>
		<?php
	}
}
?>