<?php
/*
* MassContent for Joomla 1.0.13
* @version 0.5
* @Date 01/12/2007
* @copyright (C) 2007 Johann Eriksen
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
* Official website: http://www.webtotalservice.no
*/
defined( '_VALID_MOS' ) or die( 'Direct access to this location is not allowed.' );
//this class extends the mosDBTable class
class mosDBMassContent extends mosDBTable {
	// variables needed
    var $id=null;
	var $title=null;            
	var $created_by = null;   
	var $created = null;   		
    var $catid=null;
    var $sectionid=null;
    var $state=null;
    var $publish_up=null;
    var $introtext=null;
    var $access=null;
  
 // contstructor
  function mosDBMassContent( &$db ) {
    $this->mosDBTable( '#__content', 'id', $db );
  }
}

class mosDBMassSections extends mosDBTable {
	// variables needed
    var $id=null;
	var $title=null;            
	var $name=null;    
    var $published=null;
    var $access=null;
    var $scope=null;
 
  
 // contstructor
  function mosDBMassSections( &$db ) {
    $this->mosDBTable( '#__sections', 'id', $db );
  }
}

class mosDBMassCategories extends mosDBTable {
	// variables needed
    var $id=null;
    var $parent_id=null;
	var $title=null;            
	var $name=null;    
	var $section=null;        
    var $published=null;
    var $access=null;
 
  
 // contstructor
  function mosDBMassCategories( &$db ) {
    $this->mosDBTable( '#__categories', 'id', $db );
  }
}
?>
