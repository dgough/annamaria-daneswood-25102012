<?php
/*
* MassContent for Joomla 1.0.13
* @version 0.5
* @Date 01/12/2007
* @copyright (C) 2007 Johann Eriksen
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
* Official website: http://www.webtotalservice.no
*/
defined( '_VALID_MOS' ) or die( 'Direct access to this location is not allowed.' );

// load DB class and HTML class
require_once( $mainframe->getPath( 'admin_html' ) );
require_once( $mainframe->getPath( 'class' ) );
$id 	= mosGetParam( $_GET, 'id', 0 );
$cid 	= mosGetParam( $_POST, 'cid', array(0) );
$act 		= mosGetParam( $_REQUEST, 'act', '' );
$task 		= mosGetParam( $_REQUEST, 'task', '' );
if (!is_array( $cid )) { $cid = array(0); }
 
switch ($act) {
    case "createMassSections":    
        switch ($task)
        {
        case "cpanel":
            showCPanel($option);
            break;
    	case "save":
    		saveMassSections( $option );
    		break;
        case "new":
    	default:
    		newMassSections( );
    		break;
        }        
    break; 
    case "createMassCategories":    
        switch ($task)
        {
        case "cpanel":
            showCPanel($option);
            break;        
    	case "save":
    		saveMassCategories( $option );
    		break;
        case "new":
    	default:
    		newMassCategories( );
    		break;
        }        
    break;     

    case "createMassContent":   
        switch ($task)
        {
        case "cpanel":
            showCPanel($option);
            break;        
    	case "save":
    		saveMassContent( $option );
    		break;
        case "new":            
    	default:
    		newMassContent( 0,$option );
    		break;
        }        
    break;    
  case "deleteMassContent":   
        switch ($task)
        {
        case "cpanel":
            showCPanel($option);
            break;        
    	case "delete":
    		deleteMassContent( $option );
    		break;
        case "new":            
    	default:
    		newDeleteMassContent( 0,$option );
    		break;
        }        
    break;        
    case "cpanel":
    default: 
        showCPanel($option);
        break;
}

function showCPanel($option){
    HTML_Masscontent::showCPanel($option);
}

/**
* Link the content to the menu
* @param id The id of the content to insert
* @param title: The  title of the menu element
* @param menuselect: The menu where to create the link
* @param contentType:  to know the kind of content (static content or not)
*/ 
 function menuLink( $id, $title,$menuselect,$contentType  ) {
	global $database;

	$menu = strval( $menuselect );
	$link = strval( $title );

	$link	= stripslashes( ampReplace($link) );

    //find what kind of link needs to be created in $row->link
    switch ($contentType){
        case "content_section":
            $taskLink = "section";
            break;
        case "content_blog_section":
            $taskLink = "blogsection";
            break;            
        case "content_archive_section":
            $taskLink = "archivesection";
            break;    
        case "content_category":
            $taskLink = "category";
            break;
        case "content_blog_category":
            $taskLink = "blogcategory";
            break;            
        case "content_archive_category":
            $taskLink = "archivecategory";
            break;               
        default:
        $taskLink = "view";
    }

	$row = new mosMenu( $database );
	$row->menutype 		= $menu;
	$row->name 			= $link;
	$row->type 			= $contentType;
	$row->published		= 1;
	$row->componentid	= $id;
	$row->link			= 'index.php?option=com_content&task='.$taskLink.'&id='. $id;
	$row->ordering		= 9999;

	if (!$row->check()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	if (!$row->store()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	
	$row->updateOrder( "menutype = " . $database->Quote( $row->menutype ) . " AND parent = " . (int) $row->parent );

	// clean any existing cache files
	mosCache::cleanCache( 'com_content' );	
}





// new content and menu link
function newMassContent($id,$option) {
	global $database, $mainframe,  $my;
    $contentSection=0;
    $lists=null;
    $sectioncategories=0;
    $row = new mosDBMassContent( $database );
	$row->load( $id );

    $javascript = "onchange=\"changeDynaList( 'catid', sectioncategories, document.adminForm.sectionid.options[document.adminForm.sectionid.selectedIndex].value, 0, 0);\"";
    
    //section
    $query = "SELECT s.id, s.title"
	. "\n FROM #__sections AS s"
	. "\n ORDER BY s.ordering";
	$database->setQuery( $query );

		$sections[] = mosHTML::makeOption( '-1', 'Static Content', 'id', 'title' );
		$sections = array_merge( $sections, $database->loadObjectList() );
		$lists['sectionid'] = mosHTML::selectList( $sections, 'sectionid', 'class="inputbox" size="1" '. $javascript, 'id', 'title' );

    $contentSection = '';
	foreach($sections as $section) {
		$section_list[] = $section->id;
        if ( $section->id == $sectionid ) {
            $contentSection = $section->title;
        }        
        }
   	$sectioncategories 			= array();
	$sectioncategories[-1] 		= array();
	$sectioncategories[-1][] 	= mosHTML::makeOption( '-1', 'Select Category', 'id', 'name' );
	mosArrayToInts( $section_list );
	$section_list 				= 'section=' . implode( ' OR section=', $section_list );

	$query = "SELECT id, name, section"
	. "\n FROM #__categories"
	. "\n WHERE ( $section_list )"
	. "\n ORDER BY ordering"
	;
	$database->setQuery( $query );
	$cat_list = $database->loadObjectList();
	foreach($sections as $section) {
		$sectioncategories[$section->id] = array();
		$rows2 = array();
		foreach($cat_list as $cat) {
			if ($cat->section == $section->id) {
				$rows2[] = $cat;
			}
		}
		foreach($rows2 as $row2) {
			$sectioncategories[$section->id][] = mosHTML::makeOption( $row2->id, $row2->name, 'id', 'name' );
		}
	}

 	// get list of categories
  	if ( !$row->catid && !$row->sectionid ) {
 		$categories[] 		= mosHTML::makeOption( '0', 'Select Category', 'id', 'name' );
 		$lists['catid'] 	= mosHTML::selectList( $categories, 'catid', 'class="inputbox" size="1"', 'id', 'name' );
  	} else {
		$categoriesA = array();
		if ( $sectionid == 0 ) {
			foreach($cat_list as $cat) {
				$categoriesA[] = $cat;
			}
		} else {
			//$where = "\n WHERE section = '$sectionid'";
			foreach($cat_list as $cat) {
				if ($cat->section == $sectionid) {
					$categoriesA[] = $cat;
				}
			}
		}
		$categories[] 		= mosHTML::makeOption( '0', 'Select Category', 'id', 'name' );
		$categories 		= array_merge( $categories, $categoriesA );
 		$lists['catid'] 	= mosHTML::selectList( $categories, 'catid', 'class="inputbox" size="1"', 'id', 'name', intval( $row->catid ) );
  	}

	// build the html select list for ordering
	$query = "SELECT ordering AS value, title AS text"
	. "\n FROM #__content"
	. "\n WHERE catid = " . (int) $row->catid
	. "\n AND state >= 0"
	. "\n ORDER BY ordering"
	;
	$lists['ordering'] = mosAdminMenus::SpecificOrdering( $row, $uid, $query, 1 );     
    // build the html select list for menu selection
	$lists['menuselect']		= mosAdminMenus::MenuSelect( );
    // build the html select list for the group access
	$lists['access'] 			= mosAdminMenus::Access( $row );
	// build list of users
	$lists['created_by'] 		= mosAdminMenus::UserSelect( 'created_by', $my->id );
        
	HTML_Masscontent::newMassContent($row, $option, $contentSection, $lists, $sectioncategories,$menus);
}
 

/**
* Save information for creating new content and link them to menu
*/
function saveMassContent( $option ) {
	global $database;
 
    $menu 	= strval( mosGetParam( $_POST, 'menuselect', '' ) );
    $addMenu 	= strval( mosGetParam( $_POST, 'addMenu', '' ) );
    $msg="";
	$row = new mosDBMassContent( $database );
	if (!$row->bind( $_POST )) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

   //browse each title and insert it if it is not empty
    for ($i=0;$i<10;$i++){
        if ($row->title[$i]!='')
        {    
            $row2  = new mosDBMassContent( $database );
           // $row2 = clone $row; // For PHP5...
           	if (!$row2->bind( $_POST )) {
            echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
            exit();
            }
            $row2->title=$row->title[$i];
            $row2->introtext=$row->introtext[$i];
            // code cleaner for xhtml transitional compliance
            $row2->introtext = str_replace( '<br>', '<br />', $row2->introtext );
            if (!$row2->store()) {
                echo "<script> alert('".$row2->getError()."'); </script>";
                mosRedirect( "index2.php?option=com_masscontent&act=createMassContent");
            exit();
            }
       
            $row2->checkin();
            // clean any existing cache files
            mosCache::cleanCache( 'com_content' );
           
           
            if ($addMenu) {  
                if ($row2->sectionid<=0) //static content
                    $type="content_typed" ;
                else
                    $type="content_item_link" ;
                    menuLink( $row2->id, $row2->title,$menu,$type );
            } 
            $msg = "Contents successfully created";
        }
    }
   mosCache::cleanCache();    
    mosRedirect( "index2.php?option=com_masscontent&act=createMassContent",$msg );
}

/**
*  create new sections
*/
 function newMassSections() {
	global $database, $my, $mainframe;

    $uid=0;
    $scope 		= "content";
    $option 	= "com_masscontent";
    
	$row = new mosDBMassSections( $database );
	// load the row from the db table
	$row->load( (int)$uid );
 
	
    $row->scope 		= $scope;
    $row->published 	= 1;
    $menus 				= array();

 

	// build the html select list for section types
	$types[] = mosHTML::makeOption( '', 'Select Type' );
	$types[] = mosHTML::makeOption( 'content_section', 'Section List' );
	$types[] = mosHTML::makeOption( 'content_blog_section', 'Section Blog' );
	$types[] = mosHTML::makeOption( 'content_archive_section', 'Section Archive Blog' );
	$lists['link_type'] 		= mosHTML::selectList( $types, 'link_type', 'class="inputbox" size="1"', 'value', 'text' );;

 
	// build the html select list for the group access
	$lists['access'] 			= mosAdminMenus::Access( $row );
	// build the html radio buttons for published
	$lists['published'] 		= mosHTML::yesnoRadioList( 'published', 'class="inputbox"', $row->published );
	// build the html select list for menu selection
	$lists['menuselect']		= mosAdminMenus::MenuSelect( );
  
	HTML_Masscontent::newMassSections( $row, $option, $lists, $menus );
}

function saveMassSections( $option ) {
	global $database;

	$menu 		= stripslashes( strval( mosGetParam( $_POST, 'menu', 'mainmenu' ) ) );
	$menuid		= intval( mosGetParam( $_POST, 'menuid', 0 ) );
    $type 	= strval( mosGetParam( $_POST, 'link_type', '' ) );
    $menu 	= strval( mosGetParam( $_POST, 'menuselect', '' ) );
    $addMenu 	= strval( mosGetParam( $_POST, 'addMenu', '' ) );
    
	$row = new mosDBMassSections( $database );
	if (!$row->bind( $_POST )) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
 //browse each title and insert it if it is not empty
    for ($i=0;$i<10;$i++){
        if ($row->title[$i]!='')
        {    
            $row2  = new mosDBMassSections( $database );
            //$row2 = clone $row; //for php5
            if (!$row2->bind( $_POST )) {
                echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
                exit();
            }   
            $row2->title=$row->title[$i];
            $row2->name=$row->name[$i];

            if (!$row2->store()) {
                echo "<script> alert('".$row2->getError()."'); </script>";
                mosRedirect( "index2.php?option=com_masscontent&act=createMassSections");
            exit();
            }
       
            $row2->checkin();
            // clean any existing cache files
            mosCache::cleanCache( 'com_content' );
           
            if ($addMenu) {  
                    menuLink( $row2->id, $row2->title,$menu,$type );
            } 
            $msg = "Sections successfully created";
        }
    }	
      
     mosCache::cleanCache( 'com_content' );
    mosRedirect( "index2.php?option=com_masscontent&act=createMassSections",$msg );
}

 function newMassCategories() {
	global $database, $my, $mainframe;

    $uid=0;
    $scope 		= "content";
    $option 	= "com_masscontent";
    
	$row = new mosDBMassCategories( $database );
	// load the row from the db table
	$row->load( (int)$uid );
	
    $row->scope 		= $scope;
    $row->published 	= 1;
    $menus 				= array();


	// build the html select list for section types
	$types[] = mosHTML::makeOption( '', 'Select Type' );
    $types[] = mosHTML::makeOption( 'content_category', 'Content Category Table' );
    $types[] = mosHTML::makeOption( 'content_blog_category', 'Content Category Blog' );
    $types[] = mosHTML::makeOption( 'content_archive_category', 'Content Category Archive Blog' );
	$lists['link_type'] 		= mosHTML::selectList( $types, 'link_type', 'class="inputbox" size="1"', 'value', 'text' );;

    // build the html select list for sections
	 
		$query = "SELECT s.id AS value, s.title AS text"
		. "\n FROM #__sections AS s"
		. "\n ORDER BY s.ordering"
		;
		$database->setQuery( $query );
		$sections = $database->loadObjectList();
		$lists['section'] = mosHTML::selectList(  $sections, 'section', 'class="inputbox" size="1"', 'value', 'text' );
		//$lists['section'] = '<input type="hidden" name="section" value="'. $row->section .'" />'. $section_name;
	
    
 
	// build the html select list for the group access
	$lists['access'] 			= mosAdminMenus::Access( $row );
	// build the html radio buttons for published
	$lists['published'] 		= mosHTML::yesnoRadioList( 'published', 'class="inputbox"', $row->published );
	// build the html select list for menu selection
	$lists['menuselect']		= mosAdminMenus::MenuSelect( );
  
	HTML_Masscontent::newMassCategories( $row, $option, $lists, $menus );
}


function saveMassCategories( $option ) {
	global $database;

	$menu 		= stripslashes( strval( mosGetParam( $_POST, 'menu', 'mainmenu' ) ) );
	$menuid		= intval( mosGetParam( $_POST, 'menuid', 0 ) );
    $type 	= strval( mosGetParam( $_POST, 'link_type', '' ) );
    $menu 	= strval( mosGetParam( $_POST, 'menuselect', '' ) );
    $addMenu 	= strval( mosGetParam( $_POST, 'addMenu', '' ) );
    
	$row = new mosDBMassCategories( $database );
	if (!$row->bind( $_POST )) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
 //browse each title and insert it if it is not empty
    for ($i=0;$i<10;$i++){
        if ($row->title[$i]!='')
        {    
            $row2  = new mosDBMassCategories( $database );
            //$row2 = clone $row; //for php5
            if (!$row2->bind( $_POST )) {
                echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
                exit();
            }   
            $row2->title=$row->title[$i];
            $row2->name=$row->name[$i];

            if (!$row2->store()) {
                echo "<script> alert('".$row2->getError()."'); </script>";
                mosRedirect( "index2.php?option=com_masscontent&act=createMasscategories");
            exit();
            }
       
            $row2->checkin();
            // clean any existing cache files
            mosCache::cleanCache( 'com_content' );
           
            if ($addMenu) {  
                    menuLink( $row2->id, $row2->title,$menu,$type );
            } 
            $msg = "Categories successfully created";
        }
    }	
      
     mosCache::cleanCache( 'com_content' );
    mosRedirect( "index2.php?option=com_masscontent&act=createMassCategories",$msg );
    
}

function newDeleteMassContent(){
       global $database, $my, $mainframe;
     $option 	= "com_masscontent";   
    $contentSection=0;
    $lists=null;
    $sectioncategories=0;
    $javascript = "onchange=\"changeDynaList( 'catid', sectioncategories, document.adminForm.sectionid.options[document.adminForm.sectionid.selectedIndex].value, 0, 0);\"";
    
    //section
    $query = "SELECT s.id, s.title"
	. "\n FROM #__sections AS s"
	. "\n ORDER BY s.ordering";
	$database->setQuery( $query );

		$sections[] = mosHTML::makeOption( '-1', 'Select section', 'id', 'title' );
		$sections = array_merge( $sections, $database->loadObjectList() );
		$lists['sectionid'] = mosHTML::selectList( $sections, 'sectionid', 'class="inputbox" size="1" '. $javascript, 'id', 'title' );

    $contentSection = '';
	foreach($sections as $section) {
		$section_list[] = $section->id;
        if ( $section->id == $sectionid ) {
            $contentSection = $section->title;
        }        
        }
   	$sectioncategories 			= array();
	$sectioncategories[-1] 		= array();
	$sectioncategories[-1][] 	= mosHTML::makeOption( '-1', 'Select Category', 'id', 'name' );
	mosArrayToInts( $section_list );
	$section_list 				= 'section=' . implode( ' OR section=', $section_list );

	$query = "SELECT id, name, section"
	. "\n FROM #__categories"
	. "\n WHERE ( $section_list )"
	. "\n ORDER BY ordering"
	;
	$database->setQuery( $query );
	$cat_list = $database->loadObjectList();
	foreach($sections as $section) {
		$sectioncategories[$section->id] = array();
		$rows2 = array();
		foreach($cat_list as $cat) {
			if ($cat->section == $section->id) {
				$rows2[] = $cat;
			}
		}
		foreach($rows2 as $row2) {
			$sectioncategories[$section->id][] = mosHTML::makeOption( $row2->id, $row2->name, 'id', 'name' );
		}
	}

 	// get list of categories
  	if ( !$row->catid && !$row->sectionid ) {
 		$categories[] 		= mosHTML::makeOption( '0', 'Select Category', 'id', 'name' );
 		$lists['catid'] 	= mosHTML::selectList( $categories, 'catid', 'class="inputbox" size="1"', 'id', 'name' );
  	} else {
		$categoriesA = array();
		if ( $sectionid == 0 ) {
			foreach($cat_list as $cat) {
				$categoriesA[] = $cat;
			}
		} else {
			//$where = "\n WHERE section = '$sectionid'";
			foreach($cat_list as $cat) {
				if ($cat->section == $sectionid) {
					$categoriesA[] = $cat;
				}
			}
		}
		$categories[] 		= mosHTML::makeOption( '0', 'Select Category', 'id', 'name' );
		$categories 		= array_merge( $categories, $categoriesA );
 		$lists['catid'] 	= mosHTML::selectList( $categories, 'catid', 'class="inputbox" size="1"', 'id', 'name', intval( $row->catid ) );
  	}
        
        
        
        HTML_Masscontent::deleteMassContent($row, $option, $sectioncategories,$lists);
}

function deleteMassContent($option){

    global $database;

    $sectionid = mosGetParam( $_POST, 'sectionid', '' );
    $deleteSection = mosGetParam( $_POST, 'deleteSection', '' );
    $catid = mosGetParam( $_POST, 'catid', '' );
    $deleteCategory = mosGetParam( $_POST, 'deleteCategory', '' );
    $allCat = mosGetParam( $_POST, 'allCat', '' );
    $where="";
    
    //a section has been selected
    if ( $sectionid != "-1" ){  
        
		if ($deleteSection){		
        
			//delete link menu-section		
	        $query = "DELETE m FROM #__menu m "              
	            . "\n WHERE m.componentid IN ( "       
					. "\n SELECT id FROM #__sections "
					. "\n WHERE id=$sectionid "                          
				. "\n )"       
	            . "\n AND ( m.type=\"content_section\" OR m.type=\"content_blog_section\" OR m.type=\"content_archive_section\" )"       
	            ;
		    $database->setQuery( $query );
			$database->query(); 
                      		
			//delete section
            $query = "DELETE FROM #__sections WHERE id=$sectionid";
            $database->setQuery( $query );
			$database->query();
            
        }
		
		if ($catid>0 || $allCat) //a cat is selected
		{
		
	        //when "all cats" is not selected
	        if (!$allCat && $catid>0) $where= "\n AND id=$catid ";
	       
	        if ($deleteCategory) {  		   
				//delete link menu-cat			
				$query = "DELETE m FROM #__menu m "              
	            . "\n WHERE m.componentid IN ( "       
					. "\n SELECT id FROM #__categories ca "
					. "\n WHERE ca.section=$sectionid "                  
					. $where            
				. "\n )"       
	            . "\n AND ( m.type=\"content_category\" OR m.type=\"content_blog_category\" OR m.type=\"content_archive_category\" )"       
	            ;
				$database->setQuery( $query );
				$database->query();      

				//delete cat
	            $query = "DELETE FROM #__categories"
	            . "\n WHERE section=$sectionid"
	            . $where;
	            ;
			    $database->setQuery( $query );
				$database->query();            
	        }
        }
		
         //when "all cats" is not selected
        if (!$allCat && $catid>0) $where= "\n AND catid=$catid ";		                  

        //delete link menu-content
        $query = "DELETE m FROM #__menu m "              
            . "\n WHERE m.componentid IN ( "       
				. "\n SELECT id FROM #__content co "
				. "\n WHERE co.sectionid=$sectionid "                  
				. $where            
			. "\n )"       
            . "\n AND ( m.type=\"content_typed\" OR m.type=\"content_item_link\" )"       
            ;
	    $database->setQuery( $query );
		$database->query();
		
        //delete content 
        $query = "DELETE co FROM #__content co "   
            . "\n WHERE co.sectionid=$sectionid"       
            . $where ;
		$database->setQuery( $query );
		$database->query();

		
    }
    
    mosRedirect( "index2.php?option=com_masscontent&act=deleteMassContent","Content deleted" );
}

?>
