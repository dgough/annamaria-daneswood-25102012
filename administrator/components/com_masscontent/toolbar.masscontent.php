<?php
/*
* MassContent for Joomla 1.0.13
* @version 0.5
* @Date 01/12/2007
* @copyright (C) 2007 Johann Eriksen
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
* Official website: http://www.webtotalservice.no
*/

defined( '_VALID_MOS' ) or die( 'Direct access to this location is not allowed.' );
require_once( $mainframe->getPath( 'toolbar_html' ) );
switch($act) {
    case "cpanel":
    default:
    break;
	case "createMassContent":
	case "createMassSections":
	case "createMassCategories":
		menuMasscontent::MENU_Default();
		break;
	case "deleteMassContent":
		menuMasscontent::MENU_Delete();        
}
?>
