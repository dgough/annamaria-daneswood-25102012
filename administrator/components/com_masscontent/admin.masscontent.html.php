<?php
/*
* MassContent for Joomla 1.0.13
* @version 0.5
* @Date 01/12/2007
* @copyright (C) 2007 Johann Eriksen
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
* Official website: http://www.webtotalservice.no
*/
defined( '_VALID_MOS' ) or die( 'Direct access to this location is not allowed.' );
class HTML_Masscontent {
	
	function newMassContent( &$row, $option,$section, &$lists, &$sectioncategories) {

    ?>

	  <script language="javascript" type="text/javascript">
     
       //copy 1st introtext to all the other introtext
        function copyIntroText(){
            
            if (document.getElementById("duplicateIntroText").checked){                            
                for (i=2;i<11;i++){
                    if (document.getElementById("introtext_"+i).value==""){
                        document.getElementById("introtext_"+i).value= document.getElementById("introtext_1").value;
                    }
                }
            }
            else {                               
                for (i=2;i<11;i++){
                    if (document.getElementById("introtext_"+i).value==document.getElementById("introtext_1").value){
                        document.getElementById("introtext_"+i).value= "";
                    }
                }
            }            
        }  
		function submitbutton(pressbutton) {
			var form = document.adminForm;

             if (!document.getElementById("published").checked){
                document.getElementById("publish_up").value="";
                document.getElementById("state").value=0;
            }
            if ((form.addMenu.checked) && (form.menuselect.value == '')) {
				alert( "Please select a menu." );
            }
            else
                submitform( pressbutton );
		}
	  </script>
      <h1>Mass content</h1>
	  <form action="index2.php?act=createMassContent" method="post" name="adminForm" id="adminForm" class="adminForm">
      
           <script language="javascript" type="text/javascript">   

        
    		var sectioncategories = new Array;
    		<?php
    		$i = 0;
    		foreach ($sectioncategories as $k=>$items) {
    			foreach ($items as $v) {
    				echo "sectioncategories[".$i++."] = new Array( '$k','".addslashes( $v->id )."','".addslashes( $v->name )."' );\t";
    			}
    		}
    		?>

            </script>
        
            <table border="0" cellpadding="3" cellspacing="0" >
            <tr>
                <td>
                <fieldset>      
                    <legend>Create up to 10 pages in a row!</legend> 
                    <table border="0" cellpadding="3" cellspacing="0">

                    <?php for ($i=1;$i<11;$i++) { ?>
                        <tr>
                            <td>Title (page <?php echo $i; ?>): </td>
                            <td><input class="inputbox" type="text" size="50" maxlength="100" name="title[]" value="" /></td>
                        </tr>
                        <tr>
                            <td>Intro text (page <?php echo $i; ?>): </td>
                            <td>
                            <?php editorArea( 'editor1', $row->introtext, "introtext[]", '100%;', '350', '75', '20' ) ; ?>                            
                            </td>
                        </tr>
                    <?php } ?>             
        
                    </table>
                </fieldset>
                </td>        
                <td valign="top">
                <fieldset>
                    <legend>Options</legend>
                    <table border="0" cellpadding="3" cellspacing="0">  
                      <tr>
                            <td>Copy 1st intro text to all empty pages</td>
                            <td><input type="checkbox"  id="duplicateIntroText" name="duplicateIntroText" onClick="javascript:copyIntroText()" ></td>
                        </tr>     
                        <tr>
                            <td colspan="2">PS: Only pages with title will be inserted.</td>
                        </tr>                             
                        <tr>
                            <td>Published</td>
                            <td><input type="checkbox" id="published" name="published" checked></td>
                        </tr>
                        <tr>
                            <td>Access Level ?</td>
                            <td><?php echo $lists['access']; ?>             
                        <tr>
                            <td>Change Creator:</td>
                            <td><?php echo $lists['created_by']; ?></td>
                        </tr>                          
                        <tr>
                            <td>Section: <?php echo $lists['sectionid']; ?></td>
                            <td>Category: <?php echo $lists['catid']; ?></td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" name="addMenu" >Link to menu:</td>
                            <td><?php echo $lists['menuselect']; ?></td>
                        </tr>                         
                    </table>
                </fieldset>
                </td>
            </tr>
            </table>
            <input type="hidden" name="created" value="<?php echo date( 'Y-m-d H:i:s' ); ?>" />	
            <input type="hidden" id="publish_up" name="publish_up" value="<?php echo date( 'Y-m-d H:i:s' ); ?>" />	
            <input type="hidden" name="task" value="" />
            <input type="hidden" id="state" name="state" value="1" />
            <input type="hidden" name="id" value="" />
            <input type="hidden" name="option" value="<?php echo $option; ?>" />           
        </form>
    
  <?php
	}
    
    function newMassSections(&$row, $option, &$lists, &$menus ) {
    ?>

	  <script language="javascript" type="text/javascript">
     
           function copyTitle(){
            
            if (document.getElementById("duplicateTitle").checked){                            
                for (i=1;i<11;i++){
                    if (document.getElementById("name_"+i).value==""){
                        document.getElementById("name_"+i).value= document.getElementById("title_"+i).value;
                    }
                }
            }
            else {                               
                for (i=1;i<11;i++){
                    if (document.getElementById("name_"+i).value==document.getElementById("title_"+i).value){
                        document.getElementById("name_"+i).value= "";
                    }
                }
            }            
        }  

		function submitbutton(pressbutton) {
			var form = document.adminForm;

            if ((form.addMenu.checked) && (form.menuselect.value == '')) {
				alert( "Please select a menu type." );
                return;
             }
            else if ((form.addMenu.checked) && (form.link_type.value == '')) {
				alert( "Please select a menu." );
                return;                
            }
            else{  
                submitform( pressbutton );
            }    
		}
	  </script>
      <h1>Mass Sections</h1>
	  <form action="index2.php?act=createMassSections" method="post" name="adminForm" id="adminForm" class="adminForm">
      
            
        
            <table border="0" cellpadding="3" cellspacing="0" >
            <tr>
                <td>
                <fieldset>      
                    <legend>Create up to 10 sections in a row!</legend> 
                    <table border="0" cellpadding="3" cellspacing="0">

                    <?php for ($i=1;$i<11;$i++) { ?>
                        <tr>
                            <td>Section <?php echo $i; ?>: Title:</td>
                            <td><input class="inputbox" type="text" size="25" maxlength="100" id="title_<?php echo $i; ?>" name="title[]" value="" /></td>
                            <td>Name:</td>
                            <td><input class="inputbox" type="text" size="25" maxlength="100" id="name_<?php echo $i; ?>" name="name[]" value="" /></td>
                       </tr>
                    <?php } ?>             
        
                    </table>
                </fieldset>
                </td>        
                <td valign="top">
                <fieldset>
                    <legend>Options</legend>
                    <table border="0" cellpadding="3" cellspacing="0">
                        <tr>
                            <td>Copy Title to Name</td>
                            <td><input type="checkbox"  id="duplicateTitle" name="duplicateTitle" onClick="javascript:copyTitle()" ></td>
                        </tr>                        
                        <tr>
                            <td>Access Level ?</td>
                            <td><?php echo $lists['access']; ?>             
                        <tr>
        				<tr>
        					<td>Published:</td>
        					<td><?php echo $lists['published']; ?></td>
        				</tr>                      
                        <tr>
                            <td><input type="checkbox" name="addMenu" >Link to menu:</td>
                            <td><?php echo $lists['menuselect']; ?></td>
                        </tr>                         
                        <tr>
                            <td >Select Menu Type:</td>
						<td><?php echo $lists['link_type']; ?></td>
					<tr>
                    </table>
                </fieldset>
                </td>
            </tr>
            </table>
            <input type="hidden" name="task" value="" />
            <input type="hidden" name="id" value="" />
            <input type="hidden" name="option" value="<?php echo $option;?>" />
            <input type="hidden" name="scope" value="<?php echo $row->scope; ?>" />                  
        </form>
    
  <?php
	}
    
        function newMassCategories(&$row, $option, &$lists, &$menus ) {
    ?>

	  <script language="javascript" type="text/javascript">
                function copyTitle(){
            
            if (document.getElementById("duplicateTitle").checked){                            
                for (i=1;i<11;i++){
                    if (document.getElementById("name_"+i).value==""){
                        document.getElementById("name_"+i).value= document.getElementById("title_"+i).value;
                    }
                }
            }
            else {                               
                for (i=1;i<11;i++){
                    if (document.getElementById("name_"+i).value==document.getElementById("title_"+i).value){
                        document.getElementById("name_"+i).value= "";
                    }
                }
            }            
        }  
       
		function submitbutton(pressbutton) {
			var form = document.adminForm;

            if ((form.addMenu.checked) && (form.menuselect.value == '')) {
				alert( "Please select a menu type." );
                return;
             }
            else if ((form.addMenu.checked) && (form.link_type.value == '')) {
				alert( "Please select a menu." );
                return;                
            }
            else{  
                submitform( pressbutton );
            }    
		}
	  </script>
      <h1>Mass Categories</h1>
	  <form action="index2.php?act=createMassCategories" method="post" name="adminForm" id="adminForm" class="adminForm">
      
            
        
            <table border="0" cellpadding="3" cellspacing="0" >
            <tr>
                <td>
                <fieldset>      
                    <legend>Create up to 10 categories in a row!</legend> 
                    <table border="0" cellpadding="3" cellspacing="0">

                    <?php for ($i=1;$i<11;$i++) { ?>
                        <tr>
                            <td>Category <?php echo $i; ?>: Title:</td>
                            <td><input class="inputbox" type="text" size="25" maxlength="100" id="title_<?php echo $i; ?>" name="title[]" value="" /></td>
                            <td>Name:</td>
                            <td><input class="inputbox" type="text" size="25" maxlength="100" id="name_<?php echo $i; ?>" name="name[]" value="" /></td>
                       </tr>
                    <?php } ?>             
        
                    </table>
                </fieldset>
                </td>        
                <td valign="top">
                <fieldset>
                    <legend>Options</legend>
                    <table border="0" cellpadding="3" cellspacing="0">
                        <tr>
                            <td>Copy Title to Name</td>
                            <td><input type="checkbox"  id="duplicateTitle" name="duplicateTitle" onClick="javascript:copyTitle()" ></td>
                        </tr>         				
                        <tr>
        					<td>Section:</td>
        					<td colspan="2"><?php echo $lists['section']; ?></td>
        				</tr>                    
                        <tr>
                            <td>Access Level ?</td>
                            <td><?php echo $lists['access']; ?>             
                        <tr>
        				<tr>
        					<td>Published:</td>
        					<td><?php echo $lists['published']; ?></td>
        				</tr>                      
                        <tr>
                            <td><input type="checkbox" name="addMenu" >Link to menu:</td>
                            <td><?php echo $lists['menuselect']; ?></td>
                        </tr>                         
                        <tr>
                            <td >Select Menu Type:</td>
						<td><?php echo $lists['link_type']; ?></td>
					<tr>
                    </table>
                </fieldset>
                </td>
            </tr>
            </table>
            <input type="hidden" name="task" value="" />
            <input type="hidden" name="id" value="" />
            <input type="hidden" name="option" value="<?php echo $option;?>" />
            <input type="hidden" name="scope" value="<?php echo $row->scope; ?>" />                  
        </form>
    
  <?php
	}
    
    function showCPanel($option) {
        global $mainframe;
        ?>
        <form action="index2.php" method="post" name="adminForm">
 
		<table class="adminheading">
		<tr>
			<th class="cpanel">
			Mass Content
			</th>
        </tr>
        <tr>
        <td width="55%" valign="top">
	    <div id="cpanel">
            <div style="float:left;">
        		<div class="icon">
        			<a href="index2.php?option=com_masscontent&act=createMassContent">
        				<div class="iconimage">
        					<img src="<?php echo $mainframe->getCfg('live_site');?>/administrator/images/note.png" alt="create mass content" align="middle" name="image" border="0" />				</div>
        				Create mass content</a>
        		</div>
    		</div>
    		<div style="float:left;">
        		<div class="icon">
        			<a href="index2.php?option=com_masscontent&act=createMassSections">
        				<div class="iconimage">
        					<img src="<?php echo $mainframe->getCfg('live_site');?>/administrator/images/sections.png" alt="create mass sections" align="middle" name="image" border="0" />				</div>
        				Create mass sections</a>
        		</div>
    		</div>
    		<div style="float:left;">
        		<div class="icon">
        			<a href="index2.php?option=com_masscontent&act=createMassCategories">
        				<div class="iconimage">
        					<img src="<?php echo $mainframe->getCfg('live_site');?>/administrator/images/categories.png" alt="create mass categories" align="middle" name="image" border="0" />				</div>
        				Create mass categories</a>
        		</div>
    		</div>    		
		</div>    		
		</td>
        </tr>
        <tr>
        <td width="55%" valign="top">
		<div id="cpanel">
    		<div style="float:left;">
        		<div class="icon">
        			<a href="index2.php?option=com_masscontent&act=deleteMassContent">
        				<div class="iconimage">
        					<img src="<?php echo $mainframe->getCfg('live_site');?>/administrator/images/delete.png" alt="create mass categories" align="middle" name="image" border="0" />				</div>
        				Delete mass content</a>
        		</div>
    		</div>    		
		</div>        
        </td>
        </tr>
        </table>
            <input type="hidden" name="option" value="<?php echo $option; ?>" />                   		
        </form>
        <?php
    }
    
    function deleteMassContent( &$row, $option, &$sectioncategories,&$lists  ) { 
    ?>
 <script language="javascript" type="text/javascript">
     
    
		function submitbutton(pressbutton) {
			 
			if (pressbutton=='delete'){
				if (confirm('This will definitively delete items, not trash them. \n Are you sure you want to continue ?'))
					submitform( pressbutton );			
				else return;	
			}
			else 	submitform( pressbutton );			
		}
	  </script>
      <h1>Delete mass content</h1>
	  <form action="index2.php?act=deleteMassContent" method="post" name="adminForm" id="adminForm" class="adminForm">
      
           <script language="javascript" type="text/javascript">   

        
    		var sectioncategories = new Array;
    		<?php
    		$i = 0;
    		foreach ($sectioncategories as $k=>$items) {
    			foreach ($items as $v) {
    				echo "sectioncategories[".$i++."] = new Array( '$k','".addslashes( $v->id )."','".addslashes( $v->name )."' );\t";
    			}
    		}
    		?>

            </script>                
                <fieldset>      
                    <legend>Delete sections and categories</legend> 
                    <table border="0" cellpadding="3" cellspacing="0">
                        <tr>
                            <td colspan="3">This will destroy the section, the category, all the contents in that section/category and all the associated menu links.</td>
                        </tr>
                        <tr>                   
                            <td>Section: <?php echo $lists['sectionid']; ?></td>
                            <td><input type="checkbox"  id="deleteSection" name="deleteSection" >Delete section and menu links too</td>  
                            <td></td>
                            <td></td>
                        </tr>            
                        <tr>
                            <td>Category:<?php echo $lists['catid']; ?></td>
                            <td><input type="checkbox"  id="deleteCategory" name="deleteCategory">Delete category and menu links too</td>  
                            <td><input type="checkbox"  id="allCat" name="allCat">Select all categories</td>                           
                        </tr>            
                    </table>
                </fieldset>                
            <!--    <fieldset>      
                    <legend>Delete content</legend> 
                    <table border="0" cellpadding="3" cellspacing="0">
                        <tr>
                        <td></td>
                        </tr>            
                    </table>
                </fieldset>         -->      
            <input type="hidden" name="task" value="" />
            <input type="hidden" name="id" value="" />
            <input type="hidden" name="option" value="<?php echo $option; ?>" />                   
        </form>
    
  <?php
	}
    
}
?>
