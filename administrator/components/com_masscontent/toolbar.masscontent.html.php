<?php
/*
* MassContent for Joomla 1.0.13
* @version 0.5
* @Date 01/12/2007
* @copyright (C) 2007 Johann Eriksen
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
* Official website: http://www.webtotalservice.no
*/
defined( '_VALID_MOS' ) or die( 'Direct access to this location is not allowed.' );
class menuMasscontent {
	function MENU_Default() {
		mosMenuBar::startTable();
		mosMenuBar::save();
        mosMenuBar::divider();
        mosMenuBar::custom ('cpanel','cpanel.png','cpanel.png','CPanel',false);        
		mosMenuBar::spacer();
		mosMenuBar::endTable();  
	}
 	function MENU_Delete() {
		mosMenuBar::startTable();
        mosMenuBar::custom ('delete','delete_f2.png','delete_f2.png','Delete',false);  
        mosMenuBar::divider();
        mosMenuBar::custom ('cpanel','cpanel.png','cpanel.png','CPanel',false);        
		mosMenuBar::spacer();
		mosMenuBar::endTable();  
	}
}
?>

