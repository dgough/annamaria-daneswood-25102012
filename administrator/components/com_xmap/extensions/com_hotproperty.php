<?php

defined('_VALID_MOS') or die('Direct Access to this location is not allowed.');

class xmap_com_hotproperty {


	function getTree( &$xmap, &$parent, $params ) {
		$type=0;
		if ( strpos($parent->link, 'task=viewtype') ) {
			$link_query = parse_url( $parent->link );
			parse_str( html_entity_decode($link_query['query']), $link_vars);
			$type = mosGetParam($link_vars,'id',0);
		}

		$include_properties = mosGetParam($params,'include_properties',1);
		$include_properties = ( $include_properties == 1
				  || ( $include_properties == 2 && $xmap->view == 'xml')
				  || ( $include_properties == 3 && $xmap->view == 'html'));
		$params['include_properties'] = $include_properties;

		$priority = mosGetParam($params,'type_priority',$parent->priority);
		$changefreq = mosGetParam($params,'type_changefreq',$parent->changefreq);
		if ($priority  == '-1')
			$priority = $parent->priority;
		if ($changefreq  == '-1')
			$changefreq = $parent->changefreq;

		$params['type_priority'] = $priority;
		$params['type_changefreq'] = $changefreq;

		$priority = mosGetParam($params,'property_priority',$parent->priority);
		$changefreq = mosGetParam($params,'property_changefreq',$parent->changefreq);
		if ($priority  == '-1')
			$priority = $parent->priority;

		if ($changefreq  == '-1')
			$changefreq = $parent->changefreq;

		$params['property_priority'] = $priority;
		$params['property_changefreq'] = $changefreq;


		global $mosConfig_absolute_path;
		if (file_exists("$mosConfig_absolute_path/administrator/components/com_hotproperty/config.hotproperty.php")) {
			include ("$mosConfig_absolute_path/administrator/components/com_hotproperty/config.hotproperty.php");
			$params['property_order'] = $hp_default_order? $hp_default_order : 'name';
			$params['property_order_mode'] = $hp_default_order2? $hp_default_order2 : 'asc';
		}else{
			$params['property_order'] = 'name';
			$params['property_order_mode'] = 'asc';
		}

		xmap_com_hotproperty::getHotProperty($xmap, $parent, $params,$type);
	}

	function getHotProperty ( &$xmap, &$parent, &$params,$type ) {

		global $database;

		$xmap->changeLevel(1);
		if (!$type) {
			$query = "SELECT id, name FROM #__hp_prop_types WHERE published='1' ORDER BY id";

			$database->setQuery($query);
			$rows = $database->loadObjectList();

			foreach($rows as $row) {
				$node = new stdclass;
		    		$node->name = $row->name;
		    		$node->id = $parent->id;
		    		$node->uid = $parent->uid.'t'.$row->id;
				$node->link = 'index.php?option=com_hotproperty&amp;task=viewtype&amp;id='.$row->id;
				$node->priority = $params['type_priority'];
				$node->changefreq = $params['type_changefreq'];
		    		$xmap->printNode($node);
				xmap_com_hotproperty::getHotProperty($xmap, $parent, $params,$row->id);
	    		}
	    	} else {
			if ($params['include_properties']) {
				$query = "SELECT id, name, created, modified, type FROM #__hp_properties  WHERE  published='1' AND approved='1' and type=$type ORDER BY {$params['property_order']} {$params['property_order_mode']}";
				$database->setQuery($query);
				$rows = $database->loadObjectList();
				foreach($rows as $row) {
					if( $row->modified == "0000-00-00 00:00:00" ) {
						$row->modified = $row->created;
					}
					$node = new stdclass;
					$node->name = $row->name;
					$node->link = 'index.php?option=com_hotproperty&amp;task=view&amp;id='.$row->id;
					$node->id = $parent->id;
		    			$node->uid = $parent->uid.'p'.$row->id;
					$node->modified = xmap_com_hotproperty::_toTimestamp($row->modified);
					$node->priority = $params['property_priority'];
					$node->changefreq = $params['property_changefreq'];
			    		$xmap->printNode($node);
				}
			}
		}
		$xmap->changeLevel(-1);
	}

	/** Translate Joomla datestring to timestamp */
	function _toTimestamp( &$date ) {
		if ( $date && ereg( "([0-9]{4})-([0-9]{2})-([0-9]{2})[ ]([0-9]{2}):([0-9]{2}):([0-9]{2})", $date, $regs ) )
			return mktime( $regs[4], $regs[5], $regs[6], $regs[2], $regs[3], $regs[1] );
		return NULL;
	}
}

