<?php
/**
* @version $Id: admin.menus.html.php 5993 2006-12-13 00:24:58Z friesengeist $
* @package Joomla
* @subpackage Menus
* @copyright Copyright (C) 2005 Open Source Matters. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );

/**
* @package Joomla
* @subpackage Menus
*/
class HTML_menusections {

	function showMenusections( $rows, $pageNav, $search, $levellist, $menutype, $option ) {
		global $my;

		mosCommonHTML::loadOverlib();
		?>
		<script type="text/javascript">
		
			function submitbutton(pressbutton){
				
				if(pressbutton == 'default'){
				
					if (document.adminForm.boxchecked.value > 1){ 
						alert('You can only select 1 menu item as default'); 
					}else if(document.adminForm.boxchecked.value == 0){ 
						alert('Please select an item to set as default'); 
					}else {
						submitform( pressbutton );
						return;
					}
						
				}else{
					submitform( pressbutton );
					return;
				}
			}
		
		</script>
		<form action="index2.php" method="post" name="adminForm">
		<table class="adminheading">
		<tr>
			<th class="menus">
			Menu Manager <small><small>[ <?php echo $menutype;?> ]</small></small>
			</th>
			<td nowrap="nowrap">
			Max Levels
			</td>
			<td>
			<?php echo $levellist;?>
			</td>
			<td>
			Filter:
			</td>
			<td>
			<input type="text" name="search" value="<?php echo htmlspecialchars( $search );?>" class="inputbox" onChange="document.adminForm.submit();" />
			</td>
		</tr>
		<?php
		if ( $menutype == 'mainmenu' ) {
			?>
			<tr>
				<td align="right" nowrap="nowrap" style="color: red; font-weight: normal;" colspan="5">
				<?php echo _MAINMENU_DEL; ?>
				</td>
			</tr>
			<?php
		}
		?>
		</table>

		<table class="adminlist">
		<tr>
			<th width="20">
			#
			</th>
			<th width="20">
			<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($rows); ?>);" />
			</th>
			<th class="title" width="35%">
			Menu Item
			</th>
			<th width="5%">
			Default
			</th>
			<th width="5%">
			Published
			</th>
			<th colspan="2" width="5%">
			Reorder
			</th>
			<th width="2%">
			Order
			</th>
			<th width="1%">
			<a href="javascript: saveorder( <?php echo count( $rows )-1; ?> )"><img src="images/filesave.png" border="0" width="16" height="16" alt="Save Order" /></a>
			</th>
			<th width="10%">
			Access
			</th>
			<th>
			Itemid
			</th>
			<th width="35%" align="left">
			Type
			</th>
			<th>
			CID
			</th>
		</tr>
		<?php
		$k = 0;
		$i = 0;
		$n = count( $rows );
		foreach ($rows as $row) {
			mosMakeHtmlSafe( $row, ENT_QUOTES, 'treename' );
			$access 	= mosCommonHTML::AccessProcessing( $row, $i );
			$checked 	= mosCommonHTML::CheckedOutProcessing( $row, $i );
			$published 	= mosCommonHTML::PublishedProcessing( $row, $i );
			?>
			<tr class="<?php echo "row$k"; ?>">
				<td>
				<?php echo $i + 1 + $pageNav->limitstart;?>
				</td>
				<td>
				<?php echo $checked; ?>
				</td>
				<td nowrap="nowrap">
				<?php
				if ( $row->checked_out && ( $row->checked_out != $my->id ) ) {
					echo $row->treename;
				} else {
					$link = 'index2.php?option=com_menus&menutype='. $row->menutype .'&task=edit&id='. $row->id . '&hidemainmenu=1';
					?>
					<a href="<?php echo $link; ?>">
					<?php echo $row->treename; ?>
					</a>
					<?php
				}
				?>
				</td>
				<td width="5%" align="center">
				<?php if($row->default){ ?>
					<img src='images/default_g.png' alt='Default' />
				<?php } ?>
				</td>
				<td width="10%" align="center">
				<?php echo $published;?>
				</td>
				<td>
				<?php echo $pageNav->orderUpIcon( $i ); ?>
				</td>
				<td>
				<?php echo $pageNav->orderDownIcon( $i, $n ); ?>
				</td>
				<td align="center" colspan="2">
				<input type="text" name="order[]" size="5" value="<?php echo $row->ordering; ?>" class="text_area" style="text-align: center" />
				</td>
				<td align="center">
				<?php echo $access;?>
				</td>
				<td align="center">
				<?php echo $row->id; ?>
				</td>
				<td align="left">
					<span class="editlinktip">
						<?php
						echo mosToolTip( $row->descrip, '', 280, 'tooltip.png', $row->type, $row->edit );
						?>
					</span>
				</td>
				<td align="center">
				<?php echo $row->componentid; ?>
				</td>
			</tr>
			<?php
			$k = 1 - $k;
			$i++;
		}
		?>
		</table>

		<?php echo $pageNav->getListFooter(); ?>

		<input type="hidden" name="option" value="<?php echo $option; ?>" />
		<input type="hidden" name="menutype" value="<?php echo $menutype; ?>" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="hidemainmenu" value="0" />
		<input type="hidden" name="<?php echo josSpoofValue(); ?>" value="1" />
		</form>
		<?php
	}
	
	function createDtreeItem($id, $pid, $menu, $menutype, $url = '', $open = '', $target = '', $icon = '', $iconOpen = ''){
			
		
		if(!is_object($menu)){
			$name = $menu;
			$menu = new stdClass();
			$menu->name = $name;
			$menu->descrip = 'Click to open '.$name;
			$url = 0;
		}
		
		if($url){
			$url = "index2.php?option=com_menus&menutype=$menutype&task=edit&type=$menu->type&hidemainmenu=1";		
			if($menu->type == 'components') $url .= "&componentid=$menu->id";
			$mid = mosGetParam($_REQUEST, 'id', '');
			if($mid) $url .= "&id=$mid&action=reedit";
		}else{
			$url = '';
		}
		
		return "d.add($id, $pid, '$menu->name', '$url', '$menu->descrip', '$target', '$icon', '$iconOpen', '$open');\n";
		
	}
	
	/**
	* Displays a selection list for menu item types
	*/
	function addMenuItem( &$cid, $menutype, $option, $types_content, $types_component, $types_link, $types_other, $types_submit, $menu ) {

		mosCommonHTML::loadOverlib();
		?>
		<style type="text/css">
		fieldset {
			border: 1px solid #777;
		}
		legend {
			font-weight: bold;
		}
		</style>

		<form action="index2.php" method="post" name="adminForm">
		<table class="adminheading">
		<tr>
			<th class="menus">
			New Menu Item
			</th>
		</tr>
		</table>
		
<fieldset><legend>Select Menu Type</legend>
<div class="dtree">
	<link rel="StyleSheet" href="components/com_menus/dtree/dtree.css" type="text/css" />
	<script type="text/javascript" src="components/com_menus/dtree/dtree.js"></script>
	<div class="expand_contract"><a href="javascript: setExpand();">Expand all</a> | <a href="javascript: setContract();">Contract all</a></div>
	<script type="text/javascript">
		//<!--
		d = new dTree('d');
		d.add(0,-1,'Select Menu Type'); 	
		<?php	 

		//////////// Adds section links //////////////
		//add(id, pid, name, url, title, target, icon, iconOpen, open)
		$totalCount = 0;		
		
		$space = ' - ';
		foreach($menu as $key => $value){
			$totalCount++;
			
			if(is_array($value)){
				
				if($key == 'Internal') echo HTML_menusections::createDtreeItem($totalCount, 0, $key, $menutype, 0, 1);
				else echo HTML_menusections::createDtreeItem($totalCount, 0, $key, $menutype, 0);
				
				$node1 = $totalCount;
				
				foreach($value as $k1 => $v1){
					$totalCount++;	
					
					if(is_array($v1)){
						
						echo HTML_menusections::createDtreeItem($totalCount, $node1, $k1, $menutype, 0);					
						$node2 = $totalCount;
						
						foreach($v1 as $k2 => $v2){
							$totalCount++;
							
							if(is_array($v2)){
								
								echo HTML_menusections::createDtreeItem($totalCount, $node2, $k2, $menutype, 0);
								$node3 = $totalCount;
								
								foreach($v2 as $k3 => $v3){
									$totalCount++;
									
									if(is_array($v3)){
										
									}else{
										echo HTML_menusections::createDtreeItem($totalCount, $node3, $v3, $menutype, 1);
									}		
								}
							}else{
								echo HTML_menusections::createDtreeItem($totalCount, $node2, $v2, $menutype, 1);
							}	
						}
					}else{
						
						echo HTML_menusections::createDtreeItem($totalCount, $node1, $v1, $menutype, 1);
					}
				}		
			}else{
				echo HTML_menusections::createDtreeItem($totalCount, 0, $value, $menutype, 1);
			}	
		}
	
		if (isset($_COOKIE["MenuExpand"])){
			if ($_COOKIE["MenuExpand"]==1){
				echo "d.openAll();";
			}else if($_COOKIE["MenuExpand"]==0){
				echo "d.closeAll();";
			}
		}
		else{
		echo "d.openAll();";
		}
	?>
	document.write(d);
	//-->
	</script>
	</div>	
</fieldset>			
		<input type="hidden" name="option" value="<?php echo $option; ?>" />
		<input type="hidden" name="menutype" value="<?php echo $menutype; ?>" />
		<input type="hidden" name="task" value="edit" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="hidemainmenu" value="0" />
		<input type="hidden" name="<?php echo josSpoofValue(); ?>" value="1" />
		</form>
		<?php
		
	}

	function htmlOptions( &$row, $link, $k, $i ) {
		?>
		<tr class="<?php echo "row$k"; ?>">
			<td width="20">
			</td>
			<td style="height: 30px;">
				<span class="editlinktip" style="cursor: pointer;">
						<?php
						echo mosToolTip( $row->descrip, $row->name, 250, '', $row->name, $link, 1 );
						?>
				</span>
			</td>
			<td width="20">
				<input type="radio" id="cb<?php echo $i;?>" name="type" value="<?php echo $row->type; ?>" onClick="isChecked(this.checked);" />
			</td>
			<td width="20">
			</td>
		</tr>
		<?php
	}

	/**
	* Form to select Menu to move menu item(s) to
	*/
	function moveMenu( $option, $cid, $MenuList, $items, $menutype  ) {
		?>
		<form action="index2.php" method="post" name="adminForm">
		<br />
		<table class="adminheading">
		<tr>
			<th>
			Move Menu Items
			</th>
		</tr>
		</table>

		<br />
		<table class="adminform">
		<tr>
			<td width="3%"></td>
			<td align="left" valign="top" width="30%">
			<strong>Move to Menu:</strong>
			<br />
			<?php echo $MenuList ?>
			<br /><br />
			</td>
			<td align="left" valign="top">
			<strong>
			Menu Items being moved:
			</strong>
			<br />
			<ol>
			<?php
			foreach ( $items as $item ) {
				?>
				<li>
				<?php echo $item->name; ?>
				</li>
				<?php
			}
			?>
			</ol>
			</td>
		</tr>
		</table>
		<br /><br />

		<input type="hidden" name="option" value="<?php echo $option;?>" />
		<input type="hidden" name="boxchecked" value="1" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="menutype" value="<?php echo $menutype; ?>" />
		<?php
		foreach ( $cid as $id ) {
			echo "\n <input type=\"hidden\" name=\"cid[]\" value=\"$id\" />";
		}
		?>
		<input type="hidden" name="<?php echo josSpoofValue(); ?>" value="1" />
		</form>
		<?php
	}


	/**
	* Form to select Menu to copy menu item(s) to
	*/
	function copyMenu( $option, $cid, $MenuList, $items, $menutype  ) {
		?>
		<form action="index2.php" method="post" name="adminForm">
		<br />
		<table class="adminheading">
		<tr>
			<th>
			Copy Menu Items
			</th>
		</tr>
		</table>

		<br />
		<table class="adminform">
		<tr>
			<td width="3%"></td>
			<td align="left" valign="top" width="30%">
			<strong>
			Copy to Menu:
			</strong>
			<br />
			<?php echo $MenuList ?>
			<br /><br />
			</td>
			<td align="left" valign="top">
			<strong>
			Menu Items being copied:
			</strong>
			<br />
			<ol>
			<?php
			foreach ( $items as $item ) {
				?>
				<li>
				<?php echo $item->name; ?>
				</li>
				<?php
			}
			?>
			</ol>
			</td>
		</tr>
		</table>
		<br /><br />

		<input type="hidden" name="option" value="<?php echo $option;?>" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="menutype" value="<?php echo $menutype; ?>" />
		<?php
		foreach ( $cid as $id ) {
			echo "\n <input type=\"hidden\" name=\"cid[]\" value=\"$id\" />";
		}
		?>
		<input type="hidden" name="<?php echo josSpoofValue(); ?>" value="1" />
		</form>
		<?php
	}
}
?>