<?php
/**
* @version $Id: alias.class.php 5045 2006-09-14 13:49:01Z friesengeist $
* @package Joomla
* @subpackage Menus
* @copyright Copyright (C) 2005 Open Source Matters. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );

/**
* Component item link class
* @package Joomla
* @subpackage Menus
*/
class alias_menu {

	function edit( &$uid, $menutype, $option ) {
		global $database, $my, $mainframe, $type;

		$menu = new mosMenu( $database );
		$menu->load( (int)$uid );
		$type = mosGetParam($_GET, 'type', '');
		$menu->type = $type ? $type : $menu->type;

		// fail if checked out not by 'me'
		if ($menu->checked_out && $menu->checked_out != $my->id) {
			mosErrorAlert( "The module ".$menu->title." is currently being edited by another administrator" );
		}

		if ( $uid ) {
			$menu->checkout( $my->id );
		} else {
			// load values for new entry
			$menu->type 		= 'component_item_link';
			$menu->menutype 	= $menutype;
			$menu->browserNav 	= 0;
			$menu->ordering 	= 9999;
			$menu->parent 		= intval( mosGetParam( $_POST, 'parent', 0 ) );
			$menu->published 	= 1;
		}

		$query = "SELECT * "
				. "\n FROM #__menu AS a"
				. "\n WHERE a.published = 1"
				. "\n AND a.id != '$menu->id'"
				. "\n ORDER BY a.menutype, a.name DESC"
				;
				$database->setQuery( $query );
				$components = $database->loadObjectList( );
				
		$links = array();
		foreach($components as &$mitem){
			$menuTemp = new mosMenu( $database );
			$menuTemp->load( (int)$mitem->id );
		
			switch ($mitem->type) {
				case 'separator':
				case 'component_item_link':
					continue;
					break;
					
				case 'url':
					if ( eregi( 'index.php\?', $mitem->link ) && !eregi( 'http', $mitem->link ) && !eregi( 'https', $mitem->link ) ) {
						if ( !eregi( 'Itemid=', $mitem->link ) ) {
							$mitem->link .= '&Itemid='. $mitem->id;
						}
					}
					break;
					
				case 'content_item_link':
				case 'content_typed':
					// load menu params
					$menuparams = new mosParameters( $mitem->params, $mainframe->getPath( 'menu_xml', $mitem->type ), 'menu' );
					
					$unique_itemid = $menuparams->get( 'unique_itemid', 1 );
					
					if ( $unique_itemid ) {
						$mitem->link .= '&Itemid='. $mitem->id;
					} else {
						$mitem->link = mosAdminMenus::Link( $menuTemp, $mitem->id );
					}					
					break;
	
				default:
					$mitem->link .= '&Itemid='. $mitem->id;
					break;
			}
			
			if(!in_array($mitem->type, array('component_item_link','separator')) && $mitem->type ) $links[] = mosHTML::makeOption('?Itemid='.$mitem->id, $mitem->menutype.' - '.$mitem->name);
		}

		//	Create a list of links
		$lists['components'] = mosHTML::selectList( $links, 'link', 'class="inputbox" size="10"', 'value', 'text', $menu->link );


		// build html select list for target window
		$lists['target'] 		= mosAdminMenus::Target( $menu );

		// build the html select list for ordering
		$lists['ordering'] 		= mosAdminMenus::Ordering( $menu, $uid );
		// build the html select list for the group access
		$lists['access'] 		= mosAdminMenus::Access( $menu );
		// build the html select list for paraent item
		$lists['parent'] 		= mosAdminMenus::Parent( $menu );
		// build published button option
		$lists['published'] 	= mosAdminMenus::Published( $menu );
		// build the url link output
		$lists['link'] 		= mosAdminMenus::Link( $menu, $uid, 1 );

		// get params definitions
		$params = new mosParameters( ($type ? NULL : $menu->params), $mainframe->getPath( 'menu_xml', $menu->type ), 'menu' );

		alias_menu_html::edit( $menu, $lists, $params, $option );
	}
}
?>