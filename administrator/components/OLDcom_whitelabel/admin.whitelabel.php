<?php
/**
* @version $Id: admin.WhiteLabels.php 5023 2006-09-13 12:36:21Z friesengeist $
* @package Joomla
* @subpackage WhiteLabels
* @copyright Copyright (C) 2005 Open Source Matters. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );

// ensure user has access to this function
if (!($acl->acl_check( 'administration', 'edit', 'users', $my->usertype, 'components', 'all' )
		| $acl->acl_check( 'administration', 'edit', 'users', $my->usertype, 'components', 'com_whitelabel' ))) {
	mosRedirect( 'index2.php', _NOT_AUTH );
}

require_once( $mainframe->getPath( 'admin_html' ) );
require_once( "$mosConfig_absolute_path/administrator/components/$option/whitelabel.class.php" );

$cid = josGetArrayInts( 'cid' );

switch ($task) {
	case 'new':
		editWhitelabel( $option, 0 );
		break;

	case 'edit':
		editWhitelabel( $option, $cid[0] );
		break;

	case 'editA':
		editWhitelabel( $option, $id );
		break;

	case 'save':
	case 'apply':
		saveWhitelabel( $option, $task );
		break;

	case 'remove':
		removeWhiteLabels( $cid, $option );
		break;

	case 'publish':
		publishWhiteLabels( $cid, 1, $option );
		break;

	case 'unpublish':
		publishWhiteLabels( $cid, 0, $option );
		break;

	case 'approve':
		break;

	case 'cancel':
		cancelWhitelabel( $option );
		break;

	case 'orderup':
		orderWhiteLabels( intval( $cid[0] ), -1, $option );
		break;

	case 'orderdown':
		orderWhiteLabels( intval( $cid[0] ), 1, $option );
		break;

	default:
		showWhiteLabels( $option );
		break;
}

/**
* Compiles a list of records
* @param database A database connector object
*/
function showWhiteLabels( $option ) {
	global $database, $mainframe, $mosConfig_list_limit;

	$limit 		= intval( $mainframe->getUserStateFromRequest( "viewlistlimit", 'limit', $mosConfig_list_limit ) );
	$limitstart = intval( $mainframe->getUserStateFromRequest( "view{$option}limitstart", 'limitstart', 0 ) );
	$search 	= $mainframe->getUserStateFromRequest( "search{$option}", 'search', '' );
	if (get_magic_quotes_gpc()) {
		$search	= stripslashes( $search );
	}

	$where = array();

	if ($search) {
		$where[] = "LOWER(domain) LIKE '%" . $database->getEscaped( trim( strtolower( $search ) ) ) . "%'";
	}

	// get the total number of records
	$query = "SELECT COUNT(*)"
	. "\n FROM #__whitelabel "
	. (count( $where ) ? "\n WHERE " . implode( ' AND ', $where ) : "")
	;
	$database->setQuery( $query );
	$total = $database->loadResult();

	require_once( $GLOBALS['mosConfig_absolute_path'] . '/administrator/includes/pageNavigation.php' );
	$pageNav = new mosPageNav( $total, $limitstart, $limit  );

	$query = "SELECT *"
	. "\n FROM #__whitelabel"
	. ( count( $where ) ? "\n WHERE " . implode( ' AND ', $where ) : "")
	. "\n ORDER BY domain"
	;
	$database->setQuery( $query, $pageNav->limitstart, $pageNav->limit );

	$rows = $database->loadObjectList();
	if ($database->getErrorNum()) {
		echo $database->stderr();
		return false;
	}

	HTML_WhiteLabels::showWhiteLabels( $option, $rows, $search, $pageNav );
}

/**
* Compiles information to add or edit
* @param integer The unique id of the record to edit (0 if new)
*/
function editWhitelabel( $option, $id ) {
	global $database, $my, $mosConfig_absolute_path;

	$lists = array();

	$row = new mosWhitelabel( $database );
	
	// load the row from the db table
	$row->load( (int)$id );

	// fail if checked out not by 'me'
	if ($row->isCheckedOut( $my->id )) {
		mosRedirect( 'index2.php?option='. $option, "The whitelabel $row->title is currently being edited by another administrator." );
	}

	if ($id) {
		$row->checkout( $my->id );
	} else {
		// initialise new record
		$row->published = 1;
	}
	
	//Get templates	
	$templateBaseDir = mosPathName( $mosConfig_absolute_path . '/templates' );

	$rows = array();
	// Read the template dir to find templates
	$templateDirs		= mosReadDirectory($templateBaseDir);

	$options = array();
	// Check that the directory contains an xml file
	foreach($templateDirs as $templateDir) {
		$dirName = mosPathName($templateBaseDir . $templateDir);
		$xmlFilesInDir = mosReadDirectory($dirName,'.xml$');

		if(is_dir($dirName) && count($xmlFilesInDir) > 0){
			$options[] = mosHTML::makeOption($templateDir);
		}
	}
	
	// build the html select list
	$lists['templates'] 		= mosHTML::selectList($options,'template','','value','text',$row->template);
	
	// build the html select list
	$lists['published'] 		= mosHTML::yesnoRadioList( 'published', 'class="inputbox"', $row->published );
	
	// build list of menu items
	$selected = ($row->home_menu ? $row->home_menu : 99999999);
	$lists['menus'] 		= mosAdminMenus::MenuLinks( $selected, 0, 0 );
	$lists['menus'] 		= str_replace('multiple="multiple"','',$lists['menus']);
	$lists['menus'] 		= str_replace('name="selections[]"','name="home_menu"',$lists['menus']);
	
	//Set the params
	$file 	= $mosConfig_absolute_path .'/administrator/components/com_whitelabel/whitelabel_params.xml';
	$params = new mosParameters( $row->params, $file, 'component' );
	
	//Set the global params
	$file 	= $mosConfig_absolute_path .'/administrator/components/com_whitelabel/whitelabel_params_mosconfig.xml';
	$params_mosconfig = new mosParameters( $row->params_mosconfig, $file, 'component' );

	HTML_WhiteLabels::editWhitelabel( $row, $lists, $params, $params_mosconfig, $option );
}

/**
* Saves the record on an edit form submit
* @param database A database connector object
*/
function saveWhitelabel( $option, $task ) {
	global $database, $my;

	$row = new mosWhitelabel( $database );
	if (!$row->bind( $_POST )) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	// save params
	$params = mosGetParam( $_POST, 'params', '' );
	if (is_array( $params )) {
		$txt = array();
		foreach ( $params as $k=>$v) {
			$txt[] = "$k=$v";
		}
		$row->params = implode( "\n", $txt );
	}
	
	// save params
	$params_mosconfig = mosGetParam( $_POST, 'params_mosconfig', '' );
	if (is_array( $params_mosconfig )) {
		$txt = array();
		foreach ( $params_mosconfig as $k=>$v) {
			$txt[] = "$k=$v";
		}
		$row->params_mosconfig = implode( "\n", $txt );
	}

	if (!$row->check()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	if (!$row->store()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	$row->checkin();
	$row->updateOrder( "catid = " . (int) $row->catid );

	if($task == 'save'){
		mosRedirect( "index2.php?option=$option" );
	}else{
		mosRedirect( "index2.php?option=$option&task=editA&id=$row->id" );
	}
}

/**
* Deletes one or more records
* @param array An array of unique category id numbers
* @param string The current url option
*/
function removeWhiteLabels( $cid, $option ) {
	global $database;

	if (!is_array( $cid ) || count( $cid ) < 1) {
		echo "<script> alert('Select an item to delete'); window.history.go(-1);</script>\n";
		exit;
	}
	if (count( $cid )) {
		mosArrayToInts( $cid );
		$cids = 'id=' . implode( ' OR id=', $cid );
		$query = "DELETE FROM #__whitelabel"
		. "\n WHERE ( $cids )"
		;
		$database->setQuery( $query );
		if (!$database->query()) {
			echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
		}
	}

	mosRedirect( "index2.php?option=$option" );
}

/**
* Publishes or Unpublishes one or more records
* @param array An array of unique category id numbers
* @param integer 0 if unpublishing, 1 if publishing
* @param string The current url option
*/
function publishWhiteLabels( $cid=null, $publish=1,  $option ) {
	global $database, $my;

	if (!is_array( $cid ) || count( $cid ) < 1) {
		$action = $publish ? 'publish' : 'unpublish';
		echo "<script> alert('Select an item to $action'); window.history.go(-1);</script>\n";
		exit;
	}

	mosArrayToInts( $cid );
	$cids = 'id=' . implode( ' OR id=', $cid );

	$query = "UPDATE #__whitelabel"
	. "\n SET published = " . (int) $publish
	. "\n WHERE ( $cids )"
	. "\n AND ( checked_out = 0 OR ( checked_out = " . (int) $my->id . " ) )"
	;
	$database->setQuery( $query );
	if (!$database->query()) {
		echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
		exit();
	}

	if (count( $cid ) == 1) {
		$row = new mosWhitelabel( $database );
		$row->checkin( $cid[0] );
	}
	mosRedirect( "index2.php?option=$option" );
}
/**
* Moves the order of a record
* @param integer The increment to reorder by
*/
function orderWhiteLabels( $uid, $inc, $option ) {
	global $database;
	$row = new mosWhitelabel( $database );
	$row->load( (int)$uid );
	$row->updateOrder();
	$row->move( $inc, "published >= 0" );
	$row->updateOrder();

	mosRedirect( "index2.php?option=$option" );
}

/**
* Cancels an edit operation
* @param string The current url option
*/
function cancelWhitelabel( $option ) {
	global $database;
	$row = new mosWhitelabel( $database );
	$row->bind( $_POST );
	$row->checkin();
	mosRedirect( "index2.php?option=$option" );
}
?>