<?php
/**
* @version $Id: WhiteLabels.class.php 5072 2006-09-15 22:56:27Z friesengeist $
* @package Joomla
* @subpackage WhiteLabels
* @copyright Copyright (C) 2005 Open Source Matters. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );

/**
* Category database table class
* @package Joomla
* @subpackage WhiteLabels
*/
class mosWhitelabel extends mosDBTable {
	/** @var int Primary key */
	var $id					= null;
	/** @var text */
	var $domain				= null;
	/** @var text */
	var $sitename			= null;
	/** @var string */
	var $template			= null;
	/** @var string */
	var $published			= null;
	/** @var boolean */
	var $checked_out		= null;
	/** @var time */
	var $checked_out_time	= null;
	/** @var string */
	var $params				= null;
	/** @var string */
	var $params_mosconfig		= null;
	/** @var int */
	var $home_menu			= null;
	/** @var string */
	var $home_url			= null;
	/** @var string */
	var $jos_change_tempalte= null;
	
	var $_modules = array();

	/**
	* @param database A database connector object
	*/
	function mosWhitelabel( &$db ) {
		$this->mosDBTable( '#__whitelabel', 'id', $db );
	}
	/** overloaded check function */
	function check() {
		// filter malicious code
		$ignoreList = array( 'params' );
		$this->filter( $ignoreList );
		
		/* If home_url is set, unset home_menu */
		if($this->home_url != null){
			$this->home_menu = '';
		}
		
		//Remove http
		$this->domain = preg_replace('/http[s|]?:\/\//i','',$this->domain);

		/** check for valid name */
		if (trim( $this->domain ) == '') {
			$this->_error = _Whitelabel_TITLE;
			return false;
		}

		/** check for existing name */
		$query = "SELECT id"
		. "\n FROM #__whitelabel "
		. "\n WHERE domain = " . $this->_db->Quote( $this->domain )
		;
		$this->_db->setQuery( $query );

		$xid = intval( $this->_db->loadResult() );
		if ($xid && $xid != intval( $this->id )) {
			$this->_error = _Whitelabel_EXIST;
			return false;
		}
		return true;
	}
	
	function init(){
		
		global $mosConfig_live_site, $mosConfig_absolute_path;
	
		//Set the reset flag
		$reset = 0;
			
		//Get some vars to be used later	
		$tempalte_request = mosGetParam($_REQUEST,'jos_change_template',null);
		$tempalte_cookie = mosGetParam($_COOKIE,'jos_user_template',null);
		$whitelabel_cookie = mosGetParam($_COOKIE,'jos_whitelabel_template_change',null);
		
		//Get the domain ID from DB
		$domain = $this->getDomain();
		
		$this->params_mosconfig = new mosParameters('');
		$this->params = new mosParameters('');

		if($domain->id){
			
			//Populate $this
			$this->load( (int) $domain->id);

			if($this->published){
				//Set the request array
				$this->setRequest();
				
				//Set the live site url
				$mosConfig_live_site = mosGetParam($_SERVER,'SERVER_PORT',80) == 443 ? 'https://'.$domain->domain : 'http://'.$domain->domain;
				
				//Set the params		
				$file 	= $mosConfig_absolute_path .'/administrator/components/com_whitelabel/whitelabel_params.xml';
				$this->params = new mosParameters( $this->params, $file, 'component' );

				//Set the global params
				$file 	= $mosConfig_absolute_path .'/administrator/components/com_whitelabel/whitelabel_params_mosconfig.xml';
				$this->params_mosconfig = new mosParameters( $this->params_mosconfig, $file, 'component' );
				//Set the modConfig global params
				foreach($this->params_mosconfig->toArray() as $k => $v){
					if($v != -1 && $v != ''){
						$GLOBALS[$k] = $v;
					}
				}
				
				//Set the global params
				$this->params_mosconfig = new mosParameters($this->params_mosconfig);
				
				
				//Check if we are setting jos_change_tempalte, if so, set the cookie, otherwise remove
				if($tempalte_request != ''){
					setcookie( 'jos_whitelabel_template_change', "$tempalte_request", time()+600, '/');
				}else{
					unset($_COOKIE['jos_whitelabel_template_change']);
					setcookie( 'jos_whitelabel_template_change', '', time()-3600);
				}
			}else{
				$reset = 1;
				$this->clean();
			}
		}else{			
			$reset = 1;
			$this->clean();
		}		
	}
	
	function clean(){
		
		$wl_tp = mosGetParam($_COOKIE,'jos_whitelabel_template',null);
		$wl_tp_change = mosGetParam($_COOKIE,'jos_whitelabel_template_change',null);
		$us_tp = mosGetParam($_COOKIE,'jos_user_template',null);
		if(($wl_tp == $this->template || $wl_tp = $us_tp) && !$wl_tp_change){
			//Reset templates etc
			unset($_REQUEST['jos_user_template']);
			unset($_COOKIE['jos_user_template']);
			unset($_REQUEST['jos_whitelabel_template']);
			unset($_COOKIE['jos_whitelabel_template']);
			setcookie( 'jos_user_template', '', time()-3600 );
			setcookie( 'jos_whitelabel_template', '', time()-3600);
		}		
	}
	
	function setRequest(){
		
		$request = array();
		
		//Set the template variable to change tempaltes
		if($this->template){
			$jos_change_template = mosGetParam($_REQUEST,'jos_change_template',mosGetParam($_COOKIE,'jos_user_template',''));
			$template = ($jos_change_template != '' && mosGetParam($_COOKIE,'jos_whitelabel_template','') == $this->template) ? $jos_change_template : $this->template;
			$request['jos_change_template']= $template;
			setcookie('jos_whitelabel_template', $this->template, time()+600, '/');
		}
		
		//Check which URl we are using as the homepage
		if($this->home_url){
			$link = $this->home_url;
		}else{		
			$this->_db->setQuery("SELECT link FROM #__menu WHERE id = '$this->home_menu'");
			$link = $this->_db->loadResult();
			$link = str_replace('&amp;','&',$link);
			$sep = strstr($link,'?') ? '&' : '?';
			$link = strstr($link,'Itemid=') ? $link : ($link . $sep. "Itemid=$this->home_menu");
		}

		
		//First we need to check if there is request uri supplied thats also a sef url
		$request_uri = mosGetParam($_SERVER,'REQUEST_URI');
		if($request_uri != '/'){
			$request_uri = substr($request_uri,0,1) == '/' ? substr($request_uri,1) : $request_uri;
			$request_uri_parts = explode('?',$request_uri);
			
			$this->_db->setQuery("SELECT newurl FROM #__redirection WHERE oldurl = '$request_uri' OR oldurl = '".$request_uri_parts[0]."'");
			parse_str(str_replace('index.php?','',$this->_db->loadResult()),$sef_request);
			$_REQUEST = array_merge($_REQUEST,$sef_request);
		}
		
		//Check that the link is set and starts index.php and that option isnt set
		//Set the homepage
		if($link && eregi('^index.php',$link) && !mosGetParam($_REQUEST,'option') && !mosGetParam($_REQUEST,'Itemid')){		
			$link = str_replace('index.php?','', $link);
			//Split into name/value pairs
			parse_str($link, $link_vars);
			$request = array_merge($request, $link_vars);			
		}
				
		//Set the request array
		$_REQUEST = array_merge($_REQUEST, $request);
	}
	
	function getDomain(){
				
		$host = mosGetParam($_SERVER,'HTTP_HOST','');
		$no_www = preg_replace('/www\./i','',$host);
		$domain = null;
		if($host){
			$this->_db->setQuery("SELECT id, domain FROM #__whitelabel WHERE domain = '$host' OR domain = '$no_www' OR domain = 'www.$host' LIMIT 1");
			$this->_db->loadObject($domain);
		}		
		return $domain;
	}
	
	function getModuleName($module, $fallback = true){
		
		$prefix = trim($this->params->get('unique_id'));
		$fallback = $fallback && $prefix;
		$name = $prefix ? $prefix .'_'. $module : $module;
		
		//Check if we've checked this module before
		if(isset($this->_modules[$name])) return $this->_modules[$name]->name;
		
		//Get the module count
		$count = mosCountModules($name);
		if($count > 0){
			$this->_modules[$name] = (object) array('name' => $name, 'count' => $count);
			return $this->_modules[$name]->name;
		}		
		else{
			$count = mosCountModules($module);
			if($fallback){				
				$this->_modules[$name] = (object) array('name' => $module, 'count' => $count);
			}
			
			$this->_modules[$module] = (object) array('name' => $module, 'count' => $count);
			return $this->_modules[$module]->name;
		}
	}
	
	
	function countModules($module, $fallback = true){
		
		$name = $this->getModuleName($module, $fallback);
		return $this->_modules[$name]->count;
	}
	
	function loadModules($module, $style =null, $fallback = true){		
	
		mosLoadModules($this->getModuleName($module, $fallback), $style);
	}
}