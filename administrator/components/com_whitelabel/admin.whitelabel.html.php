<?php
/**
* @version $Id: admin.WhiteLabels.html.php 5023 2006-09-13 12:36:21Z friesengeist $
* @package Joomla
* @subpackage WhiteLabels
* @copyright Copyright (C) 2005 Open Source Matters. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );

/**
* @package Joomla
* @subpackage WhiteLabels
*/
class HTML_WhiteLabels {

	function showWhiteLabels( $option, &$rows, &$search, &$pageNav ) {
		global $my;

		mosCommonHTML::loadOverlib();
		?>
		<form action="index2.php" method="post" name="adminForm">
		<table class="adminheading">
		<tr>
			<th>
			Whitelabel Manager
			</th>
			<td>
			Filter:
			</td>
			<td>
			<input type="text" name="search" value="<?php echo htmlspecialchars( $search );?>" class="text_area" onChange="document.adminForm.submit();" />
			</td>
			<td width="right">
			</td>
		</tr>
		</table>

		<table class="adminlist">
		<tr>
			<th width="5">
			#
			</th>
			<th width="20">
			<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $rows ); ?>);" />
			</th>
			<th class="title">
			Domain
			</th>
			<th width="35%">
			Template
			</th>
			<th colspan="2" width="5%">
			Published
			</th>
		</tr>
		<?php
		$k = 0;
		for ($i=0, $n=count( $rows ); $i < $n; $i++) {
			$row = &$rows[$i];

			$link 	= 'index2.php?option=com_whitelabel&task=editA&hidemainmenu=1&id='. $row->id;

			$task 	= $row->published ? 'unpublish' : 'publish';
			$img 	= $row->published ? 'publish_g.png' : 'publish_x.png';
			$alt 	= $row->published ? 'Published' : 'Unpublished';

			$checked 	= mosCommonHTML::CheckedOutProcessing( $row, $i );
			?>
			<tr class="<?php echo "row$k"; ?>">
				<td>
				<?php echo $pageNav->rowNumber( $i ); ?>
				</td>
				<td>
				<?php echo $checked; ?>
				</td>
				<td>
				<?php
				if ( $row->checked_out && ( $row->checked_out != $my->id ) ) {
					echo $row->domain;
				} else {
					?>
					<a href="<?php echo $link; ?>" title="Edit WhiteLabels">
					<?php echo $row->domain; ?>
					</a>
					<?php
				}
				?>
				</td>
				<td><?php echo $row->template ?></td>
				<td align="center">
				<a href="javascript: void(0);" onclick="return listItemTask('cb<?php echo $i;?>','<?php echo $task;?>')">
				<img src="images/<?php echo $img;?>" width="12" height="12" border="0" alt="<?php echo $alt; ?>" />
				</a>
				</td>
			</tr>
			<?php
			$k = 1 - $k;
		}
		?>
		</table>
		<?php echo $pageNav->getListFooter(); ?>
		<input type="hidden" name="option" value="<?php echo $option;?>" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="hidemainmenu" value="0">
		</form>
		<?php
	}

	/**
	* Writes the edit form for new and existing record
	*
	* A new record is defined when <var>$row</var> is passed with the <var>id</var>
	* property set to 0.
	* @param mosWhitelabel The Whitelabel object
	* @param array An array of select lists
	* @param object Parameters
	* @param string The option
	*/
	function editWhitelabel( &$row, &$lists, &$params, &$params_mosconfig, $option ) {
		mosMakeHtmlSafe( $row, ENT_QUOTES, 'description' );
		
		mosCommonHTML::loadOverlib();
		?>
		<script language="javascript" type="text/javascript">
		function submitbutton(pressbutton) {
			var form = document.adminForm;
			if (pressbutton == 'cancel') {
				submitform( pressbutton );
				return;
			}

			// do field validation
			if (form.title.value == ""){
				alert( "Whitelabel item must have a title" );
			} else {
				submitform( pressbutton );
			}
		}
		</script>
		<form action="index2.php" method="post" name="adminForm" id="adminForm">
		<table class="adminheading">
		<tr>
			<th>
			Whitelabel:
			<small>
			<?php echo $row->id ? 'Edit' : 'New';?>
			</small>
			</th>
		</tr>
		</table>

		<table width="100%">
		<tr>
			<td width="60%" valign="top">
				<table class="adminform">
				<tr>
					<th colspan="2">
					Details
					</th>
				</tr>
				<tr>
					<td width="20%" align="right">
					<div style='float: right'><?php echo mosToolTip('Enter the domain name for the whitelabel site here') ?></div>
					Domain:
					</td>
					<td width="80%">
					<input class="text_area" type="text" name="domain" size="50" maxlength="100" value="<?php echo $row->domain;?>" /> 
					</td>
				</tr>
				<tr>
					<td valign="top" align="right">
					<div style='float: right'><?php echo mosToolTip('Select your whitelabel template') ?></div>
					Template:
					</td>
					<td>
					<?php echo $lists['templates']; ?> 
					</td>
				</tr>
				<tr>
					<td valign="top" align="right">
					<div style='float: right'><?php echo mosToolTip('Choose a homepage link from a menu, or enter your own homepage manually below.') ?></div>
					Home Page Menu Item:
					</td>
					<td>
					<?php echo $lists['menus']; ?> 
					</td>
				</tr>
				<tr>
					<td valign="top" align="right">
					Or Home Page Url:
					</td>
					<td>
					<input type='text' size='50' name='home_url' value='<?php echo $row->home_url ?>' />
					</td>
				</tr>
				<tr>
					<td valign="top" align="right">
					Published:
					</td>
					<td>
					<?php echo $lists['published']; ?>
					</td>
				</tr>
				</table>
			</td>
			<td width="40%" valign="top">
				<?php 
				$tabs = new mosTabs(1);
				$tabs->startPane("parameters-pane");
				$tabs->startTab("Custom Parames","custom-params");				
					?>
					<table class="adminform">
					<tr>
						<th colspan="1">
						Parameters
						</th>
					</tr>
					<tr>
						<td>
						<?php echo $params->render();?>
						</td>
					</tr>
					</table>
					<?php 
				$tabs->endTab();
				$tabs->startTab("Global Params","global-params");	
					?>
					<table class="adminform">
					<tr>
						<th colspan="1">
						Global Parameters
						</th>
					</tr>
					<tr>
						<td>
						<?php echo $params_mosconfig->render('params_mosconfig');?>
						</td>
					</tr>
					</table>
					<?php 
				$tabs->endTab();
				$tabs->endPane();				
				?>
			</td>
		</tr>
		</table>

		<input type="hidden" name="id" value="<?php echo $row->id; ?>" />
		<input type="hidden" name="option" value="<?php echo $option;?>" />
		<input type="hidden" name="task" value="" />
		</form>
		<?php
	}
}
?>