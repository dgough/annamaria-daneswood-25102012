<?php
/**
* @version $Id: readme_de.php,v 1.1 2007/05/05 19:26:26 mic $
* @version 2.0.3
* @package Custom QuickIcons
* @copyright (C) 2006/07 mic [ http://www.joomx.com ]
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*/

/** ensure this file is being included by a parent file */
defined( '_VALID_MOS' ) or die( 'No Direct Access' );

$QI_VERSION = CustomQuickIcons::_QI_version(); ?>

<link href="<?php echo $GLOBALS['mosConfig_live_site']; ?>/administrator/components/com_customquickicons/help/help.css" rel="stylesheet" type="text/css" media="screen" />
<div style="text-align:left;">
	
	<div>
		<?php echo _QI_INST_TXT1; ?>
	</div>
	<?php
	if( !file_exists( $GLOBALS['mosConfig_absolute_path'] . '/administrator/modules/mod_customquickicons.php' ) ) { ?>
	    <div class="warning"><span class="warningHeader"><?php echo _QI_RM_NO_MOD; ?></div>
	    <?php
	}else{ ?>
		<div style="clear:both;"><hr /></div>
		<?php
	} ?>
	<div class="warning"><?php echo _QI_RM_DELETE; ?></div>
	<div style="clear:both;"><hr /></div>
	<div>
	    <?php echo _QI_RM_TRANS_BY; ?>:
		<ul>
			<li><?php echo _QI_RM_TRANS_HU; ?>: Joszef Tamas Herczeg <a href="http://www.joomlandia.eu" target="_blank">Joomlandia.eu</a></li>
		</ul>
		<?php
		$link = '<a href="http://joomlacode.org/gf/project/joomx/" title="'
		. _QI_INST_ALT_FORUM . '" target="_blank">'
		. _QI_INST_ALT_FORUM . '</a>';
		echo sprintf( _QI_RM_NEW_TRANS, $link ); ?>
	</div>
</div>
<div style="clear:both;"><hr /></div>
<div style="text-align:center; background-color:#eff9ff">
    <a href="http://www.joomlahosting.biz" target="_blank" title="powered by joomlaHosting.biz">powered by joomlaHosting.biz</a> | info@joomx.com .:. <a href="http://www.joomx.com" target="_blank" title="JoomlaXperts - Professional Joomla! Solutions">JoomlaXperts - Professional Joomla! Solutions</a>
</div>
