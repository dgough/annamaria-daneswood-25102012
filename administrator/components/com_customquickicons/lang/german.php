<?php
/**
* @version $Id: german.php,v 1.0 2007/05/06 16:17:17 mic Exp $
* german language for CQI
* @package CustomQuickIcons - admin component for Joomla! OS CMS
* @subpackage languages
* @copyright (C) 2007 mic - michael [ http://www.joomx.com ]
* @info info@joomx.com http://www.joomx.com
* @license GNU/GPL License : http://www.gnu.org/copyleft/gpl.html
*/
defined( '_VALID_MOS' ) or die( 'No Direct Access' );

if( defined( '_CONTXTD_LANG_INCLUDED' ) ) {
	return;
}else{
    require_once( dirname( __FILE__ ).'/germanf.php' );
}
?>