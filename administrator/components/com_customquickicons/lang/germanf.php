<?php
/**
* @version $Id: germanf.php,v 1.9 2007/05/06 14:26:26 mic $
* @version 2.0.5 - german language for com_customquickicons
* @package Custom QuickIcons
* @copyright (C) 2006/07 mic [ http://www.joomx.com ]
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*/

defined( '_VALID_MOS' ) or die( 'Keine Berechtigung' );

if( defined( '_LANG_QUICKICONS' )){
	return;
}else{
    define( '_LANG_QUICKICONS', 1 );

    // common
    define( '_QI_LNG',					'de'); // en - tr - etc. ....
    define( '_QI_BTN_LNG',				'de_DE' ); // nicht aendern!
    define( '_QI_QUICKICONS',			'CQI - CustomQuickIcons');
    define( '_QI_CMN_ACCESS',			'Berechtigt');
    define( '_QI_SEARCH',				'Suche');
    define( '_QI_TITLE',				'Title');
    define( '_QI_MOD_MGMNT',			'Zur Modulverwaltung' ); // new 2.0.5

    // header
    define( '_QI_HDR_MGMNT',			'Verwaltung');

    // Toolbar
    define('_QI_PUBLISH', 				'Klicken um zu Aktivieren');
    define('_QI_UNPUBLISH', 			'Klicken um zu Deaktivieren');
    define('_QI_NEW', 					'Neu');
    define('_QI_EDIT', 					'Bearbeiten');
    define('_QI_DELETE', 				'L&ouml;schen');
    define('_QI_SAVE', 					'Speichern');
    define('_QI_APPLY', 				'&Uuml;berneh.');
    define('_QI_CANCEL', 				'Abbrechen');

    // QickIcons List
    define('_QI_NAME', 					'Name/Men&uuml;');
    define('_QI_PUBLISHED', 			'Aktiviert');
    define('_QI_UNPUBLISHED', 			'Deaktiviert');
    define('_QI_REORDER', 				'Anordnen');
    define('_QI_ORDER',					'Reihenfolge');
    define('_QI_SAVE_ORDER',			'Reihenfolge speichern');
    define('_QI_TARGET',				'Ziel');
    define('_QI_ORDER_UP',				'Nach Oben');
    define('_QI_ORDER_DOWN',			'Nach Unten');

    // Edit/New QuickIcon
    define('_QI_DETAIL',				'QuickIcon Details');
    define('_QI_DETAIL_EDIT',			'Bearbeite');
    define('_QI_DETAIL_NEW',			'Neues');
    define('_QI_DETAIL_PREFIX',			'Pr&auml;fix');
    define('_QI_DETAIL_POSTFIX',		'Postfix');
    define('_QI_DETAIL_TEXT',			'Text');
    define('_QI_DETAIL_NEW_WINDOW',		'Neues Fenster');
    define('_QI_DETAIL_YES',			'Ja');
    define('_QI_DETAIL_NO',				'Nein');
    define('_QI_DETAIL_ORDER',			'Reihenfolge');
    define('_QI_DETAIL_ICON', 			'Icon');
    define('_QI_DETAIL_CHOOSE_ICON', 	'Icon ausw&auml;hlen');

    define( '_QI_ACCESSKEY',			'Tastenk&uuml;rzel');
    define( '_QI_DISPLAY',				'Ausgabe als');

    // fonts
    define( '_QI_FONT_BOLD',			'Fett');
    define( '_QI_FONT_ITALIC',			'Schr&auml;g');
    define( '_QI_FONT_UNDERLINE',		'Unterstrichen');

    // others (msg, err, etc.)
    define( '_QI_MSG_SUC_DELETED',		'Icon(s) erfolgreich gel&ouml;scht!');
    define( '_QI_MSG_CHOOSE_ICON', 		'Auf ein Icon klicken um es auszuw&auml;hlen.');
    define( '_QI_MSG_TEXT', 			'Bitte einen Text angeben!');
    define( '_QI_MSG_TARGET', 			'Bitte Ziel angeben!');
    define( '_QI_MSG_ICON', 			'Bitte Icon ausw&auml;hlen!' );
    define( '_QI_CMT_CHECK',			'&Uuml;berpr&uuml;fe Komponente');
    define( '_QI_CMT_NAME_TO_CHECK',	'Name & Pfad');
    define( '_QI_CHECK_VERSION',		'Aktuelle&nbsp;Version&nbsp;abfragen' );
    define( '_QI_ERR_NO_TARGET',		'Kein Ziellink definiert!' );

    // module
    define( '_QI_MOD_ACCESSKEY',		'Tastenk&uuml;rzel ALT +' );
    define( '_QI_MOD_NO_COM',			'Komponente nicht vorhanden - bitte installieren!' );

    // msgs
    define( '_QI_MSG_NEW_ORDER_SAVED',	'Neue Reihenfolge gesichert');

    // tips
    define( '_QI_TIP_TARGET',			'Der Link der aufzurufenden Seite/Komponente.<br />F&uuml;r Komponenten innerhalb dieses CMS etwa, muss z.B. ein Link so ausssehen:<br />index2.php?option=com_joomlastats&task=stats  [ joomlastats ist die Komponente, &task=stats ruft eine bestimmte Funktion innerhalb auf ].<br />Externe Links m�ssen als <strong>absoluter Link</strong> (z.B. http://www....) angegeben werden!');
    define( '_QI_TIP_CMT_CHECK',		'<strong>Optional</strong><br />Um sicherzugehen dass nicht eine eventuell schon deinstallierte Komponente angezeigt wird, kann das Vorhandensein dieser Komponente &uuml;berpr&uuml;ft werden.<br />Dazu hier ankreuzen und unten die weiteren Details angeben');
    define( '_QI_TIP_CM_PATH',			'<strong>Optional</strong><br />root/administrator/components/ ist fix vorgegeben<br />Einzuf&uuml;gen ist dieser Teil: <strong>Komponentename/Datei.php</strong> welche gepr&uuml;ft werden soll<br />Der vollst&auml;ndige Pfad sollte dann so aussehen:<br />../administrator/components/com_NAME/DATEI_TO_CHECK.php');
    define( '_QI_TIP_CM_PATH_CHECK',	'Als Hilfestellung hier die vorhandenen Komponenten.<br />Um den Link zu vervollst&auml;ndigen ist der Teil [ com_xxxxx ] <strong>und der Name der zu &uuml;berpr&uuml;fenden Datei</strong> anzugeben!<br />Der fertige Link k&ouml;nnte dann z.B. so aussehen:<br />com_costumquickicons/admin.customquickicons.php' );
    define( '_QI_TIP_DETAIL_NEW_WINDOW','Hier best&auml;tigen wenn der Link in einem neuen Fenster ge&ouml;ffnet werden soll');
    define( '_QI_TIP_TITLE',			'<strong>Optional</strong><br />Hier kann eine Text f&uuml;r den title.tag eingegeben werden.<br />Wichtig dann wenn z.B. die Anzeige nur als Icon erfolgen soll!');
    define( '_QI_TIP_FONT',				'Optional kann der Men&uuml;text formatiert werden; betreffende Checkbox markieren');
    define( '_QI_TIP_ACCESSKEY',		'Aus benutzerfreundlichen Gr&uuml;nden (es k&ouml;nnten ja auch einmal Personen mit einer k�rperlichen Beintr&auml;chtigung mit diesem System arbeiten - und aus gesetzlichen Gr&uuml;nden!) sollten zumindest den wichtigsten Men&uuml;eintr&auml;gen sogenannte [ <em>Shortcuts</em> oder <em>Accesskeys</em> ] zugewiesen werden.<br />Diese bestehen aus einem Buchstaben oder einer Zahl <strong>immer in Kombination</strong> mit der ALT-Taste <strong>und</strong> m&uuml;ssen einmalig sein!');
    define( '_QI_TIP_ICON', 			'Bitte Icon ausw&auml;hlen! Weitere Icons sind im Ordner ../administrator/images/ zu speichern' );

    // tabs
    define( '_QI_TABS_GENERAL',			'Generell');
    define( '_QI_TABS_TEXT',			'Text');
    define( '_QI_TABS_DISPLAY',			'Anzeige');
    define( '_QI_TABS_CHECK',			'Check');
    define( '_QI_TABS_ABOUT',			'&Uuml;ber');

    // title & alt
    define( '_QI_TIT_EDIT_ENTRY',		'Klicken um Eintrag zu bearbeiten');
    define( '_QI_TIT_CHOOSE_ICON',		'Klicken um Icon/Bild auszuw&auml;hlen (&Ouml;ffnet neues Fenster)');

    // select lists
    	// display
    define( '_QI_DISPLAY_ICON_TEXT',	'Icon &amp; Text');
    define( '_QI_DISPLAY_TEXT',			'Nur Text');
    define( '_QI_DISPLAY_ICON',			'Nur Icon');

    // install
    define( '_QI_INST_NEW_ARTICLE',		'Neuer Artikel');
    define( '_QI_INST_NEW_ARTICLE_ALT',	'Neuer Artikel');
    define( '_QI_INST_SECTIONS',		'Bereiche');
    define( '_QI_INST_SECTIONS_ALT',	'Bereichsverwaltung');
    define( '_QI_INST_FRONTPAGE',		'Startseite');
    define( '_QI_INST_FRONTPAGE_ALT',	'Startseitenverwaltung');
    define( '_QI_INST_ARTICLE',			'Artikel');
    define( '_QI_INST_ARTICLE_ALT',		'Artikelverwaltung');
    define( '_QI_INST_ST_CONTENT',		'Statische Inhalte');
    define( '_QI_INST_ST_CONTENT_ALT',	'Artikelverwaltung');
    define( '_QI_INST_MEDIA',			'Medien');
    define( '_QI_INST_MEDIA_ALT',		'Medienverwaltung');
    define( '_QI_INST_CATEGORIES',		'Kategorien');
    define( '_QI_INST_CATEGORIES_ALT',	'Kategorienverwaltung');
	define( '_QI_INST_WASTE',			'Papierkorb');
    define( '_QI_INST_WASTE_ALT',		'Papierkorb/Gel&ouml;schte Elemente');
    define( '_QI_INST_MENUS',			'Men&uuml;s');
    define( '_QI_INST_MENUS_ALT',		'Men&uuml;verwaltung');
    define( '_QI_INST_LANGUAGE',		'Sprachen');
    define( '_QI_INST_LANGUAGE_ALT',	'Sprachenverwaltung');
    define( '_QI_INST_CONFIG',			'Konfiguration');
    define( '_QI_INST_CONFIG_ALT',		'Systemsteuerung');
    define( '_QI_INST_USER',			'Benutzer');
    define( '_QI_INST_USER_ALT',		'Benutzerverwaltung');
    	// install msgs
    define( '_QI_INST_ERROR',			'Leider sind w&auml;hrend der Installierung folgende Fehler aufgetreten');
    define( '_QI_INST_SUCCESS',			'Komponente wurde erfolgreich installiert');
    define( '_QI_INST_DB_ENTRIES',		'Datenbankeintr&auml;ge');
    define( '_QI_INST_TXT1',			'QuickIcons - eine Zusatzkomponente f&uuml;r Joomla! 1.x welche das Standardmodul <strong style="color:red;"><em>mod_quickicon</em></strong> ersetzt.<br />Features:<br /><ul><li>Bedienerfreundliche Men&uuml;verwaltung</li><li>Neue Men&uuml;s anlegen</li><li>Men&uuml;s l&ouml;schen</li><li>Externe Links als Men&uuml; einbinden</li><li>Wahlweise Text und Icon, nur Icon oder nur Text</li><li>Rechteber&uuml;cksichtigung</li><li>Eigene Hilfe (Siehe Hilfeicon oben)</li><li>etc.</li></ul>');
    define( '_QI_INST_TXT2',			'CQI - CustomQuickIcons wurde erfolgreich installiert'); // changed 2.0.5

    define( '_QI_INST_MSG_BU_OLD_TABLE','Alte Tabelle erfolgreich gesichert' );// new 2.0.5
    define( '_QI_INST_MSG_NEW_TABLE',	'Neue Tabelle erfolgreich erstellt' ); // new 2.0.5
	define( '_QI_INST_MSG_DB_ENTRIES',	'Datenbankeintr&auml;ge erfolgreich' ); // new 2.0.5
	define( '_QI_INST_MSG_MOD_FILE',	'Moduldatei %s erfolgreich kopiert' ); // new 2.0.5
	define( '_QI_INST_MSG_MOD_REGGED',	'Neues Modul CQI erfolgreich registriert' ); // new 2.0.5

    define( '_QI_INST_ERR_COPY_MOD_FILE','FEHLER: Konnte Moduldatei nicht kopieren' ); // new 2.0.5
    define( '_QI_INST_ERR_TARGET_DIR',	'FEHLER: Zielverzeichnis konnte nicht erstellt werden' ); // new 2.0.5
    	// alt
    define( '_QI_INST_ALT_WEBSITE',		'QuickIcons Projektseite');
    define( '_QI_INST_ALT_ACT_VERSION',	'Aktuelle Version');
    define( '_QI_INST_ALT_BUGTRACKER',	'Fehlermeldungen');
    define( '_QI_INST_ALT_FORUM',		'Forum/Diskussionen');
    define( '_QI_INST_ALT_EMAIL',		'Email');

    // errors
    define( '_QI_ERR_NO_MOD_INSTALLED',	'Das Modul [ mod_customquickicons ] muss noch installiert werden!' );
    define( '_QI_ERR_MOD_INCORR_POS',	'Das Modul [ mod_customquickicons ] ist nicht an der Position [ icon ] ver&ouml;ffentlicht!' ); // new 2.0.5

    // support (new 2.0.5)
    define( '_QI_SUPP1',				'Wenn Sie mit dieser Komponente zufrieden sind, unterst&uuml;tzen Sie das Projekt und helfen so mit die weitere Entwicklung und den Support aktiv zu halten. Vielen Dank!' );
    define( '_QI_SUPP2',				'<br /><br />Sie k&ouml;nnen ihre Zahlung mit einem der angezeigten Buttons anweisen.<br />Jeder Betrag ist willkommen.' );
    define( '_QI_SUPP_BTN_TITLE',		'CQI unterst&uuml;tzen' );
    define( '_QI_SUPP_HEAD_TITLE',		'CustomQuickIcons - Individuelle Icons/Men&uuml;s - Support' );
    define( '_QI_SUPP_INP_TXT',			'Support CustomQuickIcons' );
    define( '_QI_SUPP_BTN_SUBMIT',		'Ja, ich will helfen' );
    define( '_QI_SUPP_TXT_PAY_W_MB',	'Zahlung mit Moneybookers' );

    // readme (new 2.0.5)
    define( '_QI_RM_VERSION',			'Version' );
    define( '_QI_RM_BY',				'Von' );
    define( '_QI_RM_COPYR',				'Copyright' );
    define( '_QI_RM_LICENSE',			'Lizenz <a href="http://www.gnu.de/gpl-ger.html" target="_blank" title="GPL in Deutsch">GPL in Deutsch</a>' );
    define( '_QI_RM_BASED',				'Basiert auf einem Script von' );
    define( '_QI_RM_NO_MOD',			'Wichtiger Hinweis:</span> Zus&auml;tzlich zu dieser Komponente muss auch noch das Modul <strong>mod_customquickicons</strong> installiert werden (Download siehe unten) und dann das Originalmodul deaktiviert und das neue Modul aktiviert werden!' );
    define( '_QI_RM_TRANS_BY',			'&Uuml;bersetzungen von' );
    define( '_QI_RM_TRANS_HU',			'Ungarisch' );
    define( '_QI_RM_NEW_TRANS',			'Wenn Sie eine &Uuml;bersetzung haben, gehen Sie bitte auf %s und hinterlassen dort die Datei' );
    define( '_QI_RM_DELETE',			'HINWEIS: Wird die Komponente gel&ouml;scht, bleiben die Datenbankeintr&auml;ge erhalten! Das CQI-Modul wird ebenso gel&ouml;scht und das Standardmodul wieder aktiviert' );
}
?>
