<?php
/**
* @version $Id: english.php,v 2.2 2007/05/06 14:26:26 mic $
* @version 2.0.5
* @package Custom QuickIcons
* @copyright (C) 2006/07 mic [ http://www.joomx.com ]
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*/

defined( '_VALID_MOS' ) or die( 'No Direct Access' );

if( defined( '_LANG_QUICKICONS' )){
	return;
}else{
    define( '_LANG_QUICKICONS', 1 );

    // common
    define( '_QI_LNG',					'en' ); // de - tr - etc. ....
    define( '_QI_BTN_LNG',				'en_US' ); // do not change!
    define( '_QI_QUICKICONS',			'CQI - CustomQuickIcons');
    define( '_QI_CMN_ACCESS',			'Access');
    define( '_QI_SEARCH',				'Search');
    define( '_QI_TITLE',				'Title' ); // (for title.tag)
    define( '_QI_MOD_MGMNT',			'Module administration' ); // new 2.0.5

    // header
    define( '_QI_HDR_MGMNT',			'Management');

    // Toolbar
    define('_QI_PUBLISH', 				'Click to activate');
    define('_QI_UNPUBLISH', 			'Click to deactivate');
    define('_QI_NEW',					'New');
    define('_QI_EDIT',					'Edit');
    define('_QI_DELETE',				'Delete');
    define('_QI_SAVE',					'Save');
    define('_QI_APPLY',					'Apply');
    define('_QI_CANCEL',				'Cancel');

    // QickIcons List
    define('_QI_NAME',					'Name');
    define('_QI_PUBLISHED',				'Published');
    define('_QI_UNPUBLISHED',			'Unpublished');
    define('_QI_REORDER',				'Reorder');
    define('_QI_ORDER',					'Order');
    define('_QI_SAVE_ORDER',			'Save Order');
    define('_QI_TARGET',				'Target');
    define('_QI_ORDER_UP',				'Order Up');
    define('_QI_ORDER_DOWN',			'Order Down');

    // Edit/New QuickIcon
    define('_QI_DETAIL',				'QuickIcon Details');
    define('_QI_DETAIL_EDIT',			'Edit');
    define('_QI_DETAIL_NEW',			'New');
    define('_QI_DETAIL_PREFIX',			'Prefix');
    define('_QI_DETAIL_POSTFIX',		'Postfix');
    define('_QI_DETAIL_TEXT',			'Text');
    define('_QI_DETAIL_NEW_WINDOW',		'New Window');
    define('_QI_DETAIL_YES',			'Yes');
    define('_QI_DETAIL_NO',				'No');
    define('_QI_DETAIL_ORDER',			'Order after');
    define('_QI_DETAIL_ICON',			'Icon');
    define('_QI_DETAIL_CHOOSE_ICON', 	'Choose Icon');

    define( '_QI_ACCESSKEY',			'Accesskey' );
    define( '_QI_DISPLAY',				'Display as' );

    // fonts
    define( '_QI_FONT_BOLD',			'Bold' );
    define( '_QI_FONT_ITALIC',			'Italic' );
    define( '_QI_FONT_UNDERLINE',		'Underline' );

    // Others
    define('_QI_MSG_SUC_DELETED', 		'Icon(s) successful deleted!' );
    define('_QI_MSG_CHOOSE_ICON', 		'Click on icon to select' );
    define('_QI_MSG_TEXT', 				'Please fill in a text' );
    define('_QI_MSG_TARGET', 			'Please provide a target' );
    define('_QI_MSG_ICON', 				'Please choose an icon' );
    define( '_QI_CMT_CHECK',			'Check Component' );
    define( '_QI_CMT_NAME_TO_CHECK',	'Name &amp; Path' );
    define( '_QI_CHECK_VERSION',		'Current&nbsp;Version' );
    define( '_QI_ERR_NO_TARGET',		'No target defined!' );

    // module
    define( '_QI_MOD_ACCESSKEY',		'Shortcut ALT +' );
    define( '_QI_MOD_NO_COM',			'Component not existing - please install first!' );

    // msgs
    define( '_QI_MSG_NEW_ORDER_SAVED',	'New ordering saved' );

	// tips
    define( '_QI_TIP_TARGET',			'Link for the calling site/component.<br />For components inside this CMS the link has to be as (e.g.): <br />index2.php?option=com_joomlastats&task=stats  [ joomlastats is the component, &task=stats calls a specific function inside ].<br />External links has to be as <strong>absolute link</strong> (e.g.: http://www....)!');
    define( '_QI_TIP_CMT_CHECK',		'<strong>Optional</strong><br />To be shure that a component wont be deinstalled and called afterwards, check here und fill in below further details');
    define( '_QI_TIP_CM_PATH',			'<strong>Optional</strong><br />root/administrator/components/ is a fix part<br />To add: <strong>name_of_component/file.php</strong><br />the whole link could look like: :<br />../administrator/components/com_NAME/FILE_TO_CHECK.php');
    define( '_QI_TIP_CM_PATH_CHECK',	'To help you with link building are here all existing (installed) components.<br />To build the link add teh part [ com_xxxxx ] <strong>and the name of the component which is to check</strong>!<br />The complete link could be like this::<br />com_costumquickicons/admin.customquickicons.php' );
    define( '_QI_TIP_DETAIL_NEW_WINDOW','Should the link be opened in a new window');
    define( '_QI_TIP_TITLE',			'<strong>Optional</strong><br />Here you can define a text for the title.tag.<br />Important if the display should be only as icon!');
    define( '_QI_TIP_FONT',				'Chech the approbiate checkbox if you want to specify some format for the link');
    define( '_QI_TIP_ACCESSKEY',		'Due the reason of accessability all links should have as well an accesskey.<br />Define here the letter or number which will be used in conjunction with the ALT.key. The <strong>must be unique</strong>!');
    define('_QI_TIP_ICON', 				'Please choose an icon. New icons have to be stored in ../administrator/images' );

    // tabs
    define( '_QI_TABS_GENERAL',			'General' );
    define( '_QI_TABS_TEXT',			'Text');
    define( '_QI_TABS_DISPLAY',			'Display' );
    define( '_QI_TABS_CHECK',			'Check');
    define( '_QI_TABS_ABOUT',			'About' );

    // title & alt
    define( '_QI_TIT_EDIT_ENTRY',		'Click to edit this entry' );
    define( '_QI_TIT_CHOOSE_ICON',		'Click to choose icon/image (opens new window)' );

    // select lists
    	// display
    define( '_QI_DISPLAY_ICON_TEXT',	'Icon & Text');
    define( '_QI_DISPLAY_TEXT',			'Only Text');
    define( '_QI_DISPLAY_ICON',			'Only Icon');
    define( '_QI_DISPLAY_SEPERATOR',	'Seperator No Text');
    define( '_QI_DISPLAY_SEPERATOR_TEXT',	'Seperator With Text');

    // install
    define( '_QI_INST_NEW_ARTICLE',		'New Article' );
    define( '_QI_INST_NEW_ARTICLE_ALT',	'New Article' );
    define( '_QI_INST_SECTIONS',		'Sections' );
    define( '_QI_INST_SECTIONS_ALT',	'Sections Management' );
    define( '_QI_INST_FRONTPAGE',		'Frontpage' );
    define( '_QI_INST_FRONTPAGE_ALT',	'Frontpage Management' );
    define( '_QI_INST_ARTICLE',			'Article' );
    define( '_QI_INST_ARTICLE_ALT',		'Article Management' );
    define( '_QI_INST_ST_CONTENT',		'Static Content' );
    define( '_QI_INST_ST_CONTENT_ALT',	'Static Content Management' );
    define( '_QI_INST_MEDIA',			'Media' );
    define( '_QI_INST_MEDIA_ALT',		'Media Management' );
    define( '_QI_INST_CATEGORIES',		'Categories' );
    define( '_QI_INST_CATEGORIES_ALT',	'Category Management' );
	define( '_QI_INST_WASTE',			'Waste' );
    define( '_QI_INST_WASTE_ALT',		'Waste/Deleted Items' );
    define( '_QI_INST_MENUS',			'Menus' );
    define( '_QI_INST_MENUS_ALT',		'Menu Management' );
    define( '_QI_INST_LANGUAGE',		'Languages' );
    define( '_QI_INST_LANGUAGE_ALT',	'Language Management' );
    define( '_QI_INST_CONFIG',			'Configuration' );
    define( '_QI_INST_CONFIG_ALT',		'Configuration' );
    define( '_QI_INST_USER',			'User' );
    define( '_QI_INST_USER_ALT',		'User Management' );
    	// install msgs
    define( '_QI_INST_ERROR',			'Unfortunately some errors happened during the installation process!');
    define( '_QI_INST_SUCCESS',			'Component successfully installed');
    define( '_QI_INST_DB_ENTRIES',		'Database entries');
    define( '_QI_INST_TXT1',			'CustomQuickIcons - an add.on for Joomla 1.x which replaces the standard.module <strong style="color:red;"><em>mod_quickicon</em></strong>.<br />Features:<br /><ul><li>User friendly managment of menus</li><li>New menus</li><li>Delete menus</li><li>External links as menu</li><li>Choose text and icon, only text, only icon</li><li>Access management</li><li>Internal Help (see icon at toolbar)</li><li>etc.</li></ul>');
    define( '_QI_INST_TXT2',			'CQI - CustomQuickIcons sucessfully installed'); // changed 2.0.5

	define( '_QI_INST_MSG_BU_OLD_TABLE','Old table successfully saved' ); // new 2.0.5
    define( '_QI_INST_MSG_NEW_TABLE',	'New table successfully created' ); // new 2.0.5
	define( '_QI_INST_MSG_DB_ENTRIES',	'Database entries successful' ); // new 2.0.5
	define( '_QI_INST_MSG_MOD_FILE',	'Module file %s successfully copied' ); // new 2.0.5
	define( '_QI_INST_MSG_MOD_REGGED',	'New module CQI successfully registered' ); // new 2.0.5

    define( '_QI_INST_ERR_COPY_MOD_FILE','ERROR: Could not copy module file' ); // new 2.0.5
    define( '_QI_INST_ERR_TARGET_DIR',	'ERROR: Could not create target directory' ); // new 2.0.5

    	// alt
    define( '_QI_INST_ALT_WEBSITE',		'QuickIcons Projectsite');
    define( '_QI_INST_ALT_ACT_VERSION',	'Latest Version');
    define( '_QI_INST_ALT_BUGTRACKER',	'Error-/Bugreporting');
    define( '_QI_INST_ALT_FORUM',		'Forum/Discussion');
    define( '_QI_INST_ALT_EMAIL',		'Email');

    //  errors
    define( '_QI_ERR_NO_MOD_INSTALLED',	'The module <em>mod_customquickicons</em> is not installed!' );
    define( '_QI_ERR_MOD_INCORR_POS',	'The module [ mod_customquickicons ] is not published at the position [ icon ]' ); // new 2.0.5

    // support (new 2.0.5)
    define( '_QI_SUPP1',				'If you find this component useful and are satisfied with, deonate to this project and help us to keep support and development alive. Many thanks!' );
    define( '_QI_SUPP2',				'<br /><br />You can send the money with one of the shown payment gateways.<br />Every amount is welcome.' );
    define( '_QI_SUPP_BTN_TITLE',		'Support CQI' );
    define( '_QI_SUPP_HEAD_TITLE',		'CustomQuickIcons - Individual Icons/Menus - Support' );
    define( '_QI_SUPP_INP_TXT',			'Support CustomQuickIcons' );
    define( '_QI_SUPP_BTN_SUBMIT',		'Yes, i want to support' );
    define( '_QI_SUPP_TXT_PAY_W_MB',	'Payment with Moneybookers' );

    // readme (new 2.0.5)
    define( '_QI_RM_VERSION',			'Version' );
    define( '_QI_RM_BY',				'By' );
    define( '_QI_RM_COPYR',				'Copyright' );
    define( '_QI_RM_LICENSE',			'License <a href="http://www.gnu.org/copyleft/gpl.html" target="_blank" title="GPL in English">GPL in English</a>' );
    define( '_QI_RM_BASED',				'Based on a script by' );
    define( '_QI_RM_NO_MOD',			'IMPORTANT:</span> Additional to this component the module <strong>mod_customquickicons</strong> must be installed and published (download see below) and then the original module unpublished!' );
    define( '_QI_RM_TRANS_BY',			'Translations by' );
    define( '_QI_RM_TRANS_HU',			'Hungarian' );
    define( '_QI_RM_NEW_TRANS',			'If you have a translation, please go to %s and post your file' );
    define( '_QI_RM_DELETE',			'ATTENTION: If you delete the component, the database entries will be saved! The CQI-Modul will be also deleted and the standard module reactivated' );
}
?>
