<?php
/**
* @version $Id: hungariani.php,v 2.2 2007/05/06 14:26:26 mic $
* @version 2.0.5
* @package Custom QuickIcons
* @copyright (C) 2006/07 mic [ http://www.joomx.com ]
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
* translation by: Joszef Tamas Herzceg - Joomlandia.eu [info@joomlandia.eu]
*/

defined( '_VALID_MOS' ) or die( 'A k�zvetlen hozz�f�r�s tilos' );

if( defined( '_LANG_QUICKICONS' )){
	return;
}else{
    define( '_LANG_QUICKICONS', 1 );

      // common
    define( '_QI_LNG',					'hu' ); // de - tr - etc. ....
    define( '_QI_BTN_LNG',				'en_US' ); // do not change!
    define( '_QI_QUICKICONS',			'CQI - CustomQuickIcons');
    define( '_QI_CMN_ACCESS',			'Hozz�f�r�s');
    define( '_QI_SEARCH',				'Keres�s');
    define( '_QI_TITLE',				'C�m' ); // (for title.tag)
    define( '_QI_MOD_MGMNT',			'Module administration' ); // new 2.0.5

      // header
    define( '_QI_HDR_MGMNT',			'Kezel�s');

      // Toolbar
    define('_QI_PUBLISH', 'K�zz�tesz');
    define('_QI_UNPUBLISH', 'Visszavon');
    define('_QI_NEW',   '�j');
    define('_QI_EDIT',    'Szerkeszt');
    define('_QI_DELETE',    'T�r�l');
    define('_QI_SAVE',    'Ment�s');
    define('_QI_APPLY',   'Alkalmaz');
    define('_QI_CANCEL',    'M�gse');

      // QickIcons List
    define('_QI_NAME', 'N�v');
    define('_QI_PUBLISHED',   'K�zz�t�ve');
    define('_QI_UNPUBLISHED',   'Visszavonva');
    define('_QI_REORDER',   '�trendez�s');
    define('_QI_ORDER',     'Sorrend');
    define('_QI_SAVE_ORDER',    'A sorrend ment�se');
    define('_QI_TARGET',      'C�l');
    define('_QI_ORDER_UP',      'L�ptet�s fel');
    define('_QI_ORDER_DOWN',    'L�ptet�s le');

      // Edit/New QuickIcon
    define('_QI_DETAIL',      'Parancsikon tulajdons�gai');
    define('_QI_DETAIL_EDIT', 'Szerkeszt�s :');
    define('_QI_DETAIL_NEW',  '�j :');
    define('_QI_DETAIL_PREFIX', 'El�tag');
    define('_QI_DETAIL_POSTFIX',  'Ut�tag');
    define('_QI_DETAIL_TEXT', 'Sz�vegc�mke');
    define('_QI_DETAIL_NEW_WINDOW', '�j ablak');
    define('_QI_DETAIL_YES',      'Igen');
    define('_QI_DETAIL_NO',     'Nem');
    define('_QI_DETAIL_ORDER',      'Sorrend');
    define('_QI_DETAIL_ICON',     'Ikon');
    define('_QI_DETAIL_CHOOSE_ICON',  'V�lasszon ikont');

    define( '_QI_ACCESSKEY',			'H�v�bet�' );
    define( '_QI_DISPLAY',				'Megjelen�t�si m�d' );

      // fonts
    define( '_QI_FONT_BOLD',			'F�lk�v�r' );
    define( '_QI_FONT_ITALIC',			'D�lt' );
    define( '_QI_FONT_UNDERLINE',		'Al�h�zott' );

      // Others
    define('_QI_MSG_SUC_DELETED', 'Az ikon(ok) t�rl�se siker�lt!');
    define('_QI_MSG_CHOOSE_ICON', 'Kattints a kiv�lasztand� ikonra.');
    define('_QI_MSG_TEXT', 				'K�rj�k, hogy �rj be valamilyen sz�veget' );
    define('_QI_MSG_TARGET',      'K�rj�k, hogy �rd be a c�lt!');
    define('_QI_MSG_ICON',        'K�rj�k, hogy v�laszd ki az ikont!');
    define( '_QI_CMT_CHECK',			'Komponens ellen�rz�se' );
    define( '_QI_CMT_NAME_TO_CHECK',	'N�v &amp; �tvonal' );
    define( '_QI_CHECK_VERSION',		'Jelenlegi&nbsp;verzi�' );
    define( '_QI_ERR_NO_TARGET',		'Meghat�rozatlan a c�l!' );

      // module
    define( '_QI_MOD_ACCESSKEY',		'Gyorsbillenty� ALT +' );
    define( '_QI_MOD_NO_COM',			'Nem l�tezik a komponens - k�rj�k, hogy telep�tsd el�bb!' );

      // msgs
    define( '_QI_MSG_NEW_ORDER_SAVED',	'Az �j sorrend ment�se k�sz' );

      // tips
    define( '_QI_TIP_TARGET',			'A h�vand� webhely/komponens hivatkoz�sa.<br />Ezen CMS-en bel�li komponensekhez ilyen legyen a hivatkoz�s (pl.): <br />index2.php?option=com_joomlastats&task=stats  [a joomlastats a komponens, melyen bel�l a &task=stats h�v meg egy bizonyos funkci�t].<br />A k�ls� hivatkoz�s <strong>abszol�t hivatkoz�s legyen</strong> (pl.: http://www....)!');
    define( '_QI_TIP_CMT_CHECK',		'<strong>Elhagyhat�</strong><br />Annak biztos�t�s�hoz, hogy egy komponenst ne lehessen a k�s�bbiekben elt�vol�tani �s megh�vni, ellen�rizd az al�bbi adatokat, �s t�ltsd ki �ket');
    define( '_QI_TIP_CM_PATH',			'<strong>Elhagyhat�</strong><br />a gy�k�r/administrator/components/ egy fix r�sz<br />Hozz�ad�s: <strong>a_komponens_neve/file.php</strong><br />a teljes hivatkoz�s �gy n�zzen ki: :<br />../administrator/components/com_N�V/ELLEN�RIZEND�_F�JL.php');
    define( '_QI_TIP_CM_PATH_CHECK',	'A hivatkoz�s l�trehoz�s�nak el�seg�t�s�hez itt megtal�lod az �sszes l�tez� (telep�tett) komponenst.<br />A hivatkoz�s l�trehoz�s�hoz add hozz� a [ com_xxxxx ] r�szt <strong>�s az ellen�rizend� komponens nev�t</strong>!<br />A teljes hivatkoz�s ehhez hasonl� legyen::<br />com_costumquickicons/admin.customquickicons.php' );
    define( '_QI_TIP_DETAIL_NEW_WINDOW','�j ablakban k�v�nod megnyitni a hivatkoz�st');
    define( '_QI_TIP_TITLE',			'<strong>Elhagyhat�</strong><br />Itt adhatod meg a title.tag sz�veg�t.<br />Csak ikonnal t�rt�n� megjelen�t�s eset�n fontos!');
    define( '_QI_TIP_FONT',				'Jel�ld be a hivatkoz�son alkalmazni k�v�nt st�lus jel�l�n�gyzet�t');
    define( '_QI_TIP_ACCESSKEY',		'A kiseg�t� lehet�s�gek �rdek�ben valamennyi hivatkoz�snak h�v�bet�vel is kell rendelkeznie.<br />Rendeld hozz� itt az ALT billenty�vel felhaszn�l�sra ker�l� bet�t vagy sz�mot. <strong>Legyen egyedi</strong>!');
    define('_QI_TIP_ICON', 				'K�rj�k, hogy v�laszd ki az ikont. Az �j ikonokat az ../administrator/images k�nyvt�rba kell tenni' );

      // tabs
    define( '_QI_TABS_GENERAL',			'�ltal�nos' );
    define( '_QI_TABS_TEXT',			'Sz�vegc�mk�k');
    define( '_QI_TABS_DISPLAY',			'Megjelen�t�s' );
    define( '_QI_TABS_CHECK',			'Ellen�rz�s');
    define( '_QI_TABS_ABOUT',			'N�vjegy' );

    // title & alt
    define( '_QI_TIT_EDIT_ENTRY',		'Kattints ide a bejegyz�s szerkeszt�s�hez' );
    define( '_QI_TIT_CHOOSE_ICON',		'Kattints ide az ikon/k�p kiv�laszt�s�hoz (�j ablakban ny�lik meg)' );

      // select lists
    	// display
    define( '_QI_DISPLAY_ICON_TEXT',	'Ikon �s sz�veg');
    define( '_QI_DISPLAY_TEXT',			'Csak sz�veg');
    define( '_QI_DISPLAY_ICON',			'Csak ikon');

      // install
    define( '_QI_INST_NEW_ARTICLE',		'�j cikk' );
    define( '_QI_INST_NEW_ARTICLE_ALT',	'�j cikk' );
    define( '_QI_INST_SECTIONS',		'Szekci�k' );
    define( '_QI_INST_SECTIONS_ALT',	'Szekci�kezel�' );
    define( '_QI_INST_FRONTPAGE',		'C�mlap' );
    define( '_QI_INST_FRONTPAGE_ALT',	'C�mlapkezel�s' );
    define( '_QI_INST_ARTICLE',			'Cikk' );
    define( '_QI_INST_ARTICLE_ALT',		'Cikk-kezel�s' );
    define( '_QI_INST_ST_CONTENT',		'Statikus tartalom' );
    define( '_QI_INST_ST_CONTENT_ALT',	'Statikus tartalom kezel�se' );
    define( '_QI_INST_MEDIA',			'M�dia' );
    define( '_QI_INST_MEDIA_ALT',		'M�diakezel�s' );
    define( '_QI_INST_CATEGORIES',		'Kateg�ri�k' );
    define( '_QI_INST_CATEGORIES_ALT',	'Kateg�riakezel�s' );
	define( '_QI_INST_WASTE',			'Kuka' );
    define( '_QI_INST_WASTE_ALT',		'Kuka/T�r�lt elemek' );
    define( '_QI_INST_MENUS',			'Men�k' );
    define( '_QI_INST_MENUS_ALT',		'Men�kezel�s' );
    define( '_QI_INST_LANGUAGE',		'Nyelvek' );
    define( '_QI_INST_LANGUAGE_ALT',	'Nyelvkezel�s' );
    define( '_QI_INST_CONFIG',			'Be�ll�t�sok' );
    define( '_QI_INST_CONFIG_ALT',		'Be�ll�t�sok' );
    define( '_QI_INST_USER',			'Felhaszn�l�k' );
    define( '_QI_INST_USER_ALT',		'Felhaszn�l�kezel�s' );
    	// install msgs
    define( '_QI_INST_ERROR',			'Sajnos n�h�ny hiba mer�lt f�l a telep�t�s sor�n!');
    define( '_QI_INST_SUCCESS',			'A komponens telep�t�se siker�lt');
    define( '_QI_INST_DB_ENTRIES',		'Adatb�zis-bejegyz�sek');
    define( '_QI_INST_TXT1',			'CustomQuickIcons - Joomla 1.x kieg�sz�t�, mely lecser�li a <strong style="color:red;"><em>mod_quickicon</em></strong> alapmodult.<br />Funkci�i:<br /><ul><li>Felhaszn�l�bar�t men�kezel�s</li><li>�j men�k</li><li>Men�k t�rl�se</li><li>K�ls� hivatkoz�sok men�k�nt</li><li>V�laszthat� sz�veg �s ikon, csak sz�veg, csak ikon</li><li>Hozz�f�r�s-kezel�s</li><li>Internal Help (see icon at toolbar)</li><li>�s sok m�s</li></ul>'); // changed 2.0.5
    define( '_QI_INST_TXT2',			'CQI - CustomQuickIcons sucessfully installed'); // changed 2.0.5

    define( '_QI_INST_MSG_BU_OLD_TABLE','Old table successfully saved' ); // new 2.0.5
    define( '_QI_INST_MSG_NEW_TABLE',	'New table successfully created' ); // new 2.0.5
	define( '_QI_INST_MSG_DB_ENTRIES',	'Database entries successful' ); // new 2.0.5
	define( '_QI_INST_MSG_MOD_FILE',	'Module file %s successfully copied' ); // new 2.0.5
	define( '_QI_INST_MSG_MOD_REGGED',	'New module CQI successfully registered' ); // new 2.0.5

    define( '_QI_INST_ERR_COPY_MOD_FILE','ERROR: Could not copy module file' ); // new 2.0.5
    define( '_QI_INST_ERR_TARGET_DIR',	'ERROR: Could not create target directory' ); // new 2.0.5

    	// alt
    define( '_QI_INST_ALT_WEBSITE',		'A QuickIcons projekt webhelye');
    define( '_QI_INST_ALT_ACT_VERSION',	'Leg�jabb verzi�');
    define( '_QI_INST_ALT_BUGTRACKER',	'Hibabejelent�');
    define( '_QI_INST_ALT_FORUM',		'F�rum/Vita');
    define( '_QI_INST_ALT_EMAIL',		'Email');

    //  errors
    define( '_QI_ERR_NO_MOD_INSTALLED',	'Nem telep�tetted m�g a [ mod_customquickicons ] modult!' );
    define( '_QI_ERR_MOD_INCORR_POS',	'The module [ mod_customquickicons ] is not published at the position [ icon ]' ); // new 2.0.5

    // support (new 2.0.5)
    define( '_QI_SUPP1',				'If you find this component useful and are satisfied with, deonate to this project and help us to keep support and development alive. Many thanks!' );
    define( '_QI_SUPP2',				'<br /><br />You can send the money with one of the shown payment gateways.<br />Every amount is welcome.' );
    define( '_QI_SUPP_BTN_TITLE',		'Support CQI' );
    define( '_QI_SUPP_HEAD_TITLE',		'CustomQuickIcons - Individual Icons/Menus - Support' );
    define( '_QI_SUPP_INP_TXT',			'Support CustomQuickIcons' );
    define( '_QI_SUPP_BTN_SUBMIT',		'Yes, i want to support' );
    define( '_QI_SUPP_TXT_PAY_W_MB',	'Payment with Moneybookers' );

    // readme (new 2.0.5)
    define( '_QI_RM_VERSION',			'Version' );
    define( '_QI_RM_BY',				'By' );
    define( '_QI_RM_COPYR',				'Copyright' );
    define( '_QI_RM_LICENSE',			'License <a href="http://www.gnu.org/copyleft/gpl.html" target="_blank" title="GPL in English">GPL in English</a>' );
    define( '_QI_RM_BASED',				'Based on a script by' );
    define( '_QI_RM_NO_MOD',			'IMPORTANT:</span> Additional to this component the module <strong>mod_customquickicons</strong> must be installed and published (download see below) and then the original module unpublished!' );
    define( '_QI_RM_TRANS_BY',			'Translations by' );
    define( '_QI_RM_TRANS_HU',			'Hungarian' );
    define( '_QI_RM_NEW_TRANS',			'If you have a translation, please go to %s and post your file' );
    define( '_QI_RM_DELETE',			'ATTENTION: If you delete the component, the database entries will be saved! The CQI-Modul will be also deleted and the standard module reactivated' );
}
?>
