<?php
/**
* @version $Id: uninstall.customquickicons.php,v 1.3 2007/05/06 16:07:07 mic $
* @package Custom QuickIcons
* @copyright (C) 2006/7 mic [ http://ww.joomx.com ]
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*/

defined( '_VALID_MOS' ) or die( 'No Direct Access' );

function com_uninstall() {
	global $mainframe, $database;

	$dir = $mainframe->getCfg('absolute_path') . '/modules/';
	// delete module files
	@unlink( $installdir . 'mod_customquickicons.php' );
	@unlink( $installdir . 'mod_customquickicons.xml' );

	// delete db.entry
	$query = "DELETE FROM #__modules"
	. "\n WHERE module = 'mod_customquickicons'"
	;
	$database->setQuery( $query );
	$database->query();

	// set original module active (if not deleted)
	$query = "SELECT id"
	. "\n FROM #__modules"
	. "\n WHERE module = 'mod_quickicon'"
	;
	$database->setQuery( $query );
	$id = $database->loadResult();

	if( $id ) {
		$query = "UPDATE #__modules"
		. "\n SET published = '1'"
		. "\n WHERE id = " . $id
		. "\n LIMIT 1"
		;
		$database->setQuery( $query );
		$database->query();
	}

	echo 'Custom QuickIcons successfully uninstalled!';
}
?>