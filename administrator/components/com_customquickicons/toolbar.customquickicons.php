<?php
/**
* @version $Id: toolbar.customquickicons.php,v 1.3 2006/12/06 09:22:22 mic $
* @version 2.0.1
* @package Custom QuickIcons
* @copyright (C) 2006 mic [ http://www.joomx.com ]
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*/

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );

require_once( $mainframe->getPath( 'toolbar_html' ) );

$task = mosGetParam( $_REQUEST, 'task', '');

switch($task) {
	case 'new':
	case 'edit':
		QI_Toolbar::_edit();
		break;

	case 'chooseIcon':
		QI_Toolbar::_chooseIcon();
		break;

	default:
		QI_Toolbar::_show();
		break;
}
?>