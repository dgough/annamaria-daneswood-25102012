<?php
/**
* @version $Id: install.customquickicons.php,v 1.6 2007/05/06 14:54:54 mic $
* @version 2.0.5
* @package Custom QuickIcons
* @copyright (C) 2006 mic [ http://www.joomx.com ]
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
* this installer is based upon a script by mic [ www.mgfi.info ] and should not be used anywhere outside!
* without express permission from the auhtor!
*/

// Dont allow direct linking
defined( '_VALID_MOS' ) or die( 'No Direct Access' );

function com_install() {
	global $database, $mosConfig_absolute_path, $mosConfig_live_site;
	global $mainframe;

	$queri		= array();
	$errors 	= array();
	$msg		= array();
	$dataSum	= '';

	$tmpLang 	= $GLOBALS['mosConfig_lang'];
	$pathLang 	= $mainframe->getCfg( 'absolute_path' ) . '/administrator/components/com_customquickicons/lang/';
	// tries to get adminlang (if defined -see MGFi www.mgfi.info)
	if( isset( $GLOBALS['mosConfig_alang'] ) && !empty( $GLOBALS['mosConfig_alang'] )){
        if( file_exists( $pathLang . $GLOBALS['mosConfig_alang'] . '.php' ) ) {
            $tmp_lng = $GLOBALS['mosConfig_alang'];
        }
    }
    if( file_exists( $pathLang . $tmpLang . '.php' ) ) {
    	include( $pathLang . $tmpLang . '.php' );
    }else{
    	include( $pathLang . 'english.php' );
    }

	// backup old table if existing
	$query = "SELECT count(id)"
	. "\n FROM #__custom_quickicons"
	;
	$database->setQuery( $query );
	$total = $database->loadResult();

	if( $total ) {
		$query = "RENAME TABLE #__custom_quickicons"
		. "\n TO #__custom_quickicons_bu_" . date( 'YmdHi' )
		;
		$database->setQuery( $query );
	    if( !$database->query() ) {
	        $errors[] = array( $database->getErrorMsg(), $query );
	    }else{
	        $dataSum++;
	        $msg[] = _QI_INST_MSG_BU_OLD_TABLE;
	    }
	}

    // create table
    $query = 'CREATE TABLE IF NOT EXISTS `#__custom_quickicons` ('
    . ' `id` int(11) NOT NULL auto_increment,'
    . ' `text` varchar(64) NOT NULL default \'\','
    . ' `target` varchar(255) NOT NULL default \'\','
    . ' `icon` varchar(100) NOT NULL default \'\','
    . ' `ordering` int(10) unsigned NOT NULL default \'0\','
    . ' `new_window` tinyint(1) NOT NULL default \'0\','
    . ' `prefix` varchar(30) NOT NULL default \'\','
    . ' `postfix` varchar(30) NOT NULL default \'\','
    . ' `published` tinyint(1) unsigned NOT NULL default \'0\','
    . ' `title` varchar(64) NOT NULL default \'\','
    . ' `cm_check` tinyint(1) NOT NULL default \'0\','
    . ' `cm_path` varchar(255) NOT NULL default \'\','
    . ' `akey` varchar(1) NOT NULL default \'\','
    . ' `display` tinyint(1) NOT NULL default \'0\','
    . ' `access` int(11) unsigned NOT NULL default \'0\','
    . ' `gid` int(3) default \'25\','
    . ' `checked_out` int(11) NOT NULL default \'0\','
    . ' PRIMARY KEY  (`id`)'
    . ') TYPE=MyISAM;'
    ;
	$database->setQuery( $query );
    if( !$database->query() ) {
        $errors[] = array( $database->getErrorMsg(), $query );
    }else{
        $dataSum++;
        $msg[] = _QI_INST_MSG_NEW_TABLE;
    }

    // now submit db.entries w lang.vars
    $queri[] = "INSERT INTO #__custom_quickicons VALUES ('', '" . _QI_INST_NEW_ARTICLE . "', 'index2.php?option=com_content&sectionid=0&task=new', 'module.png', 1, 0, '', '', 1, '" . _QI_INST_NEW_ARTICLE_ALT . "', '', '', 'N', '', '', '', '')";
    $queri[] = "INSERT INTO #__custom_quickicons VALUES ('', '" . _QI_INST_SECTIONS . "', 'index2.php?option=com_sections&scope=content', 'sections.png', 5, 0, '', '', 1, '" . _QI_INST_SECTIONS_ALT . "', '', '', 'B', '', '', '', '')";
    $queri[] = "INSERT INTO #__custom_quickicons VALUES ('', '" . _QI_INST_FRONTPAGE ."', 'index2.php?option=com_frontpage', 'frontpage.png', 4, 0, '', '', 1, '" . _QI_INST_FRONTPAGE_ALT . "', '', '', 'F', '', '', '', '')";
    $queri[] = " INSERT INTO #__custom_quickicons VALUES ('', '" . _QI_INST_ARTICLE ."', 'index2.php?option=com_content&sectionid=0', 'addedit.png', 2, 0, '', '', 1, '" . _QI_INST_ARTICLE_ALT ."', '', '', 'A', '', '', '', '')";
    $queri[] = "INSERT INTO #__custom_quickicons VALUES ('', '" . _QI_INST_ST_CONTENT . "', 'index2.php?option=com_typedcontent', 'addedit.png', 3, 0, '', '', 1, '" . _QI_INST_ST_CONTENT_ALT . "', '', '', 'S', '', '', '', '')";
    $queri[] = "INSERT INTO #__custom_quickicons VALUES ('', '" . _QI_INST_MEDIA . "', 'index2.php?option=com_media', 'mediamanager.png', 7, 0, '', '', 1, '" . _QI_INST_MEDIA_ALT ."', '', '', 'M', '', '', '', '')";
    $queri[] = "INSERT INTO #__custom_quickicons VALUES ('', '" . _QI_INST_CATEGORIES . "', 'index2.php?option=com_categories&section=content', 'categories.png', 6, 0, '', '', 1, '" . _QI_INST_CATEGORIES_ALT . "', '', '', 'K', '', '', '', '')";
    $queri[] = "INSERT INTO #__custom_quickicons VALUES ('', '" . _QI_INST_WASTE . "', 'index2.php?option=com_trash', 'trash.png', 8, 0, '', '', 1, '" . _QI_INST_WASTE_ALT . "', '', '', 'P', '', '', '', '')";
    $queri[] = "INSERT INTO #__custom_quickicons VALUES ('', '" . _QI_INST_MENUS . "', 'index2.php?option=com_menumanager', 'menu.png', 9, 0, '', '', 1, '" . _QI_INST_MENUS_ALT . "', '', '', 'R', '', '', '', '')";
    $queri[] = "INSERT INTO #__custom_quickicons VALUES ('', '" . _QI_INST_LANGUAGE . "', 'index2.php?option=com_languages', 'langmanager.png', 10, 0, '', '', 1, '" . _QI_INST_LANGUAGE_ALT . "', '', '', 'L', '', '', '', '')";
    $queri[] = "INSERT INTO #__custom_quickicons VALUES ('', '" . _QI_INST_USER . "', 'index2.php?option=com_users', 'user.png', 11, 0, '', '', 1, '" . _QI_INST_USER_ALT . "', '', '', 'U', '', '', '', '')";
    $queri[] = "INSERT INTO #__custom_quickicons VALUES ('', '" . _QI_INST_CONFIG . "', 'index2.php?option=com_config&hidemainmenu=1', 'config.png', 12, 0, '', '', 1, '" . _QI_INST_CONFIG_ALT . "', '', '', 'C', '', '', '', '')";

    foreach( $queri AS $query ){
        $database->setQuery( $query );
        if( !$database->query() ) {
            $errors[] = array( $database->getErrorMsg(), $query );
        }else{
            $dataSum++;
        }
    }

    /**
     * copy and install module
     * 1. check if directory exists (normally should)
     * 2. check perms
     * 3. copy
     * 4. register module
     * 5. delete install files/dir
     * 6. set standard module inactive
     */
    $modSuccess = false;

    $installdir	= $mainframe->getCfg('absolute_path') . '/administrator/components/com_customquickicons/modules/';
    $targetdir	= $mainframe->getCfg('absolute_path') . '/administrator/modules';
    if( !file_exists( $targetdir ) ) {
    	//create directory
    	if( !mkdir( $targetdir ) ) {
    		$errors[] = array( _QI_INST_ERR_TARGET_DIR, 'administrator/modules' );
    	} else {
    		chmod( $targetdir, 0777 );
    	}
    }else{
    	// get old perms for recovery
    	$OldPerms = fileperms( $targetdir );
    	// set temp perms
    	chmod( $targetdir, 0777 );
    }

    // step 3
    // check first if localized xml exists
    $toCopy = '';
    if( file_exists( $installdir . 'mod_customquickicons.xm_' . _QI_LNG ) ) {
    	$toCopy = 'mod_customquickicons.xm_' . _QI_LNG;
    }else{
    	$toCopy = 'mod_customquickicons.xm_en';
    }

    if( !copy( $installdir . 'mod_customquickicons.php' , $targetdir . '/mod_customquickicons.php' ) ) {
    	$errors[] = array( _QI_INST_ERR_COPY_MOD_FILE, 'mod_customquickicons.php' );
    }else{
    	$msg[] = sprintf( _QI_INST_MSG_MOD_FILE, 'mod_customquickicons.php' );
    	$modSuccess = true;
    }

    if( !copy( $installdir . $toCopy, $targetdir . '/mod_customquickicons.xml' ) ) {
    	$errors[] = array( _QI_INST_ERR_COPY_MOD_FILE, 'mod_customquickicons.xml' );
    	$modSuccess = false;
    }else{
    	$msg[] = sprintf( _QI_INST_MSG_MOD_FILE, 'mod_customquickicons.xml' );
    	$modSuccess = true;
    }

    // step 4
    // register the module and set active
    if( $modSuccess ) {
		$query = "INSERT INTO #__modules VALUES ('', 'CQI - CustomQuickIcons', '', 2, 'icon', 0, '0000-00-00 00:00:00', 1, 'mod_customquickicons', 0, 99, 1, '', 0, 1)";
		$database->setQuery( $query );
		if( !$database->query() ){
		    $errors[] = array( $database->getErrorMsg(), $query );
		}else{
			$dataSum++;
			$msg[] = _QI_INST_MSG_MOD_REGGED;
		}
    }else{
    	$msg[] = _QI_INST_MSG_MOD_REGGED;
    }

	// step 5
	$delFiles = mosReadDirectory( $installdir );
	foreach( $delFiles AS $delFile ) {
		@unlink( $installdir . $delFile );
	}
	@rmdir( $installdir );

	// reset perms
	chmod( $targetdir, $OldPerms );

	// disable standard module quick_icon (only if install was successful!)
	if( $modSuccess ) {
		$query = "SELECT id FROM #__modules WHERE module = 'mod_quickicon'";
		$database->setQuery( $query );
		$id = $database->loadResult();

		$query = 'UPDATE #__modules SET `published` = \'0\' WHERE id=' . $id . ' LIMIT 1;';
		$database->setQuery( $query );
		$database->query();
	}

    // finally delete unwanted dir at user side
    @rmdir( $mainframe->getCfg( 'absolute_path' ) . '/components/com_customquickicons' ); ?>

	<style type="text/css" media="screen">
        <!--
        .table {
            width       : 95%;
            border      : 1px solid #666666;
            text-align  : left;
            font-size   : 90%;
        }
        .text {
            color       : #666666;
            width       : 700px;
            text-align  : left;
            margin-left : 3px;
        }
        .code {
            width       : 700px;
            white-space : pre;
            margin      : 5px;
            text-align  : left;
            border      : 1px solid #FF0000;
        }
        .bold {
            font-weight : bold;
        }
        .ads {
            white-space : pre;
            border      : 1px solid #7EA9C2; /* 336699 */
            padding     : 10px;
            margin      : auto;
            width       : 750px;
            background  : #F9FDFF;
            text-align  : center;
        }
        .hint {
            background  : #F3F9FF; // FFDDDD;
            border      : 1px solid #006699; // FF0000;
            margin      : 10px;
            padding     : 10px;
        }
        -->
    </style>

    <div class="table">
    	<div class="text">
            <?php
            if( $errors ){
                echo '<strong style="color:red;">' . _QI_INST_ERROR . '</strong>';
                echo '<ul>';
                foreach( $errors AS $error ){
                    echo '<li>' . $error[0] . '</li>';
                }
                echo '</ul>';
            }

            echo '<strong style="color:green;">' . _QI_INST_SUCCESS . '</strong>';
            echo ' - ' . $dataSum . ' ' . _QI_INST_DB_ENTRIES;
            if( $msg ) {
            	echo '<ul>';
                foreach( $msg AS $mgs ){
                    echo '<li>' . $mgs . '</li>';
                }
                echo '</ul>';
            } ?>
        </div>
        <div class="text">
        	<ul>
        		<li>
        			<a href="http://joomlacode.org/gf/project/joomx/" title="<?php echo _QI_INST_ALT_WEBSITE; ?>" target="_blank"><?php echo _QI_INST_ALT_WEBSITE; ?></a>
        		</li>
        		<li>
        			<a href="http://joomlacode.org/gf/project/joomx/" title="<?php echo _QI_INST_ALT_ACT_VERSION; ?>" target="_blank"><?php echo _QI_INST_ALT_ACT_VERSION; ?></a>
        		</li>
        		<li>
        			<a href="http://joomlacode.org/gf/project/joomx/" title="<?php echo _QI_INST_ALT_BUGTRACKER; ?>" target="_blank"><?php echo _QI_INST_ALT_BUGTRACKER; ?></a>
        		</li>
        		<li>
        			<a href="http://joomlacode.org/gf/project/joomx/" title="<?php echo _QI_INST_ALT_FORUM; ?>" target="_blank"><?php echo _QI_INST_ALT_FORUM; ?></a>
        		</li>
        		<li>
        			<a href="mailto:info@joomx.com" title="<?php echo _QI_INST_ALT_EMAIL; ?>"><?php echo _QI_INST_ALT_EMAIL; ?></a>
        		</li>
        	</ul>
        </div>
        <div class="text">
            <?php echo _QI_INST_TXT1; ?>
        </div>
		<div class="hint">
			<?php
        	if( !$errors ) {
        		echo _QI_INST_TXT2;
        	} ?>
        	&nbsp;&nbsp;
	        Another professional tool from <a href="http://www.joomx.com" target="_blank" title="www.joomx.com">JoomX - Joomla Professionals @ Work</a>
		</div>
    </div>
    <?php
}
?>