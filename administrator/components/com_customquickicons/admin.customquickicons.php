<?php
/**
* @version $Id: admin.customquickicons.php,v 1.6 2007/05/03 09:55:55 mic $
* @version 2.0.4
* @package Custom QuickIcons
* @copyright (C) 2007 mic [ http://www.joomx.com ]
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*/

/** ensure this file is being included by a parent file */
defined( '_VALID_MOS' ) or die( 'No Direct Access' );

require_once( $mainframe->getPath( 'admin_html' ) );
require_once( $mainframe->getPath( 'class' ) );

//$task = mosGetParam( $_REQUEST, 'task', '' );
$id 	= mosGetParam( $_REQUEST, 	'id', 	NULL );
$cid 	= mosGetParam( $_POST, 		'cid', 	array(0) );
if( !is_array( $cid )) {
	$cid = array(0);
}

// language
if( !defined( '_LANG_QUICKICONS' )){
    $lngPath    = $GLOBALS['mosConfig_absolute_path'].'/administrator/components/com_customquickicons/lang/';
    $tmp_lng    = $GLOBALS['mosConfig_lang'];
    if( isset( $GLOBALS['mosConfig_alang'] ) && !empty( $GLOBALS['mosConfig_alang'] )){
        if( file_exists( $lngPath . $GLOBALS['mosConfig_alang'] . '.php' )){
            $tmp_lng = $GLOBALS['mosConfig_alang'];
        }
    }
    if( file_exists( $lngPath . $tmp_lng . '.php' )){
        include_once( $lngPath . $tmp_lng . '.php' );
    }else{
        // preferred language (here german .. could be also another one like chinese ?)
        if( file_exists( $lngPath . 'german.php' )){
            $tmp_lng = 'german.php';
        }else{
            $tmp_lng = 'english.php';
        }
        include_once( $lngPath . $tmp_lng );
    }
}

switch ($task) {
	case 'new':
		editIcon(null, $option);
		break;

	case 'edit':
		editIcon($id, $option);
		break;

	case 'delete':
		deleteIcon($cid, $option);
		break;

	case 'save':
		saveIcon(1, $option);
		break;

	case 'apply':
		saveIcon(0, $option);
		break;

	case 'publish':
		changeIcon($cid, 1, $option);
		break;

	case 'unpublish':
		changeIcon($cid, 0, $option);
		break;

	case 'orderUp':
		orderIcon($id, -1, $option);
		break;

	case 'orderDown':
		orderIcon($id, 1, $option);
		break;

	case 'chooseIcon':
		chooseIcon($option);
		break;

	case 'saveorder':
		saveOrder( $cid, $option );
		break;

	case 'about':
		HTML_QuickIcons::_support();
		break;

	default:
		show($option);
    	break;
}

// show the Items
function show( $option ){
	global $database, $mainframe, $mosConfig_list_limit;

	$limit 		= intval( $mainframe->getUserStateFromRequest( 'viewlistlimit', 'limit', $mosConfig_list_limit ) );
	$limitstart = intval( $mainframe->getUserStateFromRequest( "view{$option}limitstart", 'limitstart', 0 ) );
	$search 	= $mainframe->getUserStateFromRequest( "search{$option}", 'search', '' );
	$search 	= $database->getEscaped( trim( strtolower( $search ) ) );

	$where = array();

	if ( $search ) {
		$where[] = "LOWER( a.text ) LIKE '%$search%' OR LOWER( a.target ) LIKE '%$search%' OR LOWER( a.cm_path ) LIKE '%$search%'";
	}

	// check if module is installed and published at correct position
	$query = "SELECT id, position"
	. "\n FROM #__modules"
	. "\n WHERE module = 'mod_customquickicons'"
	;
	$database->setQuery( $query );
	$rows = $database->loadObjectList();

	if( !$rows[0]->id ) {
		echo "<script type=\"text/javascript\">alert('" . _QI_ERR_NO_MOD_INSTALLED . "');</script>\n";
	}

	if( $rows[0]->position != 'icon' ) {
		echo "<script type=\"text/javascript\">alert('" . html_entity_decode( _QI_ERR_MOD_INCORR_POS) . "');</script>\n";
	}

	// get the total number of records
	$query = "SELECT COUNT(*)"
	. "\n FROM #__custom_quickicons AS a"
	. ( count( $where ) ? "\n WHERE " . implode( ' AND ', $where ) : '' )
	;
	$database->setQuery( $query );
	$total = $database->loadResult();

	require_once( $GLOBALS['mosConfig_absolute_path'] . '/administrator/includes/pageNavigation.php' );
	$pageNav = new mosPageNav( $total, $limitstart, $limit );

	// Load Items
	$query = "SELECT a.*, g.name AS groupname"
	. "\n FROM #__custom_quickicons AS a"
	. "\n LEFT JOIN #__core_acl_aro_groups AS g ON g.group_id = a.gid"
	. ( count( $where ) ? "\n WHERE " . implode( ' AND ', $where ) : '' )
	. "\n ORDER BY ordering"
	;
	$database->setQuery( $query );
	$rows = $database->loadObjectList();

	// Output
	HTML_QuickIcons::show( $rows, $option, $search, $pageNav );
}

// Function to edit the Item
function editIcon($id, $option) {
	global $database, $my, $acl;

	// Load Item
	$row = new CustomQuickIcons();
	$row->load($id);

	if( $id ) {
		// do stuff for existing records
		$row->checkout( $my->id );
	} else {
		// do stuff for new records
		// $row->imagepos 	= 'top'; // what for is this [mic]
		$row->ordering 	= 0;
		$row->published = 1;
	}

	$query = "SELECT ordering AS value, text AS text"
	. "\n FROM #__custom_quickicons"
	. "\n ORDER BY ordering"
	;
	$lists['ordering']	= mosAdminMenus::SpecificOrdering( $row, $id, $query, 1 );

	// build the html select list for the components
	$query = "SELECT CONCAT_WS( ' ', link, admin_menu_link ) AS value, name AS text, id, parent"
	. "\n FROM #__components"
	. "\n WHERE link != ''"
	. "\n OR admin_menu_link != ''"
	. "\n ORDER BY id, parent"
	;
	$lists['components']	= mosAdminMenus::SpecificOrdering( $row, $id = true, $query, 1 ); // id special handling

	// get list of menu entries in all menus
    $query = "SELECT admin_menu_link AS value, CONCAT_WS( ' :: ', name, `option` ) AS text"
    . "\n FROM #__components"
	. "\n WHERE admin_menu_link != ''"
	. "\n AND (parent = 0 OR parent = 1)"
	. "\n ORDER BY name"
	;
    $database->setQuery( $query );
    $targets = $database->loadObjectList();
    $lists['targets'] = mosHTML::selectList( $targets, 'tar_gets', 'id="tar_gets" class="inputbox" size="1"', 'value', 'text', NULL );

	// components (with name) for check
	$query = "SELECT name AS value, CONCAT_WS( ' :: ', `option`, name ) AS text"
	. "\n FROM #__components"
	. "\n WHERE parent = '0'"
	. "\n AND `option` != ''"
	. "\n ORDER BY name"
	;
	$database->setQuery( $query );
    $ccheck = $database->loadObjectList();
	$lists['components_check'] = mosHTML::selectList( $ccheck, 'ccheck', 'id="ccheck" class="inputbox" size="1"', 'value', 'text', NULL );

	// list for usergroups
	$my_group = strtolower( $acl->get_group_name( $row->gid, 'ARO' ) );
	if( $my_group == 'siteowner' ) {
		$lists['gid'] = '<input type="hidden" name="gid" value="'. $my->gid .'" /><strong>Site Owner</strong>';
	} else {
		// ensure user can't add group higher than themselves
		$my_groups = $acl->get_object_groups( 'users', $my->id, 'ARO' );
		if (is_array( $my_groups ) && count( $my_groups ) > 0) {
			$ex_groups = $acl->get_group_children( $my_groups[0], 'ARO', 'RECURSE' );
		} else {
			$ex_groups = array();
		}

		// add unwanted groups to be removed below
		$ex_groups[] = '29'; // Public Frontend
		$ex_groups[] = '18'; // Registered
		$ex_groups[] = '19'; // Author
		$ex_groups[] = '20'; // Editor
		$ex_groups[] = '21'; // Publisher
		$ex_groups[] = '30'; // Public Backend

		$gtree = $acl->get_group_children_tree( null, 'USERS', false );

		// remove users 'above' me and unwanted groups as defined above
		$i = 0;
		while( $i < count( $gtree ) ) {
			if( in_array( $gtree[$i]->value, $ex_groups ) ) {
				array_splice( $gtree, $i, 1 );
			}else{
				$i++;
			}
		}
		$lists['gid'] = mosHTML::selectList( $gtree, 'gid', 'size="4"', 'value', 'text', $row->gid );
	}

	// display
	$display[] = mosHTML::makeOption( '',	_QI_DISPLAY_ICON_TEXT );
	$display[] = mosHTML::makeOption( '1',	_QI_DISPLAY_TEXT );
	$display[] = mosHTML::makeOption( '2',	_QI_DISPLAY_ICON );
	$display[] = mosHTML::makeOption( '3',	_QI_DISPLAY_SEPERATOR );
	$display[] = mosHTML::makeOption( '4',	_QI_DISPLAY_SEPERATOR_TEXT );

	$lists['display'] = mosHTML::selectList( $display, 'display', 'class="inputbox" size="1"', 'value', 'text', $row->display );

	HTML_QuickIcons::edit( $row, $lists, $option );
}

// Publish an Item
function changeIcon($cid, $action, $option) {
	global $database;

	if (!is_array( $cid ) || count( $cid ) < 1) {
		$errMsg = $action ? _QI_ERR_SELECT_PBL : _QI_ERR_SELECT_UPBL;
		echo "<script> alert('" . $errMsg . "'); window.history.go(-1);</script>\n";
		exit();
	}

	$cids = implode( ',', $cid );

	$query = "UPDATE #__custom_quickicons"
	. "\n SET published = $action"
	. "\n WHERE id IN ( $cids )"
	;
	$database->setQuery( $query );
	if (!$database->query()) {
		echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
		exit();
	}

	mosRedirect( 'index2.php?option=' . $option );
}

// Save Icon
function saveIcon( $redirect, $option ){
	global $database;

	// Get Infos
	$row = new CustomQuickIcons();
	if (!$row->bind( $_POST )) {
		echo "<script> alert('1 " . html_entity_decode( $row->getError() ) . "'); window.history.go(-1); </script>\n";
		exit();
	}

	// pre-save checks
	if (!$row->check()) {
		echo "<script> alert('" . html_entity_decode( $row->getError() ) . "'); window.history.go(-1); </script>\n";
		exit();
	}

	if( ($row->target == 'index2.php?option=' || !$row->target) && $row->display < 3){
		$row->published = 0;
	}

	// save the changes
	if (!$row->store()) {
		echo "<script> alert('3 " . html_entity_decode( $row->getError() ) . "'); window.history.go(-1); </script>\n";
		exit();
	}
	$row->checkin();
	$row->updateOrder();

	if( $redirect )
		mosRedirect( 'index2.php?option=' . $option );
	else
		mosRedirect( 'index2.php?option=' . $option . '&amp;task=edit&amp;id=' . $row->id );
}

// Reorder an Item
function orderIcon($id, $inc, $option) {
	global $database;

	// Cleaning ordering
	$query = "SELECT id, ordering"
	. "\n FROM #__custom_quickicons"
	. "\n ORDER BY ordering"
	;
	$database->setQuery($query);
	$rows = $database->loadObjectList();

	$i = 0;
	foreach( $rows as $row ){
		$query = "UPDATE #__custom_quickicons"
		. "\n SET ordering = $i"
		. "\n WHERE id = " . $row->id
		;
		$database->setQuery($query);
		$database->query();
		$i++;
	}

	$query = "SELECT ordering"
	. "\n FROM #__custom_quickicons"
	. "\n WHERE id = $id"
	;
	$database->setQuery($query);
	$database->loadObject($row);

	if ($row) {
		$newOrder = $row->ordering + $inc;

		$query = "SELECT id"
		. "\n FROM #__custom_quickicons"
		. "\n WHERE ordering = $newOrder"
		;
		$database->setQuery($query);
		$database->loadObject($row2);

		if ($row2) {
			$query = "UPDATE #__custom_quickicons"
			. "\n SET ordering = $newOrder"
			. "\n WHERE id = $id"
			;
			$database->setQuery($query);
			if (!$database->query()) {
				echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
				exit();
			}

			$query = "UPDATE #__custom_quickicons"
			. "\n SET ordering = " . $row->ordering
			. "\n WHERE id = " . $row2->id
			;
			$database->setQuery($query);
			if (!$database->query()) {
				echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
				exit();
			}
		}

		mosRedirect( 'index2.php?option=' . $option );
	}else{
		// for debug
		var_dump($row);
		exit;
		// end for debug
		mosRedirect( 'index2.php?option=' . $option );
	}
}

/* This feature (save order) is added by Eric C. Thanks Eric! */
//Save ordering of icons
function saveOrder( &$cid, $option ) {
	global $database;

	$total		= count( $cid );
	$order 		= mosGetParam( $_POST, 'order', array(0) );

	for( $i=0; $i < $total; $i++ ) {
		$query = "UPDATE #__custom_quickicons"
		. "\n SET ordering = $order[$i]"
		. "\n WHERE id = $cid[$i]"
		;
		$database->setQuery( $query );
		if (!$database->query()) {
			echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
			exit();
		}

		// update ordering
		$row = new CustomQuickicons( $database );
		$row->load( $cid[$i] );
		$row->updateOrder();
	}

	$msg 	= _QI_MSG_NEW_ORDER_SAVED;
	mosRedirect( 'index2.php?option=' . $option, $msg );
} // saveOrder

// Delete icons
function deleteIcon( &$cid, $option) {
	global $database;

	if( count( $cid )) {
		$cids = implode( ',', $cid );

		$query = "DELETE FROM #__custom_quickicons"
		. "\n WHERE id IN ( $cids )"
		;
		$database->setQuery( $query );
		if (!$database->query()) {
			echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
		}
	}

	$msg = _QI_MSG_SUC_DELETED;
	mosRedirect( 'index2.php?option=' . $option, $msg );
}

function chooseIcon( $option ){

	$handle = opendir( 'images/icons/' );

	$imgs = array();
	while( $file = readdir( $handle )){
		if (strpos($file, '.jpg') || strpos($file, '.jpeg') || strpos($file, '.gif') || strpos($file, '.png')){
			$imgs[] = $file;
		}
	}

	closedir( $handle );
	
	sort($imgs);

	HTML_QuickIcons::chooseIcon( $imgs, $option );
}
?>
