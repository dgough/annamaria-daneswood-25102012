<?php
/**
* @version $Id: toolbar.customquickicons.html.php,v 1.3 2006/12/06 09:23:23 mic $
* @version 2.0.1
* @package Custom QuickIcons
* @copyright (C) 2006 mic [ http://www.joomx.com ]
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*/

/** ensure this file is being included by a parent file */
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * @package Custom QuickIcons
 */
class QI_Toolbar {

	function _edit() {
		mosMenuBar::startTable();
		mosMenuBar::save('save', _QI_SAVE);
		mosMenuBar::spacer();
		mosMenuBar::apply('apply', _QI_APPLY);
		mosMenuBar::spacer();
		mosMenuBar::cancel('', _QI_CANCEL);
		mosMenuBar::help( 'screen.cquickicons.config_' . _QI_LNG . '.html', true );
		mosMenuBar::endTable();
	}

	function _show() {
		mosMenuBar::startTable();
		mosMenuBar::publishList('publish', _QI_PUBLISH);
		mosMenuBar::spacer();
		mosMenuBar::unpublishList('unpublish', _QI_UNPUBLISH);
		mosMenuBar::spacer();
		mosMenuBar::addNew('new',_QI_NEW);
		mosMenuBar::spacer();
		mosMenuBar::editList('edit', _QI_EDIT);
		mosMenuBar::spacer();
		mosMenuBar::deleteList('', 'delete', _QI_DELETE);
		mosMenuBar::endTable();
	}
	
	function _chooseIcon(){
		mosMenuBar::startTable();
		mosMenuBar::endTable();
	}
}
?>