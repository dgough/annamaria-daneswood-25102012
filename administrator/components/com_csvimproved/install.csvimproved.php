<?php
/**
 * Installation file for CSV Improved
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: install.csvimproved.php 386 2008-07-26 01:45:03Z Suami $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * Joomla installer
 *
 * The Joomla installer calls this function on installation, here process
 * some tasks to prepare for use
 */
function com_install() {
	global $database, $mosConfig_absolute_path, $mosConfig_lang, $mosConfig_live_site;
	
	/* Include the language file */
	if (file_exists($mosConfig_absolute_path."/administrator/components/com_csvimproved/languages/csvi_".$mosConfig_lang.".php")) {
		require_once($mosConfig_absolute_path."/administrator/components/com_csvimproved/languages/csvi_".$mosConfig_lang.".php");
	}
	else require_once($mosConfig_absolute_path."/administrator/components/com_csvimproved/languages/csvi_english.php");
	
	/* Check if a csvi_configuration table exists */
	$database->setQuery("SHOW TABLES LIKE '%csvi_configuration'");
	$database->query();
	if ($database->getAffectedRows() == 1) {
		/* Change field_id to id for 0.8 RC 1 */
		$database->setQuery("DESCRIBE #__csvi_configuration 'field_id'");
		$database->query();
		if ($database->getAffectedRows() == 1) {
			$database->setQuery("ALTER TABLE #__csvi_configuration CHANGE `field_id` `id` INT( 11 ) NOT NULL AUTO_INCREMENT");
			$database->query();
		}
		
		/* Add some new fields for 0.8 RC 1 */
		$database->setQuery("DESCRIBE #__csvi_configuration 'published'");
		$database->query();
		if ($database->getAffectedRows() == 0) {
			/* Add published fields */
			$database->setQuery("ALTER TABLE #__csvi_configuration ADD `published` TINYINT(1) UNSIGNED NOT NULL default '0'");
			$database->query();
			
			/* Publish existing fields */
			$database->setQuery("UPDATE #__csvi_configuration SET published = 1");
			$database->query();
		}
		$database->setQuery("DESCRIBE #__csvi_configuration 'checked_out'");
		$database->query();
		if ($database->getAffectedRows() == 0) {
			$database->setQuery("ALTER TABLE #__csvi_configuration ADD `checked_out` INT(10) UNSIGNED NOT NULL default '0'");
			$database->query();
		}
		
		$database->setQuery("DESCRIBE #__csvi_configuration 'field_ordering'");
		$database->query();
		if ($database->getAffectedRows() == 1) {
			$database->setQuery("ALTER TABLE #__csvi_configuration CHANGE COLUMN `field_ordering` `field_order` INT(3) NOT NULL DEFAULT 0;");
			$database->query();
		}
		
		/* 	Change the mf_name field to manufacturer_name */
		$database->setQuery("UPDATE #__csvi_configuration SET `field_name` = 'manufacturer_name' WHERE `field_name` = 'mf_name'");
		$database->query();
	}
	else {
		$database->setQuery("CREATE TABLE IF NOT EXISTS `#__csvi_configuration` (
					  `id` int(11) NOT NULL auto_increment,
					  `field_template_id` int(11) NOT NULL default '0',
					  `field_name` varchar(128) NOT NULL default '',
					  `field_column_header` varchar(128) NOT NULL default '',
					  `field_default_value` text,
					  `field_order` int(3) NOT NULL default '0',
					  `published` tinyint(1) unsigned NOT NULL default '0',
					  `checked_out` int(10) unsigned NOT NULL default '0',
					  PRIMARY KEY  (`id`)
					) COMMENT='Field settings for CSV Improved Ex-/Import'");
		$database->query();
	}
	
	/** 
	 * Add the database setup
	 * Check the number of columns on the csvi_templates table
	 * 11 fields is version 0.8b1, 0.8b2, 0.8b2.1 Add 2 missing fields
	 */
	$database->setQuery("DESCRIBE #__csvi_templates");
	$database->query();
	if ($database->getAffectedRows() == 11) {
		$database->setQuery("ALTER TABLE #__csvi_templates ADD `export_type` VARCHAR(10) NOT NULL default 'csv'");
		$database->query();
		$database->setQuery("ALTER TABLE #__csvi_templates ADD `export_site` VARCHAR(255) default NULL");
		$database->query();
	}
	
	/* Add some more fields for 0.8 beta 5 */
	$database->setQuery("ALTER TABLE #__csvi_templates ADD `template_type` VARCHAR(50) NOT NULL default '' AFTER `template_name`");
	$database->query();
	$database->setQuery("ALTER TABLE #__csvi_templates ADD `thumb_width` INT(4) default 0");
	$database->query();
	$database->setQuery("ALTER TABLE #__csvi_templates ADD `thumb_height` INT(4) default 0");
	$database->query();
	$database->setQuery("ALTER TABLE #__csvi_templates ADD `shopper_group_id` INT(4) default NULL");
	$database->query();
	$database->setQuery("ALTER TABLE #__csvi_templates ADD `producturl_suffix` VARCHAR(255) default NULL");
	$database->query();
	$database->setQuery("ALTER TABLE #__csvi_templates ADD `file_location` VARCHAR(255) default NULL");
	$database->query();
	$database->setQuery("ALTER TABLE #__csvi_templates ADD `product_publish` VARCHAR(1) default NULL");
	$database->query();
	
	/* Remove field_included, this is dropped as of 0.8 beta 5 */
	$database->setQuery("ALTER TABLE `#__csvi_configuration` DROP `field_included`");
	$database->query();
	
	/* Add new system limit fields as of 0.9 beta 1 */
	$database->setQuery('ALTER TABLE #__csvi_templates ADD COLUMN `max_execution_time` INT(4) AFTER `product_publish`,
	 ADD COLUMN `max_input_time` INT(4) AFTER `max_execution_time`,
	 ADD COLUMN `memory_limit` INT(4) AFTER `max_input_time`,
	 ADD COLUMN `post_max_size` INT(4) AFTER `memory_limit`,
	 ADD COLUMN `upload_max_filesize` INT(4) AFTER `post_max_size`');
	$database->query();
	
	/* Add export filename field as of 0.9 beta 1 */
	$database->setQuery('ALTER TABLE #__csvi_templates ADD COLUMN `export_filename` VARCHAR(255) AFTER `upload_max_filesize`');
	$database->query();
	
	/* Add manufacturer field as of 0.9 beta 1 */
	$database->setQuery('ALTER TABLE #__csvi_templates ADD COLUMN `manufacturer` VARCHAR(255) AFTER `export_filename`');
	$database->query();
	
	/* Rename some template types as they have changed */
	$database->setQuery("UPDATE #__csvi_templates SET template_type = 'ProductExport' WHERE template_type = 'RegularExport'");
	$database->query();
	
	$database->setQuery("UPDATE #__csvi_templates SET template_type = 'ProductImport' WHERE template_type = 'NormalUpload'");
	$database->query();
	
	 /* List of standard templates */
	$standardtemplates = array('CSVI Google Base Export XML',
						'CSVI Product Import',
						'CSVI Product files import',
						'CSVI Multiple Prices import',
						'CSVI Manufacturer details import',
						'CSVI Category details import', 
						'CSVI Category details export',
						'CSVI Multiple Prices export',
						'CSVI Product files export',
						'CSVI Product Export');
	
	/* Get a list of templates */
	$database->setQuery("SELECT template_name FROM #__csvi_templates");
	$database->query();
	$installedtemplates = $database->loadResultArray();
	
	/* Check if there are templates not yet */
	$installtemplates = array_diff($standardtemplates, $installedtemplates);
	
	/* Set the headers for template insert */
	$templateinsert = "INSERT INTO `#__csvi_templates` (`template_name`, `template_type`, `skip_first_line`, `use_column_headers`, `collect_debug_info`, `overwrite_existing_data`, `skip_default_value`, `show_preview`, `include_column_headers`, `text_enclosure`, `field_delimiter`, `export_type`, `export_site`, `thumb_width`, `thumb_height`, `shopper_group_id`, `producturl_suffix`, `file_location`, `product_publish`) VALUES ";
	
	/* Standard templates data */
	$templatedata = array();
	$templatedata[] = "('CSVI Google Base Export XML', 'ProductExport', 0, 1, 1, 1, 0, 0, 1, '~', '^', 'xml', 'froogle', 0, 0, NULL, NULL, '', '')";
	$templatedata[] = "('CSVI Product Import', 'ProductImport', 1, 0, 0, 1, 0, 1, 0, '~', '^', 'csv', '', 0, 0, 0, '', '', '')";
	$templatedata[] = "('CSVI Product Export', 'ProductExport', 0, 0, 0, 0, 0, 0, 1, '~', '^', 'csv', '', 0, 0, 0, '', '', '')";
	$templatedata[] = "('CSVI Product files import', 'ProductFilesUpload', 1, 0, 0, 1, 0, 1, 0, '~', '^', 'csv', '', 0, 0, 0, '', '', '')";
	$templatedata[] = "('CSVI Multiple Prices import', 'MultiplePricesUpload', 1, 0, 0, 1, 0, 1, 0, '~', '^', 'csv', '', 0, 0, 0, '', '', '')";
	$templatedata[] = "('CSVI Multiple Prices export', 'MultiplePricesExport', 0, 0, 0, 0, 0, 0, 1, '~', '^', 'csv', '', 0, 0, 0, '', '', '')";
	$templatedata[] = "('CSVI Manufacturer details import', 'ManufacturerImport', 1, 0, 0, 1, 0, 1, 0, '~', '^', 'csv', '', 0, 0, 0, '', '', '')";
	$templatedata[] = "('CSVI Category details import', 'CategoryDetailsImport', 1, 0, 0, 1, 0, 1, 0, '~', '^', 'csv', '', 0, 0, 0, '', '', '')";
	$templatedata[] = "('CSVI Category details export', 'CategoryDetailsExport', 0, 0, 0, 0, 0, 0, 1, '~', '^', 'csv', '', 0, 0, 0, '', '', '')";
	$templatedata[] = "('CSVI Product files export', 'ProductFilesExport', 1, 0, 0, 1, 0, 1, 1, '~', '^', 'csv', '', 0, 0, 0, '', '', '')";
	
	/* Set the headers for template field insert */
	$templatefieldinsert = "INSERT INTO `#__csvi_configuration` (`field_template_id`, `field_name`, `field_column_header`, `field_default_value`, `field_order`, `published`) VALUES ";
	
	/* Standard templatefields data */
	$templatefielddata[1][] = "({lastid}, 'product_name', 'title', '', 1, 1)";
	$templatefielddata[1][] = "({lastid}, 'product_url', 'link', 'http://www.<yoursite>.com/', 2, 1)";
	$templatefielddata[1][] = "({lastid}, 'product_desc', 'description', '', 3, 1)";
	$templatefielddata[1][] = "({lastid}, 'product_sku', 'g:id', '', 4, 1)";
	$templatefielddata[1][] = "({lastid}, 'picture_url', 'g:image_link', '', 5, 1)";
	$templatefielddata[1][] = "({lastid}, 'product_price', 'g:price', '', 6, 1)";
	$templatefielddata[1][] = "({lastid}, 'manufacturer_name', 'g:brand', 'Not Available', 7, 1)";
	$templatefielddata[1][] = "({lastid}, 'custom', 'g:condition', 'New', 8, 1)";
	$templatefielddata[1][] = "({lastid}, 'custom', 'g:payment_notes', '30 Days Money Back Guarantee', 9, 1)";
	$templatefielddata[1][] = "({lastid}, 'custom', 'g:expiration_date', 'None', 10, 1)";
	$templatefielddata[1][] = "({lastid}, 'product_width', 'g:width', 'Not Available', 11, 1)";
	$templatefielddata[1][] = "({lastid}, 'custom', 'g:payment_accepted', 'Check', 12, 1)";
	$templatefielddata[1][] = "({lastid}, 'custom', 'g:payment_accepted', 'Visa, 13, 1)";
	$templatefielddata[1][] = "({lastid}, 'custom', 'g:payment_accepted', 'MasterCard, 14, 1)";
	$templatefielddata[1][] = "({lastid}, 'custom', 'g:payment_accepted', 'AmericanExpress', 15, 1)";
	$templatefielddata[1][] = "({lastid}, 'custom', 'g:payment_accepted', 'Paypal', 16, 1)";
	$templatefielddata[1][] = "({lastid}, 'custom', 'g:payment_accepted', 'Money order', 17, 1)";
	$templatefielddata[1][] = "({lastid}, 'product_weight', 'g:weight', 'Not Available', 18, 1)";
	$templatefielddata[1][] = "({lastid}, 'product_length', 'g:lenght', 'Not Available', 19, 1)";
	$templatefielddata[1][] = "({lastid}, 'custom', 'g:price_type', 'Non Negotiable', 20, 1)";
	$templatefielddata[1][] = "({lastid}, 'product_in_stock', 'g:quantity', 'Not Available', 21, 1)";
	$templatefielddata[1][] = "({lastid}, 'custom', 'g:tax_region', 'Florida', 22, 1)";
	$templatefielddata[1][] = "({lastid}, 'custom', 'g:tax_percent', '6', 23, 1)";
	$templatefielddata[1][] = "({lastid}, 'attribute', 'g:tech_spec_link', 'http://www.<yoursite>.com/technical_specifications.html', 24, 1)";
	$templatefielddata[1][] = "({lastid}, 'custom', 'g:pickup', 'FALSE', 25, 1)";
	$templatefielddata[1][] = "({lastid}, 'custom', 'g:shipping', 'US:UPS Ground:5.00', 26, 1)";
	
	$templatefielddata[2][] = "({lastid}, 'category_path', '', '', 3, 1)";
	$templatefielddata[2][] = "({lastid}, 'product_availability', '', '', 10, 1)";
	$templatefielddata[2][] = "({lastid}, 'product_available_date', '', '', 11, 1)";
	$templatefielddata[2][] = "({lastid}, 'product_currency', '', 'EUR', 12, 1)";
	$templatefielddata[2][] = "({lastid}, 'product_desc', '', '', 6, 1)";
	$templatefielddata[2][] = "({lastid}, 'product_discount', '', '', 13, 1)";
	$templatefielddata[2][] = "({lastid}, 'product_discount_date_end', '', '', 14, 1)";
	$templatefielddata[2][] = "({lastid}, 'product_discount_date_start', '', '', 15, 1)";
	$templatefielddata[2][] = "({lastid}, 'product_full_image', '', '', 8, 1)";
	$templatefielddata[2][] = "({lastid}, 'product_name', '', '', 4, 1)";
	$templatefielddata[2][] = "({lastid}, 'product_packaging', '', '', 16, 1)";
	$templatefielddata[2][] = "({lastid}, 'product_parent_sku', '', '', 2, 1)";
	$templatefielddata[2][] = "({lastid}, 'product_price', '', '', 7, 1)";
	$templatefielddata[2][] = "({lastid}, 'product_publish', '', 'Y', 17, 1)";
	$templatefielddata[2][] = "({lastid}, 'product_s_desc', '', '', 5, 1)";
	$templatefielddata[2][] = "({lastid}, 'product_sku', '', '', 1, 1)";
	$templatefielddata[2][] = "({lastid}, 'product_special', '', '', 18, 1)";
	$templatefielddata[2][] = "({lastid}, 'product_tax', '', '', 19, 1)";
	$templatefielddata[2][] = "({lastid}, 'product_thumb_image', '', '', 9, 1)";
	$templatefielddata[2][] = "({lastid}, 'product_url', '', '', 20, 1)";
	
	$templatefielddata[3][] = "({lastid}, 'category_path', '', '', 3, 1)";
	$templatefielddata[3][] = "({lastid}, 'product_availability', '', '', 10, 1)";
	$templatefielddata[3][] = "({lastid}, 'product_available_date', '', '', 11, 1)";
	$templatefielddata[3][] = "({lastid}, 'product_currency', '', 'EUR', 12, 1)";
	$templatefielddata[3][] = "({lastid}, 'product_desc', '', '', 6, 1)";
	$templatefielddata[3][] = "({lastid}, 'product_discount', '', '', 13, 1)";
	$templatefielddata[3][] = "({lastid}, 'product_discount_date_end', '', '', 14, 1)";
	$templatefielddata[3][] = "({lastid}, 'product_discount_date_start', '', '', 15, 1)";
	$templatefielddata[3][] = "({lastid}, 'product_full_image', '', '', 8, 1)";
	$templatefielddata[3][] = "({lastid}, 'product_name', '', '', 4, 1)";
	$templatefielddata[3][] = "({lastid}, 'product_packaging', '', '', 16, 1)";
	$templatefielddata[3][] = "({lastid}, 'product_parent_sku', '', '', 2, 1)";
	$templatefielddata[3][] = "({lastid}, 'product_price', '', '', 7, 1)";
	$templatefielddata[3][] = "({lastid}, 'product_publish', '', 'Y', 17, 1)";
	$templatefielddata[3][] = "({lastid}, 'product_s_desc', '', '', 5, 1)";
	$templatefielddata[3][] = "({lastid}, 'product_sku', '', '', 1, 1)";
	$templatefielddata[3][] = "({lastid}, 'product_special', '', '', 18, 1)";
	$templatefielddata[3][] = "({lastid}, 'product_tax', '', '', 19, 1)";
	$templatefielddata[3][] = "({lastid}, 'product_thumb_image', '', '', 9, 1)";
	$templatefielddata[3][] = "({lastid}, 'product_url', '', '', 20, 1)";
	
	$templatefielddata[4][] = "({lastid}, 'product_files_file_description', '', '', 4, 1)";
	$templatefielddata[4][] = "({lastid}, 'product_files_file_name', '', '', 2, 1)";
	$templatefielddata[4][] = "({lastid}, 'product_files_file_published', '', '', 6, 1)";
	$templatefielddata[4][] = "({lastid}, 'product_files_file_title', '', '', 3, 1)";
	$templatefielddata[4][] = "({lastid}, 'product_files_file_url', '', '', 5, 1)";
	$templatefielddata[4][] = "({lastid}, 'product_sku', '', '', 1, 1)";
	
	$templatefielddata[5][] = "({lastid}, 'product_sku', '', '', 1, 1)";
	$templatefielddata[5][] = "({lastid}, 'product_currency', '', '', 3, 1)";
	$templatefielddata[5][] = "({lastid}, 'price_quantity_start', '', '', 4, 1)";
	$templatefielddata[5][] = "({lastid}, 'price_quantity_end', '', '', 5, 1)";
	$templatefielddata[5][] = "({lastid}, 'product_price', '', '', 2, 1)";
	
	$templatefielddata[6][] = "({lastid}, 'product_sku', '', '', 1, 1)";
	$templatefielddata[6][] = "({lastid}, 'product_currency', '', '', 3, 1)";
	$templatefielddata[6][] = "({lastid}, 'price_quantity_start', '', '', 4, 1)";
	$templatefielddata[6][] = "({lastid}, 'price_quantity_end', '', '', 5, 1)";
	$templatefielddata[6][] = "({lastid}, 'product_price', '', '', 2, 1)";
	
	$templatefielddata[7][] = "({lastid}, 'manufacturer_desc', '', '', 2, 1)";
	$templatefielddata[7][] = "({lastid}, 'manufacturer_email', '', '', 3, 1)";
	$templatefielddata[7][] = "({lastid}, 'manufacturer_name', '', '', 1, 1)";
	$templatefielddata[7][] = "({lastid}, 'manufacturer_category_name', '', '', 5, 1)";
	$templatefielddata[7][] = "({lastid}, 'manufacturer_url', '', '', 4, 1)";
	
	$templatefielddata[8][] = "({lastid}, 'category_path', '', '', 1, 1)";
	$templatefielddata[8][] = "({lastid}, 'category_description', '', '', 2, 1)";
	$templatefielddata[8][] = "({lastid}, 'category_thumb_image', '', '', 4, 1)";
	$templatefielddata[8][] = "({lastid}, 'category_full_image', '', '', 3, 1)";
	$templatefielddata[8][] = "({lastid}, 'category_publish', '', '', 8, 1)";
	$templatefielddata[8][] = "({lastid}, 'category_browsepage', '', '', 6, 1)";
	$templatefielddata[8][] = "({lastid}, 'category_products_per_row', '', '', 5, 1)";
	$templatefielddata[8][] = "({lastid}, 'category_flypage', '', '', 7, 1)";
	$templatefielddata[8][] = "({lastid}, 'category_list_order', '', '', 9, 1)";
	
	$templatefielddata[9][] = "({lastid}, 'category_path', '', '', 1, 1)";
	$templatefielddata[9][] = "({lastid}, 'category_description', '', '', 2, 1)";
	$templatefielddata[9][] = "({lastid}, 'category_thumb_image', '', '', 4, 1)";
	$templatefielddata[9][] = "({lastid}, 'category_full_image', '', '', 3, 1)";
	$templatefielddata[9][] = "({lastid}, 'category_publish', '', '', 8, 1)";
	$templatefielddata[9][] = "({lastid}, 'category_browsepage', '', '', 6, 1)";
	$templatefielddata[9][] = "({lastid}, 'category_products_per_row', '', '', 5, 1)";
	$templatefielddata[9][] = "({lastid}, 'category_flypage', '', '', 7, 1)";
	$templatefielddata[9][] = "({lastid}, 'category_list_order', '', '', 9, 1)";
	
	$templatefielddata[10][] = "({lastid}, 'product_files_file_description', '', '', 4, 1)";
	$templatefielddata[10][] = "({lastid}, 'product_files_file_name', '', '', 2, 1)";
	$templatefielddata[10][] = "({lastid}, 'product_files_file_published', '', '', 6, 1)";
	$templatefielddata[10][] = "({lastid}, 'product_files_file_title', '', '', 3, 1)";
	$templatefielddata[10][] = "({lastid}, 'product_files_file_url', '', '', 5, 1)";
	$templatefielddata[10][] = "({lastid}, 'product_sku', '', '', 1, 1)";
	
	
	foreach ($installtemplates as $key => $templatename) {
		/* Add the template */
		$database->setQuery($templateinsert.$templatedata[$key]);
		$database->query();
		
		$insertid = $database->insertid();
		/* Add the template data */
		foreach ($templatefielddata[$key+1] as $id => $fielddata) {
			$database->setQuery($templatefieldinsert.str_replace('{lastid}', $insertid, $fielddata));
			$database->query();
		}
	}
	
	
	/* Add the admin icon */
	$database->setQuery( "UPDATE #__components SET admin_menu_img = '../administrator/components/com_csvimproved/images/csvi_logo_16x16.png' WHERE admin_menu_link = 'option=com_csvimproved'");
	$database->query();
	
	/*l Modify the admin icons */
	$database->setQuery( "SELECT id FROM #__components WHERE name= 'CSV Improved'" );
	$id = $database->loadResult();
	
	/* Add the admin menu images */
	/* Templates */
	$database->setQuery( "UPDATE #__components SET admin_menu_img = '../administrator/components/com_csvimproved/images/csvi_templates_16x16.png', name = 'Templates' WHERE parent='$id' AND name = 'Templates'");
	$database->query();
	/* About */
	$database->setQuery( "UPDATE #__components SET admin_menu_img = '../administrator/components/com_csvimproved/images/csvi_about_16x16.png', name = 'About' WHERE parent='$id' AND name = 'About'");
	$database->query();
	/* Help */
	$database->setQuery( "UPDATE #__components SET admin_menu_img = '../administrator/components/com_csvimproved/images/csvi_help_16x16.png', name = 'Help' WHERE parent='$id' AND name = 'Help'");
	$database->query();
	/* Import */
	$database->setQuery( "UPDATE #__components SET admin_menu_img = '../administrator/components/com_csvimproved/images/csvi_import_16x16.png', name = 'Import' WHERE parent='$id' AND name = 'Import'");
	$database->query();
	/* Export */
	$database->setQuery( "UPDATE #__components SET admin_menu_img = '../administrator/components/com_csvimproved/images/csvi_export_16x16.png', name = 'Export' WHERE parent='$id' AND name = 'Export'");
	$database->query();
	/* Maintenance */
	$database->setQuery( "UPDATE #__components SET admin_menu_img = '../administrator/components/com_csvimproved/images/csvi_system_16x16.png', name = 'Maintenance' WHERE parent='$id' AND name = 'Maintenance'");
	$database->query();
	
	/* Copy the toolbar images */
	@copy($mosConfig_absolute_path.'/administrator/components/com_csvimproved/images/csvi_fields_32x32.png', $mosConfig_absolute_path.'/administrator/images/csvi_fields_32x32.png');
	@copy($mosConfig_absolute_path.'/administrator/components/com_csvimproved/images/csvi_continue_32x32.png', $mosConfig_absolute_path.'/administrator/images/csvi_continue_32x32.png');
	@copy($mosConfig_absolute_path.'/administrator/components/com_csvimproved/images/csvi_export_32x32.png', $mosConfig_absolute_path.'/administrator/images/csvi_export_32x32.png');
	@copy($mosConfig_absolute_path.'/administrator/components/com_csvimproved/images/csvi_import_32x32.png', $mosConfig_absolute_path.'/administrator/images/csvi_import_32x32.png');
	@copy($mosConfig_absolute_path.'/administrator/components/com_csvimproved/images/csvi_templates_32x32.png', $mosConfig_absolute_path.'/administrator/images/csvi_templates_32x32.png');

	?>
	<img src="<?php echo $mosConfig_live_site;?>/administrator/components/com_csvimproved/images/csvi_logo_name_48x244.png" />
	<br />
	version 0.9 RC 1 has been succesfully installed.
	<br /><br />
	<div style="color: #FF0000; font-weight: bold; font-size: 26px; text-align: center;">THIS IS BETA</div>
	<div id="donations" style="border: 1px solid #BB0000; float: left; padding: 5px; width: 25%;">
		<div id="donation_title" style="color: #000066; background: #FFFFFF; margin: 5px; float: left; margin-top: -15px;">Donations</div>
		<br clear="all" />
		Donations are kindly accepted through Paypal.
		<br /><br />
		<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
			<input type="hidden" name="cmd" value="_s-xclick">
			<input type="image" src="https://www.paypal.com/en_US/i/btn/x-click-but21.gif" border="0" name="submit" alt="Make payments with PayPal - it's fast, free and secure!">
			<img alt="" border="0" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
			<input type="hidden" name="encrypted" value="-----BEGIN PKCS7-----MIIHLwYJKoZIhvcNAQcEoIIHIDCCBxwCAQExggEwMIIBLAIBADCBlDCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20CAQAwDQYJKoZIhvcNAQEBBQAEgYBTCAeavz44WY0tlf+C73PtLDA8PgJ5XmDs1R9LRZS8aR3zpELKotpcuPVZntkimyLHoa0nrAQOFJZajADDxjfnt7dj1ieamZae9kPu3WIB/0FJThnjhaisRQCLlT9hyGFnqayjr0H3RP6lg5fIEEdp7LbjFWLL6l+N61cF4pZcmzELMAkGBSsOAwIaBQAwgawGCSqGSIb3DQEHATAUBggqhkiG9w0DBwQIJx98OzDEl6mAgYhrsx6oEo8ysBdbqygWyHwlRmHibOxrx20QlBK5RoWMvTai81F0At2TGPbGAb6WfGHHYyuQ0tx1S4gjVQb6S/Cerev/UDKkpAuxHXtujVXwNh8hbt1RRa1bg2xqqXzmqo+EeSq0s3xhIjebckkZeryS4x681z6flJtXgWsLPXYvYxlWJCsYUwo1oIIDhzCCA4MwggLsoAMCAQICAQAwDQYJKoZIhvcNAQEFBQAwgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tMB4XDTA0MDIxMzEwMTMxNVoXDTM1MDIxMzEwMTMxNVowgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDBR07d/ETMS1ycjtkpkvjXZe9k+6CieLuLsPumsJ7QC1odNz3sJiCbs2wC0nLE0uLGaEtXynIgRqIddYCHx88pb5HTXv4SZeuv0Rqq4+axW9PLAAATU8w04qqjaSXgbGLP3NmohqM6bV9kZZwZLR/klDaQGo1u9uDb9lr4Yn+rBQIDAQABo4HuMIHrMB0GA1UdDgQWBBSWn3y7xm8XvVk/UtcKG+wQ1mSUazCBuwYDVR0jBIGzMIGwgBSWn3y7xm8XvVk/UtcKG+wQ1mSUa6GBlKSBkTCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb22CAQAwDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQUFAAOBgQCBXzpWmoBa5e9fo6ujionW1hUhPkOBakTr3YCDjbYfvJEiv/2P+IobhOGJr85+XHhN0v4gUkEDI8r2/rNk1m0GA8HKddvTjyGw/XqXa+LSTlDYkqI8OwR8GEYj4efEtcRpRYBxV8KxAW93YDWzFGvruKnnLbDAF6VR5w/cCMn5hzGCAZowggGWAgEBMIGUMIGOMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExFjAUBgNVBAcTDU1vdW50YWluIFZpZXcxFDASBgNVBAoTC1BheVBhbCBJbmMuMRMwEQYDVQQLFApsaXZlX2NlcnRzMREwDwYDVQQDFAhsaXZlX2FwaTEcMBoGCSqGSIb3DQEJARYNcmVAcGF5cGFsLmNvbQIBADAJBgUrDgMCGgUAoF0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUxDxcNMDYxMTIzMTU1NjU0WjAjBgkqhkiG9w0BCQQxFgQU8JTXcYf6dwAInjJOVcAxzEAphxgwDQYJKoZIhvcNAQEBBQAEgYBpCn6xePu0tRVRJ+jK8ZArzoIizZhW/+PqyhFqJksal4MQF939lqSV1f/b7DPSVUbeOyGn8TbGBCedAtMeUYAiUfSheKM7Q4IizciUO0oArv2bnrb4+z9cZ0lIrO5CDPA1ziPPQyGdd11sdDUrKPe5lio3fYB0kUFHu14lw+/uSw==-----END PKCS7-----">
		</form>
		<br />
		<div id="donation_content" style="white-space: auto; width: 250px;">
			Building new features, give support, bugfixing, maintaining a website it is all part of the process to create CSV Improved. 
			Though it is done with much love and joy it does come at a price. The price is both monetary and time. To be able to continue
			expanding this great component donations make it possible to dedicate time to the project.
			<br /><br />
			<span style="font-weight: bold;">Donations work for you and for me.</span>
		</div>
	</div>
	<div id="csvimproved" style="border: 1px solid #BB0000; float: left; margin-left: 5px; padding: 5px; text-align: left; white-space: auto; width: 70%;">
		<div id="csvimproved_title" style="color: #000066; background: #FFFFFF; margin: 5px; float: left; margin-top: -15px;">CSV Improved</div>
		<br clear="all" />
		<div style="float: left;">
		Features:
		<ul>
			<li>CSV/XML/Excel import</li>
			<li>CSV/XML export</li>
			<li>Custom fields</li>
			<li>Templates</li>
			<li>Preview Import</li>
			<li>Google Base support</li>
			<li>Product URL export</li>
			<li>Image URL export</li>
			<li>Multilingual</li>
			<li>Template based field configuration</li>
			<li>Export to local file or download</li>
			<li>Manufacturer data import/export</li>
			<li>Export per shopper group</li>
			<li>Category details import/export</li>
			<li>Order export</li>
			<li>Cron support</li>
			<li>Custom database fields</li>
		</ul>
		</div>
		<div style="float: right;">
			<table border="0" cellpadding="2" cellspacing="2">
			<tr>
				<td>
				<div id="cpanel">
				<?php echo CpanelButton('csvi_templates_48x48.png', 'index2.php?option=com_csvimproved&amp;act=templates', $CsviLang->CP_TEMPLATES); ?>
				<?php echo CpanelButton('csvi_import_48x48.png', 'index2.php?option=com_csvimproved&amp;act=import', $CsviLang->CP_IMPORT); ?>
				<?php echo CpanelButton('csvi_export_48x48.png', 'index2.php?option=com_csvimproved&amp;act=export', $CsviLang->CP_EXPORT); ?>
				</div>
				</td>
			</tr>
			<tr>
				<td>
				<div id="cpanel">
				<?php echo CpanelButton('csvi_system_48x48.png', 'index2.php?option=com_csvimproved&amp;act=maintenance', $CsviLang->CP_MAINTENANCE); ?>
				<?php echo CpanelButton('csvi_help_48x48.png', 'http://www.csvimproved.com/wiki/doku.php/', $CsviLang->CP_HELP); ?>
				<?php echo CpanelButton('csvi_about_48x48.png', 'index2.php?option=com_csvimproved&amp;act=about', $CsviLang->CP_ABOUT); ?>
				</div>
				</td>
			</tr>
			</table>
		</div>
		<br clear="all" />
		Click on the control panel to start
		<br />
		<br />
		For support visit the website at <a href="http://www.csvimproved.com/" target="_new">www.csvimproved.com</a>.
	</div>
	<br clear="all" />
	<?php
}

function CpanelButton($image, $link, $title) {
	$cpanelbutton = '<div style="float:left;">';
	$cpanelbutton .= '	<div class="icon">';
	$cpanelbutton .= '		<a href="'.$link.'"';
	if (substr($link, 0, 4) == "http") $cpanelbutton .= ' target="_new"';
	$cpanelbutton .= '      >';
	$cpanelbutton .= '			'.mosAdminMenus::imageCheckAdmin( $image, '/administrator/components/com_csvimproved/images/', NULL, NULL, $title );
	$cpanelbutton .= '			<span>'.$title.'</span>';
	$cpanelbutton .= '		</a>';
	$cpanelbutton .= '	</div>';
	$cpanelbutton .= '</div>';
	return $cpanelbutton;
}
?>