<?php
/**
 * Processor for manufacturer data
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Import
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_class_manufacturer.php 226 2008-05-06 01:31:02Z Suami $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' ); 

/**
 * Main manufacturer controller
 *
 * @package CSVImproved
 * @subpackage Import
 *
 * @todo Manufacturer category if not set, set to 1
 * @todo Return correct statuses
 * @todo Integrate into Registry
 */
class CsviManufacturer {
	var $classname = 'CsviManufacturer';
	var $mf_update = 'name';
	var $current_mf_id = 0;
	var $csv_debug = array();
	var $debug = false;
	/** @var int contains a random number for debugging output */
	var $random = 0;
	
	/** 
	 * Csvi Manufacturer 
	 **/
	function CsviManufacturer() {
		
	}
	
	/** 
	 * Process Manufacturer data 
	 *
	 * This function does not create generic manufacturers when
	 * no name or ID exists. Creating generic manufacturers leads to creating 
	 * endless manufacturers on subsequent imports
	 *
	 * @param $product_id integer ID of the product
	 * @param &$csviregistry object Global register
	 */
	function ProcessManufacturer($product_id, &$csviregistry) {
		$db = $csviregistry->GetObject('database');
		$templates = $csviregistry->GetObject('templates');
		$product_data = $csviregistry->GetArray('product_data');
		$csv_fields = $csviregistry->GetArray('csv_fields');
		$csvilog = $csviregistry->GetObject('logger');
		$this->random = rand();
		
		$db_mf_name_id_count = 0;
		
		/* Check if we are updating by name or ID */
		if ($this->CheckUpdate($csviregistry)) {
			switch ($this->mf_update) {
				case 'name':
					/* Check if the manufacturer exist searching by name since it has preference */
					$condition = "mf_name='".$db->getEscaped(trim($product_data[$csv_fields["manufacturer_name"]["order"]]))."'";
					break;
				case 'id':
					/* Check if the manufacturer exist searching by id */
					$condition = "manufacturer_id='".$product_data[$csv_fields["manufacturer_id"]["order"]]."'";
					break;
			}
			$q_mf = "SELECT mf_name, manufacturer_id, mf_category_id FROM #__vm_manufacturer WHERE ".$condition." LIMIT 1";
			if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Check if manufacturer exists: <a onclick="switchMenu(\''.$product_id.'_mf_exists\');" title="Show/hide query">Show/hide query</a><div id="'.$product_id.'_mf_exists" class="debug">'.htmlentities($q_mf).'</div>');
			$db->query($q_mf);
			$db_mf_name_id_count = $db->num_rows();
			$this->current_mf_id = $db->f('manufacturer_id');
			
			/* Set the manufacturer data */
			if ($this->mf_update == 'name') $d['mf_name'] = $product_data[$csv_fields["manufacturer_name"]["order"]];
			else if(strlen(trim(($db->f('mf_name')))) > 0) $d['mf_name'] = $db->f('mf_name');
			else $d['mf_name'] = uniqid( "Generic Manufacturer_" );
			if (isset($csv_fields['manufacturer_desc'])) $d['mf_desc'] = $product_data[$csv_fields['manufacturer_desc']['order']];
			if (isset($csv_fields['manufacturer_email'])) $d['mf_email'] = $product_data[$csv_fields['manufacturer_email']['order']];
			if (isset($csv_fields['manufacturer_url'])) $d['mf_url'] = $product_data[$csv_fields['manufacturer_url']['order']];
			if (isset($csv_fields['manufacturer_category_name'])) {
				$mf_id_q = "SELECT mf_category_id FROM #__vm_manufacturer_category
						WHERE mf_category_name = '".$db->getEscaped($product_data[$csv_fields['manufacturer_category_name']['order']])."'";
				$db->query($mf_id_q);
				if ($db->f('mf_category_id') == 0) {
					$mf_id_q = "SELECT MIN(mf_category_id) as mf_category_id FROM #__vm_manufacturer_category";
					$db->query($mf_id_q);
				}
				$d['mf_category_id'] = $db->f("mf_category_id");
			}
			else {
				/* Manufacturer category is empty and there is no default value, see if there was one with the product */
				if ($db->f('mf_category_id') > 0) $d['mf_category_id'] = $db->f("mf_category_id");
				else {
					/* Manufacturer category is empty and there is no default value, use the first manufacturer category */
					$mf_id_q = "SELECT MIN(mf_category_id) as mf_category_id FROM #__vm_manufacturer_category";
					$db->query($mf_id_q);
					$d['mf_category_id'] = $db->f("mf_category_id");
				}
			}
			
			if ($db_mf_name_id_count == 0) {
				if ($this->AddManufacturer($csviregistry, $d)) $csvilog->AddStats('added', 'Line '.$csviregistry->currentline.': Manufacturer: '.$d['mf_name']);
				else $csvilog->AddStats('incorrect', 'Line '.$csviregistry->currentline.': <strong>Incorrect</strong> Manufacturer: '.$d['mf_name']." could not be added");
			}
			else {
				if ($this->UpdateManufacturer($csviregistry, $d)) $csvilog->AddStats('updated', 'Line '.$csviregistry->currentline.': <strong>Updated</strong> Manufacturer: '.$d['mf_name']);
				else $csvilog->AddStats('incorrect', 'Line '.$csviregistry->currentline.': <strong>Incorrect</strong> Manufacturer: '.$d['mf_name']." could not be added");
			}
			if ($product_id > 0) {
				/* Check if a product <--> manufacturer link exists */
				$product_relation = false;
				$q = "SELECT COUNT(product_id) AS mf_total FROM #__vm_product_mf_xref WHERE product_id = '".$product_id."'";
				$db->query($q);
				if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Check if product <---> manufacturer link exists: <a onclick="switchMenu(\''.$this->current_mf_id.'_'.$this->random.'_mf_prod_link\');" title="Show/hide query">Show/hide query</a><div id="'.$this->current_mf_id.'_'.$this->random.'_mf_prod_link" class="debug">'.htmlentities($q).'</div>');
				
				/* The product already has a product <--> manufacturer link, only update it */
				/* Each product should have only 1 manufacturer id */
				if ($db->f("mf_total") == 1) {
					$product_relation = true;
					/* Update the manufacturer ID if it is set */
					$q = "UPDATE #__vm_product_mf_xref SET manufacturer_id = ".$this->current_mf_id." ";
					$q .= "WHERE product_id = '".$product_id."'";
					if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Update manufacturer product cross reference: <a onclick="switchMenu(\''.$this->current_mf_id.'_'.$this->random.'_mf_prod_xref\');" title="Show/hide query">Show/hide query</a><div id="'.$this->current_mf_id.'_'.$this->random.'_mf_prod_xref" class="debug">'.htmlentities($q).'</div>');
					if (!$db->query($q)) if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', '<span class="sqlerror">SQL Error</span>: <a onclick="switchMenu(\''.$this->current_mf_id.'_'.$this->random.'_mf_prod_xref_error\');" title="Show/hide error">Show/hide error</a><div id="'.$this->current_mf_id.'_'.$this->random.'_manufacturer_id_error" class="debug">'.mysql_error().'</div>');
				}
				/* More than 1 link exists, inform the customer */
				else if ($db->f("mf_total") > 1) {
					$product_relation = true;
					$csvilog->AddStats('incorrect', 'Line '.$csviregistry->currentline.': More than 1 product <--> manufacturer link exists for product SKU '.$product_data[$csv_fields["product_sku"]["order"]]);
				}
				/* There is no product <--> manufacturer link, create it */
				else $product_relation = false;
				
				/* Create product <--> manufacturer link */
				if (!$product_relation) $this->AddXref($csviregistry, $product_id);
			}
		}
		else {
			/* There is no manufacturer name or ID in the uploaded file */
			if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'No manufacturer details found');
			/* There needs to be a manufacturer link otherwise user cannot update the product in VirtueMart */
			$q = "SELECT MIN(manufacturer_id) AS manufacturer_id FROM #__vm_manufacturer";
			$db->query($q);
			$this->current_mf_id = $db->loadResult();
			/* Check if a link already exists */
			$q = "SELECT COUNT(manufacturer_id) AS ids 
				FROM #__vm_product_mf_xref 
				WHERE manufacturer_id = ".$this->current_mf_id." 
				AND product_id = ".$product_id;
			$db->query($q);
			$count = $db->loadObjectList();
			if ($count[0]->ids == 0) {
				if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'No manufacturer link found, creating link');
				$this->AddXref($csviregistry, $product_id);
			}
			else {
				if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Manufacturer link found, doing nothing');
			}
		}
	}
	
	/** 
	 * Check to see if we update by name or ID 
	 *
	 * @param object &$csviregistry Global register
	 */
	function CheckUpdate(&$csviregistry) {
		$product_data = $csviregistry->GetArray('product_data');
		$csv_fields = $csviregistry->GetArray('csv_fields');
		$csvilog = $csviregistry->GetObject('logger');
		
		/* The manufacturer name takes precedence over the ID */
		/* See if the user wants to update the manufacturer */
		/* Check for default values and if both are set or only one */
		if (isset($csv_fields["manufacturer_name"])) {
			if ($product_data[$csv_fields["manufacturer_name"]["order"]]) $this->mf_update = 'name';
			if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Manufacturer update by name');
			return true;
		}
		else if (isset($csv_fields["manufacturer_id"])) {
			if ($product_data[$csv_fields["manufacturer_id"]["order"]]) {
				$this->mf_update = 'id';
				$this->current_mf_id = $product_data[$csv_fields["manufacturer_id"]["order"]];
				if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Manufacturer update by ID');
				return true;
			}
		}
		return false;
	}
	
	function AddManufacturer(&$csviregistry, &$d) {
		$db = $csviregistry->GetObject('database');
		$csvilog = $csviregistry->GetObject('logger');
		
		$q = "INSERT INTO #__vm_manufacturer (";
		if (isset($d["mf_id"])) $q .= "manufacturer_id, ";
		if (isset($d["mf_name"])) $q .= "mf_name, ";
		if (isset($d["mf_email"])) $q .= "mf_email, ";
		if (isset($d["mf_desc"])) $q .= "mf_desc, ";
		if (isset($d["mf_category_id"])) $q .= "mf_category_id, ";
		if (isset($d["mf_url"])) $q .= "mf_url, ";
		$q = substr($q, 0, -2).") ";
		$q .= "VALUES (";
		if (isset($d["mf_id"])) $q .= $d["mf_id"].", ";
		if (isset($d["mf_name"])) $q .= "'".$db->getEscaped($d["mf_name"])."', ";
		if (isset($d["mf_email"])) $q .= "'".$d["mf_email"]."', ";
		if (isset($d["mf_desc"])) $q .= "'".$db->getEscaped($d["mf_desc"])."', ";
		if (isset($d["mf_category_id"])) $q .= $d["mf_category_id"].", ";
		if (isset($d["mf_url"])) $q .= "'".$d["mf_url"] . "', ";
		$q = substr($q, 0, -2).") ";
		// NOTE: There might not be a manufacturer id, so we use a random number
		$id = rand();
		if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Add manufacturer data: <a onclick="switchMenu(\''.$id.'_mf_add\');" title="Show/hide query">Show/hide query</a><div id="'.$id.'_mf_add" class="debug">'.htmlentities($q).'</div>');
		if (!$db->query($q)) {
			if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', '<span class="sqlerror">SQL Error</span>: <a onclick="switchMenu(\''.$id.'_mf_add_error\');" title="Show/hide error">Show/hide error</a><div id="'.$id.'_mf_add_error" class="debug">'.mysql_error().'</div>');
			return false;
		}
		else {
			$this->current_mf_id = $db->last_insert_id();
			return true;
		}
	}
	
	function UpdateManufacturer(&$csviregistry, &$d) {
		$db = $csviregistry->GetObject('database');
		$csvilog = $csviregistry->GetObject('logger');
		
		$q = "UPDATE #__vm_manufacturer SET ";
		if (isset($d["mf_id"])) $q .= "manufacturer_id = ".$d["mf_id"].", ";
		if (isset($d["mf_name"])) $q .= "mf_name = '".$db->getEscaped($d["mf_name"])."', ";
		if (isset($d["mf_email"])) $q .= "mf_email = '".$d["mf_email"]."', ";
		if (isset($d["mf_desc"])) $q .= "mf_desc = '".$db->getEscaped($d["mf_desc"])."', ";
		if (isset($d["mf_category_id"])) $q .= "mf_category_id = ".$d["mf_category_id"].", ";
		if (isset($d["mf_url"])) $q .= "mf_url = '".$d["mf_url"]."', ";
		$q = substr($q, 0, -2);
		$q .= " WHERE ";
		switch ($this->mf_update) {
			case 'name':
				$q .= "mf_name = '".$db->getEscaped($d["mf_name"])."'";
				break;
			case 'id':
				$q .= "manufacturer_id = ".$this->current_mf_id;
				break;
		}
		if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Update manufacturer data: <a onclick="switchMenu(\''.$this->current_mf_id.'_'.$this->random.'_mf_update\');" title="Show/hide query">Show/hide query</a><div id="'.$this->current_mf_id.'_'.$this->random.'_mf_update" class="debug">'.htmlentities($q).'</div>');
		if (!$db->query($q)) {
			if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', '<span class="sqlerror">SQL Error</span>: <a onclick="switchMenu(\''.$this->current_mf_id.'_'.$this->random.'_mf_update_error\');" title="Show/hide error">Show/hide error</a><div id="'.$this->current_mf_id.'_'.$this->random.'_mf_update_error" class="debug">'.mysql_error().'</div>');
			return false;
		}
		else return true;
	}
	
	/**
	 * Create a product <-> manufacturer relationship
	 *
	 * @param &$csviregistry object Global register
	 * @param $product_id integer ID of the product in the #__vm_product table
	 */
	function AddXref(&$csviregistry, $product_id) {
		$db = $csviregistry->GetObject('database');
		$csvilog = $csviregistry->GetObject('logger');
		
		$q = "INSERT INTO #__vm_product_mf_xref VALUES (";
		$q .= "'".$product_id."', '";
		$q .= $this->current_mf_id;
		$q .= "')";
		if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Add manufacturer product cross reference: <a onclick="switchMenu(\''.$this->current_mf_id.'_'.$this->random.'_add_mf_prod_xref\');" title="Show/hide query">Show/hide query</a><div id="'.$this->current_mf_id.'_'.$this->random.'_add_mf_prod_xref" class="debug">'.htmlentities($q).'</div>');
		if (!$db->query($q)) if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', '<span class="sqlerror">SQL Error</span>: <a onclick="switchMenu(\''.$this->current_mf_id.'_'.$this->random.'_add_mf_prod_xref_error\');" title="Show/hide error">Show/hide error</a><div id="'.$this->current_mf_id.'_'.$this->random.'_add_mf_prod_xref_error" class="debug">'.mysql_error().'</div>');
	}
}
?>
