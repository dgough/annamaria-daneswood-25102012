<?php
/**
 * Product type parameter class
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Import
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_class_product_type_parameters.php 286 2008-06-01 02:51:30Z Suami $
 */
 
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * Product type parameter class
 *
 * @package CSVImproved
 * @subpackage Import
 */
class product_type_parameters {
	
	var $classname = "product_type_parameters";
	var $product_type_id = false;
	var $product_type_name = false;
	var $product_type_parameter_name = "";
	var $product_type_parameter_label = false;
	var $product_type_parameter_description = false;
	var $product_type_parameter_list_order = false;
	var $product_type_parameter_type = false;
	var $product_type_parameter_old_type = false;
	var $product_type_parameter_values = false;
	var $product_type_parameter_multiselect = false;
	var $product_type_parameter_default = false;
	var $product_type_parameter_unit = false;
	
	function product_type_parameters(&$csviregistry) {
		// Handle all fields in this order:
		$this->get_product_type_name($csviregistry);
		$this->get_product_type_id($csviregistry);
		$this->get_product_type_parameter_name($csviregistry);
		$this->get_product_type_parameter_list_order($csviregistry);
		$this->get_product_type_parameter_label($csviregistry);
		$this->get_product_type_parameter_description($csviregistry);
		$this->get_product_type_parameter_type($csviregistry);
		$this->get_product_type_parameter_old_type($csviregistry);
		$this->get_product_type_parameter_values($csviregistry);
		$this->get_product_type_parameter_multiselect($csviregistry);
		$this->get_product_type_parameter_default($csviregistry);
		$this->get_product_type_parameter_unit($csviregistry);
	}
	
	/**
	 * Checks the field for existing value, if not set the default value if allowed
	 */
	function ValidateCSVInput(&$csviregistry, $fieldname) {
		$csv_fields = $csviregistry->GetArray('csv_fields');
		$product_data = $csviregistry->GetArray('product_data');
		$skip_default_value = $csviregistry->GetVar('skip_default_value');
		/* Check if the columns match */
		if (count($csv_fields) != count($product_data)) return false;
		else {
			if (isset($csv_fields[$fieldname])) {
				/* Check if the field has a value */
				if (strlen($product_data[$csv_fields[$fieldname]["order"]]) > 0) {
					$this->$fieldname = trim($product_data[$csv_fields[$fieldname]["order"]]);
				}
				/* Field has no value, check if we can use default value*/
				else if (!$skip_default_value) {
					$this->$fieldname = $csv_fields[$fieldname]["default_value"];
				}
			}
		}
		return true;
	}
	
	function get_product_type_id(&$csviregistry) {
		$db = $csviregistry->GetObject('database');
		
		$q = "SELECT product_type_id FROM #__vm_product_type ";
		$q .= "WHERE product_type_name='".$this->product_type_name."' ";
		$db->query($q);
		$this->product_type_id = $db->f("product_type_id");
	}
	
	function get_product_type_name(&$csviregistry) {
		$this->ValidateCSVInput($csviregistry, "product_type_name");
	}
	
	/**
	 * Get the parameter name
	 *
	 * The parameter name is used to create the column names on the
	 * #__vm_product_type_x tables. As a column name they should have preferably 
	 * no spaces. Spaces are replaced by underscores.
	 *
	 * @param &$csviregistry array Global register
	 */
	function get_product_type_parameter_name(&$csviregistry) {
		$this->ValidateCSVInput($csviregistry, "product_type_parameter_name");
		$this->product_type_parameter_name = str_replace(" ", "_", $this->product_type_parameter_name);
	}
	
	function get_product_type_parameter_label(&$csviregistry) {
		$this->ValidateCSVInput($csviregistry, "product_type_parameter_label");
	}
	
	function get_product_type_parameter_description(&$csviregistry) {
		$this->ValidateCSVInput($csviregistry, "product_type_parameter_description");
	}
	
	function get_product_type_parameter_type(&$csviregistry) {
		$this->ValidateCSVInput($csviregistry, "product_type_parameter_type");
	}
	
	function get_product_type_parameter_old_type(&$csviregistry) {
		$db = $csviregistry->GetObject('database');
		if ($this->product_type_parameter_name) {
			$q = "SELECT parameter_type FROM #__vm_product_type_parameter ";
			$q .= "WHERE parameter_name='".$this->product_type_parameter_name."' ";
			$q .= "AND product_type_id = ".$this->product_type_id;
			$db->query($q);
			$this->product_type_parameter_old_type = $db->f("parameter_type");
		}
	}
	
	function get_product_type_parameter_values(&$csviregistry) {
		$this->ValidateCSVInput($csviregistry, "product_type_parameter_values");
	}
	
	function get_product_type_parameter_multiselect(&$csviregistry) {
		$this->ValidateCSVInput($csviregistry, "product_type_parameter_multiselect");
	}
	
	function get_product_type_parameter_default(&$csviregistry) {
		$this->ValidateCSVInput($csviregistry, "product_type_parameter_default");
	}
	
	function get_product_type_parameter_unit(&$csviregistry) {
		$this->ValidateCSVInput($csviregistry, "product_type_parameter_unit");
	}
	
	function get_product_type_parameter_list_order(&$csviregistry) {
		$this->ValidateCSVInput($csviregistry, "product_type_parameter_list_order");
		
		if ($this->product_type_id) {
			$db = $csviregistry->GetObject('database');
			
			$q = "SELECT parameter_list_order FROM #__vm_product_type_parameter ";
			$q .= "WHERE product_type_id=".$this->product_type_id." ";
			$q .= "AND parameter_name = '".$this->product_type_parameter_name."'";
			$db->query($q);
			$this->product_type_list_order = $db->f("parameter_list_order");
		}
	}
	
	/**************************************************************************
	** name: validate_add_parameter()
	** created by: Zdenek Dvorak
	** description:
	** parameters:
	** returns:
	***************************************************************************/
	function validate_add_parameter(&$csviregistry) {
		$csvilog = $csviregistry->GetObject('logger');
		
		if (!$this->product_type_parameter_name) {
			$csvilog->AddMessage('debug', "ERROR:  You must enter a name for the Parameter.");
			return false;
		}
		if (!$this->product_type_parameter_label) {
			if ($this->product_type_parameter_type == "B") { // Break line
				$this->product_type_parameter_label = $this->product_type_parameter_name;
			}
			else {
				$csvilog->AddMessage('debug', "ERROR:  You must enter a label for the Parameter.");
				return false;
			}
		}

		// field Value:
		if( @$this->product_type_parameter_multiselect == "Y" && $this->product_type_parameter_values == "" ) {
			$csvilog->AddMessage('debug', "ERROR:  If You checked Multiple select you must enter a Possible Values.");
			return false;
		}

		$db = $csviregistry->GetObject('database');

		// find if there is not a column with the same name
		$q  = "SELECT COUNT(*) AS count FROM #__vm_product_type_parameter ";
		$q .= "WHERE product_type_id='".$this->product_type_id."' ";
		$q .= "AND parameter_name='".$this->product_type_parameter_name."'";
		$db->query( $q );
		$db->next_record();
		if ($db->f("count") != 0) {
			$csvilog->AddMessage('debug', "ERROR:  The Parameter with this name in this Product Type already exist.");
			return false;
		}

		return true;
	}

	/**************************************************************************
	** name: validate_update_parameter()
	** created by: Zdenek Dvorak
	** description:
	** parameters:
	** returns:
	***************************************************************************/
	function validate_update_parameter(&$csviregistry) {
		$csvilog = $csviregistry->GetObject('logger');
		if (!$this->product_type_parameter_name) {
			$csvilog->AddMessage('debug', "ERROR:  You must enter a name for the Parameter.");
			return false;
		}
		elseif (!$this->product_type_parameter_label) {
			$csvilog->AddMessage('debug', "ERROR:  You must enter a label for the Parameter.");
			return false;
		}
		// field Value:
		elseif( @$this->product_type_parameter_multiselect == "Y" && $this->product_type_parameter_values == "" ) {
			$csvilog->AddMessage('debug', "ERROR:  If You checked Multiple select you must enter a Possible Values.");
			return false;
		}
		elseif ($this->product_type_parameter_name) {
			$db = $csviregistry->GetObject('database');
			
			// find if there is not a column with the same name
			$q  = "SELECT COUNT(*) AS count FROM #__vm_product_type_parameter ";
			$q .= "WHERE product_type_id='".$this->product_type_id."' ";
			$q .= "AND parameter_name='".$this->product_type_parameter_name."'";
			$db->query( $q );
			$db->next_record();
			if ($db->f("count") != 0) {
				$csvilog->AddMessage('debug', "ERROR:  A Parameter with this name in this Product Type already exist.");
				return false;
			}
		}
		return true;
	}


	/**************************************************************************
	** name: add_parameter()
	** created by: Zdenek Dvorak
	** description: creates a new parameter of Product Type
	** parameters:
	** returns:
	***************************************************************************/
	function add_parameter(&$csviregistry) {
		$db = $csviregistry->GetObject('database');

		if ($this->validate_add_parameter($csviregistry)) {
			// Let's find out the last product_type
			$q = "SELECT MAX(parameter_list_order) AS list_order FROM #__vm_product_type_parameter ";
			$q .="WHERE product_type_id='".$this->product_type_id."';";
			$db->query( $q );
			$db->next_record();
			$list_order = intval($db->f("list_order"))+1;

			// added for custom parameter modification
			// strips the trailing semi-colon from an values
			if (';' == substr($this->product_type_parameter_values, strlen($this->product_type_parameter_values)-1,1) ) {
				$this->product_type_parameter_values = substr($this->product_type_parameter_values, 0, strlen($this->product_type_parameter_values)-1);
			}
			if (empty($this->product_type_parameter_multiselect)) {
				$this->product_type_parameter_multiselect = "N";
			}
			// delete "\n" from field parameter_description
			$this->product_type_parameter_description = str_replace("\r\n","",$this->product_type_parameter_description);
			$this->product_type_parameter_description = str_replace("\n","",$this->product_type_parameter_description);

			$q = "INSERT INTO #__vm_product_type_parameter (product_type_id, parameter_name, parameter_label, ";
			$q .= "parameter_description, parameter_list_order, parameter_type, parameter_values, parameter_multiselect, parameter_default, ";
			$q .= "parameter_unit) VALUES ('";
			$q .= $this->product_type_id . "','";
			$q .= $this->product_type_parameter_name . "','";
			$q .= $this->product_type_parameter_label . "','";
			$q .= $this->product_type_parameter_description . "','";
			$q .= $list_order . "','";
			$q .= $this->product_type_parameter_type . "','";
			$q .= $this->product_type_parameter_values . "','";
			$q .= $this->product_type_parameter_multiselect . "','";
			$q .= $this->product_type_parameter_default . "','";
			$q .= $this->product_type_parameter_unit . "')";
			$db->query($q);

			if ($this->product_type_parameter_type != "B") { // != Break Line
				// Make new column in table product_type_<id>
				$q = "ALTER TABLE `#__vm_product_type_";
				$q .= $this->product_type_id . "` ADD `";
				$q .= $this->product_type_parameter_name."` ";
				switch($this->product_type_parameter_type ) {
					case "I": $q .= "int(11) "; break;	// Integer
					case "T": $q .= "text "; break; 	// Text
					case "S": $q .= "varchar(255) "; break; // Short Text
					case "F": $q .= "float "; break; 	// Float
					case "C": $q .= "char(1) "; break; 	// Char
					case "D": $q .= "datetime "; break; 	// Date & Time
					case "A": $q .= "date "; break; 	// Date
					case "V": $q .= "varchar(255) "; break; // Multiple Value
					case "M": $q .= "time "; break; 	// Time
					default: $q .= "varchar(255) ";		// Default type Short Text
				}
				if ($this->product_type_parameter_default != "" && $this->product_type_parameter_type != "T") {
					$q .= "DEFAULT '".$this->product_type_parameter_default."' NOT NULL;";
				}
				$db->query($q);

				// Make index for this column
				if ($this->product_type_parameter_type == "T") {
					$q  = "ALTER TABLE `#__vm_product_type_";
					$q .= $this->product_type_id."` ADD FULLTEXT `idx_product_type_".$this->product_type_id."_";
					$q .= $this->product_type_parameter_name."` (`".$this->product_type_parameter_name."`);";
					$db->query($q);
				}
				else {
					$q  = "ALTER TABLE `#__vm_product_type_";
					$q .= $this->product_type_id."` ADD KEY `idx_product_type_".$this->product_type_id."_";
					$q .= $this->product_type_parameter_name."` (`".$this->product_type_parameter_name."`);";
					$db->query($q);
				}
			}

			return true;
		}
		else return false;

	}

	/**************************************************************************
	** name: update_parameter()
	** created by: Zdenek Dvorak
	** description: updates Parameter information
	** parameters:
	** returns:
	***************************************************************************/
	function update_parameter(&$csviregistry) {
		$db = $csviregistry->GetObject('database');
		
		if ($this->validate_update_parameter($csviregistry)) {
			// added for custom parameter modification
			// strips the trailing semi-colon from an values
			if (';' == substr($this->product_type_parameter_values, strlen($this->product_type_parameter_values)-1,1) ) {
				$this->product_type_parameter_values = substr($this->product_type_parameter_values, 0, strlen($this->product_type_parameter_values)-1);
			}
			if (empty($this->product_type_parameter_multiselect)) {
				$this->product_type_parameter_multiselect = "N";
			}
			// delete "\n" from field parameter_description
			$this->product_type_parameter_description = str_replace("\r\n","",$this->product_type_parameter_description);
			$this->product_type_parameter_description = str_replace("\n","",$this->product_type_parameter_description);

			$q  = "UPDATE `#__vm_product_type_parameter` SET ";
			$q .= "`parameter_name`='".$this->product_type_parameter_name."',";
			$q .= "`parameter_label`='".$this->product_type_parameter_label."',";
			$q .= "`parameter_description`='".$this->product_type_parameter_description."',";
			$q .= "`parameter_list_order`='".$this->product_type_list_order."',";
			$q .= "`parameter_type`='".$this->product_type_parameter_type."',";
			$q .= "`parameter_values`='".$this->product_type_parameter_values."',";
			$q .= "`parameter_multiselect`='".$this->product_type_parameter_multiselect."',";
			$q .= "`parameter_default`='".$this->product_type_parameter_default."',";
			$q .= "`parameter_unit`='".$this->product_type_parameter_unit."' ";
			$q .= "WHERE `product_type_id`='" . $this->product_type_product_type_id . "' ";
			$q .= "AND `parameter_name`='".$this->product_type_parameter_name."';";
			$db->setQuery($q);
			$db->query();
			if ($this->product_type_parameter_type != "B") { // != Break Line
				// Delete old index
				$q  = "ALTER TABLE `#__vm_product_type_";
				$q .= $this->product_type_id."` DROP INDEX `idx_product_type_".$this->product_type_id."_";
				$q .= $this->product_type_parameter_old_name."`;";
				
				$db->setQuery($q);
				$db->query();

				// Update column in table product_type_<id>
				$q  = "ALTER TABLE `#__vm_product_type_";
				$q .= $this->product_type_id . "` CHANGE `";
				$q .= $this->product_type_parameter_old_name . "` `";
				$q .= $this->product_type_parameter_name."` ";
				switch( $this->product_type_parameter_type ) {
					case "I": $q .= "int(11) "; break;	// Integer
					case "T": $q .= "text "; break; 	// Text
					case "S": $q .= "varchar(255) "; break; // Short Text
					case "F": $q .= "float "; break; 	// Float
					case "C": $q .= "char(1) "; break; 	// Char
					case "D": $q .= "datetime "; break; 	// Date & Time
					case "A": $q .= "date "; break; 	// Date
					case "V": $q .= "varchar(255) "; break; // Multiple Value
					case "M": $q .= "time "; break; 	// Time
					default: $q .= "varchar(255) ";		// Default type Short Text
				}
				if ($this->product_type_parameter_default != "" && $this->product_type_parameter_type != "T") {
					$q .= "DEFAULT '".$this->product_type_parameter_default."' NOT NULL;";
				}
				$db->setQuery($q);
				$db->query();

				// Make index for this column
				if ($this->product_type_parameter_type == "T") {
					$q  = "ALTER TABLE `#__vm_product_type_";
					$q .= $this->product_type_id."` ADD FULLTEXT `idx_product_type_".$this->product_type_id."_";
					$q .= $this->product_type_parameter_name."` (`".$this->product_type_parameter_name."`);";
					$db->setQuery($q);
					$db->query();
				}
				else {
					$q  = "ALTER TABLE `#__vm_product_type_";
					$q .= $this->product_type_id."` ADD KEY `idx_product_type_".$this->product_type_id."_";
					$q .= $this->product_type_parameter_name."` (`".$this->product_type_parameter_name."`);";
					$db->setQuery($q);
					$db->query();
				}
			}
			return true;
		}
		else return false;
	}
}
?>
