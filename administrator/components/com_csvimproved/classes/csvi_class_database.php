<?php
/**
 * Database class
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Database
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_class_database.php 226 2008-05-06 01:31:02Z Suami $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * Database processor
 *
 * @package CSVImproved
 * @subpackage Database
 */
class CsviDatabase extends database {
	
	/** @var int   Current row in query result set */
	var $row = 0;
	/** @var stdclass	Current row record data */
	var $record = null;
	/** @var string  Error Message */
	var $error = "";
	/** @var int  Error Number */
	var $errno = "";
	/** @var string   The current sql Query    */
	var $_sql = "";
	/** @var boolean Flag to see if a query has been renewed between two query calls */
	var $_query_set= false;
	/** @var boolean   true if next_record has already been called   */
	var $called = false;
	/** @var database The core database object */
	var $_database = null;

	function CsviDatabase() {
		global $database;
		$this->_database = $database;
	}
	
	/**
    * Sets the SQL query string for later execution.
    *
    * This function replaces a string identifier <var>$prefix</var> with the
    * string held is the <var>_table_prefix</var> class variable.
    *
    * @param string The SQL query
    */
	function setQuery($sql, $offset, $limit) {
		$this->_sql = $sql;
		if ($limit > 0 && $offset == 0) {
			$this->_sql .= "\nLIMIT $limit";
		} else if ($limit > 0 || $offset > 0) {
			$this->_sql .= "\nLIMIT $offset, $limit";
		}
		$this->_database->setQuery( $this->_sql );
		$this->_query_set = true;
	}

	/**
	* Runs query and sets up the query id for the class.
	*
	* @param string The SQL query
	*/
	function query($q='', $offset = 0, $limit = 0) {
	global $mosConfig_dbprefix, $mosConfig_debug;
		$this->setQuery($q, $offset, $limit);
		$this->row = 0;
		$this->called = false;
		$this->record = null;
		$this->record = Array(0);
		if (strtoupper(substr( $this->_sql , 0, 6 )) == "SELECT" 
			|| strtoupper(substr( $this->_sql , 0, 4 ))=='SHOW' 
			|| strtoupper(substr( $this->_sql , 0, 7 ))=='EXPLAIN' 
			|| strtoupper(substr( $this->_sql , 0, 8 ))=='DESCRIBE' 
			) {
			$this->record = $this->_database->loadObjectList();
			if( $this->record === false ) $result = false;
		}
		else $result = $this->_database->query();
		
		$this->_query_set = false;
		
		if( isset( $result )) return $result;
		else return true;
	}

	/**
	 * Returns the next row in the RecordSet for the last query run.  
	 *
	 * @return boolean False if RecordSet is empty or the pointer is at the end.
	 */
	function next_record() {
		if ( $this->called ) {
			$this->row++;
		}
		else {
			$this->called = true;
		}

		if ($this->row < sizeof( $this->record ) ) {
			return true;
		}
		else {
			$this->row--;
			return false;
		}
	}


	/**
  *  Returns the value of the given field name for the current
  *  record in the RecordSet. 
  * f == field
  * @param string  The field name
  * @param boolean Strip slashes from the data?
  * @return string the value of the field $field_name in the recent row of the record set
  */
	function f($field_name, $stripslashes=true) {
		if (isset($this->record[$this->row]->$field_name)) {

			if($stripslashes) {
				return( stripslashes( $this->record[$this->row]->$field_name ) );
			}
			else {
				return( $this->record[$this->row]->$field_name );
			}
		}
	}

	/**
	 * Returns the number of rows in the RecordSet from a query.
	 * @return int
	 */
	function num_rows() {
		return sizeof( $this->record );
	}

	/**
	 * Returns the ID of the last AUTO_INCREMENT INSERT.
	 *
	 * @return int
	 */
	function last_insert_id() {
		return $this->_database->insertid();
	}

	/**
	 * returns true when the actual row is the last record in the record set
	 * otherwise returns false
	 *
	 * @return boolean
	 */
	function is_last_record() {
		return ($this->row+1 >= $this->num_rows());
	}

	/**
	 * Set the "next_record" pointer back to the first row.
	 *
	 */
	function reset() {
		$this->row = 0;
		$this->called = false;
	}
	
	///////////////////////////////
	// Parental Database functions
	// We must overwrite them because
	// we still use a global database
	// object, not a ps_DB object
	///////////////////////////////
	function loadResult() {
		return $this->_database->loadResult();
	}
	function loadResultArray($numinarray = 0) {
		return $this->_database->loadResultArray( $numinarray );
	}
	/* Create our own here, because the parental database executes the query 
		again and this is not set correctly */
	function loadAssocList( $key='' ) {
		foreach ($this->record as $id => $recordobj) {
			foreach ($recordobj as $name => $value) {
				$row[$name] = $value;
			}
			if ($key) {
				$array[$row[$key]] = $value;
			}
			else {
				$array[] = $row;
			}
		}
		if (isset($array)) return $array;
		else return array();
	}
	function loadObject( &$object ) {
		return $this->_database->loadObject($object);
	}
	function loadObjectList( $key='' ) {
		return $this->_database->loadObjectList( $key );
	}
	function loadRow() {
		return $this->_database->loadRow();
	}
	function loadRowList( $key='' ) {
		return $this->_database->loadRowList($key);
	}
	function getErrorMsg() {
		return $this->_database->getErrorMsg();
	}
	function stderr() {
		return $this->_database->stderr();
	}
	function getEscaped( $text ) {
		return $this->_database->getEscaped( $text );
	}
}

?>
