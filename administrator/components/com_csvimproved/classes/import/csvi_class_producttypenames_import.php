<?php
/**
 * Import processor class
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Import
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_class_producttypenames_import.php 286 2008-06-01 02:51:30Z Suami $
 */
 
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * Product type names import class
 *
 * @package CSVImproved
 * @subpackage Import
 */
class ProductTypeNamesImport extends CsviImport {
	
	/* Product Type Xref Import */
	function ProductTypeNamesImport(&$csviregistry) {
		/* Initialize CsviImport */
		$this->CsviImport();
		
		/* Set the debug collecter */
		$csvilog = $csviregistry->GetObject('logger');
		
		// NOTE: Get the template settings
		$templates = $csviregistry->GetObject('templates');
		
		// NOTE: include the necessary files
		/* Handle the file upload*/
		include($csviregistry->GetVar('class_path')."csvi_class_file.php");
		
		/* Handle the product details */
		include($csviregistry->GetVar('class_path')."csvi_class_product_details.php");
		
		/* Retrieve the supported fields */
		$supportedfields = $csviregistry->GetObject('supportedfields');
		$supportedfields->FieldsProductTypeNames();
		$csviregistry->SetArray('supported_fields', $supportedfields->fields);
		
		/* Get the file contents so we can start processing */
		// NOTE: Handle the upload here
		$this->handlefile = new CsviFile($csviregistry);
		
		/* Check if the file is OK, if not do not continue */
		if (!$this->handlefile->fp) return false;
		
		// NOTE: Retrieve first line
		$csviregistry->SetArray('product_data', $this->handlefile->ReadFirstLine($csviregistry));;
		
		/* Check if there is any product data */
		if (!$csviregistry->GetArray('product_data')) {
			return false;
		}
		else {
			/* Validate the import choices */
			$this->ValidateImportChoices($csviregistry);
			
			// NOTE: Print out the import details
			$this->ImportDetails($csviregistry);
			if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Doing a product type names import');
			
			/* Retrieve the fields to import */
			if ($this->RetrieveConfigFields($csviregistry)) {
				
				// NOTE: Create loop to handle more lines.....do not do product details if first line is column headers
				/* Do we need to import the first record? */
				if (!$templates->skip_first_line && !$templates->use_column_headers) {
					if ($this->product_details = new product_details($csviregistry)) {
						$this->ProcessRecord($csviregistry);
					}
				}
				
				/* Store some data for the preview here */
				$data_preview = array();
				while ($csviregistry->SetArray('product_data', $this->handlefile->ReadNextLine($csviregistry))) {
					if ($this->CheckLimits($csviregistry)) {
						// NOTE: Check if the user wants us to show a preview
						$this->product_details = new product_details($csviregistry);
						$csviregistry->SetObject('product_details', $this->product_details);
						if ($templates->show_preview) {
							if ($csviregistry->currentline == 6) {
								$csviregistry->SetArray('data_preview', $data_preview);
								if ($this->ShowPreview($csviregistry)) return true;
								else return false;
							}
							else {
								$data_preview[$csviregistry->currentline] = $csviregistry->GetArray('product_data');
								$csviregistry->currentline++;
							}
						}
						else {
							// NOTE: Start processing the data
							/* Now we import the rest of the records*/
							$this->ProcessRecord($csviregistry);
							// $this->CleanUp($csviregistry);
							// return true;
							$csviregistry->currentline++;
						}
					}
					else {
						/* Write out the memory usage for debug usage */
						if ($csviregistry->GetVar('debug')) $csvilog->debug_message = str_replace('{debugmem}', 'Memory usage: '.$csviregistry->GetVar('maxmem').' MB', $csvilog->debug_message);
						else $csvilog->debug_message = str_replace('{debugmem}', '', $csvilog->debug_message);
						return false;
					}
				}
				// NOTE: Check if we are doing preview but less than 6 lines
				if ($templates->show_preview && $csviregistry->currentline <= 6) {
					$csviregistry->SetArray('data_preview', $data_preview);
					if ($this->ShowPreview($csviregistry)) return true;
					else return false;
				}
				/* Write out the memory usage for debug usage */
				if ($csviregistry->GetVar('debug')) $csvilog->debug_message = str_replace('{debugmem}', 'Memory usage: '.$csviregistry->GetVar('maxmem').' MB', $csvilog->debug_message);
				else $csvilog->debug_message = str_replace('{debugmem}', '', $csvilog->debug_message);
			}
			else $csvilog->AddMessage('info', 'There was a problem getting the configuration fields');
		}
	} // NOTE:End function RegularImport
	
	function ProcessRecord(&$csviregistry) {
		// NOTE: Check for product type
		$db = $csviregistry->GetObject('database');
		$dbproductcheck = $csviregistry->CloneObj($db);
		$csvilog = $csviregistry->GetObject('logger');
		$csv_fields = $csviregistry->GetArray('csv_fields');
		$product_data = $csviregistry->GetArray('product_data');
		
		if ($this->product_details->product_id) {
			// NOTE: Get the product type parameters, we need the product_type_id
			require_once($csviregistry->GetVar('class_path').'csvi_class_product_type.php');
			$csv_product_type = new product_type($csviregistry);
			
			if ($csv_product_type->product_type_id) {
				/* Get the fields for the product type # */
				$q = "SHOW COLUMNS FROM #__vm_product_type_".$csv_product_type->product_type_id;
				$db->query($q);
				
				/* Check if the product type ID already exists */
				$q = "SELECT COUNT(product_id) AS products FROM #__vm_product_type_".$csv_product_type->product_type_id." WHERE product_id = '".$this->product_details->product_id."'";
				$dbproductcheck->query($q);
				/* Variable used for reporting query type */
				$action = '';
				if ($dbproductcheck->f('products') > 0) {
					$q = "UPDATE #__vm_product_type_".$csv_product_type->product_type_id." ";
					$q .= "SET ";
					
					foreach ($csv_fields as $fieldname => $id) {
						if (($fieldname != "product_sku") && ($fieldname != "product_type_name")) {
							$q .= "`".$fieldname."` = '".$product_data[$id["order"]]."',";
						}
					}
					$q = substr($q, 0, -1)." ";
					$q .= "WHERE product_id = '".$this->product_details->product_id."'";
					$action = 'updated';
					if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Updating product type details: <a onclick="switchMenu(\''.$this->product_details->product_sku.'_update_product_type\');" title="Show/hide query">Show/hide query</a><div id="'.$this->product_details->product_sku.'_update_product_type" class="debug">'.htmlentities($q).'</div>');
				}	
				else {
					$q = "INSERT INTO #__vm_product_type_".$csv_product_type->product_type_id." ";
					$q .= "(";
					foreach ($db->loadAssoclist() as $key => $col_details) {
						$q .= $col_details["Field"].", ";
					}
					$q = substr($q, 0, -2);
					$q .= ") VALUES (".$this->product_details->product_id.",";
					
					foreach ($csv_fields as $fieldname => $id) {
						if (($fieldname != "product_sku") && ($fieldname != "product_type_name")) {
							$q .= "'".$product_data[$id["order"]]."',";
						}
					}
					$q = substr($q, 0, -1);
					$q .= ")";
					$action = 'added';
					if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Adding new product type details: <a onclick="switchMenu(\''.$this->product_details->product_sku.'_add_product_type\');" title="Show/hide query">Show/hide query</a><div id="'.$this->product_details->product_sku.'_add_product_type" class="debug">'.htmlentities($q).'</div>');
				}
				if ($db->query($q)) {
					$csvilog->AddStats($action, 'Line '.$csviregistry->currentline.': <strong>'.ucfirst($action).'</strong> Product type detail SKU: '.$this->product_details->product_sku);
				}
				else {
					if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', '<span class="sqlerror">SQL Error</span>: <a onclick="switchMenu(\''.$this->product_details->product_sku.'_product_type_names_error\');" title="Show/hide error">Show/hide error</a><div id="'.$this->product_details->product_sku.'_product_type_names_error" class="debug">'.mysql_error().'</div>');
					$csvilog->AddStats('incorrect', 'Line '.$csviregistry->currentline.': <strong>Incorrect</strong> Product SKU could not be '.$action.': '.$this->product_details->product_sku);
				}
				
				/* Add the cross reference */
				if (!$csv_product_type->add_name($csviregistry)) {
					$csvilog->AddStats('incorrect', "Line $csviregistry->currentline: <strong>Cross reference already exists</strong> Product SKU: ".$this->product_details->product_sku);
				}
				else $csvilog->AddStats('added', "Line $csviregistry->currentline: <strong>Cross reference has been added</strong> Product SKU: ".$this->product_details->product_sku);
			}
			else $csvilog->AddStats('incorrect', 'Line '.$csviregistry->currentline.': <strong>Incorrect</strong> Product name not found: '.$csv_product_type->product_type_name);
		}
		else $csvilog->AddStats('incorrect', 'Line '.$csviregistry->currentline.': <strong>Incorrect</strong> Product SKU not found: '.$this->product_details->product_sku);
	}
}
?>
