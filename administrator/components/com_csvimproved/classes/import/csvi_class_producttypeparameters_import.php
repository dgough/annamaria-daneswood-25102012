<?php
/**
 * Import processor class
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Import
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_class_producttypeparameters_import.php 386 2008-07-26 01:45:03Z Suami $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * Product type parameter import class
 *
 * @package CSVImproved
 * @subpackage Import
 */
class ProductTypeParametersImport extends CsviImport {
	
	/* Product Type Parameters Import */
	function ProductTypeParametersImport(&$csviregistry) {
		/* Initialize CsviImport */
		$this->CsviImport();
		
		/* Set the debug collecter */
		$csvilog = $csviregistry->GetObject('logger');
		
		// NOTE: Get the template settings
		$templates = $csviregistry->GetObject('templates');
		
		// NOTE: include the necessary files
		/* Handle the file upload*/
		include($csviregistry->GetVar('class_path')."csvi_class_file.php");
		
		/* Handle the product details */
		include($csviregistry->GetVar('class_path')."csvi_class_product_details.php");
		
		/* Retrieve the supported fields */
		$supportedfields = $csviregistry->GetObject('supportedfields');
		$supportedfields->FieldsProductTypeParameters();
		$csviregistry->SetArray('supported_fields', $supportedfields->fields);
		
		/* Get the file contents so we can start processing */
		// NOTE: Handle the upload here
		$this->handlefile = new CsviFile($csviregistry);
		
		/* Check if the file is OK, if not do not continue */
		if (!$this->handlefile->fp) return false;
		
		// NOTE: Retrieve first line
		$csviregistry->SetArray('product_data', $this->handlefile->ReadFirstLine($csviregistry));;
		
		/* Check if there is any product data */
		if (!$csviregistry->GetArray('product_data')) {
			return false;
		}
		else {
			/* Validate the import choices */
			$this->ValidateImportChoices($csviregistry);
			
			// NOTE: Print out the import details
			$this->ImportDetails($csviregistry);
			if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Doing a product type import');
			
			/* Retrieve the fields to import */
			if ($this->RetrieveConfigFields($csviregistry)) {
				
				// NOTE: Set a placeholder for the memory usage
				if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', '{debugmem}');
			
				// NOTE: Create loop to handle more lines.....do not do product details if first line is column headers
				/* Do we need to import the first record? */
				if (!$templates->skip_first_line && !$templates->use_column_headers) {
					if ($this->product_details = new product_details($csviregistry)) {
						$this->ProcessRecord($csviregistry);
					}
				}
				
				/* Store some data for the preview here */
				$data_preview = array();
				while ($csviregistry->SetArray('product_data', $this->handlefile->ReadNextLine($csviregistry))) {
					if ($this->CheckLimits($csviregistry)) {
						// NOTE: Setup the product object
						if ($this->product_details = new product_details($csviregistry)) {
							// NOTE: Check if the user wants us to show a preview
							if ($templates->show_preview) {
								if ($csviregistry->currentline == 6) {
									$csviregistry->SetArray('data_preview', $data_preview);
									if ($this->ShowPreview($csviregistry)) return true;
									else return false;
								}
								else {
									$data_preview[$csviregistry->currentline] = $csviregistry->GetArray('product_data');
									$csviregistry->currentline++;
								}
							}
							else {
								// NOTE: Start processing the data
								/* Now we import the rest of the records*/
								$this->ProcessRecord($csviregistry);
								// $this->CleanUp($csviregistry);
								// return true;
								$csviregistry->currentline++;
							}
						}
					}
					else {
						/* Write out the memory usage for debug usage */
						if ($csviregistry->GetVar('debug')) $csvilog->debug_message = str_replace('{debugmem}', 'Memory usage: '.$csviregistry->GetVar('maxmem').' MB', $csvilog->debug_message);
						else $csvilog->debug_message = str_replace('{debugmem}', '', $csvilog->debug_message);
						return false;
					}
				}
				// NOTE: Check if we are doing preview but less than 6 lines
				if ($templates->show_preview && $csviregistry->currentline <= 6) {
					$csviregistry->SetArray('data_preview', $data_preview);
					if ($this->ShowPreview($csviregistry)) return true;
					else return false;
				}
				/* Write out the memory usage for debug usage */
				if ($csviregistry->GetVar('debug')) $csvilog->debug_message = str_replace('{debugmem}', 'Memory usage: '.$csviregistry->GetVar('maxmem').' MB', $csvilog->debug_message);
				else $csvilog->debug_message = str_replace('{debugmem}', '', $csvilog->debug_message);
			}
			else $csvilog->AddMessage('info', 'There was a problem getting the configuration fields');
		}
	} // NOTE: End function ProductTypeParametersImport
	
	function ProcessRecord(&$csviregistry) {
		// NOTE: Check for product type
		$db = $csviregistry->GetObject('database');
		$csvilog = $csviregistry->GetObject('logger');
		// NOTE: Check for product type
		require_once($csviregistry->GetVar('class_path').'csvi_class_product_type_parameters.php');
		$csv_product_type_parameters = new product_type_parameters($csviregistry);
		
		// NOTE: Set the values to be updated
		if ($csv_product_type_parameters->product_type_id) {
			// NOTE: Check if we need to update or add the data
			$q = "SELECT COUNT(*) AS total FROM #__vm_product_type_parameter WHERE product_type_id = '".$csv_product_type_parameters->product_type_id."' AND parameter_name = '".$csv_product_type_parameters->product_type_parameter_name."'";
			$db->query($q);
			if ($db->f("total") > 0) {
				if ($csv_product_type_parameters->update_parameter($csviregistry)) {
					$csvilog->AddStats('updated', 'Line '.$csviregistry->currentline.': <strong>Updated</strong> Product type parameter name: '.$csv_product_type_parameters->product_type_parameter_name);
				}
				else $csvilog->AddStats('incorrect', 'Line '.$csviregistry->currentline.': <strong>Incorrect</strong> Product type parameter name: '.$csv_product_type_parameters->product_type_parameter_name.' could not be updated');
			}
			else {
				if ($csv_product_type_parameters->add_parameter($csviregistry)) {
					$csvilog->AddStats('added', 'Line '.$csviregistry->currentline.': <strong>Added</strong> Product type parameter name: '.$csv_product_type_parameters->product_type_parameter_name);
				}
				else $csvilog->AddStats('incorrect', 'Line '.$csviregistry->currentline.': <strong>Incorrect</strong> Product type parameter name: '.$csv_product_type_parameters->product_type_parameter_name.' could not be added');
			}
		}
		else {
			$csvilog->AddStats('incorrect', 'Line '.$csviregistry->currentline.': <strong>Incorrect</strong> Product type parameter name: '.$csv_product_type_parameters->product_type_parameter_name.' has no product type linked');
		}
	}
}
?>
