<?php
/**
 * Import processor class
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Import
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_class_categorydetails_import.php 244 2008-05-18 06:28:53Z Suami $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * Processor for importing category details
 *
 * @package CSVImproved
 * @subpackage Import
 */
class CategoryDetailsImport extends CsviImport {
	
	/* Regular Import */
	function CategoryDetailsImport(&$csviregistry) {
		/* Initialize CsviImport */
		$this->CsviImport();
		
		/* Set the debug collecter */
		$csvilog = $csviregistry->GetObject('logger');
		
		// NOTE: Get the template settings
		$templates = $csviregistry->GetObject('templates');
		
		// NOTE: include the necessary files
		/* Handle the file upload*/
		if (!include($csviregistry->GetVar('class_path')."csvi_class_file.php")) return false;
		
		/* Handle the product details */
		include($csviregistry->GetVar('class_path')."csvi_class_category_details.php");
		
		/* Retrieve the supported fields */
		$supportedfields = $csviregistry->GetObject('supportedfields');
		$supportedfields->FieldsCategoryDetails();
		$csviregistry->SetArray('supported_fields', $supportedfields->fields);
		
		/* Get the file contents so we can start processing */
		/* Handle the upload here */
		$this->handlefile = new CsviFile($csviregistry);
		
		/* Check if the file is OK, if not do not continue */
		if (!$this->handlefile->fp) return false;
		
		/* Retrieve first line */
		$csviregistry->SetArray('product_data', $this->handlefile->ReadFirstLine($csviregistry));
		
		/* Check if there is any product data */
		if (!$csviregistry->GetArray('product_data')) {
			return false;
		}
		else {
			/* Validate the import choices */
			$this->ValidateImportChoices($csviregistry);
			
			// NOTE: Print out the import details
			$this->ImportDetails($csviregistry);
			if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Doing a category details import');
			
			/* Retrieve the fields to import */
			if ($this->RetrieveConfigFields($csviregistry)) {
				
				// NOTE: Set a placeholder for the memory usage
				if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', '{debugmem}');
				
				/* Create loop to handle more lines.....do not do category details if first line is column headers */
				/* Do we need to import the first record? */
				if (!$templates->skip_first_line && !$templates->use_column_headers) {
					if ($this->category_details = new category_details($csviregistry)) {
						$this->ProcessRecord($csviregistry);
					}
				}
				
				/* Store some data for the preview here */
				$data_preview = array();
				while ($csviregistry->SetArray('product_data', $this->handlefile->ReadNextLine($csviregistry))) {
					if ($this->CheckLimits($csviregistry)) {
						/* Setup the category object */
						if ($this->category_details = new category_details($csviregistry)) {
							/* Check if the user wants us to show a preview */
							if ($templates->show_preview) {
								if ($csviregistry->currentline == 6) {
									$csviregistry->SetArray('data_preview', $data_preview);
									if ($this->ShowPreview($csviregistry)) return true;
									else return false;
								}
								else {
									$data_preview[$csviregistry->currentline] = $csviregistry->GetArray('product_data');
									$csviregistry->currentline++;
								}
							}
							else {
								/* Now we import the rest of the records */
								$this->ProcessRecord($csviregistry);
								// $this->CleanUp($csviregistry);
								// return true;
								$csviregistry->currentline++;
							}
						}
						
					}
					else {
						/* Write out the memory usage for debug usage */
						if ($csviregistry->GetVar('debug')) $csvilog->debug_message = str_replace('{debugmem}', 'Memory usage: '.$csviregistry->GetVar('maxmem').' MB', $csvilog->debug_message);
						else $csvilog->debug_message = str_replace('{debugmem}', '', $csvilog->debug_message);
						return false;
					}
				}
				/* Check if we are doing preview but less than 6 lines */
				if ($templates->show_preview && $csviregistry->currentline <= 6) {
					$csviregistry->SetArray('data_preview', $data_preview);
					if ($this->ShowPreview($csviregistry)) return true;
					else return false;
				}
				/* Write out the memory usage for debug usage */
				if ($csviregistry->GetVar('debug')) $csvilog->debug_message = str_replace('{debugmem}', 'Memory usage: '.$csviregistry->GetVar('maxmem').' MB', $csvilog->debug_message);
				else $csvilog->debug_message = str_replace('{debugmem}', '', $csvilog->debug_message);
			}
			else $csvilog->AddMessage('info', 'There was a problem getting the configuration fields');
		}
	} // end function CategoryDetailsImport
	
	function ProcessRecord(&$csviregistry) {
		$dbp = $csviregistry->GetObject('database');
		$dbu = $csviregistry->GetObject('database');
		$csvilog = $csviregistry->GetObject('logger');
		$csv_fields = $csviregistry->GetArray('csv_fields');
		
		if ($csviregistry->GetVar('debug')) {
			if (isset($this->category_details->category_id)) $csvilog->AddMessage('debug', '<hr>Processing Category: '.$this->category_details->category_path);
			else $csvilog->AddMessage('debug', '<hr>No category ID found ');
		}
		
		/* Check if we have a category ID to update or if we need to create a new category */
		if (isset($this->category_details->category_id)) {
			$this->category_details->UpdateCategory($csviregistry);
		}
	} // end function ProcessRecord
}
?>
