<?php
/**
 * Template import processor class
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Import
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_class_template_import.php 286 2008-06-01 02:51:30Z Suami $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * Template import class
 *
 * @package CSVImproved
 * @subpackage Import
 */
class TemplateImport extends CsviImport {
	
	/* Template Import */
	function TemplateImport(&$csviregistry) {
		/* Initialize CsviImport */
		$this->CsviImport();
		
		/* Set the debug collecter */
		$csvilog = $csviregistry->GetObject('logger');
		
		// NOTE: Get the template settings
		$templates = $csviregistry->GetObject('templates');
		
		// NOTE: include the necessary files
		/* Handle the file upload*/
		include($csviregistry->GetVar('class_path')."csvi_class_file.php");
		
		/* Handle the product details */
		include($csviregistry->GetVar('class_path')."csvi_class_product_details.php");
		
		/* Retrieve the supported fields */
		$supportedfields = $csviregistry->GetObject('supportedfields');
		$supportedfields->FieldsTemplate();
		$csviregistry->SetArray('supported_fields', $supportedfields->fields);
		
		/* Get the file contents so we can start processing */
		// NOTE: Handle the upload here
		$this->handlefile = new CsviFile($csviregistry);
		
		/* Check if the file is OK, if not do not continue */
		if (!$this->handlefile->fp) return false;
		
		// NOTE: Retrieve first line
		$csviregistry->SetArray('product_data', $this->handlefile->ReadFirstLine($csviregistry));;
		
		/* Check if there is any product data */
		if (!$csviregistry->GetArray('product_data')) {
			return false;
		}
		else {
			/* Validate the import choices */
			$this->ValidateImportChoices($csviregistry);
			
			// NOTE: Print out the import details
			$this->ImportDetails($csviregistry);
			if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Doing a template import');
			
			/* Retrieve the fields to import */
			if ($this->RetrieveConfigFields($csviregistry)) {
				
				// NOTE: Set a placeholder for the memory usage
				if ($this->debug) $csvilog->AddMessage('debug', '{debugmem}');
				
				// NOTE: Create loop to handle more lines.....do not do product details if first line is column headers
				/* Do we need to import the first record? */
				if (!$templates->skip_first_line && !$templates->use_column_headers) {
					if ($this->product_details = new product_details($csviregistry)) {
						$this->ProcessRecord($csviregistry);
					}
				}
				$data_preview = array();
				while ($csviregistry->SetArray('product_data', $this->handlefile->ReadNextLine($csviregistry))) {
					if ($this->CheckLimits($csviregistry)) {
						$this->product_details = new product_details($csviregistry);
						$csviregistry->SetObject('product_details', $this->product_details);
						// NOTE: Check if the user wants us to show a preview
						if ($templates->show_preview) {
							if ($csviregistry->currentline == 6) {
								$csviregistry->SetArray('data_preview', $data_preview);
								if ($this->ShowPreview($csviregistry)) return true;
								else return false;
							}
							else {
								$data_preview[$csviregistry->currentline] = $csviregistry->GetArray('product_data');
								$csviregistry->currentline++;
							}
						}
						else {
							// NOTE: Start processing the data
							/* Now we import the rest of the records*/
							$this->ProcessRecord($csviregistry);
							// $this->CleanUp($csviregistry);
							// return true;
							$csviregistry->currentline++;
						}
					}
					else {
						/* Write out the memory usage for debug usage */
						if ($this->debug) $csvilog->debug_message = str_replace('{debugmem}', 'Memory usage: '.$csviregistry->GetVar('maxmem').' MB', $csvilog->debug_message);
						else $csvilog->debug_message = str_replace('{debugmem}', '', $csvilog->debug_message);
						return false;
					}
				}
				// NOTE: Check if we are doing preview but less than 6 lines
				if ($templates->show_preview && $csviregistry->currentline <= 6) {
					$csviregistry->SetArray('data_preview', $data_preview);
					if ($this->ShowPreview($csviregistry)) return true;
					else return false;
				}
				/* Write out the memory usage for debug usage */
				if ($this->debug) $csvilog->debug_message = str_replace('{debugmem}', 'Memory usage: '.$csviregistry->GetVar('maxmem').' MB', $csvilog->debug_message);
				else $csvilog->debug_message = str_replace('{debugmem}', '', $csvilog->debug_message);
			}
			else $csvilog->AddMessage('info', 'There was a problem getting the configuration fields');
		}
	} // NOTE: End function TemplateImport
	
	function ProcessRecord(&$csviregistry) {
		$db = $csviregistry->GetObject('database');
		$csvilog = $csviregistry->GetObject('logger');
		$csv_fields = $csviregistry->GetArray('csv_fields');
		$data = $csviregistry->GetArray('product_data');
		$newtemplate = $csviregistry->CloneObj($csviregistry->GetObject('templates'));
		
		if (count($data) != count($csv_fields)) {
			$csvilog->AddStats('incorrect', 'Line '.$csviregistry->currentline.': Incorrect number of fields');
		}
		else {
			// Check if template already exists
			if (isset($csv_fields['template_name'])) {
				$id = $newtemplate->GetTemplateId($data[$csv_fields['template_name']['order']]);
			
				// Set the template fields
				foreach ($csv_fields as $fieldname => $field) {
					if ($fieldname == 'shopper_group_name') {
						// Get the shopper group id
						$q = "SELECT shopper_group_id from #__vm_shopper_group 
						WHERE shopper_group_name = '".$data[$field['order']]."'";
						$db->query($q);
						$newtemplate->shopper_group_id = $db->loadResult();
					}
					else $newtemplate->$fieldname = $data[$field['order']];
				}
				
				// Template exists, only update it
				if (!empty($id)) {
					$newtemplate->id = $id;
					$newtemplate->UpdateTemplate($csviregistry);
					if ($newtemplate->changed == true) $csvilog->AddStats('updated', 'Line '.$csviregistry->currentline.': Template has been updated');
					else $csvilog->AddStats('incorrect', 'Line '.$csviregistry->currentline.': Template has not been updated');
				}
				// Template does not exist, add it
				else {
					$newtemplate->AddTemplate($csviregistry);
					if ($newtemplate->changed == true) $csvilog->AddStats('added', 'Line '.$csviregistry->currentline.': Template has been added');
					else $csvilog->AddStats('incorrect', 'Line '.$csviregistry->currentline.': Template has not been added');
				}
			}
			else $csvilog->AddStats('incorrect', 'Line '.$csviregistry->currentline.': Template has no name');
		}
	}
}
?>
