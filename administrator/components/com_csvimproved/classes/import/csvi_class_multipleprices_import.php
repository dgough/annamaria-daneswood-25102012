<?php
/**
 * Import processor class
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Import
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_class_multipleprices_import.php 386 2008-07-26 01:45:03Z Suami $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * Main constructor for the multiple prices import
 *
 * @package CSVImproved
 * @subpackage Import
 */
class MultiplePricesImport extends CsviImport {
	
	/**
	 * Multiple Prices Import
	 *
	 * @param &$csviregistry object Global register
	 */
	function MultiplePricesImport(&$csviregistry) {
		/* Initialize CsviImport */
		$this->CsviImport();
		
		/* Set the debug collecter */
		$csvilog = $csviregistry->GetObject('logger');
		
		// NOTE: Get the template settings
		$templates = $csviregistry->GetObject('templates');
		
		// NOTE: include the necessary files
		/* Handle the file upload*/
		include($csviregistry->GetVar('class_path')."csvi_class_file.php");
		
		/* Handle the product details */
		include($csviregistry->GetVar('class_path')."csvi_class_product_details.php");
		
		/* Retrieve the supported fields */
		$supportedfields = $csviregistry->GetObject('supportedfields');
		$supportedfields->FieldsMultiplePrices();
		$csviregistry->SetArray('supported_fields', $supportedfields->fields);
		
		/* Get the file contents so we can start processing */
		// NOTE: Handle the upload here
		$this->handlefile = new CsviFile($csviregistry);
		
		/* Check if the file is OK, if not do not continue */
		if (!$this->handlefile->fp) return false;
		
		// NOTE: Retrieve first line
		$csviregistry->SetArray('product_data', $this->handlefile->ReadFirstLine($csviregistry));
		
		/* Check if there is any product data */
		if (!$csviregistry->GetArray('product_data')) {
			return false;
		}
		else {
			/* Validate the import choices */
			$this->ValidateImportChoices($csviregistry);
			
			// NOTE: Print out the import details
			$this->ImportDetails($csviregistry);
			if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Doing a multiple prices import');
			
			/* Retrieve the fields to import */
			if ($this->RetrieveConfigFields($csviregistry)) {
				
				// NOTE: Create loop to handle more lines.....do not do product details if first line is column headers
				/* Do we need to import the first record? */
				if (!$templates->skip_first_line && !$templates->use_column_headers) {
					if ($this->product_details = new product_details($csviregistry)) {
						$this->ProcessRecord($csviregistry);
					}
				}
				
				/* Store some data for the preview here */
				$data_preview = array();
				while ($csviregistry->SetArray('product_data', $this->handlefile->ReadNextLine($csviregistry))) {
					if ($this->CheckLimits($csviregistry)) {
						// NOTE: Setup the product object
						if ($this->product_details = new product_details($csviregistry)) {
							// NOTE: Check if the user wants us to show a preview
							if ($templates->show_preview) {
								if ($csviregistry->currentline == 6) {
									$csviregistry->SetArray('data_preview', $data_preview);
									if ($this->ShowPreview($csviregistry)) return true;
									else return false;
								}
								else {
									$data_preview[$csviregistry->currentline] = $csviregistry->GetArray('product_data');
									$csviregistry->currentline++;
								}
							}
							else {
								// NOTE: Start processing the data
								/* Now we import the rest of the records*/
								$this->ProcessRecord($csviregistry);
								// $this->CleanUp($csviregistry);
								// return true;
								$csviregistry->currentline++;
							}
						}
					}
					else {
						/* Write out the memory usage for debug usage */
						if ($this->debug) $csvilog->debug_message = str_replace('{debugmem}', 'Memory usage: '.$csviregistry->GetVar('maxmem').' MB', $csvilog->debug_message);
						else $csvilog->debug_message = str_replace('{debugmem}', '', $csvilog->debug_message);
						return false;
					}
				}
				// NOTE: Check if we are doing preview but less than 6 lines
				if ($templates->show_preview && $csviregistry->currentline <= 6) {
					$csviregistry->SetArray('data_preview', $data_preview);
					if ($this->ShowPreview($csviregistry)) return true;
					else return false;
				}
				/* Write out the memory usage for debug usage */
				if ($csviregistry->GetVar('debug')) $csvilog->debug_message = str_replace('{debugmem}', 'Memory usage: '.$csviregistry->GetVar('maxmem').' MB', $csvilog->debug_message);
				else $csvilog->debug_message = str_replace('{debugmem}', '', $csvilog->debug_message);
			}
			else $csvilog->AddMessage('info', 'There was a problem getting the configuration fields');
		}
	} // NOTE: End function MultiplePricesImport
	
	function ProcessRecord(&$csviregistry) {
		//function MultiplePricesUpload(&$d, &$product_details) {
		$db = $csviregistry->GetObject('database');
		$csvilog = $csviregistry->GetObject('logger');
		require_once($csviregistry->GetVar('class_path').'csvi_class_product_price.php');
		$ps_product_price = new ps_product_price();
		
		// NOTE: Get the details of the product price
		$q = "SELECT product_price_id FROM #__vm_product_price ";
		$q .= "WHERE product_id = '".$this->product_details->product_id."' ";
		$q .= "AND product_currency = '".$this->product_details->product_currency."' ";
		$q .= "AND shopper_group_id = '".$this->product_details->shopper_group_id."' ";
		$q .= "AND price_quantity_start = '".$this->product_details->price_quantity_start."' ";
		$q .= "AND price_quantity_end = '".$this->product_details->price_quantity_end."'";
		if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Get product price ID: <a onclick="switchMenu(\'product_price_id_'.$csviregistry->currentline.$this->product_details->product_sku.$this->product_details->product_id.'\');" title="Show/hide query">Show/hide query</a><div id="product_price_id_'.$csviregistry->currentline.$this->product_details->product_sku.$this->product_details->product_id.'" class="debug">'.htmlentities($q).'</div>');
		$db->query($q);
		$d['product_price_id'] = $db->f('product_price_id');
		
		/* Check if the user wants to delete a price */
		if (strtolower($this->product_details->price_delete) == 'y') {
			if (!$db->next_record()) $csvilog->AddStats('incorrect', "Line $csviregistry->currentline: <strong>Price cannot be found</strong> Product SKU: ".$this->product_details->product_sku);
			else {
				if (!$ps_product_price->delete_record($csviregistry, $d['product_price_id'])) {
					$csvilog->AddStats('incorrect', "Line ".$csviregistry->currentline.": <strong>Price cannot be deleted</strong> Product SKU: ".$this->product_details->product_sku);
				}
				else {
					$csvilog->AddStats('deleted', "Line ".$csviregistry->currentline.": <strong>Price has been deleted</strong> Product SKU: ".$this->product_details->product_sku);
				}
			}
		}
		else {
			if (empty($this->product_details->product_id)) {
				if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Processing SKU: '.$this->product_details->product_sku.'<br />There is no product ID found');
				$csvilog->AddStats('skipped', 'Line '.$csviregistry->currentline.': <strong>There is no product ID found</strong> Product SKU: '.$this->product_details->product_sku);
			}
			else if (empty($this->product_details->product_price)) {
				if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Processing SKU: '.$this->product_details->product_sku.'<br />There is no product price found');
				$csvilog->AddStats('skipped', "Line $csviregistry->currentline: <strong>There is no product price</strong> Product SKU: ".$this->product_details->product_sku);
			}
			else {
				$d["product_id"] = $this->product_details->product_id;
				$d["shopper_group_id"] = $this->product_details->shopper_group_id;
				$d["product_price"] = $this->product_details->product_price;
				$d["product_currency"] = $this->product_details->product_currency;
				$d["price_quantity_start"] = $this->product_details->price_quantity_start;
				$d["price_quantity_end"] = $this->product_details->price_quantity_end;
				
				// TODO: Need to find out what these two fields are for
				$d["product_price_vdate"] = "";
				$d["product_price_edate"] = "";
				
				// NOTE: Check if there is already an entry
				$q = "SELECT COUNT(*) AS num_rows FROM #__vm_product_price WHERE";
				$q .= " shopper_group_id = '".$d["shopper_group_id"]."'";
				$q .= " AND product_id = '".$d['product_id']."'";
				$q .= " AND product_currency = '".$d['product_currency']."'";
				$q .= " AND (('".$d['price_quantity_start']."' >= price_quantity_start AND '".$d['price_quantity_start']."' <= price_quantity_end)";
				$q .= " OR ('".$d['price_quantity_end']."' >= price_quantity_start AND '".$d['price_quantity_end']."' <= price_quantity_end))";
				$db->query( $q ); $db->next_record();
				
				if ($db->f("num_rows") > 0) {
					if (!$ps_product_price->update($csviregistry, $d)) {
						if ($this->debug) $csvilog->AddMessage('debug', 'Line '.$csviregistry->currentline.': '.$d["error"]);
						$csvilog->AddStats('skipped', "Line $csviregistry->currentline: <strong>Error updating price</strong> Product SKU: ".$this->product_details->product_sku);
					}
					else $csvilog->AddStats('updated', "Line $csviregistry->currentline: <strong>Price has been updated</strong> Product SKU: ".$this->product_details->product_sku);
				}
				else {
					if (!$ps_product_price->add($csviregistry, $d)) {
						if ($this->debug) $csvilog->AddMessage('debug', 'Line '.$csviregistry->currentline.': '.$d["error"]);
						$csvilog->AddStats('skipped', "Line $csviregistry->currentline: <strong>Error adding price</strong> Product SKU: ".$this->product_details->product_sku);
					}
					else $csvilog->AddStats('added', "Line $csviregistry->currentline: <strong>Price has been added</strong> Product SKU: ".$this->product_details->product_sku);
				}
			}
		}
	}
}
?>
