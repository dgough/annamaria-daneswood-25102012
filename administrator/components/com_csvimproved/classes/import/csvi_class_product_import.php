<?php
/**
 * Import processor class
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Import
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_class_product_import.php 324 2008-06-11 10:46:54Z Suami $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * Main constructor for the product import
 *
 * @package CSVImproved
 * @subpackage Import
 */
class ProductImport extends CsviImport {
	
	var $processrelated = false;
	
	/* Product Import */
	function ProductImport(&$csviregistry) {
		/* Initialize CsviImport */
		$this->CsviImport();
		
		/* Set the debug collecter */
		$csvilog = $csviregistry->GetObject('logger');
		
		/* Get the template settings */
		$templates = $csviregistry->GetObject('templates');
		
		/* include the necessary files */
		/* Handle the file upload*/
		if (!include($csviregistry->GetVar('class_path')."csvi_class_file.php")) return false;
		
		/* Handle the product details */
		include($csviregistry->GetVar('class_path')."csvi_class_product_details.php");
		
		/* Retrieve the supported fields */
		$supportedfields = $csviregistry->GetObject('supportedfields');
		$supportedfields->FieldsRegular();
		$csviregistry->SetArray('supported_fields', $supportedfields->fields);
		
		/* Get the file contents so we can start processing */
		/* Handle the upload here */
		$this->handlefile = new CsviFile($csviregistry);
		
		/* Check if the file is OK, if not do not continue */
		if (!$this->handlefile->fp) return false;
		
		/* Retrieve first line */
		$csviregistry->SetArray('product_data', $this->handlefile->ReadFirstLine($csviregistry));
		
		/* Check if there is any product data */
		if (!$csviregistry->GetArray('product_data')) {
			return false;
		}
		else {
			/* Validate the import choices */
			$this->ValidateImportChoices($csviregistry);
			
			/* Print out the import details */
			$this->ImportDetails($csviregistry);
			if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Doing a regular import');
			
			/* Retrieve the fields to import */
			if ($this->RetrieveConfigFields($csviregistry)) {
				
				/* Set a placeholder for the memory usage */
				if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', '{debugmem}');
				
				/* Create loop to handle more lines.....do not do product details if first line is column headers */
				/* Do we need to import the first record? */
				if (!$templates->skip_first_line && !$templates->use_column_headers) {
					if ($this->product_details = new product_details($csviregistry)) {
						$this->ProcessRecord($csviregistry);
					}
				}
				
				/* Store some data for the preview here */
				$data_preview = array();
				while ($csviregistry->SetArray('product_data', $this->handlefile->ReadNextLine($csviregistry))) {
					if ($this->CheckLimits($csviregistry)) {
						// NOTE: Setup the product object
						if ($this->product_details = new product_details($csviregistry)) {
							// NOTE: Check if the user wants us to show a preview
							if ($templates->show_preview) {
								if ($csviregistry->currentline == 6) {
									$csviregistry->SetArray('data_preview', $data_preview);
									if ($this->ShowPreview($csviregistry)) return true;
									else return false;
								}
								else {
									$data_preview[$csviregistry->currentline] = $csviregistry->GetArray('product_data');
									$csviregistry->currentline++;
								}
							}
							else {
								/* Start processing the data */
								/* Now we import the rest of the records*/
								$this->ProcessRecord($csviregistry);
								// $this->CleanUp($csviregistry);
								// return true;
								$csviregistry->currentline++;
							}
						}
						
					}
					else {
						/* Write out the memory usage for debug usage */
						if ($csviregistry->GetVar('debug')) $csvilog->debug_message = str_replace('{debugmem}', 'Memory usage: '.$csviregistry->GetVar('maxmem').' MB', $csvilog->debug_message);
						else $csvilog->debug_message = str_replace('{debugmem}', '', $csvilog->debug_message);
						return false;
					}
				}
				/* Check if we are doing preview but less than 6 lines */
				if ($templates->show_preview && $csviregistry->currentline <= 6) {
					$csviregistry->SetArray('data_preview', $data_preview);
					if ($this->ShowPreview($csviregistry)) return true;
					else return false;
				}
				/* Write out the memory usage for debug usage */
				if ($csviregistry->GetVar('debug')) $csvilog->debug_message = str_replace('{debugmem}', 'Memory usage: '.$csviregistry->GetVar('maxmem').' MB', $csvilog->debug_message);
				else $csvilog->debug_message = str_replace('{debugmem}', '', $csvilog->debug_message);
				
				/* Check if we need to post process related products */
				if ($this->processrelated) {
					if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', '<hr />');
					$this->PostProcessRelatedProducts($csviregistry);
				}
			}
		}
	} // NOTE:End function RegularImport
	
	function ProcessRecord(&$csviregistry) {
		$dbp = $csviregistry->GetObject('database');
		$dbu = $csviregistry->GetObject('database');
		$csvilog = $csviregistry->GetObject('logger');
		$csv_fields = $csviregistry->GetArray('csv_fields');
		
		if ($csviregistry->GetVar('debug')) {
			if (isset($this->product_details->product_sku)) $csvilog->AddMessage('debug', '<hr>Processing SKU: '.$this->product_details->product_sku);
			else $csvilog->AddMessage('debug', '<hr>No SKU found');
		}
		
		if (empty($this->product_details->product_sku)) {
			$csvilog->AddStats('incorrect', 'Line '.$csviregistry->currentline.': No product SKU found');
			return false;
		}
		
		if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Going into normal upload');
		
		// NOTE: Process discount
		if (isset($csv_fields["product_discount"])) $this->product_details->ProcessDiscount($csviregistry);
		
		// NOTE: Check if product SKU is known if not add to error to main message and start next line
		// NOTE: Otherwise add or update product
		if (!isset($this->product_details->product_sku)) {
			$csvilog->AddStats('incorrect', 'Line '.$csviregistry->currentline.': Product SKU not specified');
		}
		else {
			// NOTE: See if sku exists. If so, update product - otherwise add product
			$q = "SELECT product_id FROM #__vm_product ";
			$q .= "WHERE product_sku='".$this->product_details->product_sku."'";
			if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Check if the product exists: <a onclick="switchMenu(\''.$this->product_details->product_sku.'_exist_product\');" title="Show/hide query">Show/hide query</a><div id="'.$this->product_details->product_sku.'_exist_product" class="debug">'.htmlentities($q).'</div>');
			$dbp->query($q);
			
			// NOTE: Check if the SKU exists
			if ($dbp->next_record()) {
				// NOTE: Check if the user wants to overwrite existing data
				/*************************************
				** OVERWRITE EXISTING DATA ***********
				*************************************/
				// TODO: Extend so that we might just fill empty fields
				if (!$this->overwrite_existing_data) {
					if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Skipping product as we are not overwriting data: '.$this->product_details->product_sku);
					$csvilog->AddStats('skipped', "Line $csviregistry->currentline: <strong>Data exists</strong> Product SKU: ".$this->product_details->product_sku);
				}
				else {
					// NOTE: Check if the user wants the product to be deleted
					/****************************
					** DELETE PRODUCT ***********
					****************************/
					if ($this->product_details->product_delete == "Y") {
						if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Deleting the product: '.$this->product_details->product_sku);
						if ($this->product_details->product_id) {
							$d['product_id'] = array();
							$d['product_id'][0] = $this->product_details->product_id;
							include_once($csviregistry->GetVar('class_path')."csvi_class_product.php");
							$delete_product= new ps_product();
							if ($delete_product->delete($csviregistry, $d)) {
								$csvilog->AddStats('deleted', "Line ".$csviregistry->currentline.": <strong>Deleted</strong> Product SKU: ".$this->product_details->product_sku);
							}
						}
						else {
							$csvilog->AddStats('incorrect', "Line ".$csviregistry->currentline.": <strong>Not found for deletion</strong> Product SKU: ".$this->product_details->product_sku);
						}
					}
					else {
						/** Tax Check *********************************/
						/* Check if tax is being updated */
						if (isset($csv_fields["product_tax"])) $this->ProcessTax($csviregistry);
						
						/* UPDATE PRODUCT */ 
						if (!$this->ProductQuery($csviregistry, "update")) {
							$csvilog->AddStats('incorrect', "Line ".$csviregistry->currentline.": <strong>Incorrect</strong> Product SKU: ".$this->product_details->product_sku." could not be updated");
							if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Could not update product price');
							return;
						}
						
						/* Check if the price is to be updated */
						if (isset($csv_fields["product_price"])) $this->PriceQuery($csviregistry, "update");
						
						/**********************************************************************************
						** ATTRIBUTE HANDLING *************************************************************
						** Let's first search for Attributes **********************************************
						** which are then added to this Product *******************************************
						** Syntax:   attribute_name::list_order|attribute_name::list_order...... **********
						***********************************************************************************/
						/* Check if the attributes is to be added */
						if ($this->product_details->attributes) {
							if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Adding attributes');
							$attributes = explode( "|", $this->product_details->attributes );
							$i = 0;
							$rand = rand();
							$q = "DELETE FROM #__vm_product_attribute_sku WHERE product_id ='".$this->product_details->product_id."'";
							if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Deleting old attributes: <a onclick="switchMenu(\''.$this->product_details->product_sku.$rand.'_delete_attributes\');" title="Show/hide query">Show/hide query</a><div id="'.$this->product_details->product_sku.$rand.'_delete_attributes" class="debug">'.htmlentities($q).'</div>');
							if (!$dbu->query($q)) if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', '<span class="sqlerror">SQL Error</span>: <a onclick="switchMenu(\''.$this->product_details->product_sku.$rand.'_delete_attributes_value_error\');" title="Show/hide error">Show/hide error</a><div id="'.$this->product_details->product_sku.$rand.'_delete_attributes_value_error" class="debug">'.mysql_error().'</div>');
							while(list(,$val) = each($attributes)) {
								$values = explode( "::", $val );
								if( empty( $values[1] )) {
									$values[1] = $i;
								}
								$q = "INSERT INTO #__vm_product_attribute_sku (`product_id`, `attribute_name`, `attribute_list`)
								 VALUES ('".$this->product_details->product_id."', '".$values[0]."', '".$values[1]."' )";
								if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Adding attributes: <a onclick="switchMenu(\''.$this->product_details->product_sku.$rand.'_add_attributes'.$i.'\');" title="Show/hide query">Show/hide query</a><div id="'.$this->product_details->product_sku.$rand.'_add_attributes'.$i.'" class="debug">'.htmlentities($q).'</div>');
								if (!$dbu->query($q)) if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', '<span class="sqlerror">SQL Error</span>: <a onclick="switchMenu(\''.$this->product_details->product_sku.$rand.'_add_attributes_error\');" title="Show/hide error">Show/hide error</a><div id="'.$this->product_details->product_sku.$rand.'_add_attributes_error" class="debug">'.mysql_error().'</div>');
								$i++;
							}
						}
						/******************************************************************************************
						** Now let's search for Attribute Values **************************************************
						** which are then added to this Child Product *********************************************
						** Syntax:   attribute_name::attribute_value|attribute_name::attribute_value..... *********
						******************************************************************************************/
						/* Check if the attribute values is to be updated */
						if ($this->product_details->attribute_values) {
							if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Adding attribute values');
							$attribute_values = explode( "|", $this->product_details->attribute_values );
							$i = 0;
							$rand = rand();
							$q = "DELETE FROM #__vm_product_attribute WHERE product_id ='".$this->product_details->product_id."'";
							if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Deleting old attribute values:<a onclick="switchMenu(\''.$this->product_details->product_sku.$rand.'_add_attribute_values\');" title="Show/hide query">Show/hide query</a><div id="'.$this->product_details->product_sku.$rand.'_add_attribute_values" class="debug">'.htmlentities($q).'</div>');
							if (!$dbu->query($q)) if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', '<span class="sqlerror">SQL Error</span>: <a onclick="switchMenu(\''.$this->product_details->product_sku.$rand.'_delete_attribute_value_error\');" title="Show/hide error">Show/hide error</a><div id="'.$this->product_details->product_sku.$rand.'_delete_attribute_value_error" class="debug">'.mysql_error().'</div>');
							while(list(,$val) = each($attribute_values)) {
								$values = explode( "::", $val );
								if( empty( $values[1] )) {
									$values[1] = "";
								}
								$q = "INSERT INTO #__vm_product_attribute (`product_id`, `attribute_name`, `attribute_value`) VALUES ('".$this->product_details->product_id."', '".$values[0]."', '".$values[1]."' )";
								if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Insert new value: <a onclick="switchMenu(\''.$this->product_details->product_sku.$rand.'_add_attribute_value'.$i.'\');" title="Show/hide query">Show/hide query</a><div id="'.$this->product_details->product_sku.$rand.'_add_attribute_value'.$i.'" class="debug">'.htmlentities($q).'</div>');
								if (!$dbu->query($q)) if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', '<span class="sqlerror">SQL Error</span>: <a onclick="switchMenu(\''.$this->product_details->product_sku.$rand.'_add_attribute_value_error\');" title="Show/hide error">Show/hide error</a><div id="'.$this->product_details->product_sku.$rand.'_add_attribute_value_error" class="debug">'.mysql_error().'</div>');
								$i++;
							}
						}
						
						/** Category path Check **************************/
						$this->CheckCategoryPath($csviregistry);
						
						/** Manufacturer Check ***************************/
						if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Checking manufacturer');
						$this->ManufacturerImport($csviregistry, $this->product_details->product_id);
						
						/** Add related products ***************************/
						if (isset($csv_fields["related_products"])) $this->ProcessRelatedProducts($csviregistry);
						
						// NOTE: Add report for this line to message
						$csvilog->AddStats('updated',"Line ".$csviregistry->currentline.": <strong>Updated</strong> Product SKU: ".$this->product_details->product_sku);
					}
				}
			}
			else if ($this->product_details->product_delete != "Y") {
				/* SKU does not exist - add new product */
				if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Adding new products');
				
				/* Tax Check */
				// NOTE: Check if tax is being updated
				if (isset($csv_fields["product_tax"])) $this->ProcessTax($csviregistry);
				
				/* Add product information */
				if (!$this->ProductQuery($csviregistry, "add")) {
					$csvilog->AddStats('incorrect', "Line $csviregistry->currentline: <strong>Incorrect</strong> Product SKU: ".$this->product_details->product_sku." could not be added");
					return;
				}
				
				/* Manufacturer Check */
				/* Check if the manufacturer is to be added */
				if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Checking manufacturer');
				$this->ManufacturerImport($csviregistry, $this->product_details->product_id);
				
				/* Category Check */
				$this->CheckCategoryPath($csviregistry);
				
				/* Price Check */
				/* Check if the price is to be added */
				$this->PriceQuery($csviregistry, "add");
				
				/* Add related products */
				if (isset($csv_fields["related_products"])) $this->ProcessRelatedProducts($csviregistry);
				
				/**
				** ATTRIBUTE HANDLING *************************************************************
				** Let's first search for Attributes **********************************************
				** which are then added to this Product *******************************************
				** Syntax:   attribute_name::list_order|attribute_name::list_order...... **********
				***********************************************************************************/
				/* Check if the attributes is to be added */
				if ($this->product_details->attributes) {
					$csvfields = $csviregistry->GetArray('csv_fields');
					if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Adding attributes');
					$attributes = explode( "|", $this->product_details->attributes );
					$i = 0;
					while(list(,$val) = each($attributes)) {
						$values = explode( "::", $val );
						if( empty( $values[1] ))
						$values[1] = $i;
						if (!$dbu->query( "INSERT INTO #__vm_product_attribute_sku (`product_id`, `attribute_name`, `attribute_list`)
						 VALUES ('".$this->product_details->product_id."', '".$values[0]."', '".$values[1]."' )")) if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', '<span class="sqlerror">SQL Error</span>: <a onclick="switchMenu(\''.$this->product_details->product_sku.'_product_attribute_sku_error\');" title="Show/hide error">Show/hide error</a><div id="'.$this->product_details->product_sku.'_product_attribute_sku_error" class="debug">'.mysql_error().'</div>');
						$i++;
					}
				}
				/******************************************************************************************
				** Now let's search for Attribute Values **************************************************
				** which are then added to this Child Product *********************************************
				** Syntax:   attribute_name::attribute_value|attribute_name::attribute_value..... *********
				******************************************************************************************/
				/* Check if the attribute values is to be added */
				if ($this->product_details->attribute_values) {
					$csvfields = $csviregistry->GetArray('csv_fields');
					if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Adding attribute values');
					$attribute_values = explode( "|", $this->product_details->attribute_values);
					$i = 0;
					$rand = rand();
					while(list(,$val) = each($attribute_values)) {
						$values = explode( "::", $val );
						if( empty( $values[1] ))
						$values[1] = "";
						$q = "INSERT INTO #__vm_product_attribute (`product_id`, `attribute_name`, `attribute_value`) 
							VALUES ('".$this->product_details->product_id."', '".$values[0]."', '".$values[1]."' )";
						if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Insert new value: <a onclick="switchMenu(\''.$this->product_details->product_sku.$rand.'_add_attribute_value'.$i.'\');" title="Show/hide query">Show/hide query</a><div id="'.$this->product_details->product_sku.$rand.'_add_attribute_value'.$i.'" class="debug">'.htmlentities($q).'</div>');
						if (!$dbu->query($q)) if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', '<span class="sqlerror">SQL Error</span>: <a onclick="switchMenu(\''.$this->product_details->product_sku.$rand.'_product_attribute_error\');" title="Show/hide error">Show/hide error</a><div id="'.$this->product_details->product_sku.$rand.'_product_attribute_error" class="debug">'.mysql_error().'</div>');
						$i++;
					}
				}
				// NOTE: Add report for this line to message
				$csvilog->AddStats('added', "Line $csviregistry->currentline: Product SKU: ".$this->product_details->product_sku);
			}
			else if ($this->product_details->product_delete == "Y") {
				$csvilog->AddStats('incorrect', "Line $csviregistry->currentline: <strong>Not found for deletion</strong> Product SKU: ".$this->product_details->product_sku);
			}
		}
	}
	
	/**
	  * Creates either an update or insert SQL query for a product.
	  *
	  * @param object &$csviregistry Global register
	  * @param string $query_type type of query to perform
	  * @return bool true|false true if the query executed successful|false if the query failed
	  */
	function ProductQuery(&$csviregistry, $query_type) {
		$csv_fields = $csviregistry->GetArray('csv_fields');
		$supportedfields = $csviregistry->GetObject('supportedfields');
		$supportedfields->FieldsRegularQuery();
		$dbu = $csviregistry->GetObject('database');
		$csvilog = $csviregistry->GetObject('logger');
		if ($query_type == "update") $q = "UPDATE #__vm_product SET ";
		else if ($query_type == "add") {
			$qfields = "";
			$qdata = "";
		}
		/* Update product information */
		/* Only handle the fields that are supported by the product table */
		foreach ($supportedfields->fields as $id => $column) {
			/* Set the variable to process the column */
			$addcolumn = true;
			
			/* Only process the fields the user is uploading */
			if (isset($csv_fields[$column])) {
				/* Assign the value to a variable that might be changed later */
				$value =  $this->product_details->$column;
				
				/* Add a redirect for the product_box */
				if ($column == "product_box") {
					if (!isset($csv_fields['product_packaging'])) {
						$column = "product_packaging";
					}
					else $addcolumn = false;
				}
				
				/* Add a redirect for the product discount */
				if ($column == "product_discount") {
					$column = "product_discount_id";
					$value =  $this->product_details->$column;
				}
				
				/* Add a redirect for the product tax */
				if ($column == "product_tax") {
					if (!isset($csv_fields['product_tax_id'])) {
						$column = "product_tax_id";
						$value =  $this->product_details->$column;
					}
					else $addcolumn = false;
				}
				
				/* Add a redirect for the product cdate */
				if ($column == "product_cdate") $column = "cdate";
				
				/* Add a redirect for the product mdate */
				if ($column == "product_mdate") $column = "mdate";
				
				/* Check if the column needs to be added */
				if ($addcolumn) {
					if ($query_type == "update") $q .= $column . " = '" .$dbu->getEscaped($value). "', ";			
					else if ($query_type == "add") {
						$qfields .= $column.", ";
						$qdata .= "'".$dbu->getEscaped($value) . "', ";
					}
				}
			}
		}
		
		if ($query_type == "update") {
			$q = substr($q, 0, -2);
			if (!$this->product_details->product_mdate) $q .= ", mdate= UNIX_TIMESTAMP(NOW()) ";
			/* If the user is not using the product publish field, we set it to yes */
			if (!isset($csv_fields['product_publish'])) $q .= ", product_publish ='Y' ";
			/* If we are dealing with a child product, update the product_parent_id */
			if ($this->product_details->child_product) $q .= ", product_parent_id ='".$this->product_details->product_parent_id."' ";
			/* If the user is not using the product publish field, we set it to yes */
			$q .= "WHERE product_sku='".$this->product_details->product_sku."'";
			if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Updating the product with new data: <a onclick="switchMenu(\''.$this->product_details->product_sku.'_update_product\');" title="Show/hide query">Show/hide query</a><div id="'.$this->product_details->product_sku.'_update_product" class="debug">'.htmlentities($q).'</div>');
		}
		else if ($query_type == "add") {
			$qfields = substr($qfields, 0 , -2);
			$qdata = substr($qdata, 0 , -2);
			
			/* If the user is not using the product publish field, we set it to yes */
			if (!isset($csv_fields['product_publish'])) {
				$qfields .= ", product_publish";
				$qdata .= ", 'Y'";
			}
			/* If we are dealing with a child product, update the product_parent_id */
			if ($this->product_details->child_product) {
				$qfields .= ", product_parent_id";
				$qdata .= ", '".$this->product_details->product_parent_id."' ";
			}
			
			/* Check if the cdate is used or not, if not we need to add the timestamp */
			if (!stristr($qfields, 'cdate')) {
				$qfields .= ", cdate";
				$qdata .= ", UNIX_TIMESTAMP(NOW())";
			}
			
			/* Check if the vendor_id is used or not, if not we need to add the vendor_id */
			if (!stristr($qfields, 'vendor_id')) {
				$qfields .= ", vendor_id";
				$qdata .= ", '".$this->product_details->vendor_id."'";
			}
			
			/* Put the whole SQL statement together */
			$q  = "INSERT INTO #__vm_product (";
			$q .= $qfields.") ";
			$q .= "VALUES (";
			$q .= $qdata.") ";
			$q = str_replace( ", )", ")", $q );
			if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Adding product: <a onclick="switchMenu(\''.$this->product_details->product_sku.'_adding_product\');" title="Show/hide query">Show/hide query</a><div id="'.$this->product_details->product_sku.'_adding_product" class="debug">'.htmlentities($q).'</div>');
		}
		if ($dbu->query($q)) {
			if ($query_type == "add") $this->product_details->product_id = $dbu->last_insert_id();
			return true;
		}
		else {
			if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', '<span class="sqlerror">SQL Error</span>: <a onclick="switchMenu(\''.$this->product_details->product_sku.'_product_query_error\');" title="Show/hide error">Show/hide error</a><div id="'.$this->product_details->product_sku.'_product_query_error" class="debug">'.mysql_error().'</div>');
			return false;
		}
	}
	
	/**
	 * Process Related Products
	 *
	 * @param &$csviregistry array Global register
	 **/
	function ProcessRelatedProducts(&$csviregistry) {
		$db = $csviregistry->GetObject('database');
		$csvilog = $csviregistry->GetObject('logger');
		$this->processrelated = true;
		// NOTE: Check if a product relation already exists
		$q = "SELECT COUNT(product_id) AS total FROM #__vm_product_relations
			WHERE product_id = ".$this->product_details->product_id;
		$db->query($q);
		$db->next_record();
		if ($db->f("total") > 0) {
			$q = "UPDATE #__vm_product_relations
			SET related_products='".$this->product_details->related_products."'
			WHERE product_id = ".$this->product_details->product_id;
		}	
		else {
			$q = "INSERT INTO #__vm_product_relations
			(product_id, related_products)
			VALUES (".$this->product_details->product_id.",'".$this->product_details->related_products."')";
		}
		if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Process related products: <a onclick="switchMenu(\''.$this->product_details->product_sku.'_related_products'.'\');" title="Show/hide query">Show/hide query</a><div id="'.$this->product_details->product_sku.'_related_products'.'" class="debug">'.htmlentities($q).'</div>');
		if (!$db->query($q)) if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Related Products: <span class="sqlerror">SQL Error</span>: <a onclick="switchMenu(\''.$this->product_details->product_sku.'_related_products_error\');" title="Show/hide error">Show/hide error</a><div id="'.$this->product_details->product_sku.'_related_products_error" class="debug">'.mysql_error().'</div>');
	}
	
	/**
	 * Post Process Related Products
	 */
	function PostProcessRelatedProducts(&$csviregistry) {
		$db = $csviregistry->GetObject('database');
		$csvilog = $csviregistry->GetObject('logger');
		$db_check = $csviregistry->CloneObj($db);
		$newrelations = array();
		// NOTE: Get the related products
		$q = "SELECT * FROM #__vm_product_relations";
		$db->query($q);
		while ($db->next_record()) {
			$relatedproducts = explode("|", $db->f("related_products"));
			$newrelations[$db->f("product_id")] = "";
			foreach ($relatedproducts as $key => $relatedproduct) {
				$q_check = "SELECT product_id FROM #__vm_product WHERE product_sku = '".$relatedproduct."'";
				$db_check->query($q_check);
				if ($db_check->next_record()) {
					$newrelations[$db->f("product_id")] .= $db_check->f("product_id")."|";
				}
				else $newrelations[$db->f("product_id")] = $relatedproduct."|";
			}
		}
		
		// NOTE: Store the relations
		foreach ($newrelations as $product_id => $related_products) {
			$q = "UPDATE #__vm_product_relations SET related_products = '".substr($related_products, 0 , -1)."' 
				WHERE product_id = ".$product_id;
			if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Process related products ids: <a onclick="switchMenu(\''.$product_id.'_store_related_products'.'\');" title="Show/hide query">Show/hide query</a><div id="'.$product_id.'_store_related_products'.'" class="debug">'.htmlentities($q).'</div>');
			if (!$db->query($q)) if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Related Products ids: <span class="sqlerror">SQL Error</span>: <a onclick="switchMenu(\''.$product_id.'_store_related_products_error\');" title="Show/hide error">Show/hide error</a><div id="'.$product_id.'_store_related_products_error" class="debug">'.mysql_error().'</div>');
		}
	}
}
?>
