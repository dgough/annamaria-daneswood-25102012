<?php
/**
 * Export class
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Export
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_class_export.php 324 2008-06-11 10:46:54Z Suami $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );
 
/**
 * Main processor for exports
 *
 * @todo Code for special characters (http://en.wikipedia.org/wiki/XML)
 * @package CSVImproved
 * @subpackage Export
 */
class CsviExport {
	
	/** @var string field delimiter for CSV export */
	protected $delim = "";
	/** @var string text enclosure for CSV export */
	protected $encl = "";
	/** @var bool should column headers be added to the export file */
	protected $include_column_headers = false;
	/** @var string the contents that will be exported */
	protected $contents = "";
	/** @var integer VirtueMart itemid to use in the product url */
	protected $vmitemid = 0;
	/** @var object contains the XML class for export */
	protected $xmlclass = "";
	/** @var string type of export CSV/XML */
	protected $type = '';
	/** @var string export location */
	protected $exportto = 'todownload';
	/** @var string local filename for writing export to file */
	protected $localfile = '';
	/** @var string contains the products category path */
	protected $catpath = '';
	
	/**
	 * Start initialising some standard settings
	 */
	function __construct() {
	}
	
	/**
	 * Retrieve the VirtueMart item ID for this site
	 *
	 * The Item ID is being used in the product url to link correctly to the products
	 */
	function GetVmItemId() {
		$db = new CsviDatabase();
		$q = "SELECT id FROM #__menu WHERE link='index.php?option=com_virtuemart' AND published=1 AND access=0";
		$db->query($q);
		if ($db->next_record()) $this->vmitemid = $db->f('id');
		else $this->vmitemid = 1;
	}
	
	/**
	 * Initialise settigns for use on export
	 *
	 * @param &$csviregistry array Global register
	 */
	function ExportDetails(&$csviregistry) {
		$db = $csviregistry->GetObject('database');
		$templates = $csviregistry->GetObject('templates');
		$this->exportto = $csviregistry->GetVar('exportto');
		/* Check if the export is limited, if so add it to the filename */
		/* Check if both values are greater than 0 */
		if (($csviregistry->GetVar('recordstart') > 0) && ($csviregistry->GetVar('recordend') > 0)) {
			/* We have valid limiters, add the limit to the filename */
			$filelimit = "_".$csviregistry->GetVar('recordend').'_'.(($csviregistry->GetVar('recordend')-1)+$csviregistry->GetVar('recordstart'));
		}
		else $filelimit = '';
		
		/**
		 * Set the filename to use for export
		 *
		 * If an export filename is set in the template, this will override
		 * any other settting.
		 */
		if (!empty($templates->export_filename)) {
			$this->localfile = $templates->export_filename;
		}
		else if ($this->exportto == 'tofile') {
			$pathparts = pathinfo($csviregistry->GetVar('localfile'));
			$this->localfile = str_replace("\\", "/", $pathparts['dirname'])."/".
							$pathparts["filename"].
							"/CSVI_Export_".$templates->template_type.'_'.date("j-m-Y_H.i").
							$filelimit.".".
							$templates->export_type;
		}
		else $this->localfile = "CSVI_Export_".$templates->template_name.'_'.date("j-m-Y_H.i").$filelimit.".".$templates->export_type;
		
		/* Get the field configuration */
		/* Get row positions of each element as set in csv table */
		$q = "SELECT * FROM #__csvi_configuration
			WHERE field_template_id = ".$templates->id."
			AND published = 1
			ORDER BY field_order";
		$db->query($q);
		$rows = $db->record;
		
		foreach ($rows as $id => $row) {
			/* Collect the required fields */
			if ($templates->export_type == 'xml') {
				if (!empty($row->field_column_header)) $this->required_fields[str_replace(" ", "", $row->field_column_header)] = $row->field_name;
				else $this->required_fields[str_replace(" ", "", $row->field_name)] = $row->field_name;
			}
			else {
				if (!empty($row->field_column_header)) $this->required_fields[$row->field_column_header] = $row->field_name;
				else $this->required_fields[$row->field_name] = $row->field_name;
			}
			
			/* Collect the default values */
			if (!empty($row->field_column_header)) $this->default_values[$row->field_column_header] = $row->field_default_value;
			else $this->default_values[$row->field_name] = $row->field_default_value;
		}
	}
	
	/**
	 * Initiliase and export details
	 *
	 * @todo add check if class really exists
	 * @param &$csviregistry array Global register
	 * @param &$site string the site to export XML for
	 * @return bool true|false true when xml class is found|false when when no site is given
	 */
	function GetXmlClass(&$csviregistry) {
		$templates = $csviregistry->GetObject('templates');
		$csvilog = $csviregistry->GetObject('logger');
		
		if (strlen($templates->export_site) > 0) {
			switch ($templates->export_site) {
				case 'csvi':
					include($csviregistry->GetVar('class_path').'xml/csvi_class_xml.php');
					$this->xmlclass = new CsviXmlExport();
					break;
				case 'beslist':
					include($csviregistry->GetVar('class_path').'xml/csvi_class_xml_beslist.php');
					$this->xmlclass = new CsviXmlBeslist();
					break;
				case 'froogle':
					include($csviregistry->GetVar('class_path').'xml/csvi_class_xml_froogle.php');
					$this->xmlclass = new CsviXmlFroogle();
					break;
				default:
					$csvilog->AddStats('incorrect', "No XML class found");
					return false;
					break;
			}
		}
		else if ($templates->export_type == 'xml') {
			$csvilog->AddStats('incorrect', "No export site set");
			return false;
		}
		return true;
	}
	
	/**
	  * Get the slash delimited category path of a product
	  *
	  * @name GetCategoryPath
	  * @author soeren
	  * @param int $product_id
	  * @returns String category_path
	  */
	function GetCategoryPath( $product_id ) {
		$db = new CsviDatabase();
		$database = new CsviDatabase();

		$q = "SELECT #__vm_product.product_id, #__vm_product.product_parent_id, category_name,#__vm_category_xref.category_parent_id "
		."FROM #__vm_category, #__vm_product, #__vm_product_category_xref,#__vm_category_xref "
		."WHERE #__vm_product.product_id='".$product_id."' "
		."AND #__vm_category_xref.category_child_id=#__vm_category.category_id "
		."AND #__vm_category_xref.category_child_id = #__vm_product_category_xref.category_id "
		."AND #__vm_product.product_id = #__vm_product_category_xref.product_id";
		$database->query( $q );
		$rows = $database->record;
		$k = 1;
		$category_path = "";

		foreach( $rows as $row ) {
			$category_name = Array();

			/** Check for product or item **/
			if ( $row->category_name ) {
				$category_parent_id = $row->category_parent_id;
				$category_name[] = $row->category_name;
			}
			else {
				/** must be an item
              * So let's search for the category path of the
              * parent product **/
				$q = "SELECT product_parent_id FROM #__vm_product WHERE product_id='$product_id'";
				$db->query( $q );
				$db->next_record();

				$q  = "SELECT #__vm_product.product_id, #__vm_product.product_parent_id, category_name,#__vm_category_xref.category_parent_id "
				."FROM #__vm_category, #__vm_product, #__vm_product_category_xref,#__vm_category_xref "
				."WHERE #__vm_product.product_id='".$db->f("product_parent_id")."' "
				."AND #__vm_category_xref.category_child_id=#__vm_category.category_id "
				."AND #__vm_category_xref.category_child_id = #__vm_product_category_xref.category_id "
				."AND #__vm_product.product_id = #__vm_product_category_xref.product_id";
				$db->query( $q );
				$db->next_record();
				$category_parent_id = $db->f("category_parent_id");
				$category_name[] = $db->f("category_name");
			}
			if( $category_parent_id == "") $category_parent_id = "0";

			while( $category_parent_id != "0" ) {
				$q = "SELECT category_name, category_parent_id "
				."FROM #__vm_category, #__vm_category_xref "
				."WHERE #__vm_category_xref.category_child_id=#__vm_category.category_id "
				."AND #__vm_category.category_id='$category_parent_id'";
				$db->query( $q );
				$db->next_record();
				$category_parent_id = $db->f("category_parent_id");
				$category_name[] = $db->f("category_name");
			}
			if ( sizeof( $category_name ) > 1 ) {
				for ($i = sizeof($category_name)-1; $i >= 0; $i--) {
					$category_path .= $category_name[$i];
					if( $i >= 1) $category_path .= "/";
				}
			}
			else
			$category_path .= $category_name[0];

			if( $k++ < sizeof($rows) )
			$category_path .= "|";
		}
		return $category_path;
	}
	
	function GetCategoryId($product_id) {
		$dbcatid = new CsviDatabase();
		$q = "SELECT category_id FROM #__vm_product_category_xref 
			WHERE product_id = '".$product_id."' LIMIT 1";
		$dbcatid->query($q);
		return $dbcatid->f('category_id');
	}
	
	function GetFlypage($product_id) {
		$dbflypage = new CsviDatabase();
		$q =  "SELECT `#__vm_product`.`product_parent_id` AS product_parent_id, `#__vm_category`.`category_flypage`
			FROM `#__vm_product`
			LEFT JOIN `#__vm_product_category_xref` ON `#__vm_product_category_xref`.`product_id` = `#__vm_product`.`product_id`
			LEFT JOIN `#__vm_category` ON `#__vm_product_category_xref`.`category_id` = `#__vm_category`.`category_id`
			WHERE `#__vm_product`.`product_id`='".$product_id."'";
		$dbflypage->query($q);
		if (strlen($dbflypage->f('category_flypage')) == 0) return 'shop.flypage';
		else return $dbflypage->f('category_flypage');
	}
	
	/**
	 * Add a field to the output
	 *
	 * @param $data string Data to output
	 * @param &$templates object Template object
	 * @param &$fieldname string Name of the field currently being processed
	 * @param &$column_header string Name of the column
	 * @param $cdata boolean true to add cdata tag for XML|false not to add it
	 */
	function AddExportField($data, &$templates, &$fieldname, &$column_header, $cdata=false) {
		if ($templates->export_type == 'xml') {
			$this->contents .= $this->xmlclass->ContentText($data, $column_header, $fieldname, $cdata);
		}
		else {
			$this->contents .= $templates->text_enclosure.$data.$templates->text_enclosure.$templates->field_delimiter;
		}
	}
	
	/** 
	 * Product types export 
	 */
	function ProductTypeExport(&$csviregistry) {
		// NOTE: Set the identifier for the export filename
		$this->type = 'ProductTypeExport';
		
		// Get the settings
		$templates = new Templates($csviregistry);
		$templates->GetTemplate($csviregistry);
		
		// See if the user wants column headers
		if ($templates->include_column_headers) {
			$this->contents = $templates->text_enclosure."product_type_name".$templates->text_enclosure.$templates->field_delimiter;
			$this->contents .= $templates->text_enclosure."product_type_description".$templates->text_enclosure.$templates->field_delimiter;
			$this->contents .= $templates->text_enclosure."product_type_publish".$templates->text_enclosure.$templates->field_delimiter;
			$this->contents .= $templates->text_enclosure."product_type_browsepage".$templates->text_enclosure.$templates->field_delimiter;
			$this->contents .= $templates->text_enclosure."product_type_flypage".$templates->text_enclosure.chr(10);
		}
		
		// Open a DB connection
		$db = $csviregistry->GetObject('database');
		
		// Execute the query
		$q = "SELECT * FROM #__vm_product_type ";
		
		/* Add a limit if user wants us to */
		$q .= $this->ExportLimit($csviregistry);
		
		$db->query($q);
		while ($db->next_record()) {
			$this->contents .= $templates->text_enclosure.$db->f("product_type_name").$templates->text_enclosure.$templates->field_delimiter;
			$this->contents .= $templates->text_enclosure.$db->f("product_type_description").$templates->text_enclosure.$templates->field_delimiter;
			$this->contents .= $templates->text_enclosure.$db->f("product_type_publish").$templates->text_enclosure.$templates->field_delimiter;
			$this->contents .= $templates->text_enclosure.$db->f("product_type_browsepage").$templates->text_enclosure.$templates->field_delimiter;
			$this->contents .= $templates->text_enclosure.$db->f("product_type_flypage").$templates->text_enclosure.chr(10);
		}
		$this->contents = trim($this->contents);
		$this->ExportCreateFile($csviregistry);
	}
	
	/** 
	 * Product types parmaters export 
	 */
	function ProductTypeParametersExport(&$csviregistry) {
		// NOTE: Set the identifier for the export filename
		$this->type = 'ProductTypeParametersExport';
		
		// Get the settings
		$templates = new Templates($csviregistry);
		$templates->GetTemplate($csviregistry);
		
		// See if the user wants column headers
		if ($templates->include_column_headers) {
			$this->contents = $templates->text_enclosure."product_type_name".$templates->text_enclosure.$templates->field_delimiter;
			$this->contents .= $templates->text_enclosure."product_type_parameter_name".$templates->text_enclosure.$templates->field_delimiter;
			$this->contents .= $templates->text_enclosure."product_type_parameter_label".$templates->text_enclosure.$templates->field_delimiter;
			$this->contents .= $templates->text_enclosure."product_type_parameter_description".$templates->text_enclosure.$templates->field_delimiter;
			$this->contents .= $templates->text_enclosure."product_type_parameter_list_order".$templates->text_enclosure.$templates->field_delimiter;
			$this->contents .= $templates->text_enclosure."product_type_parameter_type".$templates->text_enclosure.$templates->field_delimiter;
			$this->contents .= $templates->text_enclosure."product_type_parameter_values".$templates->text_enclosure.$templates->field_delimiter;
			$this->contents .= $templates->text_enclosure."product_type_parameter_multiselect".$templates->text_enclosure.$templates->field_delimiter;
			$this->contents .= $templates->text_enclosure."product_type_parameter_default".$templates->text_enclosure.$templates->field_delimiter;
			$this->contents .= $templates->text_enclosure."product_type_parameter_unit".$templates->text_enclosure.chr(10);
		}
		
		// Open a DB connection
		$db = $csviregistry->GetObject('database');
		
		// Execute the query
		$q = "SELECT #__vm_product_type.product_type_name, #__vm_product_type_parameter.* 
			FROM #__vm_product_type_parameter
			LEFT JOIN #__vm_product_type
			ON #__vm_product_type_parameter.product_type_id = #__vm_product_type.product_type_id ";
		
		/* Add a limit if user wants us to */
		$q .= $this->ExportLimit($csviregistry);
		
		$db->query($q);
		while ($db->next_record()) {
			$this->contents .= $templates->text_enclosure.$db->f("product_type_name").$templates->text_enclosure.$templates->field_delimiter;
			$this->contents .= $templates->text_enclosure.$db->f("parameter_name").$templates->text_enclosure.$templates->field_delimiter;
			$this->contents .= $templates->text_enclosure.$db->f("parameter_label").$templates->text_enclosure.$templates->field_delimiter;
			$this->contents .= $templates->text_enclosure.$db->f("parameter_description").$templates->text_enclosure.$templates->field_delimiter;
			$this->contents .= $templates->text_enclosure.$db->f("parameter_list_order").$templates->text_enclosure.$templates->field_delimiter;
			$this->contents .= $templates->text_enclosure.$db->f("parameter_type").$templates->text_enclosure.$templates->field_delimiter;
			$this->contents .= $templates->text_enclosure.$db->f("parameter_values").$templates->text_enclosure.$templates->field_delimiter;
			$this->contents .= $templates->text_enclosure.$db->f("parameter_multiselect").$templates->text_enclosure.$templates->field_delimiter;
			$this->contents .= $templates->text_enclosure.$db->f("parameter_default").$templates->text_enclosure.$templates->field_delimiter;
			$this->contents .= $templates->text_enclosure.$db->f("parameter_unit").$templates->text_enclosure.chr(10);
		}
		$this->contents = trim($this->contents);
		$this->ExportCreateFile($csviregistry);
	}
	
	/** 
	 * Product types parmaters export 
	 */
	function ProductTypeNamesExport(&$csviregistry) {
		// NOTE: Set the identifier for the export filename
		$this->type = 'ProductTypeNamesExport';
		
		// Get the settings
		$templates = new Templates($csviregistry);
		$templates->GetTemplate($csviregistry);
		
		// Open a DB connection
		$db = $csviregistry->GetObject('database');
		$db_data = $csviregistry->CloneObj($db);
		$db_table = $csviregistry->CloneObj($db);
		
		// Get the number of product type tables
		$q = "SHOW TABLES LIKE '%vm_product_type_%'";
		$db->query($q);
		$tables = array();
		foreach ($db->record as $id => $object) {
			foreach ($object as $oid => $oobject) {
				if (stristr('0123456789', substr($oobject, -1))) {
					// See if the user wants column headers
					if ($templates->include_column_headers) {
						$this->contents .= $templates->text_enclosure.'product_sku'.$templates->text_enclosure.$templates->field_delimiter;
						$this->contents .= $templates->text_enclosure.'product_type_name'.$templates->text_enclosure.$templates->field_delimiter;
						
						$q = "DESCRIBE ".$oobject;
						$db_table->query($q);
						while ($db_table->next_record()) {
							if ($db_table->f('Field') != 'product_id') {
								$this->contents .= $templates->text_enclosure.$db_table->f('Field').$templates->text_enclosure.$templates->field_delimiter;
							}
						}
						$this->contents = substr($this->contents, 0, -1);
						$this->contents .= chr(10);
					}
					
					// // Get the product type name
					$q = "SELECT product_type_name FROM #__vm_product_type 
						WHERE product_type_id = ".substr($oobject, -1);
					$db_data->query($q);
					$product_type_name = $db_data->f('product_type_name');
					
					// Add the contents
					$q = "SELECT #__vm_product.product_sku, ".$oobject.".* FROM ".$oobject."
						LEFT JOIN #__vm_product
						ON ".$oobject.".product_id = #__vm_product.product_id";
					$db_data->query($q);
					while ($db_data->next_record()) {
						$counter = 1;
						foreach ($db_data->record[$db_data->row] as $column => $value) {
							if ($counter != 2) $this->contents .= $templates->text_enclosure.$value.$templates->text_enclosure.$templates->field_delimiter;
							else $this->contents .= $templates->text_enclosure.$product_type_name.$templates->text_enclosure.$templates->field_delimiter;
							$counter++;
						}
						if (substr($this->contents, -1) == $templates->field_delimiter) {
							$this->contents = substr($this->contents, 0 , -1).chr(10);
						}
						else $this->contents .= chr(10);
					}
				}
			}
			$this->contents .= chr(13).chr(10).chr(13);
		}
		$this->contents = trim($this->contents);
		$this->ExportCreateFile($csviregistry);
	}
	
	/**
	 * Remove trailing 0
	 */
	function ProductPrice() {
		for ($i=1;$i<4;$i++) {
			if (substr($this->product_price, -1) == 0) $this->product_price = substr($this->product_price, 0, -1);
			else $i = 4;
		}
	}
	
	function CreateCategoryPath(&$csviregistry) {
		$dbpath = $csviregistry->CloneObj($csviregistry->GetObject('database'));
		$catpaths = array();
		while ($csviregistry->GetVar('catid') > 0) {
			$q = "SELECT category_parent_id, category_name FROM #__vm_category_xref x, #__vm_category c
				WHERE x.category_child_id = c.category_id
				AND category_child_id = ".$csviregistry->GetVar('catid');
			$dbpath->query($q);
			$dbpath->next_record();
			$catpaths[] = $dbpath->f('category_name');
			$csviregistry->SetVar('catid', $dbpath->f('category_parent_id'));
		}
		$catpaths = array_reverse($catpaths);
		$this->catpath = '';
		foreach ($catpaths as $id => $catname) {
			$this->catpath .= $catname."/";
		}
		return $this->catpath = substr($this->catpath, 0, -1);
	}
	
	/**
	 * Create export file
	 *
	 * @param &$csviregistry array Global register
	 */
	function ExportCreateFile(&$csviregistry) {
		$db = $csviregistry->GetObject('database');
		$templates = $csviregistry->GetObject('templates');
		$csvilog = $csviregistry->GetObject('logger');
		
		switch($this->exportto) {
			case 'todownload':
				if (ereg('Opera(/| )([0-9].[0-9]{1,2})', $_SERVER['HTTP_USER_AGENT'])) {
					$UserBrowser = "Opera";
				}
				elseif (ereg('MSIE ([0-9].[0-9]{1,2})', $_SERVER['HTTP_USER_AGENT'])) {
					$UserBrowser = "IE";
				} else {
					$UserBrowser = '';
				}
				$mime_type = ($UserBrowser == 'IE' || $UserBrowser == 'Opera') ? 'application/octetstream' : 'application/octet-stream';
		
				// dump anything in the buffer
				while( @ob_end_clean() );
		
				header('Content-Type: ' . $mime_type);
				header('Content-Encoding: UTF-8');
				header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');
		
				if ($UserBrowser == 'IE') {
					header('Content-Disposition: inline; filename="'.$this->localfile.'"');
					header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
					header('Pragma: public');
				} else {
					header('Content-Disposition: attachment; filename="'.$this->localfile.'"');
					header('Pragma: no-cache');
				}
				/*** Now dump the data!! ***/
				echo $this->contents;
				
				// do nothin' more
				exit();
				break;
			case 'tofile':
				/* open the file for writing */
				$handle = null;
				if (!$handle = fopen($this->localfile, 'w+')) {
					$csvilog->AddStats('incorrect', "Cannot open file (".$this->localfile.")");
					return false;
				}
				/* Let's make sure the file exists and is writable first. */
				if (is_writable($this->localfile)) {
				    if (fwrite($handle, $this->contents) === FALSE) {
					   $csvilog->AddStats('incorrect', "Cannot write file (".$this->localfile.")");
					   return false;
				    }
				    $csvilog->AddStats('information', "The file ".$this->localfile." has been created");
				    fclose($handle);
				    return true;
				} 
				else {
					$csvilog->AddStats('incorrect', "The file ".$this->localfile." is not writable");
					return false;
				}
				break;
		}
	}
	
	/**
	 * Adds a limit to a query
	 *
	 * @param &$csviregistry object Global register
	 * @return string|NULL the limit statement is returned if there is a record start and end else nothing is retrned
	 */
	function ExportLimit(&$csviregistry) {
		/* Check if the user only wants to export some products */
		if ($csviregistry->IssetVar('recordstart') && $csviregistry->IssetVar('recordend')) {
			/* Check if both values are greater than 0 */
			if (($csviregistry->GetVar('recordstart') > 0) && ($csviregistry->GetVar('recordend') > 0)) {
				/* We have valid limiters, add the limit to the query */
				/* Recordend needs to have 1 deducted because MySQL starts from 0 */
				return ' LIMIT '.($csviregistry->GetVar('recordend')-1).','.$csviregistry->GetVar('recordstart');
			}
		}
	}
}
?>
