<?php
/**
 * Class with supported fields
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_class_supported_fields.php 231 2008-05-13 23:04:28Z Suami $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * Class holding all the fields that are supported for both import and export
 *
 * The supported fields are broken down into the type of import or export it
 * supports.
 *
 * @package CSVImproved
 */ 
class CsviSupportedFields {
	/** @var array Contains the fields that can be used for that import/export */
	var $fields = array();
	
	/** @var array Contains the standard VirtueMart fields in the product table */
	var $dbfields = array();
	
	/** @var array Contains the custom fields from the product table */
	var $customfields = array();
	
	/** @var array Contains an array of all supported fields */
	var $allfields = array();
	
	function CsviSupportedFields(&$csviregistry) {
		$this->CustomDbFields($csviregistry);
	}
	
	/**
	 * An array of all the types of exports that are available
	 */
	function FieldsExport() {
		$this->fields = array();
		$this->fields['ProductExport'] = 'REGULAR_EXPORT';
		$this->fields['MultiplePricesExport'] = 'MULTIPLE_PRICES_EXPORT';
		$this->fields['ProductFilesExport'] = 'PRODUCT_FILES_EXPORT';
		$this->fields['ProductTypeExport'] = 'PRODUCT_TYPE_EXPORT';
		$this->fields['ProductTypeParametersExport'] = 'PRODUCT_TYPE_PARAMETERS_EXPORT';
		$this->fields['ProductTypeNamesExport'] = 'PRODUCT_TYPE_NAMES_EXPORT';
		$this->fields['TemplateExport'] = 'TEMPLATE_EXPORT';
		$this->fields['TemplateFieldsExport'] = 'TEMPLATE_FIELDS_EXPORT';
		$this->fields['CategoryDetailsExport'] = 'CATEGORY_FIELDS_EXPORT';
		$this->fields['ManufacturerExport'] = 'MANUFACTURER_EXPORT';
		$this->fields['OrderExport'] = 'ORDER_EXPORT';
	}
	
	/**
	 * An array of all the types of imports that are available
	 */
	function FieldsImport() {
		$this->fields = array();
		$this->fields['ProductImport'] = 'REGULAR_UPLOAD';
		$this->fields['MultiplePricesUpload'] = 'MULTIPLE_PRICES_UPLOAD';
		$this->fields['ProductFilesUpload'] = 'PRODUCT_FILES_UPLOAD';
		$this->fields['ProductTypeUpload'] = 'PRODUCT_TYPE_UPLOAD';
		$this->fields['ProductTypeParametersUpload'] = 'PRODUCT_TYPE_PARAMETERS_UPLOAD';
		$this->fields['ProductTypeNamesUpload'] = 'PRODUCT_TYPE_NAMES_UPLOAD';
		$this->fields['TemplateImport'] = 'TEMPLATE_IMPORT';
		$this->fields['TemplateFieldsImport'] = 'TEMPLATE_FIELDS_IMPORT';
		$this->fields['ManufacturerImport'] = 'MANUFACTURER_IMPORT';
		$this->fields['CategoryDetailsImport'] = 'CATEGORY_FIELDS_IMPORT';
	}
	
	function FieldsCustomImport() {
	}
	
	/**
	 * An array of all custom fields that can be used for a regular export
	 *
	 * @todo Make fields translateable
	 * @see CsviExport::RegularExport
	 */
	function FieldsCustomExport() {
		$this->fields['picture_url'] = 'picture_url';
		$this->fields['price_with_tax'] = 'price_with_tax';
		$this->fields['price_with_discount'] = 'price_with_discount';
		$this->fields['product_id'] = 'product_id';
		$this->fields['custom'] = 'custom';
		asort($this->fields);
	}
	
	/**
	 * An array of all custom fields that can be used for a regular import
	 *
	 * @todo Make fields translateable
	 * @see RegularImport::ProcessRecord
	 */
	function FieldsRegular() {
		$this->fields = array();
		$this->fields[] = 'vendor_id';
		$this->fields[] = 'product_sku';
		$this->fields[] = 'product_s_desc';
		$this->fields[] = 'product_desc';
		$this->fields[] = 'product_thumb_image';
		$this->fields[] = 'product_full_image';
		$this->fields[] = 'product_publish';
		$this->fields[] = 'product_weight';
		$this->fields[] = 'product_weight_uom';
		$this->fields[] = 'product_length';
		$this->fields[] = 'product_width';
		$this->fields[] = 'product_height';
		$this->fields[] = 'product_lwh_uom';
		$this->fields[] = 'product_url';
		$this->fields[] = 'product_in_stock';
		$this->fields[] = 'product_available_date';
		$this->fields[] = 'product_availability';
		$this->fields[] = 'product_special';
		$this->fields[] = 'product_discount';
		$this->fields[] = 'product_name';
		$this->fields[] = 'product_sales';
		$this->fields[] = 'attribute';
		$this->fields[] = 'custom_attribute';
		$this->fields[] = 'product_tax_id';
		$this->fields[] = 'product_tax';
		$this->fields[] = 'product_unit';
		$this->fields[] = 'product_packaging';
		$this->fields[] = 'category_path';
		$this->fields[] = 'category_id';
		$this->fields[] = 'attributes';
		$this->fields[] = 'attribute_values';
		$this->fields[] = 'manufacturer_id';
		$this->fields[] = 'manufacturer_name';
		$this->fields[] = 'product_box';
		$this->fields[] = 'product_delete';
		$this->fields[] = 'product_discount_date_start';
		$this->fields[] = 'product_discount_date_end';
		$this->fields[] = 'product_parent_sku';
		$this->fields[] = 'product_price';
		$this->fields[] = 'product_currency';
		$this->fields[] = 'related_products';
		$this->fields[] = 'shopper_group_name';
		$this->fields[] = 'child_options';
		$this->fields[] = 'quantity_options';
		$this->fields[] = 'child_option_ids';
		$this->fields[] = 'product_order_levels';
		$this->fields[] = 'product_cdate';
		$this->fields[] = 'product_mdate';
		$this->fields = array_merge($this->fields, $this->customfields);
		asort($this->fields);
	}
	
	/**
	 * An array of all fields that are available for a multiple prices import
	 *
	 * @see MultiplePricesImport::ProcessRecord
	 */
	function FieldsMultiplePrices() {
		$this->fields = array();
		$this->fields[] = 'product_sku';
		$this->fields[] = 'product_price';
		$this->fields[] = 'product_currency';
		$this->fields[] = 'shopper_group_name';
		$this->fields[] = 'price_quantity_start';
		$this->fields[] = 'price_quantity_end';
		$this->fields[] = 'price_delete';
		asort($this->fields);
	}
	
	/**
	 * An array of all fields that are available for a product files import
	 *
	 * @see ProductFilesImport::ProcessRecord
	 */
	function FieldsProductFiles() {
		$this->fields = array();
		$this->fields[] = 'product_sku';
		$this->fields[] = 'product_files_file_name';
		$this->fields[] = 'product_files_file_title';
		$this->fields[] = 'product_files_file_description';
		$this->fields[] = 'product_files_file_url';
		$this->fields[] = 'product_files_file_published';
		asort($this->fields);
	}
	
	/**
	 * An array of all fields that are available for a product type import
	 *
	 * @see ProductTypeImport::ProcessRecord
	 */
	function FieldsProductType() {
		$this->fields = array();
		$this->fields[] = 'product_type_name';
		$this->fields[] = 'product_type_description';
		$this->fields[] = 'product_type_publish';
		$this->fields[] = 'product_type_browsepage';
		$this->fields[] = 'product_type_flypage';
		asort($this->fields);
	}
	
	/**
	 * An array of all fields that are available for a product type parameters import
	 *
	 * @see ProductTypeParametersImport::ProcessRecord
	 */
	function FieldsProductTypeParameters() {
		$this->fields = array();
		$this->fields[] = 'product_type_name';
		$this->fields[] = 'product_type_parameter_name';
		$this->fields[] = 'product_type_parameter_label';
		$this->fields[] = 'product_type_parameter_description';
		$this->fields[] = 'product_type_parameter_list_order';
		$this->fields[] = 'product_type_parameter_type';
		$this->fields[] = 'product_type_parameter_values';
		$this->fields[] = 'product_type_parameter_multiselect';
		$this->fields[] = 'product_type_parameter_default';
		$this->fields[] = 'product_type_parameter_unit';
		asort($this->fields);
	}
	
	/**
	 * An array of all fields that are available for a product type name import
	 *
	 * The list only contains 2 fields but more fieldnames need to be added
	 * to import data. These fieldnames are the product type parameter names.
	 * This allows for filling the parameters with data.
	 *
	 * @see ProductTypeNamesImport::ProcessRecord
	 * @see FieldsProductTypeParameters()
	 */
	function FieldsProductTypeNames() {
		$this->fields = array();
		$this->fields[] = 'product_sku';
		$this->fields[] = 'product_type_name';
		asort($this->fields);
	}
	
	/**
	 * An array of all fields that are available for a template import/export
	 *
	 * @see TemplatesImport::ProcessRecord
	 */
	function FieldsTemplate() {
		$this->fields = array();
		$this->fields[] = 'template_name';
		$this->fields[] = 'template_type';
		$this->fields[] = 'skip_first_line';
		$this->fields[] = 'use_column_headers';
		$this->fields[] = 'collect_debug_info';
		$this->fields[] = 'overwrite_existing_data';
		$this->fields[] = 'skip_default_value';
		$this->fields[] = 'show_preview';
		$this->fields[] = 'include_column_headers';
		$this->fields[] = 'text_enclosure';
		$this->fields[] = 'field_delimiter';
		$this->fields[] = 'export_type';
		$this->fields[] = 'export_site';
		$this->fields[] = 'thumb_width';
		$this->fields[] = 'thumb_height';
		$this->fields[] = 'shopper_group_name';
		$this->fields[] = 'url_suffix';
		$this->fields[] = 'max_execution_time';
		$this->fields[] = 'max_input_time';
		$this->fields[] = 'memory_limit';
		$this->fields[] = 'post_max_size';
		$this->fields[] = 'upload_max_filesize';
		$this->fields[] = 'export_filename';
		asort($this->fields);
	}
	
	/**
	 * An array of all fields that are available for a template fields import/export
	 *
	 * @see TemplateFieldsImport::ProcessRecord
	 */
	function FieldsTemplateFields() {
		$this->fields = array();
		$this->fields[] = 'template_name';
		$this->fields[] = 'field_name';
		$this->fields[] = 'field_column_header';
		$this->fields[] = 'field_default_value';
		$this->fields[] = 'field_order';
		$this->fields[] = 'published';
		asort($this->fields);
	}
	
	/**
	 * An array of custom fields that are available for a regular export
	 *
	 * @see CsviExport::RegularExport
	 */
	function FieldsCustomFields() {
		$this->fields = array();
		$this->fields[] = 'custom';
		$this->fields[] = 'picture_url';
		$this->fields[] = 'price_with_tax';
		$this->fields[] = 'discount_with_tax';
	}
	
	/**
	 * An array of all fields that are available for manufacturer import
	 *
	 * @see CsviExport::RegularExport
	 */
	function FieldsManufacturer() {
		$this->fields = array();
		$this->fields[] = 'manufacturer_name';
		$this->fields[] = 'manufacturer_email';
		$this->fields[] = 'manufacturer_desc';
		$this->fields[] = 'manufacturer_category_name';
		$this->fields[] = 'manufacturer_url';
		$this->fields[] = 'manufacturer_id';
		asort($this->fields);
	}
	
	/**
	 * An array of all fields that are available for manufacturer category import
	 *
	 * @todo Use these fields
	 */
	function FieldsManufacturerCategory() {
		$this->fields = array();
		$this->fields[] = 'manufacturer_category_name';
		$this->fields[] = 'manufacturer_category_desc';
		asort($this->fields);
	}
	
	/**
	 * An array of all fields that are available for category details import/export
	 *
	 * @see CategoryDetailsImport::ProcessRecord()
	 */
	function FieldsCategoryDetails() {
		$this->fields = array();
		$this->fields[] = 'category_path';
		$this->fields[] = 'category_description';
		$this->fields[] = 'category_thumb_image';
		$this->fields[] = 'category_full_image';
		$this->fields[] = 'category_publish';
		$this->fields[] = 'category_browsepage';
		$this->fields[] = 'category_products_per_row';
		$this->fields[] = 'category_flypage';
		$this->fields[] = 'category_list_order';
	}
	
	/**
	 * An array of all fields that are available for order export
	 *
	 * @see CsviExport::OrderExport()
	 */
	 
	function FieldsOrder() {
		$this->fields = array();
		$this->fields[] = 'order_id';
		$this->fields[] = 'order_total';
		$this->fields[] = 'order_subtotal';
		$this->fields[] = 'order_tax';
		$this->fields[] = 'order_shipping';
		$this->fields[] = 'order_shipping_tax';
		$this->fields[] = 'coupon_discount';
		$this->fields[] = 'order_discount';
		$this->fields[] = 'order_currency';
		$this->fields[] = 'order_date';
		$this->fields[] = 'order_modified_date';
		$this->fields[] = 'ship_method_id';
		$this->fields[] = 'customer_note';
		$this->fields[] = 'ip_address';
		$this->fields[] = 'address_type';
		$this->fields[] = 'address_type_name';
		$this->fields[] = 'company';
		$this->fields[] = 'title';
		$this->fields[] = 'last_name';
		$this->fields[] = 'first_name';
		$this->fields[] = 'middle_name';
		$this->fields[] = 'phone_1';
		$this->fields[] = 'phone_2';
		$this->fields[] = 'fax';
		$this->fields[] = 'address_1';
		$this->fields[] = 'address_2';
		$this->fields[] = 'city';
		$this->fields[] = 'state';
		$this->fields[] = 'country';
		$this->fields[] = 'zip';
		$this->fields[] = 'user_email';
		$this->fields[] = 'extra_field_1';
		$this->fields[] = 'extra_field_2';
		$this->fields[] = 'extra_field_3';
		$this->fields[] = 'extra_field_4';
		$this->fields[] = 'extra_field_5';
		$this->fields[] = 'bank_account_nr';
		$this->fields[] = 'bank_name';
		$this->fields[] = 'bank_sort_code';
		$this->fields[] = 'bank_iban';
		$this->fields[] = 'bank_account_holder';
		$this->fields[] = 'bank_account_type';
		$this->fields[] = 'order_payment_expire';
		$this->fields[] = 'order_payment_name';
		$this->fields[] = 'order_payment_log';
		$this->fields[] = 'order_payment_trans_id';
		$this->fields[] = 'order_status_name';
		$this->fields[] = 'product_sku';
		$this->fields[] = 'product_name';
		$this->fields[] = 'product_quantity';
		$this->fields[] = 'product_price';
		$this->fields[] = 'product_final_price';
		$this->fields[] = 'order_item_currency';
		$this->fields[] = 'product_attribute';
		$this->fields[] = 'manufacturer_name';
		$this->fields[] = 'user_id';
		$this->fields[] = 'product_id';
		$this->fields[] = 'username';
		$this->fields[] = 'custom';
		asort($this->fields);
	}
	
	/**
	 * An array of all fields that are available
	 */
	function AllSupportedFields() {
		$this->FieldsRegular();
		$this->allfields = $this->fields;
		$this->FieldsMultiplePrices();
		$this->allfields = array_merge($this->allfields, $this->fields);
		$this->FieldsProductFiles();
		$this->allfields = array_merge($this->allfields, $this->fields);
		$this->FieldsProductType();
		$this->allfields = array_merge($this->allfields, $this->fields);
		$this->FieldsProductTypeParameters();
		$this->allfields = array_merge($this->allfields, $this->fields);
		$this->FieldsProductTypeNames();
		$this->allfields = array_merge($this->allfields, $this->fields);
		$this->FieldsTemplate();
		$this->allfields = array_merge($this->allfields, $this->fields);
		$this->FieldsTemplateFields();
		$this->allfields = array_merge($this->allfields, $this->fields);
		$this->FieldsCategoryDetails();
		$this->allfields = array_merge($this->allfields, $this->fields);
		$this->FieldsCustomImport();
		$this->allfields = array_merge($this->allfields, $this->fields);
		$this->FieldsCustomExport();
		$this->allfields = array_merge($this->allfields, $this->fields);
		$this->FieldsCategoryDetails();
		$this->allfields = array_merge($this->allfields, $this->fields);
		asort($this->allfields);
	}
	
	/** 
	 * Contains all fieldnames for the #__vm_products table which are to be updated
	 *
	 * @see RegularImport::ProductQuery()
	*/
	function FieldsRegularQuery() {
		$this->fields = array();
		$this->fields[] = 'vendor_id';
		$this->fields[] = 'product_parent_id';
		$this->fields[] = 'product_sku';
		$this->fields[] = 'product_s_desc';
		$this->fields[] = 'product_desc';
		$this->fields[] = 'product_thumb_image';
		$this->fields[] = 'product_full_image';
		$this->fields[] = 'product_publish';
		$this->fields[] = 'product_weight';
		$this->fields[] = 'product_weight_uom';
		$this->fields[] = 'product_length';
		$this->fields[] = 'product_width';
		$this->fields[] = 'product_height';
		$this->fields[] = 'product_lwh_uom';
		$this->fields[] = 'product_url';
		$this->fields[] = 'product_in_stock';
		$this->fields[] = 'product_available_date';
		$this->fields[] = 'product_availability';
		$this->fields[] = 'product_special';
		$this->fields[] = 'product_discount';
		$this->fields[] = 'product_name';
		$this->fields[] = 'product_sales';
		$this->fields[] = 'attribute';
		$this->fields[] = 'custom_attribute';
		$this->fields[] = 'product_tax_id';
		$this->fields[] = 'product_tax';
		$this->fields[] = 'product_unit';
		$this->fields[] = 'product_packaging';
		$this->fields[] = 'product_box';
		$this->fields[] = 'child_options';
		$this->fields[] = 'quantity_options';
		$this->fields[] = 'child_option_ids';
		$this->fields[] = 'product_order_levels';
		$this->fields[] = 'product_cdate';
		$this->fields[] = 'product_mdate';
		$this->fields = array_merge($this->fields, $this->customfields);
		return asort($this->fields);
	}
	
	/**
	 * Get the fields belonging to a certain operation type
	 *
	 * @param string $type contains the name of the supported fields to activate
	 */
	function GetSupportedFields($type) {
		switch ($type) {
			case 'ProductImport':
				$this->FieldsRegular();
				$this->FieldsCustomImport();
				break;
			case 'MultiplePricesUpload':
				$this->FieldsMultiplePrices();
				break;
			case 'ProductFilesUpload':
				$this->FieldsProductFiles();
				break;
			case 'ProductTypeUpload':
				$this->FieldsProductType();
				break;
			case 'ProductTypeParametersUpload':
				$this->FieldsProductTypeParameters();
				break;
			case 'ProductTypeNamesUpload':
				$this->FieldsProductTypeNames();
				break;
			case 'TemplateImport':
				$this->FieldsTemplate();
				break;
			case 'TemplateFieldsImport':
				$this->FieldsTemplateFields();
				break;
			case 'ManufacturerImport':
				$this->FieldsManufacturer();
				break;
			case 'ProductExport':
				$this->FieldsRegular();
				$this->FieldsCustomExport();
				break;
			case 'MultiplePricesExport':
				$this->FieldsMultiplePrices();
				break;
			case 'ProductFilesExport':
				$this->FieldsProductFiles();
				break;
			case 'ProductTypeExport':
				$this->FieldsProductType();
				break;
			case 'ProductTypeParametersExport':
				$this->FieldsProductTypeParameters();
				break;
			case 'ProductTypeNamesExport':
				$this->FieldsProductTypeNames();
				break;
			case 'TemplateExport':
				$this->FieldsTemplate();
				break;
			case 'TemplateFieldsExport':
				$this->FieldsTemplateFields();
				break;
			case 'CategoryDetailsExport':
				$this->FieldsCategoryDetails();
				break;
			case 'CategoryDetailsImport':
				$this->FieldsCategoryDetails();
				break;
			case 'ManufacturerExport':
				$this->FieldsManufacturer();
				break;
			case 'OrderExport':
				$this->FieldsOrder();
				break;
		}
	}
	
	/**
	 * Function creates an array with all fields in the product table
	 */
	function ProductTableFields() {
		$this->dbfields = array();
		$this->dbfields[] = 'product_id';
		$this->dbfields[] = 'vendor_id';
		$this->dbfields[] = 'product_parent_id';
		$this->dbfields[] = 'product_sku';
		$this->dbfields[] = 'product_s_desc';
		$this->dbfields[] = 'product_desc';
		$this->dbfields[] = 'product_thumb_image';
		$this->dbfields[] = 'product_full_image';
		$this->dbfields[] = 'product_publish';
		$this->dbfields[] = 'product_weight';
		$this->dbfields[] = 'product_weight_uom';
		$this->dbfields[] = 'product_length';
		$this->dbfields[] = 'product_width';
		$this->dbfields[] = 'product_height';
		$this->dbfields[] = 'product_lwh_uom';
		$this->dbfields[] = 'product_url';
		$this->dbfields[] = 'product_in_stock';
		$this->dbfields[] = 'product_available_date';
		$this->dbfields[] = 'product_availability';
		$this->dbfields[] = 'product_special';
		$this->dbfields[] = 'product_discount_id';
		$this->dbfields[] = 'ship_code_id';
		$this->dbfields[] = 'cdate';
		$this->dbfields[] = 'mdate';
		$this->dbfields[] = 'product_name';
		$this->dbfields[] = 'product_sales';
		$this->dbfields[] = 'attribute';
		$this->dbfields[] = 'custom_attribute';
		$this->dbfields[] = 'product_tax_id';
		$this->dbfields[] = 'product_unit';
		$this->dbfields[] = 'product_packaging';
		$this->dbfields[] = 'child_options';
		$this->dbfields[] = 'quantity_options';
		$this->dbfields[] = 'child_option_ids';
		$this->dbfields[] = 'product_order_levels';
	}
	
	/**
	 * Creates an array of custom database fields the user can use for import/export
	 *
	 * @param &$csviregistry object Global register
	 */
	function CustomDbFields(&$csviregistry) {
		$db = $csviregistry->GetObject('database');
		$this->customfields = array();
		$this->ProductTableFields();
		$q = "SHOW COLUMNS FROM #__vm_product WHERE field NOT IN (";
		foreach ($this->dbfields as $id => $fieldname) {
			$q .= "'".$fieldname."', ";
		}
		$q = substr($q, 0, -2);
		$q .= ")";
		$db->query($q);
		if ($db->num_rows() > 0) {
			$fields = $db->loadObjectList();
			foreach ($fields as $key => $field) {
				$this->customfields[] = $field->Field;
			}
		}
	}
}
?>
