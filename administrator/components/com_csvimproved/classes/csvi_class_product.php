<?php
/**
 * Product class
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Import
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_class_product.php 286 2008-06-01 02:51:30Z Suami $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * Product helper
 *
 * @package CSVImproved
 * @subpackage Import
 */
class ps_product {
	var $classname = "ps_product";

	/**
	 * Validates product fields and uploaded image files.
	 *
	 * @param array $d The input vars
	 * @return boolean True when validation successful, false when not
	 */
	function validate(&$d) {
		global $vmLogger, $database, $perm;
		$valid = true;
		$db = new CsviDatabase();
		$ps_vendor_id = $_SESSION["ps_vendor_id"];
		if( $perm->check( 'admin' )) {
			$vendor_id = $d['vendor_id'];
		}
		else {
			$vendor_id = $ps_vendor_id;
		}

		$q = "SELECT product_id,product_thumb_image,product_full_image FROM #__vm_product WHERE product_sku='";
		$q .= $d["product_sku"] . "'";
		$db->query($q);
		if ($db->next_record()&&($db->f("product_id") != $d["product_id"])) {
			$vmLogger->err( "A Product with the SKU ".$d['product_sku']." already exists." );
			$valid = false;
		}
		if( !empty( $d['product_discount_id'] )) {
			if( $d['product_discount_id'] == "override" ) {

				$d['is_percent'] = "0";
				$d['amount'] = (float)$d['product_price_incl_tax'] - (float)$d['discounted_price_override'];

				require_once( CLASSPATH. 'ps_product_discount.php' );
				$ps_product_discount = new ps_product_discount;
				$ps_product_discount->add( $d );
				$d['product_discount_id'] = $database->insertid();
			}
		}
		if (empty($d['manufacturer_id'])) {
			$d['manufacturer_id'] = "1";
		}
		if (empty( $d["product_sku"])) {
			$vmLogger->err( "A Product Sku must be entered." );
			$valid = false;
		}
		if (!$d["product_name"]) {
			$vmLogger->err( "A product name must be entered." );
			$valid = false;
		}
		if (!$d["product_available_date"]) {
			$vmLogger->err( "You must provide an availability date." );
			$valid = false;
		}
		else {
			$day = substr ( $d["product_available_date"], 8, 2);
			$month= substr ( $d["product_available_date"], 5, 2);
			$year =substr ( $d["product_available_date"], 0, 4);
			$d["product_available_date_timestamp"] = mktime(0,0,0,$month, $day, $year);
		}

		/** Validate Product Specific Fields **/
		if (!$d["product_parent_id"]) {
			if (sizeof(@$d["product_categories"]) < 1) {
				$vmLogger->err( "A Category must be selected." );
				$valid = false;
			}
		}
		if( !empty($d['downloadable']) && (empty($_FILES['file_upload']['name'] ) && empty($d['filename']))) {
			$vmLogger->err( "Please specify a Product File for Download!" );
			$valid =  false;
		}

		/** Image Upload Validation **/

		// do we have an image URL or an image File Upload?
		if (!empty( $d['product_thumb_image_url'] )) {
			// Image URL
			if (substr( $d['product_thumb_image_url'], 0, 4) != "http") {
				$vmLogger->err( "Image URL must begin with http." );
				$valid =  false;
			}

			// if we have an uploaded image file, prepare this one for deleting.
			if( $db->f("product_thumb_image") && substr( $db->f("product_thumb_image"), 0, 4) != "http") {
				$_REQUEST["product_thumb_image_curr"] = $db->f("product_thumb_image");
				$d["product_thumb_image_action"] = "delete";
				if (!validate_image($d,"product_thumb_image","product")) {
					return false;
				}
			}
			$d["product_thumb_image"] = $d['product_thumb_image_url'];
		}
		else {
			// File Upload
			if (!validate_image($d,"product_thumb_image","product")) {
				$valid = false;
			}
		}

		if (!empty( $d['product_full_image_url'] )) {
			// Image URL
			if (substr( $d['product_full_image_url'], 0, 4) != "http") {
				$vmLogger->err( "Image URL must begin with http." );
				return false;
			}
			// if we have an uploaded image file, prepare this one for deleting.
			if( $db->f("product_full_image") && substr( $db->f("product_thumb_image"), 0, 4) != "http") {
				$_REQUEST["product_full_image_curr"] = $db->f("product_full_image");
				$d["product_full_image_action"] = "delete";
				if (!validate_image($d,"product_full_image","product")) {
					return false;
				}
			}
			$d["product_full_image"] = $d['product_full_image_url'];
		}
		else {
			// File Upload
			if (!validate_image($d,"product_full_image","product")) {
				$valid = false;
			}
		}

		foreach ($d as $key => $value) {
			if (!is_array($value))
			$d[$key] = addslashes($value);
		}
		return $valid;
	}

	/**
	 * Validates that a product can be deleted
	 *
	 * @param array $d The input vars
	 * @return boolean Validation sucessful?
	 */
	function validate_delete( $product_id, &$d ) {
		
		if (empty($product_id)) {
			// $vmLogger->err( "Please specify a Product to delete!" );
			return false;
		}
	}

	/**
	 * Function to delete product(s) $d['product_id'] from the product table
	 *
	 * @param array $d The input vars
	 * @return boolean True, when the product was deleted, false when not
	 */
	function delete(&$csviregistry, &$d) {

		$product_id = $d["product_id"];

		if( is_array( $product_id)) {
			foreach( $product_id as $product) {
				if( !$this->delete_product( $csviregistry, $product, $d ))
				return false;
			}
			return true;
		}
		else {
			return $this->delete_product($csviregistry, $product_id, $d );
		}
	}
	
	/**
	 * The function that holds the code for deleting
	 * one product from the database and all related tables
	 * plus deleting files related to the product
	 *
	 * @param int $product_id
	 * @param array $d The input vars
	 * @return boolean True on success, false on error
	 */
	function delete_product(&$csviregistry, $product_id, &$d ) {
		
		$db = $csviregistry->GetObject('database');
		/**
		if (!$this->validate_delete($product_id, $d)) {
			return false;
		}
		*/
		/* If is Product */
		if ($this->is_product($product_id)) {
			/* Delete all items first */
			$q  = "SELECT product_id FROM #__vm_product WHERE product_parent_id='$product_id'";
			$db->query($q);
			while($db->next_record()) {
				$d2["product_id"] = $db->f("product_id");
				if (!$this->delete($d2)) {
					return false;
				}
			}

			/* Delete attributes */
			$q  = "DELETE FROM #__vm_product_attribute_sku WHERE product_id='$product_id' ";
			$db->query($q);

			/* Delete categories xref */
			$q  = "DELETE fFROM #__vm_product_category_xref WHERE product_id = '$product_id' ";
			$db->query($q);
		}
		/* If is Item */
		else {
			/* Delete attribute values */
			$q  = "DELETE FROM #__vm_product_attribute WHERE product_id='$product_id'";
			$db->query($q);
		}
		/* For both Product and Item */

		/* Delete product - manufacturer xref */
		$q = "DELETE FROM #__vm_product_mf_xref WHERE product_id='$product_id'";
		$db->query($q);

		/* Delete Product - ProductType Relations */
		$q  = "DELETE FROM `#__vm_product_product_type_xref` WHERE `product_id`=$product_id";
		$db->query($q);
		
		/* Delete product votes */
		$q  = "DELETE FROM #__vm_product_votes WHERE product_id='$product_id'";
		$db->query($q);

		/* Delete product reviews */
		$q = "DELETE FROM #__vm_product_reviews WHERE product_id='$product_id'";
		$db->query($q);

		/* Delete Image files */
		$this->DeleteProductImage($csviregistry, $product_id);
		
		/* Delete other Files and extra images files */
		$this->DeleteProductFiles($csviregistry, $product_id);
		
		/* Delete Product Relations */
		$q  = "DELETE FROM #__vm_product_relations WHERE product_id = '$product_id'";
		$db->query($q);

		/* Delete Prices */
		$q  = "DELETE FROM #__vm_product_price WHERE product_id = '$product_id'";
		$db->query($q);

		/* Delete entry FROM #__vm_product table */
		$q  = "DELETE FROM #__vm_product WHERE product_id = '$product_id'";
		$db->query($q);
		return true;
	}

	/**
	 * Function to check whether a product is a parent product or not
	 *
	 * @param int $product_id
	 * @return boolean True when the product is a parent product, false when product is a child item
	 */
	function is_product($product_id) {
		$db = new CsviDatabase();

		$q  = "SELECT product_parent_id FROM #__vm_product ";
		$q .= "WHERE product_id='$product_id' ";

		$db->query($q);
		$db->next_record();
		if ($db->f("product_parent_id") == 0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	function DeleteProductImage(&$csviregistry, &$product_id) {
		$templates = $csviregistry->GetObject('templates');
		$filepath = $templates->file_location;
		$img_width = $templates->thumb_width;
		$img_height = $templates->thumb_height;
		/* Get the image filenames from the database */
		$db = $csviregistry->GetObject('database');
		$q  = "SELECT product_thumb_image,product_full_image ";
		$q .= "FROM #__vm_product ";
		$q .= "WHERE product_id='".$product_id."'";
		$db->query($q);
		$db->next_record();
		
		if (!stristr($db->f("product_thumb_image"), "http")) {
			// Check if the thumbnail exists, if so delete it
			if (file_exists($filepath.'resized/'.$db->f("product_thumb_image").'_'.$img_width.'x'.$img_height)) @unlink($filepath.'resized/'.$db->f("product_thumb_image").'_'.$img_width.'x'.$img_height);
		}
		
		if (!stristr($db->f("product_full_image"), "http")) {
			// Check if the file exists, if so delete it
			if (file_exists($filepath.$db->f("product_thumb_image"))) @unlink($filepath.$db->f("product_thumb_image"));
		}
	}
	
	function DeleteProductFiles(&$csviregistry, &$product_id) {
		$db = $csviregistry->GetObject('database');
		$dbdel = $csviregistry->CloneObj($db);
		$templates = $csviregistry->GetObject('templates');
		$filepath = $templates->file_location;
		$img_width = $templates->thumb_width;
		$img_height = $templates->thumb_height;
		$db->query("SELECT file_id, file_name, file_is_image FROM #__vm_product_files WHERE file_product_id='".$product_id."'");
		while($db->next_record()) {
			$image = basename($db->f("file_name"));
			$thumbimage = substr($image, 0, (strlen($image)-(strlen(pathinfo($image, PATHINFO_EXTENSION))+1))).'_'.$img_width.'x'.$img_height.".".pathinfo($image, PATHINFO_EXTENSION);
			if ($db->f("file_is_image")) {
				if (!stristr($db->f("file_name"), "http")) {
					// Check if the thumbnail exists, if so delete it
					if (file_exists($filepath.'resized/'.$thumbimage)) @unlink($filepath.'resized/'.$thumbimage);
				}
			}
			// Check if the file exists, if so delete it
			if (file_exists($filepath.$image)) @unlink($filepath.$image);
			
			$dbdel->query("DELETE FROM #__vm_product_files WHERE file_id = ".$db->f("file_id"));
		}
	}
}
?>
