<?php
/**
 * Logging class
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Log
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_class_log.php 222 2008-04-29 06:21:12Z Suami $
 */
 
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * The central logging class
 *
 * @package CSVImproved
 * @subpackage Log
 */
class CsviLog {
	
	/** @var string contains the log messages */
	var $logmessage = '';
	/** @var string contains the debug messages */
	var $debug_message = '';
	/** @var array contains the import statistics */
	var $stats = array();
	
	function CsviLog() {
	}
	
	/**
	 * Adds a message to the log stack
	 *
	 * @param string $type type of message
	 * @param string $message message to add to the stack
	 */
	function AddMessage($type, $message) {
		switch ($type) {
			case 'debug':
				$this->debug_message .= $message."<br />\n";
				break;
			default:
				$this->logmessage .= $message."<br />\n";
				break;
		}
	}
	/**
	* Adds a message to the statistics stack
	*
	* <p>
	* Types:
	* --> Products
	* updated
	* deleted
	* added
	* skipped
	* incorrect
	* --> DB tables
	* empty
	* --> Fields
	* nosupport
	* --> No files found multiple images
	* nofiles
	* --> General information
	* information
	* </p>
	*
	* @param string $type type of message
	* @param string $message message to add to the stack
	*/
	function AddStats($type, $message) {
		if (!isset($this->stats[$type])) {
			$this->stats[$type]['message'] = $message."<br />\n";
			$this->stats[$type]['count'] = 1;
		}
		else {
			$this->stats[$type]['message'] .= $message."<br />\n";
			$this->stats[$type]['count']++;
		}
	}
}
?>
