<?php
/**
 * Templates class
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Templates
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_class_category_details.php 125 2008-02-13 22:43:26Z Roland $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * Main controller for templates
 *
 * @package CSVImproved
 * @subpackage Templates
 */
class Templates {
	
	var $id = false;
	var $template_name = "";
	var $template_type = "";
	var $skip_first_line = 0;
	var $use_column_headers = 0;
	var $collect_debug_info = 0;
	var $overwrite_existing_data = 0;
	var $skip_default_value = 0;
	var $show_preview = 0;
	var $include_column_headers = 0;
	var $text_enclosure = "";
	var $field_delimiter = "";
	var $thumb_width = "";
	var $thumb_height = "";
	var $type = "";
	var $templatelist = array();
	var $numberoffields = 0;
	var $update = false;
	var $export_type = "";
	var $export_site = "";
	var $shopper_group_id = 0;
	var $producturl_suffix = '';
	var $file_location = '';
	var $shopper_group_name = '';
	var $product_publish = '';
	var $max_execution_time = -1;
	var $max_input_time = -1;
	var $memory_limit = -1;
	var $post_max_size = -1;
	var $upload_max_filesize = -1;
	var $export_filename = '';
	var $manufacturer = '';
	
	function Templates(&$csviregistry) {
		$this->GetTemplatesList($csviregistry);
		$this->ValidateCSVInput($csviregistry);
		$this->SystemLimits();
	}

	function GetTemplatesList(&$csviregistry, $pagenav='') {
		$db = $csviregistry->GetObject('database');
		$dbshopper = $csviregistry->CloneObj($db);
		$this->templatelist = array();
		$q = "SELECT ID, template_name, template_type, shopper_group_id
			FROM #__csvi_templates
			ORDER BY template_name";
		if ($pagenav) $db->query($q, $pagenav->limitstart, $pagenav->limit);
		else $db->query($q);
		
		$qshopper = "SELECT shopper_group_name, shopper_group_id FROM #__vm_shopper_group";
		$dbshopper->query($qshopper);
		$shopper_groups = $dbshopper->loadObjectList('shopper_group_id');
		while ($db->next_record()) {
			$this->templatelist[$db->f("ID")]['name'] = $db->f("template_name");
			$this->templatelist[$db->f("ID")]['type'] = $db->f("template_type");
			if (isset($shopper_groups[$db->f("shopper_group_id")])) {
				$this->templatelist[$db->f("ID")]['shopper_group_name'] = $shopper_groups[$db->f("shopper_group_id")]->shopper_group_name;
			}
			else $this->templatelist[$db->f("ID")]['shopper_group_name'] = '';
		}
	}
	
	/**
	 * Get the template details
	 *
	 * Retrieves the template details from the csvi_templates table. 
	 *
	 * @see http://php.net/manual/function.fgetcsv.php
	 *
	 * @param &$csviregistry array Global registry
	 * @param $id integer Template ID to retrieve
	 */
	function GetTemplate(&$csviregistry, $id=0) {
		$db = $csviregistry->GetObject('database');
		if ($id == 0) $id = $csviregistry->GetVar('templateid');
		$q = "SELECT * FROM #__csvi_templates WHERE ID = ".$id;
		
		$db->query($q);
		
		if ($db->next_record()) {
			$this->id = $db->f("ID");
			$this->template_name = $db->f("template_name");
			$this->template_type = $db->f("template_type");
			$this->skip_first_line = $db->f("skip_first_line");
			$this->use_column_headers = $db->f("use_column_headers");
			$this->collect_debug_info = $db->f("collect_debug_info");
			$this->overwrite_existing_data = $db->f("overwrite_existing_data");
			$this->skip_default_value = $db->f("skip_default_value");
			$this->show_preview = $db->f("show_preview");
			$this->include_column_headers = $db->f("include_column_headers");
			$this->text_enclosure = stripslashes($db->f("text_enclosure"));
			$this->field_delimiter = stripslashes($db->f("field_delimiter"));
			$this->export_type = stripslashes($db->f("export_type"));
			$this->export_site = stripslashes($db->f("export_site"));
			$this->thumb_width = $db->f("thumb_width");
			$this->thumb_height = $db->f("thumb_height");
			$this->shopper_group_id = $db->f("shopper_group_id");
			$this->producturl_suffix = stripslashes($db->f("producturl_suffix"));
			$this->file_location = stripslashes($db->f("file_location"));
			$this->product_publish = stripslashes($db->f("product_publish"));
			if (strlen($db->f("max_execution_time")) > 0) $this->max_execution_time = $db->f("max_execution_time");
			if (strlen($db->f("max_input_time")) > 0) $this->max_input_time = $db->f("max_input_time");
			if (strlen($db->f("memory_limit")) > 0) $this->memory_limit = $db->f("memory_limit");
			if (strlen($db->f("post_max_size")) > 0) $this->post_max_size = $db->f("post_max_size");
			if (strlen($db->f("upload_max_filesize")) > 0) $this->upload_max_filesize = $db->f("upload_max_filesize");
			$this->export_filename = stripslashes($db->f("export_filename"));
			$this->manufacturer = $db->f("manufacturer");
			$this->GetNumberOfFields($id);
			$this->GetShopperGroupName();
			
			/* Set the thumb image sizes */
			if ($db->f("thumb_width") == 0) $csviregistry->SetVar('THUMB_IMG_WIDTH', '90');
			else $csviregistry->SetVar('THUMB_IMG_WIDTH', $db->f("thumb_width"));
			if ($db->f("thumb_height") == 0) $csviregistry->SetVar('THUMB_IMG_HEIGHT', '90');
			else $csviregistry->SetVar('THUMB_IMG_HEIGHT', $db->f("thumb_height"));
			
			/* Set the type export/import */
			if (stristr($this->template_type, 'export')) $csviregistry->SetVar('csvi_type', 'export');
			else $csviregistry->SetVar('csvi_type', 'import');
		}
	}
	
	function AddTemplate(&$csviregistry) {
		$db = $csviregistry->GetObject('database');
		$csvilog = $csviregistry->GetObject('logger');
		$fieldconfig = $csviregistry->GetObject('fieldconfig');
		$this->changed = false;
		
		$q = "INSERT INTO #__csvi_templates ";
		$q .= "(template_name, template_type, skip_first_line,";
		$q .= "use_column_headers, collect_debug_info, overwrite_existing_data,";
		$q .= "skip_default_value, show_preview, include_column_headers,";
		$q .= "text_enclosure, field_delimiter, export_type, export_site,";
		$q .= "thumb_width, thumb_height, shopper_group_id, producturl_suffix,";
		$q .= "file_location, product_publish, max_input_time, memory_limit,";
		$q .= "post_max_size, max_execution_time, upload_max_filesize,";
		$q .= "export_filename, manufacturer) ";
		$q .= "VALUES (";
		$q .= "'".$this->template_name."', ";
		$q .= "'".$this->template_type."', ";
		$q .= "'".$this->skip_first_line."', ";
		$q .= "'".$this->use_column_headers."', ";
		$q .= "'".$this->collect_debug_info."', ";
		$q .= "'".$this->overwrite_existing_data."', ";
		$q .= "'".$this->skip_default_value."', ";
		$q .= "'".$this->show_preview."', ";
		$q .= "'".$this->include_column_headers."', ";
		$q .= "'".$db->getEscaped($this->text_enclosure)."', ";
		$q .= "'".$db->getEscaped($this->field_delimiter)."', ";
		$q .= "'".$db->getEscaped($this->export_type)."', ";
		$q .= "'".$db->getEscaped($this->export_site)."', ";
		$q .= "'".$this->thumb_width."', ";
		$q .= "'".$this->thumb_height."', ";
		$q .= "'".$this->shopper_group_id."', ";
		$q .= "'".$db->getEscaped($this->producturl_suffix)."', ";
		$q .= "'".$db->getEscaped($this->file_location)."', ";
		$q .= "'".$this->product_publish."', ";
		$q .= "'".$this->max_input_time."', ";
		$q .= "'".$this->memory_limit."', ";
		$q .= "'".$this->post_max_size."', ";
		$q .= "'".$this->max_execution_time."', ";
		$q .= "'".$this->upload_max_filesize."', ";
		$q .= "'".$db->getEscaped($this->export_filename)."', ";
		$q .= "'".$this->manufacturer."' ";
		$q .= ")";
		if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Line '.$csviregistry->currentline.': Add a new template: <a onclick="switchMenu(\''.$this->template_name.'_add_template\');" title="Show/hide query">Show/hide query</a><div id="'.$this->template_name.'_add_template" class="debug">'.htmlentities($q).'</div>');
		if ($db->query($q)) {
			$this->id = $db->last_insert_id();
			$this->changed = true;
		}
		else {
			if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', '<span class="sqlerror">SQL Error</span>: <a onclick="switchMenu(\''.$this->template_name.'_add_template_error\');" title="Show/hide error">Show/hide error</a><div id="'.$this->template_name.'_add_template_error" class="debug">'.mysql_error().'</div>');
			$csvilog->AddMessage('info','Line '.$csviregistry->currentline.': Not able to add template<br />'.stripslashes($db->getErrorMsg()));
		}
	}
	
	/**
	 * Update template
	 *
	 * Template settings are updated. In case the template type has changed
	 * all configuration fields are deleted.
	 *
	 * @param &$csviregistry object Global register
	 */
	function UpdateTemplate(&$csviregistry) {
		$db = $csviregistry->GetObject('database');
		$csvilog = $csviregistry->GetObject('logger');
		$this->ValidateCSVInput($csviregistry);
		
		$this->changed = false;
		
		/* Check if the template type has changed */
		$q = "SELECT template_type FROM #__csvi_templates WHERE ID = ".$this->id;
		$db->query($q);
		
		if ($this->template_type != $db->f('template_type')) {
			$q = "DELETE FROM #__csvi_configuration WHERE field_template_id = ".$this->id;
			$db->query($q);
		}
		/* Update the template */
		$q = "UPDATE #__csvi_templates ";
		$q .= "SET ";
		if (!empty($this->template_name)) $q .= "template_name = '".$this->template_name."', ";
		$q .= "template_type = '".$this->template_type."', ";
		$q .= "skip_first_line = '".$this->skip_first_line."', ";
		$q .= "use_column_headers = '".$this->use_column_headers."', ";
		$q .= "collect_debug_info = '".$this->collect_debug_info."', ";
		$q .= "overwrite_existing_data = '".$this->overwrite_existing_data."', ";
		$q .= "skip_default_value = '".$this->skip_default_value."', ";
		$q .= "show_preview = '".$this->show_preview."', ";
		$q .= "include_column_headers = '".$this->include_column_headers."', ";
		$q .= "text_enclosure = '".$db->getEscaped($this->text_enclosure)."', ";
		$q .= "field_delimiter = '".$db->getEscaped($this->field_delimiter)."', ";
		$q .= "export_type = '".$db->getEscaped($this->export_type)."', ";
		$q .= "export_site = '".$db->getEscaped($this->export_site)."', ";
		$q .= "thumb_width = ".$this->thumb_width.", ";
		$q .= "thumb_height = ".$this->thumb_height.", ";
		$q .= "shopper_group_id = ".$this->shopper_group_id.", ";
		$q .= "producturl_suffix = '".$db->getEscaped($this->producturl_suffix)."', ";
		$q .= "file_location = '".$db->getEscaped($this->file_location)."', ";
		$q .= "product_publish = '".$this->product_publish."', ";
		$q .= "max_execution_time = '".$this->max_execution_time."', ";
		$q .= "max_input_time = '".$this->max_input_time."', ";
		$q .= "memory_limit = '".$this->memory_limit."', ";
		$q .= "post_max_size = '".$this->post_max_size."', ";
		$q .= "upload_max_filesize = '".$this->upload_max_filesize."', ";
		$q .= "export_filename = '".$db->getEscaped($this->export_filename)."', ";
		$q .= "manufacturer = '".$this->manufacturer."' ";
		$q .= "WHERE ID = ".$this->id;
		if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Line '.$csviregistry->currentline.': Updating template: <a onclick="switchMenu(\''.$this->template_name.'_update_template\');" title="Show/hide query">Show/hide query</a><div id="'.$this->template_name.'_update_template" class="debug">'.htmlentities($q).'</div>');
		if ($db->query($q)) {
			$csvilog->AddMessage('info','Line '.$csviregistry->currentline.': Template has been updated');
			$this->changed = true;
		}
		else {
			if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', '<span class="sqlerror">SQL Error</span>: <a onclick="switchMenu(\''.$this->template_name.'_update_template_error\');" title="Show/hide error">Show/hide error</a><div id="'.$this->template_name.'_update_template_error" class="debug">'.mysql_error().'</div>');
			$csvilog->AddMessage('info','Line '.$csviregistry->currentline.': Not able to update template<br />'.stripslashes($db->getErrorMsg()));
		}
	}
	
	function DeleteTemplate(&$csviregistry) {
		$db = $csviregistry->GetObject('database');
		$csvilog = $csviregistry->GetObject('logger');
		
		// Delete the template
		$q = "DELETE FROM #__csvi_templates WHERE ID = ".$this->id;
		if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Line '.$csviregistry->currentline.': Deleting template: <a onclick="switchMenu(\''.$this->id.'_delete_template\');" title="Show/hide query">Show/hide query</a><div id="'.$this->id.'_delete_template" class="debug">'.htmlentities($q).'</div>');
		if ($db->query($q)) {
			$csvilog->AddMessage('info','Template has been deleted');
			// Delete the fields related to the template
			$q = "DELETE FROM #__csvi_configuration WHERE field_template_id = ".$this->id;
			if ($db->query($q)) $csvilog->AddMessage('info','Line '.$csviregistry->currentline.': Template fields has been updated');
			$this->GetTemplatesList($csviregistry);
			$this->id = null;
			$csviregistry->SetVar('template', null);
		}
		else {
			if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', '<span class="sqlerror">SQL Error</span>: <a onclick="switchMenu(\''.$this->id.'_delete_template_error\');" title="Show/hide error">Show/hide error</a><div id="'.$this->id.'_delete_template_error" class="debug">'.mysql_error().'</div>');
			$csvilog->AddMessage('info','Line '.$csviregistry->currentline.': Not able to delete template');
		}
	}
	
	/**
	 * Checks the field for existing value, if not set the default value if allowed
	 */
	function ValidateCSVInput(&$csviregistry) {
		$this->type = $csviregistry->GetVar("type");
		$this->id = $csviregistry->GetVar("templateid");
		if ($csviregistry->IssetVar('template_name')) $this->template_name = $csviregistry->GetVar("template_name");
		if ($csviregistry->IssetVar('template_type')) $this->template_type = $csviregistry->GetVar("template_type");
		if ($csviregistry->IssetVar('skip_first_line')) $this->skip_first_line = 1;
		else $this->skip_first_line = 0;
		if ($csviregistry->IssetVar('use_column_headers')) $this->use_column_headers = 1;
		else $this->use_column_headers = 0;
		if ($csviregistry->IssetVar('collect_debug_info')) $this->collect_debug_info = 1;
		else $this->collect_debug_info = 0;
		if ($csviregistry->IssetVar('overwrite_existing_data')) $this->overwrite_existing_data = 1;
		else $this->overwrite_existing_data = 0;
		if ($csviregistry->IssetVar('skip_default_value')) $this->skip_default_value = 1;
		else $this->skip_default_value = 0;
		if ($csviregistry->IssetVar('show_preview')) $this->show_preview = 1;
		else $this->show_preview = 0;
		if ($csviregistry->IssetVar('include_column_headers')) $this->include_column_headers = 1;
		else $this->include_column_headers = 0;
		if ($csviregistry->IssetVar('text_enclosure')) $this->text_enclosure = stripslashes($csviregistry->GetVar("text_enclosure"));
		if ($csviregistry->IssetVar('field_delimiter')) $this->field_delimiter = stripslashes($csviregistry->GetVar("field_delimiter"));
		if ($csviregistry->IssetVar('export_type')) $this->export_type = stripslashes($csviregistry->GetVar("export_type"));
		if ($csviregistry->IssetVar('export_site')) $this->export_site = stripslashes($csviregistry->GetVar("export_site"));
		/* Set the thumb image sizes */
		if ($csviregistry->IssetVar('thumb_width')) {
			if ($csviregistry->GetVar("thumb_width") == 0) $this->thumb_width = 90;
			else $this->thumb_width = $csviregistry->GetVar("thumb_width");
		}
		if ($csviregistry->IssetVar('thumb_height')) {
			if ($csviregistry->GetVar("thumb_height") == 0) $this->thumb_height = 90;
			else $this->thumb_height = $csviregistry->GetVar("thumb_height");
		}
		if ($csviregistry->IssetVar('shopper_group_id')) $this->shopper_group_id = $csviregistry->GetVar("shopper_group_id");
		if ($csviregistry->IssetVar('producturl_suffix')) $this->producturl_suffix = $csviregistry->GetVar("producturl_suffix");
		if ($csviregistry->IssetVar('file_location')) $this->file_location = $csviregistry->GetVar("file_location");
		if ($csviregistry->IssetVar('product_publish')) $this->product_publish = $csviregistry->GetVar("product_publish");
		if ($csviregistry->IssetVar('max_execution_time')) $this->max_execution_time = $csviregistry->GetVar("max_execution_time");
		if ($csviregistry->IssetVar('max_input_time')) $this->max_input_time = $csviregistry->GetVar("max_input_time");
		if ($csviregistry->IssetVar('memory_limit')) $this->memory_limit = $csviregistry->GetVar("memory_limit");
		if ($csviregistry->IssetVar('post_max_size')) $this->post_max_size = $csviregistry->GetVar("post_max_size");
		if ($csviregistry->IssetVar('upload_max_filesize')) $this->upload_max_filesize = $csviregistry->GetVar("upload_max_filesize");
		if ($csviregistry->IssetVar('export_filename')) $this->export_filename = $csviregistry->GetVar("export_filename");
		if ($csviregistry->IssetVar('manufacturer')) $this->manufacturer = $csviregistry->GetVar("manufacturer");
		else if ($csviregistry->IssetArray('manufacturer')) {
			$mf_string = '';
			foreach ($csviregistry->GetArray('manufacturer') as $key => $mf_id) {
				$mf_string .= $mf_id.",";
			}
			$this->manufacturer = substr($mf_string, 0, -1);
		}
	}
	
	function GetNumberOfFields($id, $return = false) {
		$db = new CsviDatabase();
		
		$q = "SELECT COUNT(id) AS numberoffields FROM #__csvi_configuration ";
		$q .= "WHERE field_template_id = ".$id." ";
		$q .= "AND published = 1";
		$db->query($q);
		if (!$return) $this->numberoffields = $db->f('numberoffields');
		else return $db->f('numberoffields');
	}
	
	function GetShopperGroupName() {
		$db = new CsviDatabase();
		
		$q = "SELECT shopper_group_name FROM #__vm_shopper_group WHERE shopper_group_id = ".$this->shopper_group_id;
		$db->query($q);
		$this->shopper_group_name = $db->f('shopper_group_name');
	}
	
	function GetTemplateId($name) {
		$db = new CsviDatabase();
		
		$q = "SELECT id FROM #__csvi_templates ";
		$q .= "WHERE template_name = '".$name."'";
		if ($db->query($q)) return $db->f('id');
		else return false;
	}
	
	function CloneTemplate(&$csviregistry) {
		$db = $csviregistry->GetObject('database');
		$csvilog = $csviregistry->GetObject('logger');
		
		$q = "INSERT INTO `#__csvi_templates` (
			`template_name` ,
			`template_type` ,
			`skip_first_line` ,
			`use_column_headers` ,
			`collect_debug_info` ,
			`overwrite_existing_data` ,
			`skip_default_value` ,
			`show_preview` ,
			`include_column_headers` ,
			`text_enclosure` ,
			`field_delimiter` ,
			`export_type` ,
			`export_site` ,
			`thumb_width` ,
			`thumb_height` ,
			`shopper_group_id` ,
			`producturl_suffix` ,
			`product_publish`,
			`max_execution_time`,
			`max_input_time`,
			`memory_limit`,
			`post_max_size`,
			`upload_max_filesize`,
			`export_filename`
			)
			(SELECT concat( `template_name` , '_".date('Ymd_His', time())."' ) , 
				`template_type` , 
				`skip_first_line` , 
				`use_column_headers` , 
				`collect_debug_info` , 
				`overwrite_existing_data` , 
				`skip_default_value` , 
				`show_preview` , 
				`include_column_headers` , 
				`text_enclosure` , 
				`field_delimiter` , 
				`export_type` , 
				`export_site` , 
				`thumb_width` , 
				`thumb_height` , 
				`shopper_group_id` , 
				`producturl_suffix` ,
				`product_publish`,
				`max_execution_time`,
				`max_input_time`,
				`memory_limit`,
				`post_max_size`,
				`upload_max_filesize`,
				`export_filename`
				FROM #__csvi_templates
				WHERE ID=".$this->id."
			)";
		if ($db->query($q)) {
			$csvilog->AddMessage('info','Template has been cloned');
			/* Now duplicate the fields */
			$q = "INSERT INTO `#__csvi_configuration` (
			`field_template_id` ,
			`field_name` ,
			`field_column_header` ,
			`field_default_value` ,
			`field_order`
			)
			(SELECT ".$db->last_insert_id()." , 
				`field_name` ,
				`field_column_header` ,
				`field_default_value` ,
				`field_order`
				FROM #__csvi_configuration
				WHERE `field_template_id`=".$this->id."
			)";
			if ($db->query($q)) $csvilog->AddMessage('info','Template fields have been cloned');
			else {
				$csvilog->AddMessage('info','Not able to clone template fields <br />'.$db->getErrorMsg());
			}
			$this->GetTemplatesList($csviregistry);
			$this->id = null;
			$csviregistry->SetVar('template', null);
			return true;
		}
		else {
			$csvilog->AddMessage('info','Not able to clone template'.$db->getErrorMsg());
			return false;
		}
	}
	
	/**
	 * Sets the system limits to the user preferences
	 *
	 * @param &$csviregistry object Global register
	 */
	function SystemLimits() {
		if ($this->max_execution_time == -1 || empty($this->max_execution_time)) $this->max_execution_time = intval(ini_get('max_execution_time'));
		if ($this->max_input_time == -1) $this->max_input_time = intval(ini_get('max_input_time'));
		if ($this->memory_limit == -1) $this->memory_limit = intval(ini_get('memory_limit'));
		if ($this->post_max_size == -1) $this->post_max_size = intval(ini_get('post_max_size'));
		if ($this->upload_max_filesize == -1) $this->upload_max_filesize = intval(ini_get('upload_max_filesize'));
	}
}
?>
