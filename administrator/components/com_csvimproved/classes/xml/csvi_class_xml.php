<?php
/**
 * CSV Improved XML class
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Export
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_class_xml_froogle.php 186 2008-04-06 17:14:37Z Roland $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * CSV Improved XML Export Class
 *
 * @package CSVImproved
 * @subpackage Export
 */
class CsviXmlExport {
	
	/** @var string contains the data to export */
	var $contents = "";
	/** @var string contains the XML node to export */
	var $node = "";
	
	/**
	 * Creates the XML header
	 *
	 * @see $contents
	 * @return string XML header
	 */
	function HeaderText() {
		$this->contents = '<?xml version="1.0" encoding="UTF-8"?>'.chr(10);
		$this->contents .= '<channel>'.chr(10);
		return $this->contents;
	}
	
	/**
	 * Creates the XML footer
	 *
	 * @see $contents
	 * @return string XML header
	 */
	function FooterText() {
		$this->contents = '</channel>'.chr(10);
		return $this->contents;
	}
	
	/**
	 * Opens an XML item node
	 *
	 * @see $contents
	 * @return string XML node data
	 */
	function NodeStart() {
		$this->contents = '<item>'.chr(10); 
		return $this->contents;
	}
	
	/**
	 * Closes an XML item node
	 *
	 * @see $contents
	 * @return string XML node data
	 */
	function NodeEnd() {
		$this->contents = '</item>'.chr(10); 
		return $this->contents;
	}
	
	/**
	 * Adds an XML element
	 *
	 * @see $node
	 * @return string XML element
	 */
	function Element($column_header, $cdata=false) {
		$this->node = '<'.$column_header.'>';
		if ($cdata) $this->node .= '<![CDATA[';
		$this->node .= $this->contents;
		if ($cdata) $this->node .= ']]>';
		$this->node .= '</'.$column_header.'>';
		$this->node .= "\n";
		return $this->node;
	}
	
	/**
	 * Handles all content and modifies special cases
	 *
	 * @see $contents
	 * @return string formatted XML element
	 */
	function ContentText($content, $column_header, $fieldname, $cdata=false) {
		switch ($fieldname) {
			default:
				$this->contents = $content;
				break;
		}
		return $this->Element($column_header, $cdata);
	}
}
?>
