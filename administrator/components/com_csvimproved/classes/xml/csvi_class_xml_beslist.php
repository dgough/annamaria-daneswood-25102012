<?php
/**
 * beslist.nl XML class
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Export
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_class_xml_beslist.php 186 2008-04-06 17:14:37Z Roland $
 */
 
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * XML Export for beslist.nl
 *
 * @package CSVImproved
 * @subpackage Export
 *
 */
class CsviXmlBeslist {
	
	var $name = "CsviXmlBeslist";
	var $contents = "";
	var $node = "";
	
	function HeaderText() {
		$this->contents = '<?xml version="1.0" encoding="UTF-8"?>'.chr(10);
		$this->contents .= '<export>'.chr(10);
		return $this->contents;
	}
	
	function FooterText() {
		$this->contents = '</export>'.chr(10);
		return $this->contents;
	}
	
	function NodeStart() {
		$this->contents = '<product>'.chr(10); 
		return $this->contents;
	}
	
	function NodeEnd() {
		$this->contents = '</product>'.chr(10); 
		return $this->contents;
	}
	
	function Element($column_header, $cdata=false) {
		$this->node = '<'.$column_header.'>';
		if ($cdata) $this->node .= '<![CDATA[';
		$this->node .= $this->contents;
		if ($cdata) $this->node .= ']]>';
		$this->node .= '</'.$column_header.'>';
		return $this->node;
	}
	
	function ContentText($content, $column_header, $fieldname) {
		switch ($fieldname) {
			case 'category_path':
				$this->CategoryPath($content);
				break;
			case 'product_url':
				$this->ProductUrl($content);
				break;
			default:
				$this->contents = $content;
				break;
		}
		return $this->Element($column_header);
	}
	
	function CategoryPath($category) {
		$this->contents = str_replace("/", " &gt; ", trim($category));
	}
	
	function ProductUrl($product_url) {
		$this->contents = str_replace("&", "&amp;", $product_url);
	}
}
?>
