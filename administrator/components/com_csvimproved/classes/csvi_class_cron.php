<?php
/**
 * Cron handler
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_class_cron.php 210 2008-04-21 07:32:34Z Suami $
 */

 
/**
 * Cron handler
 */
/* Get the Joomla framework */
/* Set flag that this is a parent file */
define( '_VALID_MOS', 1 );
$basepath = str_ireplace('administrator/components/com_csvimproved/classes/csvi_class_cron.php', '', str_ireplace('\\', '/', __FILE__));
require( $basepath.'/globals.php' );
$basepath = str_ireplace('administrator/components/com_csvimproved/classes/csvi_class_cron.php', '', str_ireplace('\\', '/', __FILE__));
require( $basepath.'/configuration.php' );
require_once( $basepath.'/includes/joomla.php' );

/**
 * Handles all cron requests
 *
 * @package CSVImproved
 */
class CsviCron {
	
	/** @var $basepath string the base of the installation */
	var $basepath = '';
	
	/**
	 * Initialise some settings
	 */
	function CsviCron() {
		global $database;
		
		/* Collect the variables */
		$this->CollectVariables();
		
		/* First check if we deal with a valid user */
		if ($this->Login()) {
			/* Second check if any template is set to process */
			$template_name = stripslashes(strval(mosGetParam($_REQUEST, 'template_name', '')));
			if ($template_name) {
				/* There is a template name, get some details to streamline processing */
				$q = "SELECT ID, template_type FROM #__csvi_templates WHERE template_name = '".$template_name."'";
				$database->setQuery($q);
				$database->loadObject($row);
				if (is_object($row)) {
					if ($row->template_type) {
						/* Set the export type */
						if (stristr($row->template_type, 'export')) $_REQUEST['task'] = 'export';
						else $_REQUEST['task'] = 'import';
						/* Set the template ID */
						$_REQUEST['templateid'] = $row->ID;
						
						/* Set the file details */
						/* Set output to file as we cannot download anything */
						$_REQUEST['exportto'] = 'tofile';
						
						/* Get the filename from the user input */
						if (isset($_REQUEST['filename'])) {
							$_REQUEST['local_csv_file'] = stripslashes(strval(mosGetParam($_REQUEST, 'filename')));
							/* Tell the system it is a local file */
							$_REQUEST['selectfile'] = 2;
						}
						else {
							/* Tell the system there is no file */
							$_REQUEST['selectfile'] = 0;
						}
						$this->ExecuteJob();
						$this->CronOutput();
						
					}
				}
				else echo 'No template found with the name '.$template_name;
			}
			else echo 'No template name specified';
		}
		else echo 'No valid user';
	}
	
	/**
	 * Collect the variables
	 *
	 * Running from the command line, the variables are stored in $argc and $argv.
	 * Here we put them in $_REQUEST so that they are available to the script
	 */
	function CollectVariables() {
		/* Take the argument values and put them in $_REQUEST */
		if (isset($_SERVER['argv'])) {
			foreach ($_SERVER['argv'] as $key => $argument) {
				if ($key > 0) {
					list($name, $value) = split("=", $argument);
					$_REQUEST[$name] = $value;
				}
			}
		}
		
		/* Get the basepath */
		$this->basepath = str_ireplace('administrator/components/com_csvimproved/classes/csvi_class_cron.php', '', str_ireplace('\\', '/', __FILE__));
	}
	
	/**
	 * Check if the user exists
	 */
	function Login() {
		global $database;
		$username = stripslashes( strval( mosGetParam( $_REQUEST, 'username', '' ) ) );
		$passwd = stripslashes( strval( mosGetParam( $_REQUEST, 'passwd', '' ) ) );
		
		$query = "SELECT id, name, username, password, usertype, block, gid"
				. "\n FROM #__users"
				. "\n WHERE username = ". $database->Quote( $username )
				;
		$database->setQuery( $query );
		$database->loadObject( $row );
		
		if (is_object($row)) {
			if ($row->block == 1) {
				echo 'User is blocked';
				return false;
			}
			if ((strpos($row->password, ':') === false) && $row->password == md5($passwd)) {
				// Old password hash storage but authentic ... lets convert it
				$salt = mosMakePassword(16);
				$crypt = md5($passwd.$salt);
				$row->password = $crypt.':'.$salt;
			}
			
			list($hash, $salt) = explode(':', $row->password);
			$cryptpass = md5($passwd.$salt);
			if ($hash != $cryptpass) {
				echo 'Password incorrect';
				return false;
			}
			else {
				echo 'User '.$row->username.' logged in at '.date('Y-m-d H:i:s', time())."\n";
				return true;
			}
		}
		else return false;
	}
	 
	/**
	 * Process the requested job
	 */
	function ExecuteJob() {
		$cron = true;
		require( $this->basepath.'/administrator/components/com_csvimproved/admin.csvimproved.php' );
	}
	
	/**
	 * Output cron results
	 */
	function CronOutput() {
		$csviregistry =& $GLOBALS['csviregistry'];
		$csvilog = $csviregistry->GetObject('logger');
		$csvilang = $csviregistry->GetObject('language');
		/* strings to find */
		$find = array();
		$find[] = '<br />';
		$find[] = '<strong>';
		$find[] = '</strong>';
		/* strings to replace with */
		$replace = array();
		$replace[] = "";
		$replace[] = "";
		$replace[] = "";
		
		foreach ($csvilog->stats as $action => $stats) {
			if ($stats['count'] > 0) {
				$action = strtoupper("OUTPUT_".$action);
				echo $csvilang->$action.": ".$stats['count']."\n";
				echo str_repeat("=", 25)."\n";
				echo str_replace($find, $replace, $stats['message'])."\n";
			}
		}
	 }
}
$csvicron = new CsviCron;
?>
