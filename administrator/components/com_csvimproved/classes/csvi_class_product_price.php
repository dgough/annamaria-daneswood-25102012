<?php
/**
 * Product pricing class
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Import
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_class_product_price.php 286 2008-06-01 02:51:30Z Suami $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * ps_product_price
 *
 * @package CSVImproved
 * @subpackage Import
 */
class ps_product_price {
	var $classname = "ps_product_price";

	/**************************************************************************
	** name: validate()
	** created by:
	** description:
	** parameters:
	** returns:
	***************************************************************************/
	function validate(&$d) {
		$valid = true;
		$d["error"] = "";
		if (!isset($d["product_price"])) {
			$d["error"] .= "ERROR: A price must be entered.";
			$valid = false;
		}
		if (empty($d["product_id"])) {
			$d["error"] .= "ERROR: A product ID is missing.";
			$valid = false;
		}
		// convert all "," in prices to decimal points.
		if (stristr($d["product_price"],","))
		$d['product_price'] = str_replace(',', '.', $d["product_price"]);

		if (!$d["product_currency"]) {
			$d["error"] .= "ERROR: A currency must be entered.";
			$valid = false;
		}
		$d["price_quantity_start"] = intval($d["price_quantity_start"]);
		$d["price_quantity_end"] = intval($d["price_quantity_end"]);

		if ($d["price_quantity_end"] < $d["price_quantity_start"]) {
			$d["error"] .= "ERROR: The entered Quantity End is less than the Quantity Start.";
			$valid = false;
		}

		$db = new CsviDatabase();
		$q = "SELECT count(*) AS num_rows FROM #__vm_product_price WHERE";
		if (!empty($d["product_price_id"])) {
			$q .= " product_price_id != '".$d['product_price_id']."' AND";
		}
		$q .= " shopper_group_id = '".$d["shopper_group_id"]."'";
		$q .= " AND product_id = '".$d['product_id']."'";
		$q .= " AND product_currency = '".$d['product_currency']."'";
		$q .= " AND (('".$d['price_quantity_start']."' >= price_quantity_start AND '".$d['price_quantity_start']."' <= price_quantity_end)";
		$q .= " OR ('".$d['price_quantity_end']."' >= price_quantity_start AND '".$d['price_quantity_end']."' <= price_quantity_end))";
		$db->query( $q ); $db->next_record();

		/**
		if ($db->f("num_rows") > 0) {
			// $d["error"] .= "ERROR: This product already has a price for the selected Shopper Group and the specified Quantity Range.";
			$this->update($d);
			$valid = true;
		}
		*/
		return $valid;
	}

	/**************************************************************************
	** name: add()
	** created by:
	** description:
	** parameters:
	** returns:
	***************************************************************************/
	function add(&$csviregistry, &$d) {
		if (!$this->validate($d)) {
			return false;
		}

		$timestamp = time();
		if (empty($d["product_price_vdate"])) $d["product_price_vdate"] = '';
		if (empty($d["product_price_edate"])) $d["product_price_edate"] = '';

		$db = $csviregistry->GetObject('database');
		$q  = "INSERT INTO #__vm_product_price (product_id,shopper_group_id,";
		$q .= "product_price,product_currency,product_price_vdate,";
		$q .= "product_price_edate,cdate,mdate,price_quantity_start,price_quantity_end) ";
		$q .= "VALUES ('" . $d["product_id"] . "','" . $d["shopper_group_id"];
		$q .= "','" . $d["product_price"] . "','" . $d["product_currency"] . "','";
		$q .= $d["product_price_vdate"] . "','" . $d["product_price_edate"] . "',";
		$q .= "'$timestamp','$timestamp', '".$d["price_quantity_start"]."','".$d["price_quantity_end"]."')";

		if (!$db->query($q)) return false;
		else return true;
	}

	/**************************************************************************
	** name: update()
	** created by:
	** description:
	** parameters:
	** returns:
	***************************************************************************/
	function update(&$csviregistry, &$d) {
		if (!$this->validate($d)) {
			return false;
		}

		$timestamp = time();

		$db = $csviregistry->GetObject('database');
		if (empty($d["product_price_vdate"])) $d["product_price_vdate"] = '';
		if (empty($d["product_price_edate"])) $d["product_price_edate"] = '';

		$q  = "UPDATE #__vm_product_price SET ";
		$q .= "shopper_group_id='" . $d["shopper_group_id"] . "',";
		$q .= "product_id='" . $d["product_id"] . "',";
		$q .= "product_price='" . $d["product_price"] . "',";
		$q .= "product_currency='" . $d["product_currency"] . "',";
		$q .= "product_price_vdate='" . $d["product_price_vdate"] . "',";
		$q .= "product_price_edate='" . $d["product_price_edate"] . "',";
		$q .= "price_quantity_start='" . $d["price_quantity_start"] . "',";
		$q .= "price_quantity_end='" . $d["price_quantity_end"] . "',";
		$q .= "mdate='$timestamp' ";
		$q .= "WHERE product_price_id='" . $d["product_price_id"] . "' ";
		if (!$db->query($q)) return false;
		else	return true;
	}

	/**
	* Controller for Deleting Records.
	*/
	function delete(&$d) {

		$record_id = $d["product_price_id"];

		if( is_array( $record_id)) {
			foreach( $record_id as $record) {
				if( !$this->delete_record( $record, $d ))
				return false;
			}
			return true;
		}
		else {
			return $this->delete_record( $record_id, $d );
		}
	}
	/**
	* Deletes one Record.
	*/
	function delete_record($csviregistry, $record_id) {
		$db = $csviregistry->GetObject('database');
		$q  = "DELETE FROM #__vm_product_price ";
		$q .= "WHERE product_price_id = '$record_id' ";
		if (!$db->query($q)) return false;
		else return true;
	}


}
?>