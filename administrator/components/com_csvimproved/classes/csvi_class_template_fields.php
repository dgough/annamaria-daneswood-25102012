<?php
/**
 * Configuration class
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Templates
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_class_template_fields.php 243 2008-05-18 04:25:45Z Suami $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * Class for handling field configuration
 *
 * @package CSVImproved
 * @subpackage Templates
 */
class FieldConfig {
	
	var $templateid = 0;
	var $field_name = '';
	var $column_header = '';
	var $default_value = '';
	var $field_order = 0;
	var $field_id = null;
	
	function FieldConfig() {
		
	}
	
	/**
	 * Retrieve fields for a template
	 *
	 * Retrieves all fields for a template or retrieves a limited number of
	 * fields if set
	 *
	 * @param &$csviregistry array Global Register
	 * @param $pagenav object Page navigation object
	 */
	function GetFields(&$csviregistry, $pagenav='') {
		$db = $csviregistry->GetObject('database');
		$q = "SELECT * FROM #__csvi_configuration 
			WHERE field_template_id = ".$csviregistry->GetVar('templateid')." ";
		if (strlen($csviregistry->GetVar('filterfield')) > 0) $q .= "AND field_name LIKE '%".$csviregistry->GetVar('filterfield')."%' ";
		$q .= "ORDER BY published DESC, field_order, field_name";
		
		if ($pagenav) $db->query($q, $pagenav->limitstart, $pagenav->limit);
		else $db->query($q);
		
		return $db->loadObjectList();
	}
	
	function GetFieldsList(&$csviregistry) {
		$db = $csviregistry->GetObject('database');
		$q = "SELECT field_name FROM #__csvi_configuration 
			WHERE field_template_id = ".$csviregistry->GetVar('templateid')." ORDER BY field_order";
		$db->query($q);
		return $db->loadResultArray();
	}
	
	function AddConfigField(&$csviregistry) {
		// Saving a config field
		$csvidb = $csviregistry->GetObject('database');
		$csvilog = $csviregistry->GetObject('logger');
		$csvilang = $csviregistry->GetObject('language');
		if ($csviregistry->GetVar('task') == 'saveconfigfield') {
			$this->templateid = $csviregistry->GetVar('templateid');
			$this->field_name = $csviregistry->GetVar('field_name');
			$this->column_header = $csvidb->getEscaped($csviregistry->GetVar('column_header'));
			$this->default_value = $csvidb->getEscaped($csviregistry->GetVar('default_value'));
			$this->field_order = $csviregistry->GetVar('field_order');
		}
		
		// NOTE: If the field name is empty don't save it
		if (empty($this->field_name)) {
			$csvilog->AddMessage('info',$csvilang->NO_FIELD_NAME);
			return false;
		}
		
		$q = "INSERT INTO #__csvi_configuration 
			(field_template_id, field_name, field_column_header, field_default_value, field_order, published) 
			VALUES (".$this->templateid.", '".$this->field_name."','".$this->column_header."','".$this->default_value."', '".$this->field_order."',";
		if ($csviregistry->GetVar('setpublished')) $q .= "1";
		else $q .= "0";
		$q .= ")";
		
		if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Adding template field: <a onclick="switchMenu(\''.$this->field_name.'_add_template_field\');" title="Show/hide query">Show/hide query</a><div id="'.$this->field_name.'_add_template_field" class="debug">'.htmlentities($q).'</div>');
		if ($csvidb->query($q)) {
			$csvilog->AddMessage('info','Template field has been added');
			return true;
		}
		else {
			if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', '<span class="sqlerror">SQL Error</span>: <a onclick="switchMenu(\''.$this->field_name.'_add_template_field_error\');" title="Show/hide error">Show/hide error</a><div id="'.$this->field_name.'_add_template_field_error" class="debug">'.$csvidb->getErrorMsg().'</div>');
			$csvilog->AddMessage('info','Not able to add template field<br />'.$csvidb->getErrorMsg());
			return false;
		}
	}
	
	function UpdateConfigField(&$csviregistry) {
		// Update fields
		$csvidb = $csviregistry->GetObject('database');
		$csvilog = $csviregistry->GetObject('logger');
		$fields = $csviregistry->GetArray('field');
		
		if ($csviregistry->GetVar('task') == 'saveconfigtable') {
			if (is_array($fields)) {
				/* The {fill} is used for adding new fields dynamically */
				unset($fields['{fill}']);
				foreach ($fields as $id => $field) {
					$this->field_name = $csvidb->getEscaped($field['_field_name']);
					if (isset($field['_column_header'])) $this->column_header = $csvidb->getEscaped($field['_column_header']);
					$this->default_value = $csvidb->getEscaped($field['_default_value']);
					$this->field_order = $field['_order'];
					$this->field_id = $field['_id'];
				
					$q = "UPDATE #__csvi_configuration 
						SET field_template_id = ".$csviregistry->GetVar('templateid').",
						field_name = '".$this->field_name."',
						field_column_header = '".$this->column_header."',
						field_default_value = '".$this->default_value."',
						field_order = '".$this->field_order."' 
						WHERE id = ".$this->field_id;
					if (!$csvidb->query($q)) {
						$csvilog->AddMessage('info','Not able to update template field<br />'.stripslashes($csvidb->getErrorMsg()));
						return false;
					}
				}
			}
			$csvilog->AddMessage('info','Template fields have been updated');
			return true;
		}
		else {
			$q = "UPDATE #__csvi_configuration 
				SET field_template_id = ".$this->templateid.",
				field_name = '".$this->field_name."',
				field_column_header = '".$this->column_header."',
				field_default_value = '".$this->default_value."',
				field_order = '".$this->field_order."' 
				WHERE id = ".$this->field_id;
			if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Update template field: <a onclick="switchMenu(\''.$this->field_name.'_update_template_field\');" title="Show/hide query">Show/hide query</a><div id="'.$this->field_name.'_update_template_field" class="debug">'.htmlentities($q).'</div>');
			if ($csvidb->query($q) || $csvidb->getErrorNum() == 0) {
				$csvilog->AddMessage('info','Template field '.$this->field_name.' has been updated');
				return true;
			}
			else {
				$csvilog->AddMessage('info','Not able to update template field<br />'.stripslashes($csvidb->getErrorMsg()));
				return false;
			}
		}
	}
	
	function DeleteConfigField(&$csviregistry) {
		$db = $csviregistry->GetObject('database');
		$q = "DELETE from #__csvi_configuration WHERE id='".$csviregistry->GetVar("field_id")."'";
		return $db->query($q);
	}
	
	function DeleteConfigFieldsTemplate(&$csviregistry, $id) {
		$csvidb = $csviregistry->GetObject('database');
		$csvilog = $csviregistry->GetObject('logger');
		$q = "DELETE from #__csvi_configuration WHERE field_template_id='" .$id."'";
		if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Deleting all template fields: <a onclick="switchMenu(\''.$id.'_delete_template_field\');" title="Show/hide query">Show/hide query</a><div id="'.$id.'_delete_template_field" class="debug">'.htmlentities($q).'</div>');
		if ($csvidb->query($q)) return true;
		else {
			if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', '<span class="sqlerror">SQL Error</span>: <a onclick="switchMenu(\''.$id.'_delete_template_field_error\');" title="Show/hide error">Show/hide error</a><div id="'.$id.'_delete_template_field_error" class="debug">'.$csvidb->getErrorMsg().'</div>');
			return false;
		}
	}
	
	/**
	 * Publish fields
	 *
	 * @param &$csviregistry array Global register
	 * @return bool true|false true if all fields are published|false if there is an error
	 */
	function SwitchPublishFields(&$csviregistry) {
		$db = $csviregistry->GetObject('database');
		$cids = $csviregistry->GetArray('cid');
		$task = $csviregistry->GetVar('task');
		$ids = '';
		
		if (is_array($cids)) {
			$q = "UPDATE #__csvi_configuration
				SET published = ";
			if ($task == 'publish') $q .= "1 ";
			else if ($task == 'unpublish') $q .= "0 ";
			$q .= "WHERE id IN (";
			foreach ($cids as $key => $field_id) {
				$ids .= $field_id.', ';
			}
			$q .= substr($ids,0, -2).")";
			if ($db->query($q)) return true;
			else return false;
		}
		else return false;
		
	}
	
	/**
	 * Renumber fields
	 *
	 * Renumbers all fields ordered by published state
	 *
	 * @param &$csviregistry array Global Register
	 */
	function RenumberFields(&$csviregistry) {
		$db = $csviregistry->GetObject('database');
		$templateid = $csviregistry->GetVar('templateid');
		$csvilog = $csviregistry->GetObject('logger');
		$templates = $csviregistry->GetObject('templates');
		$q = "SELECT id
			FROM #__csvi_configuration
			WHERE field_template_id = ".$templateid."
			ORDER BY published DESC, field_order, field_name";
		$db->query($q);
		$fieldorder = $db->loadObjectList();
		$process = true;
		foreach ($fieldorder as $order => $field) {
			$q = "UPDATE #__csvi_configuration
				SET field_order = ".($order+1)."
				WHERE id = ".$field->id;
			if (!$db->query($q)) {
				$process = false;
				$csvilog->AddMessage('', 'Error saving field order: '.$db->getErrorMsg());
			}
		}
		if ($process) $csvilog->AddMessage('', 'New order has been saved');
	}
	
	/**
	 * Verify the template has all the required fields
	 *
	 * Verifies a template has all the necessary fields. If not, add the missing
	 * fields.
	 *
	 * @param &$csviregistry array Global Register
	 */
	function VerifyTemplateFields(&$csviregistry) {
		$db = $csviregistry->GetObject('database');
		$templates = $csviregistry->GetObject('templates');
		$csvisupportedfields = $csviregistry->GetObject('supportedfields');
		$templateid = $csviregistry->GetVar('templateid');
		
		/* Get the fields for the template type */
		$csvisupportedfields->GetSupportedFields($templates->template_type);
		$csvisupportedfields->fields = array_flip($csvisupportedfields->fields);
		
		/* Get the fields in database */
		$fields = FieldConfig::GetFields($csviregistry);
		
		if (is_array($fields)) {
			/* Check if field exists, if so remove it */
			foreach($fields as $id => $field) {
				if (isset($csvisupportedfields->fields[$field->field_name])) unset($csvisupportedfields->fields[$field->field_name]);
			}
		}
		
		/* Add leftover fields to the database as they are missing */
		foreach($csvisupportedfields->fields as $field_name => $key) {
			$q = "INSERT INTO #__csvi_configuration (field_template_id, field_name) 
				VALUES (".$templateid.", '".$field_name."')";
			$db->query($q);
		}
	}
}
?>
