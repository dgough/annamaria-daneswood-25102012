<?php
/**
 * Import processor class
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Import
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_class_import.php 386 2008-07-26 01:45:03Z Suami $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * Main processor for importing data
 *
 * @package CSVImproved
 * @subpackage Import
 */
class CsviImport {
	
	/** @var bool for overwriting data or not */
	var $overwrite_existing_data = false;
	
	/** @var integer for keeping track when the script started */
	var $starttime = 0;
	
	function CsviImport() {
		/* Set the start time of the script */
		$this->starttime = time();
	}
	
	/** 
	 * Function to check if execution time is going to be passed. 
	 *
	 * Memory can only be checked if the function memory_get_usage is available.
	 * If the function is not available always return true. This could lead to
	 * out of memory failure.
	 *
	 * @todo Add memory_get_usage check to system check page
	 * @see http://www.php.net/memory_get_usage
	 * @param &$csviregistry Global register
	 * @return bool true|false true when limits are not reached|false when limit is reached
	 */
	function CheckLimits(&$csviregistry) {
		$csvilog = $csviregistry->GetObject('logger');
		$csvilang = $csviregistry->GetObject('language');
		
		/* Check for maximum execution time */
		$timepassed = time() - $this->starttime;
		if (($timepassed + 10) > ini_get('max_execution_time') && ini_get('max_execution_time') > 0) {
			$csvilog->AddStats('information', $csvilang->EXECUTION_TIME_PASSED.$timepassed.$csvilang->IMPORT_SECONDS);
			return false;
		}
		
		/* Check for maximum memory usage */
		if (!function_exists('memory_get_usage')) return true;
		else {
			/** @internal Get the size of the statistics */
			$statslength = 0;
			foreach ($csvilog->stats as $type => $value) {
				$statslength += strlen($value['message']);
			}
			$statslength = round($statslength/(1024*1024));
			
			/* Get the size of the debug message */
			$debuglength = round(strlen($csvilog->debug_message)/(1024*1024));
			
			/* Get the size of the active memory in use*/
			$activelength = round(memory_get_usage()/(1024*1024));
			
			/* Combine memory limits*/
			$totalmem = $activelength + $statslength + $debuglength;
			
			/* Check if we are passing the memory limit */
			if (($totalmem * 2) > (int)ini_get('memory_limit')) {
				$csviregistry->SetVar('maxmem', $totalmem);
				$csvilog->AddStats('information', $csvilang->MEMORY_LIMIT_PASSED.$totalmem.$csvilang->IMPORT_MB);
				return false;
			}
			else $csviregistry->SetVar('maxmem', $totalmem);
			
			 /* All is good return true */
			return true;
		}
	}
	
	function CleanUp(&$csviregistry) {
		// NOTE: Check if there are any related products to update
		if ($this->processrelated) $this->PostProcessRelatedProducts();
		
		// NOTE: Check if there is any file left in the cache folder
		if (isset($d["was_preview"]) && $d["was_preview"] == "Y") {
			$remove_file = "";
			if ($d['task'] == 'cancel') $remove_file = $d['local_csv_file'];
			else $remove_file = $d['csv_file'];
			if (file_exists($remove_file)) @unlink($remove_file);
		}
		// NOTE: Make the debug messages global
		if ($this->debug) $d['csv_debug']['message'] = $this->csv_debug['message'];
		
		// NOTE: Empty some local settings
		$d['deleteconfigfieldstemplate'] = false;
		return true;
	}
	
	/**
	*	Check user options for import
  	**/
  	function ValidateImportChoices(&$csviregistry) {
		/* Get the template settings to see if we need a preview */
		$templates = $csviregistry->GetObject('templates');
		
		/* Overwrite existing data */
		$this->overwrite_existing_data = $templates->overwrite_existing_data;
		
		/* Show preview */
		$this->show_preview = $templates->show_preview;
		if ($templates->show_preview && $csviregistry->GetVar('was_preview')) {
			$templates->show_preview = false;
		}
		
		/* Default value */
		if ($templates->skip_default_value) $csviregistry->SetVar('skip_default_value', true);
		else $csviregistry->SetVar('skip_default_value', false);
		
		/* Skip first line */
		if ($templates->skip_first_line) $csviregistry->currentline++;
	}
	
	function ImportDetails($csviregistry) {
		/* Get the language */
		$CsviLang = $csviregistry->GetObject('language');
		/* Get the logger */
		$csvilog = $csviregistry->GetObject('logger');
		/* Get the template settings to see if we need a preview */
		$templates = $csviregistry->GetObject('templates');
		
		if ($csviregistry->GetVar('debug')) {
			$csvilog->AddMessage('debug', '<hr />');
			$csvilog->AddMessage('debug', 'Version: '.$CsviLang->VERSION);
			/* Show which template is used */
			$csvilog->AddMessage('debug', 'Using template: '.$templates->template_name);
			
			// NOTE: Check delimiter char
			$csvilog->AddMessage('debug', 'Using delimiter: '.$templates->field_delimiter);
			
			// NOTE: Check enclosure char
			$csvilog->AddMessage('debug', 'Using enclosure: '.$templates->text_enclosure);
			
			// NOTE: Skip first line
			if ($templates->skip_first_line) $csvilog->AddMessage('debug', 'Skipping the first line');
			else $csvilog->AddMessage('debug', 'Not skipping the first line');
			
			// NOTE: Skip default value
			if ($templates->skip_default_value) $csvilog->AddMessage('debug', 'Skip default value');
			else $csvilog->AddMessage('debug', 'Not skipping default value');
			
			// NOTE: Overwrite existing data
			if ($templates->overwrite_existing_data) $csvilog->AddMessage('debug', 'Overwriting data');
			else $csvilog->AddMessage('debug', 'Do not overwrite data');
			
			// NOTE: Use column headers as configuration
			if ($templates->use_column_headers) $csvilog->AddMessage('debug', 'Use column headers for configuration');
			else $csvilog->AddMessage('debug', 'Do not use column headers for configuration');
			
			// NOTE: Show preview
			if ($templates->show_preview && (!$csviregistry->IssetVar('was_preview'))) {
				$csvilog->AddMessage('debug', 'Using preview');
			}
			else {
				if ($csviregistry->GetVar('was_preview')) {
					$csvilog->AddMessage('debug', 'Preview used');
				}
				else $csvilog->AddMessage('debug', 'Not using preview');
			}
		}
	}
	
	/**
	 * Get the configuration fields the user wants to use
	 *
	 * The configuration fields can be taken from the uploaded file or from 
	 * the database. Depending on the template settings
	 *
	 * @todo Get supported fields from producttypenamesupload from product type database
	 * @see Templates()
	 * @param &$csviregistry array Global register
	 * @return bool true|false true when there are config fields|false when there are no or unsupported fields
	 */
	function RetrieveConfigFields(&$csviregistry) {
		$templates = $csviregistry->GetObject('templates');
		$csvilog = $csviregistry->GetObject('logger');
		$supportedfields = array_flip($csviregistry->GetArray('supported_fields'));
		$product_data = $csviregistry->GetArray('product_data');
		$db = $csviregistry->GetObject('database');
		$csv_fields = array();
		
		if ($templates->use_column_headers) {
			/* The user has column headers in the file */
			$csviregistry->SetVar('error_found', false);
			foreach ($product_data as $order => $name) {
				/* Trim the name in case the name contains any preceding or trailing spaces */
				$name = strtolower(trim($name));
				/* Check if the fieldname is supported */
				/* There are no supported fields for producttypenamesupload. */
				if (isset($supportedfields[$name]) || $templates->template_type == "ProductTypeNamesUpload") {
					if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Field: '.$name);
					$csv_fields[$name]["name"] = $name;
					$csv_fields[$name]["order"] = $order;
					/* Get the default value for the fields */
					$q = "SELECT field_default_value FROM #__csvi_configuration
						WHERE field_name='".$name."'
						AND field_template_id = '".$csviregistry->GetVar('templateid')."'";
					$db->query($q);
					$csv_fields[$name]["default_value"] = $db->f('field_default_value');
				}
				else {
					/* Check if the user has any field that is not supported */
					if (strlen($name) == 0) $name = 'Field: &lt;empty field&gt;';
					$csvilog->AddStats('nosupport', $name);
				}
			}
			if (isset($csvilog->stats['nosupport']) && $csvilog->stats['nosupport']['count'] > 0) {
				$csvilog->AddMessage('info', 'There are unsupported fields');
				$csviregistry->SetVar('error_found', true);
				return false;
			}
			if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Using the first line to setup the required fields');
		}
		else {
			/* The user has column headers in the database */
			/* Get row positions of each element as set in csv table */
			$q = "SELECT id,field_name,field_order,field_column_header,field_default_value FROM #__csvi_configuration 
				WHERE field_template_id = '".$csviregistry->GetVar('templateid')."'
				AND published = 1
				ORDER BY field_order";
			$db->query($q);
			$rows = $db->loadAssocList();
			foreach ($rows as $id => $row) {
				if (!empty($row["field_column_header"])) $field_name = $row["field_column_header"];
				else $field_name = $row["field_name"];
				if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Field: '.$field_name);
				$csv_fields[$field_name]["name"] = $field_name;
				$csv_fields[$field_name]["order"] = $row["field_order"];
				$csv_fields[$field_name]["default_value"] = $row["field_default_value"];
			}
			if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Use database for configuration');
		}
		$csviregistry->SetArray('csv_fields', $csv_fields);
		return true;
	}
	
	/**
	 * Check user options for import
	 * 
	 * @see csv_category()
  	 * @param &$csviregistry array Global register
  	 **/
  	function CheckCategoryPath(&$csviregistry) {
		$dbcat = $csviregistry->GetObject('database');
		$csvilog = $csviregistry->GetObject('logger');
		
		if (($this->product_details->category_path && !$this->product_details->child_product) || $this->product_details->category_id) {
			if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Checking category path');
			/* If product_parent_id is true, we have a child product, child products do not have category paths */
			/* if we have a category_id, no need to find the path */
			if (!$this->product_details->product_parent_id && !$this->product_details->category_id) {
				/* Use csv_category() method to confirm/add category tree for this product */
				/* Modification: $category_id now is an array */
				/* if (is_int($this->category_path)) $category */
				$this->product_details->category_id = $this->csv_category($csviregistry);
			}
			if (!$this->product_details->product_parent_id || $this->product_details->category_id) {
				/* Delete old entries */
				$q  = "DELETE FROM #__vm_product_category_xref WHERE product_id = '".$this->product_details->product_id."'";
				if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Delete old category references: <a onclick="switchMenu(\''.$this->product_details->product_sku.'_delete_old_cat_xref\');" title="Show/hide query">Show/hide query</a><div id="'.$this->product_details->product_sku.'_delete_old_cat_xref" class="debug">'.htmlentities($q).'</div>');
				$dbcat->query($q);
				
				/* Insert new product/category relationships */
				foreach( $this->product_details->category_id as $value ) {
					$q  = "INSERT INTO #__vm_product_category_xref (category_id, product_id ) VALUES (";
					$q .= "'$value', '".$this->product_details->product_id."')";
					if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Add new category references: <a onclick="switchMenu(\''.$this->product_details->product_sku.'_add_new_cat_xref'.$value.'\');" title="Show/hide query">Show/hide query</a><div id="'.$this->product_details->product_sku.'_add_new_cat_xref'.$value.'" class="debug">'.htmlentities($q).'</div>');
					if (!$dbcat->query($q)) if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', '<span class="sqlerror">SQL Error</span>: <a onclick="switchMenu(\''.$this->product_details->product_sku.'_product_category_xref_error\');" title="Show/hide error">Show/hide error</a><div id="'.$this->product_details->product_sku.'_product_category_xref_error" class="debug">'.mysql_error().'</div>');
				}
			}
		}
		else {
			if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Child product, not doing any category handling');
		}
	}
	
	/**
	 * name: csv_category()
	 * created by: John Syben
	 * Creates categories from slash delimited line
	 **/
	function csv_category(&$csviregistry) {
		$csvilog = $csviregistry->GetObject('logger');
		
		// NOTE: New: Get all categories in this field,
		// NOTE: delimited with |
		$categories = explode("|", $this->product_details->category_path);
		foreach( $categories as $line ) {
			// NOTE: Explode slash delimited category tree into array
			$category_list = explode("/", $line);
			$category_count = count($category_list);

			$db = $csviregistry->GetObject('database');
			$category_parent_id = '0';

			// NOTE: For each category in array
			for($i = 0; $i < $category_count; $i++) {
				// NOTE: See if this category exists with it's parent in xref
				$q = "SELECT #__vm_category.category_id FROM #__vm_category,#__vm_category_xref ";
				$q .= "WHERE #__vm_category.category_name='" . $db->getEscaped($category_list[$i]) . "' ";
				$q .= "AND #__vm_category_xref.category_child_id=#__vm_category.category_id ";
				$q .= "AND #__vm_category_xref.category_parent_id='$category_parent_id'";
				$db->query($q);
				// NOTE: If it does not exist, create it
				if ($db->next_record()) { // NOTE: Category exists
					$category_id = $db->f("category_id");
				}
				else { // NOTE: Category does not exist - create it

					$timestamp = time();

					// NOTE: Let's find out the last category in
					// NOTE: the level of the new category
					$q = "SELECT MAX(list_order) AS list_order FROM #__vm_category_xref,#__vm_category ";
					$q .= "WHERE category_parent_id='".$category_parent_id."' ";
					$q .= "AND category_child_id=category_id ";
					$db->query( $q );
					$db->next_record();

					$list_order = intval($db->f("list_order"))+1;

					// NOTE: Add category
					$q = "INSERT INTO #__vm_category ";
					$q .= "(vendor_id,category_name, category_publish,cdate,mdate,list_order) ";
					$q .= "VALUES ('1', '";
					$q .= $db->getEscaped($category_list[$i]) . "', '";
					$q .= "Y', '";
					$q .= $timestamp . "', '";
					$q .= $timestamp . "', '$list_order')";
					if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Add new category: <a onclick="switchMenu(\''.$db->getEscaped($category_list[$i]).'_add_category\');" title="Show/hide query">Show/hide query</a><div id="'.$db->getEscaped($category_list[$i]).'_add_category" class="debug">'.htmlentities($q).'</div>');
					if (!$db->query($q)) if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', '<span class="sqlerror">SQL Error</span>: <a onclick="switchMenu(\''.$db->getEscaped($category_list[$i]).'_category_error\');" title="Show/hide error">Show/hide error</a><div id="'.$category_list[$i].'_category_error" class="debug">'.mysql_error().'</div>');

					$category_id = $db->last_insert_id();

					// NOTE: Create xref with parent
					$q = "INSERT INTO #__vm_category_xref ";
					$q .= "(category_parent_id, category_child_id) ";
					$q .= "VALUES ('";
					$q .= $category_parent_id . "', '";
					$q .= $category_id . "')";
					if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Add new category xref: <a onclick="switchMenu(\''.$db->getEscaped($category_list[$i]).'_add_category_xref\');" title="Show/hide query">Show/hide query</a><div id="'.$db->getEscaped($category_list[$i]).'_add_category_xref" class="debug">'.htmlentities($q).'</div>');
					if (!$db->query($q)) if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', '<span class="sqlerror">SQL Error</span>: <a onclick="switchMenu(\''.$db->getEscaped($category_list[$i]).'_category_xref_error\');" title="Show/hide error">Show/hide error</a><div id="'.$category_list[$i].'_category_xref_error" class="debug">'.mysql_error().'</div>');
				}
				// NOTE: Set this category as parent of next in line
				$category_parent_id = $category_id;
			} // NOTE: end for
			$category[] = $category_id;
		}
		// NOTE: Return an array with the last category_ids which is where the product goes
		return $category;
	} // NOTE: End function csv_category
	
	/**
	  * Creates either an update or insert SQL query for a product price.
	  *
	  * @name price_query
	  * @author rolandd
	  * @param int $query_type
	  * @returns String update or insert query for a product
	  */
	function PriceQuery(&$csviregistry, $query_type) {
		$dbpq = $csviregistry->GetObject('database');
		$csvilog = $csviregistry->GetObject('logger');
		$q = "";
		/* Check if the product price is to be updated */
		/* Check if the price already exists if we are updating */
		if ($query_type == "update") {
			$q = "SELECT COUNT(product_price_id) AS total FROM #__vm_product_price ";
			$q .= "WHERE product_id='".$this->product_details->product_id."'";
			$dbpq->query($q);
			if ($dbpq->f("total") == 0) $query_type = "add";
		}
		
		/* Strip the price of all non-numeric characters */
		if (!is_numeric($this->product_details->product_price) ||
			strlen($this->product_details->product_price) == 0) $query_type = "delete";
		
		/* Get the default shopper group ID */
		switch ($query_type) {
			case "add":
				/* Add  product price for default shopper group */
				$q = "INSERT INTO #__vm_product_price ";
				$q .= "(product_price,product_currency,product_id,shopper_group_id,cdate) ";
				$q .= "VALUES ('";
				$q .= $this->product_details->product_price."', '";
				$q .= $this->product_details->product_currency."', '";
				$q .= $this->product_details->product_id."', '";
				$q .= $this->product_details->shopper_group_id. "', ";
				$q .= "UNIX_TIMESTAMP(NOW())) ";
				if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Adding price query: <a onclick="switchMenu(\''.$this->product_details->product_sku.'_add_price\');" title="Show/hide query">Show/hide query</a><div id="'.$this->product_details->product_sku.'_add_price" class="debug">'.htmlentities($q).'</div>');
				break;
				
			case "update":
				/* Update product price for default shopper group */
				$q = "UPDATE #__vm_product_price SET ";
				$q .= "product_price='" . $this->product_details->product_price."', ";
				$q .= "product_currency='" . $this->product_details->product_currency."', ";
				$q .= "mdate=UNIX_TIMESTAMP(NOW()) ";
				$q .= "WHERE product_id='".$this->product_details->product_id."' ";
				$q .= "AND shopper_group_id = '".$this->product_details->shopper_group_id."'";
				if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Updating price: <a onclick="switchMenu(\''.$this->product_details->product_sku.'_update_price\');" title="Show/hide query">Show/hide query</a><div id="'.$this->product_details->product_sku.'_update_price" class="debug">'.htmlentities($q).'</div>');
				break;
			case "delete":
				/* Delete the product price for default shopper group */
				$q = "DELETE FROM #__vm_product_price ";
				$q .= "WHERE product_id='".$this->product_details->product_id."'";
				if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Deleting price: <a onclick="switchMenu(\''.$this->product_details->product_sku.'_delete_price\');" title="Show/hide query">Show/hide query</a><div id="'.$this->product_details->product_sku.'_delete_price" class="debug">'.htmlentities($q).'</div>');
				break;
		}
		if ($dbpq->query($q)) return true;
		else {
			if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', '<span class="sqlerror">SQL Error</span>: <a onclick="switchMenu(\''.$this->product_details->product_sku.'_price_query_error\');" title="Show/hide error">Show/hide error</a><div id="'.$this->product_details->product_sku.'_price_query_error" class="debug">'.mysql_error().'</div>');
			return false;
		}
	}
	
	/** Tax Check **/
	function ProcessTax(&$csviregistry) {
		$dbcat = $csviregistry->GetObject('database');
		$dbmu = $csviregistry->GetObject('database');
		$csvilog = $csviregistry->GetObject('logger');
		$csv_fields = $csviregistry->GetArray('csv_fields');
		$product_data = $csviregistry->GetArray('product_data');
		
		// NOTE: The tax rate takes precedence over the ID
		// NOTE: See if the user wants to update the tax
		if (isset($csv_fields["product_tax_id"]) || isset($csv_fields["product_tax"])) {
			$product_relation = false;
			$product_tax_set = false;
			$product_tax_id_set = false;
			// NOTE: Check for default values and if both are set or only one
			if (isset($csv_fields["product_tax"])) {
				if ($product_data[$csv_fields["product_tax"]["order"]]) $product_tax_set = true;
			}
			if (isset($csv_fields["product_tax_id"])) {
				if ($product_data[$csv_fields["product_tax_id"]["order"]]) $product_tax_id_set = true;
			}
			
			$db_product_tax_id = $csviregistry->GetObject('database');
			$db_product_tax_id_count = 0;
			// NOTE: Check if the product tax exists, if not used, check the manufacturer ID
			if ($product_tax_set) {
				// NOTE: Check if the product tax exist searching by tax rate since it has preference
				$q_product_tax = "SELECT tax_rate, tax_rate_id FROM #__vm_tax_rate WHERE tax_rate='".($product_data[$csv_fields["product_tax"]["order"]]/100)."'";
				if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Checking if tax rate exists: <a onclick="switchMenu(\''.$this->product_details->product_sku.'_tax_rate\');" title="Show/hide query">Show/hide query</a><div id="'.$this->product_details->product_sku.'_tax_rate" class="debug">'.$q_product_tax.'</div>');
				$db_product_tax_id->query($q_product_tax);
				$db_product_tax_id_count = $db_product_tax_id->num_rows();
			}
			if ($db_product_tax_id_count == 0 && $product_tax_id_set) { 
				// NOTE: Check if the product tax exist searching by id
				$q_product_tax_id = "SELECT tax_rate, tax_rate_id FROM #__vm_tax_rate WHERE tax_rate_id='".$product_data[$csv_fields["product_tax_id"]["order"]]."'";
				if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Checking if tax rate id exists: <a onclick="switchMenu(\''.$this->product_details->product_sku.'_tax_rate_id\');" title="Show/hide query">Show/hide query</a><div id="'.$this->product_details->product_sku.'_taxe_rate_id" class="debug">'.$q_product_tax_id.'</div>');
				$db_product_tax_id->query($q_product_tax_id);
				$db_product_tax_id_count = $db_product_tax_id->num_rows();
			}
			$this->product_details->product_tax_id = $db_product_tax_id->f("tax_rate_id");
			// NOTE: Check if name or ID has been found
			if ($product_tax_set || $product_tax_id_set) {
				// NOTE: The CSV tax rate is not the same as the DB tax rate, update it
				if ($product_tax_set && $db_product_tax_id->f("tax_rate") != ($product_data[$csv_fields["product_tax"]["order"]]/100) && $db_product_tax_id_count > 0) {
					$q = "UPDATE #__vm_tax_rate SET tax_rate = '".($product_data[$csv_fields["product_tax"]["order"]]/100)."' ";
					$q .= "SET mdate = NOW() ";
					if ($product_tax_id_set) $q .= "WHERE tax_rate_id = '".$product_data[$csv_fields["product_tax_id"]["order"]]."'";
					else $q .= "WHERE tax_rate_id = '".$db_product_tax_id->f("tax_rate_id")."'";
					if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Updating tax rate: '.$q);
					if (!$dbmu->query($q)) if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', '<span class="sqlerror">SQL Error</span>: <a onclick="switchMenu(\''.$this->product_details->product_sku.'_tax_rate_error\');" title="Show/hide error">Show/hide error</a><div id="'.$this->product_details->product_sku.'_tax_rate_error" class="debug">'.mysql_error().'</div>');
				}
				// NOTE: The CSV product tax ID is not the same as the DB product tax ID, update it
				elseif ($product_tax_id_set && $db_product_tax_id->f("product_tax_id") != $product_data[$csv_fields["product_tax_id"]["order"]] && $db_product_tax_id_count > 0) {
					$q = "UPDATE #__vm_tax_rate SET tax_rate_id = '".$product_data[$csv_fields["product_tax_id"]["order"]]."', ";
					$q .= "mdate = NOW() ";
					$q .= "WHERE product_tax_id = '".$product_data[$csv_fields["product_tax_id"]["order"]]."'";
					if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Updating tax ID: '.$q); 
					if(!$dbmu->query($q)) if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', '<span class="sqlerror">SQL Error</span>: <a onclick="switchMenu(\''.$this->product_details->product_sku.'_tax_rate_id_error\');" title="Show/hide error">Show/hide error</a><div id="'.$this->product_details->product_sku.'_taxe_rate_id_error" class="debug">'.mysql_error().'</div>');
				}
				// NOTE: No tax or ID to update, set the product_tax_id to 0
				elseif ($db_product_tax_id_count == 0 && $product_tax_set) {
					if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'No tax value or ID has been found. Tax will not be updated.');
				}
			}
		}
	}
	
	/** 
	 * Manufacturer Check 
	 *
	 * @see CsviManufacturer
	 * @param object &$csviregistry Global register
	 */
	function ManufacturerImport(&$csviregistry) {
		require_once($csviregistry->GetVar('class_path')."csvi_class_manufacturer.php");
		$csvi_manufacturer =& new CsviManufacturer();
		$csvi_manufacturer->ProcessManufacturer($this->product_details->product_id, $csviregistry);
	}
	
	/**
	  * Show preview
	  *
	  * @name show_preview
	  * @author rolandd
	  * @param int $query_type
	  * @returns String update or insert query for a product
	  */
	function ShowPreview(&$csviregistry) {
		$csv_fields = $csviregistry->GetArray('csv_fields');
		$data_preview = $csviregistry->GetArray('data_preview');
		
		$preview = '<table style="empty-cells: show;"><tr>';
		foreach ($csv_fields as $name => $order) {
			$preview .= '<td style="border: 1px solid #000000; border-color: #FF0000; font-weight: bold;">'.$name.'</td>';
		}
		$preview .= '</tr><tr>';
		
		foreach ($data_preview as $id => $data) {
			foreach( $csv_fields as $fieldname ) {
				if (isset($data[$fieldname["order"]])) {
					$preview .= "<td style=\"border: 1px solid #000000; vertical-align: top;\">".substr(trim($data[$fieldname["order"]]),0,100)."</td>";
				}
				else $preview .= "<td style=\"border: 1px solid #000000; vertical-align: top;\"></td>";
			}
			$preview .= "</tr><tr>";
		}
		$preview .= "</tr></table>";
		$csviregistry->SetVar('preview', $preview);
		return true;
	}
}

?>
