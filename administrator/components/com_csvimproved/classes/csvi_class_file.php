<?php
/**
 * Main file processor class
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_class_file.php 324 2008-06-11 10:46:54Z Suami $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );
 
/** 
 * CsviFile class 
 * 
 * The CsviFile class handles all file operations regarding reading CSV and XLS
 * files. XLS reading is supported through the ExcelReader class.
 *
 * @package CSVImproved
 * @see Spreadsheet_Excel_Reader::read()
 */
class CsviFile {
	
	/** @var array Contains allowed extensions for uploaded files */
	var $suffixes = array();
	
	/** @var array Contains allowed mimetypes for uploaded files */
	var $mimetypes = array();
	
	/** @var string Contains the name of the uploaded file */
	var $filename = '';
	
	/** @var string Contains the extension of the uploaded file */
	var $extension = '';
	
	/** @var bool Contains the value whether or not the file uses
	 * an extension that is allowed.
	 *
	 * @see $suffixes
	 */
	var $valid_extension = false;
	
	/** @var bool Filepointer used when opening files */
	var $fp = false;
	
	/** @var integer Internal line pointer */
	var $linepointer = 2;
	
	/** @var array Contains the data that is read from file */
	var $data = null;
	
	/** @var bool Contains the value to convert the import UTF-8 <--> ISO-8895-1 */
	var $convertimport = true;
	
	/**
	 * Controls the reading of a file
	 *
	 * Determines on extension what file reading method to use.
	 *
	 * @todo Create valid checks for $fp
	 * @see $fp
	 * @param object &$csviregistry the main controller class
	 */
	function CsviFile(&$csviregistry) {
		$this->FileSettings($csviregistry);
		if ($this->ValidateFile($csviregistry)) {
			switch ($this->extension) {
				case 'csv':
					/* File gets closed in the ReadNextLine function */
					$this->OpenFile();
					break;
				case 'xls':
					$this->fp = true;
					/* XLS can only read the complete file */
					$this->ProcessFile($csviregistry);
					break;
				case 'xml':
					$this->fp = true;
					/* XML can only read the complete file */
					$this->ProcessFile($csviregistry);
					break;
			}
		}
		else {
			return false;
		}
	}
	
	/**
	 * Set the settings to work with
	 *
	 * @see $suffixes
	 * @see $mimetypes
	 * @see $data
	 * @param &$csviregistry the main controller class
	 */
	function FileSettings(&$csviregistry) {
		$this->suffixes = array('txt','csv','xls','xml');
		$this->mimetypes = array('text/html',
							'text/plain',
							'text/csv',
							'application/octet-stream',
							'application/x-octet-stream',
							'application/vnd.ms-excel',
							'application/excel',
							'application/ms-excel',
							'application/x-excel',
							'application/x-msexcel',
							'application/force-download',
							'text/comma-separated-values',
							'text/x-csv',
							'text/x-comma-separated-values');
		
		$this->data->sheets[0] = array();
		
		/* Get the convert import setting */
		$this->convertimport = $csviregistry->GetVar('convertimport');
	}
	
	/**
	 * Open the uploaded csv file
	 *
	 * Opens the uploaded file and sets the internal file pointer
	 *
	 * @todo Better error handling on read error
	 * @see $fp
	 * @see CloseFile()
	 */
	function OpenFile() {
		switch ($this->extension) {
			case 'csv':
				/* Open the csv file*/
				$this->fp = fopen($this->filename,"r");
				break;
		}
	}
	
	/**
	 * Open the uploaded xls file
	 *
	 * @todo Check for memory consumption xls
	 * @param object &$csviregistry the main controller class
	 */
	function ProcessFile(&$csviregistry) {
		switch($this->extension) {
			/* Only read the file if the extension is xls */
			case 'xls':
				require_once($csviregistry->GetVar('class_path').'excel/reader.php');
				$this->data = new Spreadsheet_Excel_Reader($csviregistry);
				/* Set output Encoding. */
				$this->data->setOutputEncoding('UTF-8');
				$this->data->read($this->filename);
				break;
			case 'xml':
				if (!$this->data = simplexml_load_file($this->filename)){
					echo 'Error reading the XML file';
				}
				$this->data->items = count($this->data->item);
				break;
		}
	}
	
	/**
	 * Valitdate the file
	 *
	 * Validate the file is of the supported type
	 * Types supported are csv, txt, xls
	 *
	 * @param object &$csviregistry the main controller class
	 * @return bool returns true if all fine else false
	 */
	function ValidateFile(&$csviregistry) {
		global $mosConfig_cachepath;
		$csvilog = $csviregistry->GetObject('logger');
		$templates = $csviregistry->GetObject('templates');
		
		switch ($csviregistry->GetVar('selectfile')) {
			/* No file given */
			case 0:
				$csvilog->AddStats('incorrect', 'No file provided.');
				return false;
				break;
			/* Uploaded file */
			case 1:
				/* Check if the file upload has an error */
				if ($_FILES['file']['error'] == 0) {
					/* Check if user is going to preview first */
					/* User doing a preview, we must save the file first */
					if ($templates->show_preview == true) {
						/* Start with a clean preview */
						$csviregistry->SetVar('preview_message', "");
						
						/* Empty PHP cache statistics */
						clearstatcache();
						
						/* Start processing uploaded file */
						if (is_uploaded_file($_FILES['file']['tmp_name'])) {
							if (is_dir($csviregistry->GetVar('cache_path')) && is_writable($csviregistry->GetVar('cache_path'))) {
								if (move_uploaded_file($_FILES['file']['tmp_name'], $csviregistry->GetVar('cache_path')."/".basename($_FILES['file']['name']))) {
									$csviregistry->SetVar('csv_file', $csviregistry->GetVar('cache_path')."/".basename($_FILES['file']['name']));
									$csviregistry->SetVar('upload_file_error', false);
								}
								else {
									$csviregistry->SetVar('preview_only', 'Cannot upload file to '.$csviregistry->GetVar('cache_path').'. Only preview is possible. To import the file, do a direct import.');
									$csviregistry->SetVar('upload_file_error', true);
								}
							}
							else {
								$csviregistry->SetVar('preview_only', 'Cannot upload file to '.$csviregistry->GetVar('cache_path').'. Only preview is possible. To import the file, do a direct import.');
								$csviregistry->SetVar('csv_file', $_FILES['file']['tmp_name']);
								$csviregistry->SetVar('upload_file_error', true);
							}
						}
						/* Error warning cannot save uploaded file */
						else {
							$csvilog->AddStats('incorrect', 'No uploaded file provided.');
							return false;
						}
					}
					else $csviregistry->SetVar('csv_file', $_FILES['file']['tmp_name']);
					$fileinfo = pathinfo($_FILES["file"]["name"]);
					if (isset($fileinfo["extension"])) {
						$this->extension = strtolower($fileinfo["extension"]);
						if ($this->extension == 'txt') $this->extension = 'csv';
					}
				}
				else {
					/* There was a problem uploading the file */
					switch($_FILES['file']['error']) {
						case '1':
							$csvilog->AddStats('incorrect', 'The uploaded file exceeds the maximum uploaded file size');
							break;
						case '2':
							$csvilog->AddStats('incorrect', 'The uploaded file exceeds the maximum uploaded file size');
							break;
						case '3':
							$csvilog->AddStats('incorrect', 'The uploaded file was only partially uploaded');
							break;
						case '4':
							$csvilog->AddStats('incorrect', 'No file was uploaded');
							break;
						case '6':
							$csvilog->AddStats('incorrect', 'Missing a temporary folder');
							break;
						case '7':
							$csvilog->AddStats('incorrect', 'Failed to write file to disk');
							break;
						case '8':
							$csvilog->AddStats('incorrect', 'File upload stopped by extension');
							break;
						default:
							$csvilog->AddStats('incorrect', 'There was a problem uploading the file');
							break;
					}
					return false;
				}
				break;
			/* Local file */
			case 2:
				$csviregistry->SetVar('csv_file', $csviregistry->GetVar('local_csv_file'));
				if (!file_exists($csviregistry->GetVar('csv_file'))) {
					$csviregistry->SetVar('preview', 'Specified local file doesn\'t exist.');
					return false;
				}
				else $csviregistry->SetVar('upload_file_error', false);
				$fileinfo = pathinfo($csviregistry->GetVar('csv_file'));
				if (isset($fileinfo["extension"])) {
					$this->extension = strtolower($fileinfo["extension"]);
					if ($this->extension == 'txt') $this->extension = 'csv';
				}
				break;
		}
		/* Set the filename */
		if (is_file($csviregistry->GetVar('csv_file'))) {
			$this->filename = $csviregistry->GetVar('csv_file');
		}
		else {
			$csvilog->AddMessage('', 'Specified local file doesn\'t exist.');
			return false;
		}
		
		if (in_array($this->extension, $this->suffixes)) $this->valid_extension = true;
		else {
			/* Test the mime type */
			if (!in_array($this->extension, $this->mimetypes) ) {
				$csvilog->AddStats('incorrect', 'Mime type not accepted. Type for file uploaded: '.$this->extension);
				return false;
			}
		}
		/* Debug message to know what filetype the user is uploading */
		if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Importing filetype: '.$this->extension);
		
		/* Store the users filename for display purposes */
		if (isset($_FILES['file'])) $csviregistry->SetVar('user_filename', $_FILES['file']['name']);
		
		/* All is fine */
		return true;
	}
	
	/**
	 * Read the first line of the file
	 *
	 * Return a multi-dimensional array with the layout:
	 *
	 * sheets[0]
	 * sheets[0][numRows] => 0
	 * sheets[0][numCols] => 0
	 * sheets[0][cells][1][1] => product_sku
	 * sheets[0][cells][2][1] => Wb1
	 * sheets[0][cells][3][1] => Wb2
	 *
	 * @param object &$csviregistry the main controller class
	 * @return array with the line of data read
	 */
	function ReadFirstLine(&$csviregistry) {
		switch ($this->extension) {
			case 'csv':
				$data = $this->ReadNextLine($csviregistry);
				break;
			case 'xls':
				/* Make sure we include the empty fields */
				for ($i=1;$i<=$this->data->sheets[0]['numCols'];$i++) {
					if (!isset($this->data->sheets[0]['cells'][1])) $this->data->sheets[0]['cells'][1][$i] = '';
				}
				$data = $this->data->sheets[0]['cells'][1];
				break;
			case 'xml':
				/* This should always return the column headers */
				$data = $this->ToArray($this->data->item[0], true);
				break;
			default:
				$data = false;
				break;
		}
		return $data;
	}
	
	/**
	 * Read the next line
	 *
	 * Reads the next line of the uploaded file
	 * In case of csv, it reads the next line from the file
	 * In case of xls, it returns the next entry in the array because the whole 
	 * file is already read
	 *
	 * CSV text is being converted to the host systems native encoding if
	 * convertimport is set to true. Check for iconv existence is done on
	 * import settings. Any characters not able to translate are being deleted.
	 *
	 * Encodings:
	 * - Joomla 1.0.x ISO-8895-1
	 * - Joomal 1.5.x UTF-8
	 *
	 * @see ShowConvert()
	 * @see www.csvimproved.com/wiki/doku.php/csvimproved:iconv
	 * @param object &$csviregistry the main controller class
	 * @return array with the line of data read
	 */
	function ReadNextLine(&$csviregistry) {
		$newdata = array();
		switch ($this->extension) {
			case 'csv':
				$templates = $csviregistry->GetObject('templates');
				if (!empty($templates->text_enclosure)) $csvdata = fgetcsv($this->fp, 4096, $templates->field_delimiter, $templates->text_enclosure);
				else $csvdata = fgetcsv($this->fp, 4096, $templates->field_delimiter);
				if ($csvdata) {
					/* Do BOM check */
					if ($csviregistry->currentline == 1) {
						/* Remove text delimiters as they are not recognized by fgetcsv */
						$csvdata[0] = str_replace($templates->text_enclosure, "", $this->CheckBom($csvdata[0]));
					}
					foreach ($csvdata as $key => $value) {
						if ($csviregistry->joomlaversion == '1.0' && $this->convertimport) {
							$newdata[$key+1] = iconv('UTF-8', 'ISO-8859-1//IGNORE', $value);
						}
						else if ($csviregistry->joomlaversion == '1.5' && $this->convertimport) {
							$newdata[$key+1] = iconv('ISO-8859-1', 'UTF-8//IGNORE', $value);
						}
						else $newdata[$key+1] = $value;
					}
					return $newdata;
				}
				else {
					$this->CloseFile($csviregistry);
					return false;
				}
				break;
			case 'xls':
				if ($this->data->sheets[0]['numRows'] >= $this->linepointer) {
					/* Make sure we include the empty fields */
					for ($i=1;$i<=$this->data->sheets[0]['numCols'];$i++) {
					   if (!isset($this->data->sheets[0]['cells'][$this->linepointer][$i])) $this->data->sheets[0]['cells'][$this->linepointer][$i] = '';
					}
					$newdata = $this->data->sheets[0]['cells'][$this->linepointer];
					$this->linepointer++;
					return $newdata;
				}
				else return false;
				break;
			case 'xml':
				if ($this->data->items >= $csviregistry->currentline) {
					$newdata = $this->ToArray($this->data->item[$csviregistry->currentline-1], false);
					return $newdata;
				}
				else return false;
				break;
		}
	}
	
	/**
	 * Close the file
	 *
	 * Closes the csv file pointer
	 *
	 * @see OpenFile()
	 */
	function CloseFile() {
		switch ($this->extension) {
			case 'csv':
				/* Close csv file */
				fclose ($this->fp);
				break;
		}
	}
	
	
	/**
	 * Checks if the uploaded file has a BOM
	 *
	 * If the uploaded file has a BOM, remove it since it only causes
	 * problems on import.
	 *
	 * @see ReadNextLine()
	 * @param &$data The string to check for a BOM
	 * @return string return the cleaned string
	 */
	function CheckBom(&$data) {
		/* Check the first three characters */
		if (strlen($data) > 3) {
			if (ord($data{0}) == 239 && ord($data{1}) == 187 && ord($data{2}) == 191) {
				return substr($data, 3, strlen($data));
			}
			else return $data;
		}
		else return $data;
	}
	
	/**
	 * Convert an XML node into an array
	 *
	 * @param &$data object XML node to convert
	 * @return array XML node as an array
	 */
	function ToArray(&$data, $firstline) {
		$newdata = array();
		$counter = 1;
		foreach($data as $name => $value) {
			if ($firstline) $newdata[$counter] = $name;
			else $newdata[$counter] = $value;
			$counter++;
		}
		return $newdata;
	}
}
?>
