<?php
/**
 * Product type class
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Import
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * Processor for product types
 *
 * @package CSVImproved
 * @subpackage Import
 */
class product_type {
	var $classname = "product_type";
	var $product_type_id = false;
	var $product_type_name = "";
	var $product_type_description = false;
	var $product_type_publish = false;
	var $product_type_browsepage = false;
	var $product_type_flypage = false;
	var $product_type_list_order = false;
	var $product_details = null;
	
	function product_type(&$csviregistry) {
		// Get the product details
		$this->product_details = $csviregistry->GetObject('product_details');
		
		// Handle all fields in this order:
		$this->get_product_type_name($csviregistry);
		$this->get_product_type_id($csviregistry);
		$this->get_product_type_list_order($csviregistry);
		$this->get_product_type_description($csviregistry);
		$this->get_product_type_publish($csviregistry);
		$this->get_product_type_browsepage($csviregistry);
		$this->get_product_type_flypage($csviregistry);
	}
	
	/**
	 * Checks the field for existing value, if not set the default value if allowed
	 */
	function ValidateCSVInput(&$csviregistry, $fieldname) {
		$csv_fields = $csviregistry->GetArray('csv_fields');
		$product_data = $csviregistry->GetArray('product_data');
		$skip_default_value = $csviregistry->GetVar('skip_default_value');
		/* Check if the columns match */
		if (count($csv_fields) != count($product_data)) return false;
		else {
			if (isset($csv_fields[$fieldname])) {
				/* Check if the field has a value */
				if (strlen($product_data[$csv_fields[$fieldname]["order"]]) > 0) {
					$this->$fieldname = trim($product_data[$csv_fields[$fieldname]["order"]]);
				}
				/* Field has no value, check if we can use default value*/
				else if (!$skip_default_value) {
					$this->$fieldname = $csv_fields[$fieldname]["default_value"];
				}
			}
		}
		return true;
	}
	
	function get_product_type_id(&$csviregistry) {
		$db = $csviregistry->GetObject('database');
		$csvilog = $csviregistry->GetObject('logger');

		$q = "SELECT product_type_id FROM #__vm_product_type ";
		$q .= "WHERE product_type_name='".$this->product_type_name."' LIMIT 1";
		$randid = rand();
		if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Looking for product type id: <a onclick="switchMenu(\''.$this->product_type_name.$randid.'_get_product_type_id\');" title="Show/hide query">Show/hide query</a><div id="'.$this->product_type_name.$randid.'_get_product_type_id" class="debug">'.htmlentities($q).'</div>');
		if (!$db->query($q)) if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', '<span class="sqlerror">SQL Error</span>: <a onclick="switchMenu(\''.$this->product_type_name.$randid.'_get_product_type_id_error\');" title="Show/hide error">Show/hide error</a><div id="'.$this->product_type_name.$randid.'_get_product_type_id_error" class="debug">'.mysql_error().'</div>');
		$db->next_record();
		
		if (count($db->record) > 0) $this->product_type_id = $db->f("product_type_id");
		else $this->product_type_id = false;
	}
	
	function get_product_type_name(&$csviregistry) {
		$this->ValidateCSVInput($csviregistry, "product_type_name");
	}
	
	function get_product_type_description(&$csviregistry) {
		$this->ValidateCSVInput($csviregistry, "product_type_description");
	}
	
	function get_product_type_publish(&$csviregistry) {
		$this->ValidateCSVInput($csviregistry, "product_type_publish");
		if (empty($this->product_type_publish)) $this->product_type_publish = "Y";
	}
	
	function get_product_type_browsepage(&$csviregistry) {
		$this->ValidateCSVInput($csviregistry, "product_type_browsepage");
	}
	
	function get_product_type_flypage(&$csviregistry) {
		$this->ValidateCSVInput($csviregistry, "product_type_flypage");
	}
	
	function get_product_type_list_order(&$csviregistry) {
		$db = $csviregistry->GetObject('database');

		$q = "SELECT product_type_list_order FROM #__vm_product_type ";
		$q .= "WHERE product_type_id=".$this->product_type_id;
		$db->query($q);
		$this->product_type_list_order = $db->f("product_type_list_order");
	}
	
	/**************************************************************************
	** name: validate_add()
	** created by:
	** description:
	** parameters:
	** returns:
	***************************************************************************/
	function validate_add(&$csviregistry) {
		$csvilog = $csviregistry->GetObject('logger');
		if (!$this->product_type_name) {
			if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', "ERROR:  You must enter a name for the Product Type.");
			return False;
		}
		else {
			return True;
		}
	}

	/**************************************************************************
	** name: validate_update()
	** created by:
	** description:
	** parameters:
	** returns:
	***************************************************************************/
	function validate_update(&$csviregistry) {
		$csvilog = $csviregistry->GetObject('logger');
		if (!$this->product_type_name) {
			if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', "ERROR:  You must enter a name for the Product Type.");
			return false;
		}
		else return true;
	}


	/**************************************************************************
	** name: add()
	** created by: Zdenek Dvorak
	** description: creates a new Product Type record
	** parameters:
	** returns:
	***************************************************************************/
	function add(&$csviregistry) {
		$db = $csviregistry->GetObject('database');
		$csvilog = $csviregistry->GetObject('logger');
		
		if ($this->validate_add($csviregistry)) {
			// find product_type_id
			$q  = "SELECT MAX(product_type_id) AS product_type_id FROM #__vm_product_type";
			$db->query( $q );
			$db->next_record();
			$this->product_type_id = intval($db->f("product_type_id")) + 1;

			// Let's find out the last Product Type
			$q = "SELECT MAX(product_type_list_order) AS list_order FROM #__vm_product_type";
			$db->query( $q );
			$db->next_record();
			$list_order = intval($db->f("list_order"))+1;

			$q = "INSERT INTO #__vm_product_type (product_type_id, product_type_name, product_type_description, ";
			$q .= "product_type_publish, product_type_browsepage, product_type_flypage, product_type_list_order) ";
			$q .= "VALUES ('";
			$q .= $this->product_type_id . "','";
			$q .= $this->product_type_name . "','";
			$q .= $this->product_type_description . "','";
			// TODO: Move to correct section
			if ($this->product_type_publish != "Y") {
				$this->product_type_publish = "N";
			}
			$q .= $this->product_type_publish . "','";
			$q .= $this->product_type_browsepage . "','";
			$q .= $this->product_type_flypage . "','";
			$q .= $list_order . "')";
			if ($db->query($q)) if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Add product type: <a onclick="switchMenu(\''.$this->product_type_id.'_product_type\');" title="Show/hide query">Show/hide query</a><div id="'.$this->product_type_id.'_product_type" style="display: none; border: 1px solid #000000; padding: 5px;">'.htmlentities($q).'</div>');
			else if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', '<span class="sqlerror">SQL Error</span>: <a onclick="switchMenu(\''.$this->product_type_id.'_product_type_error\');" title="Show/hide error">Show/hide error</a><div id="'.$this->product_type_id.'_product_type_error" class="debug">'.mysql_error().'</div>');

			// Make new table product_type_<id>
			$q = "CREATE TABLE `#__vm_product_type_";
			$q .= $this->product_type_id . "` (";
			$q .= "`product_id` int(11) NOT NULL,";
			$q .= "PRIMARY KEY (`product_id`)";
			$q .= ")";
			$db->query($q);

			return $this->product_type_id;
		}
		else return false;

	}

	/**************************************************************************
	** name: update()
	** created by: Zdenek Dvorak
	** description: updates Product Type information
	** parameters:
	** returns:
	***************************************************************************/
	function update(&$csviregistry) {
		$db = $csviregistry->GetObject('database');
		$csvilog = $csviregistry->GetObject('logger');
		$templates = $csviregistry->GetObject('templates');

		if ($this->validate_update($csviregistry)) {
			$q = "UPDATE #__vm_product_type SET ";
			$q .= "product_type_id='" . $this->product_type_id;
			$q .= "',product_type_name='" . $this->product_type_name;
			$q .= "',product_type_description='" . $this->product_type_description;
			$q .= "',product_type_publish='" . $this->product_type_publish;
			$q .= "',product_type_browsepage='" . $this->product_type_browsepage;
			$q .= "',product_type_flypage='" . $this->product_type_flypage;
			$q .= "',product_type_list_order='" . $this->product_type_list_order."' ";
			$q .= " WHERE product_type_id='" . $this->product_type_id . "' ";
			if ($db->query($q)) if ($templates->collect_debug_info) $csvilog->AddMessage('debug', 'Update product type: <a onclick="switchMenu(\''.$this->product_type_id.'_product_type\');" title="Show/hide query">Show/hide query</a><div id="'.$this->product_type_id.'_product_type" style="display: none; border: 1px solid #000000; padding: 5px;">'.htmlentities($q).'</div>');
			else if ($templates->collect_debug_info) $csvilog->AddMessage('debug', '<span class="sqlerror">SQL Error</span>: <a onclick="switchMenu(\''.$this->product_type_id.'_product_type_error\');" title="Show/hide error">Show/hide error</a><div id="'.$this->product_type_id.'_product_type_error" class="debug">'.mysql_error().'</div>');
			return true;
		}
		else return false;
	}
	
  /**************************************************************************
  ** name: validate_add()
  ** created by:
  ** description:
  ** parameters:
  ** returns:
  ***************************************************************************/
  function validate_add_name(&$csviregistry) {
	$db = $csviregistry->GetObject('database');
	$csvilog = $csviregistry->GetObject('logger');
	
	if (!isset($this->product_type_id)) {
		if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', "ERROR: No product type ID has been found");
		return False;
	}
	if (!isset($this->product_details->product_id)) {
		if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', "ERROR: No product ID has been found");
		return false;
	}
	$q  = "SELECT COUNT(*) AS count FROM #__vm_product_product_type_xref ";
	$q .= "WHERE product_id='".$this->product_details->product_id."' AND product_type_id='".$this->product_type_id."'";
	if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Check if cross reference exists: <a onclick="switchMenu(\''.$this->product_details->product_sku.'_product_type_name\');" title="Show/hide query">Show/hide query</a><div id="'.$this->product_details->product_sku.'_product_type_name" style="display: none; border: 1px solid #000000; padding: 5px;">'.htmlentities($q).'</div>');
	if (!$db->query($q)) if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', '<span class="sqlerror">SQL Error</span>: <a onclick="switchMenu(\''.$this->product_details->product_sku.'_product_type_name_error\');" title="Show/hide error">Show/hide error</a><div id="'.$this->product_details->product_sku.'_product_type_name_error" class="debug">'.mysql_error().'</div>');
	if ($db->f("count") != 0) {
		if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', "ERROR:  This Product is already in this Product Type.");
		return false;
	}
	else return true;
  }
    
  /**************************************************************************
  ** name: validate_delete()
  ** created by: Zdenek Dvorak
  ** description:
  ** parameters:
  ** returns:
  ***************************************************************************/
	function validate_delete_name(&$csviregistry) {
		$csvilog = $csviregistry->GetObject('logger');
		
		if (!isset($this->product_type_id)) {
			if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', "ERROR:  Please select a Product Type to delete a Product from this Product Type.");
			return false;
		}
		if (!isset($this->product_details->product_id)) {
			if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', "ERROR:  Please select a Product to delete from Product Type.");
			return false;
		}
		return true;
	}

  /**************************************************************************
  ** name: add()
  ** created by: Zdenek Dvorak
  ** description: add a Product into a Product Type
  ** parameters:
  ** returns:
  ***************************************************************************/
  function add_name(&$csviregistry) {
    $db = $csviregistry->GetObject('database');
    $csvilog = $csviregistry->GetObject('logger');
    $templates = $csviregistry->GetObject('templates');
	
    if ($this->validate_add_name($csviregistry)) {
      $q  = "INSERT INTO #__vm_product_product_type_xref (product_id, product_type_id) ";
      $q .= "VALUES ('".$this->product_details->product_id."','".$this->product_type_id."')";
	 if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Adding product type name: <a onclick="switchMenu(\''.$this->product_type_id.'_add_name\');" title="Show/hide query">Show/hide query</a><div id="'.$this->product_type_id.'_add_name" class="debug">'.htmlentities($q).'</div>');
      if (!$db->query($q)) if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', '<span class="sqlerror">SQL Error</span>: <a onclick="switchMenu(\''.$this->product_type_id.'_add_name_error\');" title="Show/hide error">Show/hide error</a><div id="'.$this->product_type_id.'_add_name_error" class="debug">'.mysql_error().'</div>');
      return true;
    }
    else return false;
  }

	/**
	* Controller for Deleting Records.
	*/
	function delete_name(&$csviregistry) {
		
		if (!$this->validate_delete_name($csviregistry)) {
		  return false;
		}
		
		return $this->delete_record_name($csviregistry, $this->product_type_id);
	}
	/**
	* Deletes one Record.
	*/
	function delete_record_name(&$csviregistry, $record_id) {
		$db = $csviregistry->GetObject('database');
	
		$q  = "DELETE FROM #__vm_product_product_type_xref WHERE product_type_id='$record_id' ";
		$q .= "AND product_id='".$this->product_details->product_id."'";
		$db->query($q);
	
		$q  = "DELETE FROM #__vm_product_type_".$record_id." WHERE product_id='".$this->product_details->product_id."'";
		$db->query($q);
		
		return true;
  }
}
?>
