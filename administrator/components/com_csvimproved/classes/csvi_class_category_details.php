<?php
/**
 * Category details
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Import
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_class_category_details.php 286 2008-06-01 02:51:30Z Suami $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * Processor for category details
 *
 * @package CSVImproved
 * @subpackage Import
 *
 * @todo change classname
 * @todo add thumb resizing
 * @todo use path from template
 */
class category_details {
	
	var $category_id = false;
	var $category_path  = false;
	var $category_name  = '';
	var $category_vendor_id = 1;
	var $category_description  = false;
	var $category_thumb_image  = false;
	var $category_full_image  = false;
	var $category_publish  = 'Y';
	var $category_browsepage  = false;
	var $category_products_per_row  = 1;
	var $category_flypage  = false;
	var $category_list_order = '';
	
	/**
	 * Process the category details
	 *
	 * @param &$csviregistry object Global register
	 * @return bool true|false
	 */
	
	function category_details(&$csviregistry) {
		$csv_fields = $csviregistry->GetArray('csv_fields');
		/* Get the statistics */
		$csvilog = $csviregistry->GetObject('logger');
		$templates = $csviregistry->GetObject('templates');
		if (!$templates->show_preview) {
			/* Validate CSV Input */
			if (count($csv_fields) != count($csviregistry->GetArray('product_data'))) {
				$csvilog->AddStats('incorrect', "Line ".$csviregistry->currentline.": <strong>Incorrect column count</strong><br />Configration: ".count($csv_fields)." fields<br />File: ".count($csviregistry->GetArray('product_data'))." fields");
				return false;
			}
			else if (!isset($csv_fields['category_path'])) {
				$csvilog->AddStats('incorrect', "Line ".$csviregistry->currentline.": <strong>No category path set</strong>");
				return false;
			}
			else {
				/* First get the category ID */
				if (!$this->GetCategoryId($csviregistry)) {
					$csvilog->AddStats('incorrect', 'Line '.$csviregistry->currentline.': Could not find a category ID');
					return false;
				}
				
				/* Load the category details */
				$this->LoadCategoryDetails($csviregistry);
				
				foreach ($csv_fields as $name => $details) {
					if ($this->ValidateCsvInput($csviregistry, $name)) {
						/* Check if the field needs extra treatment */
						switch ($name) {
							case 'category_path':
							default:
								break;
						}
					}
					else {
						$csvilog->AddStats('incorrect', 'Line '.$csviregistry->currentline.': Column mismatch for field '.$name);
					}
				}
			}
		}
		else return true;
	}
	
	/**
	 * Checks the field for existing value, if not set the default value if allowed
	 */
	function ValidateCsvInput(&$csviregistry, $fieldname) {
		$csv_fields = $csviregistry->GetArray('csv_fields');
		$product_data = $csviregistry->GetArray('product_data');
		$skip_default_value = $csviregistry->GetVar('skip_default_value');
		
		/* Check if the columns match */
		if (count($csv_fields) != count($product_data)) return false;
		else {
			if (isset($csv_fields[$fieldname])) {
				/* Check if the field has a value */
				if (strlen($product_data[$csv_fields[$fieldname]["order"]]) > 0) {
					$this->$fieldname = trim($product_data[$csv_fields[$fieldname]["order"]]);
				}
				/* Field has no value, check if we can use default value*/
				else if (!$skip_default_value) {
					$this->$fieldname = $csv_fields[$fieldname]["default_value"];
				}
			}
		}
		return true;
	} // end function ValidateCsvInput
	
	/**
	 * Gets the ID belonging to the category path
	 *
  	 * @param &$csviregistry Global register
  	 **/
	function GetCategoryId(&$csviregistry) {
		$csv_fields = $csviregistry->GetArray('csv_fields');
		$product_data = $csviregistry->GetArray('product_data');
		$db = $csviregistry->GetObject('database');
		
		/* Cut the category path into pieces, if there is no category path, go back false */
		if (!isset($csv_fields['category_path'])) return false;
		else $catpieces = explode("/", $product_data[$csv_fields["category_path"]["order"]]);
		
		/* Check for any missing categories, otherwise create them */
		$this->csv_category($csviregistry);
		
		/* Check if the category order is correct */
		$category_parent_id = 0;
		for($i = 0; $i < count($catpieces); $i++) {
			/* See if this category exists with it's parent in xref */
			$q = "SELECT #__vm_category.category_id FROM #__vm_category,#__vm_category_xref ";
			$q .= "WHERE #__vm_category.category_name='" . $db->getEscaped($catpieces[$i]) . "' ";
			$q .= "AND #__vm_category_xref.category_child_id=#__vm_category.category_id ";
			$q .= "AND #__vm_category_xref.category_parent_id='$category_parent_id'";
			$db->query($q);
			/* If it does not exist, create it */
			if ($db->next_record()) { // Category exists
				$category_id = $db->f("category_id");
				/* Set this category as parent of next in line */
				$category_parent_id = $category_id;
			}
			else $category_id = 0;
		} // @internal end for

		$this->category_id = $category_id;
		$this->category_name = $catpieces[(count($catpieces)-1)];
		return true;
	} // end function GetCategoryId
	
	
	/**
	 * Load the category details
	 *
	 * @param &$csviregistry Global register
	 * @return bool true|false true on category details found|false when no category details found
	 */
	function LoadCategoryDetails(&$csviregistry) {
		$db = $csviregistry->GetObject('database');
		$q = "SELECT * FROM #__vm_category WHERE category_id = ".$this->category_id;
		if ($db->query($q)) {
			$this->category_description = $db->f('category_name');
			$this->category_thumb_image  = $db->f('category_thumb_image');
			$this->category_full_image  = $db->f('category_full_image');
			$this->category_publish  = $db->f('category_publish');
			$this->category_browsepage  = $db->f('category_browsepage');
			$this->category_products_per_row  = $db->f('products_per_row');
			$this->category_flypage  = $db->f('category_flypage');
			$this->category_list_order = $db->f('list_order');
			return true;
		}
		else return false;
	} // end function LoadCategoryDetails
	
	function UpdateCategory(&$csviregistry) {
		$db = $csviregistry->GetObject('database');
		$csvilog = $csviregistry->GetObject('logger');
		
		$q = "UPDATE #__vm_category SET
			category_name = '".$db->getEscaped($this->category_name)."', 
			category_description = '".$db->getEscaped($this->category_description)."', 
			category_thumb_image = '".$this->category_thumb_image."', 
			category_full_image = '".$this->category_full_image."', 
			category_publish = '".$this->category_publish."', 
			mdate = UNIX_TIMESTAMP(NOW()),
			category_browsepage = '".$this->category_browsepage."', 
			products_per_row = ".$this->category_products_per_row.", 
			category_flypage = '".$this->category_flypage."', 
			list_order = ".$this->category_list_order."
			WHERE category_id = ".$this->category_id;
		if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Update category '.$this->category_name.': <a onclick="switchMenu(\''.$this->category_id.'_update_category\');" title="Show/hide query">Show/hide query</a><div id="'.$this->category_id.'_update_category" style="display: none; border: 1px solid #000000; padding: 5px;">'.htmlentities($q).'</div>');
		if ($db->query($q)) {
			$csvilog->AddStats('updated', 'Line '.$csviregistry->currentline.': Category '.$this->category_name);
			return true;
		}
		else {
			$csvilog->AddStats('incorrect', 'Line '.$csviregistry->currentline.': Category '.$this->category_name);
			return false;
		}
	} // end function UpdateCategory
	
	function csv_category(&$csviregistry) {
		$db = $csviregistry->GetObject('database');
		$csvilog = $csviregistry->GetObject('logger');
		$csv_fields = $csviregistry->GetArray('csv_fields');
		$product_data = $csviregistry->GetArray('product_data');
		
		// NOTE: Explode slash delimited category tree into array
		$category_list = explode("/", $product_data[$csv_fields["category_path"]["order"]]);
		$category_count = count($category_list);
		
		
		$category_parent_id = '0';

		// NOTE: For each category in array
		for($i = 0; $i < $category_count; $i++) {
			// NOTE: See if this category exists with it's parent in xref
			$q = "SELECT #__vm_category.category_id FROM #__vm_category,#__vm_category_xref ";
			$q .= "WHERE #__vm_category.category_name='" . $db->getEscaped($category_list[$i]) . "' ";
			$q .= "AND #__vm_category_xref.category_child_id=#__vm_category.category_id ";
			$q .= "AND #__vm_category_xref.category_parent_id='$category_parent_id'";
			$db->query($q);
			// NOTE: If it does not exist, create it
			if ($db->next_record()) { // NOTE: Category exists
				$category_id = $db->f("category_id");
			}
			else { // NOTE: Category does not exist - create it

				$timestamp = time();

				// NOTE: Let's find out the last category in
				// NOTE: the level of the new category
				$q = "SELECT MAX(list_order) AS list_order FROM #__vm_category_xref,#__vm_category ";
				$q .= "WHERE category_parent_id='".$category_parent_id."' ";
				$q .= "AND category_child_id=category_id ";
				$db->query( $q );
				$db->next_record();

				$list_order = intval($db->f("list_order"))+1;

				// NOTE: Add category
				$q = "INSERT INTO #__vm_category ";
				$q .= "(vendor_id,category_name, category_publish,cdate,mdate,list_order) ";
				$q .= "VALUES ('1', '";
				$q .= $db->getEscaped($category_list[$i]) . "', '";
				$q .= "Y', '";
				$q .= $timestamp . "', '";
				$q .= $timestamp . "', '$list_order')";
				if (!$db->query($q)) if ($this->debug) $csvilog->AddMessage('debug', '<span class="sqlerror">SQL Error</span>: <a onclick="switchMenu(\''.$db->getEscaped($category_list[$i]).'_category_error\');" title="Show/hide error">Show/hide error</a><div id="'.$category_list[$i].'_category_error" class="debug">'.mysql_error().'</div>');

				$category_id = $db->last_insert_id();

				// NOTE: Create xref with parent
				$q = "INSERT INTO #__vm_category_xref ";
				$q .= "(category_parent_id, category_child_id) ";
				$q .= "VALUES ('";
				$q .= $category_parent_id . "', '";
				$q .= $category_id . "')";
				if (!$db->query($q)) if ($this->debug) $csvilog->AddMessage('debug', '<span class="sqlerror">SQL Error</span>: <a onclick="switchMenu(\''.$db->getEscaped($category_list[$i]).'_category_xref_error\');" title="Show/hide error">Show/hide error</a><div id="'.$category_list[$i].'_category_xref_error" class="debug">'.mysql_error().'</div>');
			}
			// NOTE: Set this category as parent of next in line
			$category_parent_id = $category_id;
		} // NOTE: end for
		$this->category_id = $category_id;
	} // NOTE: End function csv_category
}
?>
