<?php
/**
 * Script to handle RPC calls
 *
 * CSV Improved
 * Copyright (C) 2006 - 2008 Roland Dalmulder
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_class_rpc.php 386 2008-07-26 01:45:03Z Suami $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 *
 * @package CSVImproved
 */
class CsviRpc {
	
	/**
	 * Initialise settings
	 *
	 * @todo Add language entry
	 */
	function CsviRpc(&$csviregistry) {
		switch ($csviregistry->GetVar('rpc_call')) {
			case 'gettemplatedetails':
				$this->GetTemplateDetails($csviregistry);
				break;
			case 'exporttemplatelist':
				$this->ExportTemplateList($csviregistry);
				break;
			case 'templateconfigurator':
				$this->TemplateConfigurator($csviregistry);
				break;
			case 'addtemplatefield':
				$this->AddTemplateField($csviregistry);
				break;
			case 'deletetemplatefield':
				$this->DeleteTemplateField($csviregistry);
				break;
			default:
				echo 'No RPC call defined';
				break;
		}
	}
	
	/**
	 * Get template details
	 *
	 * @todo Add language string
	 * @see CsviExport
	 * @see CsviRpc::ExportTemplateList()
	 * @param &$csviregistry array Global register
	 */
	function GetTemplateDetails(&$csviregistry) {
		$templates = $csviregistry->GetObject('templates');
		$csvilang = $csviregistry->GetObject('language');
		$CsviSupportedFields = $csviregistry->GetObject('supportedfields');
		
		/* First check to see if we need to display a list of templates for export  */
		if ($templates->template_type == 'TemplateExport' || $templates->template_type == 'TemplateFieldsExport') {
			$this->ExportTemplateList($csviregistry);
		}
		/* Get the fields list */
		$types = array();
		if ($csviregistry->GetVar('csvi_type') == 'export') $CsviSupportedFields->FieldsExport();
		else if ($csviregistry->GetVar('csvi_type') == 'import') $CsviSupportedFields->FieldsImport();
		$types = $CsviSupportedFields->fields;
		
		/* Line color */
		$line = 0;
		
		if (isset($types[$templates->template_type])) {
			echo '<div id="templatesettings">';
			echo '<table class="adminform">';
			echo '<th colspan="2">'.$csvilang->TEMPLATE_SETTINGS.'</th>';
			echo '<tr class="row'.$line.'"><td width="25%">'.$csvilang->NAME.'</td><td>'.$templates->template_name.'</td></tr>';
			echo '<tr class="row'.($line+1).'"><td>'.$csvilang->TYPE.'</td><td>'.$csvilang->$types[$templates->template_type].'</td></tr>';
			echo '<tr class="row'.$line.'"><td>'.$csvilang->ENCLOSURE.'</td><td>'.$templates->field_delimiter.'</td></tr>';
			echo '<tr class="row'.($line+1).'"><td>'.$csvilang->DELIMITER.'</td><td>'.$templates->text_enclosure.'</td></tr>';
			if ($csviregistry->GetVar('csvi_type') == 'export') {
				echo '<tr class="row'.$line.'"><td>'.$csvilang->TEMPLATE_EXPORT_FILENAME.'</td><td>';
				if (empty($templates->export_filename)) echo $csvilang->AUTOMATIC_EXPORT_FILENAME;
				else echo $templates->export_filename;
				echo '</td></tr>';
				echo '<tr class="row'.($line+1).'"><td>'.$csvilang->INCLUDE_COLUMN_HEADERS.'</td><td>';
				if ($templates->include_column_headers) echo $csvilang->YES;
				else echo $csvilang->NO;
				echo '</td></tr>';
			}
			if ($csviregistry->GetVar('csvi_type') == 'import') {
				echo '<tr class="row'.$line.'"><td>'.$csvilang->IMPORT_CONFIG_CSV_FILE.'</td><td>';
				if ($templates->use_column_headers) echo $csvilang->YES;
				else echo $csvilang->NO;
				echo '</td></tr>';
				echo '<tr class="row'.($line+1).'"><td>'.$csvilang->SHOW_PREVIEW.'</td><td>';
				if ($templates->show_preview) echo $csvilang->YES;
				else echo $csvilang->NO;
				echo '</td></tr>';
			}
			echo '<tr class="row0"><td>'.$csvilang->NUMBER_OF_FIELDS.'</td><td>'.$templates->numberoffields.'</td></tr>';
			if ($csviregistry->GetVar('csvi_type') == 'export') {
				echo '<tr class="row'.($line+1).'"><td>'.$csvilang->TEMPLATE_EXPORT_PRODUCT_PUBLISH.'</td><td>';
				switch ($templates->product_publish) {
					case 'Y':
						echo $csvilang->TEMPLATE_EXPORT_PRODUCT_PUBLISHED;
						break;
					case 'N':
						echo $csvilang->TEMPLATE_EXPORT_PRODUCT_UNPUBLISHED;
						break;
					default:
						echo $csvilang->TEMPLATE_EXPORT_PRODUCT_PUBLISHED_BOTH;
						break;
				}
				echo '</td></tr>';
				echo '<tr class="row'.$line.'"><td>'.$csvilang->TEMPLATE_SHOPPER_GROUP_NAME.'</td><td>'.$templates->shopper_group_name.'</td></tr>';
				echo '<tr class="row'.($line+1).'"><td>'.$csvilang->EXPORT_TYPE.'</td><td>'.$templates->export_type.'</td></tr>';
				echo '<tr class="row'.$line.'"><td>'.$csvilang->EXPORT_SITE.'</td><td>'.$templates->export_site.'</td></tr>';
			}
			echo '</table>';
			?>
			</div>
			<div id="systemlimits">
				<table class="adminform">
				<th colspan="2"><?php echo $csvilang->SYSTEM_LIMITS; ?></th>
				<tr class="row<?php echo $line; ?>"><td><?php echo $csvilang->TEMPLATE_MAX_EXECUTION_TIME; ?></td><td><?php echo $templates->max_execution_time.' '.$csvilang->IMPORT_SECONDS;?></td></tr>
				<tr class="row<?php echo ($line+1); ?>"><td><?php echo $csvilang->TEMPLATE_MAX_INPUT_TIME; ?></td><td><?php echo $templates->max_input_time.' '.$csvilang->IMPORT_SECONDS;?></td></tr>
				<tr class="row<?php echo $line; ?>"><td><?php echo $csvilang->TEMPLATE_MEMORY_LIMIT; ?></td><td><?php echo $templates->memory_limit.'M';?></td></tr>
				<tr class="row<?php echo ($line+1); ?>"><td><?php echo $csvilang->TEMPLATE_POST_MAX_SIZE; ?></td><td><?php echo $templates->post_max_size.'M';?></td></tr>
				<tr class="row<?php echo $line; ?>"><td><?php echo $csvilang->TEMPLATE_UPLOAD_MAX_FILESIZE; ?></td><td><?php echo $templates->upload_max_filesize.'M';?></td></tr>
				</table>
			</div>
			<?php
		}
		else echo 'Template type translation not found: '.$templates->template_type;
	} // End function GetTemplateDetails 
	
	/**
	 * Prints a list of templates that can be exported
	 *
	 * A list of templates that can be exported for both template exports and
	 * template fields export
	 *
	 * @param &$csviregistry array Global register
	 */
	function ExportTemplateList(&$csviregistry) {
		$CsviLang = $csviregistry->GetObject('language');
		$templates = $csviregistry->GetObject('templates');
		echo '<table class="adminform">';
		echo '<th colspan="2">'.$CsviLang->EXPORT_TEMPLATE_FIELDS.'</th>';
		echo '<tr><td style="width: 10%; vertical-align: top;">'.$CsviLang->CHOOSE_TEMPLATE.'</td>';
		echo '<td>';
		foreach ($templates->templatelist as $id => $template) {
			if ($template['type'] != 'TemplateExport' && $template['type'] != 'TemplateFieldsExport') {
				echo "<input type=\"checkbox\" name=\"exporttemplatelist[]\" value=\"".$id."\">".$template['name']."</input><br />\n";
			}
		}
		echo '</td></tr>';
		echo '</table>';
	}
	
	/**
	 * Prints out a form to configure new templates
	 *
	 * The template configurator will be used to make it easier to create new
	 * templates by showing only the possible options
	 *
	 * @param &$csviregistry array Global register
	 */
	function TemplateConfigurator(&$csviregistry) {
		$csvisupportedfields = $csviregistry->GetObject('supportedfields');
		$csvilang = $csviregistry->GetObject('language');
		$templates = $csviregistry->GetObject('templates');
		
		/* Assign the temporary values to the template */
		if ($csviregistry->IssetVar('template_name')) $templates->template_name = $csviregistry->GetVar('template_name');
		if ($csviregistry->IssetVar('template_type')) $templates->template_type = $csviregistry->GetVar('template_type');
		if ($csviregistry->IssetVar('skip_first_line')) $templates->skip_first_line = $csviregistry->GetVar('skip_first_line');
		if ($csviregistry->IssetVar('use_column_headers')) $templates->use_column_headers = $csviregistry->GetVar('use_column_headers');
		if ($csviregistry->IssetVar('collect_debug_info')) $templates->collect_debug_info = $csviregistry->GetVar('collect_debug_info');
		if ($csviregistry->IssetVar('overwrite_existing_data')) $templates->overwrite_existing_data = $csviregistry->GetVar('overwrite_existing_data');
		if ($csviregistry->IssetVar('skip_default_value')) $templates->skip_default_value = $csviregistry->GetVar('skip_default_value');
		if ($csviregistry->IssetVar('show_preview')) $templates->show_preview = $csviregistry->GetVar('show_preview');
		if ($csviregistry->IssetVar('include_column_headers')) $templates->include_column_headers = $csviregistry->GetVar('include_column_headers');
		if ($csviregistry->IssetVar('text_enclosure')) $templates->text_enclosure = $csviregistry->GetVar('text_enclosure');
		if ($csviregistry->IssetVar('field_delimiter')) $templates->field_delimiter = $csviregistry->GetVar('field_delimiter');
		if ($csviregistry->IssetVar('export_type')) $templates->export_type = $csviregistry->GetVar('export_type');
		if ($csviregistry->IssetVar('export_site')) $templates->export_site = $csviregistry->GetVar('export_site');
		if ($csviregistry->IssetVar('thumb_width')) $templates->thumb_width = $csviregistry->GetVar('thumb_width');
		if ($csviregistry->IssetVar('thumb_height')) $templates->thumb_height = $csviregistry->GetVar('thumb_height');
		if ($csviregistry->IssetVar('shopper_group_id')) $templates->shopper_group_id = $csviregistry->GetVar('shopper_group_id');
		if ($csviregistry->IssetVar('producturl_suffix')) $templates->producturl_suffix = $csviregistry->GetVar('producturl_suffix');
		if ($csviregistry->IssetVar('file_location')) $templates->file_location = $csviregistry->GetVar('file_location');
		if ($csviregistry->IssetVar('product_publish')) $templates->product_publish = $csviregistry->GetVar('product_publish');
		if ($csviregistry->IssetVar('max_execution_time')) $templates->max_execution_time = $csviregistry->GetVar('max_execution_time');
		if ($csviregistry->IssetVar('max_input_time')) $templates->max_input_time = $csviregistry->GetVar('max_input_time');
		if ($csviregistry->IssetVar('memory_limit')) $templates->memory_limit = $csviregistry->GetVar('memory_limit');
		if ($csviregistry->IssetVar('post_max_size')) $templates->post_max_size = $csviregistry->GetVar('post_max_size');
		if ($csviregistry->IssetVar('upload_max_filesize')) $templates->upload_max_filesize = $csviregistry->GetVar('upload_max_filesize');
		if ($csviregistry->IssetVar('export_filename')) $templates->export_filename = $csviregistry->GetVar('export_filename');
		if ($csviregistry->IssetVar('manufacturer')) $templates->manufacturer = $csviregistry->GetVar('manufacturer');
		
		echo '<div id="templateconfig-steps">';
		
		/* First step */
		echo '<input type="submit" class="';
		if ($csviregistry->GetVar('step') == 1 || $csviregistry->GetVar('step') == '') echo 'templateconfig-active';
		else echo 'submit';
		echo '" value="Choose your template" onclick="document.adminForm.act.value = \'rpc\';document.adminForm.step.value = 1;TemplateConfigurator(\'adminForm\', \'templateconfig\');return false;" />'."\n";
		
		/* Second step */
		echo '<input type="submit" class="';
		if ($csviregistry->GetVar('step') == 2) echo 'templateconfig-active';
		else echo 'submit';
		echo '" value="';
		switch ($csviregistry->GetVar('templatetype')) {
			case 'import':
				echo 'Import options';
				$csvisupportedfields->FieldsImport();
				break;
			case 'export':
				echo 'Export options';
				$csvisupportedfields->FieldsExport();
				break;
			default:
				echo 'No template type found!';
				break;
		}
		echo  '" onclick="document.adminForm.act.value = \'rpc\';document.adminForm.step.value = 2;TemplateConfigurator(\'adminForm\', \'templateconfig\');return false;" />'."\n";
		/* Third step */
		echo '<input type="submit" class="';
		if ($csviregistry->GetVar('step') == 3) echo 'templateconfig-active';
		else echo 'submit';
		echo '" value="'.$csvilang->SYSTEM_LIMITS.'" onclick="document.adminForm.act.value = \'rpc\';document.adminForm.step.value = 3;TemplateConfigurator(\'adminForm\', \'templateconfig\');return false;" />'."\n";
		/* Fourth step */
		echo '<input type="submit" class="';
		if ($csviregistry->GetVar('step') == 4) echo 'templateconfig-active';
		else echo 'submit';
		echo '" value="'.$csvilang->GENERAL.'" onclick="document.adminForm.act.value = \'rpc\';document.adminForm.step.value = 4;TemplateConfigurator(\'adminForm\', \'templateconfig\');return false;" />'."\n";
		echo '</div>';
		echo '<div id="templateconfig-content">';
			if ($csviregistry->GetVar('step') == 4) echo '<form name="adminForm" id="adminForm" method="post" action="'.$_SERVER['PHP_SELF'].'">';
			else echo '<form name="adminForm" id="adminForm" onsubmit="TemplateConfigurator(\'adminForm\', \'templateconfig\'); return false;">';
			
			/* Add all hidden fields to preserve user choices */
			$this->AddHiddenFields($csviregistry);
			
			if ($csviregistry->GetVar('step') == 1) {
				echo '<input type="radio" name="templatetype" id="templatetype" value="import" ';
				if ($csviregistry->GetVar('templatetype') == 'import') echo 'checked=checked';
				echo '>'.$csvilang->IMPORT.'</input>';
				echo '<br />';
				echo '<input type="radio" name="templatetype" id="templatetype" value="export" ';
				if ($csviregistry->GetVar('templatetype') == 'export') echo 'checked=checked';
				echo '>'.$csvilang->EXPORT.'</input>';
			}
			if ($csviregistry->GetVar('step') == 2) {
				echo '<label class="template_label" for="template_type">'.$csvilang->TYPE.'</label>';
				echo '<select id="template_type" name="template_type">';
				foreach ($csvisupportedfields->fields as $name => $value) {
					echo "<option value=\"".$name."\"";
					if ($templates->template_type == $name) echo 'selected="selected"';
					echo ">".$csvilang->$value."</option>\n";
				}
				echo '</select>';
				echo '<br />';
			}
			if ($csviregistry->GetVar('step') == 2 && $csviregistry->GetVar('templatetype') == 'import') {
				/* Use column headers */
				echo '<div id="div_import_config_csv_file" style="display: ';
				if ($csviregistry->IssetVar('skip_first_line') && $csviregistry->GetVar('skip_first_line') == 1 &&
					$csviregistry->IssetVar('use_column_headers') && $csviregistry->GetVar('use_column_headers') == 1
					) {
						echo 'block';
						$csviregistry->SetVar('skip_first_line', 0);
					}
				else if ($csviregistry->IssetVar('skip_first_line') && $csviregistry->GetVar('skip_first_line') == 1 &&
					$csviregistry->IssetVar('use_column_headers') && $csviregistry->GetVar('use_column_headers') == 0
				) echo 'none';
				else echo 'block';
				echo '">';
					echo '<label class="template_label" for="use_column_headers">'.$csvilang->IMPORT_CONFIG_CSV_FILE.'</label>';
					echo '<input class="template_input checkbox" type="checkbox" id="use_column_headers" name="use_column_headers" value="'.$templates->use_column_headers.'"';
					if ($csviregistry->GetVar('use_column_headers') == 1) echo 'checked=checked';
					echo '" onClick="if (document.adminForm.use_column_headers.checked == true) { 
							document.adminForm.skip_first_line.checked = false; 
							expand_layer(\'div_skip_first_line\', false);
							}
							else {
								expand_layer(\'div_skip_first_line\', true)
							}">';
					echo '<br />';
				echo '</div>';
				
				/* Skip first line */
				echo '<div id="div_skip_first_line" style="display: ';
				if ($csviregistry->IssetVar('skip_first_line') && $csviregistry->GetVar('skip_first_line') == 1) echo 'block';
				else echo 'none';
				echo '">';
					echo '<label class="template_label" for="skip_first_line">'.$csvilang->SKIP_FIRST_LINE.'</label>';
					echo '<input class="template_input checkbox" type="checkbox" id="skip_first_line" name="skip_first_line" value="'.$templates->skip_first_line.'"';
					if ($csviregistry->GetVar('skip_first_line') == 1) echo 'checked=checked';
					echo '" onClick="if (document.adminForm.skip_first_line.checked == true) { 
							document.adminForm.use_column_headers.checked = false; 
							expand_layer(\'div_import_config_csv_file\', false);
							}
							else {
								expand_layer(\'div_import_config_csv_file\', true)
							}">';
					echo '<br />';
				echo '</div>';
				
				/* Overwrite existing data */
				echo '<label class="template_label" for="overwrite_existing_data">'.$csvilang->OVERWRITE_EXISTING_DATA.'</label>';
				echo '<input class="template_input checkbox" type="checkbox" id="overwrite_existing_data" name="overwrite_existing_data" value="'.$templates->overwrite_existing_data.'"';
				if ($templates->overwrite_existing_data) echo 'checked=checked';
				echo '">';
				
				echo '<br />';
				
				/* Skip default value */
				echo '<label class="template_label" for="skip_default_value">'.$csvilang->SKIP_DEFAULT_VALUE.'</label>';
				echo '<input class="template_input checkbox" type="checkbox" id="skip_default_value" name="skip_default_value" value="'.$templates->skip_default_value.'"';
				if ($templates->skip_default_value) echo 'checked=checked';
				echo '">';
				
				echo '<br />';
				
				/* Show preview */
				echo '<label class="template_label" for="show_preview">'.$csvilang->SHOW_PREVIEW.'</label>';
				echo '<input class="template_input checkbox" type="checkbox" id="show_preview" name="show_preview" value="'.$templates->show_preview.'"';
				if ($templates->show_preview) echo 'checked=checked';
				echo '">';
				
				echo '<br />';
				
				/* Thumbnail width */
				echo '<label class="template_label" for="thumb_width">'.$csvilang->TEMPLATE_THUMB_WIDTH.'</label>';
				if (empty($templates->thumb_width)) $templates->thumb_width = 90;
				if (empty($templates->thumb_height)) $templates->thumb_height = 90;
				echo '<input class="template_input thumbs" type="text" maxlength="4" id="thumb_width" name="thumb_width" value="'.$templates->thumb_width.'">';
				echo '<span class="template_img_symbol">x</span>';
				echo '<input class="thumbs" type="text" maxlength="4" id="thumb_height" name="thumb_height" value="'.$templates->thumb_height.'">';
				echo '<br />';
				
				/* Collect debug info */
				echo '<label class="template_label" for="collect_debug_info">'.$csvilang->COLLECT_DEBUG_INFO.'</label>';
				echo '<input class="template_input checkbox" type="checkbox" id="collect_debug_info" name="collect_debug_info" value="'.$templates->collect_debug_info.'"';
				if ($templates->collect_debug_info) echo 'checked=checked';
				echo '">';
			}
			
			if ($csviregistry->GetVar('step') == 2 && $csviregistry->GetVar('templatetype') == 'export') {
				echo '<br />';
				/* Export type */
				echo '<label class="template_label" for="export_type">'.$csvilang->EXPORT_TYPE.'</label>';
				echo '<select id="export_type" name="export_type" onchange="selObj = document.getElementById(\'export_type\'); if (selObj.options[selObj.selectedIndex].text == \'XML\') {switchMenu(\'div_export_site\'); expand_layer(\'div_include_column_headers\', false);} else {expand_layer(\'div_export_site\', false);expand_layer(\'div_include_column_headers\', true);}">';
				echo '<option value="csv"';
				if ($templates->export_type == "csv") echo 'selected="selected"';
				echo '>CSV</option>';
				echo '<option value="xml"';
				if ($templates->export_type == "xml") echo 'selected="selected"';
				echo '>XML</option>';
				if (0) {
					echo '<option value="xls"';
					if ($templates->export_type == "xls") echo 'selected="selected"';
					echo '>Excel</option>';
				}
				echo '</select>';
				
				echo '<br />';
				
				echo '<div id="div_export_site" style="display: ';
				if ($csviregistry->GetVar('export_type') == "xml" || $templates->export_type == "xml") echo 'block';
				else echo 'none';
				echo ';">';
					echo '<label class="template_label" for="export_site">'.$csvilang->EXPORT_SITE.'</label>';
					echo '<select name="export_site">';
					echo '<option value=""';
					if ($templates->export_site == "") echo 'selected="selected"';
					echo '>'.$csvilang->CHOOSE_SITE.'</option>';
					echo '<option value="csvi"';
					if ($templates->export_site == "csvi") echo 'selected="selected"';
					echo '>CSV Improved</option>';
					echo '<option value="beslist"';
					if ($templates->export_site == "beslist") echo 'selected="selected"';
					echo '>beslist.nl</option>';
					echo '<option value="froogle"';
					if ($templates->export_site == "froogle") echo 'selected="selected"';
					echo '>Google Base</option>';
					echo '</select>';
					echo '<br />';
				echo '</div>';
				
				/* Include column headers */
				echo '<div id="div_include_column_headers" style="display: ';
				if ($csviregistry->GetVar('export_type') == "xml" || $templates->export_type == "xml") echo 'none';
				else echo 'block';
				echo ';">';
					echo '<label class="template_label" for="include_column_headers">'.$csvilang->INCLUDE_COLUMN_HEADERS.'</label>';
					echo '<input class="template_input checkbox" type="checkbox" id="include_column_headers" name="include_column_headers" value="'.$templates->include_column_headers.'"';
					if ($templates->include_column_headers == 1) echo 'checked=checked';
					echo '">';
					echo '<br />';
				echo '</div>';
				
				/* Export filename */
				echo '<label class="template_label" for="export_filename">'.$csvilang->TEMPLATE_EXPORT_FILENAME.'</label>';
				echo '<input class="template_input longtext" type="text" id="export_filename" name="export_filename" value="'.$templates->export_filename.'" />';
				
				echo '<br />';
				
				/* Set shopper group name the user wants to export */
				/* Get all the shopper groups */
				$db = $csviregistry->GetObject('database');
				$q = "SELECT shopper_group_id, shopper_group_name FROM #__vm_shopper_group";
				$db->query($q);
				$shopper_groups = $db->loadObjectList();
				echo '<label class="template_label" for="shopper_group_id">'.$csvilang->TEMPLATE_EXPORT_SHOPPER_GROUP.'</label>';
				echo '<select name="shopper_group_id">';
				echo '<option value="0">'.$csvilang->TEMPLATE_EXPORT_ALL.'</option>';
				foreach ($shopper_groups as $key => $group) {
					echo '<option value="'.$group->shopper_group_id.'"';
					if ($templates->shopper_group_id == $group->shopper_group_id) echo 'selected="selected"';
					echo ">".$group->shopper_group_name."</option>\n";
				}
				echo '</select>';
				
				echo '<br />';
				
				/* Set manufacturer name the user wants to export */
				/* Get all the manufacturers */
				$db = $csviregistry->GetObject('database');
				$q = "SELECT manufacturer_id, mf_name FROM #__vm_manufacturer ORDER BY mf_name";
				$db->query($q);
				$manufacturers = $db->loadObjectList();
				$template_mf = explode(",", $templates->manufacturer);
				echo '<label class="template_label" for="manufacturer">'.$csvilang->TEMPLATE_EXPORT_MANUFACTURER.'</label>';
				echo '<select multiple name="manufacturer[]" size="7">';
				echo '<option value="0" ';
				if (in_array("0", $template_mf)) echo 'selected="selected"';
				echo '>'.$csvilang->TEMPLATE_EXPORT_ALL.'</option>';
				foreach ($manufacturers as $key => $mf) {
					echo '<option value="'.$mf->manufacturer_id.'"';
					if (in_array($mf->manufacturer_id, $template_mf)) echo 'selected="selected"';
					echo ">".$mf->mf_name."</option>\n";
				}
				echo '</select>';
				
				echo '<br />';
				
				/* Check which state of products the user want to export */
				echo '<label class="template_label" for="product_publish">'.$csvilang->TEMPLATE_EXPORT_PRODUCT_PUBLISH.'</label>';
				echo '<input type="radio" id="product_publish" name="product_publish" value=""';
				if ($templates->product_publish == '') echo 'checked=checked';
				echo '">'.$csvilang->TEMPLATE_EXPORT_PRODUCT_PUBLISHED_BOTH;
				echo '<br />';
				echo '<label class="template_label" for="product_publish"></label>';
				echo '<input type="radio" id="product_publish" name="product_publish" value="Y"';
				if ($templates->product_publish == 'Y') echo 'checked=checked';
				echo '">'.$csvilang->TEMPLATE_EXPORT_PRODUCT_PUBLISHED;
				echo '<br />';
				echo '<label class="template_label" for="product_publish"></label>';
				echo '<input type="radio" id="product_publish" name="product_publish" value="N"';
				if ($templates->product_publish == 'N') echo 'checked=checked';
				echo '">'.$csvilang->TEMPLATE_EXPORT_PRODUCT_UNPUBLISHED;
				
				echo '<br />';
				
				/* Product URL suffix */
				echo '<label class="template_label" for="producturl_suffix">'.$csvilang->EXPORT_PRODUCTURL_SUFFIX.'</label>';
				echo '<input class="template_input longtext" type="text" id="producturl_suffix" name="producturl_suffix" value="'.$templates->producturl_suffix.'" />';
			}
			
			if ($csviregistry->GetVar('step') == 3) {
				/* Maximum execution time */
				echo '<label class="template_label" for="max_execution_time">'.$csvilang->TEMPLATE_MAX_EXECUTION_TIME.'</label>';
				echo '<input class="template_input" type="text" id="max_execution_time" name="max_execution_time" value="'.$templates->max_execution_time.'">';
				
				echo '<br />';
				
				/* Maximum input time */
				echo '<label class="template_label" for="max_input_time">'.$csvilang->TEMPLATE_MAX_INPUT_TIME.'</label>';
				echo '<input class="template_input" type="text" id="max_input_time" name="max_input_time" value="'.$templates->max_input_time.'">';
				
				echo '<br />';
				
				/* Maximum memory */
				echo '<label class="template_label" for="memory_limit">'.$csvilang->TEMPLATE_MEMORY_LIMIT.'</label>';
				echo '<input class="template_input" type="text" id="memory_limit" name="memory_limit" value="'.$templates->memory_limit.'">';
				
				echo '<br />';
				
				/* Maximum POST size */
				echo '<label class="template_label" for="post_max_size">'.$csvilang->TEMPLATE_POST_MAX_SIZE.'</label>';
				echo '<input class="template_input" type="text" id="post_max_size" name="post_max_size" value="'.$templates->post_max_size.'">';
				
				echo '<br />';
				
				/* Maximum Upload size */
				echo '<label class="template_label" for="upload_max_filesize">'.$csvilang->TEMPLATE_UPLOAD_MAX_FILESIZE.'</label>';
				echo '<input class="template_input" type="text" id="upload_max_filesize" name="upload_max_filesize" value="'.$templates->upload_max_filesize.'">';
			}
			
			if ($csviregistry->GetVar('step') == 4) {
				/* Template name */
				echo '<label class="template_label" for="template_name">'.$csvilang->NAME.'</label>';
				echo '<input class="template_input" type="text" id="template_name" name="template_name" value="'.$templates->template_name.'">';
				
				echo '<br />';
				if (($csviregistry->GetVar('export_type') == 'csv' && $csviregistry->GetVar('templatetype') == 'export') || $csviregistry->GetVar('templatetype') == 'import') {
					/* Field delimiter */
					echo '<label class="template_label" for="field_delimiter">'.$csvilang->ENCLOSURE.'</label>';
					echo '<input class="template_input delimiter" type="text" maxlength="1" id="field_delimiter" name="field_delimiter" value="'.$templates->field_delimiter.'">';
					
					echo '<br />';
					
					/* Text enclosure */
					echo '<label class="template_label" for="text_enclosure">'.$csvilang->DELIMITER.'</label>';
					echo '<input class="template_input delimiter" type="text" maxlength="1" id="text_enclosure" name="text_enclosure"';
					if ($templates->text_enclosure == '"') echo "value='".$templates->text_enclosure."'>";
					else echo 'value="'.$templates->text_enclosure.'">';
					
					echo '<br />';
				}
				
				/* File location */
				echo '<label class="template_label" for="file_location">'.$csvilang->TEMPLATE_FILE_LOCATION.'</label>';
				echo '<input class="template_input longtext" type="text" id="file_location" name="file_location" size="255" value="';
				if (empty($templates->file_location)) echo $csviregistry->GetVar('absolute_path').'/components/com_virtuemart/shop_image/product/';
				else echo $templates->file_location;
				echo '">';
				echo '<br />';
				echo '<label class="template_label" for="pathsuggest">'.$csvilang->STANDARD_FILE_LOCATION.'</label>';
				echo '<span id="pathsuggest">'.$csviregistry->GetVar('absolute_path').'/components/com_virtuemart/shop_image/product/</span> | 
				<a href="#" onclick="document.getElementById(\'file_location\').value=document.getElementById(\'pathsuggest\').innerHTML; return false;">'.$csvilang->PASTE.'</a> | 
				<a href="#" onclick="document.getElementById(\'file_location\').value=\'\'; return false;">'.$csvilang->CLEAR.'</a>';
			}
			
			?>
			<br />
			<br />
			</form>
		</div>
	<?php }
	
	function AddHiddenFields(&$csviregistry) {
		?>
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="option" value="com_csvimproved" />
		<input type="hidden" name="step" id="step" value="<?php echo ($csviregistry->GetVar('step')+1); ?>" />
		<input type="hidden" name="act" value="templates" />
		<input type="hidden" name="templateid" value ="<?php echo $csviregistry->GetVar('templateid'); ?>" />
		<?php
		
		if ($csviregistry->GetVar('step') <> 1 ) {
			if ($csviregistry->IssetVar('templatetype')) {
				echo '<input type="hidden" name="templatetype" id="templatetype" value="'.$csviregistry->GetVar('templatetype').'" />';
			}
			if ($csviregistry->IssetVar('template_type')) {
				echo '<input type="hidden" name="template_type" id="template_type" value="'.$csviregistry->GetVar('template_type').'" />';
			}
		}
		if ($csviregistry->GetVar('step') <> 2 && $csviregistry->GetVar('templatetype') == 'import') {
			if ($csviregistry->IssetVar('use_column_headers')) {
				echo '<input type="hidden" name="use_column_headers" id="use_column_headers" value="'.$csviregistry->GetVar('use_column_headers').'" />';
			}
			if ($csviregistry->IssetVar('skip_first_line')) {
				echo '<input type="hidden" name="skip_first_line" id="skip_first_line" value="'.$csviregistry->GetVar('skip_first_line').'" />';
			}
			if ($csviregistry->IssetVar('overwrite_existing_data')) {
				echo '<input type="hidden" name="overwrite_existing_data" id="overwrite_existing_data" value="'.$csviregistry->GetVar('overwrite_existing_data').'" />';
			}
			if ($csviregistry->IssetVar('skip_default_value')) {
				echo '<input type="hidden" name="skip_default_value" id="skip_default_value" value="'.$csviregistry->GetVar('skip_default_value').'" />';
			}
			if ($csviregistry->IssetVar('show_preview')) {
				echo '<input type="hidden" name="show_preview" id="show_preview" value="'.$csviregistry->GetVar('show_preview').'" />';
			}
			if ($csviregistry->IssetVar('thumb_width')) {
				echo '<input type="hidden" name="thumb_width" id="thumb_width" value="'.$csviregistry->GetVar('thumb_width').'" />';
			}
			if ($csviregistry->IssetVar('thumb_height')) {
				echo '<input type="hidden" name="thumb_height" id="thumb_height" value="'.$csviregistry->GetVar('thumb_height').'" />';
			}
			if ($csviregistry->IssetVar('collect_debug_info')) {
				echo '<input type="hidden" name="collect_debug_info" id="collect_debug_info" value="'.$csviregistry->GetVar('collect_debug_info').'" />';
			}
		}
		
		if ($csviregistry->GetVar('step') <> 2 && $csviregistry->GetVar('templatetype') == 'export') {
			if ($csviregistry->IssetVar('export_type')) {
				echo '<input type="hidden" name="export_type" id="export_type" value="'.$csviregistry->GetVar('export_type').'" />';
			}
			if ($csviregistry->IssetVar('export_site')) {
				echo '<input type="hidden" name="export_site" id="export_site" value="'.$csviregistry->GetVar('export_site').'" />';
			}
			if ($csviregistry->IssetVar('include_column_headers')) {
				echo '<input type="hidden" name="include_column_headers" id="include_column_headers" value="1" />';
			}
			else echo '<input type="hidden" name="include_column_headers" id="include_column_headers" value="0" />';
			if ($csviregistry->IssetVar('export_filename')) {
				echo '<input type="hidden" name="export_filename" id="export_filename" value="'.$csviregistry->GetVar('export_filename').'" />';
			}
			if ($csviregistry->IssetVar('shopper_group_id')) {
				echo '<input type="hidden" name="shopper_group_id" id="shopper_group_id" value="'.$csviregistry->GetVar('shopper_group_id').'" />';
			}
			/**
			 * Check if there is a manufacturer string or array
			 *
			 * If there is a string it means we already have a serialized array.
			 */
			if ($csviregistry->IssetVar('manufacturer')) {
				echo '<input type="hidden" name="manufacturer" id="manufacturer" value="'.$csviregistry->GetVar('manufacturer').'" />';
			}
			else if ($csviregistry->IssetArray('manufacturer')) {
				$mf_string = '';
				foreach ($csviregistry->GetArray('manufacturer') as $key => $mf_id) {
					$mf_string .= $mf_id[0].",";
				}
				echo '<input type="hidden" name="manufacturer" id="manufacturer" value="'.substr($mf_string, 0, -1).'" />';
			}
			if ($csviregistry->IssetVar('product_publish')) {
				echo '<input type="hidden" name="product_publish" id="product_publish" value="'.$csviregistry->GetVar('product_publish').'" />';
			}
			if ($csviregistry->IssetVar('producturl_suffix')) {
				echo '<input type="hidden" name="producturl_suffix" id="producturl_suffix" value="'.$csviregistry->GetVar('producturl_suffix').'" />';
			}
		}
		
		if ($csviregistry->GetVar('step') <> 3 ) {
			if ($csviregistry->IssetVar('max_execution_time')) {
				echo '<input type="hidden" name="max_execution_time" id="max_execution_time" value="'.$csviregistry->GetVar('max_execution_time').'" />';
			}
			if ($csviregistry->IssetVar('max_input_time')) {
				echo '<input type="hidden" name="max_input_time" id="max_input_time" value="'.$csviregistry->GetVar('max_input_time').'" />';
			}
			if ($csviregistry->IssetVar('memory_limit')) {
				echo '<input type="hidden" name="memory_limit" id="memory_limit" value="'.$csviregistry->GetVar('memory_limit').'" />';
			}
			if ($csviregistry->IssetVar('post_max_size')) {
				echo '<input type="hidden" name="post_max_size" id="post_max_size" value="'.$csviregistry->GetVar('post_max_size').'" />';
			}
			if ($csviregistry->IssetVar('upload_max_filesize')) {
				echo '<input type="hidden" name="upload_max_filesize" id="upload_max_filesize" value="'.$csviregistry->GetVar('upload_max_filesize').'" />';
			}
		}
		
		if ($csviregistry->GetVar('step') <> 4 ) {
			if ($csviregistry->IssetVar('template_name')) {
				echo '<input type="hidden" name="template_name" id="template_name" value="'.$csviregistry->GetVar('template_name').'" />';
			}
			if ($csviregistry->IssetVar('field_delimiter')) {
				echo '<input type="hidden" name="field_delimiter" id="field_delimiter" value="'.$csviregistry->GetVar('field_delimiter').'" />';
			}
			if ($csviregistry->IssetVar('text_enclosure')) {
				echo '<input type="hidden" name="text_enclosure" id="text_enclosure" value="'.$csviregistry->GetVar('text_enclosure').'" />';
			}
			if ($csviregistry->IssetVar('file_location')) {
				echo '<input type="hidden" name="file_location" id="file_location" value="'.$csviregistry->GetVar('file_location').'" />';
			}
		}
	}
	
	/**
	 * Adds a new template field to the database for the current template
	 *
	 * @param &$csviregistry object Global register
	 */
	 function AddTemplateField(&$csviregistry) {
		 $fieldconfig = $csviregistry->GetObject('fieldconfig');
		 if ($fieldconfig->AddConfigField($csviregistry)) {
			 require_once($csviregistry->GetVar('page_path').'csvi_page_fields.php');
			 FieldsList($csviregistry);
		 }
	 }
	 
	 /**
	 * Adds a new template field to the database for the current template
	 *
	 * @param &$csviregistry object Global register
	 */
	 function DeleteTemplateField(&$csviregistry) {
		 $fieldconfig = $csviregistry->GetObject('fieldconfig');
		 $csvilog = $csviregistry->GetObject('logger');
		 if ($fieldconfig->DeleteConfigField($csviregistry)) {
			 require_once($csviregistry->GetVar('page_path').'csvi_page_fields.php');
			 FieldsList($csviregistry);
		 }
	 }
}
?>
