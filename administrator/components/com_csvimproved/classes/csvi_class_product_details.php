<?php
/**
 * Product details
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Import
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_class_product_details.php 231 2008-05-13 23:04:28Z Suami $
 */
 
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * Processor for product details
 *
 * Main processor for handling product details.
 *
 * @package CSVImproved
 */
class product_details {
	
	var $product_id = false;
	var $vendor_id = false;
	var $product_parent_id  = false;
	var $product_sku  = false;
	var $product_s_desc  = false;
	var $product_desc  = false;
	var $product_thumb_image  = false;
	var $product_full_image  = false;
	var $product_publish  = 'Y';
	var $product_weight  = false;
	var $product_weight_uom  = false;
	var $product_length  = false;
	var $product_width  = false;
	var $product_height  = false;
	var $product_lwh_uom  = false;
	var $product_url  = false;
	var $product_in_stock  = false;
	var $product_available_date  = false;
	var $product_availability  = false;
	var $product_special  = false;
	var $product_discount = 0;
	var $product_discount_percentage = "0";
	var $product_discount_date_start = 0;
	var $product_discount_date_end = 0;
	var $product_discount_id = false;
	var $product_name  = false;
	var $product_sales  = false;
	var $attribute  = false;
	var $attributes  = false;
	var $custom_attribute  = false;
	var $product_tax  = false;
	var $product_tax_id  = false;
	var $product_unit  = false;
	var $product_delete = false;
	var $attribute_values = false;
	var $manufacturer_id = false;
	var $manufacturer_name = false;
	var $product_price = false;
	var $product_parent_sku = 0;
	var $category_path = false;
	var $category_id = false;
	var $child_product = false;
	var $product_packaging = 0;
	var $product_box = 0;
	var $product_currency = false;
	var $price_quantity_start = 0;
	var $price_quantity_end = 0;
	var $price_delete = false;
	var $shopper_group_name = false;
	var $shopper_group_id = 0;
	var $related_products = false;
	var $product_cdate = false;
	var $product_mdate = false;
	var $child_options = '';
	var $quantity_options = '';
	var $child_option_ids = '';
	var $product_order_levels = '';
	
	/**
	 * Main function to process product details
	 *
	 * @param object $csviregistry the main controller class
	 */
	function product_details(&$csviregistry) {
		/* These functions we call ourselves */
		$csv_fields = $csviregistry->GetArray('csv_fields');
		/* Get the statistics */
		$csvilog = $csviregistry->GetObject('logger');
		
		$skip_functions = array("product_parent_sku");
		$this->get_product_id($csviregistry);
		$this->GetVendorId();
		
		/* Set the default shopper_group_id */
		$this->GetDefaultShopperGroupID($csviregistry);
		$this->shopper_group_id = $csviregistry->GetVar('shopper_group_id');
		
		/* Validate CSV Input */
		if (count($csv_fields) != count($csviregistry->GetArray('product_data'))) {
			$csvilog->AddStats('incorrect', "Line ".$csviregistry->currentline.": <strong>Incorrect column count</strong><br />Configration: ".count($csv_fields)." fields<br />File: ".count($csviregistry->GetArray('product_data'))." fields");
			return false;
		}                                
		else {
			foreach ($csv_fields as $name => $details) {
				if ($this->ValidateCsvInput($csviregistry, $name)) {
					/* Check if the field needs extra treatment */
					switch ($name) {
						case 'product_available_date':
							$this->get_product_available_date();
							break;
						case 'product_box':
							$this->get_product_packaging();
							break;
						case 'product_discount':
							$this->get_product_discount($csviregistry);
							break;
						case 'product_discount_date_start':
							$this->get_product_discount_date_start();
							break;
						case 'product_discount_date_end':
							$this->get_product_discount_date_end();
							break;
						case 'product_packaging':
							$this->get_product_packaging();
							break;
						case 'product_price':
							$this->get_product_price();
							break;
						case 'shopper_group_name':
							$this->get_shopper_group_name($csviregistry);
							break;
						case 'related_products':
							$this->get_related_products();
							break;
						case 'product_publish':
							if (!$this->product_publish) $this->product_publish = 'Y';
							break;
						case 'product_cdate':
							$this->get_product_cdate();
							break;
						case 'product_mdate':
							$this->get_product_mdate();
							break;
						case 'category_id':
							$this->GetCategoryId();
							break;
					}
				}
				else {
					$csvilog->AddStats('incorrect', 'Column mismatch for field '.$name);
				}
			}
		}
		
		/* We need the currency */
		$this->get_product_currency($csviregistry);
		
		/* Check for child product and get parent SKU if it is */
		$this->get_product_parent_sku($csviregistry);
	}
	
	/**
	 * Determine vendor ID
	 *
	 * Determine for which vendor we are importing product details. At the
	 * moment this is default 1 as VirtueMart only supports 1 vendor.
	 */
	function GetVendorId() {
		$this->vendor_id = 1;
	}
	
	/**
	 * Get the product currency from the vendor 
	 *
	 * @internal If the user does not use product currency we take the one from the current vendor
	 * @param object &$csviregistry the main controller class
	 */
	function get_product_currency(&$csviregistry) {
		if (!$this->product_currency) {
			$db = $csviregistry->GetObject('database');
			$q = "SELECT vendor_currency FROM #__vm_vendor WHERE vendor_id='".$this->vendor_id."' ";
			$db->query($q);
			$db->next_record();
			$this->product_currency = $db->f("vendor_currency");
		}
	}
	
	/**
	 * Get the product id, this is necessary for updating existing products 
	 *
	 * @param object &$csviregistry the main controller class
	 */
	function get_product_id(&$csviregistry) {
		$db = $csviregistry->GetObject('database');
		$csv_fields = $csviregistry->GetArray('csv_fields');
		$product_data = $csviregistry->GetArray('product_data');
		
		if (isset($csv_fields["product_sku"])) {
			$q = "SELECT product_id FROM #__vm_product WHERE product_sku = '".$product_data[$csv_fields["product_sku"]["order"]]."'";
			$db->query($q);
			$db->next_record();
			if (isset($db->record[0])) $this->product_id = $db->f("product_id");
			else $this->product_id = false;
		}
		else $this->product_id = false;
	}
	
	/**
	 * Get the product available date 
	 *
	 * @param object &$csviregistry the main controller class
	 */
	function get_product_available_date() {
		if ($this->product_available_date) {
			$this->product_available_date = $this->ConvertDate($this->product_available_date);
		}
	}
	
	/**
	 * Get the product discount
	 *
	 * @internal Replace commas with periods for correct DB insertion of the product price
	 */
	function get_product_discount() {
		/** @internal Check first to see if we are dealing with a percentage */
		$this->get_product_discount_percentage();
		if ($this->product_discount) {
			if ($this->product_discount_percentage) {
				$this->product_discount = substr(str_replace(",",".",$this->product_discount), 0, -1);
			}
			else $this->product_discount = str_replace(",",".",$this->product_discount);
		}
	}
	
	/**
	 * Determine if the discount is a percentage or not 
	 */
	function get_product_discount_percentage() {
		if ($this->product_discount) {
			if (substr($this->product_discount,-1,1) == "%") {
				$this->product_discount_percentage = "1";
			}
		}
	}
	
	/**
	 * Get the start date of the product discount 
	 */
	function get_product_discount_date_start() {
		if ($this->product_discount_date_start) {
			$this->product_discount_date_start = $this->ConvertDate($this->product_discount_date_start);
		}
	}
	
	/**
	 * Get the end date of the product discount 
	 */
	function get_product_discount_date_end() {
		if ($this->product_discount_date_end) {
			$this->product_discount_date_end = $this->ConvertDate($this->product_discount_date_end);
		}
	}
	
	/**
	 * Get the product creation date 
	 *
	 * @param object &$csviregistry the main controller class
	 */
	function get_product_cdate() {
		if ($this->product_cdate) {
			$this->product_cdate = $this->ConvertDate($this->product_cdate);
		}
	}
	
	/**
	 * Get the product modified date 
	 *
	 * @param object &$csviregistry the main controller class
	 */
	function get_product_mdate() {
		if ($this->product_mdate) {
			$this->product_mdate = $this->ConvertDate($this->product_mdate);
		}
	}
	
	/**
	 * Format a date to unix timestamp
	 *
	 * @return integer UNIX timestamp if date is valid otherwise return 0
	 */
	function ConvertDate(&$old_date) {
		/* Date must be in the format of day/month/year */
		$new_date = preg_replace('/-|\./', '/', $old_date);
		$date_parts = explode('/', $new_date);
		if ((count($date_parts) == 3) && ($date_parts[0] > 0 && $date_parts[0] < 32 && $date_parts[1] > 0 && $date_parts[1] < 13 && (strlen($date_parts[2]) == 4))) {
			$old_date = mktime(0,0,0,$date_parts[1],$date_parts[0],$date_parts[2]);
		}
		else $old_date = 0;
		return $old_date;
	}
	
	/**
	 * Get the product packaging
	 *
	 * @internal The number is calculated by hexnumbers
	 */
	function get_product_packaging() {
		$this->product_packaging = (($this->product_box<<16) | ($this->product_packaging & 0xFFFF)); 
	}
	
	/**
	 * Get the product price
	 *
	 * @internal Replace commas with periods for correct DB insertion of the product price
	 */
	function get_product_price() {
		if ($this->product_price) $this->product_price = str_replace(",",".",$this->product_price);
	}
	
	/**
	 * Get the product parent sku if it is a child product 
	 */
	function get_product_parent_sku(&$csviregistry) {
		$db = $csviregistry->GetObject('database');
		
		if (!$this->category_path && !$this->category_id) {
			/* Check if we are dealing with a child product */
			if ($this->product_parent_sku !== $this->product_sku) $this->child_product = true;
			else $this->child_product = false;
		}
		
		if (!$this->child_product) $this->product_parent_id = 0;
		else {
			if (!empty($this->product_parent_sku)) {
				/**
				 * Get the parent id first
				 * This assumes that the Parent Product already has been added
				 */
				$db->query("SELECT product_id FROM #__vm_product WHERE product_sku = '".$this->product_parent_sku."'");
				$this->product_parent_id = $db->f("product_id");
			}
			/* We have a child ID bit no idea what the product parent sku is */
			else {
				$db->query("SELECT product_parent_id FROM #__vm_product WHERE product_sku = '".$this->product_sku."'");
				$this->product_parent_id = $db->f("product_parent_id");
			}
		}
	}
	
	/**
	 * Get the shopper group id
	 *
	 * Only get the shopper group id when the shopper_group_name is set
	 *
	 * @param object &$csviregistry the main controller class
	 * @see $shopper_group_name
	 */
	function get_shopper_group_name(&$csviregistry) {
		if (!empty($this->shopper_group_name)) {
			$db = $csviregistry->GetObject('database');
			$q = "SELECT shopper_group_id FROM #__vm_shopper_group WHERE shopper_group_name = '".$this->shopper_group_name."'";
			$db->query($q);
			if ($db->num_rows() > 0) $this->shopper_group_id = $db->f("shopper_group_id");
		}
	}
	
	/**
	 * Check if we have related products 
	 */
	function get_related_products() {
		if (substr($this->related_products, -1, 1) == "|") $this->related_products = substr($this->related_products, 0, -1);
	}
	
	/**
	 * Validate CSV input data
	 *
	 * Checks if the field has a value, if not check if the user wants us to
	 * use the default value
	 *
	 * @param object &$csviregistry the main controller class
	 * @param string $fieldname the fieldname to validate
	 * @return true|false returns true if validation is ok | return false if the column count does not match
	 */
	function ValidateCsvInput(&$csviregistry, $fieldname) {
		$csv_fields = $csviregistry->GetArray('csv_fields');
		$product_data = $csviregistry->GetArray('product_data');
		$skip_default_value = $csviregistry->GetVar('skip_default_value');
		/* Check if the columns match */
		if (count($csv_fields) != count($product_data)) return false;
		else {
			if (isset($csv_fields[$fieldname])) {
				/* Check if the field has a value */
				if (strlen($product_data[$csv_fields[$fieldname]["order"]]) > 0) {
					$this->$fieldname = trim($product_data[$csv_fields[$fieldname]["order"]]);
				}
				/* Field has no value, check if we can use default value*/
				else if (!$skip_default_value) {
					$this->$fieldname = $csv_fields[$fieldname]["default_value"];
				}
			}
		}
		return true;
	}
	
	/**
	 * Stores the discount for a product
	 *
	 * @param object &$csviregistry the main controller class
	 */
	function ProcessDiscount(&$csviregistry) {
		$db = $csviregistry->GetObject('database');
		$csvilog = $csviregistry->GetObject('logger');
		
		if ($this->product_discount) {
			/* Values of the discount to add it to the database */
			$d['amount'] = $this->product_discount;
			$d['is_percent'] = $this->product_discount_percentage;
			$d['start_date'] = $this->product_discount_date_start;
			$d['end_date'] = $this->product_discount_date_end;
			
			/* Check if the amount exists in the database */
			$q_discount = "SELECT COUNT(discount_id) AS total_discount_ids FROM #__vm_product_discount WHERE amount = '".$this->product_discount."' ";
			$q_discount .= "AND is_percent = '".$this->product_discount_percentage."' ";
			$q_discount .= "AND start_date = '".$this->product_discount_date_start."' ";
			$q_discount .= "AND end_date = '".$this->product_discount_date_end."'";
			if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Check if a discount exists: <a onclick="switchMenu(\''.$this->product_sku.'_product_discount\');" title="Show/hide query">Show/hide query</a><div id="'.$this->product_sku.'_product_discount" style="display: none; border: 1px solid #000000; padding: 5px;">'.htmlentities($q_discount).'</div>');
			$db->query($q_discount);
			
			/* There are multiple discount ids, we take the first one */
			if ($db->f('total_discount_ids') > 0) {
				$q_discount = "SELECT MIN(discount_id) AS discount_id FROM #__vm_product_discount WHERE amount = '".$this->product_discount."' ";
				$q_discount .= "AND is_percent = '".$this->product_discount_percentage."' ";
				$q_discount .= "AND start_date = '".$this->product_discount_date_start."' ";
				$q_discount .= "AND end_date = '".$this->product_discount_date_end."'";
				if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Discount exists, return discount id: <a onclick="switchMenu(\''.$this->product_sku.'_discount_id\');" title="Show/hide query">Show/hide query</a><div id="'.$this->product_sku.'_discount_id" style="display: none; border: 1px solid #000000; padding: 5px;">'.htmlentities($q_discount).'</div>');
				$db->query($q_discount);
				$this->product_discount_id = $db->f('discount_id');
			}
			else {
				require_once($csviregistry->GetVar('class_path').'csvi_class_product_discount.php');
				$ps_product_discount = new CsviProductDiscount();
				if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Discount does not exist, create discount');
				$result = $ps_product_discount->add($d, $csviregistry);
				$d['product_discount_id'] = $result[0];
				$this->product_discount_id = $d['product_discount_id'];
			}
		}
		else {
			if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'No discount value specified.');
		}
	}
	
	/**
	 *	Gets the default Shopper Group ID
	 *
	 * @param object &$csviregistry the main controller class
  	 **/
	function GetDefaultShopperGroupID(&$csviregistry) {
		$db = $csviregistry->GetObject('database');
		$csvilog = $csviregistry->GetObject('logger');
		
		$q = "SELECT shopper_group_id FROM #__vm_shopper_group ";
		$q .= "WHERE `default`='1' and vendor_id='".$this->vendor_id."'";
		if ($csviregistry->GetVar('debug')) {
			$randid = rand();
			$csvilog->AddMessage('debug', 'Get default shopper group: <a onclick="switchMenu(\''.$randid.'_shopper_group_id\');" title="Show/hide query">Show/hide query</a><div id="'.$randid.'_shopper_group_id" style="display: none; border: 1px solid #000000; padding: 5px;">'.htmlentities($q).'</div>');
		}
		$db->query($q);
		$db->next_record();
		$csviregistry->SetVar('shopper_group_id', $db->f("shopper_group_id"));
	}
	
	/**
	 * Puts the category ID(s) in an array
	 */
	function GetCategoryId() {
		$category_ids = array();
		if (stripos($this->category_id, '|') > 0) $category_ids = explode("|", $this->category_id);
		else $category_ids[] = $this->category_id;
		$this->category_id = $category_ids;
	}
}
?>
