<?php
/**
 * Product discount class
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Import
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_class_product_discount.php 286 2008-06-01 02:51:30Z Suami $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * The class is is used to manage the discounts in your store.
 * @package CSVImproved
 * @subpackage Import
 */
class CsviProductDiscount {
   var $classname = "CsviProductDiscount";
   var $error;
   
  /**************************************************************************
  ** name: validate_add()
  ** created by:
  ** description:
  ** parameters:
  ** returns:
  ***************************************************************************/  
   function validate_add($d) {
     
     $db = new CsviDatabase();
     if (!$d["amount"]) {
       $this->error = "ERROR:  You must enter an amount for the Discount.";
       return False;	
     }
     if( $d["is_percent"]=="" ) {
       $this->error = "ERROR:  You must select a Discount type.";
       return False;	
     }
     return True;    
   }

  /**************************************************************************
  ** name: validate_update
  ** created by:
  ** description:
  ** parameters:
  ** returns:
  ***************************************************************************/   
  function validate_update($d) {
    
    if (!$d["amount"]) {
      $this->error = "ERROR:  You must enter an amount for the Discount.";
      return False;	
    }
    if( $d["is_percent"]=="" ) {
      $this->error = "ERROR:  You must enter an amount type for the Discount.";
      return False;	
    }
    if (!$d["discount_id"]) {
      $this->error = "ERROR:  You must specifiy a discount to Update.";
      return False;	
    }
   return true;
  }
    
  /**************************************************************************
  ** name: validate_delete()
  ** created by:
  ** description:
  ** parameters:
  ** returns:
  ***************************************************************************/   
  function validate_delete($discount_id) {
    
    if (!$discount_id) {
      $this->error = "ERROR:  Please select a Discount to delete.";
      return False;
    }
    $db = new CsviDatabase();
	$db->query( "SELECT product_id FROM #__vm_product WHERE product_discount_id=".intval($discount_id) );
	if( $db->num_rows() > 0 ) {
		$this->error = "Error: This discount still has products assigned to it!";
		return false;
	}
	
	return True;
    
  }
  
  
  /**************************************************************************
   * name: add()
   * created by: pablo
   * description: creates a new discount record
   * parameters:
   * returns:
   **************************************************************************/
  function add(&$d, &$csviregistry) {
    $db = $csviregistry->GetObject('database');
	$csvilog = $csviregistry->GetObject('logger');
    
    if (!$this->validate_add($d)) {
      $d["error"] = $this->error;
      return False;
    }
    
    $q = "INSERT INTO #__vm_product_discount (amount, is_percent, start_date, end_date)";
    $q .= " VALUES ('";
    $q .= $d["amount"] . "','";
    $q .= $d["is_percent"] . "','";
    $q .= $d["start_date"] . "','";
    $q .= $d["end_date"] . "')";
    $db->query($q);
	if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Adding discount: <a onclick="switchMenu(\''.$db->last_insert_id().'_discount_id\');" title="Show/hide query">Show/hide query</a><div id="'.$db->last_insert_id().'_discount_id" class="debug">'.htmlentities($q).'</div>');
    return array($db->last_insert_id(), true);

  }
  
  /**************************************************************************
   * name: update()
   * created by: pablo
   * description: updates discount information
   * parameters:
   * returns:
   **************************************************************************/
  function update(&$d, &$csviregistry) {
    $db = $csviregistry->GetObject('database');
	$csvilog = $csviregistry->GetObject('logger');


    if (!$this->validate_update($d)) {
      $d["error"] = $this->error;
      return False;	
    }
    if (!empty($d["start_date"])) {
        $day = substr ( $d["start_date"], 8, 2);
        $month= substr ( $d["start_date"], 5, 2);
        $year =substr ( $d["start_date"], 0, 4);
        $d["start_date"] = mktime(0,0,0,$month, $day, $year);
    }
    else {
      $d["start_date"] = "";
    }
    if (!empty($d["end_date"])) {
        $day = substr ( $d["end_date"], 8, 2);
        $month= substr ( $d["end_date"], 5, 2);
        $year =substr ( $d["end_date"], 0, 4);
        $d["end_date"] = mktime(0,0,0,$month, $day, $year);
    }
    else {
      $d["end_date"] = "";
    }
    
    $q = "UPDATE #__vm_product_discount SET ";
    $q .= "amount='" . $d["amount"]."',";
    $q .= "is_percent='" . $d["is_percent"]."',";
    $q .= "start_date='" . $d["start_date"]."', ";
    $q .= "end_date='" . $d["end_date"]."' ";
    $q .= "WHERE discount_id='".$d["discount_id"]."'";
    $db->query($q);
    if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Adding discount: <a onclick="switchMenu(\''.$db->last_insert_id().'_discount_id\');" title="Show/hide query">Show/hide query</a><div id="'.$db->last_insert_id().'_discount_id" class="debug">'.htmlentities($q).'</div>');
    return True;
  }

	/**
	* Controller for Deleting Records.
	*/
	function delete(&$d) {
		
		$record_id = $d["discount_id"];
		
		if( is_array( $record_id)) {
			foreach( $record_id as $record) {
				if( !$this->delete_record( $record, $d ))
					return false;
			}
			return true;
		}
		else {
			return $this->delete_record( $record_id, $d );
		}
	}
	/**
	* Deletes one Record.
	*/
	function delete_record( $record_id, &$d ) {
		global $db;
		
		if (!$this->validate_delete($record_id)) {
			$d["error"]=$this->error;
			return False;
		}
		$q = "DELETE FROM #__vm_product_discount WHERE discount_id='$record_id'";
		$db->query($q);
		
		return True;
	}
  
 
}

?>
