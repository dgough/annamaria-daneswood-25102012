<?php
/**
 * Order export class
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Export
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_class_export.php 214 2008-04-23 13:03:44Z Suami $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );
 
/**
 * Processor for order exports
 *
 * @package CSVImproved
 * @subpackage Export
 */
class OrderExport extends CsviExport {
	
	/**
	 * Start initialising some standard settings
	 *
	 * @param &$csviregistry array Global register
	 */
	function __construct(&$csviregistry) {
		$this->ExportDetails($csviregistry);
	}
	
	/** 
	 * Order export
	 *
	 * Exports manufacturer details to either csv or xml format
	 *
	 * @param &$csviregistry array Global register
	 **/
	function Start(&$csviregistry) {
		/* Set the identifier for the export filename */
		$this->type = 'ManufacturerExport';
		
		/* Open a DB connection */
		$db = $csviregistry->GetObject('database');
		
		/* Get the settings */
		$templates = new Templates($csviregistry);
		$templates->GetTemplate($csviregistry);
		
		/* Get the XML class */
		$this->GetXmlClass($csviregistry);
		
		/* See if the user wants column headers */
		if ($templates->export_type == 'xml') {
			$this->contents .= $this->xmlclass->HeaderText();
		}
		else {
			if ($templates->include_column_headers) {
				foreach ($this->required_fields as $column_header => $fieldname) {
					$this->contents .= $templates->text_enclosure.$column_header.$templates->text_enclosure.$templates->field_delimiter;
				}
				if ($templates->include_column_headers) $this->contents = substr($this->contents, 0 , -1);
				$this->contents .= chr(10);
			}
		}
		
		/* Execute the query */
		$q = "SELECT
			#__vm_orders.order_id,
			#__vm_orders.user_id,
			order_total,
			order_subtotal,
			order_tax,
			order_shipping,
			order_shipping_tax,
			coupon_discount,
			order_discount,
			order_currency,
			FROM_UNIXTIME(#__vm_orders.cdate) AS order_date,
			FROM_UNIXTIME(#__vm_orders.mdate) AS order_modified_date,
			ship_method_id,
			customer_note,
			ip_address,
			address_type,
			address_type_name,
			username,
			company,
			title,
			last_name,
			first_name,
			middle_name,
			phone_1,
			phone_2,
			fax,
			address_1,
			address_2,
			city,
			state,
			country,
			zip,
			user_email,
			extra_field_1,
			extra_field_2,
			extra_field_3,
			extra_field_4,
			extra_field_5,
			bank_account_nr,
			bank_name,
			bank_sort_code,
			bank_iban,
			bank_account_holder,
			bank_account_type,
			order_payment_expire,
			order_payment_name,
			order_payment_log,
			order_payment_trans_id,
			order_status_name,
			#__vm_order_item.product_id,
			order_item_sku AS product_sku,
			order_item_name AS product_name,
			product_quantity,
			product_item_price AS product_price,
			product_final_price,
			order_item_currency,
			product_attribute,
			mf_name AS manufacturer_name
			FROM jos_vm_orders
			LEFT JOIN #__vm_order_item
			ON #__vm_orders.order_id = #__vm_order_item.order_id
			LEFT JOIN #__vm_order_user_info
			ON #__vm_orders.order_id = #__vm_order_user_info.order_id
			LEFT JOIN #__vm_order_payment
			ON #__vm_orders.order_id = #__vm_order_payment.order_id
			LEFT JOIN #__vm_order_status
			ON #__vm_orders.order_status = #__vm_order_status.order_status_code
			LEFT JOIN  #__vm_product_mf_xref
			ON #__vm_order_item.product_id = #__vm_product_mf_xref.product_id
			LEFT JOIN #__vm_manufacturer
			ON #__vm_product_mf_xref.manufacturer_id = #__vm_manufacturer.manufacturer_id
			LEFT JOIN #__users
			ON #__users.id = #__vm_order_user_info.user_id 
			ORDER BY order_id";
		
		/* Add a limit if user wants us to */
		$q .= $this->ExportLimit($csviregistry);
		
		$db->query($q);
		while ($db->next_record()) {
			if ($templates->export_type == 'xml') $this->contents .= $this->xmlclass->NodeStart();
			foreach ($this->required_fields as $column_header => $fieldname) {
				switch ($fieldname) {
					default:
						$fieldvalue = $db->f($fieldname);
						/* Check if we have any content otherwise use the default value */
						if (strlen(trim($fieldvalue)) == 0) $fieldvalue = $this->default_values[$column_header];
						$this->AddExportField($fieldvalue, $templates, $fieldname, $column_header);
						break;
				}
			}
			if ($templates->export_type == 'xml') {
				$this->contents .= $this->xmlclass->NodeEnd().chr(13).chr(10);
			}
			else if (substr($this->contents, -1) == $templates->field_delimiter) {
				$this->contents = substr($this->contents, 0 , -1).chr(13).chr(10);
			}
			else $this->contents .= chr(13).chr(10);
		}
		
		if ($templates->export_type == 'xml') {
			$this->contents .= $this->xmlclass->FooterText();
		}
		$this->ExportCreateFile($csviregistry);
	}
}
?>
