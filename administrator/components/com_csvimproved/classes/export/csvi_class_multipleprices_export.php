<?php
/**
 * Multiple prices export class
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Export
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_class_export.php 214 2008-04-23 13:03:44Z Suami $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );
 
/**
 * Processor for multiple exports
 *
 * @package CSVImproved
 * @subpackage Export
 */
class MultiplePricesExport extends CsviExport {
	
	/**
	 * Start initialising some standard settings
	 *
	 * @param &$csviregistry array Global register
	 */
	function __construct(&$csviregistry) {
		$this->ExportDetails($csviregistry);
	}
	
	/** 
	 * Multiple prices export
	 *
	 * @param &$csviregistry object Global registry
	 */
	function Start(&$csviregistry) {
		/* Set the identifier for the export filename */
		$this->type = 'MultiplePricesExport';
		
		/* Get the settings */
		$templates = new Templates($csviregistry);
		$templates->GetTemplate($csviregistry);
		
		/* Get the XML class */
		$this->GetXmlClass(&$csviregistry);
		
		/* Check if the user wants to xml or csv */
		if ($templates->export_type == 'xml') {
			$this->contents .= $this->xmlclass->HeaderText();
		}
		else {
			/* See if the user wants column headers */
			if ($templates->include_column_headers) {
				foreach ($this->required_fields as $column_header => $fieldname) {
					$this->contents .= $templates->text_enclosure.$column_header.$templates->text_enclosure.$templates->field_delimiter;
				}
				if ($templates->include_column_headers) $this->contents = substr($this->contents, 0 , -1);
				$this->contents .= chr(10);
			}
		}
		
		/* Check if there are any selectors */
		$selectors = array();
		if ($templates->shopper_group_id > 0) {
			$selectors[] = '#__vm_product_price.shopper_group_id = '.$templates->shopper_group_id;
		}
		
		/* Open a DB connection */
		$db = $csviregistry->GetObject('database');
		
		/* Execute the query */
		$q = "SELECT #__vm_product_price.*, p.product_sku, s.shopper_group_name FROM #__vm_product_price
			LEFT JOIN #__vm_product p
			ON #__vm_product_price.product_id = p.product_id
			LEFT JOIN #__vm_shopper_group s
			ON #__vm_product_price.shopper_group_id = s.shopper_group_id";
			
		/* Check if we need to attach any selectors to the query */
		if (count($selectors) > 0 ) {
			$q .= ' WHERE ';
			foreach ($selectors as $key => $selector) {
				$q .= $selector.', ';
			}
			$q = substr($q, 0, -2).' ';
		}
		
		/* Add a limit if user wants us to */
		$q .= $this->ExportLimit($csviregistry);
		
		$db->query($q);
		while ($db->next_record()) {
			if ($templates->export_type == 'xml') $this->contents .= $this->xmlclass->NodeStart();
			foreach ($this->required_fields as $column_header => $fieldname) {
				switch ($fieldname) {
					case 'product_price':
						$this->product_price = $db->f("product_price");
						$this->ProductPrice();
						$this->product_price = sprintf("%01.2f", $this->product_price);
						if (strlen(trim($this->product_price)) == 0) $this->product_price = $this->default_values[$column_header];
						$this->AddExportField($this->product_price, $templates, $fieldname, $column_header);
						break;
					default:
						$fieldvalue = $db->f($fieldname);
						/* Check if we have any content otherwise use the default value */
						if (strlen(trim($fieldvalue)) == 0) $fieldvalue = $this->default_values[$column_header];
						$this->AddExportField($fieldvalue, $templates, $fieldname, $column_header);
						break;
				}
			}
			if ($templates->export_type == 'xml') {
				$this->contents .= $this->xmlclass->NodeEnd().chr(13).chr(10);
			}
			else if (substr($this->contents, -1) == $templates->field_delimiter) {
				$this->contents = substr($this->contents, 0 , -1).chr(13).chr(10);
			}
			else $this->contents .= chr(13).chr(10);
		}
		$this->contents = trim($this->contents);
		if ($templates->export_type == 'xml') {
			$this->contents .= $this->xmlclass->FooterText();
		}
		$this->ExportCreateFile($csviregistry);
	}
}
?>
