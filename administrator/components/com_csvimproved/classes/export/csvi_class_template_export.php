<?php
/**
 * Template export class
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Export
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_class_export.php 214 2008-04-23 13:03:44Z Suami $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );
 
/**
 * Processor for template exports
 *
 * @package CSVImproved
 * @subpackage Export
 */
class TemplateExport extends CsviExport {
	
	/**
	 * Start initialising some standard settings
	 *
	 * @param &$csviregistry array Global register
	 */
	function __construct(&$csviregistry) {
		$this->ExportDetails($csviregistry);
	}
	
	/** 
	 * Template export
	 *
	 * Exports templates to either csv or xml format
	 *
	 * @param &$csviregistry array Global register
	 **/
	function Start(&$csviregistry) {
		/* Set the identifier for the export filename */
		$this->type = 'TemplateExport';
		
		/* Open a DB connection */
		$db = $csviregistry->GetObject('database');
		
		/* Get the settings */
		$templates = new Templates($csviregistry);
		$templates->GetTemplate($csviregistry);
		
		/* Get the XML class */
		$this->GetXmlClass($csviregistry);
		
		/* See if the user wants column headers */
		if ($templates->export_type == 'xml') {
			$this->contents .= $this->xmlclass->HeaderText();
		}
		else {
			if ($templates->include_column_headers) {
				foreach ($this->required_fields as $column_header => $fieldname) {
					$this->contents .= $templates->text_enclosure.$column_header.$templates->text_enclosure.$templates->field_delimiter;
				}
				if ($templates->include_column_headers) $this->contents = substr($this->contents, 0 , -1);
				$this->contents .= chr(10);
			}
		}
		
		/* Get the template settings from the database*/
		$q = "SELECT t.*, s.shopper_group_name FROM #__csvi_templates t
			LEFT JOIN #__vm_shopper_group s
			ON t.shopper_group_id = s.shopper_group_id
			WHERE ID IN (";
		foreach ($csviregistry->GetArray('exporttemplatelist') as $key => $value) {
			$q .= $value.",";
		}
		$q = substr($q, 0 , -1).") ORDER BY t.template_name ";
		
		/* Add a limit if user wants us to */
		$q .= $this->ExportLimit($csviregistry);
		
		$db->query($q);
		while ($db->next_record()) {
			if ($templates->export_type == 'xml') $this->contents .= $this->xmlclass->NodeStart();
			foreach ($this->required_fields as $column_header => $fieldname) {
				$fieldvalue = $db->f($fieldname);
				/* Check if we have any content otherwise use the default value */
				if (strlen(trim($fieldvalue)) == 0) $fieldvalue = $this->default_values[$column_header];
				$this->AddExportField($fieldvalue, $templates, $fieldname, $column_header);
			}
			if ($templates->export_type == 'xml') {
				$this->contents .= $this->xmlclass->NodeEnd().chr(13).chr(10);
			}
			else if (substr($this->contents, -1) == $templates->field_delimiter) {
				$this->contents = substr($this->contents, 0 , -1).chr(13).chr(10);
			}
			else $this->contents .= chr(13).chr(10);
		}
		$this->contents = trim($this->contents);
		if ($templates->export_type == 'xml') {
			$this->contents .= $this->xmlclass->FooterText();
		}
		$this->ExportCreateFile($csviregistry);
	}
}
?>
