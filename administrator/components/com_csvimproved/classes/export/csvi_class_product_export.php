<?php
/**
 * Product export class
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Export
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_class_export.php 214 2008-04-23 13:03:44Z Suami $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );
 
/**
 * Processor for product exports
 *
 * @package CSVImproved
 * @subpackage Export
 */
class ProductExport extends CsviExport {
	
	/**
	 * Start initialising some standard settings
	 *
	 * @param &$csviregistry array Global register
	 */
	function __construct(&$csviregistry) {
		$this->GetVmItemId();
		$this->ExportDetails($csviregistry);
	}
	
	/** 
	 * Product export
	 *
	 * Exports product data to either csv or xml format
	 *
	 * @param &$csviregistry array Global register
	 **/
	function Start(&$csviregistry) {
		/* Get the logger class */
		$csvilog = $csviregistry->GetObject('logger');
		
		/* Set the identifier for the export filename */
		$this->type = 'ProductExport';
		
		/* Get the export settings from the template */
		$templates = new Templates($csviregistry);
		$templates->GetTemplate($csviregistry);
		
		/* Get the XML class */
		$this->GetXmlClass($csviregistry);
		
		/* Open a DB connection */
		$db = $csviregistry->GetObject('database');
		
		/* Check if the user is using attributes or attribute values
		   If so, we need to add the product_parent_sku field if it is not yet set */
		if (array_key_exists("attributes", $this->required_fields) && !isset($this->required_fields["product_parent_sku"])) 
			$this->required_fields["product_parent_sku"] = 0;
		else if (array_key_exists("attribute_values", $this->required_fields) && !isset($this->required_fields["product_parent_sku"])) 
			$this->required_fields["product_parent_sku"] = 0;
		
		/* See if the user wants column headers */
		if ($templates->export_type == 'xml') {
			$this->contents .= $this->xmlclass->HeaderText();
		}
		else {
			if ($templates->include_column_headers) {
				foreach ($this->required_fields as $column_header => $fieldname) {
					$this->contents .= $templates->text_enclosure.$column_header.$templates->text_enclosure.$templates->field_delimiter;
				}
				if ($templates->include_column_headers) $this->contents = substr($this->contents, 0 , -1);
				$this->contents .= chr(10);
			}
		}
		
		/** Export SQL Query
		* Get all products - including items
		* as well as products without a price
		**/
		$q = 'SELECT 
			#__vm_product.product_id as main_product_id,
			#__vm_product.*, #__vm_product_price.*, #__vm_shopper_group.shopper_group_name,
			#__vm_product_discount.amount as product_discount,
			#__vm_product_discount.start_date as product_discount_date_start,
			#__vm_product_discount.end_date as product_discount_date_end,
			#__vm_product_discount.is_percent as product_discount_is_percent,
			#__vm_manufacturer.mf_name as manufacturer_name,
			#__vm_manufacturer.manufacturer_id as manufacturer_id,
			#__vm_tax_rate.tax_rate,
			#__vm_product_attribute.attribute_name,
			#__vm_product_attribute.attribute_value
			FROM #__vm_product
			LEFT JOIN #__vm_product_price
			ON #__vm_product.product_id = #__vm_product_price.product_id
			LEFT JOIN #__vm_product_mf_xref
			ON #__vm_product.product_id = #__vm_product_mf_xref.product_id
			LEFT JOIN #__vm_shopper_group
			ON #__vm_product_price.shopper_group_id = #__vm_shopper_group.shopper_group_id
			LEFT JOIN #__vm_product_discount
			ON #__vm_product.product_discount_id = #__vm_product_discount.discount_id
			LEFT JOIN #__vm_manufacturer
			ON #__vm_product_mf_xref.manufacturer_id = #__vm_manufacturer.manufacturer_id
			LEFT JOIN #__vm_tax_rate
			ON #__vm_product.product_tax_id = #__vm_tax_rate.tax_rate_id 
			LEFT JOIN #__vm_product_attribute
			ON #__vm_product.product_id = #__vm_product_attribute.product_id
			';
			
			
		/* Check if there are any selectors */
		$selectors = array();
		/* Filter by shopper group id */
		if ($templates->shopper_group_id > 0) {                
			$selectors[] = '#__vm_product_price.shopper_group_id = '.$templates->shopper_group_id;
		}
		
		/* Filter by published state */
		if (!empty($templates->product_publish)) {
			$selectors[] = "#__vm_product.product_publish = '".$templates->product_publish."'";
		}
		
		/* Filter by manufacturer */
		if (!empty($templates->manufacturer) && ($templates->manufacturer > 0)) {
			$selectors[] = "#__vm_manufacturer.manufacturer_id IN (".$templates->manufacturer.")";
		}
		
		/* Check if we need to attach any selectors to the query */
		if (count($selectors) > 0 ) {
			$q .= ' WHERE ';
			foreach ($selectors as $key => $selector) {
				$q .= $selector.",\n";
				if ((count($selectors)-1) > $key) {
					$q .= "AND ";
				}
			}
			$q = substr(trim($q), 0, -1)." \n";
		}
		/** 
		 * Products are grouped together if there is no price field exported. Otherwise 
		 * the export will show duplicate products.
		 */
		if (!in_array('product_price', $this->required_fields) &&
			!in_array('price_with_discount', $this->required_fields) &&
			!in_array('price_with_tax', $this->required_fields)) {
			$q .= "GROUP BY #__vm_product.product_sku\n";
		}
		$q .= 'ORDER BY #__vm_product.product_sku ';
		
		$q .= $this->ExportLimit($csviregistry);
		
		/* Execute the query */
		$db->query($q);
		while ($db->next_record()) {
			if ($templates->export_type == 'xml') $this->contents .= $this->xmlclass->NodeStart();
			foreach ($this->required_fields as $column_header => $fieldname) {
				$exportdb = $csviregistry->CloneObj($db);
				switch ($fieldname) {
					case 'category_path':
						$this->category_path = trim($this->GetCategoryPath($db->f("main_product_id")));
						if (strlen(trim($this->category_path)) == 0) $this->category_path = $this->default_values[$column_header];
						$this->AddExportField($this->category_path, $templates, $fieldname, $column_header);
						break;
					case 'attributes':
						if( $db->f("product_parent_id") == 0 ) {
							$this->attributes = $this->export_sku = "";
							$exportdb->query("SELECT attribute_name, attribute_list FROM #__vm_product_attribute_sku WHERE product_id = '".$db->f("main_product_id")."'");
							if( $exportdb->next_record() ) {
								$has_attributes = true;
								$exportdb->reset();
								while( $exportdb->next_record() ) {
									$this->attributes .= $exportdb->f("attribute_name"). "::". $exportdb->f("attribute_list");
									if (!$exportdb->is_last_record()) $this->attributes .= "|";
								}
							}
						}
						else $this->attributes = $this->default_values[$column_header];
						$this->AddExportField(trim($this->attributes), $templates, $fieldname, $column_header);
						break;
					case 'attribute_values':
						if( $db->f("product_parent_id") != 0 || strtolower($db->f("attribute_name")) == "download") {
							$this->attribute_values = $this->export_sku = "";
							$exportdb->query( "SELECT attribute_name, attribute_value FROM #__vm_product_attribute WHERE product_id = '".$db->f("main_product_id")."'" );
							if( $exportdb->next_record() ) {
								$exportdb->reset();
								while( $exportdb->next_record() ) {
									$this->attribute_values .= $exportdb->f("attribute_name")."::". $exportdb->f("attribute_value");
									if (!$exportdb->is_last_record()) $this->attribute_values .= "|";
								}
							}
						}
						else $this->attribute_values = $this->default_values[$column_header];
						$this->AddExportField(trim($this->attribute_values), $templates, $fieldname, $column_header);
						break;
					case 'product_parent_sku':
						$exportdb->query( "SELECT product_sku FROM #__vm_product WHERE product_id='".$db->f("product_parent_id")."'" );
						$exportdb->next_record();
						$this->AddExportField($exportdb->f('product_sku'), $templates, $fieldname, $column_header);
						break;
					case  'product_available_date':
						$this->product_available_date = trim($db->f('product_available_date'));
						if (strlen($this->product_available_date) == 0 || $this->product_available_date == 0) {
							/* Check if we have a default value */
							if (strlen(trim($this->default_values[$column_header])) > 0) {
								$this->product_available_date = $this->default_values[$column_header];
							}
							else $this->product_available_date = '';
						}
						else $this->product_available_date = date("d/m/Y", $this->product_available_date);
						$this->AddExportField($this->product_available_date, $templates, $fieldname, $column_header);
						break;
					case 'related_products':
						$this->related_products = "";
						$exportdb->query("SELECT related_products FROM #__vm_product_relations WHERE product_id='".$db->f("main_product_id")."'" );
						$exportdb->next_record();
						$products = explode("|", $exportdb->f("related_products"));
						$q = "SELECT product_sku FROM #__vm_product WHERE product_id in (";
						foreach ($products as $id => $productid) {
							$q .= $productid.',';
						}
						$q = substr($q, 0 , -1).")";
						$exportdb->query($q);
						while( $exportdb->next_record() ) {
							$this->related_products .= $exportdb->f("product_sku");
							if (!$exportdb->is_last_record()) $this->related_products .= "|";
						}
						if (strlen(trim($this->related_products)) == 0) $this->related_products = $this->default_values[$column_header];
						$this->AddExportField($this->related_products, $templates, $fieldname, $column_header);
						break;
					case 'product_discount':
						$product_discount = trim($db->f("product_discount"));
						if ($db->f("product_discount_is_percent")) $product_discount .= '%';
						else $product_discount = sprintf("%01.2f", $product_discount);
						if (strlen(trim($product_discount)) == 0) $product_discount = $this->default_values[$column_header];
						$this->AddExportField($product_discount, $templates, $fieldname, $column_header);
						break;
					case 'product_price':
						$this->product_price =  $db->f("product_price");
						$this->ProductPrice();
						$this->product_price = sprintf("%01.2f", $this->product_price);
						if (strlen(trim($this->product_price)) == 0) $this->product_price = $this->default_values[$column_header];
						$this->AddExportField($this->product_price, $templates, $fieldname, $column_header);
						break;
					case 'product_url':
						/* Check if there is already a product URL */
						if (strlen(trim($db->f('product_url'))) == 0) {
							/* There is no product URL, create it */
							/* Get the flypage */
							$flypage = $this->GetFlypage($db->f("main_product_id"));
							
							/* Get the category id */
							/* Check to see if we have a child product */
							if ($db->f('product_parent_id') > 0) $category_id = $this->GetCategoryId($db->f("product_parent_id"));
							else $category_id = $this->GetCategoryId($db->f("main_product_id"));
							
							if (strlen($category_id) > 0) {
								$product_url = $csviregistry->GetVar('live_site').'index.php?option=com_virtuemart&Itemid='.$this->vmitemid.'&page=shop.product_details&flypage='.$flypage.'&product_id='.$db->f("main_product_id").'&category_id='.$category_id;
							}
							else $product_url = "";
						}
						/* There is a product URL, use it */
						else $product_url = $db->f('product_url');
						
						/* Add the suffix */
						$product_url .= $templates->producturl_suffix;
						
						/* Check for https, replace with http. https has unnecessary overhead */
						if (substr($product_url, 0, 5) == 'https') $product_url = 'http'.substr($product_url, 5);
						$this->AddExportField($product_url, $templates, $fieldname, $column_header, true);
						break;
					case 'product_full_image':
						$product_full_image = str_replace($templates->file_location, '', $db->f('product_full_image'));
						$this->AddExportField($product_full_image, $templates, $fieldname, $column_header);
						break;
					case 'product_thumb_image':
						$product_thumb_image = str_replace($templates->file_location, '', $db->f('product_full_image'));
						$this->AddExportField($product_thumb_image, $templates, $fieldname, $column_header);
						break;
					case 'picture_url':
						/* Check if there is already a product full image */
						if (strlen(trim($db->f('product_full_image'))) > 0) {
							/* Create picture url */
							if (substr( $db->f('product_full_image'), 0, 4) == 'http') $picture_url = $db->f('product_full_image'); 
							else $picture_url = str_ireplace($csviregistry->GetVar('absolute_path').'/', $csviregistry->GetVar('live_site'), $templates->file_location).$db->f("product_full_image");
						}
						/* There is no product full image, use default value */
						else $picture_url = $this->default_values[$column_header];
						$this->AddExportField($picture_url, $templates, $fieldname, $column_header);
						break;
					case 'price_with_tax':
						$price_with_tax = sprintf("%01.2f", ($db->f('product_price')/100)*(100+($db->f('tax_rate')*100)));
						/* Check if we have any content otherwise use the default value */
						if (strlen(trim($price_with_tax)) == 0) $price_with_tax = $this->default_values[$column_header];
						$this->AddExportField($price_with_tax, $templates, $fieldname, $column_header);
						break;
					case 'price_with_discount':
						/* First include the tax */
						$price_with_discount = sprintf("%01.2f", ($db->f('product_price')/100)*(100+($db->f('tax_rate')*100)));
						/**
						 * Apply the discount
						 * 0 = value
						 * 1 = percentage
						 */
						switch($db->f('product_discount_is_percent')) { 
							case 0: $price_with_discount -= trim($db->f("product_discount")); break; 
							case 1: $price_with_discount *= (100 - trim($db->f("product_discount")))/100; break; 
						}
						/* Check if we have any content otherwise use the default value */
						if (strlen(trim($price_with_discount)) == 0) $price_with_discount = $this->default_values[$column_header];
						$this->AddExportField($price_with_discount, $templates, $fieldname, $column_header);
						break;
					case 'product_packaging':
						$product_packaging = $db->f("product_packaging") & 0xFFFF;
						$this->AddExportField($product_packaging, $templates, $fieldname, $column_header);
						break;
					case 'product_box':
						$product_box = $db->f("product_packaging")>>16&0xFFFF;
						$this->AddExportField($product_box, $templates, $fieldname, $column_header);
						break;
					case 'product_name':
						$fieldvalue = $db->f($fieldname);
						/* Check if we have any content otherwise use the default value */
						if (strlen(trim($fieldvalue)) == 0) $fieldvalue = $this->default_values[$column_header];
						$this->AddExportField($fieldvalue, $templates, $fieldname, $column_header, true);
						break;
					case 'product_s_desc':
						$fieldvalue = $db->f($fieldname);
						/* Check if we have any content otherwise use the default value */
						if (strlen(trim($fieldvalue)) == 0) $fieldvalue = $this->default_values[$column_header];
						$this->AddExportField($fieldvalue, $templates, $fieldname, $column_header, true);
						break;
					case 'product_desc':
						$fieldvalue = $db->f($fieldname);
						/* Check if the field is empty */
						if (strlen(trim($fieldvalue)) == 0) {
							/* Check if we are doing a Google Base export */
							if ($templates->export_type == 'xml' && $templates->export_site == 'froogle') {
								/* Check if the short description has data */
								if (strlen(trim($db->f('product_s_desc'))) == 0) {
									/* Let's use the product name */
									$fieldvalue = $db->f('product_name');
								}
								else $fieldvalue = $db->f('product_s_desc');
							}
							else {
								/* Check if we have any content otherwise use the default value */
								$fieldvalue = $this->default_values[$column_header];
							}
							
						}
						$this->AddExportField($fieldvalue, $templates, $fieldname, $column_header, true);
						break;
					case 'product_full_image':
						$fieldvalue = $db->f($fieldname);
						/* Check if we have any content otherwise use the default value */
						if (strlen(trim($fieldvalue)) == 0) $fieldvalue = $this->default_values[$column_header];
						$this->AddExportField($fieldvalue, $templates, $fieldname, $column_header, true);
						break;
					case 'product_thumb_image':
						$fieldvalue = $db->f($fieldname);
						/* Check if we have any content otherwise use the default value */
						if (strlen(trim($fieldvalue)) == 0) $fieldvalue = $this->default_values[$column_header];
						$this->AddExportField($fieldvalue, $templates, $fieldname, $column_header, true);
						break;
					case 'picture_url':
						$fieldvalue = $db->f($fieldname);
						/* Check if we have any content otherwise use the default value */
						if (strlen(trim($fieldvalue)) == 0) $fieldvalue = $this->default_values[$column_header];
						$this->AddExportField($fieldvalue, $templates, $fieldname, $column_header, true);
						break;
					case 'product_id':
						$fieldvalue = $db->f('main_product_id');
						/* Check if we have any content otherwise use the default value */
						if (strlen(trim($fieldvalue)) == 0) $fieldvalue = $this->default_values[$column_header];
						$this->AddExportField($fieldvalue, $templates, $fieldname, $column_header, true);
						break;
					default:
						$fieldvalue = $db->f($fieldname);
						/* Check if we have any content otherwise use the default value */
						if (strlen(trim($fieldvalue)) == 0) $fieldvalue = $this->default_values[$column_header];
						$this->AddExportField($fieldvalue, $templates, $fieldname, $column_header);
						break;
				}
			}
			if ($templates->export_type == 'xml') {
				$this->contents .= $this->xmlclass->NodeEnd().chr(13).chr(10);
			}
			else if (substr($this->contents, -1) == $templates->field_delimiter) {
				$this->contents = substr($this->contents, 0 , -1).chr(13).chr(10);
			}
			else $this->contents .= chr(13).chr(10);
		}
		$this->contents = trim($this->contents);
		if ($templates->export_type == 'xml') {
			$this->contents .= $this->xmlclass->FooterText();
		}
		$this->ExportCreateFile($csviregistry);
	}
}
?>
