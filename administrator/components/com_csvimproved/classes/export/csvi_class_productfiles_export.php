<?php
/**
 * Product Files export class
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Export
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_class_export.php 214 2008-04-23 13:03:44Z Suami $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );
 
/**
 * Processor for product files exports
 *
 * @package CSVImproved
 * @subpackage Export
 */
class ProductFilesExport extends CsviExport {
	
	/**
	 * Start initialising some standard settings
	 *
	 * @param &$csviregistry array Global register
	 */
	function __construct(&$csviregistry) {
		$this->ExportDetails($csviregistry);
	}
	
	/** 
	 * Product Files export
	 *
	 * Exports product files to either csv or xml format
	 *
	 * @param &$csviregistry array Global register
	 **/
	function Start(&$csviregistry) {
		/* Set the identifier for the export filename */
		$this->type = 'ProductFilesExport';
		
		/* Get the settings */
		$templates = new Templates($csviregistry);
		$templates->GetTemplate($csviregistry);
		
		/* Get the XML class */
		$this->GetXmlClass($csviregistry);
		
		/* See if the user wants column headers */
		if ($templates->export_type == 'xml') {
			$this->contents .= $this->xmlclass->HeaderText();
		}
		else {
			if ($templates->include_column_headers) {
				foreach ($this->required_fields as $column_header => $fieldname) {
					$this->contents .= $templates->text_enclosure.$column_header.$templates->text_enclosure.$templates->field_delimiter;
				}
				if ($templates->include_column_headers) $this->contents = substr($this->contents, 0 , -1);
				$this->contents .= chr(10);
			}
		}
		
		// Open a DB connection
		$db = $csviregistry->GetObject('database');
		
		// Execute the query
		$q = "SELECT j.*, p.product_sku FROM #__vm_product_files j
			LEFT JOIN #__vm_product p
			ON j.file_product_id = p.product_id ";
		
		/* Add a limit if user wants us to */
		$q .= $this->ExportLimit($csviregistry);
		
		$db->query($q);
		while ($db->next_record()) {
			if ($templates->export_type == 'xml') $this->contents .= $this->xmlclass->NodeStart();
			foreach ($this->required_fields as $column_header => $fieldname) {
				switch ($fieldname) {
					case "product_files_file_published":
						if ($db->f("file_published") == 1) $fieldvalue = 'Y';
						else $fieldvalue = 'N';
						$this->AddExportField($fieldvalue, $templates, $fieldname, $column_header);
						break;
					case "product_files_file_name":
						$fieldvalue = $db->f("file_name");
						$this->AddExportField($fieldvalue, $templates, $fieldname, $column_header, true);
						break;
					case "product_files_file_title":
						$fieldvalue = $db->f("file_title");
						$this->AddExportField($fieldvalue, $templates, $fieldname, $column_header, true);
						break;
					case "product_files_file_description":
						$fieldvalue = $db->f("file_description");
						$this->AddExportField($fieldvalue, $templates, $fieldname, $column_header, true);
						break;
					case "product_files_file_url":
						$fieldvalue = $db->f("file_url");
						$this->AddExportField($fieldvalue, $templates, $fieldname, $column_header, true);
						break;
					default:
						$fieldvalue = $db->f($fieldname);
						$this->AddExportField($fieldvalue, $templates, $fieldname, $column_header);
						break;
				}
			}
			if ($templates->export_type == 'xml') {
				$this->contents .= $this->xmlclass->NodeEnd().chr(13).chr(10);
			}
			else if (substr($this->contents, -1) == $templates->field_delimiter) {
				$this->contents = substr($this->contents, 0 , -1).chr(13).chr(10);
			}
			else $this->contents .= chr(13).chr(10);
		}
		$this->contents = trim($this->contents);
		if ($templates->export_type == 'xml') {
			$this->contents .= $this->xmlclass->FooterText();
		}
		$this->ExportCreateFile($csviregistry);
	}
}
?>
