<?php
/**
 * Product files
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Import
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_class_product_files.php 143 2008-03-15 09:38:09Z Roland $
 */
 
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * Main constructor for product files import
 *
 * Product files can be any extra file added to a product
 * Image files are automatically resized
 *
 * @package CSVImproved
 * @subpackage Import
 */
class product_files {
	
	/** @var integer */
	var $product_files_product_sku = null;
	/** @var integer */
	var $product_files_file_id = 0;
	/** @var integer */
	var $product_files_file_product_id = 0;
	/** @var string */
	var $product_files_file_name = "";
	/** @var string */
	var $product_files_file_title = "";
	/** @var string */
	var $product_files_file_description = "";
	/** @var string */
	var $product_files_file_extension = "";
	/** @var string */
	var $product_files_file_mimetype = "";
	/** @var string */
	var $product_files_file_url = "";
	/** @var bool */
	var $product_files_file_published = 1;
	/** @var bool */
	var $product_files_file_is_image = 1;
	/** @var integer */
	var $product_files_file_image_height = 0;
	/** @var integer */
	var $product_files_file_image_width = 0;
	/** @var integer */
	var $product_files_file_image_thumb_height = 0;
	/** @var integer */
	var $product_files_file_image_thumb_width = 0;
	/** @var bool */
	var $file_exists = false;
	
	function product_files(&$csviregistry) {
		/* Get the file_product_id */
		$this->get_product_id($csviregistry); 
		
		$csv_fields = $csviregistry->GetArray('csv_fields');
		
		/* Get the statistics */
		$csvilog = $csviregistry->GetObject('logger');
		
		/* Validate CSV Input */
		if (count($csv_fields) != count($csviregistry->GetArray('product_data'))) {
			$csvilog->AddStats('incorrect', "Line ".$csviregistry->currentline.": <strong>Incorrect column count</strong><br />Configration: ".count($csv_fields)." fields<br />File: ".count($csviregistry->GetArray('product_data'))." fields");
			return false;
		}                                
		else {
			/* Call the functions in this order */
			$this->get_product_files_file_name($csviregistry);
			if ($this->file_exists) {
				$this->get_product_files_file_title($csviregistry);
				$this->get_product_files_file_description($csviregistry);
				$this->get_product_files_file_url($csviregistry);
				$this->get_product_files_file_published($csviregistry);
				$this->get_product_files_file_extension();
				$this->get_file_details($csviregistry);
			}
			else {
				if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Line '.$csviregistry->currentline.': File '.$this->product_files_file_name.' does not exist. Nothing to do.');
				$csvilog->AddStats('nofiles', 'Line '.$csviregistry->currentline.': File missing: '.basename($this->product_files_file_name));
			}
		}
	}
	
	function get_product_id(&$csviregistry) {
		$db = $csviregistry->GetObject('database');
		$csv_fields = $csviregistry->GetArray('csv_fields');
		$product_data = $csviregistry->GetArray('product_data');
		$csvilog = $csviregistry->GetObject('logger');
		$this->product_files_product_sku = $product_data[$csv_fields["product_sku"]["order"]];
		if (isset($csv_fields["product_sku"])) {
			$q = "SELECT product_id FROM #__vm_product WHERE product_sku = '".$this->product_files_product_sku."'";
			$rnd = rand();
			if ($csviregistry->GetVar('debug')) $csvilog->AddMessage('debug', 'Looking for product ID that belongs to the file: <a onclick="switchMenu(\''.$this->product_files_product_sku.$rnd.'_exist_product_id\');" title="Show/hide query">Show/hide query</a><div id="'.$this->product_files_product_sku.$rnd.'_exist_product_id" class="debug">'.htmlentities($q).'</div>');
			$db->query($q);
			$db->next_record();
			if (isset($db->record[0])) $this->product_files_file_product_id = $db->f("product_id");
			else $this->product_files_file_product_id = false;
		}
		else $this->product_files_file_product_id = false;
	}
	
	/**
	 * Get the products filename
	 *
	 * @see $file_exists
	 * @param &$csviregistry Global register
	 */
	function get_product_files_file_name(&$csviregistry) {
		$csvilog = $csviregistry->GetObject('logger');
		$templates = $csviregistry->GetObject('templates');
		$this->ValidateCSVInput($csviregistry, "product_files_file_name");
		/** @internal Create the product files filename */
		$this->product_files_file_name = $templates->file_location.$this->product_files_file_name;
		
		/** @internal Check if the file is present */
		if ($templates->collect_debug_info) $csvilog->AddMessage('debug', 'Check if file exists<br />File: '.$this->product_files_file_name);
		if (!file_exists($this->product_files_file_name)) $this->file_exists = false;
		else $this->file_exists = true;
	}
	
	function get_product_files_file_title(&$csviregistry) {
		$this->ValidateCSVInput($csviregistry, "product_files_file_title");
	}
	
	function get_product_files_file_description(&$csviregistry) {
		$this->ValidateCSVInput($csviregistry, "product_files_file_description");
	}
	
	function get_product_files_file_url(&$csviregistry) {
		$this->ValidateCSVInput($csviregistry, "product_files_file_url");
		if (empty($this->product_files_file_url)) $this->product_files_file_url = $csviregistry->GetVar('live_site').'components/com_virtuemart/shop_image/product/'.basename($this->product_files_file_name);
	}
	
	function get_product_files_file_published(&$csviregistry) {
		$this->ValidateCSVInput($csviregistry, "product_files_file_published");
		if (strtoupper($this->product_files_file_published) == 'Y') $this->product_files_file_published = 1;
		else $this->product_files_file_published = 0;
	}
	
	/**
	 * Checks the field for existing value, if not set the default value if allowed
	 */
	function ValidateCsvInput(&$csviregistry, $fieldname) {
		$csv_fields = $csviregistry->GetArray('csv_fields');
		$product_data = $csviregistry->GetArray('product_data');
		$skip_default_value = $csviregistry->GetVar('skip_default_value');
		/* Check if the columns match */
		if (count($csv_fields) != count($product_data)) return false;
		else {
			if (isset($csv_fields[$fieldname])) {
				/* Check if the field has a value */
				if (strlen($product_data[$csv_fields[$fieldname]["order"]]) > 0) {
					$this->$fieldname = trim($product_data[$csv_fields[$fieldname]["order"]]);
				}
				/* Field has no value, check if we can use default value*/
				else if (!$skip_default_value) {
					$this->$fieldname = $csv_fields[$fieldname]["default_value"];
				}
			}
		}
		return true;
	}
	
	/**
	 * Get the file extension of the file being handled
	 */
	function get_product_files_file_extension() {
		$this->product_files_file_extension = pathinfo($this->product_files_file_name, PATHINFO_EXTENSION);
	}
	
	/**
	 * Get the file details
	 *
	 * Retrieve the file details. This is also where the thumbnail is generated
	 *
	 * @see Img2Thumb()
	 * @param &$csviregistry array Global register
	 */
	function get_file_details(&$csviregistry) {
		$templates = $csviregistry->GetObject('templates');
		$filepath = $templates->file_location;
		$thumb_img_width = $templates->thumb_width;
		$thumb_img_height = $templates->thumb_height;
		require_once($csviregistry->GetVar('class_path')."csvi_class_mime_type_detect.php");
		$mime_type_detect = new MimeTypeDetect();
		
		/* First get the size details of the picture */
		if (file_exists($this->product_files_file_name) && $file_details = getimagesize($this->product_files_file_name)) {
			$this->product_files_file_mimetype = $file_details["mime"];
			$this->product_files_file_image_width = $file_details[0];
			$this->product_files_file_image_height = $file_details[1];
			$this->product_files_file_is_image = 1;
			$this->product_files_mime_type = $mime_type_detect->FindMimeType($this->product_files_file_name);
			
			/* Resize the image */
			require_once($csviregistry->GetVar('class_path')."csvi_class_image_converter.php");
			$fileout = dirname($this->product_files_file_name).'/resized/'.basename($this->product_files_file_name, ".".$this->product_files_file_extension)."_".$thumb_img_width."x".$thumb_img_height.".".$this->product_files_file_extension;
			$newxsize = $thumb_img_width;
			$newysize = $thumb_img_height;
			$maxsize = 0;
	
			$bgred = $bggreen = $bgblue = 255;
			/* We need to resize the image and Save the new one (all done in the constructor) */
			new Img2Thumb($this->product_files_file_name,$newxsize,$newysize,$fileout,$maxsize,$bgred,$bggreen,$bgblue);
			
			/* Get the details of the thumb image */
			if (file_exists($fileout)) {
				if ($file_details_thumb = getimagesize($fileout)) {
					$this->product_files_file_image_thumb_width = $file_details_thumb[0];
					$this->product_files_file_image_thumb_height = $file_details_thumb[1];
				}
			}
		}
		else {
			$this->product_files_file_is_image = 0;
			$this->product_files_file_mimetype = $mime_type_detect->FindMimeType($this->product_files_file_name);
		}
	}
	
	function AddProductFile(&$csviregistry) {
		$db = $csviregistry->GetObject('database');
		$csvilog = $csviregistry->GetObject('logger');
		$templates = $csviregistry->GetObject('templates');
		
		// Check if file already exists
		$q_exists = "SELECT COUNT(file_id) AS total FROM #__vm_product_files 
				WHERE file_product_id = ".$this->product_files_file_product_id."
				AND file_name = '".$this->product_files_file_name."'";
		$db->query($q_exists);
		if ($db->error) {
			if ($templates->collect_debug_info) $csvilog->AddMessage('debug', '<span style="font-size: 14px; color: #FF0000;">SQL Error</span>: <a onclick="switchMenu(\''.$this->product_files_file_product_id.'_product_files_check_error\');" title="Show/hide error">Show/hide error</a><div id="'.$this->product_files_file_product_id.'_product_files_check_error" style="display: none; border: 1px solid #000000; padding: 5px;">'.mysql_error().'<br />'.$q_exists.'</div>');
		}
		else {
			if ($db->f("total") > 0) {
				$q =  "UPDATE #__vm_product_files 
					SET file_product_id = ".$this->product_files_file_product_id.", 
					file_name = '".$this->product_files_file_name."', 
					file_title = '".$this->product_files_file_title."', 
					file_description = '".$this->product_files_file_description."', 
					file_extension = '".$this->product_files_file_extension."', 
					file_mimetype = '".$this->product_files_file_mimetype."', 
					file_url = '".$this->product_files_file_url."', 
					file_published = '".$this->product_files_file_published."', 
					file_is_image = ".$this->product_files_file_is_image.", 
					file_image_height = ".$this->product_files_file_image_height.", 
					file_image_width = ".$this->product_files_file_image_width.", 
					file_image_thumb_height = ".$this->product_files_file_image_thumb_height.", 
					file_image_thumb_width = ".$this->product_files_file_image_thumb_width." 
					WHERE file_product_id = ".$this->product_files_file_product_id." 
					AND file_name = '".$this->product_files_file_name."'";
					$type = 'update';
					if ($templates->collect_debug_info) $csvilog->AddMessage('debug', 'Updating existing product file: <a onclick="switchMenu(\''.$this->product_files_product_sku.'_update_file\');" title="Show/hide query">Show/hide query</a><div id="'.$this->product_files_product_sku.'_update_file" class="debug">'.htmlentities($q).'</div>');
			}
			else {
				$q =  "INSERT INTO #__vm_product_files
					(file_product_id, file_name, file_title, file_description, file_extension, file_mimetype, file_url, file_published,
					file_is_image, file_image_height, file_image_width, file_image_thumb_height, file_image_thumb_width)
					VALUES (".$this->product_files_file_product_id.", '".$this->product_files_file_name."', 
					'".$this->product_files_file_title."', '".$this->product_files_file_description."', 
					'".$this->product_files_file_extension."', '".$this->product_files_file_mimetype."', 
					'".$this->product_files_file_url."', '".$this->product_files_file_published."', ".$this->product_files_file_is_image.", 
					".$this->product_files_file_image_height.", ".$this->product_files_file_image_width.", 
					".$this->product_files_file_image_thumb_height.", ".$this->product_files_file_image_thumb_width.")";
					$type = 'add';
					if ($templates->collect_debug_info) $csvilog->AddMessage('debug', 'Add a new product file: <a onclick="switchMenu(\''.$this->product_files_product_sku.'_add_file\');" title="Show/hide query">Show/hide query</a><div id="'.$this->product_files_product_sku.'_add_file" class="debug">'.htmlentities($q).'</div>');
			}
			$db->query($q);
			if ($db->error) {
				if ($templates->collect_debug_info) $csvilog->AddMessage('debug', '<span style="font-size: 14px; color: #FF0000;">SQL Error</span>: <a onclick="switchMenu(\''.$ps_csv->product_details->product_sku.'_product_files_error\');" title="Show/hide error">Show/hide error</a><div id="'.$ps_csv->product_details->product_sku.'_product_files_error" style="display: none; border: 1px solid #000000; padding: 5px;">'.mysql_error().'</div>');
				return false;
			}
			else return array($type, true);
		}
	}
}
?>
