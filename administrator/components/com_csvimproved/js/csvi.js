/**
 * CSV Improved JavaScript
 *
 * CSV Improved
 * Copyright (C) 2006 - 2008 Roland Dalmulder
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSV Improved
 * @version $Id: $
 */

/**
 * Change the display of a block
 */
function expand_layer(sid,show) {
	var sbox = document.getElementById(sid);
	var sbox_style = sbox.style;
	if (show===true) {
		sbox_style.display='block';
	}
	else if (show===false) {
		sbox_style.display='none';
	}
	else {
		if ((sbox_style.display=='none')||(sbox_style.display=='')) {
			sbox_style.display='block';
		}
		else {
			sbox_style.display='none';
		}
	}
	return false;
}
/**
 * Change the colour of a specified element
 */
function ChangeClass(id, newClass) {
	identity=document.getElementById(id);
	identity.className=newClass;
}

function CheckFieldOrder(from) {
	if (from.value == 0) {
		alert('Field value order should be greater than 0.');
		// fieldname = 'field[0>][_ordering]';
		// document.adminForm.fieldname.focus();
		document.adminForm.elements['field\[0\>\]\[_order\]'].focus();
		return false;
	}
	else return true;
}

function switchMenu(obj) {
	var el = document.getElementById(obj);
	if ( el.style.display == 'none' || el.style.display == '') {
		el.style.display = 'block';
	}
	else {
		el.style.display = 'none';
	}
}

var xmlHttp

function ShowTemplateDetails(templateid, rpc_call, target, csvi_type) {
	RpcCall("templateid="+templateid.value+"&rpc_call="+rpc_call+"&csvi_type="+csvi_type, target);
}

function TemplateConfigurator(form, target) {
	poststr=getFormValues(document.getElementById(form));
	RpcCall(poststr+"&rpc_call=templateconfigurator", target);
}

function AddTemplateField(target) {
	var fieldorder = document.getElementById('newfield_fieldorder');
	var fieldname = document.getElementById('newfieldname');
	var columnheader = document.getElementById('newfield_columnheader');
	var defaultvalue = document.getElementById('newfield_defaultvalue');
	var poststr = 'field_order='+fieldorder.firstChild.value;
	poststr += '&field_name='+fieldname.options[fieldname.selectedIndex].value;
	if (columnheader != null) poststr += '&column_header='+columnheader.firstChild.value;
	poststr += '&default_value='+defaultvalue.firstChild.value;
	poststr += '&templateid='+document.adminForm.templateid.value;
	poststr += '&limitstart='+document.adminForm.limitstart.value;
	poststr += '&task=saveconfigfield';
	poststr += '&setpublished=1';
	RpcCall(poststr+"&rpc_call=addtemplatefield", target);
}

function DeleteTemplateField(fieldid, trid, target) {
	poststr = 'field_id='+fieldid;
	poststr += '&templateid='+document.adminForm.templateid.value;
	poststr += '&limitstart='+document.adminForm.limitstart.value;
	poststr += '&task=configdelete';
	RpcCall(poststr+"&rpc_call=deletetemplatefield", target);
}

function RpcCall(extra_params, target, async) {
	xmlHttp=GetXmlHttpObject();
	if (xmlHttp==null) {
	  alert ("Your browser does not support AJAX!");
	  return;
	}
	
	/* POST */
	var url="index2.php";
	var params = "option=com_csvimproved&act=rpc&no_html=1&"+extra_params+"&sid="+Math.random();
	
	/**
	 * Asynchronous request or Synchronous request
	 * true = asynchronous
	 * false = synchronous
	*/
	if (async == null) var async = true;
	xmlHttp.open("POST",url,async);
	
	/* Send the proper header information along with the request */
	xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlHttp.setRequestHeader("Content-length", params.length);
	xmlHttp.setRequestHeader("Connection", "close");
	xmlHttp.onreadystatechange = function() {
		if (xmlHttp.readyState == 4) {
			document.getElementById(target).innerHTML=xmlHttp.responseText;
		}
	}
	xmlHttp.send(params);
}

/* Create an XmlHttpObject */
function GetXmlHttpObject() {
	var xmlHttp=null;
	try {
	  // Firefox, Opera 8.0+, Safari
	  xmlHttp=new XMLHttpRequest();
	}
	catch (e) {
	  // Internet Explorer
	  try {
		xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
	  }
	  catch (e) {
		xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	}
	return xmlHttp;
}

function getFormValues(fobj){
	var str = "";
	for(var i = 0;i < fobj.elements.length;i++) {
		switch(fobj.elements[i].type){
			case "text":
			case "textarea":
			case "password":
				if (!fobj.elements[i].disabled) str += fobj.elements[i].name + "=" + encodeURIComponent(fobj.elements[i].value) + "&";
				break;
			case "hidden":
				//hidden cannot be disabled
				str += fobj.elements[i].name + "=" + encodeURIComponent(fobj.elements[i].value) + "&";
				break;
			case "checkbox":
			case "radio":
				if(fobj.elements[i].checked && !fobj.elements[i].disabled) str += fobj.elements[i].name + "=" + encodeURIComponent(fobj.elements[i].value) + "&";
				break;
			case "select-one":
				if (!fobj.elements[i].disabled) str += fobj.elements[i].name + "=" + encodeURIComponent(fobj.elements[i].options[fobj.elements[i].selectedIndex].value) + "&";
				break;
			case "select-multiple":
				if (!fobj.elements[i].disabled){
					for (var j = 0; j < fobj.elements[i].length; j++){
						var optElem = fobj.elements[i].options[j];
							if (optElem.selected == true){
								str += fobj.elements[i].name + "[]" + "=" + encodeURIComponent(optElem.value) + "&";
							}
						}
					}
				break;
		}
	}
	//Strip final &amp;
	str = str.substr(0,(str.length - 1));
	return str;
}
