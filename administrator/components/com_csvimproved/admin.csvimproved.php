<?php
/**
 * Admin interface
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_class_category_details.php 125 2008-02-13 22:43:26Z Roland $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * Admin interface
 *
 */
if (isset($cron) && $cron == true) require( $this->basepath.'/administrator/components/com_csvimproved/csvimproved.class.php' );
else {
	require_once( $mainframe->getPath( 'class' ) );
	require_once( $mainframe->getPath( 'admin_html' ) );
	$cron = false;
}

/* Get the csviregistry object */
$csviregistry = new CsviRegistry($cron);

/* Override preview in cron mode */
if ($csviregistry->GetVar('cron')) $csviregistry->SetVar('was_preview', true);

/* Get the database object */
$CsviDatabase = $csviregistry->GetObject('database');

/* Get the language object */
$CsviLang = $csviregistry->GetObject('language');

/* Check to see if VirtueMart is installed */
$q = "SELECT COUNT(id) AS total FROM #__components where link = 'option=com_virtuemart';";
$CsviDatabase->query($q);

/* Check to see if the user uses at least PHP 5 */
if (version_compare(phpversion(), '5', '<') == '-1') {
	echo '<img src="'.$csviregistry->GetVar('live_site').'administrator/components/com_csvimproved/images/csvi_logo_header.png" alt="CSV Improved"/>';
	echo '<br />';
	echo '<img src="'.$csviregistry->GetVar('live_site').'administrator/images/cancel_f2.png" alt="PHP version not supported" /> '.str_replace('[version]', phpversion(), $CsviLang->PHP_TOO_LOW);
}
else if ($CsviDatabase->f('total') == 0) {
	if ($csviregistry->GetVar('cron')) echo $CsviLang->NO_VIRTUEMART;
	else {
		echo '<img src="'.$csviregistry->GetVar('live_site').'administrator/components/com_csvimproved/images/csvi_logo_header.png" alt="CSV Improved"/>';
		echo '<br />';
		echo '<img src="'.$csviregistry->GetVar('live_site').'administrator/images/cancel_f2.png" alt="VirtueMart not found" /> '.$CsviLang->NO_VIRTUEMART;
	}
}
else {
	/* Get the template */
	$templates = $csviregistry->GetObject('templates');
	
	// Check the page
	if (!$csviregistry->IssetVar('page')) $csviregistry->SetVar('page', 'import');
	
	// Check upload type
	if (!$csviregistry->IssetVar('upload_type')) $csviregistry->SetVar('upload_type', '');
	
	// Check task
	if (!$csviregistry->IssetVar('task')) $csviregistry->SetVar('task', '');
	
	// Check the template ID
	if (!$csviregistry->IssetVar('template')) $csviregistry->SetVar('template', '');
	
	// Check the show preview settings
	if (!$csviregistry->IssetVar('was_preview')) $csviregistry->SetVar('was_preview', '');
	
	// Check the show preview settings
	if (!$csviregistry->IssetVar('error_found')) $csviregistry->SetVar('error_found', '');
	
	/* Show any messages generated */
	$csvilog = $csviregistry->GetObject('logger');
	echo $csvilog->logmessage;
	
	/* Set the user generated limits */
	if ($csviregistry->GetVar('debug')) {
		$csvilog->AddMessage('debug', '<hr />');
		$csvilog->AddMessage('debug', 'Setting system limits:<br />');
		$csvilog->AddMessage('debug', 'Setting max_execution_time to '.$templates->max_execution_time.' seconds');
		$csvilog->AddMessage('debug', 'Setting max_input_time to '.$templates->max_input_time.' seconds');
		$csvilog->AddMessage('debug', 'Setting memory_limit to '.$templates->memory_limit.'M');
		$csvilog->AddMessage('debug', 'Setting post_max_size to '.$templates->post_max_size.'M');
		$csvilog->AddMessage('debug', 'Setting upload_max_filesize to '.$templates->upload_max_filesize.'M');
	}
	ini_set('max_execution_time', $templates->max_execution_time);
	ini_set('max_input_time', $templates->max_input_time);
	ini_set('memory_limit', $templates->memory_limit.'M');
	ini_set('post_max_size', $templates->post_max_size.'M');
	ini_set('upload_max_filesize', $templates->upload_max_filesize.'M');
	
	switch ($csviregistry->GetVar('act')) {
		case 'import':
			include($csviregistry->GetVar('page_path')."csvi_page_import.php");
			PageImport($csviregistry);
			break;
		case 'export':
			include($csviregistry->GetVar('page_path')."csvi_page_export.php");
			PageExport($csviregistry);
			break;
		case 'templates':
			include($csviregistry->GetVar('page_path')."csvi_page_templates.php");
			PageTemplate($csviregistry);
			break;
		case 'configuration':
			include($csviregistry->GetVar('page_path')."csvi_page_fields.php");
			PageConfigFields($csviregistry);
			break;
		case 'about':
			include($csviregistry->GetVar('page_path')."csvi_page_about.php");
			break;
		case 'maintenance':
			include($csviregistry->GetVar('page_path')."csvi_page_maintenance.php");
			if (PageMaintenance($csviregistry)) include($csviregistry->GetVar('page_path')."csvi_page_output.php");
			break;
		case 'rpc':
			include($csviregistry->GetVar('class_path')."csvi_class_rpc.php");
			$CsviRpc = new CsviRpc($csviregistry);
			break;
		default :
		// Set to show main page
		$showmain = true;
		switch($csviregistry->GetVar('task')) {
			case 'showtemplate':
				$templates->GetTemplate($csviregistry);
				break;
			case 'updatetemplate':
				$templates->UpdateTemplate();
				break;
			case 'deletetemplate':
				$templates->DeleteTemplate($csviregistry);
				break;
			case 'addtemplate':
				$templates->AddTemplate();
				break;
			case 'export':
				$showmain = false;
				/* Include the export class */
				require_once($csviregistry->GetVar('class_path').'csvi_class_export.php');
				switch ($templates->template_type) {
					case 'ProductExport':
						require_once($csviregistry->GetVar('class_path').'export/csvi_class_product_export.php');
						$csviexport = new ProductExport($csviregistry);
						$csviexport->Start($csviregistry);
						break;
					case 'MultiplePricesExport':
						require_once($csviregistry->GetVar('class_path').'export/csvi_class_multipleprices_export.php');
						$csviexport = new MultiplePricesExport($csviregistry);
						$csviexport->Start($csviregistry);
						break;
					case 'ProductFilesExport':
						require_once($csviregistry->GetVar('class_path').'export/csvi_class_productfiles_export.php');
						$csviexport = new ProductFilesExport($csviregistry);
						$csviexport->Start($csviregistry);
						break;
					case 'ProductTypeExport':
						$csvi_export->ProductTypeExport($csviregistry);
						break;
					case 'ProductTypeParametersExport':
						$csvi_export->ProductTypeParametersExport($csviregistry);
						break;
					case 'ProductTypeXrefExport':
						$csvi_export->ProductTypeXrefExport($csviregistry);
						break;
					case 'ProductTypeNamesExport':
						$csvi_export->ProductTypeNamesExport($csviregistry);
						break;
					case 'TemplateExport':
						require_once($csviregistry->GetVar('class_path').'export/csvi_class_template_export.php');
						$csviexport = new TemplateExport($csviregistry);
						$csviexport->Start($csviregistry);
						break;
					case 'TemplateFieldsExport':
						require_once($csviregistry->GetVar('class_path').'export/csvi_class_templatefields_export.php');
						$csviexport = new TemplateFieldsExport($csviregistry);
						$csviexport->Start($csviregistry);
						break;
					case 'CategoryDetailsExport':
						require_once($csviregistry->GetVar('class_path').'export/csvi_class_categorydetails_export.php');
						$csviexport = new CategoryDetailsExport($csviregistry);
						$csviexport->Start($csviregistry);
						break;
					case 'ManufacturerExport':
						require_once($csviregistry->GetVar('class_path').'export/csvi_class_manufacturerdetails_export.php');
						$csviexport = new ManufacturerDetailsExport($csviregistry);
						$csviexport->Start($csviregistry);
						break;
					case 'OrderExport':
						require_once($csviregistry->GetVar('class_path').'export/csvi_class_order_export.php');
						$csviexport = new OrderExport($csviregistry);
						$csviexport->Start($csviregistry);
						break;
				}
				if (!$csviregistry->GetVar('cron')) include($csviregistry->GetVar('page_path')."csvi_page_output.php");
				else if ($csviregistry->GetVar('cron')) $GLOBALS['csviregistry'] =& $csviregistry;
				break;
			case 'import':
			case 'importfile':
				// Include the csv processor class
				$csv_process = $csviregistry->GetObject('import');
				$showmain = false;
				require_once($csviregistry->GetVar('class_path').'csvi_class_import.php');
				switch ($templates->template_type) {
					case 'ProductImport':
						// NOTE: Done
						require_once($csviregistry->GetVar('class_path').'import/csvi_class_product_import.php');
						$CsviImport = new ProductImport($csviregistry);
						break;
					case 'MultiplePricesUpload':
						// NOTE: Done
						require_once($csviregistry->GetVar('class_path').'import/csvi_class_multipleprices_import.php');
						$CsviImport = new MultiplePricesImport($csviregistry);
						break;
					case 'ProductFilesUpload':
						// NOTE: Done
						require_once($csviregistry->GetVar('class_path').'import/csvi_class_productfiles_import.php');
						$CsviImport = new ProductFilesImport($csviregistry);
						break;
					case 'ProductTypeUpload':
						// NOTE: Done
						require_once($csviregistry->GetVar('class_path').'import/csvi_class_producttypes_import.php');
						$CsviImport = new ProductTypesImport($csviregistry);
						break;
					case 'ProductTypeParametersUpload':
						// NOTE: Done
						require_once($csviregistry->GetVar('class_path').'import/csvi_class_producttypeparameters_import.php');
						$CsviImport = new ProductTypeParametersImport($csviregistry);
						break;
					case 'ProductTypeNamesUpload':
						// NOTE: Done
						require_once($csviregistry->GetVar('class_path').'import/csvi_class_producttypenames_import.php');
						$CsviImport = new ProductTypeNamesImport($csviregistry);
						break;
					case 'TemplateImport':
						// NOTE: Done
						require_once($csviregistry->GetVar('class_path').'import/csvi_class_template_import.php');
						$CsviImport = new TemplateImport($csviregistry);
						break;
					case 'TemplateFieldsImport':
						// NOTE: Done
						require_once($csviregistry->GetVar('class_path').'import/csvi_class_templatefields_import.php');
						$CsviImport = new TemplateFieldsImport($csviregistry);
						break;
					case 'ManufacturerImport':
						// NOTE: Done
						require_once($csviregistry->GetVar('class_path').'import/csvi_class_manufacturer_import.php');
						$CsviImport = new ManufacturerImport($csviregistry);
						break;
					case 'CategoryDetailsImport':
						require_once($csviregistry->GetVar('class_path').'import/csvi_class_categorydetails_import.php');
						$CsviImport = new CategoryDetailsImport($csviregistry);
						break;
					default:
						return false;
				}
				// Get the preview
				if ($templates->show_preview && $csviregistry->GetVar('was_preview') == "" & !$csviregistry->GetVar('cron')) include($csviregistry->GetVar('page_path')."csvi_page_preview.php");
				// No preview, show the output
				else if (!$csviregistry->GetVar('cron')) include($csviregistry->GetVar('page_path')."csvi_page_output.php");
				else if ($csviregistry->GetVar('cron')) $GLOBALS['csviregistry'] =& $csviregistry;
				break;
			case "cancel":
				/** 
				 * @todo Check if we need to clean up something
				 */
				// $csv_process->CleanUp($vars);
				break;
		}
		
		/* Do not show the control panel when we are in cronmode */
		if ($showmain && !$csviregistry->GetVar('cron')) CsviHtml::ShowCpanel($csviregistry);
		break;
	}
}
?>