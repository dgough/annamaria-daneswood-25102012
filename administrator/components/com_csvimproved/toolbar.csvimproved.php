<?php
/**
 * Toolbar 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Toolbar
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: toolbar.csvimproved.php 310 2008-06-07 10:53:51Z Suami $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * Toolbar controller
 */
require_once( $mainframe->getPath( 'toolbar_html' ) );
require_once( $mainframe->getPath( 'toolbar_default' ) );
require_once( $mainframe->getPath( 'class' ) );

/* Since the CSVI registry is not passed on to here, include it globally */
$csviregistry = new CsviRegistry;

$CsviLang = $csviregistry->GetObject('language');
$templates = $csviregistry->GetObject('templates');
$supportedfields = $csviregistry->GetObject('supportedfields');

$task = mosGetParam ($_REQUEST, 'task', '' );
$act = mosGetParam ($_REQUEST, 'act', '' );

switch ($act) {
	case 'import':
		CsviToolbar::ImportMenu($CsviLang);
		break;
	case 'export':
		CsviToolbar::ExportMenu($CsviLang);
		break;
	case 'availablefields':
		CsviToolbar::AvailablefieldsMenu();
		break;
	case 'documentation':
		CsviToolbar::DocumentationMenu();
		break;
	case 'about':
		CsviToolbar::AboutMenu();
		break;
	case 'configuration':
		switch ($task) {
			case 'newconfigfield':
				CsviToolbar::NewConfigMenu();
				break;
			default:
				CsviToolbar::FieldsMenu($CsviLang);
		}
		break;
	case 'templates':
		switch ($task) {
			case 'newtemplate':
				CsviToolbar::NewTemplateMenu($CsviLang);
				break;
			case 'edittemplate':
				CsviToolbar::EditTemplateMenu($CsviLang);
				break;
			case 'fields':
				$custom = false;
				$supportedfields->FieldsExport();
				if (array_key_exists($templates->template_type, $supportedfields->fields) || $templates->template_type == 'ProductTypeNamesUpload') {
					$custom = true;
				}
				CsviToolbar::FieldsMenu($CsviLang, $custom);
				break;
			default:
				CsviToolbar::TemplatesMenu($CsviLang);
				break;
		}
		break;
	case 'maintenance':
		switch ($task) {
			case 'maintain':
				CsviToolbar::MaintainMenu($CsviLang);
				break;
			default:
				CsviToolbar::MaintenanceMenu($CsviLang);
				break;
		}
}
switch ($task) {
	case 'import':
		CsviToolbar::OutputImportMenu($CsviLang, $csviregistry);
		break;
	case 'importfile':
		CsviToolbar::ImportEndMenu($CsviLang);
		break;
	case 'export':
		CsviToolbar::OutputExportMenu($CsviLang);
		break;
}
?>