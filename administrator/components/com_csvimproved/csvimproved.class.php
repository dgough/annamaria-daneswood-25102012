<?php
/**
 * CSV Improved registry
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvimproved.class.php 324 2008-06-11 10:46:54Z Suami $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * The Csvi Registry holds all the objects, arrays and variables that are
 * being used throughout the program.
 *
 * @package CSVImproved
 */
class CsviRegistry {
	/** @var array Array to hold all the objects */
	var $_objects = array();
	/** @var array Array to hold all the variables */
	var $_vars = array();
	/** @var array Array to hold all the arrays */
	var $_arrays = array();
	/** @var integer The current line that is being processed */
	var $currentline = 1;
	
	function __construct($cron=false) {
		/* Set cron status */
		$this->SetVar('cron', $cron);
		$this->Logger();
		$this->CollectRequest();
		$this->BasicVariables();
		$this->Database();
		$this->Language();
		/* Include the CSS and JS file */
		if (!$this->GetVar('cron') && $this->GetVar('act') !== 'rpc') {
			$this->LoadJavaScript();
			$this->LoadCss();
		}
		$this->SupportedFields();
		$this->FieldConfig();
		$this->Template();
		
		/* Set debug mode */
		$this->SetVar('debug', $this->_objects['templates']->collect_debug_info);
	}
	
	
	/**
	 * Loads the logger component
	 *
	 * This loads the logger component and attaches it to the logger object
	 * @name logger
	 */
	function Logger() {
		global $mosConfig_absolute_path;
		/** Include the logging so we can track all responses */
		require_once($mosConfig_absolute_path.'/administrator/components/com_csvimproved/classes/csvi_class_log.php');
		$csvilog = new CsviLog;
		$this->SetObject('logger', $csvilog);
	}
	
	function BasicVariables() {
		global $mosConfig_live_site, $mosConfig_absolute_path, $mosConfig_list_limit, $mosConfig_cachepath;
		/* Set some basic variables */
		/* Check for trailing slash */
		if( $mosConfig_live_site[strlen( $mosConfig_live_site)-1] == '/' ) $app = '';
		else $app = '/';
		
		$this->SetVar('live_site', $mosConfig_live_site.$app);
		$this->SetVar('absolute_path', $mosConfig_absolute_path);
		$this->SetVar('cache_path', $mosConfig_cachepath);
		$this->SetVar('component_path', $mosConfig_live_site.'/administrator/components/com_csvimproved/');
		$this->SetVar('language_path', $mosConfig_absolute_path.'/administrator/components/com_csvimproved/languages/');
		$this->SetVar('class_path', $mosConfig_absolute_path.'/administrator/components/com_csvimproved/classes/');
		$this->SetVar('page_path', $mosConfig_absolute_path.'/administrator/components/com_csvimproved/pages/');
		$this->SetVar('image_path', $mosConfig_live_site.'/administrator/components/com_csvimproved/images/');
		$this->SetVar('URL', $mosConfig_live_site.$app);
		$this->SetVar('list_limit',  $mosConfig_list_limit);
		$this->SetVar('rpc_call',  mosGetParam ($_REQUEST, 'rpc_call', '' ));
		
		/* Set the variable for cron mode */
		if (!$this->IssetVar('cron')) $this->SetVar('cron', false);
		
		/* Set the variable for checking for preview mode */
		if (!$this->IssetVar('was_preview')) $this->SetVar('was_preview', false);
		
		/* Check if the act variable is set, if not, we set to empty */
		$this->SetVar('act', mosGetParam ($_REQUEST, 'act', '' ));
		$this->SetVar('task', mosGetParam ($_REQUEST, 'task', '' ));
		
		if ((!isset($this->_vars['templateid']) || empty($this->_vars['templateid'])) && isset($this->_arrays['templateid'][0])) $this->SetVar('templateid', $this->_arrays['templateid'][0]);
		
		/* Set the Joomla version */
		if (class_exists('JVersion')) $version = new JVersion();
		else $version = new joomlaVersion();
		$this->joomlaversion = $version->RELEASE;
	}
	
	/**
	 * Loads the database component
	 *
	 * This loads the database component and attaches it to the database object
	 *
	 * @var object
	 * @name database
	 */
	function Database() {
		/* Include the database class */
		require_once($this->GetVar('class_path')."csvi_class_database.php");
		$database = new CsviDatabase();
		$this->SetObject('database', $database);
	}
	
	/**
	 * Loads the language component
	 *
	 * This loads the language component and attaches it to the language object
	 *
	 * @var object
	 * @name language
	 */
	function Language() {
		global $mosConfig_lang;
		/* Include the language files */
		if (file_exists($this->GetVar('language_path')."csvi_".$mosConfig_lang.".php")) require_once($this->GetVar('language_path')."csvi_".$mosConfig_lang.".php");
		else require_once($this->GetVar('language_path')."csvi_english.php");
		$csvilang =& new CsviLanguage;
		
		/* NOTE: Store the settings in the registry */ 
		$this->SetObject('language', $csvilang);
	}
	
	/**
	 * Loads the supported fields component
	 *
	 * This loads the supported fields component and attaches it to the supported fields object
	 * @var object
	 * @name logger
	 */
	function SupportedFields() {
		/* Include the supported fields class */
		require_once($this->GetVar('class_path')."csvi_class_supported_fields.php");
		$csvisupportedfields = new CsviSupportedFields($this);
		$this->SetObject('supportedfields', $csvisupportedfields);
	}
	
	/**
	 * Load the template field component
	 *
	 * @name fieldconfig
	 */
	function FieldConfig() {
		/** @internal Include the csv configuration class */
		require_once($this->GetVar('class_path')."csvi_class_template_fields.php");
		$fieldconfig =& new FieldConfig();
		$this->SetObject('fieldconfig', $fieldconfig);
	}
	
	/**
	 * Loads the template component
	 *
	 * This loads the template component and attaches it to the template object
	 * @var object
	 * @name template
	 */
	function Template() {
		/* Include the templates class */
		require_once($this->GetVar('class_path')."csvi_class_templates.php");
		$templates = new Templates($this);
		
		/* Load the template details if we have an active template */
		if ($this->IssetVar('templateid')) $templates->GetTemplate($this);
		$this->SetObject('templates', $templates);
	}
	
	/* Store an object in the registry */
	function SetObject($name, &$object) {
		$this->_objects[$name] =& $object;
	}
	
	/* Retrieve an object from the registry */
	function GetObject($name) {
		if (isset($this->_objects[$name])) return $this->_objects[$name];
		else return false;
	}
	
	/* Store a variable in the registry */
	function SetVar($name, $value) {
		$this->_vars[$name] =& $value;
	}
	
	/* Retrieve a variable from the registry */
	function GetVar($name) {
		if (isset($this->_vars[$name])) return $this->_vars[$name];
		else return false;
	}
	
	/* Store an object in the registry */
	function SetArray($name, $array) {
		if (!is_array($array)) return false;
		else {
			$this->_arrays[$name] = $array;
			return true;
		}
	}
	
	/* Retrieve an object from the registry */
	function GetArray($name) {
		if (isset($this->_arrays[$name])) return $this->_arrays[$name];
		else return false;
	}
	
	/* Return status if var exists or not */
	function IssetVar($name) {
		return isset($this->_vars[$name]);
	}
	
	/* Return status if var exists or not */
	function IssetArray($name) {
		return isset($this->_arrays[$name]);
	}
	
	/* Return an array of objects in the registry */
	function ListObjects() {
		return array_keys($this->_objects);
	}
	
	/* Put all $_REQUEST variables in the var registry */
	function CollectRequest() {
		foreach ($_REQUEST as $name => $value) {
			if (is_array($value)) {
				$this->SetArray($name, $value);
			}
			else {
				$this->SetVar($name, $value);
			}
		}
	} // end function CollectRequest
	
	/* Cloning an object */
	function CloneObj($obj) {
		$cloneobj = clone($obj);
		return $cloneobj;
	} // end function CloneObj
	
	/**
	 * Load the associated Javascript files
	 */
	function LoadJavaScript() {
		echo '<script language="JavaScript" type="text/javascript" src="'.$this->GetVar('component_path').'js/csvi.js"> </script>'.chr(10);
	}
	
	/**
	 * Load the associated stylesheets
	 */
	function LoadCss() {
		echo '<link href="'.$this->GetVar('component_path').'css/csvi.css" rel="stylesheet" type="text/css" media="screen, projection" ></link>'.chr(10);
	}
}
?>
