<?php
/**
 * Un-installation file for CSV Improved
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: uninstall.csvimproved.php 386 2008-07-26 01:45:03Z Suami $
 */
 
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * Uninstaller
 */
function com_uninstall() {
	global $mosConfig_absolute_path;
	
	// Delete the toolbar images
	unlink($mosConfig_absolute_path.'/administrator/images/csvi_fields_32x32.png');
	unlink($mosConfig_absolute_path.'/administrator/images/csvi_continue_32x32.png');
	unlink($mosConfig_absolute_path.'/administrator/images/csvi_export_32x32.png');
	unlink($mosConfig_absolute_path.'/administrator/images/csvi_import_32x32.png');
	unlink($mosConfig_absolute_path.'/administrator/images/csvi_templates_32x32.png');
	
	return 'version 0.9 RC 1 has been uninstalled.';
}
?>
