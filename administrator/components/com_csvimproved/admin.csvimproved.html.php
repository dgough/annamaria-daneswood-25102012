<?php
/**
 * Admin interface
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: admin.csvimproved.html.php 226 2008-05-06 01:31:02Z Suami $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * Administrator interface
 *
 * @package CSVImproved
 */
class CsviHtml {
	
	/**
	 * Shows the control panel on the startpage
	 *
	 * @param object $csviregistry the main controller class
	 */
	function ShowCpanel(&$csviregistry) {
		$CsviLang = $csviregistry->GetObject('language');
		echo '<img src="'.$csviregistry->GetVar('live_site').'administrator/components/com_csvimproved/images/csvi_logo_header.png" alt="CSV Improved"/>';
		
		?>
		<table width="90%" border="0" cellpadding="2" cellspacing="2" class="adminform">
		<tr>
			<td>
			<div id="cpanel">
			<?php echo CsviHtml::CpanelButton('csvi_templates_48x48.png', 'index2.php?option=com_csvimproved&amp;act=templates', $CsviLang->CP_TEMPLATES); ?>
			<?php echo CsviHtml::CpanelButton('csvi_import_48x48.png', 'index2.php?option=com_csvimproved&amp;act=import', $CsviLang->CP_IMPORT); ?>
			<?php echo CsviHtml::CpanelButton('csvi_export_48x48.png', 'index2.php?option=com_csvimproved&amp;act=export', $CsviLang->CP_EXPORT); ?>
			</div>
			</td>
		</tr>
		<tr>
			<td>
			<div id="cpanel">
			<?php echo CsviHtml::CpanelButton('csvi_system_48x48.png', 'index2.php?option=com_csvimproved&amp;act=maintenance', $CsviLang->CP_MAINTENANCE); ?>
			<?php echo CsviHtml::CpanelButton('csvi_help_48x48.png', 'http://www.csvimproved.com/wiki/doku.php/', $CsviLang->CP_HELP); ?>
			<?php echo CsviHtml::CpanelButton('csvi_about_48x48.png', 'index2.php?option=com_csvimproved&amp;act=about', $CsviLang->CP_ABOUT); ?>
			</div>
			</td>
		</tr>
		</table>
		<?php
	}
	
	
	/**
	 * Prints a button for the control panel
	 *
	 * @param string $image contains the name of the image
	 * @param string $link contains the target link for the image when clicked
	 * @param string $title contains the title of the button
	 * @return string returns a complete button for the control panel
	 */
	function CpanelButton($image, $link, $title) {
		$cpanelbutton = '<div style="float:left;">';
		$cpanelbutton .= '	<div class="icon">';
		$cpanelbutton .= '		<a href="'.$link.'"';
		if (substr($link, 0, 4) == "http") $cpanelbutton .= ' target="_new"';
		$cpanelbutton .= '      >';
		$cpanelbutton .= '			'.mosAdminMenus::imageCheckAdmin( $image, '/administrator/components/com_csvimproved/images/', NULL, NULL, $title );
		$cpanelbutton .= '			<span>'.$title.'</span>';
		$cpanelbutton .= '		</a>';
		$cpanelbutton .= '	</div>';
		$cpanelbutton .= '</div>';
		return $cpanelbutton;
	}
}
?>
