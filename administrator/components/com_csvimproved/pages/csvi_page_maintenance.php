<?php
/**
 * Configuration page
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Pages
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_page_maintenance.php 286 2008-06-01 02:51:30Z Suami $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * Maintenance page
 */
function PageMaintenance(&$csviregistry) {
	$operation = $csviregistry->GetArray('operation');
	switch ($csviregistry->GetVar('task')) {
		case 'maintain':
			switch($operation[0]) {
				case 'EmptyDatabase':
					if (EmptyDatabase($csviregistry)) return true;
					else return false;
					break;
				case 'RemoveOrphan':
					if (RemoveOrphan($csviregistry)) return true;
					else return false;
					break;
				case 'OptimizeTables':
					if (OptimizeTables($csviregistry)) return true;
					else return false;
					break;
				case 'SortCategories':
					if (SortCategories($csviregistry)) return true;
					else return false;
			}
			/**
			// NOTE: Make the debug messages global
			if ($this->debug) $d['csv_debug']['message'] = $this->csv_debug['message'];
			if ($this->debug) $this->csv_debug['message'] .= 'Emptying database !!!<br />';
			*/
			// $this->MaintenanceList($CsviLang, $vars, $csv_process);
			break;
		default:
			MaintenanceList($csviregistry);
			break;
	}
}
	
function MaintenanceList(&$csviregistry) {
	$CsviLang = $csviregistry->GetObject('language');
	?>
	<form method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>" name="adminForm" enctype="multipart/form-data">
		<input type="hidden" name="act" value="maintenance" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="option" value="com_csvimproved" />
		<input type="hidden" name="boxchecked" value="" />
		<table class="adminlist">
			<th><?php echo $CsviLang->MAINTENANCE_OPTIONS;?></th>
			<tr class="row0">
				<td>
					<input type="radio" id="cb1" name="operation[]" value="EmptyDatabase" onclick="isChecked(this.checked);" /><?php echo $CsviLang->EMPTY_DATABASE; ?>
				</td>
			</tr>
			<tr class="row1">
				<td>
					<input type="radio" id="cb2" name="operation[]" value="RemoveOrphan" onclick="isChecked(this.checked);" /><?php echo $CsviLang->REMOVE_ORPHAN; ?>
				</td>
			</tr>
			<tr class="row0">
				<td>
					<input type="radio" id="cb3" name="operation[]" value="OptimizeTables" onclick="isChecked(this.checked);" /><?php echo $CsviLang->OPTIMIZE_TABLES; ?>
				</td>
			</tr>
			<tr class="row1">
				<td>
					<input type="radio" id="cb4" name="operation[]" value="SortCategories" onclick="isChecked(this.checked);" /><?php echo $CsviLang->SORT_CATEGORIES; ?>
				</td>
			</tr>
		</table>
	</form>
	<?php
}
	
function EmptyDatabase(&$csviregistry) {
	global $mosConfig_db;
	$db = $csviregistry->GetObject('database');
	$csvilog = $csviregistry->GetObject('logger');
	$templates = $csviregistry->GetObject('templates');
	// NOTE: Collect debug information
	$debug = $templates->collect_debug_info;
		
	// NOTE: Empty all the necessary tables
	$q = "TRUNCATE TABLE `#__vm_product`;";
	if ($debug) $csvilog->AddMessage('debug', 'Empty product table: <a onclick="switchMenu(\'product_table\');" title="Show/hide query">Show/hide query</a><div id="product_table" class="debug">'.htmlentities($q).'</div>');
	if ($db->query($q)) $csvilog->AddStats('empty', 'Product table has been emptied');
	else $csvilog->AddStats('incorrect', 'Product table has not been emptied');
	
	$q = "TRUNCATE TABLE `#__vm_product_price`;";
	if ($debug) $csvilog->AddMessage('debug', 'Empty product price table: <a onclick="switchMenu(\'product_price_table\');" title="Show/hide query">Show/hide query</a><div id="product_price_table" class="debug">'.htmlentities($q).'</div>');
	if ($db->query($q)) $csvilog->AddStats('empty', 'Product price table has been emptied');
	else $csvilog->AddStats('incorrect', 'Product price table has not been emptied');
	
	$q = "TRUNCATE TABLE `#__vm_product_mf_xref`;";
	if ($debug) $csvilog->AddMessage('debug', 'Empty product manufacturer link table: <a onclick="switchMenu(\'product_mf_xref_table\');" title="Show/hide query">Show/hide query</a><div id="product_mf_xreftable" class="debug">'.htmlentities($q).'</div>');
	if ($db->query($q)) $csvilog->AddStats('empty', 'Product manufacturer link table has been emptied');
	else $csvilog->AddStats('incorrect', 'Product manufactuere link table has not been emptied');
	
	$q = "TRUNCATE TABLE `#__vm_product_attribute`;";
	if ($debug) $csvilog->AddMessage('debug', 'Empty product attribute table: <a onclick="switchMenu(\'product_attribute_table\');" title="Show/hide query">Show/hide query</a><div id="product_attribute_table" class="debug">'.htmlentities($q).'</div>');
	if ($db->query($q)) $csvilog->AddStats('empty', 'Product attribute table has been emptied');
	else $csvilog->AddStats('incorrect', 'Product attribute table has not been emptied');
	
	$q = "TRUNCATE TABLE `#__vm_category`;";
	if ($debug) $csvilog->AddMessage('debug', 'Empty category table: <a onclick="switchMenu(\'category_table\');" title="Show/hide query">Show/hide query</a><div id="category_table" class="debug">'.htmlentities($q).'</div>');
	if ($db->query($q)) $csvilog->AddStats('empty', 'Category table has been emptied');
	else $csvilog->AddStats('incorrect', 'Category table has not been emptied');
	
	$q = "TRUNCATE TABLE `#__vm_category_xref`;";
	if ($debug) $csvilog->AddMessage('debug', 'Empty category link table: <a onclick="switchMenu(\'category_link_table\');" title="Show/hide query">Show/hide query</a><div id="category_link_table" class="debug">'.htmlentities($q).'</div>');
	if ($db->query($q)) $csvilog->AddStats('empty', 'Category link table has been emptied');
	else $csvilog->AddStats('incorrect', 'Category link table has not been emptied');
	
	$q = "TRUNCATE TABLE `#__vm_product_attribute_sku`;";
	if ($debug) $csvilog->AddMessage('debug', 'Empty attribute for parent products table: <a onclick="switchMenu(\'product_attribute_sku_table\');" title="Show/hide query">Show/hide query</a><div id="product_attribute_sku_table" class="debug">'.htmlentities($q).'</div>');
	if ($db->query($q)) $csvilog->AddStats('empty', 'Attribute for parent products table has been emptied');
	else $csvilog->AddStats('incorrect', 'Attribute for parent products table has not been emptied');
	
	$q = "TRUNCATE TABLE `#__vm_product_category_xref`;";
	if ($debug) $csvilog->AddMessage('debug', 'Empty product category link table: <a onclick="switchMenu(\'product_category_xref\');" title="Show/hide query">Show/hide query</a><div id="product_category_xref" class="debug">'.htmlentities($q).'</div>');
	if ($db->query($q)) $csvilog->AddStats('empty', 'Product category link table has been emptied');
	else $csvilog->AddStats('incorrect', 'Product category link table has not been emptied');
	
	$q = "TRUNCATE TABLE `#__vm_product_discount`;";
	if ($debug) $csvilog->AddMessage('debug', 'Empty product discount table: <a onclick="switchMenu(\'product_discount_table\');" title="Show/hide query">Show/hide query</a><div id="product_discount_table" class="debug">'.htmlentities($q).'</div>');
	if ($db->query($q)) $csvilog->AddStats('empty', 'Product discount table has been emptied');
	else $csvilog->AddStats('incorrect', 'Product discount table has not been emptied');
	
	$q = "TRUNCATE TABLE `#__vm_product_type`;";
	if ($debug) $csvilog->AddMessage('debug', 'Empty product type table: <a onclick="switchMenu(\'product_type_table\');" title="Show/hide query">Show/hide query</a><div id="product_type_table" class="debug">'.htmlentities($q).'</div>');
	if ($db->query($q)) $csvilog->AddStats('empty', 'Product type table has been emptied');
	else $csvilog->AddStats('incorrect', 'Product type table has not been emptied');
	
	$q = "TRUNCATE TABLE `#__vm_product_type_parameter`;";
	if ($debug) $csvilog->AddMessage('debug', 'Empty product type parameter table: <a onclick="switchMenu(\'product_type_parameter_table\');" title="Show/hide query">Show/hide query</a><div id="product_type_parameter_table" class="debug">'.htmlentities($q).'</div>');
	if ($db->query($q)) $csvilog->AddStats('empty', 'Product type parameter table has been emptied');
	else $csvilog->AddStats('incorrect', 'Product type parameter table has not been emptied');
	
	$q = "TRUNCATE TABLE `#__vm_product_product_type_xref`;";
	if ($debug) $csvilog->AddMessage('debug', 'Empty product type link table: <a onclick="switchMenu(\'product_product_type_xref_table\');" title="Show/hide query">Show/hide query</a><div id="product_product_type_xref_table" class="debug">'.htmlentities($q).'</div>');
	if ($db->query($q)) $csvilog->AddStats('empty', 'Product type link table has been emptied');
	else $csvilog->AddStats('incorrect', 'Product type link table has not been emptied');
	
	$q = "TRUNCATE TABLE `#__vm_product_relations`;";
	if ($debug) $csvilog->AddMessage('debug', 'Empty product relations table: <a onclick="switchMenu(\'product_relations_table\');" title="Show/hide query">Show/hide query</a><div id="product_relations_table" class="debug">'.htmlentities($q).'</div>');
	if ($db->query($q)) $csvilog->AddStats('empty', 'Product relations table has been emptied');
	else $csvilog->AddStats('incorrect', 'Product relations table has not been emptied');
	
	$q = "DELETE FROM `#__vm_manufacturer` WHERE manufacturer_id > 1;";
	if ($debug) $csvilog->AddMessage('debug', 'Empty manufacturer table: <a onclick="switchMenu(\'manufacturer_table\');" title="Show/hide query">Show/hide query</a><div id="manufacturer_table" class="debug">'.htmlentities($q).'</div>');
	if ($db->query($q)) $csvilog->AddStats('empty', 'Manufacturer table has been emptied');
	else $csvilog->AddStats('incorrect', 'Manufacturer table has not been emptied');
	
	// NOTE: Check if there are any product type tables created, if so, remove them
	$q = "SHOW TABLES LIKE '%product_type__'";
	$db->query($q);
	while ($db->next_record()) {
		$db_drop = $csviregistry->CloneObj($db);
		$tablename = $db->f("Tables_in_".$mosConfig_db." (%product_type__)");
		$q_drop = "DROP TABLE `".$tablename."`;";
		if ($debug) $csvilog->AddMessage('debug', 'Deleting product type name '.$tablename.' table: <a onclick="switchMenu(\'product_type_name_table_'.$tablename.'\');" title="Show/hide query">Show/hide query</a><div id="product_type_name_table_'.$tablename.'" class="debug">'.htmlentities($q_drop).'</div>');
		if ($db_drop->query($q_drop)) $csvilog->AddStats('deleted', 'Product type name table '.$tablename.' has been removed');
		else $csvilog->AddStats('incorrect', 'Product type name table '.$tablename.' has not been removed');
	}
	return true;
}

function RemoveOrphan(&$csviregistry) {
	$db = $csviregistry->GetObject('database');
	$dbclean = $csviregistry->CloneObj($db);
	$csvilog = $csviregistry->GetObject('logger');
	$q = "SELECT ID FROM #__csvi_templates";
	$db->query($q);
	$foundids = '';
	while ($db->next_record()) {
		$foundids .= $db->f('ID').',';
	}
	$q = "SELECT id FROM #__csvi_configuration
		WHERE field_template_id NOT IN (".substr($foundids, 0, -1).")";
	$db->query($q);
	if ($db->num_rows() > 0) {
		while ($db->next_record()) {
			$q = "DELETE FROM #__csvi_configuration WHERE id = ".$db->f("id");
			if ($dbclean->query($q)) $csvilog->AddStats('deleted', 'Field ID '.$db->f("id").' has been removed');
			else $csvilog->AddStats('incorrect', 'Field ID '.$db->f("id").' has not been removed');
		}
	}
	else $csvilog->AddStats('information', 'No orphaned fields found');
	return true;
}

function OptimizeTables(&$csviregistry) {
	$db = $csviregistry->GetObject('database');
	$csvilog = $csviregistry->GetObject('logger');
	$tables = array();
	$tables[] = '#__csvi_configuration';
	$tables[] = '#__csvi_templates';
	$tables[] = '#__vm_product';
	$tables[] = '#__vm_product_price';
	$tables[] = '#__vm_product_mf_xref';
	$tables[] = '#__vm_product_attribute';
	$tables[] = '#__vm_category';
	$tables[] = '#__vm_category_xref';
	$tables[] = '#__vm_product_attribute_sku';
	$tables[] = '#__vm_product_category_xref';
	$tables[] = '#__vm_product_discount';
	$tables[] = '#__vm_product_type';
	$tables[] = '#__vm_product_type_parameter';
	$tables[] = '#__vm_product_product_type_xref';
	$tables[] = '#__vm_product_relations';
	$tables[] = '#__vm_manufacturer';
	
	foreach ($tables as $id => $tablename) {
		$q =  "OPTIMIZE TABLE ".$tablename;
		if ($db->query($q)) $csvilog->AddStats('information', 'Table '.substr($tablename, 3).' has been optimized');
		else $csvilog->AddStats('incorrect', 'Table '.substr($tablename,3).' has not been optimized');
	}
	return true;
}

function SortCategories(&$csviregistry) {
	$db = $csviregistry->GetObject('database');
	$csvilog = $csviregistry->GetObject('logger');
	
	/* Get all categories */
	$query  = "SELECT category_name, category_child_id as cid, category_parent_id as pid
			FROM #__vm_category, #__vm_category_xref WHERE
			#__vm_category.category_id=#__vm_category_xref.category_child_id ";
	
	/* Execute the query */
	$db->query( $query );

	$categories = array();
	/* Group all categories together according to their level */
	while( $db->next_record() ) {
		$categories[$db->f("pid")][$db->f("cid")] = strtolower($db->f("category_name"));
	}
	
	/* Sort the categories and store the item list */
	foreach ($categories as $id => $category) {
		asort($category);
		$listorder = 1;
		foreach ($category as $category_id => $category_name) {
			/* Store the new sort order */
			$q = "UPDATE #__vm_category
				SET list_order = '".$listorder."'
				WHERE category_id = '".$category_id."'";
			$db->query($q);
			$csvilog->AddStats('information', "Saved category ".$category_name." with order ".$listorder);
			$listorder++;
		}
	}
	return true;
}
?>
