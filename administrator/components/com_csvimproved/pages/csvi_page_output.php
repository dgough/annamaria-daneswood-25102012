<?php
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );
/**
 * Output page
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Pages
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_page_output.php 244 2008-05-18 06:28:53Z Suami $
 */
$CsviLang = $csviregistry->GetObject('language');
$csvilog = $csviregistry->GetObject('logger');
$task = mosGetParam($_REQUEST, 'task');

/* Display any messages there are */
if (!empty($csvilog->logmessage)) echo $csvilog->logmessage;
else {
	?>
	<div style="text-align: left; width: 30%;" class="message"><?php echo $CsviLang->OUTPUT_CSV_UPLOAD_MESSAGES.' '.$csviregistry->GetVar('user_filename'); ?></div>
	<form method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>" name="adminForm">
		<input type="hidden" name="task" value="" />
		<!-- $task is used here to redirect back to the correct act -->
		<input type="hidden" name="act" value="<?php echo $task; ?>" />
		<input type="hidden" name="option" value="com_csvimproved" />
	</form>
	<br />
	<div style="width: 99%">
		<div style="float: left; width: 20%; border: 1px solid #000000; margin-right: 5%;">
			<table style="width: 100%;">
			<tr style="background-color: #DFDFDF;"><td>&nbsp;</td><td><?php echo $CsviLang->OUTPUT_COUNT ?></td></tr>
			<?php
			$total = 0;
			foreach ($csvilog->stats as $action => $stats) {
				if ($stats['count'] > 0) {
					$action = strtoupper("OUTPUT_".$action);
					?>
					<tr style="background-color: #EFEFEF;"><td><?php echo $CsviLang->$action; ?></td><td><?php echo $stats['count']; ?></td></tr>
					<?php $total = $total + $stats['count'];
				}
			} ?>
			<tr style="background-color: #DFDFDF;"><td><?php echo $CsviLang->OUTPUT_TOTAL ?></td><td><?php echo $total; ?></td></tr>
			</table>
		</div>
		<div style="float: left; text-align: left; align: center; width: 60%; height: 250px; overflow: auto;";>
			<?php
			foreach ($csvilog->stats as $action => $stats) {
				if ($stats['count'] > 0) {
					$action = strtoupper("OUTPUT_".$action);
					echo '<div style="text-align: center; background-color: #EFEFEF;">'.$CsviLang->$action.'</div>';
					echo $stats['message'];
					echo "<br />";
				}
			}
			?>
		</div>
<?php } ?>

<?php 
	/* Show debug log */
	if ($csvilog->debug_message != '') {?>
	<div style="float: left; width: 100%; text-align: left; margin-top: 1.0em;">
		<?php echo $csvilog->debug_message; ?>
	</div>
<?php } ?>
</div>