<?php
/**
 * Preview page
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Pages
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_page_preview.php 310 2008-06-07 10:53:51Z Suami $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * preview page
 */
$preview = $csviregistry->GetVar('preview');
$templates = $csviregistry->GetObject('templates');
$csvilog = $csviregistry->GetObject('logger');
if (!empty($csvilog->logmessage)) {
	echo $csvilog->logmessage;
	require($csviregistry->GetVar('page_path')."csvi_page_output.php");
}
else {
	?>
	<form method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>" name="adminForm" enctype="multipart/form-data"> 
		<input type="hidden" name="task" value="importfile" />
		<input type="hidden" name="option" value="com_csvimproved" /> 
		<input type="hidden" name="templateid" value="<?php echo $templates->id; ?>" />
		<input type="hidden" name="show_preview" value="" />
		<input type="hidden" name="was_preview" value="Y" />
		<input type="hidden" name="selectfile" value="2" />
		<input type="hidden" name="user_filename" value="<?php echo $csviregistry->GetVar('user_filename');?>" />
		<input type="hidden" name="local_csv_file" value="<?php echo $csviregistry->GetVar('csv_file');?>" />
		<input type="hidden" name="convertimport" value="<?php echo $csviregistry->GetVar('convertimport');?>" />
	</form>
	<div style="width: 100%; float: left; overflow: auto;">
	<?php
	if ($csviregistry->GetVar('upload_file_error')) echo $csviregistry->GetVar('preview_only');
	// Output the preview
	echo $preview;
	?>
	</div>
	<?php
}
?>
