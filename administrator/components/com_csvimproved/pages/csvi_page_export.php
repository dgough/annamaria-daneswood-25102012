<?php
/**
 * Export page
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Export
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_page_export.php 286 2008-06-01 02:51:30Z Suami $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );
 
/**
 * Export controller
 *
 * @param &$csviregistry array Global register
 */
function PageExport(&$csviregistry) {
	$CsviLang = $csviregistry->GetObject('language');
	$CsviSupportedFields = $csviregistry->GetObject('supportedfields');
	?>
	<div align="left">
	<form method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>" name="adminForm">
		<input type="hidden" name="task" value="export" />
		<input type="hidden" name="act" value="" />
		<input type="hidden" name="option" value="com_csvimproved" />
		<table class="adminform" border="0">
			<th colspan="2"><?php echo $CsviLang->EXPORT_TO_FILE ?></th>
			<tr><td>
				<div id="choosetemplate">
					<?php ExportTemplate($csviregistry); ?>
					<?php ExportLocation($csviregistry); ?>
					<?php ExportLimits($csviregistry); ?>
				</div> <!-- choosetemplate -->
			</td></tr>
		</table>
		<div id="templatedetailsbox">
			<?php
			$csviregistry->SetVar('rpc_call', 'gettemplatedetails');
			// $csviregistry->SetVar('csvi_type', 'export');
			include($csviregistry->GetVar('class_path')."csvi_class_rpc.php");
			$CsviRpc = new CsviRpc($csviregistry);
			?>
		</div> <!-- templatedetailsbox -->
	</form>
	</div>
<?php
}

function ExportTemplate(&$csviregistry) {
	$CsviLang = $csviregistry->GetObject('language');
	$templates = $csviregistry->GetObject('templates');
	$CsviSupportedFields = $csviregistry->GetObject('supportedfields');
	$CsviSupportedFields->FieldsExport();
	/* Get the selection list of templates */
	$values = '';
	$gettemplate = false;
	
	foreach ($templates->templatelist as $id => $template) {
		/* Only show export templates */
		if (isset($CsviSupportedFields->fields[$template['type']]) && $templates->GetNumberOfFields($id, true) > 0) {
			/* Get the first template id for showing the template details */
			if (!$gettemplate) {
				$templates->GetTemplate($csviregistry, $id);
				$gettemplate = true;
			}
			$values .= '<option value="'.$id.'">'.$template['name']."</option>\n";
		}
	}
	?>
	<tr>
		<td><?php echo $CsviLang->CHOOSE_TEMPLATE ?></td>
		<td>
			<?php if ($values != '') { ?>
				<select id="templateid" name="templateid" onChange="ShowTemplateDetails(this, 'gettemplatedetails', 'templatedetailsbox', 'export');">
				<?php echo $values; ?>
				</select>
				<span class="smallprint"><?php echo $CsviLang->CHOOSE_TEMPLATE_NO_FIELDS; ?></span>
				<?php
			}
			else {
				echo $CsviLang->NO_TEMPLATE_FIELDS;
			}
			?>
		</td>
	</tr>
	<?php
}

function ExportLimits(&$csviregistry) {
	$CsviLang = $csviregistry->GetObject('language');
	?>
	<tr><td><?php echo $CsviLang->EXPORT_NUMBER_RECORDS ?></td>
	<td><input type="text" name="recordstart" id="recordstart" /></td></tr>
	<tr><td><?php echo $CsviLang->EXPORT_START_RECORD ?></td>
	<td><input type="text" name="recordend" id="recordend" /></td></tr>
	<?php
}

function ExportLocation(&$csviregistry) {
	$CsviLang = $csviregistry->GetObject('language');
	?>
	<tr><td><?php echo $CsviLang->EXPORT_TO_DOWNLOAD ?></td>
	<td><input type="radio" name="exportto" id="todownload" value="todownload" checked="checked" onChange="document.getElementById('exportlocation').style.display='none';" /></td></tr>
	<tr><td><?php echo $CsviLang->EXPORT_TO_LOCAL ?></td>
	<td><input type="radio" name="exportto" id="tofile" value="tofile" onChange="document.getElementById('exportlocation').style.display='block';" /></td></tr>
	<tr><td colspan="2">
	<div id="exportlocation" style="display: none;">
		<input type="text" size="120" name="localfile" value="<?php echo realpath($csviregistry->GetVar('absolute_path')."/media/"); ?>" />
	</div>
	</td></tr>
	<?php
}
?>
