<?php
/**
 * Templates page
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Pages
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_page_templates.php 386 2008-07-26 01:45:03Z Suami $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * Template pages for configuration of templates
 *
 * @param &$csviregistry object Global register
 */
function PageTemplate(&$csviregistry) {
	$CsviLang = $csviregistry->GetObject('language');
	$CsviSupportedFields = $csviregistry->GetObject('supportedfields');
	$templates = $csviregistry->GetObject('templates');
	
	switch ($csviregistry->GetVar('task')) {
		case 'newtemplate':
			TemplateConfigurator($csviregistry);
			// NewTemplate($csviregistry);
			break;
		case 'addtemplate':
			AddTemplate($csviregistry);
			TemplateList($csviregistry);
			break;
		case 'edittemplate':
			TemplateConfigurator($csviregistry);
			// EditTemplate($csviregistry);
			break;
		case 'clonetemplate':
			CloneTemplate($csviregistry);
			TemplateList($csviregistry);
			break;
		case 'updatetemplate':
			UpdateTemplate($csviregistry);
			TemplateList($csviregistry);
			break;
		case 'deletetemplate':
			DeleteTemplate($csviregistry);
			TemplateList($csviregistry);
			break;
		case 'fields':
			require_once($csviregistry->GetVar('page_path')."csvi_page_fields.php");
			PageConfigFields($csviregistry);
			break;
		default:
			TemplateList($csviregistry);
			break;
	}
}

/**
 * Show a list of templates limited by the limit settings
 *
 * @param object &$csviregistry Global register
 */
function TemplateList(&$csviregistry) {
	$CsviLang = $csviregistry->GetObject('language');
	$csvilog = $csviregistry->GetObject('logger');
	$templates = $csviregistry->GetObject('templates');
	$CsviSupportedFields = $csviregistry->GetObject('supportedfields');
	$db = $csviregistry->GetObject('database');
	/* Output any messages generated */
	echo $csvilog->logmessage;
	?>
	<form name="adminForm" method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>">
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="act" value="<?php echo $csviregistry->GetVar('act'); ?>" />
	<input type="hidden" name="option" value="com_csvimproved" />
	<input type="hidden" name="boxchecked" value="" />
	<br />
	<div style="text-align: left;">
	<table class="adminlist">
		<tr>
			<th width="5%"></th>
			<th><?php echo $CsviLang->NAME; ?></th><th><?php echo $CsviLang->TEMPLATE_SHOPPER_GROUP_NAME; ?></th><th><?php echo $CsviLang->NUMBER_OF_FIELDS; ?></th><th><?php echo $CsviLang->TYPE; ?></th>
		</tr>
			<?php
			$i=0;
			$CsviSupportedFields->FieldsExport();
			$types = $CsviSupportedFields->fields;
			$CsviSupportedFields->FieldsImport();
			$types = array_merge($types, $CsviSupportedFields->fields);
			
			/* Class for building lists */
			/* Get the total number of templates */
			$db->query("SELECT COUNT(*) FROM #__csvi_templates");
			$total = $db->loadResult();
			
			if (!$csviregistry->IssetVar('limit')) $limit = $csviregistry->GetVar('list_limit');
			else $limit = intval( $csviregistry->GetVar('limit'));
			if (!$csviregistry->IssetVar('limitstart')) $limitstart = 0;
			else $limitstart = intval( $csviregistry->GetVar('limitstart'));
			
			require_once($csviregistry->GetVar('absolute_path').'/administrator/includes/pageNavigation.php' );
			$pagenav = new mosPageNav( $total, $limitstart, $limit  );
			
			$templates->GetTemplatesList($csviregistry, $pagenav);
			$rowcolor = 0;
			foreach ($templates->templatelist as $id => $details) {
				if ($rowcolor > 1) $rowcolor = 0;
				echo "<tr class=\"row".$rowcolor."\"><td><input type=\"radio\" id=\"cb".$i++."\" name=\"templateid[]\" value=\"$id\" onclick=\"isChecked(this.checked);\" /></td><td>".$details['name']."</td>";
				echo "<td>".$details['shopper_group_name']."</td>";
				echo "<td>".$templates->GetNumberOfFields($id, true)."</td>";
				if (isset($types[$details['type']])) echo "<td>".$CsviLang->$types[$details['type']]."</td></tr>\n";
				else echo "<td>&nbsp;</td></tr>\n";
				$rowcolor++;
			}
			?>
	</table>
	</div>
	<?php echo $pagenav->getListFooter(); ?>
	</form>
	<?php
}

/**
 * Show a new template for configuration
 *
 * @param object &$csviregistry Global register
 */
function NewTemplate(&$csviregistry) {
	TemplateLayout($csviregistry);
}

/**
 * Show a template for editing
 *
 * @param object &$csviregistry Global register
 */
function EditTemplate(&$csviregistry) {
	$templates = $csviregistry->GetObject('templates');
	$templateid = $csviregistry->GetArray('templateid');
	$templates->GetTemplate($csviregistry, $templateid[0]);
	TemplateLayout($csviregistry);
}

/**
 * Clones a template
 *
 * All template settings are cloned. The fields are not cloned.
 *
 * @param object &$csviregistry Global register
 */
function CloneTemplate(&$csviregistry) {
	$templates = $csviregistry->GetObject('templates');
	$templateid = $csviregistry->GetArray('templateid');
	$templates->CloneTemplate($csviregistry);
}


/**
 * Stores the updated template
 *
 * @param object &$csviregistry Global register
 */
function UpdateTemplate(&$csviregistry) {
	$templates = $csviregistry->GetObject('templates');
	$templates->UpdateTemplate($csviregistry);
}

/**
 * Stores a new template
 *
 * @param object &$csviregistry Global register
 */
function AddTemplate(&$csviregistry) {
	$templates = $csviregistry->GetObject('templates');
	$templates->AddTemplate($csviregistry);
}

/**
 * Deletes a selected template
 *
 * @param object &$csviregistry Global register
 */
function DeleteTemplate(&$csviregistry) {
	$templates = $csviregistry->GetObject('templates');
	$templateid = $csviregistry->GetArray('templateid');
	$templates->id = $templateid[0];
	$templates->DeleteTemplate($csviregistry);
}

/**
 * Show the page with all the templte configuration settings
 *
 * @param object &$csviregistry Global register
 */
function TemplateLayout(&$csviregistry) {
	$CsviLang = $csviregistry->GetObject('language');
	$templates = $csviregistry->GetObject('templates');
	$CsviSupportedFields = $csviregistry->GetObject('supportedfields');
	$db = $csviregistry->GetObject('database');
	/* Get all the shopper groups */
	$q = "SELECT shopper_group_id, shopper_group_name FROM #__vm_shopper_group";
	$db->query($q);
	$shopper_groups = $db->loadObjectList();
	?>
	<form name="adminForm" id="edittemplates" method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>">
		<input type="hidden" name="option" value="com_csvimproved" /> 
		<input type="hidden" name="template" value ="<?php echo $templates->id; ?>" />
		<input type="hidden" name="act" value ="<?php echo $csviregistry->GetVar('act'); ?>" />
		<input type="hidden" name="task" value ="" />
		<?php
		/* Instantiate new tab system */
		$tabs = new mosTabs(1);
		
		/* Start tab pane */
	$tabs->startPane("Templates");
		
		/* General tab */
		$tabs->startTab($CsviLang->GENERAL,"general_tab");
		?>
		<table class="adminlist">
			<tr>
				<td width="5%">
					<?php 
					/* Template type */
					echo '<label class="template_label" for="template_type">'.$CsviLang->TYPE.'</label>';
					?>
				</td>
				<td>
					<?php
					echo '<select id="template_type" name="template_type">';
					$CsviSupportedFields->FieldsImport();
					foreach ($CsviSupportedFields->fields as $name => $value) {
						echo "<option value=\"".$name."\"";
						if ($templates->template_type == $name) echo 'selected="selected"';
						echo ">".$CsviLang->$value."</option>\n";
					}
					$CsviSupportedFields->FieldsExport();
					foreach ($CsviSupportedFields->fields as $name => $value) {
						echo "<option value=\"".$name."\"";
						if ($templates->template_type == $name) echo 'selected="selected"';
						echo ">".$CsviLang->$value."</option>\n";
					}
					echo '</select>';
					?>
				</td>
			</tr>
			<tr>
				<td width="5%">
					<?php 
					/* Template name */
					echo '<label class="template_label" for="template_name">'.$CsviLang->NAME.'</label>';
					?>
				</td>
				<td>
					<?php echo '<input class="template_input" type="text" id="template_name" name="template_name" value="'.$templates->template_name.'">';?>
				</td>
			</tr>
			<tr>
				<td>
					<?php
					/* Field delimiter */
					echo '<label class="template_label" for="field_delimiter">'.$CsviLang->ENCLOSURE.'</label>';
					?>
				</td>
				<td>
					<?php
					echo '<input class="template_input delimiter" type="text" maxlength="1" id="field_delimiter" name="field_delimiter" value="'.$templates->field_delimiter.'">';
					?>
				</td>
			</tr>
			<tr>
				<td>
					<?php
					/* Text enclosure */
					echo '<label class="template_label" for="text_enclosure">'.$CsviLang->DELIMITER.'</label>';
					?>
				</td>
				<td>
					<?php
					echo '<input class="template_input delimiter" type="text" maxlength="1" id="text_enclosure" name="text_enclosure"';
					if ($templates->text_enclosure == '"') echo "value='".$templates->text_enclosure."'>";
					else echo 'value="'.$templates->text_enclosure.'">';
					?>
				</td>
			</tr>
		</table>
		<?php
		$tabs->endTab();
		$tabs->startTab($CsviLang->IMPORT,"import_tab");
		/* Import tab */
		?>
			<table class="adminlist">
			<tr>
				<td width="5%">
					<?php
					/* Skip first line */
					echo '<label class="template_label" for="skip_first_line">'.$CsviLang->SKIP_FIRST_LINE.'</label>';
					?>
				</td>
				<td>
					<?php
					echo '<input class="template_input checkbox" type="checkbox" id="skip_first_line" name="skip_first_line" value="'.$templates->skip_first_line.'"';
					if ($templates->skip_first_line) echo 'checked=checked';
					echo '">';
					?>
				</td>
			</tr>
			<tr>
				<td>
					<?php
					/* Use column headers */
					echo '<label class="template_label" for="use_column_headers">'.$CsviLang->IMPORT_CONFIG_CSV_FILE.'</label>';
					?>
				</td>
				<td>
					<?php
					echo '<input class="template_input checkbox" type="checkbox" id="use_column_headers" name="use_column_headers" value="'.$templates->use_column_headers.'"';
					if ($templates->use_column_headers) echo 'checked=checked';
					echo '">';
					?>
				</td>
			</tr>
			<tr>
				<td>
					<?php
					/* Overwrite existing data */
					echo '<label class="template_label" for="overwrite_existing_data">'.$CsviLang->OVERWRITE_EXISTING_DATA.'</label>';
					?>
				</td>
				<td>
					<?php
					echo '<input class="template_input checkbox" type="checkbox" id="overwrite_existing_data" name="overwrite_existing_data" value="'.$templates->overwrite_existing_data.'"';
					if ($templates->overwrite_existing_data) echo 'checked=checked';
					echo '">';
					?>
				</td>
			</tr>
			<tr>
				<td>
					<?php
					/* Skip default value */
					echo '<label class="template_label" for="skip_default_value">'.$CsviLang->SKIP_DEFAULT_VALUE.'</label>';
					?>
				</td>
				<td>
					<?php
					echo '<input class="template_input checkbox" type="checkbox" id="skip_default_value" name="skip_default_value" value="'.$templates->skip_default_value.'"';
					if ($templates->skip_default_value) echo 'checked=checked';
					echo '">';
					?>
				</td>
			</tr>
			<tr>
				<td>
					<?php
					/* Show preview */
					echo '<label class="template_label" for="show_preview">'.$CsviLang->SHOW_PREVIEW.'</label>';
					?>
				</td>
				<td>
					<?php
					echo '<input class="template_input checkbox" type="checkbox" id="show_preview" name="show_preview" value="'.$templates->show_preview.'"';
					if ($templates->show_preview) echo 'checked=checked';
					echo '">';
					?>
				</td>
			</tr>
			<tr>
				<td>
					<?php
					/* Thumbnail width */
					echo '<label class="template_label" for="thumb_width">'.$CsviLang->TEMPLATE_THUMB_WIDTH.'</label>';
					?>
				</td>
				<td>
					<?php
					if (empty($templates->thumb_width)) $templates->thumb_width = 90;
					echo '<input class="template_input thumbs" type="text" maxlength="4" id="thumb_width" name="thumb_width" value="'.$templates->thumb_width.'">';
					?>
				</td>
			</tr>
			<tr>
				<td>
					<?php
					/* Thumbnail height */
					echo '<label class="template_label" for="thumb_height">'.$CsviLang->TEMPLATE_THUMB_HEIGHT.'</label>';
					?>
				</td>
				<td>
					<?php
					if (empty($templates->thumb_height)) $templates->thumb_height = 90;
					echo '<input class="template_input thumbs" type="text" maxlength="4" id="thumb_height" name="thumb_height" value="'.$templates->thumb_height.'">';
					?>
				</td>
			</tr>
			<tr>
				<td>
					<?php
					/* File location */
					echo '<label class="template_label" for="file_location">'.$CsviLang->TEMPLATE_FILE_LOCATION.'</label>';
					?>
				</td>
				<td>
					<?php
					echo '<input class="template_input longtext" type="text" id="file_location" name="file_location" size="255" value="';
					if (empty($templates->file_location)) echo $csviregistry->GetVar('absolute_path').'/components/com_virtuemart/shop_image/product/';
					else echo $templates->file_location;
					echo '"><br />';
					echo $CsviLang->STANDARD_FILE_LOCATION.'<span id="pathsuggest">'.$csviregistry->GetVar('absolute_path').'/components/com_virtuemart/shop_image/product/</span> | 
					<a href="#" onclick="document.getElementById(\'file_location\').value=document.getElementById(\'pathsuggest\').innerHTML; return false;">'.$CsviLang->PASTE.'</a> | 
					<a href="#" onclick="document.getElementById(\'file_location\').value=\'\'; return false;">'.$CsviLang->CLEAR.'</a>';
					?>
				</td>
			</tr>
			<tr>
				<td>
					<?php
					/* Collect debug info */
					echo '<label class="template_label" for="collect_debug_info">'.$CsviLang->COLLECT_DEBUG_INFO.'</label>';
					?>
				</td>
				<td>
					<?php
					echo '<input class="template_input checkbox" type="checkbox" id="collect_debug_info" name="collect_debug_info" value="'.$templates->collect_debug_info.'"';
					if ($templates->collect_debug_info) echo 'checked=checked';
					echo '">';
					?>
				</td>
			</tr>
			</table>
		<?php
		$tabs->endTab();
		/* Export tab */
		$tabs->startTab($CsviLang->EXPORT,"export_tab");
		?>
			<table class="adminlist">
			<tr>
				<td width="5%">
					<?php
					/* Include column headers */
					echo '<label class="template_label" for="include_column_headers">'.$CsviLang->INCLUDE_COLUMN_HEADERS.'</label>';
					?>
				</td>
				<td>
					<?php
					echo '<input class="template_input checkbox" type="checkbox" id="include_column_headers" name="include_column_headers" value="'.$templates->show_preview.'"';
					if ($templates->include_column_headers) echo 'checked=checked';
					echo '">';
					?>
				</td>
			</tr>
			<tr>
				<td width="5%">
					<?php
					/* Export filename */
					echo '<label class="template_label" for="export_filename">'.$CsviLang->TEMPLATE_EXPORT_FILENAME.'</label>';
					?>
				</td>
				<td>
					<?php
					echo '<input class="template_input longtext" type="text" id="export_filename" name="export_filename" value="'.$templates->export_filename.'" />';
					?>
				</td>
			</tr>
			<tr>
				<td width="5%">
					<?php
					/* Set shopper group name the user wants to export */
					echo '<label class="template_label" for="shopper_group_id">'.$CsviLang->TEMPLATE_EXPORT_SHOPPER_GROUP.'</label>';
					?>
				</td>
				<td>
					<?php
					echo '<select name="shopper_group_id">';
					echo '<option value="0">'.$CsviLang->TEMPLATE_EXPORT_SHOPPER_GROUP_ALL.'</option>';
					foreach ($shopper_groups as $key => $group) {
						echo '<option value="'.$group->shopper_group_id.'"';
						if ($templates->shopper_group_id == $group->shopper_group_id) echo 'selected="selected"';
						echo ">".$group->shopper_group_name."</option>\n";
					}
					echo '</select>';
					?>
				</td>
			</tr>
			<tr>
				<td width="5%">
					<?php
					/* Check which state of products the user want to export */
					echo '<label class="template_label" for="product_publish">'.$CsviLang->TEMPLATE_EXPORT_PRODUCT_PUBLISH.'</label>';
					?>
				</td>
				<td>
					<?php
					echo '<input type="radio" id="product_publish" name="product_publish" value=""';
					if ($templates->product_publish == '') echo 'checked=checked';
					echo '">'.$CsviLang->TEMPLATE_EXPORT_PRODUCT_PUBLISHED_BOTH;
					echo '<br />';
					echo '<input type="radio" id="product_publish" name="product_publish" value="Y"';
					if ($templates->product_publish == 'Y') echo 'checked=checked';
					echo '">'.$CsviLang->TEMPLATE_EXPORT_PRODUCT_PUBLISHED;
					echo '<br />';
					echo '<input type="radio" id="product_publish" name="product_publish" value="N"';
					if ($templates->product_publish == 'N') echo 'checked=checked';
					echo '">'.$CsviLang->TEMPLATE_EXPORT_PRODUCT_UNPUBLISHED;
					?>
				</td>
			</tr>
			<tr>
				<th colspan="2"><?php echo $CsviLang->SHOPPER_SITES; ?></th>
			</tr>
			<tr>
				<td>
					<?php
					/* Export type */
					echo '<label class="template_label" for="export_type">'.$CsviLang->EXPORT_TYPE.'</label>';
					?>
				</td>
				<td>
					<?php
					echo '<select name="export_type">';
					echo '<option value="csv"';
					if ($templates->export_type == "csv") echo 'selected="selected"';
					echo '>CSV</option>';
					echo '<option value="xml"';
					if ($templates->export_type == "xml") echo 'selected="selected"';
					echo '>XML</option>';
					echo '</select>';
					?>
				</td>
			</tr>
			<tr>
				<td>
					<label class="template_label" for="export_site"><?php echo $CsviLang->EXPORT_SITE ?></label>
				</td>
				<td>
					<?php
					echo '<select name="export_site">';
					echo '<option value=""';
					if ($templates->export_site == "") echo 'selected="selected"';
					echo '>'.$CsviLang->CHOOSE_SITE.'</option>';
					echo '<option value="csvi"';
					if ($templates->export_site == "csvi") echo 'selected="selected"';
					echo '>CSV Improved</option>';
					echo '<option value="beslist"';
					if ($templates->export_site == "beslist") echo 'selected="selected"';
					echo '>beslist.nl</option>';
					echo '<option value="froogle"';
					if ($templates->export_site == "froogle") echo 'selected="selected"';
					echo '>froogle.com</option>';
					echo '</select>';
					?>
				</td>
			</tr>
			<tr>
				<td>
					<label class="template_label" for="producturl_suffix"><?php echo $CsviLang->EXPORT_PRODUCTURL_SUFFIX ?></label>
				</td>
				<td>
					<?php
					echo '<input class="template_input longtext" type="text" id="producturl_suffix" name="producturl_suffix" value="'.$templates->producturl_suffix.'" />';
					?>
				</td>
			</tr>
			</table>
		<?php
		$tabs->endTab();
		/* System limits tab */
		$tabs->startTab($CsviLang->SYSTEM_LIMITS,"systemlimits_tab");
		?>
		<table class="adminform">
		<tr>
			<td width="5%">
				<?php
				/* Maximum execution time */
				echo '<label class="template_label" for="max_execution_time">'.$CsviLang->TEMPLATE_MAX_EXECUTION_TIME.'</label>';
				?>
			</td>
			<td>
				<?php
				echo '<input class="template_input" type="text" id="max_execution_time" name="max_execution_time" value="'.$templates->max_execution_time.'">';
				?>
			</td>
		</tr>
		<tr>
			<td>
				<?php
				/* Maximum input time */
				echo '<label class="template_label" for="max_input_time">'.$CsviLang->TEMPLATE_MAX_INPUT_TIME.'</label>';
				?>
			</td>
			<td>
				<?php
				echo '<input class="template_input" type="text" id="max_input_time" name="max_input_time" value="'.$templates->max_input_time.'">';
				?>
			</td>
		</tr>
		<tr>
			<td>
				<?php
				/* Maximum memory */
				echo '<label class="template_label" for="memory_limit">'.$CsviLang->TEMPLATE_MEMORY_LIMIT.'</label>';
				?>
			</td>
			<td>
				<?php
				echo '<input class="template_input" type="text" id="memory_limit" name="memory_limit" value="'.$templates->memory_limit.'">';
				?>
			</td>
		</tr>
		<tr>
			<td width="5%">
				<?php
				/* Maximum POST size */
				echo '<label class="template_label" for="post_max_size">'.$CsviLang->TEMPLATE_POST_MAX_SIZE.'</label>';
				?>
			</td>
			<td>
				<?php
				echo '<input class="template_input" type="text" id="post_max_size" name="post_max_size" value="'.$templates->post_max_size.'">';
				?>
			</td>
		</tr>
		<tr>
			<td width="5%">
				<?php
				/* Maximum Upload size */
				echo '<label class="template_label" for="upload_max_filesize">'.$CsviLang->TEMPLATE_UPLOAD_MAX_FILESIZE.'</label>';
				?>
			</td>
			<td>
				<?php
				echo '<input class="template_input" type="text" id="upload_max_filesize" name="upload_max_filesize" value="'.$templates->upload_max_filesize.'">';
				?>
			</td>
		</tr>
		</table>
		<?php
		$tabs->endTab();
	$tabs->endPane("Templates");
	?>
	</form>
	<?php
}

/**
 * Template configurator
 *
 * @param object &$csviregistry Global register
 */
function TemplateConfigurator(&$csviregistry) {
	$csvilang = $csviregistry->GetObject('language');
	$templates = $csviregistry->GetObject('templates');
	$supportedfields = $csviregistry->GetObject('supportedfields');
	$supportedfields->FieldsExport();
	/* Check whether it is an import or export template */
	$export = false;
	if (!empty($templates->template_type)) {
		if (isset($supportedfields->fields[$templates->template_type])) {
			$csviregistry->SetVar('templatetype', 'export');
			$export = true;
		}
		else {
			$csviregistry->SetVar('templatetype', 'import');
		}
	}
	echo '<div id="templateconfig">';
		echo '<div id="templateconfig-steps">';
			/* First step */
			echo '<input type="submit" class="';
			if ($csviregistry->GetVar('step') == 1 || $csviregistry->GetVar('step') == '') echo ' templateconfig-active';
			else echo 'submit';
			echo '" value="Choose your template" onclick="document.adminForm.act.value = \'rpc\';document.adminForm.step.value = 1;TemplateConfigurator(\'adminForm\', \'templateconfig\');return false;" />'."\n";
			
			/* Second step */
			echo '<input type="submit" class="';
			if ($csviregistry->GetVar('step') == 2) echo 'templateconfig-active';
			else echo 'submit';
			echo '" value="';
			switch ($csviregistry->GetVar('templatetype')) {
				case 'import':
					echo 'Import options';
					break;
				case 'export':
					echo 'Export options';
					break;
				default:
					echo 'No template type found!';
					break;
			}
			echo  '" onclick="document.adminForm.act.value = \'rpc\';document.adminForm.step.value = 2;TemplateConfigurator(\'adminForm\', \'templateconfig\');return false;" />'."\n";
			/* Third step */
			echo '<input type="submit" class="';
			if ($csviregistry->GetVar('step') == 3) echo 'templateconfig-active';
			else echo 'submit';
			echo '" value="'.$csvilang->SYSTEM_LIMITS.'" onclick="document.adminForm.act.value = \'rpc\';document.adminForm.step.value = 3;TemplateConfigurator(\'adminForm\', \'templateconfig\');return false;" />'."\n";
			/* Fourth step */
			echo '<input type="submit" class="';
			if ($csviregistry->GetVar('step') == 4) echo 'templateconfig-active';
			else echo 'submit';
			echo '" value="'.$csvilang->GENERAL.'" onclick="document.adminForm.act.value = \'rpc\';document.adminForm.step.value = 4;TemplateConfigurator(\'adminForm\', \'templateconfig\');return false;" />'."\n";
			echo '</div>';
		echo '<div id="templateconfig-content">';
			?>
			<form name="adminForm" id="adminForm">
			<?php AddHiddenFields($csviregistry); ?>
			<input type="radio" name="templatetype" id="templatetype" value="import" <?php if (!$export) echo 'checked=checked'; ?>><?php echo $csvilang->IMPORT; ?></input>
			<br />
			<input type="radio" name="templatetype" id="templatetype" value="export" <?php if ($export) echo 'checked=checked'; ?>><?php echo $csvilang->EXPORT; ?></input>
			</form>
			<?php
		echo '</div>';
	echo '</div>';
}

function AddHiddenFields(&$csviregistry) {
		$templates = $csviregistry->GetObject('templates');
		?>
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="option" value="com_csvimproved" />
		<input type="hidden" name="step" id="step" value="2" />
		<input type="hidden" name="act" value="templates" />
		<input type="hidden" name="templateid" value ="<?php echo $csviregistry->GetVar('templateid'); ?>" />
		<?php
		
		if ($csviregistry->GetVar('templatetype') == 'import') {
			if ($templates->use_column_headers && $templates->skip_first_line) {
				echo '<input type="hidden" name="use_column_headers" id="use_column_headers" value="'.$templates->use_column_headers.'" />';
			}
			else if ($templates->use_column_headers) {
				echo '<input type="hidden" name="use_column_headers" id="use_column_headers" value="'.$templates->use_column_headers.'" />';
			}
			
			else if ($templates->skip_first_line) {
				echo '<input type="hidden" name="skip_first_line" id="skip_first_line" value="'.$templates->skip_first_line.'" />';
			}
			echo '<input type="hidden" name="overwrite_existing_data" id="overwrite_existing_data" value="'.$templates->overwrite_existing_data.'" />';
			echo '<input type="hidden" name="skip_default_value" id="skip_default_value" value="'.$templates->skip_default_value.'" />';
			echo '<input type="hidden" name="show_preview" id="show_preview" value="'.$templates->show_preview.'" />';
			echo '<input type="hidden" name="thumb_width" id="thumb_width" value="'.$templates->thumb_width.'" />';
			echo '<input type="hidden" name="thumb_height" id="thumb_height" value="'.$templates->thumb_height.'" />';
			echo '<input type="hidden" name="collect_debug_info" id="collect_debug_info" value="'.$templates->collect_debug_info.'" />';
		}
		
		if ($csviregistry->GetVar('templatetype') == 'export') {
			echo '<input type="hidden" name="export_type" id="export_type" value="'.$templates->export_type.'" />';
			echo '<input type="hidden" name="export_site" id="export_site" value="'.$templates->export_site.'" />';
			if ($templates->include_column_headers) {
				echo '<input type="hidden" name="include_column_headers" id="include_column_headers" value="1" />';
			}
			else echo '<input type="hidden" name="include_column_headers" id="include_column_headers" value="0" />';
			echo '<input type="hidden" name="export_filename" id="export_filename" value="'.$templates->export_filename.'" />';
			echo '<input type="hidden" name="shopper_group_id" id="shopper_group_id" value="'.$templates->shopper_group_id.'" />';
			echo '<input type="hidden" name="manufacturer" id="manufacturer" value="'.$templates->manufacturer.'" />';
			echo '<input type="hidden" name="product_publish" id="product_publish" value="'.$templates->product_publish.'" />';
			echo '<input type="hidden" name="producturl_suffix" id="producturl_suffix" value="'.$templates->producturl_suffix.'" />';
		}
		
		echo '<input type="hidden" name="max_execution_time" id="max_execution_time" value="'.$templates->max_execution_time.'" />';
		echo '<input type="hidden" name="max_input_time" id="max_input_time" value="'.$templates->max_input_time.'" />';
		echo '<input type="hidden" name="memory_limit" id="memory_limit" value="'.$templates->memory_limit.'" />';
		echo '<input type="hidden" name="post_max_size" id="post_max_size" value="'.$templates->post_max_size.'" />';
		echo '<input type="hidden" name="upload_max_filesize" id="upload_max_filesize" value="'.$templates->upload_max_filesize.'" />';
		echo '<input type="hidden" name="template_name" id="template_name" value="'.$templates->template_name.'" />';
		echo '<input type="hidden" name="field_delimiter" id="field_delimiter" value="'.$templates->field_delimiter.'" />';
		echo '<input type="hidden" name="text_enclosure" id="text_enclosure" value="'.$templates->text_enclosure.'" />';
		echo '<input type="hidden" name="file_location" id="file_location" value="'.$templates->file_location.'" />';
}
?>
