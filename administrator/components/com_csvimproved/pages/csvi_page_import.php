<?php
/**
 * Import page
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Pages
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_page_import.php 176 2008-04-01 18:08:30Z Roland $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * Shows the import page
 *
 * @param &$csviregistry Global register
 */
function PageImport(&$csviregistry) {
	$CsviLang = $csviregistry->GetObject('language');
	?>
	<div align="left">
		<table class="adminform" border="0">
			<th><?php echo $CsviLang->UPLOAD_SETTINGS; ?></th>
			<tr><td>
				<form method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>" name="adminForm" enctype="multipart/form-data">
					<input type="hidden" name="task" value="import" />
					<input type="hidden" name="option" value="com_csvimproved" />
					<input type="hidden" name="boxchecked" value="" />
					<div>
					<?php ImportTemplate($csviregistry); ?>
					</div>
					<br />
					<div>
					<?php ImportUploadFile($csviregistry); ?>
					</div>
					<br />
					<div>
					<?php ImportLocalFile($csviregistry); ?>
					</div>
					<br />
					<div>
					<?php ShowConvert($csviregistry); ?>
					</div>
					<br />
				</form>
			</td></tr>
		</table>
	</div>
	<div id="templatedetailsbox">
		<?php
		$csviregistry->SetVar('rpc_call', 'gettemplatedetails');
		$csviregistry->SetVar('csvi_type', 'import');
		include($csviregistry->GetVar('class_path')."csvi_class_rpc.php");
		$CsviRpc = new CsviRpc($csviregistry);
		?>
	</div> <!-- templatedetailsbox -->
	<?php
}

/**
 * Shows a dropdown box with available templates for import
 *
 * @param &$csviregistry Global register
 */
function ImportTemplate(&$csviregistry) {
	$CsviLang = $csviregistry->GetObject('language');
	$templates = $csviregistry->GetObject('templates');
	$CsviSupportedFields = $csviregistry->GetObject('supportedfields');
	$gettemplate = false;
	$CsviSupportedFields->FieldsImport();
	echo $CsviLang->CHOOSE_TEMPLATE;
	?>
	<br />
	<select id="templateid" name="templateid" onChange="ShowTemplateDetails(this, 'gettemplatedetails', 'templatedetailsbox', 'import');">
		<?php
		foreach ($templates->templatelist as $id => $template) {
			/* Only show import templates */
			if (isset($CsviSupportedFields->fields[$template['type']])) {
				/* Get the first template id for showing the template details */
				if (!$gettemplate) {
					$templates->GetTemplate($csviregistry, $id);
					$gettemplate = true;
				}
				echo "<option value=\"$id\">".$template['name']."</option>\n";
			}
		}
		?>
	</select>
	<?php
}

/**
 * Shows a file selection box for file to upload
 *
 * @param &$csviregistry Global register
 */
function ImportUploadFile(&$csviregistry) {
	$CsviLang = $csviregistry->GetObject('language');
	echo $CsviLang->UPLOAD_FILE;
	?>
	<br />
	<input type="hidden" id="cb1" name="selectfile" value="1" />
	<input type="file" name="file" id="file" size="120" onChange="document.adminForm.cb1.value = 1" />
	<?php
}

/**
 * Shows an input box for local file to upload
 *
 * @param &$csviregistry Global register
 */
function ImportLocalFile(&$csviregistry) {
	$CsviLang = $csviregistry->GetObject('language');
	echo $CsviLang->FROM_DIRECTORY;
	?>
	<br />
	<input type="text" size="120" value="<?php echo realpath($csviregistry->GetVar('absolute_path')."/media") ?>" name="local_csv_file" onChange="document.adminForm.cb1.value = 2"/>
	<?php
}

/**
 * Shows an option if the user wants to convert the import or not
 *
 * @see CsviFile::ReadNextLine()
 * @param &$csviregistry Global register
 */
function ShowConvert(&$csviregistry) {
	$csvilang = $csviregistry->GetObject('language');
	echo $csvilang->IMPORT_CONVERT_FILE.'<br />';
	if (function_exists('iconv')) {
		echo $csvilang->CONVERT_IMPORT.' ';
		if ($csviregistry->joomlaversion == "1.0") echo $csvilang->ISO_IMPORT;
		else if ($csviregistry->joomlaversion == "1.5") echo $csvilang->UTF_IMPORT;
		?>
		<input type="checkbox" id="convertimport" name="convertimport" />
		<?php
	}
	else echo $csvilang->EXPLAIN_ICONV;
}
?>
