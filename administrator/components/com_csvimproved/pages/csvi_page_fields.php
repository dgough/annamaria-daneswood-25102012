<?php
/**
 * Configuration page
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Templates
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_page_fields.php 286 2008-06-01 02:51:30Z Suami $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );
/**
 * Configuration of template fields
 *
 * @param &$csviregistry object Global register
 */
function PageConfigFields(&$csviregistry) {
	$csvilog = $csviregistry->GetObject('logger');
	$csvilang = $csviregistry->GetObject('language');
	$fieldconfig = $csviregistry->GetObject('fieldconfig');
	$showfieldlist = true;
	switch ($csviregistry->GetVar('task')) {
		case 'saveconfigfield':
			$fieldconfig->AddConfigField($csviregistry);
			break;
		case 'saveconfigtable':
			$fieldconfig->UpdateConfigField($csviregistry);
			break;
		case 'configdelete':
			$fieldconfig->DeleteConfigField($csviregistry);
			break;
		case 'renumber':
			$fieldconfig->RenumberFields($csviregistry);
			break;
		case 'publish':
			if ($fieldconfig->SwitchPublishFields($csviregistry)) $csvilog->AddMessage('info', $csvilang->TEMPLATE_FIELDS_PAGE_PUBLISHED);
			else $csvilog->AddMessage('info', $csvilang->TEMPLATE_FIELDS_PAGE_NOT_PUBLISHED);
			break;
		case 'unpublish':
			if ($fieldconfig->SwitchPublishFields($csviregistry)) $csvilog->AddMessage('info', $csvilang->TEMPLATE_FIELDS_PAGE_UNPUBLISHED);
			else $csvilog->AddMessage('info', $csvilang->TEMPLATE_FIELDS_PAGE_NOT_UNPUBLISHED);
			break;
	}
	if ($showfieldlist) FieldsList($csviregistry);
}

function FieldsList(&$csviregistry) {
	$csvilang = $csviregistry->GetObject('language');
	$templates = $csviregistry->GetObject('templates');
	$csvisupportedfields = $csviregistry->GetObject('supportedfields');
	$templateid = $csviregistry->GetVar('templateid');
	$fieldconfig = $csviregistry->GetObject('fieldconfig');
	$csvilog = $csviregistry->GetObject('logger');
	$db = $csviregistry->GetObject('database');
	
	/* Get the current template */
	$templates->GetTemplate($csviregistry);
	
	/* Get the fields for the template type */
	$csvisupportedfields->GetSupportedFields($templates->template_type);
	
	/* Class for building lists */
	$db->query('SELECT COUNT(*) FROM #__csvi_configuration WHERE field_template_id = '.$templates->id);
	$total = $db->loadResult();
	
	if (!$csviregistry->IssetVar('limit')) $limit = $csviregistry->GetVar('list_limit');
	else $limit = intval( $csviregistry->GetVar('limit'));
	if (!$csviregistry->IssetVar('limitstart')) $limitstart = 0;
	else $limitstart = intval( $csviregistry->GetVar('limitstart'));
	
	/* Get the page navigation */
	require_once($csviregistry->GetVar('absolute_path').'/administrator/includes/pageNavigation.php' );
	$pagenav = new mosPageNav( $total, $limitstart, $limit  );
	
	/* Get the fields for the active template */
	$fields = $fieldconfig->GetFields($csviregistry, $pagenav);
	
	/**
	 * The hiddenfields variable contains all the hidden fields for the form.
	 * These fields need to be in their own div otherwise it resets the newly
	 * added field. By separating them in their own div, the div can be called
	 * instead of the form HTML.
	 */
	$hiddenfields = '';
	?>
	<div id="fieldlistdiv">
		<div id="logmessage" style="float: left; width: 100%; text-align: center;">
			<?php echo $csvilog->logmessage; ?>
		</div>
		<br />
		<form method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>" name="adminForm" id="adminForm">
			<input type="hidden" name="act" value="configuration" />
			<input type="hidden" name="task" value="" />
			<input type="hidden" name="option" value="com_csvimproved" />
			<input type="hidden" name="templateid" value="<?php echo $templateid; ?>" />
			<input type="hidden" name="boxchecked" value="" />
			<?php
			/* Search field for limiting the fields */
			?>
			<div id="filterbox">
			<?php echo $csvilang->FILTER; ?> <input type="text" id="filterfield" name="filterfield" value="<?php echo $csviregistry->GetVar('filterfield'); ?>" onChange="document.adminForm.submit();" />
			</div>
			<table class="adminlist" id="fieldslist">
				<th colspan="7">
					<?php echo $templates->template_name; ?>
				</th>
				<tr id="newfield" class="row0">
					<td>&nbsp;</td>
					<td id="newfield_fieldorder"><input type="text" name="field[{fill}][_order]" value="0" size="4" /></td>
					<td id="newfield_fieldname">
					<select name="newfieldname" id="newfieldname">
					<?php
					foreach ($csvisupportedfields->fields as $fieldid => $fieldname) {
						echo '<option value="'.$fieldname.'">'.$fieldname.'</option>';
					}
					?>
					</select>
					</td>
					<?php if ($csviregistry->GetVar('csvi_type') == 'export' || $templates->template_type == 'ProductTypeNamesUpload') {?>
						<td id="newfield_columnheader"><input type="text" name="field[{fill}][_column_header]" value="" /></td>
					<?php } ?>
					<td id="newfield_defaultvalue"><input type="text" name="field[{fill}][_default_value]" value="" /></td>
					<td><a href="javascript:" onClick="AddTemplateField('fieldlistdiv'); return false;"><img src="images/tick.png" width="12" height="12" alt="Add" title="Add" /></a></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<th width="20" class="title"><input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($fields); ?>);" /></th>
					<th class="title"><?php echo $csvilang->FIELD_ORDERING ?> <a href="index2.php" onclick="document.adminForm.task.value='renumber'; document.adminForm.submit(); return false;"><img src="<?php echo $csviregistry->GetVar('image_path'); ?>csvi_menu_16x16.png" height="16" width="16" alt="<?php echo $csvilang->TB_RENUMBER;?>" title="<?php echo $csvilang->TB_RENUMBER;?>"></a></th>
					<th class="title"><?php echo $csvilang->FIELD_NAME ?></th>
					<?php if ($csviregistry->GetVar('csvi_type') == 'export'  || $templates->template_type == 'ProductTypeNamesUpload') {?>
						<th class="title"><?php echo $csvilang->COLUMN_HEADER ?></th>
					<?php } ?>
					<th class="title"><?php echo $csvilang->DEFAULT_VALUE ?></th>
					<th class="title"><?php echo $csvilang->PUBLISHED ?></th>
					<th class="title"><?php echo $csvilang->DELETE ?></th>
				</tr>
				<?php
				$rowcolor = 0;
				for ($i=0, $n=count($fields); $i < $n; $i++) {
					if ($rowcolor > 1) $rowcolor = 0;
					if (isset($fields[$i])) {
						$field = $fields[$i];
						
						$img 	= $field->published ? 'tick.png' : 'publish_x.png';
						$task 	= $field->published ? 'unpublish' : 'publish';
						$alt 	= $field->published ? 'Published' : 'Unpublished';
						
						$checked 	= mosCommonHTML::CheckedOutProcessing( $field, $i );
					}
					?>
					<tr class="row<?php echo $rowcolor; ?>" id="tr<?php echo $i;?>">
					<?php $rowcolor++; ?>
					<td>
					<?php echo $checked; ?>
					</td>
					<td><input type="text" name="field[<?php echo $i ?>][_order]" value="<?php echo $field->field_order ?>"
					size="4" /></td>
					<td>
						<select name="field[<?php echo $i ?>][_field_name]">
					<?php
					foreach ($csvisupportedfields->fields as $fieldid => $fieldname) {
						echo '<option value="'.$fieldname.'"';
						if ($field->field_name == $fieldname) echo 'selected="selected"';
						echo '>'.$fieldname.'</option>';
					}
					?>
					</select>
					</td>
					<?php if ($csviregistry->GetVar('csvi_type') == 'export' || $templates->template_type == 'ProductTypeNamesUpload') {?>
						<td><input type="text" name="field[<?php echo $i ?>][_column_header]"
						value="<?php echo $field->field_column_header ?>" /></td>
					<?php } ?>
					<td><input type="text" name="field[<?php echo $i ?>][_default_value]"
					value="<?php echo $field->field_default_value ?>" /></td>
					<td>
					<a href="javascript: void(0);" onClick="return listItemTask('cb<?php echo $i;?>','<?php echo $task;?>')">
					<img src="images/<?php echo $img;?>" width="12" height="12" border="0" alt="<?php echo $alt; ?>" />
					</a>
					</td>
					<td>
					<a href="javascript: void(0);" onClick="return DeleteTemplateField('<?php echo $field->id ?>', 'tr<?php echo $i ?>', 'fieldlistdiv')">
					<img src="images/delete_f2.png" width="12" height="12" border="0" alt="<?php echo $alt; ?>" />
					</td>
					</tr>
					<?php
						$hiddenfields .= '<input type="hidden" name="field['.$i.'][_id]" value="'.$field->id.'" />'.chr(10);
					?>
					<?php
				}
				/* All fields have been added, lets add the hidden fields */
				echo '<div id="formhiddenfields">';
				echo $hiddenfields;
				echo '</div>';
				?>
			</table>
			<?php echo $pagenav->getListFooter(); ?>
		</form>
	</div> <!-- End fieldslist  -->
	<?php
}

function CustomDbSelection(&$csviregistry) {
	$db = $csviregistry->GetObject('database');
	$supportedfields = $csviregistry->GetObject('supportedfields');
	$supportedfields->ProductTableFields();
	$db->query("SHOW COLUMNS FROM #__vm_product");
	$result = $db->loadObjectList('Field');
	foreach ($result as $fieldname => $data) {
		if (!in_array($fieldname, $supportedfields->fields)) {
			echo $fieldname;
			echo '<br />';
		}
	}
}
?>
