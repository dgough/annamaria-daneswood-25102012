<?php
/**
 * French language file
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Language
 * @author Michel LORIAUT
 * @author Xavier Lemoine
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_french.php 286 2008-06-01 02:51:30Z Suami $
 *
 */
 
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * French language file for CSV Improved
 *
 * @package CSVImproved
 * @subpackage Languages
 */
class CsviLanguage {
    var $UPLOAD = 'Importer';
	var $VERSION = '0.8 stable';
	var $ABOUT = 'A propos';
	var $DELIMITER = 'Identificateur de texte';
	var $ENCLOSURE = 'S&eacute;parateur de champ:';
	var $SKIP_FIRST_LINE = 'Ignorer la premi&egrave;re ligne';
	var $SKIP_DEFAULT_VALUE = 'Ignorer la valeur par d&eacute;faut';
	var $OVERWRITE_EXISTING_DATA = 'Ecraser les donn&eacute;es existantes';
	var $MULTIPLE_PRICES_UPLOAD = 'Importation de prix multiples';
	var $IMPORT_CONFIG_CSV_FILE = 'Utiliser les en-t&ecirc;tes de colonnes comme configuration';
	var $COLLECT_DEBUG_INFO = 'Collecter les informations de debuggage';
	var $SHOW_PREVIEW = 'Afficher un aper&ccedil;u';
	var $REGULAR_UPLOAD = 'Importation de produits';
	var $PRODUCT_FILES_UPLOAD = 'Importation de fichiers produits';
	var $PRODUCT_TYPE_UPLOAD = 'Importation de types de produits';
	var $PRODUCT_TYPE_PARAMETERS_UPLOAD = 'Importation de param&egrave;tres de types de produits';
	var $PRODUCT_TYPE_XREF_UPLOAD = 'Importation de r&eacute;f&eacute;rences crois&eacute;es de types de produits';
	var $PRODUCT_TYPE_NAMES_UPLOAD = 'Importation des noms de types de produits';
	var $MANUFACTURER_IMPORT = 'Importation des d&eacute;tails fabricants';
	var $REGULAR_EXPORT = 'Exportation de produits';
	var $PRODUCT_FILES_EXPORT = 'Exportation de fichiers produits';
	var $PRODUCT_TYPE_EXPORT = 'Exportation de types de produits';
	var $PRODUCT_TYPE_PARAMETERS_EXPORT = 'Exportation de param&egrave;tres de types de produits';
	var $PRODUCT_TYPE_XREF_EXPORT = 'Exportation de r&eacute;f&eacute;rences crois&eacute;es de types de produits';
	var $PRODUCT_TYPE_NAMES_EXPORT = 'Exportation des noms de types de produits';
	var $MULTIPLE_PRICES_EXPORT = 'Exportation de prix multiples';
	var $MANUFACTURER_EXPORT = 'Exporter les informations Fabricants';
	var $ORDER_EXPORT = 'Exporter les bons de commandes';
	var $EMPTY_DATABASE = 'Vider la base de donn&eacute;es<B>( ATTENTION !)</B>';
	var $REMOVE_ORPHAN = 'Supprimer les champs orphelins*';
	var $OPTIMIZE_TABLES = 'Optimiser les tables';
	var $INCLUDE_COLUMN_HEADERS = 'Inclure les en-t&ecirc;tes de colonnes';
	var $UPLOAD_SETTINGS = 'Configuration de l\'importation';
	var $UPLOAD_FILE = 'Importer un fichier CSV';
	var $SUBMIT_FILE = 'Envoyer un fichier CSV';
	var $FROM_DIRECTORY = 'Importer depuis un r&eacute;pertoire';
	var $FROM_SERVER = 'Importer depuis le serveur';
	var $EXPORT_TO_FILE = 'Exporter dans le fichier';
	var $EXPORT_TO_LOCAL = 'Exporter vers un chemin local';
	var $SELECT_FIELD_ORDERING = 'S&eacute;lectionner le type d\'ordre des champs';
	var $DEFAULT_ORDERING = 'Ordre par d&eacute;faut';
	var $CUSTOMIZED_ORDERING = 'Ordre  personnalis&eacute;';
	var $EXPORT_ALL = 'Exporter tout';
	var $SUBMIT_EXPORT = 'Exporter tous les produits vers le fichier CSV';
	var $EXPORT_TYPE = 'Type d\'exportation';
	var $IMPORT_TYPE = 'Type d\'importation';	
	var $CONFIGURATION_HEADER = 'Configuration de l\'import/export CSV  ';
	var $SAVE_CHANGES = 'Enregister les modifications';
	var $FIELD_NAME = 'Nom du champ';
	var $COLUMN_HEADER = 'En-t&ecirc;te de colonne';
	var $DEFAULT_VALUE = 'Valeur par d&eacute;faut';
	var $FIELD_ORDERING = 'Ordre des champs';
	var $FIELD_REQUIRED = 'Inclure le champ?';
	var $GENERAL = 'G&eacute;n&eacute;ral';
	var $IMPORT = 'Importer';
	var $EXPORT = 'Exporter';
	var $TEMPLATES = 'Mod&egrave;les';
	var $CHOOSE_TEMPLATE = 'S&eacute;lectionner un mod&egrave;le';
	var $CONFIG = 'Configuration';
	var $NEW_FIELD = 'Nouveau champ';
	var $NUMBER_OF_FIELDS = 'Nombre de champs';
	var $ADD_NEW_FIELD = 'Ajouter un nouveau champ';
	var $AVAILABLE_FIELDS = 'Champs';
	var $DOCUMENTATION = 'Documentation';
	var $CONTINUE_UPLOAD = 'Continuer l\'importation';
	var $CANCEL_UPLOAD = 'Annuler l\'importation';
	var $OUTPUT_CSV_UPLOAD_MESSAGES = 'R&eacute;sultat';
	var $OUTPUT_COUNT = 'Nombre';
	var $OUTPUT_TOTAL = 'Total';
	var $OUTPUT_FILE_IMPORTED = 'Fichier CSV Import&eacute;';
	var $OUTPUT_UPDATED = 'Actualis&eacute;s';
	var $OUTPUT_DELETED = 'Supprim&eacute;';
	var $OUTPUT_ADDED = 'Ajout&eacute;s';
	var $OUTPUT_SKIPPED = 'Ignor&eacute;s';
	var $OUTPUT_INCORRECT = 'Incorrects';
	var $OUTPUT_EMPTY = 'Vid&eacute;s';
	var $OUTPUT_NOSUPPORT = 'Non support&eacute;';
	var $OUTPUT_NOFILES = 'Manquants';
	var $OUTPUT_INFORMATION = 'Information';
	var $NEW_TEMPLATE = 'Nouveau mod&egrave;le...';
	var $OK_TEMPLATE = 'Accepter';
	var $NAME = 'Nom';
	var $DELETE = 'Effacer';
	var $YES = 'Oui';
	var $NO = 'Non';
	var $AVAILABLE_FIELDS_USE = '<H4>Les champs suivants sont disponibles pour l\'importation et l\'exportation(En anglais).</H4><H5>Cette traduction est en cours.</H5>';
	var $MINIMAL_FIELDS = '<H5>IMPORTANT: Les champs suivants sont pour ajouter un nouveau produit: product_sku, product_name y category_path.<BR>Le champs product_sku, doit &ecirc;tre plac&eacute; en premier dans chaque fichier CSV pour que le syst&egrave;me fonctionne correctement.</H5>';
	var $LATEST_ORDER = 'Dernier de la liste: ';
	var $TEMPLATE_SETTINGS = 'Configuration du mod&egrave;le';
	var $TEMPLATE_EXPORT = 'Exportation du mod&egrave;le';
	var $TEMPLATE_FIELDS_EXPORT = 'Exportation de champs de mod&egrave;le';
	var $TEMPLATE_IMPORT = 'Importation de mod&egrave;le';
	var $TEMPLATE_FIELDS_IMPORT = 'Importation de champs de mod&egrave;le';
	var $CUSTOM_FIELD = 'Champ personalis&eacute;';
	var $MAINTENANCE_OPTIONS = 'Options de maintenance';
	var $EXPORT_FORMAT = 'Format d\'exportation';
	var $TYPE = 'Type';
 	var $EXPORT_SITE = 'Site Web';
 	var $CHOOSE_SITE = 'Choisir le site Web...';
 	var $COLUMN_NAME = 'Nom de colonne';
 	var $NO_VIRTUEMART = 'installation valide de Virtuemart  Non trouv&eacute;e. Ce composant ne fonction qu\'avec VirtueMart.';
	var $PHP_TOO_LOW = 'Votre utilisez PHP version [version], CSV Improved exige au moins PHP version 5.0.';
	var $PASTE = 'Coller';
	var $CLEAR = 'Annuler';
	var $STANDARD_FILE_LOCATION = 'Emplacement standard: ';
	var $SORT_CATEGORIES = 'Rier les cat&eacute;gories';
	var $FILTER = 'Filtrer';
 	
 	
	// Strings for the control panel
	var $CP_IMPORT = 'Importer';
	var $CP_EXPORT = 'Exporter';
	var $CP_TEMPLATES = 'Mod&egrave;les';
	var $CP_MAINTENANCE = 'Maintenance';
	var $CP_HELP = 'Aide';
	var $CP_ABOUT = 'A propos';
	
	// Strings for the toolbar
	var $TB_TEMPLATES = 'Mod&egrave;les';
	var $TB_NEW = 'Nouveau';
	var $TB_DELETE = 'Effacer';
	var $TB_EDIT = 'Editer';
	var $TB_CLONE = 'Cloner';
	var $TB_FIELDS = 'Champs';
	var $TB_IMPORT = 'Importer';
	var $TB_EXPORT = 'Exporter';
	var $TB_CONTINUE = 'Continuer';
	var $CP_BACK = 'Retour';
	var $TB_RENUMBER = 'Renuméroter';
	var $TB_CUSTOM = 'Personnaliser';

	// Strings for the template
	var $TEMPLATE_THUMB_WIDTH = 'Largeur de la miniature';
	var $TEMPLATE_THUMB_HEIGHT = 'Hauteur de la miniature';
	var $TEMPLATE_EXPORT_SHOPPER_GROUP = 'Nom du groupe clients';
	var $TEMPLATE_EXPORT_SHOPPER_GROUP_ALL = 'Tous';
	var $EXPORT_PRODUCTURL_SUFFIX = 'Product URL suffix';
	var $SHOPPER_SITES = 'Shopper sites(?)';
	var $TEMPLATE_FILE_LOCATION = 'Emplacement du fichier';
	var $NO_FIELDS = 'Champs non trouv&eacute;s';
	var $NO_FIELD_NAME = 'Nom de champ non sp&eacute;ecifi&eacute;';
	var $CATEGORY_FIELDS_IMPORT = 'Importation de d&eacute;tails de Cat&eacute;gories';
	var $CATEGORY_FIELDS_EXPORT = 'Exportation de d&eacute;tails de Cat&eacute;gories';
	var $TEMPLATE_EXPORT_PRODUCT_PUBLISH = 'Exportation de produits avec &eacute;tat';
	var $TEMPLATE_EXPORT_PRODUCT_PUBLISHED_BOTH = 'Les deux';
	var $TEMPLATE_EXPORT_PRODUCT_PUBLISHED = 'Publi&eacute;';
	var $TEMPLATE_EXPORT_PRODUCT_UNPUBLISHED = 'D&eacute;publi&eacute;';
	
	// Strings for export page
	var $EXPORT_NUMBER_RECORDS = 'Nombre d\'enregistrements &agrave; exporter';
	var $EXPORT_START_RECORD = 'Commencer &agrave; l\'enregistrement num&eacute;ro';
	var $CHOOSE_TEMPLATE_NO_FIELDS = '*Les mod&egrave;les sans champs n\'apparaissent pas';
	var $EXPORT_TO_DOWNLOAD = 'T&eacute;l&eacute;charger le fichier';
	var $NO_TEMPLATE_FIELDS = 'Aucun mod&egrave;le avec champs d\'exportation trouv&eacute;';
	var $EXPORT_TEMPLATE_FIELDS = 'Utiliser comme mod&egrave;le ou champs de mod&egrave;le d\'exportation';
	
	// Strings for import page
	var $UPLOAD_LIMITS = 'limites syst&egrave;me';
	var $MAX_FILE_POST = 'Taille maximum des fichiers : ';
	var $MAX_FILE_UPLOAD = 'Taille maximum des fichiers : ';
	var $MEMORY_LIMIT = 'M&eacute;moire maximum disponible: ';
	var $EXECUTION_TIME = 'Temps maximum d\'&eacute;x&eacute;cution ';
	var $IMPORT_SECONDS = ' secondes';
	var $IMPORT_CONVERT_FILE = 'conversion UTF-8 &lt;---&gt; ISO-8859-1';
	var $CONVERT_IMPORT = 'Convertir le fichier d\'import vers ';
	var $ISO_IMPORT = 'ISO-8859-1';
	var $UTF_IMPORT = 'UTF-8';
	var $EXPLAIN_ICONV = 'PHP function iconv non disponible sur votre syst&egrave;me.Une conversion automatique est donc impossible. Pour plus d/informations, voir <a href="http://www.csvimproved.com/wiki/doku.php/csvimproved:iconv" alt="iconv explanation" target="_blank">page</a>.';
	var $NO_MEMORY_GET_USAGE = ' PHP function memory_get_usage non disponible sur votre syst&egrave;me. D&eacute;tection du d&eacute;passement  de capacit&eacute; m&eacute;moire deacute;sactiveacute;.';
	
	// Strings for result page
	var $EXECUTION_TIME_PASSED = 'Temps maximum d/&eacute;x&eacute;cution sur le point d/&ecirc;tre d&eacute;pass&eacute;. Temps &eacute;coul&eacute;: ';
	var $MEMORY_LIMIT_PASSED = 'Limite m&eacute;moire sur le point d/&ecirc;tre d&eacute;pass&eacute;. M&eacute;moire utilis&eacute;e: ';
	var $IMPORT_MB = ' Mo';
	
	// Strings for template list page
	var $TEMPLATE_SHOPPER_GROUP_NAME = 'Nom du groupe de clients';
	
	// Strings for the template fields page
	var $TEMPLATE_FIELDS_PAGE_PUBLISHED = 'Champ(s)  publi&eacute;(s)';
	var $TEMPLATE_FIELDS_PAGE_NOT_PUBLISHED = 'Champ(s) non publi&eacute;(s)';
	var $TEMPLATE_FIELDS_PAGE_UNPUBLISHED = 'Champ(s)  d&eacute;publi&eacute;(s)';
	var $TEMPLATE_FIELDS_PAGE_NOT_UNPUBLISHED = 'Champ(s) non d&eacute;publi&eacute;(s)';
	var $TEMPLATE_FIELDS_PAGE_CUSTOM_ADDED = ' Champ personnali&eacute; ajout&eacute;';
	var $TEMPLATE_FIELDS_PAGE_CUSTOM_NOT_ADDED = 'Champ personnali&eacute; non ajout&eacute;';
}
$CsviLang =& new CsviLanguage();
?>