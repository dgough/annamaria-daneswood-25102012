<?php
/**
 * Spanish language file
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Language
 * @author Joachim Schubert
 * @author Juan Ferrari
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_spanish.php 286 2008-06-01 02:51:30Z Suami $
 *
 */
 
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * Spanish language file for CSV Improved
 *
 * @package CSVImproved
 * @subpackage Languages
 */
class CsviLanguage {
    var $UPLOAD = 'Usar CSVi';
	var $VERSION = '0.8 beta 3';
	var $ABOUT = 'Acerca';
	var $DELIMITER = 'Separador de Texto:';
	var $ENCLOSURE = 'Separador de Campo:';
	var $SKIP_FIRST_LINE = 'Saltar primera linea:';
	var $SKIP_DEFAULT_VALUE = 'Ignorar valor predefinido:';
	var $OVERWRITE_EXISTING_DATA = 'Reemplazar datos:';
	var $MULTIPLE_PRICES_UPLOAD = 'Carga de múltiples listas de precios';
	var $IMPORT_CONFIG_CSV_FILE = 'Usar Encabezado de columna como configuración:';
	var $COLLECT_DEBUG_INFO = 'Registrar eventos para depuración:';
	var $SHOW_PREVIEW = 'Mostrar Vista previa:';
	var $REGULAR_UPLOAD = 'Carga normal';
	var $PRODUCT_FILES_UPLOAD = 'Carga de archivos de producto';
	var $PRODUCT_TYPE_UPLOAD = 'Carga de tipos de producto';
	var $PRODUCT_TYPE_PARAMETERS_UPLOAD = 'Carga de parámetros de tipos de producto';
	var $PRODUCT_TYPE_XREF_UPLOAD = 'Carga de referencia cruzada de tipos de producto';
	var $PRODUCT_TYPE_NAMES_UPLOAD = 'Carga de nombres de tipos de producto';
	var $MANUFACTURER_IMPORT = 'Carga de detalles de fabricante';
	var $REGULAR_EXPORT = 'Exportación de Datos Normal';
	var $PRODUCT_FILES_EXPORT = 'Exportación de Archivos de Producto';
	var $PRODUCT_TYPE_EXPORT = 'Exportación de tipos de producto';
	var $PRODUCT_TYPE_PARAMETERS_EXPORT = 'Exportación de parámetros de tipos de producto';
	var $PRODUCT_TYPE_XREF_EXPORT = 'Exportación de referencia cruzada de tipos de producto';
	var $PRODUCT_TYPE_NAMES_EXPORT = 'Exportación de nombres de tipos de producto';
	var $MULTIPLE_PRICES_EXPORT = 'Exportación de múltiples precios';
	var $MANUFACTURER_EXPORT = 'Exportación de detalles de fabricante';
	var $ORDER_EXPORT = 'Exportación de órdenes';
	var $EMPTY_DATABASE = 'Vaciar Base Datos <B>( CUIDADO )</B>';
	var $REMOVE_ORPHAN = 'Remover campos huérfanos';
	var $OPTIMIZE_TABLES = 'Optimizar tablas de Datos';
	var $INCLUDE_COLUMN_HEADERS = 'Encabezados de columna Incluidos:';
	var $UPLOAD_SETTINGS = 'Configuración de Importación';
	var $UPLOAD_FILE = 'Subir Archivo CSV';
	var $SUBMIT_FILE = 'Procesar Archivo CSV';
	var $FROM_DIRECTORY = 'Cargar desde un directorio en el servidor';
	var $FROM_SERVER = 'Cargar archivo CSV desde el servidor';
	var $EXPORT_TO_FILE = 'Exportar a archivo';
	var $EXPORT_TO_LOCAL = 'Exportar a directorio local';
	var $SELECT_FIELD_ORDERING = 'Seleccione el tipo de orden del campo';
	var $DEFAULT_ORDERING = 'Ordenamiento por defecto';
	var $CUSTOMIZED_ORDERING = 'Mi ordenamiento personalizado';
	var $EXPORT_ALL = 'Exportar todos';
	var $SUBMIT_EXPORT = 'Exportar todos los Productos a un archivo CSV';
	var $EXPORT_TYPE = 'Tipo de Exportación';
	var $IMPORT_TYPE = 'Tipo de Importación';	
	var $CONFIGURATION_HEADER = 'Configuración Importación / Exportación de CSV ';
	var $SAVE_CHANGES = 'Guardar Cambios';
	var $FIELD_NAME = 'Campo:';
	var $COLUMN_HEADER = 'Encabezado de Columna:';
	var $DEFAULT_VALUE = 'Valor por Defecto:';
	var $FIELD_ORDERING = 'Orden en Lista:';
	var $FIELD_REQUIRED = 'Incluir Campo?';
	var $GENERAL = 'General';
	var $IMPORT = 'Importar';
	var $EXPORT = 'Exportar';
	var $TEMPLATES = 'Plantillas';
	var $CHOOSE_TEMPLATE = 'Seleccione la Plantilla';
	var $CONFIG = 'Configuración';
	var $NEW_FIELD = 'Agregar un nuevo campo';
	var $NUMBER_OF_FIELDS = 'Número de Campos:';
	var $ADD_NEW_FIELD = 'Agregar nuevo Campo';
	var $AVAILABLE_FIELDS = 'Campos';
	var $DOCUMENTATION = 'Documentación';
	var $CONTINUE_UPLOAD = 'Continuar Descarga';
	var $CANCEL_UPLOAD = 'Cancelar Descarga';
	var $OUTPUT_CSV_UPLOAD_MESSAGES = 'Resultado:';
	var $OUTPUT_COUNT = 'Contados';
	var $OUTPUT_TOTAL = 'Total';
	var $OUTPUT_FILE_IMPORTED = 'Archivo CSV Importado';
	var $OUTPUT_UPDATED = 'Actualizados';
	var $OUTPUT_DELETED = 'Borrados';
	var $OUTPUT_ADDED = 'Agregados';
	var $OUTPUT_SKIPPED = 'Saltados';
	var $OUTPUT_INCORRECT = 'Incorrectos';
	var $OUTPUT_EMPTY = 'Vaciados';
	var $OUTPUT_NOSUPPORT = 'No Soportado';
	var $OUTPUT_NOFILES = 'Faltante';
	var $OUTPUT_INFORMATION = 'Información';
	var $NEW_TEMPLATE = 'Nueva Plantilla...';
	var $OK_TEMPLATE = 'Aceptar';
	var $NAME = 'Nombre:';
	var $DELETE = 'Borrar';
	var $YES = 'Si';
	var $NO = 'No';
	var $AVAILABLE_FIELDS_USE = '<H4>Los siguientes campos están disponibles para ser importados y exportados (En Ingles).</H4><H5>Esta traducción  esta pendiente. Puedes colaborar con este proyecto traduciendo este segmento al español</H5>';
	var $MINIMAL_FIELDS = '<H5>IMPORTANTE: Los siguientes campos son indispensables para agregar un nuevo producto: product_sku, product_name y category_path.<BR>El campo product_sku, debe ser único y debe estar presente en cada archivo CSV para que el sistema funcione correctamente.</H5>';
	var $LATEST_ORDER = 'Ultimo en lista: ';
	var $TEMPLATE_SETTINGS = 'Configuración de Plantilla';
	var $TEMPLATE_EXPORT = 'Exportación de Plantilla';
	var $TEMPLATE_FIELDS_EXPORT = 'Exportación de campos de Plantilla';
	var $TEMPLATE_IMPORT = 'Importación de Plantilla';
	var $TEMPLATE_FIELDS_IMPORT = 'Importación de campos de Plantilla';
	var $CUSTOM_FIELD = 'Campo personalizado';
	var $MAINTENANCE_OPTIONS = 'Opciones de Mantenimiento:';
	var $EXPORT_FORMAT = 'Formato de Exportación';
	var $TYPE = 'Tipo';
 	var $EXPORT_SITE = 'Sitio Web';
 	var $CHOOSE_SITE = 'Escoja Sitio Web...';
	var $COLUMN_NAME = 'Nombre de columna';
	var $NO_VIRTUEMART = 'No hay una instalación válida de Virtuemart. Este componente requiere Virtuemart para operar correctamente.';
	var $PHP_TOO_LOW = 'Está corriendo PHP [version], CSV Improved requiere al menos PHP versión 5.0.';
	var $PASTE = 'Pebar';
	var $CLEAR = 'Borrar';
	var $STANDARD_FILE_LOCATION = 'Ubicación de archivo por defecto: ';
	var $SORT_CATEGORIES = 'Ordenar categorías';
	var $FILTER = 'Filtrar';
	
	// Strings for the control panel
	var $CP_IMPORT = 'Importar';
	var $CP_EXPORT = 'Exportar';
	var $CP_TEMPLATES = 'Plantillas';
	var $CP_MAINTENANCE = 'Mantenimiento';
	var $CP_HELP = 'Ayuda';
	var $CP_ABOUT = 'Acerca de';
	
	// Strings for the toolbar
	var $TB_TEMPLATES = 'Plantillas';
	var $TB_NEW = 'Nuevo';
	var $TB_DELETE = 'Borrar';
	var $TB_EDIT = 'Editar';
	var $TB_CLONE = 'Clone';
	var $TB_FIELDS = 'Campos';
	var $TB_IMPORT = 'Importar';
	var $TB_EXPORT = 'Exportar';
	var $TB_CONTINUE = 'Adelante';
	var $TB_BACK = 'Atrás';
	var $TB_RENUMBER = 'Renumber';
	var $TB_CUSTOM = 'Custom';
	
	// Strings for the template
	var $TEMPLATE_THUMB_WIDTH = 'Thumbnail ancho';
	var $TEMPLATE_THUMB_HEIGHT = 'Thumbnail alto';
	var $TEMPLATE_EXPORT_SHOPPER_GROUP = 'Nombre grupo de compradores';
	var $TEMPLATE_EXPORT_SHOPPER_GROUP_ALL = 'Todos';
	var $EXPORT_PRODUCTURL_SUFFIX = 'Sufijo de URL de producto';
	var $SHOPPER_SITES = 'Sitios de compradores';
	var $TEMPLATE_FILE_LOCATION = 'Ubicación de archivo';
	var $NO_FIELDS = 'No se encontraron campos';
	var $NO_FIELD_NAME = 'No se ha especificado nombres de campos';
	var $CATEGORY_FIELDS_IMPORT = 'Carga de detalles de categoría';
	var $CATEGORY_FIELDS_EXPORT = 'Exportación de detalles de categoría';
	var $TEMPLATE_EXPORT_PRODUCT_PUBLISH = 'Exportar productos con estado';
	var $TEMPLATE_EXPORT_PRODUCT_PUBLISHED_BOTH = 'Ámbos';
	var $TEMPLATE_EXPORT_PRODUCT_PUBLISHED = 'Publicados';
	var $TEMPLATE_EXPORT_PRODUCT_UNPUBLISHED = 'No publicados';
	
	// Strings for export page
	var $EXPORT_NUMBER_RECORDS = 'Cantidad de registros a exportar';
	var $EXPORT_START_RECORD = 'Empezar desde el registro número';
	var $CHOOSE_TEMPLATE_NO_FIELDS = '*No se muestran plantillas sin campos';
	var $EXPORT_TO_DOWNLOAD = 'Descargar archivos';
	var $NO_TEMPLATE_FIELDS = 'No se ha encontrado plantilla con campos a exportar';
	var $EXPORT_TEMPLATE_FIELDS = 'Usar para exportar plantilla o campos de plantilla';
	
	// Strings for import page
	var $UPLOAD_LIMITS = 'Límites de sistema';
	var $MAX_FILE_POST = 'Tamaño máximo de archivo por POST: ';
	var $MAX_FILE_UPLOAD = 'Tamaño máximo de archivo a subir: ';
	var $MEMORY_LIMIT = 'Memoria máxima disponible: ';
	var $EXECUTION_TIME = 'Máximo tiempo de ejecución: ';
	var $IMPORT_SECONDS = ' segundos';
	var $IMPORT_CONVERT_FILE = 'Conversión de archivos UTF-8 &lt;---&gt; ISO-8859-1';
	var $CONVERT_IMPORT = 'Convertir archivo a ';
	var $ISO_IMPORT = 'ISO-8859-1';
	var $UTF_IMPORT = 'UTF-8';
	var $EXPLAIN_ICONV = 'La función php iconv no está disponible en su sistema, por lo tanto la conversión automática no es posible. Para más información vea esta <a href="http://www.csvimproved.com/wiki/doku.php/csvimproved:iconv" alt="iconv explanation" target="_blank">página</a>.';
	var $NO_MEMORY_GET_USAGE = 'La función PHP memory_get_usage no está disponible en su sistema, por lo tanto la detección de falta de memoria está desactivada.';
	
	// Strings for result page
	var $EXECUTION_TIME_PASSED = 'Máximo tiempo de ejecución a punto de ser excedido. Tiempo transcurrido: ';
	var $MEMORY_LIMIT_PASSED = 'Máximo límite de memoria a punto de ser excedido. Memoria utilizada: ';
	var $IMPORT_MB = ' MB';
	
	// Strings for template list page
	var $TEMPLATE_SHOPPER_GROUP_NAME = 'Nombre de grupo de comprador';
	
	// Strings for the template fields page
	var $TEMPLATE_FIELDS_PAGE_PUBLISHED = 'Campo(s) publicados';
	var $TEMPLATE_FIELDS_PAGE_NOT_PUBLISHED = 'Campo(s) no publicados';
	var $TEMPLATE_FIELDS_PAGE_UNPUBLISHED = 'Campo(s) despublicados';
	var $TEMPLATE_FIELDS_PAGE_NOT_UNPUBLISHED = 'Campo(s) no despublicados';
	var $TEMPLATE_FIELDS_PAGE_CUSTOM_ADDED = 'Campo personalizado agregado';
	var $TEMPLATE_FIELDS_PAGE_CUSTOM_NOT_ADDED = 'Campo personalizado no agregado';
}
$CsviLang =& new CsviLanguage();
?>
