<?php
/**
 * Italian language file
 *
 * CSV Improved
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Language
 * @author Crisalex + Pisu
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_italian.php 286 2008-06-01 02:51:30Z Suami $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * Italian language file for CSV Improved
 *
 * @package CSVImproved
 * @subpackage Languages
 */
class CsviLanguage {
	var $UPLOAD = 'Usa l\'upload CSV';
	var $VERSION = '0.8 RC1';
	var $ABOUT = 'Informazioni su';
	var $DELIMITER = 'Delimitatore di Testo';
	var $ENCLOSURE = 'Delimitatore di Campo';
	var $SKIP_FIRST_LINE = 'Salta la prima linea';
	var $SKIP_DEFAULT_VALUE = 'Salta valori predefiniti';
	var $OVERWRITE_EXISTING_DATA = 'Sovrascrivi dati esistenti';
	var $MULTIPLE_PRICES_UPLOAD = 'Carica prezzi multipli';
	var $IMPORT_CONFIG_CSV_FILE = 'Utilizza le intestazioni di colonna come configurazione';
	var $COLLECT_DEBUG_INFO = 'Raccogli informazioni di debug';
	var $SHOW_PREVIEW = 'Mostra anteprima';
	var $REGULAR_UPLOAD = 'Caricamento Regolare';
	var $PRODUCT_FILES_UPLOAD = 'Caricamento File prodotto';
	var $PRODUCT_TYPE_UPLOAD = 'Caricamento Tipi di prodotto';
	var $PRODUCT_TYPE_PARAMETERS_UPLOAD = 'Caricamento Parametri tipo di prodotto';
	var $PRODUCT_TYPE_XREF_UPLOAD = 'Caricamento Riferimenti incrociati tipo di prodotto';
	var $PRODUCT_TYPE_NAMES_UPLOAD = 'Caricamento Nome Tipo di Prodotto';
	var $MANUFACTURER_IMPORT = 'Importa dettagli produttore';
	var $REGULAR_EXPORT = 'Prodotto esportato';
	var $PRODUCT_FILES_EXPORT = 'File Prodotto esportato';
	var $PRODUCT_TYPE_EXPORT = 'Esporta Tipo di Prodotto';
	var $PRODUCT_TYPE_PARAMETERS_EXPORT = 'Esporta Parametri tipo di Prodotto';
	var $PRODUCT_TYPE_XREF_EXPORT = 'Caricamento Riferimenti incrociati tipo di prodotto';
	var $PRODUCT_TYPE_NAMES_EXPORT = 'Esporta Nome tipo di Prodotto';
	var $MULTIPLE_PRICES_EXPORT = 'Esporta prezzi multipli';
	var $MANUFACTURER_EXPORT = 'Esporta dettagli produttore';
	var $EMPTY_DATABASE = 'Svuota il Database';
	var $REMOVE_ORPHAN = 'Rimuovi campi orfani';
	var $OPTIMIZE_TABLES = 'Ottimizza le tabelle';
	var $INCLUDE_COLUMN_HEADERS = 'Include column headers';
	var $UPLOAD_SETTINGS = 'Settaggio Importazione';
	var $UPLOAD_FILE = 'Importa un File CSV';
	var $SUBMIT_FILE = 'Submit CSV File';
	var $FROM_DIRECTORY = 'Carica dalla cartella';
	var $FROM_SERVER = 'Carica il file dal server';
	var $EXPORT_TO_FILE = 'Export to file';
	var $SELECT_FIELD_ORDERING = 'Scegli Tipo di ordine dei campi';
	var $DEFAULT_ORDERING = 'Ordine di default';
	var $CUSTOMIZED_ORDERING = 'Mio ordine Personalizzato';
	var $EXPORT_ALL = 'Esporta Tutto';
	var $SUBMIT_EXPORT = 'Esporta tutti i prodotti in un File CSV';
	var $EXPORT_TYPE = 'Tipo di Esportazione';
	var $IMPORT_TYPE = 'Tipo di Import';
	var $CONFIGURATION_HEADER = 'Configurazione Importa/Esporta CSV';
	var $SAVE_CHANGES = 'Salva i cambiamenti';
	var $FIELD_NAME = 'Nome campo';
	var $COLUMN_HEADER = 'Column Header';
	var $DEFAULT_VALUE = 'Valore di Default';
	var $FIELD_ORDERING = 'Campo di ordine';
	var $FIELD_REQUIRED = 'Campo richiesto?';
	var $GENERAL = 'Generale';
	var $IMPORT = 'Importa';
	var $EXPORT = 'Esporta';
	var $TEMPLATES = 'Template';
	var $CHOOSE_TEMPLATE = 'Scegli il template';
	var $CONFIG = 'Configurazione';
	var $NEW_FIELD = 'Nuovo campo';
	var $NUMBER_OF_FIELDS = 'Numero di campi';
	var $ADD_NEW_FIELD = 'Aggiungi un Nuovo campo';
	var $AVAILABLE_FIELDS = 'Campi disponibili';
	var $DOCUMENTATION = 'Documentazione';
	var $CONTINUE_UPLOAD = 'Continua caricamento';
	var $CANCEL_UPLOAD = 'Annulla caricamento';
	var $OUTPUT_CSV_UPLOAD_MESSAGES = 'Messaggi di caricamento CSV:';
	var $OUTPUT_COUNT = 'Conteggio';
	var $OUTPUT_TOTAL = 'Totale';
	var $OUTPUT_FILE_IMPORTED = 'File CSV Importato';
	var $OUTPUT_UPDATED = 'Aggiornati';
	var $OUTPUT_DELETED = 'Eliminati';
	var $OUTPUT_ADDED = 'Aggiungi';
	var $OUTPUT_SKIPPED = 'Saltati';
	var $OUTPUT_INCORRECT = 'Errati';
	var $OUTPUT_EMPTY = 'Svuotato';
	var $OUTPUT_NOSUPPORT = 'Non supportato';
	var $OUTPUT_NOFILES = 'File non trovato';
	var $OUTPUT_INFORMATION = 'Informazioni';
	var $NEW_TEMPLATE = 'Nuovo template...';
	var $OK_TEMPLATE = 'OK';
	var $NAME = 'Nome';
	var $DELETE = 'Elimina';
	var $YES = 'Si';
	var $NO = 'No';
	var $PUBLISHED = 'Pubblicato';
	var $AVAILABLE_FIELDS_USE = 'I seguenti campi sono disponibili per l\'uso, per l\'importazione o l\'esportazione.';
	var $LATEST_ORDER = 'Ultimo ordine: ';
	var $TEMPLATE_SETTINGS = 'Settaggio Template';
	var $TEMPLATE_EXPORT = 'Esporta Template';
	var $TEMPLATE_FIELDS_EXPORT = 'Esporta Campi del Template';
	var $TEMPLATE_IMPORT = 'Importa Template';
	var $TEMPLATE_FIELDS_IMPORT = 'Importa campi del Template';
	var $CUSTOM_FIELD = 'campo personalizzato';
	var $MAINTENANCE_OPTIONS = 'Opzioni di Manutenzione';
	var $EXPORT_FORMAT = 'Formato di Esportazione';
	var $TYPE = 'Tipo';
	var $EXPORT_SITE = 'Sito';
	var $CHOOSE_SITE = 'Scegli il sito...';
	var $COLUMN_NAME = 'Nome Colonna';
	var $NO_VIRTUEMART = 'Non � stata trovata nessuna installazione VirtueMart valida. Questo componente richiede VirtueMart per operare correttamente.';
	var $PHP_TOO_LOW = 'Stai eseguendo la versione PHP [version], CSV Improved richiede almeno PHP versione 5.0.';
	var $PASTE = 'Incolla';
	var $CLEAR = 'Azzera';
	var $STANDARD_FILE_LOCATION = 'Posizione file standard: ';
	var $SORT_CATEGORIES = 'Ordina Categorie';
	
	// Stringhe per il pannello di controllo
	var $CP_IMPORT = 'Importa';
	var $CP_EXPORT = 'Esporta';
	var $CP_TEMPLATES = 'Template';
	var $CP_MAINTENANCE = 'Manutenzione';
	var $CP_HELP = 'Aiuto';
	var $CP_ABOUT = 'Informazioni su';
	
	// Strings for the toolbar
	var $TB_TEMPLATES = 'Template';
	var $TB_NEW = 'Nuovo';
	var $TB_DELETE = 'Elimina';
	var $TB_EDIT = 'Modifica';
	var $TB_FIELDS = 'Campi';
	var $TB_IMPORT = 'Importa';
	var $TB_EXPORT = 'Esporta';
	var $TB_CONTINUE = 'Continua';
	var $TB_BACK = 'Indietro';
	var $TB_RENUMBER = 'Rinumera';
	var $TB_CUSTOM = 'Personalizzato';
	
	// Strings for the template
	var $TEMPLATE_THUMB_WIDTH = 'Larghezza miniatura';
	var $TEMPLATE_THUMB_HEIGHT = 'Altezza miniatura';
	var $TEMPLATE_EXPORT_SHOPPER_GROUP = 'Nome gruppo clienti';
	var $TEMPLATE_EXPORT_SHOPPER_GROUP_ALL = 'Tutti';
	var $EXPORT_PRODUCTURL_SUFFIX = 'Suffisso URL prodotto';
	var $SHOPPER_SITES = 'Siti clienti';
	var $TEMPLATE_FILE_LOCATION = 'Posizione file';
	var $NO_FIELDS = 'Nessun campo trovato';
	var $NO_FIELD_NAME = 'Nessun nome campo specificato';
	var $CATEGORY_FIELDS_IMPORT = 'Importazione dettagli categoria';
	var $CATEGORY_FIELDS_EXPORT = 'Esportazione dettagli categoria';
	var $TEMPLATE_EXPORT_PRODUCT_PUBLISH = 'Esporta prodotti con stato';
	var $TEMPLATE_EXPORT_PRODUCT_PUBLISHED_BOTH = 'Entrambi';
	var $TEMPLATE_EXPORT_PRODUCT_PUBLISHED = 'Pubblicato';
	var $TEMPLATE_EXPORT_PRODUCT_UNPUBLISHED = 'Sospeso';
	
	// Strings for export page
	var $EXPORT_NUMBER_RECORDS = 'Numero di record da esportare';
	var $EXPORT_START_RECORD = 'Inizia dal record numero';
	var $CHOOSE_TEMPLATE_NO_FIELDS = '*I template senza campi non vengono mostrati';
	var $EXPORT_TO_DOWNLOAD = 'Scarica file';
	var $NO_TEMPLATE_FIELDS = 'Nessun template trovato con campi da esportare';
	var $EXPORT_TEMPLATE_FIELDS = 'Usa per esportazione template o campi template';
	
	// Strings for import page
	var $UPLOAD_LIMITS = 'Limiti di sistema';
	var $MAX_FILE_POST = 'Dimensione massima caricabile (POST): ';
	var $MAX_FILE_UPLOAD = 'Dimensione massima file: ';
	var $MEMORY_LIMIT = 'Memoria massima utilizzabile: ';
	var $EXECUTION_TIME = 'Tempo di esecuzione massimo: ';
	var $IMPORT_SECONDS = ' secondi';
	var $IMPORT_CONVERT_FILE = 'Conversione file UTF-8 &lt;---&gt; ISO-8859-1';
	var $CONVERT_IMPORT = 'Converti file di importazione a ';
	var $ISO_IMPORT = 'ISO-8859-1';
	var $UTF_IMPORT = 'UTF-8';
	var $EXPLAIN_ICONV = 'La funzione PHP iconv non � disponibile nel tuo sistema. Non � quindi possibile una conversione automatica. Per ulteriori informazioni vedi <a href="http://www.csvimproved.com/wiki/doku.php/csvimproved:iconv" alt="iconv explanation" target="_blank">questa pagina</a>.';
	var $NO_MEMORY_GET_USAGE = 'La funzione PHP memory_get_usage non � disponibile nel tuo sistema. Il rilevamento della memoria esaurita � quindi disabilitato.';
	
	// Strings for result page
	var $EXECUTION_TIME_PASSED = 'Il tempo massimo di esecuzione sta per essere superato. Tempo trascorso: ';
	var $MEMORY_LIMIT_PASSED = 'Il limite massimo di memoria sta per essere superato. Memoria usata: ';
	var $IMPORT_MB = ' MB';
	
	// Strings for template list page
	var $TEMPLATE_SHOPPER_GROUP_NAME = 'Nome gruppo clienti';
	
	// Strings for the template fields page
	var $TEMPLATE_FIELDS_PAGE_PUBLISHED = 'Campo/i pubblicato/i';
	var $TEMPLATE_FIELDS_PAGE_NOT_PUBLISHED = 'Campo/i non pubblicato/i';
	var $TEMPLATE_FIELDS_PAGE_UNPUBLISHED = 'Campo/i sospeso/i';
	var $TEMPLATE_FIELDS_PAGE_NOT_UNPUBLISHED = 'Campo/i non sospeso/i';
	var $TEMPLATE_FIELDS_PAGE_CUSTOM_ADDED = 'Campo personalizzato aggiunto';
	var $TEMPLATE_FIELDS_PAGE_CUSTOM_NOT_ADDED = 'Campo personalizzato non aggiunto';
}
$CsviLang =& new CsviLanguage();
?>
