<?php
/**
 * German language file (deutsche �bersetzung von Ingo Blickling blickling.net)
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Language
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_german.php 286 2008-06-01 02:51:30Z Suami $
 */
 
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * German language file for CSV Improved
 *
 * @package CSVImproved
 * @subpackage Languages
 */
class CsviLanguage {
	var $UPLOAD = 'Verwende CSV Improved';
	var $VERSION = '0.8 beta 3';
	var $ABOUT = '�ber';
	var $DELIMITER = 'Textbegrenzer';
	var $ENCLOSURE = 'Feldbegrenzer';
	var $SKIP_FIRST_LINE = 'Erste Zeile weglassen';
	var $SKIP_DEFAULT_VALUE = 'Standardwert weglassen';
	var $OVERWRITE_EXISTING_DATA = 'Bestehende Daten �berschreiben';
	var $MULTIPLE_PRICES_UPLOAD = 'Mehrere Preise hochladen';
	var $IMPORT_CONFIG_CSV_FILE = 'Spalten�berschriften als Konfiguration verwenden';
	var $COLLECT_DEBUG_INFO = 'Debug Informationen sammeln';
	var $SHOW_PREVIEW = 'Vorschau anzeigen';
	var $REGULAR_UPLOAD = 'Normaler Upload';
	var $PRODUCT_FILES_UPLOAD = 'Produktdaten hochladen';
	var $PRODUCT_TYPE_UPLOAD = 'Produkttypen hochladen';
	var $PRODUCT_TYPE_PARAMETERS_UPLOAD = 'Parameter der Produkttypen hochladen';
	var $PRODUCT_TYPE_XREF_UPLOAD = 'XReferenzen der Produkttypen hochladen';
	var $PRODUCT_TYPE_NAMES_UPLOAD = 'Bezeichnungen der Produkttypen hochladen';
	var $MANUFACTURER_IMPORT = 'Herstellerdetails importieren';
  var $REGULAR_EXPORT = 'Normaler Export';
	var $PRODUCT_FILES_EXPORT = 'Export der Produktdaten';
	var $PRODUCT_TYPE_EXPORT = 'Export der Produkttypen';
	var $PRODUCT_TYPE_PARAMETERS_EXPORT = 'Export der Parameter der Produkttypen';
	var $PRODUCT_TYPE_XREF_EXPORT = 'Export der XReferenzen der Produkttypen';
	var $PRODUCT_TYPE_NAMES_EXPORT = 'Export der Bezeichnungen der Produkttypen';
	var $MULTIPLE_PRICES_EXPORT = 'Export der Mehrfachpreise';
	var $EMPTY_DATABASE = 'Datenbank leeren';
	var $REMOVE_ORPHAN = 'Verwaiste Felder entfernen';
  var $OPTIMIZE_TABLES = 'Tabellen optimieren';
  var $INCLUDE_COLUMN_HEADERS = 'Spalten�berschriften einbeziehen';
	var $UPLOAD_SETTINGS = 'Einstellungen hochladen';
	var $UPLOAD_FILE = 'CSV-Datei hochladen';
	var $SUBMIT_FILE = 'CSV-Datei einf�gen';
	var $FROM_DIRECTORY = 'Aus Verzeichnis hochladen';
	var $FROM_SERVER = 'Vom Server hochladen';
 	var $EXPORT_TO_FILE = 'Als Datei exportieren';
	var $EXPORT_TO_LOCAL = 'Auf Server ablegen';
  var $SELECT_FIELD_ORDERING = 'Art der Reihenfolge der Felder ausw�hlen';
	var $DEFAULT_ORDERING = 'Standardreihenfolge';
	var $CUSTOMIZED_ORDERING = 'Angepasste Reihenfolge';
	var $EXPORT_ALL = 'Alle exportieren';
	var $SUBMIT_EXPORT = 'Alle Produkte als CSV-Datei exportieren';
 	var $EXPORT_TYPE = 'Typ des Exports';
 	var $IMPORT_TYPE = 'Typ des Imports';
  var $CONFIGURATION_HEADER = 'Konfiguration des CSV Imports / Exports';
	var $SAVE_CHANGES = '�nderungen speichern';
	var $FIELD_NAME = 'Feldbezeichnung';
 	var $COLUMN_HEADER = 'Spalten�berschrift';
  var $DEFAULT_VALUE = 'Standardwert';
	var $FIELD_ORDERING = 'Feldreihenfolge';
	var $FIELD_REQUIRED = 'Feld ben�tigt?';
	var $GENERAL = 'Allgemein';
	var $IMPORT = 'Import';
	var $EXPORT = 'Export';
	var $TEMPLATES = 'Templates';
 	var $CHOOSE_TEMPLATE = 'Vorlage ausw�hlen';
	var $CONFIG = 'Konfiguration';
	var $NEW_FIELD = 'Neues Feld hinzuf�gen';
 	var $NUMBER_OF_FIELDS = 'Anzahl der Felder';
 	var $ADD_NEW_FIELD = 'Neues Feld hinzuf�gen';
	var $AVAILABLE_FIELDS = 'Felder';
	var $DOCUMENTATION = 'Dokumentation';
	var $CONTINUE_UPLOAD = 'Upload fortsetzen';
	var $CANCEL_UPLOAD = 'Upload abbrechen';
 	var $OUTPUT_CSV_UPLOAD_MESSAGES = 'Ergebnisse:';
	var $OUTPUT_COUNT = 'Anzahl';
	var $OUTPUT_TOTAL = 'Gesamt';
	var $OUTPUT_FILE_IMPORTED = 'CSV-Datei importiert';
	var $OUTPUT_UPDATED = 'Aktualisiert';
	var $OUTPUT_DELETED = 'Gel�scht';
	var $OUTPUT_ADDED = 'Hinzugef�gt';
	var $OUTPUT_SKIPPED = '�bersprungen';
	var $OUTPUT_INCORRECT = 'Inkorrekt';
	var $OUTPUT_EMPTY = 'Geleert';
	var $OUTPUT_NOSUPPORT = 'Nicht unterst�tzt';
	var $OUTPUT_NOFILES = 'Fehlend';
 	var $OUTPUT_INFORMATION = 'Informationen';
	var $NEW_TEMPLATE = 'Neues Template...';
	var $OK_TEMPLATE = 'OK';
	var $NAME = 'Name';
	var $DELETE = 'L�schen';
	var $YES = 'Ja';
	var $NO = 'Nein';
	var $AVAILABLE_FIELDS_USE = 'Die folgenden Felder sind f�r den Import und den Export verf�gbar.';
 	var $LATEST_ORDER = 'Letzter Befehl: ';
 	var $TEMPLATE_SETTINGS = 'Template-Einstellungen';
 	var $TEMPLATE_EXPORT = 'Template-Export';
 	var $TEMPLATE_FIELDS_EXPORT = 'Export der Template-Felder';
 	var $TEMPLATE_IMPORT = 'Template-Import';
 	var $TEMPLATE_FIELDS_IMPORT = 'Import der Template-Felder';
 	var $CUSTOM_FIELD = 'Angepasstes Feld';
 	var $MAINTENANCE_OPTIONS = 'Instandhaltungsoptionen';
 	var $EXPORT_FORMAT = 'Export-Format';
 	var $TYPE = 'Typ';
 	var $EXPORT_SITE = 'Webseite';
 	var $CHOOSE_SITE = 'Webseite ausw�hlen';
	var $COLUMN_NAME = 'Spaltenname';
	var $NO_VIRTUEMART = 'Es wurde keine g�ltige Virtuemart Installation gefunden. Diese Komponente ben�tigt VirtueMart um richtig zu funktionieren.';
	var $PHP_TOO_LOW = 'Sie benutzen PHP version [version], CSV Improved ben�tigt mindestens PHP version 5.0.';
	var $PASTE = 'Einf�gen';
	var $CLEAR = 'Clear';
	var $STANDARD_FILE_LOCATION = 'Standarddateipfad: ';
	var $SORT_CATEGORIES = 'Kategorien sortieren';

 	// Strings for the control panel
 	var $CP_IMPORT = 'Import';
 	var $CP_EXPORT = 'Export';
 	var $CP_TEMPLATES = 'Templates';
 	var $CP_MAINTENANCE = 'Instandhaltung';
 	var $CP_HELP = 'Hilfe';
 	var $CP_ABOUT = '�ber';
 	
 	// Strings for the toolbar
 	var $TB_TEMPLATES = 'Templates';
 	var $TB_NEW = 'Neu';
 	var $TB_DELETE = 'L�schen';
 	var $TB_EDIT = 'Bearbeiten';
	var $TB_CLONE = 'Klonen';
 	var $TB_FIELDS = 'Felder';
 	var $TB_IMPORT = 'Import';
 	var $TB_EXPORT = 'Export';
 	var $TB_CONTINUE = 'Weiter';
 	var $TB_BACK = 'Zur�ck';
	var $TB_RENUMBER = 'Umnummerieren';
	
	// Strings for the template
	var $TEMPLATE_THUMB_WIDTH = 'Thumbnail breite';
	var $TEMPLATE_THUMB_HEIGHT = 'Thumbnail h�he';
	var $TEMPLATE_EXPORT_SHOPPER_GROUP = 'Shopper Gruppenname';
	var $TEMPLATE_EXPORT_SHOPPER_GROUP_ALL = 'Alle';
	var $EXPORT_PRODUCTURL_SUFFIX = 'Produkt URL suffix';
	var $SHOPPER_SITES = 'Shopper sites';
	var $TEMPLATE_FILE_LOCATION = 'Dateipfad';
	var $NO_FIELDS = 'Keine Felder gefunden';
	var $NO_FIELD_NAME = 'Kein Feldname definiert';
	var $CATEGORY_FIELDS_IMPORT = 'Kategoriedetails importieren';
	var $CATEGORY_FIELDS_EXPORT = 'Kategoriedetails exportieren';
	var $TEMPLATE_EXPORT_PRODUCT_PUBLISH = 'Export der Produkte mit Status';
	var $TEMPLATE_EXPORT_PRODUCT_PUBLISHED_BOTH = 'Beide';
	var $TEMPLATE_EXPORT_PRODUCT_PUBLISHED = 'Ver�ffentlicht';
	var $TEMPLATE_EXPORT_PRODUCT_UNPUBLISHED = 'Unver�ffentlicht';
	
	// Strings for export page
	var $EXPORT_NUMBER_RECORDS = 'Anzahl der zu exportierenden Datens�tze';
	var $EXPORT_START_RECORD = 'Beginne ab Zeile';
	var $CHOOSE_TEMPLATE_NO_FIELDS = '*Vorlagen ohne Felder werden nicht angezeigt';
	var $EXPORT_TO_DOWNLOAD = 'Datei herunterladen';
	var $NO_TEMPLATE_FIELDS = 'Es wurde keine Vorlage mit Feldern zum exportieren gefunden';
	var $EXPORT_TEMPLATE_FIELDS = 'Benutzung nur f�r Vorlagen- oder Vorlagenfeldexport | Dies ist nicht f�r andere Exports geeignet !!! |';
	
	// Strings for import page
	var $UPLOAD_LIMITS = 'Systembeschr�nkung';
	var $MAX_FILE_POST = 'Maximale Dateigr��e die �bertragen werden kann: ';
	var $MAX_FILE_UPLOAD = 'Maximale Dateigr��e f�r den Upload: ';
	var $MEMORY_LIMIT = 'Maximaler Speicher der benutzt werden kann: ';
	var $EXECUTION_TIME = 'Maximaler Ausf�hrungszeit: ';
	var $IMPORT_SECONDS = ' Sekunden';
	var $IMPORT_CONVERT_FILE = 'Dateiformat UTF-8 &lt;---&gt; ISO-8895-1';
	var $CONVERT_IMPORT = 'Konvertiere die Importdatei nach ';
	var $ISO_IMPORT = 'ISO-8895-1';
	var $UTF_IMPORT = 'UTF-8';
	var $EXPLAIN_ICONV = 'Die PHP Funktion iconv ist auf Ihrem System nicht verf�gbar. Eine automatische Konvertierung ist daher nicht m�glich. F�r weitere Informationen siehe <a href="http://www.csvimproved.com/wiki/doku.php/csvimproved:iconv" alt="iconv explanation" target="_blank">Infoseite</a>.';
	var $NO_MEMORY_GET_USAGE = 'Die PHP Funktio memory_get_usage ist auf Ihrem System nicht verf�gbar. Die Erkennung von Speicherplatzproblemen ist daher deaktiviert.';
	
	// Strings for result page
	var $EXECUTION_TIME_PASSED = 'Die maximale Ausf�hrungszeit ist dabei �berschritten zu werden. Zeit abgelaufen: ';
	var $MEMORY_LIMIT_PASSED = 'Die Speicherbegrenzung ist dabei �berschritten zu werden. Verwendeter Speicher: ';
	var $IMPORT_MB = ' MB';
	
	// Strings for template list page
	var $TEMPLATE_SHOPPER_GROUP_NAME = 'Shopper Gruppenname';
}	
$CsviLang =& new CsviLanguage();
?>
