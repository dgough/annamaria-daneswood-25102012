<?php
/**
 * Russian language file
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Language
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_russian.php 2008-02-09 17:02:56 Genny
 */
 
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * Russian language file for CSV Improved
 *
 * @package CSVImproved
 * @subpackage Languages
 */
class CsviLanguage {
  var $UPLOAD = 'Использовать загрузку CSV';
  var $VERSION = '0.8 RC 2.1';
  var $ABOUT = 'О программе';
  var $DELIMITER = 'Разделитель текста (~ или "" )';
  var $ENCLOSURE = 'Разделитель полей (^ или ,)';
  var $SKIP_FIRST_LINE = 'Не учитывать первую строку';
  var $SKIP_DEFAULT_VALUE = 'Не ставить значение по умолчанию';
  var $OVERWRITE_EXISTING_DATA = 'Заменить существующие данные';
  var $MULTIPLE_PRICES_UPLOAD = 'Загрузка нескольких цен для каждого товара';
  var $IMPORT_CONFIG_CSV_FILE = 'Заголовки как конфигураторы';
  var $COLLECT_DEBUG_INFO = 'Собирать отладочную информацию';
  var $SHOW_PREVIEW = 'Предпросмотр';
  var $REGULAR_UPLOAD = 'Загрузка продуктов';
  var $PRODUCT_FILES_UPLOAD = 'Загрузка файлов продукта';
  var $PRODUCT_TYPE_UPLOAD = 'Загрузка типов продукта';
  var $PRODUCT_TYPE_PARAMETERS_UPLOAD = 'Загрузка параметров для типов';
  var $PRODUCT_TYPE_XREF_UPLOAD = 'Привязка типов к товарам';
  var $PRODUCT_TYPE_NAMES_UPLOAD = 'Загрузка данных для типов';
  var $MANUFACTURER_IMPORT = 'Производитель деталей импорта';
  var $REGULAR_EXPORT = 'Стандартный экспорт';
  var $PRODUCT_FILES_EXPORT = 'Экспорт файлов продукта';
  var $PRODUCT_TYPE_EXPORT = 'Экспорт типов продукта';
  var $PRODUCT_TYPE_PARAMETERS_EXPORT = 'Экспорт параметров для типов';
  var $PRODUCT_TYPE_XREF_EXPORT = 'Экспорт соответствий типов товарам';
  var $PRODUCT_TYPE_NAMES_EXPORT = 'Экспорт данных для типов';
  var $MULTIPLE_PRICES_EXPORT = 'Экспорт нескольких цен для каждого товара';
  //op
  	var $MANUFACTURER_EXPORT = 'Экспорт Производителей (детали)';
	var $ORDER_EXPORT = 'Порядок экспорта или заказы';

  var $EMPTY_DATABASE = 'Очистка базы <span style="color:red;">( Все товары будут удалены!!)</span>';
  var $REMOVE_ORPHAN = 'Удалить "лишние" поля';
  var $OPTIMIZE_TABLES = 'Оптимизировать таблицы';
  var $INCLUDE_COLUMN_HEADERS = 'Импортировать заголовки';
  var $UPLOAD_SETTINGS = 'Параметры загрузки';
  var $UPLOAD_FILE = 'Загрузка CSV файла';
  var $SUBMIT_FILE = 'Загрузить CSV файл';
  var $FROM_DIRECTORY = 'Загрузка из папки';
  var $FROM_SERVER = 'Загрузить CSV файл с сервера';
  var $EXPORT_TO_FILE = 'Экспортировать в CSV файл';
  var $EXPORT_TO_LOCAL = 'Экспорт в локальную папку на сервере';
  var $SELECT_FIELD_ORDERING = 'Укажите очередность полей CSV-файла';
  var $DEFAULT_ORDERING = 'Очередность по умолчанию';
  var $CUSTOMIZED_ORDERING = 'Моя собственная очередность';
  var $EXPORT_ALL = 'Экспортировать всё';
  var $SUBMIT_EXPORT = 'Экспорт всех товаров в CSV файл';
  var $EXPORT_TYPE = 'Тип Экспорта';
  var $IMPORT_TYPE = 'Тип импорта';
  var $CONFIGURATION_HEADER = 'Конфигурация импорта/экспорта CSV';
  var $SAVE_CHANGES = 'Сохранить настройки';
  var $FIELD_NAME = 'Название поля';
  var $COLUMN_HEADER = 'Заголовок Колонки';
  var $DEFAULT_VALUE = 'Значение по умолчанию';
  var $FIELD_ORDERING = 'Очередность полей';
  var $FIELD_REQUIRED = 'Включить поле?';
  var $GENERAL = 'Обычный';
  var $IMPORT = 'Импорт';
  var $EXPORT = 'Экспорт';
  var $TEMPLATES = 'Шаблоны';
  var $CHOOSE_TEMPLATE = 'Выберите шаблон';
  var $CONFIG = ' Настройка';
  var $NEW_FIELD = 'Добавить новое поле';
  var $NUMBER_OF_FIELDS = 'Количество полей';
  var $ADD_NEW_FIELD = 'Добавить новое поле';
  var $AVAILABLE_FIELDS = 'Доступные поля';
  var $DOCUMENTATION = 'Документация';
  var $CONTINUE_UPLOAD = 'Продолжить загрузку';
  var $CANCEL_UPLOAD = 'Отменить загрузку';
  var $OUTPUT_CSV_UPLOAD_MESSAGES = 'Сообщения загрузчика CSV:';
  var $OUTPUT_COUNT = 'Всего кол-во';
  var $OUTPUT_TOTAL = 'Всего';
  var $OUTPUT_FILE_IMPORTED = 'CSV файл импортирован';
  var $OUTPUT_UPDATED = 'Обновлено';
  var $OUTPUT_DELETED = 'Удалено';
  var $OUTPUT_ADDED = 'Добавлено';
  var $OUTPUT_SKIPPED = 'Пропущено (оставлено без изменений)';
  var $OUTPUT_INCORRECT = 'Неправильные';
  var $OUTPUT_EMPTY = 'Очищено';
  var $OUTPUT_NOSUPPORT = 'Не поддерживается';
  var $OUTPUT_NOFILES = 'Пропущено пропавших';
  var $OUTPUT_INFORMATION = 'Информация';
  var $NEW_TEMPLATE = 'Новый шаблон...';
  var $OK_TEMPLATE = 'OK';
  var $NAME = 'Имя шаблона';
  var $DELETE = 'Удалить';
  var $YES = 'Да';
  var $NO = 'Нет';
  var $PUBLISHED = 'Опубликовано';
  var $AVAILABLE_FIELDS_USE = 'Для загрузки/выгрузки доступны следующие поля.';
 var $LATEST_ORDER = 'Последний заказ/сортировка: ';
var $TEMPLATE_SETTINGS = 'Настройки Шаблона '; 
var $TEMPLATE_EXPORT = 'Шаблон Экспорт'; 
var $TEMPLATE_FIELDS_EXPORT = 'Поля Шаблона для экспорта'; 
var $TEMPLATE_IMPORT = 'Шаблон импорта'; 
var $TEMPLATE_FIELDS_IMPORT = 'Шаблон области импорта'; 
var $CUSTOM_FIELD = 'Пользовательские поля'; 
var $MAINTENANCE_OPTIONS = 'Обслуживание варианты'; 
var $EXPORT_FORMAT = 'Формат Экспорта'; 
var $TYPE = 'Тип'; 
var $EXPORT_SITE = 'Сайт'; 
var $CHOOSE_SITE = 'Выбрать сайт ...'; 
var $COLUMN_NAME = 'Имя столбца'; 
var $NO_VIRTUEMART = 'Существует не действительны Virtuemart установки найдено. Этот компонент требует VirtueMart действовать правильно. '; 
var $PHP_TOO_LOW = 'Вы используете версию PHP [version], CSV Улучшение требует как минимум PHP версии 5,0.'; 
var $PASTE = 'Вставить'; 
var $CLEAR = 'Очистить'; 
var $STANDARD_FILE_LOCATION = 'стандарт расположения файлов:'; 
var $SORT_CATEGORIES = 'Упорядочить категории'; 
var $FILTER = 'Фильтр'; 

// Строки для пульта управления 
var $CP_IMPORT = 'Импорт'; 
var $CP_EXPORT = "Экспорт"; 
var $CP_TEMPLATES = 'Шаблоны'; 
var $CP_MAINTENANCE = 'Обслуживание'; 
var $CP_HELP = 'Помощь'; 
var $CP_ABOUT = 'Об этом';
	
	// Strings for the toolbar
	var $TB_TEMPLATES = 'Шаблоны';
	var $TB_NEW = 'Новый';
	var $TB_DELETE = 'Удалить';
	var $TB_EDIT = 'Редактировать';
	var $TB_CLONE = 'Клонировать';
	var $TB_FIELDS = 'Поля';
	var $TB_IMPORT = 'Импорт';
	var $TB_EXPORT = 'Экспорт';
	var $TB_CONTINUE = 'Выполнить';
	var $TB_BACK = 'Назад';
	var $TB_RENUMBER = 'Пронумеровать'; 
var $TB_CUSTOM = 'Пользовательские'; 

//Строки для этого шаблона 
var $TEMPLATE_THUMB_WIDTH = 'Ширина Эскиза'; 
var $TEMPLATE_THUMB_HEIGHT = 'Высота Эскизов';
var $TEMPLATE_EXPORT_SHOPPER_GROUP = 'Название группы покупателя'; 
var $TEMPLATE_EXPORT_SHOPPER_GROUP_ALL = 'Все'; 
var $EXPORT_PRODUCTURL_SUFFIX = 'Продукт URL суффикса'; 
var $SHOPPER_SITES = 'Сайты покупателя'; 
var $TEMPLATE_FILE_LOCATION = 'Местоположение файла'; 
var $NO_FIELDS = 'Полей не найдено'; 
var $NO_FIELD_NAME = 'Нет указанных полей'; 
var $CATEGORY_FIELDS_IMPORT = 'Категория импорта (детали)'; 
var $CATEGORY_FIELDS_EXPORT = 'Категория экспорт (детали)';
var $TEMPLATE_EXPORT_PRODUCT_PUBLISH = 'Экспорт товаров с государствами'; 
var $TEMPLATE_EXPORT_PRODUCT_PUBLISHED_BOTH = 'Оба'; 
var $TEMPLATE_EXPORT_PRODUCT_PUBLISHED = 'Опубликовано'; 
var $TEMPLATE_EXPORT_PRODUCT_UNPUBLISHED = 'Не публикованые'; 

//Строки для экспорта страницу 
var $EXPORT_NUMBER_RECORDS = 'Количество записей на экспорт';
var $EXPORT_START_RECORD ='Начать экспорт с номера строки';
var $CHOOSE_TEMPLATE_NO_FIELDS = '*Шаблоны, без полей, не показаны';
var $EXPORT_TO_DOWNLOAD = 'Загрузить в файл'; 
var $NO_TEMPLATE_FIELDS = 'Нет полей для этого шаблона для экспорта'; 
var $EXPORT_TEMPLATE_FIELDS = 'Используйте для шаблона или шаблон поля экспорт';

//Строки для импорта страницу 
var $UPLOAD_LIMITS = 'Система ограничений'; 
var $MAX_FILE_POST = 'Максимальный размер файла, которые могут быть размещены:'; 
var $MAX_FILE_UPLOAD = 'Максимальный размер файла загрузки:'; 
var $MEMORY_LIMIT = 'Максимальный объем памяти, которые могут быть использовано:'; 
var $EXECUTION_TIME = 'Максимальное время исполнения:'; 
var $IMPORT_SECONDS = 'секунд'; 
var $IMPORT_CONVERT_FILE = 'Конвертирование файлов UTF-8 <---> ISO-8859-1'; 
var $CONVERT_IMPORT = 'Конвертировать импортировать файл'; 
var $ISO_IMPORT = 'ISO-8859-1'; 
var $UTF_IMPORT = 'UTF-8'; 
var $EXPLAIN_ICONV = 'В PHP функция iconv не доступен в вашей системе. Автоматический переход с этим не представляется возможным. Дополнительную информацию см. в этом <a href="http://www.csvimproved.com/wiki/doku.php/csvimproved:iconv" alt="iconv explanation" target="_blank">стр.</a>. '; 
var $NO_MEMORY_GET_USAGE = 'В PHP функция memory_get_usage отсутствуют в вашей системе. Управление памятью недоступно. '; 

//Строки на странице результатов 
var $EXECUTION_TIME_PASSED = 'Максимальное время исполнения должны быть превышены. Время прошло: '; 
var $MEMORY_LIMIT_PASSED = 'Максимальный объем памяти лимит вскоре будет превышен. Память используется: '; 
var $IMPORT_MB = 'MB'; 

//Строки для шаблона странице списка 
var $TEMPLATE_SHOPPER_GROUP_NAME = 'Название группы покупателей'; 

//Строки для этого шаблона поля страницы 
var $TEMPLATE_FIELDS_PAGE_PUBLISHED = 'Поле (я) были опубликованы';
var $TEMPLATE_FIELDS_PAGE_NOT_PUBLISHED = 'Поле (я), не были опубликованы';
var $TEMPLATE_FIELDS_PAGE_UNPUBLISHED = 'Поле (я), были неопубликованные'; 
var $TEMPLATE_FIELDS_PAGE_NOT_UNPUBLISHED = 'Поле (я), не были неопубликованные'; 
var $TEMPLATE_FIELDS_PAGE_CUSTOM_ADDED = 'Пользовательские поля добавил'; 
var $TEMPLATE_FIELDS_PAGE_CUSTOM_NOT_ADDED = 'Пользовательские поля не добавил';
}
$CsviLang =& new CsviLanguage();
?>
