<?php
/**
 * Polish language file - charset: iso-8859-2
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Language
 * @author Roland Dalmulder
 * @author Keran from Quarkbit Software
 * @link http://www.csvimproved.com
 * @link http://www.quarkbitsoftware.pl
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_polish.php 286 2008-06-01 02:51:30Z Suami $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * Polish language file for CSV Improved
 *
 * @package CSVImproved
 * @subpackage Languages
 */
class CsviLanguage {
	var $UPLOAD = 'U�yj CSV Improved';
	var $VERSION = '0.8 Stable';
	var $ABOUT = 'O programie';
	var $DELIMITER = 'Separator tekstu';
	var $ENCLOSURE = 'Separator p�l';
	var $SKIP_FIRST_LINE = 'Pomi� pierwsz� lini�';
	var $SKIP_DEFAULT_VALUE = 'Pomi� warto�ci domy�lne';
	var $OVERWRITE_EXISTING_DATA = 'Nadpisz istniej�ce dane';
	var $PRICE_LIST_ONLY = 'Tylko aktualizacja cennika';
	var $MULTIPLE_PRICES_UPLOAD = 'Import cen wielokrotnych';
	var $IMPORT_CONFIG_CSV_FILE = 'U�yj nag��wk�w do konfiguracji';
	var $COLLECT_DEBUG_INFO = 'Zbieraj info debugera';
	var $SHOW_PREVIEW = 'Poka� podgl�d';
	var $REGULAR_UPLOAD = 'Standardowy import';
	var $PRODUCT_FILES_UPLOAD = 'Import plik�w produkt�w';
	var $PRODUCT_TYPE_UPLOAD = 'Import typ�w produkt�w';
	var $PRODUCT_TYPE_PARAMETERS_UPLOAD = 'Import parametr�w typ�w produkt�w';
	var $PRODUCT_TYPE_XREF_UPLOAD = 'Import macierzy referencji typ�w produkt�w';
	var $PRODUCT_TYPE_NAMES_UPLOAD = 'Import nazw typ�w produkt�w';
	var $MANUFACTURER_IMPORT = 'Import danych producent�w';
	var $REGULAR_EXPORT = 'Standardowy eksport';
	var $PRODUCT_FILES_EXPORT = 'Eksport plik�w produkt�w';
	var $PRODUCT_TYPE_EXPORT = 'Eksport typ�w produkt�w';
	var $PRODUCT_TYPE_PARAMETERS_EXPORT = 'Eksport parametr�w typ�w produkt�w';
	var $PRODUCT_TYPE_XREF_EXPORT = 'Eksport macierzy referencji typ�w produkt�w';
	var $PRODUCT_TYPE_NAMES_EXPORT = 'Eksport nazw typ�w produkt�w';
	var $MULTIPLE_PRICES_EXPORT = 'Eksport cen wielokrotnych';
	var $MANUFACTURER_EXPORT = 'Eksport danych producent�w';
	var $ORDER_EXPORT = 'Eksport zam�wie�';
	var $EMPTY_DATABASE = 'Wyczy�� baz� danych';
	var $REMOVE_ORPHAN = 'Usu� osierocone pola';
	var $OPTIMIZE_TABLES = 'Optymalizuj tabele';
	var $INCLUDE_COLUMN_HEADERS = 'Do��cz nag��wki kolumn';
	var $UPLOAD_SETTINGS = 'Ustawienia importu';
	var $UPLOAD_FILE = 'Importuj plik CSV';
	var $SUBMIT_FILE = 'Importuj plik CSV';
	var $FROM_DIRECTORY = 'Importuj z katalogu';
	var $FROM_SERVER = 'Importuj z serwera';
	var $EXPORT_TO_FILE = 'Eksport do pliku CSV';
	var $EXPORT_TO_LOCAL = 'Eksportuj do scie�ki lokalnej';
	var $SELECT_FIELD_ORDERING = 'Wybierz rodzaj eksportu';
	var $DEFAULT_ORDERING = 'Domy�lna kolejno��';
	var $CUSTOMIZED_ORDERING = 'Moja kolejno��';
	var $EXPORT_ALL = 'Eksportuj wszystko';
	var $SUBMIT_EXPORT = 'Eksportuj wszystkie towary do CSV';
	var $EXPORT_TYPE = 'Typ eksportu';
	var $IMPORT_TYPE = 'Importuj typ';
	var $CONFIGURATION_HEADER = 'Konfiguracja Importu / Eksportu CSV';
	var $SAVE_CHANGES = 'Zapisz zmiany';
	var $FIELD_NAME = 'Nazwa pola';
	var $COLUMN_HEADER = 'Nag��wek kolumny';
	var $DEFAULT_VALUE = 'Warto�� domy�lna';
	var $FIELD_ORDERING = 'Kolejno�� p�l';
	var $FIELD_REQUIRED = 'Uwzgl�dni� pole?';
	var $GENERAL = 'Separatory';
	var $IMPORT = 'Import';
	var $EXPORT = 'Eksport';
	var $TEMPLATES = 'Szablony';
	var $CHOOSE_TEMPLATE = 'Wybierz szablon';
	var $CONFIG = 'Konfiguracja';
	var $NEW_FIELD = 'Dodaj nowe pole';
	var $NUMBER_OF_FIELDS = 'Liczba p�l';
	var $ADD_NEW_FIELD = 'Dodaj nowe pole';
	var $AVAILABLE_FIELDS = 'Dost�pne pola';
	var $DOCUMENTATION = 'Dokumentacja';
	var $CONTINUE_UPLOAD = 'Kontynuuj import';
	var $CANCEL_UPLOAD = 'Przerwij import';
	var $OUTPUT_CSV_UPLOAD_MESSAGES = 'Raport z importu CSV:';
	var $OUTPUT_COUNT = 'Ilo��';
	var $OUTPUT_TOTAL = 'Razem';
	var $OUTPUT_FILE_IMPORTED = 'Zaimportowano plik CSV';
	var $OUTPUT_UPDATED = 'Zmienionych';
	var $OUTPUT_DELETED = 'Skasowanych';
	var $OUTPUT_ADDED = 'Dodanych';
	var $OUTPUT_SKIPPED = 'Pomini�tych';
	var $OUTPUT_INCORRECT = 'B��dy';
	var $OUTPUT_EMPTY = 'Pustych';
	var $OUTPUT_NOSUPPORT = 'Nieobs�ugiwanych';
	var $OUTPUT_NOFILES = 'Zagubionych';
	var $OUTPUT_INFORMATION = 'Informacje';
	var $NEW_TEMPLATE = 'Nowy szablon...';
	var $OK_TEMPLATE = 'OK';
	var $NAME = 'Nazwa';
	var $DELETE = 'Kasuj';
	var $YES = 'Tak';
	var $NO = 'Nie';
	var $PUBLISHED = 'Opublikowany';
	var $AVAILABLE_FIELDS_USE = 'Pola, kt�rych mo�esz u�y� do importu lub eksportu danych.';
	var $LATEST_ORDER = 'Ostatnie zam�wienie: ';
	var $TEMPLATE_SETTINGS = 'Ustawienia szablonu';
	var $TEMPLATE_EXPORT = 'Eksport szablonu';
	var $TEMPLATE_FIELDS_EXPORT = 'Eksport p�l szablonu';
	var $TEMPLATE_IMPORT = 'Import szablonu';
	var $TEMPLATE_FIELDS_IMPORT = 'Import p�l szablonu';
	var $CUSTOM_FIELD = 'Pole u�ytkownika';
	var $MAINTENANCE_OPTIONS = 'Opcje konserwacyjne';
	var $EXPORT_FORMAT = 'Format eksportu';
	var $TYPE = 'Typ';
	var $EXPORT_SITE = 'Strona internetowa';
	var $CHOOSE_SITE = 'Wybierz stron� www...';
	var $COLUMN_NAME = 'Nazwa kolumny';
	var $NO_VIRTUEMART = 'Nie znaleziono instalacji Virtuemart. Ten komponent wymaga Virtuemart do poprawnego dzia�ania.';
	var $PHP_TOO_LOW = 'Twoja wersja PHP to [version], CSV Improved wymaga wersji PHP 5.0 lub wy�szej.';
	var $PASTE = 'Wklej';
	var $CLEAR = 'Wyczy��';
	var $STANDARD_FILE_LOCATION = 'Standardowa lokalizacja pliku: ';
	var $SORT_CATEGORIES = 'Sortowanie kategorii';
	var $FILTER = 'Filtr';
	
	// Strings for the control panel
	var $CP_IMPORT = 'Import';
	var $CP_EXPORT = 'Eksport';
	var $CP_TEMPLATES = 'Szablony';
	var $CP_MAINTENANCE = 'Konserwacja';
	var $CP_HELP = 'Pomoc';
	var $CP_ABOUT = 'O CVSI';
	
	// Strings for the toolbar
	var $TB_TEMPLATES = 'Szablony';
	var $TB_NEW = 'Nowy';
	var $TB_DELETE = 'Usu�';
	var $TB_EDIT = 'Edytuj';
	var $TB_CLONE = 'Kopiuj';
	var $TB_FIELDS = 'Pola';
	var $TB_IMPORT = 'Import';
	var $TB_EXPORT = 'Eksport';
	var $TB_CONTINUE = 'Dalej';
	var $TB_BACK = 'Wstecz';
	var $TB_RENUMBER = 'Przenumeruj';
	var $TB_CUSTOM = 'Dodaj';
	
	// Strings for the template
	var $TEMPLATE_THUMB_WIDTH = 'Szeroko�� miniatury';
	var $TEMPLATE_THUMB_HEIGHT = 'Wysoko�� miniatury';
	var $TEMPLATE_EXPORT_SHOPPER_GROUP = 'Nazwa grupy klient�w';
	var $TEMPLATE_EXPORT_SHOPPER_GROUP_ALL = 'Wszyscy';
	var $EXPORT_PRODUCTURL_SUFFIX = 'Przyrostek URL produktu';
	var $SHOPPER_SITES = 'Strony kupuj�cych';
	var $TEMPLATE_FILE_LOCATION = 'Lokalizacja pliku';
	var $NO_FIELDS = 'Nie znaleziono p�l';
	var $NO_FIELD_NAME = 'Nie zdefiniowano nazwy pola';
	var $CATEGORY_FIELDS_IMPORT = 'Import opisu kategorii';
	var $CATEGORY_FIELDS_EXPORT = 'Eksport opisu kategorii';
	var $TEMPLATE_EXPORT_PRODUCT_PUBLISH = 'Eksportuj produkty ze stanem';
	var $TEMPLATE_EXPORT_PRODUCT_PUBLISHED_BOTH = 'Wszystkie';
	var $TEMPLATE_EXPORT_PRODUCT_PUBLISHED = 'Opublikowany';
	var $TEMPLATE_EXPORT_PRODUCT_UNPUBLISHED = 'Nie opublikowany';
	
	// Strings for export page
	var $EXPORT_NUMBER_RECORDS = 'Liczba rekord�w do wyeksportowania';
	var $EXPORT_START_RECORD = 'Zacznij od rekordu nr';
	var $CHOOSE_TEMPLATE_NO_FIELDS = '*Szablony bez p�l nie s� pokazywane';
	var $EXPORT_TO_DOWNLOAD = 'Pobierz plik';
	var $NO_TEMPLATE_FIELDS = 'Nie znaleziono szablonu z polami do eksportu';
	var $EXPORT_TEMPLATE_FIELDS = 'U�yj dla eksportu szablonu lub p�l szablonu';
	
	// Strings for import page
	var $UPLOAD_LIMITS = 'Limity systemowe';
	var $MAX_FILE_POST = 'Maksymalny rozmiar pliku mo�liwy do wys�ania: ';
	var $MAX_FILE_UPLOAD = 'Maksymalny rozmiar pliku uploadu: ';
	var $MEMORY_LIMIT = 'Maksymalna ilo�� pami�ci mo�liwa do u�ycia: ';
	var $EXECUTION_TIME = 'Maksymalny czas wykonywania skryptu: ';
	var $IMPORT_SECONDS = ' sekund';
	var $IMPORT_CONVERT_FILE = 'Konwersja pliku UTF-8 &lt;---&gt; ISO-8859-1';
	var $CONVERT_IMPORT = 'Konwertuj plik importu do ';
	var $ISO_IMPORT = 'ISO-8859-1';
	var $UTF_IMPORT = 'UTF-8';
	var $EXPLAIN_ICONV = 'Funckja PHP iconv nie jest dost�pna w Twoim systemie. Automatyczna konwersja jest tym samym niemo�liwa. Wi�cej informacji na tej <a href="http://www.csvimproved.com/wiki/doku.php/csvimproved:iconv" alt="iconv explanation" target="_blank">stronie</a>.';
	var $NO_MEMORY_GET_USAGE = 'Funkcja memory_get_usage nie jest dost�pna w Twoim systemie. Wykrywanie przekroczonej ilo�ci pami�ci jest zablokowane.';
	
	// Strings for result page
	var $EXECUTION_TIME_PASSED = 'Przekroczono czas wykonywania skryptu. Przekroczony czas: ';
	var $MEMORY_LIMIT_PASSED = 'Przekroczono maksymalny limit pami�ci. U�yta pami��: ';
	var $IMPORT_MB = ' MB';
	
	// Strings for template list page
	var $TEMPLATE_SHOPPER_GROUP_NAME = 'Nazwa grupy klient�w';
	
	// Strings for the template fields page
	var $TEMPLATE_FIELDS_PAGE_PUBLISHED = 'Pola zosta�y opublikowane';
	var $TEMPLATE_FIELDS_PAGE_NOT_PUBLISHED = 'Pola nie zosta�y opublikowane';
	var $TEMPLATE_FIELDS_PAGE_UNPUBLISHED = 'Pola zosta�y odpublikowane';
	var $TEMPLATE_FIELDS_PAGE_NOT_UNPUBLISHED = 'Pola nie zosta�y odpublikowane';
	var $TEMPLATE_FIELDS_PAGE_CUSTOM_ADDED = 'Dodano pole u�ytkownika';
	var $TEMPLATE_FIELDS_PAGE_CUSTOM_NOT_ADDED = 'Nie dodano pola uzytkownika';
}
$CsviLang =& new CsviLanguage();
?>
