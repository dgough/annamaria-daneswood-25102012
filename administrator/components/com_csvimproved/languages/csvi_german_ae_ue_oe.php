<?php
/**
 * German language file (deutsche Uebersetzung von Ingo Blickling blickling.net)
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Language
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_german_ae_ue_oe.php 286 2008-06-01 02:51:30Z Suami $
 */
 
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * German language file for CSV Improved
 *
 * @package CSVImproved
 * @subpackage Languages
 */
class CsviLanguage {
	var $UPLOAD = 'Verwende CSV Improved';
	var $VERSION = '0.8 beta 3';
	var $ABOUT = 'Ueber';
	var $DELIMITER = 'Textbegrenzer';
	var $ENCLOSURE = 'Feldbegrenzer';
	var $SKIP_FIRST_LINE = 'Erste Zeile weglassen';
	var $SKIP_DEFAULT_VALUE = 'Standardwert weglassen';
	var $OVERWRITE_EXISTING_DATA = 'Bestehende Daten ueberschreiben';
	var $MULTIPLE_PRICES_UPLOAD = 'Mehrere Preise hochladen';
	var $IMPORT_CONFIG_CSV_FILE = 'Spaltenueberschriften als Konfiguration verwenden';
	var $COLLECT_DEBUG_INFO = 'Debug Informationen sammeln';
	var $SHOW_PREVIEW = 'Vorschau anzeigen';
	var $REGULAR_UPLOAD = 'Normaler Upload';
	var $PRODUCT_FILES_UPLOAD = 'Produktdaten hochladen';
	var $PRODUCT_TYPE_UPLOAD = 'Produkttypen hochladen';
	var $PRODUCT_TYPE_PARAMETERS_UPLOAD = 'Parameter der Produkttypen hochladen';
	var $PRODUCT_TYPE_XREF_UPLOAD = 'XReferenzen der Produkttypen hochladen';
	var $PRODUCT_TYPE_NAMES_UPLOAD = 'Bezeichnungen der Produkttypen hochladen';
	var $REGULAR_EXPORT = 'Normaler Export';
	var $PRODUCT_FILES_EXPORT = 'Export der Produktdaten';
	var $PRODUCT_TYPE_EXPORT = 'Export der Produkttypen';
	var $PRODUCT_TYPE_PARAMETERS_EXPORT = 'Export der Parameter der Produkttypen';
	var $PRODUCT_TYPE_XREF_EXPORT = 'Export der XReferenzen der Produkttypen';
	var $PRODUCT_TYPE_NAMES_EXPORT = 'Export der Bezeichnungen der Produkttypen';
	var $MULTIPLE_PRICES_EXPORT = 'Export der Mehrfachpreise';
	var $EMPTY_DATABASE = 'Datenbank leeren';
	var $INCLUDE_COLUMN_HEADERS = 'Spaltenueberschriften einbeziehen';
 	var $REMOVE_ORPHAN = 'Verwaiste Felder entfernen';
 	var $OPTIMIZE_TABLES = 'Tabellen optimieren';
	var $UPLOAD_SETTINGS = 'Einstellungen hochladen';
	var $UPLOAD_FILE = 'CSV-Datei hochladen';
	var $SUBMIT_FILE = 'CSV-Datei einfuegen';
	var $FROM_DIRECTORY = 'Aus Verzeichnis hochladen';
	var $FROM_SERVER = 'Vom Server hochladen';
 	var $EXPORT_TO_FILE = 'Als Datei exportieren';
	var $SELECT_FIELD_ORDERING = 'Art der Reihenfolge der Felder auswaehlen';
	var $DEFAULT_ORDERING = 'Standardreihenfolge';
	var $CUSTOMIZED_ORDERING = 'Angepasste Reihenfolge';
	var $EXPORT_ALL = 'Alle exportieren';
	var $SUBMIT_EXPORT = 'Alle Produkte als CSV-Datei exportieren';
	var $CONFIGURATION_HEADER = 'Konfiguration des CSV Imports / Exports';
 	var $EXPORT_TYPE = 'Typ des Exports';
 	var $IMPORT_TYPE = 'Typ des Imports';
	var $SAVE_CHANGES = 'Aenderungen speichern';
	var $FIELD_NAME = 'Feldbezeichnung';
	var $DEFAULT_VALUE = 'Standardwert';
 	var $COLUMN_HEADER = 'Spaltenueberschrift';
	var $FIELD_ORDERING = 'Feldreihenfolge';
	var $FIELD_REQUIRED = 'Feld benoetigt?';
	var $GENERAL = 'Allgemein';
	var $IMPORT = 'Import';
	var $EXPORT = 'Export';
	var $TEMPLATES = 'Templates';
 	var $CHOOSE_TEMPLATE = 'Template auswaehlen';
	var $CONFIG = 'Konfiguration';
	var $NEW_FIELD = 'Neues Feld hinzufuegen';
 	var $NUMBER_OF_FIELDS = 'Anzahl der Felder';
 	var $ADD_NEW_FIELD = 'Neues Feld hinzufuegen';
	var $AVAILABLE_FIELDS = 'Felder';
	var $DOCUMENTATION = 'Dokumentation';
	var $CONTINUE_UPLOAD = 'Upload fortsetzen';
	var $CANCEL_UPLOAD = 'Upload abbrechen';
 	var $OUTPUT_CSV_UPLOAD_MESSAGES = 'Ergebnisse:';
	var $OUTPUT_COUNT = 'Anzahl';
	var $OUTPUT_TOTAL = 'Gesamt';
	var $OUTPUT_FILE_IMPORTED = 'CSV-Datei importiert';
	var $OUTPUT_UPDATED = 'Aktualisiert';
	var $OUTPUT_DELETED = 'Geloescht';
	var $OUTPUT_ADDED = 'Hinzugefuegt';
	var $OUTPUT_SKIPPED = 'Uebersprungen';
	var $OUTPUT_INCORRECT = 'Inkorrekt';
	var $OUTPUT_EMPTY = 'Geleert';
	var $OUTPUT_NOSUPPORT = 'Nicht unterstuetzt';
	var $OUTPUT_NOFILES = 'Fehlend';
 	var $OUTPUT_INFORMATION = 'Informationen';
	var $NEW_TEMPLATE = 'Neues Template...';
	var $OK_TEMPLATE = 'OK';
	var $NAME = 'Name';
	var $DELETE = 'Loeschen';
	var $YES = 'Ja';
	var $NO = 'Nein';
	var $AVAILABLE_FIELDS_USE = 'Die folgenden Felder sind fuer den Import und den Export verfuegbar.';
 	var $LATEST_ORDER = 'Letzter Befehl: ';
 	var $TEMPLATE_SETTINGS = 'Template-Einstellungen';
 	var $TEMPLATE_EXPORT = 'Template-Export';
 	var $TEMPLATE_FIELDS_EXPORT = 'Export der Template-Felder';
 	var $TEMPLATE_IMPORT = 'Template-Import';
 	var $TEMPLATE_FIELDS_IMPORT = 'Import der Template-Felder';
 	var $CUSTOM_FIELD = 'Angepasstes Feld';
 	var $MAINTENANCE_OPTIONS = 'Instandhaltungsoptionen';
 	var $EXPORT_FORMAT = 'Export-Format';
 	var $TYPE = 'Typ';
 	var $EXPORT_SITE = 'Webseite';
 	var $CHOOSE_SITE = 'Webseite auswaehlen';

 	// Strings for the control panel
 	var $CP_IMPORT = 'Import';
 	var $CP_EXPORT = 'Export';
 	var $CP_TEMPLATES = 'Templates';
 	var $CP_MAINTENANCE = 'Instandhaltung';
 	var $CP_HELP = 'Hilfe';
 	var $CP_ABOUT = 'Ueber';
 	
 	// Strings for the toolbar
 	var $TB_TEMPLATES = 'Templates';
 	var $TB_NEW = 'Neu';
 	var $TB_DELETE = 'Loeschen';
 	var $TB_EDIT = 'Bearbeiten';
 	var $TB_FIELDS = 'Felder';
 	var $TB_IMPORT = 'Import';
 	var $TB_EXPORT = 'Export';
 	var $TB_CONTINUE = 'Weiter';
 	var $TB_BACK = 'Zurueck';
}	
$CsviLang =& new CsviLanguage();
?>
