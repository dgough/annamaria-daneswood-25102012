<?php
/**
 * English language file
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Language
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: csvi_english.php 386 2008-07-26 01:45:03Z Suami $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * English language file for CSV Improved
 *
 * @package CSVImproved
 * @subpackage Languages
 */
class CsviLanguage {
	var $UPLOAD = 'Use CSV Improved';
	var $VERSION = '0.9 RC 1';
	var $ABOUT = 'About';
	var $DELIMITER = 'Text Delimiter';
	var $ENCLOSURE = 'Field Delimiter';
	var $SKIP_FIRST_LINE = 'Skip first line';
	var $SKIP_DEFAULT_VALUE = 'Skip default value';
	var $OVERWRITE_EXISTING_DATA = 'Overwrite existing data';
	var $MULTIPLE_PRICES_UPLOAD = 'Multiple prices import';
	var $IMPORT_CONFIG_CSV_FILE = 'Use column headers as configuration';
	var $COLLECT_DEBUG_INFO = 'Collect debug information';
	var $SHOW_PREVIEW = 'Show preview';
	var $REGULAR_UPLOAD = 'Product import';
	var $PRODUCT_FILES_UPLOAD = 'Product files import';
	var $PRODUCT_TYPE_UPLOAD = 'Product type import';
	var $PRODUCT_TYPE_PARAMETERS_UPLOAD = 'Product type parameters import';
	var $PRODUCT_TYPE_XREF_UPLOAD = 'Product type cross reference import';
	var $PRODUCT_TYPE_NAMES_UPLOAD = 'Product type names import';
	var $MANUFACTURER_IMPORT = 'Manufacturer details import';
	var $REGULAR_EXPORT = 'Product export';
	var $PRODUCT_FILES_EXPORT = 'Product files export';
	var $PRODUCT_TYPE_EXPORT = 'Product type export';
	var $PRODUCT_TYPE_PARAMETERS_EXPORT = 'Product type parameters export';
	var $PRODUCT_TYPE_XREF_EXPORT = 'Product type cross reference export';
	var $PRODUCT_TYPE_NAMES_EXPORT = 'Product type names export';
	var $MULTIPLE_PRICES_EXPORT = 'Multiple prices export';
	var $MANUFACTURER_EXPORT = 'Manufacturer details export';
	var $ORDER_EXPORT = 'Order export';
	var $EMPTY_DATABASE = 'Empty database';
	var $REMOVE_ORPHAN = 'Remove orphaned fields';
	var $OPTIMIZE_TABLES = 'Optimize tables';
	var $INCLUDE_COLUMN_HEADERS = 'Include column headers';
	var $UPLOAD_SETTINGS = 'Import Settings';
	var $UPLOAD_FILE = 'Import a CSV File';
	var $SUBMIT_FILE = 'Submit CSV File';
	var $FROM_DIRECTORY = 'Load from directory';
	var $FROM_SERVER = 'Load file from server';
	var $EXPORT_TO_FILE = 'Export to file';
	var $EXPORT_TO_LOCAL = 'Export to local path';
	var $SELECT_FIELD_ORDERING = 'Choose Field Ordering Type';
	var $DEFAULT_ORDERING = 'Default Ordering';
	var $CUSTOMIZED_ORDERING = 'My customized ordering';
	var $EXPORT_ALL = 'Export all';
	var $SUBMIT_EXPORT = 'Export all Products to CSV File';
	var $EXPORT_TYPE = 'Export type';
	var $IMPORT_TYPE = 'Import type';
	var $CONFIGURATION_HEADER = 'CSV Import / Export Configuration';
	var $SAVE_CHANGES = 'Save Changes';
	var $FIELD_NAME = 'Field Name';
	var $COLUMN_HEADER = 'Column Header';
	var $DEFAULT_VALUE = 'Default Value';
	var $FIELD_ORDERING = 'Field Ordering';
	var $FIELD_REQUIRED = 'Field Included?';
	var $GENERAL = 'General settings';
	var $IMPORT = 'Import';
	var $EXPORT = 'Export';
	var $TEMPLATES = 'Templates';
	var $CHOOSE_TEMPLATE = 'Choose template';
	var $CONFIG = 'Configuration';
	var $NEW_FIELD = 'Add a new Field';
	var $NUMBER_OF_FIELDS = 'Number Of Fields';
	var $ADD_NEW_FIELD = 'Add a new field';
	var $AVAILABLE_FIELDS = 'Fields';
	var $DOCUMENTATION = 'Documentation';
	var $CONTINUE_UPLOAD = 'Continue import';
	var $CANCEL_UPLOAD = 'Cancel import';
	var $OUTPUT_CSV_UPLOAD_MESSAGES = 'Results for';
	var $OUTPUT_COUNT = 'Count';
	var $OUTPUT_TOTAL = 'Total';
	var $OUTPUT_FILE_IMPORTED = 'CSV File Imported';
	var $OUTPUT_UPDATED = 'Updated';
	var $OUTPUT_DELETED = 'Deleted';
	var $OUTPUT_ADDED = 'Added';
	var $OUTPUT_SKIPPED = 'Skipped';
	var $OUTPUT_INCORRECT = 'Incorrect';
	var $OUTPUT_EMPTY = 'Emptied';
	var $OUTPUT_NOSUPPORT = 'Unsupported';
	var $OUTPUT_NOFILES = 'Missing';
	var $OUTPUT_INFORMATION = 'Information';
	var $NEW_TEMPLATE = 'New template...';
	var $OK_TEMPLATE = 'OK';
	var $NAME = 'Name';
	var $DELETE = 'Delete';
	var $YES = 'Yes';
	var $NO = 'No';
	var $PUBLISHED = 'Published';
	var $AVAILABLE_FIELDS_USE = 'The following fields are available for your use to import or export.';
	var $LATEST_ORDER = 'Last order: ';
	var $TEMPLATE_SETTINGS = 'Template settings';
	var $TEMPLATE_EXPORT = 'Template export';
	var $TEMPLATE_FIELDS_EXPORT = 'Template fields export';
	var $TEMPLATE_IMPORT = 'Template import';
	var $TEMPLATE_FIELDS_IMPORT = 'Template fields import';
	var $CUSTOM_FIELD = 'Custom field';
	var $MAINTENANCE_OPTIONS = 'Maintenance options';
	var $EXPORT_FORMAT = 'Export format';
	var $TYPE = 'Type';
	var $EXPORT_SITE = 'Website';
	var $CHOOSE_SITE = 'Choose website...';
	var $COLUMN_NAME = 'Column name';
	var $NO_VIRTUEMART = 'There is no valid Virtuemart installation found. This component requires VirtueMart to operate correctly.';
	var $PHP_TOO_LOW = 'You are running PHP version [version], CSV Improved requires at least PHP version 5.0.';
	var $PASTE = 'Paste';
	var $CLEAR = 'Clear';
	var $STANDARD_FILE_LOCATION = 'Standard file location: ';
	var $SORT_CATEGORIES = 'Sort Categories';
	var $FILTER = 'Filter';
	
	// Strings for the control panel
	var $CP_IMPORT = 'Import';
	var $CP_EXPORT = 'Export';
	var $CP_TEMPLATES = 'Templates';
	var $CP_MAINTENANCE = 'Maintenance';
	var $CP_HELP = 'Help';
	var $CP_ABOUT = 'About';
	
	// Strings for the toolbar
	var $TB_TEMPLATES = 'Templates';
	var $TB_NEW = 'New';
	var $TB_DELETE = 'Delete';
	var $TB_EDIT = 'Edit';
	var $TB_CLONE = 'Clone';
	var $TB_FIELDS = 'Fields';
	var $TB_IMPORT = 'Import';
	var $TB_EXPORT = 'Export';
	var $TB_CONTINUE = 'Continue';
	var $TB_BACK = 'Back';
	var $TB_RENUMBER = 'Renumber';
	var $TB_CUSTOM = 'Custom';
	
	// Strings for the template
	var $TEMPLATE_THUMB_WIDTH = 'Thumbnail width x height';
	var $TEMPLATE_THUMB_HEIGHT = 'Thumbnail height';
	var $TEMPLATE_EXPORT_SHOPPER_GROUP = 'Shopper group name';
	var $TEMPLATE_EXPORT_ALL = 'All';
	var $TEMPLATE_EXPORT_MANUFACTURER = 'Manufacturer';
	var $EXPORT_PRODUCTURL_SUFFIX = 'Product URL suffix';
	var $SHOPPER_SITES = 'Shopper sites';
	var $TEMPLATE_FILE_LOCATION = 'File location';
	var $NO_FIELDS = 'No fields found';
	var $NO_FIELD_NAME = 'No fieldname specified';
	var $CATEGORY_FIELDS_IMPORT = 'Category details import';
	var $CATEGORY_FIELDS_EXPORT = 'Category details export';
	var $TEMPLATE_EXPORT_PRODUCT_PUBLISH = 'Export products with state';
	var $TEMPLATE_EXPORT_PRODUCT_PUBLISHED_BOTH = 'Both';
	var $TEMPLATE_EXPORT_PRODUCT_PUBLISHED = 'Published';
	var $TEMPLATE_EXPORT_PRODUCT_UNPUBLISHED = 'Unpublished';
	var $SYSTEM_LIMITS = 'System Limits';
	var $TEMPLATE_MAX_EXECUTION_TIME = 'Maximum execution time of each script in seconds';
	var $TEMPLATE_MAX_INPUT_TIME = 'Maximum amount of time each script may spend parsing request data in seconds';
	var $TEMPLATE_MEMORY_LIMIT = 'Maximum amount of memory a script may consume in megabytes';
	var $TEMPLATE_POST_MAX_SIZE = 'Maximum size of POST data in megabytes';
	var $TEMPLATE_UPLOAD_MAX_FILESIZE = 'Maximum allowed size for uploaded files in megabytes';
	var $TEMPLATE_EXPORT_FILENAME = 'Filename for exported file';
	
	// Strings for export page
	var $EXPORT_NUMBER_RECORDS = 'Number of records to export';
	var $EXPORT_START_RECORD = 'Start from record number';
	var $CHOOSE_TEMPLATE_NO_FIELDS = '*Templates with no fields are not shown';
	var $EXPORT_TO_DOWNLOAD = 'Download file';
	var $NO_TEMPLATE_FIELDS = 'No template has been found with fields to export';
	var $EXPORT_TEMPLATE_FIELDS = 'Use for template or template fields export';
	var $AUTOMATIC_EXPORT_FILENAME = '&gt; automaticly generated &lt;';
	
	// Strings for import page
	var $UPLOAD_LIMITS = 'System limits';
	var $MAX_FILE_POST = 'Maximum filesize that can be posted: ';
	var $MAX_FILE_UPLOAD = 'Maximum filesize upload: ';
	var $MEMORY_LIMIT = 'Maximum memory that can be used: ';
	var $EXECUTION_TIME = 'Maximum execution time: ';
	var $IMPORT_SECONDS = ' seconds';
	var $IMPORT_CONVERT_FILE = 'File conversion UTF-8 &lt;---&gt; ISO-8859-1';
	var $CONVERT_IMPORT = 'Convert import file to ';
	var $ISO_IMPORT = 'ISO-8859-1';
	var $UTF_IMPORT = 'UTF-8';
	var $EXPLAIN_ICONV = 'The PHP function iconv is not available on your system. An automatic conversion is therefore not possible. For more information see this <a href="http://www.csvimproved.com/wiki/doku.php/csvimproved:iconv" alt="iconv explanation" target="_blank">page</a>.';
	var $NO_MEMORY_GET_USAGE = 'The PHP function memory_get_usage is not available on your system. Out of memory detection is therefore disabled.';
	
	// Strings for result page
	var $EXECUTION_TIME_PASSED = 'Maximum execution time about to be exceeded. Time passed: ';
	var $MEMORY_LIMIT_PASSED = 'Maximum memory limit about to be exceeded. Memory used: ';
	var $IMPORT_MB = ' MB';
	
	// Strings for template list page
	var $TEMPLATE_SHOPPER_GROUP_NAME = 'Shopper group name';
	
	// Strings for the template fields page
	var $TEMPLATE_FIELDS_PAGE_PUBLISHED = 'Field(s) have been published';
	var $TEMPLATE_FIELDS_PAGE_NOT_PUBLISHED = 'Field(s) have not been published';
	var $TEMPLATE_FIELDS_PAGE_UNPUBLISHED = 'Field(s) have been unpublished';
	var $TEMPLATE_FIELDS_PAGE_NOT_UNPUBLISHED = 'Field(s) have not been unpublished';
	var $TEMPLATE_FIELDS_PAGE_CUSTOM_ADDED = 'Custom field added';
	var $TEMPLATE_FIELDS_PAGE_CUSTOM_NOT_ADDED = 'Custom field not added';
}
$CsviLang =& new CsviLanguage();
?>
