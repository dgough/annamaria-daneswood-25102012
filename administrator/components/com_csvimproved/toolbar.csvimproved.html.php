<?php
/**
 * Toolbar HTML
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package CSVImproved
 * @subpackage Toolbar
 * @author Roland Dalmulder
 * @link http://www.csvimproved.com
 * @copyright Copyright (C) 2006 - 2008 Roland Dalmulder
 * @version $Id: toolbar.csvimproved.html.php 244 2008-05-18 06:28:53Z Suami $
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
 * Main display controller for the toolbar
 *
 * @package CSVImproved
 * @subpackage Toolbar
 */
class CsviToolbar {
	function AvailablefieldsMenu() {
		mosMenuBar::startTable();
		mosMenuBar::spacer();
		mosMenuBar::back( 'CSVI', $_SERVER["SCRIPT_NAME"].'?option=com_csvimproved' );
		mosMenuBar::endTable();
	}
	
	function AboutMenu() {
		mosMenuBar::startTable();
		mosMenuBar::spacer();
		mosMenuBar::back( 'CSVI', $_SERVER["SCRIPT_NAME"].'?option=com_csvimproved' );
		mosMenuBar::endTable();
	}
	
	/* Menu for import result screen */
	function OutputImportMenu(&$CsviLang, &$csviregistry) {
		mosMenuBar::startTable();
		mosMenuBar::spacer();
		mosMenuBar::back( $CsviLang->TB_BACK, $_SERVER["SCRIPT_NAME"].'?option=com_csvimproved&amp;act=import' );
		mosMenuBar::spacer();
		if (!$csviregistry->GetVar('upload_file_error')) {
			mosMenuBar::custom( 'importfile', 'csvi_import_32x32.png', 'csvi_import_32x32.png', $CsviLang->TB_IMPORT, false);
		}
		mosMenuBar::endTable();
	}
	
	/* Menu for export result screen */
	function OutputExportMenu(&$CsviLang) {
		mosMenuBar::startTable();
		mosMenuBar::spacer();
		mosMenuBar::back( 'CSVI', $_SERVER["SCRIPT_NAME"].'?option=com_csvimproved' );
		mosMenuBar::custom( 'exportpage', 'csvi_export_32x32.png', 'csvi_export_32x32.png', $CsviLang->TB_BACK, false);
		mosMenuBar::endTable();
	}
	
	function FieldsMenu(&$CsviLang) {
		mosMenuBar::startTable();
		mosMenuBar::spacer();
		mosMenuBar::back( $CsviLang->TB_TEMPLATES, $_SERVER["SCRIPT_NAME"].'?option=com_csvimproved&amp;act=templates' );
		mosMenuBar::spacer();
		mosMenuBar::publish();
		mosMenuBar::unpublish();
		mosMenuBar::save( 'saveconfigtable' );
		mosMenuBar::endTable();
	}
	
	function NewConfigMenu() {
		mosMenuBar::startTable();
		mosMenuBar::spacer();
		mosMenuBar::save( 'saveconfigfield' );
		mosMenuBar::cancel();
		mosMenuBar::endTable();
	}
	
	function TemplatesMenu(&$CsviLang) {
		mosMenuBar::startTable();
		mosMenuBar::spacer();
		mosMenuBar::back( 'CSVI', $_SERVER["SCRIPT_NAME"].'?option=com_csvimproved' );
		mosMenuBar::spacer();
		mosMenuBar::addnew( 'newtemplate', $CsviLang->TB_NEW );
		mosMenuBar::custom( 'clonetemplate','csvi_templates_32x32.png', 'csvi_templates_32x32.png', $CsviLang->TB_CLONE, true );
		mosMenuBar::editList( 'edittemplate', $CsviLang->TB_EDIT );
		mosMenuBar::deleteList( '', 'deletetemplate', $CsviLang->TB_DELETE );
		// NOTE: use CSV Improved image folder not yet supported by joomla
		mosMenuBar::custom( 'fields', 'csvi_fields_32x32.png', 'csvi_fields_32x32.png', $CsviLang->TB_FIELDS, true );
		mosMenuBar::endTable();
	}
	
	function NewTemplateMenu() {
		mosMenuBar::startTable();
		mosMenuBar::spacer();
		mosMenuBar::save( 'addtemplate' );
		mosMenuBar::cancel();
		mosMenuBar::endTable();
	}

	function EditTemplateMenu() {
		mosMenuBar::startTable();
		mosMenuBar::spacer();
		mosMenuBar::save( 'updatetemplate' );
		mosMenuBar::cancel();
		mosMenuBar::endTable();
	}		
	
	function ImportMenu(&$CsviLang) {
		mosMenuBar::startTable();
		mosMenuBar::spacer();
		mosMenuBar::back( 'CSVI', $_SERVER["SCRIPT_NAME"].'?option=com_csvimproved' );
		mosMenuBar::custom( 'import', 'csvi_import_32x32.png', 'csvi_import_32x32.png', $CsviLang->TB_IMPORT, false);
		mosMenuBar::endTable();
	}
	
	function ExportMenu(&$CsviLang) {
		mosMenuBar::startTable();
		mosMenuBar::spacer();
		mosMenuBar::back( 'CSVI', $_SERVER["SCRIPT_NAME"].'?option=com_csvimproved' );
		mosMenuBar::custom( 'export', 'csvi_export_32x32.png', 'csvi_export_32x32.png', $CsviLang->TB_EXPORT, false );
		mosMenuBar::endTable();
	}
	function DocumentationMenu() {
		mosMenuBar::startTable();
		mosMenuBar::spacer();
		mosMenuBar::back( 'CSVI', $_SERVER["SCRIPT_NAME"].'?option=com_csvimproved' );
		mosMenuBar::endTable();
	}
	function MaintenanceMenu(&$CsviLang) {
		mosMenuBar::startTable();
		mosMenuBar::spacer();
		mosMenuBar::back( 'CSVI', $_SERVER["SCRIPT_NAME"].'?option=com_csvimproved' );
		mosMenuBar::custom( 'maintain', 'csvi_continue_32x32.png', 'csvi_continue_32x32.png', $CsviLang->TB_CONTINUE, true );
		mosMenuBar::endTable();
	}
	function MaintainMenu(&$CsviLang) {
		mosMenuBar::startTable();
		mosMenuBar::back( $CsviLang->TB_BACK, $_SERVER["SCRIPT_NAME"].'?option=com_csvimproved&amp;act=maintenance' );
		mosMenuBar::endTable();
	}
	
	function ImportEndMenu(&$CsviLang) {
		mosMenuBar::startTable();
		mosMenuBar::back( $CsviLang->TB_BACK, $_SERVER["SCRIPT_NAME"].'?option=com_csvimproved&amp;act=import' );
		mosMenuBar::endTable();
	}
}
?>
