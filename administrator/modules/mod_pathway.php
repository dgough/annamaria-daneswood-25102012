<?php
/**
* @version $Id: mod_pathway.php 85 2005-09-15 23:12:03Z eddieajau $
* @package Joomla
* @copyright Copyright (C) 2005 Open Source Matters. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );

global $mosConfig_sitename, $database;

if ($option != '') {
	$html = '';

	$html .= '<div class="pathway">';
	if($task != '' && !preg_match( '/edit/', $task ) || !mosGetParam($_REQUEST, 'hidemainmenu')) $html .= '<a href="'. $mosConfig_live_site .'/administrator/index2.php">';
	$html .= '<strong>' . $mosConfig_sitename . '</strong>';
	$html .= "</a>";
	
	$database->setQuery("SELECT name from #__components WHERE `option` = '$option' AND `parent` = 0");
	$optionName = $database->loadResult();
	
	$defaultOptions = array(
		'com_content'=>'Content',
		'com_typedcontent'=>'Static Content',
		'com_categories'=>'Categories',
		'com_admin'=>'Control Panel',
		'com_users'=>'Users',
		'com_config'=>'Configuration',
		'com_menumanager'=>'Menus',
		'com_media'=>'Media Manager',
		'com_statistics'=>'Statistics',
		'com_templates'=>'Templates',
		'com_trash'=>'Trash Manager',
		'com_sections'=>'Sections',
		'com_mambots'=>'Plugins/Bots',
		'com_installer'=>'Installer',
		'com_messages'=>'Messages',
		'com_checkin'=>'Global Checkin',
		'com_modules'=>'Modules');
	
	if(array_key_exists($option, $defaultOptions)) $optionName = $defaultOptions[$option];
	if($optionName == '') $optionName = $option;
	
	if ($option != '') {
		$html .= " / ";
		// try to miss edit functions
		if ($task != '' && !preg_match( '/edit/', $task )) {
			$html .= "<a href=\"index2.php?option=$option\">$optionName</a>";
		} else {
			$html .= $optionName;
		}
	}

	if ($task != '') {
		$html .= " / $task";
	}
	$html .= '</div>';
	echo $html;
}
?>