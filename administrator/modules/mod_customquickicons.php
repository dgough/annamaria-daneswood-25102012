<?php
/**
* @version $Id: mod_customquickicons.php 2007/05/06 15:04:04 mic $
* @version 2.0.4
* @package Custom QuickIcons
* @copyright (C) 2006 mic / http://www.joomx.com
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
* based on the Joomla/Mambo mod_quickicon
*/

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );

$pathLang	= $mosConfig_absolute_path . '/administrator/components/com_customquickicons/lang/';
$tmpLang	= $GLOBALS['mosConfig_lang'];

// tries to get adminlang (if defined -see MGFi www.mgfi.info)
if( isset( $GLOBALS['mosConfig_alang'] )){
    if( file_exists( $pathLang . $GLOBALS['mosConfig_alang'] . '.php' )) {
        $tmpLang = $GLOBALS['mosConfig_alang'];
    }
}
if( file_exists( $pathLang . $tmpLang . '.php' )) {
    include( $pathLang . $tmpLang . '.php' );
} else {
    @include( $pathLang . 'english.php' );
}

// check lng.var (if com is not or uninstalled)
if( !defined( '_QI_MOD_NO_COM') ) {
	define( '_QI_MOD_NO_COM', 'Komponente CQI nicht installiert - Component CQI not installed! ' );
}

// check if module is installed and published at correct position
$query = "SELECT id, position"
. "\n FROM #__modules"
. "\n WHERE module = 'mod_customquickicons'"
;
$database->setQuery( $query );
$rows = $database->loadObjectList();

// check for existing component
if( !file_exists( $mosConfig_absolute_path . '/administrator/components/com_customquickicons/admin.customquickicons.php' ) || !$rows[0]->id ) {
	echo _QI_MOD_NO_COM;
}elseif( $rows[0]->position != 'icon' ) {
	echo _QI_ERR_MOD_INCORR_POS
	. '<br />'
	. '<a href="index2.php?option=com_modules&amp;client=admin" title="' . _QI_MOD_MGMNT . '">' . _QI_MOD_MGMNT . '</a>';
}else{
	if( !defined( '_QUICKICON_MODULE' )) {
	    /** ensure that functions are declared only once */
        define( '_QUICKICON_MODULE', 1 );

        function MOD_quickiconButton( $row, $newWindow ){

            $title  = ( $row->akey ? $row->title . ' [ ' . _QI_MOD_ACCESSKEY . ' ' . $row->akey . ' ]' : ( $row->title ? $row->title : $row->text ));

            $accKey = $row->akey ? ' accesskey="' . $row->akey . '"' : ''; ?>
            <div style="float:left;">
                <div class="icon">
                    <a href="<?php echo htmlentities( $row->target ); ?>" title="<?php echo $title; ?>"<?php echo $accKey . $newWindow; ?>>
                        <?php
                        /* output
                         * 0    icon & text
                         * 1    text only
                         * 2    icon only
                         */
                        if( $row->display == 1 ){ ?>
                            <span><?php echo $row->prefix . $row->text . $row->postfix; ?></span>
                            <?php
                        }elseif( $row->display == 2 ){
                            echo mosAdminMenus::imageCheckAdmin( $row->icon, '/administrator/images/icons/', NULL, NULL, $title );
                        }else{
                            echo mosAdminMenus::imageCheckAdmin( $row->icon, '/administrator/images/icons/', NULL, NULL, $title);?>
                            <span><?php echo $row->prefix . $row->text . $row->postfix; ?></span>
                            <?php
                        } ?>
                    </a>
                </div>
            </div>
            <?php
        } ?>

        <div id="cpanel">
            <?php
            $query = "SELECT *"
            . "\n FROM #__custom_quickicons"
            . "\n WHERE published = 1"
            . "\n AND gid <= $my->gid"
            . "\n ORDER BY ordering"
            ;
            $database->setQuery($query);
            $rows = $database->loadObjectList();
            if( $database->getErrorNum() ) {
                echo $database->stderr();
                return false;
            }

            foreach( $rows AS $row ){
                $callMenu = true;
                if( $row->cm_check ){
                    if( !file_exists( $mosConfig_absolute_path . '/administrator/components/' . $row->cm_path )){
                        $callMenu = false;
                    }
                }
                if( $callMenu ){
                    $newWindow = $row->new_window ? ' target="_blank"' : '';
                    if($row->display < 4) MOD_quickiconButton( $row, $newWindow );
                    else{
                    	echo '<div style="clear: left" class="icon_seperator"></div>';
                    	if($row->display == 4) echo "<div style='margin: 5px 0px; text-align: left; font-weight: bold; padding-left: 30px'>$row->text</div>";

                    }
                }
            }

            $securitycheck 	= intval( $params->get( 'securitycheck', 1 ) );

            if( !empty( $securitycheck )) {
            // show security setting check
                josSecurityCheck('88%');
            } ?>
        </div>
        <?php
    }
}
?>