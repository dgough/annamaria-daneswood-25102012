<?php
/**
* @version $Id: mod_online.php 5026 2006-09-13 18:45:52Z friesengeist $
* @package Joomla
* @copyright Copyright (C) 2005 Open Source Matters. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );

?>
<SCRIPT LANGUAGE="JavaScript">
//<!--
function popUp(URL) {
day = new Date();
id = day.getTime();
eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=300,height=400,left = 690,top = 325');");
}
// End -->
</script>

<?php echo mosToolTip( 'Launch the Organic Development Live Support Application', 'Organic Live Support', '', 'tooltip.png', '<img src="images/organic.gif" align="top" alt="Online Help" /> Support', "javascript:popUp('http://www.organic-development.com/live_support/live_support.php?type=support')", 1 ) ?>
