<?php
/**
* @copyright Copyright (C) 2006 Geraint Edwards
* @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// ensure this file is being included by a parent file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

$linkType = $params->get("linktype","newwindow");
$comps = $params->get("components","com_content|content|cid");
$comps=str_replace(" ","",$comps);
$components = explode("<br/>",$comps);
$mapping=null;
foreach ($components as $component){
	$map = explode("|",$component);
	if (count($map)>=3 && trim($map[0])==$option){
		if (count($map)>3 && (count($map)-3)%2==0){
			$matched=true;
			for ($p=0;$p<(count($map)-3)/2;$p++){
				$testParam = mosGetParam( $_REQUEST, trim($map[3+$p*2]), '');
				if ((strpos(trim($map[4+$p*2]),"!")!==false && strpos(trim($map[4+$p*2]),"!")==0)){
					if ($testParam == substr(trim($map[4+$p*2]),1)){
						$matched=false;
						break;
					}
				}
				else {
					if ($testParam != trim($map[4+$p*2])){
						$matched=false;
						break;
					}
				}
			}
			if ($matched) {
				$mapping=$map;
				break;
			}
		}
		else {
			$mapping=$map;
			break;
		}
	}
}
if ($mapping!=null){
	include_once($mainframe->getBasePath()."/administrator/components/com_joomfish/joomfish.class.php");
	$joomFishManager = new JoomFishManager($mainframe->getBasePath()."/administrator/components/com_joomfish");

	//Global definitions
	if( !defined('DS') ) {
		define( 'DS', DIRECTORY_SEPARATOR );
	}

	if( !defined('JOOMFISH_PATH') ) {
		define( 'JOOMFISH_PATH', mosMainFrame::getBasePath() .'components'.DS.'com_joomfish' );
		define( 'JOOMFISH_ADMINPATH', mosMainFrame::getBasePath() .'administrator'.DS.'components'.DS.'com_joomfish' );
		define( 'JOOMFISH_LIBPATH', JOOMFISH_ADMINPATH .DS. 'libraries' );
		define( 'JOOMFISH_LANGPATH', JOOMFISH_PATH .DS. 'language' );
		define( 'JOOMFISH_URL', '/components/com_joomfish');
	}

	require_once( JOOMFISH_LIBPATH .DS. 'joomla' .DS. 'language.php' );
	require_once( JOOMFISH_LIBPATH .DS. 'joomla' .DS. 'registry.php' );


	$adminLang = strtolower( $joomFishManager->getCfg( 'componentAdminLang' ) );
	$lang =& JLanguageHelper::getLanguage($adminLang);
	$lang->load( 'com_joomfish', JOOMFISH_LANGPATH );

	//$langActive = JoomFishManager::getActiveLanguages();
	$langActive = JoomFishManager::getLanguages( true );
	$langOptions[] = mosHTML::makeOption( -1, JText::_("SELECT LANGUAGE") );

	if ( count($langActive)>0 ) {
		foreach( $langActive as $language )
		{
			$langOptions[] = mosHTML::makeOption( $language->id, $language->name );
		}
	}
	$langlist = mosHTML::selectList( $langOptions, 'select_language_id', 'id="select_language_id" class="inputbox" style="margin-top:3px;margin-bottom:-3px;" size="1"', 'value', 'text', -1);//$langActive[0]->id );
	// I also need to trap component specific actions e.g. pony gallery uses
?>
<script language="JavaScript" type="text/javascript">
function translateItem(){
	var langCode=document.getElementById('select_language_id').value;
	var option="<?php echo trim($mapping[1]);?>";

	if (document.adminForm.boxchecked.value==0) {
		<?php
		$setlang="&select_language_id=\"+langCode+\"";
		global $mosConfig_live_site;
		$targetURL = "$mosConfig_live_site"
		."/administrator/index2.php?option=com_joomfish&act=translate&catid=\"+option+\"".$setlang;
		if ($linkType=="newwindow") echo "window.open(\"$targetURL\"";
		else echo "document.location.replace(\"$targetURL\"";

		if ($linkType=="newwindow") echo ',"translation","innerwidth=800,innerheight=500,menubar=yes,status=yes,location=yes,resizable=yes,scrollbars=yes"'?>);
		return;
	}

	if (document.adminForm.boxchecked.value!=1) {
		alert("You must select exactly one item to translate");
		return;
	}
	// not all components use "cid" e.g. ponygallery uses act=pictures or act=showcatg
	var cid = "<?php echo trim($mapping[2]);?>[]";
	var checkboxes = document.getElementsByName(cid);
	for (var i=0;i<checkboxes.length;i++){
		if (checkboxes[i].checked){
			//alert("you want to edit item "+(i+1)+" content item id = "+checkboxes[i].value);
			// second part is language id 1=Cymraeg,5=German etc!
			<?php
			global $mosConfig_live_site;
			$targetURL = "$mosConfig_live_site"
			."/administrator/index2.php?act=translate&boxchecked=1&catid=\"+option+\"&cid[]=0|\"+checkboxes[i].value+\"|\"+langCode+\"&option=com_joomfish&task=edit";
			if ($linkType=="newwindow") echo "window.open(\"$targetURL\"";
			else echo "document.location.replace(\"$targetURL\"";

			if ($linkType=="newwindow") echo ',"translation","innerwidth=800,innerheight=500,menubar=yes,status=yes,location=yes,resizable=yes,scrollbars=yes"'?>);
			return;
		}
	}
	alert("There was a problem");
}
</script>
<style>
.languagebox {
	vertical-align: middle;
	border: 1px solid #cccccc;
	}
</style>
<?php
echo $langlist;
?>
<a href="javascript:translateItem()" title="translate this item"><img src="modules/mod_translate.png" align="middle" border="0" alt="translate"></a>
<?php
}
else {
?>
<img src="modules/mod_translate_no.png" align="middle" border="0" alt="No Translation">
<?php
}
