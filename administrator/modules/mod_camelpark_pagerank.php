<?php
//=====================================================================================
// Camelpark PageRank Module
// Created for Joomla 1.0 by Leslie
// More info http://www.camelpark.hu/
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// The "GNU General Public License" (GPL) is available at
// http://www.gnu.org/copyleft/gpl.html.
//=====================================================================================
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );


global $database, $id;

$domain 			= $params->get( 'domain', str_replace('http://','',$mosConfig_live_site)) ;
$type 				= $params->get( 'type') ;
$align 				= $params->get( 'align') ;
$createdby 			= $params->get( 'createdby') ;

switch ($type) {
  case "1" :
    $type = "<img src=\"http://pr.camelpark.hu/sig.php?les=$domain&amp;y=0\" border=\"0\" alt=\"Google PageRank modul - Camelpark SEO centrum\" />";
  break;
  case "2" :
    $type = "<img src=\"http://pr.camelpark.hu/sig.php?les=$domain&amp;y=2\" border=\"0\" alt=\"Google PageRank modul - Camelpark SEO centrum\" />";
  break;
  case "3" :
    $type = "<img src=\"http://pr.camelpark.hu/sig.php?les=$domain&amp;y=1\" border=\"0\" alt=\"Google PageRank modul - Camelpark SEO centrum\" />";
  break;
  case "4" :
    $type = "<img src=\"http://pr.camelpark.hu/sig.php?les=$domain&amp;y=3\" border=\"0\" alt=\"Google PageRank modul - Camelpark SEO centrum\" />";
  break;
  default:
    $type = "";
  break;
}

switch ($align) {
  case "1" :
    $align = "text-align: center";
  break;
  case "2" :
    $align = "text-align: left";
  break;
  case "3" :
    $align = "text-align: right";
  break;
  default:
    $align = "";
  break;
}

switch ($createdby) {
  case "1" :
    $createdby = "<font size=\"1\">info: <a href=\"http://www.camelpark.hu/seo/mit-jelentenek-ezek-a-szamok/\">SEO figures</font></div>";
  break;
  case "2" :
    $createdby = "</div>";
  break;
  default:
    $createdby = "";
  break;
}

global $mosConfig_sitename;

echo "<div class=\"syndicate\" style='padding-bottom: 10px'>
<div style='$align'>";
echo "$type";
echo "</div><div style='$align'>";
echo "$createdby";
echo "</div>"
?>
