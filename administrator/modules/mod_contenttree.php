<?
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );
?>

<script type="text/javascript">
<!--
var image = 'modules/mod_contenttree/img/show.gif';
function showhide(layer_ref) {
if (state == "block") {
state_test = 0;
state = 'none';
image = 'modules/mod_contenttree/img/show.gif';
}
else {
state_test = 1;
state = 'block';
image = 'modules/mod_contenttree/img/hide.gif';
}
if (document.all) { //IS IE 4 or 5 (or 6 beta)
eval( "document.all." + layer_ref + ".style.display = state");
document['showHideImg'].src=image;
}
if (document.layers) { //IS NETSCAPE 4 or below
document.layers[layer_ref].display = state;
document['showHideImg'].src=image;
}
if (document.getElementById && !document.all) {
maxwell_smart = document.getElementById(layer_ref);
maxwell_smart.style.display = state;
document['showHideImg'].src=image;
}
var date = new Date();
date.setTime(date.getTime()+(365*24*60*60*1000));
var expires = date.toGMTString();
document.cookie =  'JoomlaContentBrowser='+state+'; expires='+expires+'; path=/';
}
function setExpand(){
var date = new Date();
date.setTime(date.getTime()+(365*24*60*60*1000));
var expires = date.toGMTString();
document.cookie =  'JoomlaContentBrowserExpand=1; expires='+expires+'; path=/';

d.openAll()

}

function setContract(){

var date = new Date();
date.setTime(date.getTime()+(365*24*60*60*1000));
var expires = date.toGMTString();
document.cookie =  'JoomlaContentBrowserExpand=0; expires='+expires+'; path=/';

d.closeAll()

}

//-->

</script>
<table class="adminlist">
<tbody>
<tr>
<th colspan="4"> Content Browser</th>
</tr>
<tr>
<td>
<div class="dtree">
	<link rel="StyleSheet" href="modules/mod_contenttree/dtree.css" type="text/css" />
	<script type="text/javascript" src="modules/mod_contenttree/dtree.js"></script>
	<div class="expand_contract"><a href="javascript: setExpand();">Expand all</a> | <a href="javascript: setContract();">Contract all</a></div>
	<script type="text/javascript">
		<!--
		d = new dTree('d');
		d.add(0,-1,'Content Structure','index2.php?option=com_sections&scope=content','View all sections'); 	
<?php	 

		//////////// Adds section links //////////////

		$currentSection = 1;
		$currentCategory = 1;
		$totalCount = 1;		
		$trash =0; // Set trash counter		
				
		$query = "SELECT *"
		. "\n FROM #__content AS a"
		. "\n WHERE a.sectionid = 0"		
		. "\n ORDER BY a.ordering";

		$database->setQuery( $query );
		$content = $database->loadObjectList();
		$staticID = $totalCount;
		//Output static category link
		echo "d.add($totalCount,0,'Uncategorized Content','index2.php?option=com_typedcontent','View all uncategorized articles');";
		$totalCount++;
		echo "d.add($totalCount,$staticID,'Add uncategorized article','index2.php?option=com_typedcontent&task=edit&hidemainmenu=1&redirect=home','Create new uncategorized article', '' , 'modules/mod_contenttree/img/new.png');";		
		$totalCount++;
		for ($i2=0, $n2=count( $content ); $i2 < $n2; $i2++) {
							
			$txtCont = addslashes($content[$i2]->title);
			$contid = $content[$i2]->id;
			//Is item published?
			if($content[$i2]->state==1){
				//Is item checked out?
				if($content[$i2]->checked_out > 0){
					//Is item checked out by current user
					if($content[$i2]->checked_out == $my->id){
					//If yes, display with link
					echo "d.add($totalCount,$staticID,'$txtCont','index2.php?option=com_typedcontent&task=edit&hidemainmenu=1&id=$contid&redirect=home','This item is checked out by you','' , 'modules/mod_contenttree/img/checked_out.png');";
					//If no, display with no link
					}else{
					echo "d.add($totalCount,$staticID,'$txtCont','','This item is checked out','', 'modules/mod_contenttree/img/checked_out.png');";
					}
				//If not checked out, then display as published
				}else{
				echo "d.add($totalCount,$staticID,'$txtCont','index2.php?option=com_typedcontent&task=edit&hidemainmenu=1&id=$contid&redirect=home','Edit this item');";
				}
			//If not published
			}else if($content[$i2]->state==0){
				//Is item is checked out
				if($content[$i2]->checked_out > 0){
					//Is item checked out by current user?
					if($content[$i2]->checked_out == $my->id){
					//If yes, display with link
					echo "d.add($totalCount,$staticID,'$txtCont','index2.php?option=com_typedcontent&task=edit&hidemainmenu=1&id=$contid&redirect=home','This item is unpublished and checked out by you','' , 'modules/mod_contenttree/img/unpublishedandchecked.gif');";
					//If no, display with no link
					}else{
					echo "d.add($totalCount,$staticID,'$txtCont','','This item is unpublished and checked out','', 'modules/mod_contenttree/img/unpublishedandchecked.gif');";
					}
				//If not checked out, display as unpublished
				}else{
				echo "d.add($totalCount,$staticID,'$txtCont','index2.php?option=com_typedcontent&task=edit&hidemainmenu=1&id=$contid&redirect=home','Edit this item (unpublished)','','modules/mod_contenttree/img/unpublished.png');";
				}
			//If item is trashed, increment trash counter										
			}else{
			
			$trash++;
			}
		
		$totalCount++;
		}
									
																
		$query = "SELECT a.id, a.title, a.name"
		. "\n FROM #__sections AS a"
		. "\n WHERE a.scope = 'content'"
		. "\n GROUP BY a.id"
		. "\n ORDER BY a.ordering";

		$database->setQuery( $query );
		$sections = $database->loadObjectList();


				foreach ($sections as $section) {

					$txt = addslashes( $section->title ? $section->title : $section->name );
					echo "d.add($totalCount,0,'$txt','index2.php?option=com_categories&section=$section->id','View all categories in: $txt','','modules/mod_contenttree/img/section_closed.png','modules/mod_contenttree/img/section_open.png');";			//folder
					$currentSection = $totalCount;
					$totalCount++;

					//////////// Adds category links //////////////

					$query = "SELECT *"
					. "\n FROM #__categories where section=".$section->id
					. "\n ORDER BY ordering";
					$database->setQuery( $query );
					$category = $database->loadObjectList();
					
					for ($i=0, $n=count( $category ); $i < $n; $i++) {

							$txtC = addslashes($category[$i]->name);
							$catid = $category[$i]->id;
							echo "d.add($totalCount,$currentSection,'$txtC','index2.php?option=com_content&sectionid=$section->id&catid=$catid','View all items in: $txtC');";
							$currentCategory = $totalCount;
							$totalCount++;
							echo "d.add($totalCount,$currentCategory,'Add article','index2.php?option=com_content&sectionid=$section->id&catid=$catid&task=new&hidemainmenu=1&id=$currentCategory','Click to add a article','','modules/mod_contenttree/img/new.png');";
							$totalCount++;
					

								//////////// Adds content links ////////////

									$query = "SELECT *"
									. "\n FROM #__content WHERE catid=".$category[$i]->id
									. "\n ORDER BY ordering";
									$database->setQuery( $query );
									$content = $database->loadObjectList();
									

									for ($i2=0, $n2=count( $content ); $i2 < $n2; $i2++) {
									$contid = $content[$i2]->id;
									$txtCont = addslashes($content[$i2]->title);
										//Is item published?
										if($content[$i2]->state==1){
											//Is item checked out?
											if($content[$i2]->checked_out > 0){
												//Is item checked out by current user
												if($content[$i2]->checked_out == $my->id){
												//If yes, display with link
												echo "d.add($totalCount,$currentCategory,'$txtCont','index2.php?option=com_content&sectionid=$section->id&catid=$catid&task=edit&hidemainmenu=1&id=$contid&redirect=home','This item is checked out by you','' , 'modules/mod_contenttree/img/checked_out.png');";
												//If no, display with no link
												}else{
												echo "d.add($totalCount,$currentCategory,'$txtCont','','This item is checked out','', 'modules/mod_contenttree/img/checked_out.png');";
												}
											//If not checked out, then display as published
											}else{
											echo "d.add($totalCount,$currentCategory,'$txtCont','index2.php?option=com_content&sectionid=$section->id&catid=$catid&task=edit&hidemainmenu=1&id=$contid&redirect=home','Edit this item');";
											}
										//If not published
										}else if($content[$i2]->state==0){
											//Is item is checked out
											if($content[$i2]->checked_out > 0){
												//Is item checked out by current user?
												if($content[$i2]->checked_out == $my->id){
												//If yes, display with link
												echo "d.add($totalCount,$currentCategory,'$txtCont','index2.php?option=com_content&sectionid=$section->id&catid=$catid&task=edit&hidemainmenu=1&id=$contid&redirect=home','This item is unpublished and checked out by you','' , 'modules/mod_contenttree/img/unpublishedandchecked.gif');";
												//If no, display with no link
												}else{
												echo "d.add($totalCount,$currentCategory,'$txtCont','','This item is unpublished and checked out','', 'modules/mod_contenttree/img/unpublishedandchecked.gif');";
												}
											//If not checked out, display as unpublished
											}else{
											echo "d.add($totalCount,$currentCategory,'$txtCont','index2.php?option=com_content&sectionid=$section->id&catid=$catid&task=edit&hidemainmenu=1&id=$contid&redirect=home','Edit this item (unpublished)','','modules/mod_contenttree/img/unpublished.png');";
											}
										//If item is trashed, increment trash counter										
										}else{
										
										$trash++;
										}
									
									$totalCount++;
									}
						}
				}
				echo "d.add($totalCount,0,'Trash Bin ($trash items)','index2.php?option=com_trash','Opens the trash bin','','modules/mod_contenttree/img/trash.png','modules/mod_contenttree/img/trash.png');";			//folder
				?>
	
		document.write(d);

<?php
if (isset($_COOKIE["JoomlaContentBrowserExpand"])){
	if ($_COOKIE["JoomlaContentBrowserExpand"]==1){
		echo "d.openAll();";
	}else if($_COOKIE["JoomlaContentBrowserExpand"]==0){
		echo "d.closeAll();";
	}
}
else{
echo "d.openAll();";
}
?>

		//-->

	</script>

</div>	
</td>	
</tr>
</tbody>
</table>
