<?php
/** validator error messages */

DEFINE('_VALIDATOR_LANG_INCLUDED', 1);

DEFINE('_VALIDATOR_ERRORS' , 'Please correct the <span class="hidden">with a ! </span>marked fields.');
DEFINE('_VALIDATOR_ERROR_MARK' ,"<span class=\"hidden\"> ! </span>");
DEFINE('_VALIDATOR_ERROR_REQUIRED', '"%s" is a required field.');
DEFINE('_VALIDATOR_ERROR_INVALID_EMAIL', '"%s" is not a valid e-mail address.');
DEFINE('_VALIDATOR_ERROR_VALIDATION_RULE_DOESNOT_EXIST', 'Validation rule "%s" does not exist.');
?>