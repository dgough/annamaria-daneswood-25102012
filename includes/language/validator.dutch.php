<?php
/** validator error messages */

DEFINE('_VALIDATOR_LANG_INCLUDED', 1);

DEFINE('_VALIDATOR_ERRORS' , 'U heeft de <span class="hidden">met een ! </span>gemarkeerde velden niet correct ingevuld.');
DEFINE('_VALIDATOR_ERROR_MARK' ,"<span class=\"hidden\"> ! </span>");
DEFINE('_VALIDATOR_ERROR_REQUIRED', '"%s" is een verplicht veld.');
DEFINE('_VALIDATOR_ERROR_INVALID_EMAIL', '"%s" is geen valide e-mail adres.');
DEFINE('_VALIDATOR_ERROR_VALIDATION_RULE_DOESNOT_EXIST', 'Validatieregel "%s" bestaat niet.');
?>