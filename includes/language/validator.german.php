<?php
/** validator error messages */

DEFINE('_VALIDATOR_LANG_INCLUDED', 1);

DEFINE('_VALIDATOR_ERRORS' , 'Bitte korrigieren Sie die <span class="invisible">mit einem ! </span>markierten Felder.');
DEFINE('_VALIDATOR_ERROR_MARK' ,"<span class=\"invisible\"> ! </span>");
DEFINE('_VALIDATOR_ERROR_REQUIRED', '%s ist ein Mussfeld.');
DEFINE('_VALIDATOR_ERROR_INVALID_EMAIL', '%s ist keine g&uuml;ltige E-Mail Adresse.');
DEFINE('_VALIDATOR_ERROR_VALIDATION_RULE_DOESNOT_EXIST', 'ValidationRule "%s" existiert nicht.');
?>
