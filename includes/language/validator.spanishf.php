<?php
/** validator error messages */

DEFINE('_VALIDATOR_LANG_INCLUDED', 1);

DEFINE('_VALIDATOR_ERRORS' , 'Por favor corrija los campos marcados con <span class="hidden">una ! </span>.');
DEFINE('_VALIDATOR_ERROR_MARK' ,"<span class=\"hidden\"> ! </span>");
DEFINE('_VALIDATOR_ERROR_REQUIRED', '%s es un campo obligatorio.');
DEFINE('_VALIDATOR_ERROR_INVALID_EMAIL', '%s no es una direcci�n de e-mail v�lida.');
DEFINE('_VALIDATOR_ERROR_VALIDATION_RULE_DOESNOT_EXIST',  'La regla de validaci�n "%s" no existe.');


?>