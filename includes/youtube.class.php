<?php
 
 class YouTube {
 	
 �// Variables
 �private $user;
 �private $video_feed_url;
 �public $video_count;
 �public $video_feed = array();
 �private $video_url;
 �public $video;
 �
 �
 �
 �function __construct($user = false) {
 � 
 � $this->user = $user;
 � 
 �}
 �
 �
 �
 �/**
 �* Retrieves a video
 �*/
 �public function get_video_count($playlist = "uploads") {
 � 
 � // Set the url to retrieve the feed from
 � $this->video_feed_url = "http://gdata.youtube.com/feeds/api/users/".$this->user."/".$playlist;
 � 
 � // Check the feed exists
 � $headers = get_headers($this->video_feed_url);
 � if (!strpos($headers[0], '200')) return false;
 � 
 � // Get the xml feed
 � $sxml = simplexml_load_file($this->video_feed_url);
 � 
 � // get the count of returned results from the opensearch: namespace
 � $counts = $sxml->children('http://a9.com/-/spec/opensearchrss/1.0/');
 � $this->video_count = round($counts->totalResults);
 � 
 � return $this->video_count;
 � 
 �}
 �
 �
 �
 �/**
 �* Retrieves the channel xml feed
 �*/
 �public function get_channel_xml_feed($playlist = "uploads", $offset = 1, $per_page = 10) {
 � 
 � // Set the url to retrieve the feed from
 � $this->video_feed_url = "http://gdata.youtube.com/feeds/api/users/".$this->user."/".$playlist."/?start-index=".$offset."&max-results=".$per_page;
 � 
 � // Check the feed exists
 � $headers = get_headers($this->video_feed_url);
 � if (!strpos($headers[0], '200')) return false;
 � 
 � // Get the xml feed
 � $sxml = simplexml_load_file($this->video_feed_url);
 � 
 � foreach ($sxml->entry as $entry) {
 � �
 � �// Create a blank video object
 � �$video = false;
 � �
 � �// get media information
 � �$ratings_attr = $entry->children('http://schemas.google.com/g/2005');
 � �$entry_attr = $entry->children('http://gdata.youtube.com/schemas/2007');
 � �$media = $entry->children('http://search.yahoo.com/mrss/');
 � �$media_attr = $media->children('http://gdata.youtube.com/schemas/2007');
 � �
 � �// Get the attributes
 � �$attrs = array();
 � �$attrs['group'] � = $media->group->content->attributes();
 � �$attrs['media_player'] �= $media->group->player->attributes();
 � �$attrs['thumbnail'] �= $media->group->thumbnail[0]->attributes();
 � �if ($media_attr->duration) {
 � � $attrs['duration'] �= $media_attr->duration->attributes();
 � �}
 � �if ($entry_attr->statistics) {
 � � $attrs['viewcount'] = $entry_attr->statistics->attributes();
 � �}
 � �if ($ratings_attr->rating) {
 � � $attrs['rating'] �= $ratings_attr->rating->attributes();
 � �}
 � �
 � �// Set some video settings
 � �$video->id � � = substr($entry->id, (strrpos($entry->id, "/") + 1), (strlen($entry->id) - (strrpos($entry->id, "/") + 1)));
 � �$video->title � �= $media->group->title;
 � �$video->description �= $media->group->description;
 � �$video->author � �= $entry->author->name;
 � �
 � �$video->embed � �= $attrs['group']['url'];
 � �$video->watch � �= $attrs['media_player']['url']; 
 � �$video->thumbnail � = $attrs['thumbnail']['url']; 
 � �$video->length � �= floor ( $attrs['duration']['seconds'] / 60 ) . ":" . $attrs['duration']['seconds'] % 60;
 � �$video->view_count � = $attrs['viewcount']['viewCount']; 
 � �$video->rating � �= round($attrs['rating']['average']);
 � �
 � �// Add to the channel feed array
 � �$this->video_feed[] = $video;
 � �
 � }
 � 
 � return $this->video_feed;
 � 
 �}
 �
 �
 �
 �/**
 �* Retrieves a single video
 �*/
 �public function get_video_xml($video_id = false) {
 � 
 � $this->video_feed_url = "http://gdata.youtube.com/feeds/api/users/".$this->user."/uploads/".$video_id."?v=2";
 � 
 � // Check the video exists
 � $headers = get_headers($this->video_feed_url);
 � if (!strpos($headers[0], '200')) return false;
 � 
 � // Get the xml feed
 � $entry = simplexml_load_file($this->video_feed_url);
 � 
 � // get media information
 � $ratings_attr = $entry->children('http://schemas.google.com/g/2005');
 � $entry_attr = $entry->children('http://gdata.youtube.com/schemas/2007');
 � $media = $entry->children('http://search.yahoo.com/mrss/');
 � $media_attr = $media->children('http://gdata.youtube.com/schemas/2007');
 � 
 � // Stop if there are any errors retrieving the media
 � if (!$entry || !$media) return false;
 � 
 � // Get the attributes
 � $attrs = array();
 � $attrs['group'] � = $media->group->content->attributes();
 � $attrs['media_player'] �= $media->group->player->attributes();
 � $attrs['thumbnail'] �= $media->group->thumbnail[0]->attributes();
 � if ($media_attr->duration) {
 � �$attrs['duration'] �= $media_attr->duration->attributes();
 � }
 � if ($entry_attr->statistics) {
 � �$attrs['viewcount'] = $entry_attr->statistics->attributes();
 � }
 � if ($ratings_attr->rating) {
 � �$attrs['rating'] �= $ratings_attr->rating->attributes();
 � }
 � 
 � // Set some video settings
 � $video->id � � = substr($entry->id, (strrpos($entry->id, ":") + 1), (strlen($entry->id) - (strrpos($entry->id, ":") + 1)));
 � $video->title � �= $media->group->title;
 � $video->description �= $media->group->description;
 � $video->author � �= $entry->author->name;
 � 
 � $video->embed � �= $attrs['group']['url'];
 � $video->watch � �= $attrs['media_player']['url']; 
 � $video->thumbnail � = $attrs['thumbnail']['url']; 
 � $video->length � �= floor ( $attrs['duration']['seconds'] / 60 ) . ":" . $attrs['duration']['seconds'] % 60;
 � $video->view_count � = $attrs['viewcount']['viewCount']; 
 � $video->rating � �= round($attrs['rating']['average']);
 � 
 � $this->video = $video;
 � 
 � return $this->video;
 � 
 �}
 �
 }
 
?>