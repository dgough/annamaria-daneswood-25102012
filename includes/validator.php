<?php 
/**
* @version $Id$
* @package Accessible Joomla!
* @copyright (C) Func. Internet Integration
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*/
if(! defined('_VALIDATOR_LANG_INCLUDED')) {
	if (file_exists(dirname(__FILE__)."/language/validator.".$mosConfig_lang.".php") ) {
		include_once(dirname(__FILE__)."/language/validator.".$mosConfig_lang.".php");
	} else {
		include_once(dirname(__FILE__)."/language/validator.english.php");
	}
}

/** Validator class. This class is ment to validate field (name and value)
 * 
 */
class Validator {
	
	// isValidated is ment to make the method call validatior.
	var $isValidated = false;
	
	var $fieldValidations	= array();	
	var $validationRules	= array();	
	var $fieldErrors		= array();

	/**
	 *  default constructor
	 */
	function Validator() {
		// add the default validator rules.
		$this->addValidationRule("required", new RequiredValidationRule());
		$this->addValidationRule("email", new RegularExpresionValidationRule("/[\w\.\-]+@\w+[\w\.\-]*?\.\w{1,4}/"));

		// define constants for the default rules.
		define("_VALIDATION_RULE_REQUIRED", "required");
		define("_VALIDATION_RULE_EMAIL", "email");
	}
	
	/** validate all fields
	 * 
	 */
	function validate() {
		if(! $this->isValidated) {
			// validate
			foreach($this->fieldValidations as $fieldValidation) {
				$name = $fieldValidation[0];
				$value = $fieldValidation[1];
				$validationRule = $this->validationRules[$fieldValidation[2]];
				$errorMessage = $fieldValidation[3];
				
				if(! $validationRule->validate($value)) {
					$this->addError($name, ($errorMessage != null ? $errorMessage : $validationRule->getErrorMessage()));
				}
			}
			// set validated to true, so no more validation has to be done.
			$this->isValidated = true;
		}	
	}
	
	
	/** add an error to a name
	 *  @param String name name of the field
	 *  @param String errorMessage the error message to add
	 * 
	 */
	function addError($name, $errorMessage) {
		if(! array_key_exists($name, $this->fieldErrors)) {
			$this->fieldErrors[$name] = array();
		}
		array_push($this->fieldErrors[$name], $errorMessage);			
	}
	
	
	/** returns a array with all errors for $name, if $name is not provided or null
	 *  this function will return all errors 
	 *  @param $name the name of the field to get the errors for
	 *  @return array with error messages
	 */
	function getErrors($name=null) {
		if(! $this->isValidated) {
			$this->validate();
		}
		if($name != null) {
			return ($this->fieldErrors[$name] != null ? $this->fieldErrors[$name] : array() );
		}
		return $this->fieldErrors;
	}
		
			
	/** check if all validation for name is succesfull, if name is not provided or null
	 *  this function will return valid if there are no errors 
	 *  @param $name the name of the field to get the errors for  
	 *  @return boolean true if there are no errors, otherwise false
	 */
	function isValid($name=null) {
		if(! $this->isValidated) {
			$this->validate();
		}		
		// are there errors?
		if(empty($this->fieldErrors)) {
			return true;
		} else {
			// if name is null, there are errors
			if($name != null && !array_key_exists($name, $this->fieldErrors)) {
				return true;
			}
		}
		return false;
	}
	
	/** add the validation to a field (name, value)
	 * 
	 */
	function addFieldValidation($name, $value, $ruleName, $errorMessage=null) {
				
		// check if the rule exists
		if($this->validationRules[$ruleName]==null) {
			die(sprintf(_VALIDATOR_ERROR_VALIDATION_RULE_DOESNOT_EXIST, $ruleName));
		}
		
		// add an array
		$this->fieldValidations[] = array($name, $value, $ruleName, $errorMessage);
		
		// reset the validation status
		$this->isValidated = false;
	}
	
	/** add validation rules
	 *  @param String name of the rule
	 *  @param ValidationRule validationRule ValidationRule
	 */
	function addValidationRule($name, &$validationRule) {	
		$this->validationRules[$name] = &$validationRule;

		// reset the validation status		
		$this->isValidated = false;
	}
	
	/**
	 * Get the ValidationRule by this name
	 */
	function getValidationRule($name) {
		return $this->validationRules[$name];
	}
}

/**
 * ValidationRule, base class. Each ValidationRule class MUST subclass this.
 */
class ValidationRule {

	var $errorMessage = null;

	/**
	 *  default constructor
	 */
	function ValidationRule() {	
	}	
	
	/**
	 * validate the value
	 */
	function validate($value) {
		return true;	
	}
	
	/**
	 * setter
	 */
	function setErrorMessage($errorMessage) {
		$this->errorMessage = $errorMessage;
	}
	/**
	 * getter
	 */
	function getErrorMessage() {
		return $this->errorMessage;	
	}
}

/** RequiredValidationRule
 * 
 */
class RequiredValidationRule extends ValidationRule {

	/**
	 * default constructor
	 */
	function RequiredValidationRule() {
		$this->setErrorMessage(_VALIDATOR_ERROR_REQUIRED);	
	}


	function validate($value) {
		return !empty($value);	
	}
}

/** RegularExpresionValidationRule
 * 
 */
class RegularExpresionValidationRule extends ValidationRule {
	
	var $regularExpression = null;
	
	/**
	 *  default constructor
	 *  @param String regularExpression the regular expression for validating
	 */
	function RegularExpresionValidationRule($regularExpression) {
		$this->regularExpression = $regularExpression;
		$this->setErrorMessage(_VALIDATOR_ERROR_INVALID_EMAIL);	
		
	}
	
	function validate($value) {
		return preg_match($this->regularExpression, $value);
	}		
}

class CombinedValidationRule extends ValidationRule {
	var $validationRule;
	var $combinedValidationRule;
	
	function CombinedValidationRule(&$validationRule) {
		$this->validationRule = $validationRule; 
	}
	
	function combine($validationRule) {
		$this->combinedValidationRule =& new CombinedValidationRule($validationRule);
	}
	
	function validate($value) {
		if(!$this->validationRule->validate($value)) {
			$this->errorMessage = $this->validationRule->getErrorMessage();
			return false;
		} elseif($this->combinedValidationRule != null && !$this->combinedValidationRule->validate($value)) {
			$this->errorMessage = $this->combinedValidationRule->getErrorMessage();
			return false;
		}
		
		return true;
	}
}
?>