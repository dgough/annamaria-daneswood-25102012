<?php

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

$_MAMBOTS->registerFunction( 'onPrepareContent', 'botchronocontact' );

function botchronocontact( $published, &$row, &$params, $page=0 ) {
	global $mosConfig_absolute_path;

	// define the regular expression for the bot
	$regex = "#{chronocontact}(.*?){/chronocontact}#m";

	if (!$published) {
		$row->text = preg_replace( $regex, '', $row->text );
		return;
	}
	
	$row->text = preg_replace_callback( $regex, 'showform2', $row->text );

	return true;
}

function showform2( &$matches ) {
global $mosConfig_absolute_path, $mosConfig_live_site, $mainframe, $database, $_MAMBOTS;


$mainframe->addCustomHeadTag("<link href='$mosConfig_live_site/components/com_chronocontact/style.css' rel='stylesheet' type='text/css'/>");	
 
 
$matches[0] = preg_replace('/{chronocontact}/i', '', $matches[0]);
$matches[0] = preg_replace('/{\/chronocontact}/i', '', $matches[0]);
$formname = $matches[0]; 
if ( !isset($_MAMBOTS->_content_mambot_params['mosemailcloak']) ) {
		// load mambot params info
		$query = "SELECT params"
		. "\n FROM #__mambots"
		. "\n WHERE element = 'chronocontact'"
		. "\n AND folder = 'content'"
		;
		$database->setQuery( $query );
		$database->loadObject($mambot);	
			
		$_MAMBOTS->_content_mambot_params['chronocontact'] = $mambot;
	}
	
	$mambot = $_MAMBOTS->_content_mambot_params['chronocontact'];
	
 	$botParams 	= new mosParameters( $mambot->params );
 	$type		= $botParams->def( 'type', 1 );

	if (!$type) {
			/*$mainframe->addCustomHeadTag ('<script type="text/javascript" src="' . $mosConfig_live_site . '/mambots/content/chronocontact/jquery.js"></script>');
			$mainframe->addCustomHeadTag ('<script type="text/javascript" src="' . $mosConfig_live_site . '/mambots/content/chronocontact/thickbox.js"></script>');
			$mainframe->addCustomHeadTag ('<link href="' . $mosConfig_live_site . '/mambots/content/chronocontact/thickbox.css" rel="stylesheet" type="text/css" media="screen" />');
	*/
	}


$query = "SELECT * FROM #__chrono_contact WHERE name='".$formname."'";
$database->setQuery( $query );
$rows = $database->loadObjectList();
$paramsvalues = mosParseParams( $rows[0]->paramsall );
if(trim($paramsvalues->imagever) == 'Yes'){
		$imver = '<input name="chrono_verification" type="text" id="chrono_verification" value="">
				&nbsp;&nbsp;<img src="'.$mosConfig_live_site.'/administrator/components/com_chronocontact/chrono_verification.php">';
	}
	
	
	
	
	$htmlstring = $rows[0]->html;
	
	if( trim($paramsvalues->validate) == 'Yes'){
	// Easy Validation //
			preg_match_all('/name=("|\').*?("|\')/i', $htmlstring, $matches);
			$arr_required = explode(",", $paramsvalues->val_required);
			$arr_validate_number = explode(",", $paramsvalues->val_validate_number);
			$arr_validate_digits = explode(",", $paramsvalues->val_validate_digits);
			$arr_validate_alpha = explode(",", $paramsvalues->val_validate_alpha);
			$arr_validate_alphanum = explode(",", $paramsvalues->val_validate_alphanum);
			$arr_validate_date = explode(",", $paramsvalues->val_validate_date);
			$arr_validate_email = explode(",", $paramsvalues->val_validate_email);
			$arr_validate_url = explode(",", $paramsvalues->val_validate_url);
			$arr_validate_date_au = explode(",", $paramsvalues->val_validate_date_au);
			$arr_validate_currency_dollar = explode(",", $paramsvalues->val_validate_currency_dollar);
			$arr_validate_selection = explode(",", $paramsvalues->val_validate_selection);
			$arr_validate_one_required = explode(",", $paramsvalues->val_validate_one_required);
			
			$arr_all = array_merge($arr_required, $arr_validate_number, $arr_validate_digits, $arr_validate_alpha, $arr_validate_alphanum, $arr_validate_date, $arr_validate_email, $arr_validate_url, $arr_validate_date_au, 
			$arr_validate_currency_dollar, $arr_validate_selection, $arr_validate_one_required);
			
			foreach ($matches[0] as $match)
			{
				$new_match = preg_replace('/name=("|\')/i', '', $match);
				$new_match2 = preg_replace('/("|\')/', '', $new_match);
				$name = preg_replace('/name=("|\')/', '', $new_match2);
				$class_array = array();
				if(in_array($name,$arr_all)){
					if(in_array($name,$arr_required)){
						$class_array[] = "required";
					}
					if(in_array($name,$arr_validate_number)){
						$class_array[] = "validate-number";
					}
					if(in_array($name,$arr_validate_digits)){
						$class_array[] = "validate-digits";
					}
					if(in_array($name,$arr_validate_alpha)){
						$class_array[] = "validate-alpha";
					}
					if(in_array($name,$arr_validate_alphanum)){
						$class_array[] = "validate-alphanum";
					}
					if(in_array($name,$arr_validate_date)){
						$class_array[] = "validate-date";
					}
					if(in_array($name,$arr_validate_email)){
						$class_array[] = "validate-email";
					}
					if(in_array($name,$arr_validate_url)){
						$class_array[] = "validate-url";
					}
					if(in_array($name,$arr_validate_date_au)){
						$class_array[] = "validate-date-au";
					}
					if(in_array($name,$arr_validate_currency_dollar)){
						$class_array[] = "validate-currency-dollar";
					}
					if(in_array($name,$arr_validate_selection)){
						$class_array[] = "validate-selection";
					}
					if(in_array($name,$arr_validate_one_required)){
						$class_array[] = "validate-one-required";
					}
					$class_string = implode(" ",$class_array);
					$htmlstring = str_replace($match,$match.' class="'.$class_string.'"',$htmlstring);
				}
			}
		$rows[0]->html = $htmlstring;
		}
	/// end validation //
	
	

	
ob_start();
if(!empty($rows[0]->name)){

	?>
	<?php if(!empty($rows[0]->scriptcode)){ 
	echo "<script type='text/javascript'>\n";
	echo "//<![CDATA[\n";
	echo $rows[0]->scriptcode;
	echo "//]]>\n";
	echo "</script>\n";		
	}		
	?>
	<?php if(!empty($rows[0]->submiturl)){ 
	$actionurl = $rows[0]->submiturl;			
	} else {
	$actionurl = 'index.php?option=com_chronocontact&amp;task=send&amp;chronoformname='.$rows[0]->name;
	}		
	?>
	<?php if( trim($paramsvalues->validate) == 'Yes'){ ?>
			<script src="components/com_chronocontact/js/prototype.js" type="text/javascript"></script>
			<script src="components/com_chronocontact/js/effects.js" type="text/javascript"></script>
			<script src="components/com_chronocontact/js/validation.js" type="text/javascript"></script>
		<?php } ?>
		
		
		
		
<form name="<?php echo "ChronoContact_".$rows[0]->name; ?>" id="<?php echo "ChronoContact_".$rows[0]->name; ?>" method="<?php echo $paramsvalues->formmethod; ?>" action="<?php echo $actionurl; ?>" <?php echo $rows[0]->attformtag; ?>>
		<div class="chrono_form">
				<?php 
					if( trim($paramsvalues->enmambots) == 'Yes'){
						global $_MAMBOTS;
						$_MAMBOTS->loadBotGroup( 'content' );
						$rowmam->text = $rows[0]->html;
						$results_mambots = $_MAMBOTS->trigger( 'onPrepareContent', array( &$rowmam, &$params, $page ), true );
						$rows[0]->html = $rowmam->text;
					}
					$rows[0]->html = str_replace('{imageverification}',"<span class='imgver'>".$imver,$rows[0]->html."</span>");
					eval( "?>".$rows[0]->html );
				?>
				</div>
</form>
<?php if( trim($paramsvalues->validate) == 'Yes'){ ?>
			<script type="text/javascript">
				function formCallback(result, form) {
					window.status = "valiation callback for form '" + form.id + "': result = " + result;
				}
				var valid = new Validation('<?php echo "ChronoContact_".$rows[0]->name; ?>', {immediate : true, onFormValidate : formCallback});
			</script>
		<?php } ?>
<!-- You are not allowed to remove or edit the following 3 lines anyway if you didnt buy a license --> 
<div class="chronoform">

</div>
<!-- You are not allowed to remove or edit the above 3 lines anyway if you didnt buy a license -->
	<?php
	} else {
	echo "There is no form with this name, Please check the url and the form management";

	}
	return $result = ob_get_clean();
	ob_end_clean();
	}


?>
