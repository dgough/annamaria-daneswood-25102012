<?php
/** 
* @version $Id: legacybots.php 2695 2006-03-07 20:26:09Z stingrey $
* @package Joomla
* @copyright Copyright (C) 2005 Open Source Matters. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );


$_MAMBOTS->registerFunction( 'onPrepareContent', 'botHP_custom_searchBots' );

/**
* Process any legacy bots in the /mambots directory
*
* THIS FILE CAN BE **SAFELY REMOVED** IF YOU HAVE NO LEGACY MAMBOTS
* @param object A content object
* @param int A bit-wise mask of options
* @param int The page number
*/
$hp_custom_search = '';

function botHP_custom_searchBots( $published, &$row, &$params, $page=0 ) {
	global $mosConfig_absolute_path, $_MAMBOTS, $database, $hp_custom_search, $mainframe, $hpBotParams;

	// simple performance check to determine whether bot should process further
	if ( strpos( $row->text, 'property' ) === false ) {
		return true;
	}
	
	// define the regular expression for the bot
	$regex = "#{property}(.*?){/property}#s";

	// check whether mambot has been unpublished
	if ( !$published ) {
		$row->text = preg_replace( $regex, '', $row->text );
		return true;
	}
	
	if(!preg_match( $regex, $row->text)){
		return false;
	}
	
	// check if param query has previously been processed
	if ( !isset($_MAMBOTS->_content_mambot_params['hotproperty_custom_search']) ) {
		// load mambot params info
		$query = "SELECT params"
		. "\n FROM #__mambots"
		. "\n WHERE element = 'hotproperty_custom_search'"
		. "\n AND folder = 'content'"
		;
		$database->setQuery( $query );
		$database->loadObject($mambot);

	
		// save query to class variable
		$_MAMBOTS->_content_mambot_params['hotproperty_custom_search'] = $mambot;
	}
	// pull query data from class variable
	$mambot = $_MAMBOTS->_content_mambot_params['hotproperty_custom_search'];
 	$hpBotParams = new mosParameters( $mambot->params );
 	
	
	if($hpBotParams->get('module')){
		
		preg_match_all( $regex, $row->text , $matches);
		if($matches[0])	$hp_custom_search = botHP_custom_search_replacer($matches);				
		$row->text = preg_replace( $regex, '', $row->text );
		
	}else{
		// perform the replacement
		$return = preg_replace_callback( $regex, 'botHP_custom_search_replacer', $row->text );
		
		if($hpBotParams->get('jump_link')){
			$url = sefRelToAbs(mosGetParam($_SERVER,'REQUEST_URI',''));
			$return =
			"<div style='text-align: right'>
			<a href='$url#full'>".$hpBotParams->get('jump_text','Read full article')."</a>
			</div>
			$return
			<div style='margin-bottom: 20px;'><a name='full'></a></div>";
		}
		$row->text = $return;
	}
	
	return true;
}

/**
* Replaces the matched tags an image
* @param array An array of matches (see preg_match_all)
* @return string
*/
function botHP_custom_search_replacer( &$matches ) {
	global $mosConfig_absolute_path, $mainframe,$mosConfig_lang,$option,$task, $hpBotParams,
	$hp_imgdir_standard,$hp_imgdir_thumb,$hp_imgdir_original,$hp_imgdir_agent,$hp_imgdir_company,$hp_img_noimage_thumb,$hp_img_noimage_standard,
	$hp_imgsize_standard,$hp_imgsize_thumb,$hp_imgsize_agent,$hp_imgsize_company,$hp_quality_photo,$hp_quality_agent,$hp_quality_company,
	$hp_img_connector,$hp_img_method,$hp_img_netpbmpath,$hp_img_impath,$hp_img_saveoriginal,$hp_link_open_newwin,$hp_show_thumb,$hp_show_moreinfo,
	$hp_show_pdficon,$hp_show_printicon,$hp_show_emailicon,$hp_default_agent,$hp_default_company,$hp_use_companyagent,$hp_auto_approve,$hp_css,
	$hp_currency,$hp_thousand_sep,$hp_thousand_string,$hp_dec_point,$hp_dec_string,$hp_language,$hp_use_advsearch,$hp_use_diplaynum,$hp_use_sort_name,
	$hp_use_sort_agent,$hp_use_sort_price,$hp_use_sort_suburb,$hp_use_sort_state,$hp_use_sort_country,$hp_use_sort_type,$hp_use_sort_modified,
	$hp_use_sort_hits,$hp_default_order,$hp_default_order2,$hp_default_limit,$hp_default_limit_agent,$hp_default_limit_co,$hp_default_limit_search,
	$hp_default_limit_featured,$hp_fp_featured_count,$hp_fp_show_featured,$hp_fp_show_search,$hp_log_search,$hp_agent_groupid,$hp_agent_usertype,
	$hp_show_agentdetails,$hp_show_enquiryform,$hp_show_guide,$hp_old_url;
	
	//We must globalise ALL HP variables here as they are not accessible from within nested functions
	$_REQUEST_ORIGINAL = $_REQUEST;
	$_GET_ORIGINAL = $_GET;
	$_option_original = $option;
	$_task_original = $task;
	
	$text = $matches[1];
	$default = $hpBotParams->get('default_request');
		
	//Clean the strings
	$text = str_replace('&amp;','&',$text);
	$default = str_replace('&amp;','&',$default);

	//Convert the strings to new request arrays
	parse_str($text, $_REQUEST_NEW);
	parse_str($default, $_REQUEST_DEFAULT);
	
	//Merge the default and the new request array
	$_REQUEST_NEW = array_merge($_REQUEST_DEFAULT,$_REQUEST_NEW);
	
	//Set the new variables in the request array for HP
	$option = 'com_hotproperty';
	$task = 'asearch';
	$_REQUEST_NEW['option'] = $option;
	$_REQUEST_NEW['task'] = $task;
	$_REQUEST_NEW['filter'] = 'below';
	
	//Set some default options for hp
	$titles = $hpBotParams->get('titles');
	if(!$titles) $_REQUEST_NEW['titles'] = 0;
	$pagination = $hpBotParams->get('pagination');
	if(!$pagination) $_REQUEST_NEW['pagination'] = 0;
	$pathway = $hpBotParams->get('pathway');
	if(!$titles) $_REQUEST_NEW['pathway'] = 0;

	//Set the new request and get array
	$_REQUEST = array_merge($_REQUEST, $_REQUEST_NEW);
	$_GET = array_merge($_GET, $_REQUEST_NEW);
	
	//Load HP and buffer the output
	$mainframe->_setAdminPaths($option);
	ob_start();
	include_once($mosConfig_absolute_path.'/components/com_hotproperty/hotproperty.php');
	$return = ob_get_contents();
	ob_end_clean();
	
	//Reset the request array and admin paths
	$_REQUEST = $_REQUEST_ORIGINAL;
	$_GET = $_GET_ORIGINAL;
	$option = $_option_original;
	$task = $_task_original;
	$mainframe->_setAdminPaths($option);
	
	return '<div class="hp_custom_search">'.$return.'</div>';
}
?>