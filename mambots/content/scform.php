<?php
/**
* Components Lab Support center
* www.componentslab.com
*
* @package SupportCenter
* @copyright (C) 2000-2007 Components Lab, Lda.,
* @license Commercial
*
**/

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
* Show Form Mambot
*
* <b>Usage:</b>
* <code>{scform}...form id...{/scform}</code>
*/

global $mosConfig_lang, $mosConfig_absolute_path;

include_once( $mosConfig_absolute_path.'/components/com_supportcenter/includes/common.php' );
include_once( $mosConfig_absolute_path.'/components/com_supportcenter/classes/spam.class.php' );

$_MAMBOTS->registerFunction( 'onPrepareContent', 'botCLABForms' );

function botCLABForms( $published, &$row, &$params, $page=0 ) {
	global $database, $my, $mosConfig_absolute_path, $mosConfig_lang;

	if( file_exists( $mosConfig_absolute_path.'/components/com_supportcenter/language/'.$mosConfig_lang.'.php' ) ) {
		include( $mosConfig_absolute_path.'/components/com_supportcenter/language/'.$mosConfig_lang.'.php' );
	}else{
		include( $mosConfig_absolute_path.'/components/com_supportcenter/language/english.php' );
	}
	
	$blocker = new formSpamBotBlocker();
	$blocker->setTrap(true,"sptr@p","Do not enter anything in this field!");
	$hiddenTags = $blocker->makeTags(); // create the xhtml string containing the required form elements
	
	// next line is a fix for /m option not working as it should??
	$row->text = str_replace( "\n", "__CRLF__", $row->text );
	preg_match_all( "/{scform}(.*?){\/scform}/im", $row->text, $bots, PREG_SET_ORDER );

	// split the text around the mambot
	$text = preg_split( "/{scform}(.*?){\/scform}/im", $row->text );

	// count the number of forms present
	$n = count( $text );
	if ($n > 0) {
		$row->text = '';
		for ($i=0; $i < $n; $i++) {
			$row->text .= str_replace( "__CRLF__", "\n", $text[$i] );
			if (trim( @$bots[$i][1] )) {
				$code = @$bots[$i][1];
				$code = str_replace( "\r", "", $code );
				$code = str_replace( "__CRLF__", "\n", $code );
				
				//Get the form component Itemid ***************************************************************
				$database->setQuery( "SELECT id FROM #__components WHERE link='option=com_supportcenter'" );
				$comItemid = $database->loadResult();

				//Get the form info ***************************************************************
				$database->setQuery( "SELECT * FROM #__support_form WHERE id='".$code."'" );
				$rowForm = null;
				$database->loadObject( $rowForm );
				
				//Get the form actions ************************************************************
				$database->setQuery( "SELECT * FROM #__support_form_action WHERE id_form='".$code."'" );
				$rowActions = $database->loadObjectList();
				
				//Get the form fields *************************************************************
				$database->setQuery( "SELECT * FROM #__support_form_field WHERE id_form='".$code."' ORDER BY `order`" );
				$rowFields = $database->loadObjectList();

				//Builds the validation javascript ************************************************
				$row->text .= "<script>\n";
				$row->text .= "function ValidateFields() {\n";

				for ($x=0; $x < count($rowFields); $x++) {
					$rowField = $rowFields[$x];
					if($rowField->required=="1") {
						$row->text .= "if(document.".str_replace(" ", "", $rowForm->name).".custom".$rowField->id.".value==\"\") {\n";
						$row->text .= "     alert(\"".str_replace( '%1', $rowField->caption, $sc2lang['field_required'] )."\")\n";
						$row->text .= "     document.".str_replace(" ", "", $rowForm->name).".custom".$rowField->id.".focus();\n";
						$row->text .= "     return false\n";
						$row->text .= "}\n";
					}
				}
				
				$row->text .= "return true\n";
				$row->text .= "}\n";
				$row->text .= "</script>\n";
				
				//Builds the form *****************************************************************
				$row->text .= "<form name='".str_replace(" ", "", $rowForm->name)."' action='index.php?option=com_supportcenter&task=forms_save&Itemid=".$comItemid."' method='POST' onSubmit='return ValidateFields();'>\n";
				$row->text .= "<table width='100%'>\n";
				
				for ($x=0; $x < count($rowFields); $x++) {
					$rowField = $rowFields[$x];
					$row->text .= '<tr>';
					$row->text .= '<td width="100">'.utf8_decode($rowField->caption).'</td>';
					$row->text .= '<td>'.WriteField( 0, $rowField->id, $rowField->type, $rowField->value, $rowField->size, $rowField->maxlength, 0, $code ).($rowField->required ? '<img src="components/com_supportcenter/images/16px/link.png" border="0" alt="'.$sc2lang['required'].'" align="absmiddle" />' : '').'</td>';
					$row->text .= '</tr>';
				}
				
				$row->text .= "</table>\n";
				$row->text .= '<p>'.$sc2lang['field_required_desc']."\n<br />";
				$row->text .= "<input type='submit' name='submit' value='".$sc2lang['save']."' class='button'></p>\n";
				$row->text .= "<input type='hidden' name='redirect' value='".$rowForm->redirect."'>\n";
				$row->text .= "<input type='hidden' name='id' value='".$rowForm->id."'>\n";
				$row->text .= "<input type='hidden' name='pageurl' value='".$_SERVER["PHP_SELF"]."'>\n";
				$row->text .= $hiddenTags;
				$row->text .= "</form>\n";
			}
		}
	} else {
		$row->text = str_replace( "__CRLF__", "\n", $row->text );
	}
}

?>