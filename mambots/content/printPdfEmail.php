<?php
/**
* @version $Id: printPdfEmail.php 1.0
* @copyright Information Madness.
* @license GNU/GPL,
* @author Information Madness - http://www.informationmadness.com/cms
*/

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );

$_MAMBOTS->registerFunction( 'onPrepareContent', 'botPrintPdfEmail' );

function botPrintPdfEmail( $published, &$row, &$params, $page=0 )
{	
	global $mainframe, $Itemid, $database, $_MAMBOTS;

	// Hide adding tags on Modules - tags on modules is not supported 
	if (@$row->content){
		return;
	}
	
	$option =  mosGetParam($_REQUEST, 'option', '');
    
	$task = mosGetParam($_REQUEST,'task','view');
	
	$id = mosGetParam($_REQUEST,'id','');

	// check whether plugin has been unpublished
	if ( !$published ) {
		return true;
	}
	
	if($option == 'com_content' || $option == 'com_frontpage'){

		if ( !isset($_MAMBOTS->_content_mambot_params['printPdfEmail']) ) {
			// load J!Tags info
			$query = "SELECT params"
			. "\n FROM #__mambots"
			. "\n WHERE element = 'printPdfEmail'"
			. "\n AND folder = 'content'"
			;
			$database->setQuery( $query );
			$database->loadObject($mambot);
			
			// save query to class variable
			$_MAMBOTS->_content_mambot_params['printPdfEmail'] = $mambot;
		}
		
		$mambot = $_MAMBOTS->_content_mambot_params['printPdfEmail'];
	    $botParams = new mosParameters( $mambot->params );
	    
		$beforeOrAfter = $botParams->get('beforeOrAfter','before');
		$alignment = $botParams->get('alignment','right');
		
		$text = "<div style='clear: " . $alignment . "; float:" . $alignment . "' class='icons_bot'>";

		// display pdf icon
		$text .= PdfIcon( $row, $params, !$botParams->get('jos_popup', 1) );

		// displays Print Icon
		$text .= PrintIcon( $row, $params, !$botParams->get('jos_popup', 1) );

		// displays Email Icon
		$text .= EmailIcon( $row, $params, !$botParams->get('jos_popup', 1) );
				
		$text .= '</div><div style="clear: '.$alignment.'"></div>';
		
		if($beforeOrAfter == 'before'){
			$row->text = $text . $row->text;
		}else{
			$row->text .= $text;
		}

		return true;
	}
}

	/**
	* Writes PDF icon
	*/
	function PdfIcon( &$row, &$params, $hide_js ) {
		global $mosConfig_live_site;

		if ( $params->get( 'pdf' ) && !$params->get( 'popup' ) && !$hide_js ) {

			$link 	= $mosConfig_live_site. '/index2.php?option=com_content&amp;do_pdf=1&amp;id='. $row->id;
			$js = "window.open('$link', '".rand(0, 100)."', 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=1,directories=no,width=640,height=640,left = ((window.screen.width/2)) - 320),top = ((window.screen.height/2) - 320)')";
			
			if ( $params->get( 'icons' ) ) {
				$image = mosAdminMenus::ImageCheck( 'pdf_button.png', '/images/M_images/', NULL, NULL, _CMN_PDF, NULL );
			} else {
				$image = _CMN_PDF .'&nbsp;';
			}
			// ------------------------ A8E fixes start ------------------------
			
			if($hide_js) $onclick = "onclick='$js; return false'";
			else $onclick = '';
			
			return "<a href='$link' $onclick title='"._CMN_PDF."' class='pdf_icon' rel='nofollow'>$image</a>";
			// ------------------------ A8E fixes end ------------------------
		}
	}


	/**
	* Writes Email icon
	*/
	function EmailIcon( &$row, &$params, $hide_js ) {
		// ------------------------ A8E fixes start ------------------------
		global $mosConfig_live_site, $Itemid, $task;
		if ( $params->get( 'email' ) ) {
			
			if ($task == 'view') {
				$_Itemid = '&amp;Itemid='. $Itemid;
			} else {
				$_Itemid = '';
			}
			
			$link	= $mosConfig_live_site .'/index.php?option=com_content&amp;task=emailform&amp;id='. $row->id . $_Itemid;
			$link2	= $mosConfig_live_site .'/index2.php?option=com_content&amp;task=emailform&amp;id='. $row->id . $_Itemid;
			$js = "window.open('$link2', '".rand(0, 100)."', 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=1,directories=no,width=400,height=300,left = ((window.screen.width/2)) - 200),top = ((window.screen.height/2) - 150)')";
			
			if ( $params->get( 'icons' ) ) {
				$image = mosAdminMenus::ImageCheck( 'emailButton.png', '/images/M_images/', NULL, NULL, _CMN_EMAIL, NULL );
			} else {
				$image = '&nbsp;'. _CMN_EMAIL;
			}
			
			if($hide_js) $onclick = "onclick='$js; return false'";
			else $onclick = '';
			
			return "<a href='$link' $hide_js title='"._CMN_EMAIL."' class='email_icon' rel='nofollow'>$image</a>";
			
		}
		// ------------------------ A8E fixes end ------------------------
	}
	
	
	/**
	* Writes Print icon
	*/
	function PrintIcon( &$row, &$params, $hide_js, $status=NULL ) {
		if ( $params->get( 'print' )  && !$hide_js ) {
			// use default settings if none declared
			if ( !$status ) {
				$status = 'status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no';
			}
			
			$link = $mosConfig_live_site. '/index2.php?option=com_content&amp;task=view&amp;id=' . $row->id .'&amp;pop=1&amp;page='. $page . $row->Itemid_link;
			$js = "window.open('$link', '".rand(0, 100)."', 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=1,directories=no,width=400,height=300,left = ((window.screen.width/2)) - 200),top = ((window.screen.height/2) - 150)')";

			// checks template image directory for image, if non found default are loaded
			if ( $params->get( 'icons' ) ) {
				$image = mosAdminMenus::ImageCheck( 'printButton.png', '/images/M_images/', NULL, NULL, _CMN_PRINT, NULL );
			} else {
				$image = _ICON_SEP .'&nbsp;'. _CMN_PRINT. '&nbsp;'. _ICON_SEP;
			}

			// ------------------------ A8E fixes start ------------------------
			if ( $params->get( 'popup' ) && !$hide_js ) {
				// Print Preview button - used when viewing page
				
				return 
				"<script language='javascript' type='text/javascript'>
				<!--
				document.write('<a href='#' onclick='window.print(); return false;' title='"._CMN_PRINT."' class='print_icon'>');
				document.write('$image');
				document.write('</a>');
				window.onload = self.print();
				//-->
				</script>";		
							
			} else {	
	
				if($hide_js) $onclick = "onclick='$js; return false'";
				else $onclick = '';
				
				// ------------------------ A8E fixes start ------------------------			
				return "<a href='$link' $onclick rel='nofollow' title='"._CMN_PRINT."' class='print_icon'>$image</a>";
			
			}
			// ------------------------ A8E fixes end ------------------------
		}
	}	
?>