<?php
/**
* @version $Id: legacybots.php 2695 2006-03-07 20:26:09Z stingrey $
* @package Joomla
* @copyright Copyright (C) 2005 Open Source Matters. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );

$_MAMBOTS->registerFunction( 'onPrepareContent', 'botAddthis' );

/**
* Process any legacy bots in the /mambots directory
*
* THIS FILE CAN BE **SAFELY REMOVED** IF YOU HAVE NO LEGACY MAMBOTS
* @param object A content object
* @param int A bit-wise mask of options
* @param int The page number
*/
function botAddthis( $published, &$row, &$params, $page=0 ) {
	global $_MAMBOTS, $database, $task, $option;



	// check whether mambot has been unpublished
	if ( !$published || ($option == 'com_content' && $task != 'view') ) {
		return true;
	}

	if ( !isset($_MAMBOTS->_content_mambot_params['addthis']) ) {
		// load J!Tags info
		$query = "SELECT params"
			. "\n FROM #__mambots"
			. "\n WHERE element = 'addthis'"
			. "\n AND folder = 'content'"
		;
		$database->setQuery( $query );
		$database->loadObject($mambot);

		// save query to class variable
		$_MAMBOTS->_content_mambot_params['addthis'] = new mosParameters($mambot->params);
	}
	
	$bot_params = $_MAMBOTS->_content_mambot_params['addthis'];

	$catid = $bot_params->get('catid') ? explode(',', $bot_params->get('catid')) : array();

	if(!isset($row->catid) || (count($catid) && !in_array($row->catid, $catid))) return true;

	$addthis = '<!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style ">
<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
<a class="addthis_button_tweet"></a>
<a class="addthis_button_pinterest_pinit"></a>
<a class="addthis_counter addthis_pill_style"></a>
</div>
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-5017e62152cd36c1"></script>
<!-- AddThis Button END -->';

	$row->text = $addthis.$row->text;
}