<?php
/**
* Content Plug for BannersManager
* Author: Thomas PAPIN
* URL:  http://www.joomprod.com
* mail: webmaster@joomprod.com
* Based on Missus Plug for CB
**/

$_MAMBOTS->registerFunction( 'onGetInfo', 'botContent_getInfo' );
$_MAMBOTS->registerFunction( 'onGetValue', 'botContent_getValue' );
$_MAMBOTS->registerFunction( 'onSave', 'botContent_getOptionName' );

function botContent_getOptionName() {
	return "com_content";
}

function botContent_getInfo() {
	global $database;
	
	$info->name = "Content";
	$info->description = "Description";
	$info->option = "com_content";
	$info->list = array();
	
	$database->setQuery( "SELECT c.title as ctitle,c.id as cid, ".
						 "s.title as stitle,s.id as sid, ".
						 "cat.title as cattitle,cat.id as catid ".
						 "FROM #__content as c ".
						 "LEFT JOIN #__categories as cat ON c.catid = cat.id ".
						 "LEFT JOIN #__sections as s ON c.sectionid = s.id ".
						 "WHERE 1 ".
						 "ORDER BY s.ordering,cat.ordering,c.ordering");
					 
	$contents = $database->loadObjectList();
	
						
	$info->list[] = mosHTML::makeOption( "0", "All");
	$info->list[] = mosHTML::makeOption( "-1", "----");
	$info->list[] = mosHTML::makeOption( "-1", "None");
	$info->list[] = mosHTML::makeOption( -1, "----");
	$current_section = 0;
	$current_category = 0;
	foreach($contents as $content)
	{
		if ($content->sid == null)
		{	
			$info->list[] = mosHTML::makeOption( "c".$content->cid, "Static page | ".$content->ctitle);
		} 
		else if ($current_section != $content->sid)
		{
			$current_section = $content->sid;
			$current_category = $content->catid;
			$info->list[] = mosHTML::makeOption( -1, "----");
			$info->list[] = mosHTML::makeOption( "s".$content->sid, $content->stitle);
			$info->list[] = mosHTML::makeOption( "cat".$content->catid, $content->stitle." | ".$content->cattitle);
			$info->list[] = mosHTML::makeOption( "c".$content->cid, $content->stitle." | ".$content->cattitle. " | ".$content->ctitle);	
		}
		else if ($current_category != $content->catid)
		{
			$current_category = $content->catid;
			$info->list[] = mosHTML::makeOption( "cat".$content->catid, $content->stitle." | ".$content->cattitle);
			$info->list[] = mosHTML::makeOption( "c".$content->cid, $content->stitle." | ".$content->cattitle. " | ".$content->ctitle);	
		}
		else
		{
			$info->list[] = mosHTML::makeOption( "c".$content->cid, $content->stitle." | ".$content->cattitle. " | ".$content->ctitle);	
		}
	}
	return $info;
}

function botContent_getValue() 
{	
	$option	= mosGetParam( $_GET, 'option', "" );
	$task	= mosGetParam( $_GET, 'task', "" );
	$id     = intval(mosGetParam( $_GET, 'id', 0 ));
	
	if ($option == "com_content")
	{
		switch($task)
		{
			case "section":
			case "blogsection":
				return "s".$id;
			case "category":
			case "blogcategory":
				return "cat".$id;
			case "view":
				return "c".$id;
		}
		return -5;
	}
	else
	{
		return -5;
	}
}

?>