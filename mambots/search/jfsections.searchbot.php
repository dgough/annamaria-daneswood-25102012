<?php
/**
 * Joom!Fish - Multi Lingual extention and translation manager for Joomla!
 * Copyright (C) 2003-2007 Think Network GmbH, Munich
 * Copyright (C) 2005 Open Source Matters. All rights reserved.
 *
 * All rights reserved.  The Joom!Fish project is a set of extentions for
 * the content management system Joomla!. It enables Joomla!
 * to manage multi lingual sites especially in all dynamic information
 * which are stored in the database.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * The "GNU General Public License" (GPL) is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * -----------------------------------------------------------------------------
 * $Id: jfweblinks.searchbot.php 567 2007-07-17 05:53:43Z akede $
 * @package joomfish
 * @subpackage sections.searchbot
 *
*/

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );

$_MAMBOTS->registerFunction( 'onSearch', 'botSearchJFSections' );

/**
* Sections Search method
*
* The sql must return the following fields that are used in a common display
* routine: href, title, section, created, text, browsernav
* @param string Target search string
* @param string mathcing option, exact|any|all
* @param string ordering option, newest|oldest|popular|alpha|category
*/
function botSearchJFSections( $text, $phrase='', $ordering='' ) {
	global $database, $my, $_MAMBOTS, $mosConfig_lang;

	// check if param query has previously been processed
	if ( !isset($_MAMBOTS->_search_mambot_params['jfsections']) ) {
		// load mambot params info
		$query = "SELECT params"
		. "\n FROM #__mambots"
		. "\n WHERE element = 'jfsections.searchbot'"
		. "\n AND folder = 'search'"
		;
		$database->setQuery( $query );
		$database->loadObject($mambot);

		// save query to class variable
		$_MAMBOTS->_search_mambot_params['jfsections'] = $mambot;
	}

	// pull query data from class variable
	$mambot = $_MAMBOTS->_search_mambot_params['jfsections'];

	$botParams = new mosParameters( $mambot->params );

	$limit = $botParams->def( 'search_limit', 50 );
	$activeLang = $botParams->def( 'active_language_only', 0);

	 $text = trim( $text );
	if ($text == '') {
		return array();
	}

	switch ( $ordering ) {
		case 'alpha':
			$order = 'a.name ASC';
			break;

		case 'category':
		case 'popular':
		case 'newest':
		case 'oldest':
		default:
			$order = 'a.name DESC';
	}

	$query = "SELECT a.id, a.name AS title,"
	. "\n a.description AS text,"
	. "\n '' AS created,"
	. "\n '2' AS browsernav,"
	. "\n a.id AS secid, m.id AS menuid, m.type AS menutype"
	. "\n FROM #__sections AS a"
	. "\n LEFT JOIN #__menu AS m ON m.componentid = a.id"
	. "\n LEFT JOIN #__jf_content as jfc ON jfc.reference_id = a.id"
	. "\n LEFT JOIN #__languages as jfl ON jfc.language_id = jfl.id"
	. "\n WHERE LOWER(jfc.value) LIKE '%$text%'"
	. "\n AND a.published = 1"
	. "\n AND jfc.published = 1"
	. "\n AND a.access <= " . (int) $my->gid
	. "\n AND jfc.reference_table = 'sections'"
	. ( $activeLang ? "\n AND jfl.code = '$mosConfig_lang'" : '')
	. "\n AND ( m.type = 'content_section' OR m.type = 'content_blog_section' )"
	. "\n GROUP BY a.id"
	. "\n ORDER BY $order"
	;
	$database->setQuery( $query, 0, $limit );
	$rows = $database->loadObjectList();

	$count = count( $rows );
	for ( $i = 0; $i < $count; $i++ ) {
		if ( $rows[$i]->menutype == 'content_section' ) {
			$rows[$i]->href 	= 'index.php?option=com_content&task=section&id='. $rows[$i]->secid .'&Itemid='. $rows[$i]->menuid;
			$rows[$i]->section 	= _SEARCH_SECLIST;
		}
		if ( $rows[$i]->menutype == 'content_blog_section' ) {
			$rows[$i]->href 	= 'index.php?option=com_content&task=blogsection&id='. $rows[$i]->secid .'&Itemid='. $rows[$i]->menuid;
			$rows[$i]->section 	= _SEARCH_SECBLOG;
		}
	}

	return $rows;
}
?>