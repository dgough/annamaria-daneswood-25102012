<?php
/**
 * Joom!Fish - Multi Lingual extention and translation manager for Joomla!
 * Copyright (C) 2003-2007 Think Network GmbH, Munich
 * Copyright (C) 2005 Open Source Matters. All rights reserved.
 * 
 * All rights reserved.  The Joom!Fish project is a set of extentions for 
 * the content management system Joomla!. It enables Joomla! 
 * to manage multi lingual sites especially in all dynamic information 
 * which are stored in the database.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * The "GNU General Public License" (GPL) is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * -----------------------------------------------------------------------------
 * $Id: jfcontact.searchbot.php 569 2007-07-17 08:53:20Z akede $
 * @package joomfish
 * @subpackage contact.searchbot
 *
*/

/** ensure this file is being included by a parent file */
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

$_MAMBOTS->registerFunction( 'onSearch', 'botSearchJFContact' );

/**
* Search method
*
* The sql must return the following fields that are used in a common display
* routine: href, title, section, created, text, browsernav
* @param string Target search string
* @param integer The state to search for -1=archived, 0=unpublished, 1=published [default]
* @param string A prefix for the section label, eg, 'Archived '
*/
function botSearchJFContact( $text, $phrase='', $ordering='' ) {
	global $my, $database,$_MAMBOTS;
	global $mosConfig_abolute_path, $mosConfig_lang, $mosConfig_offset;
	$_SESSION['searchword'] = $text;
	
	// check if param query has previously been processed
	if ( !isset($_MAMBOTS->_search_mambot_params['jfcontact']) ) {
		// load mambot params info
		$query = "SELECT params"
		. "\n FROM #__mambots"
		. "\n WHERE element = 'jfcontact.searchbot'"
		. "\n AND folder = 'search'"
		;
		$database->setQuery( $query );
		$database->loadObject($mambot);	
		
		// save query to class variable
		$_MAMBOTS->_search_mambot_params['jfcontact'] = $mambot;
	}
	
	// pull query data from class variable
	$mambot = $_MAMBOTS->_search_mambot_params['jfcontact'];	
	
	$botParams = new mosParameters( $mambot->params );
	$limit 		= $botParams->def( 'search_limit', 50 );
	$nonmenu	= $botParams->def( 'nonmenu', 1 );
	$activeLang = $botParams->def( 'active_language_only', 0);
	
	$nullDate 	= $database->getNullDate();
	$now 		= defined("_CURRENT_SERVER_TIME")?constant("_CURRENT_SERVER_TIME"):date( 'Y-m-d H:i', time() );

	$text = trim( $text );
	if ($text == '') {
		return array();
	}

	$wheres = array();
	switch ($phrase) {
		case 'exact':
			$where = "LOWER(jfc.value) LIKE '%$text%'";
			break;
		case 'all':
		case 'any':
		default:
			$words = explode( ' ', $text );
			$wheres = array();
			foreach ($words as $word) {
				$wheres[] = "LOWER(jfc.value) LIKE '%$word%'";
			}
			$where = '(' . implode( ($phrase == 'all' ? ') AND (' : ') OR ('), $wheres ) . ')';
			break;
	}

	$morder = '';
	switch ($ordering) {
		case 'newest':
		case 'oldest':
		case 'popular':
			$order = 'a.name DESC';
			break;
		case 'category':
			$order = 'b.title ASC, title ASC';
			$morder = 'title ASC';
			break;
		case 'alpha':
		default:
			$order = 'title ASC';
			break;
	}

	// search contact
	$section 	= _CONTACT_TITLE; 
	$query = "SELECT  DISTINCT a.id, a.name AS title,"
	. "\n a.con_position, a.misc,"
	. "\n '' AS created,"
	. "\n CONCAT_WS( ' / ', " . $database->Quote( $section ) . ", b.title ) AS section,"
	. "\n '2' AS browsernav,"
	. "\n CONCAT( 'index.php?option=com_contact&task=view&contact_id=', a.id ) AS href"
	. "\n ,jfl.code as jflang"
	. "\n FROM #__contact_details AS a"
	. "\n LEFT JOIN #__jf_content as jfc ON reference_id = a.id"
	. "\n LEFT JOIN #__languages as jfl ON jfc.language_id = jfl.id"
	. "\n INNER JOIN #__categories AS b ON b.id = a.catid"
	. "\n WHERE $where"
	. "\n AND a.published = 1"
	. "\n AND b.published = 1"
	. "\n AND a.access <= " . (int) $my->gid
	. "\n AND b.access <= " . (int) $my->gid
	. "\n AND jfc.reference_table = 'contact_details'"
	. ( $activeLang ? "\n AND jfl.code = '$mosConfig_lang'" : '')
	. "\n ORDER BY $order"
	;
	$database->setQuery( $query, 0, $limit );
	$rows= $database->loadObjectList();

	foreach ($rows as $key=>$row){
		$rows[$key]->text =  $row->title.", ".$row->con_position.", ".$row->misc;
	}

	return $rows;
}
?>
