<?php
//*******************************************************
//* SEF Service Map Component Integrator
//* http://fun.kubera.org
//* (C) Radoslaw Kubera
//* license http://www.gnu.org/copyleft/gpl.html GNU/GPL
//*******************************************************


/** ensure this file is being included by a parent file */
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

function Content_categories ($id, $level,$Itemid,$params)
{
  $priority=$params->get( 'priority', '' ) ;
  $changefreq=$params->get( 'changefreq', '' );
  $show_cat_desc=intval($params->get( 'show_content_desc', '0' )) ;
  $show_item_icons=intval($params->get( 'show_item_icons', '1' )) ;
  global $database,$gid,$mosConfig_offset;
  global $my;
  $lev=$level;
  $lev++;

  $now = date( 'Y-m-d H:i:s', time() + $mosConfig_offset * 60 * 60 );
  $xwhere = "\n AND state='1'"
		. "\n AND ( publish_up = '0000-00-00 00:00:00' OR publish_up <= '$now' )"
		. "\n AND ( publish_down = '0000-00-00 00:00:00' OR publish_down >= '$now' )"
		;

  $database->setQuery( "SELECT title,id,sectionid,catid,ordering,modified,created,introtext"
	 	. "\nFROM #__content"
		. "\n where catid=$id".$xwhere
                . " and access<=$my->gid order by ordering"
		);
  $contes = $database->loadObjectList();
  if (count($contes)!=0)
  {
    foreach ($contes as $cont)
    { 
      $description = '';
      if ($show_cat_desc) $description = $cont->introtext;
      $element->description = $description;
      
      $linkcont ="index.php?option=com_content&amp;task=view&amp;id=".$cont->id."&amp;Itemid=".$Itemid;
      $element->name = $cont->title;
      $element->link = $linkcont;
      $element->level = $lev;
      $element->priority = $priority;
      $element->changefreq= $changefreq;
      if ($cont->modified> $cont->created) $element->lastmod = $cont->modified; else $element->lastmod = $cont->created;
      if ($show_item_icons) $element->image = 'content_article.gif'; else $element->image='none';
      $element->icon_path='/mambots/com_sefservicemap/';
      AddExtMenuItem ($element);
    } 
  }
}


function Content_sections($id, $level,$Itemid,$params)
{
  $priority=$params->get( 'priority', '' ) ;
  $changefreq=$params->get( 'changefreq', '' );
  $show_cat_icons=intval($params->get( 'show_cat_icons', '1' )) ;
  $show_cat_desc=intval($params->get( 'show_cat_desc', '1' )) ;
  $show_mode=intval($params->get( 'show_mode', '1' )) ;
  global $database;
  global $my;
  $lev=$level;
  $lev++;
  $database->setQuery( "SELECT title,id,section,ordering,description"
	 	. "\nFROM #__categories"
		. "\n where section='$id' and access<=$my->gid and published='1' order by ordering"
		);
  
  $section=$database->loadObjectList();
  if (count($section)!=0)
  {
    foreach ($section as $sec)
    { 
      $description='';
      if ($show_cat_desc) $description = $sec->description;

      $database->setQuery("select count(id) FROM #__content"
		. "\n where catid=$sec->id and access<=$my->gid and state = 1 order by ordering");
      $count=$database->LoadResult();
      if ($count!=0)
      {
        $linkcont ="index.php?option=com_content&amp;task=category&amp;sectionid=".$id."&amp;id=".$sec->id."&amp;Itemid=".$Itemid;
        $element->name = $sec->title;
        $element->link = $linkcont;
        $element->level = $lev;
        if ($show_cat_icons) $element->image = 'content_folder.gif'; else $element->image='none';
        $element->icon_path='/mambots/com_sefservicemap/';
        $element->description=$description;
        $element->priority = $priority;
        $element->changefreq= $changefreq;
        AddExtMenuItem ($element);
        if ($show_mode) Content_categories($sec->id,$lev,$Itemid,$params);
      }
    }
  } 
}

function Content_blogcategory ($id, $level,$Itemid,$params)
{
  $priority=$params->get( 'priority', '' ) ;
  $changefreq=$params->get( 'changefreq', '' );

  $show_cat_desc=intval($params->get( 'show_content_desc', '0' )) ;
  $show_item_icons=intval($params->get( 'show_item_icons', '1' )) ;
  global $database, $mainframe, $mosConfig_offset,$gid;
  $lev=$level+1;
  
  $params = new stdClass();
	if ( $Itemid ) {
		$menu = new mosMenu( $database );
		$menu->load( $Itemid );
		$params =& new mosParameters( $menu->params );
	} else {
		$menu = "";
		$params =& new mosParameters( '' );
	}

	// new blog multiple section handling
	if ( !$id ) {
		$id 		= $params->def( 'categoryid', 0 );
	}
        $categories = explode(",", $id);

 $query = "SELECT a.*"
	. "\n FROM #__content AS a"
	. "\n LEFT JOIN #__categories AS cc ON cc.id = a.catid"
	. "\n LEFT JOIN #__users AS u ON u.id = a.created_by"
	. "\n LEFT JOIN #__content_rating AS v ON a.id = v.content_id"
	. "\n LEFT JOIN #__sections AS s ON a.sectionid = s.id"
	. "\n LEFT JOIN #__groups AS g ON a.access = g.id"
        . "\n WHERE s.access<=$gid AND a.access<=$gid AND cc.access<=$gid "
        . "\n AND a.state >0";
	$database->setQuery( $query );
        $contes=$database->loadObjectList();

  if (count($contes)!=0)
  {
    foreach ($contes as $cont)
    { 
      if (in_array ($cont->catid,$categories))
      {

        $description = '';
        if ($show_cat_desc) $description = $cont->introtext;
        $element->description = $description;

        $linkcont ="index.php?option=com_content&amp;task=view&amp;id=".$cont->id."&amp;Itemid=".$Itemid;
        $element->name = $cont->title;
        $element->link = $linkcont;
        $element->level= $lev;
        $element->priority = $priority;
        $element->changefreq= $changefreq;
        if ($cont->modified> $cont->created) $element->lastmod = $cont->modified; else $element->lastmod = $cont->created;
        if ($show_item_icons) $element->image = 'content_article.gif'; else $element->image='none';
        $element->icon_path='/mambots/com_sefservicemap/';
        AddExtMenuItem ($element);
      }
    } 
  }
}

function Content_blogsections($id, $level,$Itemid,$params)
{
  $priority=$params->get( 'priority', '' ) ;
  $changefreq=$params->get( 'changefreq', '' );
  global $database, $mainframe, $mosConfig_offset,$gid;
  $show_cat_desc=intval($params->get( 'show_content_desc', '0' )) ;
  $show_item_icons=intval($params->get( 'show_item_icons', '1' )) ;

        $lev=$level+1;
	// Parameters
	$params = new stdClass();
	if ( $Itemid ) {
		$menu = new mosMenu( $database );
		$menu->load( $Itemid );
		$params =& new mosParameters( $menu->params );
	} else {
		$menu = "";
		$params =& new mosParameters( '' );
	}

	// new blog multiple section handling
	if ( !$id ) {
		$id		= $params->def( 'sectionid', 0 );
	}
        
       $sections = explode(",", $id);



	
         $query = "SELECT a.*"
	. "\n FROM #__content AS a"
	. "\n INNER JOIN #__categories AS cc ON cc.id = a.catid"
	. "\n LEFT JOIN #__users AS u ON u.id = a.created_by"
	. "\n LEFT JOIN #__content_rating AS v ON a.id = v.content_id"
	. "\n LEFT JOIN #__sections AS s ON a.sectionid = s.id"
	. "\n LEFT JOIN #__groups AS g ON a.access = g.id"
	. "\n WHERE s.access<=$gid AND a.access<=$gid AND cc.access<=$gid "
        . "\n AND a.state >0";
	$database->setQuery( $query );
	$contes = $database->loadObjectList();
   if (count($contes)!=0)
  {
    foreach ($contes as $cont)
    { 
      if (in_array ($cont->sectionid,$sections))
      {
        $description = '';
        if ($show_cat_desc) $description = $cont->introtext;
        $element->description = $description;

        $linkcont ="index.php?option=com_content&amp;task=view&amp;id=".$cont->id."&amp;Itemid=".$Itemid;
        $element->name = $cont->title;
        $element->link = $linkcont;
        $element->level= $lev;
        $element->priority = $priority;
        $element->changefreq= $changefreq;
        if ($cont->modified> $cont->created) $element->lastmod = $cont->modified; else $element->lastmod = $cont->created;
        if ($show_item_icons) $element->image = 'content_article.gif'; else $element->image='none';
        $element->icon_path='/mambots/com_sefservicemap/';
        AddExtMenuItem ($element);
      }
    } 
  }

}

function Content_archivesection ($id, $level,$Itemid,$params)
{
  $priority=$params->get( 'priority', '' ) ;
  $changefreq=$params->get( 'changefreq', '' );
  global $database, $mainframe, $mosConfig_offset,$gid;
  $show_cat_desc=intval($params->get( 'show_content_desc', '0' )) ;
  $show_item_icons=intval($params->get( 'show_item_icons', '1' )) ;

  $lev=$level+1;
  $query = "SELECT a.*"
  . "\n FROM #__content AS a"
  . "\n INNER JOIN #__categories AS cc ON cc.id = a.catid"
  . "\n LEFT JOIN #__users AS u ON u.id = a.created_by"
  . "\n LEFT JOIN #__content_rating AS v ON a.id = v.content_id"
  . "\n LEFT JOIN #__sections AS s ON a.sectionid = s.id"
  . "\n LEFT JOIN #__groups AS g ON a.access = g.id"
  . "\n WHERE s.access<=$gid AND a.access<=$gid AND cc.access<=$gid "
  . "\n AND a.state =-1 and sectionid=$id";
  $database->setQuery( $query );
  $contes=$database->loadObjectList();

  if (count($contes)!=0)
  {
    foreach ($contes as $cont)
    { 
        $description = '';
        if ($show_cat_desc) $description = $cont->introtext;
        $element->description = $description;
      
        $linkcont ="index.php?option=com_content&amp;task=view&amp;id=".$cont->id."&amp;Itemid=".$Itemid;
        $element->name = $cont->title;
        $element->link = $linkcont;
        $element->level= $lev;
        $element->priority = $priority;
        $element->changefreq= $changefreq;
        if ($cont->modified> $cont->created) $element->lastmod = $cont->modified; else $element->lastmod = $cont->created;
        if ($show_item_icons) $element->image = 'content_article.gif'; else $element->image='none';
        $element->icon_path='/mambots/com_sefservicemap/';
        AddExtMenuItem ($element);
    } 
  }
}

function Content_archivecategory ($id, $level,$Itemid,$params)
{
  $priority=$params->get( 'priority', '' ) ;
  $changefreq=$params->get( 'changefreq', '' );
  global $database, $mainframe, $mosConfig_offset,$gid;
  $show_cat_desc=intval($params->get( 'show_content_desc', '0' )) ;
  $show_item_icons=intval($params->get( 'show_item_icons', '1' )) ;
  $lev=$level+1;
  $query = "SELECT a.*"
  . "\n FROM #__content AS a"
  . "\n INNER JOIN #__categories AS cc ON cc.id = a.catid"
  . "\n LEFT JOIN #__users AS u ON u.id = a.created_by"
  . "\n LEFT JOIN #__content_rating AS v ON a.id = v.content_id"
  . "\n LEFT JOIN #__sections AS s ON a.sectionid = s.id"
  . "\n LEFT JOIN #__groups AS g ON a.access = g.id"
  . "\n WHERE s.access<=$gid AND a.access<=$gid AND cc.access<=$gid "
  . "\n AND a.state =-1 and catid=$id";
  $database->setQuery( $query );
  $contes=$database->loadObjectList();

  if (count($contes)!=0)
  {
    foreach ($contes as $cont)
    { 
        $description = '';
        if ($show_cat_desc) $description = $cont->introtext;
        $element->description = $description;
      
        $linkcont ="index.php?option=com_content&amp;task=view&amp;id=".$cont->id."&amp;Itemid=".$Itemid;
        $element->name = $cont->title;
        $element->link = $linkcont;
        $element->level= $lev;
        $element->priority = $priority;
        $element->changefreq= $changefreq;
        if ($cont->modified> $cont->created) $element->lastmod = $cont->modified; else $element->lastmod = $cont->created;
        if ($show_item_icons) $element->image = 'content_article.gif'; else $element->image='none';
        $element->icon_path='/mambots/com_sefservicemap/';
        AddExtMenuItem ($element);
    } 
  }

}


function com_content_bot ($level,$link,$Itemid,$params)
{
  $task = smGetParam($link,'task','');
  $sectionid = smGetParam($link,'sectionid','');
  $id = smGetParam($link,'id','');
  switch ($task)
  {    
    case 'view':; break;
    case 'category': Content_categories($id,$level,$Itemid,$params); break;
    case 'section': Content_sections($id,$level,$Itemid,$params);break;
    case 'blogsection': Content_blogsections($id,$level,$Itemid,$params); break;
    case 'blogcategorymulti':Content_blogcategory ($id, $level,$Itemid,$params); break;
    case 'blogcategory':Content_blogcategory ($id, $level,$Itemid,$params); break;
    case 'archivesection':Content_archivesection ($id, $level,$Itemid,$params); break;
    case 'archivecategory':Content_archivecategory ($id, $level,$Itemid,$params); break;
  }
}
 
?>