<?
//*******************************************************
//* SEF Service Map Component Integrator for Hot Property
//* http://www.sherred.com
//* (C) Paolo Ciarrocca
//* license http://www.gnu.org/copyleft/gpl.html GNU/GPL
//*******************************************************


/** ensure this file is being included by a parent file */
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

function Hotproperty_apartments ($id, $level,$Itemid,$params)
{
  $show_apartment_desc=intval($params->get( 'show_apartment_desc', '1' )) ;
  $show_apartment_icons=intval($params->get( 'show_apartment_icons', '1' )) ;
  global $database,$gid,$mosConfig_offset;
  global $my;
  $lev=$level;
  $lev++;

  $now = date( 'Y-m-d H:i:s', time() + $mosConfig_offset * 60 * 60 );
  $xwhere = "\n AND published='1'"
		. "\n AND ( publish_up = '0000-00-00 00:00:00' OR publish_up <= '$now' )"
		. "\n AND ( publish_down = '0000-00-00 00:00:00' OR publish_down >= '$now' )"
		;

  $database->setQuery( "SELECT id,name,intro_text,type"
	 	. "\nFROM #__hp_properties"
		. "\n where type=$id".$xwhere
                . " and approved='1' order by id"
		);
  $contes = $database->loadObjectList();
  if (count($contes)!=0)
  { 
    foreach ($contes as $cont)
    { 
      $description = '';
      if ($show_apartment_desc) $description = $cont->intro_text;
      $element->description = $description;
      
      $linkcont ="index.php?option=com_hotproperty&Itemid=".$Itemid."&task=view&id=".$cont->id;
      $element->name = $cont->name;
      $element->link = $linkcont;
      $element->level = $lev;
      if ($show_apartment_icons) $element->image = 'hp_article.gif'; else $element->image='none';
      $element->icon_path='/mambots/com_sefservicemap/';
      if (function_exists ('AddExtMenuItem')) AddExtMenuItem ($element);else AddMenuItem($cont->name,$linkcont, $lev);     
    } 
  }
}


function Hotproperty_types($id, $level,$Itemid,$params)
{
  $show_type_icons=intval($params->get( 'show_type_icons', '1' )) ;
  $show_type_desc=intval($params->get( 'show_type_desc', '1' )) ;
  $show_mode=intval($params->get( 'show_mode', '1' )) ;
  global $database;
  global $my;
  $lev=$level;
  $lev++;
  $database->setQuery( "SELECT *"
	 	. "\nFROM #__hp_prop_types"
		. "\n where published='1' order by ordering"
		);
  
  $types=$database->loadObjectList();
  
  if (count($types)!=0)
  {
    foreach ($types as $type)
    { 
      $description='';
      if ($show_type_desc) $description = $type->desc;

      $database->setQuery("select count(id) FROM #__hp_properties"
		. "\n where type=$type->id and approved='1'");
      $count=$database->LoadResult();
	  
      if ($count!=0)
      {
	  	// /index.php?option=com_hotproperty&Itemid=47&task=viewtype&id=7
        $linkcont ="index.php?option=com_hotproperty&Itemid=".$Itemid."&task=viewtype&id=".$type->id;
        $element->name = $type->name;
        $element->link = $linkcont;
        $element->level = $lev;
        if ($show_type_icons) $element->image = 'hp_folder.gif'; else $element->image='none';
        $element->icon_path='/mambots/com_sefservicemap/';
        $element->description=$description;
        if (function_exists ('AddExtMenuItem')) AddExtMenuItem ($element);else AddMenuItem($type->name,$linkcont, $lev); 
        if ($show_mode) Hotproperty_apartments($type->id,$lev,$Itemid,$params);
      }
    }
  } 
}

function com_hotproperty_bot ($level,$link,$Itemid,$params)
{
  $task = smGetParam($link,'task','');
  //$sectionid = smGetParam($link,'sectionid','');
  $id = smGetParam($link,'id','');
  if(!$task) Hotproperty_types($id,$level,$Itemid,$params);
  switch ($task)
  {    
    case 'view': echo"task=view"; break;
    case 'viewtype': Hotproperty_apartments($id,$level,$Itemid,$params); break;
  }
}
 
?>
