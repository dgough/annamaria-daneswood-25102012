<?php
//*******************************************************
//* SEF Service Map Component Integrator
//* http://fun.kubera.org
//* (C) Radoslaw Kubera
//* license http://www.gnu.org/copyleft/gpl.html GNU/GPL
//*******************************************************


/** ensure this file is being included by a parent file */
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

function Contact_items ($level, $id,$Itemid,$params)
{
  $priority=$params->get( 'priority', '' ) ;
  $changefreq=$params->get( 'changefreq', '' );
  $show_item_icons=intval($params->get( 'show_item_icons', '1' )) ;
  global $database;
  $database->setQuery("SELECT * FROM #__contact_details where catid='$id' and published >='1'"
  ." order by ordering");
  $items=$database->loadObjectList();
  $lev=$level+1;
  if (count($items!=0))
  {
    foreach ($items as $item)
    {
      $link="index.php?option=com_contact&amp;task=view&amp;contact_id=".$item->id."&amp;Itemid=".$Itemid;
      $element->name = $item->name;
      $element->link = $link;
      $element->level = $lev;
      $element->priority = $priority;
      $element->changefreq= $changefreq;
      if ($show_item_icons) $element->image = 'contact_people.gif'; else $element->image='none';
      $element->icon_path='/mambots/com_sefservicemap/';
      AddExtMenuItem ($element);
    }
  }
}

function Contact_category ($level, $id,$Itemid,$params)
{
  $priority=$params->get( 'priority', '' ) ;
  $changefreq=$params->get( 'changefreq', '' );
  $show_cat_icons=intval($params->get( 'show_cat_icons', '1' )) ;
  $show_cat_desc=intval($params->get( 'show_cat_desc', '1' )) ;
  $show_mode=intval($params->get( 'show_mode', '1' )) ;
  global $database;
  $database->setQuery("SELECT * FROM #__categories where parent_id='$id' and section='com_contact_details' and published >='1'"
  ." order by ordering");
  $categories=$database->loadObjectList();
  $lev=$level+1;
  if (count($categories!=0))
  {
    foreach ($categories as $category)
    {
      $description = '';
      if ($show_cat_desc) $description = $category->name;
      $element->description = $description;

      $link="index.php?option=com_contact&amp;catid=".$category->id."&amp;Itemid=".$Itemid;
      $element->name = $category->title;
      $element->link = $link;
      $element->level = $lev;
      $element->priority = $priority;
      $element->changefreq= $changefreq;
      if ($show_cat_icons) $element->image = 'contact_peopledir.gif'; else $element->image='none';
      $element->icon_path='/mambots/com_sefservicemap/';
      AddExtMenuItem($element);
      if ($show_mode) Contact_items ($lev,$category->id,$Itemid,$params);
      Contact_category($lev, $category->id,$Itemid,$params);
    }
  }

}


function com_contact_bot ($level,$link,$Itemid,$params)
{
  $catid = smGetParam($link,'catid',0);
  $task = smGetParam($link,'task','');
  if (!$task) Contact_category($level,$catid,$Itemid,$params);
  Contact_items ($level,$catid,$Itemid,$params);

}
?>