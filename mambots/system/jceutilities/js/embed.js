function writeFlash(p) {
	writeEmbed(
		'D27CDB6E-AE6D-11cf-96B8-444553540000',
		'http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0',
		'application/x-shockwave-flash',
		p
	);
}
function writeShockWave(p) {
	writeEmbed(
	'166B1BCA-3F9C-11CF-8075-444553540000',
	'http://download.macromedia.com/pub/shockwave/cabs/director/sw.cab#version=8,5,1,0',
	'application/x-director',
		p
	);
}
function writeQuickTime(p) {
	writeEmbed(
		'02BF25D5-8C17-4B23-BC80-D3488ABDDC6B',
		'http://www.apple.com/qtactivex/qtplugin.cab#version=6,0,2,0',
		'video/quicktime',
		p
	);
}
function writeRealMedia(p) {
	writeEmbed(
		'CFCDAA03-8BE4-11cf-B84B-0020AFBBCCFA',
		'http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0',
		'audio/x-pn-realaudio-plugin',
		p
	);
}
function writeWindowsMedia(p) {
	p.url = p.src;
	writeEmbed(
		'6BF52A52-394A-11D3-B153-00C04F79FAA6',
		'http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=5,1,52,701',
		'video/x-ms-wmv',
		p
	);
}
function writeEmbed(cls, cb, mt, p) {
	var h = '', n;
		
	var msie = navigator.userAgent.toLowerCase().indexOf('msie') != -1;
	if(jceEmbedStrict === 1){
		h += '<object codebase="' + cb + '"';
		if(msie){
			h += 'classid="clsid:' + cls + '"';
			h += typeof(p.id) != "undefined" ? 'id="' + p.id + '"' : '';
			h += typeof(p.name) != "undefined" ? 'name="' + p.name + '"' : '';
			h += typeof(p.width) != "undefined" ? 'width="' + p.width + '"' : '';
			h += typeof(p.height) != "undefined" ? 'height="' + p.height + '"' : '';
			h += typeof(p.align) != "undefined" ? 'align="' + p.align + '"' : '';
		}
		h += '>';
		if(msie){
			for (n in p){
				h += '<param name="' + n + '" value="' + p[n] + '">';
			}
		}
		if(!msie){
			h += '<object type="'+ mt +'" data="'+ p.src +'"';
			for (n in p){
				h += n + '="' + p[n] + '"';
			}
			h += '></object>';
		}
	}else{
		h += '<object codebase="' + cb + '" classid="clsid:' + cls + '"';
			h += typeof(p.id) != "undefined" ? 'id="' + p.id + '"' : '';
			h += typeof(p.name) != "undefined" ? 'name="' + p.name + '"' : '';
			h += typeof(p.width) != "undefined" ? 'width="' + p.width + '"' : '';
			h += typeof(p.height) != "undefined" ? 'height="' + p.height + '"' : '';
			h += typeof(p.align) != "undefined" ? 'align="' + p.align + '"' : '';
		h += '>';
		for (n in p){
			h += '<param name="' + n + '" value="' + p[n] + '">';
		}
		h += '<embed type='+ mt +' ';
		for (n in p){
			h += n + '="' + p[n] + '"';
		}
		h += '></embed>';
	}
	h += '</object>';
	document.write(h);
}
