<?php
/**
 * Whitelabel bot for Joomla!
 * 
 */

// Security check to ensure this file is being included by a parent file.
if (!defined('_VALID_MOS')) die('Direct Access to this location is not allowed.');

$_MAMBOTS->registerFunction('onStart', 'initWhitelabelHP_types');

function initWhitelabelHP_types()
{

	global $whitelabel;
	
	if($whitelabel){
		if($unique_id = $whitelabel->params->get('unique_id')){
			$_REQUEST['whitelabel'] = $_GET['whitelabel'] = $_POST['whitelabel'] = $unique_id;		
		}
	}
}
?>