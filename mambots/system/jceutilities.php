<?php
/**
* @version $Id: jceutilities.php 2008-01-07 $
* @package JCEUtilites
* @copyright Copyright (C) 2006/2007/2008 Ryan Demmer. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
*/

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );

$_MAMBOTS->registerFunction( 'onAfterStart', 'botJCEUtilities' );

function botJCEUtilities() {
	global $mainframe, $database;
	
	$pop = intval( mosGetParam( $_REQUEST, 'pop', 0 ) );
	$task = mosGetParam( $_REQUEST, 'task' );
	
	if( $pop || ( $task == 'new' || $task == 'edit' ) ){
		return;
	}
		
	$query = "SELECT id, published"
    . "\n FROM #__mambots"
    . "\n WHERE element = 'jceutilities'"
    . "\n AND folder = 'system'"
    ;
    $database->setQuery( $query );
    $id = $database->loadResult();
    $mambot = new mosMambot( $database );
    $mambot->load( $id );
    $params = new mosParameters( $mambot->params );
	
	if( !$mambot->published ) return;
	
	$components = $params->get('components', '');
	if( $components ){
		$excluded 	= explode( ',', $components );
		$option 	= mosGetParam( $_REQUEST, 'option' );
		foreach( $excluded as $exclude ){
			if( $option == 'com_'. $exclude || $option == $exclude ){
				return;
			}
		}
	}
	
	$query = "SELECT published"
    . "\n FROM #__mambots"
    . "\n WHERE element = 'jceembed'"
    . "\n AND folder = 'system'"
    ;
    $database->setQuery( $query );
    $embed = $database->loadResult();
	
	//Popup
	$param['legacyPopup'] 		= $params->get( 'legacy', '1' ); 
	$param['boxConvert'] 		= $params->get( 'convert', '0' );
	$param['boxResize'] 		= $params->get( 'resize', '1' );
	$param['boxIcons'] 			= $params->get( 'icons', '1' );
	$param['boxOverlayOpacity'] = $params->get( 'overlay_opacity', '0.8' ); 
	$param['boxOverlayColor'] 	= "'". $params->get( 'overlay_color', '#000000' ) ."'"; 
	$param['boxFadeSpeed'] 		= $params->get( 'fadespeed', '500' );
	$param['boxScaleSpeed'] 	= $params->get( 'scalespeed', '500' );
	$param['imagePath'] 		= "'". $params->get( 'images_path', 'mambots/system/jceutilities/img/' ) ."'";
	//Tooltips
	$param['tipClass'] 			= "'". $params->get( 'tooltip_class', 'tooltip' ) ."'";
	$param['tipOpacity'] 		= $params->get( 'tooltip_opacity', '1' );
	$param['tipFxspeed'] 		= $params->get( 'tooltip_fxspeed', '150' );
	$param['tipPosition'] 		= "'". $params->get( 'tooltip_position', 'br' ) ."'";
	$param['tipOffsets'] 		= "{'x': ". $params->get( 'tooltip_offset_x', '16' ) .", 'y': ". $params->get( 'tooltip_offset_y', '16' ) ."}";
	
	$param['pngFix'] 			= $params->get( 'pngfix', '0' );
		
	$html = "<script type=\"text/javascript\" src=\"" . $mainframe->getCfg('live_site') . "/mambots/system/jceutilities/js/jquery-121.js\"></script>\n";
	$html .= "<script type=\"text/javascript\" src=\"" . $mainframe->getCfg('live_site') . "/mambots/system/jceutilities/js/jceutilities-150.js\"></script>\n";
	if( !$embed ){
		$html .= "<script type=\"text/javascript\" src=\"" . $mainframe->getCfg('live_site') . "/mambots/system/jceutilities/js/embed.js\"></script>\n";
	}
	$html .= "<link href=\"" . $mainframe->getCfg('live_site') . "/mambots/system/jceutilities/css/jceutilities.css\" rel=\"stylesheet\" type=\"text/css\" media=\"all\"/>\n";
	$html .= "<script type=\"text/javascript\">\n";
	$html .= "var jceEmbedStrict=". $params->get('embed_strict', '0') .";\n";
	$html .= "jQuery(document).ready(function(){\n";
 	$html .= "	jceutilities({\n";
	$i = 0;
	foreach( $param as $k => $v ){
		if( $i < count( $param ) -1 ){
			$v .= ',';
		}
		$html .= "		". $k .": ". $v ."\n";
		$i++;
	}
	$html .= "	});\n";
	$html .= "});\n";
	$html .= "</script>";
	$mainframe->addCustomHeadTag( $html );	
	return true;
}
?>