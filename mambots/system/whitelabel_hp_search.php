<?php
/**
 * Whitelabel bot for Joomla!
 * 
 */

// Security check to ensure this file is being included by a parent file.
if (!defined('_VALID_MOS')) die('Direct Access to this location is not allowed.');

$_MAMBOTS->registerFunction('onStart', 'initWhitelabelHP_search');


function initWhitelabelHP_search()
{
	global $whitelabel;

	if(!$whitelabel or !$whitelabel->params){
		return;
	}

	if($unique_id = $whitelabel->params->get('unique_id')) $_REQUEST['whitelabel'] = $unique_id;

}

