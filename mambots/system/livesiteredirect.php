<?php
/**
* @version $Id $
* @package Joomla
* @copyright Copyright (C) 2006 Yonca Teknoloji. All rights reserved.
* @author Alaattin Kahramanlar
**/
 
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

$_MAMBOTS->registerFunction( 'onStart', 'liveSiteRedirect' );

function liveSiteRedirect ()
{
	global $mosConfig_live_site;
	
	//Get the host variable (the domain the user is browsing)
	$host = strtolower(mosGetParam($_SERVER,'HTTP_HOST',''));
	//Get the live site url and strip off http(s)://
	$live_site = preg_replace('/http[s|]?:\/\//i','',strtolower($mosConfig_live_site));
	//Get the requested page
	$uri = mosGetParam($_SERVER,'REQUEST_URI','/');
	
	//Check that we found a host domain
	if($host){			
		//Check if the host does not equal live site, ie, user is browsing on an alias domain
		if($host != $live_site){
			//Redirect to the live site url preserving the uri					
			mosRedirect($mosConfig_live_site.$uri);			
		}

	}
}
?>