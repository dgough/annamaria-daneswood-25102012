<?php
/**
 * Whitelabel bot for Joomla!
 * 
 */

// Security check to ensure this file is being included by a parent file.
if (!defined('_VALID_MOS')) die('Direct Access to this location is not allowed.');

$_MAMBOTS->registerFunction('onStart', 'initWhitelabel');

function initWhitelabel()
{
	global $whitelabel, $database, $mosConfig_absolute_path;
	
	//Include whitelable class
	if(file_exists($mosConfig_absolute_path.'/administrator/components/com_whitelabel/whitelabel.class.php')){
		require_once( $mosConfig_absolute_path.'/administrator/components/com_whitelabel/whitelabel.class.php' );		
		$whitelabel = new mosWhitelabel( $database );
		$whitelabel->init();
	}else{
		echo '<div style="text-align: center"><strong>Whitelabel component appears not to be installed</strong></div>';
	}
	
}
?>