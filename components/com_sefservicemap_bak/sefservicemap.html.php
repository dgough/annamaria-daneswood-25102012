<?php
//*******************************************************
//* SEF Service Map Component
//* http://fun.kubera.org
//* (C) Radoslaw Kubera
//* license http://www.gnu.org/copyleft/gpl.html GNU/GPL
//*******************************************************


/** ensure this file is being included by a parent file */
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

class HTML_sefservicemap
{
  var $database;
  var $ComponentParams='';

  function HTML_sefservicemap()
  {
    global $database,$mainframe;
    $this->database = $database;
    $this->ComponentParams = $mainframe->ParamsArray;
  }


  function RenderMapXMLHeader()
  {
    header('Content-type: application/xml');
    echo '<?xml version="1.0" encoding="UTF-8"?>'."\n";
    echo '<urlset xmlns="http://www.google.com/schemas/sitemap/0.84">'."\n";
  }

  function RenderMapHTMLHeader()
  {
    global $mosConfig_live_site, $mainframe;

    $id	= mosGetParam( $_REQUEST, 'Itemid',0);
    $query = "SELECT id,name from #__menu where id='$id'";
    $this->database->setQuery($query); 
    $this->database->loadObject($tmp);
    $name=$tmp->name;
    ?><h1 class="componentheading"><?php echo ampReplace($name)?></h1>
    <div class='sefServiceMap'>
    <?php

    $mainframe->addCustomHeadTag('<link href="'.$mosConfig_live_site.'/components/com_sefservicemap/css/template_css.css" rel="stylesheet" type="text/css" />');
  }

  function RenderMapXMLFooter()
  {
    echo '</urlset>';
  }

  function RenderMapHTMLFooter()
  {
    global $mosConfig_live_site;
    $lang = mosGetParam($_REQUEST,'lang','');
    if ($lang) $lang = '&amp;lang='.$lang;
    if (defined( '_JEXEC' ))
    {
      $xmllink = $mosConfig_live_site.'/index.php?option=com_sefservicemap&amp;task=xmlmap&apm;tmpl=component&amp;no_html=1'.$lang;
      $txtlink = $mosConfig_live_site.'/index.php?option=com_sefservicemap&amp;task=txtmap&apm;tmpl=component&amp;no_html=1'.$lang;

    }
    else
    {
      $xmllink = $mosConfig_live_site.'/index2.php?option=com_sefservicemap&amp;task=xmlmap&amp;no_html=1'.$lang;
      $txtlink = $mosConfig_live_site.'/index2.php?option=com_sefservicemap&amp;task=txtmap&amp;no_html=1'.$lang;
    }

    $complink = getlink(); if ($complink!='l') $complink = 'by '.$complink; else $complink='';
    ?>
    </div>
    <div style='text-align: center'>
        <div style='float: left; width: 50%'><a href="<?php echo $xmllink ?>" title="<?php echo _SITEMAP_XML ?>"><img src="<?php echo $mosConfig_live_site ?>/components/com_sefservicemap/images/xml.png" style="border: 0px" alt="<?php echo _SITEMAP_XML ?>" />&nbsp;<?php echo _SITEMAP_XML ?></a>
        </div>
        <div style='float: left; width: 50%'>
          <a href="<?php echo $txtlink ?>" title="<?php echo _SITEMAP_TXT ?>"><img src="<?php echo $mosConfig_live_site ?>/components/com_sefservicemap/images/txt.png" style="border: 0px" alt="<?php echo _SITEMAP_TXT ?>" />&nbsp;<?php echo _SITEMAP_TXT ?></a>
         </div>
    </div>
    <div style='clear: left'></div>
  <?php
  }

}

?>