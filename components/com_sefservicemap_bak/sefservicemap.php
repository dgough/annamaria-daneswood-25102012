<?php
//*******************************************************
//* SEF Service Map Component
//* http://fun.kubera.org
//* (C) Radoslaw Kubera
//* license http://www.gnu.org/copyleft/gpl.html GNU/GPL
//*******************************************************


/** ensure this file is being included by a parent file */
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

ini_set('max_execution_time', 0);

include $mosConfig_absolute_path.'/components/com_sefservicemap/version.php';
include $mosConfig_absolute_path."/components/com_sefservicemap/sefservicemap.html.php";
include_once ($mosConfig_absolute_path."/administrator/components/com_sefservicemap/sefservicemap.util.php");

if (file_exists("$mosConfig_absolute_path/administrator/components/com_sefservicemap/language/$mosConfig_lang.php")) 
{ 
  include_once ("$mosConfig_absolute_path/administrator/components/com_sefservicemap/language/$mosConfig_lang.php");
} 
else 
{
 include_once ("$mosConfig_absolute_path/administrator/components/com_sefservicemap/language/english.php");
}

$mainframe->ParamsArray =array();
$maptype='';
$sef_sm_cache_file='';

smGetComponentParams();

function cache_start($id,$integrator)
{   
  global $lang,$mosConfig_absolute_path,$sef_sm_cache_file,$mainframe;
  $sef_sm_cache_file='';
    
  if ($mainframe->ParamsArray->get($integrator,''))
  { 
    $cache_dir = $mosConfig_absolute_path.'/components/com_sefservicemap/cache/';
    @mkdir ($cache_dir.$id.'/');
    if ($lang) $file = $id.'_'.$lang; else $file=$id;
    if (file_exists($cache_dir.$id.'/'.$file))
    {
      $int_time = $integrator.'_time';
      $time = $mainframe->ParamsArray->get($int_time); 
      $filetime=filemtime($cache_dir.$id.'/'.$file);
      if (($filetime+$time)<mktime())
      { 
        unlink ($cache_dir.$id.'/'.$file);
        $sef_sm_cache_file=fopen($cache_dir.$id.'/'.$file,"w");
        return 1;
      }
      else
      {
        return 0;
      }
    } else
    {
      $sef_sm_cache_file=fopen($cache_dir.$id.'/'.$file,"w");
      return 1;
    }
  }
  return 0;
}

function cache_stop()
{ 
  global $sef_sm_cache_file;
  if ($sef_sm_cache_file) 
    fclose ($sef_sm_cache_file);
  $sef_sm_cache_file='';
}

function get_from_cache($id,$integrator)
{
  global $mosConfig_absolute_path,$lang;

  $cache_dir = $mosConfig_absolute_path.'/components/com_sefservicemap/cache/';
  if ($lang) $file = $id.'_'.$lang; else $file=$id;
  if (file_exists($cache_dir.$id.'/'.$file))
  {
    $handle = fopen ($cache_dir.$id.'/'.$file,'r');
    while (!feof($handle)) 
    {
      $line = fgets($handle);
      $element='';
      $data = explode ('<sefsm>',$line);
      if (isset($data[0]) && $data[0]=='<sefrec>')
      {
        if (isset($data[9]))
        {
          $element='';
          $element->name = $data[1];
          $element->link = $data[2];
          $element->image = $data[3];
          $element->level = $data[4];
          $element->icon_path = $data[5];
          $element->lastmod = $data[6];
          $element->changefreq = $data[7];
          $element->priority = $data[8];
          $element->description = $data[9];
          AddExtMenuItem($element);
        }
      }
    }
    fclose($handle);
    return 1;
  }
  else return 0;
}

function HTML_To_Plaintext($html_text)
{
  $search = array ('@<script[^>]*?>.*?</script>@si', // Strip out javascript
                 '@<[\/\!]*?[^<>]*?>@si',          // Strip out HTML tags
                 '@([\r\n])[\s]+@',                // Strip out white space
                 '@&(quot|#34);@i',                // Replace HTML entities
                 '@&(amp|#38);@i',
                 '@&(lt|#60);@i',
                 '@&(gt|#62);@i',
                 '@&(nbsp|#160);@i',
                 '@&(iexcl|#161);@i',
                 '@&(cent|#162);@i',
                 '@&(pound|#163);@i',
                 '@&(copy|#169);@i',
                 '@&#(\d+);@e');                    // evaluate as php

  $replace = array ('',
                 '',
                 '\1',
                 '"',
                 '&',
                 '<',
                 '>',
                 ' ',
                 chr(161),
                 chr(162),
                 chr(163),
                 chr(169),
                 'chr(\1)');

  $text = preg_replace($search, $replace, $html_text);
  $text = preg_replace( '/{.*?\}/', ' ', $text );

  return $text;
}

function AddExtMenuItem ($element)
{ 
  global $maptype,$mainframe,$sef_sm_cache_file,$mosConfig_absolute_path,$no_html;


  if (isset ($element->icon_path)) $icon_path = $element->icon_path; else $icon_path='';  
  if (eregi('mambots',$icon_path))
  { 
    $tmp = $icon_path;
    if (!is_dir($mosConfig_absolute_path.$icon_path))
      $icon_path = str_replace('mambots','plugins',$icon_path);
    if (!is_dir($mosConfig_absolute_path.$icon_path)) 
      $icon_path=$tmp;
  }
  $element->icon_path = $icon_path;

  if ($maptype!='html') $no_html=1;
  switch ($maptype)
  {
    case 'xml':    show_xml_element ($element); break;
    case 'txt':    show_txt_element ($element); break;
    case 'html':   show_element ($element); break;
  }

  $sublevels=$mainframe->ParamsArray->get( 'sublevels', '0' );  
  $max_desc_length = $mainframe->ParamsArray->get( 'max_desc_length', '0' );
  if ($element->level<$sublevels || $sublevels==0)
  {  
    if (isset ($element->name)) $name = $element->name; else $name='';
    if (isset ($element->link)) $link = $element->link; else $link='';
    if (isset ($element->image)) $image = $element->image; else $image='';
    if (isset ($element->level)) $level = $element->level; else $level=0;
    if (isset ($element->lastmod)) $lastmod = $element->lastmod; else $lastmod='';
    if (isset ($element->changefreq)) $changefreq = $element->changefreq; else $changefreq='';
    if (isset ($element->priority)) $priority = $element->priority; else $priority='';
 
    if (isset($element->description) && $element->description!='') 
    { 
      $short_desc=HTML_To_Plaintext ($element->description);
      if ($max_desc_length!=0) 
      {
        $sub_desc = substr ($short_desc,0,$max_desc_length); 
        if (strlen($sub_desc)==strlen($short_desc)) $short_desc=$sub_desc; else $short_desc=$sub_desc.'...';
      }
    } else $short_desc='';
    $description = $short_desc;

    if ($sef_sm_cache_file)
    { 
      $str = trim ("<sefrec><sefsm>".$name."<sefsm>".$link."<sefsm>".$image."<sefsm>".$level."<sefsm>".$icon_path."<sefsm>".$lastmod."<sefsm>".$changefreq."<sefsm>".$priority."<sefsm>".$description."<sefsm>")."\n";
      fwrite ($sef_sm_cache_file,$str);
    }
  }
}

function show_element ($element)
{
  global $mosConfig_live_site,$mainframe;
  $point = '<img hspace="5" src="'.$mosConfig_live_site.'/components/com_sefservicemap/images/point.gif" border="0" />'; 
  $icons = $mainframe->ParamsArray->get( 'show_icons', '1' ) ;
  $max_length=intval($mainframe->ParamsArray->get( 'max_length', '0' )); 
  $max_desc_length=intval($mainframe->ParamsArray->get( 'max_desc_length', '50' )); 
  $sublevels=$mainframe->ParamsArray->get( 'sublevels', '0' ) ;  
  $level0=$mainframe->ParamsArray->get( 'level0','level0');
  $level1=$mainframe->ParamsArray->get( 'level1','level1');
  $level2=$mainframe->ParamsArray->get( 'level2','level2');
  $level3=$mainframe->ParamsArray->get( 'level3','level3');
  $level4=$mainframe->ParamsArray->get( 'level4','level4');
  $level5=$mainframe->ParamsArray->get( 'level5','level5');
  $level6=$mainframe->ParamsArray->get( 'level6','level6');
  $level7=$mainframe->ParamsArray->get( 'level7','level7');
  $level8=$mainframe->ParamsArray->get( 'level8','level8');
  $level9=$mainframe->ParamsArray->get( 'level9','level9');
  $levelx=$mainframe->ParamsArray->get( 'levelx','levelx');
  $desc = $mainframe->ParamsArray->get( 'desc','description');
  $show_desc = intval($mainframe->ParamsArray->get( 'show_desc','1'));
  $icon_path=$mainframe->ParamsArray->get( 'icon_path','');

  $description='';
  if ($show_desc)
  {  
    if (isset($element->description) && $element->description!='') 
    { 
      $short_desc=HTML_To_Plaintext ($element->description);
      if ($max_desc_length!=0) 
      {
        $sub_desc = substr ($short_desc,0,$max_desc_length); 
        if (strlen($sub_desc)==strlen($short_desc)) $short_desc=$sub_desc; else $short_desc=$sub_desc.'...';
      }
      $description='<div class="'.$desc.'">'.$short_desc.'</div>'; 
    }
  }

  $name = $element->name;
  if ($max_length!=0) 
  {
    $subname = substr ($name,0,$max_length); 
    if (strlen($subname)==strlen($name)) $name=$subname; else $name=$subname.'...';
  }
  if ($icons)
  {
    if (!$icon_path) $icon_path = $element->icon_path;
    if ($element->image=='none' ) $image='';
    else
    {
      if (!$element->image) $image='<img style="border: 0px; margin: 0px 5px" src=$mosConfig_live_site./components/com_sefservicemap/images/directory.gif" alt="" />';
      else $image='<img style="border: 0px; margin: 0px 5px" src="'.$mosConfig_live_site.$icon_path.'/'.$element->image.'" alt="" />';
    }
  } else 
  {
    if ($name!='' && $name!="&nbsp;") $image=$point;
    else $image='';
  }

  switch ($element->level)
  {
    case 0: $class = 'class="'.$level0.'"';break;
    case 1: $class = 'class="'.$level1.'"';break;
    case 2: $class = 'class="'.$level2.'"';break;
    case 3: $class = 'class="'.$level3.'"';break;
    case 4: $class = 'class="'.$level4.'"';break;
    case 5: $class = 'class="'.$level5.'"';break;
    case 6: $class = 'class="'.$level6.'"';break;
    case 7: $class = 'class="'.$level7.'"';break;
    case 8: $class = 'class="'.$level8.'"';break;
    case 9: $class = 'class="'.$level9.'"';break;
    default: $class = 'class="'.$levelx.'"';break;       
  }

  if ($element->link!='') 
  {  
    echo '<div '.$class.'><a href="'.sefRelToAbs(ampReplace($element->link)).'">'.ampReplace($image.$name).'</a>'.$description."</div>\n";
  }
  else  
  {
    echo '<div '.$class.'>'.ampReplace($image.$name.$description)."</div>\n";
  }
}

function show_xml_element($element)
{
  if (eregi('Itemid=',$element->link) && !eregi('index.php',$element->link)) return;
  if (!isset($element->link) || $element->link=='') return;
  global $mosConfig_live_site;
  $live_site=$mosConfig_live_site;
  $last = substr ($mosConfig_live_site,strlen($mosConfig_live_site)-1,1);
  if ($last=='/') $live_site= substr($mosConfig_live_site,0,strlen($mosConfig_live_site)-1);
  $link = $element->link;


  if (isset($element->lastmod) && $element->lastmod!='') $lastmod = $element->lastmod; else $lastmod='';

  if ($lastmod)
  {
   
    $time = strtotime($lastmod);
    $lastmod= date('Y-m-d',$time);
  }

  if (isset($element->changefreq) && $element->changefreq!='') $changefreq = $element->changefreq; else $changefreq='';
  if (isset($element->priority) && $element->priority!='') $priority = $element->priority; else $priority='';
  $link = sefRelToAbs($link);
  $index = substr($link,0,4);
  $first = substr($link,0,1);
  $sep='/';
  if ($first=='/') $sep = '';
  if ($index!='http') 
  {  
    $link = $live_site.$sep.$link;
  }
  else 
  {
    if (substr ($link,0,strlen($live_site)) !=$live_site) $link='';
  }

  $link= utf8_encode($link);
  $link = str_replace('&amp;','&',$link);
  $link = str_replace('&apos;',"'",$link);
  $link = str_replace('&quot;','"',$link);
  $link = str_replace('&gt;','>',$link);
  $link = str_replace('&lt;','<',$link);
  $link = str_replace('&','&amp;',$link);
  $link = str_replace("'",'&apos;',$link);
  $link = str_replace('"','&quot;',$link);
  $link = str_replace('>','&gt;',$link);
  $link = str_replace('<','&lt;',$link);
  echo '<url>'."\n";
  echo '  <loc>'.$link.'</loc>'."\n";
  if ($priority) echo '  <priority>'.$priority.'</priority>'."\n";
  if ($changefreq) echo '  <changefreq>'.$changefreq.'</changefreq>'."\n";
  if ($lastmod) echo '  <lastmod>'.$lastmod.'</lastmod>'."\n";
  echo '</url>'."\n";
}

function show_txt_element($element)
{
  if (eregi('Itemid=',$element->link) && !eregi('index.php',$element->link)) return;
  if (!isset($element->link) || $element->link=='') return;
  global $mosConfig_live_site;
  $live_site=$mosConfig_live_site;
  $last = substr ($mosConfig_live_site,strlen($mosConfig_live_site)-1,1);
  if ($last=='/') $live_site= substr($mosConfig_live_site,0,strlen($mosConfig_live_site)-1);
  $link = $element->link;
  $link = sefRelToAbs($link);
  $index = substr($link,0,4);
  $first = substr($link,0,1);
  $sep='/';
  if ($first=='/') $sep = '';
  if ($index!='http') 
  {  
    $link = $live_site.$sep.$link;
  }
  else 
  {
    if (substr ($link,0,strlen($live_site)) !=$live_site) $link='';
  }
  $link= utf8_encode($link);
  $link = str_replace('&amp;','&',$link);
  $link = str_replace('&apos;',"'",$link);
  $link = str_replace('&quot;','"',$link);
  $link = str_replace('&gt;','>',$link);
  $link = str_replace('&lt;','<',$link);
  $link = str_replace('&','&amp;',$link);
  $link = str_replace("'",'&apos;',$link);
  $link = str_replace('"','&quot;',$link);
  $link = str_replace('>','&gt;',$link);
  $link = str_replace('<','&lt;',$link);
  echo $link."\n";
}

class sefservicemap
{
  var $database = '';
  function sefservicemap()
  {
    global $mosConfig_absolute_path,$mosConfig_lang,$database,$gid,$mosConfig_live_site,$maptype,$mainframe;
    $this->database = $database;

    $MyHTML_sefservicemap = new HTML_sefservicemap ();


    $cache_dir = $mosConfig_absolute_path.'/components/com_sefservicemap/cache/';
    @mkdir ($cache_dir);

    smSynchronize();
    cache_clear_Check();

    $task = mosGetParam($_REQUEST,'task','');

    $maptype = 'html';
    switch ($task)
    { 
      case 'googlemap':
      case 'xmlmap': { $maptype='xml'; } break;
      case 'txtmap': { $maptype='txt'; } break;
    }

    if ($maptype=='html')  $MyHTML_sefservicemap->RenderMapHTMLHeader();
      else if ($maptype=='xml')  $MyHTML_sefservicemap->RenderMapXMLHeader();

    $query = "select name as title,ordering AS menu_ordering ,published AS menu_published, params FROM #__sef_sm_menus WHERE published='1' order by ordering";
    $this->database->setQuery($query);

    $menus=$this->database->LoadObjectList();

    $menu_titles=$mainframe->ParamsArray->get( 'menu_titles', '1' ); 
    $show_menu_icons= intval($mainframe->ParamsArray->get('show_menu_icons','0'));
    $icon_path = $mainframe->ParamsArray->get('icon_path','');
    $show_module_icons = intval($mainframe->ParamsArray->get('show_module_icons','1'));
    if ($icon_path=='') $icon_path ='/components/com_sefservicemap/images';
    foreach ($menus as $menu)
    { 
      $level=0;
      $pars = new mosParameters($menu->params);
      $title = $menu->title;
      $menutype=$menu->title;
      $menu_images = intval($pars->get('menu_images','0'));
      if ($menu_titles) 
      {
        $query = "SELECT * "
		. "\n FROM #__modules"
		. "\n WHERE module = 'mod_mainmenu'"
		. "\n ORDER BY title"
		;
        $this->database->setQuery( $query	);
        $modMenus = $this->database->loadObjectList();
        foreach ( $modMenus as $modMenu ) 
        {
          mosMakeHtmlSafe( $modMenu) ;
          $modParams 	= mosParseParams( $modMenu->params );
          $menuType 	= @$modParams->menutype;
          if (!$menuType) 
          {
            $menuType = 'mainmenu';
          }
          if ($menuType==$menu->title) $id=$modMenu->id; 
        }

        $this->database->setQuery("select * from #__modules where id='$id'");
        $this->database->loadObject($module);

        $level=1;
        $element='';
        $element->link='';
        $element->name=$module->title;
        $element->level=0;
        if ($show_module_icons) $element->image='menu.gif'; else $element->image='none';
        $element->icon_path=$icon_path;
        $element->description = '';
        AddExtMenuItem($element);
      }
      $this->showMenuMap($title,$menutype,$menu_images,$show_menu_icons,$level,$icon_path);
      if ($menu_titles) 
      { 
        $element='';
        $element->name= '&nbsp;';
        $element->link='';
        $element->level=10;
        $element->image='none';
        $element->icon_path=$icon_path;
        $element->description = '';
        AddExtMenuItem($element);
      }
    }

    $nohtml = mosGetParam($_REQUEST,'no_html','');
    if ($maptype=='html')  $MyHTML_sefservicemap->RenderMapHTMLFooter();
      else if ($maptype=='xml') $MyHTML_sefservicemap->RenderMapXMLFooter();

     if ($maptype!='html' && !defined( '_JEXEC' ) && !$nohtml) exit;
  }
    
  function showMenuMap($title,$menutype,$menu_images,$show_menu_icons,$level,$icon_path)
  { 
    $this->showMenu($title,$menutype,0,$level,$menu_images,$show_menu_icons,$icon_path);
  }

  function showMenu($title,$menutype,$parent,$level,$menu_images,$show_menu_icons,$icon_path)
  {
    global $gid,$mosConfig_live_site,$_MAMBOTS, $mosConfig_dbprefix,$mosConfig_absolute_path,$mainframe;
  

    $plugtable=$mosConfig_dbprefix.'plugins';
    $alltables=$this->database->getTableList();
 
    if (!in_array($mosConfig_dbprefix.'plugins',$alltables)) $plugtable = $mosConfig_dbprefix.'mambots';

    $query = "SELECT count(id) from #__menu where menutype='$menutype' and parent='$parent' and published >'0' and access<='$gid'";
    $this->database->setQuery($query);
    $count = $this->database->loadResult();
  
    $query = "SELECT a.*,b.published AS menu_published,b.integrator AS menu_integrator,b.ordering AS menu_ordering, b.params AS menu_params"
    ."\n FROM #__menu AS a LEFT JOIN #__sef_sm_menu as b ON b.menu_id = a.id "
    ."\n WHERE a.menutype='$menutype' and a.published >'0' and a.parent='$parent' and b.published = '1' and a.access<='$gid' order by 'ordering'";
    $this->database->setQuery($query);

    $submenus = $this->database->loadObjectList();
    foreach ($submenus as $submenu)
    {
      $pars = new mosParameters($submenu->params);
      $image='directory.gif';
      if ($show_menu_icons==1)
      {
        $icon = $pars->get('menu_image','-1');
        if ($icon!='-1') 
        {
          $image=$icon;
          $icon_path = '/images/stories/';       
        }
     }

     if ($show_menu_icons==2)
     {
       $image='none';
     }

     $Itid=smGetParam($submenu->link,'Itemid','');
     if (!$Itid) 
     {  
       if ($submenu->type!='url')
       {
         if (defined( '_JEXEC' ))
         {  
            $option = smGetParam($submenu->link,'option','');
            $link = 'index.php?Itemid='.$submenu->id.'&option='.$option;
         }
         else 
         {
           $link=$submenu->link.'&amp;Itemid='.$submenu->id; 
         }
       } else $link=$submenu->link;

       $Itid=$submenu->id;
     } 
     else
     {
       $link = $submenu->link;
     }

     $element->name = $submenu->name;
     $element->link = $link;
     $element->level = $level;
     $element->image = $image;
     $element->icon_path=$icon_path;
     $element->description='';

     AddExtMenuItem($element);

     $component= smGetParam($submenu->link,'option','');
     if ($component=='content') $component='com_content';
     $id=$submenu->id;

     if ($submenu->menu_integrator!=0)
     { 
       $query = "SELECT count(element) from ".$plugtable." where folder='com_sefservicemap' and element='".$component."_bot' and published='1' and access<='$gid'";
       $this->database->setQuery($query);
       $integrator = $this->database->loadResult();
     } 
     else $integrator=0;

     if ($integrator && $submenu->menu_integrator!=0)
     {  
       if (file_exists($mosConfig_absolute_path."/mambots/com_sefservicemap/".$component."_bot.php"))
       {
         include_once $mosConfig_absolute_path."/mambots/com_sefservicemap/".$component."_bot.php";
       } 
       else
       if (file_exists($mosConfig_absolute_path."/plugins/com_sefservicemap/".$component."_bot.php"))
       {
         include_once $mosConfig_absolute_path."/plugins/com_sefservicemap/".$component."_bot.php";
       }

       $func=$component.'_bot'; 
       if ($submenu->menu_integrator==2)
       {
         $pars = new mosParameters( $submenu->menu_params);
       }
       else
       {
         $query = "SELECT params from ".$plugtable." where folder='com_sefservicemap' and element='".$component."_bot' and published='1' and access<='$gid'";      
         $this->database->setQuery($query);
         $parameters = $this->database->loadResult();
         $pars= new mosParameters ($parameters);
       }

       if (function_exists($func)) 
       { 
         if (intval($mainframe->ParamsArray->get($component.'_bot','0'))==1)
         { 
           if (cache_start($submenu->id,$component."_bot")==1)
           { 
             $func($level,$submenu->link,$Itid,$pars); 
             cache_stop();
           } 
           else get_from_cache($submenu->id,$component."_bot");
         } else $func($level,$submenu->link,$Itid,$pars); 
       }
     }

     $lev=$level+1;
     $this->showMenu($submenu->name,$menutype,$submenu->id,$lev,$menu_images,$show_menu_icons,$icon_path);
   }
 }

}


$mysefservicemap = new sefservicemap();
?>