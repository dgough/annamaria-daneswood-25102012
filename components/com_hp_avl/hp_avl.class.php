<?php
/**
* Hot Property Availability Extension - Class
*
* @package Hot Property 0.9
* @copyright (C) 2004 Lee Cher Yeong
* @url http://www.Mosets.com/
* @author Lee Cher Yeong <cy@mosets.com>
**/

// ensure this file is being included by a parent file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

class frontend_Calendar extends Calendar {

		function getCalendarLink($month, $year)
    {
        return "index2.php?option=com_hp_avl&task=ext_viewmonth&month=$month&year=$year&property_id=$this->property_id";
    }
    
    function getDateLink($month, $year)
    {
        return "";
    }

    function getMonthLink($month, $year)
    {
        return "index2.php?option=com_hp_avl&task=ext_viewmonth&month=$month&year=$year&property_id=$this->property_id";
    }

		function getYearLink($month, $year)
		{
				return "index2.php?option=com_hp_avl&task=ext_viewyear&year=$year&property_id=$this->property_id";
		}

}

class view_Calendar extends Calendar {

		function getCalendarLink($month, $year)
    {
//        return "index.php?option=com_hp_avl&task=ext_main&month=$month&year=$year&property_id=$this->property_id";
    }
    
    function getDateLink($month, $year)
    {
        return "";
    }

    function getMonthLink($month, $year)
    {
        return "";

    }

		function getYearLink($month, $year)
		{
				return "index.php?option=com_hp_avl&task=view&year=$year&id=$this->property_id";

		}

}

?>