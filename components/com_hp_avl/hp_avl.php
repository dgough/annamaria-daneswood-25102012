<?php
// hp_avl.php
/**
* Hot Property Availability Extension
*
* @package Hot Property 0.9
* @copyright (C) 2004 Lee Cher Yeong
* @url http://www.Mosets.com/
* @author Lee Cher Yeong <cy@mosets.com>
**/

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

# Backend classes
include_once ( $mosConfig_absolute_path.'/administrator/components/com_hp_avl/admin.hp_avl.class.php' );

# Front-end classes
include_once( $mosConfig_absolute_path.'/components/com_hp_avl/hp_avl.class.php' );
# Hot Property's Frontend Class - HP_Agent
include_once( $mosConfig_absolute_path.'/components/com_hotproperty/hotproperty.class.php' );

require( $mosConfig_absolute_path.'/administrator/components/com_hotproperty/config.hotproperty.php' );
require_once( $mainframe->getPath( 'front_html' ) );
include_once( $mosConfig_absolute_path.'/components/com_hotproperty/hotproperty.html.php' );

# Include the language file. Default is English
if ($hp_language=='') {
	$hp_language='english';
}
include_once('components/com_hp_avl/language/'.$hp_language.'.php');
include_once('components/com_hotproperty/language/'.$hp_language.'.php');

$id = intval( mosGetParam( $_REQUEST, 'id', 0 ) );
$task = mosGetParam( $_REQUEST, 'task', '' );

switch( $task ) {
	# Extension Event - Editing
	case "ext_main":
		ext_viewyear( $option );
		break;
	case "ext_viewmonth":
		ext_main( $option );
		break;
	case "ext_viewyear":
		ext_viewyear( $option );
		break;
	case "ext_save":
		ext_save( $option );
		break;

	# Front end view
	case 'view':
		clear_prev_month();
		viewAvl( $id, $option );
		break;

	# Display Mambo default error msg when $task provided does not exists
	default:
		echo _NOT_EXIST;
		break;
}

function viewAvl( $property_id, $option ) {
	global $database;

	$today = getdate();
	$cal = new view_Calendar();
	$cal->edit = 0;
	$cal->property_id = $property_id;

	$year = intval( mosGetParam( $_REQUEST, 'year', $today['year'] ) );

	/*
	# Get Availability Type
	$database->setQuery('SELECT * FROM #__hp_avl_types');
	$avl_types = $database->loadObjectList('avl_type_id');
	$cal->avl_types = $avl_types;

	# Retrieve assigned Availability
	
	$database->setQuery("SELECT avl_type_id, date FROM #__hp_avl WHERE property_id = '".$property_id."' AND YEAR(`date`) = '".$year."'");
	$cal->avl = $database->loadObjectList('date');
	*/
	
	$avl_types = $cal->setUnavailable($property_id,$year);

	# Get property name & type
	$database->setQuery("SELECT a.user AS user, p.id AS id, p.name AS name, t.id AS type_id, t.name AS type_name "
		.	"\nFROM #__hp_properties AS p, #__hp_prop_types AS t, #__hp_agents AS a "
		.	"\nWHERE p.type = t.id AND p.agent = a.id AND p.id = '".$property_id."'"
		.	"\nLIMIT 1");
	$database->loadObject($property);

	$html = $cal->getYearHTML( $year );

	hp_avl_HTML::viewAvl( $html, $avl_types, $property );
}

function ext_main( $option ) {
	global $database, $my, $Itemid;

	$today = getdate();
	$year = intval( mosGetParam( $_REQUEST, 'year', 0 ) );
	$month = intval( mosGetParam( $_REQUEST, 'month', 0 ) );
	$property_id = intval( mosGetParam( $_REQUEST, 'property_id', 0 ) );
	
	$agent = new HP_Agent($database);
	$agent->load($my->id);

	# Redirect to HP main page if user is not a valid Agent
	if ( !($agent->user == $my->id && $agent->user > 0 && !empty($agent->id) && $my->id > 0) ) {
		mosRedirect( "index.php?option=com_hotproperty&Itemid=".$Itemid );
	}

	# Property exists?
	$database->setQuery("SELECT agent FROM #__hp_properties WHERE id = '".$property_id."'");
	$tmp_agent = $database->loadResult();
	if($tmp_agent == '') {
		echo _NOT_EXIST;
		exit();
	# Correct owner?
	} elseif($tmp_agent <> $agent->id) {
		echo _NOT_EXIST;
		exit();
	}

	if ($property_id == 0) {
		hp_avl_HTML::ext_error( "<center><br /><h2>Unavailable</h2>Please save the property first before setting up availability's option</center>" );		
		exit();
	}

	if ($year == 0 && $month == 0) {
		$year = intval( mosGetParam( $_POST, 'year', $today['year'] ) );
		$month = intval( mosGetParam( $_POST, 'month', $today['mon'] ) );
	}

	if ($property_id == 0)	 {
		$property_id = intval( mosGetParam( $_POST, 'property_id', 0 ) );
	}

	$cal = new frontend_Calendar();
	$cal->property_id = $property_id;

	/*
	# Get Availability Type
	$database->setQuery('SELECT * FROM #__hp_avl_types');
	$avl_types = $database->loadObjectList('avl_type_id');
	$cal->avl_types = $avl_types;

	# Retrieve assigned Availability
	$database->setQuery("SELECT avl_type_id, date FROM #__hp_avl WHERE property_id = '".$property_id."' AND MONTH(`date`) = '".$month."' AND YEAR(`date`) = '".$year."'");
	$cal->avl = $database->loadObjectList('date');
	*/
	
	# Retrieve assigned Availability
	$avl_types = $cal->setUnavailable($property_id,$year);

	$html['calendar'] = $cal->getMonthHTML( $month, $year );

	hp_avl_HTML::ext_main( $html, $avl_types, $month, $year, $property_id, $option );
}

function ext_save( $option ) {
	global $database, $my, $Itemid;

	$year = intval( mosGetParam( $_REQUEST, 'year', $today['year'] ) );
	$month = intval( mosGetParam( $_REQUEST, 'month', $today['mon'] ) );
	$property_id = intval( mosGetParam( $_POST, 'property_id', 0 ) );
	
	$agent = new HP_Agent($database);
	$agent->load($my->id);

	# Redirect to HP main page if user is not a valid Agent
	if ( !($agent->user == $my->id && $agent->user > 0 && !empty($agent->id) && $my->id > 0) ) {
		mosRedirect( "index.php?option=com_hotproperty&Itemid=".$Itemid );
	}

	# Property exists?
	$database->setQuery("SELECT agent FROM #__hp_properties WHERE id = '".$property_id."'");
	$tmp_agent = $database->loadResult();
	if($tmp_agent == '') {
		echo _NOT_EXIST;
		exit();
	# Correct owner?
	} elseif($tmp_agent <> $agent->id) {
		echo _NOT_EXIST;
		exit();
	}

	$row = new hp_avl($database);

	$row->clear_avl($property_id, $month, $year);

	foreach($_POST AS $k => $v) {
		if (substr($k,0,3) == "day" && $v > 0) {
			$date = date('Y-m-d', strtotime(substr($k,3,2).' '.date('F',mktime(0, 0, 0, $month, 1, 2004)).' '.$year));
			$row->add_avl($property_id, $date, $v);
		}
	}
	mosRedirect( "index2.php?option=$option&task=ext_viewmonth&month=$month&year=$year&property_id=$property_id", "Calendar has been saved"  );
  
}

function ext_viewyear( $option ) {
	global $database, $my, $Itemid;

	$today = getdate();

	$year = intval( mosGetParam( $_REQUEST, 'year', $today['year'] ) );
	$property_id = intval( mosGetParam( $_REQUEST, 'property_id', 0 ) );

	if ($property_id == 0) {
		hp_avl_HTML::ext_error( _HPAVL_UNAVAILABLE );		
		//exit();
	} else {

		$agent = new HP_Agent($database);
		$agent->load($my->id);

		# Redirect to HP main page if user is not a valid Agent
		if ( !($agent->user == $my->id && $agent->user > 0 && !empty($agent->id) && $my->id > 0) ) {
			mosRedirect( "index.php?option=com_hotproperty&Itemid=".$Itemid );
		}

		# Property exists?
		$database->setQuery("SELECT agent FROM #__hp_properties WHERE id = '".$property_id."'");
		$tmp_agent = $database->loadResult();
		if($tmp_agent == '') {
			echo _NOT_EXIST;
			exit();
		# Correct owner?
		} elseif($tmp_agent <> $agent->id) {
			echo _NOT_EXIST;
			exit();
		}

		$cal = new frontend_Calendar();
		$cal->edit = 0;
		$cal->property_id = $property_id;

		# Get Availability Type
		/*
		$database->setQuery('SELECT * FROM #__hp_avl_types');
		$avl_types = $database->loadObjectList('avl_type_id');
		$cal->avl_types = $avl_types;

		# Retrieve assigned Availability
		$database->setQuery("SELECT `avl_type_id`, `date` FROM #__hp_avl WHERE property_id = '".$property_id."' AND YEAR(`date`) = '".$year."'");
		$cal->avl = $database->loadObjectList('date');
		*/
		
		# Retrieve assigned Availability
		$avl_types = $cal->setUnavailable($property_id,$year);

		$html = $cal->getYearHTML( $year );

		hp_avl_HTML::ext_viewyear( $html, $option, $avl_types );
	}
}

function clear_prev_month() {
	global $database;

	$database->setQuery("SELECT value FROM #__hp_avl_cfg WHERE name = 'delete_prev_month'");
	$delete_prev_month = $database->loadResult();

	if ($delete_prev_month == '1') {
		$database->setQuery("DELETE LOW_PRIORITY FROM #__hp_avl WHERE `date` < '".date('Y-m-01')."'");
		//$database->setQuery("SELECT `date` FROM #__hp_avl WHERE `date` < '".date('Y-m-01')."'");
		$database->query();
	}

}

?>