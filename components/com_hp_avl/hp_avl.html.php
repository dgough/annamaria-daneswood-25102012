<?php
// hp_avl.html.php
/**
* Hot Property Availability Extension - Presentation Code
*
* @package Hot Property 0.9
* @copyright (C) 2004 Lee Cher Yeong
* @url http://www.Mosets.com/
* @author Lee Cher Yeong <cy@mosets.com>
**/

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

class hp_avl_HTML {

	function viewAvl( $html, &$avl_types = array(), $property ) {
		global $Itemid, $my;
		hp_avl_HTML::ext_printcss();
		hotproperty_HTML::include_CSS();


		# Global Container - Start
		hotproperty_HTML::include_Container_Start();
		?>
		<div id="heading_Prop">
			<a href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&Itemid=$Itemid"); ?>"><?php echo _HP_COM_PATHWAY_MAIN ." "._HP_ARROW." "; ?></a>  <a href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&task=viewtype&id=".$property->type_id."&Itemid=$Itemid"); ?>"><?php echo $property->type_name ." "._HP_ARROW." "; ?>  </a> <a href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&task=view&id=".$property->id."&Itemid=$Itemid"); ?>"><?php echo $property->name ." "._HP_ARROW." "; ?> </a> <?php echo _HPAVL_NAME; ?>
			<?php
			# Show edit icon for authorized agent
			if ($property->user == $my->id && $property->user > 0 && $my->id > 0) { ?>
			&nbsp;
			<a href="index.php?option=com_hotproperty&task=editprop&id=<?php echo $property->id; ?>&Itemid=<?php echo $Itemid; ?>" title="<?php echo _E_EDIT; ?>">
			<img src="images/M_images/edit.png" width="13" height="14" align="middle" border="0" alt="<?php echo _E_EDIT; ?>" />
			</a>
			<?php } ?>
		</div>
	
		<div style='text-align: center'>
		<?php echo $html; ?>
		<p />
		<a href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&task=view&id=".$property->id."&Itemid=$Itemid"); ?>"><?php echo _HPAVL_RETURN_TO_PROPERTY; ?></a>		
		</div>
		<p />
		<?php hp_avl_HTML::showAvailTypes($avl_types) ?>
		<?php
		# Global Container - End
		hotproperty_HTML::include_Container_End();
	}

	function ext_printcss() {
		global $mainframe;
	
		$mainframe->addCustomHeadTag('<link href="components/com_hp_avl/css/hp_avl.css" rel="stylesheet" type="text/css">');
	}
	function ext_main( &$html, &$avl_types = array(), $month, $year, $property_id, $option ) {
		hp_avl_HTML::ext_printcss();
		?>

		<form action="index2.php" method="POST" name="adminForm">
		<div style='text-align: center'>
		<a href="index2.php?option=<?php echo $option; ?>&task=ext_viewyear&year=<?php echo $year; ?>&returnmonth=<?php echo $month; ?>&property_id=<?php echo $property_id; ?>">View Year</a>
		<?php
			echo $html['calendar'];
		 /*
		 ?>
		<p />
		<input type="hidden" name="option" value="<?php echo $option; ?>" />
		<input type="hidden" name="task" value="ext_save" />
		<input type="hidden" name="property_id" value="<?php echo $property_id; ?>" />
		<input type="hidden" name="month" value="<?php echo $month; ?>" />
		<input type="hidden" name="year" value="<?php echo $year; ?>" />
		<input type="submit" value="Apply Changes" class="button" />
		</form>
		</div>
		<p />
		*/ ?>
		<?php hp_avl_HTML::showAvailTypes($avl_types) ?>
		<?php
	}

	function ext_viewyear( $html, $option, $avl_types = array() ) {
		hp_avl_HTML::ext_printcss();
		?>
		<div style='text-align: center'>
		<?php
			echo $html;
		?>
		<?php hp_avl_HTML::showAvailTypes($avl_types) ?>
		</div>
		<?php
	}

	function ext_error( $html ) {

		echo $html;
	}
	
	function showAvailTypes($types){
		
		if(count($types)){ ?>
		<div style="border: 1px solid #C0C0C0; width: 100px; margin: 15px auto 0px" class='hp_avail_types'>
			<?php foreach($types AS $t) {	?>
				<div class='<?php echo strtolower($t) ?>' style="padding: 3px; font-weight: bold"><?php echo $t ?></div>
			<?php	}	?>
		<?php } ?>
		</div>
		<?php
	}

}

?>