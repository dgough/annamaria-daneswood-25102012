<?php
/**
* @version $Id: login.php 4055 2006-06-19 20:00:59Z stingrey $
* @package Joomla
* @subpackage Users
* @copyright Copyright (C) 2005 Open Source Matters. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );

// load the html drawing class
require_once( $mainframe->getPath( 'front_html' ) );

global $database, $my, $mainframe;
global $mosConfig_live_site, $mosConfig_frontend_login, $mosConfig_db;

if ( $mosConfig_frontend_login != NULL && ($mosConfig_frontend_login === 0 || $mosConfig_frontend_login === '0')) {
	header( "HTTP/1.0 403 Forbidden" );
	echo _NOT_AUTH;
	return;
}

$menu = $mainframe->get( 'menu' );
$params = new mosParameters( $menu->params );

$params->def( 'page_title', 1 );
$params->def( 'header_login', ($menu->name ? $menu->name : _USER_LOGIN ) );
$params->def( 'header_logout', $menu->name );
$params->def( 'pageclass_sfx', '' );
$params->def( 'back_button', $mainframe->getCfg( 'back_button' ) );
$params->def( 'login', urldecode(mosGetParam($_REQUEST,'return', urlencode($mosConfig_live_site))));
$params->def( 'logout', urldecode(mosGetParam($_REQUEST,'return', urlencode($mosConfig_live_site))));
$params->def( 'login_message', 0 );
$params->def( 'logout_message', 0 );
$params->def( 'description_login', 1 );
$params->def( 'description_logout', 1 );
$params->def( 'description_login_text', _LOGIN_DESCRIPTION );
$params->def( 'description_logout_text', _LOGOUT_DESCRIPTION );
$params->def( 'image_login', 'lock.gif' );
$params->def( 'image_logout', 'lock.gif' );
$params->def( 'image_login_align', 'right' );
$params->def( 'image_logout_align', 'right' );
$params->def( 'registration', $mainframe->getCfg( 'allowUserRegistration' ) );

$image_login = '';
$image_logout = '';
if ( $params->get( 'image_login' ) != -1 ) {
	$image = $params->get( 'image_login' ) == 'lock.gif' ? $mosConfig_live_site .'/components/com_login/'. $params->get( 'image_login' ) : $mosConfig_live_site .'/images/stories/'. $params->get( 'image_login' );
}
if ( $params->get( 'image_logout' ) != -1 ) {
	$image = $params->get( 'image_logout' ) == 'lock.gif' ? $mosConfig_live_site .'/components/com_login/'. $params->get( 'image_logout' ) : $mosConfig_live_site .'/images/stories/'. $params->get( 'image_logout' );
}

if ( $my->id ) {
	loginHTML::logoutpage( $params, $image );
} else {
	loginHTML::loginpage( $params, $image );
}
?>