<?php
/**
* @version $Id: login.html.php 4055 2006-06-19 20:00:59Z stingrey $
* @package Joomla
* @subpackage Users
* @copyright Copyright (C) 2005 Open Source Matters. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );

/**
* @package Joomla
* @subpackage Users
*/
class loginHTML {

	function loginpage ( &$params, $image ) {
		global $mosConfig_lang, $mainframe, $mosConfig_live_site;

		$mainframe->addCustomheadTag("<link type='text/css' rel='stylesheet' href='$mosConfig_live_site/components/com_login/styles.css' />");
		
		// used for spoof hardening
		$validate = josSpoofValue(1);

		$return = $params->get('login');
		?>
		<div id="login">
			<form action="<?php echo sefRelToAbs( 'index.php?option=login' ); ?>" method="post" name="login">
				<div class="contentpane<?php echo $params->get( 'pageclass_sfx' ); ?>" style='background: url(<?php echo $image; ?>) no-repeat top <?php echo $params->get( 'image_login_align', 'right' ) ?>;'>
			
					<?php
					if ( $params->get( 'page_title' ) ) {
						?>
						<h1 class="componentheading<?php echo $params->get( 'pageclass_sfx' ); ?>">
						<?php echo $params->get( 'header_login' ); ?>
						</h1>
						<?php
					}
					?>
					<div class="contentdescription">					
					<?php
					if ( $params->get( 'description_login' ) ) {
						 ?>
						<?php echo $params->get( 'description_login_text' ); ?>
						<br /><br />
						<?php
					}
					?>
					</div>
		
					<div class="credentials">
						<div class="row">
							<label for="com_login_username">
								<?php echo _USERNAME; ?>
							</label>
							<input name="username" id="com_login_username" type="text" alt="username" class="inputbox" size="20" />
						</div>
						<div class="row">
							<label for="com_login_password">
							<?php echo _PASSWORD; ?>
							</label>
							<input name="passwd" type="password" id="com_login_password"  class="inputbox" size="20" />
						</div>
						<div class="row">
						<input type="checkbox" name="remember" id="com_login_remember" alt="Remember Me" class="inputbox" value="yes" />
							<label for="com_login_remember">
							<?php echo _REMEMBER_ME; ?>
							</label>
						</div>
						<div class="row">
							<input type="submit" name="submit" class="button" value="<?php echo _BUTTON_LOGIN; ?>" />
						</div>
					</div>
						<div class="lostpasssword">
							<a href="<?php echo sefRelToAbs( 'index.php?option=com_registration&amp;task=lostPassword' ); ?>">
							<?php echo _LOST_PASSWORD; ?>
							</a>
						</div>
							<?php
							if ( $params->get( 'registration' ) ) {
								?>
								<div class="register">
								<?php echo _NO_ACCOUNT; ?>
								<a href="<?php echo sefRelToAbs( 'index.php?option=com_registration&amp;task=register' ); ?>">
								<?php echo _CREATE_ACCOUNT;?>
								</a>
								<?php
							}
							?>
						</div>
				<noscript>
				<?php echo _CMN_JAVASCRIPT; ?>
				</noscript>
				
				<?php
					// displays back button
					mosHTML::BackButton($params);
				?>
				<input type="hidden" name="return" value="<?php echo sefRelToAbs( $return ); ?>" />
				<input type="hidden" name="op2" value="login" />
				<input type="hidden" name="lang" value="<?php echo $mosConfig_lang; ?>" />
				<input type="hidden" name="message" value="<?php echo $params->get('login_message'); ?>" />
				<input type="hidden" name="<?php echo $validate; ?>" value="1" />
				<div style='clear: left'></div>
				</div>				
			</form>  
        </div>
		<?php
  	}

	function logoutpage( &$params, $image ) {
		global $mosConfig_lang, $mainframe, $mosConfig_live_site;

		$mainframe->addCustomheadTag("<link type='text/css' rel='stylesheet' href='$mosConfig_live_site/components/com_login/styles.css' />");
		$return = $params->get('logout');
		?>	
		<div id="login">	
			<form action="<?php echo sefRelToAbs( 'index.php?option=logout' ); ?>" method="post" name="login">
			
			<div class="contentpane<?php echo $params->get( 'pageclass_sfx' ); ?>" style='background: url(<?php echo $image; ?>) no-repeat top <?php echo $params->get( 'image_logout_align', 'right' ) ?>;'>
				<h1 class='componentheading'>Logout</h1>
				<?php
				if ( $params->get( 'page_title' ) ) { ?>
					<h1 class="componentheading<?php echo $params->get( 'pageclass_sfx' ); ?>"><?php echo $params->get( 'header_logout' ); ?></h1>
				<?php
				}
				?>
				<div class="contentdescription">
				<?php
	
				if ( $params->get( 'description_logout' ) ) {
					echo $params->get( 'description_logout_text' );
					?>
					<br/><br/>
					<?php
				}
				?>
				</div>
				
				<div class="row">
				<input type="submit" name="Submit" class="button" value="<?php echo _BUTTON_LOGOUT; ?>" />
				</div>
				
			</div>
			<?php
			// displays back button
			mosHTML::BackButton ( $params );
			?>
			<input type="hidden" name="return" value="<?php echo sefRelToAbs( $return ); ?>" />
			<input type="hidden" name="op2" value="logout" />
			<input type="hidden" name="lang" value="<?php echo $mosConfig_lang; ?>" />
			<input type="hidden" name="message" value="<?php echo $params->get( 'logout_message' ); ?>" />
			<div style='clear: left'></div>
			</form>
		</div>
		<?php
		}
}
?>