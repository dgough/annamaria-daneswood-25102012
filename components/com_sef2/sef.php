<?php
/**
 * SEF module for Joomla!
 *
 * @author      $Author: michal $
 * @copyright   ARTIO s.r.o., http://www.artio.cz
 * @package     JoomSEF
 * @version     $Name$, ($Revision: 4994 $, $Date: 2005-11-03 20:50:05 +0100 (??t, 03 XI 2005) $)
 */

// Security check to ensure this file is being included by a parent file.
if (!defined('_VALID_MOS')) die('Direct Access to this location is not allowed.');

$debug = 0;
// IIS patch.
if (isset($_SERVER['HTTP_X_REWRITE_URL'])) {
    $_SERVER['REQUEST_URI'] = $_SERVER['HTTP_X_REWRITE_URL'];
}
// OpenSEF patch.
define('_OPENSEF', 1);

if ($mosConfig_sef) {
    // VirtueMart redirection hack
    if( file_exists($GLOBALS['mosConfig_absolute_path'].'/components/com_virtuemart/virtuemart.php') ) {
        @session_start();
        $_SESSION['vmhack'] = 1;
    }

    $sefDirAdmin = $GLOBALS['mosConfig_absolute_path'].'/administrator/components/com_sef/';

    // Load language file.
    if (file_exists($sefDirAdmin.'language/'.$mosConfig_lang.'.php')){
        include($sefDirAdmin.'language/'.$mosConfig_lang.'.php');
    }
    else {
        include($sefDirAdmin.'language/english.php');
    }

    // load config data
    $sef_config_class = $sefDirAdmin.'sef.class.php';
    $sef_config_file  = $sefDirAdmin.'config.sef.php';

    if (!is_readable($sef_config_file)) die(_COM_SEF_NOREAD."($sef_config_file)<br />"._COM_SEF_CHK_PERMS);
    if (is_readable($sef_config_class)) require_once($sef_config_class);
    else die(_COM_SEF_NOREAD."( $sef_config_class )<br />"._COM_SEF_CHK_PERMS);

    $sefConfig = new SEFConfig();

    // load checksums
    $license  = trim(@file_get_contents($sefDirAdmin.'signature.b64'));
    $checksum = trim(@file_get_contents($sefDirAdmin.'checksum.md5'));
    $checkstr = '';

    $checkA = array_fill(0, 4, '');
    $checkA = explode('-', $license);
    foreach ($checkA as $id => $checkpart) {
        $checkA[$id] = base64_decode($checkpart);
        $checkstr .= $checkA[$id];
    }
    $sefConfig->enabled &= ($checksum == md5($checkstr));

    // check for kind of SEF or no SEF at all
    // test if this is Joomla! style URL
    if (strstr($_SERVER['REQUEST_URI'], 'index.php/content/')
    || strstr($_SERVER['REQUEST_URI'], '/content/')
    || strstr($_SERVER['REQUEST_URI'], 'index.php/component/option,')
    || strstr($_SERVER['REQUEST_URI'], '/component/option,')) {
        require_once('functions.php');
        decodeurls_mambo();
    }
    // or TIM style url
    elseif (strstr($_SERVER['REQUEST_URI'], 'index.php/view/')) {
        require_once('functions.php');
        decodeurls_tim();
    }
    // otherwise operate as with JoomSEF URL
    else {
    }

    if ($sefConfig->enabled) {
        $sef404 = $GLOBALS['mosConfig_absolute_path']."/components/com_sef/joomsef.php";
        if (is_readable($sef404)) {
            $index = str_replace($GLOBALS['mosConfig_live_site'], '', $_SERVER['PHP_SELF']);
            $base = dirname($index);
            if ($base == '\\') $base = '/';
            $base .= (($base == '/') ? '' : '/');
            $index = basename($index);
            $URI = array();
            if (isset($_SERVER['REQUEST_URI'])) {
                //strip out the base
                $REQUEST = str_replace($GLOBALS['mosConfig_live_site'], '', $_SERVER['REQUEST_URI']);
                $REQUEST = preg_replace("/^".preg_quote($base,'/').'/', '', $REQUEST);
                $URI = new Net_URL($REQUEST);
            }
            else {
                $QUERY_STRING = isset($_SERVER['QUERY_STRING']) ? '?'.$_SERVER['QUERY_STRING'] : '';
                $URI = new Net_URL($index.$QUERY_STRING);
            }

            if ($debug) {
                echo('<pre>');
                print_r($URI);
                print_r($_SERVER);
                echo('</pre>');
                die();
            }

            // Make sure host name matches our config, we need this later.
            if (strpos($GLOBALS['mosConfig_live_site'], $URI->host) === false) {
                header('HTTP/1.0 301 Moved Permanently');
                header('Location: '.$GLOBALS['mosConfig_live_site']);
            }
            else include_once($sef404);

            if (($URI->path == $base)
            || ($URI->path == ($base.$index))
            || (@$option == 'com_frontpage')) {
                // Frontpage code.
                $_VERSION->URL .= $checkA[0];
                $_VERSION->COPYRIGHT .= $checkA[1];
            }
            else {
                // Other page code.
                $_VERSION->URL .= $checkA[2];
                $_VERSION->COPYRIGHT .= $checkA[3];
            }
        }
        else die(_COM_SEF_NOREAD."($sef404)<br />"._COM_SEF_CHK_PERMS);
    }
    else {
        $mambo_sef = $GLOBALS['mosConfig_absolute_path'].'/includes/sef.php';
        if (is_readable($mambo_sef)) include($mambo_sef);
        else die(_COM_SEF_NOREAD."($mambo_sef)<br />"._COM_SEF_CHK_PERMS);
    }
}
else {

    /**
     * Translate URL.
     *
     * @param  string $string
     * @return string
     */
    function sefRelToAbs($string)
    {
		//Make links XHTML compliant, replace & with &amp;
		$string = str_replace( '&', '&amp;' , $string );
        return $string;
    }

}
?>
