<?php
/**
 * SEF module for Joomla!
 *
 * @author      $Author: michal $
 * @copyright   ARTIO s.r.o., http://www.artio.cz
 * @package     JoomSEF
 * @version     $Name$, ($Revision: 4994 $, $Date: 2005-11-03 20:50:05 +0100 (??t, 03 XI 2005) $)
 */

// Security check to ensure this file is being included by a parent file.
if (!defined('_VALID_MOS')) die('Direct Access to this location is not allowed.');

global $database, $URI, $my, $option, $index, $base;

// Debugging on/off
$debug = true;

// Hack in mos database as required
if (!is_object(@$database)) {
    $database = new database($GLOBALS['mosConfig_host'], $GLOBALS['mosConfig_user'], $GLOBALS['mosConfig_password'], $GLOBALS['mosConfig_db'], $GLOBALS['mosConfig_dbprefix']);
}

$REQUEST = $SRU = $_SERVER['REQUEST_URI'];
if ($debug) {
    $GLOBALS['JOOMSEF_DEBUG']['INDEX'] = $index;
    $GLOBALS['JOOMSEF_DEBUG']['BASE'] = $base;
    $GLOBALS['JOOMSEF_DEBUG']['QUERY_STRING'] = $_SERVER['QUERY_STRING'];
    $GLOBALS['JOOMSEF_DEBUG']['REQUEST'] = $REQUEST;
    $GLOBALS['JOOMSEF_DEBUG']['URI'] = $URI;
}

// Check the URL nesting.
switch ($URI->path) {
    case $base:
    case $base.$index: {
        $option = isset($_GET['option']) ? $_GET['option'] : isset($_REQUEST['option']) ? $_REQUEST['option'] : null;

        if (is_null($option)) {
            $GLOBALS['JOOMSEF_DEBUG']['REDIRECT_INDEX'] = 0;
            // fix those funky polls by ensuring we have an Itemid for the homepage
            $query = "SELECT `id`,`link` FROM #__menu where ((`menutype`='mainmenu') AND (`published` > 0) AND (`ordering`='1') AND (`parent` = 0) AND (`access` >= '".(isset($my) ? (intval(@$my->id)) : 0)."'))";		/*Beat: strange rule, but sometimes my is not set here, so at least stop PHP error*/
            $database->setQuery($query);

            if (($row = $database->loadRow())) {
                $GLOBALS['JOOMSEF_DEBUG']['REDIRECT_INDEX'] = 1;
                $_GET['Itemid'] = $_REQUEST['Itemid'] = $Itemid = $row[0];
                $_SERVER['QUERY_STRING'] = $QUERY_STRING  = str_replace('index.php?', '', $row[1])."&Itemid=$Itemid";
                $REQUEST_URI = $GLOBALS['mosConfig_live_site'].'/index.php?'.$QUERY_STRING;
                $_SERVER['REQUEST_URI'] = $REQUEST_URI;

                $matches = array();
                if (preg_match("/option=([a-zA-Z_0-9]+)/", $QUERY_STRING, $matches)) {
                    $_GET['option'] = $_REQUEST['option'] = $option = $matches[1];
                }

                $GLOBALS['JOOMSEF_DEBUG']['ROW'] = $row;
                $GLOBALS['JOOMSEF_DEBUG']['QUERY_STRING'] = $QUERY_STRING;
                $GLOBALS['JOOMSEF_DEBUG']['REQUEST_URI'] = $REQUEST_URI;

                unset($matches);
                if (!headers_sent()) {
                    header('HTTP/1.0 200 OK');
                }
                else {
                    $url = $GLOBALS['mosConfig_live_site'].$_SERVER['QUERY_STRING'];
                    //print_r($path_array);
                    die("<br />Are we debugging?<br />Killed at line ".__LINE__." in ".basename(__FILE__).": HEADERS ALREADY SENT (200)<br />URL=".@$url.":<br />OPTION=".@$option.":");
                }
            }

            // If MetaBot is installed ...
            $database->setQuery("SELECT id FROM #__mambots WHERE element = 'joomsef_metabot' AND folder = 'system' AND published = 1");
            if ($database->loadResult()) {
                // ... and has meta tags.
                $database->setQuery("SELECT * FROM #__redirection WHERE oldurl = '' OR oldurl = '$index' LIMIT 1");
                if (!is_null($database->loadObject($sefRow))) {
                    if (@$sefRow->metatitle)  $GLOBALS['sefMetaTags']['title']     = $sefRow->metatitle;
                    if (@$sefRow->metadesc)   $GLOBALS['sefMetaTags']['metadesc']  = $sefRow->metadesc;
                    if (@$sefRow->metakey)    $GLOBALS['sefMetaTags']['metakey']   = $sefRow->metakey;
                    if (@$sefRow->metalang)   $GLOBALS['sefMetaTags']['lang']      = $sefRow->metalang;
                    if (@$sefRow->metarobots) $GLOBALS['sefMetaTags']['robots']    = $sefRow->metarobots;
                    if (@$sefRow->metagoogle) $GLOBALS['sefMetaTags']['googlebot'] = $sefRow->metagoogle;
                }
            }
        }
        break;
    }
    case '': {
        die(_COM_SEF_STRANGE.' URI->path='.$URI->path.":<br />".basename(__FILE__)."-".__LINE__);
    }
    default: {
        // Lets process the URL
        // strip out the base
        $path = preg_replace('/^'.preg_quote($base, '/').'/', '', $URI->path);
        $path_array = explode('/', $path);
        $ext = getExt($path_array);
        $sef_ext_class = 'sef_'.$ext['name'];
        if ($sef_ext_class != 'sef_404') {
            // do our space conversion
            // then find our suffix (.html) and strip it off,
            // SEF Advance extensions don't want the suffix
            // and some them require the spaces
            $x = 0;
            foreach ($path_array as $pathdata) {
                $path_array[$x] = $pathdata = str_replace($sefConfig->replacement, ' ', $pathdata);
                if ($sefConfig->suffix && strpos($pathdata, $sefConfig->suffix) !== false) {
                    $path_array[$x] = str_replace($sefConfig->suffix, '', $pathdata);
                }
                $x++;
            }
        }

        if ($debug) {
            $GLOBALS['JOOMSEF_DEBUG']['EXT'] = $ext;
            $GLOBALS['JOOMSEF_DEBUG']['SEF_EXT'] = $sef_ext_class;
            $GLOBALS['JOOMSEF_DEBUG']['PATH'] = $path;
        }
        
        // Set php show_error settings to prevent notices breaking headers.
        $displayErrors = ini_get('display_errors');
        ini_set('display_errors', 0);

        // Instantiate class that will take care about URL conversion
        require_once($ext['path']);
        // Also instantiate always ours.
        require_once($GLOBALS['mosConfig_absolute_path'].'/components/com_sef/sef_ext.php');

        eval("\$sef_ext = new $sef_ext_class;");
        $pos = 0;

        if (isset($_REQUEST['option'])) {
            $pos = array_search($_REQUEST['option'], $path_array);
            if ($pos === false) $pos = 0;
        }

        if ($sef_ext_class != 'sef_content' && $sef_ext_class != 'sef_component' && $pos == 0) {
            array_unshift($path_array, 'option');
        }

        if ($debug) {
            $GLOBALS['JOOMSEF_DEBUG']['POS'] = $pos;
            $GLOBALS['JOOMSEF_DEBUG']['PATH_ARRAY'] = $path_array;
        }

        $_SEF_SPACE   = $sefConfig->replacement;
        $QUERY_STRING = $sef_ext->revert($path_array, $pos);
        
        ini_set('display_errors', $displayErrors);

        // Append the original query string because some components
        // (like SMF Bridge and SOBI2) use it
        if( !empty($URI->querystring) ) {
            foreach( $URI->querystring as $name => $value ) {
                if( empty($QUERY_STRING) )
                    $QUERY_STRING = $name.'='.$value;
                else 
                    $QUERY_STRING .= '&'.$name.'='.$value;
            }
        }
        
        if (is_valid($QUERY_STRING)) {
            // Set anchor without extras.
            $anchor = ($URI->anchor) ? '#'.$URI->anchor : '';
            
            $_SERVER['QUERY_STRING'] = $QUERY_STRING = str_replace('&?', '&', $QUERY_STRING./*(isset($QS) ? $QS : '').*/$anchor);
            $REQUEST_URI = $GLOBALS['mosConfig_live_site'].'/index.php?'.$QUERY_STRING;
            $_SERVER['REQUEST_URI'] = $REQUEST_URI;

            // Make sure to set option variable.
            if (preg_match("/option=([a-zA-Z_0-9]+)/", $QUERY_STRING, $matches)) {
                $_GET['option'] = $_REQUEST['option'] = $option = $matches[1];
            }

            unset($matches);

            while (list($key, $value) = each($_GET)) {
                $GLOBALS[$key] = $value;
            }

            if (!headers_sent()) {
                header('HTTP/1.0 200 OK');
            }
            else {
                $url = $GLOBALS['mosConfig_live_site']."/index.php?".$_SERVER['QUERY_STRING'];
                print_r($path_array);
                die("<br />Are we debugging???<br />Killed at line ".__LINE__." in ".basename(__FILE__).": HEADERS ALREADY SENT (200)<br />URL=".@$url.":<br />OPTION=".@$option.":");
            }
        }
        // bad URL, so check to see if we've seen it before
        else {
            $query = "SELECT * FROM #__redirection WHERE oldurl = '".$path."'";
            $database->setQuery($query);
            $results = $database->loadObjectList();

            if ($results) {
                // we have, so update counter
                $database->setQuery("UPDATE #__redirection SET cpt=(cpt+1) WHERE oldurl = '".$path."'");
                $database->query();
            }
            else {
                // record the bad URL
                $query = 'INSERT INTO `#__redirection` ( `cpt` , `oldurl` , `newurl` , `dateadd` ) '
                . ' VALUES ( \'1\', \''.$path.'\', \'\', CURDATE() );'
                . ' ';
                $database->setQuery($query);
                $database->query();
            }

            // redirect to the error page
            // You MUST create a static content page with the title 404 for this to work properly
            $mosmsg = 'FILE NOT FOUND: '.$path;
            $_GET['mosmsg'] = $_REQUEST['mosmsg'] = $mosmsg;
            $option = 'com_content';
            $task = 'view';

            if ($sefConfig->page404 == '0') {
                $sql='SELECT id  FROM #__content WHERE `title`="404"';
                $database->setQuery($sql);

                if (($id = $database->loadResult())) {
                    $Itemid = null; /*Beat: was wrong: =$id : the $Itemid represents the menuId, and $id the contentId ! */
                    $_SERVER['QUERY_STRING'] = "option=com_content&task=view&id=$id&Itemid=$id&mosmsg=$mosmsg";
                    $_SERVER['REQUEST_URI'] = $GLOBALS['mosConfig_live_site']."/index.php?".$_SERVER['QUERY_STRING'];
                    $_GET['option'] = $_REQUEST['option'] = $option;
                    $_GET['task'] = $_REQUEST['task'] = $task;

                    /*Beat: was wrong: $_GET['Itemid'] = $_REQUEST['Itemid'] = $Itemid; */
                    unset($_GET['Itemid']);
                    unset($_REQUEST['Itemid']);
                    $_GET['id'] = $_REQUEST['id'] = $id;
                }
                else {
                    die(_COM_SEF_DEF_404_MSG.$mosmsg."<br>URI:".$_SERVER['REQUEST_URI']);
                }
            }
            elseif ($sefConfig->page404 == '9999999') {
                //redirect to frontpage
                $front404 = 1;
            }
            else{
                $id = $Itemid  = $sefConfig->page404;
                $_SERVER['QUERY_STRING'] = "option=com_content&task=view&id=$id&Itemid=$id&mosmsg=$mosmsg";
                $_SERVER['REQUEST_URI'] = $GLOBALS['mosConfig_live_site'].'/index.php?'.$_SERVER['QUERY_STRING'];
                $_GET['option'] = $_REQUEST['option'] = $option;
                $_GET['task'] = $_REQUEST['task'] = $task;
                $_GET['Itemid'] = $_REQUEST['Itemid'] = $Itemid;
                $_GET['id'] = $_REQUEST['id'] = $id;
            }

            if (!headers_sent()) {
                header('HTTP/1.0 404 NOT FOUND');
                if (isset($front404) && $front404) mosRedirect( $GLOBALS['mosConfig_live_site'] );
            }
            else {
                $url = sefRelToAbs($GLOBALS['mosConfig_live_site'].'/index.php?'.$_SERVER['QUERY_STRING']);
                print_r($path_array);
                die("<br />Are we debugging???<br />Killed at line ".__LINE__." in ".basename(__FILE__).": HEADERS ALREADY SENT (404)<br />URL=".@$url.":<br />OPTION=".@$option.":");
                //mosRedirect($url);
            }
        } //end bad url
    }//
}

if ($debug) {
    $GLOBALS['JOOMSEF_DEBUG']['SERVER_QUERY_STRING'] = $_SERVER['QUERY_STRING'];
    $GLOBALS['JOOMSEF_DEBUG']['SERVER_REQUEST_URI'] = $_SERVER['REQUEST_URI'];
}

/**
 * Check if own extension exists for a component.
 * 
 * @param  string $component  Component name
 * @return object
 */
function existOwnExt($option)
{
    return is_readable($GLOBALS['mosConfig_absolute_path'].'/components/com_sef/sef_ext/com_'.$option.'.php');
}

/**
 * Does the component in question has own (3rd party) sef extension?
 * Returns DB select result if found or null.
 * 
 * @param  string $component  Component name
 * @return object
 */
function exist3rdExt($option)
{
    $path = $GLOBALS['mosConfig_absolute_path']."/components/$option/sef_ext.php";
    return file_exists($path) ? $path : false;
}

function testComponent($component)
{
    global $database, $sefConfig;

    $debug = 0;

    // check for sef_ext
    $component = str_replace($sefConfig->replacement, " ", $component);
    $sql = "SELECT `id`,`link` FROM #__menu  WHERE ((`name` LIKE '%".$component."%') AND (`published` > 0))";
    $database->setQuery($sql);
    $rows = @$database->loadObjectList();

    if ($database->getErrorNum()) {
        die($database->stderr());
    }

    if ($debug) {
        $GLOBALS['JOOMSEF_DEBUG']['SEF_EXT_EXISTS']['THIS_NAME'] = $component;
        $GLOBALS['JOOMSEF_DEBUG']['SEF_EXT_EXISTS']['ROWS'] = $rows;
    }

    if (@count($rows) > 0) {
        //$option = str_replace('index.php?option=', '', $rows[0]->link);
        return $rows[0];
    } 
    
    return null;
}

/**
 * Determine what class use to convert URLs.
 *
 * @param  array $urlArray
 * @return string
 */
function getExt($urlArray)
{
    global $database, $sefConfig;

    $ext = array();
    $ext['path'] = $GLOBALS['mosConfig_absolute_path'].'/components/com_sef/sef_ext.php';

    // test if component with given name can be found
    $component = testComponent($urlArray[0]);

    // if found our own plug-in, use it
    if (existOwnExt($urlArray[0])) {
        $option = 'com_404';
    }
    // try to find sef_ext for other components
    elseif (($path = exist3rdExt($urlArray[0]))) {
        $_GET['option'] = $_REQUEST['option'] = $option = str_replace('index.php?option=', '', $row->link);
        $_GET['Itemid'] = $_REQUEST['Itemid'] = $row->id;
        $ext['path'] = $path;
    }
    elseif ((strpos($urlArray[0], 'com_') !== false) or ($urlArray[0] == 'component')) {
        $_GET['option'] = $_REQUEST['option'] = $option = 'com_component';
    }
    elseif($urlArray[0] == 'content') {
        $_GET['option'] = $_REQUEST['option'] = $option = 'com_content';
    }
    // otherwise use default handler
    else $option = 'com_404';

    $ext['name'] = str_replace('com_', '', $option);

    return $ext;
}

/**
 * Enter description here...
 *
 * @param  string $string
 * @return bool
 */
function is_valid($string)
{
    global $base, $index;

    if (strcmp($string, $index) == 0 || strcmp($string, $base.$index) == 0) {
        $state = true;
    }
    else {
        $state = false;
        require_once($GLOBALS['mosConfig_absolute_path'].'/components/com_sef/sef_ext.php');
        $sef_ext = new sef_404();
        $option = (isset($_GET['option'])) ? $_GET['option'] : (isset($_REQUEST['option'])) ? $_REQUEST['option'] : null;

        $vars = array();
        if (is_null($option)) {
            parse_str($string, $vars);
            if (isset($vars['option'])) {
                $option = $vars['option'];
            }
        }

        switch ($option) {
            case is_null($option): break;
            case 'login':		/*Beat: makes this also compatible with CommunityBuilder login module*/
            case 'logout': {
                $state = true;
                break;
            }
            default: {
                if (is_valid_component($option)){
                    if ($option != 'com_content' | $option != 'content') {
                        $state = true;
                    }
                    else {
                        $title = $sef_ext->getContentTitles($_REQUEST['task'], $_REQUEST['id']);
                        //die(count($title));
                        if (count($title) > 0) $state = true;
                    }
                }
            }
        }
    }

    return $state;
}

/**
 * Check whether object is a valid component.
 *
 * @param  object $this
 * @return bool
 */
function is_valid_component($this)
{
    $state = false;
    $path = $GLOBALS['mosConfig_absolute_path'] .'/components/';

    if (is_dir($path) && $contents = opendir($path)) {
        while (($node = readdir($contents)) !== false) {
            if ($node != '.' && $node != '..'
            && is_dir($path.'/'.$node) && $this == $node) {
                $state = true;
                break;
            }
        }
    }
    return $state;
}

/**
 * Rewrite relative URL to absolute.
 *
 * @param  string $string
 * @return string
 */
function sefRelToAbs($string)
{
    global $database, $sefConfig, $_SEF_SPACE, $mosConfig_db, $mainframe;
	$string = str_replace( '&', '&amp;' , $string );
    // If selected db is not default joomla's db, then select default db
    // and save previous one for restoring
    $database->setQuery("SELECT DATABASE()");
    $prevDb = $database->loadResult();
    if ($prevDb != $mosConfig_db) {
        $database->setQuery("USE $mosConfig_db");
        $database->query();
    }

    $debug = 0;

    // Check if this is site root.
    if ($string == $GLOBALS['mosConfig_live_site']
    || $string == $GLOBALS['mosConfig_live_site'].'/'
    || $string == $GLOBALS['mosConfig_live_site'].'/index.php') {
        restoreDB($prevDb);
        return $GLOBALS['mosConfig_live_site'];
        //return $string;
    }

    $newstring = str_replace($GLOBALS['mosConfig_live_site'].'/', '', $string);

    // If this appears to be SEO-able URL, work with it.
    if ((!strcasecmp(substr($newstring, 0, 9), 'index.php'))
    && !eregi('^(([^:/?#]+):)', $newstring)
    && !eregi('this\.options\[selectedIndex\]\.value', $newstring))
    {
        // Replace & character variations.
        $string = str_replace(array('&amp;', '&#38;'), array('&', '&'), $newstring);

        // Add lang if configured to and it is missing.
        if (class_exists('JoomFish')) {
            // Define language if set to.
            if ($sefConfig->alwaysUseLang && strpos($string, 'lang=') === false) {
                $string .= ((strpos($string, '?') !== false) ? '&' : '?');
                $string .= 'lang='.SEFTools::getLangCode();
            }
        }

        $URI = new Net_URL($string);

        if (count($URI->querystring) > 0) {
            // Import new vars here.
            $option = null;
            $task = null;
            $sid = null;
            extract($URI->querystring, EXTR_REFS);
        }
        else {
            restoreDB($prevDb);
            return $URI->url;
        }

        // is there a named anchor attached to $string? If so, strip it off, we'll put it back later.
        if ($URI->anchor) $string = str_replace('#'.$URI->anchor, '', $string);

        if ($debug) {
            $GLOBALS['JOOMSEF_DEBUG']['sefRelToAbs'][$string]= $URI;
            $debugString = $string;
        }

        //if (!((isset($task) ? ((@$task == "new") | (@$task == "edit")) : false)) && isset($option)   ) {
        if (isset($option) && @$task != 'edit') {
            /*Beat: sometimes task is not set, e.g. when $string = "index.php?option=com_frontpage&Itemid=1" */
            switch ($option) {
                // Skipped extensions.
                case (in_array($option, $sefConfig->skip)): {
                    $sefstring = str_replace('&', '&amp;', $string);
                    $skipThis = true;
                    break;
                }
                // Non-cached extensions.
                case (in_array($option, $sefConfig->nocache)): {
                    $sefstring = 'component/';
                    foreach($URI->querystring as $key => $value) {
                        $sefstring .= "$key,$value/";
                    }
                    $sefstring = str_replace('option/', '', $sefstring);
                    break;
                }
                // Default handler.
                default: {
                    // If component has its own sef_ext plug-in included.
                    // However, prefer own plugin if exists (added by Michal, 28.11.2006)
                    if (file_exists($GLOBALS['mosConfig_absolute_path']."/components/$option/sef_ext.php")
                    && !file_exists($GLOBALS['mosConfig_absolute_path']."/components/com_sef/sef_ext/$option.php")) {
                        // Load the plug-in file.
                        require_once($GLOBALS['mosConfig_absolute_path']."/components/$option/sef_ext.php");
                        // Load our sef also to provide general functions.
                        require_once($GLOBALS['mosConfig_absolute_path'].'/components/com_sef/sef_ext.php');

                        $_SEF_SPACE = $sefConfig->replacement;
                        //$longurl = ($sefConfig->useAlias == 1);
                        //$lowerCase = $sefConfig->lowerCase;
                        $className = str_replace('com_', 'sef_', $option);
                        eval("\$sef_ext = new $className;");
                        $title[] = getMenuTitle($option, null);
                        $string = str_replace('&', '&amp;', $string);
                        $sefstring = $sef_ext->create($string);

                        if ($sefstring == $string) {
                            restoreDB($prevDb);
                            return $string;
                        }
                        else {
                            $sefstring = str_replace(' ', $_SEF_SPACE, $sefstring);
                            $sefstring = str_replace(' ', '', titleToLocation($title[0]).'/'.$sefstring.(($sefstring != '') ? $sefConfig->suffix : ''));
                            $sefstring = str_replace('/'.$sefConfig->suffix, $sefConfig->suffix, $sefstring);
                        }
                    }
                    // Component has no own sef extension.
                    else {
                        // Ensure that the session IDs are removed.
                        if (isset($sid)) $string = str_replace("sid=$sid", '', $string);
                        // Ensure that the mosmsg are removed.
                        if (isset($mosmsg)) $string = str_replace("mosmsg=$mosmsg", '', $string);
                        // Clean Itemid if desired.
                        if (isset($sefConfig->excludeSource) && $sefConfig->excludeSource && isset($Itemid)) {
                            $string = str_replace("Itemid=$Itemid", '', $string);
                        }
                        // Clean remaining characters.
                        $string = trim($string, '&?');
                        $string = str_replace('&&', '&', $string);

                        // Let's reorder the variables in non-sef url first so they
                        // are in alphabetical order (some components create for
                        // the same page urls with different variables order)
                        // -- except option which should de first (changed in 1.3.4)
                        $pos = strpos($string, '?');
                        if ($pos) {
                            $firstPart = substr($string, 0, $pos + 1);
                            $secondPart = substr($string, $pos + 1);
                            $urlVars = explode('&', $secondPart);
                            // make params unique and order them
                            $urlVars = array_unique($urlVars);
                            sort($urlVars);
                            // search for option param and remove it from array
                            for ($i = 0; $i < count($urlVars); $i++) {
                                if (strpos($urlVars[$i], 'option=') === 0) {
                                    $opt = $urlVars[$i];
                                    unset($urlVars[$i]);
                                }
                            }
                            // readd option to the beginning of the
                            if (isset($opt)) array_unshift($urlVars, $opt);
                
                            $secondPart = implode('&', $urlVars);
                            $string = $firstPart.$secondPart;
                        }
                
                        // Check if the url is already saved in the database.
                        If (!($sefstring = getSefUrlFromDatabase($string))) {
                            require_once($GLOBALS['mosConfig_absolute_path'].'/components/com_sef/sef_ext.php');
                            $sef_ext = new sef_404();

                            // Rewrite the URL now.
                            $sefstring = $sef_ext->create($string, $URI->querystring);
                        }
                        // Reconnect the sid to the url.
                        if (isset($sid)) $sefstring .= (strpos($sefstring, '?') !== false ? '&' : '?').'sid='.$sid;
                        // Reconnect mosmsg to the url.
                        if (isset($mosmsg)) $sefstring .= (strpos($sefstring, '?') !== false ? '&' : '?').'mosmsg='.urlencode($mosmsg);
                        // Reconnect ItemID to the url.
                        if (isset($sefConfig->excludeSource) && $sefConfig->excludeSource && $sefConfig->reappendSource & isset($Itemid)) {
                            $sefstring .= (strpos($sefstring, '?') !== false ? '&amp;' : '?').'Itemid='.urlencode($Itemid);
                            //$URI->anchor .= (($URI->anchor) ? '-' : '').urlencode('ii'.$Itemid);
                        }
                    }
                }
            }
            if ($debug){ $GLOBALS['JOOMSEF_DEBUG']['sefRelToAbs']['SEF_EXT'][$debugString] = $sef_ext;}
            if (isset($sef_ext)) unset($sef_ext);

            $string = $GLOBALS['mosConfig_live_site'].'/'.$sefstring.(($URI->anchor)? '#'.$URI->anchor : '');
        }
        $ret = (!isset($skipThis) || !$skipThis) ? ($sefConfig->lowerCase ? strtolower($string) : $string) : $string;
        $ret = str_replace('itemid', 'Itemid', $ret);
    }

    if (!isset($ret)) $ret = $string;
    if ($debug) $GLOBALS['JOOMSEF_DEBUG']['sefRelToAbs']['RET'][$debugString] = $ret;

    return $ret;
}

/**
 * If given DB name is different from default joomla's one, select it
 *
 * @param string $db
 */
function restoreDB($db)
{
    global $mosConfig_db;

    if ($db != $mosConfig_db) {
        $database->setQuery("USE $db");
        $database->query();
    }
}

/**
 * Convert title to URL name.
 *
 * @param  string $title
 * @return string
 */
function titleToLocation(&$title)
{
    global $sefConfig;

    // remove accented characters
    // $title = strtr($title,
    //'�������������������������������������ݍ�������������������������������',
    //'SOZsozzAuRAAAALCCCEEEEIIDDNNOOOORUUUUYTsraaaalccceeeeiiddnnooooruuuuyt-');
    // Replace non-ASCII characters.
    $title = strtr($title, $sefConfig->getReplacements());

    // remove quotes, spaces, and other illegal characters
    $title = preg_replace(array('/\'/', '/[^a-zA-Z0-9\-!.,+]+/', '/(^_|_$)/'), array('', $sefConfig->replacement, ''), $title);

    return $sefConfig->lowerCase ? strtolower($title) : $title;
}

function getMenuTitle($option, $task, $id = null, $string = null)
{
    global $database, $sefConfig;

    $debug = 0;

    // JF translate extension.
    $jfTranslate = $sefConfig->translateNames ? ', id' : '';

    if (isset($string)) {
        $sql = "SELECT name$jfTranslate FROM #__menu WHERE link = '$string'";
    }
    elseif (isset($id) && $id != 0) {
        $sql = "SELECT name$jfTranslate FROM #__menu WHERE id = '$id'";
    }
    else {
        $sql = "SELECT name$jfTranslate FROM #__menu WHERE link = 'index.php?option=$option'";
    }

    $database->setQuery($sql);
    $rows = @$database->loadObjectList();

    if ($debug) {
        echo('<pre>');
        $GLOBALS['JOOMSEF_DEBUG']['getMenuTitle']['ROWS-'.$option.'-'.$task] = $rows;
        echo('</pre>');
    }

    if ($database->getErrorNum()) die($database->stderr());
    elseif (@count($rows) > 0) {
        if (!empty($rows[0]->name)) $title = strtolower($rows[0]->name);
    }
    else $title = str_replace('com_', '', $option);

    return $title;
}

/**
 * Try to load an SEF URL from redirection table.
 *
 * @param  string $url  Original Joomla! URL
 * @return mixed        Found SEF URL or false if not found
 */
function getSefUrlFromDatabase($url)
{
    global $database;

    $query = "SELECT oldurl FROM #__redirection WHERE newurl = '".addslashes(urldecode($url))."'";
    $database->setQuery($query);
    $result = $database->loadresult();

    return !empty($result) ? $result : false;
}
?>
