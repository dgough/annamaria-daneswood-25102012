<?php
/**
 * SEF module for Joomla!
 *
 * @author      $Author: michal $
 * @copyright   ARTIO s.r.o., http://www.artio.cz
 * @package     JoomSEF
 * @version     $Name$, ($Revision: 4994 $, $Date: 2005-11-03 20:50:05 +0100 (??t, 03 XI 2005) $)
 */

// Security check to ensure this file is being included by a parent file.
if (!defined('_VALID_MOS')) die('Direct Access to this location is not allowed.');

// Set title.
$title = array();

//Limitstart, limit nog toevoegen
switch ($task) {
	case 'new': {
    	/*
    	$title[] = getMenuTitle($option, $task, $Itemid, $string);
    	$title[] = 'new' . $sefConfig->suffix;
    	*/
    	break;
	}
	case 'archivecategory':
	case 'archivesection': {
		if (eregi($task.".*id=".$id, $_SERVER['REQUEST_URI'])) break;
	}
	default: {
		//$title = array_merge($title, sef_404::getContentTitles($task,$id));
		$title = sef_404::getContentTitles($task, $id);
		if (count($title) === 0) $title[] = getMenuTitle($option, $task, $Itemid);
//		if ((@$task == "view") && isset($sefConfig->suffix)) {
//			// throw the suffix on the last item
//			if ($sefConfig->suffix == "/") {
//				$title[] = "/";
//			}else{
//				$title[count($title)-1] .= $title[count($title)-1].$sefConfig->suffix;
//			}
//		}
		if ((@$task == 'view') && isset($sefConfig->suffix)) {
			$title[count($title) - 1] .= $sefConfig->suffix;
		}
		else {
			$title[] = '/';
		}
	}
}
if (count($title) > 0) {
	$string = sef_404::sefGetLocation($string, $title, null, @$limit, @$limitstart, @$lang);
}
?>