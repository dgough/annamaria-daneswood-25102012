<?php
/**
 * SEF module for Joomla!
 * Extension for Mosets Tree component.
 *
 * @author      $Author: michal $
 * @copyright   ARTIO s.r.o., http://www.artio.cz
 * @package     JoomSEF
 * @version     $Name$, ($Revision: 4994 $, $Date: 2005-11-03 20:50:05 +0100 (??t, 03 XI 2005) $)
 */

// Security check to ensure this file is being included by a parent file.
if (!defined('_VALID_MOS')) die('Direct Access to this location is not allowed.');


/**
 * Use this to get variables from the original Joomla! URL, such as $task, $page, $id, $catID, ...
 */
extract($vars);
$title = array();

$title[] = getMenuTitle(@$option, @$task, @$Itemid);

/**
 * Helper function to get parent and sub category tree array for building URL
 */
if (!function_exists( 'mtree_sef_get_category_array')) {
    function mtree_sef_get_category_array(&$db, $cat_id)
    {
        static $tree = false;
        if (!$tree){
            $db->setQuery("SELECT cat_name, cat_id, cat_parent FROM #__mt_cats");
            $tree = $db->loadObjectList('cat_id');
        }
        $title = array();
        while ($cat_id != 0) {
            $title[] = $tree[$cat_id]->cat_name;
            $cat_id = $tree[$cat_id]->cat_parent;
        } 
        //$title[] = "directory"; //root 'folder' for URL, change if you like
        return array_reverse($title);
    }
}

/**
 * Get category tree (parent cat and sub cats).
 */
if (isset($cat_id) && $cat_id != 0) {
    $title = array_merge($title, mtree_sef_get_category_array($database, $cat_id));
    unset($vars['cat_id']);
}
/**
 * Get info for recommend, contact and viewlink links.
 */
if (isset($link_id)) {
    //get category tree (parent cat and sub cats)
    $sql  = "SELECT cat_id FROM #__mt_cl WHERE link_id=".$link_id;
    $database->setQuery( $sql );
    $cat_id = $database->loadResult();
    $title = mtree_sef_get_category_array( $database, $cat_id);
    //get link name (company name)
    $sql  = "SELECT link_name FROM #__mt_links WHERE link_id=".$link_id;
    $database->setQuery( $sql );
    if($link_name=$database->loadResult()){
        $title[] = $link_name;
    }
    unset($vars['link_id']);
}

/**
 * Alphabetical search / filter (a, b, c, d, etc...)
 */
if (@$task == 'listalpha' && isset($start)) {
    $title[] = $start;
    unset($vars['start']);
    unset($task);
}
/**
 * Remove the 'listcats.html' url ending. Change it to index.html
 */
elseif (@$task == 'listcats') {
    unset($task);
}

/**
 * Pass the title array to the sefGetLocation function.
 */ 
if (count($title) > 0) {
    $string = sef_404::sefGetLocation($string, $title, @$task, @$limit, @$limitstart, @$lang);
}
?>
