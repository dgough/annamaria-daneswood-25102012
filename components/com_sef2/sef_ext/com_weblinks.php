<?php
/**
 * SEF module for Joomla!
 *
 * @author      $Author: michal $
 * @copyright   ARTIO s.r.o., http://www.artio.cz
 * @package     JoomSEF
 * @version     $Name$, ($Revision: 4994 $, $Date: 2005-11-03 20:50:05 +0100 (??t, 03 XI 2005) $)
 */

// Security check to ensure this file is being included by a parent file.
if (!defined('_VALID_MOS')) die('Direct Access to this location is not allowed.');

// JF translate extension.
global $sefConfig;
$jfTranslate = $sefConfig->translateNames ? ', `id`' : '';

if ((empty($this_task)) && (@$task == 'view')) {
    $database->setQuery("SELECT `title`$jfTranslate FROM #__weblinks WHERE id = '$id'");
    $rows = $database->loadObjectList();

    if ($debug) $GLOBALS['JOOMSEF_DEBUG']['CLASS_SEF_404'][$debug_string][$option] = $rows;

    if ($database->getErrorNum()) die( $database->stderr());
    elseif (@count($rows) > 0 && !empty($rows[0]->title)) {
        $this_task = titleToLocation($rows[0]->title);
    }
}

$title = array();
$title[] = getMenuTitle($option, @$this_task);
$title[] = '/';

$arg2 = array();
if (isset($catid)) {
    $arg2[] = sef_404::getCategories($catid);
}
$title = array_merge($title, $arg2);

if (isset($task) && $task == 'new') {
    $title[] = 'new'.$sefConfig->suffix;
}

//$this_task wordt hier meegenomen, maar ik snap niet hoe het werkt
//Even aan Adam vragen
//Title zijn de menuitem, categorie, enz...
//String = url
if (count($title) > 0) $string = sef_404::sefGetLocation($string, $title, @$this_task, null, null, @$vars['lang']);
if ($debug) $GLOBALS['JOOMSEF_DEBUG']['CLASS_SEF_404'][$debug_string]['STRING'] = $string;
?>
