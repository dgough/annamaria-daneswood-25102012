<?php
/**
 * mosets Hot Property SEF extension for Joomla!
 *
 * @author      $Author: David Jozefov $
 * @copyright   ARTIO s.r.o., http://www.artio.cz
 * @package     JoomSEF
 * @version     $Name$, ($Revision: 4994 $, $Date: 2005-11-03 20:50:05 +0100 (??t, 03 XI 2005) $)
 */

// Security check to ensure this file is being included by a parent file.
if (!defined('_VALID_MOS')) die('Direct Access to this location is not allowed.');

# Include the config file
require( $GLOBALS['mosConfig_absolute_path'].'/administrator/components/com_hotproperty/config.hotproperty.php' );

# Include the language file. Default is English
if ($hp_language=='') {
	$hp_language='english';
}
include_once('components/com_hotproperty/language/'.$hp_language.'.php');


class sefext_com_hotproperty extends sef_joomsef
{

	function create($string, &$vars) {
        global $sefConfig;

        $url = parse_url($string);
        parse_str($url['query'], $vars);

        $params = SEFTools::GetExtParams('com_hotproperty');

        extract($vars);

        // Set title.        
		$title[] = getMenuTitle(@$option, @$task, @$Itemid);
		
        switch (@$task) {
           
            case 'viewtype':
	            $title[] = $this->sefHPGetTypeName($id);
	            unset($task);
           	 	break;
            
	        case 'view':
	            $title = array_merge($title, $this->sefHPGetPropertyPath($id));
	            unset($task);
	            break;
	            
	        case 'viewagent':
	            $title[] = _HP_VIEW_AGENT_TITLE;
	            $title[] = $this->sefHPGetAgentName($id);
	            unset($task);
	            break;
	
	        case 'viewagentemail':
	            $title[] = _HP_VIEW_AGENT_TITLE;
	            $title[] = $this->sefHPGetAgentName($id);
	            $title[] = 'email';
	            unset($task);
	            break;
	
	        case 'viewco':
	            $title[] = _HP_COMPANY;
	            $title[] = $this->sefHPGetCompanyName($id);
	            unset($task);
	            break;
	            
	        case 'viewcoemail':
	            $title[] = _HP_COMPANY;
	            $title[] = $this->sefHPGetCompanyName($id);
	            $title[] = 'email';
	            unset($task);
	            break;
	            
	        case 'viewwishlist':
	            $title[] = 'wishlist';
	            unset($task);
	            break;
	            
	        case 'newproperties':
	            $title[] = 'newproperties';
	            unset($task);
	            break;
	            
	        case 'viewspecials':
	            $title[] = 'specialoffers';
	            unset($task);
	            break;
            
	        case 'search':
	            $title[] = 'search';
	            unset($task);
	            break;
	            
	        case 'advsearch':
	            $title[] = 'advanced search';
	            unset($task);
	            break;
	            
            case 'asearch':
	            $title[] = 'results';
	            unset($task);
	            break;
   
            }
            
        $nonSefVars = array();
        foreach($vars AS $k => $v)
        {
            if(!in_array($k, array('id','task','option','lang'))) $nonSefVars[$k] = $v;
        }
        
        if (count($title) > 0) {
           
		    if( isset($limit) ) $nonSefVars['limit'] = $limit;
		    if( isset($limitstart) ) $nonSefVars['limitstart'] = $limitstart;
		    if( isset($order) ) $nonSefVars['order'] = $order;
		    if( isset($sort) ) $nonSefVars['sort'] = $sort;

            $string = sef_joomsef::sefGetLocation($string, $title, null, null, null, @$lang, $nonSefVars);
        }

        return $string;
    }
        
	// Returns type name for given id
    function sefHPGetTypeName($typeId) {
        global $database;
        
        $database->setQuery("SELECT `name` FROM `#__hp_prop_types` WHERE `id` = $typeId");
        return $database->loadResult();
    }


	// Returns agent name for given id
    function sefHPGetAgentName($agentId) {
        global $database;
        
        $database->setQuery("SELECT `name` FROM `#__hp_agents` WHERE `id` = $agentId");
        return $database->loadResult();
    }


	// Returns company name for given id
    function sefHPGetCompanyName($corpId) {
        global $database;
        
        $database->setQuery("SELECT `name` FROM `#__hp_companies` WHERE `id` = $corpId");
        return $database->loadResult();
    }


	// Returns path to given property id (type/name)
    function sefHPGetPropertyPath($propId) {
        global $database;
        
        //Modified to include the property name and location in the url
        $database->setQuery("SELECT `name`, `suburb` FROM `#__hp_properties` WHERE `id` = $propId");
        $row = $database->loadRow();
        $name = $row[0];
        $suburb = $row[1];
        
        return array($suburb, $name);
    }
    
    /* Original method
     function sefHPGetPropertyPath($propId) {
        global $database;
        
        $database->setQuery("SELECT `name`, `type` FROM `#__hp_properties` WHERE `id` = $propId");
        $row = $database->loadRow();
        $name = $row[0];
        $type = $this->sefHPGetTypeName($row[1]);
        
        return array($type, $name);
    }
    */
}
?>