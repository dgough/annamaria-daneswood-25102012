<?php
/**
 * SEF module for Joomla!
 *
 * @author      $Author: michal $
 * @copyright   ARTIO s.r.o., http://www.artio.cz
 * @package     JoomSEF
 * @version     $Name$, ($Revision: 4994 $, $Date: 2005-11-03 20:50:05 +0100 (??t, 03 XI 2005) $)
 */

// Security check to ensure this file is being included by a parent file.
if (!defined('_VALID_MOS')) die('Direct Access to this location is not allowed.');

class sefext_com_frontpage extends sef_joomsef
{

    function create($string, &$vars)
    {
        extract($vars);
        global $database;

        // check first publish mainmenu link
        $query = "SELECT `link` FROM #__menu WHERE `menutype` = 'mainmenu' AND `published` > 0 AND parent = 0 ORDER BY ordering LIMIT 1";
        $database->setQuery($query);
        $menulink = $database->loadResult();
        
        $title = array();
        
        // frontpage is not default
        if (strpos($menulink, 'com_frontpage') === false) $title[] = defined('_FP_SEF_HOME') ? _FP_SEF_HOME : 'home';
        
        $string = sef_joomsef::sefGetLocation($string, $title, @$task, @$limit, @$limitstart, @$lang);
        
        return $string;
    }

}
?>