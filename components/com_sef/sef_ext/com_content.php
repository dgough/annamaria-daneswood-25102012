<?php
/**
 * SEF module for Joomla!
 *
 * @author      $Author: michal $
 * @copyright   ARTIO s.r.o., http://www.artio.cz
 * @package     JoomSEF
 * @version     $Name$, ($Revision: 4994 $, $Date: 2005-11-03 20:50:05 +0100 (??t, 03 XI 2005) $)
 */

// Security check to ensure this file is being included by a parent file.
if (!defined('_VALID_MOS')) die('Direct Access to this location is not allowed.');

class sefext_com_content extends sef_joomsef
{
    function beforeCreate($string, &$vars) {
        $params =& SEFTools::GetExtParams('com_content');

        // Set the Ignore source to No if multiple categories or sections selected
        if( isset($vars['task']) && (($vars['task'] == 'blogcategory') || ($vars['task'] == 'blogsection')) && isset($vars['id']) && ($vars['id'] == 0) ) {
            $this->_oldIgnore = $params->get('ignoreSource', 2);
            $params->set('ignoreSource', 0);
        }
        
        // Remove the limitstart and limit variables if they point to the first page
        if( isset($vars['limitstart']) && ($vars['limitstart'] == '0') ) {
            $string = SEFTools::RemoveVariable($string, 'limitstart');
            $string = SEFTools::RemoveVariable($string, 'limit');
        }

        // Try to guess the correct Itemid if set to
        if( $params->get('guessId', '0') != '0' ) {
            if( isset($vars['Itemid']) && isset($vars['id']) ) {
                global $mainframe;
                $string = SEFTools::RemoveVariable($string, 'Itemid');
                $i = $mainframe->getItemid($vars['id']);
                $string .= '&Itemid='.$i;
            }
        }

        return $string;
    }
    
    function afterCreate($string) {
        if( isset($this->_oldIgnore) ) {
            $params =& SEFTools::getExtParams('com_content');
            $params->set('ignoreSource', $this->_oldIgnore);
        }
        
        return $string;
    }

    function GoogleNews($title, $id) {
        global $database;
        $params =& SEFTools::GetExtParams('com_content');

        $num = '';
        $add = $params->get('googlenewsnum', '0');

        if( $add == '1' ) {
            // Article ID
            $digits = trim($params->get('digits', '3'));
            if( !is_numeric($digits) ) {
                $digits = '3';
            }

            $num = sprintf('%0'.$digits.'d', $id);
        }
        else if( $add == '2' ) {
            // Publish date
            $query = "SELECT `publish_up` FROM `#__content` WHERE `id` = '$id'";
            $database->setQuery($query);
            $time = $database->loadResult();

            $time = strtotime($time);

            $date = $params->get('dateformat', 'ddmm');

            $search = array( 'dd', 'd', 'mm', 'm', 'yyyy', 'yy' );
            $replace = array( date('d', $time),
            date('j', $time),
            date('m', $time),
            date('n', $time),
            date('Y', $time),
            date('y', $time) );
            $num = str_replace($search, $replace, $date);
        }

        if( !empty($num) ) {
            global $sefConfig;
            $sep = $sefConfig->replacement;

            $where = $params->get('numberpos', '1');

            if( $where == '1' ) {
                $title = $title.$sep.$num;
            } else {
                $title = $num.$sep.$title;
            }
        }

        return $title;
    }

    function create($string, &$vars) {
        global $sefConfig;

        $params =& SEFTools::GetExtParams('com_content');

        extract($vars);

        // Set title.
        $title = array();

        switch (@$task) {
            case 'new': {
                /*
                $title[] = getMenuTitle($option, $task, $Itemid, $string);
                $title[] = 'new' . $sefConfig->suffix;
                */
                break;
            }
            case 'archivecategory':
            case 'archivesection': {
                if (eregi($task.".*id=".$id, $_SERVER['REQUEST_URI'])) break;
            }
            default: {
                if( isset($do_pdf) && ($do_pdf == 1) ) {
                    // Create PDF
                    $title = sef_joomsef::getContentTitles('view', $id);
                    if (count($title) === 0) $title[] = getMenuTitle(@$option, @$task, @$Itemid);

                    $title[] = _CMN_PDF;
                } else {
                    $title = sef_joomsef::getContentTitles($task, $id);
                    if (count($title) === 0) $title[] = getMenuTitle(@$option, @$task, @$Itemid);

                    // Add Google News number if set to
                    if( (@$task == 'view') && ($params->get('googlenewsnum', '0') != '0') ) {
                        $i = count($title) - 1;
                        $title[$i] = $this->GoogleNews($title[$i], $id);
                    }

                    if ((@$task == 'view') && isset($sefConfig->suffix)) {
                        $title[count($title) - 1] .= $sefConfig->suffix;
                    }
                    else if( $sefConfig->contentUseIndex ) {
                        $title[] = '/';
                    }

                    if( isset($limitstart) && (!$sefConfig->appendNonSef || ($params->get('pagination', '0') == '0')) ) {
                        $title[count($title) - 1] .= '-' . ($limitstart+1);
                    }

                    if( isset($pop) && ($pop == 1) ) {
                        // Print article
                        $title[] = _CMN_PRINT. (isset($page) ? '-'.($page+1) : '');
                    }
                }
            }
        }
        if (count($title) > 0) {
            $nonSefVars = array();
            if( $sefConfig->appendNonSef && ($params->get('pagination', '0') != '0') ) {
                if( isset($limit) )         $nonSefVars['limit'] = $limit;
                if( isset($limitstart) )    $nonSefVars['limitstart'] = $limitstart;
            }
            
            $string = sef_joomsef::sefGetLocation($string, $title, null, null, null, @$lang, $nonSefVars);
        }

        return $string;
    }
}
?>