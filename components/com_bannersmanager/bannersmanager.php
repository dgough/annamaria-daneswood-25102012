<?php
/**
* @version $Id: banners.php 4542 2006-08-15 13:49:12Z predator $
* @package Joomla
* @subpackage Banners
* @copyright Copyright (C) 2005 Open Source Matters. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );

$bid = intval( mosGetParam( $_REQUEST, 'bid', 0 ) );

switch($task) {
	case 'click':
		clickbanner( $bid );
		break;

	default:
		break;
}

/**
/* Function to redirect the clicks to the correct url and add 1 click
*/
function clickbanner( $bid ) {
	global $database, $mainframe;

	require_once( $mainframe->getPath( 'class' ) );

	$row = new bannersManagerBanner($database);
	$row->load((int)$bid);
	$row->clicks();

	$pat = "http.*://";
	if (!eregi( $pat, $row->clickurl )) {
		$clickurl = "http://$row->clickurl";
	} else {
		$clickurl = $row->clickurl;
	}
	mosRedirect( $clickurl );
}
?>