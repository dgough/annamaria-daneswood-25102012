<?php
//
// Copyright (C) 2006 Thomas Papin
// http://www.gnu.org/copyleft/gpl.html GNU/GPL

// This file is part of the AdsManager Component,
// a Joomla! Classifieds Component by Thomas Papin
// Email: thomas.papin@free.fr
//
// Dont allow direct linking
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

//General
DEFINE( "BANNERSMANAGER_BANNERS_MANAGER", "Gestion des Banni�res");
DEFINE( "BANNERSMANAGER_GROUPS_MANAGER", "Gestion des Groupes");


DEFINE( "BANNERSMANAGER_BANNER_NAME", "Nom :");
DEFINE( "BANNERSMANAGER_IMPRESSION_MADE", "Nombre d'affichages");
DEFINE( "BANNERSMANAGER_IMPRESSION_LEFT", "Nombre d'affichages restantes");
DEFINE( "BANNERSMANAGER_CLICKS", "Clics");
DEFINE( "BANNERSMANAGER_PERCENT_CLICKS", "% Clics");

DEFINE( "BANNERSMANAGER_EDIT_BANNER","Edition");
DEFINE( "BANNERSMANAGER_PROVIDE_BANNER_NAME","Vous devez donner un nom � la banni�re.");
DEFINE( "BANNERSMANAGER_PROVIDE_URL","Vous devez remplir le champ URL.");
DEFINE( "BANNERSMANAGER_PROVIDE_FILE","Vous devez indiquer un nom de fichier.");
DEFINE( "BANNERSMANAGER_PROVIDE_CUSTOM_CODE","Vous devez entrer un code specifique pour la banni�re.");

DEFINE( "BANNERSMANAGER_BANNER_TITLE","Banni�re:");
DEFINE( "BANNERSMANAGER_BANNER_DETAILS","D�tails");
DEFINE( "BANNERSMANAGER_GROUP","Groupe");
DEFINE( "BANNERSMANAGER_GROUP_TITLE","Groupe: ");
DEFINE( "BANNERSMANAGER_IMPRESSIONS_PURCHASED","Nombre maximum d'affichages:");
DEFINE( "BANNERSMANAGER_CLICKS_PURCHASED","Nombre maximum de clics:");
DEFINE( "BANNERSMANAGER_UNLIMITED","Illimit�");
DEFINE( "BANNERSMANAGER_SHOW_BANNER","Publi�: ");
DEFINE( "BANNERSMANAGER_BANNER_TYPE","Type de banni�re");
DEFINE( "BANNERSMANAGER_CUSTOM_BANNER_CODE","Code Specifique");
DEFINE( "BANNERSMANAGER_CLICK_URL","Adresse URL: ");
DEFINE( "BANNERSMANAGER_FILE_URL","Fichier Image/Flash: ");
DEFINE( "BANNERSMANAGER_RESTRICTION_PAGES","Restriction");
DEFINE( "BANNERSMANAGER_MENU_ITEMS_SELECTION","Selection les items Menu sur lequel la banni�re peut s'afficher");
DEFINE( "BANNERSMANAGER_GROUP_DETAILS","D�tails");
DEFINE( "BANNERSMANAGER_GROUP_NAME","Nom: ");

DEFINE( "BANNERSMANAGER_GROUP_ID","Groupe ID");
DEFINE( "BANNERSMANAGER_MODULE","Module");

DEFINE( "BANNERSMANAGER_CANNOT_DELETE_GROUP","Le Groupe ne peut pas �tre effac�, il contient des banni�res");
DEFINE( "BANNERSMANAGER_CREATE_GROUP_FIRST","Pour devez cr�er un groupe pour ajouter une banni�re");
DEFINE( "BANNERSMANAGER_IMAGE","Image");
DEFINE( "BANNERSMANAGER_CUSTOM","Code Personnalis�");

DEFINE( "BANNERSMANAGER_ALL_GROUPS","- Tout les groupes - ");
DEFINE( "BANNERSMANAGER_REORDER","Trier");
DEFINE( "BANNERSMANAGER_ORDER","Ordre");

DEFINE( "BANNERSMANAGER_BANNERS","Banni�res");
DEFINE( "BANNERSMANAGER_GROUPS","Groupes");
DEFINE( "BANNERSMANAGER_IMPORT_JOOMLA_BANNERS","Importer les Banni�res de Joomla");

DEFINE( "BANNERSMANAGER_CANNOT_DELETE_CLIENT","Le client ne peut pas �tre effac�, il contient des banni�res");
DEFINE( "BANNERSMANAGER_CLIENTS_MANAGER","Gestion des Clients");
DEFINE( "BANNERSMANAGER_CLIENT_NAME","Nom du Client");
DEFINE( "BANNERSMANAGER_CLIENT_NB_BANNERS","Nombre de Banni�res");
DEFINE( "BANNERSMANAGER_CLIENT_TITLE","Client");
DEFINE( "BANNERSMANAGER_CLIENT_DETAILS","Details");
DEFINE( "BANNERSMANAGER_EMAIL","Email");
DEFINE( "BANNERSMANAGER_CONTACT","Contact");
DEFINE( "BANNERSMANAGER_DESCRIPTION","Description");
DEFINE( "BANNERSMANAGER_CREATE_CLIENT_FIRST","Pour devez cr�er un client pour ajouter une banni�re");
DEFINE( "BANNERSMANAGER_CLIENT","Client");
DEFINE( "BANNERSMANAGER_CLIENTS","Clients");
DEFINE ("BANNERSMANAGER_ALL_CLIENTS","- Tout les Clients -");
?>
