<?php
//
// Copyright (C) 2006 Thomas Papin
// http://www.gnu.org/copyleft/gpl.html GNU/GPL

// This file is part of the BannersManager Component,
// Email: thomas.papin@free.fr
//

// Dont allow direct linking
defined( '_VALID_MOS' ) or die( 'Restricted access' );

class bannersManagerBanner extends mosDBTable {
	var $id	= null;
	var $name = null;
	var $groupid = null;
	var $clientid = null;
	var $type = 0;
	var $imageurl = null;
	var $clickurl = null;
	var $custombannercode = null;
	var $impmade = null;
	var $implimit = null;
	var $clicks = null;
	var $clickslimit = null;
	var $width = null;
	var $height = null;
	var $border = null;
	var $wmode = null;
	var $play = null;
	var $menu = null;
	var $loop = null;
	var $bgcolor = null;
	var $menuid = null;
	var $ordering = null;
	var $published = 1;
	var $target = '_blank';

	function bannersManagerBanner( &$db ) {
		$this->mosDBTable( '#__bannersmanager_banner', 'id', $db );
	}

	function clicks() {
		$this->clicks++;
		if (($this->clickslimit > 0) && ($this->clicks >= $this->clickslimit))
		{
			$query = "UPDATE #__bannersmanager_banner"
			. "\n SET published = 0"
			. "\n WHERE id = $this->id"
			;
			$this->_db->setQuery( $query );
			$this->_db->query();
		}
		
		$query = "UPDATE #__bannersmanager_banner"
		. "\n SET clicks = ( clicks + 1 )"
		. "\n WHERE id = $this->id"
		;
		$this->_db->setQuery( $query );
		$this->_db->query();
	}
}

class bannersManagerGroup extends mosDBTable {
	var $id	= null;
	var $name = null;

	function bannersManagerGroup( &$db ) {
		$this->mosDBTable( '#__bannersmanager_group', 'id', $db );
	}
}

class bannersManagerClient extends mosDBTable {
	var $id	= null;
	var $name = null;
	var $contact = null;
	var $email = null;
	var $description = null;

	function bannersManagerClient( &$db ) {
		$this->mosDBTable( '#__bannersmanager_client', 'id', $db );
	}
}
?>