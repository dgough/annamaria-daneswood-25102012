<?php
/**
* @version $Id: contact.html.php 6085 2006-12-24 18:59:57Z robs $
* @package Joomla
* @subpackage Contact
* @copyright Copyright (C) 2005 Open Source Matters. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );


/**
* @package Joomla
* @subpackage Contact
*/
class HTML_contact {


	function displaylist( &$categories, &$rows, $catid, $currentcat=NULL, &$params, $tabclass ) {
		global $Itemid, $mosConfig_live_site, $hide_js;

		if ( $params->get( 'page_title' ) ) {
			?>
			<h1 class="componentheading<?php echo $params->get( 'pageclass_sfx' ); ?>"><?php echo $currentcat->header; ?></h1>
			<?php
		}
		?>
		<form action="index.php" method="post" name="adminForm">

		<div class="contentdescription<?php echo $params->get( 'pageclass_sfx' ); ?>">
		<div>
			<?php
			// show image
			if ( $currentcat->img ) {
				?>
				<img src="<?php echo $currentcat->img; ?>" align="<?php echo $currentcat->align; ?>" hspace="6" alt="<?php echo _WEBLINKS_TITLE; ?>" />
				<?php
			}
			echo $currentcat->descrip;
			?>
		</div>
		<div>
			<?php
			if ( count( $rows ) ) {
				HTML_contact::showTable( $params, $rows, $catid, $tabclass );
			}
			?>
		</div>
		<br />
		<div>
			<?php
			// Displays listing of Categories
			if ( ( $params->get( 'type' ) == 'category' ) && $params->get( 'other_cat' ) ) {
				HTML_contact::showCategories( $params, $categories, $catid );
			} else if ( ( $params->get( 'type' ) == 'section' ) && $params->get( 'other_cat_section' ) ) {
				HTML_contact::showCategories( $params, $categories, $catid );
			}
			?>
		</div>
		</div>
		</form>
		<?php
		// displays back button
		mosHTML::BackButton ( $params, $hide_js );
	}

	/**
	* Display Table of items
	*/
	function showTable( &$params, &$rows, $catid, $tabclass ) {
		global $mosConfig_live_site, $Itemid;
		?>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<?php
		if ( $params->get( 'headings' ) ) {
			?>
			<tr>
				<td class="sectiontableheader<?php echo $params->get( 'pageclass_sfx' ); ?>">
					<?php echo _CONTACT_HEADER_NAME; ?>
				</td>
				<?php
				if ( $params->get( 'position' ) ) {
					?>
					<td class="sectiontableheader<?php echo $params->get( 'pageclass_sfx' ); ?>">
						<?php echo _CONTACT_HEADER_POS; ?>
					</td>
					<?php
				}
				?>
				<?php
				if ( $params->get( 'email' ) ) {
					?>
					<td class="sectiontableheader<?php echo $params->get( 'pageclass_sfx' ); ?>">
						<?php echo _CONTACT_HEADER_EMAIL; ?>
					</td>
					<?php
				}
				?>
				<?php
				if ( $params->get( 'telephone' ) ) {
					?>
					<td class="sectiontableheader<?php echo $params->get( 'pageclass_sfx' ); ?>">
						<?php echo _CONTACT_HEADER_PHONE; ?>
					</td>
					<?php
				}
				?>
				<?php
				if ( $params->get( 'fax' ) ) {
					?>
					<td class="sectiontableheader<?php echo $params->get( 'pageclass_sfx' ); ?>">
						<?php echo _CONTACT_HEADER_FAX; ?>
					</td>
					<?php
				}
				?>
			</tr>
			<?php
		}

		$k = 0;
		foreach ($rows as $row) {
			$link = 'index.php?option=com_contact&amp;task=view&amp;contact_id='. $row->id .'&amp;Itemid='. $Itemid;
			?>
			<tr>
				<td class="<?php echo $tabclass[$k]; ?>" style="width: 20%">
					<a href="<?php echo sefRelToAbs( $link ); ?>" class="category<?php echo $params->get( 'pageclass_sfx' ); ?>">
						<?php echo $row->name; ?></a>
				</td>
				<?php
				if ( $params->get( 'position' ) ) {
					?>
					<td style="width: 25%" class="<?php echo $tabclass[$k]; ?>">
						<?php echo $row->con_position; ?>
					</td>
					<?php
				}
				?>
				<?php
				if ( $params->get( 'email' ) ) {
					if ( $row->email_to ) {
						$row->email_to = mosHTML::emailCloaking( $row->email_to, 1 );
					}
					?>
					<td  style="width: 20%" class="<?php echo $tabclass[$k]; ?>">
						<?php echo $row->email_to; ?>
					</td>
					<?php
				}
				?>
				<?php
				if ( $params->get( 'telephone' ) ) {
					?>
					<td style="width: 15%" class="<?php echo $tabclass[$k]; ?>">
						<?php echo $row->telephone; ?>
					</td>
					<?php
				}
				?>
				<?php
				if ( $params->get( 'fax' ) ) {
					?>
					<td style="width: 15%" class="<?php echo $tabclass[$k]; ?>">
						<?php echo $row->fax; ?>
					</td>
					<?php
				}
				?>
			</tr>
			<?php
			$k = 1 - $k;
		}
		?>
		</table>
		<?php
	}

	/**
	* Display links to categories
	*/
	function showCategories( &$params, &$categories, $catid ) {
		global $mosConfig_live_site, $Itemid;
		?>
		<ul>
		<?php
		foreach ( $categories as $cat ) {
			if ( $catid == $cat->catid ) {
				?>
				<li>
					<strong>
					<?php echo $cat->title;?>
					</strong>
					&nbsp;
					<span class="small<?php echo $params->get( 'pageclass_sfx' ); ?>">
					(<?php echo $cat->numlinks;?>)
					</span>
				</li>
				<?php
			} else {
				$link = 'index.php?option=com_contact&amp;catid='. $cat->catid .'&amp;Itemid='. $Itemid;
				?>
				<li>
					<a href="<?php echo sefRelToAbs( $link ); ?>" class="category<?php echo $params->get( 'pageclass_sfx' ); ?>">
						<?php echo $cat->title;?></a>
					<?php
					if ( $params->get( 'cat_items' ) ) {
						?>
						&nbsp;
						<span class="small<?php echo $params->get( 'pageclass_sfx' ); ?>">
							(<?php echo $cat->numlinks;?>)
						</span>
						<?php
					}
					?>
					<?php
					// Writes Category Description
					if ( $params->get( 'cat_description' ) ) {
						echo '<br />';
						echo $cat->description;
					}
					?>
				</li>
				<?php
			}
		}
		?>
		</ul>
		<?php
	}


	// ------------------------ A8E fix ------------------------
	function viewcontact( &$contact, &$params, $count, &$list, &$menu_params, $validator=null ) {
		// ------------------------ A8E TODO: make a8e ------------------------
		global $mosConfig_live_site;
		global $mainframe, $Itemid;

		$template 		= $mainframe->getTemplate();
		$sitename 		= $mainframe->getCfg( 'sitename' );
		$hide_js 		= intval( mosGetParam($_REQUEST,'hide_js', 0 ) );
				?>
		<script language="JavaScript" type="text/javascript">
		//<!--
		function validate(){
			if ( ( document.emailForm.text.value == "" ) || ( document.emailForm.email.value.search("@") == -1 ) || ( document.emailForm.email.value.search("[.*]" ) == -1 ) ) {
				alert( "<?php echo _CONTACT_FORM_NC; ?>" );
			} else if ( ( document.emailForm.email.value.search(";") != -1 ) || ( document.emailForm.email.value.search(",") != -1 ) || ( document.emailForm.email.value.search(" ") != -1 ) ) {
				alert( "<?php echo _CONTACT_ONE_EMAIL; ?>" );			
			} else {
				document.emailForm.action = "<?php echo sefRelToAbs("index.php?option=com_contact&Itemid=$Itemid"); ?>"
				document.emailForm.submit();
			}
		}
		//-->
		</script>
		<script type="text/javascript">
		//<!--
		function ViewCrossReference( selSelectObject ){
			var links = new Array();
			<?php
			$n = count( $list );
			for ($i = 0; $i < $n; $i++) {
				echo "\nlinks[".$list[$i]->value."]='"
					. sefRelToAbs( 'index.php?option=com_contact&task=view&contact_id='. $list[$i]->value .'&Itemid='. $Itemid )
					. "';";
			}
			?>

			var sel = selSelectObject.options[selSelectObject.selectedIndex].value
			if (sel != "") {
				location.href = links[sel];
			}
		}
		//-->
		</script>
		<?php
		// For the pop window opened for print preview
		if ( $params->get( 'popup' ) ) {
			$mainframe->setPageTitle( $contact->name );
			$mainframe->addCustomHeadTag( '<link rel="stylesheet" href="templates/'. $template .'/css/template_css.css" type="text/css" />' );
		}
		if ( $menu_params->get( 'page_title' ) ) {
			?>
			<h1 class="componentheading<?php echo $params->get( 'pageclass_sfx' ); ?>"><?php echo $menu_params->get( 'header' ); ?></h1>
			<?php
		}
		?>

		<div class="contentpane<?php echo $menu_params->get( 'pageclass_sfx' ); ?>">
		<?php
		// displays Page Title
		HTML_contact::_writePageTitle( $params, $menu_params );
		
		// displays Image
		HTML_contact::_writeImage( $contact, $params );		

		// displays Contact Select box
		HTML_contact::_writeSelectContact( $contact, $params, $count );

		// displays Name & Positione
		HTML_contact::_writeContactName( $contact, $params, $menu_params );

		// displays Address
		HTML_contact::_writeContactAddress( $contact, $params );

		// displays Email & Telephone
		HTML_contact::_writeContactContact( $contact, $params );

		// displays Misc Info
		HTML_contact::_writeContactMisc( $contact, $params );

		// displays Email Form
		HTML_contact::_writeVcard( $contact, $params );
		// displays Email Form
		// ------------------------ A8E fix ------------------------
		HTML_contact::_writeEmailForm( $contact, $params, $sitename, $menu_params, $validator );

		// display Close button in pop-up window
		mosHTML::CloseButton ( $params, $hide_js );

		// displays back button
		mosHTML::BackButton ( $params, $hide_js );
		?>
		</div>
		<?php
	}


	/**
	* Writes Page Title
	*/
	function _writePageTitle( &$params, &$menu_params ) {
		if ( $params->get( 'page_title' )  && !$params->get( 'popup' ) ) {
			?>
			<h1 class="componentheading<?php echo $params->get( 'pageclass_sfx' ); ?>"><?php echo $params->get( 'header' ); ?></h1>
			<?php
		}
	}

	/**
	* Writes Dropdown box to select contact
	*/
	function _writeSelectContact( &$contact, &$params, $count ) {
		if ( ( $count > 1 )  && !$params->get( 'popup' ) && $params->get( 'drop_down' ) ) {
			global $Itemid;
			?>
			<div>
					<form action="<?php echo sefRelToAbs( 'index.php?option=com_contact&amp;Itemid='. $Itemid ); ?>" method="post" name="selectForm" target="_top" id="selectForm">
						<?php echo (_CONTACT_SEL); ?>
						<br />
						<?php echo $contact->select; ?>
						<br /><br />
					</form>
			</div>
			<?php
		}
	}

	/**
	* Writes Name & Position
	*/
	function _writeContactName( &$contact, &$params, &$menu_params ) {
		global $Itemid, $hide_js, $mosConfig_live_site;
		
		$width = $params->get( 'column_width', 20 )."px";
		
		if ( $contact->name ||  $contact->con_position ) {
			if ( $contact->name && $params->get( 'name' ) ) {
				?>
				<div class="row">
					<div class="icons">
					<?php
					// displays Print Icon
					$print_link = $mosConfig_live_site .'/index2.php?option=com_contact&amp;task=view&amp;contact_id='. $contact->id .'&amp;Itemid='. $Itemid .'&amp;pop=1';
					mosHTML::PrintIcon( $contact, $params, $hide_js, $print_link );
					?>
					</div>
	
					<div class="contact_name" style="margin-left: <?php echo $width ?>; font-weight: bold; font-size: 1.2em">
					<?php echo $contact->name ?>
					</div>
				</div>
				<?php
			}
			if ( $contact->con_position && $params->get( 'position' ) ) {
				?>
				<div class="row">
					<div class="contact_position" style="margin-left: <?php echo $width ?>">
						<?php
						echo $contact->con_position;
						?>
					</div>
				</div>			
				<?php
			}
		}
	}

	/*
	* Writes Image
	*/
	function _writeImage( &$contact, &$params ) {
		global $mosConfig_live_site;

		if ( $contact->image && $params->get( 'image' ) ) {
			?>
			<div style="float: right;">
			<img src="<?php echo $mosConfig_live_site;?>/images/stories/<?php echo $contact->image; ?>" align="middle" alt="<?php echo _CONTACT_TITLE; ?>" />
			</div>
			<?php
		}
	}

	/**
	* Writes Address
	*/
	function _writeContactAddress( &$contact, &$params ) {
		if ( ( $params->get( 'address_check' ) > 0 ) &&  ( $contact->address || $contact->suburb  || $contact->state || $contact->country || $contact->postcode ) ) {
			
			$width = $params->get( 'column_width', 20 )."px";
			?>
			<div class="row contact_address_wrap">
				<?php
				if ( $params->get( 'address_check' ) > 0 ) {
					?>
					<div class="contact_icon" style="width: <?php echo $width ?>; float: left">
						
						<?php
						echo $params->get( 'marker_address' );
						?>
						
					</div>
					<?php
				}
				?>
				<div class="contact_address"  style="margin-left: <?php echo $width ?>">
				<?php
				if ( $contact->address && $params->get( 'street_address' ) ) {
					?>
					<div class="contact_address_street">
						<?php
						echo $contact->address;
						?>
					</div>
					<?php
				}
				if ( $contact->suburb && $params->get( 'suburb' ) ) {
					?>
					<div class="contact_address_suburb">
						<?php
						echo $contact->suburb;
						?>
					</div>
					<?php
				}
				if ( $contact->state && $params->get( 'state' ) ) {
					?>
					<div class="contact_address_state">
						<?php
						echo $contact->state;
						?>
					</div>
					<?php
				}
				if ( $contact->country && $params->get( 'country' ) ) {
					?>
					<div class="contact_address_country">
						<?php
						echo $contact->country;
						?>
					</div>
					<?php
				}
				if ( $contact->postcode && $params->get( 'postcode' ) ) {
					?>
					<div class="contact_address_postcode">
						<?php
						echo $contact->postcode;
						?>
					</div>
					<?php
				}
				?>
				</div>
			</div>
			<?php
		}
	}

	/**
	* Writes Contact Info
	*/
	function _writeContactContact( &$contact, &$params ) {
		$width = $params->get( 'column_width', 20 )."px";
		
		if ( $contact->email_to || $contact->telephone  || $contact->fax ) {
			?>
			<div class="contact_phone_email_fax">
			<?php

			if ( $contact->email_to && $params->get( 'email' ) ) {
				?>
				<div class='row' style='clear: left; padding: 5px 0px;'>
					<div class="contact_icon" style="float: left; width: <?php echo $width ?>">
						<?php
						echo $params->get( 'marker_email' );
						?>
					</div>
					<div class="contact_email" style='margin-left: <?php echo $width ?>'>
						<?php
						echo $contact->email;
						?>
					</div>
				</div>
				<?php
			}
			if ( $contact->telephone && $params->get( 'telephone' ) ) {
				?>
				<div class='row' style='clear: left; padding: 5px 0px;'>
					<div class="contact_icon" style="float: left; width: <?php echo $width ?>">
						<?php
						echo $params->get( 'marker_telephone' );
						?>
					</div>
					<div class="contact_phone" style='margin-left: <?php echo $width ?>'>
						<?php
						echo $contact->telephone;
						?>
					</div>
				</div>
				<?php
			}
			if ( $contact->fax && $params->get( 'fax' ) ) {
				?>
				<div class='row' style='clear: left; padding: 5px 0px;'>
					<div class="contact_icon" style="float: left; width: <?php echo $width ?>">
						<?php
						echo $params->get( 'marker_fax' );
						?>
					</div>
					<div class="contact_fax" style='margin-left: <?php echo $width ?>'>
						<?php
						echo $contact->fax;
						?>
					</div>
				</div>
				<?php
			}
			?>
			</div>
			<?php
		}
	}

	/**
	* Writes Misc Info
	*/
	function _writeContactMisc( &$contact, &$params ) {
		
		$width = $params->get( 'column_width', 20 )."px";
		
		if ( $contact->misc && $params->get( 'misc' ) ) {
			?>
			<div class='row' style='clear: left; padding: 5px 0px;'>
				<div class="contact_misc_wrap">
					<div class="contact_icon" style="float: left; width: <?php echo $width ?>">
					<?php
					echo $params->get( 'marker_misc' );
					?>
					</div>
					<div class="contact_misc" style='margin-left: <?php echo $width ?>'>
					<?php
					echo $contact->misc;
					?>
					</div>
				</div>
			</div>
			<?php
		}
	}

	/**
	* Writes Email form
	*/
	function _writeVcard( &$contact, &$params ) {
		$width = $params->get( 'column_width', 20 )."px";
		
		if ( $params->get( 'vcard' ) ) {
			?>
			<div class="contact_vcard" style="margin-left: <?php echo $width ?>">
				<?php echo(_CONTACT_DOWNLOAD_AS);?>
				<a href="index2.php?option=com_contact&amp;task=vcard&amp;contact_id=<?php echo $contact->id; ?>&amp;no_html=1">
				<?php echo(_VCARD);?>
				</a>
			</div>
			<?php
		}
	}

	// ------------------------ A8E fixes start ------------------------
	/**
	* Writes Email form
	*/
	function _writeEmailForm( &$contact, &$params, $sitename, &$menu_params, $validator=null ) {
		global $Itemid;
		$width = $params->get( 'column_width', 20 )."px";
		
		if ( $contact->email_to && !$params->get( 'popup' ) && $params->get( 'email_form' ) ) {
			// used for spoof hardening
			$validate = josSpoofValue();
			?>
			<div style="margin-left: <?php echo $width ?>" >
				<br />
				<?php echo $params->get( 'email_description' ) ?>
				<br /><br />
				<form action="<?php echo sefRelToAbs( 'index.php?option=com_contact&amp;Itemid='. $Itemid ); ?>" method="post" name="emailForm" target="_top" id="emailForm">
				<?php
				$name		= trim(strval(mosGetParam( $_POST, 'name', '' )));
				$email		= trim(strval(mosGetParam( $_POST, 'email', '' )));
				$subject	= trim(strval(mosGetParam( $_POST, 'subject', '')));
				$text		= trim(strval(mosGetParam( $_POST, 'text', '')));
				
		
				if( $validator != null && !$validator->isValid()) { 
					$labels = array();
					$labels["email"] = _EMAIL_PROMPT;
					$labels["subject"] = _SUBJECT_PROMPT;
					$labels["text"] = _MESSAGE_PROMPT;
					?>
					<div class="error">
						<span class="invisible">
							<?php
							// show the errors
							$errors = $validator->getErrors();
							$errorKeys =  array_keys($errors);
							foreach($errorKeys as $key) {
								$errorMessages = $errors[$key];
								foreach($errorMessages as $errorMessage) {
									echo(sprintf($errorMessage, $labels[$key])."<br/>");
								}
							}
							?>
						</span>
					</div>
					<?php // missing open php tag - fix by snoboJohan (17 Jan 2007)
				}
				?>
				<div class="contact_email_form<?php echo $menu_params->get( 'pageclass_sfx' ); ?>">
					<div class='row'>
						<label for="contact_name">
							<?php echo(_NAME_PROMPT);?>
						</label>
						<br />
						<input type="text" name="name" id="contact_name" size="30" class="inputbox" value="<?php echo $name; ?>" />
					</div>
					<div class='row'>
						<label for="contact_email">
							<?php echo(_EMAIL_PROMPT);?>
						</label>
						<br />
						<input type="text" name="email" id="contact_email" size="30" class="inputbox" value="<?php echo $email; ?>" />
					</div>
					<div class='row'>
						<label for="contact_subject">
							<?php echo(_SUBJECT_PROMPT);?>
						</label>
						<br />
						<input type="text" name="subject" id="contact_subject" size="30" class="inputbox" value="<?php echo $subject; ?>" />
					</div>
					<br />
					<div class='row'>
						<label for="contact_text">
							<?php echo(_MESSAGE_PROMPT);?>
						</label>
						<br />
						<textarea cols="50" rows="10" name="text" id="contact_text" class="inputbox" ><?php echo $text; ?></textarea>
					</div>
					<div class='row'>
						<?php
						if ( $params->get( 'email_copy' ) ) {
							?>
							<br />
								<input type="checkbox" name="email_copy" id="contact_email_copy" value="1"  />
								<label for="contact_email_copy">
									<?php echo(_EMAIL_A_COPY); ?>
								</label>
							<?php
						}
						?>
					</div>
					<br />
					<div class='row email_submit'>
						<input type="submit" name="send" value="<?php echo(_SEND_BUTTON); ?>" class="button" />
					</div>
				</div>
				<input type="hidden" name="option" value="com_contact" />
				<input type="hidden" name="con_id" value="<?php echo $contact->id; ?>" />
				<input type="hidden" name="sitename" value="<?php echo $sitename; ?>" />
				<input type="hidden" name="op" value="sendmail" />
				<input type="hidden" name="<?php echo $validate; ?>" value="1" />
				</form>
				<br />
			</div>
			<?php
		}
	}
	// ------------------------ A8E fixes end ------------------------


	function nocontact( &$params ) {
		?>
		<br />
		<br />
			<?php echo _CONTACT_NONE;?>
		<br />
		<br />
		<?php
		// displays back button
		mosHTML::BackButton ( $params );
	}
}
?>