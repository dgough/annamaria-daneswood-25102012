<?php
/**
 * MyBlog
 * @package MyBlog
 * @copyright (C) 2006 - 2008 by Azrul Rahim - All rights reserved!
 * @license Copyrighted Commercial Software
 **/
 
(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

// Include our custom cmslib if its not defined
if(!defined('CMSLIB_DEFINED'))
	include_once ((dirname((dirname(dirname(dirname(__FILE__)))))) . '/components/libraries/cmslib/spframework.php');

// If the memory limit is too low, increase it 
if(ini_get('memory_limit') == '8M')
	@ini_set('memory_limit', '16M');
	
class MYMedia{
	var $db		= null;
	var $cms	= null;
	
	var $media		= '';	// Full Path to the image root
	var $mediaURI	= '';	// Full URI Path to the image root
	var $cache		= '';	// Full Path to cache folder

	function MYMedia(){
		$this->cms	=& cmsInstance('CMSCore');
		$this->db	=& cmsInstance('CMSDb');
		$this->_init();
	}
	
	function _init(){
		global $_MY_CONFIG, $mainframe;
		
		$this->cms->load('libraries', 'user');

		$this->media	= $this->cms->get_path('root') . '/' . trim($_MY_CONFIG->get('imgFolderRoot'), '\\/');
		$this->mediaURI	= $this->cms->get_path('live') . '/' . trim($_MY_CONFIG->get('imgFolderRoot') , '\\/');
		
		if($_MY_CONFIG->get('imgFolderRestrict')){
			$this->media	.= '/' . $this->cms->user->id;
			
			// If folder doesn't exists create it.
			if(!file_exists($this->media)){
				mkdir($this->media);
			}
			
			$this->mediaURI	.= '/' . $this->cms->user->id;
		}
	}

	// Returns the extensions for the provided file
	function extension($filePath, $uppercase = false){
		if($uppercase)
			return strtoupper(pathinfo($filePath, PATHINFO_EXTENSION));

		return strtolower(pathinfo($filePath, PATHINFO_EXTENSION));
	}
	
	/**
	 * Return an array of contents in a specific directory
	 **/	 	
	function _getContents($directory){
		$handle	= opendir($directory);		
		if(!$handle)
			return false;

		$directoryContents	= array();
		// Get listing of files in this directory
		while(($file = readdir($handle)) !== false){
			//array_push($directoryContents, $file);
			$directoryContents[]	= $file;
		}
		closedir($handle);
		return $directoryContents;
	}
	
	/**
	 * Browse a specific folder and list the contents
	 * $folder:	 string (folder that you want to browse)  
	 **/	 	
	function _getFiles($folder, $advanced){
		global $_MY_CONFIG, $mainframe;

		$directory	= $this->media . '/' . $folder;

		// Don't waste time processing it
		if(!is_dir($directory)){
			return false;
		}

		$contents	= $this->_getContents($directory);
		$items		= array();

		// Process folders first
		foreach($contents as $content){
			
			$contentDirectory	= $this->media . '/' . $folder . '/' . $content;
			
			if(is_dir($contentDirectory)){
					$content	= $this->_processFolder($content, $folder, $advanced);
				
			
				if(is_array($content) && ($content !== false))
					$items[]	= $content;
				}
		}

		// Then we process files
		foreach($contents as $content){
			if(is_file($this->media . '/' . $folder . '/' . $content)){
// 				if($advanced)
// 					$content	= $this->_processFileAdvanced($content, $folder);
// 				else
					$content	= $this->_processFile($content, $folder);
				$items[]	= $content;
			}
		}
		
		return $items;
	}

	//
	// $folder = the folder in this folder
	// $cwd	   = the current folder
	//
	function _processFolder($folder, $cwd, $escape){
		$open	= '';
		
		if($folder == '..'){
			$image	= $this->_thumbnail['folderup'];
			
			// We need to know that this is a subfolder of current working directory
			$parent		= rtrim(dirname($cwd), '\\/');

			// Remove the media directory from the path
			$open		= str_replace($this->media, '', $parent);
		
		} else if($folder != '.' && $folder != '..'){
		
			// If folder is not '.' and not '..' we know its a folder.
			$image	= $this->_thumbnail['folder'];
			
			$open	= $cwd . '/' . $folder;
		}
		
		$items	= array();
		$eventData = '';
		if($folder != '.'){
			switch($folder){
				case $folder != '/..':
				case $folder == '/..' && !$_MY_CONFIG->get('imgFolderRestrict'):
				case $folder == '/..' && $this->cms->user->id == '62':
				$event	= 'o';//($escape) ? 'myblog.openFolder(\\\''. $open . '\\\');' : 'myblog.openFolder(\'' . $open . '\');';
				$eventData = $open;
				$items	= array(
								'i'	=> $image,
								'e'	=> $event,
								'd' => $eventData,
								't'	=> $folder,
								'c'	=> $folder
								);

				break;
			}
		} else
			return false;

		
		return $items;
	}
}
