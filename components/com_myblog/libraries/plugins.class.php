<?php
/**
 * @copyright (C) 2007 by Slashes & Dots Sdn Bhd - All rights reserved!
 * @license http://www.azrul.com Copyrighted Commercial Software
 *
 * Rem:
 * This file is the library for plugins which allows myblog to load plugins / mambots
 * that is installed on a joomla site. 
 **/
(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

// Include our custom cmslib if its not defined
if(!defined('CMSLIB_DEFINED'))
	include_once ((dirname((dirname(dirname(dirname(__FILE__)))))) . '/components/libraries/cmslib/spframework.php');

global $mainframe, $_MY_CONFIG;

if(cmsVersion() == _CMS_JOOMLA10 || cmsVersion() == _CMS_MAMBO){
	global $_MAMBOTS;
}

class MYPlugins{
	
	/** Private Properties **/
	var $_folder	= '';		// Plugins / Mambots folder
	var $_db		= null;
	var $_cms		= null;

	var $_events	= null;

	function MYPlugins(){
		$this->_cms	=& cmsInstance('CMSCore');

		// Include our custom cmslib if its not defined
		if(!defined('MYPluginsDB'))
			include_once (MY_MODEL_PATH . '/plugins.db.php');

		$this->_db	= new MYPluginsDB();

		// Set the plugins / mambots folder for this specific environment.
		$this->_folder	= $this->_cms->get_path('plugins');
	}
	
	/**
	 * Load plugins for specific event.
	 **/	 	
	function load(){
		global $mainframe;
		$plugins	= $this->_db->getPlugins();
		
		if($plugins){
			foreach($plugins as $plugin){
			    // Instead of including the mambots, we allow the mainframe mambot to include for us.
			    // so that other module that has triggers would not come up with an error.
			    $plugin->folder 		= 'content';
			    $plugin->published 	= '1';
			    $plugin->params		= null; // no params
			    
				@$_GET['task']  	= 'view';
				@$_GET['option']    = 'com_content';
	
				if(cmsVersion() == _CMS_JOOMLA10 || cmsVersion() == _CMS_MAMBO){
					global $_MAMBOTS;
					$_MAMBOTS->loadBot($plugin->folder, $plugin->element, $plugin->published, $plugin->params);

					$mambotsToInit=@$_MAMBOTS->_events['onPrepareContent'];
					if ($mambotsToInit){ 
						foreach ($mambotsToInit as $mambotToInit){
							$funcName=$mambotToInit[0];
							$this->register('onPrepareContent',$funcName,"","1");
						}
					}
					
					$mambotsToInit=@$_MAMBOTS->_events['onBeforeDisplayContent'];
					if($mambotsToInit){ 
						foreach ($mambotsToInit as $mambotToInit){
							$funcName=$mambotToInit[0];
							$this->register('onBeforeDisplayContent',$funcName,"","1");
						}
					}
					
					$mambotsToInit=@$_MAMBOTS->_events['onAfterDisplayContent'];
					if ($mambotsToInit) {
						foreach ($mambotsToInit as $mambotToInit) {
							$funcName=$mambotToInit[0];
							$this->register('onAfterDisplayContent',$funcName,"","1");
						}
					}
				}
				else
				{
					// Import plugin.
					JPluginHelper::importPlugin('content', $plugin->element);
					$plg	= JPluginHelper::getPlugin('content' , $plugin->element);
					
// 					echo '<pre>';
// 					print_r($plg);
// 					echo '</pre>';
					
					$dispatcher	=& JDispatcher::getInstance();
					$plgObj		= 'plgContent' . ucfirst($plg->name);
					
					if( class_exists( $plgObj ) )
					{
						
						$instance = new $plgObj($dispatcher , (array) $plg);
						
						if( method_exists($instance , 'onPrepareContent') )
						{
							$this->register( 'onPrepareContent' , $plgObj , $plugin->params , $plugin->published );
						}
						
						if( method_exists($instance , 'onBeforeDisplayContent') )
						{
							$this->register( 'onBeforeDisplayContent' , $plgObj , $plugin->params , $plugin->published );
						}

						if( method_exists($instance , 'onAfterDisplayContent') )
						{
							$this->register( 'onAfterDisplayContent' , $plgObj , $plugin->params , $plugin->published );
						}
					}
					else 
					{
						//echo 'this is function';
//  						echo '<pre>';
//  						print_r($plg);
//  						echo '</pre>';
						foreach($dispatcher->_observers as $observer)
						{
							if( is_array($observer) )
							{
								if($observer['event'] == 'onPrepareContent')
								{
									$this->register('onPrepareContent', $observer['handler'], $plugin->params, $plugin->published);
								}
	
								if($observer['event'] == 'onBeforeDisplayContent')
								{
									$this->register('onBeforeDisplayContent', $observer['handler'], $plugin->params, $plugin->published);
								}
	
								if($observer['event'] == 'onAfterDisplayContent')
								{
									$this->register('onAfterDisplayContent', $observer['handler'], $plugin->params, $plugin->published);
								}
							}
						}
					}
					
// 					echo '<pre>';
// 					print_r($plugins);
// 					echo '</pre>';
// 					$dispatcher	=& JDispatcher::getInstance();
// 
// 					foreach($dispatcher->_observers as $observer){
// 						
// 						if(is_array($observer))
// 						{
// 							//echo $observer['event'];
// 							if($observer['event'] == 'onPrepareContent'){
// 								$this->register('onPrepareContent', $observer['handler'], $plugin->params, $plugin->published);
// 							}
// 
// 							if($observer['event'] == 'onBeforeDisplayContent'){
// 								$this->register('onBeforeDisplayContent', $observer['handler'], $plugin->params, $plugin->published);
// 							}
// 
// 							if($observer['event'] == 'onAfterDisplayContent'){
// 								$this->register('onAfterDisplayContent', $observer['handler'], $plugin->params, $plugin->published);
// 							}
// 						}
// 						else if(is_object($observer))
// 						{
// // 							echo '<pre>';
// // 							print_r($observer);
// // 							echo '</pre>';
// 							//var_dump(get_class($observer));
// 							// Classes type of plugins														
// 							if($observer->_type == 'content'){
// 								echo 'debug';
// //								echo get_class($observer);
// 								$this->register('onPrepareContent', $observer, $plugin->params, $plugin->published);
// 							}
// 							
// 
// 
// 						}
// 					}
				}
			}// End for loop
		}// End if
	}// End function

	/**
	 * Register specific plugin in our events list
	 **/	 	
	function register($event, $handler, $params, $published = 1){
		
		if(!isset($this->_events[$event])) 
			$this->_events[$event]	= array();
	
		if(!in_array($handler, $this->_events[$event]))
			$this->_events[$event][] = $handler;
	}
	
	/**
	 * Call the necessary mambots to run.
	 **/	 	
	function _callFunction($handler, &$row, &$params, $page, $event){
	
		// Use call_user_func_array instead so it wont 
		// return call time pass by reference errors stuffs.
		if(cmsVersion() == _CMS_JOOMLA10 || cmsVersion() == _CMS_MAMBO){
			// Preparecontent event uses 4 parameters
			if($event == 'onPrepareContent'){
				return call_user_func_array($handler, array(true, &$row, &$params, $page));
				//return $handler(true, &$row, &$params, $page);
			}
			else{
				return call_user_func_array($handler, array(&$row, &$params, $page));
			}
		}
		else{
		
			if( class_exists($handler) )
			{
				$dispatcher	=& JDispatcher::getInstance();

				// load plugin parameters
				$plugin =& JPluginHelper::getPlugin('content' , $handler);

				// create the plugin
				$instance = new $handler($dispatcher, (array)($plugin));

				
				return $instance->$event($row, $params , $page);
			}
			else if( function_exists($handler) )
			{
				return call_user_func_array($handler, array(&$row, &$params, $page));
			}
			
// 			if(!is_object($handler) && function_exists($handler)){
// 				//echo $handler . '<br />';
// 				// Joomla 1.5 uses 3 parameters instead.
// 				// Use call_user_func_array instead so it wont return call time pass by reference errors stuffs.
// 				
// 				
// 			}
// 			
// 			if(is_object($handler)){
// 				
// 				if($event == 'onPrepareContent'){
// 					return call_user_func_array(array($handler, $event), array(&$row, &$params, $page));
// 				}
// 			}
		}
	}
	
	function trigger($event, &$row, &$params, $page = '0'){
		$result	= '';
		if(isset($this->_events[$event])){
			foreach($this->_events[$event] as $handler){
				//var_dump($handler);
				$result	.= $this->_callFunction($handler, $row, $params, $page, $event);
// 				if($event == 'onBeforeDisplayContent' || $event == 'onAfterDisplayContent'){
// 					if(function_exists($handler)){
// 						$result	.= $this->_callFunction($handler, $row, $params, $page, $event);
// 					}
// 				}
// 				else{
// 					if(function_exists($handler) || is_object($handler)){
// 						
// 					}
// 				}
			}
		}
 		return $result;
	}
	
	/**
	 * Initialize all mambots / plugins to be stored into #__myblog_mambots	 
	 **/	 	
	function init(){
		$this->_db->initPlugins();
	}
	
	function get($limitstart, $limit){
		return $this->_db->get($limitstart, $limit);
	}
}