<?php
(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

class MYAvatarBase{
	var $_width		= '';
	var $_height	= '';
	var $_user		= '';
	
	var	$_default	= '';
	var $_email		= '';
	
	var $cms		= null;
	
	function MYAvatarBase($userId){
		global $_MY_CONFIG;
		
		$this->cms		=& cmsInstance('CMSCore');
		
		$this->_user	= $userId;
		$this->_email	= myGetAuthorEmail( $userId );
				
		$this->_width	= $_MY_CONFIG->get('avatarWidth');
		$this->_height	= $_MY_CONFIG->get('avatarHeight');
		
		$this->_default	= $this->cms->get_path('live') . '/components/com_myblog/images/guest.gif';
	}
	
	// Return the HTML code to be inserted as avatar
	function display($url = false){
		global $_MY_CONFIG;
		
		$url		= !empty($url) ? $url : $this->_default;
		$alt		= myGetAuthorName($this->_user, $_MY_CONFIG->get('useFullName'));
		$content	= '';
		$link		= $this->_link();

		if($this->_link() && $_MY_CONFIG->get('linkAvatar')){
			$content	= '<a href="' . $link . '">'
						. '<img src="' . $url . '" border="0" alt="' . $alt . '" width="' . $this->_width . '" height="' . $this->_height . '" />'
						. '</a>';
		} else{
			$content	= '<img src="' . $url . '" border="0" alt="' . $alt . '" width="' . $this->_width . '" height="' . $this->_height . '" />';
		}
		
		return $content;
	}
}

class MYGravatarAvatar extends MYAvatarBase{
		
	// Returns html source
	function get(){
		global $_MY_CONFIG;
		
		$url	= 'http://www.gravatar.com/avatar.php?gravatar_id=' . md5(myGetAuthorEmail($this->_user))
				. '&default=' . urlencode($this->_default)	. '&size=' . $this->_width;
	
		return $this->display($url);
	}
	
	function _link(){
		return false;
	}
}

class MYFireboardAvatar extends MYAvatarBase{

	// Fireboard configuration file
	var $_config	= '';
	
	// Fireboard configurations
	var $_version	= '';
	var $_src		= '';
	
	function _init(){

		// Test if this is prior to 1.0.5
		if( file_exists( $this->_config ) )
		{
			// We assume it might be version 1.0.5 and above
			require_once( $this->_config );
			global $fbConfig;
				
			$this->_config	= new fb_config();
			$this->_config->load();
		}
		else
		{
			$this->_config  = $this->cms->get_path('root') . '/administrator/components/com_fireboard/fireboard_config.php';
			include($this->_config);
			
			$this->_version	= $fbConfig['version'];
			$this->_src		= $fbConfig['avatar_src'];
		}
	}

	function _loadVars()
	{
		if( !is_object( $this->_config ) )
		{
			require( $this->_config );
			global $fbConfig;
		
			$obj	= new stdclass();
				
			$obj->avatar_src	= $fbConfig['avatar_src'];
			$obj->version		= $fbConfig['version'];
			
			$this->_config	= $obj;
		}
		
	}
		
	// Returns path to image
	function get(){
		$this->_config	= $this->cms->get_path('root') . '/components/com_fireboard/sources/fb_config.class.php';
		//$this->_config	= $this->cms->get_path('root') . '/administrator/components/com_fireboard/fireboard_config.php';
		
		$this->_init();
		$this->_loadVars();
		
		if($this->_config->avatar_src == 'fb'){
			// Get fireboard avatar path
			$strSQL	= "SELECT `avatar` FROM #__fb_users WHERE `userid`='{$this->_user}'";
			$this->cms->db->query($strSQL);
			$relativePath	= $this->cms->db->get_value();
			
			if($relativePath){
				$avatar	= '';
				
				if($this->_config->_version == '1.0.2' || $this->_config->_version == '1.0.3'){
					// Older versions
					$avatar	= '/components/com_fireboard/avatars/' . $relativePath;
				} else {
					// Newer versions
					$avatar	= '/images/fbfiles/avatars/' . $relativePath;
				}
				
				// Check if avatar file really exists.
				if(file_exists($this->cms->get_path('root') . $avatar)){
					return $this->display($this->cms->get_path('live') . $avatar);
				}
			}
		} else if($this->_config->avatar_src != 'cb'){
			// handling cases where fireboard avatar source is from clexus PM
		}
		return $this->display();
	}
	
	function _link(){
		global $Itemid;
		$link = cmsSefAmpReplace("index.php?option=com_fireboard&func=fbprofile&task=showprf&userid={$this->_user}&Itemid={$Itemid}");
		
		return $link;
	}
}

class MYCbAvatar extends MYAvatarBase{
	
	function get(){
		$strSQL	= "SELECT `avatar` FROM #__comprofiler WHERE `user_id`='{$this->_user}' AND `avatarapproved`='1'";
		
		$this->cms->db->query($strSQL);
		$result	= $this->cms->db->get_value();

		if($result){
			if(file_exists($this->cms->get_path('root') . '/components/com_comprofiler/images/' . $result)){
				$url	= $this->cms->get_path('live') . '/components/com_comprofiler/images/' . $result;
				return $this->display($url);
			} else if(file_exists($this->cms->get_path('root') . '/images/comprofiler/' . $result)){
				$url	= $this->cms->get_path('live') . '/images/comprofiler/' . $result;
				return $this->display($url);
			}
		}
		return $this->display();
	}
	
	function _link(){
		$link = cmsSefAmpReplace("index.php?option=com_comprofiler&task=userProfile&user={$this->_user}");
		return $link;
	}
}

class MYSmfAvatar extends MYAvatarBase{

	var $_path	= '';
	var $_db;
	
	function _selectDB( $server , $user , $password , $db )
	{
		static $selected;
		
		$selected	= false;
		if( !$selected )
		{
			$resource	= mysql_connect( $server , $user , $password );
		
			mysql_select_db( $db , $resource );
			
			$selected	= true;
		}
		return true;
	}
	
	function get(){
		global $_MY_CONFIG, $mainframe;
		
		$this->_path	= rtrim(trim($_MY_CONFIG->get('smfPath')), '/');
		

		if(!$this->_path || $this->_path == '' || !file_exists($this->_path . '/Settings.php')){
			$this->_path	= $this->cms->get_path('root') . '/forum';
		}

		// check for SMF bridge, then use SMF bridge settings if SMF bridge installed
		if(!$this->_path || $this->_path == '' || !file_exists($this->_path . '/Settings.php')){
			$strSQL	= "SELECT `id` FROM #__components WHERE `option`='com_smf'";
			$this->cms->db->query($strSQL);
			if($this->cms->db->get_value()){
				$strSQL	= "SELECT `value1` FROM #__smf_config WHERE variable='smf_path'";
				$this->cms->db->query($strSQL);
				$this->_path	= rtrim(str_replace("\\", "/", $this->cms->db->get_value()) , "/");
			}
		}

		// need to get the settings as SMF db / tables can be in different location
		if(file_exists($this->_path . '/Settings.php')){
			include($this->_path . '/Settings.php');

			mysql_select_db($mainframe->getCfg('db') , $this->cms->db->db);

			$this->_selectDB($db_server , $db_user , $db_passwd , $db_name );
			
			// query for user avatar, id using email address
			$strSQL	= sprintf("SELECT avatar, ID_MEMBER FROM {$db_prefix}members WHERE emailAddress='{$this->_email}'");
			$result	= mysql_query($strSQL);

			if($result){
				$result_row	= mysql_fetch_array($result);
				mysql_select_db($mainframe->getCfg('db'));
				
				if($result_row){
					$id_member	= $result_row[1];
					
					if(trim($result_row[0]) != ''){
						if(substr($result_row[0], 0, 7) != 'http://'){
							$url	= $boardurl . '/avatars/' . $result_row[0];
							return $this->display($url);
						}else{
							$url	= $result_row[0];
							return $this->display($url);
						}
							
					} else {
						mysql_select_db($db_name);
						$strSQL	= sprintf("SELECT ID_ATTACH FROM {$db_prefix}attachments WHERE ID_MEMBER='{$id_member}' AND ID_MSG=0 AND attachmentType=0");
						$result = mysql_query($strSQL);
						
						if($result){
							$result_avatar = mysql_fetch_array($result);
							mysql_select_db($mainframe->getCfg('db'));
							if ($result_avatar[0]){
								$url = "$boardurl/index.php?action=dlattach;attach=" . $result_avatar[0] . ";type=avatar";
								return $this->display($url);
							}
						}
					}
				}
			}
		}
		return $this->display();
	}
	
	function _link(){
		global $mainframe;

		if(file_exists($this->_path . '/Settings.php')){
			include($this->_path . '/Settings.php');

			$this->_selectDB($db_server , $db_user , $db_passwd , $db_name );
			
			$strSQL	= sprintf("SELECT ID_MEMBER FROM {$db_prefix}members WHERE emailAddress='%s'", mysql_real_escape_string($this->_email));
			$result = mysql_query($strSQL);
			$result_row = @mysql_fetch_array($result);
			mysql_select_db($mainframe->getCfg('db'));
				
			if($result_row)
			{
				$link = $boardurl . "/index.php?action=profile&u=" . $result_row[0];
				return $link;
			}		
		}
		return false;
	}
}

class MYJuserAvar extends MYAvatarBase{

	// Returns html source
	function get(){
	
		$path	= $this->cms->get_path('root') . '/administrator/components/com_juser/avatars';
		
		$path	= $path . '/' . myGetAuthorName($this->_user);
		$source	= $this->cms->get_path('live') . '/administrator/components/com_juser/avatars/' . myGetAuthorName($this->_user);
		
		if(file_exists($path . '.gif')){
			$source	.= '.gif';
		} else if(file_exists($path . '.png')){
			$source .= '.png';
		} else if(file_exists($path . '.jpg')){
			$source .= '.jpg';
		} else {
			return false;
		}
	
		return $this->display($url);
	}
	
	function _link(){
		return false;
	}
}

class MYJomsocialAvatar extends MYAvatarBase
{
	// Returns html source
	function get()
	{
		$core	= $this->cms->get_path('root') . '/components/com_community/libraries/core.php';
		
		if( file_exists( $core ))
		{
			require_once( $core );
			
			$user	=& CFactory::getUser( $this->_user );
			
			return $this->display( $user->getThumbAvatar() );
		}

		return $this->display();
	}
	
	function _link()
	{
		$core	= $this->cms->get_path('root') . '/components/com_community/libraries/core.php';
		
		if( file_exists( $core ))
		{
			require_once( $core );
			
			$url 	= CRoute::_('index.php?option=com_community&view=profile&userid=' . $this->_user );
			
			return $url;
		}
		return false;
	}
}

class MYNoneAvatar extends MYAvatarBase{
	
	function get(){
		return "";
	}
}
?>