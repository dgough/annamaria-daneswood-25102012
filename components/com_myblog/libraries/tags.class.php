<?php
/**
 * MyBlog
 * @package MyBlog
 * @copyright (C) 2006 - 2008 by Azrul Rahim - All rights reserved!
 * @license Copyrighted Commercial Software
 **/
 
(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

class MYTags {
	
	var $_errMsg;
	var $tags; // array of tag
	var $insertId;
	
	
	/**
	 * Transform the new tag so that it is suitable to become a tag, remove
	 * invalid char etc..	 
	 */	 	
	function _prepNewtag($newtag){
// 		$db	=& cmsInstance('CMSDb');
// 		
// 		$newtag = strip_tags($newtag);
// 		$newtag = html_entity_decode($newtag);
// 		$newtag = preg_replace('/[\s]{2,}/', ' ', $newtag);
// 		
// 		// Remove some invalid character
// 		$replacechar = array('-', '!', ':');
// 		$newtag = str_replace($replacechar, '',$newtag);
// 
// 		// Get page ISO
// 		if(cmsVersion() == _CMS_JOOMLA10 || cmsVersion() == _CMS_MAMBO){
// 			$iso    = split('=',_ISO);
// 		}elseif(cmsVersion() == _CMS_JOOMLA15){
// 			$iso    = 'UTF-8';
// 		}

// 		# Remove unwanted characters
// 		$newtag = preg_replace('/[`~!@#$%\^&*\(\)\+=\{\}\[\]|\\<">,\\/\^\*;:\?\'\\\]/', '', $newtag);
// 		# UTF8 to ISO 8859
// 		$newtag = preg_replace("/([\xC2\xC3])([\x80-\xBF])/e", "chr(ord('\\1')<<6&0xC0|ord('\\2')&0x3F)", $newtag);
// 		$newtag = trim($newtag);
		
		if(cmsVersion() != _CMS_JOOMLA15)
		{
			# Remove unwanted characters
			
		
			# UTF8 to ISO 8859
			$newtag = preg_replace("/([\xC2\xC3])([\x80-\xBF])/e", "chr(ord('\\1')<<6&0xC0|ord('\\2')&0x3F)", $newtag);
			$newtag = trim($newtag);
		}
		# ISO 8859 to UTF8
		// Check if page is already UTF-8
// 		if(is_array($iso) && $iso[1] != 'UTF-8' && (cmsVersion() != _CMS_JOOMLA15))
// 			$newtag = preg_replace("/([\x80-\xFF])/e", "chr(0xC0|ord('\\1')>>6).chr(0x80|ord('\\1')&0x3F)", $newtag);
// 		
		return $newtag;
	}
	
	/**
	 * Fix tag to not contain any special characters
	 **/	 	
	function strip( $tag ){
		$tag = preg_replace('/[`~!@#$%\^&*\(\)\+=\{\}\[\]|\\<">,\\/\^\*;:\?\'\\\]/', '', $tag);
		
		return $tag;
	}
	
	function getTagCloud(){
	}
	
	/**
	 * Add a new tag. Return false if tag cannot be added
	 */	 	
	function add(&$newtag){
		$cms    =& cmsInstance('CMSCore');
		$db     =& cmsInstance('CMSDb');
		$addOk  = false;
		
		
		// Tag can contain any words.
		$newtag	= $cms->db->_escape( $newtag );
		
		// Slug should need to contain proper naming
		$slug	= $this->_prepNewTag($newtag);
		$slug	= $this->strip($slug);
		
		// Check if tag is valid.
		if($newtag == '' || $slug == '')
			return false;

		// Lookup the database to check if there are any existing same tag
		$strSQL     = "SELECT COUNT(*) FROM `#__myblog_categories` WHERE `name`='{$newtag}' OR `slug`='{$slug}'";
		$db->query($strSQL);
		
		$totalMatch = $db->get_value();
	
		if($totalMatch == 0){
		    // Do something;
			 
		    $strSQL = "INSERT INTO `#__myblog_categories` (`name`,`slug`) VALUES ('{$newtag}','{$slug}')";
		    $db->query($strSQL);
		    
		    $this->insertId = $db->get_insert_id();
		    $addOk = true;
		} else {
			// Set the insertId for existing tag.
			$strSQL	= "SELECT `id` FROM `#__myblog_categories` WHERE `name`='{$newtag}'";
			$cms->db->query($strSQL);
			$this->insertId	= $cms->db->get_value();
		}
		
		return $addOk;
	}
	
	/***
	 * Return the id of the last inserted
	 */	 	
	function getInsertId(){
		return $this->insertId;
	}	
}
