<?php
/**
 * MyBlog
 * @package MyBlog
 * @copyright (C) 2006 - 2008 by Azrul Rahim - All rights reserved!
 * @license Copyrighted Commercial Software
 **/
 
(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

// Include our custom cmslib if its not defined
if(!defined('CMSLIB_DEFINED'))
	include_once ((dirname((dirname(dirname(dirname(__FILE__)))))) . '/components/libraries/cmslib/spframework.php');

class MYMailer {
	var $mail		= null;
	var $cms		= null;
	
	var $subject	= '';
	var $body		= '';
	
	function MYMailer(){
		global $mainframe;
		
		$this->cms		=& cmsInstance('CMSCore');
		
		if(cmsVersion() == _CMS_JOOMLA15){
			jimport('joomla.mail.mail');
			$this->mail	=& JFactory::getMailer();
		} else {
			$this->mail	= new mosPHPMailer();
			$this->mail->PluginDir = $this->cms->get_path('root') . '/includes/phpmailer/';
			$this->mail->SetLanguage('en', $this->cms->get_path('root') . '/includes/phpmailer/language/');
			$this->mail->CharSet = 'UTF-8';
			$this->mail->IsMail();
			$this->mail->From = $mainframe->getCfg('mailfrom');
			$this->mail->FromName = $mainframe->getCfg('fromname');
			$this->mail->Mailer = $mainframe->getCfg('mailer');
			
			// Add smtp values if needed
			if ($mainframe->getCfg('mailer') == 'smtp') {
				$this->mail->IsSMTP();
				$this->mail->SMTPAuth = $mainframe->getCfg('smtpauth');
				$this->mail->Username = $mainframe->getCfg('smtpuser');
				$this->mail->Password = $mainframe->getCfg('smtppass');
	
				$host	= $mainframe->getCfg('smtphost');
				$hostIp = split(":", $host);
				if(count($hostIp)> 1){
					$this->mail->Host = $hostIp[0];
					$this->mail->Port = $hostIp[1];
				} else {
					$this->mail->Host = $mainframe->getCfg('smtphost');
				}
			} else{
				// Set sendmail path
				if ($mainframe->getCfg('mailer') == 'sendmail') {
					$sendmail = $mainframe->getCfg('sendmail');
					if (isset ($sendmail))
						$this->mail->Sendmail = $sendmail;
					} // if
			}
		}
		
	}
	
	function send($from, $recipient, $subject, $body, $html = false, $cc = NULL, $bcc = NULL, $attachment = NULL, $replyto = NULL, $replytoname = NULL){
		
		if(cmsVersion() == _CMS_JOOMLA15){
			$this->mail->setSubject($subject);
			$this->mail->setBody(stripslashes($body));
		} else {
			$this->mail->Subject	= $subject;
			$this->mail->Body		= stripslashes($body);
		}

		// activate HTML formatted emails
		if($html)
			$this->mail->IsHTML(true);
	
		// Check if mail address is valid.
		$regexp	= "^([_a-z0-9-]+)(\.[_a-z0-9-]+)*@([a-z0-9-]+)(\.[a-z0-9-]+)*(\.[a-z]{2,4})$";
		
		if (is_array($recipient)) {
			foreach ($recipient as $to) {
				if(!empty($to) && eregi($regexp, $to)){
					$this->mail->AddAddress($to);
				}
			}
		} else {
			if(!empty($recipient) && eregi($regexp, $recipient)){
				$this->mail->AddAddress($recipient);
			}
		}
		if (isset ($cc)) {
			if (is_array($cc)) {
				foreach ($cc as $to) {
					$this->mail->AddCC($to);
				}
			} else {
				$this->mail->AddCC($cc);
			}
		}
		if (isset ($bcc)) {
			if (is_array($bcc)) {
				foreach ($bcc as $to) {
					$this->mail->AddBCC($to);
				}
			} else {
				$this->mail->AddBCC($bcc);
			}
		}
		if ($attachment) {
			if (is_array($attachment)) {
				foreach ($attachment as $fname) {
					$this->mail->AddAttachment($fname);
				}
			} else {
				$this->mail->AddAttachment($attachment);
			}
		}
		
		if(cmsVersion() == _CMS_JOOMLA15){
			if($replyto){
				$this->mail->addReplyTo(array($replyto, $replytoname));
			}
		} else {
			//Important for being able to use mosMail without spoofing...
			if ($replyto) {
				if (is_array($replyto)) {
					reset($replytoname);
					foreach ($replyto as $to) {
						$toname = ((list ($key, $value) = each($replytoname)) ? $value : '');
						$this->mail->AddReplyTo($to, $toname);
					}
				} else {
					$this->mail->AddReplyTo($replyto, $replytoname);
				}
			}
		}
		$status = $this->mail->Send();
		return $status;
	}
}