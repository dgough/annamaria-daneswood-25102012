<?php
(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

/**
 *	Shows toolbar for the My Blog frontpage
 */ 
function mb_showViewerToolbar($op = "") {
// 	global $MYBLOG_LANG, $mainframe, $Itemid;
// 	global $_MYBLOG, $_MY_CONFIG;
// 	$cms    =& cmsInstance('CMSCore');
// 	$db		=& cmsInstance("CMSDb");
// 	
// 	if ($_MY_CONFIG->get('frontpageToolbar')) {
// 		
// 		$rssLink = "index.php?option=com_myblog";
// 		if (isset ($_REQUEST['blogger']) and $_REQUEST['blogger'] != "")
// 			$rssLink .= "&blogger=" . htmlspecialchars($_REQUEST['blogger']);
// 		
// 		if (isset ($_REQUEST['category']) and $_REQUEST['category'] != "")
// 			$rssLink .= "&category=" . htmlspecialchars($_REQUEST['category']);
// 		
// 		if (isset ($_REQUEST['keyword']) and $_REQUEST['keyword'] != "")
// 			$rssLink .= "&keyword=" . htmlspecialchars($_REQUEST['keyword']);
// 		
// 		if (isset ($_REQUEST['archive']) and $_REQUEST['archive'] != "")
// 			$rssLink .= "&archive=" . htmlspecialchars($_REQUEST['archive']);
// 		
// 		if (isset ($_REQUEST['Itemid']) and $_REQUEST['Itemid'] != "" and $_REQUEST['Itemid'] != "0")
// 			$rssLink .= "&Itemid=" . intval($_REQUEST['Itemid']);
// 		else
// 		{
// 			# autodetect Itemid
// 			$query = "SELECT id FROM #__menu  WHERE type='components' "
// 			        ."AND link='index.php?option=com_myblog' "
// 			        ."AND published='1'";
// 
// 			$db->query($query);
// 
// 			$myItemid = $db->get_value();
// 			if (!$myItemid)
// 				$myItemid = 1;
// 			$Itemid = $myItemid;
// 		}
// 		$rssLink .= "&task=rss";
// 		
// 		$cms->load('helper', 'url');
// 		$homeLink = cmsSefAmpReplace("index.php?option=com_myblog&Itemid=$Itemid");
// 		$categoryLink = cmsSefAmpReplace("index.php?option=com_myblog&task=categories&Itemid=$Itemid");
// 		$searchLink = cmsSefAmpReplace("index.php?option=com_myblog&task=search&Itemid=$Itemid");
// 		$bloggersLink = cmsSefAmpReplace("index.php?option=com_myblog&task=bloggers&Itemid=$Itemid");
// 		
// 		$rssLink = cmsSefAmpReplace($rssLink);
// 
// 		$dashboardClass = "thickbox";
// 		$thickboxScript="";
// 		
// 		if(!class_exists('AzrulJXTemplate'))
// 		    include_once($cms->get_path('plugins') . '/system/pc_includes/template.php');
// 
// 		$tpl = new AzrulJXTemplate();
// 		$toolbar = array();
// 		$active = array();
// 		$show = array();
// 		$toolbar['op'] = $op;	
// 		$toolbar['homeLink'] 		= $homeLink;
// 		$toolbar['categoryLink'] 	= $categoryLink;
// 		$toolbar['searchLink'] 		= $searchLink;
// 		$toolbar['bloggersLink'] 	= $bloggersLink;
// 		
// 		$show['feed'] = $_MY_CONFIG->get('useRSSFeed');
// 		
// 		$active['home'] = '';
// 		$active['category'] = '';
// 		$active['search'] ='';
// 		$active['blogger'] ='';
// 		$active[$op] = ' blogActive';
// 		
// 		# If viewing userblog, only display Home and Dashboard links
// 		if ($op == "userblog") {
// 			$homeLink = cmsSefAmpReplace("index.php?option=com_myblog&Itemid=$Itemid&task=userblog");
// 			$manageBlogLink = cmsSefAmpReplace("index2.php?option=com_myblog&no_html=1&admin=1&task=adminhome&Itemid=$Itemid&keepThis=true&TB_iframe=true&height=600&width=850");
// 			$toolbar['homeLink'] = $homeLink;
// 			$toolbar['manageBlogLink'] = $manageBlogLink;
// 		} else {
// 			$toolbar['rssFeedLink'] = $rssLink;
// 		}
// 		
// 		$tpl->set('toolbar', $toolbar);
// 		$tpl->set('show', $show);
// 		$tpl->set('active', $active);
// 		$tpl->set('title', stripslashes($_MY_CONFIG->get('mainBlogTitle')));
// 		$tpl->set('summary', stripslashes($_MY_CONFIG->get('mainBlogDesc')));
// 		
// 		$template_file = MY_TEMPLATE_PATH . "/" . $_MY_CONFIG->get('template') . "/toolbar.tmpl.html";
// 		if(!file_exists($template_file))
// 			$template_file = MY_TEMPLATE_PATH . "/_default/toolbar.tmpl.html";
// 
// 		$toolbar_output = $tpl->fetch($template_file);
// 		echo str_replace(array_keys($MYBLOG_LANG), array_values($MYBLOG_LANG), $toolbar_output);
// 	}
}
