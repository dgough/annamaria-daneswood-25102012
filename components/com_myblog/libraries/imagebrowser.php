<?php

/**
 * MyBlog
 * @package MyBlog
 * @copyright (C) 2006 - 2008 by Azrul Rahim - All rights reserved!
 * @license Copyrighted Commercial Software
 **/
 
(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

// If the memory limit is too low, increase it 
if(ini_get('memory_limit') == '8M'){
	@ini_set('memory_limit', '16M');
} 
//----------------------------------- FUNCTION ------------------------------------
/**
 * Loads a directory based on parameter
 **/
// function myxLoadFolder($directory){
// 	global $_MY_CONFIG;
// 	
// 	$objResponse = new JAXResponse();
// 	
// 	$cms =& cmsInstance('CMSCore');
// 	$cms->load('libraries', 'user');
// 
// 	// Check if user really allowed to go out of current directory.
// 	if($_MY_CONFIG->get('imgFolderRestrict') && $directory == '/..' && $cms->user->id != '62'){
// 	    $objResponse->addScriptCall('alert("Directory Permissions Error. Are you trying to exploit the system?");');
// 	}else if($_MY_CONFIG->get('imgFolderRestrict') && $directory == '/..' && $cms->user->id == '62'){
// 	    // User is really allowed.
// 	    $printHTML = mnBrowse("$directory/");
// 	    $objResponse->addAssign('mn_fileExplorer','innerHTML', $printHTML);
// 	}else {
// 	    // Other allowed directories.
// 		$printHTML = mnBrowse("$directory/");
// 		$objResponse->addAssign('mn_fileExplorer', 'innerHTML', $printHTML);
// 	}
// 	return $objResponse->sendResponse();
// }

/**
 * If imgFolderRestrict is enabled, we should first append the user folder in 
 * the filename 
 */ 
function mnShowThumb($filename,$maxwidth=60){
	global $mainframe;
	$isCached = false;
	$cms =& cmsInstance('CMSCore');
	
	$filename = myGetImageBrowserRoot() . $filename;
	
	$cachedPath = $mainframe->getCfg('absolute_path').'/cache/cache_img'. md5($filename);
	  
	if (is_file($cachedPath)){
		//$isCached = true;
		//$image_resized = $mosConfig_absolute_path.'/images/cache/'.md5($filename);
		if (strtolower (substr($filename,-3)) == 'png')
			header('Content-type: image/png');
		else if (strtolower (substr($filename,-3)) == 'gif')
			header('Content-type: image/gif');
		else if (strtolower (substr($filename,-3)) == 'jpg')
			header('Content-type: image/jpg');
		else
			header('Content-type: image/bmp');
		
		$handle = fopen($cachedPath, "r");
		$contents = fread($handle, filesize($cachedPath));
		fclose($handle);
		print($contents);
		die;
	} else if (!$isCached){
		$filename = urldecode($filename);
		$filepath	= $filename;
	
		
		$image = mnOpenImage($filepath);
		if ($image === false) { die ('Unable to open image'); }
		
		// Get original width and height
	 	$width = imagesx($image);
	 	$height = imagesy($image);
		
		// New width and height
		if ($width > $maxwidth){
			$new_width = $maxwidth;
		 	$new_height = intval($maxwidth * $height / $width);
		} else {
			$new_width = $width;
		 	$new_height = $height;
		}
	
	 	$image_resized = imagecreatetruecolor( $new_width, $new_height );
	
		$background = imagecolorallocate($image_resized, 0, 0, 0);
	
		if (mnCheckFormat($filepath) == "gif" || mnCheckFormat($filepath) == "png"){
			ImageColorTransparent($image_resized, $background); // make the new temp image all transparent
			imagealphablending($image_resized, false); // turn off the alpha blending to keep the alpha channel
		}
	
	 	imagecopyresampled($image_resized, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
	
		// Display resized image
	
		ob_start();
		//imagejpeg ($image_resized, '', 70);
	
		$mimetype = mnCheckFormat($filepath);
	
	     if ($mimetype == "gif"){
		    header("Content-type: image/gif");
	 		imagegif ($image_resized);
		 }
	     else if ($mimetype == "png"){
		    header("Content-type: image/png");
	 		imagepng ($image_resized, '', 7);
		 }
	     else{
		    header("Content-type: image/jpeg");
	 		imagejpeg ($image_resized, '', 80);
		 }
	
		$somecontent = ob_get_contents();
		ob_end_flush() ;
		
		   // In our example we're opening $filename in append mode.
		   // The file pointer is at the bottom of the file hence 
		   // that's where $somecontent will go when we fwrite() it.
		   if (!$handle = fopen($cachedPath, 'w')) {
		        echo "Cannot open file ($cachedPath)";
		        exit;
		   }
		
		   // Write $somecontent to our opened file.
		   if (fwrite($handle, $somecontent) === FALSE) {
		       echo "Cannot write to file ($cachedPath)";
		       exit;
		   }
		   
		   fclose($handle);	 
	}
	
	die();
}

function mnPrintImage($filename){
	global $mainframe;
	$cms =& cmsInstance('CMSCore');
	$filename = urldecode($filename);

	$filename = $cms->get_path('root') . $filename;

	if (strtolower (substr($filename,-3)) == 'png')
		header('Content-type: image/png');
	else if (strtolower (substr($filename,-3)) == 'gif')
		header('Content-type: image/gif');
	else if (strtolower (substr($filename,-3)) == 'jpg')
		header('Content-type: image/jpg');
	else
		header('Content-type: image/bmp');

	$handle = fopen($filename, "r");
	print fread($handle, filesize($filename));
	fclose($handle);

	die();
}

function mnBrowse($directory = ''){
	global $mainframe, $_MY_CONFIG, $my;
	
	$cms =& cmsInstance('CMSCore');
	$cms->load('libraries', 'user');
	
	$imgroot = $_MY_CONFIG->get('imgFolderRoot');
	
	// The very base of url
	$basePath 	= myGetImageBrowserRoot(); 
	$baseUrl 	= myGetImageBrowserLive(); 
	
	$browseDir	= $basePath. $directory;
	$browseUrl	= $baseUrl .$directory;
	$dir = $browseDir;
	
	$cellcount=0;
	$thumnailperrow = "3";
	$thumnailmaxwidth = "44";
	$thumnailborder1 = "47";
	$thumnailborder2 = "46";
	$html = "";

$html = <<< ENDHTML
<script type="text/javascript">
var dblClickTimer = false;

function openFolder(path){
	jax.call('myblog', 'myxLoadFolder' , path);
}

</script>
<table cellpadding=2 cellspacing=0 border=0>
ENDHTML;

	$printdirarray = array();
	
	// List all files for the images folder
	if (is_dir($dir)) {
	   if ($dh = opendir($dir)) {
	       
			while (($file = readdir($dh)) !== false) {
				array_push($printdirarray, "$file");
			}
			closedir($dh);
			
			// Now we have all the files, group it into folders and files
			$files	 = array();
			$folders = array();
			foreach($printdirarray as $dirObject){
				if(is_file("$dir/$dirObject")){
					// Need to test is the file is an image. Use a simple
					// extenstion test
					$types = array( 
						'GIF', 'JPG', 'JPEG', 'PNG','SWF','PSD',
						'BMP','TIFF','TIFF','JPC','JP2',
						'JPX','JB2','SWC','IFF','WBMP','XBM'
						);
					
					$path_parts = pathinfo("$dir/$dirObject");
					if(!empty($path_parts['extension']) && in_array(strtoupper($path_parts['extension']) , $types))
						$files[] = $dirObject;
				} else
					$folders[] = $dirObject;
			}
			
			sort ($files);
			sort ($folders);
			
			// We don't need this anymore
			unset($printdirarray);
			
			// Now we have a list of files and folder, display them, obviously,
			// folder will be displayed first. And since, everything else if the
			// same except the last content, we can generate the content first
			$items = array();
			
			// Process folder items
			foreach($folders as $f){
				$img = '';
				$onclick = '';
				$title ='';
				$caption = '';
				
				if($f == '..' && !empty($directory) && $basePath != rtrim($browseDir, '\\/') ){
					$img = $cms->get_path('live') .'/components/com_myblog/images/folderup.gif';
					$parentPath = dirname($browseDir);
					$parentPath = str_replace($basePath, '', $parentPath);
					$onclick = "openFolder('$parentPath');";
				} elseif($f != '.'){
					$img = $cms->get_path('live').'/components/com_myblog/images/bigfolder.gif';
					$op = $f;
					if(empty($directory))
						$op = "/$op";
						
					$onclick = "openFolder('{$directory}{$op}');";
				}
				
				$title = $f;
				$caption = $f;
				
				if($f != '.' && $f != '/..'){
					$items[] = array('img' =>$img, 'onclick' => $onclick, 'title' => $title, 'caption' => $caption);
				} else if($f != '.' && $f == '/..' && !$_MY_CONFIG->get('imgFolderRestrict')){
				    // If users are not restricted.
				    $items[] = array('img' => $img, 'onclick' => $onclick, 'title' => $title, 'caption' => $caption);
				} else if ($f != '.' && $f == '/..' && $cms->user->id == '62') {
           	        // Default to allow admin to view all
           	        $items[] = array('img' => $img, 'onclick' => $onclick, 'title' => $title, 'caption' => $caption);
           		}
			}
			
			// Process file items
			foreach($files as $f){
				$filepath = "$dir/$f";
				list($width, $height) = getimagesize($filepath);
				$img = '';
				$onclick = '';
				$title ='';
				$caption = '';
				
				// We assume files that we can getthe image sizes are the supported
				// ones
				if($sizes = getimagesize($filepath)){
					list($width, $height) = $sizes;
				
					$srcimg = rtrim ($browseUrl,'\\/') . '/' . ltrim ($f,'\\/');	
					$img = $srcimg;
					if($width > $thumnailmaxwidth){
						$thumbimg = rtrim ($directory,'\\/') . '/' . ltrim ($f,'\\/');
						
						// If image is big, we parse it through a resizer
						$img = "index2.php?option=com_myblog&task=thumb&maxwidth=$thumnailmaxwidth&fn=". urlencode($thumbimg);
					} 
					
					$onclick  = "tinyMCE.execCommand('mceFocus',false, 'mce_editor_0'); ";
					$onclick .= "tinyMCE.execCommand('mceInsertContent',false, '<img border=\\'0\\' src=\\'$srcimg\\' /> ');";
					
					$caption = $f;
					$title = "$f - $width x $height";
					$items[] = array('img' =>$img, 'onclick' => $onclick, 'title' => $title, 'caption' => $caption);					
				}
			}
			
			// Now, $items contain everything we need in the correct order. print
			// it out
			$html .= '<table>';
			foreach($items as $item){
			
				if (($cellcount % $thumnailperrow) == 0)
					$html .= "<tr>";
				
				$html .= '<td><div class="imgBrowserItem" style="cursor:pointer;width:100%" onclick="'.$item['onclick'].'" title="'.$item['title'].'">';
				$html .= '<table style="width:'.$thumnailborder1.'px; height: '.$thumnailborder1.'px; border: 1px solid #ece9d8;" align="center" valign="middle"><tr><td align="center" valign="middle">';
				$html .= '<img border="0" src="'.$item['img'].'" />';
				$html .= '</td></tr></table></div><div style="width:'.$thumnailborder1.'px;overflow:hidden; margin-bottom:2px">'.$item['caption'].'</div></td>';
				
				$cellcount++;
				if (($cellcount % $thumnailperrow) == 0)
					$html .= "</tr>";
				
			} 
			$html .= '</table>';
			return $html;
		}
	}
}

function mnOpenImage ($file) {
	# JPEG:
	$im = @imagecreatefromjpeg($file);
	if ($im !== false) { return $im; }
	
	# GIF:
	$im = @imagecreatefromgif($file);
	if ($im !== false) { return $im; }
	
	# PNG:
	$im = @imagecreatefrompng($file);
	if ($im !== false) { return $im; }
	
	# GD File:
	$im = @imagecreatefromgd($file);
	if ($im !== false) { return $im; }
	
	# GD2 File:
	$im = @imagecreatefromgd2($file);
	if ($im !== false) { return $im; }
	
	# WBMP:
	$im = @imagecreatefromwbmp($file);
	if ($im !== false) { return $im; }
	
	# XBM:
	$im = @imagecreatefromxbm($file);
	if ($im !== false) { return $im; }
	
	# XPM:
	$im = @imagecreatefromxpm($file);
	if ($im !== false) { return $im; }
	
	# Try and load from string:
	$im = @imagecreatefromstring(file_get_contents($file));
	if ($im !== false) { return $im; }
	
	return false;
}

function mnCheckFormat ($file) {
	# JPEG:
	$im = @imagecreatefromjpeg($file);
	if ($im !== false) { return "jpeg"; }
	
	# GIF:
	$im = @imagecreatefromgif($file);
	if ($im !== false) { return "gif"; }
	
	# PNG:
	$im = @imagecreatefrompng($file);
	if ($im !== false) { return "png"; }
	
	# GD File:
	$im = @imagecreatefromgd($file);
	if ($im !== false) { return "gd"; }
	
	# GD2 File:
	$im = @imagecreatefromgd2($file);
	if ($im !== false) { return "gd2"; }
	
	# WBMP:
	$im = @imagecreatefromwbmp($file);
	if ($im !== false) { return "im"; }
	
	# XBM:
	$im = @imagecreatefromxbm($file);
	if ($im !== false) { return "xbm"; }
	
	# XPM:
	$im = @imagecreatefromxpm($file);
	if ($im !== false) { return "xpm"; }
	
	# Try and load from string:
	$im = @imagecreatefromstring(file_get_contents($file));
	if ($im !== false) { return "string"; }

	return false;
}

function myxAjaxUpload(){
	global $_MY_CONFIG;
	
	$error = "";
	$msg = "";
		//check if there are files uploaded
		if((isset($_FILES['fileToUpload']['error']) 
		&& $_FILES['fileToUpload'] == 0) 
		||	(!empty($_FILES['fileToUpload']['tmp_name']) 
		&& $_FILES['fileToUpload']['tmp_name'] != 'none'))	{
			
			$userfile_name = $_FILES['fileToUpload']['name'];
			$savepath = myGetImageBrowserRoot(). '/'.$_FILES['fileToUpload']['name']; 
			
			// Lowercase all extensions so that we can match it
			$extAllowed		= strtolower($_MY_CONFIG->get('allowedUploadFileType'));
			$userfile_name	= strtolower($userfile_name);
			
			// Verify file type, this isn't a very good code at all
			if (strpos($extAllowed, substr($userfile_name, -3)) === false){
				$error = $userfile_name . 'The file must be either'. $_MY_CONFIG->get('allowedUploadFileType');
			} 
			
			// Check if image exceeds image upload size limit defined in My Blog config
			if ($_FILES['fileToUpload']['size'] >= $_MY_CONFIG->get('uploadSizeLimit') * 1024){
				$error = "Upload of ".$_FILES['fileToUpload']['name']." failed. File size too large.";
			}
			
			// Make sure similarly named files doesn't exist
			if(file_exists($savepath)){
				$error = "File with similar name already exist.";
			}
			
			// No error, move files to user's folder
			if(empty($error)){
				move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $savepath);
			}
						
			$msg .= " File Name: " . $_FILES['fileToUpload']['name'] . " successfully uploaded. ";
			//$msg .= " File Size: " . @filesize($_FILES['fileToUpload']['tmp_name']);
			
			//for security reason, we force to remove all uploaded file
			@unlink($_FILES['fileToUpload']);
		}else {			
			$error = "No file has been uploaded.";
		}
		
	echo "{";
	echo				"error: '" . $error . "',\n";
	echo				"msg: '" . $msg . "'\n";
	echo "}";
	exit;
}

// Return the root path for current user. If the user is admin, it can view the
// $_MY_CONFIG->get('imgFolderRoot') values, otherwise, if 'imgFolderRestrict' is
// set, set the root path to userpath
function myGetImageBrowserRoot(){
	global $_MY_CONFIG, $mainframe;
	
	$cms =& cmsInstance('CMSCore');
	$cms->load('libraries', 'user');
	
	$imgroot = $cms->get_path('root') .'/'. trim($_MY_CONFIG->get('imgFolderRoot'), '\\/');
	if($_MY_CONFIG->get('imgFolderRestrict')){
		$imgroot .= '/'.$cms->user->id;
		
		// Make sure the folder does exist, if not, create it
		if(!file_exists($imgroot)){
			mkdir($imgroot);
		}
	}
	
	return $imgroot;
}

function myGetImageBrowserLive(){
	global $_MY_CONFIG, $mainframe;
	
	$cms =& cmsInstance('CMSCore');
	$cms->load('libraries', 'user');
	
	$imgroot = $cms->get_path('live') .'/'. trim($_MY_CONFIG->get('imgFolderRoot'), '\\/');
	if($_MY_CONFIG->get('imgFolderRestrict')){
		$imgroot .= '/'.$cms->user->id;
		// This is for browsing.
		// Make sure the folder does exist, if not, create it
		// 		if(!file_exists($imgroot)){
		// 			mkdir($imgroot);
		// 		}
	}
	
	return $imgroot;
}
