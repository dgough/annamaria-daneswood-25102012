<?php

/** 
 *	Polish translation by marcin owczarewicz
 */ 


/** Front page **/
$MYBLOG_LANG['_MB_HOME']			=	"Ogólne";
$MYBLOG_LANG['_MB_TAGS']			=	"tagi";
$MYBLOG_LANG['_MB_TAG']				=	"Tag";	// Single word
$MYBLOG_LANG['_MB_SEARCH']			=	"szukaj";
$MYBLOG_LANG['_MB_FEED']			=	"feed";
$MYBLOG_LANG['_MB_UNTAGGED']		=	"Nieotagowane";
$MYBLOG_LANG['_MB_DASHBOARD']		=	"Dodaj wpis";
$MYBLOG_LANG['_MB_POSTED_BY']		=	"Dodane przez";
$MYBLOG_LANG['_MB_KEYWORDS']		=	"S³owa kluczowe";
$MYBLOG_LANG['_MB_BLOGGER']			= 	"Blogger";
$MYBLOG_LANG['_MB_CATEGORY']		= 	"Kategoria";
$MYBLOG_LANG['_MB_ARCHIVE']			= 	"Archiwum";
$MYBLOG_LANG['_MB_NO_ENTRY']		= 	"Brak wpisów";
$MYBLOG_LANG['_MB_DESCRIPTION']		= 	"Opis";
$MYBLOG_LANG['_MB_READ_MORE']		= 	"Czytaj wiêcej";
$MYBLOG_LANG['_MB_TODAY']			= 	"Dzi¶";
$MYBLOG_LANG['_MB_DAYS']			= 	"dni"; // in blog list, x days ago
$MYBLOG_LANG['_MB_YESTERDAY']		=	"Wczoraj";
$MYBLOG_LANG['_MB_NEVER']			=	"Nigdy";
$MYBLOG_LANG['_MB_ENTRIES']			= 	"wpisy"; // in blog list, x entries
$MYBLOG_LANG['_MB_NO_TAGS']			= 	"Brak tagów"; // in blog list, no tags
$MYBLOG_LANG['_MB_DISPLAY']			= 	"Wy¶wietl"; // in blog list, display # pages


// Dashboard stuff (Dashboard Home)
$MYBLOG_LANG['_MB_CURRENT_DRAFTS']	= 	"Wersje robocze";
$MYBLOG_LANG['_MB_NO_DRAFTS']		= 	"Brak wersji roboczych";
$MYBLOG_LANG['_MB_SITE_STATS']		= 	"Statystyki";
$MYBLOG_LANG['_MB_VERSION']			= 	"Wersja";
$MYBLOG_LANG['_MB_TOTAL_ENTRIES']	= 	"Wpisów ³±cznie";
$MYBLOG_LANG['_MB_TOTAL_COMMENTS']	= 	"Komentarzy ³±cznie";
$MYBLOG_LANG['_MB_TOTAL_HITS']		= 	"Liczba ods³on";
$MYBLOG_LANG['_MB_MY_PROFILE']		= 	"Mój profil";
$MYBLOG_LANG['_MB_BLOG_DESCRIPTION']    = 	"Opis";
$MYBLOG_LANG['_MB_MY_BLOG_ENTRIES']	= 	"Wpisy";
$MYBLOG_LANG['_MB_TITLE']			= 	"Tytu³";
$MYBLOG_LANG['_MB_PUBLISHED_SHORT']	= 	"Opublikowany";
$MYBLOG_LANG['_MB_DATE']			= 	"Data";
$MYBLOG_LANG['_MB_ACTION']			= 	"Akcja";
$MYBLOG_LANG['_MB_EDIT']			= 	"Edytuj";
$MYBLOG_LANG['_MB_DELETE']			= 	"Usuñ";
$MYBLOG_LANG['_MB_WELCOME']			= 	"Witaj";

// Dashboard stuff (Dashboard Write/edit entry)
$MYBLOG_LANG['_MB_ADD_NEW_ENTRY']	= 	"Dodaj nowy wpis";
$MYBLOG_LANG['_MB_EDIT_ENTRY']		= 	"Edytuj wpis";
$MYBLOG_LANG['_MB_PUBLISH_STATUS']	= 	"Status publikacji";
$MYBLOG_LANG['_MB_PUBLISHED']		= 	"Opublikowane";
$MYBLOG_LANG['_MB_DRAFT']			= 	"Wersja robocza";
$MYBLOG_LANG['_MB_ADVANCED']		= 	"Opcje zaawansowane";
$MYBLOG_LANG['_MB_PUB_DATE']		= 	"Data publikacji";
$MYBLOG_LANG['_MB_PUB_TIME']		= 	"Czas publikacji";
$MYBLOG_LANG['_MB_TIME']			= 	"Czas";
$MYBLOG_LANG['_MBD_SEARCH_POSTS_INFO']	= 	"Zrób szybkie wyszukiwanie w¶ród postów i przeci±gnij odsy³acze aby siê do nich odwo³aæ";
$MYBLOG_LANG['_MBD_SEARCH_POSTS']	= 	"Przeszukaj wpisy";
$MYBLOG_LANG['_MBD_TAGS_INFO']		= 	"Dodaj tagi odzielaj±c je przecinkami lub kliknij na tag poni¿ej aby oznaczyæ ten wpis.";
$MYBLOG_LANG['_MBD_TAGS']			= 	"Tagi"; // dashboard 'Tags', different from frontpage
$MYBLOG_LANG['_MB_BLOG_CONTENT']	= 	"Zawarto¶æ";
$MYBLOG_LANG['_MB_FULLTEXT']		= 	"Pe³en tekst";
$MYBLOG_LANG['_MB_IMAGES_FILES']	= 	"Obrazki";
$MYBLOG_LANG['_MB_ADDITIONAL']		= 	"Dodatkowe opcje";
$MYBLOG_LANG['_MB_PERMALINK']		= 	"Permanentny link";
$MYBLOG_LANG['_MB_TRACKBACK_URLS']	= 	"Trackback URLs - oddzielone";
$MYBLOG_LANG['_MB_SAVE']			= 	"Zapisz";
$MYBLOG_LANG['_MB_DISCARD']			= 	"Anuluj";
$MYBLOG_LANG['_MBD_NO_PUBLISH']		= 	"Nie masz uprawnieñ, ¿eby dodaæ lub usun±æ wpis";

// Dashbaord Misc stuff
$MYBLOG_LANG['_MB_WRITE']			= 	"Nowy ...";
$MYBLOG_LANG['_MB_VIEW_BLOG']		= 	"Podgl±d";
$MYBLOG_LANG['_MB_BLOG_SAVED']		= 	"Wpis zachowany";
$MYBLOG_LANG['_MB_NO_POST']			= 	"Nie masz uprawnieñ, ¿eby dodaæ lub usun±æ wpis. Skontaktuj siê z administratorem.";

// build 24
$MYBLOG_LANG['_MB_ENTRY_DATE_TIME']	= "Entry Date/Time";
$MYBLOG_LANG['_MB_COMMENT']			= "Comment";
$MYBLOG_LANG['_MB_ALL_BLOG_ENTRIES']=	"All Blog Entries";
$MYBLOG_LANG['_MB_ALL_BLOGS']		=	"All Blogs";


// build 25
// RSS feed stuff
$MYBLOG_LANG['_MB_RSS_BLOG_ENTRIES']=	"Blog Entries";
$MYBLOG_LANG['_MB_RSS_BLOG_FOR']	=	"for"; // blog entries for xxuser
$MYBLOG_LANG['_MB_RSS_BLOG_TAGGED']	=	"tagged"; // blog entries for xxuser tagged xxtag
$MYBLOG_LANG['_MB_RSS_BLOG_KEYWORD']=	"with keywords"; //blog entries for xxuser tagged xxtag with keywords

// myblog cb tab stuff
$MYBLOG_LANG['_MB_CB_VIEW_ENTRIES'] =	"View Blog Entries"; 
$MYBLOG_LANG['_MB_CB_VIEW_COMMENTS']=	"View comments regarding this blog";
$MYBLOG_LANG['_MB_CB_BLOG_STATS']	=	"Blog Statistics";

// myblog dashboard
$MYBLOG_LANG['_MB_BLOG_DESC_UPDATED']=	"Blog description has been updated";
$MYBLOG_LANG['_MB_NOT_LOGGEDIN']	=	"You are not logged in. Please login to manage your blog.";
$MYBLOG_LANG['_MB_EDIT_ERROR']		=	"Trying to edit someone else's entry?";

// myblog image browser
$MYBLOG_LANG['_MB_ALL_IMAGES']		= 	"All Images";
$MYBLOG_LANG['_MB_IMAGES_THIS_BLOG']=	"Images uploaded for this blog entry";
$MYBLOG_LANG['_MB_UPLOAD']			=	"Upload";
$MYBLOG_LANG['_MB_IMAGEB_INSTRUCTIONS']	=	"Double click on image to add to content.";
$MYBLOG_LANG['_MB_IMAGEB_INFO']		= 	"Newly uploaded images will not be saved until you save this message";
$MYBLOG_LANG['_MB_FILE_UPLOAD']		= 	"File Upload";
$MYBLOG_LANG['_MB_MAX_IMG_SIZE']	=	"Max size";

// date/time stuff
// $MYBLOG_LANG['Monday']				=	"poniedzialek";
// $MYBLOG_LANG['Tuesday']				=	"wtorek";
// $MYBLOG_LANG['Wednesday']			=	"sroda";
// $MYBLOG_LANG['Thursday']			=	"czwartek";
// $MYBLOG_LANG['Friday']				=	"piatek";
// $MYBLOG_LANG['Saturday']			=	"sobota";
// $MYBLOG_LANG['Sunday']				=	"niedziela";
// $MYBLOG_LANG['January']				=	"styczen";
// $MYBLOG_LANG['February']			=	"luty";
// $MYBLOG_LANG['March']				=	"marzec";
// $MYBLOG_LANG['April']				=	"kwiecien";
// $MYBLOG_LANG['May']					=	"maj";
// $MYBLOG_LANG['June']				=	"czerwiec";
// $MYBLOG_LANG['July']				=	"lipiec";
// $MYBLOG_LANG['August']				=	"sierpien";
// $MYBLOG_LANG['September']			=	"wrzesien";
// $MYBLOG_LANG['October']				=	"pazdziernik";
// $MYBLOG_LANG['November']			=	"listopad";
// $MYBLOG_LANG['December']			=	"grudzien";
//month short forms
// $MYBLOG_LANG['Jan']					=	"sty";
// $MYBLOG_LANG['Feb']					=	"luty";
// $MYBLOG_LANG['Mar']					=	"marz";
// $MYBLOG_LANG['Apr']					=	"kwi";
// $MYBLOG_LANG['May']					=	"maj";
// $MYBLOG_LANG['Jun']					=	"czer";
// $MYBLOG_LANG['Jul']					=	"lip";
// $MYBLOG_LANG['Aug']					=	"sier";
// $MYBLOG_LANG['Sep']					=	"wrz";
// $MYBLOG_LANG['Oct']					=	"paz";
// $MYBLOG_LANG['Nov']					=	"lis";
// $MYBLOG_LANG['Dec']					=	"grud";

/**
 * v1.0 Dashboard (NEW!)
 * Do not modify any values that contain {SOMEVALUE}!
 * It may break your translations.
 **/
$MYBLOG_LANG['TPL_DB_PAGETITLE']    = 'My Blog Writepad';
$MYBLOG_LANG['TPL_DB_HEADER']       = 'MyBlog Editor';
$MYBLOG_LANG['TPL_DB_DEFAULTTITLE'] = 'Blog title (click to edit)';
$MYBLOG_LANG['TPL_DB_ERRORHIDE']    = 'Hide';
$MYBLOG_LANG['TPL_DB_SAVE']     	= 'Save';
$MYBLOG_LANG['TPL_DB_CLOSESAVE'] 	= 'Save and Close';
$MYBLOG_LANG['TPL_DB_PUBSTATUS'] 	= 'Publish Information';
$MYBLOG_LANG['TPL_DB_PUBLISH'] 		= 'Publish';
$MYBLOG_LANG['TPL_DB_UNPUBLISH'] 	= 'Unpublish';
$MYBLOG_LANG['TPL_DB_PUBDATE'] 		= 'Publish date';
$MYBLOG_LANG['TPL_DB_TAGS']			= 'Tags';
$MYBLOG_LANG['TPL_DB_CATEGORY']   	= 'Select a category';
$MYBLOG_LANG['TPL_DB_ADDTAG']       = 'Add a new tag';
$MYBLOG_LANG['TPL_DB_INVALIDTAG']  	= 'Invalid tag!';
$MYBLOG_LANG['TPL_DB_SUCCESSADDTAG'] = 'Tag {TAGNAME} successfully added!';
$MYBLOG_LANG['TPL_DB_EXISTSTAG'] 	= 'Tag {TAGNAME} exists!';
$MYBLOG_LANG['TPL_DB_ADDLINKTAG']  	= 'Add';
$MYBLOG_LANG['TPL_DB_IMGDOC']   	= 'Images and document';
$MYBLOG_LANG['TPL_DB_IMGUPLOAD']	= 'Upload';
$MYBLOG_LANG['TPL_DB_SEARCH']       = 'Seach older post';
$MYBLOG_LANG['TPL_DB_INFOSEARCH']   = 'Do a quick search on existing posts and drag the links to the editor to link to them';
$MYBLOG_LANG['TPL_DB_JOMCOMMENT']   = 'Jom Comment';
$MYBLOG_LANG['TPL_DB_ENABLECOMMENT']   = 'Enable Comments';
$MYBLOG_LANG['TPL_DB_DISABLECOMMENT']   = 'Disable Comments';
$MYBLOG_LANG['TPL_DB_DEFAULTCOMMENT']   = 'Default';
$MYBLOG_LANG['TPL_DB_COMMENTINFO']  = 'Enable, disable or use default status for comments on this article.';
$MYBLOG_LANG['TPL_DB_WORDCOUNT']        = 'Word count';

$MYBLOG_LANG['TPL_MNU_MYBLOGS']         = 'My Entries';
$MYBLOG_LANG['TPL_MNU_COMMENTS']        = 'Comments';
$MYBLOG_LANG['TPL_MNU_PREF']            = 'Preferences';
$MYBLOG_LANG['TPL_MNU_STATS']           = 'Stats';
$MYBLOG_LANG['TPL_ADM_HEADING']         = 'My Entries';
$MYBLOG_LANG['TPL_ADM_TITLE']			= 'Title';
$MYBLOG_LANG['TPL_ADM_DATE']			= 'Date';
$MYBLOG_LANG['TPL_ADM_HITS']			= 'Hits';
$MYBLOG_LANG['TPL_ADM_COMMENTS']		= 'Comments';
$MYBLOG_LANG['TPL_ADM_NOBLOGS']			= 'No Blog Entries';
$MYBLOG_LANG['TPL_ADM_JSDELETE']			= 'Are you sure you want to delete this blog entry?';
$MYBLOG_LANG['TPL_ADM_DELETE']          = 'Delete';
$MYBLOG_LANG['TPL_ADM_EDIT']            = 'Edit';
$MYBLOG_LANG['TPL_ADM_NEWENTRY']        = 'Write New Entry';

// New 2.0
$MYBLOG_LANG['TPL_NOTIFY_MAILTITLE']		= 'A new blog entry has been posted';
$MYBLOG_LANG['TPL_NOTIFY_UPDATEMAILTITLE']	= 'A blog entry has been updated';

$MYBLOG_LANG['_TPL_SRCH_TAGS']			= 'Tags';

$MYBLOG_LANG["_MB_BLOGGERS_TITLE"]	= '\'s Blog';
?>