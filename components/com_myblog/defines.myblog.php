<?php

(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

$cms = &cmsInstance('CMSCore');

define('MY_COM_PATH', 			dirname(__FILE__));
define('MY_COM_LIVE', 			$cms->get_path('live').'/components/com_myblog');
define('MY_ADMIN_COM_PATH', 	dirname(dirname(MY_COM_PATH)) . '/administrator/components/com_myblog/');
define('MY_LIBRARY_PATH', 		MY_COM_PATH . '/libraries');
define('MY_TASK_PATH', 			MY_COM_PATH . '/task');
define('MY_FRONTVIEW_PATH', 	MY_COM_PATH . '/frontview');
define('MY_FRONTADMIN_PATH', 	MY_COM_PATH . '/frontadmin');
define('MY_TEMPLATE_PATH', 		MY_COM_PATH . "/templates");
define('MY_MODEL_PATH',			MY_COM_PATH . '/model');
define('MY_CACHE_PATH',		$cms->get_path('root') . '/components/libraries/cmslib/cache');

define('MY_DEFAULT_LIMIT', 30);

define('MY_TOOLBAR_HOME', 	 	'home');
define('MY_TOOLBAR_BLOGGER', 	'blogger');
define('MY_TOOLBAR_TAGS',		'category');
define('MY_TOOLBAR_SEARCH',		'search');
define('MY_TOOLBAR_FEED',		'feed');
