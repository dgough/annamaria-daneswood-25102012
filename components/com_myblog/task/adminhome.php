<?php
(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

include_once(MY_COM_PATH . '/task/base.php');
class MyblogAdminhomeTask extends MyblogBaseController{

	function MyblogAdminhomeTask(){
		$this->cms		=& cmsInstance('CMSCore');
		$this->toolbar	= MY_TOOLBAR_BLOGGER;
	}

	function display()
	{
		global $_MY_CONFIG;
		include(MY_COM_PATH.'/language/'.$_MY_CONFIG->language);

		$cms = &cmsInstance('CMSCore');
		$cms->load('libraries', 'user');
		$cms->load('helper', 'url');

 		$limitstart = cmsGetVar('limitstart', 0, 'REQUEST');
 		$limitstart = intval($limitstart);
		$secid = $_MY_CONFIG->get('postSection');
		$limit		= cmsGetVar( 'limit' , MY_DEFAULT_LIMIT , 'REQUEST' );

		$secid = $_MY_CONFIG->get('postSection');
		$limit = $limitstart ? "LIMIT $limitstart, ".MY_DEFAULT_LIMIT : 'LIMIT '.MY_DEFAULT_LIMIT;
		
		$query	= "SELECT * FROM #__content WHERE `created_by`='{$cms->user->id}' "
				. "AND `sectionid`='{$secid}' "
				. "ORDER BY created DESC, id DESC "
				. $limit;

		$this->cms->db->query($query);
		$entries = $this->cms->db->get_object_list();

		// Add the actions buttons and modify the date format
		for($i = 0; $i < count($entries); $i++)
		{
		    // Htmlspecialchars the title
			$entries[$i]->title    = htmlspecialchars($entries[$i]->title);
			$entries[$i]->action = '[ edit | delete ]';

			$query	= "SELECT COUNT(*) FROM #__jomcomment AS a WHERE a.contentid='" .$entries[$i]->id . "' AND a.option='com_myblog'";
			$count	= $this->cms->db->get_value($query);
			$entries[$i]->commentCount = $count;
		}

		// Add pagination
		$this->cms->load('libraries', 'pagination');
		$config = array();

		$query	= "SELECT count(*) FROM #__content WHERE `created_by`='{$cms->user->id}' AND sectionid='{$secid}' ORDER BY created";
		$this->cms->db->query( $query );
		$total	= $this->cms->db->get_value();

		$pagination	= myPagination( $total , $limitstart , MY_DEFAULT_LIMIT );

		if(!class_exists('AzrulJXTemplate'))
			include_once($this->cms->get_path('plugins'). '/system/pc_includes/template.php');

		$tpl = new AzrulJXTemplate();
		$tpl->set('postingRights', myGetUserCanPost());
		$tpl->set('publishRights', myGetUserCanPublish());
		$tpl->set('myitemid', myGetItemId());
		$tpl->set('pagination', $pagination->links );
		$tpl->set('myentries', $entries);
		$tpl->set( 'limit' , $limit);
		$html = $tpl->fetch(MY_TEMPLATE_PATH."/admin/home.html");
		$html = str_replace(array_keys($MYBLOG_LANG), array_values($MYBLOG_LANG), $html);

		return $html;
	}
}


