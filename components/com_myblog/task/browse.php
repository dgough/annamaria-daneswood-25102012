<?php

(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

include_once(MY_COM_PATH . '/task/browse.base.php');


class MyblogBrowseTask extends MyblogBrowseBase{
	
	function MyblogBrowseTask(){
		parent::MyblogBrowseBase();
		$this->toolbar = MY_TOOLBAR_HOME;
	}
	
}
