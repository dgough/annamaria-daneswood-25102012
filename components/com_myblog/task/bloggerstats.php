<?php
(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

include_once(MY_COM_PATH . '/task/base.php');
class MyblogBloggerstatsTask extends MyblogBaseController{

	function MyblogBloggerstatsTask(){
		$this->cms		=& cmsInstance('CMSCore');
		$this->toolbar	= MY_TOOLBAR_BLOGGER;
	}
	
	function display(){
		$cms =& cmsInstance('CMSCore');
		$cms->load('libraries', 'user');
		
		$desc = $this->cms->db->query("SELECT `description` FROM #__myblog_user WHERE `user_id` = '{$cms->user->id}'");
		$desc = $this->cms->db->get_value();
		
		if(!class_exists('AzrulJXTemplate'))
			include_once($this->cms->get_path('plugins'). '/system/pc_includes/template.php');
		
		$tpl = new AzrulJXTemplate();
		$tpl->set('num_entries', myCountUserEntry($cms->user->id));
				
		// Need to check if integrations with jomcomment is enabled.
		if(myGetJomComment()){
		    $tpl->set('jomcomment',true);
		    $tpl->set('num_comments', myCountUserComment($cms->user->id));
		}
		
		$tpl->set('num_hits', myCountUserHits($cms->user->id));
		$tpl->set('tags', myGetUsedTags($cms->user->id));
		$tpl->set('myitemid', myGetItemId());
		$tpl->set('description', $desc);
		$html = $tpl->fetch(MY_TEMPLATE_PATH."/admin/blogger_stats.html");
		return $html;
	}
}