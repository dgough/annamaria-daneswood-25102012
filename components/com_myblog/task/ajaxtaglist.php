<?php
(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

// Include own library.
if(!defined('CMSLIB_DEFINED'))
	require_once(dirname(dirname(dirname(dirname(__FILE__)))) . '/components/libraries/cmslib/spframework.php');

$cms	=& cmsInstance('CMSCore');

$cms->load('libraries','user');

if($cms->user->id == '0'){
	echo 'Not authenticated';
	return;
}

$query	= cmsGetVar('q','','GET');
$query	= strtolower($query);

if(!$query)
	return;

$strSQL	= "SELECT `name` FROM #__myblog_categories";
$cms->db->query($strSQL);

$tags	= $cms->db->get_object_list();

foreach ($tags as $key) {
	if (strpos(strtolower($key->name), $query) !== false) {
		echo $key->name . "|\n";
	}
}