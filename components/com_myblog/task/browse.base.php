<?php

(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

include_once(MY_COM_PATH . '/task/base.php');
include_once(MY_LIBRARY_PATH . '/avatar.class.php');
include_once(MY_COM_PATH . '/libraries/plugins.class.php');

class MyblogBrowseBase extends MyblogBaseController{
	var $entries;
	var $totalEntries;
	var $filters;
	var $html;
	var $limit;
	var $limitstart;
	var $_plugins	= null;
	
	function MyblogBrowseBase(){
		global $_MY_CONFIG;
				
		parent::MyblogBaseController();

		$this->_plugins	= new MYPlugins();
		$this->toolbar = MY_TOOLBAR_HOME;
		$this->limit = isset ($_GET['limit']) ? $_GET['limit'] : $_MY_CONFIG->get('numEntry');
		$this->limitstart = isset ($_GET['limitstart']) ? $_GET['limitstart'] : 0;
	}
	
	function _header(){
		return parent::_header();
	}
	
	function display(){
		global $_MY_CONFIG, $MYBLOG_LANG;

		if(!myAllowedGuestView('intro')){
			$template		= new AzrulJXTemplate();
			$content		= $template->fetch($this->_getTemplateName('permissions'));
			return $content;
		}
		
		$this->cms->load('libraries','user');
		$this->cms->load('helper','url');
		$this->cms->load('libraries','trunchtml');
		
		myAddPageTitle($MYBLOG_LANG['_MB_ALL_BLOG_ENTRIES']);
		
		// Get the entries data
		$this->setData(); 
		$this->_getEntries($this->filters);
		
		$tpl = new AzrulJXCachedTemplate(time() . $this->cms->user->usertype . $_MY_CONFIG->get('template'));
		$html = '';
		
		// Prepare the entries
		array_walk($this->entries, array($this, '_prepareData') );
		
		$entryArray = $tpl->object_to_array($this->entries);
		$tpl->set('entry', $entryArray);
		$tpl->set('showAnchor', $_MY_CONFIG->get('anchorReadmore') ? '#readmore' : '');
		unset($entryArray);
		
				
		$template = $this->_getTemplateName('index');
		$html = $tpl->fetch_cache($template);

		// Fix for IIS webservers that doesn't have REQUEST_URI
		if ( !isset($_SERVER['REQUEST_URI']) ) {

			$_SERVER['REQUEST_URI'] = substr($_SERVER['PHP_SELF'],1 );
		
			if (isset($_SERVER['QUERY_STRING'])) {
				$_SERVER['REQUEST_URI'].='?'.$_SERVER['QUERY_STRING'];
			}
		}

		if(!isset($_SERVER['QUERY_STRING'])){
			$_SERVER['QUERY_STRING'] = ''; //new
		
			foreach($_GET as $key => $val){
				$_SERVER['QUERY_STRING'] .= $key . '=' . $val . '&';
			}
			$_SERVER['QUERY_STRING'] = rtrim($_SERVER['QUERY_STRING'] , '&');
		}
		// For page navigation
		$queryString = $_SERVER['QUERY_STRING'];
		$queryString = preg_replace("/\&limit=[0-9]*/i", "", $queryString);
		$queryString = preg_replace("/\&limitstart=[0-9]*/i", "", $queryString);
		
		$pageNavLink = $_SERVER['REQUEST_URI'];
		$pageNavLink = preg_replace("/\&limit=[0-9]*/i", "", $pageNavLink);
		$pageNavLink = preg_replace("/\&limitstart=[0-9]*/i", "", $pageNavLink);

		if ($this->totalEntries > $this->limit) {
			require_once ($this->cms->get_path('root') . "/includes/pageNavigation.php");
			$pageNav = new mosPageNav($this->totalEntries, $this->limitstart, $this->limit);
			$html .= '<div class="my-pagenav">' . $pageNav->writePagesLinks('index.php?' . $queryString) . '</div>';
		}
		
		// Translate language
		$html = str_replace(array_keys($MYBLOG_LANG), array_values($MYBLOG_LANG), $html);
		
		return $html;	
	}
	
	function setData(){
// 		$searchby = array('limit' => $limit,
// 		'limitstart' => $limitstart,
// 		'authorid' => $author,
// 		'category' => $category,
// 		'jcategory' => $jcategory,
// 		'search' => $search,
// 		'archive' => $archive,
// 		);
		
		$searchby = array();
		
		// Request might contain 'category'
		if (cmsGetVar('category', '', 'REQUEST') != '') {
// 
// 			// Test if category passed here are joomla's category by testing the type of the variable
// 			if(is_numeric($category)){
// 				$category = intval(urldecode(cmsGetVar('category', '', 'REQUEST')));
// 				$jcategory = $category;
// 				$jCatName = myGetJoomlaCategoryName($category);
// 			} else {
// 				$category = strval(urldecode(cmsGetVar('category', '', 'REQUEST')));
// 				$category = str_replace("+", " ", $category);
// 				$searchby['category'] = $category;
// 			}
				$category = strval(urldecode(cmsGetVar('category', '', 'REQUEST')));
				$category = str_replace("+", " ", $category);
				$searchby['category'] = $category;
		}


		if (cmsGetVar('archive', '', 'REQUEST') != '' ) {
		
			$archive = urldecode(cmsGetVar('archive', '', 'REQUEST'));

			// Joomla 1.5 might convert the '-' to ':'
			$archive = str_replace(':', '-', $archive);
			$archive = date("Y-m-d 00:00:00", strtotime(str_replace("-", " ", "01 " . $archive)));
			$searchby['archive']	= $archive;
		}

		$this->filters = $searchby;
	}
	
	// Get all required variables for entries display
	function _prepareData(&$row, $key){
		global $_MY_CONFIG, $Itemid , $MYBLOG_LANG, $mainframe;

		// Load plugins
		$this->_plugins->load();

		// Test if there is blogger in the url.
		$blogger		= cmsGetVar('blogger' , '' , 'GET');
		
		if( $blogger != '' )
			myAddPageTitle( $blogger . $MYBLOG_LANG['_MB_BLOGGERS_TITLE'] );
		// add permalink		
		$row->permalink = myGetPermalinkUrl($row->id , '' , $blogger);
		
		// Change all relative url to absolute url
		$row->introtext = str_replace('src="images', 'src="'. $this->cms->get_path('live') .'/images', $row->introtext);
		$row->fulltext  = str_replace('src="images', 'src="'. $this->cms->get_path('live') .'/images',  $row->fulltext);
		
		$row->author = myGetAuthorName($row->created_by, $_MY_CONFIG->get('useFullName'));
		$row->authorLink = cmsSefAmpReplace("index.php?option=com_myblog&blogger=" . urlencode(myUserGetName($row->created_by , $_MY_CONFIG->get('useFullName'))) . "&Itemid=$Itemid");
		$row->categories = myCategoriesURLGet($row->id, true);

		$row->createdFormatted =  cmsFormatDate($row->created, $_MY_CONFIG->get('dateFormat') , $mainframe->getCfg('offset'));

		// We need to format the date correctly and add the necessary offset back.
		$row->created			= cmsFormatDate($row->created, '%Y-%m-%d %H:%M:%S', $mainframe->getCfg('offset'));

		// Add readmore link if necessary
		$row->readmore	= ($_MY_CONFIG->get('useIntrotext') == '1') ? '1' : '0';
		
		if($_MY_CONFIG->get('necessaryReadmore') == '1' && $row->readmore == '1')
		{
			if($row->introtext && empty($row->fulltext) )
			{
				// Check for the number of X <p> tags in the introtext that is set in the back end of My Blog
				// to determine whether to display the read more link.
				$count = blogContent::getParagraphCount($row->introtext);
				if( $count <= $_MY_CONFIG->get('autoReadmorePCount') )
				{
					$row->readmore = '0';
				}
			}
			else if( empty($row->introtext) && $row->fulltext )
			{
				// Check for the number of X <p> tags in the fulltext that is set in the back end of My Blog
				// to determine whether to display the read more link.
				$count = blogContent::getParagraphCount($row->fulltext);
				
				if( $count <= $_MY_CONFIG->get('autoReadmorePCount') )
				{
					$row->readmore = '0';
				}
			}
		}
				
		blogContent::getBrowseText($row);
		
		$row->comments = ($_MY_CONFIG->get('useComment') == "1") ? myCommentsURLGet($row->id, true) : '';

		$avatar	= 'My' . ucfirst($_MY_CONFIG->get('avatar')) . 'Avatar';
		$avatar	= new $avatar($row->created_by);
		
		$row->avatar	= $avatar->get();
		$params			= $this->_buildParams();
		
		if ($_MY_CONFIG->get('mambotFrontpage')=="1"){
			$row->beforeContent = $this->_plugins->trigger('onBeforeDisplayContent', $row, $this->_buildParams(), 0);
			$this->_plugins->trigger('onPrepareContent', $row, $params, 0);			
			$row->afterContent	= @$this->_plugins->trigger('onAfterDisplayContent', $row, $this->_buildParams(), 0);
			if ($row->afterContent != "") $row->afterContent = "<br/>" . $row->afterContent;
		}

		// Clean up the final text
		$row->text 	= str_replace(array('{mosimage}', 
			'{mospagebreak}', 
			'{readmore}',
			'{jomcomment}',
			'{!jomcomment}'), '', $row->text);

	}
	
	function _processPlugins(){
	}
	
	// process the entries and return $html
	function _getEntriesHtml(){
		
	}
	
	
	// Return an array of entries based on the given search paramaters 
	function _getEntries(&$searchby){
		global $_MY_CONFIG;
		
		$db =& cmsInstance('CMSDb');
		$cms =& cmsInstance('CMSCore');
		
		$limit 		= isset($searchby['limit']) 	 ? intval($searchby['limit']): $this->limit;
		$limitstart = isset($searchby['limitstart']) ? intval($searchby['limitstart']): $this->limitstart;
		$jcategory 	= isset($searchby['jcategory'])  ? intval($searchby['jcategory']): 0;
		
		$authorid 	= isset($searchby['authorid']) 	 ? $db->_escape($searchby['authorid']): "";
		$category 	= isset($searchby['category']) 	 ? $db->_escape($searchby['category']): "";
		$search 	= isset($searchby['search']) 	 ? $db->_escape($searchby['search']): "";
		$archive 	= isset($searchby['archive']) 	 ? $db->_escape($searchby['archive']): "";
		
		$sections       = $_MY_CONFIG->get('managedSections');
		$selectMore		= "";
		$searchWhere	= "";
		$primaryOrder	= "";
		$use_tables		= "";
		
		#search by tags
		if (!empty ($category) && empty($jcategory)) {
			$categoriesArray = explode(",", $category);
			$categoriesList = "0";
			foreach ($categoriesArray as $mycat) {
				$mycat = $db->_escape(trim($mycat));
				
				// Use LIKE tag search since we might get confused with  space and dash 
				// get mixed up
				$mycat = str_replace(' ', '%', $mycat);
				$db->query("SELECT id FROM #__myblog_categories WHERE name LIKE '$mycat' OR `slug` LIKE '$mycat'");
				$searchCategoryId = $db->get_value();
				
				if ($searchCategoryId) {
					$categoriesList .= ",";
					$categoriesList .= "$searchCategoryId";
				}
			}
			
			$use_tables .= ",#__myblog_categories as b,#__myblog_content_categories as c ";
			$searchWhere .= " AND (b.id=c.category AND c.contentid=a.id AND b.id IN ($categoriesList)) ";
		}
		
		# seach by joomla category
		if (!empty ($jcategory) && $jcategory > 0) {		
			$searchWhere .= " AND (a.catid='$jcategory') ";
		}
		
		# search  by blogger
		if (!empty ($authorid) or $authorid == "0") {
			$searchWhere .= " AND a.created_by IN ($authorid)";
		}
		
		# search keywords
		if (!empty ($search)) {
			$searchWhere .= " AND match (a.title,a.fulltext,a.introtext) against ('$search' in BOOLEAN MODE) ";
		}
		
		# display entries for a specific month/year
		if (!empty ($archive)) {
			$searchWhere .= " AND a.created BETWEEN '$archive' AND date_add('$archive', INTERVAL 1 MONTH) ";
		}
		
		$query = " SELECT count(*) FROM #__content as a,#__myblog_permalinks as p $use_tables WHERE a.state=1 and a.publish_up < '" . date( 'Y-m-d H:i:s' ) . "' and a.sectionid in ($sections) and a.id=p.contentid $searchWhere";
		$db->query($query);
		$total = $db->get_value();
		$searchby['total'] = $total;
		$this->totalEntries = $total;
		
		// New version, no permalink joins
		$query = " SELECT a.*, round(r.rating_sum/r.rating_count) as rating, r.rating_count $selectMore 
			FROM (#__content as a $use_tables ) 
				left outer join #__content_rating as r 
					on (r.content_id=a.id) 
			WHERE a.state=1 and a.publish_up < '" . date( 'Y-m-d H:i:s' ) . "' 
				and a.sectionid in ($sections) 
				$searchWhere ORDER BY $primaryOrder a.created DESC,a.id DESC LIMIT $limitstart,$limit";
		
		
		$db->query($query);
	
		$rows = $db->get_object_list();		
		$this->entries = $rows;
		unset($rows);
	}
}
