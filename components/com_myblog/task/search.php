<?php
(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

include_once(MY_COM_PATH . '/task/base.php');
include_once(MY_LIBRARY_PATH . '/plugins.class.php');
/***
 * Show the search page
 */
 
class MyblogSearchTask extends MyblogBaseController{
	var $cms			= null;
	var $_resultLength	= 250;
	var $_plugins		= null;
	
	function MyblogSearchTask(){
		$this->cms		=& cmsInstance('CMSCore');
		$this->toolbar	= MY_TOOLBAR_SEARCH;
		
		
		$this->_plugins	= new MYPlugins();
	}
	
	function display(){
		global $Itemid, $mainframe, $MYBLOG_LANG, $_MY_CONFIG;
		$this->cms->load('libraries','user');
		$this->cms->load('helper','url');
		
		myAddPageTitle('Search blog entry');

		$template	= new AzrulJXCachedTemplate(time() . $this->cms->user->usertype . $_MY_CONFIG->get('template'));
		
		$blogger		= cmsGetVar('blogger','','POST');
		$keyword		= cmsGetVar('keyword','','POST');
		$category		= cmsGetVar('category','','POST');

		// Display form for user
		$searchURL	= cmsSefAmpReplace('index.php?option=com_myblog&task=search&Itemid=' . myGetItemId());
		
		$template->set('searchURL', $searchURL);
		$template->set('Itemid', myGetItemId());
		$results	= false;
		if((!empty($blogger) && isset($blogger))|| (!empty($keyword) && isset($keyword)) || (!empty($category) && isset($category)) ){
			// Post action, perform search
			$results	= $this->_search(array('blogger' => $blogger, 'keyword' => $keyword, 'category' => $category));
		}
		$template->set('blogger', $blogger);
		$template->set('keyword', $keyword);
		$template->set('category', $category);
		$template->set('results', $results);
		$content	= $template->fetch($this->_getTemplateName('search'));
		
		// Translate language
		$content = str_replace(array_keys($MYBLOG_LANG), array_values($MYBLOG_LANG), $content);
		
		return $content;
	}
	
	/**
	 * _search
	 * params: $filter (assoc array)
	 **/	 	 	
	function _search($filter){
		global $_MY_CONFIG;

		$sections	= $_MY_CONFIG->get('managedSections');
		$strSQL		= "SELECT * FROM #__content WHERE ";

		$blogger	= isset( $filter['blogger'] ) ? $this->cms->db->_escape( $filter['blogger'] ) : '';
		$keyword	= isset( $filter['keyword'] ) ? $this->cms->db->_escape( $filter['keyword'] ) : '';
		$category	= isset( $filter['category'] ) ? $this->cms->db->_escape( $filter['category'] ) : '';

		if(!empty($blogger)){
				$strSQL	.= "`created_by`='" . myGetAuthorId($blogger) ."'";
		}

		if(!empty($keyword)){
			if(!empty($blogger))
			{
				$strSQL		.= " AND (`title` LIKE '%{$keyword}%' "
							 . "OR `introtext` LIKE '%{$keyword}%' "
							 . "OR `fulltext` LIKE '%{$keyword}%')";
			}else{
				$strSQL		.= "`title` LIKE '%{$keyword}%' "
							 . "OR (`introtext` LIKE '%{$keyword}%' "
							 . "OR `fulltext` LIKE '%{$keyword}%')";
			}
		}

		if(!empty($category)){
			$category	= myGetCategoryId($category);
			if(!empty($blogger) || !empty($keyword)){
				$strSQL		.= " AND (catid='{$category}')";
			}else{
				$strSQL		.= "catid='{$category}'";
			}
		}
		
		$strSQL	.= "AND `sectionid` IN ({$sections})";

		$this->cms->db->query($strSQL);	
		$results	= $this->cms->db->get_object_list();
		$this->_format($results);
		return $results;
	}
	
	function _format(&$rows){
		global $_MY_CONFIG;
		// Load Plugins
		$this->_plugins->load();
		
		// Format results
		for($i =0; $i < count($rows); $i++){
			$row    =& $rows[$i];
			
			// Merge introtext and fulltext.
			$row->text	= $row->introtext . $row->fulltext;
			$row->text	= substr($row->text, 0, $this->_resultLength) . '...';
			
			$row->user	= myGetAuthorName($row->created_by, $_MY_CONFIG->get('useFullName'));
			$row->user	= $row->user;
			$row->link	= myGetPermalinkURL($row->id);
			$row->userlink	= cmsSefAmpReplace('index.php?option=com_myblog&blogger=' . myGetAuthorName($row->created_by));
			$row->date	= cmsFormatDate($row->created, $_MY_CONFIG->get('dateFormat'));
		}
	}
}
