<?php
(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

include_once(MY_COM_PATH . '/task/base.php');
class MyblogShowcommentsTask extends MyblogBaseController{

	function MyblogShowcommentsTask(){
		$this->cms		=& cmsInstance('CMSCore');
		$this->toolbar	= MY_TOOLBAR_BLOGGER;
	}
	
	function display(){
		global $_MY_CONFIG;
		
		$cms =& cmsInstance('CMSCore');
		$this->cms->load('libraries', 'trunchtml');
		
		// get List of content id by this blogger
		$secid = $_MY_CONFIG->get('postSection');
			
		$this->cms->db->query("SELECT `id` FROM #__content WHERE `created_by`='{$cms->user->id}' AND sectionid='{$secid}' ");
		$contents = $this->cms->db->get_object_list();
		$sections = array();
		foreach($contents as $row){
			$sections[] = $row->id;
		}
			
		// Make sure that there are indeed some article written by the author
		if(!empty($sections)){
			$limitstart = cmsGetVar('limitstart','', 'GET');			
			$limit = $limitstart ? "LIMIT $limitstart, ".MY_DEFAULT_LIMIT : 'LIMIT '.MY_DEFAULT_LIMIT;
			$this->cms->db->query("SELECT * FROM #__jomcomment WHERE (`option`='com_content' OR `option`='com_myblog') AND `contentid` IN (". implode(',', $sections).") ORDER BY `date` DESC $limit");
			$comments = $this->cms->db->get_object_list(); 
			
			// Add pagination
			$this->cms->load('libraries', 'pagination');
			$config = array();
					
			$this->cms->db->query("SELECT count(*) FROM #__jomcomment WHERE (`option`='com_content' OR `option`='com_myblog') AND `contentid` IN (". implode(',', $sections).")");
			$config['total_rows'] = $this->cms->db->get_value();
			$config['base_url'] = $_SERVER['REQUEST_URI'];
			$config['per_page'] = MY_DEFAULT_LIMIT;
			
			$this->cms->pagination->initialize($config);
			$pagination =  $this->cms->pagination->create_links();
		}else{
			$pagination = '';
			$comments = array();
		}
		
		for($i = 0; $i < count($comments); $i ++){
			if($comments[$i]->referer == ''){
				$comments[$i]->referer	= myGetPermalinkURL($comments[$i]->contentid) . '#comment-' . $comments[$i]->id;
			}
		}
		
		$tpl = new AzrulJXTemplate();
		$tpl->set('myitemid', myGetItemId());
		$tpl->set('pagination', $pagination);
		$tpl->set('comments', $comments);
		$html = $tpl->fetch(MY_TEMPLATE_PATH."/admin/comments.html");

		return $html;
	}
}