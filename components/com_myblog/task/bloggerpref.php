<?php
(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

include_once(MY_COM_PATH . '/task/base.php');
class MyblogBloggerprefTask extends MyblogBaseController{

	function MyblogBloggerprefTask(){
		$this->cms		=& cmsInstance('CMSCore');
		$this->toolbar	= MY_TOOLBAR_BLOGGER;
	}
	
	function display(){
		global $_MY_CONFIG;
		
		$cms =& cmsInstance('CMSCore');
		$cms->load('libraries', 'user');
		$cms->load('model', 'user', 'com_myblog');
		$cms->load('helper', 'url');
		
		// Check if user submitted a profile change.
		$profile    = cmsGetVar('blog-subtitle','', 'POST');
		$feedburner	= cmsGetVar('feedburnerURL','','POST');
		$title		= cmsGetVar('blog-title', '', 'POST');
		
		$titleColor	= cmsGetVar('blog-title-color', '', 'POST');
		$descColor	= cmsGetVar('blog-subtitle-color', '', 'POST');
		
		$save       = false;

		if(isset($profile) && !empty($profile)){
			$profile    = strip_tags($profile);
		    $user = new myblogUser();
			$user->load($cms->user->id);
			$this->cms->db->update('#__myblog_user', array('description' => $profile), array('user_id' => $user->user_id));
			$save   	= true;
		}
		
		if(isset($feedburner) && !empty($feedburner) && ($_MY_CONFIG->get('userUseFeedBurner'))){
			$user	= new myblogUser();
			$user->load($cms->user->id);
			$cms->db->update('#__myblog_user', array('feedburner' => $feedburner), array('user_id' => $user->user_id));
			$save	= true;
		}
		
		if(isset($title) && !empty($title)){
			$title	= strip_tags($title);
		    $user = new myblogUser();
			$user->load($cms->user->id);
			$this->cms->db->update('#__myblog_user', array('title' => $title), array('user_id' => $user->user_id));
			$save   	= true;
		}

		if(isset($titleColor) && !empty($titleColor) || isset($descColor) && !empty($descColor)){
			$style	= Array();
			
			if($titleColor)
				$style['blog-title-color']		= $titleColor;
				
			if($descColor)
				$style['blog-subtitle-color']	= $descColor;
			
			$style	= serialize($style);
			$user	= new myblogUser();
			$user->load($cms->user->id);
			$this->cms->db->update('#__myblog_user', array('style' => $style), array('user_id' => $user->user_id));
			$save   	= true;
		}
		
		$user = new myblogUser();
		$user->load($cms->user->id);
		$desc			= myGetAuthorDescription($cms->user->id);
		$showFeedburner	= $_MY_CONFIG->get('userUseFeedBurner') ? true : false;
		
		if($showFeedburner){
			$feedburner	= $user->feedburner;
		}
		
		if(!class_exists('AzrulJXTemplate'))
			include_once($this->cms->get_path('plugins'). '/system/pc_includes/template.php');
			
		$tpl = new AzrulJXTemplate();

		$tpl->set('title',$title);
		$tpl->set('showFeedburner', $showFeedburner);
		$tpl->set('feedburner', $feedburner);
		$tpl->set('saved', $save ? 'Profile updated.' : '');
		$tpl->set('myitemid', myGetItemId());
		
		$tpl->set('description', stripslashes($user->description));
		$tpl->set('descColor', $user->getStyle('blog-subtitle-color'));
		$tpl->set('title', stripslashes($user->title));
		$tpl->set('titleColor', $user->getStyle('blog-title-color'));
		
		$html = $tpl->fetch(MY_TEMPLATE_PATH."/admin/blogger_profile.html");
		
		return $html;
	}
}
