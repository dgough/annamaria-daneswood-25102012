<?php
(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

class MyblogDeleteTask extends CMSController{
	function display(){
		$cms =& cmsInstance('CMSCore');
		$cms->load('helper','url');
		
		// @todo: limit delete action to your own content only
		$id = cmsGetVar('id', 0, 'GET');
				
		$this->cms->db->query("DELETE FROM #__content WHERE id=$id");
		$this->cms->db->query("DELETE FROM #__myblog_permalinks WHERE contentid=$id");
		$this->cms->db->query("DELETE FROM #__myblog_images WHERE contentid=$id");
		$this->cms->db->query("DELETE FROM #__myblog_content_categories WHERE contentid=$id");
		
		// Need to include paging info if necessary, the paging info will come from
		$redirect = 'index.php?option=com_myblog&task=adminhome&Itemid='. myGetItemId();
		$redirect = cmsSefAmpReplace($redirect, false);
		cmsRedirect($redirect, 'Blog entry deleted.');
	}
}