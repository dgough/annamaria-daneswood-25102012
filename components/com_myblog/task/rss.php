<?php


(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

include_once(MY_COM_PATH . '/task/base.php');

class MyblogRssTask extends MyblogBaseController{
	
	/**
	 * Show RSS feed
	 */	 	
	function display(){
		header('Content-type:application/xml');
		$author = "";
		$category = "";
		$search = "";
		$archive="";
		header('Content-type:application/xml');
		
		if (isset ($_REQUEST['blogger'])) {
			if (is_string($_REQUEST['blogger']))
				$author = myGetAuthorId(urldecode(cmsGetVar('blogger', '', 'REQUEST')));
			else
				$author = intval(urldecode(cmsGetVar('blogger', '', 'REQUEST')));
				
		}
		
		if (isset ($_REQUEST['category']) and trim($_REQUEST['category']) != "") {
			$category = urldecode(htmlspecialchars(cmsGetVar('category','','REQUEST')));
		}
		
		if (isset ($_REQUEST['keyword']) and trim($_REQUEST['keyword']) != "") {
			$search = urldecode(htmlspecialchars(cmsGetVar('keyword','','REQUEST')));
		}
		
		if (isset ($_REQUEST['archive']) and trim($_REQUEST['archive']) != "") {
			$archive = urldecode(htmlspecialchars(cmsGetVar('archive','','REQUEST')));
		}
		
		$this->_rss($author, $category, $search, $archive);
		exit;
	}
	
	/**
	 *	Gets RSS feed for a specific blog view
	 */ 
	function _rss($bloggerID = "", $tags = "", $keywords = "", $archive="") {
		global $MYBLOG_LANG, $_MY_CONFIG, $sections, $Itemid;
		
		$cms =& cmsInstance('CMSCore');
		$cms->load('helper', 'url');
		$cms->load('libraries' , 'utf8');
		
		include_once ($cms->get_path() . '/includes/feedcreator.class.php');
		include_once ($cms->get_path() . '/includes/feedcreator.class.php');
		include_once ($cms->get_path() . '/components/com_myblog/libraries/datamanager.class.php');
		
		$db = &cmsInstance('CMSDb');
		
		if (!$_MY_CONFIG->get('useRSSFeed') or $_MY_CONFIG->get('useRSSFeed') == "0") {
			echo "<error>RSS feed not enabled</error>";
			return;
		}
		
		$blogger_username = "";
		
		if ($bloggerID != "") {
			$db->query("SELECT * from #__users WHERE id='$bloggerID'");
			$blogger = $db->get_object_list();
			
			if ($blogger) {
				$blogger = $blogger[0];
				$blogger_username = ($_MY_CONFIG->get('useFullName')=="1" ? $blogger->name :$blogger->username);
			} else
				$blogger_username = "";
		} else {
			$bloggerID = "";
		}
		
		if ($archive!=""){
			// Joomla 1.5 might convert the '-' to ':'
			$archive = urldecode($archive);
			$archive = str_replace(':', '-', $archive);
			$archive = date("Y-m-d 00:00:00", strtotime(str_replace("-", " ", "01 " . cmsGetVar('archive'))));
		}
		
		$rss = new RSSCreator20();
		
		$searchby = array('limit' => 20,
				'limitstart' => 0,
				'authorid' => $bloggerID,
				'category' => $tags,
				'search' => $keywords,
				'archive' => $archive,
				);
	
		$entries = mb_get_entries($searchby);
		$total = $searchby['total'];
		
		if(!class_exists('AzrulJXTemplate'))
		    include_once($cms->get_path('plugins') . '/system/pc_includes/template.php');
		    
		$tpl = new AzrulJXCachedTemplate(serialize($entries) . "_rss" . strval($bloggerID) . strval($tags) . strval($keywords) , strval($archive));
		
		if (!$tpl->is_cached()) 
		{
			$title = $MYBLOG_LANG['_MB_RSS_BLOG_ENTRIES'];
			
			if ($blogger_username and $blogger_username != "")
				$title .= " ".$MYBLOG_LANG['_MB_RSS_BLOG_FOR']." $blogger_username";
			
			if ($tags and $tags != "")
				$title .= " ".$MYBLOG_LANG['_MB_RSS_BLOG_TAGGED']." '$tags'";
			
			if ($keywords and $keywords != "")
				$title .= ", ".$MYBLOG_LANG['_MB_RSS_BLOG_KEYWORD']." '$keywords'";
			
			if ($archive and $archive != "")
			{
				$archive_display = date("F Y", strtotime($archive));
				$title .= " - $archive_display";
			}
			
			$db->query("SELECT description from #__myblog_user WHERE user_id='$bloggerID'");
			$description = $db->get_value();
			
			if (!$description or $description == "")
				$description = "$title";
			
			// remove readmore tag
			$description = str_replace('{readmore}', '', $description);	
				
			$rss->title = $title;
			$rss->description = $description;
			$rss->encoding = cmsGetISO();
			
			$rss->link = $cms->get_path('live');
			$rss->cssStyleSheet = NULL;
			
			if ($entries) {
				$count = 0;
				
				foreach ($entries as $row) {
					$count++;
					if ($count > 20)
						break;
					
					$item = new FeedItem();
					$item->title = $row->title != "" ? $row->title : "...";
					$item->title = myUnhtmlspecialchars($item->title);
					
					if ($row->fulltext == "")
						$itemDesc = $row->introtext != "" ? $row->introtext : "No description";
					else
						$itemDesc = $row->introtext . $row->fulltext;
						
					$desc_length_max = 500;
					$itemDesc = strip_tags($itemDesc, '<p> <br /> <br/> <br> <u> <i> <b> <img>');
					$actualDescLength = strlen($itemDesc);
					$itemDesc = substr($itemDesc, 0, $desc_length_max);
					$itemDesc = preg_replace("/\r\n|\n|\r/", "<br/>", $itemDesc);
					
					if ($actualDescLength > $desc_length_max)
						$itemDesc .= " [...]";
					
					$itemDesc = str_replace('{readmore}', '', $itemDesc);
					$item->description = $itemDesc;
					$item->link = html_entity_decode($row->permalink);
					$item->date = date('r', strtotime($row->created));
					$item->author	= myGetAuthorEmail($row->created_by);
					$categoriesList = myGetTags($row->id);
					$extraElements = array ();
					
					if ($categoriesList) {
						$categories = "";
						$indentString = " ";
						
						foreach ($categoriesList as $category) {
							$categoryName = $category->name;
							if ($categories != "")
								$categories .= "</category>\n$indentString<category>";
							$categories .= $categoryName;
						}
						
						$extraElements['category'] = $categories;
						$item->additionalElements = $extraElements;
					}
					
					$rss->addItem($item);
				}
			}
			
			$tpl->set('rss', $rss->createFeed());
		}
		
		$rsscontent = $tpl->fetch_cache(MY_TEMPLATE_PATH . "/admin/rss.tmpl.html");
		while (@ob_end_clean());
		echo trim(str_replace(array_keys($MYBLOG_LANG), array_values($MYBLOG_LANG), $rsscontent));
	}
}
	
