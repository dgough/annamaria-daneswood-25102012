<?php
(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

include_once(MY_COM_PATH . '/task/show.base.php');
include_once(MY_COM_PATH . '/libraries/plugins.class.php');

class MyblogShowTask extends MyblogShowBase{

	var $_plugins	= null;
	var $row = null;
	var $uid = null;
	
	function MyblogShowTask(){

		$cms    =& cmsInstance('CMSCore');
		$cms->load('libraries', 'user');
		$cms->load('helper', 'url');
		
		$this->_plugins	= new MYPlugins();
		$this->toolbar = MY_TOOLBAR_HOME;
		
		$this->uid = isset($_GET['show']) ? cmsGetVar('show','','GET') : cmsGetVar('id','','GET');
		$uid = $this->uid;
		
		// Get blog entry
		if (is_numeric($uid)){
			$cms->db->query("SELECT c.*,p.permalink, '".date('Y-m-d H:i:s')."' as curr_time, r.rating_sum/r.rating_count as rating, r.rating_count from (#__content as c,#__myblog_permalinks as p) left outer join #__content_rating as r on (r.content_id=c.id) WHERE c.id=p.contentid and c.id='$uid' and c.sectionid in ($sections)");
			$row = $cms->db->get_object_list();

			if ($row)
				$row = $row[0];
			else{
				$row = new blogContent();
				$row->load($uid);
			}
		} else {
			
			$uid = stripslashes($uid);
			$uid = urldecode($uid);
			$uid = $cms->db->_escape($uid);

			$row = new blogContent();
			$row->load($uid);
		}
		
		$this->row = &$row;
	}
	
	function _header(){
		echo parent::_header();
		if($this->isMyEntry()){
			?>
			<script type="text/javascript" language="javascript" src="<?php echo MY_COM_LIVE; ?>/js/myblog.js"></script>
			<link href="<?php echo MY_COM_LIVE; ?>/css/azwindow.css" rel="stylesheet" type="text/css" />
			<?php
		}
	}
	
	// return true if the entry is logged in user entry
	function isMyEntry(){
		$cms    =& cmsInstance('CMSCore');
		return ($this->row->created_by == $cms->user->id);
	}

	function display($styleid = '', $wrapTag = 'div'){
		global $MYBLOG_LANG, $mainframe, $_MY_CONFIG;

		if(!myAllowedGuestView('entry')){
			$template		= new AzrulJXTemplate();
			$content		= $template->fetch($this->_getTemplateName('permissions'));
			return $content;
		}
		
		$cms    =& cmsInstance('CMSCore');
		
		$Itemid		= myGetItemId();
		$row		= null;
		$task		= '';
		$task_url	= "";

		if ($task!=""){
			$task_url = "&task=$task";
		}
		
		// Load plugins
		$this->_plugins->load();
		
		$row = &$this->row;

		// Need to fix the permalink with complete path
		$row->permalink = myGetPermalinkUrl($row->id);
		$row->comments = "";
		$row->createdFormatted = cmsFormatDate($row->created, $_MY_CONFIG->get('dateFormat') , $mainframe->getCfg('offset'));

		// We need to format the date correctly and add the necessary offset back.
		$row->created			= cmsFormatDate($row->created, '%Y-%m-%d %H:%M:%S', $mainframe->getCfg('offset'));

		$row->title = myUnhtmlspecialchars($row->title);
		$row->title = htmlspecialchars($row->title); 
				
		if ($row->state != 1 || $row->publish_up > date('Y-m-d H:i:s')) {
			echo "Cannot find the entry.The user has either change the permanent link or the content has not been published.";
			return;
		} else {
			$cms->db->query("UPDATE #__content SET hits=hits+1 WHERE id=$row->id");
		}

		/** add meta tags **/

		// Page title will be added whatever $mosConfig_MetaTitle setting is
		myAddPageTitle(myUnhtmlspecialchars($row->title));
		myAddPathway($row->title);

		if ($mainframe->getCfg('MetaAuthor') == '1') {
			$mainframe->addMetaTag( 'author' , myGetAuthorName( $row->created_by , '1' ) );
		}

		// Attach meta tags for tags. Get the tags for this article.
		$tags	= myGetTags($row->id);
		
		$keywords	= '';
		foreach($tags as $tag)
		{
			$keywords	.= $tag->name . ' ';
		}
		
		if(cmsVersion() == _CMS_JOOMLA15)
		{
			$document=& JFactory::getDocument();
			
			if($document->getDescription() == '' || $row->metadesc != '')
				$document->setDescription( $row->metadesc );
		}
		else
		{
			$mainframe->appendMetaTag( 'description', $row->metadesc );
		}
		
		if( !empty( $row->metakey ) )
		{
			$keywords	.= ' ' . $row->metakey;
		}
		$mainframe->appendMetaTag( 'keywords' , $keywords );

		/** end add meta tags **/

		$tpl = new AzrulJXCachedTemplate(serialize($row) . $cms->user->usertype . $_MY_CONFIG->get('template') . $task);


		if (!$tpl->is_cached())
		{
			// Process text, combine introtext/fulltext
			$row->text	= '';
			
			if($row->introtext && trim($row->introtext) != ''){
				$row->text	.= $row->introtext;
			}
			
			// Add the rest of the fulltext
			if($row->fulltext && trim($row->fulltext) != ''){

				// Process anchor for #readmore only when there is fulltext
				if($_MY_CONFIG->get('anchorReadmore')){
					$row->text	.= '<a name="readmore"></a>'; 
// 					$tmpFull	= $row->text;
// 					$tmp		= substr($row->text, 0 , strlen($row->text) - 10);
// 					$row->text	= $tmp;
// 					$row->text	.= '<a name="readmore"></a>';
// 					$row->text	.= substr($tmpFull, strlen($tmpFull) - 10, strlen($tmpFull));					
				}
				$row->text	.= $row->fulltext;
			}

			// if JC integration enabled, display JC
			if ($_MY_CONFIG->get('useComment'))
			{
				if (file_exists($cms->get_path('plugins') . "/content/jom_comment_bot.php"))
				{
					if(cmsVersion() == _CMS_JOOMLA10 || cmsVersion() == _CMS_MAMBO)
					{
						global $_MAMBOTS;
						// Default handle to load the mambots
						$_MAMBOTS->loadBot('content', 'jom_comment_bot', true, null);
					}
					else
					{
						include_once ($cms->get_path('plugins') . "/content/jom_comment_bot.php");
					}

					// Check if admin allows user to enable or disable the comment on the blog
 					if($_MY_CONFIG->get('enableJCDashboard'))
					{
						
 						if(eregi('\{!jomcomment\}',$row->text))
						{
 							$row->text	= str_replace('{!jomcomment}','',$row->text);
 						}
						else if(eregi('\{jomcomment\}',$row->text))
						{
 							$row->text	= str_replace('{jomcomment}','',$row->text);
 							$row->comments	= "";
 							$row->comments 	= jomcomment($row->id, "com_myblog");
 						}
 						else if(eregi('\{jomcomment lock\}', $row->text) )
 						{
 							$row->text	= str_replace('{jomcomment lock}','',$row->text);
 							$row->comments	= "";
 							$row->comments 	= jomcomment($row->id, "com_myblog" , '' , '' , true );
						}
						else
						{
 							// Default
 							// User is not allowed to enable or disable comments
 							// so we use the default value to display
 							$row->comments	= "";
 							$row->comments 	= jomcomment($row->id, "com_myblog");
 						}
 					}
					else
					{
 						// User is not allowed to enable or disable comments
 						// so we use the default value to display
 						$row->comments	= "";
 						$row->comments 	= jomcomment($row->id, "com_myblog");
 					}
				}
			}
						
			// This is not needed
			// $row->text = str_replace("{readmore}", "", $row->text) . "<br/>";

			$row->author = myUserGetName($row->created_by, $_MY_CONFIG->get('useFullName'));
			$row->authorLink = cmsSefAmpReplace("index.php?option=com_myblog$task_url&blogger=" . urlencode(myGetAuthorName($row->created_by , $_MY_CONFIG->get('useFullName'))) . "&Itemid=$Itemid");
			$row->categories = myCategoriesURLGet($row->id, true, $task);
			$row->emailLink = cmsSefAmpReplace("index2.php?option=com_content&task=emailform&id={$this->uid}");

			// Get the avatar
			//$this->_setupAvatarHTML($row);
			$avatar	= 'My' . ucfirst($_MY_CONFIG->get('avatar')) . 'Avatar';
			$avatar	= new $avatar($row->created_by);
			
			$row->avatar	= $avatar->get();
			
// 			// Social Bookmarking display
// 			if($_MY_CONFIG->get('showBookmarking')){
// 				myGetBookmarks($row);
// 			}
			
			$row->afterContent = '';
			$row->beforeContent = '';

			$params	= $this->_buildParams();
			$row->beforeContent		= @$this->_plugins->trigger('onBeforeDisplayContent', $row, $params, 0);
			$row->onPrepareContent	= @$this->_plugins->trigger('onPrepareContent', $row, $params, 0);
			$row->afterContent		= "<br/>". @$this->_plugins->trigger('onAfterDisplayContent', $row, $params, 0);
			$row->created			= cmsFormatDate($row->created, "%Y-%m-%d %H:%M:%S");
			
			// Add [edit] link
			$cms->load('libraries','user');
			
			$row->editLink = '<span style="cursor:pointer;" onclick="myAzrulShowWindow(\'index2.php?option=com_myblog&task=write&keepThis=true&TB_iframe=true&no_html=1&id='.$row->id.'\');">&nbsp;[Edit]&nbsp;</span>';
			// Check if user enables back link
			if($_MY_CONFIG->get('enableBackLink'))
				$row->afterContent .= myGetBackLink();

			// Check if user enables PDF link
			if($_MY_CONFIG->get('enablePdfLink'))
				$tpl->set('enablePdfLink',true);
			else
				$tpl->set('enablePdfLink',false);

			// Check if user enables Print link
			if($_MY_CONFIG->get('enablePrintLink'))
				$tpl->set('enablePrintLink',true);
			else
				$tpl->set('enablePrintLink',false);


			// Remove all member with '_' prefix. Get rid of reference to cms/cmsdb
			unset($row->_table);
			unset($row->_key);
			unset($row->_db);
			
			// apply tidy library
// 			if(function_exists('tidy_parse_string')){
// 				$tidy = tidy_parse_string($row->text);
// 				$body = tidy_get_body($tidy);
// 				$row->text = $body->value;
// 				$row->text = str_replace('<body>', '', $row->text);
// 				$row->text = str_replace('</body>', '', $row->text);
// 				unset($body);
// 			}


			$tpl->set('userId', $cms->user->id);
			$tpl->set('entry', $tpl->object_to_array($row));

		}

		$content	= '';
		if ($_MY_CONFIG->get('overrideTemplate')) {
			$myCustomIndexTpl = $mainframe->getCfg('absolute_path') .'/templates/' .$mainframe->getTemplate() .'/com_myblog/entry.tmpl.html';
			if(file_exists($myCustomIndexTpl))
				$content .= $tpl->fetch_cache($myCustomIndexTpl);
			else
				$content .= $tpl->fetch_cache(MY_TEMPLATE_PATH . "/_default/entry.tmpl.html");
		}else{
			if(file_exists(MY_TEMPLATE_PATH . "/" . $_MY_CONFIG->get('template') . "/entry.tmpl.html"))
				$content .= $tpl->fetch_cache(MY_TEMPLATE_PATH . "/" . $_MY_CONFIG->get('template') . "/entry.tmpl.html");
			else
				$content .= $tpl->fetch_cache(MY_TEMPLATE_PATH . "/_default/entry.tmpl.html");
		}

		$content = str_replace(array_keys($MYBLOG_LANG), array_values($MYBLOG_LANG), $content);
		return $content;
	}
}
?>
