<?php

(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

include_once(MY_COM_PATH . '/task/browse.base.php');


class MyblogAuthorTask extends MyblogBrowseBase{
	var $author = null;
	var $authorId = 0;
	
	function MyblogAuthorTask(){
		parent::MyblogBrowseBase();
		$this->toolbar = MY_TOOLBAR_BLOGGER;
		$this->cms->load('libraries', 'user');
		$this->cms->load('model', 'user', 'com_myblog');
		
		$authorId = cmsGetVar('blogger', '', 'REQUEST');
		$authorId = is_string($authorId) ? myGetAuthorId(urldecode($authorId)) : intval($authorId); 
		
		$this->authorId = $authorId;
		
		$this->author = new myblogUser();
		$this->author->load($authorId);
	}
	
	function _header(){
		$html = parent::_header();
		//$html .= '<h2>Viewing a user blog! </h2>';
// 		$html .= '
// 		
// 		<style type="text/css" >
// 		#myBlog-wrap #myBlog-head {
// 		background:#EEEEEE url('.$this->author->getStyle('headerImg').') no-repeat scroll 0%;
// 		}
// 		
// 		#myBlog-wrap #myBlog-head h1 {
// 		color:'.$this->author->getStyle('blog-title-color').';
// 		}
// 		
// 		#myBlog-wrap #myBlog-head h2{
// 		color:'.$this->author->getStyle('blog-subtitle-color').';
// 		}
// 		</style> ';

		return $html;
	}
		
	function setData(){
		$searchby = array(); 
		$searchby['authorid'] = $this->authorId;
		
		// Request might contain 'category'
		if (cmsGetVar('category', '', 'REQUEST') != '') {
			$category = strval(urldecode(cmsGetVar('category', '', 'REQUEST')));
			$category = str_replace("+", " ", $category);
			$searchby['category'] = $category;
		}
		
		$this->filters = $searchby;		
	}
}
