<?php
(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

include_once(MY_COM_PATH . '/task/base.php');

class MyblogBlogsTask extends MyblogBaseController {

	function display($styleid = '', $wrapTag = 'div'){
		global $MYBLOG_LANG, $_MY_CONFIG, $sectionid, $sections, $Itemid, $mainframe, $mainframe;
		$cms    =& cmsInstance('CMSCore');
		$db     =& cmsInstance('CMSDb');
		$cms->load('helper', 'url');
		require_once ($cms->get_path('root') . "/includes/pageNavigation.php");

		myAddPageTitle($MYBLOG_LANG['_MB_ALL_BLOGS']);
		$content = "<br/>";
		$total = 0;
		
		$limit = isset ($_GET['limit']) ? $_GET['limit'] : 10;
		$limitstart = isset ($_GET['limitstart']) ? $_GET['limitstart'] : 0;
		
		if(empty($limit) && empty($limitstart)){
			$limit = isset ($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
			$limitstart = isset ($_REQUEST['limitstart']) ? $_REQUEST['limitstart'] : 0;
		}
		
		// make sure it really is an int
		$limit = intval($limit);
		$limitstart = intval($limitstart);
		
		$content .= '<form action="' . cmsSefAmpReplace("index.php?option=com_myblog&task=blogs") . '" name="blogsForm" id="blogsForm" method="POST"> '
			.$MYBLOG_LANG['_MB_DISPLAY']	.' # <select onChange="document.blogsForm.submit();" class="inputbox" name="limit">';
		$c = 5;

		// Pagination
		do {
			$selected = ($c == $limit ? "selected" : "");
			$content .= " <option value=\"$c\" $selected>$c</option>";
			$c = $c +5;
		} while ($c < 30);

		$content .= ' </select></form><br/>';

		// In case previous authors have not used myblog before, create a 'blog' for them
		$db->query("SELECT distinct c.created_by from #__content c left outer join #__myblog_user m on (m.user_id=c.created_by) WHERE m.user_id IS NULL and sectionid in ($sections)");

		if ($not_in_myblog = $db->get_object_list()) {
			foreach ($not_in_myblog as $to_insert) {
				$db->query("INSERT INTO #__myblog_user SET user_id='" . $to_insert->created_by . "',description=''");
			}
		}

		$db->query("SELECT distinct(u.id) as user_id, mu.description,u.username, u.name "
			." FROM #__users u,#__myblog_user mu, #__content c "
			." WHERE mu.user_id=u.id and c.created_by = mu.user_id AND c.sectionid in ($sections) limit $limitstart,$limit");
		$blogs = $db->get_object_list();

		$db->query("SELECT count(distinct(u.id)) "
			." FROM #__users u, #__myblog_user mu, #__content c "
			." WHERE mu.user_id=u.id and c.created_by = mu.user_id AND c.sectionid in ($sections)");
			
		$total = $db->get_value();

		if ($blogs) {
			foreach ($blogs as $blog) {

				if ($_MY_CONFIG->get('avatar') and $_MY_CONFIG->get('avatar') != "none"){
					$avatar_url = getAvatarImg($blog->user_id);
					$imgsize = @getimagesize($avatar_url);
					$imageWidth = $imgsize[0];
					$imageHeight = $imgsize[1];

					if (!$imageWidth or $imageWidth==0 or $imageWidth=="")
					$imageWidth = 40;
					if (!$imageHeight or $imageHeight==0 or $imageHeight=="")
					$imageHeight = 40;

					$configWidth = 	$_MY_CONFIG->avatarWidth;
					$configHeight = $_MY_CONFIG->avatarHeight;

					if ($configWidth=="" or $configWidth=="0")
					{
						$configWidth = $imageWidth;
					}
					if ($configHeight=="" or $configHeight=="0")
					{
						$configHeight = $imageHeight;
					}

					$srcRatio = $imageWidth/$imageHeight; // width/height ratio
					$destRatio = $configWidth/$configHeight;

					if ($destRatio > $srcRatio) {
						$imageHeight = $configHeight;
						$imageWidth = $configHeight*$srcRatio;
					}
					else {
						$imageWidth = $configWidth;
						$imageHeight = $configWidth/$srcRatio;
					}

					$blog->avatar = '<img src="' . $avatar_url . '" border="0" alt="' . $blog->name . '" width="' . $imageWidth . '" height="' . $imageHeight . '" />';

				}else
					$blog->avatar = "";
				
				if ($_MY_CONFIG->get('linkAvatar') and $_MY_CONFIG->get('linkAvatar') == "1" and $blog->avatar != "")
				$blog->avatar = '<a href="' .getAvatarLink($blog->user_id) . '" title="View Profile">' . $blog->avatar . '</a>';

				$blog->numEntries = myCountUserEntry($blog->user_id, "1");
				$blog->numHits = myCountUserHits($blog->user_id);
				$blog->blogLink = cmsSefAmpReplace("index.php?option=com_myblog&blogger=" . $blog->username . "&Itemid=$Itemid");
				$blog->description = strip_tags(my_strict_nl2br($blog->description, " "), '<u> <i> <b>');
				$db->query("SELECT datediff(curdate(),MAX(created)) from #__content WHERE sectionid IN ($sections) AND created_by='" . $blog->user_id . "' and state='1' and publish_up < now()");
				$last_update = $db->get_value();

				if ($last_update != "") {
					if ($last_update > 0)
					$last_update = ($last_update == 1 ? $MYBLOG_LANG['_MB_YESTERDAY'] : "$last_update ".$MYBLOG_LANG['_MB_DAYS']." ago");
					else
					$last_update = $MYBLOG_LANG['_MB_TODAY'];
					$blog->last_updated = $last_update;
				} else {
					$blog->last_updated = $MYBLOG_LANG['_MB_NEVER'];
				}
				$blog->tagLinks = "";
				$categories = myGetUserTags($blog->user_id);

				if ($categories) {
					foreach ($categories as $category) {
						if ($blog->tagLinks != "")
						$blog->tagLinks .= ", ";
						$blog->tagLinks .= '<a href="'.cmsSefAmpReplace('index.php?option=com_myblog&blogger=' . $blog->username . '&task=tag&category=' . urlencode($category->name) . '&Itemid='.$Itemid) . '">' . $category->name . '</a>';
					}
				} else {
					$blog->tagLinks = "<i>".$MYBLOG_LANG['_MB_NO_TAGS']	."</i>";
				}

				$leftmargin = $_MY_CONFIG->get('avatarWidth')+10;
				 $content .= '
								<div class="blogList" style="float: left; margin-bottom: 20px;">
								<div class="avatar_container" style="float: left; width: ' . $_MY_CONFIG->get('avatarWidth') . 'px"> ' . $blog->avatar . ' </div>
								<div style="padding-left: ' . ($_MY_CONFIG->get('avatarWidth') + 30 ). 'px;">
								<div class="blogTitle"><strong><a href="' . $blog->blogLink . '">' . ($_MY_CONFIG->get('useFullName')=="1"? $blog->name : $blog->username) . '\'s Blog</a></strong></div>
								<div class="updated"> <span title="Last updated">' . $blog->last_updated . '</span> </div>
								<div class="blogDesc"> ' . stripslashes($blog->description) . ' </div>
								<div class="blogStats"> <img class="numEntries_image" alt="" src="' . $mainframe->getCfg('live_site') . '/components/com_myblog/images/table_multiple.png" />' . $blog->numEntries . ' '.$MYBLOG_LANG['_MB_ENTRIES'].'<br/>
								<img class="numEntries_image" alt="" src="' . MY_COM_LIVE . '/templates/_default/images/tagcloud.png" />' . $blog->tagLinks . '
								</div>
								</div>
								</div>
								<div style="clear:both;"></div>';
			}
		}

		$queryString = $_SERVER['QUERY_STRING'];
		$queryString = preg_replace("/\&limit=[0-9]*/i", "", $queryString);
		$queryString = preg_replace("/\&limitstart=[0-9]*/i", "", $queryString);
		$pageNavLink = $_SERVER['REQUEST_URI'];
		$pageNavLink = preg_replace("/\&limit=[0-9]*/i", "", $pageNavLink);
		$pageNavLink = preg_replace("/\&limitstart=[0-9]*/i", "", $pageNavLink);
		$pageNav = new mosPageNav($total, $limitstart, $limit);
		$content .= '<div class="my-pagenav">' . $pageNav->writePagesLinks('index.php?' . $queryString) . '</div>';

		$content = str_replace(array_keys($MYBLOG_LANG), array_values($MYBLOG_LANG), $content);
		return $content;
	}
}