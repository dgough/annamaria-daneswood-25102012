<?php

(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

include_once('browse.base.php');


class MyblogTagsTask extends MyblogBrowseBase{
	
	function MyblogTagsTask(){
		parent::MyblogBrowseBase();
		$this->toolbar = MY_TOOLBAR_HOME;
	}
	
	function setData(){
		$searchby = array(); 

		// Request might contain 'category'
		if (cmsGetVar('category', '', 'REQUEST') != '') {
			$category = strval(urldecode(cmsGetVar('category', '', 'REQUEST')));
			$category = str_replace("+", " ", $category);
			$searchby['category'] = $category;
		}
		
		$this->filters = $searchby;	
	}
	
}
