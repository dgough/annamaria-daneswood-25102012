<?php
(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

// Include Imagebrowser.
include_once(MY_LIBRARY_PATH . '/imagebrowser.class.php');

class MyblogWriteTask extends CMSController{
	function MyblogWriteTask(){
	}
	
	function display(){
		global $mainframe, $_MY_CONFIG;
		$cms =& cmsInstance('CMSCore');
		$cms->load('helper', 'url');
		
		$jax	= new JAX($cms->get_path('plugin-live') ."/system/pc_includes");
		
		if(cmsVersion() == _CMS_JOOMLA15)
			$jax->setReqURI($cms->get_path('live')."/index.php?tmpl=component");
		else
			$jax->setReqURI($cms->get_path('live')."/index2.php");
		$jax->process();
				
		if(!class_exists('AzrulJXCachedTemplate'))
			include_once($cms->get_path('plugins') . '/system/pc_includes/template.php');
		
		include(MY_COM_PATH.'/language/'.$_MY_CONFIG->language);
		#$MYBLOG_LANG = myTranslateTemplate();


		// Detect if user is trying to access from back end
		$cms->load('helper' , 'url');		
	
		# If user is not allowed to post
		if(!myGetUserCanPost()){
		    // User not allowed to post blogs
			echo $MYBLOG_LANG['_MB_NO_POST'];
			return;
		}
		
				
		$tpl = new AzrulJXTemplate();
				
		// Get content param to edit
		$cms->load('libraries', 'input');
		$id = $cms->input->get('id');
		
		// If form id is set in in the 'post' data, use that one instead. You see, when
		// creating new entry, it the url will have id=0, even after saving. To avoid
		// redirecting to a new url, with id=34324 (whatever the new id is), we use the 
		// Post id instead.
		$postid = $cms->input->post('id', 0);
		if(!empty($postid))
			$id = $postid;
				
		// load the editor
		$row = new blogContent();
		$row->load($id);

		$isNew	= true;
		
		if($row->id != '0' || $row->id != 0)
			$isNew	= false;

		// If post var exist, bind them
		if(!empty($_POST))
			$row->bind($_POST, true);

		// Check if state is published or unpublished.
		if(!myGetUserCanPublish()){
		    // User may not be able to publish or unpublish even if they try
		    // to hack the system by posting additional $_POST['state']
		    $row->state    = 0;
		}

		// Get publishing time according to server time settings
		if ($row->publish_up != ""){
			// If publishing time is defined for existing entry
			$publish_up = strtotime(cmsFormatDate($row->publish_up, "%Y-%m-%d %H:%M:%S"));
		} else { 
			// If this is new entry, get current time as publish time
			$publish_up = time() + ( $mainframe->getCfg('offset') * 60 * 60 );	
		}

		/**
		 * Check if creation of tags configuration is enabled. If not enabled,
		 * dont display add new tag form.
		 **/
		$userCreateTag  = (boolean) $_MY_CONFIG->get('enableUserCreateTags');
		
		/**
		 * Check if image browser / uploads are enabled.
		 **/
		$userImageBrowser	= (boolean) $_MY_CONFIG->get('useImageBrowser');

		// If user is admin, we dont want to disable the tag creations for him/her
		if($cms->user->id == '62'){
		    $userCreateTag  = true;
		}

		// list of categories
		$categories = "";
		$categoriesArray = "";
		$query = "SELECT c.name,c.slug, count(c2.category) frequency FROM #__myblog_categories c left outer join #__myblog_content_categories c2 on (c.id=c2.category) GROUP BY c.name ORDER BY frequency DESC";
		$categoriesArray = myGetTagClouds($query); // get list of available tags

		// Process HTML for list of tags
		if ($categoriesArray != "") {
			foreach ($categoriesArray as $category) {
				$catclass = "tag".$category['cloud'];
				$catname = $category['name'];
				$catname_enc = $catname;
				
				$categories .= ", <a class=\"$catclass\" href=\"javascript:addTag('$catname_enc')\">$catname_enc</a>";
			}
		}
		$categories = trim($categories, ","); // available tags
		
		$tagListing	= '<span id="notags">None</span>';
		if($row->id != 0){
		
			// Display tags that this content uses.
			$strSQL	= "SELECT a.category AS id, b.name FROM #__myblog_content_categories AS a, #__myblog_categories AS b "
					. "WHERE b.id=a.category AND a.contentid='{$row->id}'";
			$cms->db->query($strSQL);
			
			$tagList	= $cms->db->get_object_list();

			if($tagList != ''){
				$tagListing		= '';
				foreach($tagList as $tag){
					$tagListing	.= '<span>'
								.  '<input type="hidden" value="' . $tag->name . '" name="tags[]">'
								.  '<a >X</a>' . $tag->name 
								.  '</span>';
				}
			}
		} else if ($row->id == '0' && $_MY_CONFIG->get('allowDefaultTags')) {
			// Load default tags
        	$strSQL	= "SELECT id,name FROM #__myblog_categories WHERE `default`='1'";
			$cms->db->query($strSQL);

			$tagList	= $cms->db->get_object_list();

			if($tagList != ''){
				$tagListing		= '';
				foreach($tagList as $tag){
					$tagListing	.= '<span>'
								.  '<input type="hidden" value="' . $tag->name . '" name="tags[]">'
								.  '<a >X</a>' . $tag->name 
								.  '</span>';
				}
			}
        }

		// Get list of tags
 		$strSQL	= 'SELECT * FROM #__myblog_categories';
 		$cms->db->query($strSQL);
 		$tags	= $cms->db->get_object_list();

		// Grab existing trackback URLs for this blog entry		
		$db =& cmsInstance('CMSDb');
		$db->query("SELECT url from #__myblog_tb_sent WHERE contentid='$row->id'");
		$trackbackurls = $db->get_object_list();
		$trackbackcontent = "";
		
		if ($trackbackurls) {
			foreach ($trackbackurls as $trackbackurl) {
				if ($trackbackcontent != "")
					$trackbackcontent .= " ";
				$trackbackcontent .= $trackbackurl->url;
			}
		}
		
		// Grab created time if existing entry, otherwise set created time to current time if new entry
		if ($row->created != ""){

			// Convert datetime to time. For Joomla 1.5 users, the offset are not calculated by Joomla
			// It is stored as it is, so we need to add the offset back to the time as we removed it
			// when saving
			if( cmsVersion() == '_CMS_JOOMLA15')
			{
				$created = strtotime($row->created) + ( $mainframe->getCfg('offset') * 60 * 60 );
			}
			else
			{
				$created	= strtotime($row->created);
			}
		}else{
			$created = time() + ( $mainframe->getCfg('offset') * 60 * 60 );
		}
		

		// Show jomcomment locks in dashboard?
		$jcDashboard    = false;
		$enableDashboard    = $_MY_CONFIG->get('enableJCDashboard');
		$enableJC           = $_MY_CONFIG->get('useComment');
		$jcFile             = $cms->get_path('root') . '/components/com_jomcomment/jomcomment.php';
				
		if($enableDashboard && $enableJC && file_exists($jcFile))
		$jcDashboard    = true;
				
		// Save attempt
		$validation_msg = array();
		$message = "";
		
			
		if(isset($_POST['saving'])){
			// perform validation, if it validates, redirect to success page
		
			// Make sure the title not empty
			if(empty($_POST['title']) || $_POST['title'] == $MYBLOG_LANG['TPL_DB_DEFAULTTITLE']){
				$validation_msg['title'] = "You must enter a title for this entry";
			}else{
				$cms	=& cmsInstance('CMSCore');
				$cms->load('helper', 'url');		
			}
		
			// Send technorati ping if enabled and xmlrpc extensions is enabled.
			if($_MY_CONFIG->get('pingTechnorati') && extension_loaded('xmlrpc')){
				include_once(MY_LIBRARY_PATH . '/technorati.class.php');
		
				$cms->load('libraries', 'user');
				$technorati	= new MYTechnorati(myGetAuthorName($cms->user->id), $_SERVER['HTTP_USER_AGENT']);
				$technorati->ping();
				
				// Debugging
				//$debug	= $technorati->ping();
				//var_dump($debug);
				//exit();
			}	

			// Make sure the title not empty
			if(empty($_POST['fulltext']))
				$validation_msg['fulltext'] = "Blog text cannot be empty";
		
			// If category is specified, set the category to the $_POST value. Make
			// Sure that it is actually part of the post section
			if(empty($_POST['catid'])){
				$validation_msg['catid'] = 'Category must be selected';
			}
		
			// Set the date time
			$createdDate = cmsGetVar('created','', 'POST');
				
			if(isset($createdDate) && !empty($createdDate)){
				// Date is already defined when $_POST is bind().
				if ($row->created && strlen(trim( $row->created )) <= 10) {
					$row->created .= ' 00:00:00';
				}
				$row->created = cmsFormatDate($row->created, '%Y-%m-%d %H:%M:%S', -$mainframe->getCfg('offset'));
			}else {
				$row->created = date( 'Y-m-d H:i:s' );
			}
			
			// Set the publish date same with the created date as myblog does not have publish settings?
			$row->publish_up    = $row->created;

			// Get the params
			$jcStatus	= cmsGetVar('jcState', '', 'POST');
			$row->fulltext 	= stripslashes($row->fulltext);
			$row->introtext = stripslashes($row->introtext);
			$row->title 	= stripslashes($row->title);
			
			// Check if Jom comment is installed and enabled on myblog
			if($jcDashboard){
		  			// Check if jcState is set
				if(isset($jcStatus) && !empty($jcStatus)){
					// Check if user enables or disables comment
					// default we do not need to add any tags.
						if($jcStatus == 'enabled'){
						$row->fulltext  .= '{jomcomment}';
					} else if($jcStatus == 'disabled'){
					    $row->fulltext  .= '{!jomcomment}';
					}
				}
			}
	
			// Is everything ok?
			// This is the one and only save point
			if(empty($validation_msg)){
				// No error, save the entry				
				$row->store();

				$row->load($id);
				
				
				$tags	= cmsGetVar('tags','','POST');
				myAddTags($row->id, $tags);

				myNotifyAdmin($row->id, myGetAuthorName($row->created_by, $_MY_CONFIG->get('useFullName')), $row->title, $row->introtext . $row->fulltext, $isNew);
				
				// Set the odering so that it doesn't mess up.
				mySortOrder($row);
				
				// Save OK
				$tpl->set('saveok', '1');
					
				// Send trackback, if necessary
				if(!empty($row->trackbacks))
					mySendTrackbacks($row->id, $row->trackbacks);
				
				
				
				$message =  '<span style="color:#6CC417;font-size:12px;font-weight:bold;">Save successful</span>';

				$onload = '';
				// If everything is ok, close th thickbox if needed
				if(isset($_POST['closeme']) && $_POST['closeme'] == '1'){
					$onload = "if(typeof(parent.azrulWindowSaveClose) == 'function'){ parent.azrulWindowSaveClose();}";
				} else {
					// Redirect the dashboard.
					$cms->load('helper','url');
					
					// Check if query string 'session' is set.
					$session	= cmsGetVar('session' , null , 'GET');
					
					$location	= $cms->get_path('live') . '/index2.php?option=com_myblog&task=write&no_html=1&id=' . $row->id;
					
					if( cmsVersion() == _CMS_JOOMLA15 )
						$location	= $cms->get_path('live') . '/index.php?option=com_myblog&task=write&no_html=1&id=' . $row->id . '&tmpl=component';
					
					if(isset($session) && !empty($session))
						$location	.= '&session=' . $session;

					cmsRedirect($location);
					exit;
					//$onload = 'if(typeof(parent.azrulWindowSave) == \'function\'){ parent.azrulWindowSave('.$id.');}';
				}
				$tpl->set('onload', $onload);
			} else {
					$message = '<span style="color:#FF0000;font-size:12px;font-weight:bold;">Save error</span>';
			}
			
			
			
		}
		
		
		
		// Combine introtext and fulltext
		$readmoreTag = '<p id="readmore"><img src="'.MY_COM_LIVE.'/images/readmoreline.gif"/></p>';
		$readmore = (!empty($row->introtext) && !empty($row->fulltext))? $readmoreTag : '';
		$row->fulltext = $row->introtext . $readmore. $row->fulltext; 
		
		// Check if Jom comment is installed and enabled on myblog
		// No jomcomment tags, use default behaviour
		$jcState = 'disabled';
		if($jcDashboard){
			// Check if there are any jomcomment locking tags in the text
			if(stristr($row->fulltext, '{jomcomment}') !== false){
			
				// Set $jcState so that templates know comment is enabled.
				$jcState = 'enabled';
			
				// Remove {jomcomment} from being displayed.
				$row->fulltext = str_replace('{jomcomment}','',$row->fulltext);
			}else if(stristr($row->fulltext, '{!jomcomment}') !== false){
				// Set $jcState so that templates know comment is disabled.
				$jcState = 'disabled';
		
				// Remove {jomcomment_lock} from being displayed.
				$row->fulltext = str_replace('{!jomcomment}','',$row->fulltext);
			}else {
				// Use default
				$jcState = 'default';
			}
		}

			$tpl->set('enableImageResize', $_MY_CONFIG->get('enableImageResize'));
			$tpl->set('trackbacks', myGetTrackbacks($row->id));
			$tpl->set('imageBrowser', $userImageBrowser);
			$tpl->set('userCreateTag',$userCreateTag);
			$tpl->set('jcState',$jcState);
			$tpl->set('message', $message);
			$tpl->set('jcDashboard',$jcDashboard);
			$tpl->set('validation_msg', $validation_msg);
			$tpl->set('jax_script', $jax->getScript());
//			$tpl->set('image_browser', $imgBrowserContent);
//			$tpl->set('image_browser_script', ' <script language="JavaScript" type="text/javascript">  	jax.call(\'myblog\',\'myxViewImages\', \'entry\', document.formEdit.id.value);              </script>');
			$tpl->set('date', strftime("%Y-%m-%d", $created));
			$tpl->set('time', strftime("%I:%M:%S %p", $created));
			$tpl->set('trackback_urls', $trackbackcontent);
			$tpl->set('publishRights', myGetUserCanPublish());
			$tpl->set('publishStatus', true);
			
			// Build category selection if necessary
			$contentcat = '';
			//if($_MY_CONFIG->get('allowCategorySelection'))
			{
				$contentcat = '<label><strong>Category</strong><select name="esrt" id="esrt">';
				$cats = myGetCategoryList($_MY_CONFIG->get('postSection'));
				foreach($cats as $c){
					$contentcat .= '<option value="'.$c->id.'">'.$c->title.'</option>';
				}	
				$contentcat .= '</select></label>';
			}
				
			// Grab default publishing status defined in My Blog config.
			$row->id = intval($row->id);
			if($row->id == 0)
				$row->state = $_MY_CONFIG->get('defaultPublishStatus');
			
			$tpl->set('metakey', $row->metakey);
			$tpl->set('metadesc', $row->metadesc);
			$tpl->set('state', $row->state);
			$tpl->set('existingTags', $row->tags);
			$tpl->set('categories', $contentcat);
			$tpl->set('videobot',$_MY_CONFIG->get('enableAzrulVideoBot'));
			$tpl->set('tags', $tagListing);
//			$tpl->set('tagAdvance', $tagAdvance);
			$tpl->set('fulltext', $row->fulltext);
			$tpl->set('id', $row->id);
			$tpl->set('permalink', $row->permalink);
			$tpl->set('title', $row->title);
			$tpl->set('publish_up', $publish_up);
			$tpl->set('pub_date', strftime("%Y-%m-%d", $publish_up));
			$tpl->set('pub_time', strftime("%I:%M:%S %p", $publish_up));
			$tpl->set('iso', cmsGetISO());
			$tpl->set('use_mce', $_MY_CONFIG->get('useMCEeditor') ? "true" : "false");
			
			$html = $tpl->fetch(MY_TEMPLATE_PATH."/admin/write.html");
			$html = str_replace(array_keys($MYBLOG_LANG), array_values($MYBLOG_LANG), $html);
			$html = str_replace("src=\"icons", "src=\"" . $mainframe->getCfg('live_site') . "/components/com_myblog/templates/admin/icons", $html);
			
			# Perform ping to server so user does not get logged out while writing/editing
			$html .= '<script language="Javascript" type="text/javascript">';
			$html .= 'setTimeout("jax.call(\'myblog\',\'myxPingMyBlog\')", 120000);';
			$html .= '</script>';
		
			echo $html;
	}

	function login()
	{
		require_once (MY_FRONTADMIN_PATH . "/frontadmin.class.php");
		
		global $frontadmin, $mainframe;
		$task = cmsGetVar('task', 'my_show_admin_toolbar', 'REQUEST');
		$id = isset ($_GET['id']) ? intval($_GET['id']) : 0;
		
		global $MYBLOG_LANG, $_MYBLOG,$_MY_CONFIG;
		$cms =& cmsInstance('CMSCore');
		if (!class_exists('JAX'))
			include_once($cms->get_path('plugins') .'/system/pc_includes/ajax.php');
		$jax = new JAX($cms->get_path('plugin-live') . "/system/pc_includes");
		$jax->setReqURI($cms->get_path('live') . "/index2.php");
		$jax->process();
		
		global $Itemid;
		
		$cms =& cmsInstance('CMSCore');
		$cms->load('libraries', 'user');
		$cms->load('helper', 'url');
		
		$db =& cmsInstance('CMSDb');
		

		// Detect if user is trying to access from back end
		$cms->load('helper' , 'url');		
		$session	= cmsGetVar('session' , '' , 'GET');

		if($session)
		{
			// Override cms->user
			$cms->user		= myGetUser($session);
			
		}

		# if Itemid is 0, need to autodetect itemid
		if ($Itemid==0){
			# first detect userblog itemid
			$db->query("select id from #__menu where link LIKE 'index.php?option=com_myblog%task=userblog%' and published='1'");
			$myItemid = $db->get_value();
			
			if (!$myItemid) {
				# then detect main myblog component itemid if userblog link DNE
				$db->query("select id from #__menu where type='components' and link='index.php?option=com_myblog' and published='1'");
				$myItemid = $db->get_value();
			}
			
			if ($myItemid)
				$Itemid=$myItemid;
		}
		
		# Displays login form for a user if login form enabled and user is not logged in while viewing dashboard
		# - useLoginForm is removed as we require a login form.
		if (!isset($cms->user->id) || !$cms->user->id){
			if (cmsVersion() == _CMS_JOOMLA10 || cmsVersion() == _CMS_MAMBO) {
				global $_VERSION;
				if ($_VERSION->getShortVersion() >= '1.0.10')
					$validate = josSpoofValue(1);
		?>
		<form action="<?php echo cmsSefAmpReplace('index.php');?>" method="POST" name="login">
		<?php echo "<strong>".$MYBLOG_LANG['_MB_NOT_LOGGEDIN']."</strong>";?>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td>
					<label for="mod_login_username"><?php echo _USERNAME; ?></label>
					<br />
					<input name="username" id="mod_login_username" type="text" class="inputbox" alt="username" size="10" />
					<br />
					<label for="mod_login_password"><?php echo _PASSWORD; ?></label>
					<br />
					<input type="password" id="mod_login_password" name="passwd" class="inputbox" size="10" alt="password" />
					<br />
					<input type="checkbox" name="remember" id="mod_login_remember" class="inputbox" value="yes" alt="Remember Me" />
					<label for="mod_login_remember">
						<?php echo _REMEMBER_ME; ?>
					</label>
					<br />
					<input type="submit" name="Submit" class="button" value="<?php echo _BUTTON_LOGIN; ?>" />
				</td>
			</tr>
			<tr>
				<td>
					<a href="<?php echo cmsSefAmpReplace('index.php?option=com_registration&amp;task=lostPassword' ); ?>"><?php echo _LOST_PASSWORD; ?></a>
				</td>
			</tr>
		</table>
		<input type="hidden" name="option" value="login" />
		<input type="hidden" name="op2" value="login" />
		<input type="hidden" name="lang" value="<?php echo $mainframe->getCfg('lang'); ?>" />
		<input type="hidden" name="return" value="<?php echo $_SERVER['REQUEST_URI']; ?>" />
		<input type="hidden" name="message" value="" />
		<input type="hidden" name="force_session" value="1" />
		<input type="hidden" name="<?php echo $validate; ?>" value="1" />
		</form>
		<?php
		}elseif (cmsVersion() == _CMS_JOOMLA15){
			$dbLink	= base64_encode(rtrim($cms->get_path('live') ,'/') . '/index2.php?option=com_myblog&task=write&keepThis=true&TB_iframe=true&no_html=1&id=0');
		?>
		<form action="<?php echo $cms->get_path('live') . '/index.php';?>" method="POST" name="login" id="form-login" >
			<fieldset class="input">
			<p id="form-login-username">
				<label for="modlgn_username"><?php echo JText::_('Username') ?></label><br />
				<input id="modlgn_username" type="text" name="username" class="inputbox" alt="username" size="18" />
			</p>
			<p id="form-login-password">
				<label for="modlgn_passwd"><?php echo JText::_('Password') ?></label><br />
				<input id="modlgn_passwd" type="password" name="passwd" class="inputbox" size="18" alt="password" />
			</p>
			<?php if(JPluginHelper::isEnabled('system', 'remember')) : ?>
			<p id="form-login-remember">
				<label for="modlgn_remember"><?php echo JText::_('Remember me') ?></label>
				<input id="modlgn_remember" type="checkbox" name="remember" class="inputbox" value="yes" alt="Remember Me" />
			</p>
			<?php endif; ?>
				<input type="submit" name="Submit" class="button" value="<?php echo JText::_('LOGIN') ?>" />
				</fieldset>
			<ul>
				<li><a href="<?php echo JRoute::_( 'index.php?option=com_user&view=reset' ); ?>"><?php echo JText::_('FORGOT_YOUR_PASSWORD'); ?></a></li>
			</ul>
			<input type="hidden" name="option" value="com_user" />
			<input type="hidden" name="task" value="login" />
			<input type="hidden" name="return" value="<?php echo $dbLink;?>" />
		
			<?php echo JHTML::_( 'form.token' ); ?>
		</form>
		<?php
		}
		
		} else {
			// User is already authenticated
			$this->display();
// 			$mb_admin_controller = new MB_Admin_Controller();
// 			$mb_admin_controller->init();
// 			$task = cmsGetVar('task', '', 'GET');
// 
// 			if(method_exists($mb_admin_controller, $task)){
// 				$mb_admin_controller->$task();	
// 			} 
		
		}
	}
}
