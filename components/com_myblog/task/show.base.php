<?php
(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

include_once(MY_COM_PATH . '/task/base.php');
include_once(MY_LIBRARY_PATH . '/avatar.class.php');

class MyblogShowBase extends MyblogBaseController{

	// Build the content params object
	function _buildParams(){
		global $mainframe;

		if(cmsVersion() == _CMS_JOOMLA10 || cmsVersion() == _CMS_MAMBO){
			$mosParams = new mosParameters('');
			$mosParams->def('link_titles', $mainframe->getCfg('link_titles'));
			$mosParams->def('author', !$mainframe->getCfg('hideAuthor'));
			$mosParams->def('createdate', !$mainframe->getCfg('hideCreateDate'));
			$mosParams->def('modifydate', !$mainframe->getCfg('hideModifyDate'));
			$mosParams->def('print', !$mainframe->getCfg('hidePrint'));
			$mosParams->def('pdf', !$mainframe->getCfg('hidePdf'));
			$mosParams->def('email', !$mainframe->getCfg('hideEmail'));
			$mosParams->def('rating', $mainframe->getCfg('vote'));
			$mosParams->def('icons', $mainframe->getCfg('icons'));
			$mosParams->def('readmore', $mainframe->getCfg('readmore'));
			$mosParams->def('popup', $mainframe->getCfg('popup'));
			$mosParams->def('image', 1);
			$mosParams->def('section', 0);
			$mosParams->def('section_link', 0);
			$mosParams->def('category', 0);
			$mosParams->def('category_link', 0);
			$mosParams->def('introtext', 1);
			$mosParams->def('pageclass_sfx', '');
			$mosParams->def('item_title', 1);
			$mosParams->def('url', 1);
			$mosParams->set('intro_only', 0);
		}elseif(cmsVersion() == _CMS_JOOMLA15){
			$mosParams = new JParameter('');
			$mosParams->def('link_titles', $mainframe->getCfg('link_titles'));
			$mosParams->def('author', !$mainframe->getCfg('hideAuthor'));
			$mosParams->def('createdate', !$mainframe->getCfg('hideCreateDate'));
			$mosParams->def('modifydate', !$mainframe->getCfg('hideModifyDate'));
			$mosParams->def('print', !$mainframe->getCfg('hidePrint'));
			$mosParams->def('pdf', !$mainframe->getCfg('hidePdf'));
			$mosParams->def('email', !$mainframe->getCfg('hideEmail'));
			$mosParams->def('rating', $mainframe->getCfg('vote'));
			$mosParams->def('icons', $mainframe->getCfg('icons'));
			$mosParams->def('readmore', $mainframe->getCfg('readmore'));
			$mosParams->def('popup', $mainframe->getCfg('popup'));
			$mosParams->def('image', 1);
			$mosParams->def('section', 0);
			$mosParams->def('section_link', 0);
			$mosParams->def('category', 0);
			$mosParams->def('category_link', 0);
			$mosParams->def('introtext', 1);
			$mosParams->def('pageclass_sfx', '');
			$mosParams->def('item_title', 1);
			$mosParams->def('url', 1);
			$mosParams->set('intro_only', 0);
		}
		return $mosParams;
	}


// 	function _setupAvatarHTML(&$row){
// 		global $_MY_CONFIG;
// 
// 		$row->avatarWidth = $_MY_CONFIG->get('avatarWidth');
// 
// 		// display and resize image avatars according to values set in My Blog  config, while maintain aspect ratio
// 		if ($_MY_CONFIG->get('avatar') and $_MY_CONFIG->get('avatar') != "none"){
// 			$avatar_url = getAvatarImg($row->created_by);
// 			$imgsize 	= @getimagesize($avatar_url);
// 			$imageWidth = $imgsize[0];
// 			$imageHeight = $imgsize[1];
// 
// 			if (!$imageWidth or $imageWidth==0 or $imageWidth=="")
// 			$imageWidth = 40;
// 
// 			if (!$imageHeight or $imageHeight==0 or $imageHeight=="")
// 			$imageHeight = 40;
// 
// 			$configWidth = 	$_MY_CONFIG->avatarWidth;
// 			$configHeight = $_MY_CONFIG->avatarHeight;
// 
// 			if ($configWidth=="" or $configWidth=="0")
// 			{
// 				$configWidth = $imageWidth;
// 			}
// 			if ($configHeight=="" or $configHeight=="0")
// 			{
// 				$configHeight = $imageHeight;
// 			}
// 
// 			$srcRatio = $imageWidth/$imageHeight; // width/height ratio
// 			$destRatio = $configWidth/$configHeight;
// 
// 			if ($destRatio > $srcRatio) {
// 				$imageHeight = $configHeight;
// 				$imageWidth = $configHeight*$srcRatio;
// 			}
// 			else {
// 				$imageWidth = $configWidth;
// 				$imageHeight = $configWidth/$srcRatio;
// 			}
// 
// 			$row->avatar = '<img src="' . $avatar_url . '" border="0" alt="' . myGetAuthorName($row->created_by) . '" width="' . $imageWidth . '" height="' . $imageHeight . '" />';
// 
// 		}else
// 		$row->avatar = "";
// 
// 		if ($_MY_CONFIG->get('linkAvatar') and $_MY_CONFIG->get('linkAvatar') == "1" and $row->avatar != "")
// 		$row->avatar = '<a href="' . getAvatarLink($row->created_by) . '" title="View Profile">' . $row->avatar . '</a>';
// 
// 	}
}