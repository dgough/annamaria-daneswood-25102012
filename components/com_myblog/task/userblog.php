<?php

/**
 * $task = userblog is equal to task=author. The only difference is the author id
 * will always revert to the current user 
 */ 
(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

include_once(MY_COM_PATH . '/task/author.php');

class MyblogUserblogTask extends MyblogAuthorTask{
	
	function MyblogUserblogTask(){
		parent::MyblogBrowseBase();
		
		$this->toolbar = MY_TOOLBAR_BLOGGER;
		$this->cms->load('libraries', 'user');
		$this->cms->load('model', 'user', 'com_myblog');
		
		$authorId = $author = $this->cms->user->id; 
		
		$this->authorId = $authorId;
		
		$this->author = new myblogUser();
		$this->author->load($authorId);

	}
	
	function display(){
		if ($this->cms->user->id == "0"){
			# If user not logged in, cannot view his/her blog
			echo '<div id="fp-content">';
			echo 'You need to login to view your blog';
			echo '</div>';
		} else {
			$content	= parent::display();
			myAddPageTitle( $this->cms->user->name . "'s Blog");
			return $content;
		}
	}
}
