<?php

(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

include_once(MY_COM_PATH . '/task/browse.base.php');


class MyblogTagTask extends MyblogBrowseBase{
	var $category;
	
	function MyblogTagTask(){
		parent::MyblogBrowseBase();
		$this->toolbar = MY_TOOLBAR_HOME;
	}
	
	
	function _header(){
		global $MYBLOG_LANG;
		echo parent::_header();
		
		
// 		$tagWord = ucfirst($MYBLOG_LANG['_MB_TAG']);
// 		echo "<div><span class=\"filterLabel\">{$tagWord} >> </span><span class=\"filterData\">".htmlspecialchars($this->category) ."</span></div>";


		$category		= cmsGetVar('category' , '' , 'REQUEST');

		if(is_numeric($category))
		{
			// Category is an integer. We know its Joomla's category
			$this->category = intval(urldecode(cmsGetVar('category', '', 'REQUEST')));
	
			// Set main tag pathway
			myAddPathway($MYBLOG_LANG['_MB_CATEGORY'], cmsSefAmpReplace('index.php?option=com_myblog&task=categories&Itemid='.myGetItemId()));
			
			// Set page title
			myAddPageTitle(htmlspecialchars(myGetJoomlaCategoryName($this->category)));
			
			// @todo: add to standard breadcrumb
			myAddPathway(htmlspecialchars(myGetJoomlaCategoryName($this->category)));

		}
		else
		{
			// Category is not an integer. We know its the tags.
			$this->category = strval(urldecode(cmsGetVar('category', '', 'REQUEST')));
			$this->category = str_replace("+", " ", $category);
			//$category = preg_replace("/([\xC2\xC3])([\x80-\xBF])/e", "chr(ord('\\1')<<6&0xC0|ord('\\2')&0x3F)", $category);
			
			// Set main tag pathway
			myAddPathway($MYBLOG_LANG['_MB_TAGS'], cmsSefAmpReplace('index.php?option=com_myblog&task=categories&Itemid='.myGetItemId()));
			
			// Set page title
			myAddPageTitle(htmlspecialchars(myGetTagName($this->category)));
			
			// @todo: add to standard breadcrumb
			myAddPathway(htmlspecialchars(myGetTagName($this->category)));
		}
		//$rssLink = '&category=' . htmlspecialchars(cmsGetVar('category', '', 'REQUEST'));
	}
	
	function setData(){
		$searchby = array();
		
		if(is_numeric($this->category))
			$searchby['jcategory'] = $this->category;
		else
			$searchby['category'] = $this->category;
		
		$this->filters = $searchby;	
	}
	
}
