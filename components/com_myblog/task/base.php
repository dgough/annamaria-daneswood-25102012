<?php
(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

/**
 * Base class for all MyBlog task
 */

class MyblogBaseController extends CMSController{
	var $toolbar = MY_TOOLBAR_HOME;
	var $pageTitle = '';
	var $category = "";
	
	function MyblogBaseController(){
		parent::CMSController();
		
		// Request might contain 'category'
		$this->category = '';
		if (cmsGetVar('category', '', 'REQUEST') != '') {
			$category = strval(urldecode(cmsGetVar('category', '', 'REQUEST')));
			$this->category = str_replace("+", " ", $category);
		}
	}

	function _header(){
		global $mainframe, $_MY_CONFIG, $MYBLOG_LANG;;
		
		$cms =& cmsInstance('CMSCore');
		if ($_MY_CONFIG->get('overrideTemplate')){
			$myCustomTplStyle = $cms->get_path('root') .'/templates/' .$mainframe->getTemplate() .'/com_myblog/template_style.css';
			if(file_exists($myCustomTplStyle)){
				$style = '<link rel="stylesheet" type="text/css" href="' . $cms->get_path('live') . '/templates/' .$mainframe->getTemplate() .'/com_myblog/template_style.css" />';
				$mainframe->addCustomHeadTag($style);
			}else{
				if(file_exists(MY_TEMPLATE_PATH . "/" . $_MY_CONFIG->get('template') . "/template_style.css")){
					$style = '<link rel="stylesheet" type="text/css" href="' . $cms->get_path('live') . '/components/com_myblog/templates/' . $_MY_CONFIG->get('template') . "/template_style.css" . '"/>';
					$mainframe->addCustomHeadTag($style);
				}	
			}
		}else{
			if(file_exists(MY_TEMPLATE_PATH . "/" . $_MY_CONFIG->get('template') . "/template_style.css")){
				$style = '<link rel="stylesheet" type="text/css" href="' . $cms->get_path('live') . '/components/com_myblog/templates/' . $_MY_CONFIG->get('template') . "/template_style.css" . '"/>';
				$mainframe->addCustomHeadTag($style);
			}
		}
		// Add the myblog div-wrapper
		$html  = '<div id="myBlog-wrap">';
		
		// Add the toolbar
		$html .= $this->_showToolbar($this->toolbar);
		
		// Add some filtering options
		if(!empty($this->category)){
		
			if(is_numeric($this->category)){
				$tagWord	= ucfirst($MYBLOG_LANG['_MB_CATEGORY']);
				$html .= "<div><span class=\"filterLabel\">{$tagWord} >> </span><span class=\"filterData\">".htmlspecialchars(myGetJoomlaCategoryName($this->category)) ."</span></div>";
			}
			else{
				$tagWord = ucfirst($MYBLOG_LANG['_MB_TAG']);
				$html .= "<div><span class=\"filterLabel\">{$tagWord} >> </span><span class=\"filterData\">".htmlspecialchars(myGetTagName($this->category)) ."</span></div>";
			}
		}
		
		return $html;
	}
	
	function _footer(){
		$html  = getPoweredByLink();
		$html .= '</div>'; 
		return $html;
	}

	// Build the content params object
	function _buildParams(){
		global $mainframe;

		if(cmsVersion() == _CMS_JOOMLA10 || cmsVersion() == _CMS_MAMBO){
			$mosParams = new mosParameters('');
			$mosParams->def('link_titles', $mainframe->getCfg('link_titles'));
			$mosParams->def('author', !$mainframe->getCfg('hideAuthor'));
			$mosParams->def('createdate', !$mainframe->getCfg('hideCreateDate'));
			$mosParams->def('modifydate', !$mainframe->getCfg('hideModifyDate'));
			$mosParams->def('print', !$mainframe->getCfg('hidePrint'));
			$mosParams->def('pdf', !$mainframe->getCfg('hidePdf'));
			$mosParams->def('email', !$mainframe->getCfg('hideEmail'));
			$mosParams->def('rating', $mainframe->getCfg('vote'));
			$mosParams->def('icons', $mainframe->getCfg('icons'));
			$mosParams->def('readmore', $mainframe->getCfg('readmore'));
			$mosParams->def('popup', $mainframe->getCfg('popup'));
			$mosParams->def('image', 1);
			$mosParams->def('section', 0);
			$mosParams->def('section_link', 0);
			$mosParams->def('category', 0);
			$mosParams->def('category_link', 0);
			$mosParams->def('introtext', 1);
			$mosParams->def('pageclass_sfx', '');
			$mosParams->def('item_title', 1);
			$mosParams->def('url', 1);
			$mosParams->set('intro_only', 0);
		}elseif(cmsVersion() == _CMS_JOOMLA15){
			$mosParams = new JParameter('');
			$mosParams->def('link_titles', $mainframe->getCfg('link_titles'));
			$mosParams->def('author', !$mainframe->getCfg('hideAuthor'));
			$mosParams->def('createdate', !$mainframe->getCfg('hideCreateDate'));
			$mosParams->def('modifydate', !$mainframe->getCfg('hideModifyDate'));
			$mosParams->def('print', !$mainframe->getCfg('hidePrint'));
			$mosParams->def('pdf', !$mainframe->getCfg('hidePdf'));
			$mosParams->def('email', !$mainframe->getCfg('hideEmail'));
			$mosParams->def('rating', $mainframe->getCfg('vote'));
			$mosParams->def('icons', $mainframe->getCfg('icons'));
			$mosParams->def('readmore', $mainframe->getCfg('readmore'));
			$mosParams->def('popup', $mainframe->getCfg('popup'));
			$mosParams->def('image', 1);
			$mosParams->def('section', 0);
			$mosParams->def('section_link', 0);
			$mosParams->def('category', 0);
			$mosParams->def('category_link', 0);
			$mosParams->def('introtext', 1);
			$mosParams->def('pageclass_sfx', '');
			$mosParams->def('item_title', 1);
			$mosParams->def('url', 1);
			$mosParams->set('intro_only', 0);
		}
		return $mosParams;
	}

	/**
	 *	Shows toolbar for the My Blog frontpage
	 */ 
	function _showToolbar($op = "") {
		global $MYBLOG_LANG, $mainframe, $Itemid;
		global $_MYBLOG, $_MY_CONFIG;
		$cms    =& cmsInstance('CMSCore');
		$db		=& cmsInstance("CMSDb");
		
		$show	= array();
		$category	= cmsGetVar('category','','GET');
		$search		= cmsGetVar('search','','GET');

		// Check if current page is blogger
		$blogger	= cmsGetVar('blogger','','GET');

		$show['feed'] = $_MY_CONFIG->get('useRSSFeed');

		$rssLink	 = '';
		if($show['feed']){
			$rssLink = "index.php?option=com_myblog";
			if (isset ($_REQUEST['blogger']) and $_REQUEST['blogger'] != "")
				$rssLink .= "&blogger=" . htmlspecialchars($_REQUEST['blogger']);
			
			if (isset ($_REQUEST['category']) and $_REQUEST['category'] != "")
				$rssLink .= "&category=" . htmlspecialchars($_REQUEST['category']);
			
			if (isset ($_REQUEST['keyword']) and $_REQUEST['keyword'] != "")
				$rssLink .= "&keyword=" . htmlspecialchars($_REQUEST['keyword']);
			
			if (isset ($_REQUEST['archive']) and $_REQUEST['archive'] != "")
				$rssLink .= "&archive=" . htmlspecialchars($_REQUEST['archive']);
			
			if (isset ($_REQUEST['Itemid']) and $_REQUEST['Itemid'] != "" and $_REQUEST['Itemid'] != "0")
				$rssLink .= "&Itemid=" . intval($_REQUEST['Itemid']);
			else
			{
				# autodetect Itemid
				$query = "SELECT id FROM #__menu  WHERE type='components' "
				        ."AND link='index.php?option=com_myblog' "
				        ."AND published='1'";
	
				$db->query($query);
	
				$myItemid = $db->get_value();
				if (!$myItemid)
					$myItemid = 1;
				$Itemid = $myItemid;
			}
			$rssLink	.= "&task=rss";
			$rssLink	= cmsSefAmpReplace($rssLink);

			if(isset($blogger) && !empty($blogger)){
				// Check if user uses feedburner instead.
				if($_MY_CONFIG->get('userUseFeedBurner')){
					
					if(!class_exists('myblogUser'))
						include_once(MY_MODEL_PATH . '/user.php');

					// Get RSS link for specific user.
					$user		= new myblogUser();
					$user->load(myGetAuthorId($blogger));

					// Check if user's feedburner link is empty, we use the administrator's defined
					// link
					if($user->feedburner == '' && $_MY_CONFIG->get('useFeedBurner')){
						$rssLink	= $_MY_CONFIG->get('useFeedBurnerURL');
					}else{
						$rssLink	= $user->feedburner;
					}
				}
			}
			
			// Check if user wants to use feedburner
			if($_MY_CONFIG->get('useFeedBurner') && empty($blogger)){
				$rssLink	= $_MY_CONFIG->get('useFeedBurnerURL');
			}

			$rssTitle = $MYBLOG_LANG['_MB_RSS_BLOG_ENTRIES'];
			
			if ($blogger && $blogger != "")
			{
				$rssTitle .= $MYBLOG_LANG['_MB_RSS_BLOG_FOR'] . ' ' . $blogger;
			}
			
			
			if ($category && $category != "")
			{
				$rssTitle .= ' ' . $MYBLOG_LANG['_MB_RSS_BLOG_TAGGED'] . ' \'' . htmlspecialchars($category) . "'";
			}
			
			
			if ($search && $search != "")
			{
				$rssTitle .= "," . $MYBLOG_LANG['_MB_RSS_BLOG_KEYWORD'] . "'" . htmlspecialchars($search) ."'";
			}
			
			$rssLinkHeader = '<link rel="alternate" type="application/rss+xml" title="' . $rssTitle . '" href="' . $rssLink . '" />';
			
			$mainframe->addCustomHeadTag($rssLinkHeader);
		}
				
		if ($_MY_CONFIG->get('frontpageToolbar')) {
			$cms->load('helper', 'url');
			$cms->load('model', 'user', 'com_myblog');
			
			$Itemid				= myGetBlogItemId();
			$dashboardItemid	= myGetAdminItemId();
			
			$homeLink     = cmsSefAmpReplace("index.php?option=com_myblog&Itemid=$Itemid");
			$categoryLink = cmsSefAmpReplace("index.php?option=com_myblog&task=categories&Itemid=$Itemid");
			$searchLink   = cmsSefAmpReplace("index.php?option=com_myblog&task=search&Itemid=$Itemid");
			$bloggersLink = cmsSefAmpReplace("index.php?option=com_myblog&task=bloggers&Itemid=$Itemid");
			$accountLink  = cmsSefAmpReplace("index.php?option=com_myblog&task=adminhome&Itemid=$dashboardItemid");

			$dashboardClass = "thickbox";
			$thickboxScript="";
			
			if(!class_exists('AzrulJXTemplate'))
			    include_once($cms->get_path('plugins') . '/system/pc_includes/template.php');
	
			$tpl = new AzrulJXTemplate();
			$toolbar = array();
			$active = array();

			$toolbar['op']            = $op;	
			$toolbar['homeLink']      = $homeLink;
			$toolbar['categoryLink']  = $categoryLink;
			$toolbar['searchLink']    = $searchLink;
			$toolbar['bloggersLink']  = $bloggersLink;
			$toolbar['accountLink']   = $accountLink;


			$active['home']		= '';
			$active['category'] = '';
			$active['search'] 	='';
			$active['blogger'] 	='';
			$active[$op] 		= ' blogActive';
			
			# If viewing userblog, only display Home and Dashboard links
			if ($op == "userblog") {
				$homeLink = cmsSefAmpReplace("index.php?option=com_myblog&Itemid=$Itemid&task=userblog");
				$manageBlogLink = cmsSefAmpReplace("index2.php?option=com_myblog&no_html=1&admin=1&task=adminhome&Itemid=$Itemid&keepThis=true&TB_iframe=true&height=600&width=850");
				$toolbar['homeLink'] = $homeLink;
				$toolbar['manageBlogLink'] = $manageBlogLink;
			} else {
				$toolbar['rssFeedLink'] = $rssLink;
			}
			
			$title 	= '';
			$desc	= '';
			
			if(cmsGetVar('blogger', '', 'GET') != ''){
				$author	= cmsGetVar('blogger','','GET');
				$title	= stripslashes(myGetAuthorTitle(myGetAuthorId($author)));
				$desc	= stripslashes(myGetAuthorDescription(myGetAuthorId($author)));	
			}
			
			$title 	= empty($title) ? stripslashes($_MY_CONFIG->get('mainBlogTitle'))	: $title;
			$desc	= empty($desc)  ? stripslashes($_MY_CONFIG->get('mainBlogDesc'))	: $desc;
			
			$tpl->set('toolbar', $toolbar);
			$tpl->set('show', $show);
			$tpl->set('active', $active);
			$tpl->set('title', $title);
			$tpl->set('summary', $desc);
			
			$template_file = MY_TEMPLATE_PATH . "/" . $_MY_CONFIG->get('template') . "/toolbar.tmpl.html";
			if(!file_exists($template_file))
				$template_file = MY_TEMPLATE_PATH . "/_default/toolbar.tmpl.html";
	
			$toolbar_output = $tpl->fetch($template_file);
			return str_replace(array_keys($MYBLOG_LANG), array_values($MYBLOG_LANG), $toolbar_output);
		}
	}

	function _getTemplateName($templateType){
		global $mainframe, $_MY_CONFIG;
		
		$cms	=& cmsInstance('CMSCore');
		
		$template = MY_TEMPLATE_PATH . '/_default/' . $templateType. '.tmpl.html';
		if ($_MY_CONFIG->get('overrideTemplate')) {
			$t = $cms->get_path('root') .'/templates/' .$mainframe->getTemplate() .'/com_myblog/' . $templateType . '.tmpl.html';
			$template = file_exists($t) ? $t: $template;
		}else{
			$t = MY_TEMPLATE_PATH . "/" . $_MY_CONFIG->get('template') . '/' . $templateType . '.tmpl.html';
			$template = file_exists($t) ? $t: $template;
		}
		
		return $template;
	}
	
	function _checkViewPermissions($context){

	}
}
