<?php
(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

include_once('browse.base.php');

class MyblogAdminTask extends MyblogBrowseBase{
	var $cms		= null;
		
	function MyblogAdminTask(){
		$this->cms	=& cmsInstance('CMSCore');
		$this->cms->load('helper','url');
		
		$this->toolbar	= MY_TOOLBAR_HOME;
		
	}
	
	function display(){
		$action		= cmsGetVar('do','','GET');
		
		// Check if sid is valid
		$sid	= cmsGetVar('sid','','GET');
		
		$strSQL	= "SELECT `cid` FROM #__myblog_admin WHERE `sid`='{$sid}'";
		$this->cms->db->query($strSQL);

		if($cid = $this->cms->db->get_value()){
			if(method_exists( $this , $action))
				$content	= $this->$action($cid);
			else
				$content	= $this->_error();
		} else {
			$content	= $this->_error();
		}			
		return $content;
	}
	
	function publish($cid){
		// We know that the sid is valid.
		$strSQL	= "UPDATE #__content SET `state`='1' WHERE `id`='{$cid}'";
		$this->cms->db->query($strSQL);
		
		return 'Blog entry published.';
	}
	
	function unpublish($cid){
		// We know that the sid is valid.
		$strSQL	= "UPDATE #__content SET `state`='0' WHERE `id`='{$cid}'";
		$this->cms->db->query($strSQL);
		
		return 'Blog entry unpublished.';
	}

	function remove($cid){
		// We know that the sid is valid.
		$strSQL	= "DELETE FROM #__content WHERE `id`='{$cid}'";
		$this->cms->db->query($strSQL);
		
		return 'Blog entry removed.';
	}
	
	function _error(){
		return "<p><b>The link is invalid. You can use the backend to publish the comment.</b></p>";
	}

}