<?php
(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

ini_set('xdebug.profiler_enable', 0);
ini_set('xdebug.profiler_output_dir', 'C:\xampp\log');
ini_set('xdebug.trace_format' , 1);
ini_set('xdebug.show_mem_delta', 1);


if(!defined('CMSLIB_DEFINED'))
	include_once(dirname(dirname(dirname(__FILE__))) . '/components/libraries/cmslib/spframework.php');

$cms = &cmsInstance('CMSCore');
global $option, $_MYBLOG, $_MYADMIN, $_MY_CONFIG, $mainframe, $sectionid, $MYBLOG_LANG;

require_once($cms->get_path('root').'/components/com_myblog/defines.myblog.php');
include_once (MY_COM_PATH . "/model/class.myblog.php");
include_once (MY_COM_PATH . "/functions.myblog.php");
include_once (MY_LIBRARY_PATH . "/myblog.functions.php");
include_once (MY_LIBRARY_PATH . "/datamanager.class.php");
include_once (MY_FRONTADMIN_PATH . "/frontadmin.class.php");
require_once ($cms->get_path('root') . "/administrator/components/com_myblog/config.myblog.php");

$_MY_CONFIG 	= new MYBLOG_Config();
$_MYBLOG 		= new MyBlogMainFrame();

include_once(MY_COM_PATH . "/language/english.php"); // load english language file first
include_once(MY_COM_PATH . "/language/" . $_MY_CONFIG->language);

global $frontadmin, $sectionid, $catid, $sections, $_MAMBOTS,$mainframe;

$lang = $mainframe->getCfg('lang');
if ($lang == '') {
	$lang = 'english';
}

if(cmsVersion() == _CMS_JOOMLA10 || cmsVersion() == _CMS_MAMBO){
	include_once( $cms->get_path('root') .'/language/' . $lang . '.php' );
}elseif(cmsVersion() == _CMS_JOOMLA15){
	//skip language file inclusion as joomla 1.5 is automatically loaded.
}

$sectionid = $_MY_CONFIG->postSection;
$catid = $_MY_CONFIG->catid;
$sections = $_MY_CONFIG->get('managedSections');

// Sections should never been empty
if ($sections == "")
	$sections = "-1";


/** Ajax calls **/
function myxSetVideoType($url, $width, $height){
	$cms    =& cmsInstance('CMSCore');
	
	//$videoImage = $cms->get_path('live') . '/components/com_myblog/images/youtube_logo.jpg';
	
	$objResponse    = new JAXResponse();
	$js		= "tinyMCE.execCommand('mceFocus',false, 'mce_editor_0'); ";
	$js    .= "tinyMCE.execCommand('mceInsertContent',false, '[video:$url {$width}x{$height}]');";
	$objResponse->addScriptCall($js);
	$objResponse->sendResponse();
}

/**
 * Ajax function for addition of tags from user dashboard.
 **/
function myxUserAddTag($tagName,$contentId = 0){
	global $_MY_CONFIG, $my;
	$cms    =& cmsInstance('CMSCore');
	$cms->load('libraries', 'user');

	include(MY_COM_PATH.'/language/'.$_MY_CONFIG->language);
	include_once(MY_LIBRARY_PATH.'/tags.class.php');
	
	$tagObj			= new MYTags();
	$objResponse    = new JAXResponse();
	
	// Check if user is allowed to create tags from configurations,
	// some nasty users may issue a javascript even the form is disabled.
	// Need to check also if user is an administrator
	if((!$_MY_CONFIG->get('enableUserCreateTags') && $cms->user->id != '62') || (!isValidMember())){
	    // Display error message to hacker/spammer/bad user
	    $objResponse->addScriptCall('alert("Not allowed.");');
		$objResponse->sendResponse();
		return;
	}
	
	if($tagObj->add($tagName)){
		$objResponse->addAssign('tagList','innerHTML',myGetTagsSelectHtml($contentId));
	    $objResponse->addScriptCall('window.alert("' . str_replace('{TAGNAME}',$tagName,$MYBLOG_LANG['TPL_DB_SUCCESSADDTAG'] . '");'));
	} else {
		$objResponse->addScriptCall('window.alert("' . str_replace('{TAGNAME}', $tagName,$MYBLOG_LANG['TPL_DB_EXISTSTAG'] . '");'));
	}

	$objResponse->sendResponse();
}

function isValidMember(){
	$cms	=& cmsInstance('CMSCore');
	$cms->load('libraries','user');
	
	if($cms->user->id == '0')
		return false;
	
	return true;
}

function isEditable($contentId){
	$cms	=& cmsInstance('CMSCore');
	$cms->load('libraries', 'user');
	
	$strSQL	= "SELECT `created_by` FROM #__content WHERE `id`='{$contentId}'";
	
	$cms->db->query($strSQL);
	$creator	= $cms->db->get_value();
	
	if($cms->user->id != $creator && $cms->user->id != '62')
		return false;
	
	return true;
}

// Toggle publish state of article in user dashboard
function myxTogglePublish($id) {
	global $mainframe, $sectionid;
	
	$objResponse = new JAXResponse();
	$cms = &cmsInstance('CMSCore');
	$db = &cmsInstance('CMSDb');
	
	$cms->load('libraries', 'user');

	// Check if user is really allowed to publish this article.
	if(!isValidMember() || !isEditable($id)){
	    // Display error message to hacker/spammer/bad user
	    $objResponse->addScriptCall('alert("Not allowed.");');
		$objResponse->sendResponse();
		return;
	}
	
	while (@ ob_end_clean());
	$db->query("SELECT state FROM #__content WHERE id=$id");
	$publish = $db->get_value();
	
	$publish = intval(!($publish));
	$db->query("UPDATE #__content SET state='$publish' WHERE id=$id");
	

	
	if ($publish)
		$objResponse->addAssign('pubImg' . $id, 'src', MY_COM_LIVE.'/images/publish_g.png');
	else
		$objResponse->addAssign('pubImg' . $id, 'src', MY_COM_LIVE.'/images/publish_x.png');
	
	$draftContent = myGetUserDraft($cms->user->id);
	$draftContent = preg_replace("/([\x80-\xFF])/e", "chr(0xC0|ord('\\1')>>6).chr(0x80|ord('\\1')&0x3F)", $draftContent);
	$objResponse->addAssign('drafts', 'innerHTML', $draftContent);
	$objResponse->sendResponse();
	
	return true;
}
function myxShowFolder($root, $show = "true") {
	global $_MY_CONFIG, $my;
	$cms        =& cmsInstance('CMSCore');
	$cms->load('libraries', 'user');
	
	$frontadmin= new MB_Frontadmin();
	$content = $frontadmin->show_folder($root, $show);
	
	$objResponse = new JAXResponse();
	$img_site_path = $cms->get_path('live') . $root;
	
	$root_content_id = str_replace("/", "_", $root) . "content";
	$root_link_id = str_replace("/", "_", $root) . "link";
	$uploadedImages = "";
	$file_path = $cms->get_path('root') . $root;
	$img_array = array ();
	$curr_dir = $file_path;
	$selected_class = "folderBrowserSelectedOption";
	$deselected_class = "folderBrowserDeselectedOption";
	$base_img_dir = $_MY_CONFIG->get('imgFolderRoot');
	
	if (!$base_img_dir or $base_img_dir == "")
		$base_img_dir = '/images/';
	
	$base_img_dir = trim($base_img_dir, "/ ");
	$base_img_dir = "/" . $base_img_dir . "/";
	
	if ($_MY_CONFIG->get('imgFolderRestrict') == "1") {
		$base_img_dir .= $cms->user->id . "/";
	}
	
	$base_img_dir_content = str_replace("/", "_", $base_img_dir) . "content";
	$base_img_dir_link = str_replace("/", "_", $base_img_dir) . "link";
	$selectionScript = "el=document.getElementById('$base_img_dir_content');nodes=el.getElementsByTagName('a');end=nodes.length;var iii=0;for (iii;iii<end;iii++) nodes[iii].className='$deselected_class';";
	$myroot = str_replace(rtrim($base_img_dir, "/"), "", $root);
	$selected_dirs = explode("/", substr($myroot, 1, strlen($myroot) - 2));
	$selected_dir_current = "_";
	array_unshift($selected_dirs, $base_img_dir);
	
	foreach ($selected_dirs as $dir) {
		$dir = trim($dir, "/ ");
		$dir = str_replace("/", "_", $dir);
		$selected_dir_current .= $dir . "_";
		$selected_dir_current_id = $selected_dir_current . "link";
		$selectionScript .= "document.getElementById('$selected_dir_current_id').className='$selected_class';";
	}
	
	$imageRoot = $_MY_CONFIG->get('imgFolderRoot');
	
	if (!$imageRoot or $imageRoot == "")
		$imageRoot = "/images/";
	
	$imageRoot = trim($imageRoot, "/ ");
	$imageRoot = "/" . $imageRoot;
	
	if ($_MY_CONFIG->get('imgFolderRestrict') == "1") {
		$img_site_path = $cms->get_path('live') . $imageRoot . str_replace($imageRoot, "", $root);
		$file_path = $cms->get_path('root') . $imageRoot . str_replace($imageRoot, "", $root);
	} else {
		$file_path = $cms->get_path('root') . $root;
	}
	
	$curr_dir = $file_path;
	
	if ($dir_handle = opendir($file_path)) {
		while (($file = readdir($dir_handle)) !== false) {
			if (is_file($curr_dir . $file) and $file != "." and $file != ".." and (!(strcasecmp(substr($file, -4), ".gif")) or !(strcasecmp(substr($file, -4), ".jpg")) or !(strcasecmp(substr($file, -4), ".png")) or !(strcasecmp(substr($file, -4), ".bmp")))) {
				$imgsize = @ getimagesize($curr_dir . $file);
				array_push($img_array, array (
					'filename' => $img_site_path.$file,
					'width' => $imgsize[0],
					'height' => $imgsize[1]
				));
			}
		}
		closedir($dir_handle);
	}
	
	$uploadedImages = displayAndResizeImages($img_array);
	$objResponse->addAssign($root_content_id, 'innerHTML', $content);
	$objResponse->addAssign('uploadedImg', 'innerHTML', $uploadedImages);
	
	if ($show == "true") {
		$show = "false";
		$class = "folderBrowserSelectedOption";
		$objResponse->addScriptCall("if (document.getElementById('$root_content_id').innerHTML=='') document.getElementById('$root_content_id').style.display='none';else document.getElementById('$root_content_id').style.display='';");
	} else {
		$show = "true";
		$class = "folderBrowserDeselectedOption";
		$objResponse->addScriptCall("document.getElementById('$root_content_id').style.display='none';");
	}
	
	if ($base_img_dir == $root)
		$selectionScript = "";
	
	$objResponse->addScriptCall("if(el=document.getElementById('$root_link_id')) el.setAttribute('href','javascript:jax.icall(\'myblog\',\'myxShowFolder\',\'$root\',\'$show\');');$selectionScript");
	$objResponse->sendResponse();

	return true;
}

function myxViewImages($option, $id = 0) {
	global $frontadmin, $_MY_CONFIG;
	$db = &cmsInstance('CMSDb');
	$cms =& cmsInstance('CMSCore');
	
	$cms->load('libraries', 'user');
	$objResponse = new JAXResponse();
	
	switch ($option) {
		case 'all' :
			$imageRootDir = $_MY_CONFIG->get('imgFolderRoot');
			
			if (!$imageRootDir or $imageRootDir == "")
				$imageRootDir = "/images/";
			
			$imageRootDir = trim($imageRootDir, "/ ");
			$imageRootDir = "/" . $imageRootDir . "/";
			
			if ($_MY_CONFIG->get('imgFolderRestrict') == "1") {
				$imageRootDir .= $cms->user->id . "/";
			}
			
			$imageRootDirName = trim($imageRootDir, "/ ");
			
			if (strrpos($imageRootDirName, "/") > 0)
				$imageRootDirName = substr($imageRootDirName, strrpos($imageRootDirName, "/") + 1);
			
			$root_content_id = str_replace("/", '_', $imageRootDir) . "content";
			$root_link_id = str_replace("/", '_', $imageRootDir) . "link";
			$browsercontent = "<a id=\"$root_link_id\" href=\"javascript:void(0);\" onclick=\"jax.icall('myblog','myxShowFolder','$imageRootDir','false')\">$imageRootDirName</a> \n<div id=\"$root_content_id\" name=\"$root_content_id\"></div>";
			$objResponse->addAssign('folderBrowser', 'innerHTML', $browsercontent);
			$objResponse->addScriptCall("jax.icall('myblog','myxShowFolder','$imageRootDir');");
			$objResponse->addAssign('allImages', 'className', 'imgBrowserSelectedOption');
			$objResponse->addAssign('entryImages', 'className', 'imgBrowserDeselectedOption');
			$objResponse->sendResponse();
		
		case 'entry' :
			$uploadedImages = "";
			$currentImages = "";
			$query = "SELECT * FROM #__myblog_images WHERE (contentid='$id' or contentid='0') and user_id='{$cms->user->id}'";
			$db->query($query);
			$imgRows = $db->get_object_list();
			$img_array = array ();
			
			if ($imgRows) {
				foreach ($imgRows as $img) {
					$imgsize = getimagesize($img->filename);
					array_push($img_array, array (
						'filename' => $img->filename,
						'width' => $imgsize[0],
						'height' => $imgsize[1]
					));
				}
			}
			
			$uploadedImages = displayAndResizeImages($img_array);
			$objResponse->addAssign('uploadedImg', 'innerHTML', $uploadedImages);
			$objResponse->addAssign('folderBrowser', 'innerHTML', "");
			$objResponse->addAssign('allImages', 'className', 'imgBrowserDeselectedOption');
			$objResponse->addAssign('entryImages', 'className', 'imgBrowserSelectedOption');
			$objResponse->sendResponse();
	}
}

 
function viewEntries($limit, $limitstart) {
	global $my;
	
	$frontadmin= new MB_Frontadmin();
	$objResponse = new JAXResponse();
	
	$mb_admin_controller = new MB_Admin_Controller();
	$mb_admin_controller->init();

	//$content = $frontadmin->viewEntries(array (
	$content = $mb_admin_controller->_get_entries(array (
		'limit' => $limit,
		'limitstart' => $limitstart
	));
	$content = preg_replace("/([\x80-\xFF])/e", "chr(0xC0|ord('\\1')>>6).chr(0x80|ord('\\1')&0x3F)", $content);
	$objResponse->addAssign('blogEntries', 'innerHTML', "$content");
	$objResponse->addAssign('loading_status', 'innerHTML', "");
	$objResponse->sendResponse();
}
// 

/***
 * Search related article through ajax in the writing pad
 */ 
function myxSearchPosts($text, $page) {
	global $_MY_CONFIG;
	
	$db = &cmsInstance('CMSDb');
	
	$text 		= "+" . $text;
	$text		= ereg_replace(' ', "+", $text);
	$Itemid 	= myGetItemId();
	$sections	= $_MY_CONFIG->get('managedSections');
	
	$objResponse = new JAXResponse();

	$text = preg_replace("/([\xC2\xC3])([\x80-\xBF])/e", "chr(ord('\\1')<<6&0xC0|ord('\\2')&0x3F)", $text);
	
	$strSQL	= "SELECT COUNT(*) FROM #__content AS c, "
			. "#__myblog_permalinks AS p "
			. "WHERE c.sectionid IN ({$sections}) "
			. "AND c.id=p.contentid "
			. "AND c.state='1' "
			. "AND c.publish_up < now() "
			. "AND MATCH(c.`title`,c.`fulltext`) AGAINST('{$text}' in boolean mode)";
	$db->query($strSQL);

// 	$query = sprintf("SELECT count(*) FROM #__content as c,#__myblog_permalinks as p WHERE c.sectionid IN ($sections) and c.id=p.contentid and c.state='1' and c.publish_up < now() and MATCH (c.`title`,c.`fulltext`) AGAINST ('%s' in boolean mode)", $text);
// 	$db->query($query);
	$total = $db->get_value();

	$maxPerPage = 5;
	$start = min($total, max(0, ($page -1) * $maxPerPage));
	$max_this_page = min($total - $start, $maxPerPage);
	$end = $start + $max_this_page;
	
	$strSQL	= "SELECT c.*,p.permalink FROM #__content AS c, "
			. "#__myblog_permalinks AS p "
			. "WHERE c.sectionid IN ({$sections}) "
			. "AND c.id=p.contentid "
			. "AND c.state='1' "
			. "AND c.publish_up < now() "
			. "AND MATCH(c.`title`,c.`fulltext`) AGAINST('{$text}' in boolean mode)"
			. "ORDER BY c.created DESC LIMIT {$start},{$max_this_page}";
	$db->query($strSQL);
	//$query = sprintf("SELECT c.*,p.permalink FROM #__content as c,#__myblog_permalinks as p WHERE c.sectionid IN ($sections) and c.id=p.contentid and c.state='1' and c.publish_up < now() and MATCH (c.`title`,c.`fulltext`) AGAINST ('%s' in boolean mode) order by c.created desc limit $start,$max_this_page", $text);
	//$db->query($query);
	$rows = $db->get_object_list();
	$searchresult = "";
	$excerptLength = 60;
	$titleLength = 45;
	
	if ($rows) {
		foreach ($rows as $row) {
			$excerpt = substr(strip_tags($row->fulltext), 0, $excerptLength);
			$row->title = preg_replace("/([\x80-\xFF])/e", "chr(0xC0|ord('\\1')>>6).chr(0x80|ord('\\1')&0x3F)", $row->title);
			$row->permalink = preg_replace("/([\x80-\xFF])/e", "chr(0xC0|ord('\\1')>>6).chr(0x80|ord('\\1')&0x3F)", $row->permalink);
			$title = substr(strip_tags($row->title), 0, $titleLength);
			
			if (strlen(strip_tags($row->title)) > $titleLength)
				$title .= "...";
			
			if (strlen(strip_tags($row->fulltext)) > $excerptLength)
				$excerpt .= "...";
			
			$titleLink = cmsSefAmpReplace("index.php?option=com_myblog&show={$row->permalink}&Itemid={$Itemid}");
			$author = myUserGetName($row->created_by);
			
			// add 'myblog_insert' as a cue to editor that the targe need to be removed when saving
			$searchresult .= "<div class=\"result\"><div><a title=\"$row->title\" class=\"title\" href=\"$titleLink\" target=\"_blank myblog_insert\">$title</a></div> <div class=\"posted\">Posted:$row->created</div> <div>$excerpt</div></div>";
		}
		
		if ($start > 0 or $end < $total)
			$searchresult .= "<br/>";
		
	}
	
	if ($searchresult == "")
		$searchresult = "No results found.";
	
	$objResponse->addAssign("searchResults", "innerHTML", $searchresult);
	$objResponse->sendResponse();
}

function myxPingMyBlog() {
	$objResponse = new JAXResponse();
	$objResponse->addScriptCall("setTimeout('jax.call(\\'myblog\\',\\'myxPingMyBlog\\');',180000);");
	$objResponse->sendResponse();
	
	return;
}

/**
 * Loads a directory based on parameter
 **/
function myxLoadFolder($folder, $isAdvance= false){
	global $_MY_CONFIG;
	
	$objResponse = new JAXResponse();
	
	$cms =& cmsInstance('CMSCore');
	$cms->load('libraries', 'user');
	
	if(!class_exists('MYMediaBrowser'))
		include_once(MY_LIBRARY_PATH . '/imagebrowser.class.php');

	$imgBrowser	= new MYMediaBrowser();
		
	// Check if user really allowed to go out of current directory.
	if($_MY_CONFIG->get('imgFolderRestrict') && $folder == '/..' && $cms->user->id != '62'){
	    $objResponse->addScriptCall('alert("Not allowed to browse parent directory.");');
	}else if($_MY_CONFIG->get('imgFolderRestrict') && $folder == '/..' && $cms->user->id == '62'){
		
		// User is allowed
		//$html		= $imgBrowser->browse($folder);
	    //$printHTML = mnBrowse("$folder/");
	    //$objResponse->addAssign('mn_fileExplorer','innerHTML', $html);
	}else {
		
		//$html		= $imgBrowser->browse($folder);
		//$printHTML = mnBrowse("$folder/");
		//$objResponse->addAssign('mn_fileExplorer', 'innerHTML', $html);
	}
	
	$data = $imgBrowser->_getFiles($folder, false);
	$objResponse->addScriptCall('myblog.browserBuild', $data, $isAdvance);
	return $objResponse->sendResponse();
}

function myxEditBloggerProfile(){
	$objResponse = new JAXResponse();
	$cms = &cmsInstance('CMSCore');
	
	$cms->db->query("SELECT `description` FROM #__myblog_user WHERE `user_id` = '{$cms->user->id}'");
	$desc = $cms->db->get_value();

	$html = '<form id="blogger-info" action="#" method="POST">
				<textarea class="button" rows="10" style="width:80%" name="description">'.$desc.'</textarea><br />
				<input type="submit" value="Save" />
				<input type="button" onclick="jQuery(\'p#blogger-form\').fadeOut();jQuery(\'p#blogger-desc\').fadeIn();" value="Cancel"/>
             </form>';
	$objResponse->addScriptCall("jQuery('p#blogger-form').fadeIn();jQuery('p#blogger-desc').fadeOut();");
	$objResponse->addAssign("blogger-form", "innerHTML", $html);
	$objResponse->sendResponse();
}

function myxSaveBloggerProfile($newprofile){
	$objResponse = new JAXResponse();
	$cms = &cmsInstance('CMSCore');
	
	$newprofile['description'] = strip_tags($newprofile['description']);
	if(!empty($newprofile)){
		$cms->db->update('#__myblog_user', array('description' => $newprofile['description']), array('user_id' => $cms->user->id));
	}
	$cms->db->query("SELECT `description` FROM #__myblog_user WHERE `user_id` = '{$cms->user->id}'");
	$desc = $cms->db->get_value();
	
	$objResponse->addAssign("blogger-desc", "innerHTML", $desc);
	$objResponse->sendResponse();
}


// function myxTogglePublaish(){
// 	
// 	$cms = &cmsInstance('CMSCore');
// 	while (@ ob_end_clean());
// 	ob_start();
// 	$cms->db->query("SELECT published FROM #__content WHERE id=$id");
// 	$publish = $cms->db->get_value();
// 	$publish = intval(!($publish));
// 	$cms->db->query("UPDATE #__content SET published='$publish' WHERE id=$id");
// 	
// 	$objResponse = new JAXResponse();
// 	if ($publish) {
// 		$objResponse->addAssign('pubImg' . $id, 'src', MY_COM_LIVE.'/images/publish_g.png');
// 		$d['id'] = $id;
// 	} else {
// 		$objResponse->addAssign('pubImg' . $id, 'src', MY_COM_LIVE.'/images/publish_g.png');
// 		$d['id'] = $id;
// 	}
// 	return $objResponse->sendResponse();
// }

/**
 * Toggle publish/unpublish state of the comment
 * Must verify that the comments article does belong to current logged user
 */ 
function myxToggleCommentPublish($id){
	
	$objResponse = new JAXResponse();
	$cms = &cmsInstance('CMSCore');
	$cms->load('libraries', 'user');
	
	// Check the content, make sure it belongs to current user
	$cid = $cms->db->get_value("SELECT contentid FROM #__jomcomment WHERE `id`='$id'");
	$author = $cms->db->get_value("SELECT created_by FROM #__content WHERE `id`='$cid'");
	
	if($author != $cms->user->id){
		// Permission error
		return;
	}
	
	$publish = $cms->db->get_value("SELECT published FROM #__jomcomment WHERE `id`='$id'");
	$publish = intval(!($publish));
	$cms->db->query("UPDATE #__jomcomment SET published='$publish' WHERE `id`='$id'");
	
	if ($publish) {
		$objResponse->addAssign('pubImg' . $id, 'src', MY_COM_LIVE.'/images/publish_g.png');
		$d['id'] = $id;
	} else {
		$objResponse->addAssign('pubImg' . $id, 'src', MY_COM_LIVE.'/images/publish_x.png');
		$d['id'] = $id;
	}
	
	return $objResponse->sendResponse();
}

/**
 * Approve all comments for current users' article
 */ 
function myxCommentApproveAll(){
	
	$objResponse = new JAXResponse();
	$cms = &cmsInstance('CMSCore');
	$cms->load('libraries', 'user');
	$cms->load('helper' , 'url');
	
	// Get blog entries that this user owns	
	$strSQL	= "SELECT id FROM #__content WHERE created_by='{$cms->user->id}'";
	$cms->db->query($strSQL);
	$result	= $cms->db->get_object_list();
	$rows	= array();
	
	foreach($result as $row){
		$rows[]	= $row->id;
	}
	$rows	= implode(',', $rows);
	
	$strSQL	= "UPDATE #__jomcomment SET `published`='1' WHERE `contentid` IN({$rows}) "
			. "AND `published`='0'";
	$cms->db->query($strSQL);

	$link	= cmsSefAmpReplace('index.php?option=com_myblog&task=showcomments' , false);
	$objResponse->addScriptCall('alert' , 'Unpublished comments published.');
	$objResponse->addScriptCall('document.location.href="' . $link .'";');
		
	return $objResponse->sendResponse();
}

/**
 * remove all unpublished comment for current user's article
 */ 
function myxCommentRemoveUnpublished(){
	$objResponse = new JAXResponse();
	$cms = &cmsInstance('CMSCore');
	$cms->load('libraries', 'user');
	$cms->load('helper' , 'url');
	
	// Get blog entries that this user owns	
	$strSQL	= "SELECT id FROM #__content WHERE created_by='{$cms->user->id}'";
	$cms->db->query($strSQL);
	$result	= $cms->db->get_object_list();
	$rows	= array();
	
	foreach($result as $row){
		$rows[]	= $row->id;
	}
	$rows	= implode(',', $rows);
	
	$strSQL	= "DELETE FROM #__jomcomment WHERE `published`='0' AND `contentid` IN({$rows})";
	$cms->db->query($strSQL);

	$link	= cmsSefAmpReplace('index.php?option=com_myblog&task=showcomments' , false);
	$objResponse->addScriptCall('alert' , 'Unpublished comments removed.');
	$objResponse->addScriptCall('document.location.href="' . $link .'";');
	
	return $objResponse->sendResponse();
}

class Myblog {
	var $task;
	var $cms;
	var $adminTask ;
	
	function Myblog(){
		$this->adminTask = array('adminhome', 'edit', 'delete', 'write', 'showcomments', 
			'bloggerpref', 'bloggerstats', 'media');
	}
	
	function init(){
		$this->cms =& cmsInstance('CMSCore');
		$this->cms->load('libraries', 'user');
		$this->cms->load('helper', 'url');
	
		$this->task = cmsGetVar('task', '', 'GET');
		if(empty($this->task)){
			// By default, it is browse
			$this->task = 'browse';
			
			# BLOODY MESS: task could be under a different name
			$show = false;
			$show = cmsGetVar('show', '', 'GET');
			if(!empty($show))
				$this->task = 'show';

			// Display blogger's entries. Need to check the $show because, 
			// we need to append the index.php?show=xxx.html&blogger=USERNAME in the permalink and it will cause
			// issues with the permalink.
			$author = cmsGetVar('blogger', '', 'GET');
			if(!empty($author) && $show == '')
				$this->task = 'author';
		}
	}
	
	
	/**
	 * Check if the task perform is valid for current user
	 */	 	
	function _validate_access(){
	}
	
	/**
	 * Default view
	 */	 	
	function index(){
		include(MY_COM_PATH . '/jax.myblog.php');
		
		if(in_array($this->task, $this->adminTask)){
			include_once(MY_COM_PATH . '/task/' . strtolower($this->task) . '.php');
			
			$cName	= 'Myblog' . ucfirst($this->task) . 'Task';
			$obj	= new $cName();
			$obj->execute();
		}
		else if(in_array($this->task, $jaxFuncNames))
			return;
		else
			$this->browse();
	}
	
	function view(){
		$this->show();
	}
	
// 	function ajaxupload(){
// 		myxAjaxUpload();
// 	}
	
	function thumb(){
		include_once(MY_LIBRARY_PATH . '/imagebrowser.class.php');
		
		$path	= $_GET['fn'];
		$image	= new MYMediaBrowser($path);
		$image->thumbnail($_GET['maxwidth']);
	}

	function thumb_noresize(){
		mnPrintImage($_GET['fn']);
	}
	
	// Return javascript that loads the editor
	function editorjs(){
	}
	

	
	function printblog(){
		$this->show();
	}
	
	function userblog(){
		global $my;
	    include_once(MY_COM_PATH . '/frontview.php');
		if ($this->cms->user->id == "0") { # If user not logged in, cannot view his/her blog
			echo '<div id="fp-content">';
			echo 'You need to login to view your blog';
			echo '</div>';
		}else{
			echo '<div id="myBlog-wrap">';
			mb_showViewerToolbar("home");
			$frontview = new MB_Frontview();
			$frontview->attachHeader();
			echo $frontview->browse('userblog');
			echo '</div>';
			echo getPoweredByLink();
		}
	}
	
	function execute(){
		global $_MY_CONFIG;

		include(MY_COM_PATH . "/language/" . $_MY_CONFIG->get('language'));

		if(in_array($this->task, $this->adminTask)){
		
			$this->cms->load('helper' , 'url');
			
 			$session	= cmsGetVar('session' , '' , 'GET');

			if ( ($this->cms->user->id == "0" || isset($session) )&& $this->task == 'write') {
				include_once(MY_COM_PATH . '/task/'.  strtolower($this->task) .'.php');
				
				$cname = 'Myblog'.ucfirst($this->task).'Task';
				$obj = new $cname();
				$obj->login();
				return;
			}
			
			if ($this->cms->user->id == "0") {
				echo '<div id="fp-content">';
				echo $MYBLOG_LANG['_MB_NO_POST'];
				echo '</div>';
				return;
			}
		}
		
		// Dont process ajax calls.
		if($this->task != 'azrul_ajax' && $this->task != 'ajaxtaglist' && $this->task != 'thumb'){
			include_once(MY_COM_PATH . '/task/'.  strtolower($this->task) .'.php');
			$cname = 'Myblog'.ucfirst($this->task).'Task';
			$obj = new $cname();
			$obj->execute();
		}
		
		if($this->task == 'ajaxtaglist'){
			include_once(MY_COM_PATH . '/task/ajaxtaglist.php');
		}
		
		if($this->task == 'thumb'){
			$this->thumb();
		}
	}
}

$myblog = new Myblog();
$myblog->init();
$task = $myblog->task;

$myblog->execute();	
