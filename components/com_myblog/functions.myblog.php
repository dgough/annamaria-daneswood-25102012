<?php
(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

// Add 3rd party compatibilities
if(!defined('CMSLIB_DEFINED'))
	include_once(dirname(dirname(dirname(__FILE__))) . '/components/libraries/cmslib/spframework.php');

/**
 *	Info: To reset the ordering for older entries and set the new ordering for a content
 *	@params: $entry (blogContent) 
 **/
function mySortOrder(&$entry){
	$cms	=& cmsInstance('CMSCore');
	$strSQL	= "SELECT * FROM #__content WHERE `catid`='{$entry->catid}'";

	$cms->db->query($strSQL);
	$result	= $cms->db->get_object_list();

	// If there is already an ordering, dont perform anything since My Blog doesn't allow user to change the ordering.
	// User may be editing the content, we dont want to update the ordering.
	if(!$entry->ordering)
	{
		// We give ourself a new ordering id for this specific content it.
		if(count($result) == 0)
		{
			$entry->ordering	= 1;
		}
		else
		{
			$entry->ordering	= count($result) + 1;
		}
		//$entry->store();
		$strSQL	= "UPDATE #__content SET `ordering`='{$entry->ordering}' WHERE `id`='{$entry->id}'";
		$cms->db->query($strSQL);
	}
}

/**
 * Retrieves pagination object
 **/ 
function myPagination($total, $limitstart, $limit)
{
	$pagination	= new stdClass();
	
	if(cmsVersion() == _CMS_JOOMLA15){
		jimport('joomla.html.pagination');
		$pageNav				= new JPagination($total, $limitstart, $limit);
		
		$pagination->limitstart	= $limitstart;
		$pagination->limit		= $limit;
		$pagination->total		= $total;
		$pagination->footer		= $pageNav->getListFooter();
		$pagination->links		= $pageNav->getPagesLinks();
	}
	else{
		$cms	=& cmsInstance('CMSCore');
		include_once($cms->get_path('root') . '/administrator/includes/pageNavigation.php');
		// We assume that this is a joomla 1.0 or mambo.
		$pageNav	= new mosPageNav($total, $limitstart, $limit);
		$pagination->limitstart	= $limitstart;
		$pagination->limit		= $limit;
		$pagination->total		= $total;
		$pagination->footer		= $pageNav->getListFooter();
		$pagination->links		= $pageNav->getPagesLinks();
	}
	return $pagination;
}

function mySendTrackbacks($contentId, $trackbacks){
	global $mainframe, $_MY_CONFIG;

	$cms		=& cmsInstance('CMSCore');
	$sections	= $_MY_CONFIG->get('managedSections');
	
	if(!class_exists('Trackback'))
		include_once ($cms->get_path('root').'/components/com_myblog/libraries/trackback/trackback_cls.php');
	
	$strSQL	= "SELECT a.*, b.permalink, c.name "
			. "FROM #__content AS a, "
			. "#__myblog_permalinks AS b, "
			. "#__users AS c "
			. "WHERE a.id=b.contentid "
			. "AND a.id='{$contentId}' "
			. "AND a.created_by=c.id "
			. "AND a.sectionid IN ($sections)";

	$cms->db->query($strSQL);
	$row	= $cms->db->first_row();
	$Itemid	= myGetItemId();
	
	if($trackbacks && $trackbacks != '' && $row){
		$trackbacks	= explode(',', $trackbacks);
		
		foreach($trackbacks as $url){
			// Check if we have already sent a trackback request.
			$strSQL	= "SELECT COUNT(*) FROM `#__myblog_tb_sent` "
					. "WHERE `url`='{$url}' AND "
					. "`contentid`='{$row->id}'";
					
			$cms->db->query($strSQL);
			
			// Since we never send a request before, send it.
			if($cms->db->get_value() <= 0){
				$trackback	= new Trackback($mainframe->getCfg('sitename'), $row->name, 'UTF-8');
				$url		= trim(strip_tags($url));
				
				$cms->load('helper','url');
				
				$content			= new stdClass();
				$content->id		= $row->id;
				$content->url		= myGetPermalinkUrl($row->id);
				$content->title		= $trackback->cut_short($row->title);
				$content->excerpt	= $trackback->cut_short($row->introtext . $row->fulltext);

				if($trackback->ping($url, $content->url, $content->title, $content->excerpt)){
					// Add into the database that we actually sent a trackback
					$strSQL	= "INSERT INTO `#__myblog_tb_sent` SET `url`='{$url}',`contentid`='{$content->id}'";
					$cms->db->query($strSQL);
				}
			}
		}
	} else {
		return false;
	}
}

function myAllowedGuestView($context){
	global $_MY_CONFIG;
	
	$cms		=& cmsInstance('CMSCore');
	
	$cms->load('libraries','user');

	$allowed	= true;

	if($context == 'intro'){
		if($_MY_CONFIG->get('viewIntro') == '2' && $cms->user->id == '0')
			$allowed	= false;
	} else if($context == 'entry'){
		if($_MY_CONFIG->get('viewEntry') == '2' && $cms->user->id == '0')
			$allowed	= false;
	}
	return $allowed;
}

// Add a list of new and existing tags based on the array
function myAddTags($contentId, $tagArray){
	global $_MY_CONFIG, $my;
	
	$cms    =& cmsInstance('CMSCore');
	$cms->load('libraries', 'user');

	include_once(MY_LIBRARY_PATH.'/tags.class.php');
	
	$tagObj			= new MYTags();

	// delete old tags used by this current content id.
	$strSQL			= "DELETE FROM #__myblog_content_categories WHERE `contentid`='{$contentId}'";
// 	var_dump($tagArray);
// 	echo $strSQL;exit;
	$cms->db->query($strSQL);

	if($tagArray != ''){
		// Add tags
		foreach($tagArray as $tag){
			$tagObj->add(trim($tag));
	
			// Update the blog entry with this tag's id.
			$strSQL	= "INSERT INTO #__myblog_content_categories "
					. "(`contentid`,`category`) VALUES ($contentId, $tagObj->insertId)";
	
			$cms->db->query($strSQL);
		}
	}
}

/**
 *	Get the tag name since we have a slug now, we need to map it to the correct tag.
 **/ 
function myGetTagName($tag){
	$cms	=& cmsInstance('CMSCore');
	
	$strSQL	= "SELECT name FROM #__myblog_categories WHERE `name` LIKE '{$tag}' OR `slug` LIKE '{$tag}'";
	$cms->db->query($strSQL);
	
	return $cms->db->get_value();
}

function myGetUsedTags($userId){
	$db	=& cmsInstance('CMSDb');
	
	$strSQL	= "SELECT DISTINCT c.name, c.slug FROM #__content AS a, "
			. "#__myblog_content_categories AS b, "
			. "#__myblog_categories AS c "
			. "WHERE a.id=b.contentid "
			. "AND c.id=b.category "
			. "AND a.created_by='{$userId}'";

	$db->query($strSQL);
	return $db->get_object_list();
}

/**
 *	Get list of tags for a given content
 */ 
function myGetTags($contentid) {
	$db = &cmsInstance('CMSDb');
	
	$query = "SELECT b.id,b.name, b.slug FROM #__myblog_content_categories as a,#__myblog_categories as b WHERE b.id=a.category AND a.contentid='$contentid' ORDER BY b.name DESC";
	$db->query($query);
	$result = $db->get_object_list();
	
	// Check for slug if it exists, use it as name
	for($i = 0; $i < count($result); $i++){
		$tag	= $result[$i];
		
		if($tag->slug == '')
			$tag->slug	= $tag->name;
	}
	
	
	return $result;
}

function myGetSid($length = 12){
	$token	= md5(uniqid('a'));
	$sid	= md5(uniqid(rand(), true));
	
	$return	= '';
	for($i = 0; $i < $length; $i++){
		$return .= substr($sid, rand(1, ($length-1)), 1 );
	}
	return $return;
}

function myNotifyAdmin($contentId, $author = '', $title = '', $text = '', $isNew){
	global $_MY_CONFIG, $mainframe, $MYBLOG_LANG;
	$status	= false;
	$cms	=& cmsInstance('CMSCore');
	
	// Notify administrators.
	if($_MY_CONFIG->get('allowNotification')){
		$emails 	= $_MY_CONFIG->get('adminEmail');
		
		$recipients	= split(',', $_MY_CONFIG->get('adminEmail'));
		
		if(!class_exists('MYMailer'))
			include_once(MY_LIBRARY_PATH . '/mail.class.php');
		
		if(!class_exists('AzrulJXTemplate'))
			include_once($cms->get_path('plugins') . '/system/pc_includes/template.php');

		$sid	= myGetSid();
		$date	= strftime("%Y-%m-%d %H:%M:%S", time() + ($mainframe->getCfg('offset') * 60 * 60));

		// Maintenance mode, clear existing 'sid'
		$strSQL	= "DELETE FROM #__myblog_admin WHERE `sid`='{$sid}'";
		$cms->db->query($strSQL);
		
		// Re-insert new sid
		$strSQL	= "INSERT INTO #__myblog_admin SET `sid`='{$sid}', `cid`='{$contentId}', `date`='{$date}'";
		$cms->db->query($strSQL);

		// Publish link
		$publish	= 'index.php?option=com_myblog&task=admin&do=publish&sid=' . $sid;
		
		// Unpublish link
		$unpublish	= 'index.php?option=com_myblog&task=admin&do=unpublish&sid=' . $sid;
		
		// Delete link
		$delete		= 'index.php?option=com_myblog&task=admin&do=remove&sid=' . $sid;

		$template	= new AzrulJXTemplate();
		
		$text		= strip_tags($text);
		
		if($isNew){
			$content	= $template->fetch(MY_TEMPLATE_PATH . "/_default/new.notify.tmpl.html");
			
			$content	= str_replace('%PUBLISH%', cmsSefAmpReplace($publish), $content);
			$content	= str_replace('%UNPUBLISH%', cmsSefAmpReplace($unpublish), $content);
			$content	= str_replace('%DELETE%', cmsSefAmpReplace($delete), $content);
			$content	= str_replace('%AUTHOR%', $author, $content);
			$content	= str_replace('%TITLE%', $title, $content);
			$content	= str_replace('%ENTRY%', $text, $content);
			
			$title		= $MYBLOG_LANG['TPL_NOTIFY_MAILTITLE'];
		} else {
			// Entry is updated.
			$content	= $template->fetch(MY_TEMPLATE_PATH . "/_default/update.notify.tmpl.html");
			
			$content	= str_replace('%UNPUBLISH%', cmsSefAmpReplace($unpublish), $content);
			$content	= str_replace('%DELETE%', cmsSefAmpReplace($delete), $content);
			$content	= str_replace('%AUTHOR%', $author, $content);
			$content	= str_replace('%TITLE%', $title, $content);
			$content	= str_replace('%ENTRY%', $text, $content);
			
			$title		= $MYBLOG_LANG['TPL_NOTIFY_UPDATEMAILTITLE'];
		}
		$mail		= new MYMailer();
		
		$status		= $mail->send($mainframe->getCfg('mailfrom'), $recipients, $title, $content);
	}
	return $status;
}

function myClearCache(){
	global $mainframe;

	$cms    =& cmsInstance('CMSCore');
	$cms->load('helper','directory');

	$list   = cmsGetFiles(MY_CACHE_PATH, '');

	foreach($list as $file){
	    // Only remove files that contains the naming convention for cache_
	    if(strstr($file, 'cache_')){
	        @unlink(MY_CACHE_PATH . '/' . $file);
		}
	}

	// For Joomla 1.0 we would still need to clear the cached files
	// located in $mosConfig_cachepath
	if(cmsVersion() == _CMS_JOOMLA10 || cmsVersion() == _CMS_MAMBO){
	    $list   = cmsGetFiles($mainframe->getCfg('cachepath'), '');
	    foreach($list as $file){
	        if(strstr($file, 'cache_'))
	            @unlink($mainframe->getCfg('cachepath') . '/' . $file);
		}
	}
}

function myGetUser($session)
{
	$cms	=& cmsInstance('CMSCore');
	$cms->load('libraries' , 'user');
	
	if($session)
	{
		// $vars[0] : userid
		// $vars[1] : sessionid
		$vars	= explode(':' , $session);
		
		$strSQL	= "SELECT userid, username, usertype FROM #__session WHERE `userid`='{$vars[0]}' "
				. "AND `session_id`='{$vars[1]}'";

		$cms->db->query( $strSQL );
		$data	= $cms->db->first_row( $strSQL );

		if($data)
		{
			$my				= new stdclass();
			$my->id			= $data->userid;
			$my->username	= $data->username;
			$my->usertype	= $data->usertype;

			return $my;
		}
	}
	return false;
}

/**
 * Get the status if user is allowed to post
 * Return: boolean
 **/
function myGetUserCanPost(){
	global $_MY_CONFIG;
	
	// Check posting permissions
	$posterIds  		= explode(',', $_MY_CONFIG->get('allowedPosters'));
	$postGroups 		= explode(',', $_MY_CONFIG->get('postGroup'));
	$extraPostGroups    = explode(',', $_MY_CONFIG->get('extraPostGroups'));
	$adminPostGroups    = explode(',', $_MY_CONFIG->get('adminPostGroup'));
	
	$disallowedUsers	= explode(',', $_MY_CONFIG->get('disallowedPosters'));
	
	$cms =& cmsInstance('CMSCore');
	$cms->load('libraries', 'user');
	
	$userId		= $cms->user->id;
	$userType	= $cms->user->usertype;
	$userName	= $cms->user->username;
		
	if(!$userId){
		return false;
	}
	
	array_walk($postGroups, 'myTrim');
	array_walk($extraPostGroups, 'myTrim');
	array_walk($adminPostGroups, 'myTrim');

	# If user is not allowed to post
	if (!in_array($userType, $adminPostGroups)) {
		if (!in_array($userType, $extraPostGroups) and !in_array($userType, $postGroups) AND !(in_array($userId, $posterIds) OR in_array(myUserGetName($userId),$posterIds))) {
			return false;
		}
	}

	// Check if users is specifically blocked from posting.
	if(in_array($userId, $disallowedUsers) || in_array($userName, $disallowedUsers))
		return false;

	return true;
}

/**
 * Get the status if user is allowed to publish or unpublish
 * Return: boolean
 **/
function myGetUserCanPublish(){
	global $_MY_CONFIG, $my;

	$publishRights  = false;
	
	if(cmsVersion() == _CMS_JOOMLA10 || cmsVersion() == _CMS_MAMBO){
		$userId 	= $my->id;
		$userType	= $my->usertype;
	}elseif(cmsVersion() == _CMS_JOOMLA15){
		$user = & JFactory::getUser();
		$userId		= $user->id;
		$userType	= $user->usertype;
	}
	
	// Check publishing permissions
	$posterIds      	= explode(',', $_MY_CONFIG->get('allowedPublishers'));
	$publishers     	= explode(',', $_MY_CONFIG->get('publishControlGroup'));
	$extraPublishGroups = explode(',', $_MY_CONFIG->get('extraPublishGroups'));
	$adminPublishGroup	= explode(',', $_MY_CONFIG->get('adminPublishControlGroup'));

	array_walk($extraPublishGroups, 'myTrim');

	if (in_array($userType, $extraPublishGroups) or (in_array($userId, $posterIds) or in_array(myUserGetName($userId),$posterIds)) or in_array($userType, $publishers) or in_array($userType, $adminPublishGroup))
			return true;

	return false;
}

/**
 * Get the username of the specific userid
 **/

/**
 * Get the status of the azrul video mambot
 * Return: boolean
 **/
function myGetAzVideoBot(){
	$cms        =& cmsInstance('CMSCore');
	$db			=& cmsInstance('CMSDb');
	$azVideoId  = '';

	// Get the azrul video mambot's id
	$strSQL		= "SELECT `id` FROM #__mambots WHERE `element`='azvideobot' AND `folder`='content'";
	$db->query($strSQL);
	
	// Check if azrulvideomambot file even exists
	if(file_exists($cms->get_path('plugins') . '/content/azvideobot.php') && ($azVideoId = $db->get_value())){
		$strSQL = "SELECT `my_published` FROM #__myblog_mambots WHERE `mambot_id`='{$azVideoId}'";
		$db->query($strSQL);

		if($db->get_value() == "1"){
		    return true;
		} else{
		    return false;
		}
	} else {
	    return false;
	}
}

/**
 * Get the status of Jom Comment installation and integrated on the site.
 * Return: boolean
 **/
function myGetJomComment(){
	global $_MY_CONFIG;

	$cms        =& cmsInstance('CMSCore');
	
	if($_MY_CONFIG->get('useComment') && file_exists($cms->get_path('root') . '/administrator/components/com_jomcomment/config.jomcomment.php'))
	    return true;

	return false;
}

/**
 *	Get list of tags for a given user
 */ 
function myGetUserTags($uid) {
	global $sections;
	$db = &cmsInstance('CMSDb');
	$db->query("SELECT distinct(b.name) from #__content as c,#__myblog_content_categories as a,#__myblog_categories as b WHERE c.id=a.contentid and b.id=a.category and c.created_by=$uid and c.state='1' and c.sectionid in ($sections) and c.publish_up < now() ORDER BY b.name DESC");
	return $db->get_object_list();
}

/**
 *	Replace newlines with <br/> tags
 */ 
function my_strict_nl2br($text, $replac = " <br />") {
	return preg_replace("/\r\n|\n|\r/", $replac, $text);
}

/**
 *	Retrieve number of entries for a given user
 */ 
function myCountUserEntry($uid, $exclude_drafts = "") {
	global $_MY_CONFIG;

	$db 		= &cmsInstance('CMSDb');
	$sections   = $_MY_CONFIG->get('managedSections');

	$extra 		= "";
	
	if ($exclude_drafts == "1") {
		$extra = "and state='1' and publish_up < '" . date( 'Y-m-d H:i:s' ) . "'";
	}
	$db->query("SELECT COUNT(*) FROM #__content WHERE created_by='$uid' and sectionid IN ($sections) $extra");
	$result = $db->get_value();
	
	if ($result == "")
		return 0;
	
	return $result;
}

/**
 *	Retrieve number of comments for a given user
 */ 
function myCountUserComment($uid) {
	global $_MY_CONFIG;
	
	$db			= &cmsInstance('CMSDb');
	$sections   = $_MY_CONFIG->get('managedSections');
	
	$strSQL = "SELECT COUNT(*) FROM #__jomcomment AS a "
			. "INNER JOIN #__content AS b "
			. "WHERE b.id=a.contentid "
			. "AND a.option='com_myblog' "
			. "AND b.created_by='{$uid}'";
	$db->query($strSQL);
	
	$result = $db->get_value();

	if ($result == "")
		return 0;
	
	return $result;
}

/**
 *	Retrieve number of hits for a given user
 */ 
function myCountUserHits($uid) {
	global $_MY_CONFIG;

	$db 		= &cmsInstance('CMSDb');
	$sections   = $_MY_CONFIG->get('managedSections');
	
	$db->query("SELECT SUM(hits) FROM #__content WHERE created_by='$uid' and sectionid in ($sections) and state='1' and publish_up < NOW()");
	$result = $db->get_value();
	
	if ($result == "")
		return 0;
	
	return $result;
}

/**
 *	Retrieves a list of links to the unpublsihed entries for a user
 */ 
function myGetUserDraft($uid) {
	global $MYBLOG_LANG, $Itemid, $sectionid, $sections;
	
	$cms =& cmsInstance('CMSCore');
	$cms->load('helper', 'url');
	$db = & cmsInstance('CMSDb');
	$db->query("SELECT title,created,id FROM #__content WHERE created_by='$uid' AND state=0 and sectionid in ($sections) ORDER BY created");
	$result = $db->get_object_list();
	$drafts = "<em>";

	if ($result) {
		foreach ($result as $row) {
			$draftlink = cmsSefAmpReplace("index2.php?option=com_myblog&no_html=1&task=edit&id=$row->id&Itemid=$Itemid&admin=1");
			
			if ($drafts != "<em>")
				$drafts .= ", ";
			$drafts .= "<a class=\"draftLink\" href=\"$draftlink\">".$row->title."</a>";
		}
	} else {
		$drafts = $MYBLOG_LANG['_MB_NO_DRAFTS'];
	}
	
	$drafts .= "</em>";
	
	return $drafts;
}

/**
 *	Retrieves number of Jom comments for an article.
 */ 
function myCountJomcomment($article_Id, $com = "com_myblog") {
	$db = &cmsInstance('CMSDb');
	
	$db->query("SELECT COUNT(*) FROM #__jomcomment WHERE contentid='$article_Id' AND (`option`='$com' OR `option`='com_content') AND published='1'");
	$result = $db->get_value();
	
	return $result;
}

/**
 * Retrieves number of articles in a selected category.
 **/
function myGetCategoryCount($categoryId){
	$db =& cmsInstance('CMSDb');
	
	$strSQL = "SELECT COUNT(*) FROM #__content "
			. "WHERE `catid`='{$categoryId}' AND `state`='1' AND created <= NOW()";

	$db->query($strSQL);
	return $db->get_value();
}

/**
 *	Get a list of categories for a content.
 */ 
function myCategoriesURLGet($contentid, $linkCats = true, $task="") {
	global $Itemid, $MYBLOG_LANG;
	
	$cms	=& cmsInstance('CMSCore');
	$cms->load('helper','url');
	
	$result = myGetTags($contentid);
	$catCount = count($result);
	$link = "";
	$task = empty($task) ? '&task=tag' : "&task=$task" ;
	
	if ($result) {
		$count = 0;
		
		foreach ($result as $row) {
			if ($link != "")
				$link .= ",&nbsp;";
			
			if ($linkCats) {
				$url	= cmsSefAmpReplace('index.php?option=com_myblog' . $task . '&category=' . urlencode($row->slug) . '&Itemid=' . $Itemid);
// 				if(cmsVersion() == _CMS_JOOMLA10 || cmsVersion() == _CMS_MAMBO){
// 					$url = ampReplace(sefRelToAbs("index.php?option=com_myblog$task&category=" . urlencode($row->name) . "&Itemid=$Itemid"));
// 				}elseif(cmsVersion() == _CMS_JOOMLA15){
// 					$url = JFilterOutput::ampReplace(JRoute::_("index.php?option=com_myblog$task&category=" . urlencode($row->name) . "&Itemid=$Itemid"));
// 				}
				
				$link .= "<a href=\"$url\">$row->name</a>";
			} else
				$link .= $row->name;
			$count++;
		}
	} else {
		$link .= "<i>".$MYBLOG_LANG['_MB_UNTAGGED']."</i>&nbsp;";
	}
	
	return $link;
}

/**
 *	Retrieves the comment link for a content
 */ 
function myCommentsURLGet($contentid, $addCommentCount = true, $task="") {
	global $Itemid, $MYBLOG_LANG;
	
	$link = "";
	
	if ($task!="")
		$task = "&task=$task";
	
	if ($addCommentCount) {
		//$commentlink = myGetPermalinkUrl($contentid, $task);
		$numcomment = intval(myCountJomcomment($contentid));
		//$link .= "<a href=\"$commentlink#comments\">".$MYBLOG_LANG['_MB_COMMENT']." ($numcomment)</a>";
	}
	
	return $MYBLOG_LANG['_MB_COMMENT']." ($numcomment) "; //$link;
}


/**
 *	Gets the author username/fullname   
 */ 
function myGetAuthorName($uid, $useFullName="") {
	$db = &cmsInstance('CMSDb');
	
	$select = "username";
	
	if ($useFullName == "1")
		$select = "name";
	
	$db->query("SELECT $select FROM #__users WHERE id='$uid'");
	$result = $db->get_value();
	
	return $result;
}

function myGetAuthorEmail($uid){
	$db = &cmsInstance('CMSDb');
	
	$db->query("SELECT `email` FROM #__users WHERE id='$uid'");
	$result = $db->get_value();
	
	return $result;
}

/** %UserName% 
 *	Gets the author's id
 */ 
function myGetAuthorId($name, $useFullName="") {
	$db = &cmsInstance('CMSDb');
	
	$name = urldecode($name);

	// If name is empty, just return 0 since some site somehow has username's ''
	if(!$name)
		return '0';

	// If display full name instead of username, need to check both fullname nad username,
	// just in case the blogger url param contains the username.
	if ($useFullName=="1")
	{
		$db->query("SELECT id FROM #__users WHERE name RLIKE '[[:<:]]$name"."[[:>:]]' or username RLIKE '[[:<:]]$name"."[[:>:]]' ");
		$result = $db->get_value();
		return ($result ? $result : "0");
	}
	
	// get exact username match
	$name = $db->_escape($name);

	$db->query("SELECT id FROM #__users WHERE username='$name'");

	$result = $db->get_value();

	// if not found, try get similar match
	if (!$result)
	{
		$db->query("SELECT id FROM #__users WHERE username RLIKE '[[:<:]]$name"."[[:>:]]'");
		$result = $db->get_value();
	}
	
	// if still not found, check name and see if contains username.
	if (!$result)
	{
		$db->query("SELECT id FROM #__users WHERE name RLIKE '[[:<:]]$name"."[[:>:]]'");
		$result = $db->get_value();
	}
	
	return ($result ? $result : "0");
}

/**
 *	Gets the author username/fullname 
 */ 
function myUserGetName($uid, $useFullName="") {
	$db = &cmsInstance('CMSDb');
	
	$uid = intval($uid);
	$select = "username";
	
	if ($useFullName == "1")
		$select = "name";
	
	$db->query("SELECT $select FROM #__users WHERE id=$uid");
	$username = $db->get_value();

	return $username;
}

function myEncodeString($contents) {
	$ascii = '';
	$strlen_var = strlen($contents);
	
	for ($c = 0; $c < $strlen_var; ++ $c) {
		$ord_var_c = ord($contents {
			$c });
		
		switch ($ord_var_c) {
			case 0x08 :
				$ascii .= '\b';
				break;
				
			case 0x09 :
				$ascii .= '\t';
				break;
				
			case 0x0A :
				$ascii .= '\n';
				break;
				
			case 0x0C :
				$ascii .= '\f';
				break;
				
			case 0x0D :
				$ascii .= '\r';
				break;
				
			default :
				$ascii .= $contents {
					$c };
		}
	}
	
	return $ascii;
}

// Return the complete & valid permalink-URL for the given content 
function myGetPermalinkUrl($uid, $task="", $blogger = '') {
	$db = &cmsInstance('CMSDb');
	$cms = &cmsInstance('CMSCore');
	$cms->load('helper', 'url');
	
	$Itemid = myGetItemId();
	
	$db->query("SELECT permalink from #__myblog_permalinks WHERE contentid='$uid'");
	$link = $db->get_value();
	
	if (!$link OR empty($link)) {
		// The permalink might be empty. We need to delete it
		$db->query("DELETE FROM #__myblog_permalinks WHERE contentid='$uid'");
		
		$db->query("SELECT title from #__content WHERE id='$uid'");
		$title = $db->get_value();
		
		// remove unwatned chars from permalink
		$link = myTitleToLink(trim($title . ".html"));;
		$db->query("SELECT count(*) from #__myblog_permalinks WHERE permalink='$link' and contentid!='$uid'");
		$linkExists = $db->get_value();
		
		if ($linkExists) {
			$link = myTitleToLink(trim($title));
			
			$plink = "$link-$uid.html";
			$db->query("SELECT contentid from #__myblog_permalinks WHERE permalink='$plink' and contentid!='$uid'");
			$count = 0;
			
			while ($db->get_value()) {
				$count++;
				$plink = "$link-$uid-$count.html";
				$db->query("SELECT contentid from #__myblog_permalinks WHERE permalink='$plink' and contentid!='$uid'");
			}
			
			//$plink = urlencode($plink);
			$db->query("INSERT INTO #__myblog_permalinks SET permalink='$plink',contentid='$uid'");
		} else
		{
			$db->query("INSERT INTO #__myblog_permalinks SET permalink='$link',contentid='$uid' ");
		}		
	}
	
	$link = urlencode($link);
	
	if ($task!=""){
		$task = "&task=$task";
	}

	if ($blogger!=""){
		$blogger = "&blogger=$blogger";
	}
		
	$url = "index.php?option=com_myblog&show={$link}{$task}{$blogger}&Itemid={$Itemid}";
	$sefUrl = cmsSefAmpReplace($url);

	// If $sefUrl does not start with http, we will have to make the url absolute,
	// this ensure that this link works with RSS Feed
// 	if(substr($sefUrl,0, 4) != 'http'){
// 		$sefUrl = $cms->get_path('live') .'/'. $sefUrl;
// 	}
	return $sefUrl;
}



// Return the complete & valid permalink-URL for the given content
function myGetPermalink($uid, $task="") {
	$db = &cmsInstance('CMSDb');
	$cms = &cmsInstance('CMSCore');

	$Itemid = myGetItemId();

	$db->query("SELECT permalink from #__myblog_permalinks WHERE contentid='$uid'");
	$link = $db->get_value();

	if (!$link OR empty($link)) {
		// The permalink might be empty. We need to delete it
		$db->query("DELETE FROM #__myblog_permalinks WHERE contentid='$uid'");

		$db->query("SELECT title from #__content WHERE id='$uid'");
		$title = $db->get_value();

		// remove unwatned chars from permalink
		$link = myTitleToLink(trim($title . ".html"));;
		$db->query("SELECT count(*) from #__myblog_permalinks WHERE permalink='$link' and contentid!='$uid'");
		$linkExists = $db->get_value();

		if ($linkExists) {
			$link = myTitleToLink(trim($title));

			$plink = "$link-$uid.html";
			$db->query("SELECT contentid from #__myblog_permalinks WHERE permalink='$plink' and contentid!='$uid'");
			$count = 0;

			while ($db->get_value()) {
				$count++;
				$plink = "$link-$uid-$count.html";
				$db->query("SELECT contentid from #__myblog_permalinks WHERE permalink='$plink' and contentid!='$uid'");
			}

			//$plink = urlencode($plink);
			$db->query("INSERT INTO #__myblog_permalinks SET permalink='$plink',contentid='$uid'");
   			return $plink;
		} else{
			$db->query("INSERT INTO #__myblog_permalinks SET permalink='$link',contentid='$uid' ");
            return $link;
		}
	}
	return $link;
}

/**
 * Get bookmarking links to be displayed when viewing blogs.
 **/
function myGetBookmarks(& $row){

	$link       = $row->permalink;
	$title      = $row->title;
	$text       = '';
	
	if(cmsVersion() == _CMS_JOOMLA10 || cmsVersion() == _CMS_MAMBO){
		$bookmarks  = array(
		                    'digg'  	=> ampReplace(_getBookmarkLink('digg', $link, $title)),
		                    'delicious' => ampReplace(_getBookmarkLink('delicious', $link, $title)),
		                    'spurl'     => ampReplace(_getBookmarkLink('spurl', $link, $title)),
		                    'reddit'    => ampReplace(_getBookmarkLink('reddit', $link, $title)),
		                    'furl'      => ampReplace(_getBookmarkLink('furl', $link, $title))
							);	
	}elseif(cmsVersion() == _CMS_JOOMLA15){
			$bookmarks  = array(
	                    'digg'  	=>  JFilterOutput::ampReplace(_getBookmarkLink('digg', $link, $title)),
	                    'delicious' =>  JFilterOutput::ampReplace(_getBookmarkLink('delicious', $link, $title)),
	                    'spurl'     =>  JFilterOutput::ampReplace(_getBookmarkLink('spurl', $link, $title)),
	                    'reddit'    =>  JFilterOutput::ampReplace(_getBookmarkLink('reddit', $link, $title)),
	                    'furl'      =>  JFilterOutput::ampReplace(_getBookmarkLink('furl', $link, $title))
						);
	}

	$row->text   .= "<div class=\"clear\">"
			. "<div>"
			. "<p>"
			. "<a href=\"" . $bookmarks['delicious'] . "\">del.icio.us</a>&#183;"
			. "<a href=\"" . $bookmarks['digg'] . "\">digg this</a>&#183;"
			. "<a href=\"" . $bookmarks['spurl'] . "\">spurl</a>&#183;"
	        . "<a href=\"" . $bookmarks['reddit'] . "\">reddit</a>&#183;"
	        . "<a href=\"" . $bookmarks['furl'] . "\">furl this</a>"
			. "</p>"
			. "</div>"
			. "</div>";

	return true;
}

function _getBookmarkLink($type, $link, $title)
{
	switch ($type)
	{
		case 'digg':
			return 'http://www.digg.com/submit?phase=2&url='.$link."&title=".$title;
		case 'delicious':
			return "javascript:void(window.open('http://del.icio.us/post?url='+encodeURIComponent('".$link."')+'&title='+encodeURIComponent('".str_replace("%26amp%3B", "%26", urlencode(addslashes($title)))."')+'&noui&jump=close&v=4','_blank','width=700,height=280,status=no,resizable=yes,scrollbars=auto'))";
		case 'spurl':
			return "javascript:d=document;void(window.open('http://www.spurl.net/spurl.php?v=3
			&title='+encodeURIComponent('".str_replace("%26amp%3Bamp%3B", "%26amp%3B", urlencode(htmlentities(addslashes($title))))."')+
			'&url='+escape('".$link."')+
			'&blocked='+encodeURIComponent(d.selection?d.selection.createRange().text:d.getSelection()),
			'_blank','width=450,height=550,status=no,resizable=yes,scrollbars=auto'))";
		case 'reddit':
			return  'http://reddit.com/submit?url='.$link.'&title='.$title;
		case'furl':
			return "javascript:(function(){
					var%20d=document;
					var%20c=window.getSelection?window.getSelection():'';
						var%20f=window.open('http://www.furl.net/storeIt.jsp?t='
						+encodeURIComponent('".str_replace(array("%26amp%3Bamp%3B", "%25") , array("%26amp%3B", "%2525"), htmlentities(addslashes($title)))
						."')+'&u=
						'+escape(escape('".$link."'))+'&c=
						'+encodeURIComponent(c),'myfurlwindow','width=475,height=540,left=75,top=20,resizable=yes');
						f.focus();return;
					})();";
	}
}

/**
 *	Displays images on image browser, resized while maintaining aspect ratio
 */ 
function displayAndResizeImages($img_array) {
	$uploadedImages = "";
	
	if ($img_array) {
		foreach ($img_array as $img) {
			if (!empty ($img)) {
				$img['filename'] = trim($img['filename']);
				$imageMaxSize = 80;
				$imageWidth = $img['width'];
				$imageHeight = $img['height'];
				
				if ($imageHeight > $imageMaxSize or $imageWidth > $imageMaxSize) {
					if ($imageHeight > $imageWidth) {
						$imageWidth = ($imageMaxSize / $imageHeight) * $imageWidth;
						$imageHeight = $imageMaxSize;
					} else {
						$imageHeight = ($imageMaxSize / $imageWidth) * $imageHeight;
						$imageWidth = $imageMaxSize;
					}
				}
				
				$basename = basename($img['filename']);
				
				
				$imgtag = "<img src='{$img['filename']}' hspace='4' vspace='4' align='left' alt='$basename' border='0' />";
				$imgtag = addslashes($imgtag);
				$onDblCick	 ="tinyMCE.execCommand('mceFocus',false, 'mce_editor_0');";
				$onDblCick	.="tinyMCE.execCommand('mceInsertContent',false, '$imgtag');";
				
				
				$uploadedImages .= "<div title=\"$basename\" class=\"imgContainerOut\" ondblclick=\"$onDblCick\">
					<span class=\"imgContainer\" onmouseover=\"this.className='imgContainerHover';\" onmouseout=\"this.className='imgContainer';\">
						<center>
							<img src=\"" . $img['filename'] . "\" width=\"$imageWidth\" height=\"$imageHeight\" />
						</center>
					</span>
					</div>";
			}
		}
	} else {
		$uploadedImages = " ";
	}
	
	return $uploadedImages;
}

/**
 *	Retrieves AJAX page navigation links
 */ 
function writeAjaxPageNav($comp, $func, $limit, $limitstart, $total, $loadingDivName, $loadingImgURL = "") {
	$txt = '';
	$displayed_pages = 10;
	$total_pages = $limit ? ceil($total / $limit) : 0;
	$this_page = $limit ? ceil(($limitstart +1) / $limit) : 1;
	$start_loop = (floor(($this_page -1) / $displayed_pages)) * $displayed_pages +1;
	
	if ($start_loop + $displayed_pages -1 < $total_pages) {
		$stop_loop = $start_loop + $displayed_pages -1;
	} else {
		$stop_loop = $total_pages;
	}
	
	$link = "javascript:loading('$loadingDivName','$loadingImgURL');jax.icall('$comp','" . $func . "','" . $limit . "',";
	
			
	$pnSpace = '';
	
	if (_PN_LT || _PN_RT)
		$pnSpace = "&nbsp;";
	
	if ($this_page > 1) {
		$page = ($this_page -2) * $limit;
		$txt .= '<a href="' . "$link '0');" . '" class="pagenav" ><img border=0 src="components/com_myblog/images/Backward_16x16.png" alt=""/>' . $pnSpace . _PN_START . '</a> ';
		$txt .= '<a href="' . "$link '$page');" . '" class="pagenav" ><img border=0 src="components/com_myblog/images/Play2_16x16.png" alt=""/>' . $pnSpace . _PN_PREVIOUS . '</a> ';
	} else {
	}
	
	for ($i = $start_loop; $i <= $stop_loop; $i++) {
		$page = ($i -1) * $limit;
		
		if ($i == $this_page) {
			$txt .= '<span class="pagenav">' . $i . '</span> ';
		} else {
			$txt .= '<a href="' . "$link '$page');" . '" class="pagenav"><strong>' . $i . '</strong></a> ';
		}
	}

	if ($this_page < $total_pages) {
		$page = $this_page * $limit;
		$end_page = ($total_pages -1) * $limit;
		$txt .= '<a href="' . "$link '$page');" . '" class="pagenav">' . _PN_NEXT . $pnSpace . '<img border=0 src="components/com_myblog/images/Play_16x16.png" alt=""/></a> ';
		$txt .= '<a href="' . "$link '$end_page');" . '" class="pagenav">' . _PN_END . $pnSpace . '<img border=0 src="components/com_myblog/images/Forward_16x16.png" alt=""/></a>';
	} else {
	}
	
	return $txt;
}

/**
 *	Gets the tag clouds given a query and number of cloud variations
 */ 
function myGetTagClouds($query, $clouds = 8) {
	$db = &cmsInstance('CMSDb');
	
	$db->query($query);
	$rows = $db->get_object_list();
	
	/* Test Data
	$rows = array();
	for($i = 0; $i < 10; $i++){
		$a = new StdClass();
		$a->name = $i.'-myblog';
		$a->frequency = 100*$i;
		$rows[] = $a;
	}
	*/
	
	if (!$rows)
		return "";
	
	$vals = array();
	foreach ($rows as $row) {
		$vals["{$row->frequency}"] = $row->frequency;
	}
	$maxFreq = max($vals);
	$minFreq = min($vals);
	
	$freqSize = $maxFreq - $minFreq;
	$freqSpacing	= $freqSize / $clouds;
	
	if($freqSpacing < 1){
		$freqSpacing = 1;
	}
	
	foreach ($rows as $row) {
		$tagClass = round($row->frequency / $freqSpacing);
		$result[] = array (
			'name' => $row->name,
			'cloud' => $tagClass,
			'slug'	=> $row->slug
		);
	}
	
	usort($result, 'mySortTags');
	return $result;	

}

/**
 *	sort tags alphabetically 
 */ 
function mySortTags($a, $b) {
	return (strtoupper($a['name']) < strtoupper($b['name'])) ? -1 : 1;
}

/**
 *	Display page navigation, non ajaxed 
 */ 
function myWritePagesLinks($link, $total, $limitstart, $limit) {
	$total = (int) $total;
	$limitstart = (int) max($limitstart, 0);
	$limit = (int) max($limit, 0);
	$txt = '';
	$displayed_pages = 10;
	$total_pages = $limit ? ceil($total / $limit) : 0;
	$this_page = $limit ? ceil(($limitstart +1) / $limit) : 1;
	$start_loop = (floor(($this_page -1) / $displayed_pages)) * $displayed_pages +1;
	
	if ($start_loop + $displayed_pages -1 < $total_pages) {
		$stop_loop = $start_loop + $displayed_pages -1;
	} else {
		$stop_loop = $total_pages;
	}
	
	$link .= '&amp;limit=' . $limit;
	
	if (!defined('_PN_LT') || !defined('_PN_RT')) {
		DEFINE('_PN_LT', '&lt;');
		DEFINE('_PN_RT', '&gt;');
	}
	
	if (!defined('_PN_START'))
		define('_PN_START', 'Start');
	
	if (!defined('_PN_PREVIOUS'))
		define('_PN_PREVIOUS', 'Previous');
	
	if (!defined('_PN_END'))
		define('_PN_END', 'End');
	
	if (!defined('_PN_NEXT'))
		define('_PN_NEXT', 'Next');
	
	$pnSpace = '';
	
	if (_PN_LT || _PN_RT)
		$pnSpace = "&nbsp;";
	
	if ($this_page > 1) {
		$page = ($this_page -2) * $limit;
		$txt .= '<a href="' . sefRelToAbs("$link&amp;limitstart=0") . '" class="pagenav" title="' . _PN_START . '"><img border=0 src="components/com_myblog/images/Backward_16x16.png" alt=""/>' . $pnSpace . _PN_START . '</a> ';
		$txt .= '<a href="' . sefRelToAbs("$link&amp;limitstart=$page") . '" class="pagenav" title="' . _PN_PREVIOUS . '"><img border=0 src="components/com_myblog/images/Play2_16x16.png" alt=""/>' . $pnSpace . _PN_PREVIOUS . '</a> ';
	} else {
		$txt .= '';
		$txt .= '';
	}
	
	for ($i = $start_loop; $i <= $stop_loop; $i++) {
		$page = ($i -1) * $limit;

		if ($i == $this_page) {
			$txt .= '<span class="pagenav">' . $i . '</span> ';
		} else {
			$txt .= '<a href="' . sefRelToAbs($link . '&amp;limitstart=' . $page) . '" class="pagenav"><strong>' . $i . '</strong></a> ';
		}
	}
	
	if ($this_page < $total_pages) {
		$page = $this_page * $limit;
		$end_page = ($total_pages -1) * $limit;
		$txt .= '<a href="' . sefRelToAbs($link . '&amp;limitstart=' . $page) . ' " class="pagenav" title="' . _PN_NEXT . '">' . _PN_NEXT . $pnSpace . '<img border=0 src="components/com_myblog/images/Play_16x16.png" alt=""/></a> ';
		$txt .= '<a href="' . sefRelToAbs($link . '&amp;limitstart=' . $end_page) . ' " class="pagenav" title="' . _PN_END . '">' . _PN_END . $pnSpace . '<img border=0 src="components/com_myblog/images/Forward_16x16.png" alt=""/></a>';
	} else {
		$txt .= '';
		$txt .= '';
	}
	
	return $txt;
}

/**
 *	Retrieves avatar image url for a user
 */ 
function getAvatarImg($user_id) {
	global $_MY_CONFIG, $mainframe;
	
	$cms =& cmsInstance('CMSCore');
	$db = &cmsInstance('CMSDb');
	$db->query("SELECT email from #__users WHERE id='$user_id'");
	$email = $db->get_value();
	$grav_url = $cms->get_path('live') . "/components/com_myblog/images/guest.gif"; #get_url() doesnt work
	$avatarWidth = ($_MY_CONFIG->get('avatarWidth')) ? intval($_MY_CONFIG->get('avatarWidth')) : "";
	$avatarHeight = ($_MY_CONFIG->get('avatarHeight')) ? intval($_MY_CONFIG->get('avatarHeight')) : "";
	
	switch ($_MY_CONFIG->get('avatar')) {
		case "gravatar" :
			$avatarWidth = ($avatarWidth) ? $avatarWidth : "40";
			$grav_url = "http://www.gravatar.com/avatar.php?gravatar_id=" . md5($email) . "&default=" . urlencode($cms->get_path('live') . "/components/com_myblog/images/guest.gif") . "&size=$avatarWidth";
			break;
			
		case "fireboard":
			$fb_config_file = $cms->get_path('root') . "/administrator/components/com_fireboard/fireboard_config.php";
			if (file_exists($fb_config_file))
				include($fb_config_file);
			else
			    break;
				

			if ($fbConfig['avatar_src']=='fb')
			{
				// get the fireboard avatar path if fireboard config has avatar source set to fireboard
				$db->query("SELECT avatar from #__fb_users WHERE userid='$user_id'");
				$avatar_relative_path = $db->get_value();
				if ($avatar_relative_path)
				{
				    $fb_avatar  = '';
				    
				    #Check version
				    if($fbConfig['version'] == '1.0.2' || $fbConfig['version'] == '1.0.3'){
						$fb_avatar = "/components/com_fireboard/avatars/" . $avatar_relative_path;
					}
					else{
						// need to get the abs path since avatar path stored is relative to avatars directory for fireboard
						$fb_avatar = '/images/fbfiles/avatars/' . $avatar_relative_path;
					}
					$avatar_abs_path = $cms->get_path('root') . $fb_avatar;

					if (file_exists($avatar_abs_path))
							$grav_url   = $cms->get_path('live') . $fb_avatar;
				}
				break;
			} else if ($fbConfig['avatar_src']!="cb")
			{
				// this is for handling case where fireboard avatar source is clexus PM
				break;
			} 
				// if is CB, go to CB avatar
		case "cb" :
			$db->query("SELECT avatar FROM #__comprofiler WHERE user_id=" . $user_id . " AND avatarapproved=1");
			$result = $db->get_value();
			
			if ($result) {
				if (file_exists($cms->get_path('root') . "/components/com_comprofiler/images/" . $result))
					$grav_url = $cms->get_path('live') . "/components/com_comprofiler/images/" . $result;
				else
					if (file_exists($cms->get_path('root') . "/images/comprofiler/" . $result))
						$grav_url = $cms->get_path('live') . "/images/comprofiler/" . $result;
			}
			break;
		
		case "smf" :
			$smfPath = $_MY_CONFIG->get('smfPath');
			$smfPath = trim($smfPath);
			$smfPath = rtrim($smfPath, '/');
			if (!$smfPath or $smfPath == "" or !file_exists("$smfPath/Settings.php"))
				$smfPath = $cms->get_path() . "/forum";
			
			// check for SMF bridge, then use SMF bridge settings if SMF bridge installed
			if (!$smfPath or $smfPath == "" or !file_exists("$smfPath/Settings.php")) {
				$db->query("select id from #__components WHERE `option`='com_smf'");
				if ($db->get_value()) {
					$db->query("select value1 from #__smf_config WHERE variable='smf_path'");
					$smfPath = $db->get_value();
					$smfPath = str_replace("\\", "/", $smfPath);
					$smfPath = rtrim($smfPath, "/");
				}
			}
			
			// need to get the settings as SMF db / tables can be in different location
			if (file_exists("$smfPath/Settings.php")) {
				include("$smfPath/Settings.php");
				mysql_select_db($mainframe->getCfg('db'));
				$useremail = $email;
				mysql_select_db($db_name);
				
				// query for user avatar, id using email address
				$q = sprintf("SELECT avatar,ID_MEMBER FROM $db_prefix" . "members WHERE emailAddress='$useremail'");
				$result = mysql_query($q);
				
				if ($result)
				{
					$result_row = mysql_fetch_array($result);
					mysql_select_db($mainframe->getCfg('db'));
					
					if ($result_row) {
						$id_member = $result_row[1];
						
						if (trim($result_row[0]) != "") {
							if (substr($result_row[0], 0, 7) != "http://")
								$grav_url = $boardurl . "/avatars/$result_row[0]";
							else
								$grav_url = $result_row[0];
						} else {
							mysql_select_db($db_name);
							$q = sprintf("SELECT ID_ATTACH FROM $db_prefix" . "attachments WHERE ID_MEMBER='$id_member' and ID_MSG=0 and attachmentType=0");
							$result = mysql_query($q);
							
							if ($result)
							{
								$result_avatar = mysql_fetch_array($result);
								mysql_select_db($mainframe->getCfg('db'));
								if ($result_avatar[0])
									$grav_url = "$boardurl/index.php?action=dlattach;attach=" . $result_avatar[0] . ";type=avatar";
							}
						}
					}
				}
			}
			break;
		
		default :
			break;
	}
	
	return $grav_url;
}

/**
 *	Retrieves avatar link for a user
 */ 
function getAvatarLink($user_id) {
	global $_MY_CONFIG, $Itemid, $mainframe;
	
	$cms =& cmsInstance('CMSCore');
	$cms->load('helper', 'url');
	$db  = &cmsInstance('CMSDb');
	$db->query("SELECT email from #__users WHERE id='$user_id'");
	$email = $db->get_value();
	$link = "";
	
	switch ($_MY_CONFIG->get('avatar')) {
		case "fireboard":
			// Find the correct itemid
			$link = cmsSefAmpReplace("index.php?option=com_fireboard&Itemid=$Itemid&func=fbprofile&task=showprf&userid=$user_id");
			break;
			
		case "cb" :
			if ($user_id) {
				$link = cmsSefAmpReplace("index.php?option=com_comprofiler&task=userProfile&user=$user_id");
			}
			break;
		
		case "smf" :
			$smfPath = $_MY_CONFIG->get('smfPath');
			if (substr($smfPath, strlen($smfPath) - 1, 1) == "/")
				$smfPath = substr($smfPath, 0, strlen($smfPath) - 1);
			if (!$smfPath or $smfPath == "" or !file_exists("$smfPath/Settings.php")) {
				$db->query("select id from #__components WHERE `option`='com_smf'");
				if ($db->get_value()) {
					$db->query("select value1 from #__smf_config WHERE variable='smf_path'");
					$smfPath = $db->get_value();
					$smfPath = str_replace("\\", "/", $smfPath);
					$smfPath = rtrim($smfPath, "/");
				}
			}
			
			if (!$smfPath or $smfPath == "" or !file_exists("$smfPath/Settings.php"))
				$smfPath = $cms->get_path() . "/forum";
			
			if (file_exists("$smfPath/Settings.php")) {
				include("$smfPath/Settings.php");
				mysql_select_db($db_name);
				$useremail = $email;
				$q = sprintf("SELECT ID_MEMBER FROM $db_prefix" . "members WHERE emailAddress='%s'", mysql_real_escape_string($useremail));
				$result = mysql_query($q);
				$result_row = @mysql_fetch_array($result);
				mysql_select_db($mainframe->getCfg('db'));
				
				if ($result_row) {
					$db->query("select id from #__components WHERE `option`='com_smf'");
					$smfWrap = "";
					
					if ($db->get_value()) {
						$db->query("SELECT value1 from #__smf_config WHERE variable='wrapped'");
						$smfWrap = $db->get_value();
						
						if ($smfWrap == "true") {
							$smfWrap = "1";
						} else
							$smfWrap = "";
					}
					
					if ($smfWrap and $smfWrap != "") {
						$link = cmsSefAmpReplace("index.php?option=com_smf&action=profile&u=" . $result_row[0] . "&Itemid=$Itemid");
					} else
						$link = cmsSefAmpReplace($boardurl . "/index.php?action=profile&u=" . $result_row[0]);
				}
			}
			break;
		
		default :
			break;
	}
	
	return $link;
}

/**
 *	Gets the blog description for a user's blog
 */ 
function myGetAuthorDescription($userid) {
	$db = &cmsInstance('CMSDb');
	$db->query("SELECT description FROM #__myblog_user WHERE user_id='$userid'");
	$desc = $db->get_value();
	
	if (!$desc) {
		$desc = "<p>No desc available</p>";
	}
	
	return $desc; 
}

function myGetAuthorTitle($userid) {
	$db = &cmsInstance('CMSDb');
	$db->query("SELECT `title` FROM #__myblog_user WHERE user_id='$userid'");
	$title = $db->get_value();

	return $title; 
}


/**
 *	close all open HTML tags. works as HTML cleanup function
 */ 
function myCloseTags($html){
	#put all opened tags into an array
	preg_match_all("#<([a-z]+)( .*)?(?!/)>#iU",$html,$result);
	$openedtags=$result[1];
	
	#put all closed tags into an array
	preg_match_all("#</([a-z]+)>#iU",$html,$result);
	$closedtags=$result[1];
	$len_opened = count($openedtags);
	// all tags are closed
	if(count($closedtags) == $len_opened){
		return $html;
	}
	$openedtags = array_reverse($openedtags);
	// close tags
	for($i=0;$i < $len_opened;$i++) {
		if (!in_array($openedtags[$i],$closedtags) && $openedtags[$i] != 'img'){
			$html .= '</'.$openedtags[$i].'>';
		} else {
			unset($closedtags[array_search($openedtags[$i],$closedtags)]);
		}
	}
	return $html;
}

// Create a suitable link from a given title
function myTitleToLink($link)
{
    global $_MY_CONFIG;

    // Replace non-ASCII characters.
    $link = strtr($link,
	 "\xe1\xc1\xe0\xc0\xe2\xc2\xe4\xc4\xe3\xc3\xe5\xc5".
	 "\xaa\xe7\xc7\xe9\xc9\xe8\xc8\xea\xca\xeb\xcb\xed".
	 "\xcd\xec\xcc\xee\xce\xef\xcf\xf1\xd1\xf3\xd3\xf2".
	 "\xd2\xf4\xd4\xf6\xd6\xf5\xd5\x8\xd8\xba\xf0\xfa\xda".
	 "\xf9\xd9\xfb\xdb\xfc\xdc\xfd\xdd\xff\xe6\xc6\xdf\xf8",
	 "aAaAaAaAaAaAacCeEeEeEeEiIiIiIiInNo".
	 "OoOoOoOoOoOoouUuUuUuUyYyaAso");
	$link = strtr($link, $_MY_CONFIG->replacements_array);
	
    // remove quotes, spaces, and other illegal characters
    $link = preg_replace(array('/\'/', '/[^a-zA-Z0-9\-.+]+/', '/(^_|_$)/'), array('', '-', ''), $link);
    
    // Replace multiple '-' with a single '-'
	$link = ereg_replace('-(-)*', '-', $link);
	
	// Remove first occurence of '-'
	$link = ereg_replace('^-','', $link);
	
    return $link;
}

// Similar to trim, but with reference
function myTrim(&$string)
{
	$string = trim($string);
}

function i8n_date($date)
{
	global $MYBLOG_LANG;
	if($MYBLOG_LANG){
		foreach($MYBLOG_LANG as $key => $val)
		{
			$date = str_replace($key, $val, $date);
		}
	}
	
	return $date;
}

/**
 * Return the pdf link for specific content
 **/
function myGetPDFLink($contentId, $ItemId){
	$cms    =& cmsInstance('CMSCore');
	$cms->load('helper', 'url');

	if(cmsVersion() == _CMS_JOOMLA10 || cmsVersion() == _CMS_MAMBO){
	    $link   = cmsSefAmpReplace('index2.php?option=com_content&do_pdf=1&id=' . $contentId);
	    //$link   = cmsSefAmpReplace('index2.php?option=com_content&format=pdf&id=' . $contentId . '&Itemid=' . $ItemId);
	}else{
		$link	= cmsSefAmpReplace('index.php?view=article&id=' . $contentId . '&format=pdf');
	}
	$str    = '<a title="PDF" onClick="window.open(\'' . $link . '\',\'win2\',\'status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no\'); return false;" ';
	$str    .= 'target="_blank" href="' . $link . '">';
	$str    .= '<img border="0" name="PDF" alt="PDF" src="' . $cms->get_path('live') . '/images/M_images/pdf_button.png"></a>';

	return $str;
}

/**
 * Return the Print link for specific content
 **/
function myGetPrintLink($contentId, $ItemId){
	$cms    =& cmsInstance('CMSCore');
	$cms->load('helper', 'url');
	
	if(cmsVersion() == _CMS_JOOMLA10 || cmsVersion() == _CMS_MAMBO){
		$link   = cmsSefAmpReplace('index2.php?option=com_content&task=view&id=' . $contentId . '&pop=1&page=0&Itemid=' . $ItemId);
	} else {
		$link   = cmsSefAmpReplace('index.php?index.php?view=article&id=' . $contentId . '&tmpl=component&print=1&task=printblog');
	}
	

	$str    = '<a title="Print" onClick="window.open(\'' . $link . '\',\'win2\',\'status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no\'); return false;" ';
	$str    .= 'target="_blank" href="' . $link . '">';
	$str    .= '<img border="0" name="Print" alt="Print" src="' . $cms->get_path('live') . '/images/M_images/printButton.png"></a>';

	return $str;
}

/**
 * Return the Back link for specific content
 **/
function myGetBackLink(){
	global $MYBLOG_LANG;
	
	if(cmsVersion() == _CMS_JOOMLA10 || cmsVersion() == _CMS_MAMBO){
		$str    = '<div class="back_button"><a href="javascript:void(0);" onClick="javascript:history.go(-1);">' . _BACK . '</a></div>';
	}elseif(cmsVersion() == _CMS_JOOMLA15){
		$str    = '<div class="back_button"><a href="javascript:void(0);" onClick="javascript:history.go(-1);">' . JText::_( 'BACK' ) . '</a></div>';
	}
	return $str;
}

/**
 * Returns the Itemid for the main menu item so that
 * it doesn't conflict with the "dashboard" menu's item id
 **/  
function myGetBlogItemId(){
	static $mbItemid = -1;
	
	if($mbItemid == -1){
		
		global $Itemid;
		$db = &cmsInstance('CMSDb');
		$mbItemid = $Itemid;
		
		$strSQL	= "SELECT `id` FROM #__menu WHERE "
				. "`link` LIKE '%option=com_myblog%' "
				. "AND `link` NOT LIKE '%option=com_myblog&task=adminhome%' "
				. "AND `published`='1' "
				. "AND `id`='{$Itemid}'";
		$db->query($strSQL);

		if(!$db->get_value()){
			// The current itemId is not myblog related, ignore it, and find a valid one
			// Menu type is 'component' for Joomla 1.0 and 'components' for Jooma 1.5
			$strSQL	= "SELECT `id` FROM #__menu WHERE "
					. "(type='component' OR type='components') "
					. "AND `link`='index.php?option=com_myblog' "
					. "AND `published`='1'";
			$db->query($strSQL);
			$mbItemid = $db->get_value();
		}
	}
	return $mbItemid;
}

/**
 * Return valid Itemid for my blog links
 * If 'option' are currently on myblog, then use current ItemId 
 */  
function myGetItemId(){
	static $mbItemid = -1;
	
	if($mbItemid == -1){
		
		global $Itemid;
		$db = &cmsInstance('CMSDb');
		$mbItemid = $Itemid;
		$db->query("select id from #__menu where link LIKE '%option=com_myblog%' and published='1' AND `id`='$Itemid'");
		if(!$db->get_value()){
			// The current itemId is not myblog related, ignore it, and find a valid one
			// Menu type is 'component' for Joomla 1.0 and 'components' for Jooma 1.5
			$db->query("select id from #__menu where (type='component' or type='components') and link='index.php?option=com_myblog' and published='1'");
			$mbItemid = $db->get_value();
		}
	}

	return $mbItemid;
}

// Return the adminhome itemid
function myGetAdminItemId(){
	static $mbItemid = -1;
	
	if($mbItemid == -1){
		global $Itemid;
		$db = &cmsInstance('CMSDb');
		$mbItemid = $Itemid;
		$db->query("select id from #__menu where link LIKE '%option=com_myblog%' AND published='1' AND `id`='$Itemid' ");
		if(!$db->get_value()){
			// The current itemId is not myblog related, ignore it, and find a valid one
			$db->query("select id from #__menu where `link` LIKE '%option=com_myblog&task=adminhome%' AND published='1' AND `menutype`='usermenu' ");
			$mbItemid = $db->get_value();
		}
		
		// If no link from the preferred 'usermenu' Itemid, just use whatever myblog task=adminhome itemid we can find
		if(!$mbItemid){
			$db->query("select id from #__menu where `link` LIKE '%option=com_myblog&task=adminhome%' AND published='1'");
			$mbItemid = $db->get_value();
		}
		
		// If still cannot found, just search for a normal com_myblog link
		if(!$mbItemid){
			$db->query("select id from #__menu where `link` LIKE '%option=com_myblog%' AND published='1'");
			$mbItemid = $db->get_value();
		}
	}
	
	return $mbItemid;
}

/** Admin Stuffs **/

// Return array of links for the dashboard tabs
function myGetDashboardLinks(){
	global $_MY_CONFIG;

	$myitemid = myGetAdminItemId();

	$links   = array(
			cmsSefAmpReplace('index.php?option=com_myblog&task=adminhome&Itemid='.$myitemid),
			cmsSefAmpReplace('index.php?option=com_myblog&task=bloggerpref&Itemid='.$myitemid),
			cmsSefAmpReplace('index.php?option=com_myblog&task=bloggerstats&Itemid='.$myitemid)
	);	


 	// Check if integration with Jom comment is set.
	if($_MY_CONFIG->get('useComment')){
		$links[]	= cmsSefAmpReplace('index.php?option=com_myblog&task=showcomments&Itemid='.$myitemid);		
	}
	return $links;
}

function myGetDashboardLinksTitle(){
	global $_MY_CONFIG;

	include(MY_COM_PATH.'/language/'.$_MY_CONFIG->language);
	
	$captions   = array(
	                    $MYBLOG_LANG['TPL_MNU_MYBLOGS'],
	                    $MYBLOG_LANG['TPL_MNU_PREF'],
	                    $MYBLOG_LANG['TPL_MNU_STATS']
						);

 	// Check if integration with Jom comment is set.
	if($_MY_CONFIG->get('useComment')){
		$captions[] = $MYBLOG_LANG['TPL_MNU_COMMENTS'];
	}
	return $captions;
}

function myUnhtmlspecialchars( $string ){
	$string = str_replace ( '&amp;', '&', $string );
	$string = str_replace ( '&#039;', '\'', $string );
	$string = str_replace ( '&quot;', '"', $string );
	$string = str_replace ( '&lt;', '<', $string );
	$string = str_replace ( '&gt;', '>', $string );
	$string = str_replace ( '&uuml;', '�', $string );
	$string = str_replace ( '&Uuml;', '�', $string );
	$string = str_replace ( '&auml;', '�', $string );
	$string = str_replace ( '&Auml;', '�', $string );
	$string = str_replace ( '&ouml;', '�', $string );
	$string = str_replace ( '&Ouml;', '�', $string );    
	return $string;
}

// Return list of all category for  the target section
function myGetCategoryList($sectionid){
	$categories = array();
	$cms =& cmsInstance('CMSCore');
	$cms->db->query("SELECT * FROM #__categories WHERE `section`='$sectionid' AND `published`='1' ORDER BY title");
	$categories = $cms->db->get_object_list();
	 
	return $categories;
}

function myGetCategoryId($categoryName){
	$cms =& cmsInstance('CMSCore');
	$cms->db->query("SELECT `id` FROM #__categories WHERE `name`='{$categoryName}'"); 
	return $cms->db->get_value();
}

// Custom callback function for tag sorting
// @param $a/$b is a myblog_category object
function myTagCmp($a, $b) {
	return strcmp($a->name, $b->name);
}

// Return the number of times the tag is used
function myCountTagUsed($tagid){
	$db = &cmsInstance('CMSDb');	
	$query = "SELECT COUNT(*) FROM #__myblog_content_categories WHERE `category`='$tagid'";
	$db->query($query);
	return $db->get_value();
}

function myGetTrackbacks($contentId){
	$db	=& cmsInstance('CMSDb');
	
	$strSQL	= "SELECT `url` FROM #__myblog_tb_sent WHERE `contentid`='{$contentId}'";
	$db->query($strSQL);
	
	$trackbacks	= $db->get_object_list();
	
	$retVal		= '';
	
	foreach($trackbacks as $trackback){
		$retVal	.= $trackback->url . ',';
	}
	return $retVal;
}

// Return the html code for categories list, if content id is specified, 
// Set the checkbox as slected
function myGetTagsSelectHtml($contentid = 0){
	$db = &cmsInstance('CMSDb');	
	$query = "SELECT * FROM #__myblog_categories";
	$db->query($query);
	
	$result = $db->get_object_list();
	$currentTags = array();
	if($contentid){
		$ctags = myGetTags($contentid);
		if(!empty($ctags)){
			foreach($ctags as $ct)
				$currentTags[] = $ct->id;
		}
	}
	
	// Sort the tag alphabetically
	usort($result, 'myTagCmp');
	
	$html = '<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" ><tbody id="tagListings">';
	foreach($result as $row){
		$row->useCount = myCountTagUsed($row->id);
		
		$checked = in_array($row->id, $currentTags) ? 'checked="checked"' : '';
		$html .= '<tr>
                <td valign="middle"><input type="checkbox" value="'. $row->name.'"  '.$checked.' /></td>
                <td width="100%" valign="middle"><label class="catitem" style="vertical-align:middle" title="'.$row->useCount.' blog entries">'. $row->name.'</label></td>
            </tr>';
    }
    $html .= '</tbody></table>';
	return $html;
}

// Return the html code for categories list, if content id is specified,
// Set the checkbox as slected
function myGetCategoryHtml($contentid = 0){
	global $_MY_CONFIG;
	
	$db			= &cmsInstance('CMSDb');
	$sections   = $_MY_CONFIG->get('postSection');

	// Get current category if the content has been set a category
	$strSQL     = "SELECT `catid` FROM #__content WHERE `id`='{$contentid}'";
	$db->query($strSQL);
	$selCat     = $db->get_value();

	// Load the list of categories on the site and dont show unpublished categories
	$strSQL = 'SELECT * FROM #__categories WHERE `section` IN (' . $sections . ') AND `published`=\'1\'';
	$db->query($strSQL);

	$categories = $db->get_object_list();

	$html = '<select id="catid" name="catid" size="1" class="text">';
	
	foreach($categories as $row){
		if(cmsVersion() == _CMS_JOOMLA10 || cmsVersion() == _CMS_MAMBO)
			$rowName	= $row->name;
		else
			$rowName	= $row->title;

	    if($selCat && $selCat == $row->id){
	        $html   .= '<option value="' . $row->id . '" selected="selected">' . $rowName . '</option>';
		}else{
		    $html   .= '<option value="' . $row->id . '">' . $rowName .'</option>';
		}
    }
    $html   .= '</select>';
    
	return $html;
}


function myGetJoomlaCategoryName($id){
	global $_MY_CONFIG;
	
	$db	 = &cmsInstance('CMSDb');
	
	$sql = "SELECT `title` FROM #__categories WHERE `id`='$id' ";
	return $db->get_value($sql);
}

function getPoweredByLink(){
	$powered_by_link = '<div align="center" style="text-align:center; font-size:90%"><a href="http://www.azrul.com">Powered by Azrul&#39;s MyBlog for Joomla!</a></div>';
	return ''; //; 
}


function myAddPathway($title, $link=''){
	global $mainframe;
	
	if(cmsVersion() == _CMS_JOOMLA15){
		$pathway	= &$mainframe->getPathway();
		$pathway->addItem($title, $link);
	} else {
		global $mainframe;
		if(!empty($link))
			$title = '<a href="'.$link.'">'.$title.'</a>';
			
		$mainframe->appendPathWay($title);
	}
}

function myAddPageTitle($title){
	if(cmsVersion() == _CMS_JOOMLA15){
		$document =& JFactory::getDocument();
		$document->setTitle($title);
	} else {
		global $mainframe;
		$mainframe->setPageTitle($title);
	}
}
