<?php
/**
 * @copyright (C) 2008 by Slashes & Dots Sdn Bhd - All rights reserved!
 * @license http://www.azrul.com Copyrighted Commercial Software
 *   
 **/
 
//no direct access
(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

global $_MY_CONFIG, $sections, $sectionid, $authorid, $MYBLOG_LANG, $mainframe;

if(!function_exists('cmsInstance'))
     include_once($mainframe->getCfg('absolute_path') . '/components/libraries/cmslib/spframework.php');

$cms =& cmsInstance('CMSCore');

// Include defines.php if not included
if(!defined('MY_COM_PATH'))
	include_once($cms->get_path('root') . '/components/com_myblog/defines.myblog.php');

include_once($cms->get_path('root')."/administrator/components/com_myblog/config.myblog.php");
include_once($cms->get_path('root')."/components/com_myblog/libraries/datamanager.class.php");
include_once($cms->get_path('root')."/components/com_myblog/functions.myblog.php");


$_MY_CONFIG = new MYBLOG_Config();
include_once($cms->get_path('root'). "/components/com_myblog/language/english.php");
//include_once($cms->get_path('root'). "/components/com_myblog/language/" . $_MY_CONFIG->language);
//$MYBLOG_LANG	= myTranslateTemplate();

// Include utf8 helper.
include_once(MY_COM_PATH . '/modules/utf8.helper.php');


$Utf8helper = new Utf8Helper();
/**** Encoding fix end ****/

if(!class_exists("MyblogModule")){

	class MyblogModule
	{
		var $cms    = null;
		
		function MyBlogModule(){
			$this->cms  =& cmsInstance('CMSCore');
		}
	
		function showArchive(){
		    global $_MY_CONFIG, $authorid, $blogger;
		    
		    $cms =& cmsInstance('CMSCore');
			$cms->load('helper', 'url');
			
			$mbItemid	= $this->myGetItemID();
			$Loop 		= 0;
			$bloggerhref  = "";
			$where		= "";
			$content	= "";
			$sections	= $_MY_CONFIG->get('managedSections');
			
			//blogger name		
			if(!empty($authorid)){
				$where =" and created_by=".$authorid ;
				$bloggerhref ="&blogger=".$blogger;
			}
					
			$queryString = 'SELECT DISTINCT (date_format(jc.created,"%M-%Y")) as archive 
				FROM #__content as jc 
					WHERE 
					jc.sectionid IN('.$sections.') 
					AND state = 1 AND jc.created < NOW() '.$where.' 
				ORDER BY jc.created DESC';
				
			$this->cms->db->query($queryString);
			
			$objList    = $this->cms->db->get_object_list();
			
			//echo $this->_getCSS('archive');
			
			if($objList){
				$content .= '<ul class="blog-archives">';
				while($Loop < count($objList)){
					$strSQL	= "SELECT COUNT(*) FROM #__content AS total "
							. "WHERE sectionid IN ({$sections}) "
							. "AND state=1 "
							. "AND date_format(created, '%M-%Y') = '{$objList[$Loop]->archive}'";
					$this->cms->db->query($strSQL);
					$count	= $this->cms->db->get_value();
					$link = cmsSefAmpReplace("index.php?option=com_myblog&archive={$objList[$Loop]->archive}{$bloggerhref}&Itemid=$mbItemid"); 
					$content .= "<li><a href='$link'>";
					$content .= i8n_date(str_replace("-"," ",$objList[$Loop]->archive))." <span>($count)</span></a>";
					$content .= "</li>";
					$Loop++;
				}
				$content .= '</ul>';
			}
			echo $content;
		}
	
		function myGetItemID(){
			return myGetItemId(); 
		}
	
		function showLatestEntriesIntro(&$params){
			global $_MY_CONFIG;
			
			$postedByDisplay    = $params->get('latestEntriesPostedBy');
			$titleMaxLength 	= $params->get('titleMaxLength', 20);
			$introMaxLength     = $params->get('introMaxLength', 50);
			$wrapIntroText      = $params->get('wrapIntro', 10);		
			$limit				= $params->get('numLatestEntries', 5);
			$sections           = $_MY_CONFIG->get('managedSections');
			$showAuthor         = $params->get('showAuthor',1);
			$showReadmore		= $params->get('showReadmore', 1);
			$readmoreText		= $params->get('readmoreText', 'Readmore...');
	
			$this->cms->load('helper', 'url');
			$this->cms->load('libraries' , 'utf8');
			$blogger    = cmsGetVar('blogger','','GET');
			$authorid   = myGetAuthorId($blogger);
			
			if($authorid == '0')
				$authorid	= '';
	
			if (!is_numeric($titleMaxLength) or $titleMaxLength == "0")
				$titleMaxLength = 20;
	
			if (!is_numeric($limit))
				$limit = 5;
	
	
			if(function_exists('mb_get_entries')){
				$filter = array(
									'limit'=> $limit,
									'limitstart' => 0,
									'authorid' => $authorid
								);
				$entries = mb_get_entries($filter);
			}
			else {
				$objDataMngr = new MY_DataManager();
				$entries = $objDataMngr->getEntries($total,$limit,0,$authorid);
			}
	
			$mbItemid	= 1;
	
			$mbItemid = $this->myGetItemID();
	
			if($entries){
	?>
				<ul class="blog-latest">
	<?php
				foreach ($entries as $row){
					$row->permalinkURL	= myGetPermalinkUrl($row->id);
					$row->titleLink		= $row->permalinkURL;
					$row->author		= myGetAuthorName($row->created_by, ($postedByDisplay=="1" ? "0" : "1"));
	
					$row->authorLink 	= cmsSefAmpReplace("index.php?option=com_myblog&blogger=".urlencode($row->author)."&Itemid=".$mbItemid."");
					$row->title 		= htmlspecialchars($row->title);
	?>
					
						<li>
							<a title="<?php echo $row->title; ?>" href="<?php echo $row->titleLink; ?>">
	<?php 
						$titlelength = $this->cms->utf8->strlen($row->title);
						
						if ($titlelength>$titleMaxLength)
							$row->title = $this->cms->utf8->substr($row->title,0,$titleMaxLength);
						
						echo $row->title; 
	
						if ($titlelength>$titleMaxLength)
							echo " ...";
						echo "</a>";
						/**
						 * Introtext
						 **/					 					
						echo '<br />';
						
						// Strip unwanted tags
						$text	= $row->introtext . $row->fulltext;
						$text	= strip_tags($text);
						$text	= preg_replace('#\s*<[^>]+>?\s*$#','',$text);

						if($this->cms->utf8->strlen($text) > $introMaxLength)
						{
					    	$text  = $this->cms->utf8->substr($text,0,$introMaxLength);
					    }
				
						//print_r($this->cms->utf8->substr($text,0,$introMaxLength));
						$text   = wordwrap($text,$wrapIntroText,'<br />');
						$text .= ' ...';
	
						echo $text;
						if ($showAuthor != "2"){
	?>
							<span> by <a href="<?php echo $row->authorLink; ?>"><?php echo $row->author; ?></a></span>
	<?php 
						}
						
						if($showReadmore){
	?>
							<br /><a href="<?php echo $row->titleLink; ?>"><?php echo $readmoreText; ?></a>
	<?php
						}
	?>
						</li>		
	<?php	
				}
	?>
				</ul>
	<?php
		 	}else{
		 		echo '<i>None</i>';
			}
		}
	
		function _getCSS($type)
		{
			global $_MY_CONFIG , $mainframe;
			
			$path	= $this->cms->get_path('root') . '/components/com_myblog/templates/_default/module.' . $type . '.css';
			$file	= '';
					
			if( $_MY_CONFIG->get('overrideTemplate') )
			{
				$file	= $this->cms->get_path('root') . '/templates/' . $mainframe->getTemplate() . '/com_myblog/module.' . $type . '.css';
			}
			else
			{
				$file	= $this->cms->get_path('root') . '/components/com_myblog/templates/' . $_MY_CONFIG->get('template') . '/module.' . $type . '.css';
			}
	
			if( file_exists( $file ) )
			{
				$path	= $file;
			}		
	
			$handle		= fopen( $path , 'r');
			$contents	= fread( $handle , filesize( $path ) );
			fclose($handle);
			$data		= '';
			
			if(!empty($contents)){
				$data		= '<style type="text/css">';
				$data		.= $contents;
				$data		.= '</style>';
			}
			return $data;
		}
		
		function _getAvatar($creator)
		{
			global $_MY_CONFIG;
			
			require_once( $this->cms->get_path('root') . '/components/com_myblog/libraries/avatar.class.php' );
						
			$avatar	= 'My' . ucfirst($_MY_CONFIG->get('avatar')) . 'Avatar';
			$avatar	= new $avatar($creator);
			
			$avatar	= $avatar->get();
			return $avatar;
		}
	
		function showLatestEntries(&$params)
		{
			global $_MY_CONFIG;
	
			$sections           = $_MY_CONFIG->get('managedSections');
			$postedByDisplay 	= $params->get('latestEntriesPostedBy', 1);
			$titleMaxLength 	= $params->get('titleMaxLength',20);
			
			$suffix				= $params->get('moduleclass_sfx');

			$showAuthor         = $params->get('showAuthor',1);
			
			$showReadmore		= $params->get('displayReadmore' , 0);
			$showReadmoreText	= $params->get('displayReadmoreText' , 'Read More...');			
			$showAvatar			= $params->get('displayAvatar' , 0);
			
	
			$this->cms->load('helper','url');
			$this->cms->load('libraries', 'utf8');
						
			$blogger    = cmsGetVar('blogger','','GET');
			$authorid   = myGetAuthorId($blogger);
			
			if($authorid == '0')
				$authorid	= '';
	
			if (!is_numeric($titleMaxLength) or $titleMaxLength == "0")
				$titleMaxLength = 20;
			
			$limit = 5;
			if(isset($params))
				$limit	= $params->get('numLatestEntries', 5);
	
			if (!is_numeric($limit))
				$limit = 5;
		
			if(function_exists('mb_get_entries')){
				$filter = array(
					'limit'=> $limit,
					'limitstart' => 0,
					'authorid' => $authorid);
				$entries = mb_get_entries($filter);
			}
			else {
				$objDataMngr = new MY_DataManager();
				$entries = $objDataMngr->getEntries($total,$limit,0,$authorid);
			}
		
			$mbItemid	= 1;
			$mbItemid = $this->myGetItemID();
			
			//$css	= $this->_getCSS('latestentry');
			
			//echo $css;
			
			if($entries){
			
				if($showAvatar){
?>
					<table width="100%" cellpadding="3" cellspacing="0" border="0">
<?php
				}
				else
				{
?>
				<ul class="blog-latest">
<?php
				}
				
				foreach ($entries as $row){
					$row->permalinkURL = myGetPermalinkUrl($row->id);
					$row->titleLink = $row->permalinkURL;
					$row->author = myGetAuthorName($row->created_by, ($postedByDisplay=="1" ? "0" : "1"));
	
					$row->authorLink = cmsSefAmpReplace("index.php?option=com_myblog&blogger=".urlencode($row->author)."&Itemid=".$mbItemid."");
					$row->title = htmlspecialchars($row->title);
	
	
					$titlelength = $this->cms->utf8->strlen($row->title);
						
					if ($titlelength>$titleMaxLength)
						$row->title = $this->cms->utf8->substr($row->title,0,$titleMaxLength);
						
					$title	= $row->title; 
						
					if ($titlelength>$titleMaxLength)
						$title .= ' ...';
						
						
					if( $showAvatar )
					{
						// Table style
?>
							<tr style="border-bottom: 1px solid #eee;">
								<td width="70%" valign="top">
									<span>
										<a title="<?php echo $row->title; ?>" href="<?php echo $row->titleLink; ?>">
											<?php echo $title; ?>
										</a>
<?php 
										if ($showAuthor != "2")
										{
?>
										by <a href="<?php echo $row->authorLink; ?>"><?php echo $row->author; ?></a>
<?php 
										}
?>
									</span>
<?php					
										if( $showReadmore )
										{
?>
										<br /><a href="<?php echo $row->titleLink; ?>" title="<?php echo $row->title;?>"><?php echo $showReadmoreText; ?></a>
<?php
										}
?>
								</td>
								<td width="30%" align="right">
									<span><?php echo $this->_getAvatar($row->created_by); ?></span>
								</td>
							</tr>
<?php
					}
					else
					{
?>				
					<li>
						<span>
							<a title="<?php echo $row->title; ?>" href="<?php echo $row->titleLink; ?>">
								<?php echo $title; ?>
							</a>
<?php 
						if ($showAuthor != "2")
						{
?>
							by <a href="<?php echo $row->authorLink; ?>"><?php echo $row->author; ?></a>
<?php 
						}
				
						if( $showReadmore )
						{
?>
							<div><a href="<?php echo $row->titleLink; ?>" title="<?php echo $row->title;?>"><?php echo $showReadmoreText; ?></a></div>
<?php
						}
?>
						</span>
					</li>		
					
					<?php
					}	
				}

				if( $showAvatar )
				{
?>
						</table>
<?php
				} else {
?>
						</ul>
<?php
				}
?>
				<?php
		 	}else
		 	{
				echo '<i>None</i>';
			}
		}
	
		function showTagClouds($params){
			global $moduleparams, $mainframe, $_MY_CONFIG;
			$cms =& cmsInstance('CMSCore');
			$cms->load('helper', 'url');
			
			
			$wrapperTag   = isset($params) ? $params->get('wrapTag', 'div') : 'div';
			
			include_once($this->cms->get_path('root'). "/components/com_myblog/task/categories.php");
			
			$mbItemid = $this->myGetItemID();
			$objFrontView = new MyblogCategoriesTask();
			$tagCloud = $objFrontView->display('id="blog-tags-mod"', $wrapperTag);
	
			$tagCloud = str_replace("<p>","",$tagCloud);
			$tagCloud = str_replace("</p>","",$tagCloud);
			$tagCloud = str_replace("<br/>","",$tagCloud);
		
			// Add the custom css file. Firstly, check in the current template folder if
			// module.tags.css exits. If tempate overriede is used, check in those.
			// If both of the above fails, use the default one in _default
			$cssFilePath = MY_COM_PATH . '/templates/_default/module.tags.css';
			if ($_MY_CONFIG->get('overrideTemplate')){
				$filename = $mainframe->getCfg('absolute_path') .'/templates/' .$mainframe->getTemplate() .'/com_myblog/module.tags.css';
				if(file_exists($filename))
					$cssFilePath = $filename ;
			}else{
				$filename = MY_TEMPLATE_PATH . "/" . $_MY_CONFIG->get('template') . "/module.tags.css";
				if(file_exists($filename))
					$cssFilePath = $filename; 
			}
	
			$handle		= fopen($cssFilePath, 'r');
			$contents	= fread($handle, filesize($cssFilePath));
	
			if(!empty($contents)){
				echo '<style type="text/css">';
				echo $contents;
				echo '</style>';
			}
			fclose($handle);
	
			if (trim($tagCloud)!="")
				echo $tagCloud;
			else
				echo '<i>None</i>';
		}
	
		function showLatestComment(&$params){
		    global $moduleparams, $_MY_CONFIG;
	
			$mbItemid = $this->myGetItemID();
			$this->cms->load('helper','url');
			$this->cms->load('libraries' , 'utf8');
			
			$postedByDisplay    = $params->get('latestCommentsPostedBy', 1);
	   		$titleMaxLength		= $params->get('titleMaxLength', 20);
	
			if (!is_numeric($titleMaxLength) or $titleMaxLength == "0")
				$titleMaxLength = 20;
	
			$where="";
		
		    $sections   = $_MY_CONFIG->get('managedSections');
	
			$blogger    = cmsGetVar('blogger','','GET');
			$authorid   = myGetAuthorId($blogger);
			
			if($authorid == 0){
				$authorid = "";
			} else{
				$where = "and created_by = '".$authorid."'";
			}
		
			$limit = $params->get('numLatestComments', 5);
	
			if (!is_numeric($limit))
				$limit = 5;
	
			$strSQL	= "SELECT a.id, a.comment, a.preview AS preview, a.contentid, a.date, a.user_id FROM #__jomcomment AS a "
					. "JOIN #__content AS b ON a.contentid=b.id "
					. "WHERE b.sectionid IN ($sections) "
					. "AND b.state=1 "
					. "AND a.published=1 "
					. "AND a.option='com_myblog' $where ORDER BY date DESC LIMIT 0, $limit";
		
		    $this->cms->db->query($strSQL);
		    
			$results    = $this->cms->db->get_object_list();
			$Loop = 0;
			$content = "";
		
			$content = '<ul class="blog-comments">';
			
			foreach($results as $row){
				// Test if the preview is available, if it is, just use the preview field
				if($row->preview)
					$row->comment	= $row->preview;

				if($this->cms->utf8->strlen($row->comment) > $titleMaxLength)
					$row->comment	= $this->cms->utf8->substr(strip_tags(trim($row->comment)), 0, $titleMaxLength) . ' ...';
			
				if($row->contentid){
					$permalink	= myGetPermalinkUrl($row->contentid);
					$titleHref	= $permalink . '#comment-' . $row->id;
				}
				$authorLink	= '';
				
				if($postedByDisplay != '0'){
					if($row->user_id == '0'){
						$authorLink	= '<span class="blog-comment-author">by Guest</span>';
					} else {
						$author	= myUserGetName($row->user_id, ($postedByDisplay == '1' ? '0' : '1'));
						$authorLinkHref	= cmsSefAmpReplace('index.php?option=com_myblog&blogger=' . urlencode($author) . '&Itemid=' . $mbItemid);
						$authorLink	= '<span class="blog-comment-author">by <a href="' . $authorLinkHref . '">' . $author . '</a></span>';
					}
				}
				
				/** Encoding fix start **/
				global $Utf8helper;
				$content	.= "<li><a href='".$titleHref."'>".$Utf8helper->utf8ToHtmlEntities($row->comment)."</a>$authorLink</li>";
				/**** Encoding fix end ****/
			}
			
			$content .= "</ul>";
			echo $content;
		}
	
	
		function showPopularBlogger(&$params)
		{
		    global $_MY_CONFIG, $_MAMBOTS;
		    
			$myItemid	= $this->myGetItemID();
	
			$showAvatar	= $params->get('displayAvatar' , 0);
			
			$this->cms->load('helper','url');
			$content = "";
			
			// Get the proper param values.
			$postedByDisplay    = $params->get('popularBlogsPostedBy', 0);
			$limit				= $params->get('numPopularBlogs', 5);
	
			if (!is_numeric($limit))
				$limit = 5;
	
			$sections   	= $_MY_CONFIG->get('managedSections');
	
			$strSQL         = "SELECT `created_by`, sum(hits) AS `hits` "
			                . "FROM #__content WHERE `sectionid` IN ({$sections}) "
			                . "AND `state`='1' GROUP BY `created_by` "
			                . "ORDER BY `hits` DESC "
			                . "LIMIT 0,{$limit}";
	
			$this->cms->db->query($strSQL);
			$rows    = $this->cms->db->get_object_list();
	
			
			// Load css file
			//echo $this->_getCSS('blogger');
			if( $showAvatar )
			{
?>
				<table width="100%" cellpadding="3" cellspacing="0">
<?php
			}
			else
			{
?>
				<ul class="blog-bloggers">
<?php
			}

			if($rows)
			{
				foreach($rows as $row)
				{
					$strSQL	= "SELECT COUNT(*) FROM #__content WHERE `created_by`='{$row->created_by}' AND `sectionid` IN ({$sections}) AND `state` != '0' AND created <= NOW()";
					$this->cms->db->query($strSQL);
					$count	= $this->cms->db->get_value();
					$authorname = myUserGetName($row->created_by, 0);
				    $link   = cmsSefAmpReplace("index.php?option=com_myblog&blogger={$authorname}&Itemid={$myItemid}");
				    
				    if($showAvatar)
				    {
?>
					<tr style="border-bottom: 1px solid #eee;">
						<td width="70%">
								<a href="<?php echo $link;?>">
									<?php echo myUserGetName($row->created_by, ($postedByDisplay == '1' ? '0' : 1 )); ?>
								</a>
								(<?php echo $count; ?>)
						</td>
						<td align="right">
							<?php echo ($showAvatar) ? $this->_getAvatar( $row->created_by ) : ''; ?>
						</td>
					</tr>
<?php				}
					else
					{
?>
					<li>
						<a href="<?php echo $link;?>">
							<?php echo myUserGetName($row->created_by, ($postedByDisplay == '1' ? '0' : 1 )); ?>
						</a>
						(<?php echo $count; ?>)
					</li>
<?php
					}
				}
				
				if( $showAvatar )
				{
?>
					</table>
<?php
				}
				else
				{
?>
					</ul>
<?php
				}
			}
			else
			{
?>
				<p>No bloggers yet.</p>
	<?php
			}
		}
		
		function showCategories()
		{
		    global $_MY_CONFIG;
		    
		    $categories	= myGetCategoryList( $_MY_CONFIG->get('postSection') );

		    $this->cms->load('helper','url');
		    
		    //echo $this->_getCSS('categories');
?>
			<ul class="blog-categories">
<?php
			foreach( $categories as $category)
			{
				$link	= cmsSefAmpReplace('index.php?option=com_myblog&task=tag&category=' . $category->id . '&Itemid=' . myGetItemId() );
				$count	= myGetCategoryCount( $category->id );
?>
				<li>
					<span>
						<a href="<?php echo $link;?>"><?php echo $category->title; ?> (<?php echo $count; ?>)</a>
					</span>
				</li>
<?php
			}
?>
			</ul>
<?php
		}
		
	}//end class
}//end outher if