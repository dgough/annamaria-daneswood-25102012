# phpMyAdmin MySQL-Dump
# version 2.5.0
# http://www.phpmyadmin.net/ (download page)
#
# Host: localhost
# Generation Time: Sep 10, 2003 at 06:47 PM
# Server version: 3.23.33
# PHP Version: 4.3.2
# Database : `isolani`
# --------------------------------------------------------

#
# Table structure for table `atomauthor`
#
# Creation: Sep 10, 2003 at 06:46 PM
# Last update: Sep 10, 2003 at 06:46 PM
#

CREATE TABLE `atomauthor` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(31) NOT NULL default '',
  `url` varchar(255) NOT NULL default '',
  `email` varchar(255) NOT NULL default '',
  PRIMARY KEY (`id`),
  KEY `name`(`name`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

#
# Dumping data for table `atomauthor`
#

# --------------------------------------------------------

#
# Table structure for table `atomcontent`
#
# Creation: Sep 10, 2003 at 06:47 PM
# Last update: Sep 10, 2003 at 06:47 PM
#

CREATE TABLE `atomcontent` (
  `id` int(11) NOT NULL auto_increment,
  `type` varchar(63) NOT NULL default '',
  `mode` varchar(20) NOT NULL default '',
  `text` text NOT NULL,
  `entryid` int(11) default '0',
  PRIMARY KEY (`id`),
  KEY `entryid`(`entryid`),
  FULLTEXT KEY `text`(`text`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

#
# Dumping data for table `atomcontent`
#

# --------------------------------------------------------

#
# Table structure for table `atomentry`
#
# Creation: Sep 10, 2003 at 06:47 PM
# Last update: Sep 10, 2003 at 06:47 PM
#

CREATE TABLE `atomentry` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL default '',
  `shortName` varchar(255) NOT NULL default '',
  `link` varchar(255) NOT NULL default '',
  `modified` timestamp(14) NOT NULL,
  `authorid` int(11) default '0',
  `entryid` varchar(255) NOT NULL default '',
  `issued` timestamp(14) NOT NULL,
  `created` timestamp(14) NOT NULL,
  `summary` text NOT NULL,
  `blogname` varchar(255) default NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `title`(`title`),
  FULLTEXT KEY `summary`(`summary`),
  KEY `created`(`created`),
  KEY `shortName`(`shortName`),
  KEY `blogname`(`blogname`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

#
# Dumping data for table `atomentry`
#