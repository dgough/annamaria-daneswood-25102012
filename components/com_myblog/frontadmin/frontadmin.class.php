<?php
(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');



class MB_Frontadmin {

	/**
	 *	Retrieve content of directory $root and return the HTML to be inserted into the
	 *	image browser's directory tree browser
	 */	 
	function show_folder($root, $show = "true") {
		global $_MY_CONFIG;
		$cms    =& cmsInstance('CMSCore');
		
		// Get the root folder
		$imageRoot = $_MY_CONFIG->get('imgFolderRoot');
		
		if (!$imageRoot or $imageRoot == "")
			$imageRoot = "/images/";
		
		$imageRoot = trim($imageRoot, "/ ");
		$imageRoot = "/".$imageRoot;
		
		// if user is restricted to his/her own folder
		if ($_MY_CONFIG->get('imgFolderRestrict') == "1") {
			$file_path = $cms->get_path('root').$imageRoot.str_replace($imageRoot, "", $root);
		} else
			$file_path = $cms->get_path('root').$root;
		
		$content = "";
		$curr_dir = $file_path;
		
		if ($show == "false")
			return "";
		
		if (!file_exists($file_path))
			mkdir($file_path);
		
		if ($dir_handle = opendir($file_path)) {
			while (($file = readdir($dir_handle)) !== false) {
				if (is_dir($curr_dir.$file) and $file != "." and $file != "..") {
					$newroot = $root.$file."/";
					$root_content_id = str_replace("/", '_', $newroot)."content";
					$root_link_id = str_replace("/", '_', $newroot)."link";
					$content .= "<a id=\"$root_link_id\" href=\"javascript:jax.call('myblog','myxShowFolder','$newroot')\">$file</a><br/><div id=\"$root_content_id\" name=\"$root_content_id\" style=\"display:none;\"></div>";
				}
			}
			closedir($dir_handle);
		}
		return $content;
	}
	
	/**
	 *	Function to clean up any images uploaded for a content, but that content was not saved.
	 */	 	
	function cleanupTempImages() {
		global $mainframe;
		$cms    =& cmsInstance('CMSCore');
		$db		=& cmsInstance('CMSDb');
		
		$cms->load('libraries', 'user');
		
		$query = "SELECT * FROM #__myblog_images WHERE contentid='0' and user_id='{$cms->user->id}'";
		$db->query($query);
		$imgRows = $db->get_object_list();
		
		if ($imgRows) {
			foreach ($imgRows as $img) {
				if (!empty ($img)) {
					$file_site_path = $img->filename;
					$file_path = str_replace($mainframe->getCfg('live_site'), $cms->get_path('root'), $file_site_path);
					unlink($file_path);
				}
			}
		}
		
		$db->query("DELETE from #__myblog_images WHERE contentid='0' and user_id='{$cms->user->id}'");
		
		//cleanup Tags as well
		$db->query("DELETE FROM #__myblog_content_categories WHERE contentid='0'");
	}
	
	/**
	 *	Uploads image to the server
	 */	 	
	function uploadImage() {
		global $MYBLOG_LANG, $_MY_CONFIG, $mainframe;
		$cms    =& cmsInstance('CMSCore');
		$cms->load('libraries', 'user');
		$cms->load('helper', 'url');
		
		include_once ($cms->get_path('root').'/language/'.$mainframe->getCfg('lang').'.php');
		
		$uploadOk = false;
		$directory = cmsGetVar('directory');
		$media_path = $cms->get_path('root') .'/media/';
		$userfile2 = (isset ($_FILES['userfile']['tmp_name']) ? $_FILES['userfile']['tmp_name'] : "");
		$userfile_name = (isset ($_FILES['userfile']['name']) ? $_FILES['userfile']['name'] : "");
		$base_Dir = "";
		$file_path = "";
		
		// If there is an image to upload
		if (isset ($_FILES['userfile'])) {
			if ($directory == 'banners') {
				$base_Dir = "$cms->get_path('root')/images/banners/";
				$file_path = $mainframe->getCfg('live_site').'/images/banners/';
			} elseif ($directory != '') {
					$base_Dir = "$cms->get_path('root')/images/stories/".$directory;
					$file_path = $mainframe->getCfg('live_site')."/images/stories/$directory/";
			} else { // No directory defined. Grab image folder directory.
				$imageRootDir = $_MY_CONFIG->get('imgFolderRoot');
				
				if (!$imageRootDir or $imageRootDir == "")
					$imageRootDir = "/images/";
				$imageRootDir = trim($imageRootDir, "/ ");
				$imageRootDir = "/".$imageRootDir."/";
				
				// Always upload images into a user's directory.
				$base_Dir = $cms->get_path('root').$imageRootDir."$cms->user->id/";
				$file_path = $mainframe->getCfg('live_site').$imageRootDir."$cms->user->id/";
				
				// Create directory if it does not exist
				if (!file_exists($base_Dir)) {
					mkdir("$base_Dir");
				}
			}
			
			if (empty ($userfile_name)) {
				echo "<script>alert('Please select an image to upload');</script><a href=\"javascript:history.go(-1)\">[ Back ]</a>";
			}
			
			$filename = split("\.", $userfile_name);
			
			if (eregi("[^0-9a-zA-Z_]", $filename[0])) {
				mosErrorAlert("File must only contain alphanumeric characters and no spaces please.");
			}
			
			if (file_exists($base_Dir.$userfile_name)) {
				mosErrorAlert("Image ".$userfile_name." already exists.");
			}
			
			if ((strcasecmp(substr($userfile_name, -4), ".gif")) && (strcasecmp(substr($userfile_name, -4), ".jpg")) && (strcasecmp(substr($userfile_name, -4), ".png")) && (strcasecmp(substr($userfile_name, -4), ".bmp")) && (strcasecmp(substr($userfile_name, -4), ".doc")) && (strcasecmp(substr($userfile_name, -4), ".xls")) && (strcasecmp(substr($userfile_name, -4), ".ppt")) && (strcasecmp(substr($userfile_name, -4), ".swf")) && (strcasecmp(substr($userfile_name, -4), ".pdf"))) {
				mosErrorAlert("The file must be gif,png,jpg,bmp,swf,doc,xls or ppt");
			}
			
			// Check if image exceeds image upload size limit defined in My Blog config
			if ($_FILES['userfile']['size'] >= $_MY_CONFIG->get('uploadSizeLimit') * 1024)
			{
				mosErrorAlert("Upload of ".$userfile_name." failed. File size exceeds limit.");
			}
			
			if (eregi(".pdf", $userfile_name) || eregi(".doc", $userfile_name) || eregi(".xls", $userfile_name) || eregi(".ppt", $userfile_name)) {
				if (!move_uploaded_file($_FILES['userfile']['tmp_name'], $media_path.$_FILES['userfile']['name']) || !mosChmod($media_path.$_FILES['userfile']['name'])) {
					mosErrorAlert("Upload of ".$userfile_name." failed");
				} else {
					mosErrorAlert("Upload of ".$userfile_name." to $media_path successful");
				}
			}
			elseif (!move_uploaded_file($_FILES['userfile']['tmp_name'], $base_Dir.$_FILES['userfile']['name']) || !mosChmod($base_Dir.$_FILES['userfile']['name'])) {
				mosErrorAlert("Upload of ".$userfile_name." failed");
			} else {
				$uploadOk = true;
			}
		}
		
		echo '<?xml version="1.0" encoding="'.cmsGetISO().'"?'.'>';
		echo ' <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <title>'.$MYBLOG_LANG['_MB_FILE_UPLOAD'].'</title>';
		
		// If image uploaded, add that image into the image browser.
		if ($uploadOk) {
			$i = getimagesize($base_Dir.$userfile_name);
			$h = $i[1];
			$w = $i[0];
		
			$uploadedImgAppend = displayAndResizeImages(array (
				array (
					'filename' => $file_path.$userfile_name,
					'width' => "$w",
					'height' => "$h"
				)
			));
			$uploadedImgAppend = myEncodeString($uploadedImgAppend);
			$uploadedImgAppend = str_replace('"', '\\"', $uploadedImgAppend);
			echo " <script language=\"JavaScript\" type=\"text/javascript\">"."alert('Upload successful');opener.document.getElementById(\"uploadedImg\").innerHTML+=\"".$uploadedImgAppend."\";opener.document.getElementById(\"imageList\").value+=\";".$file_path.$userfile_name."\";</script> ";
			$uploadImg = new MyBlogImages();
			$uploadImg->id = 0;
			$uploadImg->filename = $file_path.$userfile_name;
			$uploadImg->contentid = '0';
			$uploadImg->user_id = $cms->user->id;
			$uploadImg->store();
		}
		
		echo ' </head> <body> <form method="post" action="" enctype="multipart/form-data" name="filename"> <table width="480"> <tr> <td bgcolor="#CCCCCC"> <font size="2" face="Trebuchet MS,Verdana"><strong>'.$MYBLOG_LANG['_MB_FILE_UPLOAD']	.':<?php echo $directory;?> </strong></font> </td> </tr> <tr> <td align="center"> <div align="left"> <font size="2" face="Trebuchet MS,Verdana"> <input name="userfile" type="file" class="inputbox" size="48" /> </font> </div> </td> </tr> <tr> <td> <font size="2" face="Trebuchet MS,Verdana"> <input onlick="document.getElementById(\'contentid\').value=opener.document.getElementById(\'id\').value" class="button" type="submit" value="'.$MYBLOG_LANG['_MB_UPLOAD'].'" name="fileupload" /> '.$MYBLOG_LANG['_MB_MAX_IMG_SIZE'].'='.$_MY_CONFIG->get('uploadSizeLimit').'K </font> </td> </tr> </table> <input type="hidden" name="directory" value="'.$directory.'" /> <input type="hidden" name="contentid" value="0" /> </form> </body> </html> ';
	}
	
	
}

class MB_Admin_Controller {
	var $task;
	var $jax;
	var $cms;
	
	// init
	function init(){
		
		$this->cms = &cmsInstance('CMSCore');
		$this->cms->load('helper', 'url');
		$this->task = cmsGetVar('task');
		
		$this->jax = new JAX($this->cms->get_path('plugin-live') ."/system/pc_includes");
		$this->jax->setReqURI($this->cms->get_path('live')."/index2.php");
		$this->jax->process();
		
		$this->cms->load('libraries', 'user');
		
	}
	
	/**
	 *	Function to clean up any images uploaded for a content, but that content was not saved.
	 */	 	
	function _cleanup_temp_images() {
		global $mainframe;
		$cms    =& cmsInstance('CMSCore');
		$cms->load('libraries', 'user');
		
		$db =& cmsInstance('CMSDb');
		
		$query = "SELECT * FROM #__myblog_images WHERE contentid='0' and user_id='$cms->user->id'";
		$db->query($query);
		$imgRows = $db->get_object_list();
		
		if ($imgRows) {
			foreach ($imgRows as $img) {
				if (!empty ($img)) {
					$file_site_path = $img->filename;
					$file_path = str_replace($mainframe->getCfg('live_site'), $cms->get_path('root'), $file_site_path);
					unlink($file_path);
				}
			}
		}
		
		$db->query("DELETE from #__myblog_images WHERE contentid='0' and user_id='{$cms->user->id}'");
		
		//cleanup Tags as well
		$db->query("DELETE FROM #__myblog_content_categories WHERE contentid='0'");
	}
	
	/**
	 *	View list of blog entries
	 */	 	
// 	function _get_entries($params = array ()) {
// 		global $MYBLOG_LANG,  $Itemid, $_MY_CONFIG, $sectionid, $sections, $mainframe;
// 		
// 		$cms    =& cmsInstance('CMSCore');
// 		$cms->load('libraries', 'user');
// 		$cms->load('helper', 'url');
// 		
// 		$db		=& cmsInstance('CMSDb');
// 		
// 		$content = "";
// 		include(MY_COM_PATH."/language/".$_MY_CONFIG->language);
// 		// Get total number of entries for this user
// 		$db->query(" SELECT count(*) FROM #__content WHERE `created_by`='{$cms->user->id}' and sectionid IN ($sections) ORDER BY state,created");
// 		$count = $db->get_value();
// 		
// 		if(cmsVersion() == _CMS_JOOMLA10 || cmsVersion() == _CMS_MAMBO){
// 			$limit = mosGetParam($params, 'limit', $_MY_CONFIG->get('numEntry'));
// 			$limitstart = mosGetParam($params, 'limitstart', 0);		
// 		}elseif(cmsVersion() == _CMS_JOOMLA15){
// 			$limit = mosGetParam($params, 'limit', $_MY_CONFIG->get('numEntry'));
// 			$limitstart = mosGetParam($params, 'limitstart', 0);	
// 		}
// 		
// 		include_once ($cms->get_path('root')."/includes/pageNavigation.php");
// 		
// 		// Check publishing rights
// 		$publisher_ids = explode(',', $_MY_CONFIG->get('allowedPublishers'));
// 		$publishers = explode(',', $_MY_CONFIG->get('publishControlGroup'));
// 		$extraPublishGroups = explode(",", $_MY_CONFIG->get('extraPublishGroups'));
// 		array_walk($extraPublishGroups, "myTrim");
// 		$canPublish = false;
// 		$backendusers = explode(',', $_MY_CONFIG->get('adminPublishControlGroup'));
// 		if (in_array($cms->user->usertype, $extraPublishGroups) 
// 			or in_array($cms->user->id, $publisher_ids) 
// 			or in_array($cms->user->usertype, $publishers) 
// 			or in_array($cms->user->usertype, $backendusers))
// 			$canPublish = true;
// 			
// 		
// 		$content .= ' <table width="100%" border="0" cellspacing="0" cellpadding="4"> <tr><td> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="mytable"> <tr> <th>'.$MYBLOG_LANG['_MB_TITLE'].'</th> <th>'.$MYBLOG_LANG['_MB_PUBLISHED_SHORT'].'</th> <th>'.$MYBLOG_LANG['_MB_DATE'].'</th> <th width="25%">'.$MYBLOG_LANG['_MB_ACTION'].'</th> </tr>';
// 		
// 		// Retrieve blog entries for current page
// 		$db->query(" SELECT c.*,p.permalink FROM #__content as c,#__myblog_permalinks as p WHERE c.created_by='{$cms->user->id}' and sectionid IN ($sections) and p.contentid=c.id ORDER BY c.created desc,c.state ASC LIMIT $limitstart,$limit");
// 		$entries = $db->get_object_list();
// 		$k = 0;
// 		
// 		if (count($entries)>0) {
// 			foreach ($entries as $row) {
// 				$k++;
// 				$published = "";
// 				$onClick = "\"alert('You are not allowed to publish/unpublish');\"";
// 				$href='javascript:void(0);';
// 				
// 				
// 				if ($canPublish){
// 					$onClick = "\"jax.call('myblog','myxTogglePublish',$row->id);\"";
// 					if ($_MY_CONFIG->languageCompat==1){
// 						$onClick = "";
// 						$href = "index2.php?option=com_myblog&task=publish&id={$row->id}&admin=1&limitstart=$limitstart";
// 					}
// 				}
// 				if ($row->state == 1)
// 					$published = "<a href=\"$href\" onclick=$onClick><img id=\"pubImg$row->id\" src=\"$mainframe->getCfg('live_site')/components/com_myblog/images/publish_g.png\" border=\"0\" alt=''/></a>";
// 				else
// 					$published = "<a href=\"$href\" onclick=$onClick><img id=\"pubImg$row->id\" src=\"$mainframe->getCfg('live_site')/components/com_myblog/images/publish_x.png\" border=\"0\" alt=''/></a>";
// 					//$row->title = preg_replace("/([\x80-\xFF])/e", "chr(0xC0|ord('\\1')>>6).chr(0x80|ord('\\1')&0x3F)", $row->title);
// 				
// 				$onclick = "show_dashboard('index2.php?option=com_myblog&task=write&no_html=1&id=$row->id')";
// 				$link = "javascript:void(0)";
// 				$linkDel = "index2.php?option=com_myblog&task=delete&admin=1&no_html=1&id=$row->id&limitstart=$limitstart&limit=$limit";
// 				
// 				// Create entry row
// 				$created = cmsFormatDate($row->created, "%m/%d/%y");
// 				$content .= " <tr class='mytableentry$k'> <td>".str_replace('}', "&#125;", (str_replace("{", "&#123;", $row->title)))."</td> <td align=\"center\">&nbsp;$published</td> <td>". $created."</td> <td> <a href=\"$link\" onclick=\"$onclick\" class=\"CommonTextButtonSmall\"><span>".$MYBLOG_LANG['_MB_EDIT']."</span></a> <a href=\"$linkDel\" class=\"CommonTextButtonSmall\" onclick=\"if(!confirm('Are you sure you want to delete this blog entry?')) return false;\"><span>".$MYBLOG_LANG['_MB_DELETE']."</span></a> </td> </tr> ";
// 				
// 				$k++;
// 				$k = 1 - $k;
// 			}
// 		}
// 		
// 		$newlink = "index2.php?option=com_myblog&no_html=1&task=edit&id=0&Itemid=$Itemid";
// 		$content .= ' <tr class="mytableentry1"> <td colspan="5"><!--<a onclick="'.$onclick.'" href="'.$newlink.'" class="CommonTextButtonSmall"><span>+Add new entry</span></a> !--></td> </tr> ';
// 		$content .= '</tr></td></table></table>';
// 		
// 		if ($_MY_CONFIG->languageCompat == 1){
// 			// for language encoding issues
// 			require_once ($cms->get_path('root')."/includes/pageNavigation.php");
// 			$queryString = $_SERVER['QUERY_STRING'];
// 			$queryString = preg_replace("/\&limit=[0-9]*/i", "", $queryString);
// 			$queryString = preg_replace("/\&limitstart=[0-9]*/i", "", $queryString);
// 			$pageNavLink = $_SERVER['REQUEST_URI'];
// 			$pageNavLink = preg_replace("/\&limit=[0-9]*/i", "", $pageNavLink);
// 			$pageNavLink = preg_replace("/\&limitstart=[0-9]*/i", "", $pageNavLink);
// 			$pageNav = new mosPageNav($count, $limitstart, $limit);
// 			$content .= '<div class="my-pagenav">'.$pageNav->writePagesLinks('index2.php?'.$queryString).'</div>';
// 		} else {
// 			// Display AJAXED pagination buttons
// 			$content .= "<p align=\"center\">".writeAjaxPageNav("myblog", "viewEntries", "$limit", "$limitstart", "$count", "loading_status", "$mainframe->getCfg('live_site')/components/com_myblog/images/ajax_loader.gif")."</p>";
// 		}
// 		echo 'eddy';
// 		return $content;
// 	}
	
	
	/**
	 *	Process and send trackbacks for a specific content
	 */	 	
	function _trackback_send($uid, $trackbacks) {
		global  $mainframe, $Itemid, $sectionid, $sections;
		$cms    =& cmsInstance('CMSCore');
		$db		=& cmsInstance('CMSDb');
		include_once ($cms->get_path('root').'/components/com_myblog/libraries/trackback/trackback_cls.php');
		
		$db->query("SELECT c.*,p.permalink,u.name FROM #__content as c,#__myblog_permalinks as p,#__users as u WHERE c.id=p.contentid and c.id=$uid and c.created_by=u.id and c.sectionid IN ($sections)");
		$rows = $db->get_object_list();
		$row = $rows[0];
		
		if ($Itemid==0){
			// try get itemid from db.
			$Itemid = $db->get_value("select id from #__menu WHERE link='index.php?option=com_myblog' and published='1'");
			if (!$Itemid)
				$Itemid = 99999999;
		}
		
		
		if ($trackbacks and $trackbacks != "" and $row) {
			$trackback_arr = explode(" ", $trackbacks);
			
			// Process and send all trackbacks defined by user for this entry
			foreach ($trackback_arr as $url) {
				$db->query("SELECT count(*) FROM `#__myblog_tb_sent` WHERE `url`='$url' and `contentid`='$row->id'");
				
				// If trackback has not been sent before, send it
				if ($db->get_value() == 0) {
					$trackback = new Trackback($mainframe->getCfg('sitename'), $row->name, 'UTF-8');
					$trackback_url = trim(strip_tags($url));
					$cms->load('helper', 'url');
					$content_url = cmsSefAmpReplace("index.php?option=com_myblog&show=$row->permalink&Itemid=$Itemid");
					$content_title = $trackback->cut_short($row->title);
					$content_excerpt = $trackback->cut_short($row->fulltext);
					$content_id = $row->id;
					//return "$trackback_url $content_url $content_title $content_excerpt";
					
					// Send trackback ping
					if ($trackback->ping($trackback_url, $content_url, $content_title, $content_excerpt)) {
						$db->query("INSERT INTO `#__myblog_tb_sent` SET `url`='$trackback_url',`contentid`='$content_id'");
					}
				} else
				{
					return "error sending tb";
				}
			}
		} else {
			return "error sending TB";
		}
	}
	
	
	/**
	 *	Ping Technorati for an author's blog
	 */	 	
	function _pingTechnorati($author) {
		global $mainframe;
		
		$cms =& cmsInstance('CMSCore');
		$cms->load('helper', 'url');
		
		$errstr = "";
		$errno = "";
		$blogname = "$author's Blog Entries";
		$siteurl = cmsSefAmpReplace('index.php?option=com_myblog&blogger='.$author);
		$message = xmlrpc_encode_request("weblogUpdates.ping", array (
			$blogname,
			$siteurl
		));
		$tb_sock = @fsockopen('rpc.technorati.com', 80, $errno, $errstr, 15);
		
		if (!is_resource($tb_sock)) {
			//echo 'cannot connect to rpc.technorati.com';
			return;
		}
		
		// Avoid stalling for too long if connection cannot be established.
		socket_set_timeout($tb_sock, 15);
		fputs($tb_sock, "POST /rpc/ping HTTP/1.0\r\n");
		fputs($tb_sock, "User-Agent:".$_SERVER['HTTP_USER_AGENT']."\r\n");
		fputs($tb_sock, "Host:rpc.technorati.com\r\n");
		fputs($tb_sock, "Content-type:text/xml\r\n");
		fputs($tb_sock, "Content-length:".strlen($message)."\r\n");
		fputs($tb_sock, "Connection:close\r\n\r\n");
		fputs($tb_sock, $message);
		
		$response = "";
		$response = fread ($tb_sock, 8192); // Grab response from Technorati
		
		fclose($tb_sock);
		
		
		// Send technorati ping for site blog entries as well.
		$blogname2 = $mainframe->getCfg('sitename');
		$siteurl2 = cmsSefAmpReplace('index.php?option=com_myblog');
		$message2 = xmlrpc_encode_request("weblogUpdates.ping", array (
			$blogname2,
			$siteurl2
		));
		$tb_sock2 = @fsockopen('rpc.technorati.com', 80, $errno, $errstr, 15);
		
		if (!is_resource($tb_sock2)) {
			//echo 'cannot connect to rpc.technorati.com';
			return;
		}
		
		// Avoid stalling for too long if connection cannot be established.
		socket_set_timeout($tb_sock2, 15);
		fputs($tb_sock2, "POST /rpc/ping HTTP/1.0\r\n");
		fputs($tb_sock2, "User-Agent:".$_SERVER['HTTP_USER_AGENT']."\r\n");
		fputs($tb_sock2, "Host:rpc.technorati.com\r\n");
		fputs($tb_sock2, "Content-type:text/xml\r\n");
		fputs($tb_sock2, "Content-length:".strlen($message2)."\r\n");
		fputs($tb_sock2, "Connection:close\r\n\r\n");
		fputs($tb_sock2, $message2);
		
		$response2 = "";
		$response2 = fread ($tb_sock2, 8192); // Grab response from Technorati
		
		fclose($tb_sock2);
		
		return "<!--$siteurl ping,technorati response:$response-->";
	}
	
	// Show admin home
	function adminhome(){
		include_once(MY_TASK_PATH .'/adminhome.php');
	}
	
	/**
	 * Show a list of all comments on this blogger's article
	 */	 	
	function showcomments(){
		include_once(MY_TASK_PATH .'/showcomments.php');
	}
	
	/**
	 * Display all the media (images/video) uploaded by the user
	 */	 	
	function media(){
		include_once(MY_TASK_PATH .'/media.php');
	}
	
	/**
	 * Show current blogger's preferences/profile
	 */	 	
	function bloggerpref(){
		include_once(MY_TASK_PATH .'/bloggerpref.php');
	}
	
	/**
	 * Show the blogger statistics page
	 */	 	
	function bloggerstats(){
		include_once(MY_TASK_PATH .'/bloggerstats.php');
	}
	
	// delete the given entry
	function delete(){
		global  $my;
		
		$cms =& cmsInstance('CMSCore');
		$cms->load('helper','url');
		
		// @todo: limit delete action to your own content only
		$id = cmsGetVar('id', 0, 'GET');
				
		$this->cms->db->query("DELETE FROM #__content WHERE id=$id");
		$this->cms->db->query("DELETE FROM #__myblog_permalinks WHERE contentid=$id");
		$this->cms->db->query("DELETE FROM #__myblog_images WHERE contentid=$id");
		$this->cms->db->query("DELETE FROM #__myblog_content_categories WHERE contentid=$id");
		
		// Need to include paging info if necessary, the paging info will come from
		$redirect = 'index.php?option=com_myblog&task=adminhome&Itemid='. myGetItemId();
		$redirect = cmsSefAmpReplace($redirect, false);
		cmsRedirect($redirect, 'Blog entry deleted.');
	}
	
	// Save the user blog description
	function save_desc(){
		global  $my;

		$cms =& cmsInstance('CMSCore');
		$db  =& cmsInstance('CMSDb');
		$cms->load('libraries', 'user');
		
		$row = new myblogUser();
		$statusMsg = "";
		
		// If user does not have a blog, create one.
		if (!$row->load($cms->user->id)) {
			$db->query("INSERT INTO #__myblog_user SET user_id={$cms->user->id},description='Write something to describe your blog' ");
			$row->load($cms->user->id);
		}
		
		// if saving blog description
		if (isset ($_POST['user_id'])) {
			$row->bind($_POST);
			$row->user_id = $cms->user->id;
			$row->store();
			$statusMsg = $MYBLOG_LANG['_MB_BLOG_DESC_UPDATED'];
		}
		
		header("Refresh:0;url=index.php?option=com_myblog&task=adminhome&Itemid=1");
	}
	


	function write(){
		include_once(MY_TASK_PATH .'/write.php');
	}
}

