<?php
(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

require_once (MY_FRONTADMIN_PATH . "/frontadmin.class.php");

global $frontadmin, $mainframe;
$task = cmsGetVar('task', 'my_show_admin_toolbar', 'REQUEST');
$id = isset ($_GET['id']) ? intval($_GET['id']) : 0;

global $MYBLOG_LANG, $_MYBLOG,$_MY_CONFIG;
$cms =& cmsInstance('CMSCore');
if (!class_exists('JAX'))
	include_once($cms->get_path('plugins') .'/system/pc_includes/ajax.php');
$jax = new JAX($cms->get_path('plugin-live') . "/system/pc_includes");
$jax->setReqURI($cms->get_path('live') . "/index2.php");
$jax->process();

global $Itemid;

$cms =& cmsInstance('CMSCore');
$cms->load('libraries', 'user');
$cms->load('helper', 'url');

$db =& cmsInstance('CMSDb');

# if Itemid is 0, need to autodetect itemid
if ($Itemid==0){
	# first detect userblog itemid
	$db->query("select id from #__menu where link LIKE 'index.php?option=com_myblog%task=userblog%' and published='1'");
	$myItemid = $db->get_value();
	
	if (!$myItemid) {
		# then detect main myblog component itemid if userblog link DNE
		$db->query("select id from #__menu where type='components' and link='index.php?option=com_myblog' and published='1'");
		$myItemid = $db->get_value();
	}
	
	if ($myItemid)
		$Itemid=$myItemid;
}

# Displays login form for a user if login form enabled and user is not logged in while viewing dashboard
# - useLoginForm is removed as we require a login form.
if (!$cms->user->id){
	if (cmsVersion() == _CMS_JOOMLA10 || cmsVersion() == _CMS_MAMBO) {
		global $_VERSION;
		if ($_VERSION->getShortVersion() >= '1.0.10')
			$validate = josSpoofValue(1);
?>
<form action="<?php echo cmsSefAmpReplace('index.php');?>" method="POST" name="login">
<?php echo "<strong>".$MYBLOG_LANG['_MB_NOT_LOGGEDIN']."</strong>";?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td>
			<label for="mod_login_username"><?php echo _USERNAME; ?></label>
			<br />
			<input name="username" id="mod_login_username" type="text" class="inputbox" alt="username" size="10" />
			<br />
			<label for="mod_login_password"><?php echo _PASSWORD; ?></label>
			<br />
			<input type="password" id="mod_login_password" name="passwd" class="inputbox" size="10" alt="password" />
			<br />
			<input type="checkbox" name="remember" id="mod_login_remember" class="inputbox" value="yes" alt="Remember Me" />
			<label for="mod_login_remember">
				<?php echo _REMEMBER_ME; ?>
			</label>
			<br />
			<input type="submit" name="Submit" class="button" value="<?php echo _BUTTON_LOGIN; ?>" />
		</td>
	</tr>
	<tr>
		<td>
			<a href="<?php echo cmsSefAmpReplace('index.php?option=com_registration&amp;task=lostPassword' ); ?>"><?php echo _LOST_PASSWORD; ?></a>
		</td>
	</tr>
</table>
<input type="hidden" name="option" value="login" />
<input type="hidden" name="op2" value="login" />
<input type="hidden" name="lang" value="<?php echo $mainframe->getCfg('lang'); ?>" />
<input type="hidden" name="return" value="<?php echo $_SERVER['REQUEST_URI']; ?>" />
<input type="hidden" name="message" value="" />
<input type="hidden" name="force_session" value="1" />
<input type="hidden" name="<?php echo $validate; ?>" value="1" />
</form>
<?php
}elseif (cmsVersion() == _CMS_JOOMLA15){
	$dbLink	= base64_encode(rtrim($cms->get_path('live') ,'/') . '/index2.php?option=com_myblog&task=write&keepThis=true&TB_iframe=true&no_html=1&id=0');
?>
<form action="<?php echo $cms->get_path('live') . '/index.php';?>" method="POST" name="login" id="form-login" >
	<fieldset class="input">
	<p id="form-login-username">
		<label for="modlgn_username"><?php echo JText::_('Username') ?></label><br />
		<input id="modlgn_username" type="text" name="username" class="inputbox" alt="username" size="18" />
	</p>
	<p id="form-login-password">
		<label for="modlgn_passwd"><?php echo JText::_('Password') ?></label><br />
		<input id="modlgn_passwd" type="password" name="passwd" class="inputbox" size="18" alt="password" />
	</p>
	<?php if(JPluginHelper::isEnabled('system', 'remember')) : ?>
	<p id="form-login-remember">
		<label for="modlgn_remember"><?php echo JText::_('Remember me') ?></label>
		<input id="modlgn_remember" type="checkbox" name="remember" class="inputbox" value="yes" alt="Remember Me" />
	</p>
	<?php endif; ?>
		<input type="submit" name="Submit" class="button" value="<?php echo JText::_('LOGIN') ?>" />
		</fieldset>
	<ul>
		<li><a href="<?php echo JRoute::_( 'index.php?option=com_user&view=reset' ); ?>"><?php echo JText::_('FORGOT_YOUR_PASSWORD'); ?></a></li>
	</ul>
	<input type="hidden" name="option" value="com_user" />
	<input type="hidden" name="task" value="login" />
	<input type="hidden" name="return" value="<?php echo $dbLink;?>" />

	<?php echo JHTML::_( 'form.token' ); ?>
</form>
<?php
}

} else {
	// User is already authenticated
	$mb_admin_controller = new MB_Admin_Controller();
	$mb_admin_controller->init();
	$task = cmsGetVar('task', '', 'GET');
	
	if(method_exists($mb_admin_controller, $task)){
		$mb_admin_controller->$task();	
	} 

}
