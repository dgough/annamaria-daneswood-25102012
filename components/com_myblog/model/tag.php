<?php

(defined('_VALID_MOS') OR defined('_JEXEC')) or 
	die('Direct Access to this location is not allowed.');


class MYTag extends CMSDBTable {
	var $id = 0;
	var $name;
	var $slug;
	var $default;
	
	function MYTag(){
		$this->CMSDBTable( '#__myblog_categories','id');
	}
	
	// a tag might be loaded via id, name or the slug, in that order
	function load($tag){
		
		if(is_numeric($tag)){
			parent::load($tag);
		} else {
			// search via the name 
			
			// search their slug
		}
	}
	
	// Store the tag in db
	function store(){
		// If the slug is empty, create it
		if(empty($this->slug)){
			$this->getSlug();
		}
		parent::store();
	}
	
	// Return the tag slug. If it is empty, create one and return the correct
	// one
	function getSlug(){
		if(empty($this->slug)){
			$this->slug = $this->_prepSlug($this->getName());
			
			// If this is existing slug, we need to update the database
			if($this->id != 0){
				//$this->store();
				parent::store();
			}
		}
		
		return $this->slug;
	}
	
	
	// Return slug name
	function getName(){
		return $this->name;
	}
	
	// Set the new slug name, make sure no duplicates
	// return false if it fails
	function setSlug($slug){
		$cms	=& cmsInstance('CMSCore');
		$newslug = $this->_prepSlug($slug);
		
		if($newslug == '')
			return false;
		
		$strSQL	= "SELECT COUNT(*) FROM `#__myblog_categories` WHERE `slug`='{$newslug}'";
		$cms->db->query($strSQL);
		
		if($cms->db->get_value() <= 0){
			$this->slug = $newslug;
			return true;
		}
		
		return false;
	}	
	
	// Set the tag name,
	// return true if succeed
	function setName($name){
		$cms	=& cmsInstance('CMSCore');
		
		$strSQL	= "SELECT COUNT(*) FROM `#__myblog_categories` WHERE `name`='{$name}'";
		$cms->db->query($strSQL);
		
		if($cms->db->get_value() <= 0){
			$strSQL	= "UPDATE #__myblog_categories SET `name`='{$name}' WHERE `id`='{$this->id}'";
			$cms->db->query($strSQL);
			$this->name = $name;
					
			return true;
		}
		return false;
	}
	
	/**
	 * Set the tag as the default tag
	 **/	 	
	function setDefault($status = '1'){
		$this->default = $status;
		return true;
	}
	
	// Prep the given slug. If slug is not yet available, use the tagname,
	// static 
	function _prepSlug($newtag){
	
		$db	=& cmsInstance('CMSDb');

		# Remove unwanted characters
		$newtag = preg_replace('/[`~!@#$%\^&*\(\)\+=\{\}\[\]|\\<">,\\/\^\*;:\?\'\\\]/', '', $newtag);

		if(cmsVersion() != _CMS_JOOMLA15)
		{
			# UTF8 to ISO 8859
			$newtag = preg_replace("/([\xC2\xC3])([\x80-\xBF])/e", "chr(ord('\\1')<<6&0xC0|ord('\\2')&0x3F)", $newtag);
			$newtag = trim($newtag);
		}
		# ISO 8859 to UTF8
		// Check if page is already UTF-8
// 		if(is_array($iso) && $iso[1] != 'UTF-8' && (cmsVersion() != _CMS_JOOMLA15))
// 			$newtag = preg_replace("/([\x80-\xFF])/e", "chr(0xC0|ord('\\1')>>6).chr(0x80|ord('\\1')&0x3F)", $newtag);
		
		return $newtag;
	}
}

?>
