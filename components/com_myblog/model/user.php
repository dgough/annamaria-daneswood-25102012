<?php

(defined('_VALID_MOS') OR defined('_JEXEC')) or 
	die('Direct Access to this location is not allowed.');

class myblogUser extends CMSDBTable { 
	var $user_id =null;
	var $description = "A short description about your blog";
	var $title		= 'A title about your blog';
	var $feedburner	= '';
	var $style =null;
	var $param =null;
	var $userStyle = null;
	
	function myblogUser() { 
		$this->CMSDBTable('#__myblog_user','user_id');
	}
	
	// Return the customized css styling for each user. Stored in
	// $userStyle array
	function getStyle($style){
		if(isset($this->userStyle[$style])){
			return $this->userStyle[$style];
		} else 
			return '';
	}
	
	function load($id){
		$cms =& cmsInstance('CMSCore');
		
		if($id != 0){
			$user_id = $cms->db->query("SELECT `user_id` FROM #__myblog_user WHERE `user_id`='{$id}'");
			$user_id = $cms->db->get_value();
			
			if(!$user_id){
				// need to create a default entry
				$data = array('user_id' => $id,
								'description' => "A short description about your blog");
				$cms->db->insert('#__myblog_user', $data);
			}
		}
		// Check if the user exist, if not, load up with default values
		parent::load($id);
		
		// Load userstyle
		if(!empty($this->style)){
			$this->userStyle = unserialize($this->style);
		}
		
//		$this->userStyle['headerImg'] = 'http://www.pearsonified.com/wp-content/themes/Pearsonified%203.1/images/banner_5.jpg';
// 		$this->userStyle['blog-title-color'] = "#FFFFFF";
// 		$this->userStyle['blog-subtitle-color'] = "#FFFFFF"; 
		
	} 
} 
