<?php
/**
 * @copyright (C) 2007 by Slashes & Dots Sdn Bhd - All rights reserved!
 * @license http://www.azrul.com Copyrighted Commercial Software
 *
 * Rem:
 *
 * 
 **/
(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

// Include our custom cmslib if its not defined
if(!defined('CMSLIB_DEFINED'))
	include_once ((dirname((dirname(dirname(dirname(__FILE__)))))) . '/components/libraries/cmslib/spframework.php');

class MYPluginsDB{
	var $table	= '#__myblog_mambots';
	var $key	= '';
	var $db		= null;
	
	var $_plugins	= '';
	function MYPluginsDB(){
		$this->db		=& cmsInstance('CMSDb');
		$this->_plugins	= cmsVersion() == _CMS_JOOMLA10 ? '#__mambots' : '#__plugins';
	}
	
	function getPlugins($type = 'content', $published = true){
	
		if($type == 'content')
			$type	= "AND a.folder='content' ";
		else
			$type	= "AND a.folder='{$type}' ";

		// Check if only get published mambots
		if($published)
			$published	= "AND b.my_published='1' ";

		$strSQL	= "SELECT a.element, a.ordering "
				. "FROM {$this->_plugins} AS a, {$this->table} AS b "
				. "WHERE b.mambot_id=a.id "
				. "AND a.published='1' "
				. $published
				. $type
				. "AND a.element !='jom_comment_bot' "
				. "ORDER BY a.ordering";

		$this->db->query($strSQL);
		return $this->db->get_object_list();
	}
	
	function get($limitstart , $limit){
		$strSQL	= "SELECT a.name, b.mambot_id, b.my_published "
				. "FROM {$this->_plugins} AS a, {$this->table} AS b "
				. "WHERE b.mambot_id=a.id "
				. "AND a.published='1' "
				. "AND a.folder='content' "
				. "AND a.element!='jom_comment_bot' "
				. "LIMIT {$limitstart}, {$limit}";
		$this->db->query($strSQL);
		return $this->db->get_object_list();
	}

	function initPlugins($type = 'content'){
	
		if($type == 'content')
			$type	= "AND a.folder='content' ";
		else
			$type	= "AND a.folder='{$type}' ";

		$strSQL	= "SELECT a.name, a.id FROM {$this->_plugins} AS a "
				. "LEFT OUTER JOIN {$this->table} AS b "
				. "ON (a.id=b.mambot_id) "
				. "WHERE b.mambot_id IS NULL "
				. $type
				. "AND a.published='1' "
				. "AND a.element!='jom_comment_bot'";

		$this->db->query($strSQL);
		$plugins	= $this->db->get_object_list();
		
		// Debug Only
		//echo '<pre>'; var_dump($plugins); echo '</pre>';
		// Insert any plugins that is found to myblog bots.
		if($plugins){
			foreach($plugins as $plugin){
				$strSQL	= "INSERT INTO {$this->table} SET mambot_id='{$plugin->id}'";
				$this->db->query($strSQL);
			}
		}
	}
}
