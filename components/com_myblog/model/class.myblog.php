<?php 
(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

DEFINE("MB_SECTION",8);
DEFINE("MB_CATEGORY",18);

class blogContent extends CMSDBTable { 
	var $id =0;
	var $title =null;
	var $title_alias =null;
	var $introtext =null;
	var $images =null;
	var $fulltext =null;
	var $state =null;
	var $sectionid =null;
	var $mask =null;
	var $catid =null;
	var $created =null;
	var $created_by =null;
	var $created_by_alias=null;	
	var $modified =null;	
	var $modified_by =null;	
	var $checked_out =null;	
	var $checked_out_time=null;	
	var $publish_up =null;	
	var $publish_down =null;	
	var $urls =null;	
	var $attribs =null;	
	var $version =null;	
	var $parentid =null;	
	var $ordering =null;	
	var $metakey =null;	
	var $metadesc =null;	
	var $access =null;
	var $hits =null;	
	
	/* Custom params */
	var $permalink = null;
	var $tags	= null; // string, saperated by comma
	var $tagobj	= null;	// array, obj(id, name) of the tags
	var $rating = null;
	var $rating_count	= null;
	

	function blogContent() { 
		//$this->created=date( 'Y-m-d H:i:s' );
		$this->CMSDBTable( '#__content','id');
	
	}
	
	function autoCloseTags($string) {
		// automatically close HTML-Tags
		// (usefull e.g. if you want to extract part of a blog entry or news as preview/teaser)
		// coded by Constantin Gross <connum at googlemail dot com> / 3rd of June, 2006
		// feel free to leave comments or to improve this function!
		$donotclose=array('br','img','input'); //Tags that are not to be closed
		//prepare vars and arrays
		$tagstoclose='';
		$tags=array();
		//put all opened tags into an array
		preg_match_all("/<(([A-Z]|[a-z]).*)(( )|(>))/isU",$string,$result);
		$openedtags=$result[1];
		$openedtags=array_reverse($openedtags); //this is just done so that the order of the closed tags in the end will be better
		//put all closed tags into an array
		preg_match_all("/<\/(([A-Z]|[a-z]).*)(( )|(>))/isU",$string,$result2);
		$closedtags=$result2[1];
		//look up which tags still have to be closed and put them in an array
		for ($i=0;$i<count($openedtags);$i++) {
			if (in_array($openedtags[$i],$closedtags)) { unset($closedtags[array_search($openedtags[$i],$closedtags)]); }
			else array_push($tags, $openedtags[$i]);
		}
		$tags=array_reverse($tags); //now this reversion is done again for a better order of close-tags
		//prepare the close-tags for output
		for($x=0;$x<count($tags);$x++) {
			$add=strtolower(trim($tags[$x]));
			if(!in_array($add,$donotclose)) $tagstoclose.='</'.$add.'>';
		}
		//and finally
		return $tagstoclose;
	}

	function getParagraphCount($text)
	{
		$position = -1;
		$count	  = 0;

		while( ( $position = strpos($text , '</p>' , $position + 1) ) !== false )
		{
			$count++;
		}
			
		return $count;
	}
	
	function getBrowseText(&$row){
		global $_MY_CONFIG;
		
		if($_MY_CONFIG->get('useIntrotext')){
			if(empty($row->fulltext)){
				$ending = strpos($row->introtext, '</p>');
				
				$pos=-1;
				$pos_array = array();
				while (($pos=strpos($row->introtext,'</p>',$pos+1))!==false) 
					$pos_array[]=$pos;
				
				$pNum = $_MY_CONFIG->get('autoReadmorePCount');
				if (count($pos_array) <= $pNum) {
				   $row->text = $row->introtext;
				} else {
					$ending = $pos_array[$pNum-1];
					$row->introtext = substr($row->introtext, 0, $ending + 4);
					$row->introtext = myCloseTags(preg_replace('#\s*<[^>]+>?\s*$#','',$row->introtext));
				}
			}
			else if( !empty($row->fulltext) && empty($row->introtext) )
			{
				// Strip x paragraphs
				
				$ending = strpos($row->fulltext, '</p>');
				
				$pos=-1;
				$pos_array = array();
				while (($pos=strpos($row->fulltext,'</p>',$pos+1))!==false) 
				$pos_array[]=$pos;
				
				$pNum = $_MY_CONFIG->get('autoReadmorePCount');
				if (count($pos_array) <= $pNum) {
					$row->text = $row->fulltext;
				} else {
					$ending = $pos_array[$pNum-1];
					$row->fulltext = substr($row->fulltext, 0, $ending + 4);
					$row->fulltext = myCloseTags(preg_replace('#\s*<[^>]+>?\s*$#','',$row->fulltext));
				}
			}
			
			// If user set to display introtext but introtext might be empty
			// due to the way previous version of My Blog stores the entries.
			if( empty($row->introtext) )
			{
				$row->text = $row->fulltext;
			}
			else
			{
				$row->text = $row->introtext;
			}
		} else{
			if(empty($row->introtext))
				$row->text = $row->fulltext;
			else
				$row->text = $row->introtext;	
		}
		
		
// 		// Set the text content if introtext available, display introtext. Otherwise display fulltext
// 		if ($row->introtext && trim($row->introtext) != "" && $_MY_CONFIG->get('useIntrotext')=="1"){
// 			$row->text = myCloseTags(preg_replace('#\s*<[^>]+>?\s*$#','',$row->introtext));
// 		} else {
// 			// Split the default intro text length if use intro text is set
// 			// and no introtext is present
// 			if($_MY_CONFIG->get('introLength') && $_MY_CONFIG->get('useIntrotext') == "1" && trim($row->introtext) == ""){
// 				//$row->text = $this->cms->trunchtml->trunchtml($row->fulltext, $_MY_CONFIG->get('introLength'));
// 				
// 				
// 				$ending = strpos($row->fulltext, '</p>');
// 				
// 				if ($ending === false) {
// 				   $row->text = $row->fulltext;
// 				} else {
// 					echo '<h1>hello'.$row->title.$ending.'</h1>';
// 					
// 					$row->text = substr($row->fulltext, 0, $ending + 4);
// 					$row->text = myCloseTags(preg_replace('#\s*<[^>]+>?\s*$#','',$row->text));
// 				}
// 	
// 			
// 			}else{
// 				// If introlength is not set or useIntrotext is disabled then use entire text.
// 				$row->text = $row->fulltext;
// 			}
// 		}
// 		
// 		// If text is still empty, use the introtext
// 		if(empty($row->text)){
// 			$row->text = $row->introtext;
// 		}
		
		
	}
	
	function _splitReadmoreOnSave(){
		// During save, the text in the editor will be stored in $this->fulltext.
		// If readmore is detected, we split it up and place it in introtext / fulltext
		// If it doesn't exists just place it in introtext like the default Joomla.
		
		
		// we are assuming everything is now in fulltext
		$this->fulltext = preg_replace('/<p id="readmore">(.*?)<\\/p>/i', '{readmore}', $this->fulltext);
		$pos = strpos($this->fulltext, '{readmore}');
		if ($pos === false) {
			$this->introtext = $this->fulltext;
			$this->fulltext = '';
		} 
		else
		{
			$this->introtext = substr($this->fulltext, 0, $pos);
			$this->fulltext  = substr($this->fulltext, $pos + 10);
		} 
	}
	
	// We can load the row using either numeric id or a permalink string
	function load($id){
		
		$originalid = $id;
		$db =& cmsInstance('CMSDb');
		
		if(is_numeric($id)){
			parent::load($id);
		} else {
			$sql = "SELECT contentid FROM #__myblog_permalinks WHERE permalink='{$id}'";
			$db->query($sql);
			$id = $db->get_value();
			
			// IF we cannot find it, need to try and convert ':' to '-'. Joomla 1.5
			// seems to convert this
			if(!$id){
				$id = str_replace(':', '-', $originalid);
				$sql = "SELECT contentid FROM #__myblog_permalinks WHERE permalink='{$id}'";
				
				$db->query($sql);
				$id = $db->get_value();
			}
			parent::load($id);
		}
		
		// get the permalink
		if(is_numeric($id) && ($id != 0)){
			$db->query("SELECT `permalink` FROM #__myblog_permalinks WHERE `contentid`='{$id}'");
			$this->permalink = $db->get_value();
		}
		
		// if the fulltext contain the '{readmore}', that means this is the old data and we need to clean them up a bit
		// $this->_split_fulltext_readmore();
		// @gotcha. if fulltext contain readmore, and {introtext}
		$pos = strpos($this->fulltext, '{readmore}');
		
		if ($pos === false) {
		} else {
			$this->introtext .= substr($this->fulltext, 0, $pos);
			$this->fulltext  = substr($this->fulltext, $pos + 10);
		}
		
		// We store all the text in the introtext if no {readmore} is present. 
		// Otherwise it is stored in introtext
		// and fulltext appropriately.
 		if(!empty($this->fulltext) && empty($this->introtext)){
 			$this->introtext = $this->fulltext;
 			$this->fulltext = '';
 		}
// 			// If introtext is empty, we assume all has been stored in fulltext section
// 			if(!empty($this->introtext)){
// 			//	$this->fulltext	= $this->fulltext;
// 			//else
// 				$this->fulltext	= $this->introtext . '{readmore}' . $this->fulltext;
// 				echo "<h2>Readmore in fulltext</h2>";
// 			}
// 		}
//		else
//			$this->fulltext	= $this->introtext; 

		// Load the tags into a string
		$sql = "SELECT `tag`.`id` ,`tag`.`name` "
			." \n FROM #__myblog_categories as tag ,  #__myblog_content_categories as c "
			." \n WHERE tag.`id` = c.category AND c.contentid='{$this->id}'";
		$db->query($sql);
		$this->tagobj = $db->get_object_list();
		$tags = array();
		if($this->tagobj){
			foreach($this->tagobj as $tag){
				$tags[] = $tag->name;
			}
		}
		$this->tags = implode(',', $tags);
		
		# Get the rating
		$db->query("SELECT *, round( rating_sum / rating_count ) AS rating FROM #__content_rating WHERE content_id='{$this->id}'");
		$rating = $db->first_row();
		if($rating){
			$this->rating = $rating->rating;
			$this->rating_count = $rating->rating_count;
		}
		
		$cms        =& cmsInstance('CMSCore');
		//Change all relative url to absolute url
		$this->introtext = str_replace('src="images', 'src="'. $cms->get_path('live') .'/images', $this->introtext);
		$this->fulltext  = str_replace('src="images', 'src="'. $cms->get_path('live') .'/images',  $this->fulltext);
		
		// Convert back to htmlentities
		$this->title        = htmlspecialchars($this->title);
		
		// make sure no bloody {readmore} tag ever
		$this->introtext = str_replace('{readmore}', '', $this->introtext);
		$this->fulltext = str_replace('{readmore}', '', $this->fulltext);

		# Trim all necessary text
		$this->introtext = trim($this->introtext);
		$this->fulltext = trim($this->fulltext);
		$this->permalink = trim($this->permalink);
		
		
	}
	
	function bind($vars, $editorSave=false){
		if(empty($vars))
			return;
			
		parent::bind($vars);

		// if saving from editor, everything is in the fulltext, need to split it
		if($editorSave && empty($vars['introtext'])){
			$this->_splitReadmoreOnSave();
		}
	}
	
	function store(){
		global $_MY_CONFIG, $mainframe;
		$temp = $this->permalink;
		$tags = $this->tags;
		
		$cms =& cmsInstance('CMSCore');
		$cms->load('libraries', 'user');
		$db	=& cmsInstance('CMSDb');
		
		unset($this->permalink);
		unset($this->tags);
		unset($this->rating);
		unset($this->rating_count);
		
		
		if(empty($this->publish_up)){
			$this->publish_up = $this->created;
		}
		
		if(empty($this->sectionid)){
			$this->sectionid = $_MY_CONFIG->get('postSection');
		}
		
		if(empty($this->catid)){
			$this->catid = $_MY_CONFIG->get('catid');
		}
		
		if(empty($this->created_by)){
			$this->created_by = $cms->user->id;
		}
		
		if($this->id != NULL && $this->id != 0){
			$this->modified = strftime("%Y-%m-%d %H:%M:%S", time() + ( $mainframe->getCfg('offset') * 60 * 60 ));	
		}
		
		//$this->_splitReadMoreOnSave();
		
		// Decode back to normal
		$this->title    = html_entity_decode($this->title);
		//$this->title = htmlspecialchars($this->title);
		
		parent::store();
		
		
		// restore custom fields
		$this->permalink = $temp;
		$this->tags =  $tags;
		
		// If the permalink is empty, we need to create them or update if necessary
		if(empty($this->permalink)){
			$this->permalink    = myGetPermalink($this->id);
		}
		
		if($this->id != 0){
			$sql = "SELECT COUNT(*) FROM `#__myblog_permalinks` WHERE `contentid`='{$this->id}'";
			$db->query($sql);
			$permalink_exist = $db->get_value();
			
			if($permalink_exist){
				$db->query("UPDATE `#__myblog_permalinks` SET `permalink`= '{$this->permalink}' WHERE `contentid`='{$this->id}'");
			} else {
				$db->query("INSERT INTO `#__myblog_permalinks` (`contentid`, `permalink`) VALUES ({$this->id}, '{$this->permalink}') ");
			}
		}
		
		// delete all previous tags
//		$db->query("DELETE FROM #__myblog_content_categories WHERE contentid='{$this->id}'");
		
// 		// Update the tags
// 		if($this->id != 0 AND !empty($this->tags)){
// 			myAddTags($this->id, $this->tags);
// // 			foreach($this->tags as $tag){
// // 				$insert[]	= '(' . $this->id . ',' . $tag
// // 			}
// // 			$tagsids_insert= array();
// // 			foreach($this->tags as $row){
// // 				$tagsids_insert[] = "({$this->id}, {$row})";
// // 			}
// // 			
// // 			$sql = "INSERT INTO `#__myblog_content_categories` "
// // 				."\n (`contentid` ,`category`) VALUES "
// // 				. implode(', ', $tagsids_insert);
// // 			
// // 			$db->query($sql);
// 		}
	}
	
	/**
	 * Return an array of strings with all the validation error of the given entry.	  	
	 * The data given will be blogContent object.
	 * 
	 * If no error is found, return an empty array	 	 
	 */	 	
	function validate_save(){
		$validate = array();
		
		# Title cannot be empty
		if(empty($this->title)){
			$validate[] = "Title is empty"; 
		} 
		
		# Fulltext area cannot be empty
		if(empty($this->fulltext)){
			$validate[] = "You cannot save a blank entry. "; 
		}
		
		# Check if permalink contains any unallowed characters and no duplicate is allowed
		if (preg_match('/[!@#$%\^&*\(\)\+=\{\}\[\]|\\<">,\\/\^\*;:\?\']/', $this->permalink)) {
			$validate[] = "Permanent link can only contain ASCII alphanumeric characters and.-_ only";
		} else {
			$db->query("SELECT count(*) from #__myblog_permalinks WHERE permalink='{$this->permalink}' and contentid!={$this->id}");
			
			if ($db->get_value() and $this->permalink != "") {
				$validate[] = "Permanent link has already been taken. Please choose a different permanent link.";
			} 
		}
		
		return $validate;
	}
} 

class MyBlogImages extends CMSDBTable { 
	var $id =null;
	var $filename =null;
	var $contentid =null;
	var $user_id=null;
	
	function MyBlogImages( &$db ) {
		$this->CMSDBTable( '#__myblog_images','id');
	} 
} 

class MyBlogMainFrame { var $_language =null;
	var $_access=true;	
	var $_functions=null;
	
	function MyBlogMainFrame(){ 
		global $_MY_CONFIG;
		$cms    =& cmsInstance('CMSCore');
		$cms->load('libraries', 'user');

		if(!$cms->user->id){ 
			$this->_access=false;
		} 
		else 
		{ 
			if($_MY_CONFIG->get('allowedUser'))
			{ 
				$allowedUser=explode(",",$_MY_CONFIG->get('allowedUser'));
				if(!in_array($cms->user->username,$allowedUser)){ 
					$this->$_access=false;
				} 
			} 
		} 
		$this->_functions=array();
	} 
	
	function validAccess(){ 
		return $this->_access;
		return true;
	} 

	function loadAzVideoBot(){
		global $_MAMBOTS;

		if(myGetAzVideoBot()){
			// Call mambot?
			$cms    	=& cmsInstance('CMSCore');
			include_once($cms->get_path('plugins') . '/content/azvideobot.php');

			$this->registerFunction('onPrepareContent','mb_videobot',"","1");
   		}
	}
	
	function loadPlugins(){ 
		global $_MAMBOTS;
		$cms    =& cmsInstance('CMSCore');
		$db 	=& cmsInstance('CMSDb');
		
		$pluginsDir=$cms->get_path('plugins'). '/content/';
		$db->query( "SELECT mambots.element,mambots.ordering FROM #__myblog_mambots as mybots, #__mambots as mambots WHERE mybots.mambot_id=mambots.id and mambots.published='1' and mybots.my_published='1' and mambots.folder='content' and mambots.element !='jom_comment_bot' ORDER BY mambots.ordering");	
		$rows = $db->get_object_list();
	
		if ($rows) { 
			foreach ($rows as $row) { 
			    // Instead of including the mambots, we allow the mainframe mambot to include for us.
			    // so that other module that has triggers would not come up with an error.
			    $row->folder 	= 'content';
			    $row->published = '1';
			    $row->params = null; // no params
			    
				@$_GET['task']  	= 'view';
				@$_GET['option']    = 'com_content';
				$_MAMBOTS->loadBot($row->folder, $row->element, $row->published, $row->params);

// 				$curr_file=$pluginsDir.$row->element.".php";
//
// 				if (file_exists($curr_file) and is_file($curr_file))
// 				{
// 					include_once($curr_file);
// 				}

			}
				
			$mambotsToInit=@$_MAMBOTS->_events['onPrepareContent'];
		
			if ($mambotsToInit) 
			{ 
				foreach ($mambotsToInit as $mambotToInit) 
				{ 
					$funcName=$mambotToInit[0];
		
					$this->registerFunction('onPrepareContent',$funcName,"","1");
				}
			}
			
			$mambotsToInit=@$_MAMBOTS->_events['onBeforeDisplayContent'];
		
			if ($mambotsToInit) 
			{ 
				foreach ($mambotsToInit as $mambotToInit) 
				{ 
					$funcName=$mambotToInit[0];
		
					$this->registerFunction('onBeforeDisplayContent',$funcName,"","1");
				}
			}
			
			$mambotsToInit=@$_MAMBOTS->_events['onAfterDisplayContent'];
		
			if ($mambotsToInit) 
			{ 
				foreach ($mambotsToInit as $mambotToInit) 
				{ 
					$funcName=$mambotToInit[0];
		
					$this->registerFunction('onAfterDisplayContent',$funcName,"","1");
				}
			}
		} 
		
		$pluginsDir= MY_COM_PATH.'/plugins';
		
		$db->query("SELECT filename,folder from #__myblog_bots");
		$rows = $db->get_object_list();
		
		if ($rows) { 
			foreach ($rows as $row) 
			{ 
				$curr_plugin_path=$pluginsDir."/".$row->folder."/".$row->filename.".php";
		
				if (is_file($curr_plugin_path)){ 
					include_once($curr_plugin_path);
				} 
			} 
		}
	} 
	
	function trigger($triggerPoint,&$row,&$params,$page="0"){
		
		$result = "";
		if(isset($this->_functions[$triggerPoint])){ 
			foreach($this->_functions[$triggerPoint] as $func){ 
				$funcName=$func[1];
				$published=$func[0];
				//echo "<!-- params: $funcName , $published, -->";
				//echo "<!-- rowtext: " . htmlspecialchars($row->text) . " -->\n\n";
				if ($triggerPoint == "onBeforeDisplayContent" or $triggerPoint == "onAfterDisplayContent"){
					if(function_exists($funcName))
					    $result .= @$funcName($row, $params, $page);
				}
				else{
					if(function_exists($funcName))
					    $result .= @$funcName($published, $row, $params, $page);
				}
				//echo "<!-- rowtext AFTER: " . htmlspecialchars($row->text) . " -->\n\n";
			} 
		}
		return $result;
	} 
	
	
	function registerFunction($triggerPoint,$funcName,$filename,$published=""){ 
		global $database;
		
		$db =& cmsInstance('CMSDb');
		
		if(!isset($this->_functions[$triggerPoint])) 
			$this->_functions[$triggerPoint]=array();
		
		$func=array();
		
		if ($published=="") 
		{ 
			$db->query("SELECT published from #__myblog_bots WHERE filename='$filename'");
			
			$published=false;
		
			if (!($row=$database->loadRow())) { 
				return;
			} else { 
				$published=$row[0];
		
				$func[0]=$published;
				
				$func[1]=$funcName;
				
				$this->_functions[$triggerPoint][]=$func;
				
			} 
		} else { 
			$func[0]=$published;
		
			$func[1]=$funcName;
			
			$this->_functions[$triggerPoint][]=$func;
			
		}
	}
} 

