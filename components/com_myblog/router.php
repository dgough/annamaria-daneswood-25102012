<?php

if(!defined('CMSLIB_DEFINED'))
	include_once(dirname(dirname(dirname(__FILE__))) . '/components/libraries/cmslib/spframework.php');

function MyblogBuildRoute( &$query )
{
	global $mainframe, $option;
	$segments = array();
	$admintask = array(
		'adminhome',
		'bloggerpref',
		'bloggerstats',
		'showcomments',
		'delete');
		
	$endWithSlash = array(
		'rss', 'feed', 'tags', 'search', 'archive');
	
	// default article view
	if (isset($query['show'])) {
		$query['task'] = $query['show'];
		unset($query['show']);
	}
	
	if (isset($query['task']) && $query['task'] != 'tag') {
		// For task == categories, we rename it as 'tags' to get a nicer
		// url
		if($query['task'] == 'categories'){
			$query['task'] = 'tags';
		}
		
		if($query['task'] == 'rss'){
			$query['task'] = 'feed';
		}
		
		/** front-end admin **/
		// Adminhome
		if(in_array($query['task'], $admintask)){	
			if (isset($query['amp;Itemid'])) {
				unset($query['amp;Itemid']);
			}
		}
		
		// If 'Add suffix to URLs' is enabled in Joomla 1.5, remove the
		// .html in the permalink
		if($mainframe->getCfg('sef') && $mainframe->getCfg('sef_suffix')){
			if(strlen($query['task']) > 5 && (substr($query['task'], -5) == '.html')){
				$query['task'] = substr($query['task'], 0, -5);
			}
		}
		
		if(in_array($query['task'], $endWithSlash) || 
			in_array($query['task'], $admintask)){
			
			$query['task'] .= '/';
		}
		
		$segments[] = $query['task'];
		unset($query['task']);
		
		// any additonal param?
		
	} else
	
	// Tags view
	if (isset($query['category'])) {
		$segments[] = 'tags';
		// Convert all spaces to + sign
		$query['category'] = str_replace(' ', '-', $query['category']);
		$segments[] = $query['category'] . '/';
		unset($query['category']);
		if(isset($query['task'])) unset($query['task']);
	} else
	
	// Archive view
	if (isset($query['archive'])) {
		$segments[] = 'archive';
		$segments[] = $query['archive'] .'/';
		unset($query['archive']);
	} else
	
	// Blogger
	if (isset($query['blogger'])) {
		$segments[] = 'blogger';
		$segments[] = $query['blogger'] .'/';
		unset($query['blogger']);
	}
	
	
	// Additional 2nd segment parameters
	if(in_array('delete', $segments)){
		$segments[] = $query['id'];
		unset($query['id']);
	}
	
// 	if(isset($query['start'])){
// 	    $segments[] = 'cpage/' . $query['start'];
// 	    unset($query['start']);
// 	}
	
	return $segments;
}

/**
 * @param	array
 * @return	array
 */
function MyblogParseRoute( $segments )
{
	$vars = array();
	//print_r($segments);
	
	$admintask = array(
		'adminhome',
		'bloggerpref',
		'bloggerstats',
		'showcomments');
	
	$actions    = array(
	                        'cpage',
	                        'tags',
	                        'archive',
	                        'feed',
	                        'search',
						);
	if(isset($segments[0])){
		for($i = 0; $i < count($segments); $i++){
			if(strlen($segments[$i]) > 5 && substr($segments[$i], -5) == '.html'){
				$segments[$i] = substr($segments[$i], 0, -5);
			}
		}
		
		// Extract limit based on cmslib cpage
// 		if(in_array('cpage', $segments)){
// 			$limitIndex = 0;
// 			foreach ($segments as $key=>$value){ 
// 				if ($value == 'cpage'){
// 					$limitIndex = $key+1;  
// 				} 
// 			}
// 			
// 			$limitdata = $segments[$limitIndex];
// 			$_GET['start'] = intval($limitdata);
// 			$_REQUEST['start'] = intval($limitdata);
// 			$vars['start'] = intval($limitdata);
// 			
// 			//$nonsefstring .= "&amp;limit=$limit&amp;limitstart=$limitstart";
// 			$nonsefstring .= "&amp;cpage=$limitdata";
// 			
// 			// Need to remove this so that the pagination is transparent to 
// 			// everyone else
// 			array_splice($segments, $limitIndex-1);
// 		}
		
// 	    if($segments[0] == 'cpage' && isset($segments[1])){
// 	        $vars['limitstart'] = $segments[1];
// 		}
		
		if($segments[0] == 'tags' && !isset($segments[1])){
			$vars['task'] = 'categories';
		} else
		
		if($segments[0] == 'tags' && isset($segments[1])){
			$vars['task']	= 'tag';
			$segments[1] = str_replace(':', '-', $segments[1]); // J1.5
			$segments[1] = str_replace('-', ' ', $segments[1]);
			$vars['category'] = $segments[1];
		} else
		
		if($segments[0] == 'archive' && isset($segments[1])){
			$vars['archive'] = $segments[1];
		} else 
		
		if($segments[0] == 'delete'){
			$vars['task'] = 'delete';
		} else 
		
		if($segments[0] == 'blogger' && isset($segments[1])){
			$vars['blogger'] = $segments[1];
		} else
		
		if($segments[0] == 'feed'){
			$vars['task'] = 'rss';
		}else
		
		if($segments[0] == 'search'){
			$vars['task'] = 'search';
		} else if(myIsPermalink($segments[0])){
            // default show view
			$vars['show'] = $segments[0];
		} 

		/** front-admin **/
		if(in_array($segments[0], $admintask) || empty($vars)){
			$vars['task'] = $segments[0];
		} 

	}

	return $vars;
}

// Check if the given url is a permalink or not
function myIsPermalink(&$inUrl) {
	$db = &cmsInstance('CMSDb');
	$cms = &cmsInstance('CMSCore');
	
	$url = $inUrl;
	if(!strpos($url, '.html')){
		$url .= '.html';
	}
	
	$url = $db->_escape($url); 
	$db->query("SELECT count(*) from #__myblog_permalinks WHERE `permalink`='$url'");
	$isexist = $db->get_value();
	
	if(!$isexist && (cmsVersion() == _CMS_JOOMLA15)){
		// maybe the dumb Joomla 1.5 try to convert the first - to : change them
		// and test it
		$pos = strpos($url, ':');
		if($pos){
			$url[$pos] = '-';
		}
		
		$db->query("SELECT count(*) from #__myblog_permalinks WHERE `permalink`='$url'");
		$isexist = $db->get_value();
	}
	
	if($isexist){
		$inUrl = $url;
	}
	
	return $isexist;
}
