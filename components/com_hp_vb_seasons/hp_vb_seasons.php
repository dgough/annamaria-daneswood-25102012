<?php
// hp_vb_seasons.php
/**
* Hot Property Pricing Bridge
*
* @package Hot Property 0.9
* @copyright (C) 2008 Organic Development
* @url http://www.organic-development.com/
* @author Organic Development <grow@organic-development.com>
**/

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/*error_reporting(E_ALL);
ini_set('display_errors','On');*/

global $option, $mosConfig_live_site, $mosConfig_absolute_path, $mainframe;

# Backend classes

if ($_REQUEST['property_id']!='') {
	
	include_once ( $mosConfig_absolute_path.'/administrator/components/com_hp_vb_seasons/admin.hp_vb_seasons.php' );

} else {
	echo '<center><br /><h2>Unavailable</h2>Please save the property first before setting up your property pricing option</center>';
}

?>