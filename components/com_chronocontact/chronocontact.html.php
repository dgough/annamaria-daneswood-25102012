<?php

/*
/**
* CHRONOFORMS version 2.0 stable
* Copyright (c) 2006 Chrono_Man, ChronoEngine.com. All rights reserved.
* Author: Chrono_Man (ChronoEngine.com)
* See readme.html.
* Visit http://www.ChronoEngine.com for regular update and information.
**/

/* ensuring that this file is called up from another file */
defined( '_VALID_MOS' ) or die( 'Direct access to this file is prohibited.' );
class HTML_ChronoContact {
// Procedure for building the table
 function showform( $rows , $imver ) {

 $paramsvalues = mosParseParams( $rows[0]->paramsall );
 if(!empty($rows[0]->name)){
 global $mosConfig_live_site, $mainframe;
 
 $style = "<link href='$mosConfig_live_site/components/com_chronocontact/style.css' rel='stylesheet' type='text/css'/>";

	$mainframe->addCustomHeadTag($style);
		?>
		<?php if(!empty($rows[0]->scriptcode)){ 
		echo "<script type='text/javascript'>\n";
        echo "//<![CDATA[\n";
		echo $rows[0]->scriptcode;
		echo "//]]>\n";
        echo "</script>\n";				
		}		
		?>
		<?php if(!empty($rows[0]->submiturl)){ 
		$actionurl = $rows[0]->submiturl;			
		} else {
		$actionurl = 'index.php?option=com_chronocontact&amp;task=send&amp;chronoformname='.$rows[0]->name;
		}		
		?>
		<?php if( trim($paramsvalues->validate) == 'Yes'){ ?>
			<script src="components/com_chronocontact/js/prototype.js" type="text/javascript"></script>
			<script src="components/com_chronocontact/js/effects.js" type="text/javascript"></script>
			<script src="components/com_chronocontact/js/validation.js" type="text/javascript"></script>
		<?php } ?>
<form name="<?php echo "ChronoContact_".$rows[0]->name; ?>" id="<?php echo "ChronoContact_".$rows[0]->name; ?>" method="<?php echo $paramsvalues->formmethod; ?>" action="<?php echo $actionurl; ?>" <?php echo $rows[0]->attformtag; ?> <?php if(strtoupper($paramsvalues->uploads) == 'YES') echo 'enctype="multipart/form-data"'; ?>>
		<div class="chrono_form">
				<?php 
					if( trim($paramsvalues->enmambots) == 'Yes'){
						global $_MAMBOTS;
						$_MAMBOTS->loadBotGroup( 'content' );
						$rowmam->text = $rows[0]->html;
						$results_mambots = $_MAMBOTS->trigger( 'onPrepareContent', array( &$rowmam, &$params, $page ), true );
						$rows[0]->html = $rowmam->text;
					}
					$rows[0]->html = str_replace('{imageverification}',"<span class='imgver'>".$imver."</span>",$rows[0]->html);
					eval( "?>".$rows[0]->html );
				?>
				</div>
</form>
		<?php if( trim($paramsvalues->validate) == 'Yes'){ ?>
			<script type="text/javascript">
				function formCallback(result, form) {
					window.status = "valiation callback for form '" + form.id + "': result = " + result;
				}
				var valid = new Validation('<?php echo "ChronoContact_".$rows[0]->name; ?>', {immediate : true, onFormValidate : formCallback});
			</script>
		<?php } ?>
<!-- You are not allowed to remove or edit the following 3 lines anyway if you didnt buy a license --> 

<!-- You are not allowed to remove or edit the above 3 lines anyway if you didnt buy a license -->
		<?php
		} else {
		echo "There is no form with this name, Please check the url and the form management";
		}
	}
}
?>
