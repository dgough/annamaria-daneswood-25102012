<jos:comment>
@package Joomla!
@subpackage com_sm2emailmarketing
@copyright Copyright (C) 2006 SM2 / All Rights Reserved
@license commercial
@author SM2 <info@sm2joomla.com>
</jos:comment>


<jos:tmpl name="pagetitle" varscope="list,archive,forward,forwarderror,sendforward,sendforwarderror">
  <div class="componentheading{PARAM_PAGECLASS_SFX}">{PAGETITLE}</div>
</jos:tmpl>

<jos:tmpl name="list">
  <jos:link src="pagetitle" />
  <table width="100%" cellspacing="0" cellpadding="0" border="0" class="contentpane{PARAM_PAGECLASS_SFX}">
    <tr>
      <td class="sectiontableheader{PARAM_PAGECLASS_SFX}" width="80%">{LANG_EMKTG_MESSAGE_SUBJECT}</td>
      <td class="sectiontableheader{PARAM_PAGECLASS_SFX}" width="20%">{LANG_EMKTG_MESSAGE_SENDDATE}</td>
    </tr>
    <jos:tmpl name="rows" addSystemVars="integer">
      <jos:tmpl type="condition" conditionvar="PAT_IS_EVEN" varscope="rows,list">
        <jos:sub condition="0"><tr class="sectiontableentry{PARAM_PAGECLASS_SFX}1"></jos:sub>
        <jos:sub condition="1"><tr class="sectiontableentry{PARAM_PAGECLASS_SFX}2"></jos:sub>
      </jos:tmpl>
        <td><a href="{LINK}">{SUBJECT}</a></td>
        <td>{SEND_DATE_DISPLAY}</td>
      </tr>
    </jos:tmpl>
  </table>
</jos:tmpl>

<jos:tmpl name="listempty">
  <jos:link src="pagetitle" />
  <div class="contentpane{PARAM_PAGECLASS_SFX}">
    {LANG_EMKTG_MESSAGE_FRONT_EMPTY}
  </div>
</jos:tmpl>


<jos:tmpl name="archive">
<jos:link src="pagetitle" />
    <table cellpadding="4" cellspacing="1" border="0" class="contentpane{PARAM_PAGECLASS_SFX}">
      <tr>
        <td class="sectiontableheader{PARAM_PAGECLASS_SFX}">{LANG_EMKTG_MESSAGE_SUBJECT}:</td>
        <td>{SUBJECT}</td>
      </tr>
      <tr>
        <td class="sectiontableheader{PARAM_PAGECLASS_SFX}">{LANG_EMKTG_MESSAGE_SENDDATE}:</td>
        <td>{SEND_DATE_DISPLAY}</td>
      </tr>
    </table>
    <table width="100%">
	  <tr>
	    <th>{LANG_EMKTG_MESSAGE}</th>
      </tr>
      <tr>
        <td>
<iframe
src="index2.php?option={OPTION}&task=showarchivemessage&action=1&no_html=1&id={MESSAGEID}{CONFIRMURL}"
width="100%"
height="500"
scrolling="auto"
align="top"
frameborder="0"></iframe>
        </td>
      </tr>
    </table>
  <jos:tmpl type="condition" conditionvar="PARAM_BACK_BUTTON" varscope="archive">
    <jos:sub condition="1">
      <div class="back_button">
        <a href='javascript:history.go(-1)'>
          {LANG_BACK}
        </a>
      </div>
    </jos:sub>
  </jos:tmpl>
</jos:tmpl>


<jos:tmpl name="forward">
  <jos:link src="pagetitle" />
<script language="javascript" type="text/javascript">
		function submitSM2EMForward() {
			var coll = document.sm2EMForward;
			var r = new RegExp("[^0-9A-Za-z]", "i");
			var errorMSG = '';
			var iserror=0;

            if (coll != null) {
                var elements = coll.elements;
                // loop through all input elements in form
                for (var i=0; i < elements.length; i++) {
                    // check if element is mandatory; here mosReq=1
                    if (elements.item(i).className.indexOf('required') != -1) {
                        // check for empty field value
                        if (elements.item(i).value == '') {
                            // add up all error messages
                            errorMSG += elements.item(i).getAttribute('title') + '\n';
                            // notify user by changing background color, in this case to red
                            elements.item(i).style.backgroundColor = "#CC0000";
                            iserror=1;
                            continue;
                        }
                    }
                }
		    }
			if(iserror==1) {
                alert('{LANG_EMKTG_FORWARD_JS_ERROR}\n'+errorMSG);
                return false;
            } else {
                return true;
			}
		}
</script>
  <form action="index.php?option={OPTION}&task=sendforward&id={MESSAGEID}&subscriberid={SUBSCRIBERID}&confirm={CONFIRM}&Itemid={ITEMID}" method="post" name="sm2EMForward" id="sm2EMForward" onsubmit="return submitSM2EMForward()">
  <table width="100%" cellspacing="0" cellpadding="0" border="0" class="contentpane{PARAM_PAGECLASS_SFX}">
    <tr>
      <td colspan="2">{LANG_EMKTG_FORWARD_PRETEXT}</td>
    </tr>
    <tr>
      <th valign="top">{LANG_EMKTG_MESSAGE_SUBJECT}</th>
      <td>{SUBJECT}</td>
    </tr>
    <tr>
      <th valign="top">{LANG_EMKTG_MESSAGE_FROMNAME}</th>
      <td>{NAME} ({EMAIL})</td>
    </tr>
    <tr>
      <th valign="top">{LANG_EMKTG_FORWARD_MESSAGE_FORMAT}</th>
      <td>{RECEIVEHTML}</td>
    </tr>
    <tr>
      <th valign="top">{LANG_EMKTG_FORWARD_MESSAGE_CONTENT}</th>
      <td>{MESSAGECONTENT}</td>
    </tr>
    <tr>
      <th valign="top">{LANG_EMKTG_ARCHIVE_RECIPIENT_NAME}</th>
      <td><input type="text" name="name" value="" class="inputbox required" title="{LANG_EMKTG_ARCHIVE_RECIPIENT_NAME}" /></td>
    </tr>
    <tr>
      <th valign="top">{LANG_EMKTG_ARCHIVE_RECIPIENT_EMAIL}</th>
      <td><input type="text" name="email" value="" class="inputbox required" title="{LANG_EMKTG_ARCHIVE_RECIPIENT_EMAIL}" /></td>
    </tr>
    <tr>
      <th valign="top">{LANG_EMKTG_FORWARD_PERSONAL_MESSAGE}</th>
      <td><textarea name="note" class="inputbox required" title="{LANG_EMKTG_FORWARD_PERSONAL_MESSAGE}"></textarea></td>
    </tr>
    <tr>
      <td colspan="2"><input type="submit" value="{LANG_EMKTG_FORWARD}" class="button" /></td>
    </tr>
  </table>
  </form>
</jos:tmpl>

<jos:tmpl name="forwarderror">
  <jos:link src="pagetitle" />
  <table width="100%" cellspacing="0" cellpadding="0" border="0" class="contentpane{PARAM_PAGECLASS_SFX}">
    <tr>
      <th>{LANG_EMKTG_FORWARD_LINK_ERROR_TITLE}</th>
    </tr>
    <tr>
      <td>{LANG_EMKTG_FORWARD_LINK_ERROR_DESC}</td>
    </tr>
  </table>
</jos:tmpl>

<jos:tmpl name="forwardexpired">
  <jos:link src="pagetitle" />
  <table width="100%" cellspacing="0" cellpadding="0" border="0" class="contentpane{PARAM_PAGECLASS_SFX}">
    <tr>
      <th>{LANG_EMKTG_FORWARD_EXPIRE_ERROR_TITLE}</th>
    </tr>
    <tr>
      <td>{LANG_EMKTG_FORWARD_EXPIRE_ERROR_DESC}</td>
    </tr>
  </table>
</jos:tmpl>

<jos:tmpl name="sendforward">
  <jos:link src="pagetitle" />
  <table width="100%" cellspacing="0" cellpadding="0" border="0" class="contentpane{PARAM_PAGECLASS_SFX}">
    <tr>
      <td colspan="2">{LANG_EMKTG_FORWARD_SENT}</td>
    </tr>
    <tr>
      <th valign="top">{LANG_EMKTG_MESSAGE_SUBJECT}</th>
      <td>{SUBJECT}</td>
    </tr>
    <tr>
      <th valign="top">{LANG_EMKTG_MESSAGE_FROMNAME}</th>
      <td>{FROMNAME} ({FROMEMAIL})</td>
    </tr>
    <tr>
      <th valign="top">{LANG_EMKTG_ARCHIVE_RECIPIENT_NAME}</th>
      <td>{NAME}</td>
    </tr>
    <tr>
      <th valign="top">{LANG_EMKTG_ARCHIVE_RECIPIENT_EMAIL}</th>
      <td>{EMAIL}</td>
    </tr>
    <tr>
      <th valign="top">{LANG_EMKTG_FORWARD_PERSONAL_MESSAGE}</th>
      <td>{NOTE}</td>
    </tr>
  </table>
  <div class="back_button">
    <a href='javascript:history.go(-1)'>
      {LANG_BACK}
    </a>
  </div>
</jos:tmpl>

<jos:tmpl name="sendforwarderror">
  <jos:link src="pagetitle" />
  <table width="100%" cellspacing="0" cellpadding="0" border="0" class="contentpane{PARAM_PAGECLASS_SFX}">
    <tr>
      <th>{LANG_EMKTG_FORWARD_SEND_ERROR_TITLE}</th>
    </tr>
    <tr>
      <td>{ERROR}</td>
    </tr>
  </table>
  <div class="back_button">
    <a href='javascript:history.go(-1)'>
      {LANG_BACK}
    </a>
  </div>
</jos:tmpl>