<?php
// $Id: english.php,v 1.72 2008/04/21 20:27:06 sm2tony Exp $
/**
 * sm2emailmarketing - Email Marketing Newsletter
 *
 * Joomla! Newsletter Component that supports multiple list mailing and message queueing
 *
 * @package Joomla!
 * @subpackage com_sm2emailmarketing
 * @copyright Copyright (C) 2006 SM2 / All Rights Reserved
 * @license commercial
 * @author SM2 <info@sm2joomla.com>
 */

/**
 * Add some default langauge constructs if Joomla! 1.5 does not have them
 */
if (defined('_JLEGACY')) {
    define('_CMN_ARCHIVE', JText::_('Archive'));
    define('_CMN_EMAIL', JText::_('Email'));
    define('_CMN_NAME', JText::_('Name'));
    define('_CMN_NEXT', JText::_('Next'));
    if (!defined('_CMN_NO')) define('_CMN_NO', JText::_('No'));
    define('_CMN_PUBLISHED', JText::_('Published'));
    define('_CMN_UNPUBLISHED', JText::_('Unpublished'));
    if (!defined('_CMN_YES')) define('_CMN_YES', JText::_('Yes'));
    define('_E_ADD', JText::_('Add'));
    define('_E_EDIT', JText::_('Edit'));
}

/**#@+
 * Language definitions
 */
define("_EMKTG", "SM2 Email Marketing");
define("_EMKTG_FULL_TITLE", "Email Marketing Newsletter");

define("_EMKTG_USER_UPDATE_ERROR1", "Error getting newly added Joomla users. Database error");
define("_EMKTG_USER_UPDATE_ERROR2", "Error adding new users to subscribers table. Database error");

define("_EMKTG_SEARCH_TITLE", "Keyword");
define("_EMKTG_ACCESS", "Access");
define("_EMKTG_REQUIRED", "<span style=\"color: red;\">*</span>");
define("_EMKTG_SUBSCRIPTION", "Subscription");
define("_EMKTG_PREVIEW", "Preview");
define("_EMKTG_COPY", "Copy");
define("_EMKTG_VALIDATION_ERROR", "Validation Errors");
define("_EMKTG_RECEIVE_HTML", "Receive HTML");
define("_EMKTG_RECEIVE_TEXT", "Receive Text");
define("_EMKTG_CONFIRM", "Confirm");
define("_EMKTG_CONFIRM_VARIABLE", "[CONFIRM]");
define("_EMKTG_UNSUBSCRIBE_VARIABLE", "[UNSUBSCRIBE]");
define("_EMKTG_UNCONFIRM", "Unconfirm");
define("_EMKTG_CONFIRMED", "Confirmed");
define("_EMKTG_UNCONFIRMED", "Unconfirmed");
define("_EMKTG_SUBSCRIBE_DATE", "Subscribe Date");
define("_EMKTG_UNSUBSCRIBE_DATE", "Unsubscribe Date");
define("_EMKTG_ENABLE", "Enable");
define("_EMKTG_ENABLED", "Enabled");
define("_EMKTG_DISABLE", "Disable");
define("_EMKTG_DISABLED", "Disabled");
define("_EMKTG_ERROR", "Error");
define("_EMKTG_HELP", "Help");

define("_EMKTG_TOOLBAR_UNSUBSCRIBE", "Unsubscribe");
define("_EMKTG_TOOLBAR_PREVIEW", "Preview");
define("_EMKTG_TOOLBAR_VIEW", "View");
define("_EMKTG_TOOLBAR_SEND", "Send");
define("_EMKTG_TOOLBAR_SEND_CONFIRM", "Are you sure you wish to send this message now?");
define("_EMKTG_TOOLBAR_SENDTEST", "Send Test");
define("_EMKTG_TOOLBAR_COPY", "Copy");
define("_EMKTG_TOOLBAR_CONFIRM", "Confirm");
define("_EMKTG_TOOLBAR_UNCONFIRM", "Unconfirm");
define("_EMKTG_TOOLBAR_HTML", "HTML");
define("_EMKTG_TOOLBAR_TEXT", "Text");
define("_EMKTG_TOOLBAR_PROCESS", "Process");
define("_EMKTG_TOOLBAR_NEXTBATCH", "Next Batch");
define("_EMKTG_TOOLBAR_ENABLE", "Enable");
define("_EMKTG_TOOLBAR_DISABLE", "Disable");
define("_EMKTG_TOOLBAR_CUSTCONFIRM", "Are you sure you wish to change all of the records?");
define('_EMKTG_TOOLBAR_IMPORT', 'Import');

define("_EMKTG_LIST", "List");
define("_EMKTG_LIST_DISABLED", "sm2emailmarketing Lists have been disabled.\\nTo re-enable sm2emailmarketing Lists edit the sm2emailmarketing Lists plugin in the sm2emailmarketing Plugin Management and set the enabled parameter to Yes");
define("_EMKTG_LIST_ERROR", "Error retreiving lists.");
define("_EMKTG_LIST_FORM", "List Manager");
define("_EMKTG_LIST_EDIT", "Edit List");
define("_EMKTG_LIST_NAME", "List Name");
define("_EMKTG_LIST_NAME_DESC", "Enter a name for the List.");
define("_EMKTG_LIST_DESCRIPTION", "Description");
define("_EMKTG_LIST_ACCESS", "Access level");
define("_EMKTG_LIST_ACCESS_DESC", "Select the Access level");
define("_EMKTG_LIST_PUBLISH", "Published?");
define("_EMKTG_LIST_PUBLISH_ERROR", "No Lists selected.");
define("_EMKTG_LIST_SAVED", "List saved successfully.");
define("_EMKTG_LIST_PLEASE_SELECT", "Please select 1 or more lists to be ");
define("_EMKTG_LIST_DELETED", "List(s) deleted successfully.");
define("_EMKTG_LIST_PUBLISHED", "List(s) "._CMN_PUBLISHED." successfully.");
define("_EMKTG_LIST_UNPUBLISHED", "List(s) "._CMN_UNPUBLISHED." successfully.");
define("_EMKTG_LIST_ACCESS_CHANGED", "List(s) access changed successfully.");

define("_EMKTG_TEMPLATE", "Template");
define("_EMKTG_TEMPLATE_FORM", "Template Manager");
define("_EMKTG_TEMPLATE_ERROR", "Error retreiving Templates.");
define("_EMKTG_TEMPLATE_SAVED", "Template saved successfully.");
define("_EMKTG_TEMPLATE_EDIT", "Edit Template");
define("_EMKTG_TEMPLATE_NAME", "Template Name");
define("_EMKTG_TEMPLATE_NAME_DESC", "Enter a name for the Template.");
define("_EMKTG_TEMPLATE_LAYOUT_HTML", "HTML Layout");
define("_EMKTG_TEMPLATE_LAYOUT_HTML_DESC", "Enter the HTML Version of the Template");
define("_EMKTG_TEMPLATE_LAYOUT_TEXT", "Text Layout");
define("_EMKTG_TEMPLATE_LAYOUT_TEXT_DESC", "Enter the Text Version of the Template");
define("_EMKTG_TEMPLATE_PUBLISH", _EMKTG_LIST_PUBLISH);
define("_EMKTG_TEMPLATE_PLEASE_SELECT", "Please select 1 or more templates to be ");
define("_EMKTG_TEMPLATE_DELETE", "Template(s) deleted successfully.");
define("_EMKTG_TEMPLATE_PUBLISHED", "Templates(s) "._CMN_PUBLISHED." successfully.");

define("_EMKTG_SUBSCRIPTION_EDIT", "Edit Subscription");
define("_EMKTG_SUBSCRIBER_SAVED", "Subscriber saved successfully.");
define("_EMKTG_SUBSCRIBER_DELETED", "Subscriber(s) deleted successfully.");
define("_EMKTG_SUBSCRIBER_UNSUBSCRIBED", "Subscriber(s) unsubscribed successfully.");
define("_EMKTG_SUBSCRIBER_CONFIRMED", "Subscriber(s) "._EMKTG_CONFIRMED." successfully.");
define("_EMKTG_SUBSCRIBER_UNCONFIRMED", "Subscriber(s) "._EMKTG_UNCONFIRMED." successfully.");
define("_EMKTG_SUBSCRIBER_RECEIVE_HTML", "Subscriber(s) "._EMKTG_RECEIVE_HTML." successfully.");
define("_EMKTG_SUBSCRIBER_RECEIVE_TEXT", "Subscriber(s) "._EMKTG_RECEIVE_TEXT." successfully.");
define("_EMKTG_SUBSCRIBER_PLEASE_SELECT", "Please select 1 or more Subscribers to be ");

define("_EMKTG_REGISTERED_FORM", "Client Management Subscribers");
define("_EMKTG_REGISTERED_ERROR", "Error retreiving Client Management Subscribers.");
define("_EMKTG_REGISTERED_CONFIRM_ERROR", "No Subscribers selected.");
define("_EMKTG_REGISTERED_RECEIVE_HTML_ERROR", _EMKTG_REGISTERED_CONFIRM_ERROR);

define("_EMKTG_UNREGISTERED_FORM", "List Only Subscribers");
define("_EMKTG_UNREGISTERED_ERROR", "Error retreiving List Only Subscribers.");
define("_EMKTG_UNREGISTERED_CONFIRM_ERROR", _EMKTG_REGISTERED_CONFIRM_ERROR);
define("_EMKTG_UNREGISTERED_RECEIVE_HTML_ERROR", _EMKTG_REGISTERED_CONFIRM_ERROR);
define('_EMKTG_UNREGISTERED_IMPORT', 'Import');
define('_EMKTG_UNREGISTERED_IMPORT_UPLOAD', 'Upload CSV File');
define('_EMKTG_UNREGISTERED_IMPORT_UPLOAD_DESC', 'Select a CSV File to upload.');
define('_EMKTG_UNREGISTERED_UPLOAD_ERR', 'File upload Failed: unknown upload error.');
define('_EMKTG_UNREGISTERED_UPLOAD_ERR_FORM_SIZE', 'File upload Failed: File was too large.');
define('_EMKTG_UNREGISTERED_UPLOAD_ERR_PARTIAL', 'File upload Failed: Only partial file received.');
define('_EMKTG_UNREGISTERED_UPLOAD_ERR_NO_FILE', 'File upload Failed: No file uploaded.');
define('_EMKTG_UNREGISTERED_IMPORT_NAME', 'Name');
define('_EMKTG_UNREGISTERED_IMPORT_NAME_DESC', 'Select the field that contains the name of the Subscriber.');
define('_EMKTG_UNREGISTERED_IMPORT_EMAIL', 'Email');
define('_EMKTG_UNREGISTERED_IMPORT_EMAIL_DESC', 'Select the field that contains the email address of the Subscriber.');
define('_EMKTG_UNREGISTERED_IMPORT_RECEIVE_HTML', 'Receive HTML');
define('_EMKTG_UNREGISTERED_IMPORT_RECEIVE_HTML_DESC', 'Select the default value to be used for Receive HTML. Optionally also select the field that contains the Receive HTML status of the Subscriber.');
define('_EMKTG_UNREGISTERED_IMPORT_LIST', 'Lists');
define('_EMKTG_UNREGISTERED_IMPORT_LIST_DESC', 'Select the default List(s) to be added to. Optionally also select the field(s) that contains the Lists to add the Subscriber to.');
define('_EMKTG_UNREGISTERED_IMPORT_ERR_FILE', 'Unable to process Import. File has disappeared.');
define('_EMKTG_UNREGISTERED_IMPORT_ERR_OPTIONS', 'Unable to process Import. No import options could be found.');
define('_EMKTG_UNREGISTERED_IMPORT_ERR_NAME', 'Unable to process Import. No Name field was selected.');
define('_EMKTG_UNREGISTERED_IMPORT_ERR_EMAIL', 'Unable to process Import. No Email field was selected.');
define('_EMKTG_UNREGISTERED_IMPORT_ERR_RECEIVE_HTML', 'Unable to process Import. No default Receive HTML was selected.');
define('_EMKTG_UNREGISTERED_IMPORT_ERR_STORE', 'Error importing subscriber: %n [%e]<br />%d');
define('_EMKTG_UNREGISTERED_IMPORT_ERR_MAX', 'Too many errors were encounter to list them all, only the first 100 are listed here.');
define('_EMKTG_UNREGISTERED_IMPORT_ERR_SUBSCRIBE', 'Error importing subscriber to lists: %n [%e];<br />%d');
define('_EMKTG_UNREGISTERED_IMPORT_SUMMARY', 'Summary');
define('_EMKTG_UNREGISTERED_IMPORT_RECORDS', 'CSV Records');
define('_EMKTG_UNREGISTERED_IMPORT_ERRORS', 'Errors');

define("_EMKTG_PLUGIN_FORM", "Plugin Manager");
define("_EMKTG_PLUGIN_ERROR", "Error retreiving plugins.");
define("_EMKTG_PLUGIN_NAME", "Plugin Name");
define("_EMKTG_PLUGIN_DESCRIPTION", "Description");
define("_EMKTG_PLUGIN_ORDER", "Order");
define("_EMKTG_PLUGIN_SAVE_ORDER", "Save Order");
define("_EMKTG_PLUGIN_REORDER", "Reorder");
define("_EMKTG_PLUGIN_EDIT", "Edit Plugin");
define("_EMKTG_PLUGIN_DETAILS", "Details");
define("_EMKTG_PLUGIN_PARAMETER", "Parameters");
define("_EMKTG_PLUGIN_SAVED", "Plugin saved successfully.");
define("_EMKTG_PLUGIN_ORDER_SAVED", "Plugin Order updated successfully.");
define("_EMKTG_PLUGIN_ORDER_UP", "Move Up");
define("_EMKTG_PLUGIN_ORDER_DOWN", "Move Down");
define("_EMKTG_PLUGIN_PLEASE_SELECT", "Please select 1 or more Plugins to be ");
define("_EMKTG_PLUGIN_ENABLED", "Plugin(s) "._EMKTG_ENABLED." successfully.");
define("_EMKTG_PLUGIN_DISABLED", "Plugin(s) "._EMKTG_DISABLED." successfully.");
define("_EMKTG_PLUGIN_CONFIRM_ERROR", "No Plugins selected.");
define("_EMKTG_PLUGIN_INVALID", "Plugin Invalid");
define("_EMKTG_PLUGIN_INVALID_DESC", "This plugin is most likely invalid because a related Joomla component is not installed.");

define("_EMKTG_QUEUE", "Queue");
define("_EMKTG_QUEUE_FORM", "Queue Manager");
define("_EMKTG_QUEUE_ERROR", "Error retreiving Queue.");
define("_EMKTG_QUEUE_LEFT", "Unprocessed");
define("_EMKTG_QUEUE_PROCESS", "Process");
define("_EMKTG_QUEUE_SIZE", "Queue Size");
define("_EMKTG_QUEUE_HTMLMSGSENT", "Sending HTML Message");
define("_EMKTG_QUEUE_TEXTMSGSENT", "Sending TEXT Message");
define("_EMKTG_QUEUE_ERRORSENDINGMSG", "There was an error sending the message");
define("_EMKTG_QUEUE_ERRORTEMPLATE", "Unable to determine template of Queued Message");
define("_EMKTG_QUEUE_TIMEOUT_EXCEEDED", "Queue process exceeded timeout of [MIN] minutes.");
define("_EMKTG_QUEUE_FINISHED", "Queue has finished processing!.");
define("_EMKTG_QUEUE_PAUSE_MSG", "Pausing [PAUSE] seconds before processing next batch.");
define("_EMKTG_QUEUE_CONT_MSG", "Click the [A]Next Batch[/A] button to force the queue to go to the next batch");
define("_EMKTG_QUEUE_DELETED", "Queue has been deleted");
define("_EMKTG_QUEUE_NOTDELETED", "The Queue was not deleted");

define("_EMKTG_STATISTICS_FORM", "Statistics Manager");
define("_EMKTG_STATISTICS_ERROR", "Error retreiving Statistics.");
define("_EMKTG_STATISTICS_HTML_SENT", "HTML Sent");
define("_EMKTG_STATISTICS_TEXT_SENT", "Text Sent");
define("_EMKTG_STATISTICS_HTML_READ", "HTML Read");
define("_EMKTG_STATISTICS_HTML_UNREAD", "HTML Unread");
define("_EMKTG_STATISTICS_ERRORS", "Errors");
define("_EMKTG_STATISTICS_BLACKLISTS", "Blacklisted");
define("_EMKTG_STATISTICS_BOUNCES", "Bounced");
define("_EMKTG_STATISTICS_MESSAGES_SENT", "Messages Sent");
define("_EMKTG_STATISTICS_OVERALL", "Overall Statistics");

define("_EMKTG_ARCHIVE_FORM", "View Archive");
define("_EMKTG_ARCHIVE_RECIPIENT_EMAIL", "Recipient Email");
define("_EMKTG_ARCHIVE_RECIPIENT_NAME", "Recipient Name");

define("_EMKTG_MESSAGE", "Message");
define("_EMKTG_MESSAGE_FORM", "Message Manager");
define("_EMKTG_MESSAGE_ERROR", "Error retreiving messages.");
define("_EMKTG_MESSAGE_EDIT", "Edit Message");
define("_EMKTG_MESSAGE_CONTENT", "Fill out sender details, choose template and provide content for this mailing.");
define("_EMKTG_MESSAGE_EXTRAS", "Choose attachments and extra content to include in the mailing.");
define("_EMKTG_MESSAGE_RECIPIENTS", "Select who you want to send the mailing to.");

define("_EMKTG_MESSAGE_RECIPIENTS_DESC", "Select one or more filters from the sections below to determine the recipients of the message.");
define("_EMKTG_MESSAGE_SUBJECT", "Subject");
define("_EMKTG_MESSAGE_SUBJECT_DESC", "Enter a subject for the Message.");
define("_EMKTG_MESSAGE_FROMNAME", "Sender name");
define("_EMKTG_MESSAGE_FROMNAME_DESC", "Enter the Sender's Name for the Message.");
define("_EMKTG_MESSAGE_FROMEMAIL", "Sender Email");
define("_EMKTG_MESSAGE_FROMEMAIL_DESC", "Enter the Sender's Email for the Message.");
define("_EMKTG_MESSAGE_BOUNCEEMAIL", "Bounce Email");
define("_EMKTG_MESSAGE_BOUNCEEMAIL_DESC", "Enter the Bounce Email for the Message.");
define("_EMKTG_TEMPLATE_DESC", "Select a Template");
define("_EMKTG_MESSAGE_HTML", "HTML Message");
define("_EMKTG_MESSAGE_HTML_DESC", "This is the area for the HTML version of your message");
define("_EMKTG_MESSAGE_TEXT", "Text Only");
define("_EMKTG_MESSAGE_TEXT_DESC", "This is the area for the TEXT version of your message");
define("_EMKTG_MESSAGE_NONE_DESC", "Please enter either a text or html message");
define("_EMKTG_MESSAGE_SENDDATE", "Sent Date");
define("_EMKTG_MESSAGE_SENDDATE_FORMAT", "%d %B %Y");
define("_EMKTG_MESSAGE_ATTACHMENTS", "Attachments");
define("_EMKTG_MESSAGE_ATTACHMENTS_DESC", "Select the attachments to send along as part of the message.<br />Including a lot of attachments, or large attachments may require setting, or increasing, the value of the Process Memory Confirguation option.");
define("_EMKTG_MESSAGE_CONTENT_ITEMS", "Content Items");
define("_EMKTG_MESSAGE_CONTENT_ITEMS_DESC", "Select the Content Items to include in the message.<br />Use the [ &gt;&gt; ] button to add an item from the Available Content to the Selected Content.<br />Use the [ &lt;&lt; ] button to remove an item from the Selected Content.<br />Use the [ Up ] and [ Down ] buttons to order the items in the Selected Content list.");
define("_EMKTG_MESSAGE_AVAILABLE_CONTENT", "Available Content");
define("_EMKTG_MESSAGE_AVAILABLE_CONTENT_DESC", "This is the list of available Content items.<br />Use the [ &gt;&gt; ] button to add items from here to the Selected Content list.");
define("_EMKTG_MESSAGE_SELECTED_CONTENT", "Selected Content");
define("_EMKTG_MESSAGE_SELECTED_CONTENT_DESC", "This is the list of Content Items that will be included in the Message and they order they will appear.<br />Use the [ &lt;&lt; ] button to remove items.<br />Use the [ Up ] and [ Down ] buttons to change the items order.");
define("_EMKTG_MESSAGE_CONTENT_UP", " Up ");
define("_EMKTG_MESSAGE_CONTENT_DOWN", " Down ");
define("_EMKTG_MESSAGE_CONTENT_ADD", " &gt;&gt; ");
define("_EMKTG_MESSAGE_CONTENT_REMOVE", " &lt;&lt; ");
define("_EMKTG_MESSAGE_INTRO_ONLY", "Intro Only");
define("_EMKTG_MESSAGE_INTRO_ONLY_DESC", "By checking this option all articles selected will only show the introductory text and a ?Read More?? link which takes the reader to the website.");
define("_EMKTG_MESSAGE_NO_STATS", "You cannot view the Statistics until the Message has been sent.");
define("_EMKTG_MESSAGE_SEND_ERROR", "You cannot Send this message because it has already been sent.\\nWould you like to Copy this message?\\n\\nNote: Copying will add 'Copy of' to the start of the Message Subject");
define("_EMKTG_MESSAGE_ALREADY_SENT", "You cannot Edit this message because it has already been sent.");
define("_EMKTG_MESSAGE_ALREADY_SENT_MSG", "If you would like to preview the message as it was sent then click Preview.<br />If you would like to send this message again then Select Copy.<br />Or click cancel to return to the Message Manager.");
define("_EMKTG_MESSAGE_SAVED", "Message saved successfully.");
define("_EMKTG_MESSAGE_PREVIEW", "Preview Message");
define("_EMKTG_MESSAGE_SENDTEST", "Send Test Message");
define("_EMKTG_MESSAGE_TESTSENT", "Test Message was sent successfully");
define("_EMKTG_MESSAGE_TESTERROR", "There was an error sending the message");
define("_EMKTG_MESSAGE_PREVIEW_MSG", "The following is a list of the Recipients who will receive this Message based on the currently selected filters.");
define("_EMKTG_MESSAGE_PREVIEW_PAGE", 100);
define("_EMKTG_MESSAGE_PREVIEW_TOTAL", "Total Recipients");
define("_EMKTG_MESSAGE_ARCHIVE_NONEXISTENT", "Unable to find archived message");
define("_EMKTG_MESSAGE_DELETED", "Message(s) deleted successfully.");

define("_EMKTG_BOUNCE_FORM", "Bounce Manager");
define("_EMKTG_BOUNCE_ERROR_CONNECT", "Unable to connect to Bounce Mail Server. Check Configuration and try again.");


define("_EMKTG_CONFIGURATION_FORM", "SM2 Email Marketing Configuration");
define("_EMKTG_CONFIGURATION_LICENCE", "Licencing");
define("_EMKTG_CONFIGURATION_LICENCE_KEY", "Licence Key");
define("_EMKTG_CONFIGURATION_LICENCE_KEY_DESC", "Enter the SM2 Provided Licence Key to activate this product.");
define("_EMKTG_CONFIGURATION_PROCESSING_OPTIONS", "Processing Options");
define("_EMKTG_CONFIGURATION_ENABLEREADSTATS", "Enable read statistics");
define("_EMKTG_CONFIGURATION_ENABLEREADSTATS_DESC", "Select yes if you want to log the number of views. This technique can only be used with html mailings.");
define("_EMKTG_CONFIGURATION_LOGVIEWSPERSUB", "Log views per subscriber");
define("_EMKTG_CONFIGURATION_LOGVIEWSPERSUB_DESC", "Select yes if you want to log the number of views per subscriber. This technique can only be used with html mailings.");
define("_EMKTG_CONFIGURATION_PAUSEX", "Pause x seconds every configured amount of emails");
define("_EMKTG_CONFIGURATION_PAUSEX_DESC", "Enter a number of seconds sm2emailmarketing will give the SMTP server the time to send out the messages before proceeding with the next configured amount of messages.");
define("_EMKTG_CONFIGURATION_EMAILS_BETWEEN_PAUSES", "Emails between pauses");
define("_EMKTG_CONFIGURATION_EMAILS_BETWEEN_PAUSES_DESC", "The number of emails to send before pausing.");
define("_EMKTG_CONFIGURATION_SCRIPT_TIMEOUT", "Script timeout");
define("_EMKTG_CONFIGURATION_SCRIPT_TIMEOUT_DESC", "The number of minutes the script should be able to run.");
define("_EMKTG_CONFIGURATION_ALLOWUNREG", "Allow unregistered");
define("_EMKTG_CONFIGURATION_ALLOWUNREG_DESC", "Select yes if you want to allow users to subscribe to newsletters without registering at the site.");
define("_EMKTG_CONFIGURATION_REQCONFIRM", "Require confirmation");
define("_EMKTG_CONFIGURATION_REQCONFIRM_DESC", "Select yes if you require that unregistered subscribers confirm their email address.");
define("_EMKTG_CONFIGURATION_HTMLCONFIRM", "HTML confirm");
define("_EMKTG_CONFIGURATION_HTMLCONFIRM_DESC", "Select yes if confirmation letters should be html if the user allows html.");
define("_EMKTG_CONFIGURATION_PLUGIN_JOIN", "Plugin Join");
define("_EMKTG_CONFIGURATION_PLUGIN_JOIN_DESC", "On Message send how should Plugin Filter groups be precessed.&lt;br /&gt;Should the Filters be AND or OR.&lt;br /&gt;i.e. State 'QLD' AND User Groups 'Registered',&lt;br /&gt;or&lt;br /&gt;State 'QLD' OR User Groups 'Registered'");
define("_EMKTG_CONFIGURATION_PLUGIN_JOIN_AND", "And");
define("_EMKTG_CONFIGURATION_PLUGIN_JOIN_OR", "Or");
define('_EMKTG_CONFIGURATION_PROCESS_MEMORY', 'Process Memory');
define('_EMKTG_CONFIGURATION_PROCESS_MEMORY_DESC', 'The amount of Memory in Mbytes to make available when sending messages. Leave at 0 or empty to use the system default. If large attachments (1M+) set to a high figure like 40.');
define("_EMKTG_CONFIGURATION_EMAIL_OPTIONS", "Email Options");
define("_EMKTG_CONFIGURATION_DEFAULTFROMNAME", "Default from name");
define("_EMKTG_CONFIGURATION_DEFAULTFROMNAME_DESC", "Enter the default from name to use for Messages.");
define("_EMKTG_CONFIGURATION_DEFAULTFROMEMAIL", "Default from email");
define("_EMKTG_CONFIGURATION_DEFAULTFROMEMAIL_DESC", "Enter the default email address to use for Messages.");
define("_EMKTG_CONFIGURATION_ERROR_UNSUBSCRIBE", "Error Unsubscribe");
define("_EMKTG_CONFIGURATION_ERROR_UNSUBSCRIBE_DESC", "Number of times an email is allowed to error before it is auto unsubscribed. 0 means never unsubscribed.");
define('_EMKTG_CONFIGURATION_ATTACHMENT_PATH', 'Attachment Path');
define('_EMKTG_CONFIGURATION_ATTACHMENT_PATH_DESC', 'Full Path on the server to the folder where the attachments are stored.');
define("_EMKTG_CONFIGURATION_BOUNCE_OPTIONS", "Bounce Options");
define("_EMKTG_CONFIGURATION_BOUNCE", "Bounce Processing");
define("_EMKTG_CONFIGURATION_BOUNCE_DESC", "Use Bounce processing to determine email addresses that no longer exist?");
define("_EMKTG_CONFIGURATION_BOUNCE_BATCH", "Bounce Batch Size");
define("_EMKTG_CONFIGURATION_BOUNCE_BATCH_DESC", "Number of records to check for Bounces at a time. 0 means all, but only recommended if Mail Server is on the Webserver.");
define("_EMKTG_CONFIGURATION_DEFAULTBOUNCE", "Default bounce address");
define("_EMKTG_CONFIGURATION_DEFAULTBOUNCE_DESC", "Enter the default bounce address to use for Messages.");
define("_EMKTG_CONFIGURATION_BOUNCE_SERVER", "Bounce Mail Server");
define("_EMKTG_CONFIGURATION_BOUNCE_SERVER_DESC", "Enter the address of the mail server that received the mail for the "._EMKTG_CONFIGURATION_DEFAULTBOUNCE.".");
define("_EMKTG_CONFIGURATION_BOUNCE_PORT", "Bounce Mail Server Port");
define("_EMKTG_CONFIGURATION_BOUNCE_PORT_DESC", "Port on the "._EMKTG_CONFIGURATION_BOUNCE_SERVER." to use when processing the "._EMKTG_CONFIGURATION_DEFAULTBOUNCE.".&lt;br /&gt; Leave blank to use the default.");
define("_EMKTG_CONFIGURATION_BOUNCE_SERVICE", "Bounce Mail Server Type");
define("_EMKTG_CONFIGURATION_BOUNCE_SERVICE_DESC", "Select the type of mail server used for "._EMKTG_CONFIGURATION_BOUNCE.".");
define("_EMKTG_CONFIGURATION_BOUNCE_USERNAME", "Bounce Mail Username");
define("_EMKTG_CONFIGURATION_BOUNCE_USERNAME_DESC", "Username to use when connecting to the "._EMKTG_CONFIGURATION_BOUNCE.".");
define("_EMKTG_CONFIGURATION_BOUNCE_PASSWORD", "Bounce Mail Password");
define("_EMKTG_CONFIGURATION_BOUNCE_PASSWORD_DESC", "Password to use when connecting to the "._EMKTG_CONFIGURATION_BOUNCE.".");
define("_EMKTG_CONFIGURATION_BOUNCE_MODE", "Bounce Mode");
define("_EMKTG_CONFIGURATION_BOUNCE_MODE_TEST", "Test");
define("_EMKTG_CONFIGURATION_BOUNCE_MODE_BOUNCE", "Bounce");
define("_EMKTG_CONFIGURATION_BOUNCE_MODE_ALL", "All");
define("_EMKTG_CONFIGURATION_BOUNCE_MODE_DESC", "Run "._EMKTG_CONFIGURATION_BOUNCE." as&lt;dl%gt;&lt;dt&gt;"._EMKTG_CONFIGURATION_BOUNCE_MODE_TEST."&lt;/dt&gt;&lt;dd&gt;"._EMKTG_CONFIGURATION_BOUNCE." will not disable any email addresses or delete any messages from the "._EMKTG_CONFIGURATION_BOUNCE_SERVER.".&lt;/dd&gt;&lt;dt&gt;"._EMKTG_CONFIGURATION_BOUNCE_MODE_BOUNCE."&lt/dt&gt;&lt;dd&gt;"._EMKTG_CONFIGURATION_BOUNCE." will only delete bounce messages from the "._EMKTG_CONFIGURATION_BOUNCE_SERVER.".&lt;/dd&gt;&lt;dt&gt;"._EMKTG_CONFIGURATION_BOUNCE_MODE_ALL."&lt;/dt&gt;&lt;dd&gt;"._EMKTG_CONFIGURATION_BOUNCE." will delete all messages from the "._EMKTG_CONFIGURATION_BOUNCE_SERVER.".&lt;/dd&gt;&lt;/dl&gt;");
define("_EMKTG_CONFIGURATION_BOUNCE_UNSUBSCRIBE", "Bounce Unsubscribe");
define("_EMKTG_CONFIGURATION_BOUNCE_UNSUBSCRIBE_DESC", "Number of times an email is allowed to bounce before it is auto unsubscribed. 0 means never unsubscribed.");
define("_EMKTG_CONFIGURATION_BLACKLIST_OPTIONS", "BlackList Options");
define("_EMKTG_CONFIGURATION_BLACKLIST", "BlackList");
define("_EMKTG_CONFIGURATION_BLACKLIST_DESC", "List of Email address to never send to. One per line and use * as wildcard. i.e. *@hotmail.com");
define("_EMKTG_CONFIGURATION_WHITELIST", "WhiteList");
define("_EMKTG_CONFIGURATION_WHITELIST_DESC", "List of Email addresses to always send to. One per line.");
define("_EMKTG_CONFIGURATION_BLACKLIST_UNSUBSCRIBE", "BlackList Unsubscribe");
define("_EMKTG_CONFIGURATION_BLACKLIST_UNSUBSCRIBE_DESC", "Automatically unsubscribe email addresses if they are found to be one the blacklist. Also refuse subscription to blacklisted addresses?");
define("_EMKTG_CONFIGURATION_BLACKLIST_QUEUE", "BlackList Queue");
define("_EMKTG_CONFIGURATION_BLACKLIST_QUEUE_DESC", "Do not add email addresses to the Queue if they are blacklisted. Note this will not process the Blacklist Unsubscribe correctly.");
define("_EMKTG_CONFIGURATION_CONFIRMATION_OPTIONS", "Confirmation Options");
define("_EMKTG_CONFIGURATION_SUBMESSAGE", "Subscribe message");
define("_EMKTG_UNSUBSCRIBE_SUBMESSAGE", "Unsubscribe text");
define("_EMKTG_UNSUBSCRIBE_SUBMESSAGE_DESC", "Enter the text to display for the unsubscribe link.");
define("_EMKTG_CONFIGURATION_CONFIRMFROMNAME", "Confirm from name:");
define("_EMKTG_CONFIGURATION_CONFIRMFROMNAME_DESC", "Enter the from name to display on confirmation letters.");
define("_EMKTG_CONFIGURATION_CONFIRMFROMEMAIL", "Confirm from email:");
define("_EMKTG_CONFIGURATION_CONFIRMFROMEMAIL_DESC", "Enter the email address to display on confirmation letters.");
define("_EMKTG_CONFIGURATION_CONFIRMBOUNCE", "Confirm bounce address:");
define("_EMKTG_CONFIGURATION_CONFIRMBOUNCE_DESC", "Enter the bounce address to display on confirmation letters.");
define('_EMKTG_CONFIGURATION_UNSUBSCRIBE_ADMIN_OPTIONS', 'Unsubscribe Admin Email');
define('_EMKTG_CONFIGURATION_UNSUBSCRIBE_ADMIN_EMAIL', 'Send Unsubscribe Email to Admin');
define('_EMKTG_CONFIGURATION_UNSUBSCRIBE_ADMIN_EMAIL_DESC', 'When a user is unsubscribed should an email be sent to the site Administrator?');
define('_EMKTG_CONFIGURATION_UNSUBSCRIBE_ADMIN_ADMIN', 'Send on Admin Unsubscribe');
define('_EMKTG_CONFIGURATION_UNSUBSCRIBE_ADMIN_ADMIN_DESC', 'When an Administrator unsubscribes a user should the email still be sent?');
define('_EMKTG_CONFIGURATION_UNSUBSCRIBE_ADMIN_SUBJECT', 'Unsubscribe Admin Subject');
define('_EMKTG_CONFIGURATION_UNSUBSCRIBE_ADMIN_SUBJECT_DESC', 'Subject of the message that is sent to the Administrator.');
define('_EMKTG_CONFIGURATION_UNSUBSCRIBE_ADMIN_SUBJECT_DEFAULT', 'User Unsubscribed');
define('_EMKTG_CONFIGURATION_UNSUBSCRIBE_ADMIN_MSG', 'Unsubscribe Admin Message');
define('_EMKTG_CONFIGURATION_UNSUBSCRIBE_ADMIN_MSG_DESC', 'Body of the message sent to the administrator&lt;br /&gt;The following tags are allowed:&lt;dl&gt;&lt;dt&gt;[NAME]&lt;/dt&gt;&lt;dd&gt;The name of the person who unsubscribed.&lt;/dd&gt;&lt;dt&gt;[EMAIL]&lt;/dt&gt;&lt;dd&gt;Email address of the person who unsubscribed.&lt;/dd&gt;&lt;dt&gt;[REASON]&lt;/dt&gt;&lt;dd&gt;Reason the person was unsubscribed.&lt;/dd&gt;&lt;dt&gt;[TIME]&lt;/dt&gt;&lt;dd&gt;Date / Time that the person unsubscribed.&lt;/dd&gt;&lt;/dl&gt;');

define("_EMKTG_CONFIGURATION_UPDATED", "Configuration Updated");

define("_EMK    TG_USABLETAGS", "Usable tags:");

define("_EMKTG_HELP_GENERAL", "General");
define("_EMKTG_HELP_TEMPLATE", "Template");
define("_EMKTG_HELP_LIST", "List");
define("_EMKTG_HELP_REGISTERED", "Client Mgmt");
define("_EMKTG_HELP_UNREGISTERED", "List-only");
define("_EMKTG_HELP_MESSAGE", "Message");
define("_EMKTG_HELP_QUEUE", "Queue");
define("_EMKTG_HELP_STATISTICS", "Statistics");
define("_EMKTG_HELP_PLUGINS", "Plugins");

define("_EMKTG_LOREM", "<h3>Lorem ipsum dolor sit amet</h3> <p>consectetuer adipiscing elit. Nam tempus, leo at lacinia tempor, magna sapien nonummy urna, at aliquet massa sapien ut ligula. </p><ul><li>Fusce faucibus nunc ornare massa.</li><li>Duis porta tortor eget felis.</li></ul><p>Curabitur eleifend mattis lacus. In laoreet faucibus dui. Mauris ac arcu in tellus egestas pulvinar. Aliquam ornare aliquam pede.</p><ol><li>Aliquam vulputate scelerisque tortor.</li><li>Suspendisse potenti.</li></ol><p>Nam interdum. Phasellus nec dolor. Vestibulum vel felis in tellus fermentum aliquet. Ut fermentum tortor sed eros. Praesent sed metus id nisi volutpat dictum. Morbi ornare, sapien id volutpat mollis, lacus enim placerat magna, <a href=\"#\">a pellentesque nisi lectus gravida nisi</a>.</p>");
define("_EMKTG_PREVIEW_NAME", "SM2 Email Marketing User");

// Quick Tips language:
define("_EMKTG_QUICKTIP", "Quick Tips");
define("_EMKTG_QUICKTIP_TEMPLATE_PREVIEW", "Please note that the text displayed here is used to simply show you how the content may look when you send a message using this template.<br />
    			We advise that you preview the actual message before sending AND by sending yourself a test message.");
define("_EMKTG_QUICKTIP_LIST", "A list allows subscribers to choose which newsletters they will receive. <br />Use the form below to create a new list and give ample description so that subscribers know what will be included in these newsletters.");
define("_EMKTG_QUICKTIP_UNREGISTERED", "Below is a subscription profile for a user who only gave their email address.<br />
		You can change their subscription options, including which lists they are subscribed to. <br />
		Keep in mind, however, that if you subscribe a user to a list without their consent you can be held liable for spamming the user.");
define('_EMKTG_QUICKTIP_IMPORT_1', '<h3>Import Step 1</h3>Select a CSV Format file to Import.<br />
CSV file requires the following fields<ul><li>Name</li><li>Email</li></ul>
Optional fields<ul><li>Receive HTML</li><li>Lists</li></ul><br />
CSV file should have the following format<ul><li>Field Title in first record</li><li>Fields Comma seperated</li><li>Fields &#34; enclosed</li><li>Records terminated by newline</li></ul>
<h4>Notes</h4>
<h5>Optional Fields</h5>
In Step 2 if an optional field is not found in the CSV file then the option exists to preset the value to an existing value. i.e. if no lists are included in the CSV option to be able to have all records imported added to 1 or more existing lists.
<br />
<h5>Receive HTML field</h5>
A value of 0 in this field will indicate TEXT, any other value equates to HTML
<br />
<h5>Lists fields</h5>
Can contain a pipe | seperated list of the Lists that the user is to be subscribed to. If the List does not exist it will be created. Make sure spelling is correct or multiple lists will be created. Lists can be spread across multiple fields if required.');
define('_EMKTG_QUICKTIP_IMPORt_2', '<h3>Import Step 2</h3>
<p>Select the fields to Import from.<br />
For each of the listed SM2 Email Marketing List Subscriber fields select a field from the list
that will be used to import the data.</p>
The fields able to be imported are
<h5>Name</h5>Select the field from the list that contains the Subscribers name.
<h5>Email</h5>Select the field from the list that contains the Subscribers email address.
<h5>Receive HTML</h5>Select the default Receive HTML value and then if required select the field from the list that contains the Subscribers Receive HTML option.
<h5>Lists</h5>Select the default Lists the Subscriber should be added to if any. Then If required select the fieldls that contains the Lists the Subscriber should belong to. Hold down CTRL to select multiple.
<br />
<h5>Note</h5>
It is assumed that all of the Email addresses to be imported are valid and unique. All List Subscribers
imported will be automatically confirmed. If an email address already exists in the List Subscribers then
that user will not be imported.');

define("_EMKTG_QUICKTIP_REGISTERED", "Below is a subscription profile for a user who gave more details that just their email address.<br />
		You can change their subscription options, including which lists they are subscribed to. <br />
		Keep in mind, however, that if you subscribe a user to a list without their consent you can be held liable for spamming the user.<br />
		(<em>You can change their username and/or password using the client management component.</em>)");

// Front-end component language:
define("_EMKTG_SUBSCRIBE_CONFIRMATION", "Subscription Confirmation");
define("_EMKTG_SUBSCRIBE_CLICKTOCONFIRM", "Click Here to confirm that you wish to subscribe");
define("_EMKTG_SUBSCRIBE_EMAILADDED", '<div class="big">
Thanks for choosing to register with Menorca Holidays Direct
</div>
<p>
Thanks for signing up for our <span class="highlight-orange">free newsletter</span><br />
We will contact you no more than once a month with details of newly listed properties, special offers and Menorca news.
</p>
<p>Please keep your eyes peeled for our sister sites which will be coming soon!</p>
<div class="highlight-orange" style="font-weight: bold">
	<div style="float: left; width: 22%">
	<ul>
		<li>Mallorca Holidays Direct</li>
		<li>Ibiza Holidays Direct</li>
		<li>Spain Holidays Direct</li>
		<li>Lanzarote Holidays Direct</li>
		<li>Fuerteventura Holidays Direct</li>
	</ul>
	</div>
	<div style="float: left; width: 22%">
	<ul>
		<li>Tenerife Holidays Direct</li>
		<li>Gran Canaria Holidays Direct</li>
		<li>France Holidays Direct</li>
		<li>Greece Holidays Direct</li>
		<li>Italy Holidays Direct</li>
	</ul>
	</div>
	<div style="float: left; width: 22%">
	<ul>
		<li>Portugal Holidays Direct</li>
		<li>Bulgaria Holidays Direct</li>
		<li>Croatia Holidays Direct</li>
		<li>England Holidays Direct</li>
		<li>Ireland Holidays Direct</li>
	</ul>
	</div>
</div>
<div style="float: left; width: 33%">
<img src="http://menorcaholidaysdirect.siteincubator.co.uk/images/stories/suitcases.gif" alt="suitcases.gif" title="suitcases.gif" style="margin: 5px; float: right; width: 287px; height: 113px" height="113" width="287" /><br />
<div align="right" style="clear: right">
* We promise not to contact you more than once a month, and won\'t pass your details to anyone else.<br />
</div>
</div>
');
define("_EMKTG_SUBSCRIBE_ERRORSENDINGMSG", "There was an error sending the message");
define("_EMKTG_SUBSCRIBE_MSGSENDSUCCESS", '<div class="big">
Thanks for choosing to register with Menorca Holidays Direct
</div>
<p>
Thanks for signing up for our <span class="highlight-orange">free newsletter</span><br />
We will contact you no more than once a month with details of newly listed properties, special offers and Menorca news.
</p>
<p>Please keep your eyes peeled for our sister sites which will be coming soon!</p>
<div class="highlight-orange" style="font-weight: bold">
	<div style="float: left; width: 22%">
	<ul>
		<li>Mallorca Holidays Direct</li>
		<li>Ibiza Holidays Direct</li>
		<li>Spain Holidays Direct</li>
		<li>Lanzarote Holidays Direct</li>
		<li>Fuerteventura Holidays Direct</li>
	</ul>
	</div>
	<div style="float: left; width: 22%">
	<ul>
		<li>Tenerife Holidays Direct</li>
		<li>Gran Canaria Holidays Direct</li>
		<li>France Holidays Direct</li>
		<li>Greece Holidays Direct</li>
		<li>Italy Holidays Direct</li>
	</ul>
	</div>
	<div style="float: left; width: 22%">
	<ul>
		<li>Portugal Holidays Direct</li>
		<li>Bulgaria Holidays Direct</li>
		<li>Croatia Holidays Direct</li>
		<li>England Holidays Direct</li>
		<li>Ireland Holidays Direct</li>
	</ul>
	</div>
</div>
<div style="float: left; width: 33%">
<img src="http://menorcaholidaysdirect.siteincubator.co.uk/images/stories/suitcases.gif" alt="suitcases.gif" title="suitcases.gif" style="margin: 5px; float: right; width: 287px; height: 113px" height="113" width="287" /><br />
<div align="right" style="clear: right">
* We promise not to contact you more than once a month, and won\'t pass your details to anyone else.<br />
</div>
</div>');
define("_EMKTG_SUBSCRIBE_ISUSER", "Your email is stored as a registered user, please login to manage your mailing lists subscriptions");

define("_EMKTG_UNSUBSCRIBE_CONFIRMATION", "Unsubscription Confirmation");
define("_EMKTG_UNSUBSCRIBE_CLICKTOCONFIRM", "Click Here to confirm that you wish to unsubscribe");
define("_EMKTG_UNSUBSCRIBE_ERRORSENDINGMSG", "There was an error sending the message");
define("_EMKTG_UNSUBSCRIBE_MSGSENDSUCCESS", "Message was sent successfully");
define("_EMKTG_UNSUBSCRIBE_SUCCESS", "You have been unsubscribed.");
define("_EMKTG_UNSUBSCRIBE_MAILINGLIST", "You have been unsubscribed from the selected lists.");
define("_EMKTG_UNSUBSCRIBE_NOTFOUND", "Sorry, your email address was not found, therefore we could not unsubscribe you");
define("_EMKTG_UNSUBSCRIBE_ERRORREMOVING", "Sorry, there was a problem removing your email address");

define('_EMKTG_UNSUBSCRIBE_STATUS', 'Status');
define('_EMKTG_UNSUBSCRIBE_REASON_NULL', 'Not Subscribed');
define('_EMKTG_UNSUBSCRIBE_REASON_0', 'Subscribed');
define('_EMKTG_UNSUBSCRIBE_REASON_1', 'User Unsubscribed');
define('_EMKTG_UNSUBSCRIBE_REASON_2', 'Error Email');
define('_EMKTG_UNSUBSCRIBE_REASON_3', 'Blacklisted');
define('_EMKTG_UNSUBSCRIBE_REASON_4', 'Bounce Unsubscribed');
define('_EMKTG_UNSUBSCRIBE_REASON_5', 'Admin Unsubscribed');

define('_EMKTG_RESUBSCRIBE', 'Re-Subscribe');
define('_EMKTG_RESUBSCRIBE_MSG_1','Warning! This account was previously unsubscribed by the user. Re-subscribing this user without their express permission could be considered spamming. Are you sure you wish to re-subscribe?');
define('_EMKTG_RESUBSCRIBE_MSG_2','Warning! This account was previously unsubscribed because the email address was found to be an error. Are you sure you wish to re-subscribe?');
define('_EMKTG_RESUBSCRIBE_MSG_3','Warning! This account was previously unsubscribed because the email address was found to be blacklisted. Are you sure you wish to re-subscribe?');
define('_EMKTG_RESUBSCRIBE_MSG_4','Warning! This account was previously unsubscribed because the email address was bounced. Are you sure you wish to re-subscribe?');
define('_EMKTG_RESUBSCRIBE_MSG_5','Warning! This account was previously unsubscribed by an Administrator. Are you sure you wish to re-subscribe?');

define('_EMKTG_RESUBSCRIBE_ERROR_2','This account was unsubscribed because the email address was found to be an error. Please contact an administrator to re-subscribe this account.');
define('_EMKTG_RESUBSCRIBE_ERROR_3','This account was unsubscribed because the email address was found to be blacklisted. You cannot re-subscribe at this time.');
define('_EMKTG_RESUBSCRIBE_ERROR_4','This account was unsubscribed because the email address has bounced. Please contact an administrator to re-subscribe this account.');
define('_EMKTG_RESUBSCRIBE_ERROR_5','This account was unsubscribed by an Administrator. Please contact an administrator to re-subscribe this account.');

define("_EMKTG_CONFIRMATION_CONFIRMSUCCESS", "You have been confirmed");
define("_EMKTG_CONFIRMATION_CONFIRMERROR", "Sorry, there was a problem confirming your subscription");
define("_EMKTG_CONFIRMATION_NOTFOUND", "Sorry, we couldn't find your email address in our database");


// Module language:

define("_EMKTG_MODULE_HTML", "HTML");
define("_EMKTG_MODULE_TEXT", "Text");
define("_EMKTG_MODULE_NAME", "Name");
define("_EMKTG_MODULE_EMAIL", "Email");
define("_EMKTG_MODULE_SUBSCRIBE", "Subscribe");
define("_EMKTG_MODULE_UNSUBSCRIBE", "Unsubscribe");
define("_EMKTG_MODULE_MAIL_FORMAT", "Mail Format");
define("_EMKTG_MODULE_ACTION", "I want to");
define("_EMKTG_MODULE_MAILINGLIST", "Mailing List");
define("_EMKTG_MODULE_SUBMIT", "Submit");
define("_EMKTG_MODULE_ERRNAME", "You need to tell us your name");
define("_EMKTG_MODULE_ERREMAIL", "You need to provide your email address");
define("_EMKTG_MODULE_ERROR", "Sorry, there are some errors!");

// default data
define("_EMKTG_INSTALL_DEFAULT", "Install Default Data");
define("_EMKTG_INSTALL_DEFAULT_DONE", "The default data has been installed.<br />A default Template and Message have been created.");
define("_EMKTG_INSTALL_DEFAULT_FAILED", "The default data has not been installed.<br /> It may have already existed.");

define("_EMKTG_DEFAULT_TEMPLATE_NAME", "Demo Template");
define("_EMKTG_DEFAULT_TEMPLATE_TEXT", '');

define("_EMKTG_DEFAULT_MESSAGE_SUBJECT", "Demo Message");
define("_EMKTG_DEFAULT_MESSAGE_TEXT", "Dear [NAME],

SM2 Email Marketing is a Newsletter / Mailing List component that supports many features not seen in Joomla Newsletter components.

What is SM2 Email Marketing?
- an affordable email marketing solution that takes care of all your online needs.

How can SM2 Email Marketing help me?
- Reduce marketing and operations costs
- Allow you to target you Marketing to very specific users or groups
- Increase sales &amp; open new opportunities
- Enhance your image
- Better service your customers

To see how SM2 Email Marketing works try our online demo site [http://demo.sm2joomla.com/] or download the free demo and install it on your own Joomla site to see the punch that SM2 Email Marketing really packs!");
define("_EMKTG_DEFAULT_MESSAGE_HTML", '<p>Dear [NAME],</p><p>
SM2 Email Marketing is a Newsletter / Mailing List component that supports many features not seen in Joomla Newsletter components.
</p>
<h4>What is SM2 Email Marketing?</h4>
<ul>
    <li>an affordable email marketing solution that takes care of all your online needs.</li>
</ul>
<h4>How can SM2 Email Marketing help me? </h4>
<ul>
    <li>Reduce marketing and operations costs</li>
    <li>Allow you to target you Marketing to very specific users or groups </li>
    <li>Increase sales &amp; open new opportunities</li>
    <li>Enhance your image</li>
    <li>Better service your customers</li>
</ul>
<p>
To see how SM2 Email Marketing works try our <a href="http://demo.sm2joomla.com/">online demo site </a>or download the free demo and install it on your own Joomla site to see the punch that SM2 Email Marketing really packs!
</p>
');

// licene response codes
define("_EMKTG_LICENCE_0", "Unable to read licence key file.");
define("_EMKTG_LICENCE_1", "");
define("_EMKTG_LICENCE_2", "Key may have been tampered with.");
define("_EMKTG_LICENCE_3", "Key may have been tampered with.");
define("_EMKTG_LICENCE_4", "Licence Key does not match Key file.");
define("_EMKTG_LICENCE_5", "Licence has expired.");
define("_EMKTG_LICENCE_6", "Host name does not match Key file.");
define("_EMKTG_LICENCE_7", "IP does not match Key file.");
define("_EMKTG_LICENCE_8", "Licence Disabled.");
define("_EMKTG_LICENCE_9", "Licence Suspended.");
define("_EMKTG_LICENCE_10", "Unable to open Key file for writing.");
define("_EMKTG_LICENCE_11", "Unable to write to Key file.");
define("_EMKTG_LICENCE_12", "Unable to communicate with SM2 Joomla.");
define("_EMKTG_LICENCE_SM2_PLUG", '<br />Click <a href="http://sm2extensions.com/">here</a> to find more about purchasing the full version of SM2 Email Marketing and remove these restrictions.');
define("_EMKTG_LICENCE_LITE_PLUGIN", "Only the first plugin can be enabled when using SM2 Email Marketing Lite.");
define("_EMKTG_LICENCE_LITE_MESSAGE", "Only the first 100 emails found will receive the Message when using SM2 Email Marketing Lite.");
define("_EMKTG_LICENCE_DEMO_RESTRICTED", "Note: Demo version is restricted so that only Message Preview Test Messages can be sent. Use the Message Preview to send the Test Message.");
define("_EMKTG_LICENCE_DEMO_RESTRICT_SEC", 60);
define("_EMKTG_LICENCE_DEMO_RESTRICT_SEC_WARNING", "Test Message could not be sent. This version is restricted so that a Test Message can only be sent every "._EMKTG_LICENCE_DEMO_RESTRICT_SEC." seconds.");

// languages additions 2007-06-06
define('_EMKTG_PRIV_SET', '<p>To ensure that SM2 Email Marketing can function correctly please either make sure that the Joomla! database user has the `CREATE TEMPORARY TABLE` privilege, or change the <strong>Temporary Table Type</strong> to <em>Session</em> in the SM2 Email Marketing Configuration. Contact your hosting provider if you do not know how to set the CREATE TEMPORARY TABLE permission.</p>');
define('_EMKTG_PRIV_UNKNOWN', '<p>Unable to determine if the `CREATE TEMPORARY TABLE` permission has been set.</p>'._EMKTG_PRIV_SET);
define('_EMKTG_PRIV_UNSET', '<p>`CREATE TEMPORARY TABLE` permission has not been set.</p>'._EMKTG_PRIV_SET);
define('_EMKTG_LIST_NUM_SUBSCRIBERS', '# List<br />Subscribers');
define('_EMKTG_LIST_NUM_SUBSCRIBERS_DESC', 'Number of List Subscribers. Does not include Unsubscribed or Unconfirmed subscribers.');
define('_EMKTG_LIST_TOTAL_SUBSCRIBERS', 'Total List Subscribers');
define('_EMKTG_LIST_TOTAL_SUBSCRIBERS_DESC', 'Total number of List Subscribers. Does not include Unsubscribed or Unconfirmed subscribers.');
define('_EMKTG_LIST_NUM_USERS', '# Client<br />Subscribers');
define('_EMKTG_LIST_NUM_USERS_DESC', 'Numer of Client Managed Subscribers. Does not include Unsubscribed or Unconfirmed Joomla! users.');
define('_EMKTG_LIST_TOTAL_USERS', 'Total Client Subscribers');
define('_EMKTG_LIST_TOTAL_USERS_DESC', 'Total numer of Client Managed Subscribers. Does not include Unsubscribed or Unconfirmed Joomla! users.');
define('_EMKTG_LIST_NO_SUBSCRIBERS', 'No Subscribers');
define('_EMKTG_TOTAL_SUBSCRIBERS', 'Total number of Subscribers');
define('_EMKTG_SUBSCRIBERS', 'Subscribers');
define('_EMKTG_MESSAGE_PREVIEW_NO_RECIPIENTS', 'No Recipient Data available');
define('_EMKTG_FILTER_RECEIVE_HTML_TEXT', '[ All Receive Options ]');
define('_EMKTG_FILTER_CONFIRMED_TEXT', '[ All Confirmed Options ]');
define('_EMKTG_FILTER_LIST_TEXT', '[ All Lists ]');
define('_EMKTG_FILTER_NOLIST_TEXT', '-- No List subscriptions --');
define('_EMKTG_FILTER_SUBSCRIBED_TEXT', '[ All Subscriptions ]');
define('_EMKTG_MESSAGE_FRONT_EMPTY', 'No messages could be found to view.');
define('_EMKTG_LIST_SUBSCRIBE_ALL', 'Subscribe All');
define('_EMKTG_LIST_SUBSCRIBE_ALL_DESC', 'Selecting this will subscribe all Client Managed and List Subscribers to this List. Note: this could be considered Spamming when adding Subscribers to a list without their permission.');
define('_EMKTG_LIST_SUBSCRIBE_ALL_CONFIRM','Are you sure you wish to add all Subscribers to this list? It may be considered spamming.');
define('_EMKTG_LIST_SUBSCRIBE_ALL_DONE', 'All Subscribers add to list: %s (%d added)');
define('_EMKTG_LIST_SUBSCRIBE_ALL_ERROR', 'Error adding Subscribers to list: %s');
define('_EMKTG_LIST_SUBSCRIBE_NONE', 'Subscribe None');
define('_EMKTG_LIST_SUBSCRIBE_NONE_DESC', 'Selecting this will remove all Client Managed and List Subscribers from this List. Note: any messages still in the queue sending to subscribers of this list will not be affected.');
define('_EMKTG_LIST_SUBSCRIBE_NONE_CONFIRM','Are you sure you wish to remove all Subscribers from this list?');
define('_EMKTG_LIST_SUBSCRIBE_NONE_DONE', 'All Subscribers removed from list: %s (%d removed)');
define('_EMKTG_LIST_SUBSCRIBE_NONE_ERROR', 'Error removing Subscribers to list: %s');
define('_EMKTG_HELP_BOUNCES', 'Bounces');

// languages additions 1.0.6.0
define('_EMKTG_CONFIGURATION_TEMP_TABLES', 'Temporary Table Type');
define('_EMKTG_CONFIGURATION_TEMP_TABLES_DESC', 'The Type of Temporary Tables to use.&lt;p&gt;Temporary tables use MySQL TEMPORARY tables and are the fastest to process and no management is required, however the database must have the CREATE TEMPORARY TABLE permission.&lt;/p&gt;&lt;p&gt;Session tables create real MySQL tables named after the session identifier but are slower because they must be deleted after they are used.&lt;/p&gt;Note: In some special circumstances the Session table may not be deleted correctly.');
define('_EMKTG_CONFIGURATION_TEMP_TABLES_TEMPORARY', 'Temporary');
define('_EMKTG_CONFIGURATION_TEMP_TABLES_SESSION', 'Session');
define('_EMKTG_CONFIGURATION_IMAP', 'imap');
define('_EMKTG_CONFIGURATION_POP3', 'pop3');
define('_EMKTG_CONFIGURATION_CONVERT_ARTICLE', 'Convert Content Items to Text');
define('_EMKTG_CONFIGURATION_CONVERT_ARTICLE_DESC', 'System will attempt to convert Content Items, to be include, to Text. Attempts to strip tags from the HTML to leave only text. If No then no Content Items will be displayed in Text version of Message.');
define('_EMKTG_TOOLS_FORM', 'Tools');
define('_EMKTG_TOOLS_INSTALL_DEFAULT', 'Install Default Message and Template');
define('_EMKTG_TOOLS_INSTALL_DEFAULT_DESC', 'Installs a Default Message and Template that can be used as a starting point for your own Messages and Templates.');
define('_EMKTG_TOOLS_CLEAN_TEMP', 'Cleanup Temporary Tables');
define('_EMKTG_TOOLS_CLEAN_TEMP_DESC', 'Removes any Temporary Session tables that are no longer needed.');
define('_EMKTG_TOOLS_IMPORT_SUBSCRIBERS', 'Import List Subscribers');
define('_EMKTG_TOOLS_IMPORT_SUBSCRIBERS_DESC', 'The Import List Subscriber wizard used to import subscribers from a CSV file.');
define('_EMKTG_TOOLS_CLEAN_FORM', 'Cleanup Temporary Tables');
define('_EMKTG_TOOLS_CLEAN_NO_TABLES','No Temporary Tables found to clean');
define('_EMKTG_TOOLS_CLEAN_TABLES','Cleaned the following Temporary Tables:');
define('_EMKTG_UNREGISTERED_IMPORT_DUP', 'Subscriber already exists: %n [%e]<br />Subscribing lists');

// languages additions 1.0.6.2
define('_EMKTG_TOOLS_DELETE_NOTICE', 'Most of the Delete actions will also delete Statistics and Queued Messages. Statistics are relient on so much other information that they will not display correctly otherwise.');
define('_EMKTG_TOOLS_DELETE_MESSAGES', 'Delete all Messages');
define('_EMKTG_TOOLS_DELETE_MESSAGES_DESC', 'Deletes all Messages, Statistics and Queued Messages');
define('_EMKTG_TOOLS_DELETE_MESSAGES_CONFIRM', 'Are you sure you wish to delete all Messages?\nThis will also delete all Statistics and Queued Messages.');
define('_EMKTG_TOOLS_DELETE_STATISTICS', 'Delete all Statistics');
define('_EMKTG_TOOLS_DELETE_STATISTICS_DESC', 'Deletes all Statistics and Queued Messages. All existing messages will show as sent but will have no statistics.');
define('_EMKTG_TOOLS_DELETE_STATISTICS_CONFIRM', 'Are you sure you wish to delete all Statistics?\nThis will also delete all Queued Messages.');
define('_EMKTG_TOOLS_DELETE_SUBSCRIBERS', 'Delete all Subscribers');
define('_EMKTG_TOOLS_DELETE_SUBSCRIBERS_DESC', 'Deletes all Subscribers and Statistics.');
define('_EMKTG_TOOLS_DELETE_SUBSCRIBERS_CONFIRM', 'Are you sure you wish to delete all Subscribers?\nThis will also delete all Statistics.');
define('_EMKTG_TOOLS_DELETE_LISTS', 'Delete all Lists');
define('_EMKTG_TOOLS_DELETE_LISTS_DESC', 'Deletes all Lists, Subscribers, Statistics and Queued Messages.');
define('_EMKTG_TOOLS_DELETE_LISTS_CONFIRM', 'Are you sure you wish to delete all Lists?\nThis will also delete all Subscribers, Statistics and Queued Messages.');
define('_EMKTG_TOOLS_DELETE_ALL', 'Delete All');
define('_EMKTG_TOOLS_DELETE_ALL_DESC', 'Deletes All SM2 Email Marketing data. This is the equivelent to a fresh install. Note that an uninstall of SM2 Email Marketing will not usually delete the data.');
define('_EMKTG_TOOLS_DELETE_ALL_CONFIRM', 'Are you sure you wish to delete all SM2 Email Marketing data?');
define('_EMKTG_TOOLS_DELETE_FAILED', 'Failed to delete data from %t tables');
define('_EMKTG_TOOLS_DELETE_SUCCESS', 'Deleted all data from %t tables');
define('_EMKTG_TOOLS_DELETE_UNKNOWN', 'Nothing was selected to delete');
define('_EMKTG_CONFIGURATION_WORD_WRAP', 'Word Wrap');
define('_EMKTG_CONFIGURATION_WORD_WRAP_DESC', 'Word Wrap the body of the message to the specified number of characters. 0 means no word wrapping.&lt;br /&gt;&lt;strong&gt;BETA&lt;/strong&gt;&lt;br /&gt;Useful if mail servers truncate messages with long lines, as Joomla! content editor often includes all content on one line');
define('_EMKTG_QUICKTIP_MESSAGE_CONTENT_TAGS', '<p>Include either an <code>[ITEMS]</code> tags, which places all of the selected content items, or use the <code>[ARTICLE]...[/ARTICLE]</code> block as detailed below.<p>
<p>
<code>[ARTICLE_TITLE]</code> - Title of the Article. Contains a named anchor (in HTML messages only) so that internal links will work.<br />
<code>[ARTICLE_SECTION]</code> - Name of the Joomla! Section that the article belongs to.<br />
<code>[ARTICLE_CATEGORY]</code> - Name of the Joomla! Category that the article belongs to.<br />
<code>[ARTICLE_INTRO]</code> - Intro Text of the Content Item.<br />
<code>[ARTICLE_FULL]</code> - Full Text of the Content Item. This does NOT include the Intro text. Static Content Items do not have Full Text. Do NOT include this if you want Intro Only option.<br />
<code>[ARTICLE_URL]</code> - URL to the Content Item on the Website. For use in a link.<br />
<code>[ARTICLE_READMORE]</code> - Fully formatted anchor that uses the Joomla! standard Read More text to create a link to the Content Item on the website.<br />
</p>
<p>
These tags must be inside an <code>[ARTICLE] ... [/ARTICLE]</code> block.<br />
i.e.<br />
<code>[ARTICLE]<br />
&lt;h1&gt;[ARTICLE_TITLE]&lt;/h1&gt;<br />
[ARTICLE_INTRO]<br />
[ARTICLE_READMORE]<br />
[/ARTICLE]
</code>
</p>
<p>Only use one such <code>[ARTICLE]</code> block in the message content as it will be replace for each event found.</p>
<p>You can also create a Table of Contents using the <code>[ARTICLE_TOC]...[/ARTICLE_TOC]</code> block.</p>
<p>
<code>[ARTICLE_TOC_TITLE]</code> - Title of the Article.<br />
<code>[ARTICLE_TOC_LINK]</code> - URL to the named anchor of the Article. Used to go to the article in the message.<br />
<code>[ARTICLE_TOC_ITEM]</code> - Combines the [ARTICLE_TOC_LINK] and [ARTICLE_TOC_TITLE] into a single linked Article title.
</p>
<p>These tags must be inside an <code>[ARTICLE_TOC] ... [/ARTICLE_TOC]</code> block.<br />
i.e.<br />
<code>
&lt;ul&gt;<br />
[ARTICLE_TOC]<br />
&lt;li&gt;[ARTICLE_TOC_ITEM]&lt;/li&gt;<br />
[/ARTICLE_TOC]<br />
&lt;/ul&gt;<br />
</code>
</p>');
define('_EMKTG_CONFIGURATION_SUBMESSAGE_DESC', 'Enter the text to display for the subscribe message. This is the email that is sent to the user when they subscribe.&lt;br /&gt;Use the [CONFIRM] tag to place the confirmation link. If no tag is entered it will be added to the end of the message.');
define('_EMKTG_CONFIGURATION_SEF', 'SEF Links');
define('_EMKTG_CONFIGURATION_SEF_DESC', 'Use Joomla! front end SEF processing for links. Requires Joomla! Global Configuration to be SEF enabled.&lt;br /&gt;&lt;strong&gt;BETA&lt;/strong&gt;&lt;br /&gt;May not work correctly with custom SEF components.');
define('_EMKTG_QUICKTIP_TEMPLATE', 'The template is the "wrapper" (look and feel) for your messages. This is achieved using web-languages such as XHTML/CSS. Unless you have a solid knowledge of these languages and their affect on email clients, it is best that this is left to the experts.<br /><br />
Use [BASEURL] to automatically insert the sites url (http://www.example.com)<br />
Use [CONTENT] to determine where the content of the message will go<br />
Use [UNSUBSCRIBE] to display a unique unsubscribe link for each user (which is set in the config for this component).<br />
Use [DATE] to display the date that the email is sent out (i.e. 12/08/2006).<br />
Use [TIME] to display the time that the email is sent out (i.e. 1:59pm).<br />
Use [SUBJECT] to display the subject line of the email.<br />
USE [ARCHIVE_URL] to display the url of the front end message display. This should be used in a link.');
define('_EMKTG_CONFIGURATION_EMAIL_LIMIT', 'Email Limit');
define('_EMKTG_CONFIGURATION_EMAIL_LIMIT_DESC', 'The Server Email Limit. When processing the queue it will stop processing after it has reached the Email Limit, if it has not finished the queue or reached the script timeout.&lt;br /&gt;Use this  to ensure you do not exceed your Server Email Limit.&lt;br /&gt; Recommend that you set to 80% - 90% of server limit. Setting to 80% - 90% of server limit means you can still send other emails if required.&lt;br /&gt;Use the hourly limit if one exists, otherwise use the daily limit.&lt;br /&gt;Once the limit is reset by the server, i.e. hourly or daily, go back into the Queue Manager and continue processing the queue.&lt;br /&gt;A value of 0 means no limit.');
define('_EMKTG_EMAIL_LIMIT_REACHED', 'The Server Email Limit has been reached for this queue. You may continue processing this queue but you will most likely exceed your server limit and the emails with not be sent. Please wait until the server limit resets and continue processing.');

// languages addition / changes 1.0.6.7
define('_EMKTG_CONFIRMATION_SECURITYCHECKFAILED', 'Sorry, security check failed. You may need to login to complete this action.');

// languages additions 1.0.7.0
define('_EMKTG_FORWARD', 'Forward');
define('_EMKTG_CONFIGURATION_FORWARDING', 'Mesage Forwarding');
define('_EMKTG_CONFIGURATION_ENABLE_FORWARD', 'Enable Message Forwarding');
define('_EMKTG_CONFIGURATION_ENABLE_FORWARD_DESC', 'Message Forwarding allows a link to be placed in the message that can be used to forward the Message to someone else. Link takes the subscriber to a form on the site where the details of who the message is to be forward to are entered.');
define('_EMKTG_CONFIGURATION_FORWARD_TEXT', 'Forward Link Text');
define('_EMKTG_CONFIGURATION_FORWARD_TEXT_DESC', 'Enter the text to display for the Forward link');
define('_EMKTG_CONFIGURATION_FORWARD_DAYS', 'Forward Days');
define('_EMKTG_CONFIGURATION_FORWARD_DAYS_DESC', 'Number of days after the Message is sent that forwarding is allowed. 0 means forever.');
define('_EMKTG_CONFIGURATION_FORWARD_SUBJECT', 'Forward Email Subject');
define('_EMKTG_CONFIGURATION_FORWARD_SUBJECT_DESC', 'Subject of the Message sent for forwarding.');
define('_EMKTG_CONFIGURATION_FORWARD_SUBJECT_DEFAULT', 'FW: [SUBJECT]');
define('_EMKTG_CONFIGURATION_FORWARD_MSG','Forward Email Message');
define('_EMKTG_CONFIGURATION_FORWARD_MSG_DESC','Content of the Forward Message.&lt;br /&gt;&lt;br /&gt;Can contain the following tags:&lt;br /&gt;[FORWARD_NAME] - Name of the person being forwarded to.&lt;br /&gt;[FORWARD_EMAIL] - Email address of the person being forwarded to.&lt;br /&gt;[FROM_NAME] - Name of the person being forwarded from.&lt;br /&gt;[FROM_EMAIL] - Email of the person being forwarded from.&lt;br /&gt;[NOTE] - Personal Message from the person being forwarded from.&lt;br /&gt;[SUBJECT] subject of the original message.&lt;br /&gt;[CONTENT] - Content of the original message.&lt;br /&gt;[ARCHIVE_URL] - URL to the archive of the message.&lt;br /&gt;&lt;br /&gt;Note: If no [CONTENT] or [ARCHIVE_URL] tag is found the content will be as an attachment.');
define('_EMKTG_CONFIGURATION_FORWARD_MSG_DEFAULT','[FORWARD_NAME],&lt;br /&gt;&lt;br /&gt;[FROM] thought you might be interested in this message.&lt;br /&gt;&lt;br /&gt;Here is what they had to say:&lt;br /&gt;[NOTE]&lt;br /&gt;&lt;br /&gt;You can view the message here:&lt;br /&gt;[ARCHIVE_URL]&lt;br /&gt;&lt;br /&gt;Thank you.');
define('_EMKTG_FORWARD_LINK_TEXT', 'Forward this message to someone');
define('_EMKTG_FORWARD_PRETEXT', 'Please enter the details of the person you would like to forward this message to:');
define('_EMKTG_FORWARD_PERSONAL_MESSAGE', 'Personal Message');
define('_EMKTG_FORWARD_MESSAGE_FORMAT', 'Message Format');
define('_EMKTG_FORWARD_MESSAGE_FORMAT_TEXT', 'The message will be sent as Text');
define('_EMKTG_FORWARD_MESSAGE_FORMAT_HTML', 'The message will be sent as HTML');
define('_EMKTG_FORWARD_MESSAGE_CONTENT', 'Message Content');
define('_EMKTG_FORWARD_MESSAGE_CONTENT_LINK', 'The message will contain a link to the Message Archive.');
define('_EMKTG_FORWARD_MESSAGE_CONTENT_MESSAGE', 'The message will contain the Original Email');
define('_EMKTG_FORWARD_MESSAGE_CONTENT_ATTACHMENT', 'The message will contain the Original Email as an Attachment');
define('_EMKTG_FORWARD_JS_ERROR', 'The following fields are required:');
define('_EMKTG_FORWARD_LINK_ERROR_TITLE', 'Forward Error');
define('_EMKTG_FORWARD_LINK_ERROR_DESC', 'The link provided is invalid. Please ensure that you copy the link correctly from your email. If the link is across 2 lines it may need to be pasted into the location as 1 line not 2. Please try again.');
define('_EMKTG_FORWARD_EXPIRE_ERROR_TITLE', 'Forward Error');
define('_EMKTG_FORWARD_EXPIRE_ERROR_DESC', 'The forward link is too old and has been expired. Sorry.');
define('_EMKTG_FORWARD_SEND_ERROR_TITLE', 'Forward Error');
define('_EMKTG_FORWARD_UNKNOWN_ERROR', 'Unknown error in processing the forward. Please try again later.');
define('_EMKTG_FORWARD_DUPLICATE_ERROR', 'The recipient is already subscribed and has received this message.');
define('_EMKTG_FORWARD_SEND_ERROR', 'There was an error whilst sending the forward. The email address you supplied may be incorrect or the email server may be unavailable. Please try again later.');
define('_EMKTG_FORWARD_SENT', 'Forward was successfully sent.');
define('_EMKTG_CONFIGURATION_UNSUBSCRIBE_ADMIN_MSG_DEFAULT', 'User with the following details unsubscribed&lt;br /&gt;Name: [NAME]&lt;br /&gt;Email: [EMAIL]&lt;br /&gt;Reason: [REASON]&lt;br /&gt;Time: [TIME]');
define('_EMKTG_CONFIGURATION_QUICK_REGISTERED_UNSUBSCRIBE','Quick Registered Unsubscribe');
define('_EMKTG_CONFIGURATION_QUICK_REGISTERED_UNSUBSCRIBE_DESC','When a Registered Subscriber attempts to unsubscribe should they be able to unsubscribe Quickly without having to log in first?&lt;br /&gt;If No they will need to log in to unsubscribe.&lt;br /&gt;If Yes they can unsubscribe quickly without logging in');
define('_EMKTG_QUICKTIP_MESSAGE', '<p>This is where you create, edit, preview and publish your newsletters.<br />
    To <strong>preview</strong>, select the Preview icon from the tool bar above.<br />
    To <strong>send</strong>, ensure you have finalised the content and filters, then select the Send icon from the tool bar above. Keep in mind that this WILL send the email to all recipients.<br /><br />
    You can also include the following tags to personalise your message further<br />
    <code>[NAME]</code> - This will be the name of the person the email is sent to (i.e. Joe Bloggs)<br />
    <code>[DATE]</code> - The current date (i.e. 12/08/2006)<br />
    <code>[TIME]</code> - The current time, written in 12-hour time-format (i.e. 1:52pm)<br />
    <code>[SUBJECT]</code> - The subject line of the message<br />
    <code>[DETAILS]</code> - Username and password. This will only work under certain conditions and will renew user\'s username and password. Please consult with SM2 before using this tag.<br />
    <code>[ARCHIVE_URL]</code> will display the url of the front end message display. This should be used in a link.<br />
    <code>[FORWARD]</code> will create the forwarding link allowing the message to be forwarded to someone else. Note: Forwarding must be enabled in the config and the link is only created in final send not in Preview or Test Message.
</p>'._EMKTG_QUICKTIP_MESSAGE_CONTENT_TAGS);

// languages additions 1.0.8.0
define('_EMKTG_MESSAGE_FINISH', 'Preview and Send');
define('_EMKTG_MESSAGE_FINISH_DESC', 'Select <strong>Preview</strong> from the toolbar to Preview the message, with the option to <strong>Send</strong> or <strong>Send a Test</strong>.<br/><br/>');
define('_EMKTG_DEFAULT_TEMPLATE_HTML', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Generic Newsletter</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <meta http-equiv="Content-Language" content="EN" />
    <style type="text/css">
        * { margin: 0; padding: 0; }
        body { font-family: Arial, Helvetica, sans-serif;   font-size: 11px; background: #97c61d; }
        #container { position: relative; width: auto !important; width: 600px; max-width: 600px; margin: 0 auto; background-color: #FFFFFF; }
        #masthead { padding: 41px 0 0 23px; height: 150px; background: url([BASEURL]/images/sm2emailmarketing/mastheadBg.gif) no-repeat 0 80px; }
        #infoBar { position: absolute; top: 50px; right: 20px; text-align: right; }
        #infoBar p.subtitle { font-size: 14px; text-transform: uppercase; color: #3e6f1b; }
        #infoBar p.date { margin-top: 40px; font-size: 14px; text-transform: uppercase; color: #FFF; }
        #content { position: relative; padding: 10px 20px; line-height: 17px; }
        #content h3 {
            padding: 5px 0;
            color: #3e6f1b;
            border-bottom: 1px solid #e8e5e5;
            font-size: 16px;
            font-weight: bold;
            margin: 0px;
        }
        #content p { margin: 15px 0; }
        #content ul, #content ol { margin-left: 15px; }
        #content ul { list-style: outside url([BASEURL]/images/sm2emailmarketing/bullet.gif); }
        #content a { color: #97c61d; font-weight: bold; }
        #footer { position: relative; padding: 4px 10px; color: #878082; background: #ececec; }
        #footer p { margin: 2px 0 0 0; }
        #footer a { color: #97c61d; font-weight: bold; }
        .clearing { clear: both; }
    </style>
</head>
<body>
    <div id="container">
        <div id="masthead">
            <!-- Top Headings -->
            <h1><img src="[BASEURL]/images/sm2emailmarketing/logo.gif" width="243" height="32" /></h1>
        </div>
        <div id="infoBar">
            <p class="subtitle">Newsletter</p>
            <p class="date">[DATE]</p>
        </div>
        <div id="content">

            <div class="wrapper">
                [CONTENT]
                <br class="clearing" />
            </div>
        </div>
        <div id="footer">
            <p>Click <a href="[ARCHIVE_URL]" target="_blank">here</a> to view a online version of this message.</p>
            <p>&copy; Generic Company 2006.</p>
            <p>Generic Company distributes this on-line newsletter regularly for it\'s members.
                You are receiving this issue as a registered subscriber of this service. If
                you have received this mailing in error, or do not wish to receive any further
                newsletters please manage your subscription. [UNSUBSCRIBE]</p>
        </div>
    </div>
</body>
</html>');
define('_EMKTG_MESSAGE_PREVIEW_HTML', 'HMTL Message');
define('_EMKTG_MESSAGE_PREVIEW_TEXT', 'Text Message');
define('_EMKTG_MESSAGE_PREVIEW_RECIPIENTS', 'Recipients');
define('_EMKTG_HELP_CONFIG', 'Config');
define('_EMKTG_HELP_LICENCE', 'Licence');

// languages additions 1.0.8.2
define('_EMKTG_CONFIGURATION_SPOOF', 'Enable Spoof Protection');
define('_EMKTG_CONFIGURATION_SPOOF_DESC', 'Stops robots from creating fraudulent subscriptions.');
define('_EMKTG_CONFIGURATION_USEEMBEDDED', 'Use Embedded Images.');
define('_EMKTG_CONFIGURATION_USEEMBEDDED_DESC', 'Select yes if the images in the content should be embedded in the email for html messages, or no to use default image tags that link to the images on the site.');
define('_EMKTG_CONFIGURATION_EMBEDDEDCSS', 'Embed CSS Images');
define('_EMKTG_CONFIGURATION_EMBEDDEDCSS_DESC', 'If using Embedded Images, this will attempt to embed any images in inline CSS styles. This may not be supported by email clients and images may just appear as attachments.');

/**#@-*/
