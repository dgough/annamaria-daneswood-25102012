<?php
/**
* @version $Id: registration.html.php 5928 2006-12-06 00:49:07Z friesengeist $
* @package Joomla
* @subpackage Users
* @copyright Copyright (C) 2005 Open Source Matters. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );

/**
* @package Joomla
* @subpackage Users
*/
class HTML_registration {
	function lostPassForm($option) {
		global $mosConfig_uniquemail, $mainframe, $mosConfig_live_site;
		
		//Add stylesheet
		$mainframe->addCustomheadTag("<link type='text/css' rel='stylesheet' href='$mosConfig_live_site/components/com_login/styles.css' />");
	
		// used for spoof hardening
		$validate = josSpoofValue();
		?>
		<div id='lost_password'>
			<form action="index.php" method="post">
				<div class="background" style="background: url(<?php echo $mosConfig_live_site ?>/components/com_login/lock.gif) no-repeat top right">					
					<h1 class="componentheading"><?php echo _PROMPT_PASSWORD; ?></h1>
					<div class="lost_password">
						<div class='row' id='pass_desc'><?php echo _NEW_PASS_DESC; ?></div>
						<?php if(!$mosConfig_uniquemail){ ?>
						<div class='row' id='pass_multiple'><?php echo _NEWPASS_MULTIPLE; ?></div>
						<?php } ?>
						<div class="row">
							<label><?php echo _PROMPT_EMAIL; ?></label>
							<input type="text" name="confirmEmail" class="inputbox" size="25" />
						</div>
			            <div class='row'><label>&nbsp; </label> <input type="submit" class="button" value="<?php echo _BUTTON_SEND_PASS; ?>" /></div>
			        </div>
					<div style='clear: both'></div>
					<input type="hidden" name="option" value="<?php echo $option;?>" />
					<input type="hidden" name="task" value="sendNewPass" /> 
					<input type="hidden" name="<?php echo $validate; ?>" value="1" />
				</div>
			</form>
		</div>
		<?php
	}

	function registerForm($option, $useractivation) {
		global $mainframe, $mosConfig_live_site;
	
		//Add stylesheet
		$mainframe->addCustomheadTag("<link type='text/css' rel='stylesheet' href='$mosConfig_live_site/components/com_login/styles.css' />");
		
		// used for spoof hardening
		$validate = josSpoofValue();
		?>
		<script language="javascript" type="text/javascript">
		//<!--
		function submitbutton_reg() {
			var form = document.mosForm;
			var r = new RegExp("[\<|\>|\"|\'|\%|\;|\(|\)|\&|\+|\-]", "i");

			// do field validation
			if (form.name.value == "") {
				alert( "<?php echo addslashes( html_entity_decode(_REGWARN_NAME) );?>" );
			} else if (form.username.value == "") {
				alert( "<?php echo addslashes( html_entity_decode(_REGWARN_UNAME) );?>" );
			} else if (r.exec(form.username.value) || form.username.value.length < 3) {
				alert( "<?php printf( addslashes( html_entity_decode(_VALID_AZ09_USER) ), addslashes( html_entity_decode(_PROMPT_UNAME) ), 2 );?>" );
			} else if (form.email.value == "") {
				alert( "<?php echo addslashes( html_entity_decode(_REGWARN_MAIL) );?>" );
			} else if (form.password.value.length < 6) {
				alert( "<?php echo addslashes( html_entity_decode(_REGWARN_PASS) );?>" );
			} else if (form.password2.value == "") {
				alert( "<?php echo addslashes( html_entity_decode(_REGWARN_VPASS1) );?>" );
			} else if ((form.password.value != "") && (form.password.value != form.password2.value)){
				alert( "<?php echo addslashes( html_entity_decode(_REGWARN_VPASS2) );?>" );
			} else if (r.exec(form.password.value)) {
				alert( "<?php printf( addslashes( html_entity_decode(_VALID_AZ09) ), addslashes( html_entity_decode(_REGISTER_PASS) ), 6 );?>" );
			} else {
				form.submit();
			}
		}
		//-->
		</script>
        <div id="registration_form">
            <form action="index.php" method="post" name="mosForm">
    			<div class="background" style="background: url(<?php echo $mosConfig_live_site ?>/components/com_login/lock.gif) no-repeat top right">		            
		            <h1 class="componentheading"><?php echo _REGISTER_TITLE; ?></h1>		            
		            <div id="req"><?php echo _REGISTER_REQUIRED; ?></div>		            
		            <div class='row'><label><?php echo _REGISTER_NAME; ?> *</label> <input type="text" name="name" size="40" value="" class="inputbox" maxlength="50" /></div>		            
		            <div class='row'><label><?php echo _REGISTER_UNAME; ?> *</label> <input type="text" name="username" size="40" value="" class="inputbox" maxlength="25" /></div>		            
		            <div class='row'><label><?php echo _REGISTER_EMAIL; ?> *</label> <input type="text" name="email" size="40" value="" class="inputbox" maxlength="100" /></div>		    
		            <div class='row'><label><?php echo _REGISTER_PASS; ?> *</label> <input class="inputbox" type="password" name="password" size="40" value="" /></div>		    
		            <div class='row'><label><?php echo _REGISTER_VPASS; ?> *</label> <input class="inputbox" type="password" name="password2" size="40" value="" /></div>		    		
		            <input type="hidden" name="id" value="0" />
		            <input type="hidden" name="gid" value="0" />
		            <input type="hidden" name="useractivation" value="<?php echo $useractivation;?>" />
		            <input type="hidden" name="option" value="<?php echo $option; ?>" />
		            <input type="hidden" name="task" value="saveRegistration" />
		            <!-- value="<?php echo _BUTTON_SEND_REG; ?>" -->
		            <div style='clear: left'> 
		            	<input type="button" value="<?php echo _BUTTON_SEND_REG; ?>" class="button" onclick="submitbutton_reg()" />
		            </div>
		            <input type="hidden" name="<?php echo $validate; ?>" value="1" />
		            <div style='clear: left'></div>
            	</div>
            </form>
        </div>
		<?php
	}
}
?>