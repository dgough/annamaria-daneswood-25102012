<?php
/**
* @version $Id: content.html.php 5928 2006-12-06 00:49:07Z friesengeist $
* @package Joomla
* @subpackage Content
* @copyright Copyright (C) 2005 Open Source Matters. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );

/**
* Utility class for writing the HTML for content
* @package Joomla
* @subpackage Content
*/
class HTML_content {
	/**
	* Draws a Content List
	* Used by Content Category & Content Section
	*/
	function showContentList( $title, &$items, &$access, $id=0, $sectionid=NULL, $gid, &$params, &$pageNav, $other_categories, &$lists, $order, $categories_exist, $section = '' ) {
		global $Itemid, $mosConfig_live_site, $_MAMBOTS;

		if ( $sectionid ) {
			$id = $sectionid;
		}

		if ( strtolower(get_class( $title )) == 'mossection' ) {
			$catid = 0;
		} else {
			$catid = $title->id;
		}
		
		if(!empty($section)){
			$sectionTitle = $section->title;
			$categoryTitle = $title->title;
		}else{
			$sectionTitle = $title->title;
			$categoryTitle = '';
		}

		if ( $params->get( 'page_title' ) ) {
			// ------------------------ A8E fixes start ------------------------
			?>
			<h1 class="componentheading<?php echo $params->get( 'pageclass_sfx' ); ?> category_head">
			<?php 
			if ( $params->get('description_image') && $title->image ) {
				$link = $mosConfig_live_site .'/images/stories/'. $title->image;
				?>
				<img src="<?php echo $link;?>" align="<?php echo $title->image_position;?>" alt="<?php echo $title->image;?>" />
				<?php
			}
			echo $title->name; ?>
			</h1> 
			<?php 
			// ------------------------ A8E fixes end ------------------------
		}
		// process the new bots
		$_MAMBOTS->loadBotGroup( 'content' );
		$title->text = $title->description;
		$results = $_MAMBOTS->trigger( 'onPrepareContent', array( &$title, &$params, 0 ), true );
		$title->description = $title->text;

		// ------------------------ A8E fixes start ------------------------
		if ( $params->get('description') || $params->get('description_image') ) {
			if ( $params->get('description') ) {
				echo $title->description;
			}
			
		}
			
			// Displays the Table of Items in Category View
			if ( $items ) {
				if($params->layout){
					HTML_content::showTable( $params, $items, $gid, $catid, $id, $pageNav, $access, $sectionid, $lists, $order );
				}else{
					HTML_content::showArticles( $params, $items, $gid, $catid, $id, $Itemid, $sectionTitle, $categoryTitle );
				}
			} else if ( $catid ) {
				?>
				<br />
				<?php echo _EMPTY_CATEGORY; ?>
				<br /><br />
				<?php
			}
			// New Content Icon
			if ( ( $access->canEdit || $access->canEditOwn ) && $categories_exist ) {
				$link = sefRelToAbs( 'index.php?option=com_content&amp;task=new&amp;sectionid='. $id .'&amp;Itemid='. $Itemid );
				?>
				<a href="<?php echo $link; ?>">
					<img src="<?php echo $mosConfig_live_site;?>/images/M_images/new.png" width="13" height="14" align="middle" border="0" alt="<?php echo _CMN_NEW;?>" />
						&nbsp;<?php echo _CMN_NEW;?>...</a>
				<br /><br />
				<?php
			}
			?>				
			<?php
			// Displays listing of Categories
			if ( ( ( count( $other_categories ) > 1 ) || ( count( $other_categories ) < 2 && count( $items ) < 1 ) ) ) {
				if ( ( $params->get( 'type' ) == 'category' ) && $params->get( 'other_cat' ) ) {
					HTML_content::showCategories( $params, $items, $gid, $other_categories, $catid, $id, $Itemid, $sectionTitle, $categoryTitle );
				}
				if ( ( $params->get( 'type' ) == 'section' ) && $params->get( 'other_cat_section' ) ) {
					HTML_content::showCategories( $params, $items, $gid, $other_categories, $catid, $id, $Itemid, $sectionTitle, $categoryTitle );
				}
			}
			?>
		<?php
		//$mainframe->setPageTitle( $title->title );
		if($params->get('metadesc','')) $mainframe->addMetaTag( 'description', 	$params->get('metadesc','') );
		if($params->get('metakey',''))  $mainframe->addMetaTag( 'keywords', 	$params->get('metakey','') );
		// ------------------------ A8E fixes end ------------------------
		// displays back button
		mosHTML::BackButton ( $params );
	}

/**
	* Display links to categories
	*/
	function showArticles( &$params, &$items, $gid, $catid, $id, $Itemid, $sectionTitle, $categoryTitle ) {
		// ------------------------ A8E fixes start ------------------------
		// TODO: remove asBlog functionality, as this is not an accessibility fix
		$title = $sectionTitle ? " section_".str_replace(" ", "_", $sectionTitle) : '';
		$title .= $categoryTitle ? " category_".str_replace(" ", "_", $categoryTitle) : '';
		?>
		<ul class="articleList<?php echo $title ?>">
		<?php

		foreach ( $items as $row ) {
			$row->title = htmlspecialchars( stripslashes( ampReplace( $row->title ) ), ENT_QUOTES );
			$image = "<img src='$mosConfig_live_site/images/stories$row->images' alt='$row->title' style='vertical-align: middle' />&nbsp;";
				?>
				<li>
					<?php
					if ( $row->access <= $gid ) {
						$link = sefRelToAbs( 'index.php?option=com_content&amp;task=view&amp;id='. $row->id .'&amp;Itemid='. $Itemid );
						?><?php if(!empty($row->images)) echo $image ?>
						<a href="<?php echo $link; ?>" class="article">						
							<?php echo $row->title;?></a>
						<?php
					} else {
						echo $row->title;
						?>
						<a href="<?php echo sefRelToAbs( 'index.php?option=com_registration&amp;task=register' ); ?>">
							( <?php echo _E_REGISTERED; ?> )</a>
						<?php
					}
					?>
				</li>
				<?php
			
		}
		?>
		</ul>
		<?php
		// ------------------------ A8E fixes end ------------------------
	}	

	/**
	* Display links to categories
	*/
	function showCategories( &$params, &$items, $gid, &$other_categories, $catid, $id, $Itemid ) {
		// ------------------------ A8E fixes start ------------------------
		// TODO: remove asBlog functionality, as this is not an accessibility fix
		$asBlog = ($params->get('category_as_blog') ? 'blog' : '');
		?>
		<ul class="categorylist">
		<?php
		foreach ( $other_categories as $row ) {
			$row->name = htmlspecialchars( stripslashes( ampReplace( $row->name ) ), ENT_QUOTES );
			if ( $catid != $row->id ) {
				?>
				<li>
					<?php
					if ( $row->access <= $gid ) {
						$link = sefRelToAbs( 'index.php?option=com_content&amp;task=category&amp;sectionid='. $id .'&amp;id='. $row->id .'&amp;Itemid='. $Itemid );
						?>
						<a href="<?php echo $link; ?>" class="category">
							<?php echo $row->name;?></a>
						<?php
						if ( $params->get( 'cat_items' ) ) {
							?>
							&nbsp;<span class="nrofitems">( <?php echo $row->numitems; echo _CHECKED_IN_ITEMS;?> )</span>
							<?php
						}

						// Writes Category Description
						if ( $params->get( 'cat_description' ) && $row->description ) {
							?>
							<br />
							<?php
							echo $row->description;
						}
					} else {
						echo $row->name;
						?>
						<a href="<?php echo sefRelToAbs( 'index.php?option=com_registration&amp;task=register' ); ?>">
							( <?php echo _E_REGISTERED; ?> )</a>
						<?php
					}
					?>
				</li>
				<?php
			}
		}
		?>
		</ul>
		<?php
		// ------------------------ A8E fixes end ------------------------
	}


	/**
	* Display Table of items
	*/
	function showTable( &$params, &$items, &$gid, $catid, $id, &$pageNav, &$access, &$sectionid, &$lists, $order ) {
		global $mosConfig_live_site, $Itemid;
		$link = 'index.php?option=com_content&amp;task=category&amp;sectionid='. $sectionid .'&amp;id='. $catid .'&amp;Itemid='. $Itemid;
		?>
		<form action="<?php echo sefRelToAbs($link); ?>" method="post" name="adminForm">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<?php
		if ( $params->get( 'filter' ) || $params->get( 'order_select' ) || $params->get( 'display' ) ) {
			?>
			<tr>
				<td colspan="4">
					<table>
					<tr>
						<?php
						if ( $params->get( 'filter' ) ) {
							?>
							<td align="right" width="100%" nowrap="nowrap">
								<?php
								echo _FILTER .'&nbsp;';
								?>
								<input type="text" name="filter" value="<?php echo $lists['filter'];?>" class="inputbox" onchange="document.adminForm.submit();" />
							</td>
							<?php
						}

						if ( $params->get( 'order_select' ) ) {
							?>
							<td align="right" width="100%" nowrap="nowrap">
								<?php
								echo '&nbsp;&nbsp;&nbsp;'. _ORDER_DROPDOWN .'&nbsp;';
								echo $lists['order'];
								?>
							</td>
							<?php
						}

						if ( $params->get( 'display' ) ) {
							?>
							<td align="right" width="100%" nowrap="nowrap">
								<?php
								$order = '';
								if ( $lists['order_value'] ) {
									$order = '&amp;order='. $lists['order_value'];
								}
								$filter = '';
								if ( $lists['filter'] ) {
									$filter = '&amp;filter='. $lists['filter'];
								}

								$link = 'index.php?option=com_content&amp;task=category&amp;sectionid='. $sectionid .'&amp;id='. $catid .'&amp;Itemid='. $Itemid . $order . $filter;

								echo '&nbsp;&nbsp;&nbsp;'. _PN_DISPLAY_NR .'&nbsp;';
								echo $pageNav->getLimitBox( $link );
								?>
							</td>
							<?php
						}
						?>
					</tr>
					</table>
				</td>
			</tr>
			<?php
		}

		if ( $params->get( 'headings' ) ) {
			?>
			<tr>
				<?php
				if ( $params->get( 'date' ) ) {
					?>
					<td class="sectiontableheader<?php echo $params->get( 'pageclass_sfx' ); ?>" width="35%">
						<?php echo _DATE; ?>
					</td>
					<?php
				}
				if ( $params->get( 'title' ) ) {
					?>
					<td class="sectiontableheader<?php echo $params->get( 'pageclass_sfx' ); ?>">
						<?php echo _HEADER_TITLE; ?>
					</td>
					<?php
				}
				if ( $params->get( 'author' ) ) {
					?>
					<td class="sectiontableheader<?php echo $params->get( 'pageclass_sfx' ); ?>" align="left" width="25%">
						<?php echo _HEADER_AUTHOR; ?>
					</td>
					<?php
				}
				if ( $params->get( 'hits' ) ) {
					?>
					<td align="center" class="sectiontableheader<?php echo $params->get( 'pageclass_sfx' ); ?>" width="5%">
						<?php echo _HEADER_HITS; ?>
					</td>
					<?php
				}
				?>
			</tr>
			<?php
		}

		$k = 0;
		foreach ( $items as $row ) {
			$row->created = mosFormatDate ($row->created, $params->get( 'date_format' ));

			// calculate Itemid
			HTML_content::_Itemid( $row );
			?>
			<tr class="sectiontableentry<?php echo ($k+1) . $params->get( 'pageclass_sfx' ); ?>" >
				<?php
				if ( $params->get( 'date' ) ) {
					?>
					<td>
					<?php echo $row->created; ?>
					</td>
					<?php
				}
				if ( $params->get( 'title' ) ) {
					if( $row->access <= $gid ){
						$link = sefRelToAbs( 'index.php?option=com_content&amp;task=view&amp;id='. $row->id .'&amp;Itemid='. $Itemid );
						?>
						<td>
						<a href="<?php echo $link; ?>">
						<?php echo $row->title; ?>
						</a>
						<?php
						HTML_content::EditIcon( $row, $params, $access );
						?>
						</td>
						<?php
					} else {
						?>
						<td>
						<?php
						echo $row->title .' : ';
						$link = sefRelToAbs( 'index.php?option=com_registration&amp;task=register' );
						?>
						<a href="<?php echo $link; ?>">
						<?php echo _READ_MORE_REGISTER; ?>
						</a>
						</td>
						<?php
					}
				}
				if ( $params->get( 'author' ) ) {
					?>
					<td align="left">
					<?php echo $row->created_by_alias ? $row->created_by_alias : $row->author; ?>
					</td>
					<?php
				}
				if ( $params->get( 'hits' ) ) {
				?>
					<td align="center">
					<?php echo $row->hits ? $row->hits : '-'; ?>
					</td>
				<?php
			} ?>
		</tr>
		<?php
			$k = 1 - $k;
		}
		if ( $params->get( 'navigation' ) ) {
			?>
			<tr>
				<td colspan="4">&nbsp;</td>
			</tr>
			<tr>
				<td align="center" colspan="4" class="sectiontablefooter<?php echo $params->get( 'pageclass_sfx' ); ?>">
				<?php
				$order = '';
				if ( $lists['order_value'] ) {
					$order = '&amp;order='. $lists['order_value'];
				}
				$filter = '';
				if ( $lists['filter'] ) {
					$filter = '&amp;filter='. $lists['filter'];
				}

				$link = 'index.php?option=com_content&amp;task=category&amp;sectionid='. $sectionid .'&amp;id='. $catid .'&amp;Itemid='. $Itemid . $order . $filter;
				echo $pageNav->writePagesLinks( $link );
				?>
				</td>
			</tr>
			<tr>
				<td colspan="4" align="right">
				<?php echo $pageNav->writePagesCounter(); ?>
				</td>
			</tr>
			<?php
		}
		?>
		</table>
		<input type="hidden" name="id" value="<?php echo $catid; ?>" />
		<input type="hidden" name="sectionid" value="<?php echo $sectionid; ?>" />
		<input type="hidden" name="task" value="<?php echo $lists['task']; ?>" />
		<input type="hidden" name="option" value="com_content" />
		</form>
		<?php
	}


	/**
	* Display links to content items
	*/
	function showLinks( &$rows, $links, $total, $i=0, $show=1, $ItemidCount=NULL ) {
		global $mainframe, $Itemid;

		// getItemid compatibility mode, holds maintenance version number
		$compat = (int) $mainframe->getCfg('itemid_compat');

		if ( $show ) {
			?>
			<div>
				<strong>
				<?php echo _MORE; ?>
				</strong>
			</div>
			<?php
		}
		?>
		<ul>
		<?php
		for ( $z = 0; $z < $links; $z++ ) {
			if (!isset( $rows[$i] )) {
				// stops loop if total number of items is less than the number set to display as intro + leading
				break;
			}

			if ($compat > 0 && $compat <= 11) {
				$_Itemid = $mainframe->getItemid( $rows[$i]->id, 0, 0  );
			} else {
			$_Itemid = $Itemid;
			}

			if ( $_Itemid && $_Itemid != 99999999 ) {
			// where Itemid value is returned, do not add Itemid to url
				$Itemid_link = '&amp;Itemid='. $_Itemid;
			} else {
			// where Itemid value is NOT returned, do not add Itemid to url
				$Itemid_link = '';
			}

			$link = sefRelToAbs( 'index.php?option=com_content&amp;task=view&amp;id='. $rows[$i]->id . $Itemid_link )
			?>
			<li>
				<a class="blogsection" href="<?php echo $link; ?>">
					<?php echo $rows[$i]->title; ?></a>
			</li>
			<?php
			$i++;
		}
		?>
		</ul>
		<?php
	}


	/**
	* Show a content item
	* @param object An object with the record data
	* @param boolean If <code>false</code>, the print button links to a popup window.  If <code>true</code> then the print button invokes the browser print method.
	*/
	function show( &$row, &$params, &$access, $page=0 ) {
		global $mainframe, $hide_js;
		global $mosConfig_live_site;
		global $_MAMBOTS;

		$mainframe->appendMetaTag( 'description', 	$row->metadesc );
		$mainframe->appendMetaTag( 'keywords', 		$row->metakey );

		// adds mospagebreak heading or title to <site> Title
		if ( isset($row->page_title) && $row->page_title ) {
			$mainframe->setPageTitle( $row->title .' '. $row->page_title );
		}

		// calculate Itemid
		HTML_content::_Itemid( $row );

		// determines the link and `link text` of the readmore button & linked title
		HTML_content::_linkInfo( $row, $params );

		// link used by print button
		$print_link = $mosConfig_live_site. '/index2.php?option=com_content&amp;task=view&amp;id=' . $row->id .'&amp;pop=1&amp;page='. $page . $row->Itemid_link;

		// process the new bots
		$_MAMBOTS->loadBotGroup( 'content' );
		$results = $_MAMBOTS->trigger( 'onPrepareContent', array( &$row, &$params, $page ), true );

		// ------------------------ A8E fixes start ------------------------
		?>
		<div class="contentitem<?php echo $params->get( 'pageclass_sfx' ); ?>">
		<?php
		if ( $params->get( 'item_title' ) || $params->get( 'pdf' )  || $params->get( 'print' ) || $params->get( 'email' ) ) {
			// link used by print button
			// a8ejoomla-patch-1.0.12 fixed by snoboJohan (17 jan 2007) // $print_link = $mosConfig_live_site. '/index2.php?option=com_content&task=view&id='. $row->id .'&Itemid='. $Itemid .'&pop=1&page='. @$page;
			$print_link = $mosConfig_live_site. '/index2.php?option=com_content&amp;task=view&amp;id=' . $row->id .'&amp;pop=1&amp;page='. $page . $row->Itemid_link;
			
				// displays Item Title
				HTML_content::Title( $row, $params, $access );

				// displays PDF Icon
				if(!isset($_MAMBOTS->_content_mambot_params['printPdfEmail'])){
					?>
					<div class="icons">
					<?PHP
					HTML_content::PdfIcon( $row, $params, $hide_js );
	
					// displays Print Icon
					mosHTML::PrintIcon( $row, $params, $hide_js, $print_link );
	
					// displays Email Icon
					HTML_content::EmailIcon( $row, $params, $hide_js );
					?>
					</div>
					<?php
				}
 		} else if ( $access->canEdit ) {
 			// edit icon when item title set to hide
			echo("<div  class=\"buttonheading\">");
				HTML_content::EditIcon( $row, $params, $access );
			echo("</div>");
  		}

		if ( !$params->get( 'intro_only' ) ) {
			$results = $_MAMBOTS->trigger( 'onAfterDisplayTitle', array( &$row, &$params, $page ) );
			echo trim( implode( "\n", $results ) );
		}

		$results = $_MAMBOTS->trigger( 'onBeforeDisplayContent', array( &$row, &$params, $page ) );
		echo trim( implode( "\n", $results ) );

		// displays Section & Category
		HTML_content::Section_Category( $row, $params );

		// A8E: Group credentials so they can be styled better.
		if ($credentials = ($params->get( 'author' ) || $params->get('createdate' )) ) {
			?>
			<div class="credentials">
			<?php
		}
		// displays Author Name
		HTML_content::Author( $row, $params );

		// displays Created Date
		HTML_content::CreateDate( $row, $params );

		if ($credentials) {
			?>
			</div>
			<?php
		}
		
		// displays Urls
		HTML_content::URL( $row, $params );

			// displays Table of Contents
			HTML_content::TOC( $row );

			// displays Item Text
			echo ampReplace( $row->text );

		// displays Modified Date
		HTML_content::ModifiedDate( $row, $params );

		// displays Readmore button
		HTML_content::ReadMore( $row, $params );

		$results = $_MAMBOTS->trigger( 'onAfterDisplayContent', array( &$row, &$params, $page ) );
		echo trim( implode( "\n", $results ) );

		// displays the next & previous buttons
		HTML_content::Navigation ( $row, $params );

		// displays close button in pop-up window
		mosHTML::CloseButton ( $params, $hide_js );

		// displays back button in pop-up window
		mosHTML::BackButton ( $params, $hide_js );
		?>
		</div>
		<?php
		// ------------------------ A8E fixes end ------------------------
	}

	/**
	* calculate Itemid
	*/
	function _Itemid( &$row ) {
		global $task, $Itemid, $mainframe;

		// getItemid compatibility mode, holds maintenance version number
		$compat = (int) $mainframe->getCfg('itemid_compat');

		if ( ($compat > 0 && $compat <= 11) && $task != 'view' && $task != 'category' ) {
			$row->_Itemid = $mainframe->getItemid( $row->id, 0, 0 );
		} else {
			// when viewing a content item, it is not necessary to calculate the Itemid
		$row->_Itemid = $Itemid;
		}
		
		if ( $row->_Itemid && $row->_Itemid != 99999999 ) {
			// where Itemid value is returned, do not add Itemid to url
			$row->Itemid_link = '&amp;Itemid='. $row->_Itemid;
		} else {
			// where Itemid value is NOT returned, do not add Itemid to url
			$row->Itemid_link = '';
		}
	}

	/**
	* determines the link and `link text` of the readmore button & linked title
	*/
	function _linkInfo( &$row, &$params ) {
		global $my;

		$row->link_on 	= '';
		$row->link_text	= '';

		if ($params->get( 'readmore' ) || $params->get( 'link_titles' )) {
			if ( $params->get( 'intro_only' ) ) {
				// checks if the item is a public or registered/special item
				if ( $row->access <= $my->gid ) {
					$row->link_on = sefRelToAbs( 'index.php?option=com_content&amp;task=view&amp;id=' . $row->id . $row->Itemid_link );

					if ( isset($row->readmore) && @$row->readmore) {
						// text for the readmore link
						$row->link_text = _READ_MORE;
					}
				} else {
					$row->link_on = sefRelToAbs( 'index.php?option=com_registration&amp;task=register' );

					if ( isset($row->readmore) && @$row->readmore ) {
						// text for the readmore link if accessible only if registered
						$row->link_text	= _READ_MORE_REGISTER;
					}
				}
			}
		}
	}

	/**
	* Writes Title
	*/
	function Title( &$row, &$params, &$access ) {
		// ------------------------ A8E fixes start ------------------------
		if ( $params->get( 'item_title' ) ) {
			global $option, $task;
			$headingNr = 2;
			if (($option=='com_content' && $task=='view') || $params->get('page_title')==0) {
				$headingNr = 1;
			}
			if ( $params->get( 'link_titles' ) && $row->link_on != '' ) {
				?>
				<h<?php echo $headingNr; ?> class="componentheading<?php echo $params->get( 'pageclass_sfx' ); ?>">
					<a href="<?php echo $row->link_on;?>" class="contentpagetitle<?php echo $params->get( 'pageclass_sfx' ); ?>">
						<?php echo ampReplace($row->title);?></a>
				</h<?php echo $headingNr; ?>>
				<?php HTML_content::EditIcon( $row, $params, $access ); ?>
				<?php
			} else {
				?>
				<h<?php echo $headingNr; ?> class="componentheading<?php echo $params->get( 'pageclass_sfx' ); ?>">
					<?php echo ampReplace($row->title);?>
				</h<?php echo $headingNr; ?>>
				<?php HTML_content::EditIcon( $row, $params, $access ); ?>
				<?php
			}
		} else {
			?>
			<?php HTML_content::EditIcon( $row, $params, $access ); ?>
			<?php
		}
		// ------------------------ A8E fixes end ------------------------
	}

	/**
	* Writes Edit icon that links to edit page
	*/
	function EditIcon( &$row, &$params, &$access ) {
		global $my;

		if ( $params->get( 'popup' ) ) {
			return;
		}
		if ( $row->state < 0 ) {
			return;
		}
		if ( !$access->canEdit && !( $access->canEditOwn && $row->created_by == $my->id ) ) {
			return;
		}

		mosCommonHTML::loadOverlib();

		$link 	= 'index.php?option=com_content&amp;task=edit&amp;id='. $row->id . $row->Itemid_link .'&amp;Returnid='. $row->_Itemid;
		$image 	= mosAdminMenus::ImageCheck( 'edit.png', '/images/M_images/', NULL, NULL, _E_EDIT, _E_EDIT );

		if ( $row->state == 0 ) {
			$overlib = _CMN_UNPUBLISHED;
		} else {
			$overlib = _CMN_PUBLISHED;
		}
		$date 		= mosFormatDate( $row->created );
		$author		= $row->created_by_alias ? $row->created_by_alias : $row->author;

		$overlib 	.= '<br />';
		$overlib 	.= $row->groups;
		$overlib 	.= '<br />';
		$overlib 	.= $date;
		$overlib 	.= '<br />';
		$overlib 	.= $author;
		?>
		<a href="<?php echo sefRelToAbs( $link ); ?>" onmouseover="return overlib('<?php echo $overlib; ?>', CAPTION, '<?php echo _E_EDIT; ?>', BELOW, RIGHT);" onmouseout="return nd();">
			<?php echo $image; ?></a>
		<?php
	}


	/**
	* Writes PDF icon
	*/
	function PdfIcon( &$row, &$params, $hide_js ) {
		global $mosConfig_live_site;

		if ( $params->get( 'pdf' ) && !$params->get( 'popup' ) && !$hide_js ) {
			$status = 'status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no';
			$link 	= $mosConfig_live_site. '/index2.php?option=com_content&amp;do_pdf=1&amp;id='. $row->id;

			if ( $params->get( 'icons' ) ) {
				$image = mosAdminMenus::ImageCheck( 'pdf_button.png', '/images/M_images/', NULL, NULL, _CMN_PDF, NULL );
			} else {
				$image = _CMN_PDF .'&nbsp;';
			}
			// ------------------------ A8E fixes start ------------------------
			?>
			<span class="PDFIcon"><a href="<?php echo $link; ?>" title="<?php echo _CMN_PDF;?>">
				<?php echo $image; ?></a></span>
			<?php
			// ------------------------ A8E fixes end ------------------------
		}
	}


	/**
	* Writes Email icon
	*/
	function EmailIcon( &$row, &$params, $hide_js ) {
		// ------------------------ A8E fixes start ------------------------
		global $mosConfig_live_site, $Itemid, $task;
		if ( $params->get( 'email' ) ) {
			
			if ($task == 'view') {
				$_Itemid = '&amp;itemid='. $Itemid;
			} else {
				$_Itemid = '';
			}
			
			$link	= $mosConfig_live_site .'/index.php?option=com_content&amp;task=emailform&amp;id='. $row->id . $_Itemid;
			
			if ( $params->get( 'icons' ) ) {
				$image = mosAdminMenus::ImageCheck( 'emailButton.png', '/images/M_images/', NULL, NULL, _CMN_EMAIL, NULL );
			} else {
				$image = '&nbsp;'. _CMN_EMAIL;
			}
			?>
			<span class="EmailIcon"><a href="<?php echo $link; ?>" title="<?php echo _CMN_EMAIL;?>">
				<?php echo $image; ?></a></span>
			<?php
		}
		// ------------------------ A8E fixes end ------------------------
	}

	/**
	* Writes Container for Section & Category
	*/
	function Section_Category( &$row, &$params ) {
		// ------------------------ A8E fix ------------------------

		// displays Section Name
		HTML_content::Section( $row, $params );

		// displays Section Name
		HTML_content::Category( $row, $params );

		// ------------------------ A8E fix ------------------------
	}

	/**
	* Writes Section
	*/
	function Section( &$row, &$params ) {
		if ( $params->get( 'section' ) ) {
				?>
				<span>
					<?php
					echo $row->section;
					// writes dash between section & Category Name when both are active
					if ( $params->get( 'category' ) ) {
						echo ' - ';
					}
					?>
				</span>
			<?php
		}
	}

	/**
	* Writes Category
	*/
	function Category( &$row, &$params ) {
		if ( $params->get( 'category' ) ) {
			?>
			<span>
				<?php
				echo $row->category;
				?>
			</span>
			<?php
		}
	}

	/**
	* Writes Author name
	*/
	function Author( &$row, &$params ) {
		if ( ( $params->get( 'author' ) ) && ( $row->author != '' ) ) {
			// ------------------------ A8E fixes start ------------------------
			?>
			<span class="small author">
				<?php echo _WRITTEN_BY . ' '.( $row->created_by_alias ? $row->created_by_alias : $row->author ); ?>
			</span>
			<?php
			// ------------------------ A8E fixes end ------------------------
		}
	}


	/**
	* Writes Create Date
	*/
	function CreateDate( &$row, &$params ) {
		$create_date = null;

		if ( intval( $row->created ) != 0 ) {
			$create_date = mosFormatDate( $row->created );
		}

		if ( $params->get( 'createdate' ) ) {
			// ------------------------ A8E fixes start ------------------------
			?>
			<span class="createdate date">
				<?php echo $create_date; ?>
            </span>
			<?php
			// ------------------------ A8E fixes end ------------------------
		}
	}

	/**
	* Writes URL's
	*/
	function URL( &$row, &$params ) {
		if ( $params->get( 'url' ) && $row->urls ) {
			// ------------------------ A8E fixes start ------------------------
			?>
				<a href="http://<?php echo $row->urls ; ?>" target="_blank">
					<?php echo $row->urls; ?></a>
			<?php
			// ------------------------ A8E fixes end ------------------------
		}
	}

	/**
	* Writes TOC
	*/
	function TOC( &$row ) {
		if ( isset($row->toc) ) {
			echo $row->toc;
		}
	}

	/**
	* Writes Modified Date
	*/
	function ModifiedDate( &$row, &$params ) {
		$mod_date = null;

		if ( intval( $row->modified ) != 0) {
			$mod_date = mosFormatDate( $row->modified );
		}

		if ( ( $mod_date != '' ) && $params->get( 'modifydate' ) ) {
			// ------------------------ A8E fixes start ------------------------
			?>
			<span class="modifydate date">
				<?php echo _LAST_UPDATED; ?> ( <?php echo $mod_date; ?> )
			</span>
			<?php
			// ------------------------ A8E fixes end ------------------------
		}
	}

	/**
	* Writes Readmore Button
	*/
	function ReadMore ( &$row, &$params ) {
		if ( $params->get( 'readmore' ) ) {
			if ( $params->get( 'intro_only' ) && $row->link_text ) {
				// ------------------------ A8E fixes start ------------------------
				?>
				<a href="<?php echo $row->link_on;?>" class="readon<?php echo $params->get( 'pageclass_sfx' ); ?>" title="<?php echo $row->title;?>">
					<?php echo $row->link_text;?></a>
				<?php
				// ------------------------ A8E fixes end ------------------------
			}
		}
	}

	/**
	* Writes Next & Prev navigation button
	*/
	function Navigation( &$row, &$params ) {
		global $task;

		$link_part	= 'index.php?option=com_content&amp;task=view&amp;id=';

		// determines links to next and prev content items within category
		if ( $params->get( 'item_navigation' ) ) {
			if ( $row->prev ) {
				$row->prev = sefRelToAbs( $link_part . $row->prev . $row->Itemid_link );
			} else {
				$row->prev = 0;
			}

			if ( $row->next ) {
				$row->next = sefRelToAbs( $link_part . $row->next . $row->Itemid_link );
			} else {
				$row->next = 0;
			}
		}

		if ( $params->get( 'item_navigation' ) && ( $task == 'view' ) && !$params->get( 'popup' ) && ( $row->prev || $row->next ) ) {
			?>
			<div style="margin-top: 25px; text-align:center">
				<?php
				if ( $row->prev ) {
					?>
					<span class="pagenav_prev">
						<a href="<?php echo $row->prev; ?>">
							<?php echo _ITEM_PREVIOUS; ?></a>
					</span>
					<?php
				}

				if ( $row->prev && $row->next ) {
					?>

					<?php
				}

				if ( $row->next ) {
					?>
					<span class="pagenav_next">
						<a href="<?php echo $row->next; ?>">
							<?php echo _ITEM_NEXT; ?></a>
					</span>
					<?php
				}
				?>
			</div>
			<?php
		}
	}

	/**
	* Writes the edit form for new and existing content item
	*
	* A new record is defined when <var>$row</var> is passed with the <var>id</var>
	* property set to 0.
	* @param mosContent The category object
	* @param string The html for the groups select list
	*/
	function editContent( &$row, $section, &$lists, &$images, &$access, $myid, $sectionid, $task, $Itemid ) {
		global $mosConfig_live_site, $mainframe;

		mosMakeHtmlSafe( $row );

		require_once( $GLOBALS['mosConfig_absolute_path'] . '/includes/HTML_toolbar.php' );

		// used for spoof hardening
		$validate = josSpoofValue();

		$Returnid 	= intval( mosGetParam( $_REQUEST, 'Returnid', $Itemid ) );
		$tabs 		= new mosTabs(0, 1);

		$mainframe->addCustomHeadTag( '<link rel="stylesheet" type="text/css" media="all" href="includes/js/calendar/calendar-mos.css" title="green" />' );
		?>
  		<div id="overDiv" style="position:absolute; visibility:hidden; z-index:10000;"></div>
		<!-- import the calendar script -->
		<script language="javascript" type="text/javascript" src="<?php echo $mosConfig_live_site;?>/includes/js/calendar/calendar_mini.js"></script>
		<!-- import the language module -->
		<script language="javascript" type="text/javascript" src="<?php echo $mosConfig_live_site;?>/includes/js/calendar/lang/calendar-en.js"></script>
	  	<script language="javascript" type="text/javascript" src="<?php echo $mosConfig_live_site;?>/includes/js/overlib_mini.js"></script>
	  	<script language="javascript" type="text/javascript">
		onunload = WarnUser;
		var folderimages = new Array;
		<?php
		$i = 0;
		foreach ($images as $k=>$items) {
			foreach ($items as $v) {
				echo "\n	folderimages[".$i++."] = new Array( '$k','".addslashes( $v->value )."','".addslashes( $v->text )."' );";
			}
		}
		?>
		function submitbutton(pressbutton) {
			var form = document.adminForm;
			if (pressbutton == 'cancel') {
				submitform( pressbutton );
				return;
			}

			// var goodexit=false;
			// assemble the images back into one field
			form.goodexit.value=1;
			/*
			var temp = new Array;
			for (var i=0, n=form.imagelist.options.length; i < n; i++) {
				temp[i] = form.imagelist.options[i].value;
			}
			form.images.value = temp.join( '\n' );
			*/
			try {
				form.onsubmit();
			}
			catch(e){}
			// do field validation
			if (form.title.value == "") {
				alert ( "<?php echo addslashes( _E_WARNTITLE ); ?>" );
			} else if (parseInt('<?php echo $row->sectionid;?>')) {
				// for content items
				if (getSelectedValue('adminForm','catid') < 1) {
					alert ( "<?php echo addslashes( _E_WARNCAT ); ?>" );
				//} else if (form.introtext.value == "") {
				//	alert ( "<?php echo addslashes( _E_WARNTEXT ); ?>" );
				} else {
					<?php
					getEditorContents( 'editor1', 'introtext' );
					getEditorContents( 'editor2', 'fulltext' );
					?>
					submitform(pressbutton);
				}
			//} else if (form.introtext.value == "") {
			//	alert ( "<?php echo addslashes( _E_WARNTEXT ); ?>" );
			} else {
				// for static content
				<?php
				getEditorContents( 'editor1', 'introtext' ) ;
				?>
				submitform(pressbutton);
			}
		}

		function setgood(){
			document.adminForm.goodexit.value=1;
		}

		function WarnUser(){
			if (document.adminForm.goodexit.value==0) {
				alert('<?php echo addslashes( _E_WARNUSER );?>');
				window.location="<?php echo sefRelToAbs("index.php?option=com_content&task=".$task."&sectionid=".$sectionid."&id=".$row->id."&Itemid=".$Itemid); ?>";
			}
		}
		
		
		function insertReadmore(editor) {
				var content = tinyMCE.getContent();
				if (content.match(/<hr id="system-readmore" \/>/)) {
					alert('There is already a Read more... link that has been inserted. Only one such link is permitted.');
					return false;
				} else {					
					tinyMCE.execCommand('mceInsertContent',false,'<hr id="system-readmore" />')
				}
			}		
		</script>

		<?php
		$docinfo = "<strong>"._E_EXPIRES."</strong> ";
		$docinfo .= $row->publish_down."<br />";
		$docinfo .= "<strong>"._E_VERSION."</strong> ";
		$docinfo .= $row->version."<br />";
		$docinfo .= "<strong>"._E_CREATED."</strong> ";
		$docinfo .= $row->created."<br />";
		$docinfo .= "<strong>"._E_LAST_MOD."</strong> ";
		$docinfo .= $row->modified."<br />";
		$docinfo .= "<strong>"._E_HITS."</strong> ";
		$docinfo .= $row->hits."<br />";
		?>
		<form action="index.php" method="post" name="adminForm" onSubmit="javascript:setgood();">

		<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
			<td class="contentheading" >
			<?php echo $section;?> / <?php echo $row->id ? _E_EDIT : _E_ADD;?>&nbsp;
			<?php echo _E_CONTENT;?> &nbsp;&nbsp;&nbsp;
			<a href="javascript: void(0);" onMouseOver="return overlib('<table><?php echo $docinfo; ?></table>', CAPTION, '<?php echo _E_ITEM_INFO;?>', BELOW, RIGHT);" onMouseOut="return nd();">
			<strong>[Info]</strong>
			</a>
			</td>
		</tr>
		</table>

		<table class="adminform" cellpadding="7">
		<tr>
			<td>
				<div style="float: left;">
					<?php echo _E_TITLE; ?>
					<br />
					<input class="inputbox" type="text" name="title" size="50" maxlength="100" value="<?php echo $row->title; ?>" />
				</div>
				<div style="float: right;">
					<?php
					// Toolbar Top
					mosToolBar::startTable();
					mosToolBar::save();
					mosToolBar::apply( 'apply_new' );
					mosToolBar::cancel();
					mosToolBar::endtable();
					?>
				</div>
			</td>
		</tr>
		<?php
		if ($row->sectionid) {
			?>
			<tr>
				<td>
				<?php echo _E_CATEGORY; ?>
				<br />
				<?php echo $lists['catid']; ?>
				</td>
			</tr>
			<?php
		}
		?>
		<tr>
			<?php
			if (intval( $row->sectionid ) > 0) {
				?>
				<td>
				<?php echo _E_INTRO.' ('._CMN_REQUIRED.')'; ?>:
				</td>
				<?php
			} else {
				?>
				<td>
				<?php echo _E_MAIN.' ('._CMN_REQUIRED.')'; ?>:
				</td>
			<?php
			} ?>
		</tr>
		<tr>
			<td>
			<?php
			// parameters : areaname, content, hidden field, width, height, rows, cols
			editorArea( 'editor1',  $row->introtext , 'introtext', '600', '400', '70', '15' ) ;
			?>
			<img src='<?php echo $mosConfig_live_site?>/mambots/editors-xtd/mosreadmore.gif' onclick="insertReadmore('introtext');return false;" />
			</td>
		</tr>
		<?php
		/*
		if (intval( $row->sectionid ) > 0) {
			?>
			<tr>
				<td>
				<?php echo _E_MAIN.' ('._CMN_OPTIONAL.')'; ?>:
				</td>
			</tr>
			<tr>
				<td>
				<?php
				// parameters : areaname, content, hidden field, width, height, rows, cols
				editorArea( 'editor2',  $row->fulltext , 'fulltext', '600', '400', '70', '15' ) ;
				?>
				</td>
			</tr>
			<?php
		}
		*/
		?>
		</table>

		<div style='float: right'>
		<?php
		// Toolbar Bottom
		mosToolBar::startTable();
		mosToolBar::save();
		mosToolBar::apply();
		mosToolBar::cancel();
		mosToolBar::endtable();
		?>
		</div>
		<div style='clear: right'></div>
	 	<?php
		$tabs->startPane( 'content-pane' );
		$tabs->startTab( _E_PUBLISHING, 'publish-page' );
		?>
			<table class="adminform">
			<?php
			if ($access->canPublish) {
				?>
				<tr>
					<td align="left">
					<?php echo _E_STATE; ?>
					</td>
					<td>
					<?php echo $lists['state']; ?>
					</td>
				</tr>
				<?php
			} ?>
			<tr>
				<td align="left">
				<?php echo _E_ACCESS_LEVEL; ?>
				</td>
				<td>
				<?php echo $lists['access']; ?>
				</td>
			</tr>
			<tr>
				<td align="left">
				<?php echo _E_AUTHOR_ALIAS; ?>
				</td>
				<td>
				<input type="text" name="created_by_alias" size="50" maxlength="100" value="<?php echo $row->created_by_alias; ?>" class="inputbox" />
				</td>
			</tr>
			<tr>
				<td align="left">
				<?php echo _E_ORDERING; ?>
				</td>
				<td>
				<?php echo $lists['ordering']; ?>
				</td>
			</tr>
			<tr>
				<td align="left">
				<?php echo _E_START_PUB; ?>
				</td>
				<td>
				<input class="inputbox" type="text" name="publish_up" id="publish_up" size="25" maxlength="19" value="<?php echo $row->publish_up; ?>" />
				<input type="reset" class="button" value="..." onclick="return showCalendar('publish_up', 'y-mm-dd');" />
				</td>
			</tr>
			<tr>
				<td align="left">
				<?php echo _E_FINISH_PUB; ?>
				</td>
				<td>
				<input class="inputbox" type="text" name="publish_down" id="publish_down" size="25" maxlength="19" value="<?php echo $row->publish_down; ?>" />
				<input type="reset" class="button" value="..." onclick="return showCalendar('publish_down', 'y-mm-dd');" />
				</td>
			</tr>
			<tr>
				<td align="left">
				<?php echo _E_SHOW_FP; ?>
				</td>
				<td>
				<input type="checkbox" name="frontpage" value="1" <?php echo $row->frontpage ? 'checked="checked"' : ''; ?> />
				</td>
			</tr>
			</table>
		<?php
		$tabs->endTab();
		$tabs->startTab( 'Icon', 'images-page' );
		?>
			<script type='text/javascript'>
				//<!--
				
				function setIcon(){

					var imagelist = getIcon();
					
					var folderIndex = document.forms[0].folders.selectedIndex;
					var folder = document.forms[0].folders[folderIndex].value;
					
					document.getElementById('icon').value = folder+imagelist;
					document.getElementById('view_icon').src = '<?php echo $mosConfig_live_site.'/images/stories/' ?>'+imagelist;					
				}
				
				function removeIcon(){
					document.getElementById('icon').value = '';
					document.getElementById('view_icon').src = '../images/M_images/blank.png';		
				}
				
				function getIcon(){
					var imageIndex = document.getElementById('imagefiles').selectedIndex;
					var imagelist = document.getElementById('imagefiles')[imageIndex].value;
					
					return imagelist;
				}
				
				//-->				
				</script>
				<table class="adminform" width="100%">
				<tr>
					<th colspan="2">
						Icon Control
					</th>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%">
						<tr height='115px'>
							<td valign="top">
								Sub-folder: 
								<br />
								<br />
								<?php echo $lists['folders'];?>
							</td>
							<td valign="top">
								<div align="center">
									Gallery Images:
									<br />
									<br />
									<?php echo $lists['imagefiles'];?>
								</div>
							</td>
							<td valign="top">
							<div align="center">
								Preview Icon:
								<br />
								<br />									
								<img name="view_imagefiles" src="../images/M_images/blank.png" alt="Sample Image" />
							</div>
							</td>
							<td valign="top">
								<div align="center">
									Active Icon:
									<br />
									<br />
									<?php if($row->images != ''){ ?>
										<img id='view_icon' src="<?php echo "$mosConfig_live_site/images/stories/$row->images" ?>" alt="Active Image"  />
									<?php }else{ ?>
										<img id='view_icon' src="../images/M_images/blank.png" alt="Active Image"  />
									<?php } ?>
								</div>
							</td>
						
						</tr>
						</table>
						
					</td>
				</tr>
				
				</table>
		<?php
		$tabs->endTab();		
		$tabs->startTab( _E_METADATA, 'meta-page' );
		?>
			<table class="adminform">
			<tr>
				<td align="left" valign="top">
				<?php echo _E_M_DESC; ?>
				</td>
				<td>
				<textarea class="inputbox" cols="45" rows="3" name="metadesc"><?php echo str_replace('&','&amp;',$row->metadesc); ?></textarea>
				</td>
			</tr>
			<tr>
				<td align="left" valign="top">
				<?php echo _E_M_KEY; ?>
				</td>
				<td>
				<textarea class="inputbox" cols="45" rows="3" name="metakey"><?php echo str_replace('&','&amp;',$row->metakey); ?></textarea>
				</td>
			</tr>
			</table>
		<?php
		$tabs->endTab();
		$tabs->endPane();
		?>

		<div style="clear:both;"></div>

		<input type="hidden" name="goodexit" value="0" />
		<input type="hidden" name="option" value="com_content" />
		<input type="hidden" name="Returnid" value="<?php echo $Returnid; ?>" />
		<input type="hidden" name="id" value="<?php echo $row->id; ?>" />
		<input type="hidden" name="version" value="<?php echo $row->version; ?>" />
		<input type="hidden" name="sectionid" value="<?php echo $row->sectionid; ?>" />
		<input type="hidden" name="created_by" value="<?php echo $row->created_by; ?>" />
		<input type="hidden" name="referer" value="<?php echo ampReplace( @$_SERVER['HTTP_REFERER'] ); ?>" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="<?php echo $validate; ?>" value="1" />
		</form>
		<?php
	}

	/**
	* Writes Email form for filling in the send destination
	*/
	// ------------------------ A8E fixes start ------------------------
	function emailForm($uid, $title, $template='', $itemid, $validator = null) {
		global $mainframe;
		
		// used for spoof hardening
		$validate = josSpoofValue();
		
		?>
		<div class="emailform">
			<?php
			echo("<h1>" . _EMAIL_FRIEND . "</h1>");

			if($validator != null && !$validator->isValid()) { 
				$labels = array();
				$labels["email"] = _EMAIL_FRIEND_ADDR;
				$labels["yourname"] = _EMAIL_YOUR_NAME;
				$labels["youremail"] = _EMAIL_YOUR_MAIL;
				$labels["subject"] = _SUBJECT_PROMPT;
				?>
				<div class="error">
 					<?php echo(_VALIDATOR_ERRORS); ?><br/>
 					<?php
					// show the errors
					$errors = $validator->getErrors();
					$errorKeys =  array_keys($errors);				
					echo("<span class=\"invisible\">");				
					foreach($errorKeys as $key) {
						$errorMessages = $errors[$key];
						foreach($errorMessages as $errorMessage) {
							echo(sprintf($errorMessage, $labels[$key])."<br/>");
						}
					}
					echo("</span>");
					?>
				</div>
				<?php
			}

			$email = trim( mosGetParam( $_POST, 'email', '' ) );
			$yourname = trim( mosGetParam( $_POST, 'yourname', '' ) );
			$youremail = trim( mosGetParam( $_POST, 'youremail', '' ) );
			$subject = trim( mosGetParam( $_POST, 'subject', '') );

			// mark fields with an error with:
			define("_MARK", _VALIDATOR_ERROR_MARK);
			define("_ERROR_SPAN_OPEN", "<span class=\"error\">");
			define("_ERROR_SPAN_CLOSE", "</span>");

			?>		
			<form action="<?php echo basename($_SERVER['PHP_SELF']) ?>?option=com_content&amp;task=emailsend" name="frontendForm" method="post">
				<div class="row">
					<label for="email">
						<?php 
						if($validator != null && !$validator->isValid('email')) {
							echo(_ERROR_SPAN_OPEN);
							echo(_MARK);
						}
						echo _EMAIL_FRIEND_ADDR;
						if($validator != null && !$validator->isValid('email')) {
							echo(_ERROR_SPAN_CLOSE);
						}				
						?>
					</label>
					<div class="formelm">
						<input type="text" name="email" class="inputbox" size="25" value="<?php echo $email;?>"  id="email" /> 
					</div>
				</div>
				<div class="row">
					<label for="yourname">
						<?php 
						if($validator != null && !$validator->isValid('yourname')) {
							echo(_ERROR_SPAN_OPEN);
							echo(_MARK);
						}
						echo _EMAIL_YOUR_NAME;
						if($validator != null && !$validator->isValid('yourname')) {
							echo(_ERROR_SPAN_CLOSE);
						}				
						?>
					</label>
					<div class="formelm">
						<input type="text" name="yourname" class="inputbox" size="25" value="<?php echo $yourname;?>" id="yourname" />
					</div>
				</div>

				<div class="row">
					<label for="youremail">
						<?php 
						if($validator != null && !$validator->isValid('youremail')) {
							echo(_ERROR_SPAN_OPEN);
							echo(_MARK);
						}
						echo _EMAIL_YOUR_MAIL; 
						if($validator != null && !$validator->isValid('youremail')) {
							echo(_ERROR_SPAN_CLOSE);
						}					
						?>	
					</label>
					<div class="formelm">
						<input type="text" name="youremail" class="inputbox" size="25" value="<?php echo $youremail;?>" id="youremail" />
					</div>
				</div>

				<div class="row">
					<label for="subject">
						<?php echo _SUBJECT_PROMPT; ?>	
					</label>
					<div class="formelm">
						<input type="text" name="subject" class="inputbox" maxlength="100" size="40" value="<?php echo $subject;?>" id="subject" />
					</div>
				</div>
				<div class="row">
					<div class="formelm">
						<input type="submit" name="submit" class="button" value="<?php echo _BUTTON_SUBMIT_MAIL; ?>" />
						&nbsp;
						<input type="submit" name="cancel" value="<?php echo _BUTTON_CANCEL; ?>" class="button" />
					</div>
				</div>
				<input type="hidden" name="id" value="<?php echo $uid; ?>" />
				<input type="hidden" name="itemid" value="<?php echo $itemid; ?>" />
				<input type="hidden" name="<?php echo $validate; ?>" value="1" />
			</form>
		</div>
		<?php
	}
	// ------------------------ A8E fixes end ------------------------

	// ------------------------ A8E fixes start ------------------------
	/**
	* Writes Email sent message
	* @param string Who it was sent to
	*/
	function emailSent( $to) {
		global $mosConfig_sitename, $mainframe;

		?> 
		<span class="contentheading"><?php echo _EMAIL_SENT." $to";?></span>
		<?php
	}
	// ------------------------ A8E fixes end ------------------------
}
?>