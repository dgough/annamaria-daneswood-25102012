	<script type="text/javascript" src="/templates/annamaria_2011/lib/js/jquery.PrintArea.js"></script>
	<script type="text/javascript">
		jQuery(function($){
			$("#<?php echo $reference ?>-getPrice").click(function(){
				$("div#your_prices").printArea();
			});
		});
	</script>
	<form action="<?php echo sefRelToAbs('index.php?option=com_virtuemart&page=booking.index') ?>" method="post" id='vbAjaxForm'>
	<input type='hidden' name='people' value='1' />
	<input type='hidden' name='property_id' value='<?php echo $property_id ?>' />
	<div class="property_calendar_left">
		<div class="cal-box">
		
		
			<?php
/*
				if($_SERVER['REMOTE_ADDR'] == "93.96.178.119"){
					var_dump($calendar);
				
				}
*/
			?>
		
		
			<?php
			$calendar->getCalendar();
			?>
			<div id="key_box">
				<ul class='key'>
					<!-- <li class='today'><span></span><?php echo $VM_LANG->_('VM_CAL_TODAYS_DATE'); ?></li> -->
					<li class='available'><span></span><?php echo $VM_LANG->_('VM_CAL_AVL_DATE'); ?></li>
					<li class='unavailable'><span></span><?php echo $VM_LANG->_('VM_CAL_UNAVL_DATE'); ?></li>
					<li class='selected'><span></span><?php echo $VM_LANG->_('VM_CAL_SELECTED_DATE'); ?></li>
				</ul>
			</div>
		</div>
	    <div id="date_select">
	    	<div class="cal-box"> 
				<div class='title'><strong><?php echo $VM_LANG->_('VM_CAL_ARRIVAL'); ?></strong></div>
				<?php echo $calendar->renderInput('dateFrom',$dateFrom,'',0); ?>
				<div class='title'><strong><?php echo $VM_LANG->_('VM_CAL_DEPARTURE'); ?></strong></div>
				<?php echo $calendar->renderInput('dateTo',$dateTo,'',0); ?>
			</div>
			<div class="get_price" style="margin-top: 6px;">
				<input type="button" value="<?php echo $VM_LANG->_('VM_CAL_PRINT') ?>" id='<?php echo $reference ?>-getPrice' class="button_print" />
				<input type="reset" value="<?php echo $VM_LANG->_('VM_CAL_CLEAR') ?>" id="<?php echo $reference ?>-clearCal" class="button_clear" />
			</div>
		</div>
		<div id="your_prices">  
	    	<?php include(dirname(__FILE__).'/calendar.prices.tpl.php') ?>
		</div>	
        <div id="calendar_submit_button">
        	<input type="submit" value="<?php echo $VM_LANG->_('VM_CAL_BOOK_NOW') ?>" class="button_book" />
        </div>	
        </form>
	</div>
	<div style="clear: both"></div>


<script type="text/javascript">
//<!--

window.addEvent('domready',function() {
<?php 
if(vmGet($_REQUEST,'dateFrom') && vmGet($_REQUEST,'dateTo')){
	$reqDateFrom = vmGet($_REQUEST,'dateFrom');
	$reqDateTo = vmGet($_REQUEST,'dateTo');
	echo 'getDateFromCalendar({year:'.$reqDateFrom['y'].',month:'.$reqDateFrom['m'].',day:'.$reqDateFrom['d']."});\n";
	echo 'getDateFromCalendar({year:'.$reqDateTo['y'].',month:'.$reqDateTo['m'].',day:'.$reqDateTo['d']."});\n";
} 
?>
});

function priceCallBack(responseText, responseXML){
	
	var status = getText(responseXML.getElementsByTagName('status').item(0));
	
	if(status == 1){
		var prices = getText(responseXML.getElementsByTagName('prices').item(0));
		$('your_prices').setHTML(prices);
		
	}else{
		
		var message = getText(responseXML.getElementsByTagName('message').item(0));
		$('your_prices').setHTML('<strong>Error:</strong> '+message);
		alert(message);
	}
}

function getText(node){	
	if(node){	
		if(typeof(node.text) == 'undefined'){
			return node.textContent;
		}else{
			return node.text;
		}	
	}else{
		return '';
	}
}	

function clearCal(){
	clicks = 0;
	model_<?php echo $calendar->calendarCount ?>.initialDay=-1;
	cal_<?php echo $calendar->calendarCount ?>.resetViewDisplayedMonth();
}
//-->
</script>
