<?php
global $CURRENCY_DISPLAY, $CURRENCY;

$VM_LANG->load('calendar');

$was = '&nbsp;';
$discounts = array();
$total = '&nbsp;';

if(isset($booking)){
	$was = $CURRENCY_DISPLAY->getFullValue($CURRENCY->convert($booking->maxPrice));	
	$offSeason = $booking->maxPrice - ($booking->original);
	$discounts_total = $CURRENCY_DISPLAY->getFullValue($CURRENCY->convert($booking->discount));	
	$discounts = $booking->discounts;
	$total = $CURRENCY_DISPLAY->getFullValue($CURRENCY->convert($booking->total));
}
$i = 0;
function getRowNumber(){
	global $i;
	
	$row = $i ? 2 : 1;
	$i = !$i;
	return $row;
}
?>
<div id="vb_price_wrap">
<table class='vmAjaxPricing' style="text-align: center; width: 100%;">
	<!--  <tr>
		<th class='sectiontableheader' colspan="2"><?php echo $VM_LANG->_('VM_CAL_YOUR_PRICES') ?></th>					
	</tr> -->
	<?php if(!isset($booking)){ ?>
		<tr>
			<td colspan="2">
				<strong><?php echo $VM_LANG->_('VM_CAL_SELECT_DATE_RANGE'); ?></strong>
			</td>
		</tr>	
	<?php }else{ ?>
		<tr class='sectiontableentry<?php echo getRowNumber() ?>'>
			<td>
			<strong><?php echo $VM_LANG->_('VM_CAL_FULL_PRICE'); ?></strong></td>
			<td id='price_was' width="70px"><?php echo $was ?></td>					
		</tr>
		<?php if(count($discounts)){
			
			$surcharges = array();
			foreach($discounts as $key => $discount){ 
				//Check if its a discount or surcharge
				if($discount->value == 0){
					unset($discounts[$key]);
					
				}else if($discount->value < 0){
					$discount->value = $discount->value * -1;
					$surcharges[] = $discount;
					unset($discounts[$key]);
				}
			}
			
			if (isset($booking->tax)) {
				$c->name = $VM_LANG->_('VM_BOOKING_TAX_AT') . " " . $booking->tax_rate * 100 . "%)";
				$c->value = $booking->tax;
				$surcharges[] = $c;
				unset($c);
			}
			
			if (isset($booking->securityDeposit)) {
				$c->name = $VM_LANG->_('VM_BOOKING_DAMAGE_PROTECTION');
				$c->value = $booking->securityDeposit;
				$surcharges[] = $c;
				unset($c);
			}
			
			if(count($discounts)){
				?>
			    <tr class='sectiontableentry<?php echo getRowNumber() ?> discounts'> 
			    	<td colspan="2">
			    	<strong><?php echo $VM_LANG->_('VM_CAL_DISCOUNTS'); ?></strong>
			    	</td> 
			    </tr> 
			    <?php 
			    if($offSeason){
			    ?>
			    <tr class='sectiontableentry<?php echo getRowNumber() ?> discounts'> 
			     	<td><?php echo $VM_LANG->_('VM_CAL_DISCOUNTS_OFF_SEASON'); ?></td><td><?php echo $CURRENCY_DISPLAY->getFullValue($CURRENCY->convert($offSeason)) ?></td>
			    </tr>
			    <?php } ?>   
			    <?php 
				
			    $i = 0;
			    foreach($discounts as $discount){ 
			    
			    	$row = $i ? 2 : 1;
			    	$i = !$i;
			    	echo "<tr class='sectiontableentry".getRowNumber()." discounts'><td>$discount->name</td><td>{$CURRENCY_DISPLAY->getFullValue($CURRENCY->convert($discount->value))}</td></tr>\n";
			    
			    }
			  $row = $i ? 2 : 1;
			}
			?>
			    <tr class='sectiontableentry<?php echo getRowNumber() ?> surcharges'> 
			    	<td colspan="2">
			    	<strong><?php echo $VM_LANG->_('VM_CAL_CHARGES'); ?></strong>
			    	</td> 
			    </tr>
			<?php
			
			if ($booking->cleaning) {
				$row = $i ? 2 : 1; ?>
				<tr class='sectiontableentry<?php echo getRowNumber() ?> surcharges'>
					<td>Cleaning Fee</td>
					<td><?php echo $CURRENCY_DISPLAY->getFullValue($CURRENCY->convert($booking->cleaning)) ?></td>
				</tr>
			    <?php
			}
			if(count($surcharges)){
				
			    foreach($surcharges as $charge){ 
			    
			    	$row = $i ? 2 : 1;
			    	$i = !$i;
			    	echo "<tr class='sectiontableentry".getRowNumber()." surcharges'><td>$charge->name</td><td>{$CURRENCY_DISPLAY->getFullValue($CURRENCY->convert($charge->value))}</td></tr>\n";
			    
			    }
			  $row = $i ? 2 : 1;
			}
		}		
	    ?>
	     <tr class='sectiontableentry<?php echo getRowNumber() ?>'> 
	     	<th colspan="2">
	     		<?php echo $VM_LANG->_('VM_CAL_ONLINE_PRICE'); ?>
	     	</th> 
	     </tr>			
		<tr class='sectiontableentry<?php echo getRowNumber() ?>'>
			<td id='price_total' colspan="2"><?php echo $total ?></td>					
		</tr>
	<?php } ?>
</table>	
</div>