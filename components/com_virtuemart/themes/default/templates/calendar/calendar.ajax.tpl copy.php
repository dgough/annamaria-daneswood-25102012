	<form action="<?php echo sefRelToAbs('index.php?option=com_virtuemart&page=booking.index') ?>" method="post" id='vbAjaxForm'>
	<input type='hidden' name='people' value='1' />
	<input type='hidden' name='property_id' value='<?php echo $property_id ?>' />
	<div class="property_calendar_left">
		<?php
		$calendar->getCalendar();
		?>
	    <div id="date_select"> 
			<div class='title'><strong>Arrival Date</strong></div>
			<?php echo $calendar->renderInput('dateFrom',$dateFrom,'',0); ?>
			<div class='title'><strong>Departure Date</strong></div>
			<?php echo $calendar->renderInput('dateTo',$dateTo,'',0); ?>
			<div class="get_price" style="margin-top: 6px;">
				<input type="button" value="Get Price" id='<?php echo $reference ?>-getPrice' class="button" />
			</div>
		</div>
    </div>
    
	<div class="property_calendar_right" >
        <div id="key_box">
			<ul class='key'>
				<li class='today'><span></span>Todays Date</li>
				<li class='available'><span></span>Available Date</li>
				<li class='unavailable'><span></span>Unavailable Date</li>
				<li class='selected'><span></span>Selected Dates</li>
			</ul>
		</div>
		<div id="your_prices">  
        	<form>
	    	<?php include(dirname(__FILE__).'/calendar.prices.tpl.php') ?>
		</div>	
        <div id="calendar_submit_button">
        	<input style="margin-top: 6px;" type="reset" value="Clear" id="<?php echo $reference ?>-clearCal" class="button" />
        	<input style="margin-top: 6px;" type="submit" value="Book Now" class="button" />
        </div>	
        </form>	
        <a id='backButton2' href='#' onclick='history.go(-1); return false'>Return to the listings</a>
	</div>
	<div style="clear: both"></div>


<script type="text/javascript">
//<!--

window.addEvent('domready',function() {
<?php 
if(vmGet($_REQUEST,'dateFrom') && vmGet($_REQUEST,'dateTo')){
	$reqDateFrom = vmGet($_REQUEST,'dateFrom');
	$reqDateTo = vmGet($_REQUEST,'dateTo');
	echo 'getDateFromCalendar({year:'.$reqDateFrom['y'].',month:'.$reqDateFrom['m'].',day:'.$reqDateFrom['d']."});\n";
	echo 'getDateFromCalendar({year:'.$reqDateTo['y'].',month:'.$reqDateTo['m'].',day:'.$reqDateTo['d']."});\n";
} 
?>
});

function priceCallBack(responseText, responseXML){
	
	var status = getText(responseXML.getElementsByTagName('status').item(0));
	
	if(status == 1){
		var prices = getText(responseXML.getElementsByTagName('prices').item(0));
		$('your_prices').setHTML(prices);
		
	}else{
		
		var message = getText(responseXML.getElementsByTagName('message').item(0));
		$('your_prices').setHTML('<strong>Error:</strong> '+message);
		alert(message);
	}
}

function getText(node){	
	if(node){	
		if(typeof(node.text) == 'undefined'){
			return node.textContent;
		}else{
			return node.text;
		}	
	}else{
		return '';
	}
}	

function clearCal(){
	clicks = 0;
	model_<?php echo $calendar->calendarCount ?>.initialDay=-1;
	cal_<?php echo $calendar->calendarCount ?>.resetViewDisplayedMonth();
}
//-->
</script>
