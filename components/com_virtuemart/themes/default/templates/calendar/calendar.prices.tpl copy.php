<?php
global $CURRENCY_DISPLAY, $CURRENCY;

$was = '&nbsp;';
$discounts = array();
$total = '&nbsp;';

if(isset($booking)){
	$was = $CURRENCY_DISPLAY->getFullValue($CURRENCY->convert($booking->maxPrice));	
	$offSeason = $booking->maxPrice - ($booking->original);
	$discounts_total = $CURRENCY_DISPLAY->getFullValue($CURRENCY->convert($booking->discount));	
	$discounts = $booking->discounts;
	$total = $CURRENCY_DISPLAY->getFullValue($CURRENCY->convert($booking->subtotal));	
	
}
$i = 0;
function getRowNumber(){
	global $i;
	
	$row = $i ? 2 : 1;
	$i = !$i;
	return $row;
}
?>

<table class='vmAjaxPricing' style="text-align: center; width: 100%;">
	<tr>
		<th class='sectiontableheader' colspan="2">Your Prices</th>					
	</tr>
	<?php if(!isset($booking)){ ?>
		<tr>
			<td colspan="2">
				Please select your date ranges from the calendar on the left.
			</td>
		</tr>	
	<?php }else{ ?>
		<tr class='sectiontableentry<?php echo getRowNumber() ?>'>
			<td>
			<strong>Full Price</strong></td>
			<td id='price_was' width="70px"><?php echo $was ?></td>					
		</tr>
		<?php if(count($discounts)){
			
			$surcharges = array();
			foreach($discounts as $key => $discount){ 
				//Check if its a discount or surcharge
				if($discount->value == 0){
					unset($discounts[$key]);
					
				}else if($discount->value < 0){
					$discount->value = $discount->value * -1;
					$surcharges[] = $discount;
					unset($discounts[$key]);
				}
			}
			
			if(count($discounts)){
				?>
			    <tr class='sectiontableentry<?php echo getRowNumber() ?> discounts'> 
			    	<td colspan="2">
			    	<strong>Discounts</strong>
			    	</td> 
			    </tr> 
			    <?php 
			    if($offSeason){
			    ?>
			    <tr class='sectiontableentry<?php echo getRowNumber() ?> discounts'> 
			     	<td>Off Season Discount</td><td><?php echo $CURRENCY_DISPLAY->getFullValue($CURRENCY->convert($offSeason)) ?></td>
			    </tr>
			    <?php } ?>   
			    <?php 
				
			    $i = 0;
			    foreach($discounts as $discount){ 
			    
			    	$row = $i ? 2 : 1;
			    	$i = !$i;
			    	echo "<tr class='sectiontableentry".getRowNumber()." discounts'><td>$discount->name</td><td>{$CURRENCY_DISPLAY->getFullValue($CURRENCY->convert($discount->value))}</td></tr>\n";
			    
			    }
			  $row = $i ? 2 : 1;
			}
			
			if(count($surcharges)){
				?>
			    <tr class='sectiontableentry<?php echo getRowNumber() ?> surcharges'> 
			    	<td colspan="2">
			    	<strong>Charges</strong>
			    	</td> 
			    </tr> 
			    <?php 
				
			    $i = 0;
			    foreach($surcharges as $charge){ 
			    
			    	$row = $i ? 2 : 1;
			    	$i = !$i;
			    	echo "<tr class='sectiontableentry".getRowNumber()." surcharges'><td>$charge->name</td><td>{$CURRENCY_DISPLAY->getFullValue($CURRENCY->convert($charge->value))}</td></tr>\n";
			    
			    }
			  $row = $i ? 2 : 1;
			}
		}		
	    ?>
	     <tr class='sectiontableentry<?php echo getRowNumber() ?>'> 
	     	<th colspan="2">
	     		Your Online Price
	     	</th> 
	     </tr>			
		<tr class='sectiontableentry<?php echo getRowNumber() ?>'>
			<td id='price_total' colspan="2"><?php echo $total ?></td>					
		</tr>
	<?php } ?>
</table>	
