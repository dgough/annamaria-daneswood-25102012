<div id="maint-list-wrapper">
<?php
global $modulename;

$listObj = new listFactory();

// print out the search field and a list heading
//$listObj->writeSearchHeader('Maintenance', '', $modulename, "index");

// start the list table
$listObj->startTable();

// these are the columns in the table
$columns = array('Property' => 'class="sectiontableheader"',
				'Worked From' => 'class="sectiontableheader"',
				'Work To' => 'class="sectiontableheader"',
				'Hours' => 'class="sectiontableheader"',
				'Minutes' => 'class="sectiontableheader"',
				'Amount' => 'class="sectiontableheader"',
				'Approved' => 'class="sectiontableheader"',
				'Paid' => 'class="sectiontableheader"'
				);

$listObj->writeTableHeader( $columns );

$i=0;
foreach ($maintlist as $maint) {
	
	$listObj->newRow();
	
	// The row number
	//$listObj->addCell( $pageNav->rowNumber( $i ) );
		
	// The Checkbox
	$tmpcell = '<a href="?option=com_virtuemart&page=maintenance.index&task=edit&maint_id='.$maint->maint_id.'&user_id='.$maint->user_id.'">'.$maint->name.'</a>';
	
	$listObj->addCell($tmpcell);
	$listObj->addCell($maint->worked_from);
	$listObj->addCell($maint->worked_to);
	$listObj->addCell($maint->hours);
	$listObj->addCell($maint->minutes);
	$listObj->addCell($maint->amount);
	$listObj->addCell(vmCommonHTML::getYesNoIcon( $maint->approved ),'style="text-align:center;"');
	$listObj->addCell(vmCommonHTML::getYesNoIcon( $maint->paid ),'style="text-align:center;"');

	//$listObj->addCell($maint->workedfrom);
	
	//$listObj->addCell( $ps_html->deleteButton( "season_id", $db->f("season_id"), "deleteSeason", $keyword, $limitstart, "&property_id=$prop_id" ) );
	
	$i++;
}

$listObj->writeTable();
$listObj->endTable();
$listObj->writeFooter( '', "" );
?>
</div>