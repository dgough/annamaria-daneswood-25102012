<?php 
	global $mosConfig_absolute_path;
?>
<style>
	th {
		font-weight:bold !important;
	}
</style>
<table class="admin_heading" width="100%;">
	<tbody>
    	<tr>
        	<td><div class="header"><h2 style="font-size:16px;">Maintenance Personnels' Associated Properties</h2></div></td>
        </tr>
    </tbody>
</table>
<table cellpadding="2" cellspacing="1" border="0" width="100%" class="adminlist">
<tbody>
<tr>
	<th class="title" width="20%">User</th>
    <th class="title" width="80%">Properties</th>
</tr>
<tr>
	<td valign="top">
	<?php 
		if (count($users)>0) { ?>
        	<ul>
        	<?php
			foreach($users as $user) { ?>
	       		<li>- <a href="?option=com_virtuemart&page=maintenance.userprop_form&task=edit&user_id=<?php echo $user->id; ?>"><?php echo $user->name.' ('.$user->company.')'; ?></a></li>
			<?php } ?>
            </ul>
            <?php
		} else {
			echo 'There are no users to select';
		}
		?>
    </div>
    </td>
    <td style="padding:5px;" valign="top">
       <?php
        if ($user_id!='') {
            include($mosConfig_absolute_path.'/administrator/components/com_virtuemart/html/maintenance.index.php');
			?>            
            <?php
        } else { ?>
            <p><strong>Please select a user from the left</strong></p>
        <?php }
        ?>
        
	</td>
	</tr>
    </tbody>
</table>