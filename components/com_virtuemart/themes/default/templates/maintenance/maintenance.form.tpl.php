<?php
global $mosConfig_live_site, $modulename, $my;

echo vmCommonHTML::scriptTag( $mosConfig_live_site .'/includes/js/calendar/calendar.js');
if( class_exists( 'JConfig' ) ) {
	// in Joomla 1.5, the name of calendar lang file is changed...
	echo vmCommonHTML::scriptTag( $mosConfig_live_site .'/includes/js/calendar/lang/calendar-en-GB.js');
} else {
	echo vmCommonHTML::scriptTag( $mosConfig_live_site .'/includes/js/calendar/lang/calendar-en.js');
}
echo vmCommonHTML::linkTag( $mosConfig_live_site .'/includes/js/calendar/calendar-mos.css');
?>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="adminForm" id="adminForm">
<input type="hidden" name="option" value="com_virtuemart" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="page" value="maintenance.index" />
<input type="hidden" name="func" value="<?php echo $func; ?>" />
<input type="hidden" name="maint_id" value="<?php echo $maint_id; ?>" />
<input type="hidden" name="user_id" value="<?php echo vmGet($_REQUEST,'user_id','')!='' ? vmGet($_REQUEST,'user_id','') : $my->id; ?>" />
<input type="hidden" name="vmtoken" value="<?php echo vmSpoofValue($sess->getSessionId()) ?>" />
<?php if (in_array($my->get('gid'),array(23,24,25))) { ?>
    <fieldset class="vb-fieldset">
        <legend>Associated Properties</legend>
        <label>Properties</label> <span><?php echo $properties; ?></span>
    </fieldset>
<?php } ?>
<?php if (in_array($my->get('gid'),array(23,24,25)) && $func=='addMaint') { ?>
<fieldset class="vb-fieldset">
	<legend>Add Maintenance?</legend>
    <div class="row">
    	<label>Add Maintenance?</label> <span><input type="checkbox" name="addmaint" value="1" /></span>
    </div>
</fieldset>
<?php } else { ?>
<input type="hidden" name="addmaint" value="1" />
<?php } ?>
<fieldset class="vb-fieldset">
	<legend>Maintenance Time and Costs</legend>
    <div class="row">
    	<label>Property</label> <span><?php echo vmCommonHTML::selectList($property_list,'property_id','','id','name', $selected_prop); ?></span>
    </div>
    <div class="row">
    	<label>Worked From</label> <span><input class="inputbox" type="text" name="worked_from" id="workedfrom" size="20" maxlength="19" value="<?php echo $worked_from; ?>" />
        <input name="reset" type="reset" class="button" onClick="return showCalendar('workedfrom', 'y-mm-dd');" value="..." style="vertical-align:middle" /></span>
    </div>
    <div class="row">
    	<label>Work To</label> <span><input class="inputbox" type="text" name="worked_to" id="workedto" size="20" maxlength="19" value="<?php echo $worked_to; ?>" />
        <input name="reset" type="reset" class="button" onClick="return showCalendar('workedto', 'y-mm-dd');" value="..." style="vertical-align:middle" /></span>
    </div>
    <div class="row">
    	<label>Hours : Minutes</label> <span><input type="text" name="hours" size="3" value="<?php echo $hours; ?>" /> : <input type="text" size="3" name="minutes" value="<?php echo $minutes; ?>" /></span>
    </div>
    <div class="row">    
    	<label>Amount</label> <span><input type="text" name="amount" size="5" value="<?php echo intval($amount); ?>" /></span>
    </div>
    <div class="row">    
    	<label>Comments</label> <span><textarea name="comments" rows="5" cols="40"><?php echo $comments; ?></textarea></span>
    </div>
    <?php if (in_array($my->get('gid'),array(23,24,25))) { ?>
    <fieldset class="vb-fieldset">
    	<legend>Admin Functions</legend>
    	<div class="row">
        	<label>Approved</label> <span><input type="checkbox" name="approved" value="1"<?php echo ($approved==1 ? 'checked="checked"' : ''); ?> /></span>
        </div>
        <div class="row">
        	<label>Paid</label> <span><input type="checkbox" name="paid" value="1"<?php echo ($paid==1 ? 'checked="checked"' : ''); ?> /></span>
        </div>
        
        <div class="row">
        	<label>Supporting Docs</label> <br/>
            <span>
            <?php 
			$mainID = $_GET['maintenance_id'];
			$docs = mysql_query("select * from supDocs WHERE maint_id='".$mainID."'")or die(mysql_error());
				  while($docRow = mysql_fetch_array($docs)){
					  echo "<a href='/documents/";
					  echo $docRow['document'];
					  echo "' target='_blank'>";
					  echo $docRow['document'];
					  echo "</a><br/>";
					 } 
					  ?>
                      </span><br/>
            <span><input type="file" name="file" /></span>
        </div>
        
    </fieldset>
    <?php } else { ?>
    	<input type="hidden" name="approved" value="<?php echo $approved; ?>" />
        <input type="hidden" name="paid" value="<?php echo $paid; ?>" />
    <?php } ?>
	<div class="row">
    	<label>&nbsp; </label> <span><input type="submit" value=" Submit Time and Costs " /></span>
    </div>
</fieldset>
</form>