<?php
$offSeason = $max - $original;
?>
<table style="width: 320px">	
	
	<tr><td><?php echo $VM_LANG->_('VM_BOOKING_FULL_PRICE') ?></td><td align="right"><?php echo $CURRENCY_DISPLAY->getFullValue($CURRENCY->convert($max)) ?></td></tr>
	<?php
	foreach($discounts as $d){
		if($d->value != 0){
			$d->value = $d->value * -1; //Change the sign
			echo "<tr><td>$d->name</td><td align='right'>{$CURRENCY_DISPLAY->getFullValue($CURRENCY->convert($d->value))}</td></tr>\n";
		}
	}	
	?>
	<?php if($offSeason){ ?><tr><td><?php echo $VM_LANG->_('VM_BOOKING_SEASON_DISCOUNT') ?></td><td align="right">-<?php echo $CURRENCY_DISPLAY->getFullValue($CURRENCY->convert($offSeason)) ?></td></tr><?php } ?>
	<?php if($discount_total){ ?><tr style="color: #ff0000"><td><?php echo $VM_LANG->_('VM_BOOKING_TOTAL') ?></td><td align="right">-<?php echo $CURRENCY_DISPLAY->getFullValue($CURRENCY->convert($discount_total + $offSeason)) ?></td></tr><?php } ?>
	<?php if($extras){ ?><tr><td><?php echo $VM_LANG->_('VM_BOOKING_EXTRAS') ?></td><td align="right"><?php echo $CURRENCY_DISPLAY->getFullValue($CURRENCY->convert($extras)) ?></td></tr><?php } ?>	
	<?php if($cleaning){ ?><tr><td>Cleaning Charge</td><td align="right"><?php echo $CURRENCY_DISPLAY->getFullValue($CURRENCY->convert($cleaning)) ?></td></tr><?php } ?>
	<tr><td colspan="2" style="height: 1px; font-size: 1px;"><hr style='height: 1px'/></td></tr>
	<tr style="font-weight: bold;"><td><?php echo $VM_LANG->_('VM_BOOKING_COST_OF_RETAIL') ?></td><td align="right"><?php echo $CURRENCY_DISPLAY->getFullValue($CURRENCY->convert($subtotal)) ?></td></tr>
	<?php// if($counter == 1){ ?>
	<?php if($tax){ ?><tr><td><?php echo $VM_LANG->_('VM_BOOKING_TAX_AT') ?> <?php echo $tax_rate * 100 ?>%)</td><td align="right"><?php echo $CURRENCY_DISPLAY->getFullValue($CURRENCY->convert($tax)) ?></td></tr><?php } ?>	
	<tr><td><?php echo $VM_LANG->_('VM_BOOKING_DAMAGE_PROTECTION') ?></td><td align="right"><?php echo $CURRENCY_DISPLAY->getFullValue($CURRENCY->convert($security)) ?></td></tr>
	<tr style="font-weight: bold;"><td><?php echo $VM_LANG->_('VM_BK_EMAIL_TOTAL') ?></td><td align="right"><?php echo $CURRENCY_DISPLAY->getFullValue($CURRENCY->convert($total)) ?></td></tr>
	<?php// } ?>
</table>