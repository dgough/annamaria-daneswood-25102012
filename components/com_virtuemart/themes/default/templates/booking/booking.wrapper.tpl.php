<?php 
global $mainframe,$mosConfig_live_site;
$mainframe->addCustomHeadTag(vmCommonHTML::scriptTag("$mosConfig_live_site/components/com_virtuemart/js/booking.js"));
//$mainframe->addCustomHeadTag(vmCommonHTML::scriptTag("$mosConfig_live_site/components/com_virtuemart/js/booking.js"));
?>
<script type="text/javascript">
        //<!--
        window.addEvent('domready', function() {
        	new vmBooking(<?php echo $status ? 1 : 0 ?>,'<?php echo $VM_LANG->_('VM_BOOKING_EXTRASTEXT') ?>');
        });
        //-->
</script>
<style type="text/css">
    #booking_wrapper {
        position: relative;
    }
    
    #booking_wrapper table {
        width: 100%;
    }
    
    #booking_wrapper fieldset {
        border: 1px solid black;
        margin: 10px 0px;
        padding: 10px;
        /*position:relative;*/
    }
    
    #booking_wrapper #booking_wrap_msg {
        padding: 1px 0px;
    }
    
    #booking_wrapper .total {
        float: right;
    }
    
    #booking_wrapper legend {
        font-weight: bold;
        font-size: 120%;
    }
</style>
<br/>
<h1><?php echo $VM_LANG->_('VM_BOOKING_HEADING')?></h1>
<?php echo $VM_LANG->_('VM_BOOKING_INTRO')?>
<div id="booking_wrapper">
    <div id='booking_wrap_msg'>
    </div>
    <form id="booking_form" action="index.php" method="post">
        <input type="hidden" name="option" value="com_virtuemart" /><input type="hidden" name="page" value="booking.index" />
        <fieldset>
            <legend>
                1. <?php echo $VM_LANG->_('VM_BOOKING_LEGEND1')?>
            </legend>
            <?php if ($VM_LANG->_('VM_BOOKING_TIP1')) { ?>
            <div class='tip'>
                <?php echo $VM_LANG->_('VM_BOOKING_TIP1')?>
            </div>
            <?php } ?>
            <div id="booking_wrap_total_top" class='total'>
                <?php 
                echo $outputTotalTop;
                ?>
            </div>
            <div id='booking_wrap_property'>
                <?php 
                echo $outputProperty;
                ?>
            </div>
        </fieldset>
        <fieldset>
            <legend>
                2. <?php echo $VM_LANG->_('VM_BOOKING_LEGEND2')?>
            </legend>
            <?php if ($VM_LANG->_('VM_BOOKING_TIP2')) { ?>
            <div class='tip'>
                <?php echo $VM_LANG->_('VM_BOOKING_TIP2')?>
				<div style="color: red"><?php echo $VM_LANG->_('PHPSHOP_CHECKOUT_CHARGES'); ?></div>
            </div>
            <?php } ?>
            <div id='booking_wrap_form'>
                <?php 
                echo $outputForm;
                ?>
            </div>
        </fieldset>
        <?php if ($enableExtras) { ?>
        <fieldset>
            <legend>
                3. <?php echo $VM_LANG->_('VM_BOOKING_LEGEND3')?>
            </legend>
            <?php if ($VM_LANG->_('VM_BOOKING_TIP3')) { ?>
            <div class='tip'>
                <?php echo $VM_LANG->_('VM_BOOKING_TIP3')?>
            </div>
            <?php } ?>
            <div id="extras_text">
            </div>
            <div id='booking_wrap_extras'>
                <?php 
                echo $outputExtras;
                ?>
            </div>
        </fieldset>
        <?php } ?>
        <fieldset>
            <legend>
                <?php echo(3 + $enableExtras).'. '.$VM_LANG->_('VM_BOOKING_LEGEND4')?>
            </legend>
            <?php if ($VM_LANG->_('VM_BOOKING_TIP4')) { ?>
            <div class='tip'>
                <?php echo $VM_LANG->_('VM_BOOKING_TIP4')?>
            </div>
            <?php } ?>
            <div id='booking_wrap_total_bottom' class='total'>
                <?php 
                echo $outputTotalBottom;
                ?>
            </div>
            <div style='float: left; width: 50%'>
				<?php
				$db = new ps_DB();
				//Get echeque charge
				$db->query("SELECT payment_method_discount, payment_method_discount_is_percent FROM #__{vm}_payment_method WHERE payment_method_id = 27 OR payment_method_id = 26 ORDER BY payment_method_id");
				
				$db->next_record();
				$creditcard = $db->f('payment_method_discount') * -1;
				$creditcard = $db->f('payment_method_discount_is_percent') ? $creditcard.'%' : '$'.$creditcard;
				
				$db->next_record();
				$echeque = $db->f('payment_method_discount') * -1;
				$echeque = $db->f('payment_method_discount_is_percent') ? $echeque.'%' : '$'.$echeque;				
				?>
                <?php echo str_replace('{echeque}', $echeque, str_replace('{creditcard}', $creditcard, $VM_LANG->_('VM_BOOKING_TERMS'))); ?>
            </div>
        </fieldset>
        <table>
            <tr>
                <td align="left">
                    <?php /* <input type="reset" value="Reset" class='button' /> */ ?>
                </td>
                <td align="right">
                    <input id='save_checkout' type="submit" value="<?php echo !$status ? ' Update ' : ' Book Now ' ?>"<?php echo!$status ? 'disabled="disabled"' : ''?> class='button' />
                </td>
            </tr>
        </table>
    </form>
</div>
<div class='note'>
    <span style='color: red; font-weight: bold'><?php echo $VM_LANG->_('VM_BOOKING_PLAESE_NOTE')?></span>
</div>
