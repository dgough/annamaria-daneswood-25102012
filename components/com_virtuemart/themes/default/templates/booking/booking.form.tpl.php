<?php
global $VM_LANG;
$ps_calendar = new ps_calendar('vb_book',$propertyid, 1);
$ps_calendar->setScripts();
$ps_calendar->getCalendar();
?>

<table>
    <tr>
        <td><?php echo $VM_LANG->_('VM_BOOKING_DATE_FROM') ?></td><td><?php echo $ps_calendar->renderInput('dateFrom',$dateFrom) ?></td>
        <td><?php echo $VM_LANG->_('VM_BOOKING_DATE_TO') ?></td><td><?php echo $ps_calendar->renderInput('dateTo',$dateTo,'dateFrom') ?></td>
    </tr>
    <?php /*
    <tr>
        <td><label>Arrival airport</label></td><td><input type="text" name="arrivalAirport" value='<?php echo $arrivalAirport ?>' /></td>
        <td><label>Dept airport</label></td><td><input type="text" name="departureAirport" value='<?php echo $departureAirport ?>' /></td>
    </tr>
    <tr>
        <td><label>Arrival flight</label></td><td><input type="text" name="arrivalFlightNo" value='<?php echo $arrivalFlightNo ?>' /></td>
        <td><label>Dept flight</label></td><td><input type="text" name="departureFlightNo" value='<?php echo $departureFlightNo ?>' /></td>
    </tr>
    */ ?>
    <tr>
        <td>
            <label><?php echo $VM_LANG->_('VM_BOOKING_PEOPLE') ?></label>
        </td>
        <td>
            <?php echo ps_html::dropdown_display('people', $people, $people_list,1,'',''); ?>
        </td>
        <td>
        	<?php if($petCost && $allowsPets){ ?>
        	<label><?php echo $VM_LANG->_('VM_BOOKING_PETS') ?></label>
        	<?php } ?>        	
        </td>
        <td>
        
        <?php  if($petCost && $allowsPets){ 
        	
        	$pet_list = array('0', '1', '2', '3', '4', '5');
            echo ps_html::dropdown_display('pets', $pets, $pet_list, 0); 
        
         	echo sprintf($VM_LANG->_('VM_BOOKING_PET_PER_NIGHT'), $CURRENCY_DISPLAY->getFullValue($petCost)); 
		 } elseif (!$allowsPets) {
		 	echo '<input type="hidden" name="pets" value="0" />';
		 }
		 
		 ?> 

        
        	<?php /* if($petCost){ ?>
        		<input type='checkbox' value="1" name='pets' <?php echo $pets ? 'checked="checked"' : '' ?> style="vertical-align:middle;" /> @ <?php echo $CURRENCY_DISPLAY->getFullValue($petCost) ?> per night.
        	<?php } */ ?>
        
        </td>
    </tr>
    <tr>
       	<td>
       		<?php if($isAdmin){ ?><?php echo $VM_LANG->_('VM_BOOKING_OWNER'); } ?>   			
   		</td>
   		<td>
       		<?php if($isAdmin){ ?>
       			<input type='radio' value="1" name="make_free" <?php echo $isFree ? 'checked="checked"' : '' ?>/> <?php echo $VM_LANG->_('VM_BOOKING_YES') ?><br />
       			<input type='radio' value="0" name="make_free" <?php echo !$isFree ? 'checked="checked"' : '' ?>/> <?php echo $VM_LANG->_('VM_BOOKING_NO') ?>     			
       		<?php } ?>   			
   		</td>
   		<td></td>
        <td>        	
            <input type="submit" style="float: right" id='save_form' name="save_form" value=' <?php echo $VM_LANG->_('VM_BOOKING_UPDATE') ?> ' class='button' />                      
        </td>
    </tr>    
</table>
<input type="hidden" name="property_id" value="<?php echo $propertyid; ?>" />