<script type="text/javascript" src="/components/com_virtuemart/js/functions.js"></script>
<script type="text/javascript">
//<!--

window.addEvent('domready', function() {
	
	initMsg()	
	var task = '';
		
	if ($('booking_wrap_extras')) {
		
		$('extras_text').setHTML('Extras will be selectable upon completion of the primary booking information');
		
		if($('booking_wrap_extras')) {
			var booking_extras_slide = new Fx.Slide('booking_wrap_extras', {duration: 750});
			<?php if(!$status){ ?>
				booking_extras_slide.hide();	
				$('save_checkout').setProperty('disabled','true');	
			<?php } ?>
		}
		
	}
	
	var status = <?php echo $status ? $status : 0 ?>;	
	var checkout = 0;
	
	$('save_form').addEvent('click', function() {
		task = 'main_form';
		checkout = 0;
	});
	
	if($('save_extras')){
		$('save_extras').addEvent('click', function() {
			task = 'extras_form';
			checkout = 0;
		});
	}
	
	$('save_checkout').addEvent('click', function() {
		checkout = 1;
	});	
		
	$('booking_form').addEvent('submit', function(e) {
		
		if(task ==''){
			task = 'main_form';
		}
		
		//Show the loading indicator
		var element = (task == 'main_form' ? 'booking_wrap_form' : 'booking_wrap_extras');
		showLoadingScreen(element);
				
		/**
		 * Prevent the submit event
		 */
		new Event(e).stop();
	 	
		this.setProperty('action','index2.php?option=com_virtuemart&page=booking.index&no_html=1&only_page=1&action=ajax&task='+task);
		
		this.send({
			onComplete: updateRegion
		});
				
	});
	
	
	function setState(state){
		
		status = state;
		if(status == 1){
			$('save_checkout').setProperty('value','Continue');
			$('save_checkout').removeProperty('disabled');
		}else{
			$('save_checkout').setProperty('value','Save');
			$('save_checkout').setProperty('disabled','true');
			save_checkout = 0;
		}
	}
	
	var loadingScreen;	
	function showLoadingScreen(element){
				
		//Show loading screens		
		var statusIndicator = new Element('div',{'class':'loading'});
		statusIndicator.setOpacity(0.8);
		
		statusIndicator.injectInside('booking_wrapper');
		
		loadingScreen = statusIndicator;
	}
	
	function updateRegion(responseText, responseXML){
		
		if(responseXML != null){
			
			//Set the global state variable
			setState(getText(responseXML.getElementsByTagName('status').item(0)));
			
			var message = getText(responseXML.getElementsByTagName('message').item(0));
			var message_clean = getText(responseXML.getElementsByTagName('message_clean').item(0));
			var form = getText(responseXML.getElementsByTagName('form').item(0));
			var extras = getText(responseXML.getElementsByTagName('extras').item(0));		
			var total_top = getText(responseXML.getElementsByTagName('total_top').item(0));	
			var total_bottom = getText(responseXML.getElementsByTagName('total_bottom').item(0));	
			
			showMessage(message,message_clean);
			$('booking_wrap_form').setHTML(form);
			if($('booking_wrap_extras')) $('booking_wrap_extras').setHTML(extras);
			$('booking_wrap_total_top').setHTML(total_top);
			$('booking_wrap_total_bottom').setHTML(total_bottom);

			if(!(status == 1 && checkout == 1)){
				loadingScreen.remove();
			}
							
			if(status == 1) {				
				if (typeof(booking_extras_slide) != 'undefined') {
					booking_extras_slide.slideIn();
					$('extras_text').setHTML('');
				}
			}
		}else{
			$('booking_form').removeEvent('submit');
			$('booking_form').setProperty('action','index.php');
			$('booking_form').submit();
		}

		if(status == 1 && checkout == 1){
			$('booking_form').setProperty('action','index.php?page=checkout.index&ssl_redirect=1&option=com_virtuemart');
			$('booking_form').submit();
			return true;
		}
	}
	
	function getText(node){
		
		if(typeof(node.text) == 'undefined'){
			return node.textContent;
		}else{
			return node.text;
		}
		
		
	}
	
	var shop_msg_parent;
	var booking_msg_slide;
	
	function initMsg(){
				
		var shop_info = $$('div.shop_info');
		var shop_err = $$('div.shop_error');
		var shop_warning = $$('div.shop_warning');		
		
		if(shop_info.length) shop_msg_parent = shop_info.getParent();
		if(shop_err.length) shop_msg_parent = shop_err.getParent();
		if(shop_warning.length) shop_msg_parent = shop_warning.getParent();
		
		if(!shop_msg_parent){
			shop_msg_parent = new Element('div',{'id':'shop_msg_parent'});
			shop_msg_parent.injectTop('vmMainPage');
		}else{
			shop_msg_parent.setProperty('id','shop_msg_parent');
		}
		
		booking_msg_slide = new Fx.Slide('shop_msg_parent', {duration: 750});
	}
	
	function showMessage(message, message_clean){
		
		if(message != ''){
			if(status == 0) alert(message_clean)
			shop_msg_parent.setHTML(message);
			booking_msg_slide.slideIn();
		}else{
			booking_msg_slide.slideOut();
			shop_msg_parent.setHTML(message);
		}
	}
	
});

//-->
</script>
<style type="text/css">
#booking_wrapper {
	position:relative;
}
#booking_wrapper table{
width: 100%;
}

#booking_wrapper fieldset{
border: 1px solid black;
margin: 10px 0px;
padding: 10px;
/*position:relative;*/
}

#booking_wrapper #booking_wrap_msg{
padding: 1px 0px;
}

#booking_wrapper .total{
float: right;
}

#booking_wrapper legend{
font-weight: bold;
font-size: 120%;
}

</style>
<br />
<h1>Booking Summary</h1>
<p>Thank you for your interest in booking with Anna Maria Vacations. We will now take you through the booking process. </p>
<p>Please follow the three step process on this screen to confirm the details of your desired booking. </p>
<div id="booking_wrapper">
	<div id='booking_wrap_msg'></div>
	<form id="booking_form" action="index.php" method="post">
	<fieldset>
       	<legend>1. Your Provisional booking summary</legend>
       	<div class='tip'>
			Please check your provisional booking summary details are correct. Note: the cost of rental is excluding tax. 
		</div>
        <div id="booking_wrap_total_top" class='total'>
        	<?php
			echo $outputTotalTop;
			?>
        </div>
		<div id='booking_wrap_property'>
		<?php
		echo $outputProperty;
		?>
		</div>
    </fieldset>   
    <fieldset>
    	<legend>2. Amend / Update your booking</legend>
    	<div class='tip'>
			Use this section to change the dates of your booking or add any additional options you may require including additional persons. Clicking UPDATE will update your provisional booking summary.
		</div>
    	<div id='booking_wrap_form'>
	    <?php
		echo $outputForm;
		?>
		</div>
    </fieldset>
    
    <?php if($enableExtras){ ?>
    <fieldset>
        <legend>Extras</legend>
        <div id="extras_text"></div>
        <div id='booking_wrap_extras'>
        <?php
            echo $outputExtras;
        ?>
        </div>
    </fieldset>
    <?php } ?>
    
    <fieldset>
    	<legend>3. Your Final Booking summary</legend>
    	<div class='tip'>
			This section shows the final total price of your booking including itemised costs, discounts applied and your total cost including state and resort tax. Click BOOK NOW to confirm and continue your booking. 
		</div>
    	<div id='booking_wrap_total_bottom' class='total'>
    	<?php
		echo $outputTotalBottom;
		?>
		</div>
		<div style='font-size: 75%; float: left; width: 50%; color: red'>
		PAYMENTS <br />
		$250 non-refundable fee is charged to your Visa or Mastercard at time of Booking.		
		This is part of the total cost of your reservation.  Remaining payments by check, bank transfer or credit card.  25% is payable within 7 days of receipt of the reservation. Final balance due must be received 90 days prior to arrival.  Reservations made within 90 days prior to arrival, full payment is due immediately.<br />  
		All funds should be made payable to Anna Maria Vacations Inc.  (Please note that for non USA residents the US dollar rate
		prevails.)  Exchange rate differences will be debited or credited.
		<br />
		<br />
		CANCELATIONS<br />
		Refunds are subject to Anna Maria Vacations ability to re-rent the accommodation.
		</div>
    </fieldset>
    <table>
    	<tr>
        	<td align="left"><input type="reset" value="Reset" class='button' /></td>
            <td align="right"><input id='save_checkout' type="submit" value="<?php echo !$status ? ' Update ' : ' Book Now ' ?>" <?php echo !$status ? 'disabled="disabled"' : '' ?> class='button' /></td>
        </tr>
    </table>
    <input type="hidden" name="option" value="com_virtuemart" />
    <input type="hidden" name="page" value="booking.index" />
    </form>
</div>
<div class='note'>
<span style='color: red; font-weight: bold'>! Please note your booking will not be confirmed until you have paid the initial $250 deposit and received your confirmation email.</span>
</div>