<?php
//Quick price form for use with Hot Property
//Available vars:
/*
$price_nightly - string formatted price with currency included;
$price_weekly - string formatted price with currency included;
$min_net - int;
$max_net - int;
$min_gross - int;
$max_gross - int;
*/
?>
<div class='vb_price'>	
	<div class='vb_price_bounds'>
		<?php 	
		echo sprintf('From %s to %s per week',$min->currency_weekly,$max->currency_weekly);
		?>
	</div>
</div>