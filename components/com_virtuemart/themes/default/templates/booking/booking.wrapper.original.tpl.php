<script type="text/javascript" src="/components/com_virtuemart/js/functions.js"></script>
<script type="text/javascript">
//<!--

window.addEvent('domready', function() {
	
	initMsg()	
	var task = '';
		
	if ($('booking_wrap_extras')) {
		
		$('extras_text').setHTML('Extras will be selectable upon completion of the primary booking information');
		
		if($('booking_wrap_extras')) {
			var booking_extras_slide = new Fx.Slide('booking_wrap_extras', {duration: 750});
			<?php if(!$status){ ?>
				booking_extras_slide.hide();	
				$('save_checkout').setProperty('disabled','true');	
			<?php } ?>
		}
		
	}
	
	var status = <?php echo $status ? $status : 0 ?>;	
	var checkout = 0;
	
	$('save_form').addEvent('click', function() {
		task = 'main_form';
		checkout = 0;
	});
	
	if($('save_extras')){
		$('save_extras').addEvent('click', function() {
			task = 'extras_form';
			checkout = 0;
		});
	}
	
	$('save_checkout').addEvent('click', function() {
		checkout = 1;
	});	
		
	$('booking_form').addEvent('submit', function(e) {
		/**
		 * Prevent the submit event
		 */

		if(status && checkout){
			this.setProperty('action','index.php?page=checkout.index&ssl_redirect=1&option=com_virtuemart');
			this.submit();
			return;
		}else if(task ==''){
			task = 'main_form';
		}
		
		//Show the loading indicator
		var element = (task == 'main_form' ? 'booking_wrap_form' : 'booking_wrap_extras');
		showLoadingScreen(element);
		
		new Event(e).stop();
	 	
		this.setProperty('action','index2.php?option=com_virtuemart&page=booking.index&no_html=1&only_page=1&action=ajax&task='+task);
		
		this.send({
			onComplete: updateRegion
		});
	});
	
	
	function setState(state){
		
		status = state;
		if(status == 1){
			$('save_checkout').setProperty('value','Continue');
			$('save_checkout').removeProperty('disabled');
		}else{
			$('save_checkout').setProperty('value','Save');
			$('save_checkout').setProperty('disabled','true');
		}
	}
	
	var loadingScreen;	
	function showLoadingScreen(element){
				
		//Show loading screens		
		var statusIndicator = new Element('div',{'class':'loading'});
		statusIndicator.setOpacity(0.8);
		
		statusIndicator.injectInside('booking_wrapper');
		
		loadingScreen = statusIndicator;
	}
	
	function updateRegion(responseText, responseXML){
		
		if(responseXML != null){
			
			//Set the global state variable
			setState(getText(responseXML.getElementsByTagName('status').item(0)));
			
			var message = getText(responseXML.getElementsByTagName('message').item(0));
			var form = getText(responseXML.getElementsByTagName('form').item(0));
			var extras = getText(responseXML.getElementsByTagName('extras').item(0));		
			var total = getText(responseXML.getElementsByTagName('total').item(0));	
			
			showMessage(message);
			$('booking_wrap_form').setHTML(form);
			if($('booking_wrap_extras')) $('booking_wrap_extras').setHTML(extras);
			$('booking_wrap_total_top').setHTML(total);
			$('booking_wrap_total_bottom').setHTML(total);
							
			loadingScreen.remove();
							
			if(status == 1) {				
				if (typeof(booking_extras_slide) != 'undefined') {
					booking_extras_slide.slideIn();
					$('extras_text').setHTML('');
				}
			}
		}else{
			$('booking_form').removeEvent('submit');
			$('booking_form').setProperty('action','index.php');
			$('booking_form').submit();
		}
		
	}
	
	function getText(node){
		
		if(typeof(node.text) == 'undefined'){
			return node.textContent;
		}else{
			return node.text;
		}
		
		
	}
	
	var shop_msg_parent;
	var booking_msg_slide;
	
	function initMsg(){
				
		var shop_info = $$('div.shop_info');
		var shop_err = $$('div.shop_error');
		var shop_warning = $$('div.shop_warning');		
		
		if(shop_info.length) shop_msg_parent = shop_info.getParent();
		if(shop_err.length) shop_msg_parent = shop_err.getParent();
		if(shop_warning.length) shop_msg_parent = shop_warning.getParent();
		
		if(!shop_msg_parent){
			shop_msg_parent = new Element('div',{'id':'shop_msg_parent'});
			shop_msg_parent.injectTop('vmMainPage');
		}else{
			shop_msg_parent.setProperty('id','shop_msg_parent');
		}
		
		booking_msg_slide = new Fx.Slide('shop_msg_parent', {duration: 750});
	}
	
	function showMessage(message){
		
		if(message != ''){
			shop_msg_parent.setHTML(message);
			booking_msg_slide.slideIn();
		}else{
			booking_msg_slide.slideOut();
			shop_msg_parent.setHTML(message);
		}
	}
	
});

//-->
</script>
<style type="text/css">
#booking_wrapper {
	position:relative;
}
#booking_wrapper table{
width: 100%;
}

#booking_wrapper fieldset{
border: 1px solid black;
margin: 10px 0px;
padding: 10px;
/*position:relative;*/
}

#booking_wrapper #booking_wrap_msg{
padding: 1px 0px;
}

#booking_wrapper .total{
float: right;
}

</style>
<div id="booking_wrapper">
	<div id='booking_wrap_msg'></div>
	<form id="booking_form" action="index.php" method="post">
	<fieldset>
       	<legend><?php echo $VM_LANG->_('VM_BOOKING_PROPERTY_SUMMARY'); ?></legend>
        <div id="booking_wrap_total_top" class='total'>
        	<?php
			echo $outputTotal;
			?>
        </div>
		<div id='booking_wrap_property'>
		<?php
		echo $outputProperty;
		?>
		</div>
    </fieldset>   
    <fieldset>
    	<legend><?php echo $VM_LANG->_('VM_BOOKING_INFO'); ?></legend>
    	<div id='booking_wrap_form'>
	    <?php
		echo $outputForm;
		?>
		</div>
    </fieldset>
    
    <?php if($enableExtras){ ?>
    <fieldset>
        <legend><?php echo $VM_LANG->_('VM_BOOKING_EXTRAS'); ?></legend>
        <div id="extras_text"></div>
        <div id='booking_wrap_extras'>
        <?php
            echo $outputExtras;
        ?>
        </div>
    </fieldset>
    <?php } ?>
    
    <fieldset>
    	<legend><?php echo $VM_LANG->_('VM_BOOKING_TOTAL'); ?></legend>
    	<div id='booking_wrap_total_bottom' class='total'>
    	<?php
		echo $outputTotal;
		?>
		</div>
    </fieldset>
    <table>
    	<tr>
        	<td align="left"><input type="reset" value="Reset" class='button' /></td>
            <td align="right"><input id='save_checkout' type="submit" value="<?php echo !$status ? 'Save' : 'Continue' ?>" <?php echo !$status ? 'disabled="disabled"' : '' ?> class='button' /></td>
        </tr>
    </table>
    <input type="hidden" name="option" value="com_virtuemart" />
    <input type="hidden" name="page" value="booking.index" />
    </form>
</div>