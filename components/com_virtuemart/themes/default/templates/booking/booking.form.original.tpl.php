<?php
$ps_calendar = new ps_calendar();
$ps_calendar->setScripts();
$ps_calendar->getCalendar($propertyid);
?>

<table>
    <tr>
        <td><?php echo $VM_LANG->_('VM_BOOKING_DATE_FROM') ?></td><td><?php echo $ps_calendar->renderInput('dateFrom',$dateFrom) ?></td>
        <td><?php echo $VM_LANG->_('VM_BOOKING_DATE_TO') ?></td><td><?php echo $ps_calendar->renderInput('dateTo',$dateTo,'dateFrom') ?></td>
    </tr>
    <tr>
        <td><label><?php echo $VM_LANG->_('VM_BOOKING_ARRIVAL_AIRPORT') ?></label></td><td><input type="text" name="arrivalAirport" value='<?php echo $arrivalAirport ?>' /></td>
        <td><label><?php echo $VM_LANG->_('VM_BOOKING_DEPT_AIRPORT') ?></label></td><td><input type="text" name="departureAirport" value='<?php echo $departureAirport ?>' /></td>
    </tr>
    <tr>
        <td><label><?php echo $VM_LANG->_('VM_BOOKING_ARRIVAL_FLIGHT') ?></label></td><td><input type="text" name="arrivalFlightNo" value='<?php echo $arrivalFlightNo ?>' /></td>
        <td><label><?php echo $VM_LANG->_('VM_BOOKING_DEPT_FLIGHT') ?></label></td><td><input type="text" name="departureFlightNo" value='<?php echo $departureFlightNo ?>' /></td>
    </tr>
    <tr>
        <td>
            <p><label><?php echo $VM_LANG->_('VM_BOOKING_PEOPLE') ?></label></td><td>
            <?php echo ps_html::dropdown_display('people', $people, $people_list,'','',''); ?></p>
        </td>
        <td><?php if($isAdmin){ ?><?php echo $VM_LANG->_('VM_BOOKING_OWNER') ?><?php } ?></td>
        <td>        	
            <input type="submit" style="float: right" id='save_form' name="save_form" value='Save' class='button' />
            <input type="hidden" name="property_id" value="<?php echo $propertyid; ?>" />
            <?php if($isAdmin){ ?>
            	<input type="radio" name="make_free" value="1" <?php echo $isFree ? 'checked="checked"' : '' ?> /><?php echo $VM_LANG->_('VM_BOOKING_YES') ?><br />
            	<input type="radio" name="make_free" value="-1" <?php echo !$isFree ? 'checked="checked"' : '' ?> /><?php echo $VM_LANG->_('VM_BOOKING_NO') ?>           
            <?php } ?>           
        </td>
    </tr>
</table>