<?php
//Quick price form for use with Hot Property
?>
<div class='vb_price' style="text-align: right">
	<?php if($total){ ?><div class='vb_price_subtotal'><?php echo $CURRENCY_DISPLAY->getFullValue($CURRENCY->convert($total)) ?></div><?php } ?>

	<?php /*if($original){ ?><div class='vb_price_original'><?php echo $CURRENCY_DISPLAY->getFullValue($CURRENCY->convert($original)) ?></div><?php } ?>
	<?php if($total){ ?><div class='vb_price_subtotal'><?php echo $CURRENCY_DISPLAY->getFullValue($CURRENCY->convert($total)) ?></div><?php } ?>
	<?php if($tax){ ?><div class='vb_price_tax'><?php echo $CURRENCY_DISPLAY->getFullValue($CURRENCY->convert($tax)) ?></div><?php }
	<?php if($max){ ?><span class='vb_price_max'><?php echo $VM_LANG->_('VM_BOOKING_WAS').' '.$CURRENCY_DISPLAY->getFullValue($CURRENCY->convert($max)) ?></span><?php }  ?>
	<?php if($subtotal){ ?><span class='vb_price_total'><?php echo $VM_LANG->_('VM_BOOKING_NOW').' '.$CURRENCY_DISPLAY->getFullValue($CURRENCY->convert($subtotal)) ?></span><?php } ?>
	<?php if($max && $subtotal){ ?><span class='vb_price_max'><?php echo $VM_LANG->_('VM_BOOKING_SAVE').' '.$CURRENCY_DISPLAY->getFullValue($CURRENCY->convert($max - $subtotal)) ?></span><?php } ?>
  */ ?>
</div>