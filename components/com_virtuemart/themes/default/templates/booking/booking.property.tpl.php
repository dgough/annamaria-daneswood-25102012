<?php
//summary of property on booking page

?>
<div id="booking_property">
	<div id="booking_prop_image" style='float: left'>
    	<a href='<?php echo $prop_url ?>' target="_blank"><img alt="<?php echo $prop_name ?>" src="<?php echo $prop_thumb ?>" class='photo_border' width="<?php echo $prop_thumb_width ?>px" /></a>
    </div>
    <div style='margin-left: <?php echo $prop_thumb_width+20 ?>px'>
	    <h2><?php echo $prop_name ?></h2>
	    <div id="booking_prop_summary">
	    	<?php echo $prop->address ?><br />
	    	<?php echo $prop->suburb ?><br />
	    	<?php echo $prop->state ?><br />
	    	<?php echo $prop->postcode ?><br />
	    </div>
    </div>
</div>