<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
/**
*
* @version $Id: login_form.tpl.php 1359 2008-04-07 20:02:17Z soeren_nb $
* @package VirtueMart
* @subpackage templates
* @copyright Copyright (C) 2008 soeren - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
if( vmIsJoomla('1.5')) {
	if(vmGet($_REQUEST,'tmpl') == 'component') {
		$lostPwUrl =  JRoute::_( 'index.php?option=com_user&view=reset&tmpl=component' );
	} else {
		$lostPwUrl =  JRoute::_( 'index.php?option=com_user&view=reset' );
	}
} else {
	$lostPwUrl = sefRelToAbs( basename($_SERVER['PHP_SELF']).'?option=com_registration&amp;task=lostPassword' );
}


?><div id="login">
<form action="<?php echo $action ?>" method="post" name="login" style="margin-left:20px;">
<div class="contentpane" style="background: transparent url(/components/com_login/lock.gif) no-repeat scroll right top; -moz-background-clip: -moz-initial; -moz-background-origin: -moz-initial; -moz-background-inline-policy: -moz-initial;">
	<h1 class="componentheading">  Login </h1>
	<div class="credentials">
	<div class="row">
	<label for="username_login"><?php echo $VM_LANG->_('USERNAME') ?>:</label><br/>
	<input type="text" id="username_login" name="username" class="inputbox" size="20" type="text" />
    </div>
    <div class="row">
	<label for="passwd_login"><?php echo $VM_LANG->_('PASSWORD') ?>:</label> <br/>
	<input type="password" id="passwd_login" name="passwd" class="inputbox" size="20" />
	(<a title="<?php echo $VM_LANG->_('LOST_PASSWORD'); ?>" href="<?php echo $lostPwUrl; ?>"><?php echo $VM_LANG->_('LOST_PASSWORD'); ?></a>)
	</div>
    <div class="row">
	<input id="com_login_remember" class="inputbox" type="checkbox" value="yes" alt="Remember Me" name="remember"/>
	<label for="com_login_remember"><?php echo $VM_LANG->_('REMEMBER_ME') ?></label>
	</div>
    <div class="row">
    <br />
	<input type="submit" name="Submit" class="button" value="<?php echo $VM_LANG->_('BUTTON_LOGIN') ?>" />
    </div>
	<br />
	<div class="lostpasssword">
		<a href="<?php echo sefRelToAbs( 'index.php?option=com_registration&amp;task=lostPassword' ); ?>">
		<?php echo _LOST_PASSWORD; ?>
		</a>
	</div>
		<?php
		global $mosConfig_allowUserRegistration;
		if ( $mosConfig_allowUserRegistration ) {
			?>
			<div class="register">
			<?php echo _NO_ACCOUNT; ?>
			<a href="<?php echo sefRelToAbs( 'index.php?option=com_registration&amp;task=register' ); ?>">
			<?php echo _CREATE_ACCOUNT;?>
			</a>
			<?php
		}
		?>
	</div>
	<noscript> !Warning! JavaScript must be enabled for proper operation. </noscript>
	<?php if( @VM_SHOW_REMEMBER_ME_BOX == '1' ) : ?>
	<input type="checkbox" name="remember" id="remember_login" value="yes" checked="checked" />
	<label for="remember_login"><?php echo $VM_LANG->_('REMEMBER_ME') ?></label>
	<?php else : ?>
	<input type="hidden" name="remember" value="yes" />
	<?php endif; ?>
	<input type="hidden" name="op2" value="login" />
	<input type="hidden" name="lang" value="<?php echo vmIsJoomla() ? $mosConfig_lang : $GLOBALS['mosConfig_locale'] ?>" />
	<input type="hidden" name="return" value="<?php echo $return_url ?>" />
	<input type="hidden" name="<?php echo $validate; ?>" value="1" />
    </div>
</form>
</div>
</div>

