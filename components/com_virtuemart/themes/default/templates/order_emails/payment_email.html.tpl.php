<?php
// $Id: confirmation_email.tpl.php 1308 2008-03-11 08:15:58Z thepisu $
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );
global $VM_LANG; 
?>
<html>
<head>
<title><?php echo $subject ?></title>
<style type="text/css">

#payment_email{
color: #333333;
font-family: Arial, Helvetica, sans-serif;
font-size:12px;
margin: 0px;
}

#payment_email table{
font-size: 100%;
}

</style>
</head>
<body>
<div id='payment_email'>
<img src="cid:vendor_image" alt="<?php echo $vendor_name ?>" border="0" align="right" />
<div style="clear: right"></div>
<table width="100%" cellpadding="3px">
	<tr>
		<td colspan="2">
		<p><?php echo $VM_LANG->_('VM_ORDER_EMAIL_DEAR'); echo $customer ?></p>
		<p><?php echo $VM_LANG->_('VM_ORDER_EMAIL_PAYMENT_IS_DUE'); echo $order_id ?>. <?php echo $VM_LANG->_('VM_ORDER_EMAIL_SEE_DETAILS') ?></p>
		<p><?php echo $VM_LANG->_('VM_ORDER_EMAIL_IF_YOU_HAVE_PAID') ?></p><br />
		</td>	
	</tr>
	<tr>
		<td width="100px" valign="top"><strong><?php echo $VM_LANG->_('VM_ORDER_EMAIL_PROPERTY') ?></strong></td>
		<td><?php echo nl2br($property) ?></td>
	</tr>
	<tr>
		<td width="100px"><strong><?php echo $VM_LANG->_('VM_ORDER_EMAIL_ARRIVAL_DATE') ?></strong></td>
		<td><?php echo $arrival ?></td>
	</tr>
	<tr>
		<td width="100px"><strong><?php echo $VM_LANG->_('VM_ORDER_EMAIL_DEPARTURE_DATE') ?></strong></td>
		<td><?php echo $departure ?></td>
	</tr>
	<tr>
		<td width="100px"><strong><?php echo $VM_LANG->_('VM_ORDER_EMAIL_NUMBER_OF_GUESTS') ?></strong></td>
		<td><?php echo $people ?></td>
	</tr>
	<tr>
		<td width="100px"><strong><?php echo $VM_LANG->_('VM_ORDER_EMAIL_ALMOST_DUE') ?></strong></td>
		<td><strong><?php echo $amount ?></strong></td>
	</tr>
	<tr>
		<td colspan="2">
			<br />
			<?php echo sprintf( $VM_LANG->_('VM_ORDER_EMAIL_OUSTANDING_BALANCE_FORMATTED'), $payment_url, $order_url,  ''); ?>
			
		</td>
	</tr>
	<tr>
		<td colspan="2" style="font-size: 90%">
		<?php ps_vendor::formatted_store_address(1) ?>
		</td>
	</tr>
</table>
</div>
</body>
</html>