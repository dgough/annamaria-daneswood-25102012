<?php
// $Id: confirmation_email.tpl.php 1308 2008-03-11 08:15:58Z thepisu $
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 

$checkins = array();
$checkouts = array();
?>
<html>
<head>
<title>[SUBJECT]</title>
<style type="text/css">

#payment_email{
color: #333333;
font-family: Arial, Helvetica, sans-serif;
font-size:12px;
margin: 0px;
}

#payment_email table{
font-size: 100%;
}

</style>
</head>
<body>
<div id='payment_email'>
<img src="cid:vendor_image" alt="<?php echo $vendor_name ?>" border="0" align="right" />
<div style="clear: right"></div>
<br />
<p><?php echo $VM_LANG->_('VM_ORDER_EMAIL_DEAR_NAME') ?></p>
<br />		

<p><?php echo $VM_LANG->_('VM_ORDER_EMAIL_WEEKLY_SCHEDULE') ?></p> 


<table cellpadding="3px" border='1px' cellspacing='0' style='text-align: center'>
	<tr>
		<td>&nbsp;</td>
		<?php 
		
			for($i = $fromDateUnix; $i < $toDateUnix; $i += 86400){
				echo '<td>'.date('l',$i).'</td>';
				$checkins[$i] = 0;
				$checkouts[$i] = 0;
			}
		?>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td align="left"><?php echo $VM_LANG->_('VM_ORDER_EMAIL_PROPERTY_B') ?></td>
		<?php 
			for($i = $fromDateUnix; $i < $toDateUnix; $i += 86400){
				echo '<td>'.date('d-M',$i).'</td>';
			}
		?>
		<td>&nbsp;</td>
	</tr>
	<?php 
		foreach($properties as $prop){
			echo "<tr><td align='left'>$prop->name</td>\n";
			
			$i = $fromDateUnix;			
			while($i < $toDateUnix){		

				echo '<td>';
				if(isset($prop->arrival[$i]) && isset($prop->departure[$i])){
					echo 'In / Out';
					$checkins[$i]++;
					$checkouts[$i]++;					
				}else if(isset($prop->arrival[$i])){
					echo 'In';
					$checkins[$i]++;
				}else if(isset($prop->departure[$i])){
					echo 'Out';
					$checkouts[$i]++;
				}else{
					echo '&nbsp;';
				}
				echo '</td>';
				
				$i = strtotime('+1 day', $i);
			}
			echo "<td align='left'>$prop->name</td></tr>\n";
		}	
	?>
	<tr>
		<td align='left'><?php echo $VM_LANG->_('VM_ORDER_EMAIL_CHECK_IN_OUT') ?></td>
		<?php 
			for($i = $fromDateUnix; $i < $toDateUnix; $i += 86400){
				echo '<td>';
				echo $checkouts[$i];
				echo ' / ';
				echo $checkouts[$i];
				echo '</td>';
			}
		?>
		<td>&nbsp;</td>
	</tr>
</table>

<p><?php echo $VM_LANG->_('VM_ORDER_EMAIL_THNAKS') ?></p>

<p>[FROM]</p>
</div>
</body>
</html>