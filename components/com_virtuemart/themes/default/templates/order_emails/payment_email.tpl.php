<?php
// $Id: confirmation_email.tpl.php 1308 2008-03-11 08:15:58Z thepisu $
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
global $VM_LANG; 
?>

<?php echo $VM_LANG->_('VM_ORDER_EMAIL_DEAR'); ?> <?php echo $customer ?>


<?php echo $VM_LANG->_('VM_ORDER_EMAIL_PAYMENT_IS_DUE'); echo $order_id ?><?php echo $VM_LANG->_('VM_ORDER_EMAIL_PLEASE_SEE_DETAILS') ?>


<?php echo $VM_LANG->_('VM_ORDER_EMAIL_PROPERTY') ?>
<?php echo $property ?>


<?php echo $VM_LANG->_('VM_ORDER_EMAIL_ARRIVAL_DATE') ?>
<?php echo $arrival ?>


<?php echo $VM_LANG->_('VM_ORDER_EMAIL_DEPARTURE_DATE') ?>
<?php echo $departure ?>


<?php echo $VM_LANG->_('VM_ORDER_EMAIL_NUMBER_OF_GUESTS') ?>
<?php echo $people ?>


<?php echo $VM_LANG->_('VM_ORDER_EMAIL_ALMOST_DUE') ?>
<?php echo $amount ?>

<?php echo sprintf( $VM_LANG->_('VM_ORDER_EMAIL_OUSTANDING_BALANCE'), $payment_url, $order_url ); ?>

<?php ps_vendor::formatted_store_address(0) ?>