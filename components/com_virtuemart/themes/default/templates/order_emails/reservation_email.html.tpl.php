<?php
// $Id: confirmation_email.tpl.php 1308 2008-03-11 08:15:58Z thepisu $
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
?>
<html>
<head>
<title><?php echo $subject ?></title>
<style type="text/css">

#vm_email{
color: #333333;
font-family: Arial, Helvetica, sans-serif;
font-size:12px;
margin: 0px;
}

#vm_email table{
font-size: 100%;
}

#vm_email table td{
vertical-align: top;
}

#vm_email td.heading{
font-weight: bold;
font-size: 120%;
padding: 10px 0px 5px;
}

#vm_email td.title{
font-weight: bold;
}

</style>
</head>
<body>
<div id='vm_email'>
<img src="cid:vendor_image" alt="vendor_image" border="0" align="right" />
<div style="clear: right"></div>
<br />
<p><?php echo $VM_LANG->_('VM_ORDER_EMAIL_DEAR'); echo $customer->name ?>,</p>

<p><?php echo $VM_LANG->_('VM_ORDER_EMAIL_CONFIRMATION') ?></p> 
   
  <table>
  	<tr>
  		<td class='heading' colspan='2'><?php echo $VM_LANG->_('VM_ORDER_EMAIL_GUEST_DETAILS') ?></td>
  	</tr>
  	<tr>
  		<td class='title' width='150px'><?php echo $VM_LANG->_('VM_ORDER_EMAIL_GUEST_NAME') ?></td>
  		<td><?php echo $customer->name ?></td>
  	</tr>
  	<tr> 
		<td class='title'><?php echo $VM_LANG->_('VM_ORDER_EMAIL_GUEST_ADDRESS') ?></td>
		<td><?php echo implode(', ',$customer->address_array) ?></td>
	</tr>
	<tr>
		<td class='title'><?php echo $VM_LANG->_('VM_ORDER_EMAIL_GUEST_TELEPHONE') ?></td>
		<td><?php echo $customer->phone_1 ?></td>
	</tr>
	<tr>
		<td class='title'><?php echo $VM_LANG->_('VM_ORDER_EMAIL_GUEST_EMAIL') ?></td>
		<td><?php $customer->user_email ?></td>
	</tr>
	<tr>
		<td class='title'><?php echo $VM_LANG->_('VM_ORDER_EMAIL_WEBSITE') ?></td>
		<td><a href='<?php echo $account_url ?>'><?php echo $VM_LANG->_('VM_ORDER_EMAIL_LOGIN') ?></a></td>
	</tr>
	<tr>
  		<td class='heading' colspan='2'><?php echo $VM_LANG->_('VM_ORDER_EMAIL_DETAILS') ?></td>
  	</tr>
	<tr>
		<td class='title'><?php echo $VM_LANG->_('VM_ORDER_EMAIL_PROPERTY_NAME') ?></td>
		<td><?php echo $property->name ?></td> 
	</tr>
	<tr>
		<td class='title'><?php echo $VM_LANG->_('VM_ORDER_EMAIL_PROPERTY_ADDRESS') ?></td>
		<td><?php echo implode('<br />',$property->address_array) ?></td>
	</tr>
	<tr>
		<td class='title'><?php echo $VM_LANG->_('VM_ORDER_EMAIL_PROPERTY_PHONE') ?></td>
		<td><?php  ?></td>
	</tr>
	<tr>
  		<td class='heading' colspan='2'><?php echo $VM_LANG->_('VM_ORDER_EMAIL_RESERVATION') ?></td>
  	</tr>
	<tr> 
		<td class='title'><?php echo $VM_LANG->_('VM_ORDER_EMAIL_ARRIVAL_DATE') ?></td>
		<td><?php echo $booking->arrival ?></td>
	</tr>
	<tr>
		<td class='title'><?php echo $VM_LANG->_('VM_ORDER_EMAIL_DEPARTURE_DATE') ?></td>
		<td><?php echo $booking->departure ?></td>
	</tr>
	<tr>
		<td class='title'><?php echo $VM_LANG->_('VM_ORDER_EMAIL_NUMBER_PEOPLE') ?></td>
		<td><?php echo $booking->people ?></td>
	</tr>
	<tr>
		<td class='title'><?php echo $VM_LANG->_('VM_ORDER_EMAIL_BALANCE_OUTSTANDING') ?></td>
		<td><?php echo $booking->balance ?></td>
	</tr>
	<tr>
		<td class='title'><?php echo $VM_LANG->_('VM_ORDER_EMAIL_CHECK_IN') ?></td>
		<td><?php echo $VM_LANG->_('VM_ORDER_EMAIL_ON_OR_AFTER') ?></td>
	</tr>
	<tr>
		<td class='title'><?php echo $VM_LANG->_('VM_ORDER_EMAIL_CHECK_OUT') ?></td>
		<td><?php echo $VM_LANG->_('VM_ORDER_EMAIL_LATEST_DEPARTURE') ?></td>
	</tr>
	<tr>
		<td class='title'><?php echo $VM_LANG->_('VM_ORDER_EMAIL_KEY_PICKUP') ?></td>																					
		<td><?php echo $VM_LANG->_('VM_ORDER_EMAIL_PLEASE_BRING_MAP') ?><a href='http://maps.google.com/?q=<?php echo $property->latitude.','.$property->longitude; ?>&iwloc=A&hl=en'><?php echo $VM_LANG->_('VM_ORDER_EMAIL_CLICK_FOR_MORE') ?></a></td>
	</tr>
		<td class='title'>Reservations</td>																					
		<td>Click <a href="<?php echo 'http://www.annamaria.com/index.php?&option=com_virtuemart&page=account.index'; ?>">here</a> to view your reservations</a></td>
	</tr>
	
	<tr>
		<td class='title'>To Access your property:</td>
		<td><p>Please locate the lock box by the front door, press reset, key in your access code which is shown below. Your keycode can also be found on your <a href='<?php echo $account_url ?>'>Personal Web Page</a>, pull down the bottom drawer, open the front door and replace key back in the lock box. Housekeeping will leave another set of keys inside the house for your everyday use. If you inadvertently get locked out you will still have access to the lock box keys.</p>
        
		<?php
		
		$order_id = $booking->order_id;
		
        $dbp = new ps_DB();
	$dbp->query("SELECT p.*, photo.thumb, photo.title, ef.value AS keycode, lat.value as latitude, lng.value as longitude, b.arrival as arrival 
		FROM #__{vm}_order_booking AS b
		LEFT JOIN #__{vm}_orders AS o ON o.order_id = b.order_id
		LEFT JOIN #__hp_properties AS p on p.id = b.property_id
		LEFT JOIN #__hp_photos AS photo ON photo.property = p.id
		LEFT JOIN #__hp_properties2 AS ef on ef.property = p.id AND ef.field = 48
		LEFT JOIN #__hp_properties2 AS lat on lat.property = p.id AND lat.field = 40
		LEFT JOIN #__hp_properties2 AS lng on lng.property = p.id AND lng.field = 41
		WHERE o.order_id = $order_id
		GROUP BY p.id
		ORDER BY photo.ordering");
	$dbp->next_record();
	
	echo "Keycode: ";
	echo $dbp->f('keycode');
	?>
    
        </td>
	</tr>
    
    
	
</table><?php 
echo $VM_LANG->_('VM_ORDER_EMAIL_NOTES_EMAIL'); ?>
<p><?php echo $vendor_name ?></p>
</div>
</body>
</html>
