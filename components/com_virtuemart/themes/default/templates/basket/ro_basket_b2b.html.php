<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
/**
*
* @version $Id: ro_basket_b2b.html.php 1377 2008-04-19 17:54:45Z gregdev $
* @package VirtueMart
* @subpackage templates
* @copyright Copyright (C) 2004-2005 Soeren Eberhardt. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
?>
<table style='text-align: right' width="100%" cellspacing="2" cellpadding="4" border="0">
  <tr class="sectiontableheader">
    <th align="left" width='50%'><?php echo $VM_LANG->_('PHPSHOP_CART_NAME') ?></th>
     <?php /* <th><?php //echo $VM_LANG->_('PHPSHOP_CART_SKU') ?></th> */ ?>
	<th><?php echo $VM_LANG->_('PHPSHOP_CART_PRICE') ?></th>
	<th><?php echo $VM_LANG->_('PHPSHOP_CART_QUANTITY') ?> (Nights)</th>
	<th><?php echo $VM_LANG->_('PHPSHOP_CART_SUBTOTAL') ?></th>
  </tr>
<?php foreach( $product_rows as $product ) { ?>
  <tr valign="top" class="<?php echo $product['row_color'] ?>">
	<td align="left"><?php echo $product['product_name'] . $product['product_attributes'] ?></td>
    <?php /* <td><?php echo $product['product_sku'] ?></td> */ ?>
    <td><?php echo $product['product_price'] ?></td>
    <td align="center"><?php echo $product['quantity'] ?></td>
    <td><?php echo $product['subtotal'] ?></td>
  </tr>
<?php } ?>
<!--Begin of SubTotal, Tax, Shipping, Coupon Discount and Total listing -->
  <tr class="sectiontableentry1">
    <td colspan="3"><?php echo $VM_LANG->_('PHPSHOP_CART_SUBTOTAL') ?>:</td> 
    <td><?php echo $subtotal_display ?></td>
  </tr>
<?php if( $payment_discount_before ) { ?>
  <tr class="sectiontableentry1">
    <td colspan="3"><?php echo $discount_word ?>:
    </td> 
    <td><?php echo $payment_discount_display ?></td>
  </tr>
<?php } 
if( $coupon_discount_before ) { ?>
  <tr class="sectiontableentry1">
    <td colspan="3"><?php echo $VM_LANG->_('PHPSHOP_COUPON_DISCOUNT') ?>:
    </td> 
    <td><?php echo $coupon_display ?></td>
  </tr>
<?php 
}
if( $shipping ) { ?>
  <tr class="sectiontableentry1">
	<td colspan="3"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_SHIPPING') ?>: </td> 
	<td><?php echo $shipping_display ?></td>
  </tr>
<?php } 
if ( $tax ) { ?>
  <tr class="sectiontableentry1">
        <td colspan="3" valign="top"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_TOTAL_TAX') ?>: </td> 
        <td><?php echo $tax_display ?></td>
  </tr>
<?php }
if( $payment_discount_after ) { ?>
  <tr class="sectiontableentry1">
    <td colspan="3"><?php echo $discount_word ?>:
    </td> 
    <td><?php echo $payment_discount_display ?></td>
  </tr>
<?php } 
if( $coupon_discount_after ) { ?>
  <tr class="sectiontableentry1">
    <td colspan="3"><?php echo $VM_LANG->_('PHPSHOP_COUPON_DISCOUNT') ?>:
    </td> 
    <td><?php echo $coupon_display ?></td>
  </tr>
<?php 
}
?>
  <tr>
    <td colspan="3">&nbsp;</td>
    <td colspan="1"><hr size="1" /></td>
  </tr>
  <tr class="sectiontableentry1">
    <td colspan="3"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_TOTAL') ?>: </td>
    <td><strong><?php echo $order_total_display ?></strong>
    </td>
  </tr>
  <?php if($order_deposit <= $order_total){ ?>
  <tr class="sectiontableentry2">
    <td colspan="3"><?php echo 'Due Today' /*$VM_LANG->_('PHPSHOP_ORDER_PRINT_TOTAL')*/ ?>: </td>
    <td colspan="1"><strong><?php echo $order_deposit_display ?></strong></td>
  </tr>  
  <?php } ?>  
  <tr>
    <td colspan="3"><hr /></td>
  </tr>
</table>
