<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
/**
*
* @version $Id: get_payment_method.tpl.php 1140 2008-01-09 20:44:35Z soeren_nb $
* @package VirtueMart
* @subpackage templates
* @copyright Copyright (C) 2007 Soeren Eberhardt. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/

ps_checkout::show_checkout_bar();

echo $basket_html;

echo '<br />';

$varname = 'PHPSHOP_CHECKOUT_MSG_' . CHECK_OUT_GET_PAYMENT_METHOD;
echo '<h4>'. $VM_LANG->_($varname) . '</h4>';
echo '<h4>'.$VM_LANG->_('PHPSHOP_CHECKOUT_CHARGES').'</h4>';
?>
<br />
<style type="text/css">
.formLabel { width:40% }
.formField { width: 50% }
</style>
<div id="creditcard_warning" style="color: red; float: left; width: 50%;">
<?php echo $VM_LANG->_('VM_CHECKOUT_CREDITCARD'); ?>
</div>
<div style="color: red; float: left; width: 50%;">
<?php echo $VM_LANG->_('VM_CHECKOUT_ECHEQUE') ?>
</div>
<div style="clear: both"></div>
<?php
echo ps_checkout::list_payment_methods( $payment_method_id );
?>
<fieldset>
	<legend><strong>Pay in full</strong></legend>
	<label for="pay_in_full">Check box to pay in full now.</label>
	<input type="checkbox" name="pay_in_full" id="pay_in_full" value="1" />
</fieldset>

<br />
<div id='echeque_image'>
<?php echo $VM_LANG->_('VM_CHECKOUT_ECHEQUE_DETAILS') ?>
<img src="<?php echo VM_THEMEURL.'images/echeque.jpg' ?>" alt="E-cheque" width="639px" height="228px" />
</div>