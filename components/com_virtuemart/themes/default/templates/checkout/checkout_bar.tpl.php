<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
/**
*
* @version $Id: checkout_bar.tpl.php 1095 2007-12-19 20:19:16Z soeren_nb $
* @package VirtueMart
* @subpackage templates
* @copyright Copyright (C) 2007 Soeren Eberhardt. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
return;
?>
<?php
// CSS style for the <td> tag of the cell which is actually highlighted
$highlighted_style = 'font-weight: bold;';
$imgWidth = 120;


echo '<div style="width: '.($imgWidth * (count($steps_to_do) + 2)).'px; margin: 0px auto; background: url( '. VM_THEMEURL .'images/checkout/checkout_bar_l.png) no-repeat center left">';
echo '<div style="background: url( '. VM_THEMEURL .'images/checkout/checkout_bar_r.png) no-repeat center right">';

echo '<div style="width: '.$imgWidth.'px; padding-top: 100px; float: left; text-align: center; background: url( '. VM_THEMEURL .'images/checkout/checkout_1.png) no-repeat center">';
echo '<a href="'. $sess->url(SECUREURL."index.php?page=booking.index").'">Booking Details</a>';
echo '</div>';

echo '<div style="width: '.$imgWidth.'px; padding-top: 100px; float: left; text-align: center; background: url( '. VM_THEMEURL .'images/checkout/checkout_2.png) no-repeat center">';
echo '<a href="'. $sess->url(SECUREURL."index.php?page=account.billing&amp;next_page=checkout.index").'">Address Details</a>';
echo '</div>';

$i = 3;
foreach ($steps_to_do as $step ) {

	echo '<div style="'.(($highlighted_step==$step[0]['step_order']) ? $highlighted_style : '').' width: '.$imgWidth.'px; padding-top: 100px; float: left; text-align: center; background: url( '. VM_THEMEURL .'images/checkout/checkout_'.$i.'.png) no-repeat center">';
	if ($highlighted_step > $step[0]['step_order'] ) {
		echo '<a href="'. $sess->url(SECUREURL."index.php?page=checkout.index&amp;option=com_virtuemart&amp;ship_to_info_id=$ship_to_info_id&amp;shipping_rate_id=".@$shipping_rate_id."&amp;checkout_stage=".$step[0]['step_order'] ).'">';
		foreach( $step as $substep ) {
			echo $substep['step_msg'].'<br />';
		}
		echo '</a>';
	}
	else {
		foreach( $step as $substep ) {
			if( $substep['step_order'] > $highlighted_step ) {
				echo $substep['step_msg'];
			} else {
				echo '<a href="#'.$substep['step_name'].'">'.$substep['step_msg'].'</a>';
			}
			echo '<br />';
		}
	}
	echo '</div>';
	$i++;
}
echo '
<br style="clear: left"/>
 </div>
 </div>
 <br />
';
