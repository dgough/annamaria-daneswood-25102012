<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );
/**
*
* @version $Id: account.order_details.tpl.php 1362 2008-04-08 19:56:44Z soeren_nb $
* @package VirtueMart
* @subpackage html
* @copyright Copyright (C) 2004-2008 soeren - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
mm_showMyFileName( __FILE__ );

if( $db->f('order_number')) {

/**
 * Get property details from DB
 */
/*
$db->query("SELECT *, min(photo.ordering) FROM #__hp_properties AS p
		LEFT JOIN #__hp_photos AS photo ON photo.property = p.id
		WHERE p.id = 4
		GROUP BY p.id
		ORDER BY photo.ordering");
*/
$dbp = new ps_DB();
$dbp->query("SELECT p.*, photo.thumb, photo.title, ef.value AS keycode, lat.value as latitude, lng.value as longitude 
		FROM #__{vm}_order_booking AS b
		LEFT JOIN #__{vm}_orders AS o ON o.order_id = b.order_id
		LEFT JOIN #__hp_properties AS p on p.id = b.property_id
		LEFT JOIN #__hp_photos AS photo ON photo.property = p.id
		LEFT JOIN #__hp_properties2 AS ef on ef.property = p.id AND ef.field = 48
		LEFT JOIN #__hp_properties2 AS lat on lat.property = p.id AND lat.field = 40
		LEFT JOIN #__hp_properties2 AS lng on lng.property = p.id AND lng.field = 41
		WHERE o.order_id = $order_id
		GROUP BY p.id
		ORDER BY photo.ordering");
$dbp->next_record();


$dbb = new ps_DB();
$dbb->query("SELECT b.*, p.name AS property FROM #__{vm}_order_booking AS b, #__hp_properties AS p WHERE b.property_id = p.id AND b.order_id = '$order_id'");

?>	


	<?php if (empty( $print )) : ?>
	<div class="buttons_heading">
	<?php echo vmCommonHTML::PrintIcon($sess->url('index2.php?pop=1&page='.$page.'&order_id='.$order_id, 0, 1, 1)); ?>
	</div>
	<div class="pathway"><?php echo $vmPathway; ?></div>	
	 <?php endif; ?>
<?php
/**
 * Get booking info text from DB
 */
$db->setQuery("SELECT id, introtext FROM #__content WHERE id = 30");
$db->loadObject( $article );

if($article){
?>
    <table width="100%">
      <tr class="sectiontableheader">
        <th align="left" colspan="2"><?php echo $VM_LANG->_('VM_ACCOUNT_FOOR_INFO') ?></th>
      </tr>
      <tr>
        <td colspan="2">
         <?php 	         
         echo $article->introtext
          ?>
       </td>
      </tr>
    </table>
<?php
}
?>	 

  <?php
  $dbt = new ps_DB();
//  $dbt->query('SELECT concat(`introtext`,`fulltext`) as text FROM #__content WHERE id = 36');
  echo $VM_LANG->_('VM_ACCOUNT_BOOKING_SUMMARY') ? '<div style="clear:both" class="info">' . $VM_LANG->_('VM_ACCOUNT_BOOKING_SUMMARY') . '</div>' : '';
  ?>
	<table width="100%" align="center" border="0" cellspacing="0" cellpadding="2">
	  <tr>
	    <td valign="top">
	     <h2><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_PO_LBL') ?></h2>
	     
	     <p><?php echo ps_vendor::formatted_store_address(true) ?></p>
	    </td>
	    <td valign="top" width="10%" align="right">
	    	<?php echo $vendor_image; ?>
		    <p>
		     	<?php echo $VM_LANG->_('VM_ORDER_CHECKIN_TIME') ?><br />
				<?php echo $VM_LANG->_('VM_ORDER_CHECKOUT_TIME') ?><br />


				<?php
				$arrive = $dbb->f('arrival');
				$arriveMinus10 = date("Y-m-d", strtotime($arrive."-10 days"));
				$today = date("Y-m-d");
								
				if($today > $arriveMinus10){
					echo "<div><strong>";
					echo $VM_LANG->_('VM_ORDER_KEYCODE');
					echo $dbp->f('keycode');
                	echo "</strong></div>";
					}
				?>


		     </p>
	     </td>
	  </tr>
	</table>
	<?php
	if ( $db->f("order_status") == "P" ) {
		// Copy the db object to prevent it gets altered
		$db_temp = ps_DB::_clone( $db );
	 /** Start printing out HTML Form code (Payment Extra Info) **/ ?>
	<table width="100%">
	  <tr>
	    <td width="100%" align="center">
	    <?php 
	    @include( CLASSPATH. "payment/".$dbpm->f("payment_class").".cfg.php" );
	
	    echo DEBUG ? vmCommonHTML::getInfoField($VM_LANG->_('VM_ACCOUNT_PAYMENT_CODE')) : '';
	
	    // Here's the place where the Payment Extra Form Code is included
	    // Thanks to Steve for this solution (why make it complicated...?)
	    if( eval('?>' . $dbpm->f("payment_extrainfo") . '<?php ') === false ) {
	    	echo vmCommonHTML::getErrorField( $VM_LANG->_('VM_ACCOUNT_C') . $dbpm->f( 'payment_method_name').' ('.$dbpm->f('payment_method_code').') '
	    	. $VM_LANG->_('VM_ACCOUNT_PARSE_ERROR'));
	    }
	      ?>
	    </td>
	  </tr>
	</table>
	<?php
		$db = $db_temp;
	}
	// END printing out HTML Form code (Payment Extra Info)
	?>
	<table border="0" cellspacing="0" cellpadding="2" width="100%">
	  <!-- begin customer information --> 
	  <tr class="sectiontableheader"> 
	    <th align="left" colspan="2"><?php echo $VM_LANG->_('PHPSHOP_ACC_ORDER_INFO') ?></th>
	  </tr>
	  <tr> 
	    <td><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_PO_NUMBER')?>:</td>
	    <td><?php printf("%08d", $db->f("order_id")); ?></td>
	  </tr>
	
	  <tr> 
		<td><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_PO_DATE') ?>:</td>
	    <td><?php echo vmFormatDate($db->f("cdate")+$time_offset); ?></td>
	  </tr>
	  <tr> 
	    <td><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_PO_STATUS') ?>:</td>
	    <td><?php echo ps_order_status::getOrderStatusName( $db->f("order_status") ); ?></td>
	  </tr>
	  <!-- End Customer Information --> 
	  <!-- Begin 2 column bill-ship to --> 
	  <tr class="sectiontableheader"> 
	    <th align="left" colspan="2"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_CUST_INFO_LBL') ?></th>
	  </tr>
	  <tr valign="top"> 
	    <td width="50%"> <!-- Begin BillTo -->
	      <table width="100%" cellspacing="0" cellpadding="2" border="0">
	        <tr> 
	          <td colspan="2"><strong><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_BILL_TO_LBL') ?></strong></td>
	        </tr>
	        <?php 
		foreach( $registrationfields as $field ) {
			if( $field->type == 'captcha') continue;
			if( $field->name == 'email') $field->name = 'user_email';
			?>
		  <tr> 
			<td align="right"><?php echo $VM_LANG->_($field->title) ? $VM_LANG->_($field->title) : $field->title ?>:</td>
			<td><?php
				switch($field->name) {
		          	case 'country':
		          		require_once(CLASSPATH.'ps_country.php');
		          		$country = new ps_country();
		          		$dbc = $country->get_country_by_code($dbbt->f($field->name));
	          			if( $dbc !== false ) echo $dbc->f('country_name');
		          		break;
		          	default: 
		          		echo $dbbt->f($field->name);
		          		break;
		          }
		          ?>
			</td>
		  </tr>
		  <?php
			}
		   ?>
	      </table>
	      <!-- End BillTo --> </td>
	    <td width="50%"> <!-- Begin Property -->
	    	<?php
	    		global $mosConfig_absolute_path;
	    		include_once(ADMINPATH.'../com_hotproperty/config.hotproperty.php');
	    		$imagesize = getimagesize($mosConfig_absolute_path.$hp_imgdir_thumb.$dbp->f('thumb'))
	    	?>	    
	    	<div style="float: right">
	    		<a href='<?php echo sefRelToAbs('index.php?option=com_hotproperty&task=view&id='.$dbp->f('id')) ?>' target='_blank'>
	    			<img style="margin: 3px; padding: 3px; border: 1px solid #ccc; background: #fff" title='<?php echo $dbp->f('title') ?>' src='<?php echo $mosConfig_live_site.$hp_imgdir_thumb.$dbp->f('thumb') ?>' <?php echo $imagesize[3] ?> />
	    		</a>

				<?php
				$arrive = $dbb->f('arrival');
				$arriveMinus10 = date("Y-m-d", strtotime($arrive."-10 days"));
				$today = date("Y-m-d");
								
				if($today > $arriveMinus10){
					echo "<div><strong>";
					echo $VM_LANG->_('VM_ORDER_KEYCODE');
					echo $dbp->f('keycode');
                	echo "</strong></div>";
					}
				?>
	
</div>
	    	<h4><a href='<?php echo sefRelToAbs('index.php?option=com_hotproperty&task=view&id='.$dbp->f('id')) ?>' target='_blank'><?php echo $dbp->f('name') ?></a></h4>
	    	<p>
	    		<?php echo $dbp->f('address') ?><br />
	    		<?php echo $dbp->f('suburb') ?><br />
	    		<?php echo $dbp->f('state') ?><br />
	    		<?php echo $dbp->f('country') ?><br />
	    		<?php echo $dbp->f('postcode') ?><br />
	    	</p>
	    	<div style="clear: both">
	    		<style type="text/css">
	    			#hp_map{
	    				width: 380px;
	    				height: 280px;
	    			}
	    		</style>
	    		<?php
	    		$p = $dbp->get_row();	    		
	    		include_once($mosConfig_absolute_path.'/components/com_hotproperty/includes/google_maps.php');
	    		getMap($p, 3);
	    		?>
	    	</div>
	    </td>
	  </tr>
	  <?php if ( $PSHOP_SHIPPING_MODULES[0] != "no_shipping" && $db->f("ship_method_id")) { ?> 
	  <tr> 
	    <td colspan="2"> 
	      <table width="100%" border="0" cellspacing="0" cellpadding="1">
	        
	        <tr class="sectiontableheader"> 
	          <th align="left"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_CUST_SHIPPING_LBL') ?></th>
	        </tr>
	        <tr> 
	          <td> 
	            <table width="100%" border="0" cellspacing="0" cellpadding="0">
	              <tr> 
	                <td><strong><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_SHIPPING_CARRIER_LBL') ?></strong></td>
	                <td><strong><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_SHIPPING_MODE_LBL') ?></strong></td>
	                <td><strong><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_PRICE') ?>&nbsp;</strong></td>
	              </tr>
	              <tr> 
	                <td><?php 
	                $details = explode( "|", $db->f("ship_method_id"));
	                echo $details[1];
	                    ?>&nbsp;
	                </td>
	                <td><?php 
	                echo $details[2];
	                    ?>
	                </td>
	                <td><?php 
	                     echo $CURRENCY_DISPLAY->getFullValue($details[3], '', $db->f('order_currency'));
	                    ?>
	                </td>
	              </tr>
	            </table>
	          </td>
	        </tr>
	        
	      </table>
	    </td>
	  </tr><?php
	  }
	
	  ?> 
	  <tr>
	    <td colspan="2">&nbsp;</td>
	  </tr>	  
	  <!-- Begin Order Items Information --> 
	  
	  <tr>
	    <td colspan="2">
	<?php
	$dbdl = new ps_DB;
	/* Check if the order has been paid for */
	if ($db->f("order_status") == ENABLE_DOWNLOAD_STATUS && ENABLE_DOWNLOADS) {
	
		$q = "SELECT `download_id` FROM #__{vm}_product_download WHERE";
		$q .= " order_id =" .(int)$vars["order_id"];
		$dbdl->query($q);
	
		// $q = "SELECT * FROM #__{vm}_product_download WHERE order_id ='" . $db->f("order_id")
		// $dbbt->query($q);
	
	
		// check if download records exist for this purchase order
		if ($dbdl->next_record()) {
			echo "<b>" . $VM_LANG->_('PHPSHOP_DOWNLOADS_CLICK') . "</b><br /><br />";
	
			echo($VM_LANG->_('PHPSHOP_DOWNLOADS_SEND_MSG_3').DOWNLOAD_MAX.". <br />");
	
			$expire = ((DOWNLOAD_EXPIRE / 60) / 60) / 24;
			echo(str_replace("{expire}", $expire, $VM_LANG->_('PHPSHOP_DOWNLOADS_SEND_MSG_4')));
			
			echo "<br /><br />";
		}
		//else {
			//echo "<b>" . $VM_LANG->_('PHPSHOP_DOWNLOADS_EXPIRED') . "</b><br /><br />";
		//}
	}
	?>
	    </td>
	  </tr>
	  <!-- END HACK EUGENE -->
	  <tr> 
	    <td colspan="2"> 
	      <table width="100%" cellspacing="0" cellpadding="2" border="0">
	      <?php
	      /*** Organic Mod ***/
	      /* Display booking info here */
      
		  	
			if($dbb->num_rows() > 0){
	
				$GLOBALS['VM_LANG']->load('booking');
			  	global $modulename;  	
			  	$oldmodulename = $modulename;
			  	$modulename = 'booking';
			  	?>
			  <tr>
			  	<td colspan="6" class='sectiontableheader'>
			  		<?php echo $VM_LANG->_('VM_BK_EMAIL_YOUR_BOOKING') ?>
			  	</td>
			  </tr>
		
			    <tr align="left">
					<th class="title"><?php echo $VM_LANG->_('VM_BK_EMAIL_PROPERTY') ?></th>
			        <th class="title"><?php echo $VM_LANG->_('VM_BK_EMAIL_ARRIVE') ?></th>
			        <th class="title"><?php echo $VM_LANG->_('VM_BK_EMAIL_DEPART') ?></th>
					<th class="title"><?php echo $VM_LANG->_('VM_BK_EMAIL_NIGHTS') ?></th>
					<th class="title"><?php echo $VM_LANG->_('VM_BK_EMAIL_SUBTOTAL') ?></th>
					<th class="title"><?php echo $VM_LANG->_('VM_BK_EMAIL_TOTAL') ?></th>
			    </tr>
				<tr>
					<td><?php echo $dbb->f('property') ?></td>
					<td><?php echo $dbb->f('arrival') ?></td>
					<td><?php echo $dbb->f('departure') ?></td>
					<td><?php echo round(((strtotime($dbb->f('departure')) - strtotime($dbb->f('arrival'))) / 86400)) ?></td>
					<td><?php echo $CURRENCY_DISPLAY->getFullValue($dbb->f('subtotal'), '', $db->f('order_currency')) ?></td>
					<td><?php echo $CURRENCY_DISPLAY->getFullValue($dbb->f('total'), '', $db->f('order_currency')) ?></td>
				</tr>
		
			  <?php 
			  $modulename = $oldmodulename;
			  } 
			  ?>
			
	        <?php 
	        $dbcart = new ps_DB;
	        $q  = "SELECT * FROM #__{vm}_order_item ";
	        $q .= "WHERE #__{vm}_order_item.order_id='$order_id' ";
	        $dbcart->query($q);
	        
	        $dbi = new ps_DB;
	        if($dbcart->num_rows()){
	        	?>
	        	<tr class="sectiontableheader"> 
		    		<th align="left" colspan="6"><?php echo $VM_LANG->_('PHPSHOP_ORDER_ITEM') ?></th>
			  	</tr>
		        <tr align="left"> 
		          <th><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_QTY') ?></th>
		          <th><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_NAME') ?></th>
		          <th><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_SKU') ?></th>
		          <th>&nbsp;</th>
		          <th><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_PRICE') ?></th>
		          <th align="right"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_TOTAL') ?>&nbsp;&nbsp;&nbsp;</th>
		        </tr>
		        <?php
	        
		        while ($dbcart->next_record()) {
		
		        	/* BEGIN HACK EUGENE */
		        	/*HACK SCOTT had to retest order status else unpaid were able to download*/
		        	if ($db->f("order_status") == ENABLE_DOWNLOAD_STATUS && ENABLE_DOWNLOADS) {
		        		/* search for download record that corresponds to this order item */
		        		$q = "SELECT `download_id` FROM #__{vm}_product_download WHERE";
		        		$q .= " `order_id`=" . intval($vars["order_id"]);
		        		$q .= " AND `product_id`=". intval($dbcart->f("product_id"));
		        		$dbdl->query($q);
		
		        	}
		        	/* END HACK EUGENE */
		
		        	$product_id = null;
		        	$dbi->query( "SELECT product_id FROM #__{vm}_product WHERE product_sku='".$dbcart->f("order_item_sku")."'");
		        	$dbi->next_record();
		        	$product_id = $dbi->f("product_id" );
		?> 
		        <tr align="left"> 
		          <td><?php $dbcart->p("product_quantity"); ?></td>
		          <td><?php 
		              if ($dbdl->next_record()) {
		        			// hyperlink the downloadable order item	
		        			$url = $mosConfig_live_site."/index.php?option=com_virtuemart&page=shop.downloads";
		        			echo '<a href="'."$url&download_id=".$dbdl->f("download_id").'">'
		        					. '<img src="'.VM_THEMEURL.'images/download.png" alt="'.$VM_LANG->_('PHPSHOP_DOWNLOADS_CLICK').'" align="left" border="0" />&nbsp;'
		        					. $dbcart->f("order_item_name")
		        					. '</a>';
						}
		        		else {
				        	if( !empty( $product_id )) {
				          		echo '<a href="'.$sess->url( $mm_action_url."index.php?page=shop.product_details&product_id=$product_id") .'" title="'.$dbcart->f("order_item_name").'">';
				          	}
				          	$dbcart->p("order_item_name");
				          	echo " <div style=\"font-size:smaller;\">" . $dbcart->f("product_attribute") . "</div>";
				          	if( !empty( $product_id )) {
				          		echo "</a>";
				          	}
		        		}
				?>
		          </td>
		          <td><?php $dbcart->p("order_item_sku"); ?></td>
		          <td>&nbsp;</td>
		          <td><?php /*
				$price = $ps_product->get_price($dbcart->f("product_id"));
				$item_price = $price["product_price"]; */
				if( $auth["show_price_including_tax"] ){
					$item_price = $dbcart->f("product_final_price");
				}
				else {
					$item_price = $dbcart->f("product_item_price");
				}
				echo $CURRENCY_DISPLAY->getFullValue($item_price, '', $db->f('order_currency'));
		
		           ?></td>
		          <td align="right"><?php $total = $dbcart->f("product_quantity") * $item_price; 
		          $subtotal += $total;
		          echo $CURRENCY_DISPLAY->getFullValue($total, '', $db->f('order_currency'));
		           ?>&nbsp;&nbsp;&nbsp;</td>
		        </tr><?php
		        }
	        }
	?> 
	        <tr> 
	          <td colspan="5" align="right">&nbsp;&nbsp;</td>
	          <td>&nbsp;</td>
	        </tr>
	        <tr> 
	          <td colspan="5" align="right"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_SUBTOTAL') ?> :</td>
	          <td align="right"><?php echo $CURRENCY_DISPLAY->getFullValue($db->f("order_subtotal"), '', $db->f('order_currency')) ?>&nbsp;&nbsp;&nbsp;</td>
	        </tr>
	<?php 
	/* COUPON DISCOUNT */
	$coupon_discount = $db->f("coupon_discount");
	$order_discount = $db->f("order_discount");
	
	if( PAYMENT_DISCOUNT_BEFORE == '1') {
		if (($db->f("order_discount") != 0)) {
	?>
	          <tr>
	              <td colspan="5" align="right"><?php 
	              if( $db->f("order_discount") > 0)
	              echo $VM_LANG->_('PHPSHOP_PAYMENT_METHOD_LIST_DISCOUNT');
	              else
	              echo $VM_LANG->_('PHPSHOP_FEE');
	                ?>:
	              </td> 
	              <td align="right"><?php
	              if ($db->f("order_discount") > 0 ) {
	              	echo "- ".$CURRENCY_DISPLAY->getFullValue(abs($db->f("order_discount")), '', $db->f('order_currency'));
	              }
	              elseif ($db->f("order_discount") < 0 )  {
	              	echo "+ ".$CURRENCY_DISPLAY->getFullValue(abs($db->f("order_discount")), '', $db->f('order_currency'));
	              } 
	              ?>
	              &nbsp;&nbsp;&nbsp;</td>
	          </tr>
	        
	        <?php 
		}
		if( $coupon_discount > 0 ) {
	?>
	        <tr>
	          <td colspan="5" align="right"><?php echo $VM_LANG->_('PHPSHOP_COUPON_DISCOUNT') ?>:
	          </td> 
	          <td align="right"><?php
	            echo "- ".$CURRENCY_DISPLAY->getFullValue( $coupon_discount, '', $db->f('order_currency') ); ?>&nbsp;&nbsp;&nbsp;
	          </td>
	        </tr>
	<?php
		}
	}
	?>      
	<?php if ( $PSHOP_SHIPPING_MODULES[0] != "no_shipping" && $db->f("ship_method_id")) { ?>   
	        <tr> 
	          <td colspan="5" align="right"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_SHIPPING') ?> :</td>
	          <td align="right"><?php 
	          $shipping_total = $db->f("order_shipping");
	          if ($auth["show_price_including_tax"] == 1)
	          $shipping_total += $db->f("order_shipping_tax");
	          echo $CURRENCY_DISPLAY->getFullValue($shipping_total, '', $db->f('order_currency'));
	
	            ?>&nbsp;&nbsp;&nbsp;</td>
	        </tr>
	  <?php } ?>
	  <?php
	  $tax_total = $db->f("order_tax") + $db->f("order_shipping_tax");
	  if ($auth["show_price_including_tax"] == 0) {
	  ?>
	        <tr> 
	          <td colspan="5" align="right"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_TOTAL_TAX') ?> :</td>
	          <td align="right"><?php 
	
	          echo $CURRENCY_DISPLAY->getFullValue($db->f("order_tax"), '', $db->f('order_currency'));
	            ?>&nbsp;&nbsp;&nbsp;</td>
	        </tr>
	<?php
	  }
	  if( PAYMENT_DISCOUNT_BEFORE != '1') {
	  	if (($db->f("order_discount") != 0)) {
	?>
	          <tr>
	              <td colspan="5" align="right"><?php 
	              if( $db->f("order_discount") > 0)
	              echo $VM_LANG->_('PHPSHOP_PAYMENT_METHOD_LIST_DISCOUNT');
	              else
	              echo $VM_LANG->_('PHPSHOP_FEE');
	                ?>:
	              </td> 
	              <td align="right"><?php
	              if ($db->f("order_discount") > 0 )
	              echo "- ".$CURRENCY_DISPLAY->getFullValue(abs($db->f("order_discount")), '', $db->f('order_currency'));
	              elseif ($db->f("order_discount") < 0 )
	                 echo "+ ".$CURRENCY_DISPLAY->getFullValue(abs($db->f("order_discount")), '', $db->f('order_currency')); ?>
	              &nbsp;&nbsp;&nbsp;</td>
	          </tr>
	        
	        <?php 
	  	}
	  	if( $coupon_discount > 0 ) {
	?>
	        <tr>
	          <td colspan="5" align="right"><?php echo $VM_LANG->_('PHPSHOP_COUPON_DISCOUNT') ?>:
	          </td> 
	          <td align="right"><?php
	            echo "- ".$CURRENCY_DISPLAY->getFullValue( $coupon_discount, '', $db->f('order_currency') ); ?>&nbsp;&nbsp;&nbsp;
	          </td>
	        </tr>
	<?php
	  	}
	  }
	?>      <tr> 
	          <td colspan="4" align="right">&nbsp;</td>
	          <td colspan="2" align="right"><hr/></td>
	        </tr>
	        <tr> 
	          <td colspan="5" align="right">
	          <strong><?php echo $VM_LANG->_('PHPSHOP_CART_TOTAL') .": "; ?></strong>
	          </td>
	          
	          <td align="right"><strong><?php  
	          echo $CURRENCY_DISPLAY->getFullValue($db->f("order_total"), '', $db->f('order_currency'));
	          ?></strong>&nbsp;&nbsp;&nbsp;</td>
	        </tr>
	  <?php
	  if ($auth["show_price_including_tax"] == 1) {
	  ?>
	        
	        <tr> 
	          <td colspan="4" align="right">&nbsp;</td>
	          <td colspan="2" align="right"><hr/></td>
	        </tr>
	        <tr> 
	          <td colspan="5" align="right"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_TOTAL_TAX') ?> :</td>
	          <td align="right"><?php 
	
	          echo $CURRENCY_DISPLAY->getFullValue($db->f("order_tax"), '', $db->f('order_currency'));
			  
	            ?>&nbsp;&nbsp;&nbsp;</td>
	        </tr>
	<?php
	  }
	  ?>    <tr> 
	          <td colspan="4" align="right">&nbsp;</td>
	          <td colspan="2" align="right"><hr/></td>
	        </tr>
	        <tr> 
	          <td colspan="4" align="right">&nbsp;</td>
	          <td colspan="2" align="right"><?php 
					echo ps_checkout::show_tax_details( $db->f('order_tax_details'), $db->f('order_currency') );
	            ?>&nbsp;&nbsp;&nbsp;</td>
	        </tr>
	      </table>
	    </td>
	  </tr>
	 </table>
	  <!-- End Order Items Information --> 
	
	<br />
	
	  <!-- Begin Payment Information --> 
	
	      <table width="100%">
	      <tr class="sectiontableheader"> 
	        <th align="left" colspan="2"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_PAYINFO_LBL') ?></th>
	      </tr>
	      <?php if(!@VB_STAGE_PAYMENTS){ ?>
		      <tr> 
		        <td width="20%"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_PAYMENT_LBL') ?> :</td>
		        <td><?php $dbpm->p("payment_method_name"); ?> </td>
		      </tr>
			  <?php
			  require_once(CLASSPATH.'ps_payment_method.php');
			  $ps_payment_method = new ps_payment_method;
			  $payment = $dbpm->f("payment_method_id");
		
			  if ($ps_payment_method->is_creditcard($payment)) {
		
			  	// DECODE Account Number
			  	$dbaccount = new ps_DB;
			  	$q = 'SELECT '.VM_DECRYPT_FUNCTION.'(order_payment_number,\''.ENCODE_KEY.'\') as account_number 
			  				FROM #__{vm}_order_payment WHERE order_id=\''.$order_id.'\'';
			  	$dbaccount->query($q);
		        $dbaccount->next_record();
		         ?>
		      <tr> 
		        <td width="10%"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_ACCOUNT_NAME') ?> :</td>
		        <td><?php $dbpm->p("order_payment_name"); ?> </td>
		      </tr>
		      <tr> 
		        <td><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_ACCOUNT_NUMBER') ?> :</td>
		        <td> <?php echo ps_checkout::asterisk_pad($dbaccount->f("account_number"),4);
		    ?> </td>
		      </tr>
		      <tr> 
		        <td><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_EXPIRE_DATE') ?> :</td>
		        <td><?php echo strftime("%m - %Y", $dbpm->f("order_payment_expire")); ?> </td>
		      </tr>
			  <?php } ?>
		  <?php } ?>		  
		  <tr>
		  	<td colspan="2">
		  	  <?php

			 	$db = new ps_DB();
				//Get echeque charge
				$db->query("SELECT payment_method_discount, payment_method_discount_is_percent FROM #__{vm}_payment_method WHERE payment_method_id = 27 OR payment_method_id = 26 ORDER BY payment_method_id");
				
				$db->next_record();
				$creditcard = $db->f('payment_method_discount') * -1;
				$creditcard = $db->f('payment_method_discount_is_percent') ? $creditcard.'%' : '$'.$creditcard;
				
				$db->next_record();
				$echeque = $db->f('payment_method_discount') * -1;
				$echeque = $db->f('payment_method_discount_is_percent') ? $echeque.'%' : '$'.$echeque;				
				$deposit_info =  str_replace('{echeque}', $echeque, str_replace('{creditcard}', $creditcard, $VM_LANG->_('VM_ACCOUNT_DEPOSIT_INFO')));
				
				echo $deposit_info ? '<div style="clear:both" class="info">'.$deposit_info.'</div>' : '';
			  
		  			/**
		  			 * Include the customers payment info
		  			 */
					include_once(CLASSPATH.'ps_payments.php');
					$ps_payments = new ps_payments();
					$ps_payments->getCustomerPayments($order_id);		  			
		  		?>	
		  	  <?php
			 // $dbt = new ps_DB();
			 // $dbt->query('SELECT concat(`introtext`,`fulltext`) as text FROM #__content WHERE id = 49');
			  echo $VM_LANG->_('VM_ACCOUNT_DEPOSIT_INFO') ? '<div style="clear:both" class="info">'.$VM_LANG->_('VM_ACCOUNT_DEPOSIT_INFO').'</div>' : '';
			  ?>  		
		  	</td>
		  </tr>
	      <!-- end payment information --> 
	      </table>
	
	<?php // }
	
	    /** Print out the customer note **/
	    if ( $db->f("customer_note") ) {
	    ?>
	    <table width="100%">
	      <tr>
	        <td colspan="2">&nbsp;</td>
	      </tr>
	      <tr class="sectiontableheader">
	        <th align="left" colspan="2"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_CUSTOMER_NOTE') ?></th>
	      </tr>
	      <tr>
	        <td colspan="2">
	         <?php echo nl2br($db->f("customer_note"))."<br />"; ?>
	       </td>
	      </tr>
	    </table>
	    <?php
	    }	 
}
else {
	echo '<h4>'._LOGIN_TEXT .'</h4><br/>';
	include(PAGEPATH.'checkout.login_form.php');
	echo '<br/><br/>';
}