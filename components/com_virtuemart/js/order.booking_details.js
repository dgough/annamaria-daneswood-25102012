
window.addEvent('domready', function() {
	
	/*
	* For updating the price : 
	*/	
	$('mod_price').addEvent('click', function(e) {
		
		new Event(e).stop();
		
		var agree = confirm("Are you sure you wish to modify the price for the new dates that you have entered?");
		
		if (agree) {
								
			var req = new Ajax('index2.php?option=com_virtuemart&page=order.booking_modify_ajax&no_html=1&only_page=1&action=ajax&task=price', {
				data: ($('modify_booking_form').toQueryString())+'&page=order.booking_modify_ajax&func=',
				method:'post',
				onComplete: function (responseText, responseXML) {
					
					var status = getText(responseXML.getElementsByTagName('status').item(0));
					var message = getText(responseXML.getElementsByTagName('message').item(0));
					var content = getText(responseXML.getElementsByTagName('content').item(0));
					
					if (!status){
						alert(message);
					}else{
						$('price').value = content;
					}
				}
			}).request();
			
		} //end if
		
	});
		
	function getText(node) {
		if(typeof(node.text) == 'undefined') {
			return node.textContent;
		} else {
			return node.text;
		}
	}
	
});