			
var vbCalendar = new Class({

	options: {
        reference: '',
        popup: 0,
        inlineRef: '',
        endDate: {year: 0, month: 0, day: 0},
        prop_id: '',
		preUpdate: true,
		language: 'en'
    },

	initialize: function(options){
		
		this.setOptions(options); 		
		if(!this.options.reference.match(/-$/))	this.options.reference = this.options.reference+'-';		
		this.arrival = null;
		this.departure = null;
		this.clicks = 0;
		var today = new Date();
		this.hpCalModel;
		
		//Initialize calendar
		if(!this.hpCalModel){
		
			//Create calendar
			this.hpCalModel = new DHTMLSuite.calendarModel();
			this.hpCalModel.setLanguageCode(this.options.language)
			
			//Set invalid dates - first is all dates before today	
			// this.hpCalModel.addInvalidDateRange(false,{year: today.getFullYear(),month: today.getMonth()+1,day: today.getDate()-1});
			
			
			// KJW 27 Feb 2012. Modified version of the above line as the calendar was allowing yesterday's date to be selectable
			this.hpCalModel.addInvalidDateRange(false,{year: today.getFullYear(),month: today.getMonth()+1,day: today.getDate()});
			
			//Set invalid end date
			if(this.options.endDate) this.hpCalModel.addInvalidDateRange(this.options.endDate, false);
			
			//Setup default calendar options
			var defaultCalOptions = {  
				id: this.options.reference+'_Cal',
				callbackFunctionOnDayClick: 'getDateFromCalendar',
				scrollInYearDropDownActive: false,
				dropdownStartYear: today.getFullYear(),
				dropdownEndYear: this.options.endDate.year ? this.options.endDate.year : 3,						
				combineNaviation: false,
				displayCloseButton: false
			}
			
			if(this.options.popup){
				defaultCalOptions.closeButtonPosition = 'top';
				defaultCalOptions.displayCloseButton = true;
			}
			
			//Combine options
			var calOptions = $merge(defaultCalOptions, options)
			
			//Create calendar
			this.hpCal = new DHTMLSuite.calendar(calOptions);
			
			//Set a reference to this for use in the getDateFromCalendar method
			this.hpCal.self = this;
			
			//Set the calendar reference
			this.hpCal.setCalendarModelReference(this.hpCalModel);	
			
			var date = this.getDate('dateFrom');
			if(date){
				//Set the initial date
				this.hpCalModel.initialDay = date.day;
				this.hpCalModel.initialMonth = date.month;
				this.hpCalModel.initialYear = date.year;
				this.hpCalModel.__setDisplayedDateToInitialData();
			}
		
			if(!this.options.popup){
				this.hpCal.setTargetReference(this.options.inlineRef);
				this.hpCal.display();
			}else{
				this.hpCal.display();
				this.hpCal.hide();
			}
		}
		
		//Set the initial dates
		if (this.options.preUpdate) {
			this.updateDate('dateFrom');
			this.updateDate('dateTo');
		} else {
			this.options.preUpdate = true;
		}
		
		//Set the on change events for arrive/depart
		if($(this.options.reference+'dateFrom-d')) $(this.options.reference+'dateFrom-d').addEvent('change',function(){return this.updateDate('dateFrom')}.bind(this));
		if($(this.options.reference+'dateFrom-d')) $(this.options.reference+'dateFrom-m').addEvent('change',function(){return this.updateDate('dateFrom')}.bind(this));		
		if($(this.options.reference+'dateFrom-d')) $(this.options.reference+'dateFrom-y').addEvent('change',function(){return this.updateDate('dateFrom')}.bind(this));		
		
		if($(this.options.reference+'dateTo-d')) $(this.options.reference+'dateTo-d').addEvent('change',function(){return this.updateDate('dateTo')}.bind(this));
		if($(this.options.reference+'dateTo-d')) $(this.options.reference+'dateTo-m').addEvent('change',function(){return this.updateDate('dateTo')}.bind(this));		
		if($(this.options.reference+'dateTo-d')) $(this.options.reference+'dateTo-y').addEvent('change',function(){return this.updateDate('dateTo')}.bind(this));		
		
		if(this.options.popup) $(this.options.reference+'dateFrom-icon').addEvent('click',function(e){this.showPopup(e)}.bind(this));		
		if(this.options.popup) $(this.options.reference+'dateTo-icon').addEvent('click',function(e){this.showPopup(e)}.bind(this));		
		if($(this.options.reference+'clearCal')) $(this.options.reference+'clearCal').addEvent('click',this.clearForm.bind(this));		
		if($(this.options.reference+'getPrice')) $(this.options.reference+'getPrice').addEvent('click',this.getPrice.bind(this));	
		
	},
	
	
	addInvalidDateRange: function(date1,date2){
		this.hpCalModel.addInvalidDateRange(date1,date2);	
	},
	
	updateCalendar: function(){
		this.hpCal.resetViewDisplayedMonth();
	},
	
	showPopup: function(e){
		
		var buttonObj = e.target ? e.target : e.srcElement;
		
		this.hpCal.setCalendarPositionByHTMLElement(buttonObj,0,buttonObj.offsetHeight+2);	// Position the calendar right below the form input
	
		//Set the target
		this.source = buttonObj.hasClass('dateFrom') ? 'dateFrom' : 'dateTo';
		
		//Detect which input has been clicked and set the initial date accordingly	
		var dateFrom = this.getDate('dateFrom');
		var dateTo = this.getDate('dateTo');
		var date = this.source == 'dateTo' && dateTo ? dateTo : dateFrom;
		
		if(date){
			//Set the initial date
			this.hpCalModel.initialDay = date.day;
			this.hpCalModel.initialMonth = date.month;
			this.hpCalModel.initialYear = date.year;
			this.hpCalModel.__setDisplayedDateToInitialData();
		}
		
		//Set the selected dates if previously set
		if(this.arrival){
			this.hpCalModel.FromDate = dateFrom.year+''+dateFrom.month+''+dateFrom.day;
		}
		if(this.departure){
			this.hpCalModel.ToDate = dateTo.year+''+dateTo.month+''+dateTo.day;
		}
	
		if(this.hpCal.isVisible()){
			this.hpCal.hide();
		}else{		
			this.hpCal.resetViewDisplayedMonth();	// This line resets the view back to the inital display, i.e. it displays the inital month and not the month it displayed the last time it was open.		
			this.hpCal.display();
			this.hpCal.__populateMonthView();		
		}		
	},
	
	getDateFromCalendar: function(inputArray){

		if(!this.options.popup){
			if(this.clicks == 0){
				this.clicks = 1;
				this.source = 'dateFrom';
			}else{
				this.clicks = 0;
				this.source = 'dateTo';
			}
		}
		
		if(this.source == 'dateFrom'){
			this.arrival = inputArray;
			this.departure = null;
			this.setDate(this.source,inputArray);	
			this.hpCalModel.ToDate = '';
		}
		else{
			//Check if arrival is set, if so, validate
			this.departure = inputArray;	
			this.setDate(this.source,inputArray);							
		}

		if(this.arrival && this.departure && !this.options.popup){
			this.getPrice();
		}
		
		if(this.options.popup){	
			this.hpCal.hide();
		}
		return true;	
	},
	
	
	/**
	 * Gets the date for a particular field
	 **/
	getDate: function(obj){
			
		if(!obj) return false;		
		if(!$(this.options.reference+obj+'-y') || !$(this.options.reference+obj+'-m') || !$(this.options.reference+obj+'-d')) return false;
		
		var year = $(this.options.reference+obj+'-y').value;
		var month = $(this.options.reference+obj+'-m').value;
		var day = $(this.options.reference+obj+'-d').value;
		
		if(day && month && year) return {day: day, month: month, year: year};
		else return false;
	},
	
	
	/**
	 * Sets the dates for a particulare field
	 **/
	setDate: function (obj, value, ignoreVal){

		// KJW: 28 Feb, added a quick hack to stop the booking date range popup on the customer account order infor page
		// if(!ignoreVal && !this.hpCalModel.isDateWithinValidRange(value)){
	
		var url = window.location.href;
		if(!ignoreVal && !this.hpCalModel.isDateWithinValidRange(value) && (url.indexOf('?page=account.order_details') == -1)){
			alert('Your date range appears to fall over another booking. Please amend your selection.');
			return false;
		}
		
		$(this.options.reference + obj+'-d').value = value.day;
		$(this.options.reference + obj+'-m').value = value.month;
		$(this.options.reference + obj+'-y').value = value.year;
		
		if(obj == 'dateFrom'){
			this.hpCalModel.FromDate = value.year+''+value.month+''+value.day;	
			this.arrival = value;
		}
		else{
			this.hpCalModel.ToDate = value.year+''+value.month+''+value.day;		
			this.departure = value;
		}

		this.hpCal.__populateMonthView();	
				
	},
	
	
	updateDate: function(obj){
				
		var date = this.getDate(obj);
		
		if(date.year && date.month && date.day){
			this.setDate(obj, date);	
			if(this.arrival && this.departure && !this.options.popup){
				this.getPrice();
			}
		}
	},
	
		
	validateForm: function(){
		
		var from = this.getDate('dateFrom');
		var to = this.getDate('dateTo');
		
		if(from){
			if(!validateDate(from)){
				return false;
			}
		}
		if(to){
			if(!validateDate(to)){
				return false;
			}	
		}
		if(from || to){		
			if(!validateDateRange(this.hpCalModel, from, to)){
				return false;
			}
		}
		return true;
	},
	
	//Ajax function to get the price
	getPrice: function(){
		
		this.arrival = this.getDate('dateFrom');
		this.departure = this.getDate('dateTo');
		
		var url = 'index.php?option=com_virtuemart&page=booking.get_price&only_page=1&no_html=1&people=1&property_id='+this.options.prop_id;
			url += (this.arrival ? '&dateFrom[y]='+this.arrival.year+'&dateFrom[m]='+this.arrival.month+'&dateFrom[d]='+this.arrival.day : '&dateFrom=');
			url += (this.departure ? '&dateTo[y]='+this.departure.year+'&dateTo[m]='+this.departure.month+'&dateTo[d]='+this.departure.day : '&dateTo=');
			
		try{
			var myAjax = new Ajax(url, {
				method: 'get',
				onSuccess: this.priceCallBack.bind(this),
				onFailure: function(xhr){
					alert('We appologise we were unable to retrieve the price for this property. \nPlease call us so that we may assist with your enquiry.\nError code:'+xhr.status)
				}
			}).request();
		}catch(e){
			alert('We appologise we were unable to retrieve the price for this property. \nPlease call us so that we may assist with your enquiry')
		}
	},
	
	priceCallBack: function(responseText, responseXML){
		
		var status = this.getText(responseXML.getElementsByTagName('status').item(0));
		
		if(status == 1){
			var prices = this.getText(responseXML.getElementsByTagName('prices').item(0));
			$('your_prices').setHTML(prices);
			
		}else{
			
			var message = this.getText(responseXML.getElementsByTagName('message').item(0));
			$('your_prices').setHTML('<strong>Error:</strong> '+message);
			alert(message);
		}
	},
	
	getText: function(node){	
		if(node){	
			if(typeof(node.text) == 'undefined'){
				return node.textContent;
			}else{
				return node.text;
			}	
		}else{
			return '';
		}
	},
	
	//Resets the form
	clearForm: function(){	
		this.hpCalModel.initialDay=-1;	
		var nullDate = {year: '', month: '', day: ''}
		this.setDate('dateTo',nullDate,1)
		this.setDate('dateFrom',nullDate,1)		
		this.arrival = null;
		this.departure = null;
		this.hpCalModel.FromDate = null;
		this.hpCalModel.ToDate = null;		
		//this.hpCal.resetViewDisplayedMonth();		
		this.hpCal.__populateMonthView();	
		if($('vb_price_wrap')) $('vb_price_wrap').setHTML('')
	},
	
	
	destroy: function(){
		
		
	
	}
	
})

vbCalendar.implement(new Options);


//This funciton has to be outside the main class as otherwise the calendar
//tries to call a method within itself if you use "this.getDateFromCalendar" as the call back method
function getDateFromCalendar(inputArray){
	
	var cal = inputArray.calendarRef;
	if(cal){
		return cal.self.getDateFromCalendar(inputArray);	
	}	
	return true
}