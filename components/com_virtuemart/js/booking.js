
//Have to keep this outside the class otherwize it gets reset when the ajax response is evaluates
var checkout = 0;

var vmBooking = new Class({
	
	initialize: function(status, extrasText){
		
		this.task = 'main_form';
		this.status = status;
		this.loadingScreen;	
		this.shop_msg_parent;
		this.booking_msg_slide;
		this.initMsg();
		checkout = 0;
		
		if ($('booking_wrap_extras')) {
		
		$('extras_text').setHTML(extrasText);
		
		if($('booking_wrap_extras')) {
			var booking_extras_slide = new Fx.Slide('booking_wrap_extras', {duration: 750});
			if(this.status)
				booking_extras_slide.hide();	
				if($('save_checkout')) $('save_checkout').setProperty('disabled','true');	
			}			
		}
		
		if($('save_form')){	
			$('save_form').addEvent('click', function() {
				this.task = 'main_form';
				checkout = 0;
			})
		}
		
		if($('save_extras')){
			$('save_extras').addEvent('click', function() {
				this.task = 'extras_form';
				checkout = 0;
			});
		}
		
		if($('save_checkout')){
			$('save_checkout').addEvent('click', function() {
				checkout = 1;
			})
		}	
			
		var form = $('booking_form');
		form.addEvent('submit', function(e) {
			
			if(this.task ==''){
				this.task = 'main_form';
			}
			
			//Show the loading indicator
			var element = (this.task == 'main_form' ? 'booking_wrap_form' : 'booking_wrap_extras');
			this.showLoadingScreen(element);
					
			/**
			 * Prevent the submit event
			 */
			new Event(e).stop();
			
			var url = 'index2.php?'+form.toQueryString();

			try{
				var vmAjax = new Ajax(url, {
					onSuccess: function(responseText, responseXML){
						this.updatePage(responseText, responseXML)
						
					}.bind(this),
					onFailure: function(){
						form.submit();
	
					}.bind(this)
				}).request('&no_html=1&only_page=1&action=ajax&task='+this.task);
			}catch(e){
				
				if(this.status == 1 && checkout == 1){
					if(this.status == 1 && checkout == 1){
						this.checkout()
						return true;
					}
				}else{
					form.submit()
				}
			}

		}.bind(this));
	},	
	
	
	setState: function(state){
		
		this.status = state;
		if(this.status == 1){
			$('save_checkout').setProperty('value','Continue');
			$('save_checkout').removeProperty('disabled');
		}else{
			$('save_checkout').setProperty('value','Save');
			$('save_checkout').setProperty('disabled','true');
			checkout = 0;
		}
	},
	
	
	showLoadingScreen: function(element){
				
		//Show loading screens		
		this.statusIndicator = new Element('div',{'class':'loading'});
		this.statusIndicator.setOpacity(0.8);		
		this.statusIndicator.injectInside('booking_wrapper');		
		this.loadingScreen = this.statusIndicator;
	},
	
	updatePage: function(responseText, responseXML){
		
		if(responseXML != null){
			
			//Set the global state variable
			this.setState(this.getText(responseXML.getElementsByTagName('status').item(0)));			
			var message = this.getText(responseXML.getElementsByTagName('message').item(0));
			var message_clean = this.getText(responseXML.getElementsByTagName('message_clean').item(0));
			var form = this.getText(responseXML.getElementsByTagName('form').item(0));
			this.evalScripts(form);
			var extras = this.getText(responseXML.getElementsByTagName('extras').item(0));		
			var total_top = this.getText(responseXML.getElementsByTagName('total_top').item(0));	
			var total_bottom = this.getText(responseXML.getElementsByTagName('total_bottom').item(0));	
			
			this.showMessage(message,message_clean);
			
			if(!(this.status == 1 && checkout == 1)){
				this.loadingScreen.remove();
			}
			
			if(this.status != 1){
				return;
			}
			
			$('booking_wrap_form').setHTML(form);
			if($('booking_wrap_extras')) $('booking_wrap_extras').setHTML(extras);
			$('booking_wrap_total_top').setHTML(total_top);
			$('booking_wrap_total_bottom').setHTML(total_bottom);
			
			
			if(this.status == 1) {				
				if (typeof(booking_extras_slide) != 'undefined') {
					booking_extras_slide.slideIn();
					$('extras_text').setHTML('');
				}
			}
		}else{
			$('booking_form').removeEvent('submit');
			$('booking_form').setProperty('action','index.php');
			$('booking_form').submit();
		}

		if(this.status == 1 && checkout == 1){
			this.checkout()
			return true;
		}
		
		//Check the 
	},
	
	checkout: function(){
		$('booking_form').setProperty('action','index.php?page=checkout.index&ssl_redirect=1&option=com_virtuemart');
		var page = $('booking_form').getElement('input[name=page]')
		if(page) page.setProperty('value','checkout.index')
		$('booking_form').submit();
	},
	
	getText: function(node){
		
		if(typeof(node.text) == 'undefined'){
			return node.textContent;
		}else{
			return node.text;
		}
	},
	
	initMsg: function(){
				
		var shop_info = $$('div.shop_info');
		var shop_err = $$('div.shop_error');
		var shop_warning = $$('div.shop_warning');		
		
		if(shop_info.length) this.shop_msg_parent = shop_info.getParent();
		if(shop_err.length) this.shop_msg_parent = shop_err.getParent();
		if(shop_warning.length) this.shop_msg_parent = shop_warning.getParent();
		
		if(!this.shop_msg_parent){
			this.shop_msg_parent = new Element('div',{'id':'this.shop_msg_parent'});
			this.shop_msg_parent.injectTop('vmMainPage');
		}else{
			this.shop_msg_parent.setProperty('id','this.shop_msg_parent');
		}
		
		this.booking_msg_slide = new Fx.Slide('this.shop_msg_parent', {duration: 750});
	},
	
	showMessage: function(message, message_clean){
		
		if(message != ''){
			if(this.status == 0) alert(message_clean)
			this.shop_msg_parent.setHTML(message);
			this.booking_msg_slide.slideIn();
		}else{
			this.booking_msg_slide.slideOut();
			this.shop_msg_parent.setHTML(message);
		}
	},
	
	evalScripts: function(src){
		var script, scripts = '', regexp = /<script[^>]*>([\s\S]*?)<\/script>/gi;
		while ((script = regexp.exec(src))) scripts += script[1] + '\n';		
		if (scripts) (window.execScript) ? window.execScript(scripts) : window.setTimeout(scripts, 0);
	}
});