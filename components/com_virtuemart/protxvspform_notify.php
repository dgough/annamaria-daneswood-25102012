<?php
// *************************************************************************
// *                                                                       *
// * Ecom Solution Protx VSP Form Payments Module for Virtuemart           *
// * Copyright (c) 2007 Ecom Solution LTD. All Rights Reserved             *
// * Release Date: 09.08.2007                                              *
// * Version 1.1                                                           *
// *                                                                       *
// *************************************************************************
// *                                                                       *
// * Email: support@ecommerce-host.co.uk                                   *
// * Website: http://www.e-commerce-solution.co.uk                         *
// *                                                                       *
// *************************************************************************
// *                                                                       *
// * This software is furnished under a license and may be used and copied *
// * only  in  accordance  with  the  terms  of such  license and with the *
// * inclusion of the above copyright notice.  This software  or any other *
// * copies thereof may not be provided or otherwise made available to any *
// * other person.  No title to and  ownership of the  software is  hereby *
// * transferred.                                                          *
// *                                                                       *
// * You may not reverse  engineer, decompile, defeat  license  encryption *
// * mechanisms, or  disassemble this software product or software product *
// * license.  Ecom Solution  may  terminate  this license  if you fail to *
// * comply with any of the terms and conditions set forth in our end user *
// * license agreement (EULA).  In such event,  licensee  agrees to return *
// * licensor  or destroy  all copies of software  upon termination of the *
// * license.                                                              *
// *                                                                       *
// *                                                                       *
// *************************************************************************


    global $mosConfig_absolute_path, $mosConfig_live_site, $mosConfig_lang, $database,
    $mosConfig_mailfrom, $mosConfig_fromname;
	
        /*** access Joomla's configuration file ***/
        $my_path = dirname(__FILE__);
        
        if( file_exists($my_path."/../../../configuration.php")) {
            $absolute_path = dirname( $my_path."/../../../configuration.php" );
            require_once($my_path."/../../../configuration.php");
        }
        elseif( file_exists($my_path."/../../configuration.php")){
            $absolute_path = dirname( $my_path."/../../configuration.php" );
            require_once($my_path."/../../configuration.php");
        }
        elseif( file_exists($my_path."/configuration.php")){
            $absolute_path = dirname( $my_path."/configuration.php" );
            require_once( $my_path."/configuration.php" );
        }
        else {
            die( "Joomla Configuration File not found!" );
        } 
        $absolute_path = realpath( $absolute_path );
      
	   
        // Set up the appropriate CMS framework
        if( class_exists( 'jconfig' ) ) {
			define( '_VALID_MOS', 1 );
			define( 'JPATH_BASE', $absolute_path );
			define( 'DS', DIRECTORY_SEPARATOR );
			
			// Load the framework
			require_once ( JPATH_BASE . DS . 'includes' . DS . 'defines.php' );
			require_once ( JPATH_BASE . DS . 'includes' . DS . 'framework.php' );

			// create the mainframe object
			$mainframe = & JFactory::getApplication( 'site' );
			
			// Initialize the framework
			$mainframe->initialise();
			
			// load system plugin group
			JPluginHelper::importPlugin( 'system' );
			
			// trigger the onBeforeStart events
			$mainframe->triggerEvent( 'onBeforeStart' );


			// Adjust the live site path
			//$mosConfig_live_site = str_replace('/administrator/components/com_virtuemart', '', $mosConfig_live_site); 
			//$mosConfig_absolute_path = $absolute_path;			
			
  $mosConfig_absolute_path = JPATH_SITE;   
  $mosConfig_live_site = JURI::base( true ); // /administrator
  
        } else {
        	define('_VALID_MOS', '1');
        	require_once($mosConfig_absolute_path. '/includes/joomla.php');
        	require_once($mosConfig_absolute_path. '/includes/database.php');
        	$database = new database( $mosConfig_host, $mosConfig_user, $mosConfig_password, $mosConfig_db, $mosConfig_dbprefix );
        }

        // load Joomla Language File
		/*
        if (file_exists( $mosConfig_absolute_path. '/language/'.$mosConfig_lang.'.php' )) {
            require_once( $mosConfig_absolute_path. '/language/'.$mosConfig_lang.'.php' );
        }
        elseif (file_exists( $mosConfig_absolute_path. '/language/english.php' )) {
            require_once( $mosConfig_absolute_path. '/language/english.php' );
        }
		/*
    /*** END of Joomla config ***/
    
    
    /*** VirtueMart part ***/        
        require_once($mosConfig_absolute_path.'/administrator/components/com_virtuemart/virtuemart.cfg.php');
        include_once( ADMINPATH.'/compat.joomla1.5.php' );
        require_once( ADMINPATH. 'global.php' );
        require_once( CLASSPATH. 'ps_main.php' );
        
        /* @MWM1: Logging enhancements (file logging & composite logger). */
        $vmLogIdentifier = "notify.php";
        require_once(CLASSPATH."Log/LogInit.php");
              
        /* Load the PayPal Configuration File 
        require_once( CLASSPATH. 'payment/ps_paypal.cfg.php' );
        
		if( PAYPAL_DEBUG == "1" ) {
			$debug_email_address = $mosConfig_mailfrom;
		}
		else {
			$debug_email_address = PAYPAL_EMAIL;
		}
		*/ 
		
	    // restart session
	    // Constructor initializes the session!
	    $sess = new ps_session();                        
	    
    /*** END VirtueMart part ***/

    // include generic_fns
    require_once($mosConfig_absolute_path . '/administrator/components/com_protxvspform/generic_fns.php');
  	
    // include config 
    require_once( CLASSPATH. 'payment/ps_protxvspform.cfg.php' );
    require_once( CLASSPATH. 'payment/ps_protxvspform.php' );

	// show all errors
	//error_reporting(E_ALL);
    $err_level = error_reporting(E_ALL);
	

DEFINE('PXFM_DEBUG','0');
if (PXFM_DEBUG==1) {

  ?> 
  <html>
  <head>
  <title>Protx VSP Forms Payments Module for Virtuemart</title>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <style type="text/css">
  <!--
  body,p,td,th {  
    font-family: Verdana, Arial, Helvetica, sans-serif; 
    font-size: 11px
  }
  -->
  </style>
  </head>
  <body>

  <h3>Protx VSP Forms Payments Module for Virtuemart </h3>

  &copy; 2007 Ecom Solutions Ltd. All rights reserved. 
  <a href="http://www.e-commerce-solution.co.uk/">www.e-commerce-solution.co.uk</a> 
  <br>Support: 
  <a href="mailto:support@e-commerce-solution.co.uk">support@e-commerce-solution.co.uk</a> 	
  <p><b>Warning</b>: This website requires authenticated access. 
  <br>Unauthorised access to this website is a criminal offence under 
  <a href="http://www.hmso.gov.uk/acts/acts1990/Ukpga_19900018_en_1.htm" 
  target="_blank">The Computer Misuse Act 1990</a>. 
  <br>Access to this website is logged and monitored. 
  <p>

  <?php 

} // (PXFM_DEBUG==1) 


$your_ip = $_SERVER['REMOTE_ADDR']; 
$hostname = gethostbyaddr($your_ip);

// check ip
DEFINE('PXFM_ALLOWEDIP','');
if (PXFM_ALLOWEDIP != '') {
  $AllowedIPs=split(',',  PXFM_ALLOWEDIP);
  if (CheckAccess($AllowedIPs) == 0) {
    echo "<p>Access is restricted by IP address. Your IP address is: ";
    echo $your_ip;
    echo " <b>Access Unauthorised:</b> " . $hostname;
    exit; 
  } else { 
    //echo " <b>Access Authorised:</b> " . $hostname;
  }
}
//echo "<p>";

// any data sent? 
$crypt = Trim($_REQUEST['crypt']);
$crypt = str_replace (" ", "+", $crypt);

if ($crypt != '') {    
	
    $Decoded=SimpleXor(base64_decode($crypt),PXFM_PASS);

    if (PXFM_DEBUG==1) 
      echo($Decoded . "<p>");

    // ** Split out the useful information into variables we can use **
    $values = getToken($Decoded);

    // Turn Option_explicit on
    //$err_handler = set_error_handler("option_explicit");

    $VendorTxCode = getArrayValue($values,'VendorTxCode');
    $Status = getArrayValue($values,'Status');
    $StatusDetail = getArrayValue($values,'StatusDetail');
    $VPSTxID = getArrayValue($values,'VPSTxId');
    $AVSCV2 = getArrayValue($values,'AVSCV2');
    $Amount = getArrayValue($values,'Amount');
    // protocol 2.22 fields
    $AddressResult = getArrayValue($values, 'AddressResult' );
    $PostCodeResult = getArrayValue($values, 'PostCodeResult' );
    $CV2Result = getArrayValue($values, 'CV2Result' );
    $GiftAid = getArrayValue($values, 'GiftAid' );
    $VBVSecureStatus = getArrayValue($values, '3DSecureStatus' );
    $CAVV = getArrayValue($values, 'CAVV' );

    $order_payment_log_add = "\n<b>Payment Result</b>: ". 
			"\n<br>Status: ". $Status. 
			"\n<br>StatusDetail: ". $StatusDetail. 
			"\n<br>VPSTxID: ". $VPSTxID. 
			"\n<br>AVSCV2: ". $AVSCV2. 
			"\n<br>AddressResult: ". $AddressResult. 
			"\n<br>PostCodeResult: ". $PostCodeResult. 
			"\n<br>CV2Result: ". $CV2Result. 
			"\n<br>3DSecureStatus: ". $VBVSecureStatus; 

    $payment_status  = trim($Status); 
    $order_number = trim($VendorTxCode); 
    $total = trim($Amount);

    if (PXFM_DEBUG==1) {
        echo "Protx Order number: " . $order_number . "<br>"; 
        echo "Protx Status: " . $payment_status . "<br>";
        echo "Protx Detail: " . $StatusDetail . "<br>";
        echo "Protx Total: " . $total . "<br>";
    }
    
    // check the order  
    if ( trim($order_number) != '') {
	  
    // check the order in db
    $q = "SELECT order_id, order_total FROM #__{vm}_orders "; 
    $q .= " WHERE order_number LIKE '" . $order_number . "' ";  
    $q .= " OR order_number LIKE '" . substr($order_number, 0, 32) . "'"; 
    $db = new ps_DB;
    $db->query($q);
    $db->next_record();

    if (PXFM_DEBUG==1) 
      echo("<br>" . $q . "<br>");

    $order_id = $db->f("order_id");
    $order_total = $db->f("order_total");

    if (PXFM_DEBUG==1) {
        echo "Order Id: " . $order_id . "<br>"; 
        echo "Order Total: " . $total . "<br>";
    }

    if ($order_total == '')
      exit('<b>Error: Order not recognized</b>');          
    if ($order_total != $total)
      exit('<b>Error: Wrong amount</b>');  

    $d['order_id'] = $order_id;    // this identifies the order record
    $d['order_comment'] = $Status; 
	
    //AUTHENTICATED and REGISTERED statuses are only returned if the TxType is AUTHENTICATE.
    if(( strtoupper($payment_status) == 'OK' ) || ( strtoupper($payment_status) == 'REGISTERED' ) || ( strtoupper($payment_status) == 'AUTHENTICATED' )) {
        $d['order_status'] = 'C'; 
        // $d['notify_customer'] = "Y";
    }
    else {
        $d['order_status'] = 'X';  
    }
    //else if( strtoupper($payment_status == 'DECLINED') ){ 

    // update order_payment_log      
    $q = "UPDATE #__{vm}_order_payment SET ";
    $q .= "\n order_payment_log='" . $order_payment_log_add . "',  "; 
    $q .= "\n order_payment_trans_id='" . $VPSTxID . "'  "; 
    $q .= "\n WHERE order_id='".$order_id."'";
    $db->query($q);               

    // set back error-handling 
    error_reporting($err_level); 

    require_once ( CLASSPATH . 'ps_order.php' );
    $ps_order= new ps_order;
    $ps_order->order_status_update($d);

    //exit('<b>Order updated</b>');  

// redirect to the order detail page
?>
<script language="JavaScript">

 var u ='/index.php?option=com_virtuemart&page=checkout.result_notify'
+'&order_id=<?php echo $order_id; ?>'
+'&order_number=<?php echo $order_number; ?>'
+'&desc=<?php echo $StatusDetail; ?>';

window.location.href=u;
//document.write(u);

</script>
<?php
 
	} else {
      exit('<b>Error: No Order</b>');   	
	} // order_number
	
} else {  
    exit('<b>Error: No crypt data received</b>');  
}

?>
</body></html>
