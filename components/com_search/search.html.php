<?php
/**
* @version $Id: search.html.php 4953 2006-09-06 13:07:03Z predator $
* @package Joomla
* @subpackage Search
* @copyright Copyright (C) 2005 Open Source Matters. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );

/**
* @package Joomla
* @subpackage Search
*/
class search_html {

	function openhtml( $params ) {
		if ( $params->get( 'page_title' ) ) {
			// ------------------------ A8E fix ------------------------
			?>
			<h1 class="componentheading<?php echo $params->get( 'pageclass_sfx' ); ?>"><?php echo $params->get( 'header' ); ?></h1>
			<?php
		}
	}

	function searchbox( $searchword, &$lists, $params ) {
		global $Itemid;
		// ------------------------ A8E fixes start ------------------------
		?>
		<form action="index.php" method="get">
		<input type="hidden" name="option" value="com_search" />
		<input type="hidden" name="Itemid" value="<?php echo $Itemid; ?>" />
		<div class="contentpaneopen<?php echo $params->get( 'pageclass_sfx' ); ?>">
			<div class="searchblock">
					<label for="search_searchword">
						<?php echo _PROMPT_KEYWORD; ?>:
					</label>

					<input type="text" name="searchword" id="search_searchword" size="30" maxlength="20" value="<?php echo stripslashes($searchword);?>" class="inputbox" />

					<input type="submit" name="submit" value="<?php echo _SEARCH_TITLE;?>" class="button" />
			</div>
				<div class="searchparams">
					<?php echo $lists['searchphrase']; ?>
				</div>
				<div class="searchordering">
					<label for="search_ordering">
						<?php echo _CMN_ORDERING;?>:
					</label>
					<?php echo $lists['ordering'];?>
				</div>
		</div>
		</form>
		<?php
		// ------------------------ A8E fixes end ------------------------
	}

	function searchintro( $searchword, $params ) {
		// ------------------------ A8E fixes start ------------------------
		?>
		<div class="searchintro<?php echo $params->get( 'pageclass_sfx' ); ?>">
			<?php echo _PROMPT_KEYWORD . ' <b class="searchword">' . stripslashes($searchword) . '</b>'; ?>
		</div>
		<?php
		// ------------------------ A8E fixes end ------------------------
	}

	function message( $message, $params ) {
		// ------------------------ A8E fixes start ------------------------
		?>
		<div class="searchintro<?php echo $params->get( 'pageclass_sfx' ); ?>">
			<?php eval ('echo "'.$message.'";'); ?>
		</div>
		<?php
		// ------------------------ A8E fixes end ------------------------
	}

	function displaynoresult() {
		// ------------------------ A8E fix ------------------------
	}

	function display( &$rows, $params, $pageNav, $limitstart, $limit, $total, $totalRows, $searchword ) {
		global $mosConfig_hideCreateDate;
		global $mosConfig_live_site, $option, $Itemid;

		$c 			= count ($rows);
		$image 		= mosAdminMenus::ImageCheck( 'google.png', '/images/M_images/', NULL, NULL, 'Google', 'Google', 1 );
		$searchword = urldecode( $searchword );
		$searchword	= htmlspecialchars($searchword, ENT_QUOTES);
		// ------------------------ A8E fixes start ------------------------
				// number of matches found
				echo '<div class="searchconclusion">';
				eval ('echo "'._CONCLUSION.'";');

				?>
				<a href="http://www.google.com/search?q=<?php echo $searchword; ?>" target="_blank">
					<?php echo $image; ?></a>
				</div>
			<div class="resultcounter">		
			<?php
			echo $pageNav->writePagesCounter();

			$ordering 		= strtolower( strval( mosGetParam( $_REQUEST, 'ordering', 'newest' ) ) );
			$searchphrase 	= strtolower( strval( mosGetParam( $_REQUEST, 'searchphrase', 'any' ) ) );
			
			$searchphrase	= htmlspecialchars($searchphrase);

			$link = $mosConfig_live_site ."/index.php?option=$option&Itemid=$Itemid&searchword=$searchword&searchphrase=$searchphrase&ordering=$ordering";
			echo $pageNav->getLimitBox( ampReplace( $link ) );
			?>
		</div>
		<div class="contentpaneopen<?php echo $params->get( 'pageclass_sfx' ); ?>">
		<div class="<?php echo $params->get( 'pageclass_sfx' ); ?>">
				<?php
				$z		= $limitstart + 1;
				$end 	= $limit + $z;
				if ( $end > $total ) {
					$end = $total + 1;
				}
				for( $i=$z; $i < $end; $i++ ) {
					$row = $rows[$i-1];
					if ($row->created) {
						$created = mosFormatDate ($row->created, _DATE_FORMAT_LC);
					} else {
						$created = '';
					}
					?>
					<div class="itemblock">
						<h2>
							<span class="small<?php echo $params->get( 'pageclass_sfx' ); ?> searchresultcount">
								<?php echo $i.'. ';?>
							</span>
							<?php
							if ( $row->href ) {
								$row->href = ampReplace( $row->href );
								if ($row->browsernav == 1 ) {
									?>
									<a href="<?php echo sefRelToAbs($row->href); ?>" target="_blank">
									<?php
								} else {
									?>
									<a href="<?php echo sefRelToAbs($row->href); ?>">
									<?php
								}
							}

							echo $row->title;

							if ( $row->href ) {
								?>
								</a>
								<?php
							}
							?>
						</h2>
							<?php
							if ( $row->section ) {
								?>
								<span class="small<?php echo $params->get( 'pageclass_sfx' ); ?> section">
									(<?php echo $row->section; ?>)
								</span>
								<?php
							}
							?>
						
							<?php echo ampReplace( $row->text );?>

						<?php
						if ( !$mosConfig_hideCreateDate ) {
							?>
							<div class="small<?php echo $params->get( 'pageclass_sfx' ); ?> date">
								<?php echo $created; ?>
							</div>
							<?php
						}
						?>
					</div>
					<?php
				}
				?>
			</div>
		</div>	
		<?php
		// ------------------------ A8E fixes end ------------------------
	}

	function conclusion( $searchword, $pageNav ) {
		global $mosConfig_live_site, $option, $Itemid;
		
		$ordering 		= strtolower( strval( mosGetParam( $_REQUEST, 'ordering', 'newest' ) ) );
		$searchphrase 	= strtolower( strval( mosGetParam( $_REQUEST, 'searchphrase', 'any' ) ) );	
		
		$searchphrase	= htmlspecialchars($searchphrase);
		
		$link 			= $mosConfig_live_site ."/index.php?option=$option&Itemid=$Itemid&searchword=$searchword&searchphrase=$searchphrase&ordering=$ordering";
		// ------------------------ A8E fixes start ------------------------
		?>
				<div class="pagenav">
					<?php echo $pageNav->writePagesLinks( ampReplace( $link ) ); ?>
				</div>
		<?php
		// ------------------------ A8E fixes end ------------------------
	}
}
?>