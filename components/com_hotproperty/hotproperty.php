<?php
// hotproperty.php
/**
* Hot Property
*
* @package Hot Property 0.9
* @copyright (C) 2004-2006 Lee Cher Yeong
* @url http://www.Mosets.com/
* @author Lee Cher Yeong <hotproperty@mosets.com>
**/

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

include_once ( $mosConfig_absolute_path.'/administrator/components/com_hotproperty/function.hotproperty.php' );

# Front-end classes
include_once( $mosConfig_absolute_path.'/components/com_hotproperty/hotproperty.class.php' );

# Backend classes
include_once ( $mosConfig_absolute_path.'/administrator/components/com_hotproperty/admin.hotproperty.class.php' );

# Edit Property
include_once( $mosConfig_absolute_path.'/components/com_hotproperty/property.php' );
include_once( $mosConfig_absolute_path.'/components/com_hotproperty/property.html.php' );
include_once( $mosConfig_absolute_path.'/administrator/components/com_hotproperty/classes/oohforms.inc' );

# Edit Agent
include_once( $mosConfig_absolute_path.'/components/com_hotproperty/agent.php' );
include_once( $mosConfig_absolute_path.'/components/com_hotproperty/agent.html.php' );

require( $mosConfig_absolute_path.'/administrator/components/com_hotproperty/config.hotproperty.php' );
include_once( $mosConfig_absolute_path.'/components/com_hotproperty/functions.hotproperty.php' );
include_once( $mosConfig_absolute_path.'/includes/pageNavigation.php' );
require_once( $mainframe->getPath( 'front_html' ) );

# Include the language file. Default is English
if ($hp_language=='') {
	$hp_language='english';
} else {
	$hp_language=$mosConfig_lang;
}

include_once('components/com_hotproperty/language/'.$hp_language.'.php');

if ($task <> "rss" && !mosGetParam($_REQUEST,'no_html',0)) {
	# include Hot Property's CSS file
	# To allows greater control over the presentation layer, through CSS.
	hotproperty_HTML::include_CSS();

	# Global Container - Start
	hotproperty_HTML::include_Container_Start();
}

# Include a special container for Image management - Used when editing property in front-end
if (substr($task,-5) == "image")  {
	HP_Property_HTML::listprop_starthtml_image();
}

#
$id = intval( mosGetParam( $_REQUEST, 'id', 0 ) );

# Caching
$cache =& mosCache::getCache( 'com_hotproperty' );

switch( $task ) {
	case 'viewtype':
		$limit = trim( mosGetParam( $_REQUEST, 'limit', $hp_default_limit ) );
		$limitstart = trim( mosGetParam( $_REQUEST, 'limitstart', 0 ) );

		$sortby_sort = trim( mosGetParam( $_REQUEST, 'sort', $hp_default_order ) );
		$sortby_order = trim( mosGetParam( $_REQUEST, 'order', $hp_default_order2 ) );

		$cache->call( 'showType', $id, $limit, $limitstart, $sortby_sort, $sortby_order );
		break;
	case 'viewco':
		$limit = trim( mosGetParam( $_REQUEST, 'limit', $hp_default_limit_co ) );
		$limitstart = trim( mosGetParam( $_REQUEST, 'limitstart', 0 ) );
		$cache->call( 'showCompany', $id, $limit, $limitstart );
		break;
	case 'viewagent':
		$limit = trim( mosGetParam( $_REQUEST, 'limit', $hp_default_limit_agent ) );
		$limitstart = trim( mosGetParam( $_REQUEST, 'limitstart', 0 ) );
		$cache->call( 'showAgent', $id, $limit, $limitstart );
		break;
	case 'viewfeatured':
		$limit = trim( mosGetParam( $_REQUEST, 'limit', $hp_default_limit_featured) );
		$limitstart = trim( mosGetParam( $_REQUEST, 'limitstart', 0 ) );
		$cache->call( 'showFeatured', $limit, $limitstart );
		break;
	case "viewagentemail":
		showAgentEmail();
		break;
	case "viewcoemail":
		showCoEmail();
		break;
	case 'view':
		$cache->call('showProp', $id );		
		break;
	case 'viewwishlist':		
		viewWishList();
		break;
	case 'viewspecials':
		viewSpecials();
		break;
	case 'sendenquiry':
		sendEnquiry();
		break;
	case 'search':
		search();
		break;
	case 'asearch':
		asearch();
		break;
	case 'advsearch':
		searchAdv();
		break;
	case 'resetsearch':
		resetSearch();
		break;
	case 'emailform':
		emailForm();
		break;
	case "emailsend":
		emailSend();
		break;
	case "showpdf":
		showpdf();
		break;
		
		
	/* Edit Agent */
	case 'editagent':
		editagent();
		break;
	case 'saveagent':
		mosCache::cleanCache( 'com_hotproperty' );
		saveagent($option);
		break;
	case 'cancelagent':
		cancelagent($option);
		break;

	/* Edit Property */
	case 'editprop':
		editproperty($option);
		break;
	case 'addprop':
		editproperty($option);
		break;
	case 'saveprop':
		mosCache::cleanCache( 'com_hotproperty' );
		saveproperty($option);
		break;
	case "saveproperty_image_new":
		# Save and Edit again - For new record
		mosCache::cleanCache( 'com_hotproperty' );
		saveproperty($option, 'newimage');
		break;
	case "saveproperty_updateEF":
		# au changment de type, on rafraichit les EFs
		mosCache::cleanCache( 'com_hotproperty' );
		saveproperty($option, 'updateEF');
		break;
	case 'cancelprop':
		cancelproperty($option);
		break;
	case 'manageprop':
		manageprop();
		break;

	/* Image Management */
	case "listprop_image":
		listprop_showupload_image();
		break;
	case "listprop_upload_image":
		mosCache::cleanCache( 'com_hotproperty' );
		listprop_upload_image();
		break;
	case "listprop_edit_image":
		listprop_edit_image();
		break;
	case "listprop_delete_image":
		mosCache::cleanCache( 'com_hotproperty' );
		listprop_delete_image();
		break;
	case "orderup_images":
		mosCache::cleanCache( 'com_hotproperty' );
		listprop_order_image(-1, $option);
		break;
	case "orderdown_images":
		mosCache::cleanCache( 'com_hotproperty' );
		listprop_order_image(1, $option);
		break;
	case "listprop_edit_images":
		listprop_edit_images($option);
		break;

	/* RSS Feeds */
	case "rss":
		include_once ( $mosConfig_absolute_path.'/components/com_hotproperty/rss.hotproperty.php' );
		break;

	# Show Main Listing by default
	
	# Organic Mods - pulled from bluechip
	
	case "newproperties":
		newproperties();
	break;
	
	case "listallproperties":
		listAllProperties();
	break;
	
	case 'wishlist':
		processWishlist($id);
	break;
	
	case "":
		showFrontpage();
		break;
	
	# Display Mambo default error msg when $task provided does not exists
	default:
		echo _NOT_EXIST;
		break;
}

# Image Management Container - End
if (substr($task,-5) == "image") {
	listprop_image($option);
	HP_Property_HTML::listprop_endhtml_image();
}

if ($task <> "rss" && !mosGetParam($_REQUEST,'no_html',0)) {
	# Global Container - End
	hotproperty_HTML::include_Container_End();
}

function showFrontpage() {
	global $database, $hp_fp_featured_count, $cache;

	# Get featured listings' Data
	if ( !is_numeric($hp_fp_featured_count) ) $hp_fp_featured_count = 0;
	HP_getProperty('featured', null,$featured, $featured_fields_caption, 0, $hp_fp_featured_count);
	$featured_total = hotproperty::getNumProp('p.featured=1');

	# Select published types
	$database->setQuery( "SELECT *, parent as '0' FROM #__hp_prop_types AS t"
		. "\nWHERE t.published='1'"
		. "\nORDER BY t.ordering ASC");
	$types = $database->loadObjectList();
	
	# Get the top 3 HOT property under every published types. It will be more attractive to the user
	$types_hotproperty = array();
	
	foreach($types AS $t) {
		$database->setQuery( "SELECT p.name, p.id FROM #__hp_properties AS p"
			.	"\nLEFT JOIN #__hp_prop_types AS t ON t.id=p.type"
			. "\nWHERE p.published='1' AND p.approved='1' AND t.published='1' AND type=".$t->id
			. "\n	AND (p.publish_up = '0000-00-00 00:00:00' OR publish_up <= NOW())"
			. "\n	AND (p.publish_down = '0000-00-00 00:00:00' OR publish_down >= NOW())"
			. "\nORDER BY p.hits DESC"
			. "\nLIMIT 0,3");

		$types_hotproperty[$t->id] = $database->loadObjectList();
		echo $database->getErrorMsg();
	}
	$types_total = count($types);
	$cache->call( 'hotproperty_HTML::show_Frontpage', $featured, $featured_fields_caption, $featured_total, $types, $types_hotproperty, $types_total );

}

function showType( $id, $limit, $limitstart, $sortby_sort, $sortby_order ) {
	global $database, $Itemid, $hp_default_limit;

	# Validate input
	if ( $sortby_order <> "asc" ) $sortby_order = "desc";
	if ( $sortby_sort <> "name" && $sortby_sort <> "agent" && $sortby_sort <> "price" && $sortby_sort <> "suburb" && $sortby_sort <> "state" && $sortby_sort <> "country" && $sortby_sort <> "type" && $sortby_sort <> "modified" && $sortby_sort <> "hits") $sortby_sort = "name";

	# Redirect to main listing if invalid ID
	if (!HP_isTypePublished($id)) mosRedirect(sefRelToAbs('index.php?option=com_hotproperty&Itemid='.$Itemid)) ;

	# Pagination
	$total = HP_getNumProp_GroupType($id);
	$pageNav = new mosPageNav( $total, $limitstart, $limit );

	# Get type
	$database->setQuery("SELECT name, id FROM #__hp_prop_types WHERE id='".$id."' LIMIT 1");
	$database->loadObject($type);

	# Get properties
	$where[] = "type='".$id."'";
	HP_getProperty('listing', $where, $prop, $caption, $limitstart, $limit, $sortby_sort, $sortby_order);

	hotproperty_HTML::show_Type($prop, $type, $caption, $pageNav, $sortby_sort, $sortby_order);
}

function showProp( $id ) {
	global $database, $hp_show_agentdetails, $hp_use_companyagent;

	if (!HP_isPropPublished($id)) mosRedirect(sefRelToAbs('index.php?option=com_hotproperty&amp;Itemid=$Itemid'));

	$where[] = "p.id='".$id."'";
	HP_getProperty('all', $where, $prop, $caption);

	# Get All thumbnails
	$database->setQuery("SELECT * FROM #__hp_photos"
		.	"\nWHERE property='".$id."'"
		.	"\nORDER BY ordering ASC");
	$images = $database->loadObjectList();

	# Get Agent's info
	if ($hp_show_agentdetails && $hp_use_companyagent) {
		$sql = "SELECT a.*, COUNT(p.id) AS properties, c.id AS companyid, c.name AS company FROM (#__hp_agents AS a, #__hp_prop_types AS t) "
		.	"\nLEFT JOIN #__hp_companies AS c ON c.id=a.company"
		.	"\nLEFT JOIN #__hp_properties AS p ON p.agent=a.id"
		.	"\nWHERE a.id='".$prop[0]->agentid."'"
		.	"\n AND t.id=p.type"
		. "\n AND p.published='1' AND p.approved='1' AND t.published='1'"
		. "\n	AND (publish_up = '0000-00-00 00:00:00' OR publish_up <= NOW())"
		. "\n	AND (publish_down = '0000-00-00 00:00:00' OR publish_down >= NOW())"
		. "\nGROUP BY p.agent"
		.	"\nLIMIT 1";
		$database->setQuery($sql);
		$agent = $database->loadObjectList();
	}

	# Record Hit
	$database->setQuery("UPDATE #__hp_properties SET hits = hits + 1 WHERE id ='".$id."' LIMIT 1");
	$database->query();

	hotproperty_HTML::show_Prop($prop, $caption, $images, $agent);

}

function viewWishList() {
	global $database, $my, $Itemid, $mainframe;
	
	$database->setQuery("SELECT prop_id FROM #__hp_wishlist WHERE user_id='$my->id'");
	$ids = $database->loadResultArray();
	
	$mainframe->setPageTitle('Your Property Short List');
	
	if ($ids) {
		$where[] = "p.id IN (".implode(',',$ids).")";
		HP_getProperty('listing', $where, $prop, $caption);
	}
	
	if ($prop) {
		hotproperty_HTML::custom_list_properties($prop, $caption);
	} else {
		hotproperty_HTML::throwError('You don\'t have any properties in your Short List');
	}
}

function viewSpecials() {
	global $database, $my, $Itemid, $mainframe;
	
	$sql = 	"SELECT p.id from #__hp_properties p ".
			"LEFT JOIN #__vm_bk_seasons s on s.property_id=p.id " .
			"WHERE s.discount!='' ORDER BY p.id DESC";
	
	$database->setQuery($sql);
	$ids = $database->loadResultArray();
	
	$mainframe->setPageTitle('Property Special Offers');
	
	if ($ids) {
		$where[] = "p.id IN (".implode(',',$ids).")";
		HP_getProperty('listing', $where, $prop, $caption);
	}
	
	if ($prop) {
		hotproperty_HTML::custom_list_properties($prop, $caption);
	} else {
		hotproperty_HTML::throwError('There are no properties on special offer');
	}
}

function recommendProp( $id ) {
	global $database, $hp_show_agentdetails, $hp_use_companyagent;

	if (!HP_isPropPublished($id)) mosRedirect(sefRelToAbs('index.php?option=com_hotproperty&amp;Itemid=$Itemid'));

	$database->setQuery("SELECT distinct property_id from #__hp_enquiry WHERE (email in 
						(SELECT email from jos_hp_enquiry WHERE property_id = '$id') 
						OR user_id IN 
						(SELECT user_id from jos_hp_enquiry WHERE property_id = '$id'))
						AND property_id != '$id'");
	
	if($database->getNumRows == 0) $database->setQuery("SELECT distinct id FROM jos_hp_properties ORDER BY RAND()");

	$enquiries = $database->loadResultArray();
	
	$wishlist = array();
	
	$recommend_id = implode(',',array_merge($enquiries, $wishlist));
	$where[] = "p.id IN(".$recommend_id.")";
	HP_getProperty('listing', $where, $prop, $caption, 0 , 3);

	# Get All thumbnails
	$database->setQuery("SELECT * FROM #__hp_photos"
		.	"\nWHERE property '".$id."'"
		.	"\nORDER BY ordering ASC");
	$images = $database->loadObjectList();

	# Get Agent's info
	if ($hp_show_agentdetails && $hp_use_companyagent) {
		$sql = "SELECT a.*, COUNT(p.id) AS properties, c.id AS companyid, c.name AS company FROM (#__hp_agents AS a, #__hp_prop_types AS t) "
		.	"\nLEFT JOIN #__hp_companies AS c ON c.id=a.company"
		.	"\nLEFT JOIN #__hp_properties AS p ON p.agent=a.id"
		.	"\nWHERE p.suburb LIKE '".$prop[0]->suburb."'"
		.	"\n AND t.id=p.type"
		. "\n AND p.published='1' AND p.approved='1' AND t.published='1'"
		. "\n	AND (publish_up = '0000-00-00 00:00:00' OR publish_up <= NOW())"
		. "\n	AND (publish_down = '0000-00-00 00:00:00' OR publish_down >= NOW())"
		. "\nGROUP BY p.agent"
		.	"\nLIMIT 3";
		$database->setQuery($sql);
		$agent = $database->loadObjectList();
	}

	# Record Hit
	$database->setQuery("UPDATE #__hp_properties SET hits = hits + 1 WHERE id ='".$id."' LIMIT 1");
	$database->query();

	hotproperty_HTML::recommend_Prop($prop, $caption, $images, $agent);

}

function showCompany( $id, $limit, $limitstart ) {
	global $database, $Itemid;

	if ($id == 0) mosRedirect(sefRelToAbs('index.php?option=com_hotproperty&amp;Itemid=$Itemid'));

	# Get Company's info
	$database->setQuery("SELECT c.* FROM #__hp_companies AS c"
		.	"\nWHERE c.id='".$id."'"
		.	"\nLIMIT 1");
	$company = $database->loadObjectList();

	# Redirect to main listing if company does not exists
	if (count($company) == 0) mosRedirect(sefRelToAbs('index.php?option=com_hotproperty&Itemid='.$Itemid));

	# Pagination
	$database->setQuery("SELECT COUNT(DISTINCT a.id) FROM (#__hp_agents AS a, #__hp_prop_types AS t) "
		. "\nLEFT JOIN #__hp_properties AS p ON p.agent=a.id"
		. "\nWHERE a.company='".$id."'"
		. "\n AND t.id=p.type"
		. "\n AND p.published='1' AND p.approved='1' AND t.published='1'"
		. "\n	AND (publish_up = '0000-00-00 00:00:00' OR publish_up <= NOW())"
		. "\n	AND (publish_down = '0000-00-00 00:00:00' OR publish_down >= NOW())");
	$total = $database->loadResult();
	$pageNav = new mosPageNav( $total, $limitstart, $limit );
	
	# Get Agents' info who works for this company
	$database->setQuery("SELECT a.*, COUNT(p.id) AS properties FROM (#__hp_agents AS a, #__hp_prop_types AS t)"
						. "\nLEFT JOIN #__hp_properties AS p ON p.agent=a.id"
						. "\nWHERE a.company='".$id."'"
						. "\n AND t.id=p.type"
						. "\n AND p.published='1' AND p.approved='1' AND t.published='1'"
						. "\n	AND (publish_up = '0000-00-00 00:00:00' OR publish_up <= NOW())"
						. "\n	AND (publish_down = '0000-00-00 00:00:00' OR publish_down >= NOW())"
						. "\nGROUP BY p.agent"
						. "\nORDER BY name ASC"
						. "\nLIMIT ".$limitstart.", ".$limit);
	$agents = $database->loadObjectList();

	hotproperty_HTML::show_Company($company, $agents, $pageNav);
}

function showAgent( $id, $limit, $limitstart ) {
	global $database, $Itemid, $my;

	# Get Agent's info
	# - Only get agent that has at least 1 property
	# if not logged in, then get agent based on id, otherwise use user id
	
	$database->setQuery("SELECT a.*, COUNT(p.id) AS properties, c.id AS companyid, c.name AS company FROM (#__hp_agents AS a, #__hp_prop_types AS t) "
		.	"\nLEFT JOIN #__hp_companies AS c ON c.id=a.company"
		.	"\nLEFT JOIN #__hp_properties AS p ON p.agent=a.id"
		.	"\nWHERE a.id='".($id=='' ? $my->id : $id)."'"
		.	"\n AND t.id=p.type"
		. "\n AND p.published='1' AND p.approved='1' AND t.published='1'"
		. "\n	AND (publish_up = '0000-00-00 00:00:00' OR publish_up <= NOW())"
		. "\n	AND (publish_down = '0000-00-00 00:00:00' OR publish_down >= NOW())"
		.	"\nGROUP BY p.agent");
	$agent = $database->loadObjectList();

	if (empty($agent) )	mosRedirect(sefRelToAbs('index.php?option=com_hotproperty&Itemid='.$Itemid));

	# Pagination
	$total = HP_getNumProp_GroupAgent(($id=='' ? $my->id : $id));
	$pageNav = new mosPageNav( $total, $limitstart, $limit );
	
	# Load properties belong to this agent
	$where[] = "agent='".$my->id."'";
	HP_getProperty('listing', $where, $prop, $caption, $limitstart, $limit);

	hotproperty_HTML::show_Agent($prop, $caption, $agent, $pageNav);
}

function showFeatured( $limit, $limitstart) {
	global $database, $Itemid;

	# Pagination
	$total = hotproperty::getNumProp('p.featured=1');
	$pageNav = new mosPageNav( $total, $limitstart, $limit );

	# Load properties belong to this agent
	HP_getProperty('featured', null, $prop, $caption, $limitstart, $limit);

	hotproperty_HTML::show_Featured($prop, $caption, $pageNav);
}

function showCoEmail() {
	global $database, $Itemid	;

	$id = intval( mosGetParam( $_REQUEST, 'id', 0 ) );

	# Get Company's info
	$database->setQuery("SELECT c.* FROM #__hp_companies AS c"
		.	"\nWHERE c.id='".$id."'"
		.	"\nLIMIT 1");
	$company = $database->loadObjectList();

	# If company does not exists, redirect to main listing
	if (empty($company))	mosRedirect(sefRelToAbs('index.php?option=com_hotproperty&Itemid='.$Itemid));

	hotproperty_HTML::show_CoEmail($company);
}

function showAgentEmail() {
	global $database, $Itemid	;

	$id = intval( mosGetParam( $_REQUEST, 'id', 0 ) );

	# Get Agent's info
	$database->setQuery("SELECT a.*, COUNT(p.id) AS properties, c.id AS companyid, c.name AS company FROM #__hp_agents AS a"
		.	"\nLEFT JOIN #__hp_companies AS c ON c.id=a.company"
		.	"\nLEFT JOIN #__hp_properties AS p ON p.agent=a.id"
		.	"\nWHERE a.id='".$id."'"
		.	"\nGROUP BY p.agent"
		.	"\nLIMIT 1");
	$agent = $database->loadObjectList();

	# If agent does not exists, redirect to main listing
	if (empty($agent))	mosRedirect(sefRelToAbs('index.php?option=com_hotproperty&Itemid='.$Itemid));

	hotproperty_HTML::show_AgentEmail($agent);
}

function sendEnquiry() {
	global $database, $Itemid, $mosConfig_live_site, $mosConfig_sef, $option, $my;

	josSpoofCheck(1);

	$id = intval( mosGetParam( $_REQUEST, 'id', 0 ) );
	$sbj = trim( strtolower( mosGetParam( $_POST, 'sbj', '' ) ) );

	# Redirect to main listing if property/agent is invalid or not unpublish
	if ($id == 0) mosRedirect(sefRelToAbs('index.php?option=com_hotproperty&Itemid='.$Itemid));
	if (empty($sbj)) mosRedirect(sefRelToAbs('index.php?option=com_hotproperty&Itemid='.$Itemid));

	if ($sbj == "property") {
		if (!HP_isPropPublished($id)) mosRedirect(sefRelToAbs('index.php?option=com_hotproperty&Itemid='.$Itemid));
	} elseif ($sbj == "agent") {
		$database->setQuery("SELECT id FROM #__hp_agents WHERE id='".$id."' LIMIT 1");
		$agentid = $database->loadResult();
		if ( empty($agentid) ) {
			mosRedirect(sefRelToAbs('index.php?option=com_hotproperty&Itemid='.$Itemid));
		}
	} elseif ($sbj == "company") {
		$database->setQuery("SELECT id FROM #__hp_companies WHERE id='".$id."' LIMIT 1");
		$companyid = $database->loadResult();
		if ( empty($companyid) ) {
			mosRedirect(sefRelToAbs('index.php?option=com_hotproperty&Itemid='.$Itemid));
		}
	}

	# Assign form's value
	$hp_email = trim( mosGetParam( $_POST, 'hp_email', '' ) );
	$hp_contactnumber = trim( mosGetParam( $_POST, 'hp_contactnumber', '' ) );
	$hp_name = trim( mosGetParam( $_POST, 'hp_name', '' ) );
	$hp_enquiry = trim( mosGetParam( $_POST, 'hp_enquiry', '' ) );

	# Validate form's data
	if (!$hp_name || !$hp_enquiry){
		echo "<script>alert (\""._HP_CONTACT_ERR_COMPLETE."\"); window.history.go(-1);</script>";
		exit(0);
	}
	if (!$hp_email && !$hp_contactnumber){
		echo "<script>alert (\""._HP_CONTACT_ERR_ONECONTACT."\"); window.history.go(-1);</script>";
		exit(0);
	}
	if ($hp_email && !is_email($hp_email)) {
		echo "<script>alert (\""._HP_CONTACT_ERR_VALIDEMAIL."\"); window.history.go(-1);</script>";
		exit(0);
	}

	# Get agent's email & name, Property's title
	if ($sbj == "property") {
		$sql = "SELECT a.name AS name, a.email AS email, p.name AS propTitle FROM #__hp_properties AS p"
			.	"\nLEFT JOIN #__hp_agents AS a ON a.id=p.agent"
			.	"\nWHERE p.id='".$id."'";
	} elseif ($sbj == "agent") {
		$sql = "SELECT a.name AS name, a.email AS email FROM #__hp_agents AS a"
			.	"\nWHERE a.id='".$id."'";
	} elseif ($sbj == "company") {
		$sql = "SELECT c.name AS name, c.email AS email FROM #__hp_companies AS c"
			.	"\nWHERE c.id='".$id."'";
	}
	$database->setQuery($sql);
	$database->loadObject($agent);

	$email_to = $agent->email;

	# Construct the email
	$text  = ""._HP_CONTACT_ENQUIRY_TEXT.$hp_name;
	$text .= "\n"._CMN_EMAIL.": ". $hp_email . "\n"._HP_CONTACTNUMBER.": ".$hp_contactnumber."\r\n".stripslashes($hp_enquiry);
	if ($sbj == "property") {
		$text .= "\n\n"._HP_CONTACT_ENQUIRY_TEXT2;
		$text .= "\n\n\t";

		if ($mosConfig_sef) {
			$url = sefRelToAbs("index.php?option=com_hotproperty&task=view&id=$id");
		} else {
			// Get Itemid
			$database->setQuery("SELECT id FROM #__menu WHERE link='index.php?option=$option'");
			$Itemid = $database->loadResult();
			$url = $mosConfig_live_site .'/'. "index.php?option=com_hotproperty&task=view&id=$id&Itemid=$Itemid";
		}

		$text .= $url;
	}
	if ($sbj == "property")	$subject = _HP_CONTACT_ENQUIRY_SUBJECTP.$agent->propTitle;
	elseif ($sbj == "agent") $subject = _HP_CONTACT_ENQUIRY_SUBJECTA;
	elseif ($sbj == "company") $subject = _HP_CONTACT_ENQUIRY_SUBJECTC;
	$headers .= "From: ".$hp_name." <".$hp_email.">\r\n";
	$headers .= "Reply-To: <".$hp_email.">\r\n";
	$headers .= "X-Priority: 3\r\n";
	$headers .= "X-MSMail-Priority: Low\r\n";
	$headers .= "X-Mailer: Hot Property\r\n\r\n";
	
	
	if($sbj == 'property'){
		$database->setQuery("SELECT count(id) FROM #__hp_enquiry WHERE property_id = '$id' AND name = '$hp_name' AND enquiry = '$hp_enquiry'");
		if(!$database->loadResult()){			
			# Send the email
			mosMail($hp_email,$hp_name,$email_to,$subject,$text);
			
			# Save the enquiry to the database
			$database->setQuery("INSERT INTO #__hp_enquiry (`property_id`,`user_id`,name`,`email`,`phone`,`enquiry`,`date`) VALUES ('$id','$hp_name','$my->id','$hp_email','$hp_contactnumber','".stripslashes($hp_enquiry)."',now())");
			if (!$database->query()) echo $database->getErrorMsg();
		}
	}else{
		# Send the email
		mosMail($hp_email,$hp_name,$email_to,$subject,$text);
	}	

	if ($sbj == "property") {			
		mosRedirect("index.php?option=com_hotproperty&task=view&id=$id&Itemid=$Itemid",_HP_CONTACT_THANK);
	} elseif ($sbj == "agent") { 		
		mosRedirect("index.php?option=com_hotproperty&task=viewagent&id=$id&Itemid=$Itemid",_HP_CONTACT_THANK);
	} elseif ($sbj == "company") { 
		mosRedirect("index.php?option=com_hotproperty&task=viewco&id=$id&Itemid=$Itemid",_HP_CONTACT_THANK);
	}
}

function search() {
	global $database, $Itemid, $hp_default_limit_search;

	$searchString = new stdClass();
	$searchString->search = rawurldecode(trim( mosGetParam( $_REQUEST, 'search', '' ) ) );

	if ( empty($searchString->search) ) $searchString->search = rawurldecode(trim( mosGetParam( $_REQUEST, 'search', '' ) ) );

	$searchString->type = intval( mosGetParam( $_REQUEST, 'type', '' ) );

	if ( empty($searchString->type) ) $searchString->type = intval( mosGetParam( $_REQUEST, 'type', '' ) );

	if ( !empty($searchString->type) && empty($searchString->search) ) mosRedirect(sefRelToAbs('index.php?option=com_hotproperty&task=viewtype&id='.$searchString->type.'Itemid='.$Itemid)) ;

	if ( empty($searchString->search) ) mosRedirect(sefRelToAbs('index.php?option=com_hotproperty&Itemid='.$Itemid)) ;

  $where = array();
	$where[] = "p.name LIKE '%".  htmlentities($searchString->search, ENT_NOQUOTES)."%'";
	$where[] = "p.address LIKE '%".htmlentities($searchString->search, ENT_NOQUOTES)."%'";
	$where[] = "p.suburb LIKE '%$searchString->search%'";
	$where[] = "p.state LIKE '%$searchString->search%'";
	$where[] = "p.country LIKE '%$searchString->search%'";
	$where[] = "p.intro_text LIKE '%$searchString->search%'";
	$where[] = "p.full_text LIKE '%$searchString->search%'";

	# Pagination
	$database->setQuery("SELECT COUNT(*) FROM #__hp_properties AS p"
		. "\nLEFT JOIN #__hp_prop_types AS t ON p.type = t.id"
		. "\nWHERE p.published='1' AND p.approved='1' AND t.published='1'"
		. "\n	AND (publish_up = '0000-00-00 00:00:00' OR publish_up <= NOW())"
		. "\n	AND (publish_down = '0000-00-00 00:00:00' OR publish_down >= NOW())"
		.	(count( $where ) ? "\nAND (" . implode( ' OR ', $where ) .")": "")
		. (($searchString->type <> 0) ? "\nAND p.type=".$searchString->type: "")
		);
	$total = $database->loadResult();
	$limit = trim( mosGetParam( $_REQUEST, 'limit', $hp_default_limit_search) );
	$limitstart = trim( mosGetParam( $_REQUEST, 'limitstart', 0 ) );
	$pageNav = new mosPageNav( $total, $limitstart, $limit );

	# Only log search if the listing is on the first page. This prevents multiple hits for the same search if a
	# search result span multipls pages
	if ($limit == $hp_default_limit_search && $limitstart == 0) HP_logSearch($searchString->search);

	# Select published types
	$database->setQuery( "SELECT * FROM #__hp_prop_types AS t"
		. "\nWHERE t.published='1' AND t.id<>0"
		. "\nORDER BY t.ordering ASC");
	$types = $database->loadObjectList();

	# Load Result of the search
	$where2[] = "(".implode( ' OR ', $where ).")";
	if ($searchString->type <> 0){
		$type=new mosHPPropTypes($database);
		$type->load($searchString->type);
		$where2[] = "t.lft>=$type->lft AND t.rgt<=$type->rgt";
	}
	HP_getProperty('listing', $where2, $prop, $caption, $limitstart, $limit);

	hotproperty_HTML::show_SearchResults($types, $prop, $caption, $pageNav, $searchString);
}

/**
 * Reset the search
 *  
 */
function resetSearch() {

	global $Itemid, $ps_booking;
	$redirect = mosGetParam( $_REQUEST, 'source_page' );
	$ps_booking->reset();
	$ps_booking->updateSession();
	$redirect = ($redirect == '/') ? $redirect : cleanRedirectUrl($redirect);
	mosRedirect($redirect);
}

/**
 * Clears search variables from the redirect url
 *  
 */
function cleanRedirectUrl($url){
	$parse_url = parse_url($url);
	parse_str($parse_url['query'], $parsed);

	unset($parsed['dateTo']);
	unset($parsed['dateFrom']);
	unset($parsed['sleeps']);
	unset($parsed['prop_type']);

	$new_query_string = '';
	foreach($parsed as $key => $arg)
			$new_query_string .= $key .'='.$arg.'&';
	return $parse_url['path'].'?'.$new_query_string;
}

/**
* Perform the Advanced Search
*
*/
function asearch2() {
	global $database, $Itemid, $hp_default_limit_search, $hp_use_advsearch;

	if ($hp_use_advsearch <> '1') {
		mosRedirect(sefRelToAbs('index.php?option=com_hotproperty&Itemid='.$Itemid)) ;
	}

	# Load up search ID if available
	$search_id = intval( mosGetParam( $_REQUEST, 'search_id', 0 ) );
	
	//Get limiting options
	$limit = trim( mosGetParam( $_REQUEST, 'limit', $hp_default_limit_search) );
	$limitstart = trim( mosGetParam( $_REQUEST, 'limitstart', 0 ) );

	if ($search_id > 0)
	{
		$database->setQuery("SELECT search_text FROM #__hp_search_results WHERE search_id ='".$search_id."'");
		$database->query();
		$_REQUEST = unserialize($database->loadResult());
	}

	# Get Extra Fields setting
	$sql = "SELECT name, iscore, size, field_elements, field_type, search_caption, search_type FROM #__hp_prop_ef WHERE search=1 AND published = 1 AND hidden = 0 ORDER BY iscore DESC, ordering ASC";
	$database->setQuery($sql);
	$rows = $database->loadObjectList();

	# Look for range_1 & range_2 fields and validate is_numeric

	foreach($rows AS $row) {
		switch($row->search_type) {
			case 'range_1':
				if ( mosGetParam( $_REQUEST, $row->name, '' ) <> '' ) {
					if ( !is_numeric(trim( mosGetParam( $_REQUEST, $row->name."_range", '' ))) || !is_numeric(trim( mosGetParam( $_REQUEST, $row->name, '' ))) ) {
						//echo "range_1";
						hotproperty_HTML::show_advSearchResults_error(_HP_SEARCH_NOTVALID);
						//exit();
					}
				}
				break;
			case 'range_2':
				if( (trim( mosGetParam( $_REQUEST, $row->name."_from", '' )) <> '' && !is_numeric(trim( mosGetParam( $_REQUEST, $row->name."_from", '' )))) || (trim( mosGetParam( $_REQUEST, $row->name."_to", '' )) <> '' && !is_numeric(trim( mosGetParam( $_REQUEST, $row->name."_to", '' )))) ) {
					//echo "range_2";
					hotproperty_HTML::show_advSearchResults_error(_HP_SEARCH_NOTVALID);
					//exit();
				}
				break;
		}
	}

	/***************************************************************
	 * Construct mos_hp_temp table for advance searching
	 * Hot Property Advanced Searching works by elimination.
	 * It first creates a temporary table consisting all searchable fields and it's data.
	 * HP will narrow the result by eliminations.
	 ***************************************************************/
	$database->setQuery("DROP TABLE IF EXISTS #__hp_temp");
	$database->query();

	$sql = "CREATE TABLE #__hp_temp ("
		."\n id INT(11) NOT NULL, ";

	foreach ($rows AS $col) {
		if ($col->name == 'type') {
			$sql .= "\n type MEDIUMTEXT NOT NULL, ";
			$sql .= "\n lft int(11) NOT NULL, ";
			$sql .= "\n rgt int(11) NOT NULL, ";
		} 
		else if ($col->field_type == 'checkbox' || $col->field_type == 'selectmultiple')
		{
			// Create SET field for checkbox and selectmultiple fields
			$sql .= "\n".$col->name." SET('".str_replace('|',"', '",addslashes($col->field_elements))."') NOT NULL, ";
		# On rajoute lft et rgt:
		}else {
			$sql .= "\n".$col->name." MEDIUMTEXT NOT NULL, ";
		}
	}
	$sql .= "\nPRIMARY KEY  (id)"
		. "\n) TYPE=MyISAM ;";

	$database->setQuery($sql);
	if (!$database->query())
	{
		echo $database->getErrorMsg();
	}

	# Populating core field's data
	$sql2 = "INSERT INTO #__hp_temp (`id`";

	foreach ($rows AS $col) {
		if ($col->iscore) {
			if($col->name == 'type'){
				$sql2 .= ", `type`";
				$sql2 .= ", `lft`";
				$sql2 .= ", `rgt`";
			} else {
				$sql2 .= ", `".$col->name."`";
			}
		}
	}
	$sql2 .= ") \n SELECT p.id";
	foreach ($rows AS $col) {
		if ($col->iscore) {
			if ($col->name == 'company')
			{
				$sql2 .= ", c.id AS company";
			# On peuple les champs lft et rgt rajout�s:
			} elseif ($col->name == 'type') {
				$sql2 .= ", p.type";
				$sql2 .= ", t.lft";
				$sql2 .= ", t.rgt";
			} else {
				$sql2 .= ", p.".$col->name;
			}
		}
	}

	$sql2 .= "\n FROM (#__hp_properties AS p, #__hp_companies AS c)"
		. "\n LEFT JOIN #__hp_prop_types AS t ON p.type = t.id"
		. "\n LEFT JOIN #__hp_agents AS a ON p.agent = a.id"
		. "\n WHERE p.published='1' AND p.approved='1' AND t.published='1'"
		.	"\n AND a.company=c.id"
		. "\n	AND (publish_up = '0000-00-00 00:00:00' OR publish_up <= NOW())"
		. "\n	AND (publish_down = '0000-00-00 00:00:00' OR publish_down >= NOW())";

	$database->setQuery($sql2);
	if (!$database->query()) {
		echo $database->getErrorMsg();
	}

	# Populating non-core field's data
	$sql3 = "SELECT e.name, e.field_type, p.property, p.field, p.value FROM (#__hp_properties2 AS p, #__hp_prop_ef AS e)"
		.	"\nWHERE e.id=p.field AND e.published=1 AND e.hidden=0 AND e.search=1";

	$database->setQuery($sql3);
	$rows3 = $database->loadObjectList();

	foreach ($rows3 AS $row3) {
		if ($row3->field_type == 'checkbox' || $row3->field_type == 'selectmultiple')
		{
			$sql4 = "UPDATE #__hp_temp SET ".$row3->name."='".mysql_escape_string(str_replace('|',",",$row3->value))."' WHERE id='".$row3->property."'";
		} else {
			$sql4 = "UPDATE #__hp_temp SET ".$row3->name."='".$row3->value."' WHERE id='".$row3->property."'";
		}
		$database->setQuery($sql4);
		$database->query();
	}

	/***************************************************************
	 * End of creating mos_hp_temp
	 ***************************************************************/

	# Setup the sql conditions
	$where = '';

	foreach ( $rows AS $row ) {

		if ( ($row->field_type == "link" || $row->field_type == "text" || $row->field_type == "multitext" || $row->name == "name") && trim( mosGetParam( $_REQUEST, $row->name )) <> '' && ($row->search_type <> 'range_1' && $row->search_type <> 'range_2' && $row->search_type <> 'range_3') ) {

			$where = $row->name ." NOT LIKE '%".mysql_escape_string(trim( mosGetParam( $_REQUEST, $row->name, '' ) ))."%'";
			$database->setQuery("DELETE FROM #__hp_temp WHERE ".$where);
			if(!$database->query()) {
				echo $database->getErrorMsg();
			}

		} elseif($row->field_type == "selectlist" && trim( mosGetParam( $_REQUEST, $row->name, '' )) <> '' ) {
			if ( //Ignore All Agent, All Company & ALL type conditions with value = 0
				!($row->name == "type" && trim( mosGetParam( $_REQUEST, $row->name, '' ) ) == '0') &&
				!($row->name == "agent" && trim( mosGetParam( $_REQUEST, $row->name, '' ) ) == '0') &&
				!($row->name == "company" && trim( mosGetParam( $_REQUEST, $row->name, '' ) ) == '0') ) {

				# Cas particulier du type:
				if($row->name == "type"){
					# On charge le type s�lectionn� dans le formulaire:
					$searchtype = new mosHPPropTypes($database);
					$searchtype->load(trim( mosGetParam( $_REQUEST, "type", '' ) ));

					# On �carte tous les types n'�tant pas dans l'intervalle:
					$where = "lft < '".$searchtype->lft."' OR rgt > '".$searchtype->rgt."'";
				} else {
					$where = $row->name ." <> '".trim( mosGetParam( $_REQUEST, $row->name, '' ) )."'";
				}

				$database->setQuery("DELETE FROM #__hp_temp WHERE ".$where);
				if(!$database->query()) {
					echo $database->getErrorMsg();
				}
			}

		} elseif(
			($row->field_type == "checkbox" || $row->field_type == "selectmultiple")
			&& trim( mosGetParam( $_REQUEST, $row->name, '' )) <> '' ) {

			$tmp = mosGetParam( $_REQUEST, $row->name, '' );
			if (is_array($tmp) && count($tmp) > 0) {
				foreach($tmp AS $t) {
					if (!empty($t))
					{
						$where = "FIND_IN_SET('".$t."', ".$row->name.") < 1";
						$database->setQuery("DELETE FROM #__hp_temp WHERE ".$where);
						if(!$database->query()) {
							echo $database->getErrorMsg();
						}
					}
				}
			}
		} elseif($row->field_type == "radiobutton" && trim( mosGetParam( $_REQUEST, $row->name, '' )) <> '' ) {

			$where = $row->name ." <> '".trim( mosGetParam( $_REQUEST, $row->name, '' ) )."'";
			$database->setQuery("DELETE FROM #__hp_temp WHERE ".$where);
			if(!$database->query())
			{
				echo $database->getErrorMsg();
			}

		} elseif($row->search_type == "range_1" && trim( mosGetParam( $_REQUEST, $row->name."_range", '' )) <> '' && trim( mosGetParam( $_REQUEST, $row->name, '' )) <> '' ) {
			# Delete all records which has empty value
			$where = $row->name ." = ''";
			$database->setQuery("DELETE FROM #__hp_temp WHERE ".$where);
			if(!$database->query()) echo $database->getErrorMsg();

			if (trim( mosGetParam( $_REQUEST, $row->name."_range", '' ) ) == '0' ) {
				$where = $row->name ." >= ".intval( trim( mosGetParam( $_REQUEST, $row->name, 0 ) ) );
				$database->setQuery("DELETE FROM #__hp_temp WHERE ".$where);
				if(!$database->query())
				{
					echo $database->getErrorMsg();
				}

			} else {
				$where = $row->name ." <= ".intval( trim( mosGetParam( $_REQUEST, $row->name, 0 ) ) );
				$database->setQuery("DELETE FROM #__hp_temp WHERE ".$where);
				if(!$database->query()) {
					echo $database->getErrorMsg();
				}
			}

		} elseif($row->search_type == "range_2" && (trim( mosGetParam( $_REQUEST, $row->name."_from", '' )) <> '' || trim( mosGetParam( $_REQUEST, $row->name."_to", '' )) <> '') ) {
			# Delete all records which has empty value
			$where = $row->name ." = ''";
			$database->setQuery("DELETE FROM #__hp_temp WHERE ".$where);
			if(!$database->query()) echo $database->getErrorMsg();

			# Delete lower bound
			if (mosGetParam( $_REQUEST, $row->name."_from", '' ) <> '') {
				$where = $row->name ." < ".intval( trim( mosGetParam( $_REQUEST, $row->name."_from", '' ) ) );
				$database->setQuery("DELETE FROM #__hp_temp WHERE ".$where);
				if(!$database->query()) {
					echo $database->getErrorMsg();
				}
			}

			# Delete upper bound
			if (mosGetParam( $_REQUEST, $row->name."_to", '' ) <> '') {
				$where = $row->name ." > ".intval( trim( mosGetParam( $_REQUEST, $row->name."_to", '' ) ) );
				$database->setQuery("DELETE FROM #__hp_temp WHERE ".$where);
				if(!$database->query()) {
					echo $database->getErrorMsg();
				}
			}

		} elseif($row->search_type == "range_3" && mosGetParam( $_REQUEST, $row->name, 0 ) == '1' && trim( mosGetParam( $_REQUEST, $row->name."_from_y", '' )) <> '' && trim( mosGetParam( $_REQUEST, $row->name."_from_m", '' )) <> '' && trim( mosGetParam( $_REQUEST, $row->name."_from_d", '' )) <> '' && trim( mosGetParam( $_REQUEST, $row->name."_to_y", '' )) <> '' && trim( mosGetParam( $_REQUEST, $row->name."_to_m", '' )) <> '' && trim( mosGetParam( $_REQUEST, $row->name."_to_d", '' )) <> '' ) {

			# Delete lower bound
			$where = $row->name ." < '".trim( mosGetParam( $_REQUEST, $row->name."_from_y", '' ) )."-".trim( mosGetParam( $_REQUEST, $row->name."_from_m", '' ) )."-".trim( mosGetParam( $_REQUEST, $row->name."_from_d", '' ) )."'";
			$database->setQuery("DELETE FROM #__hp_temp WHERE ".$where);
			if(!$database->query()) {
				echo $database->getErrorMsg();
			}

			# Delete upper bound
			$where = $row->name ." > '".trim( mosGetParam( $_REQUEST, $row->name."_to_y", '' ) )."-".trim( mosGetParam( $_REQUEST, $row->name."_to_m", '' ) )."-".trim( mosGetParam( $_REQUEST, $row->name."_to_d", '' ) )."'";
			$database->setQuery("DELETE FROM #__hp_temp WHERE ".$where);
			if(!$database->query()) {
				echo $database->getErrorMsg();
			}

		}

	}

	//if (empty($where)) {
	//	hotproperty_HTML::show_advSearchResults_error(_HP_SEARCH_NOSEARCH);
	//} else {
		# How many results?
		$database->setQuery('SELECT COUNT(id) FROM #__hp_temp');
		$total = $database->loadResult();
		if($total <= 0)
		{
			hotproperty_HTML::show_advSearchResults_error(_HP_SEARCH_NORESULT);
		} else {

			# Store this search for later retrieval.
			$database->setQuery("INSERT INTO #__hp_search_results (search_text) VALUES ('".serialize($_REQUEST)."')");
			if(!$database->query())
			{
				echo $database->getErrorMsg();
			}

			# Get the above search ID
			$database->setQuery("SELECT search_id FROM #__hp_search_results WHERE search_text ='".serialize($_REQUEST)."'");
			$database->query();
			$search_id = $database->loadResult();

			# Retrieve the properties' ID
			$database->setQuery('SELECT id FROM #__hp_temp');
			$ids = $database->loadResultArray();
			unset($where);
			$where[] = 'p.id IN ('.implode(',',$ids).')';

			# Setup pagination
			$pageNav = new mosPageNav( $total, $limitstart, $limit );

			HP_getProperty('listing', $where, $prop, $caption, $limitstart, $limit);
			hotproperty_HTML::show_advSearchResults($search_id, $prop, $caption, $pageNav, $searchString);
		}

	//}

}


/**
* Perform the Advanced Search
*
*/
function asearch() {
	global $database, $Itemid, $hp_default_limit_search, $hp_use_advsearch;
	
	$starttime = explode(' ',microtime());
	$starttime = $starttime[1] + $starttime[0];
	
	//Check if we advanced search is enabled
	if ($hp_use_advsearch <> '1') {
		mosRedirect(sefRelToAbs('index.php?option=com_hotproperty&Itemid='.$Itemid)) ;		
	}
	
	# Setup pagination
	$limit = trim( mosGetParam( $_REQUEST, 'limit', $hp_default_limit_search) );
	$limitstart = trim( mosGetParam( $_REQUEST, 'limitstart', 0 ) );
			
	# Load up search ID if available
	$search_id = intval( mosGetParam( $_REQUEST, 'search_id', 0 ) );
	
	if ($search_id > 0) 
	{
		$database->setQuery("SELECT search_text FROM #__hp_search_results WHERE search_id ='".$search_id."'");
		$database->query();
		$_REQUEST = unserialize($database->loadResult());
	}
	
	/* Get search fields from query string */
	
	$request = $_REQUEST;
	//Remove some common non search felds
	unset($request['option']);
	unset($request['task']);
	unset($request['limit']);
	unset($request['limitstart']);
	unset($request['sort']);
	unset($request['order']);
	unset($request['Itemid']);
	
	$fields = implode("','",array_keys($request));
	
	# Get Extra Fields setting
	$sql = "SELECT id, name, field_type, field_elements, iscore, search_type FROM #__hp_prop_ef WHERE search=1 AND published = 1 AND hidden = 0 AND name IN ('$fields') ORDER BY iscore DESC, ordering ASC";
	$database->setQuery($sql);
	$rows = $database->loadObjectList('name');

	//Gather all the properties from the DB
	$sql = "SELECT p.id FROM (#__hp_properties AS p)"
		. "\n LEFT JOIN #__hp_prop_types AS t ON p.type = t.id"
		. "\n WHERE p.published='1' AND p.approved='1' AND t.published='1'"
		. "\n AND (publish_up = '0000-00-00 00:00:00' OR publish_up <= NOW())"
		. "\n AND (publish_down = '0000-00-00 00:00:00' OR publish_down >= NOW())";
		
	$database->setQuery($sql);
	$properties = $database->loadResultArray();
	
	$arrays = array('selectmultiple', 'checkbox');
	$text = array('text','multitext','wysiwyg');
	$props = array();
		
	//Loop through the searched fields
	foreach($rows AS $row) {
		
		$value = mosGetParam($_REQUEST, $row->name, '');
		$run = 0;
		
		//Check that if were searching for type/agent/company that the value isnt 0 or value isnt ''
		if(	!($row->name == "type" && trim( $value ) == '0') && 
			!($row->name == "agent" && trim( $value ) == '0') && 
			!($row->name == "company" && trim( $value ) == '0') &&
			$value !== ''){
				
			//Check which table we are searching
			if($row->iscore){
				
				$type = '';				
				if($row->name == "type"){
					# On charge le type s�lectionn� dans le formulaire:
					$searchtype = new mosHPPropTypes($database);
					$searchtype->load(trim( $value ));

					//$type = ' LEFT JOIN #__hp_type AS type ON type.id = #__hp_properties.type';
				}
				
				$q = "SELECT id FROM #__hp_properties $type WHERE ";
				$valueName = $row->name;
			}else{
				$q = "SELECT property FROM #__hp_properties2 WHERE field = '$row->id' AND ";
				$valueName = 'value';
			}
			
			if($row->name == 'name'){
				
				$run = 1;
				$value = preg_replace('/ +/', ' ', strtolower($value));
				$parts = explode(' ', $value);
				$q .= " (`$valueName` LIKE '%".implode("%' AND `$valueName` LIKE '%", $parts)."%')";	
				
			}else{
				
				$search_type = mosGetParam($_REQUEST, $row->name.'_searchtype', $row->search_type);
		
				switch($search_type) {
					//Less / more than x
					case 'range_1':
						$range = mosGetParam( $_REQUEST, $row->name."_range", '' );
							
						//Check that the range type is specified and that the range value is an integer
						if ( !is_numeric(trim( $range )) || !is_numeric(trim( $value )) ) {
							//echo "range_1";
							hotproperty_HTML::show_advSearchResults_error(_HP_SEARCH_NOTVALID);
							return;
						}
						
						$q .= " (`$valueName` ".($range ? '>' : '<')." $value)";	
						$run = 1;									
						break;
						
					//From x to y
					case 'range_2':
						$valueFrom = trim(mosGetParam( $_REQUEST, $row->name."_from", '' ));
						$valueTo = trim(mosGetParam( $_REQUEST, $row->name."_to", '' ));
			
						if( (trim( mosGetParam( $_REQUEST, $row->name."_from", '' )) !== '' && !is_numeric(trim( $valueFrom ))) || (trim( mosGetParam( $_REQUEST, $row->name."_to", '' )) !== '' && !is_numeric($valueTo)) ) {
							//echo "range_2";
							hotproperty_HTML::show_advSearchResults_error(_HP_SEARCH_NOTVALID);
							return;
						}
	
						$q .= " (`$valueName` > $valueFrom AND `$valueName` < $valueTo)";		
						$run = 1;							
						break;
					
					//Less than or equal to
					case 'range_3':
						
						//Check that the range type is specified and that the range value is an integer
						if ( !is_numeric(trim( $value )) ) {
							//echo "range_1";
							hotproperty_HTML::show_advSearchResults_error(_HP_SEARCH_NOTVALID);
							return;
						}
						
						$q .= " (`$valueName` <= $value)";	
						$run = 1;											
						break;
						
					//More than or equal to
					case 'range_4':
						
						//Check that the range type is specified and that the range value is an integer
						if ( !is_numeric(trim( $value )) ) {
							//echo "range_1";
							hotproperty_HTML::show_advSearchResults_error(_HP_SEARCH_NOTVALID);
							return;
						}					
						$q .= " (`$valueName` >= $value)";	
						$run = 1;			
						break;
											
					//Simple value check
					case 0:
						
						//Check if we are dealing with arrays in the url
						//Check if the field type is an array
						if(in_array($row->field_type, $arrays)){								
							//Check if request value is an array
							if(is_array($value)){
								//Loop through array values to check for
								$q_tmp = array();
								foreach($value as $v){
									$q_tmp[] =  " (`$valueName` = '$v' OR `$valueName` like '%|$v|%' OR `$valueName` like '$v|%' OR `$valueName` like '%|$v') ";
								}
								$q .= implode('AND',$q_tmp);
							}else{
								$q .=  " (`$valueName` = '$value' OR `$valueName` like '%|$value|%' OR `$valueName` like '$value|%' OR `$valueName` like '%|$value') ";
							}
						//If not, we're simply checking the value					
						}else if(in_array($row->field_type,$text)){
							$q .=  " LOWER(`$valueName`) LIKE LOWER('%$value%')";
						}else{
							$q .=  " `$valueName` = '$value'";				
						}
						$run = 1;													
						break;
				}
			}
			if($run){
				//Get properties from DB
				$database->setQuery($q);	
								
				$result = $database->loadResultArray();	
				//Record the result for this field
				$props[$row->name] = $result;
				
				//Create a new properties array that is an intersect 
				//of the current array and the result of this query
				$properties = array_intersect($properties, $result);
			}
		}
	}
	
	$time = explode(' ',microtime());
	$time = $time[1] + $time[0];
	
	//echo 'Asearch queries : '.($time - $starttime).' seconds<br />';
	
	if($search_id == 0){
		# Store this search for later retrieval.
		$request = $_REQUEST;
		unset($request['filter']);
		$database->setQuery("INSERT INTO #__hp_search_results (search_text) VALUES ('".serialize($request)."')");		
		if(!$database->query())
		{
			echo $database->getErrorMsg();
		}
	
		$search_id = $database->insertid();
	}
	
	/**
	 * Get the unavailable properties from VirtueBook
	 */
	global $mosConfig_absolute_path, $ps_booking;
	if(file_exists("$mosConfig_absolute_path/components/com_virtuemart/virtuemart_parser.php")){
		$hp_limitstart = $limitstart;
		$hp_limit = $limit;
		require_once( "$mosConfig_absolute_path/components/com_virtuemart/virtuemart_parser.php" );
		if(is_object($ps_booking)){
			
			//Get the unavailable properties
			$available = $ps_booking->getAvailableProperties();	
			//$ps_booking->updateSession();
			if(is_array($available)){
				//Remove the unavailable properties from the props array
				$properties = array_unique(array_intersect($properties, $available));
			}
		}
		$limitstart = $hp_limitstart;
		$limit = $hp_limit;
	}
	
	/**
	 * Set the where clause for HP_getProperty
	 */
	$where[] = 'p.id IN ('.implode(',',$properties).')';
	
	/**
	 * Create the page nav object
	 */
	$total = count($properties);
	$prop = $caption = '';
	$pageNav = new mosPageNav( $total, $limitstart, $limit );
	
	if($total == 0){
		hotproperty_HTML::show_advSearchResults_error(_HP_SEARCH_NORESULT);
		return;
	}else{	
	
		/**
		 * Get the properties
		 */
		HP_getProperty('listing', $where, $prop, $caption, $limitstart, $limit, 'featured');
	}
	
	/**
	 * Show the results
	 */
	hotproperty_HTML::show_advSearchResults($search_id, $prop, $caption, $pageNav, $searchString);
	$time = explode(' ',microtime());
	$time = $time[1] + $time[0];	
	//echo 'End : '.($time - $starttime).' seconds<br />';
}

/**
* Display the Advanced Search Form
*/
function searchAdv() {
	global $database, $hp_use_advsearch, $Itemid, $mosConfig_live_site;

	if ($hp_use_advsearch <> '1') {
		mosRedirect(sefRelToAbs('index.php?option=com_hotproperty&Itemid='.$Itemid)) ;
	}

	$type=mosGetParam($_GET, 'type', 0);
	$type_O=new mosHPPropTypes($database);
	$type_O->load($type);	
	//echo "type=".$type_O->name;

	# la requete s�lectionnant les Extra-fields li�s � la cat�gorie s�lectionn�e:
	$sql = "SELECT DISTINCT ef.id, ef.field_type, ef.name, ef.caption, ef.default_value, ef.size, ef.field_elements, ef.prefix_text, ef.append_text, ef.ordering, ef.hidden, ef.published, ef.featured, ef.listing, ef.hideCaption, ef.iscore, ef.search, ef.search_caption, ef.search_type FROM #__hp_prop_ef AS ef"
	. "\nLEFT JOIN #__hp_prop_ef2 AS ef2 ON ef2.id=ef.id"
	. "\nLEFT JOIN #__hp_prop_types AS t ON t.id=ef2.type"
	. "\nWHERE ef.search=1 AND ef.published=1 AND ef.hidden=0"
	. "\nAND ( t.lft<=".$type_O->lft." AND t.rgt>=".$type_O->rgt." ) OR ef.name='name' "
	. "\nORDER BY ef.iscore DESC, t.lft ASC, ef.ordering ASC";

	$database->setQuery($sql);
	$rows = $database->loadObjectList();


	$database->setQuery($sql);
	$rows_elements = $database->loadObjectList("name");
	$i = 0;
	foreach($rows AS $row) {

		$fields[$i]->name = $row->name;
		$fields[$i]->caption = $row->search_caption;

		if ( !isset($fields[$i]->input) ) $fields[$i]->input = '';

		unset($list);

		# Handle core field's search
		if ($row->iscore) {

			# ------------ Type
			if ($row->name == "type") {
				# Select published types
				$database->setQuery( "SELECT * FROM #__hp_prop_types AS t"
					. "\nWHERE t.published='1'"
					. "\nORDER BY t.ordering ASC");
				$types = $database->loadObjectList();

				// liste indent�e des cat�gories:
				$preload = array();
				$preload[] = mosHTML::makeOption( '0', _HP_SEARCH_ALLTYPES );
				$selected = array();
				$selected[] = mosHTML::makeOption( $type );
				$fields[$i]->input = mosHTML::treeSelectList( $types, -1, $preload, 'type', 'class="inputbox" size="1" onchange="javascript:window.location=\''.$mosConfig_live_site.'/index.php?option=com_hotproperty&task=advsearch&type=\'+this.value+\'&Itemid='.$Itemid.'\'"', 'value', 'text', $selected );

			}
			# ------------ Price
			elseif ($row->name == "price") {
				$fields[$i]->input = mosHTML_hp::rangeText( $row->name, 'size="8" class="inputbox"');
			}
			# ------------ Featured
			elseif ($row->name == "featured") {
				$fields[$i]->input .= ' <input type="checkbox" name="'.$row->name.'" value="1" /> ';
			}
			# ------------ Agent
			elseif ($row->name == "agent") {
				# Get Agents that has at least one property
				$database->setQuery("SELECT a.*, COUNT(p.id) AS properties, c.id AS companyid, c.name AS company FROM (#__hp_agents AS a, #__hp_prop_types AS t)"
					.	"\nLEFT JOIN #__hp_companies AS c ON c.id=a.company"
					.	"\nLEFT JOIN #__hp_properties AS p ON p.agent=a.id"
					.	"\nWHERE t.id=p.type"
					. "\n AND p.published='1' AND p.approved='1' AND t.published='1'"
					. "\n	AND (publish_up = '0000-00-00 00:00:00' OR publish_up <= NOW())"
					. "\n	AND (publish_down = '0000-00-00 00:00:00' OR publish_down >= NOW())"
					.	"\nGROUP BY p.agent");
				$agents = $database->loadObjectList();

				$search_agents = array();
				$search_agents[] = mosHTML::makeOption( '0', _HP_SEARCH_ALLAGENTS );
				foreach($agents AS $agent) {
					$search_agents[] = mosHTML::makeOption( $agent->id, $agent->name );
				}

				$fields[$i]->input = mosHTML::selectList( $search_agents, $row->name, 'class="inputbox" size="1"',	'value', 'text', '0');
			}
			# ------------ Company
			elseif ($row->name == "company") {
				# Get Companies that has at least one property
				$database->setQuery("SELECT c.name, c.id FROM #__hp_companies AS c"
					.	"\nLEFT JOIN #__hp_agents AS a ON c.id=a.company"
					.	"\nLEFT JOIN #__hp_properties AS p ON p.agent=a.id"
					.	"\nLEFT JOIN #__hp_prop_types AS t ON t.id=p.type"
					.	"\nWHERE t.id=p.type"
					. "\n AND p.published='1' AND p.approved='1' AND t.published='1'"
					. "\n	AND (publish_up = '0000-00-00 00:00:00' OR publish_up <= NOW())"
					. "\n	AND (publish_down = '0000-00-00 00:00:00' OR publish_down >= NOW())"
					.	"\nGROUP BY a.company");
				$companies = $database->loadObjectList();

				$search_companies = array();
				$search_companies[] = mosHTML::makeOption( '0', _HP_SEARCH_ALLCOMPANIES );
				foreach($companies AS $company) {
					$search_companies[] = mosHTML::makeOption( $company->id, $company->name );
				}

				$fields[$i]->input = mosHTML::selectList( $search_companies, $row->name, 'class="inputbox" size="1"',	'value', 'text', '0');
			}
			# ------------ Hits
			elseif ($row->name == "hits") {
				$search_hits = array();
				$search_hits[] = mosHTML::makeOption( '0', _HP_SEARCH_LESS_THAN );
				$search_hits[] = mosHTML::makeOption( '1', _HP_SEARCH_MORE_THAN );

				$fields[$i]->input = mosHTML::selectList( $search_hits, "hits_range", 'class="inputbox" size="1"',	'value', 'text', '1');
				$fields[$i]->input .= '&nbsp; <input type="text" name="hits" class="inputbox" size="8" />';
			}
			# ------------ Created
			elseif ($row->name == "created") {
				# Get min & max year from available properties
				$sql = "SELECT MIN(created) AS start, MAX(created) AS end FROM #__hp_properties AS p"
					. "\nLEFT JOIN #__hp_prop_types AS t ON p.type = t.id"
					. "\nWHERE p.published='1' AND p.approved='1' AND t.published='1'"
					. "\n	AND (publish_up = '0000-00-00 00:00:00' OR publish_up <= NOW())"
					. "\n	AND (publish_down = '0000-00-00 00:00:00' OR publish_down >= NOW())"
					.	"\n AND created <> '0000-00-00 00:00:00'";
				$database->setQuery($sql);
				$database->loadObject($created);

				$fields[$i]->input = mosHTML_hp::rangeDateSelectList( $row->name, '', $selected, strtotime($created->start), strtotime($created->end) );
			}
			# ------------ Modified
			elseif ($row->name == "modified") {
				# Get min & max year from available properties
				$sql = "SELECT MIN(modified) AS start, MAX(modified) AS end FROM #__hp_properties AS p"
					. "\nLEFT JOIN #__hp_prop_types AS t ON p.type = t.id"
					. "\nWHERE p.published='1' AND p.approved='1' AND t.published='1'"
					. "\n	AND (publish_up = '0000-00-00 00:00:00' OR publish_up <= NOW())"
					. "\n	AND (publish_down = '0000-00-00 00:00:00' OR publish_down >= NOW())"
					.	"\n AND created <> '0000-00-00 00:00:00'";
				$database->setQuery($sql);
				$database->loadObject($modified);

				$fields[$i]->input = mosHTML_hp::rangeDateSelectList( $row->name, '', $selected, strtotime($modified->start), strtotime($modified->end) );
			}
			elseif ( $row->field_type == "selectlist" ) {
				$olist_translated_caption = explode("|",$row->field_elements);
				$olist = explode("|",$rows_elements[$row->name]->field_elements);
				$list[] = mosHTML::makeOption( " " );
				$j=0;
				foreach($olist AS $li) {
					$list[] = mosHTML::makeOption( trim($li),$olist_translated_caption[$j++] );
				}
				$fields[$i]->input = mosHTML::selectList( $list, $row->name, 'class="inputbox" size="1"',	'value', 'text', ' ');
			}
			# Other Core fields will use normal inputbox
			# Postcode, Name, State, Country, Suburb, Intro Text, Full Text
			else {
				$fields[$i]->input = ' <input type="text" name="'.$row->name.'" size="35" class="inputbox" /> ';
			}

		}
		# Non-core Extra Fields
		else {
			if ($row->search_type == "range_1") {
				$range_1_options = array();
				$range_1_options[] = mosHTML::makeOption( '0', _HP_SEARCH_LESS_THAN );
				$range_1_options[] = mosHTML::makeOption( '1', _HP_SEARCH_MORE_THAN );

				$fields[$i]->input = mosHTML::selectList( $range_1_options, $row->name."_range", 'class="inputbox" size="1"',	'value', 'text', '1');
				$fields[$i]->input .= '&nbsp; <input type="text" name="'.$row->name.'" class="inputbox" size="'.$row->size.'" />';
			}
			elseif ($row->search_type == "range_2") {
				$fields[$i]->input = mosHTML_hp::rangeText( $row->name, 'size="'.$row->size.'" class="inputbox"');
			}
			elseif ($row->field_type == "link" || $row->field_type == "text" || $row->field_type == "multitext") {
				$fields[$i]->input = ' <input type="text" name="'.$row->name.'" size="'.(($row->field_type == "multitext") ? '20' : $row->size).'" class="inputbox" /> ';
			}
			elseif ( $row->field_type == "selectlist" ) {
				$olist_translated_caption = explode("|",$row->field_elements);
				$olist = explode("|",$rows_elements[$row->name]->field_elements);
				$list[] = mosHTML::makeOption( " " );
				$j=0;
				foreach($olist AS $li) {
					$list[] = mosHTML::makeOption( trim($li),$olist_translated_caption[$j++] );
				}
				$fields[$i]->input = mosHTML::selectList( $list, $row->name, 'class="inputbox" size="1"',	'value', 'text', ' ');
			}
			elseif ( $row->field_type == "radiobutton" ) {
				$olist_translated_caption = explode("|",$row->field_elements);
				$olist = explode("|",$rows_elements[$row->name]->field_elements);
				$j=0;
				foreach($olist AS $li) {
					$fields[$i]->input .= '<input type="radio" name="'.$row->name.'" value="'.trim($li).'"> '.trim($olist_translated_caption[$j++]). "<br />";
				}
			}
			elseif ( $row->field_type == "checkbox" ) {
				$olist_translated_caption = explode("|",$row->field_elements);
				$olist = explode("|",$rows_elements[$row->name]->field_elements);
				$j=0;
				foreach($olist AS $li) {
					$fields[$i]->input .= "<input type='checkbox' name='".$row->name."[]' value='".trim($li)."'> ".trim($olist_translated_caption[$j++]). "<br />";
				}
			} elseif ( $row->field_type == "selectmultiple" ) {
				$olist_translated_caption = explode("|",$row->field_elements);
				$olist = explode("|",$rows_elements[$row->name]->field_elements);
				$fields[$i]->input .= "<select multiple name='".$row->name."[]' size='".$row->size."' class='inputbox'>";
				$j=0;
				foreach($olist AS $li) {
					$fields[$i]->input .= "<option value='".$li."' > ".trim($olist_translated_caption[$j++]). "</option>";
				}
				$fields[$i]->input .= "</select>";
			}

		}
		$i++;
	}

	hotproperty_HTML::show_AdvSearch($fields);
}

function emailForm() {
	global $database;

	$id = intval( mosGetParam( $_REQUEST, 'id', 0 ) );

	# Retrieve the Property title
	$database->setQuery("SELECT name FROM #__hp_properties WHERE id='$id' LIMIT 1");
	$title = $database->loadResult();

	hotproperty_HTML::sendEmailForm( $id, $title );

}

function emailSend() {
	global $database;
	global $mosConfig_live_site, $mosConfig_sitename, $mosConfig_sef, $option;

	josSpoofCheck(1);

	$id = intval( mosGetParam( $_REQUEST, 'id', 0 ) );
	$email = trim( mosGetParam( $_POST, 'email', '' ) );
	$yourname = trim( mosGetParam( $_POST, 'yourname', '' ) );
	$youremail = trim( mosGetParam( $_POST, 'youremail', '' ) );

	if ($mosConfig_sef) {
		$url = sefRelToAbs("index.php?option=com_hotproperty&task=view&id=$id");
	} else {
		// Get Itemid
		$database->setQuery("SELECT id FROM #__menu WHERE link='index.php?option=$option'");
		$Itemid = $database->loadResult();
		$url = $mosConfig_live_site .'/'. "index.php?option=com_hotproperty&task=view&id=$id&Itemid=$Itemid";
	}

	if (!$email || !$youremail || (is_email($email)==false) || (is_email($youremail)==false) ){
		echo "<script>alert (\""._EMAIL_ERR_NOINFO."\"); window.history.go(-1);</script>";
		exit(0);
	}

	$msg = sprintf( _EMAIL_MSG,	$mosConfig_sitename, $yourname,	$youremail,	$url);

	$recipient = $email;
	$subject = _EMAIL_INFO." $yourname";
	$headers .= "From: ".$yourname." <".$youremail.">\r\n";
	$headers .= "Reply-To: <".$youremail.">\r\n";
	$headers .= "X-Priority: 3\r\n";
	$headers .= "X-MSMail-Priority: Low\r\n";
	$headers .= "X-Mailer: Hot Property\r\n\r\n";
	mosMail($youremail,$yourname,$recipient,$subject,$msg);

	hotproperty_HTML::emailSent( $email );
}

function newproperties() {
	global $database;
		
	$where = array();
	$database->setQuery('select id from #__hp_properties where published=1 and approved=1 ORDER BY created DESC LIMIT 15');
	$ids = $database->loadResultArray();
	//var_dump($ids);
	$where[] = 'p.id IN ('.implode(',',$ids).')';
	
	//$pageNav = new mosPageNav( $total, $limitstart, $limit );
	
	HP_getProperty('listing', $where, $prop, $caption, 0, 15, 'created','ASC');
	
	hotproperty_HTML::showNewProperties( $prop, $caption );
}

function listAllProperties() {
	global $database;
	
	$query = "select prop.id from #__hp_properties as prop, #__hp_properties2 as ef
			where published = 1 
			and (prop.publish_up<=NOW()) 
			and (prop.publish_down >= NOW() || prop.publish_down = '0000-00-00 00:00:00') 
			and prop.approved = 1";
	
	if(mosGetParam($_REQUEST, 'county', '') != '')
		$query .= "	and ef.property = prop.id and ef.field = '53' and ef.value = '".mosGetParam($_REQUEST, 'county', '')."'";
	
	$database->setQuery($query);
	$ids = $database->loadResultArray();
	
	$where[] = 'p.id IN ('.implode(',',$ids).')';

	HP_getProperty('listing', $where, $prop, $caption, 0, 99999, 'name', 'ASC');
	
	$properties = array();
	foreach($prop as $property) {
		//Region exists in array
		
		if (array_key_exists($property->Region, $properties)) {
			//Process towns
			if (array_key_exists($property->Town, $properties[$property->Region])) {
				$properties[$property->Region][$property->Town][] = $property;
			} else {
				$properties[$property->Region][$property->Town] = array();
				$properties[$property->Region][$property->Town][] = $property;
			}
		}else{
			//Create region array
			$properties[$property->Region] = array();
			//Now process towns
			if (array_key_exists($property->Town, $properties[$property->Region])) {
				$properties[$property->Region][$property->Town][] = $property;
			} else {
				$properties[$property->Region][$property->Town] = array();
				$properties[$property->Region][$property->Town][] = $property;
			}
		}
	}
	foreach($properties as &$prop){
		ksort($prop);
	}
	
	hotproperty_HTML::listAllProperties($properties,$caption);
}


function showpdf() {
	$id = intval( mosGetParam( $_REQUEST, 'id', 0 ) );
	include ("components/com_hotproperty/includes/pdf.php");
	exit();
}


function processWishlist($id){
	global $database, $my;
	
	$func = mosGetParam($_REQUEST,'func','');
	$ajax = mosGetParam($_REQUEST,'no_html',0);
	
	
	if($func == 'add' || $func == 'del'){	
		if($func == 'add'){
			//SQL to add to wishlish
			$query="INSERT IGNORE INTO #__hp_wishlist (`prop_id`,`user_id`) VALUES ('$id','$my->id')";
			$database->setQuery($query);
			if($database->query()){					
				if($ajax) echo 1;			
				else mosRedirect($_SERVER,'HTTP_REFERER','index.php?option=com_hotproperty','The property has been successfully added to your wishlist');
			}else{
				echo $database->getErrorMsg();
			}			
		}else{
			//SQL to remove from wish list
			$query="DELETE FROM #__hp_wishlist WHERE prop_id='$id' AND user_id='$my->id'";
			$database->setQuery($query);
			if($database->query()){
				if($ajax) echo 0;
				else mosRedirect($_SERVER,'HTTP_REFERER','index.php?option=com_hotproperty','The property has been successfully removed from your wishlist');
			}else{
				echo $database->getErrorMsg();
			}			
		}	
	}else{
		if($ajax) echo 'No function specified';
		else mosRedirect($_SERVER,'HTTP_REFERER','index.php?option=com_hotproperty','No function specified');
	}
	
	
}

class mosHTML_hp {

	function rangeDateSelectList( $tag_name, $tag_attribs, $selected, $from, $to ) {
		$search_created_yyyy = array();
		$search_end_yyyy = array();
		$search_mm = array();
		$search_dd = array();

		$result = '<input type="radio" name="'.$tag_name.'" value="0" checked /> '._HP_ANYTIME. "<br />";
		$result .= '<input type="radio" name="'.$tag_name.'" value="1"> ';

		# Generate Generic Days drop down
		for($j=1 ; $j<=31 ; $j++) {
			$search_dd[] = mosHTML::makeOption( $j );
		}

		# Generate Start's YYYY drop down
		for($j=date('Y',$from) ; $j<=date('Y',$to) ; $j++) {
			$search_created_yyyy[] = mosHTML::makeOption( $j );
		}
		$result .= mosHTML::selectList( $search_created_yyyy, $tag_name."_from_y", 'class="inputbox" size="1"',	'value', 'text', date('Y',$from));
		# Generate Start's MM drop down
		$result .= mosHTML::monthSelectList( $tag_name."_from_m", 'class="inputbox" size="1"',	date('m',$from));
		# Generate Start's DD drop down
		$result .= mosHTML::selectList( $search_dd, $tag_name."_from_d", 'class="inputbox" size="1"',	'value', 'text', date('j',$from));

		$result .= _HP_SEARCH_TO;

		# Generate End's YYYY drop down
		for($j=date('Y',$to) ; $j<=date('Y',$to) ; $j++) {
			$search_end_yyyy[] = mosHTML::makeOption( $j );
		}
		$result .= mosHTML::selectList( $search_end_yyyy, $tag_name."_to_y", 'class="inputbox" size="1"',	'value', 'text', date('Y',$to));
		# Generate End's MM drop down
		$result .= mosHTML::monthSelectList( $tag_name."_to_m", 'class="inputbox" size="1"',	date('m',$to));
		# Generate End's DD drop down
		$result .= mosHTML::selectList( $search_dd, $tag_name."_to_d", 'class="inputbox" size="1"',	'value', 'text', date('j',$to));

		return $result;
	}

	function rangeText( $tag_name, $tag_attribs ) {
		$result = _HP_SEARCH_FROM;
		$result .= ' <input type="text" name="'.$tag_name.'_from"'.(($tag_attribs) ? " ".$tag_attribs : "").' />';
		$result .= _HP_SEARCH_TO;
		$result .= ' <input type="text" name="'.$tag_name.'_to"'.(($tag_attribs) ? " ".$tag_attribs : "").' />';

		return $result;
	}
	
	
}
?>