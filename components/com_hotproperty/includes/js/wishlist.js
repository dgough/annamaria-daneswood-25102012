window.addEvent('domready', function() {
	
	$$('a.wish_list_bttn_add','a.wish_list_bttn_del','a.fav_tab_add','a.fav_tab_del').addEvent('click', function(e) {
		e = new Event(e);
		var link = this;
		var myurl = this.href;
		
		myurl = myurl.replace('index.php','index2.php');
		switch(this.className) {
			case 'wish_list_bttn_add':
			case 'fav_tab_add':
				myurl = myurl.replace('func=del','func=add');
				break;
			case 'wish_list_bttn_del':
			case 'fav_tab_del':
				myurl = myurl.replace('func=add','func=del');			
				break;
		}	
		myurl = myurl + '&no_html=1';

		new Ajax(myurl,{
			method: 'get', 
			onRequest: function() {
				if (link.className=='wish_list_bttn_add' || link.className=='wish_list_bttn_del') link.setProperty('class','wish_list_bttn_load');
				if (link.className=='fav_tab_add' || link.className=='fav_tab_del') link.setProperty('class','fav_tab_load');
			},
			onComplete: function(responseText) {
				if (responseText==1) {
					if (link.className=='wish_list_bttn_load') link.setProperty('class','wish_list_bttn_del');
					if (link.className=='fav_tab_load')	link.setProperty('class','fav_tab_del');
					link.setText('Remove from Shortlist');
					
				} else if (responseText==0) {
					if (link.className=='wish_list_bttn_load') link.setProperty('class','wish_list_bttn_add');
					if (link.className=='fav_tab_load') link.setProperty('class','fav_tab_add');
					link.setText('Add to Shortlist');
				} else {
					link.remove();
				}
			},
			onFailure: function() {
				link.remove();
			}
		}).request();
		e.stop();
	});
	
});