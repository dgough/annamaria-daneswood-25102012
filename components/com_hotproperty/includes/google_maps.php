<?php	

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

function getMap($p, $zoomControl = 0, $typeContol = 0, $width = 0, $height = 0){

	$key = 'AIzaSyBkeiZ_cZ7qb_hluB_Go1hf0zyECOQk7Rk';
	$http = $_SERVER['HTTPS'] == 'on' ? 'https' : 'http';
	if($p->longitude && $p->latitude){ ?>
	
	    <div id='hp_map' style="min-height: 1px"></div>
	    <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=<?php echo $key ?>"  type="text/javascript"></script>
		<script type="text/javascript">
		//<![CDATA[
		try{

		    var building = '<?php echo $p->name ?>';
			var address = '<?php echo $p->address ?>';
			var town = '<?php echo $p->suburb ?>';
			var postcode = '<?php echo $p->postcode ?>';
			var latitude = <?php echo $p->latitude ?>;
			var longitude = <?php echo $p->longitude ?>;

			var iconRed = new GIcon();
			iconRed.image = '<?php echo $http ?>://labs.google.com/ridefinder/images/mm_20_red.png';
			iconRed.shadow = '<?php echo $http ?>://labs.google.com/ridefinder/images/mm_20_shadow.png';
			iconRed.iconSize = new GSize(12, 20);
			iconRed.shadowSize = new GSize(22, 20);
			iconRed.iconAnchor = new GPoint(6, 20);
			iconRed.infoWindowAnchor = new GPoint(5, 1);
			var map;
			var point;

			function loadMap() {
				if(map || $('hp_map').getSize().size.y == 0) return;

				  if (GBrowserIsCompatible()) {
					map = new GMap2(document.getElementById("hp_map"));
					point = new GLatLng(latitude,longitude);
					var marker = createMarker(point, building, address, town, postcode);

					map.setCenter(point, 14);
					<?php
					switch($zoomControl){

						case 1:
							echo "map.addControl(new GLargeMapControl());";
							break;

						case 2:
							echo "map.addControl(new GSmallMapControl());";
							break;

						case 3:
							echo "map.addControl(new GSmallZoomControl());";
							break;

					}
					switch($typeContol){

						case 1:
							echo "map.addControl(new GMapTypeControl());";
							break;
					}
					?>
					map.addOverlay(marker);
				  }
			}

			function createMarker(point, building, address, town, postcode) {
			  var marker = new GMarker(point, iconRed);
			  var html = "<b>" + building + "<\/b><br \/>" + address +  "<br \/>" + town + "<br \/>" + postcode;
			  GEvent.addListener(marker, 'click', function() {
				marker.openInfoWindowHtml(html);
			  });
			  return marker;
			}
			$$('h2.tab').each(function(tab){
				tab.addEvent('click',function(){
					loadMap()
					map.setCenter(point, 14);
					//map.checkResize();
				})
			})
			window.addEvent('domready',loadMap);
			window.addEvent('unload',GUnload);
		}catch(e){}
	    //]]>
	    </script>	                                      
	    <?php		
	}
}
?>