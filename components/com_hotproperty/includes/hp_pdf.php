<?php
	defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

	class HP_PDF extends FPDF {

		var $thumb_x;
		var $thumb_y;
		var $thumbPadding_top;
		var $thumbPadding_right;
		var $pagePadding_left;
		var $pagePadding_top;
		var $pagePadding_bottom;
		var $detailsXY;
		var $thumbsXY;
		var $defaultLineHeight;
		
		// Offsets
		var $thumbTitle_offset_y;
		var $details_offset_x;
		var $details_offset_y;
		var $standardPhoto_offset_y ;
		var $pdfThumb_newpage_offset_y;

		// Hot Property data
		var $hp_currency;
		var $hp_dec_point;
		var $hp_thousand_sep;
		var $hp_show_moreinfo;
		var $hp_view_features_title;
		var $hp_view_agent_title;
		var $hp_agent_company;
		var $hp_agent_mobile;
		var $hp_properties;

		// MOS Config var
		var $mosConfig_absolute_path;
		var $mosConfig_live_site;

		//variables of html parser
		var $B;
		var $I;
		var $U;
		var $HREF;
		var $fontList;
		var $issetfont;
		var $issetcolor;

		//Temporary variable to flag next execution Thumbnail generator to generate newpage
		var $tmp_newpage;

		function HP_PDF($orientation = 'P', $unit = 'mm', $format = 'A4') {

				# Initialization
				$this->thumb_x=$this->px2mm(0);
				$this->thumb_y=$this->px2mm(0);
				$this->thumbPadding_top = $this->px2mm(25);
				$this->thumbPadding_right = $this->px2mm(10);
				$this->pagePadding_left = $this->px2mm(30);
				$this->pagePadding_top = $this->px2mm(30);
				$this->pagePadding_bottom = $this->px2mm(30);
				$this->detailsXY = array();
				$this->thumbsXY = array();
				$this->defaultLineHeight = $this->px2mm(16);

				$this->thumbTitle_offset_y = $this->px2mm(0);
				$this->details_offset_x = $this->px2mm(10);
				$this->details_offset_y = $this->px2mm(27);
				$this->standardPhoto_offset_y = $this->px2mm(12);
				$this->pdfThumb_newpage_offset_y = 9;

				$this->B=0;
				$this->I=0;
				$this->U=0;
				$this->HREF='';
				$this->tableborder=0;
				$this->tdbegin=false;
				$this->tdwidth=0;
				$this->tdheight=0;
				$this->tdalign="L";
				$this->tdbgcolor=false;

				$this->oldx=0;
				$this->oldy=0;

				$this->fontlist=array("arial","times","courier","helvetica","symbol");
				$this->issetfont=false;
				$this->issetcolor=false;

				$this->tmp_newpage=false;

				# Call parent constructor
				$this->FPDF($orientation, $unit, $format);
				$this->setTopMargin($this->pagePadding_top);
				$this->setLeftMargin($this->pagePadding_left);
				$this->SetAutoPageBreak(1, $this->pagePadding_bottom);

		}

		/***************************************
		 * Page Header
	   ***************************************/
		function Header()
		{
				global $mosConfig_sitename, $mosConfig_live_site;

				$this->Line ($this->pagePadding_left ,10, ($this->px2mm($this->fwPt) - $this->pagePadding_left), 10 );

				$this->SetFont('Arial','',8);
				$this->SetX( $this->pagePadding_left );
				$this->Cell(0,-5,$mosConfig_sitename.' ('.$mosConfig_live_site.')');
				
				//Line break
				$this->Ln(8);
		}

		/***************************************
		 * Page Footer
	   ***************************************/
		function Footer()
		{
				$this->Line ($this->pagePadding_left ,($this->px2mm($this->fhPt) - $this->pagePadding_bottom), ($this->px2mm($this->fwPt) - $this->pagePadding_left), ($this->px2mm($this->fhPt) - $this->pagePadding_bottom) );
		}

		function printTitle($title) {
			$this->SetFont('Arial','',22);
			$this->Cell(0,0,$title);
			$this->SetY($this->pagePadding_top + $this->GetY());
		}

		function addStandardPhoto($image, $title, $desc) {
			$size = GetImageSize($image);
			$padding_left = $this->px2mm(($this->fwPt - $size[0])/2);
			$this->Image($image,$padding_left, $this->GetY());

			# Print Standard's photo title & desc
			$this->SetFont('Arial','B',12);
			$this->SetFillColor(192,192,192);

			$this->SetXY($padding_left, ($this->px2mm($size[1]) + $this->GetY() ));
			$this->Cell($this->px2mm($size[0]), $this->px2mm(18),$title, 0, 1, 'C', 1);

			if ($desc <> '') {
				$this->SetXY($padding_left, $this->GetY());
				$this->SetFont('Arial','',10);
				$this->MultiCell($this->px2mm($size[0]), $this->px2mm(14),$desc, 0, 'C', 1);
			}

			# Store value for thumb's and details' starting coordinates

			// Prepare Coordinate for Thumbnails
			$this->thumbsXY = array('x' => $this->pagePadding_left, 'y' => $this->GetY());

			// Prepare Coordinate for Details
			$this->detailsXY = array('x' => $this->pagePadding_left, 'y' => $this->GetY());

			// Prepare Y Coordinate for writeHTML
			$this->SetY($this->GetY() + $this->thumbPadding_top);

		}

		function addThumbPhoto($image, $title) {

			if ($this->tmp_newpage) {
				$this->AddPage();
				$this->tmp_newpage = false;
			}

			$size = GetImageSize($image);
			$this->thumbsXY['y'] += $this->thumbPadding_top;

			# Insert the thumbnail
			$this->Image($image, $this->thumbsXY['x'], $this->thumbsXY['y']);

			# Update $thumb_x / $thumb_y
			$this->thumbsXY['y'] += $this->px2mm($size[1]);
			
			# Print Thumbnail's title
			$this->SetXY($this->thumbsXY['x'],($this->thumbsXY['y'] + $this->thumbTitle_offset_y));
			$this->SetFont('Arial','B',12);
			$this->SetFillColor(192,192,192);
			$this->Cell($this->px2mm($size[0]),$this->px2mm(18),$title,0,0,'C',1);

			# These thumbnails are on the left side. Move 'Details' text to right
			# This condition will ensure it has enough space for the widest thumbnail
			if ( $this->detailsXY['x'] < ($this->thumbsXY['x'] + $this->px2mm($size[0])  + $this->thumbPadding_right) ) {
				$this->detailsXY['x'] = $this->thumbsXY['x'] + $this->px2mm($size[0])  + $this->thumbPadding_right;
			}

			$this->SetXY( $this->detailsXY['x'], ($this->detailsXY['y'] + $this->thumbPadding_top) );
			$this->setLeftMargin($this->detailsXY['x']);

			# Add new page if this thumbnail will overflow the page
			if ( ($this->thumbsXY['y'] + $this->px2mm($size[1]) + $this->pdfThumb_newpage_offset_y) > ($this->px2mm($this->fhPt)- $this->pagePadding_bottom) ){
				$this->tmp_newpage = true;
				$this->thumbsXY['y'] = $this->pagePadding_top;
			}
		}

		/***************************************
		 * Write 'Details'
	   ***************************************/
		function writeDetails($text) {
			$this->writeHTML( $text );
		}

		/***************************************
		 * Write 'Caption: Value'
	   ***************************************/
		function writeCaptionValue( $hideCaption, $caption, $value ) {
			if ($hideCaption) {
				$this->writeDetails($value);
			} else {
				$this->writeDetails('<b>'.$caption .'</b>: '. $value);
			}
		}

		/***************************************
		 * Print Agent Details
	   ***************************************/
		function printAgent($agent) {
			$this->Ln();
			$this->writeDetails( '<u><b>'.$agent->name.'</b></u>'); 
			$this->Ln();
			$this->writeDetails('<b>'.$this->hp_agent_company.'</b>: '.$agent->company);
			$this->Ln();
			$this->writeDetails( '<b>'.$this->hp_agent_mobile.'</b>: '.$agent->mobile );
			$this->Ln();
			$this->writeDetails( '<b>'.$this->hp_properties.'</b>: '.$agent->properties );
			$this->Ln();
		}

		/***************************************
		 * Big huge function to print details
	   ***************************************/
		function printDetails($prop, $caption) {
			$p = $prop[0];
			$this->page = 1; // This will ensure details always appear in Page 1.

			# Perform the offset
			$this->detailsXY['y'] += $this->details_offset_y;

			# Write the details!
			foreach($p as $key => $value) {
				if ( array_key_exists($key,$caption) && $key <> 'name' && $key <> 'suburb' && $key <> 'state' && $key <> 'postcode' && $key <> 'country' &&	$caption[$key]->iscore == 1 ) { 
					if ($key=="address") {

						if (!$caption['address']->hideCaption) { 
							$this->writeDetails( '<b>'.$caption['address']->caption . '</b>:' );
							$this->Ln();
						} 
						
						if (trim($p->address)!="") {
							$this->writeDetails( $p->address );
							$this->Ln();
						}
						
						if ((trim($p->suburb)!="") && (trim($p->state)!="") && (trim($p->postcode)!="")) {
							$this->writeDetails( $p->suburb .', '. $p->state .', '. $p->postcode );
							$this->Ln();
						} elseif ((trim($p->suburb)!="") && (trim($p->state)!="")) {
							$this->writeDetails( $p->suburb .', '. $p->state );
							$this->Ln();
						} elseif ((trim($p->suburb)!="") && (trim($p->postcode)!="")) {
							$this->writeDetails( $p->suburb .', '. $p->postcode );
							$this->Ln();
						} elseif ((trim($p->state)!="") && (trim($p->postcode)!="")) {
							$this->writeDetails( $p->state .', '. $p->postcode );
							$this->Ln();
						} elseif ((trim($p->state)!="")) {
							$this->writeDetails( $p->state );
							$this->Ln();
						} elseif ((trim($p->suburb)!="")) {
							$this->writeDetails( $p->suburb );
							$this->Ln();
						} elseif ((trim($p->postcode)!="")) {
							$this->writeDetails( $p->postcode );
							$this->Ln();
						}
						if (trim($p->country)!="") {
							$this->writeDetails( $p->country );
							$this->Ln();
						}
						$this->Ln();
					} // End if (address)

					if ($key=="price" && isset($caption['price'])) { 
						$this->writeCaptionValue( $caption['price']->hideCaption, $caption[$key]->caption, ( $caption["price"]->prefix_text.$this->hp_currency." ".number_format($p->price, $this->hp_dec_point, ".", ($this->hp_thousand_sep) ? ',':'').$caption["price"]->append_text ) );
						$this->Ln();
					}

					elseif ($key=="type" && isset($caption['type'])) { 
						$this->writeCaptionValue( $caption['type']->hideCaption, $caption[$key]->caption, $p->type );
						$this->Ln();
					}

					elseif ($key=="intro_text" && $p->intro_text) { 
						$this->writeCaptionValue( $caption['intro_text']->hideCaption, $caption[$key]->caption, $p->intro_text );
						$this->Ln();
						$this->Ln();
					}
						
					elseif ($key=="full_text" && $p->full_text) {
						$this->writeCaptionValue( $caption['full_text']->hideCaption, $caption[$key]->caption, $p->full_text );
						$this->Ln();
						$this->Ln();
					}

					elseif ($key <> "address") {
						$this->writeCaptionValue( $caption[$key]->hideCaption, $caption[$key]->caption, $value );
						$this->Ln();
					}

				} // End if
			} // End foreach

			if ($this->hp_show_moreinfo) {
				$this->Ln();
				$this->writeDetails( '<u><b>'.$this->hp_view_features_title.'</b></u>'); 
				$this->Ln();
			}

			foreach($p as $key => $value) {
				if ( array_key_exists($key,$caption) && $caption[$key]->name <> 'name' && $caption[$key]->name <> 'agent' && $caption[$key]->name <> 'address' && $caption[$key]->name <> 'suburb' && $caption[$key]->name <> 'state' && $caption[$key]->name <> 'postcode' && $caption[$key]->name <> 'country' && $caption[$key]->name <> 'price' && $caption[$key]->name <> 'type' && $caption[$key]->name <> 'intro_text' && $caption[$key]->name <> 'full_text' && $caption[$key]->name <> 'thumb' && $caption[$key]->name <> 'thumb_title' && $caption[$key]->name <> '' && $value <> "" && $caption[$key]->iscore == 0) {

					if ($caption[$key]->field_type == "checkbox" || $caption[$key]->field_type == "selectmultiple") {
						if (!$caption[$key]->hideCaption) {
							$this->writeDetails( '<b>'.$caption[$key]->caption.':</b>');
							$this->Ln();
						}
						# Generate list
						$olist = explode("|",$value);
						foreach ($olist AS $ol) {
							if ($ol <> '') {
								$this->writeDetails( ' � '.$ol );
								$this->Ln();
							}
						}
					} 
					elseif($caption[$key]->field_type == "link") {
						$link = explode("|",$value);
						if (count($link) == 1 && substr(trim($link[0]),0,4) == "http") {
							$this->writeCaptionValue( $caption[$key]->hideCaption, $caption[$key]->caption, $caption[$key]->prefix_text.'<A HREF="'.$link[0].'">'.$link[0].'</A>'.$caption[$key]->append_text);
						} elseif (count($link) > 1 && substr(trim($link[1]),0,4) == "http") {
							$this->writeCaptionValue( $caption[$key]->hideCaption, $caption[$key]->caption, $caption[$key]->prefix_text.'<A HREF="'.$link[1].'">'.$link[0].'</A>'.$caption[$key]->append_text);
						}
						$this->Ln();
					} else {
						$this->writeCaptionValue( $caption[$key]->hideCaption, $caption[$key]->caption, $caption[$key]->prefix_text.$value.$caption[$key]->append_text );
						$this->Ln();
					}

				} // End if
			} // End foreach

		}

		###########################################
		# Based on HTML2PDF by Cl�ment Lavoillotte
		###########################################
		 
		function WriteHTML($html) {
				//Messey but needed
				 //global $_LANG;
				 
				$html = str_replace("<li>", "<BR>-", $html);
				$html = str_replace("<LI>", "<BR>-", $html);
				$html = str_replace("</LI>", "<BR>", $html);
				$html = str_replace("</li>", "<BR>", $html);
				$html = str_replace("</div>", "<BR>", $html);
				$html = str_replace("</DIV>", "<BR>", $html);
				$html = str_replace("<a href='javascript:window.close();'>Close Window</a>", "<BR>", $html);
				 
				$html = strip_tags($html, "<b><u><i><a><img><p><br><strong><em><font><table><tr><td><blockquote>"); //remove all unsupported tags
				$html = str_replace("\n", '', $html); 
		 
				$a = preg_split('/<(.*)>/U', $html, -1, PREG_SPLIT_DELIM_CAPTURE); //explodes the string
				foreach($a as $i => $e) {
						if ($i%2 == 0) {
								//Text
								if (isset($this->inTable) && $this->inTable) {
										if ($this->inCell) {
												$this->Cell(40, 6, $this->txtentities($e), 1);
										}
								} else {
										if ($this->HREF) {
												$this->PutLink($this->HREF, $e);
										} else {
												$this->Write(5, stripslashes($this->txtentities($e)));
										}
								}
						} else {
								//Tag
								if ($e {
										0 }
								== '/')
								$this->CloseTag(strtoupper(substr($e, 1)));
								else
										{
										//Extract attributes
										$a2 = explode(' ', $e);
										$tag = strtoupper(array_shift($a2));
										$attr = array();
										foreach($a2 as $v)
										if (ereg('^([^=]*)=["\']?([^"\']*)["\']?$', $v, $a3))
										$attr[strtoupper($a3[1])] = $a3[2];
										$this->OpenTag($tag, $attr);
								}
						}
				}
		}
		 
		function OpenTag($tag, $attr) {
				//  print_r ($tag);
				//Opening tag
				switch($tag) {
						case 'STRONG':
						$this->SetStyle('B', true);
						break;
						case 'EM':
						$this->SetStyle('I', true);
						break;
						case 'B':
						case 'I':
						case 'U':
						$this->SetStyle($tag, true);
						break;
						case 'A':
						$this->HREF = $attr['HREF'];
						break;
						case 'IMG':
//						if (isset($attr['SRC']) and (isset($attr['WIDTH']) or isset($attr['HEIGHT']))) {
								if (!isset($attr['WIDTH']))
									$attr['WIDTH'] = 0;
								if (!isset($attr['HEIGHT']))
									$attr['HEIGHT'] = 0;
								$this->ln(7);
								if ($attr['WIDTH'] == 0 && $attr['HEIGHT'] == 0) {
									$this->Image($attr['SRC'], $this->GetX(), $this->GetY(), $this->px2mm($attr['WIDTH']), $this->px2mm($attr['HEIGHT']));
								} else {
									# 
									if (substr($attr['SRC'], 0, strlen($this->mosConfig_live_site)) == $this->mosConfig_live_site) {
										$attr['SRC'] = 	str_replace( $this->mosConfig_live_site, $this->mosConfig_absolute_path, $attr['SRC']);
									}
									$this->Image($attr['SRC'], $this->GetX(), $this->GetY());
								}
								$xy = (getImageSize ($attr['SRC']));
								$height = explode ("=", $xy[3]);
								$moveY = str_replace("\"", "", $height[2]);
								$this->ln($this->px2mm($moveY)+3); // Cant work out how to wrap - so just put text under
//						}
						break;
						case 'TABLE':
						$this->inTable = 1;
						break;
						case 'TR':
						$this->Ln(6);
						break;
						case 'TD':
						$this->inCell = 1;
						break;
						case 'BLOCKQUOTE':
						case 'BR':
						$this->Ln(5);
						break;
						case 'P':
						$this->Ln(10);
						break;
						case 'FONT':
						if (isset($attr['COLOR']) and $attr['COLOR'] != '') {
								$coul = $this->hex2dec($attr['COLOR']);
								$this->SetTextColor($coul['R'], $coul['G'], $coul['B']);
								$this->issetcolor = true;
						}
						if (isset($attr['FACE']) and in_array(strtolower($attr['FACE']), $this->fontlist)) {
								$this->SetFont(strtolower($attr['FACE']));
								$this->issetfont = true;
						}
						break;
				}
		}
		 
		function CloseTag($tag) {
				//Closing tag
				if ($tag == 'STRONG')
				$tag = 'B';
				if ($tag == 'EM')
				$tag = 'I';
				if ($tag == 'B' or $tag == 'I' or $tag == 'U')
				$this->SetStyle($tag, false);
				if ($tag == 'A')
				$this->HREF = '';
				if ($tag == 'FONT') {
						if ($this->issetcolor == true) {
								$this->SetTextColor(0);
						}
						if ($this->issetfont) {
								$this->SetFont('arial');
								$this->issetfont = false;
						}
				}
				switch ($tag) {
						case 'TABLE':
						$this->inTable = 0;
						$this->Ln(6);
						break;
						case 'TD':
						$this->inCell = 0;
						break;
				}
		}
		 
		function SetStyle($tag, $enable) {
				//Modify style and select corresponding font
				$this->$tag += ($enable ? 1 : -1);
				$style = '';
				foreach(array('B', 'I', 'U') as $s)
				if ($this->$s > 0)
				$style .= $s;
				$this->SetFont('', $style);
		}
		 
		function PutLink($URL, $txt) {
				//Put a hyperlink
				$this->SetTextColor(0, 0, 255);
				$this->SetStyle('U', true);
				$this->Write(5, $txt, $URL);
				$this->SetStyle('U', false);
				$this->SetTextColor(0);
		}

    function txtentities($html) {
        $trans = get_html_translation_table(HTML_ENTITIES);
        $trans = array_flip($trans);
        return strtr($html, $trans);
    }

		function px2mm($px) {
        return $px * 25.4/72;
    }
		
		function hex2dec($hex) {
			$color = str_replace('#', '', $hex);
			$ret = array(
			 'R' => hexdec(substr($color, 0, 2)),
			 'G' => hexdec(substr($color, 2, 2)),
			 'B' => hexdec(substr($color, 4, 2))
			);
			return $ret;
		}

		###################################################################
		# Jan Slabon <jan@traum-projekt.com>
		# The _beginpage() method needs to be overriden to solve a problem 
		# that occurs when an automatic page break for one page is reached 
		# more than one time.
		###################################################################

		function _beginpage($orientation) {
				$this->page++;
				if(array_key_exists($this->page,$this->pages) && !$this->pages[$this->page]) // solved the problem of overwriting a page, if it already exists
						$this->pages[$this->page]='';
				$this->state=2;
				$this->x=$this->lMargin;
				$this->y=$this->tMargin;
				$this->lasth=0;
				$this->FontFamily='';
				//Page orientation
				if(!$orientation)
						$orientation=$this->DefOrientation;
				else
				{
						$orientation=strtoupper($orientation{0});
						if($orientation!=$this->DefOrientation)
								$this->OrientationChanges[$this->page]=true;
				}
				if($orientation!=$this->CurOrientation)
				{
						//Change orientation
						if($orientation=='P')
						{
								$this->wPt=$this->fwPt;
								$this->hPt=$this->fhPt;
								$this->w=$this->fw;
								$this->h=$this->fh;
						}
						else
						{
								$this->wPt=$this->fhPt;
								$this->hPt=$this->fwPt;
								$this->w=$this->fh;
								$this->h=$this->fw;
						}
						$this->PageBreakTrigger=$this->h-$this->bMargin;
						$this->CurOrientation=$orientation;
				}
		}

	}


?>