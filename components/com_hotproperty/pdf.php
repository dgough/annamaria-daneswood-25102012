<?php
	/**
	* Hot Property
	*
	* @package Hot Property 0.9
	* @copyright (C) 2004 Lee Cher Yeong
	* @url http://www.Mosets.com/
	* @author Lee Cher Yeong <hotproperty@mosets.com>
	**/

	define( "_VALID_MOS", 1 );

	include_once( '../../globals.php' );
	require_once( "../../configuration.php" );
	require_once( "../../includes/mambo.php" );
	require_once( "../../includes/frontend.php" );

	define('FPDF_FONTPATH', $mosConfig_absolute_path . "/components/com_hotproperty/includes/fpdf/font/");
	require($mosConfig_absolute_path."/components/com_hotproperty/includes/fpdf/fpdf.php" );
	require($mosConfig_absolute_path."/components/com_hotproperty/includes/hp_pdf.php" );
	include($mosConfig_absolute_path."/components/com_hotproperty/functions.hotproperty.php" );
	include($mosConfig_absolute_path."/administrator/components/com_hotproperty/config.hotproperty.php" );

	# Include the language file. Default is English
	if ($hp_language=='') {
		$hp_language='english';
	}
	include_once($mosConfig_absolute_path.'/components/com_hotproperty/language/'.$hp_language.'.php');

	if ( $mosConfig_lang == '' ) {
		$mosConfig_lang = 'english';
	}
	include_once ( $mosConfig_absolute_path.'/language/'.$mosConfig_lang.'.php' );

	$database = new database( $mosConfig_host, $mosConfig_user, $mosConfig_password, $mosConfig_db, $mosConfig_dbprefix );

	$id = intval( trim( mosGetParam( $_REQUEST, 'id', 0 ) ) );

	# Get All photos
	$database->setQuery("SELECT * FROM #__hp_photos"
		.	"\nWHERE property='".$id."'"
		.	"\nORDER BY ordering ASC");
	$images = $database->loadObjectList();

	# Retrieve property's details
	$where[] = "p.id = '".$id."'";
	HP_getProperty('all', $where, $prop, $caption, 0, 1, 'name', 'ASC');

	/*
	 * Starts generating PDF file
	 */
	$pdf=new HP_PDF('P','mm','A4');
	$pdf->AddPage();
	$pdf->setTitle($prop[0]->name);
	$pdf->setSubject($prop[0]->type);
	$pdf->setAuthor($prop[0]->agent);

	# Assign Hot Property data
	$pdf->hp_currency = html_entity_decode($hp_currency);
	$pdf->hp_dec_point = $hp_dec_point;
	$pdf->hp_thousand_sep = $hp_thousand_sep;
	$pdf->hp_show_moreinfo = $hp_show_moreinfo;
	$pdf->hp_view_features_title = _HP_VIEW_FEATURES_TITLE;
	$pdf->hp_view_agent_title = _HP_VIEW_AGENT_TITLE;

	$pdf->hp_view_agent_title = _HP_VIEW_AGENT_TITLE;
	$pdf->hp_agent_company = _HP_AGENT_COMPANY;
	$pdf->hp_agent_mobile = _HP_AGENT_MOBILE;
	$pdf->hp_properties = _HP_PROPERTIES;

	# Assign MOS Config vars
	$pdf->mosConfig_absolute_path = $mosConfig_absolute_path;
	$pdf->mosConfig_live_site = $mosConfig_live_site;

	# Print Name of the property
	$pdf->printTitle($prop[0]->name);

	# Attach standard photo
	if ($images[0]->standard <> '') { 
		$pdf->addStandardPhoto($mosConfig_absolute_path.$hp_imgdir_standard.$images[0]->standard,$images[0]->title, $images[0]->desc);
	}

	# Attach thumbnails, not including standard's thumbnail (above)
	if (count($images) > 1) { 
		foreach($images AS $image) {
			// Prevent repeating Standard's photo
			if ($images[0]->id <> $image->id && $image->thumb <> '' ) {
				$pdf->addThumbPhoto($mosConfig_absolute_path.$hp_imgdir_thumb.$image->thumb, $image->title);
			}
		}
	}
	
	#
	$prop[0]->featured = ( ($prop->featured == '1') ? _CMN_YES : _CMN_NO );
	# Print details now.
	$pdf->SetFont('Arial','',12);
	$pdf->printDetails($prop, $caption);

	# Print Agent details 
	if ($hp_show_agentdetails && $hp_use_companyagent) {
		$sql = "SELECT a.*, COUNT(p.id) AS properties, c.id AS companyid, c.name AS company FROM #__hp_agents AS a, #__hp_prop_types AS t "
		.	"\nLEFT JOIN #__hp_companies AS c ON c.id=a.company"
		.	"\nLEFT JOIN #__hp_properties AS p ON p.agent=a.id"
		.	"\nWHERE a.id='".$prop[0]->agentid."'"
		.	"\n AND t.id=p.type"
		. "\n AND p.published='1' AND p.approved='1' AND t.published='1'"
		. "\n	AND (publish_up = '0000-00-00 00:00:00' OR publish_up <= NOW())"
		. "\n	AND (publish_down = '0000-00-00 00:00:00' OR publish_down >= NOW())"
		. "\nGROUP BY p.agent"
		.	"\nLIMIT 1";
		$database->setQuery($sql);
		$database->loadObject($agent);
		$pdf->printAgent($agent);
	}

	$pdf->Output("mambo.pdf","I");

?>