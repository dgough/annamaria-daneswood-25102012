<?php
// hotproperty.html.php
//error_reporting(E_ALL);
//ini_set('display_errors','On');
/**
* Hot Property's Presentation Code
*
* @package Hot Property 0.9
* @copyright (C) 2004 Lee Cher Yeong
* @url http://www.Mosets.com/
* @author Lee Cher Yeong <cy@mosets.com>
**/

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

class hotproperty_HTML {

	/***
	 * Include CSS file
	 ***/
	function include_CSS() {
		global $hp_css;
		?>
   		<link rel="stylesheet" href="components/com_hotproperty/css/<?php echo $hp_css; ?>.css" type="text/css">
		<?php
	}

	function include_Container_Start() { ?>
		<div id="con_global">
		<?php /* <h1 class="componentheading"><?php echo _HP_COM_TITLE; ?></h1> */ ?>
	<?php
	}

	function include_Container_End() { ?>
		</div>
	<?php
	}

	/***
	 * Front Page
	 ***/
	function show_Frontpage(&$featured, &$featured_fields_caption, &$featured_total, &$types, &$types_hotproperty) {
		global $hp_fp_show_featured, $hp_fp_show_search, $mainframe;

		$mainframe->setPageTitle( _HP_COM_TITLE );

		if ($hp_fp_show_featured && count($featured) > 0 ){
			hotproperty_HTML::show_fp_Featured($featured, $featured_fields_caption, $featured_total);
			echo '<br class="clearboth" />';
		}

		if ($hp_fp_show_search) {
			hotproperty_HTML::show_Search($types);
			echo '<br class="clearboth" />';
		}

		//hotproperty_HTML::show_Types($types, $types_hotproperty);
	}

	/***
	 * Featured Listing
	 ***/
	function show_fp_Featured(&$prop, &$caption, $featured_total) {
		global $hp_fp_featured_count, $Itemid;
	?>
	<div id="con_featured1">
	<div id="heading_Featured"><?php //echo _HP_FEATURED_TITLE; ?></div>
		<div id="list_featured">
		<?php 	hotproperty_HTML::list_properties($prop, $caption); ?>
		<div class="clearboth" ></div>
		<?php
		if ($featured_total > $hp_fp_featured_count) {
			echo "<a href=\"". sefRelToAbs("index.php?option=com_hotproperty&task=viewfeatured&Itemid=$Itemid") ."\">"._HP_MOREFEATURED."</a>";
		} ?>
		</div>
	</div>
	<?php
	}

	/***
	 * List Types
	 ***/
	function show_Types(&$types, &$types_hotproperty) {
		global $Itemid, $database, $hp_fp_depth_count;
	?>
	<div id="con_types1">
	<div id="heading_Types"><?php echo _HP_TYPES_TITLE; ?></div>
		<div id="con_types2">
		<?php
			$children = array();
			foreach ($types as $type) {
				// Build the hierarchy
				$pt	= $type->parent;
				$list = @$children[$pt] ? $children[$pt] : array();
				array_push ($list, $type);
				$children[$pt] = $list;
			}

			# Fonction récursive qui trace une liste(limitée si levelmax) imbriquée des catégories:
			hotproperty_HTML::treeHTML (0, $children, $types_hotproperty, $maxlevel=$hp_fp_depth_count);
		?>
		</div>
	</div>
	<?php
	}

	/***
	 * Search Facility
	 ***/
	function show_Search(&$types) {
		global $Itemid, $hp_use_advsearch, $mosConfig_live_site;
		/*
		global $custom404, $mosConfig_sef, $sufix;

		# Using Built in SEF feature in Mambo
		if ( !isset($custom404) && $mosConfig_sef ) {
			$onclickCmd = "document.location.href= '$mosConfig_live_site/component/option,com_hotproperty/task,search/Itemid,$Itemid/type,0/search,' + encodeURI(document.searchfrm.hp_search.value) + '/'";
		} elseif ( $mosConfig_sef && isset($custom404) && !empty($sufix) ) {

			global $custom_comp, $hp_default_limit_search;
			$hotproperty = "hotproperty";
			if (in_array("hotproperty", $custom_comp)) {
				$hotproperty = array_search($component_name, $custom_comp);
			}

			$onclickCmd = "document.location.href='" . $hotproperty . "/" . _HP_SEF_SEARCH . "/0/".$hp_default_limit_search."/0/" . "' + encodeURI(document.searchfrm.hp_search.value)";

		} else {
			# Using SEF advance or no SEF at all
			$onclickCmd = "document.location.href='" . sefRelToAbs("index.php?option=com_hotproperty&task=search&Itemid=$Itemid&type=0&search=' + encodeURI(document.searchfrm.hp_search.value)");
		}
		*/
	?>
	<div id="con_search1">
	<div id="heading_Search"><?php echo _HP_SEARCH_TITLE; ?></div>
		<div id="con_search2">
			<form action="index.php" method="POST" name="searchfrm">
			<strong><?php echo _HP_SEARCH_TEXT; ?></strong>
			<input type="text" name="search" class="inputbox" />
			<input type="submit" class="button" value="<?php echo _HP_SEARCH; ?>" /><?php
			if ($hp_use_advsearch == '1') {
			?>&nbsp;<a href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&task=advsearch&Itemid=$Itemid"); ?>"><?php echo _HP_SEARCH_ADV_TITLE; ?></a><?php
			}
			?>
			<input type="hidden" name="option" value="com_hotproperty" />
			<input type="hidden" name="task" value="search" />
			<input type="hidden" name="Itemid" value="<?php echo $Itemid;?>" />
			</form>
		</div>
	</div>
	<?php
	}

	/***
	 * Advanced Search Facility
	 ***/
	function show_AdvSearch($fields) {
		global $Itemid, $mainframe;

		/*echo "<pre>";
		echo "fields=";
		print_r($fields);
		echo "</pre>";*/

		$mainframe->setPageTitle( _HP_SEARCH_ADV_TITLE );
	?>
	<div id="con_asearch1">
		<?php $mainframe->appendPathWay(_HP_SEARCH_ADV_TITLE); ?>
		<div id="con_asearch2">
			<form action="<?php echo sefRelToAbs("index.php?option=com_hotproperty&task=asearch&Itemid=$Itemid"); ?>" method="get" name="searchfrm" id="searchfrm">
				<?php
					$i = 1;
				?>
				<table id="search_ef" cellpadding="3" cellspacing="0" border="0" width="100%">
					<tr class="hp_asearch_row<?php echo ($i++%2); ?>"><td colspan="2" align="center"><input type="reset" class="inputbox" value="<?php echo _HP_RESET ?>" /> <input type="submit" value="<?php echo _HP_SEARCH; ?>" class="inputbox" /></td></tr>
						<?php
						foreach ($fields AS $field)
						{
						?>
							<tr class="hp_asearch_row<?php echo ($i++%2); ?>">
								<td valign="top"><strong><?php echo $field->caption; ?></strong></td>
								<td>
									<?php
									echo $field->input;
									?>
								</td>
							</tr>
						<?php
						}
						?>
					<tr class="hp_asearch_row<?php echo ($i++%2); ?>"><td colspan="2" align="center"><input type="reset" class="inputbox" value="<?php echo _HP_RESET ?>" /> <input type="submit" value="<?php echo _HP_SEARCH; ?>" class="inputbox" /></td></tr>
				</table>
			</form>
		</div>
	</div>
	<?php
	}

	/***
	 * Empty Advanced Search Results
	 ***/
	function show_advSearchResults_error($msg) {
		global $Itemid, $mainframe, $hp_fp_featured_count;
		$mainframe->setPageTitle( _HP_SEARCH_ADV_TITLE );
		?>
		<div id="con_asearch1">
		<?php $mainframe->appendPathWay(_HP_SEARCH_ADV_TITLE); ?>
			<div class='alert' style='font-weight: bold; color: red'>
			<?php echo $msg; ?>
			</div>
			<a href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&task=advsearch&Itemid=$Itemid"); ?>"><?php echo _HP_SEARCH_TRYAGAIN; ?></a>
		</div>
		<?php
		//Show the featured properties if no results returned
		if ( !is_numeric($hp_fp_featured_count) ) $hp_fp_featured_count = 0;
		HP_getProperty('featured', null,$featured, $featured_fields_caption, 0, $hp_fp_featured_count);
		$featured_total = hotproperty::getNumProp('p.featured=1');
		// @organic mod [MH] : Disable the advised feature properties
		//hotproperty_HTML::show_fp_Featured($featured, $featured_fields_caption, $featured_total);
	}

	/***
	 * Advanced Search Results
	 ***/
	function show_advSearchResults(&$search_id, &$prop, &$caption, &$pageNav, &$searchString) {
		global $Itemid, $mainframe;
		$mainframe->setPageTitle( _HP_SEARCH_ADV_TITLE );
	?>
	<div id="con_asearch1">
	<?php $mainframe->appendPathWay(_HP_SEARCH_ADV_TITLE); ?>
	<div id="hp_searchresult_con">
		<div id="hp_search_pagecounter_top">
			<span class="counter"><?php echo $pageNav->writePagesCounter(); ?></span>
			<div class="search-pagenav">
				<?php echo $pageNav->writePagesLinks('index.php?option=com_hotproperty&task=asearch&Itemid='.$Itemid.'&search_id='.$search_id); ?>
			</div>
		</div>
	</div>
	<div id="list_searchresults">
		<?php hotproperty_HTML::list_properties($prop, $caption); ?>
	</div>
	</div>
	<div id="hp_search_pagecounter_bottom">
		<span class="counter"><?php echo $pageNav->writePagesCounter(); ?></span>
		<div class="search-pagenav">
			<?php echo $pageNav->writePagesLinks('index.php?option=com_hotproperty&task=asearch&Itemid='.$Itemid.'&search_id='.$search_id); ?>
		</div>
	</div>

	<?php
	}

	/***
	 * Search Results
	 ***/
	function show_SearchResults(&$types, &$prop, &$caption, &$pageNav, &$searchString) {
		global $Itemid, $hp_use_advsearch, $mosConfig_live_site, $mainframe;
		global $custom404, $mosConfig_sef, $sufix;
		$mainframe->setPageTitle( _HP_SEARCH_RESULT_TITLE );

		# Using Built in SEF feature in Mambo
		/*
		if ( !isset($custom404) && $mosConfig_sef ) {

			$onclickCmd = "document.location.href= '$mosConfig_live_site/component/option,com_hotproperty/task,search/Itemid,$Itemid/type,' + document.searchfrm.type.options[document.searchfrm.type.selectedIndex].value + '/search,' + encodeURI(document.searchfrm.hp_search.value) + '/'";

		} elseif ( $mosConfig_sef && isset($custom404) && !empty($sufix) ) {

			global $custom_comp, $hp_default_limit_search;
			$hotproperty = "hotproperty";
			if (in_array("hotproperty", $custom_comp)) {
				$hotproperty = array_search($component_name, $custom_comp);
			}

			$onclickCmd = "document.location.href='" . $hotproperty . "/" . _HP_SEF_SEARCH . "/' + document.searchfrm.type.options[document.searchfrm.type.selectedIndex].value + '/".$hp_default_limit_search."/0/" . "' + encodeURI(document.searchfrm.hp_search.value)";

		} else {

		# Using SEF advance or no SEF at all
			$onclickCmd = "document.location.href='" . sefRelToAbs("index.php?option=com_hotproperty&task=search&Itemid=$Itemid&type=' + document.searchfrm.type.options[document.searchfrm.type.selectedIndex].value + '&search=' + encodeURI(document.searchfrm.hp_search.value)");
		}
		*/
	?>
	<div id="con_search1">
	<form action="index.php" method="POST" name="searchfrm">
	<div id="heading_Search"><a href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&Itemid=$Itemid"); ?>"><?php echo _HP_COM_PATHWAY_MAIN ." "._HP_ARROW." "; ?></a><?php echo _HP_SEARCH_RESULT_TITLE; ?></div>
		<div id="hp_searchresult_con">
			<strong><?php echo _HP_SEARCH_TEXT; ?></strong>
			<input type="text" name="search" class="inputbox" value="<?php echo $searchString->search; ?>" /> <?php echo _HP_IN; ?>
			<?php
			$preload = array();
			$preload[] = mosHTML::makeOption( '0', _HP_SEARCH_ALLTYPES );
			$selected = array();
			$selected[] = mosHTML::makeOption( $_POST['type'] ); //sélectionne par défaut dans la liste l'item d'id
			echo mosHTML::treeSelectList( $types, -1, $preload, 'type',
		  	               'class="inputbox" size="1"', 'value', 'text', $selected );
			?>
			&nbsp;
			<input type="submit" class="button" value="<?php echo _HP_SEARCH; ?>" /><?php
			if ($hp_use_advsearch == '1') {
			?>&nbsp;<a href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&task=advsearch&Itemid=$Itemid"); ?>"><?php echo _HP_SEARCH_ADV_TITLE; ?></a><?php
			}
			?>
			<div id="hp_search_pagecounter_top">
			<div style="float: right"><?php echo $pageNav->writePagesCounter(); ?></div>
				<br class="clearboth" />
				<?php echo $pageNav->writePagesLinks('index.php?option=com_hotproperty&task=search&Itemid='.$Itemid.'&type='.$searchString->type.'&search='.$searchString->search); ?>
			</div>
		</div>
		<input type="hidden" name="option" value="com_hotproperty" />
		<input type="hidden" name="task" value="search" />
	</form>
	</div>

	<div id="list_searchresults">
		<?php 	hotproperty_HTML::list_properties($prop, $caption); ?>
	</div>
	<br class="clearboth" />
			<div id="hp_search_pagecounter_bottom">
			<div style="float: right"><?php echo $pageNav->writePagesCounter(); ?></div>
		<br class="clearboth" />
		<?php echo $pageNav->writePagesLinks('index.php?option=com_hotproperty&task=search&Itemid='.$Itemid.'&type='.$searchString->type.'&search='.$searchString->search); ?>
	</div>

	<?php
	}

	/***
	 * List Properties for a particular Type
	 ***/
	function show_Type($prop, $type, $caption, $pageNav, $sortby_sort, $sortby_order) {
		global $hp_use_diplaynum, $hp_use_sort_name, $hp_use_sort_agent, $hp_use_sort_price, $hp_use_sort_suburb, $hp_use_sort_state, $hp_use_sort_country, $hp_use_sort_type, $hp_use_sort_modified, $hp_use_sort_hits;

		global $Itemid, $database, $mainframe;
		global $hp_fp_show_nbr, $hp_fp_show_empty; //paramètres de configuration additionnels

		$mainframe->setPageTitle( $type->name );

		# On charge la catégorie courante dans une mosHPPropTypes classe(pour disposer de ses méthodes):
		$type_O = new mosHPPropTypes($database);
		$type_O->load($type->id);
		?>
	<div id="con_type1">
		<div id="heading_Type">
			<?php
			# Impression du pathway en parcourant la liste des pères de la catégorie courante:
			foreach($type_O->parentsType($included=true, $order="ASC") as $item){
				switch($item->id){
					case 0:
						echo '<a href="'.sefRelToAbs("index.php?option=com_hotproperty&Itemid=$Itemid").'">'._HP_COM_PATHWAY_MAIN.' '._HP_ARROW.' </a>';
						break;
					case $type_O->id:
						echo $item->name;
						break;
					default:
						echo '<a href="'.sefRelToAbs("index.php?option=com_hotproperty&task=viewtype&id=$item->id&Itemid=$Itemid").'">'.$item->name.' '._HP_ARROW.' </a>';
						break;
				}
			}
			?>
		</div>
		<?php
			if ($hp_use_diplaynum == '1' || !empty($hp_use_sort_name) || !empty($hp_use_sort_agent) || !empty($hp_use_sort_price) || !empty($hp_use_sort_suburb) || !empty($hp_use_sort_state) || !empty($hp_use_sort_country) || !empty($hp_use_sort_type) || !empty($hp_use_sort_modified) || !empty($hp_use_sort_hits))	{
		?>
		<div id="con_sort">
			<?php if ($hp_use_diplaynum == '1') { ?>
			<div id="con_sort1">
				<?php echo _PN_DISPLAY_NR; ?>
				<?php echo $pageNav->writeLimitBox('index.php?option=com_hotproperty&task=viewtype&id='.$prop[0]->typeid.'&sort='.$sortby_sort.'&order='.$sortby_order.'&Itemid='.$Itemid); ?>
			</div>
			<?php
						}
						if (!empty($hp_use_sort_name) || !empty($hp_use_sort_agent) || !empty($hp_use_sort_price) || !empty($hp_use_sort_suburb) || !empty($hp_use_sort_state) || !empty($hp_use_sort_country) || !empty($hp_use_sort_type) || !empty($hp_use_sort_modified) || !empty($hp_use_sort_hits)) {
			?>
			<div id="con_sort2">&nbsp;
				<?php echo _HP_SORT_BY; ?>
				|

				<?php if (!empty($hp_use_sort_name)) { ?>
				<a href="<?php echo sefRelToAbs('index.php?option=com_hotproperty&task=viewtype&id='.$prop[0]->typeid.'&sort=name&order=asc&limit='.$pageNav->limit.'&limitstart='.$pageNav->limitstart.'&Itemid='.$Itemid); ?>" title="<?php echo _HP_SORT_A_Z; ?>"> <?php echo _HP_SORT_ASC; ?> </a> <?php echo _HP_SORT_AZ; ?> <a href="<?php echo sefRelToAbs('index.php?option=com_hotproperty&task=viewtype&id='.$prop[0]->typeid.'&sort=name&order=desc&limit='.$pageNav->limit.'&limitstart='.$pageNav->limitstart.'&Itemid='.$Itemid); ?>" title="<?php echo _HP_SORT_Z_A; ?>"> <?php echo _HP_SORT_DESC; ?> </a> |
				<?php } ?>

				<?php if (!empty($hp_use_sort_agent) && !empty($caption['agent']->caption)) { ?>
				<a href="<?php echo sefRelToAbs('index.php?option=com_hotproperty&task=viewtype&id='.$prop[0]->typeid.'&sort=agent&order=desc&limit='.$pageNav->limit.'&limitstart='.$pageNav->limitstart.'&Itemid='.$Itemid); ?>"> <?php echo _HP_SORT_ASC; ?> </a> <?php echo $caption['agent']->caption; ?> <a href="<?php echo sefRelToAbs('index.php?option=com_hotproperty&task=viewtype&id='.$prop[0]->typeid.'&sort=agent&order=asc&limit='.$pageNav->limit.'&limitstart='.$pageNav->limitstart.'&Itemid='.$Itemid); ?>"> <?php echo _HP_SORT_DESC; ?> </a> |
				<?php } ?>

				<?php if (!empty($hp_use_sort_price) && !empty($caption['price']->caption)) { ?>
				<a href="<?php echo sefRelToAbs('index.php?option=com_hotproperty&task=viewtype&id='.$prop[0]->typeid.'&sort=price&order=asc&limit='.$pageNav->limit.'&limitstart='.$pageNav->limitstart.'&Itemid='.$Itemid); ?>" title="<?php echo _HP_SORT_LOWEST_PRICE; ?>"> <?php echo _HP_SORT_ASC; ?> </a> <?php echo $caption['price']->caption; ?> <a href="<?php echo sefRelToAbs('index.php?option=com_hotproperty&task=viewtype&id='.$prop[0]->typeid.'&sort=price&order=desc&limit='.$pageNav->limit.'&limitstart='.$pageNav->limitstart.'&Itemid='.$Itemid); ?>" title="<?php echo _HP_SORT_HIGHEST_PRICE; ?>"> <?php echo _HP_SORT_DESC; ?> </a> |
				<?php } ?>

				<?php if (!empty($hp_use_sort_suburb) && !empty($caption['suburb']->caption)) { ?>
				<a href="<?php echo sefRelToAbs('index.php?option=com_hotproperty&task=viewtype&id='.$prop[0]->typeid.'&sort=suburb&order=asc&limit='.$pageNav->limit.'&limitstart='.$pageNav->limitstart.'&Itemid='.$Itemid); ?>"> <?php echo _HP_SORT_ASC; ?> </a> <?php echo $caption['suburb']->caption; ?> <a href="<?php echo sefRelToAbs('index.php?option=com_hotproperty&task=viewtype&id='.$prop[0]->typeid.'&sort=suburb&order=desc&limit='.$pageNav->limit.'&limitstart='.$pageNav->limitstart.'&Itemid='.$Itemid); ?>"> <?php echo _HP_SORT_DESC; ?> </a> |
				<?php } ?>

				<?php if (!empty($hp_use_sort_state) && !empty($caption['state']->caption)) { ?>
				<a href="<?php echo sefRelToAbs('index.php?option=com_hotproperty&task=viewtype&id='.$prop[0]->typeid.'&sort=state&order=asc&limit='.$pageNav->limit.'&limitstart='.$pageNav->limitstart.'&Itemid='.$Itemid); ?>"> <?php echo _HP_SORT_ASC; ?> </a> <?php echo $caption['state']->caption; ?> <a href="<?php echo sefRelToAbs('index.php?option=com_hotproperty&task=viewtype&id='.$prop[0]->typeid.'&sort=state&order=desc&limit='.$pageNav->limit.'&limitstart='.$pageNav->limitstart.'&Itemid='.$Itemid); ?>"> <?php echo _HP_SORT_DESC; ?> </a> |
				<?php } ?>

				<?php if (!empty($hp_use_sort_country) && !empty($caption['country']->caption)) { ?>
				<a href="<?php echo sefRelToAbs('index.php?option=com_hotproperty&task=viewtype&id='.$prop[0]->typeid.'&sort=country&order=asc&limit='.$pageNav->limit.'&limitstart='.$pageNav->limitstart.'&Itemid='.$Itemid); ?>"> <?php echo _HP_SORT_ASC; ?> </a> <?php echo $caption['country']->caption; ?> <a href="<?php echo sefRelToAbs('index.php?option=com_hotproperty&task=viewtype&id='.$prop[0]->typeid.'&sort=country&order=desc&limit='.$pageNav->limit.'&limitstart='.$pageNav->limitstart.'&Itemid='.$Itemid); ?>"> <?php echo _HP_SORT_DESC; ?> </a> |
				<?php } ?>

				<?php if (!empty($hp_use_sort_type) && !empty($caption['type']->caption)) { ?>
				<a href="<?php echo sefRelToAbs('index.php?option=com_hotproperty&task=viewtype&id='.$prop[0]->typeid.'&sort=type&order=asc&limit='.$pageNav->limit.'&limitstart='.$pageNav->limitstart.'&Itemid='.$Itemid); ?>"> <?php echo _HP_SORT_ASC; ?> </a> <?php echo $caption['type']->caption; ?> <a href="<?php echo sefRelToAbs('index.php?option=com_hotproperty&task=viewtype&id='.$prop[0]->typeid.'&sort=type&order=desc&limit='.$pageNav->limit.'&limitstart='.$pageNav->limitstart.'&Itemid='.$Itemid); ?>"> <?php echo _HP_SORT_DESC; ?> </a> |
				<?php } ?>

				<?php if (!empty($hp_use_sort_modified) && !empty($caption['modified']->caption)) { ?>
				<a href="<?php echo sefRelToAbs('index.php?option=com_hotproperty&task=viewtype&id='.$prop[0]->typeid.'&sort=modified&order=asc&limit='.$pageNav->limit.'&limitstart='.$pageNav->limitstart.'&Itemid='.$Itemid); ?>"> <?php echo _HP_SORT_ASC; ?> </a> <?php echo $caption['modified']->caption; ?> <a href="<?php echo sefRelToAbs('index.php?option=com_hotproperty&task=viewtype&id='.$prop[0]->typeid.'&sort=modified&order=desc&limit='.$pageNav->limit.'&limitstart='.$pageNav->limitstart.'&Itemid='.$Itemid); ?>"> <?php echo _HP_SORT_DESC; ?> </a> |
				<?php } ?>

				<?php if (!empty($hp_use_sort_hits) && !empty($caption['hits']->caption)) { ?>
				<a href="<?php echo sefRelToAbs('index.php?option=com_hotproperty&task=viewtype&id='.$prop[0]->typeid.'&sort=hits&order=asc&limit='.$pageNav->limit.'&limitstart='.$pageNav->limitstart.'&Itemid='.$Itemid); ?>"> <?php echo _HP_SORT_ASC; ?> </a> <?php echo $caption['hits']->caption; ?> <a href="<?php echo sefRelToAbs('index.php?option=com_hotproperty&task=viewtype&id='.$prop[0]->typeid.'&sort=hits&order=desc&limit='.$pageNav->limit.'&limitstart='.$pageNav->limitstart.'&Itemid='.$Itemid); ?>"> <?php echo _HP_SORT_DESC; ?> </a> |
				<?php } ?>

				</div>
			<?php } ?>
		</div>
		<?php } ?>
		<div id="list_subtypes">
			<?php
				# On liste les catégories directement enfants de la catégorie courante:
				$subtypes = $type_O->childrenType($published=1);

				# S'il y a effectivement des catégories enfants,
				if(!empty($subtypes))
				{
					echo '<ul>';
					$s=0; //compteur des <li> pour fermer proprement le <ul>
					# Pour chaque sous-catégorie directe
					foreach($subtypes as $subtype)
					{
						# On charge la sous-catégorie dans une mosHPPropTypes classe(pour disposer de ses méthodes):
						$subtype_O = new mosHPPropTypes($database);
						$subtype_O->load($subtype->id);

						# On affiche ou non(en fonction de la config) le nombre de propriétés:
						if($hp_fp_show_nbr) {
							$nbr = $subtype_O->countProps_R($included=true, $published=1, $forced=false);
							$nbr = ($nbr) ? " (".$nbr.")" : " (0)";
						} else {
							$nbr="";
						}

						# Si la catégorie n'est pas vide, ou bien si l'on a choisi(dans la config) d'afficher les catégories vides:
						if($subtype_O->countProps_R($included=true, $published=1, $forced=false) || $hp_fp_show_empty) {
							$s++;
							?>
							<li>
								<a id="<?php echo $subtype_O->id; ?>" class="types_title" href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&task=viewtype&id=$subtype_O->id&Itemid=$Itemid"); ?>"><?php echo $subtype_O->name; ?></a><?php echo $nbr; ?><br />
								<span class="types_desc"><?php echo $subtype_O->desc; ?></span>
							</li>
							<?php
						}
					}
					# On ferme proprement le <ul>:
					echo ($s>0) ? '</ul>' :	'<li></li></ul>';
				}
			?>
		</div>
		<div id="list_properties">
			<?php hotproperty_HTML::list_properties($prop, $caption); ?>
		</div>
		<br class="clearboth" />
		<div id="hp_pagecounter_bottom">
			<div style="float: right"><?php echo $pageNav->writePagesCounter(); ?></div>
			<?php echo $pageNav->writePagesLinks('index.php?option=com_hotproperty&task=viewtype&id='.$prop[0]->typeid.'&sort='.$sortby_sort.'&order='.$sortby_order.'&Itemid='.$Itemid);

			?>
		</div>
	</div>
		<?php
	}

	/***
	 * List Featured Properties
	 ***/
	function show_Featured($prop, $caption, $pageNav) {
		global $Itemid, $mainframe;
		$mainframe->setPageTitle( _HP_FEATURED );
		?>
	<div id="con_type1">
		<div id="heading_Featured"><a href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&Itemid=$Itemid"); ?>"><?php echo _HP_COM_PATHWAY_MAIN ." "._HP_ARROW." "; ?></a><?php echo _HP_FEATURED; ?></div>
		<div id="list_properties">
			<?php 	hotproperty_HTML::list_properties($prop, $caption); ?>
		</div>
		<br class="clearboth" />
		<div id="hp_pagecounter_bottom">
			<div style="float: right"><?php echo $pageNav->writePagesCounter(); ?></div>
			<br class="clearboth" />
			<?php echo $pageNav->writePagesLinks('index.php?option=com_hotproperty&task=viewfeatured'); ?>
		</div>
	</div>
		<?php
	}

	/***
	 * Display Company's contact details and list all agents under it.
	 ***/
	function show_Company($company, $agents, $pageNav) {
		global $mosConfig_live_site, $hp_imgdir_agent, $mainframe, $Itemid;
		$mainframe->setPageTitle( $company[0]->name );
		?>
		<div id="heading_Co"><a href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&Itemid=$Itemid"); ?>"><?php echo _HP_COM_PATHWAY_MAIN ." "._HP_ARROW." "; ?></a> <?php echo _HP_CO_TITLE; ?></div>
		<div id="hp_view_co_con">
			<?php hotproperty_HTML::show_CoInfo($company) ?>
		</div>
		<br class="clearboth" />

		<div id="heading_Agent"><?php echo _HP_VIEW_AGENT_TITLE; ?></div>
		<div id="list_agents">
		<?php hotproperty_HTML::show_AgentInfo($agents) ?>
		</div>

		<br class="clearboth" />
		<div id="hp_pagecounter_bottom">
			<div style="float: right"><?php echo $pageNav->writePagesCounter(); ?></div>
		<br class="clearboth" />
		<?php echo $pageNav->writePagesLinks('index.php?option=com_hotproperty&task=viewco&id='.$company[0]->id); ?>
		</div>

		<?php
	}

	/***
	 * Display Agent's information and list all properties under it.
	 ***/
	 function show_Agent($prop, $caption, $agent, $pageNav) {
		 global $mosConfig_live_site, $hp_imgdir_agent, $Itemid, $mainframe;
		$mainframe->setPageTitle( $agent[0]->name );

		?>

		<div id="hp_view_agent_title"><a href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&Itemid=$Itemid"); ?>"><?php echo _HP_COM_PATHWAY_MAIN ." "._HP_ARROW." "; ?></a> <a href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&task=viewco&id=".$agent[0]->companyid."&Itemid=".$Itemid); ?>"><?php echo $agent[0]->company ." "._HP_ARROW." "; ?></a><?php echo _HP_VIEW_AGENT_TITLE; ?></div>
		<div id="hp_view_agent_con">
		<?php hotproperty_HTML::show_AgentInfo($agent) ?>
		</div>
		<br class="clearboth" />

		<div id="heading_Agent"><?php echo _HP_PROPBYAGENT.$agent[0]->name; ?></div>
		<div id="list_properties">
		<?php 	hotproperty_HTML::list_properties($prop, $caption); ?>
		</div>

		<br class="clearboth" />
		<div id="hp_pagecounter_bottom">
			<div style="float: right"><?php echo $pageNav->writePagesCounter(); ?></div>
		<br class="clearboth" />
		<?php echo $pageNav->writePagesLinks('index.php?option=com_hotproperty&task=viewagent&id='.$agent[0]->id); ?>
		</div>

		<?php
	}

	/***
	 * Display Company's information and an enquiry form.
	 ***/
	function show_CoEmail($company) {
		global $Itemid, $mainframe;
		$mainframe->setPageTitle( _HP_CO_CONTACT .' - '.$company[0]->name );

		?>
		<div id="heading_Co"><a href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&Itemid=$Itemid"); ?>"><?php echo _HP_COM_PATHWAY_MAIN ." "._HP_ARROW." "; ?></a><?php echo _HP_CO_TITLE; ?></div>
		<div id="hp_view_co_con">
			<?php hotproperty_HTML::show_CoInfo($company) ?>
		</div>
		<br class="clearboth" />
		<div id="heading_Co_Contact"><?php echo _HP_CO_CONTACT; ?></div>
		<div id="hp_emailform_con">
			<?php hotproperty_HTML::show_EmailForm('company',$company[0]->id) ?>
		</div>

		<?php
	}

	/***
	 * Display Agent's information and an enquiry form.
	 ***/
	function show_AgentEmail($agent) {
		global $Itemid, $mainframe;
		$mainframe->setPageTitle( _HP_VIEW_AGENT_CONTACT .' - '.$agent[0]->name );
		?>
		<div id="hp_view_agent_title"><a href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&Itemid=$Itemid"); ?>"><?php echo _HP_COM_PATHWAY_MAIN ." "._HP_ARROW." "; ?></a> <a href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&task=viewco&id=".$agent[0]->companyid."&Itemid=".$Itemid); ?>"><?php echo $agent[0]->company ." "._HP_ARROW." "; ?></a><?php echo _HP_VIEW_AGENT_TITLE; ?></div>
		<div id="hp_view_agent_con">
			<?php hotproperty_HTML::show_AgentInfo($agent) ?>
		</div>
		<br class="clearboth" />
		<div id="hp_view_agent_contact"><?php echo _HP_VIEW_AGENT_CONTACT; ?></div>
		<div id="hp_emailform_con">
		<?php hotproperty_HTML::show_EmailForm('agent',$agent[0]->id) ?>
		</div>

		<?php
	}

	/***
	 * Show Property
	 ***/
	function original_show_Prop(&$prop, &$caption, &$images, &$agent) {
		global $Itemid, $my, $mosConfig_live_site, $mosConfig_absolute_path, $pop, $mainframe, $database;
		global $hp_imgdir_thumb, $hp_imgdir_standard, $hp_currency, $hp_imgsize_thumb, $hp_img_noimage_thumb, $hp_imgdir_agent, $hp_show_agentdetails, $hp_show_enquiryform, $hp_thousand_sep, $hp_dec_point, $hp_link_open_newwin, $hp_show_moreinfo, $hp_use_companyagent, $hp_dec_string, $hp_thousand_string;
		global $hp_show_pdficon, $hp_show_printicon, $hp_show_emailicon;

		$mainframe->appendMetaTag( 'description', $prop[0]->metadesc );
		$mainframe->appendMetaTag( 'keywords', $prop[0]->metakey );
		$mainframe->setPageTitle( $prop[0]->name );
		?>
	<div id="con_hp1">
	<div id="heading_Prop">
		<?php
		# On charge la catégorie parente de $prop dans une classe mosHPPropTypes(pour disposer de ses méthodes):
		$parentprop_O = new mosHPPropTypes($database);
		$parentprop_O->load($prop[0]->typeid);

		#On imprime le pathway en remontant dans les parents de $parenttype:
		foreach($parentprop_O->parentsType($included=true, $order="ASC") as $item){
			switch($item->id){
				case 0:
					echo '<a href="'.sefRelToAbs("index.php?option=com_hotproperty&Itemid=$Itemid").'">'._HP_COM_PATHWAY_MAIN.' '._HP_ARROW.' </a>';
					break;
				default:
					echo '<a href="'.sefRelToAbs("index.php?option=com_hotproperty&task=viewtype&id=$item->id&Itemid=$Itemid").'">'.$item->name.' '._HP_ARROW.' </a>';
					break;
			}
		}
		echo $prop[0]->name;

		# Show edit icon for authorized agent
		if ($prop[0]->user == $my->id && $prop[0]->user > 0 && $my->id > 0) { ?>
		&nbsp;
		<a href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&task=editprop&id=". $prop[0]->id ."&Itemid=$Itemid"); ?>" title="<?php echo _E_EDIT; ?>"><img src="images/M_images/edit.png" width="13" height="14" align="middle" border="0" alt="<?php echo _E_EDIT; ?>" /></a>
		<?php } ?>
	</div>
	<div id="hp_icons"><?php

		if ($hp_show_pdficon && !$pop) {
		?>
		<a href="javascript:void window.open('<?php echo $mosConfig_live_site; ?>/components/com_hotproperty/pdf.php?id=<?php echo $prop[0]->id; ?>', 'win2', 'status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no');" title="<?php echo _CMN_PDF;?>"><img
    src="<?php echo $mosConfig_live_site;?>/images/M_images/pdf_button.png" border="0" alt="<?php echo _CMN_PDF;?>" /></a>
		&nbsp;
		<?php
		} // End of if $hp_show_pdficon

		if ($hp_show_printicon) {
			if ($pop) { ?>
		<a href="#" onClick="javascript:window.print(); return false" title="<?php echo _CMN_PRINT;?>"><?php
			} else {
		?><a href="javascript:void window.open('<?php echo $mosConfig_live_site; ?>/index2.php?option=com_hotproperty&amp;task=view&amp;id=<?php echo $prop[0]->id; ?>&amp;pop=1', 'win2', 'status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no');" title="<?php echo _CMN_PRINT;?>"><?php
			} ?><img
    src="<?php echo $mosConfig_live_site;?>/images/M_images/printButton.png" border="0" alt="<?php echo _CMN_PRINT;?>" /></a>
		&nbsp;
		<?php } // End of if $hp_show_printicon

		if ($hp_show_emailicon && !$pop) {
			?><a href="javascript:void window.open('<?php echo $mosConfig_live_site; ?>/index2.php?option=com_hotproperty&amp;task=emailform&amp;id=<?php echo $prop[0]->id; ?>', 'win2', 'status=no,toolbar=no,scrollbars=no,titlebar=no,menubar=no,resizable=yes,width=400,height=235,directories=no,location=no');" title="<?php echo _CMN_EMAIL;?>"><img
    src="<?php echo $mosConfig_live_site;?>/images/M_images/emailButton.png" border='0' alt="<?php echo _CMN_EMAIL; ?>" /></a>
		&nbsp;
		<?php
		}
		?>
		</div>
		<?php if (isset($images[0]->standard) && $images[0]->standard <> '') {
						$standard_imgsize = GetImageSize ($mosConfig_absolute_path.$hp_imgdir_standard.$images[0]->standard); ?>
		<div id="hp_view_standard_photo_con1">
			<div id="hp_view_standard_photo_con2">
			<div id="hp_view_standard_photo_con3"><img name="standard_photo" src="<?php echo $mosConfig_live_site.$hp_imgdir_standard.$images[0]->standard; ?>" alt="<?php echo $images[0]->title; ?>"></div>
			<div id="hp_view_standard_photo_con4">
			<div id="hp_view_standard_photo_title"><?php echo $images[0]->title; ?>&nbsp;</div>
			<div id="hp_view_standard_photo_desc"><?php echo htmlentities($images[0]->desc, ENT_QUOTES); ?>&nbsp;</div>
			</div>
			</div>
		</div>
		<?php } ?>
        <script language="javascript" type="text/javascript">
				function show_photo(what, photo, title, desc) {
					document.standard_photo.src ='<?php echo $mosConfig_live_site.$hp_imgdir_standard; ?>' + photo;
					document.standard_photo.alt =title;

					if (title == '') {
						document.getElementById("hp_view_standard_photo_title").childNodes[0].nodeValue = '';
					} else {
						document.getElementById("hp_view_standard_photo_title").childNodes[0].nodeValue = title;
					}

				  if (desc == '') {
						document.getElementById("hp_view_standard_photo_desc").childNodes[0].nodeValue = '';
					} else {
						document.getElementById("hp_view_standard_photo_desc").childNodes[0].nodeValue = desc;
					}
				}
				</script>
		<div id="con_hp2">
		<div id="image_scroll">
		<?php foreach($prop AS $p) {
			if (count($images) > 1) { ?>
			<div class="hp_view_thumb_con">
			<ul class="thumb"><?php
					foreach($images AS $image) {
						if ($image->thumb <> '') {
							$thumb_imgsize = GetImageSize ($mosConfig_absolute_path.$hp_imgdir_thumb.$image->thumb);
						} else {
							$thumb_imgsize = GetImageSize ($mosConfig_absolute_path.$hp_imgdir_thumb.$hp_img_noimage_thumb);
						}
						?><li><?php
						echo "<a href=\"javascript: show_photo(this, '$image->standard', '".addslashes(htmlspecialchars($image->title, ENT_COMPAT))."', '".addslashes(htmlspecialchars($image->desc, ENT_COMPAT))."');\">";
				?><img <?php echo $thumb_imgsize[3]; ?> border="0" src="<?php echo $mosConfig_live_site.$hp_imgdir_thumb.(($image->thumb <> '') ? $image->thumb : $hp_img_noimage_thumb);?>" alt="<?php echo $image->title; ?>"></a><br /><?php echo $image->title; ?></li><?php
					} // End foreach
				?>
			</ul></div>
			</div>
			<br class="clearboth" /><div class="corner_bl">&nbsp;</div>	
			<div class="clearfix"></div>
			<?php } // End If: count($images) ?>
			<div class="hp_view_details">
					<?php
					foreach($p as $key => $value) {
						if ( array_key_exists($key,$caption) && ($key <> 'name' && $key <> 'suburb' && $key <> 'state' && $key <> 'postcode' && $key <> 'country' &&	$caption[$key]->iscore == 1) ){
							if ($key=="address") {
								if (!$caption['address']->hideCaption) { ?>
									<span class="hp_caption"><?php echo $caption['address']->caption; ?></span>:<br />
									<?php } ?>
									<div id="hp_view_addr">
									<?php
										if (trim($p->address)!="") {
											echo "$p->address <br />";
										}
										if ((trim($p->suburb)!="") && (trim($p->state)!="") && (trim($p->postcode)!="")) {
											echo "$p->suburb, $p->state, $p->postcode <br />";
										 } elseif ((trim($p->suburb)!="") && (trim($p->state)!="")) {
											echo "$p->suburb, $p->state <br />";
										} elseif ((trim($p->suburb)!="") && (trim($p->postcode)!="")) {
											echo "$p->suburb, $p->postcode <br />";
										} elseif ((trim($p->state)!="") && (trim($p->postcode)!="")) {
											echo "$p->state, $p->postcode <br />";
										} elseif ((trim($p->state)!="")) {
											echo "$p->state <br />";
										} elseif ((trim($p->suburb)!="")) {
											echo "$p->suburb <br />";
										} elseif ((trim($p->postcode)!="")) {
											echo "$p->postcode <br />";
										}
										if (trim($p->country)!="") {
											echo "$p->country <br />";
										}
									?>
									</div>
									<?php
							} // End if (address)

							if ($key=="price") {
								if (isset($caption['price'])) {
									if (!$caption['price']->hideCaption) { ?><span class="hp_caption"><?php echo $caption[$key]->caption; ?>: </span> <?php }
									echo "<span class=\"hp_price\">".$caption["price"]->prefix_text.$hp_currency." ".number_format($p->price, $hp_dec_point, $hp_dec_string, ($hp_thousand_sep) ? $hp_thousand_string:'').$caption["price"]->append_text. "</span><br /> ";
								}
							} // End if (price)

							elseif ($key=="type") {
								if (isset($caption['type'])) {
									if (!$caption['type']->hideCaption) { ?><span class="hp_caption"><?php echo $caption[$key]->caption; ?>: </span><?php }
									echo $p->type." <br /> ";
								}
							} // End if (type)

							elseif ($key=="intro_text") {
								if ($p->intro_text) {
									if (!$caption['intro_text']->hideCaption) { ?><span class="hp_caption"><?php echo $caption[$key]->caption; ?>: </span><?php } ?>
									<p id="hp_view_intro_text"><?php echo $p->intro_text; ?></p>
								<?php }
							} // End if (intro_text)

							elseif ($key=="full_text") {
								if ($p->full_text) {
									if (!$caption['full_text']->hideCaption) { ?><span class="hp_caption"><?php echo $caption[$key]->caption; ?>: </span><?php } ?>
								<p id="hp_view_full_text"><?php echo $p->full_text; ?></p>
								<?php }
							} // End if (full_text)

							elseif ($key == "featured") {
								if (!$caption[$key]->hideCaption) {
									echo '<span class="hp_caption">'.$caption[$key]->caption."</span>: ";
								}
								echo $caption[$key]->prefix_text
									.	( ($value == '1') ? _CMN_YES : _CMN_NO )
									.	$caption[$key]->append_text
									.	"<br />";
							} // End if (featured)

							else {
								if ($key <> "address") {
									if (!$caption[$key]->hideCaption) { ?><span class="hp_caption"><?php echo $caption[$key]->caption; ?>: </span><?php }
									echo $value." <br /> ";
								}
							}
						} // End if
					} // End foreach

					if ($hp_show_moreinfo) {
					?>
					<p />
					<div id="hp_view_features_title"><?php echo _HP_VIEW_FEATURES_TITLE; ?></div>
					<?php
					}
						foreach($p as $key => $value) {
							if ( array_key_exists($key,$caption) && ($caption[$key]->name <> 'name' && $caption[$key]->name <> 'agent' && $caption[$key]->name <> 'address' && $caption[$key]->name <> 'suburb' && $caption[$key]->name <> 'state' && $caption[$key]->name <> 'postcode' && $caption[$key]->name <> 'country' && $caption[$key]->name <> 'price' && $caption[$key]->name <> 'type' && $caption[$key]->name <> 'intro_text' && $caption[$key]->name <> 'full_text' && $caption[$key]->name <> 'thumb' && $caption[$key]->name <> 'thumb_title' && $caption[$key]->name <> '' && $value <> "" && $caption[$key]->iscore == 0) ) {
								if (!$caption[$key]->hideCaption) {
									echo '<span class="hp_caption">'.$caption[$key]->caption.":</span> ";
								}
								if ($caption[$key]->field_type == "checkbox" || $caption[$key]->field_type == "selectmultiple") {
									# Generate list
									$olist = explode("|",$value);
									echo "<ul class=\"olist\">\n";
									foreach ($olist AS $ol) {
										echo "<li>".$ol."</li>\n";
									}
									echo "</ul>\n";
								} elseif($caption[$key]->field_type == "link") {

									// Evaluate mambot style data
									$value = str_replace( '{property_id}', $p->id, $value );
									$value = str_replace( '{type_id}', $p->typeid, $value );
									$value = str_replace( '{agent_id}', $p->agentid, $value );
									$value = str_replace( '{company_id}', $p->companyid, $value );
									$value = str_replace( '{Itemid}', $Itemid, $value );

									echo $caption[$key]->prefix_text;
									$link = explode("|",$value);
									if (count($link) == 1 && ( substr(trim($link[0]),0,4) == "http" || substr(trim($link[0]),0,5) == "index" ) ) {
										?><a <?php echo ($hp_link_open_newwin) ? 'target="_blank" ': ''; ?>href="<?php echo $link[0]; ?>"><?php echo $link[0]; ?></a><?php
									} elseif (count($link) > 1 && ( substr(trim($link[1]),0,4) == "http" || substr(trim($link[1]),0,5) == "index" ) ) {
										?><a <?php echo ($hp_link_open_newwin) ? 'target="_blank" ': ''; ?>href="<?php echo $link[1]; ?>"><?php echo $link[0]; ?></a><?php
									} else {
										echo $value;
									}
									echo $caption[$key]->append_text."<br />";

								} else {
									echo $caption[$key]->prefix_text.$value.$caption[$key]->append_text."<br />";
								}
							} // End if
						} // End foreach
					?>					
			</div>
			<br class="clearboth" />					
		<?php } ?>
		</div>
		<?php if ($hp_show_agentdetails && $hp_use_companyagent) { ?>
		<div id="hp_view_agent_title"><?php echo _HP_VIEW_AGENT_TITLE; ?></div>
		<div id="hp_view_agent_con">
		<?php hotproperty_HTML::show_AgentInfo($agent) ?>
		</div>
		<?php } ?>
		<?php if ($hp_show_enquiryform && !$pop) { ?>
		<br class="clearboth" />
		<div id="hp_view_agent_contact"><?php echo _HP_VIEW_AGENT_CONTACT; ?></div>
		<div id="hp_emailform_con">
		<?php hotproperty_HTML::show_EmailForm('property',$prop[0]->id); ?>
		</div>
		<?php }
		if ($pop) {
		?>
		<center><a href='javascript:window.close();'><span class="small"><?php echo _PROMPT_CLOSE;?></span></a></center>
		<?php } ?>		
	</div>
		<?php
	}
	
	/***
	 * Show Property
	 ***/
	function show_Prop(&$prop, &$caption, &$images, &$agent) {
		global $Itemid, $my, $mosConfig_live_site, $mosConfig_absolute_path, $pop, $mainframe, $database, $id;
		global $hp_imgdir_thumb, $hp_imgdir_standard, $hp_currency, $hp_imgsize_thumb, $hp_imgdir_zoom, $hp_img_noimage_thumb, $hp_imgdir_agent, $hp_show_agentdetails, $hp_show_enquiryform, $hp_thousand_sep, $hp_dec_point, $hp_link_open_newwin, $hp_show_moreinfo, $hp_use_companyagent, $hp_dec_string, $hp_thousand_string;
		global $hp_show_pdficon, $hp_show_printicon, $hp_show_emailicon;

		$mainframe->appendMetaTag( 'description', $prop[0]->metadesc );
		$mainframe->appendMetaTag( 'keywords', $prop[0]->metakey );
		$mainframe->setPageTitle( $prop[0]->name );
		
		$mainframe->addCustomheadTag("<link rel='stylesheet' href='$mosConfig_live_site/components/com_hotproperty/includes/gallery/gallery.css' type='text/css' media='screen' />");
		$mainframe->addCustomheadTag("<script src='$mosConfig_live_site/components/com_hotproperty/includes/gallery/gallery.js' type='text/javascript'></script>");
		
		$mainframe->addCustomheadTag("<link rel='stylesheet' href='$mosConfig_live_site/components/com_hotproperty/includes/gallery/slimbox.css' type='text/css' media='screen' />");
	    $mainframe->addCustomheadTag("<script src='$mosConfig_live_site/components/com_hotproperty/includes/gallery/slimbox.js' type='text/javascript'></script>");

		?>

	<h1 class="prop-heading">
		<?php echo $prop[0]->name; ?> - 
			<span>
					<?php echo $prop[0]->area ? $prop[0]->area : 'Anna Maria Island Vacation Rentals' ?>
			</span>
	</h1>
	<a id='backButton' href='#' onclick='history.go(-1); return false'><?php echo _HP_PROP_RETURN;?></a>
	<div id="con_hp1">
		<div id="heading_Prop">
			<?php
			# On charge la catégorie parente de $prop dans une classe mosHPPropTypes(pour disposer de ses méthodes):
			$parentprop_O = new mosHPPropTypes($database);
			$parentprop_O->load($prop[0]->typeid);
			
			# Show edit icon for authorized agent
			if ($prop[0]->user == $my->id && $prop[0]->user > 0 && $my->id > 0) { ?>
			<a href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&task=editprop&id=". $prop[0]->id ."&Itemid=$Itemid"); ?>" title="<?php echo _E_EDIT; ?>"><img src="images/M_images/edit.png" width="13" height="14" align="middle" border="0" alt="<?php echo _E_EDIT; ?>" /></a>
			<?php } ?>
		</div>
		<?php $p = $prop[0]; ?>
		<div class="prop-left-col">
			<?php if(count($images)){ ?>
			<div class="hp_gallery">
				<div id="hpGallery" class="galleryElement" style="display: none; width: 680px;"></div>											
				<script type="text/javascript">
					//<!--	
						
					var myGallery;	
					var myArray = new Array();
					
					<?php foreach($images as $k => $image) { 														
						if (file_exists($mosConfig_absolute_path.$hp_imgdir_zoom.$image->zoom) && $image->zoom){
							$zoom = $mosConfig_live_site.$hp_imgdir_zoom.str_replace(' ','%20', $image->zoom);
						}
						else {
							$zoom = $mosConfig_live_site.$hp_imgdir_standard.str_replace(' ','%20', $image->standard); 
						}
						$image->thumb = str_replace(' ','%20', $image->thumb);
						?>													
					myArray[<?php echo $k ?>] = {
								number: <?php echo $k ?>,
								title: '<?php echo addslashes($image->title) ?>',
								description: '<?php echo htmlentities(addslashes($image->desc), ENT_QUOTES) ?>',
								link: '<?php echo $zoom ?>',
								transition: 'fade',
								image: '<?php echo $mosConfig_live_site.$hp_imgdir_standard.$image->standard ?>',
								thumbnail: '<?php echo $mosConfig_live_site.$hp_imgdir_thumb.$image->thumb ?>'
								};
					<?php } ?>												
			
					window.addEvent('domready', function() {
						$('hpGallery').setStyle('display','block');				
							myGallery = new gallery($('hpGallery'), {
								timed: false,
								manualData: myArray
						});
					});														
					//-->
				</script>
			</div>
			<?php } ?>
			<?php
			//Output addthis
			global $_MAMBOTS;
			$_MAMBOTS->loadBotGroup( 'content' );
			$params = new mosEmpty();

			$article = new stdClass();
			$article->text = '';
			$article->catid = 22;

			$_MAMBOTS->trigger( 'onPrepareContent', array( &$article, &$params ), true );
			echo $article->text;
			?>
			<div id="prop-glance">
				<h3><?php echo _HP_PROP_KEY_FEATURES;?></h3>
				<?php 
				echo $p->bullets 
				?>
			</div>
			<div class="tabs-menu">
				<?php
				// instantiate new tab system
				$tabs = new mosTabs(1);
				// start tab pane
				$tabs->startPane("TabPaneOne");
									
				//Second tab
				if($p->intro_text || $p->full_text){
					$tabs->startTab(_HP_PROP_TAB_GENERAL_INFO,"secondtab-page");
						echo '<div class="introtext">'.$p->intro_text.'</div>';
						echo '<div class="fulltext">'.$p->full_text.'</div>';
					$tabs->endTab();
				}
				
				//Third Tab
				if($p->living_info){
					$tabs->startTab(_HP_PROP_TAB_LIVING_AREA,"thirdtab-page");
						echo $p->living_info;
					$tabs->endTab();
				}
				
				//Fourth tab
				if($p->kitchen_info){
					$tabs->startTab(_HP_PROP_TAB_KITCHEN,"forthtab-page");
						echo $p->kitchen_info;
					$tabs->endTab();
				}
				
				//Fifth Tab
				if($p->bedroom_info){
					$tabs->startTab(_HP_PROP_TAB_BEDROOM,"fithtab-page");
						echo $p->bedroom_info;
					$tabs->endTab();
				}
				
				//Sixth tab
				if($p->bathroom_info){
					$tabs->startTab(_HP_PROP_TAB_BATHROOM,"sixthtab-page");
						echo $p->bathroom_info;
					$tabs->endTab();
				}
				
				//Seventh tab
				if($p->outside_info){
					$tabs->startTab(_HP_PROP_TAB_OUTSIDE,"seventhtab-page");
						echo $p->outside_info;
					$tabs->endTab();
				}
				
				//Eighth tab
				$isIe = isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false);
				$https = $_SERVER['HTTPS'] == 'on';
				if($p->longitude && $p->latitude && (!$https || (!$isIe && $https))){
					$tabs->startTab(_HP_PROP_TAB_MAP,"eighthtab-page");
						include_once 'includes/google_maps.php';
						getMap($p,1,1);
					$tabs->endTab();
				}
				
				//Ninth tab
				if($p->outside_info){
					$tabs->startTab(_HP_PROP_TAB_COMMENTS,"ninthtab-page");
						echo '<div class="jom-comment">';
						hotproperty_HTML::showComments($p);
						echo '</div>';
					$tabs->endTab();
				}
											
				// end tab pane
				$tabs->endPane("TabPaneOne");
				
				?>
			</div>
		</div>
		<div class="prop-right-col">
			<?php if($p->typeid == 16){ ?>
				<?php
				/*
					if the calendar class exists, then output it - but only if on a property page
				*/
				if (file_exists($mosConfig_absolute_path.'/administrator/components/com_virtuemart/classes/ps_calendar.php')) {
				
					require_once($mosConfig_absolute_path.'/administrator/components/com_virtuemart/classes/ps_calendar.php');
					?>
					<div id='hp_availability'>															
						<?php	
						$ps_calendar = new ps_calendar('hp_cal',$p->id);
						echo $ps_calendar->getAjaxCalendar();									
						?>
					</div>
				<?php } ?>
			<?php } ?>
			<?php
			if ($p->youtube) {
				hotproperty_HTML::showYoutubeVid($p);
			}
			hotproperty_HTML::showRelatedProperties($p);
			?>
		</div>		
	</div>
		<?php
	}
	
	/**
	 * Show comments
	 */
	
	function showComments(&$p) {
		global $_MAMBOTS;
		$_MAMBOTS->loadBotGroup( 'content' );
		$params = new mosEmpty();
		
		$article = new stdClass();
		$article->id = $p->id;
		$article->text = '';
		$article->sectionid = 1;
		$article->catid = 0;
		
		$results = $_MAMBOTS->trigger( 'onPrepareContent', array( &$article, &$params, 0, 'com_hotproperty' ), true );
		
		?>
		<h2>Property Reviews</h2>
		<?php
		
		echo str_replace('</form>', '<input type="hidden" class="inputbox" name="jc_title" value="'.$p->name.'"></form>', $article->text);
	}
	
	/**
	 * Function for trimming a string to a certain number of characters, will strip tags and break on whole words.
	 * @param string $str String to be trimmed
	 * @param int $n Number of characters to return
	 * @param string $delim [optional]
	 * @return string $str Trimmed string
	 */
	function neat_trim($str, $n, $delim='...') {
	   $len = strlen($str);
	   if ($len > $n) {
	       preg_match('/(.{' . $n . '}.*?)\b/', $str, $matches);
	       return rtrim($matches[1]) . $delim;
	   }
	   else {
	       return $str;
	   }
	}
	
	/**
	 * Show related properties
	 * @param object $prop Property object
	 */
	function showRelatedProperties(&$prop){
		global $database;
		global $Itemid, $task, $my, $mosConfig_live_site, $mosConfig_absolute_path, $mainframe;
		global $hp_imgdir_thumb, $hp_imgdir_standard, $hp_currency, $hp_imgsize_thumb, $hp_img_noimage_thumb, $hp_thousand_sep, $hp_dec_point, $hp_link_open_newwin, $hp_show_thumb, $hp_dec_string, $hp_thousand_string;
		
		$dateFrom = mosGetParam($_REQUEST,'dateFrom', array());
		$dateTo = mosGetParam($_REQUEST,'dateTo', array());
		
		if(is_array($dateFrom)){
			$dateFromQ = array();
			foreach($dateFrom as $k => $v){
				if($v){
					$dateFromQ[] = "dateFrom[$k]=$v";
				}
			}
			$dateFromStr = count($dateFromQ) == 3 ? implode('&amp;',$dateFromQ) : '';
		}else{
			$dateFromStr = "dateFrom=$dateFrom";
		}
		
		if(is_array($dateTo)){
			$dateToQ = array();
			foreach($dateTo as $k => $v){
				if($v){
					$dateToQ[] = "dateTo[$k]=$v";
				}
			}
			$dateToStr = count($dateTo) == 3 ? implode('&amp;',$dateToQ) : '';
		}else{
			$dateToStr = "dateFrom=$dateFrom";
		}
		
		$database->setQuery("SELECT DISTINCT p.property
								FROM #__hp_properties2 AS p
								LEFT JOIN #__hp_properties AS p2 ON p.property = p2.id
								WHERE ( ( p.field = 22 AND p.value = '" . $prop->bedrooms . "' AND p.value != '' )
								OR property IN ( SELECT id FROM #__hp_properties WHERE suburb = '" . $prop->suburb . "'  AND suburb != '' )
								OR ( p.field = 51 AND p.value = '" . $prop->area . "' AND p.value != '' )
								OR ( p.field = 47 AND p.value = '" . $prop->price_band . "' AND p.value != '' ) )
								AND p2.published='1' AND p2.approved='1'
								AND (p2.publish_up = '0000-00-00 00:00:00' OR p2.publish_up <= NOW())
								AND (p2.publish_down = '0000-00-00 00:00:00' OR p2.publish_down >= NOW())
								AND p2.id != " . $prop->id . "
								LIMIT 10
								");
		$ids = $database->loadResultArray();
		
		
		//echo $database->_sql;
		if (count($ids) > 0) {
			
			?>
			<h3 class="rel-props"><?php echo _HP_PEOPLE_ALSO_VIEWED; ?></h3>
			<script type="text/javascript" src="<?php echo $mosConfig_live_site . '/templates/' . $mainframe->getTemplate() . '/lib/js/addons/slideshow.js' ?>"></script>
			<div id="fprop-wrapper">
			<ul>
			<?php
			echo "\n";
			
			foreach ($ids as $id) {
				
				$where[0] = "p.id='".$id."'";
				HP_getProperty('listing', $where, $p, $caption);
				
				if ($p[0]->thumb <> '') {
					$img = $p[0]->thumb;
				} else {
					$img = $hp_img_noimage_thumb;
				}
				
				$link = sefRelToAbs("index.php?option=com_hotproperty&amp;task=view&amp;id=".$p[0]->id."&amp;Itemid=$Itemid");
				$sep = strstr($link,'?') ? '&amp;' : '?';
				if($dateFrom && $dateTo){
					$link = $link.$sep.$dateFromStr.'&amp;'.$dateToStr;
				}
				
				?>
				<li class="fp_prop" id="fprop-<?php echo $p[0]->id ?>">
					<div class="fp_thumb">
				    	<img src="/includes/phpthumb/phpThumb.php?zc=1&amp;q=100&amp;w=206&amp;h=220&amp;src=<?php echo $hp_imgdir_standard . $img; ?>" alt="<?php echo $p[0]->title; ?>" />
					</div>
					<div class="fp_more">
						<h4><?php echo $p[0]->name; ?></h4>
						<p>
						<?php echo strip_tags(hotproperty_HTML::neat_trim($p[0]->intro_text, 125))."<br />"; ?>
						</p>
						<p class="readmore"><a href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&task=view&id=" . $p[0]->id . "&Itemid=$Itemid"); ?>"><?php echo _HP_MORE_DETAILS; ?></a></p>
					</div>
				</li>
				
				<?php
			}
			
			?>
			</ul>
			</div>
			<script type="text/javascript">
	
				jQuery(function($){	
					
					$("#fprop-wrapper").slideshow({
						autoplay 	: true,
						controls 	: true,
						animation 	: "horizontal",
						numeric 	: true,
						pause 		: 9999,
						speed 		: 2000
					});
					
				});
	
			</script>
			<?php
			
		}
		
	}
	
	/**
	 * Embeds youtube video on page using url
	 * @param object $prop Object containing property details
	 * @return nothing
	 */
	function showYoutubeVid(&$prop) {
		
		global $mainframe, $mosConfig_live_site;
		
		if (isset($prop->youtube) && $prop->youtube != '') {
			
			$prop->youtube = ereg_replace("(https?)://", "", $prop->youtube);
			$pathArray = split("/", $prop->youtube);
			$vidID = $pathArray[count($pathArray) - 1];
			$vidID = ereg_replace("watch\?v=", "", $vidID);
			$vidID = preg_replace('/\&.*/', '', $vidID);
			
			$mainframe->addCustomheadTag("<link rel='stylesheet' href='" . $mosConfig_live_site . "/templates/" . $mainframe->getTemplate() . "/lib/js/colorbox/colorbox.css' type='text/css' media='screen' />");
	    	$mainframe->addCustomheadTag("<script src='" . $mosConfig_live_site . "/templates/" . $mainframe->getTemplate() . "/lib/js/colorbox/jquery.colorbox-min.js' type='text/javascript'></script>");
			
			?>
			
			<a href="http://www.youtube.com/embed/<?php echo $vidID; ?>?rel=0&amp;wmode=transparent" id="btn-video">View Video</a>
			<?php /*
			<iframe width="320" height="212" src="http://www.youtube.com/embed/<?php echo $vidID; ?>" frameborder="0" allowfullscreen></iframe>
			*/?>
			<script type="text/javascript">
				jQuery(function($){
					$("#btn-video").colorbox({iframe:true, innerWidth:425, innerHeight:344});
				});
			</script>
			<?php
			
		}
		
	}
	
	/**
	* New properties and List all properties
	*/
	
	function showNewProperties(&$prop,&$caption) {
		?>
		<div id="con_asearch1">
			<div id="heading_AdvSearch">
				<div id='hp_pathway'>
					<a href="<?php echo sefRelToAbs("index.php"); ?>"><?php echo _HP_COM_PATHWAY_MAIN; ?></a>&nbsp;<?php echo _HP_ARROW ?>
					&nbsp;
					New Properties
				</div>
	
			</div>
			<div id="hp_searchresult_con">
			   <?php
                hotproperty_HTML::list_properties($prop, $caption);
                ?>
                <div style="clear:both;">&nbsp; </div>
			</div>
            <div style="clear:both;">&nbsp; </div>
		</div>
		<?php
    }
	
	/***
	 * Common Routine to display Agent's Info
	 **/
	function show_AgentInfo($agents) {
		global $mosConfig_live_site, $hp_imgdir_agent, $task, $Itemid, $my;

		if (empty($agents)) {
			echo _HP_AGENT_ERROR_EMPTY;
		} else {
			foreach($agents AS $agent) {
		?>
		<div class="hp_view_agent">
			<?php if (!empty($agent->photo)) { ?>
			<div id="hp_view_agent_photo">
				<?php if ($task <> "viewagent") { ?>
				<a href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&task=viewagent&id=$agent->id&Itemid=$Itemid"); ?>">
				<img border="0" src="<?php echo $mosConfig_live_site.$hp_imgdir_agent.$agent->photo; ?>" alt="<?php echo $agent->name; ?>" />
				</a>
				<?php } else { ?>
				<img border="0" src="<?php echo $mosConfig_live_site.$hp_imgdir_agent.$agent->photo; ?>" alt="<?php echo $agent->name; ?>" />
				<?php } ?>
			</div>
			<?php } ?>
			<div id="hp_view_agent_details">
				<span id="hp_caption_agentname"><?php if ($task <> "viewagent") { ?><a id="hp_caption_agentname" href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&task=viewagent&id=$agent->id&Itemid=$Itemid"); ?>"><?php echo $agent->name; ?></a><?php } else { ?><?php echo $agent->name; ?><?php } ?></span>
				<?php
				# Show an edit icon to allow user to edit their own profile
				if ($agent->user == $my->id && $agent->user > 0 && $my->id > 0) { ?>
				&nbsp;
				<a href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&task=editagent&Itemid=$Itemid"); ?>" title="<?php echo _E_EDIT; ?>"><img src="images/M_images/edit.png" width="13" height="14" align="middle" border="0" alt="<?php echo _E_EDIT; ?>" /></a>
				<?php } ?>
				<br />
				<?php
				# Prevent displaying Company if user is at viewco
				if ($task<>"viewco") { ?>
				<span class="hp_caption"><?php echo _HP_AGENT_COMPANY; ?>:</span> <a href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&task=viewco&id=$agent->companyid&Itemid=$Itemid"); ?>"><?php echo $agent->company; ?></a><br />
				<?php } ?>
				<?php
				# Display Mobile number if not empty
				if (!empty($agent->mobile)) { ?>
				<span class="hp_caption"><?php echo _HP_AGENT_MOBILE; ?>:</span> <?php echo $agent->mobile; ?><br />
				<?php } ?>
				<?php
				# Display Number of properties
				?>
				<span class="hp_caption"><?php echo _HP_PROPERTIES; ?>:</span> <?php echo $agent->properties; ?><br />
				<?php
				# Display "Send email link" if user at viewagent or viewco
				if( ($task == "viewagent" || $task == "viewco") && !empty($agent->email) ) { ; ?>
				<a href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&task=viewagentemail&id=$agent->id&Itemid=$Itemid"); ?>"><?php echo _HP_AGENT_SENDEMAIL; ?></a><br />
				<?php } ?>
				<?php if (!empty($agent->desc)) { ?>
				<div id="hp_view_agent_desc"><?php echo $agent->desc; ?></div>
				<?php } ?>
			</div>
		</div>
		<br class="clearboth" />
		<?php
			} // End Foreach
		} // End If
	}

	/***
	 * Common Routine to display Company's Info
	 **/
	function show_CoInfo($companies) {
		global $mosConfig_live_site, $hp_imgdir_company, $task, $Itemid;

		foreach($companies AS $co) {
	?>
	<div class="hp_view_co">
		<?php if (!empty($co->photo)) { ?>
		<div id="hp_view_co_photo"><img src="<?php echo $mosConfig_live_site.$hp_imgdir_company.$co->photo; ?>" alt="<?php echo $co->name; ?>" /></div>
		<?php } ?>
		<div id="hp_view_co_details">
			<span id="hp_caption_coname">
			<?php if ($task <> "viewco") { ?><a id="hp_caption_coname" href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&task=viewco&id=$co->id&Itemid=$Itemid"); ?>"><?php echo $co->name; ?></a><?php } else {
				echo $co->name;
			} ?></span>
			<br />
			<div id="hp_co_addr">
			<?php
				if (trim($co->address)!="") {
					echo "$co->address <br />";
				}
				if ((trim($co->suburb)!="") && (trim($co->state)!="") && (trim($co->postcode)!="")) {
					echo "$co->suburb, $co->state, $co->postcode <br />";
				 } elseif ((trim($co->suburb)!="") && (trim($co->state)!="")) {
					echo "$co->suburb, $co->state <br />";
				} elseif ((trim($co->suburb)!="") && (trim($co->postcode)!="")) {
					echo "$co->suburb, $co->postcode <br />";
				} elseif ((trim($co->state)!="") && (trim($co->postcode)!="")) {
					echo "$co->state, $co->postcode <br />";
				} elseif ((trim($co->state)!="")) {
					echo "$co->state <br />";
				} elseif ((trim($co->suburb)!="")) {
					echo "$co->suburb <br />";
				} elseif ((trim($co->postcode)!="")) {
					echo "$co->postcode <br />";
				}
				if (trim($co->country)!="") {
					echo "$co->country <br />";
				}
			?></div>
			<?php if (!empty($co->telephone)) { ?>
			<span class="hp_caption"><?php echo _CONTACT_TELEPHONE; ?></span><?php echo $co->telephone; ?><br />
			<?php }?>
			<?php if (!empty($co->fax)) { ?>
			<span class="hp_caption"><?php echo _CONTACT_FAX; ?></span><?php echo $co->fax; ?><br />
			<?php }?>
			<?php if (!empty($co->website)) { ?>
			<span class="hp_caption"><?php echo _HP_CO_WEBSITE; ?> </span><a href="<?php echo $co->website; ?>" target="_blank"><?php echo $co->website; ?></a><br />
			<?php }?>
			<?php if ($task <> "viewcoemail") { ?>
			<a href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&task=viewcoemail&id=$co->id&Itemid=$Itemid"); ?>"><?php echo _HP_CO_SENDEMAIL; ?></a>
			<?php } ?>
			<?php if (!empty($co->desc)) { ?>
			<p />
			<?php echo $co->desc; ?>
			<?php } ?>
		</div>
	</div>
	<br class="clearboth" />
	<?php
		}
	}

	/***
	 * Display Email form
	 ***/
	function show_EmailForm($subject, $id) {
		global $Itemid;

		if ($subject <> "agent" && $subject <> "property" && $subject <> "company") return false;

		$validate = josSpoofValue();
	?>
		<form method="POST" action="<?php echo sefRelToAbs("index.php?option=com_hotproperty&task=sendenquiry&id=".$id."&Itemid=".$Itemid); ?>">

		<span class="hp_caption"><?php echo _CMN_NAME; ?>: </span><br />
		<input type="text" class="inputbox" name="hp_name" size="24" /><br />

		<span class="hp_caption"><?php echo _CMN_EMAIL; ?>: </span><br />
		<input type="text" class="inputbox" name="hp_email" size="30" /><br />

		<span class="hp_caption"><?php echo _HP_CONTACTNUMBER; ?>: </span><br />
		<input type="text" class="inputbox" name="hp_contactnumber" size="30" /><br />

		<span class="hp_caption"><?php echo _ENQUIRY; ?>: </span> <br />
		<textarea rows="4" cols="40" class="inputbox" name="hp_enquiry"></textarea><br />
		<br class="clearboth" />
		<input type="hidden" name="sbj" value="<?php echo $subject; ?>" />
		<input type="hidden" name="<?php echo $validate; ?>" value="1" />
		<input class="button" type="submit" value="<?php echo _HP_SENDENQUIRY; ?>" />
		</form>
	<?php
	}

	function sendEmailForm($id, $title) {
		global $mosConfig_sitename;

		$validate = josSpoofValue();
?>
<script language="javascript" type="text/javascript">
	function submitbutton() {
		var form = document.frontendForm;

		// do field validation
		if (form.email.value == "" || form.youremail.value == "") {
			alert( '<?php echo addslashes( _EMAIL_ERR_NOINFO ); ?>' );
			return false;
		}
		return true;
	}
	</script>
<title><?php echo $mosConfig_sitename; ?> :: <?php echo $title; ?></title>
<body class="contentpane">
<form action="index2.php?option=com_hotproperty&task=emailsend" name="frontendForm" method="POST" onSubmit="return submitbutton();">
  <table cellspacing="2" cellpadding="2" border="0">
    <tr>
      <td colspan="2"><?php echo _EMAIL_FRIEND; ?></td>
    </tr>
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td width="130"><?php echo _EMAIL_FRIEND_ADDR; ?></td>
      <td><input type="text" name="email" class="inputbox" size="25"></td>
    </tr>
    <tr>
      <td height="27"><?php echo _EMAIL_YOUR_NAME; ?></td>
      <td><input type="text" name="yourname" class="inputbox" size="25"></td>
    </tr>
    <tr>
      <td><?php echo _EMAIL_YOUR_MAIL; ?></td>
      <td><input type="text" name="youremail" class="inputbox" size="25"></td>
    </tr>
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2"><input type="submit" name="submit" class="button" value="<?php echo _BUTTON_SUBMIT_MAIL; ?>">
        &nbsp;&nbsp; <input type="button" name="cancel" value="<?php echo _BUTTON_CANCEL; ?>" class="button" onClick="window.close();"></td>
    </tr>
  </table>
 		<input type="hidden" name="<?php echo $validate; ?>" value="1" />
  <input type="hidden" name="id" value="<?php echo $id; ?>">
</form>
<?php
	}

	function emailSent( $to ) {
		global $mosConfig_sitename;
		?>
		<br />
		<?php echo _EMAIL_SENT." $to";?>
		<br />
		<br />
		<?php if (!$hide_js) { php?>
		 <a href='javascript:window.close();'>
				<span class="small"><?php echo _PROMPT_CLOSE;?></span>
		 </a>
		<?php
		}
	}
	/***
	 * Common Routine to display properties
	 ***/
	function original_list_properties(&$prop, &$caption) {
		global $Itemid, $task, $my, $mosConfig_live_site, $mosConfig_absolute_path;
		global $hp_imgdir_thumb, $hp_currency, $hp_imgsize_thumb, $hp_img_noimage_thumb, $hp_thousand_sep, $hp_dec_point, $hp_link_open_newwin, $hp_show_thumb, $hp_dec_string, $hp_thousand_string;

		if(empty($prop)) {
			?>
			<div id="hp_error_empty">
				<?php echo _HP_PROP_ERROR_EMPTY; ?>
			</div>
			<?php
		} else {
			foreach($prop AS $p) {
				if ($p->thumb <> '') {
					$thumb_imgsize = GetImageSize ($mosConfig_absolute_path.$hp_imgdir_thumb.$p->thumb);
				} else {
					$thumb_imgsize = GetImageSize ($mosConfig_absolute_path.$hp_imgdir_thumb.$hp_img_noimage_thumb);
				}
			?>
		<div class="hp_prop">
					<?php if ($hp_show_thumb) { ?>
					<div class="img_thumb"><a href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&task=view&id=$p->id&Itemid=$Itemid"); ?>"><img <?php echo $thumb_imgsize[3]; ?> border="0" src="<?php
						if ($p->thumb <> '') echo $mosConfig_live_site.$hp_imgdir_thumb.$p->thumb;
						else echo $mosConfig_live_site.$hp_imgdir_thumb.$hp_img_noimage_thumb;
					?>" alt="<?php echo $p->thumb_title ?>" /></a></div>
					<?php } ?>
					<div class="hp_details">
                    		<?php
								/*<a class="hp_title" href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&task=view&id=$p->id&Itemid=$Itemid"); ?>"><?php echo $p->name; ?></a> */
                            ?>
							<?php
							# Show an edit icon to allow user to edit the property
							if ($p->user == $my->id && $p->user > 0 && $my->id > 0) { ?>
							&nbsp;
							<a href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&task=editprop&id=$p->id&Itemid=$Itemid"); ?>" title="<?php echo _E_EDIT; ?>"><img src="images/M_images/edit.png" width="13" height="14" align="middle" border="0" alt="<?php echo _E_EDIT; ?>" /></a>
							<?php }
							?><br />
							<?php
								$j=0;
								foreach($p as $key => $value) {
									if ( array_key_exists($key,$caption) && ($caption[$key]->name <> 'name' && $caption[$key]->name <> 'thumb' && $caption[$key]->name <> 'thumb_title' && $caption[$key]->name <> '' && $value <> "") )
										# Replace '|' with a comma for checkbox and select multiple fields
										if ($caption[$key]->field_type == "checkbox" || $caption[$key]->field_type == "selectmultiple") {
											if (!$caption[$key]->hideCaption) echo '<span class="hp_caption">'.$caption[$key]->caption."</span>: ";
											echo str_replace("|",", ",$value)."<br />";
										# Web Link
										} elseif ($caption[$key]->field_type == "link") {

											// Evaluate mambot style data
											$value = str_replace( '{property_id}', $p->id, $value );
											$value = str_replace( '{type_id}', $p->typeid, $value );
											$value = str_replace( '{agent_id}', $p->agentid, $value );
											$value = str_replace( '{company_id}', $p->companyid, $value );
											$value = str_replace( '{Itemid}', $Itemid, $value );

											if (!$caption[$key]->hideCaption) {
											?><span class="hp_caption"><?php echo $caption[$key]->caption; ?></span>: <?php }
											echo $caption[$key]->prefix_text;
											$link = explode("|",$value);
											if (count($link) == 1 && ( substr(trim($link[0]),0,4) == "http" || substr(trim($link[0]),0,5) == "index" ) ) {
												?><a <?php echo ($hp_link_open_newwin) ? 'target="_blank" ': ''; ?>href="<?php echo $link[0]; ?>"><?php echo $link[0]; ?></a><?php
											} elseif (count($link) > 1 && ( substr(trim($link[1]),0,4) == "http" || substr(trim($link[1]),0,5) == "index" ) ) {
												?><a <?php echo ($hp_link_open_newwin) ? 'target="_blank" ': ''; ?>href="<?php echo $link[1]; ?>"><?php echo $link[0]; ?></a><?php
											} else {
												echo $value;
											}
											echo $caption[$key]->prefix_text."<br />";

										} else {
												# Do not display agent field when viewing agent's properties
												# Do not display type field when viewing type's properties
												if ( !($key == "agent" && $task == "viewagent") && !($key =="type" && $task == "viewtype") ) {
													# Show agent link
													if ($key == "agent") {
														if (!$caption[$key]->hideCaption) {
														?><span class="hp_caption"><?php echo $caption[$key]->caption; ?></span>: <?php
														}
														?><a href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&task=viewagent&id=$p->agentid&Itemid=$Itemid"); ?>"><?php echo $caption[$key]->prefix_text.$value.$caption[$key]->append_text; ?></a><br /><?php
													# Show company link
													} elseif ($key == "company") {
														if (!$caption[$key]->hideCaption) {
														?><span class="hp_caption"><?php echo $caption[$key]->caption; ?></span>: <?php }
														?><a href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&task=viewco&id=".$p->companyid."&Itemid=$Itemid"); ?>">	<?php echo $caption[$key]->prefix_text.$value.$caption[$key]->append_text; ?></a><br /> <?php
													# Show type link
													} elseif ($key == "type") {
														if (!$caption[$key]->hideCaption) {
														?><span class="hp_caption"><?php echo $caption[$key]->caption; ?></span>: <?php }
														?><a href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&task=viewtype&id=".$p->typeid."&Itemid=$Itemid"); ?>">	<?php echo $caption[$key]->prefix_text.$value.$caption[$key]->append_text; ?></a><br /> <?php
													# Show Price with proper formating
													} elseif ($key == "price") {
														if (!$caption[$key]->hideCaption) {
														?><span class="hp_caption"><?php echo $caption[$key]->caption; ?></span>:<?php }
														?><span class="hp_price"><?php echo $caption[$key]->prefix_text.$hp_currency." ".number_format($value, $hp_dec_point, $hp_dec_string, ($hp_thousand_sep) ? $hp_thousand_string:'').$caption[$key]->append_text; ?></span><br /> <?php
													# Show Featured as Yes/No instead of 1/0
													} elseif ($key == "featured") {
														if (!$caption[$key]->hideCaption) {
															echo '<span class="hp_caption">'.$caption[$key]->caption."</span>: ";
														}
														echo '<span class="hp_desc" id="'.$key.'_'.$j.'">';
														echo $caption[$key]->prefix_text
															.	( ($value == '1') ? _CMN_YES : _CMN_NO )
															.	$caption[$key]->append_text
															.	'</span>'
															.	"<br />";
													# Else, show normal 'caption: value'
													} else {
														
														/*
														check whether showing description (resort_location key) - if so, close hp_details div and open a new one to enclose desc only
														*/
														if ($key=='location_resort') {
															echo '</div>';
															echo '<div id="description">';
														}
														
														if (!$caption[$key]->hideCaption) {
															echo '<span class="hp_caption">'.$caption[$key]->caption."</span>: ";
														}
														echo '<span class="hp_desc" id="'.$key.'_'.$j.'">';
														echo $caption[$key]->prefix_text
															.	( ($key=="price") ? $hp_currency." " : "" )
															.	$value
															.	$caption[$key]->append_text
															.	'</span>'
															.	"<br />";

													}
												}
										}
									$j++;
								}
							?>
					</div>
					<br class="clearboth" />
				</div>
			<?php
			} // End Foreach
		} // End If
	}
	
	/***
	 * Hacked version of list_properties to output as per designs
	 ***/
	function list_properties(&$prop, &$caption) {
		global $Itemid, $task, $my, $mosConfig_live_site, $mosConfig_absolute_path, $mainframe;
		global $hp_imgdir_thumb, $hp_imgdir_standard, $hp_currency, $hp_imgsize_thumb, $hp_img_noimage_thumb, $hp_thousand_sep, $hp_dec_point, $hp_link_open_newwin, $hp_show_thumb, $hp_dec_string, $hp_thousand_string;
		
		$mainframe->addCustomheadTag("<script type='text/javascript' src='$mosConfig_live_site/components/com_hotproperty/includes/js/wishlist.js'></script>");
		
		if(empty($prop)) {
			?>
			<div id="hp_error_empty">
				<?php echo _HP_PROP_ERROR_EMPTY; ?>
			</div>
			<?php
		} else {
			?>
            <?php /*<h1 class="componentheading"><?php echo _HP_SEARCH_HEADING; ?></h1>*/?>
            <?php 
            	$prop_type = mosGetParam($_REQUEST,'prop_type');
            	if($prop_type){ 
            	?><div id='hp_searched_for'><strong><?php echo _HP_SEARCH_SEARCHED_FOR; ?></strong> <?php echo is_array($prop_type) ? implode($prop_type,', ') : $prop_type ?></div>
            <?php } ?>
            <?php
			$highlight = 0;
			$dateFrom = mosGetParam($_REQUEST,'dateFrom', array());
			$dateTo = mosGetParam($_REQUEST,'dateTo', array());
			
			if(is_array($dateFrom)){
				$dateFromQ = array();
				foreach($dateFrom as $k => $v){
					if($v){
						$dateFromQ[] = "dateFrom[$k]=$v";
					}
				}
				$dateFromStr = count($dateFromQ) == 3 ? implode('&amp;',$dateFromQ) : '';
			}else{
				$dateFromStr = "dateFrom=$dateFrom";
			}
			
			if(is_array($dateTo)){
				$dateToQ = array();
				foreach($dateTo as $k => $v){
					if($v){
						$dateToQ[] = "dateTo[$k]=$v";
					}
				}
				$dateToStr = count($dateTo) == 3 ? implode('&amp;',$dateToQ) : '';
			}else{
				$dateToStr = "dateFrom=$dateFrom";
			}
			
			foreach($prop as $p) {
				if ($p->thumb <> '') {
					$thumb_imgsize = GetImageSize ($mosConfig_absolute_path.$hp_imgdir_thumb.$p->thumb);
				} else {
					$thumb_imgsize = GetImageSize ($mosConfig_absolute_path.$hp_imgdir_thumb.$hp_img_noimage_thumb);
				}
				$highlight = !$highlight;
				
				$link = sefRelToAbs("index.php?option=com_hotproperty&amp;task=view&amp;id=$p->id&amp;Itemid=$Itemid");
				$sep = strstr($link,'?') ? '&amp;' : '?';
				if($dateFrom && $dateTo && $dateFromStr != "" && $dateToStr != "") {
					$link = $link.$sep.$dateFromStr.'&amp;'.$dateToStr;
				}

				$features = explode('|', $p->prop_type);
				?>
				<div class="hp_prop hp_prop_row<?php echo (int) $highlight ?>">
					
						
					<?php if ($hp_show_thumb) { ?>
					<div class="img_thumb">
						<a href="<?php echo $link; ?>"><img border="0" src="/includes/phpthumb/phpThumb.php?zc=1&amp;w=235&amp;h=160&amp;src=<?php
						if ($p->thumb <> '') echo $hp_imgdir_standard.$p->thumb;
							else echo $hp_imgdir_thumb.$hp_img_noimage_thumb;
						?>" alt="<?php echo $p->thumb_title ?>" /></a>
					</div>
					<?php } ?>
				
					<?php
					//We're not allowing owners to edit properties, remove && 0 to turn on
                    if ($p->user == $my->id && $p->user > 0 && $my->id > 0 && 0) { ?>
						<a href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&task=editprop&id=$p->id&Itemid=$Itemid"); ?>" title="<?php echo _E_EDIT; ?>">Edit Property</a>
                    <?php
					}
					?>
					
					<div class="hp_details">

						<?php
						echo $p->price;
						?>

						<h2><?php echo $p->name; ?></h2>
						<span class="area"><?php echo $p->area ? $p->area : _HP_SEARCH_AREA; ?></span>
						<div class="hp_more">
							<?php							
								if(trim($p->keypoints)){
									echo $p->keypoints;
								}else{
									
									preg_match_all('/<li>(.*)<\/li>/m', $p->bullets, $matches);
															
									if(count($matches[1]) > 0){
										$lis = array_slice($matches[1], 0, 5);
										echo '<ul>';
										foreach($lis AS $li) echo '<li>'.strip_tags($li).'</li>';
										echo '</ul>';
									}
								}
							?>
						</div>
						<div class="hp_features">
							<ul>
								<li class="bed"><?php echo strip_tags($p->bedrooms).' '._HP_SEARCH_BEDS; ?></li>
								<?php /*<?php if(in_array('Waterfront Rentals', $features)) { ?>
								<li class="waterfront">waterfront</li>
								<?php } ?>
								<?php if(in_array('Villas with Private Pools', $features)) { ?>
								<li class="privpool">private pool</li>
								<?php } ?>
								<?php if ($p->min_to_beach) { ?>
								<li class="beach"><?php echo $p->min_to_beach; ?> min to beach</li>
								<?php } ?>*/ ?>
							</ul>
						</div>
						<a class="viewlink" href="<?php echo $link; ?>"><?php echo _HP_SEARCH_MORE_LINK; ?></a>
					</div>                         

                    <!--<div class="hp_addshortlist">
                    	<?php //hotproperty_HTML::getWishListButton($p->id); ?>
                    </div>	-->
					<div class="clearboth"></div>			
							
				
			</div>
			
			<?php
			} // End Foreach
		} // End If
	}
	
	/*
	* Output an error or list of errors
	*/
	function throwError($msg) {
		if (is_array($msg)) {
			foreach ($msg as $message) {
				echo '<p class="error">'.$message.'</p>';
			}
		} else {
			echo '<p class="error">'.$msg.'</p>';
		}
	}
	
	
	function recommend_Prop(&$prop, &$caption, &$images, &$agent) {
		global $Itemid, $task, $my, $mosConfig_live_site, $mosConfig_absolute_path;
		global $hp_imgdir_thumb, $hp_currency, $hp_imgsize_thumb, $hp_img_noimage_thumb, $hp_thousand_sep, $hp_dec_point, $hp_link_open_newwin, $hp_show_thumb, $hp_dec_string, $hp_thousand_string;

		if(empty($prop)) {
			?>
			<div id="hp_error_empty">
				<?php echo _HP_PROP_ERROR_EMPTY; ?>
			</div>
			<?php
		} else {
			?>
			<div id="recommend">You Might Also Like</div>
			<div class="hp_prop">
				<div class="corner_tl">
					<div class="corner_tr">
						<div class="corner_bl">
							<div class="corner_br">
								<?php
								foreach($prop AS $p) {
									if ($p->thumb <> '') {
										$thumb_imgsize = GetImageSize ($mosConfig_absolute_path.$hp_imgdir_thumb.$p->thumb);
									} else {
										$thumb_imgsize = GetImageSize ($mosConfig_absolute_path.$hp_imgdir_thumb.$hp_img_noimage_thumb);
									}
									?>
									<div class="recommends">
					      
										<?php if ($hp_show_thumb) { ?>
										<div class="img_thumb"><a href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&task=view&id=$p->id&Itemid=$Itemid"); ?>"><img <?php echo $thumb_imgsize[3]; ?> border="0" src="<?php
											if ($p->thumb <> '') echo $mosConfig_live_site.$hp_imgdir_thumb.$p->thumb;
											else echo $mosConfig_live_site.$hp_imgdir_thumb.$hp_img_noimage_thumb;
										?>" alt="<?php echo $p->thumb_title ?>" /></a>
										<p class"clearboth" style="font-weight: bold; font-size: 90%; color:#000">From <?php echo $p->price_range; ?></p>
										</div>
										<?php } ?>
										
										<div class="hp_details">
											<ul class="info">
												<li><h4 class="location"><?php echo $p->suburb; ?></h4></li>
								                <?php 
												echo "<li>".$p->bedrooms." bedrooms,</li>";
												echo "<li>".$p->Bathrooms." bathrooms,</li>";
												if (!empty($p->pool)) {
													echo "<li>swimming pool,</li>";	
												}
												if (!empty($p->dist_beach)) {
													echo "<li>beach is ".$p->dist_beach."</li>";
												}
												?>
												<li class="readmore"><a href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&task=view&id=$p->id&Itemid=$Itemid"); ?>">More details</a></li>
											</ul>
										</div>
									</div>
								<?php
								} // End Foreach
								?>
								<div class='clearboth'></div>
							</div>
						</div>
					</div>
				</div>			
			<?php
		} // End If
	}
	
	function getWishlistButton($id){
		global $database, $my;
				
		if($my->id){
			//Check database to see if the property exists in wishlist
			$database->setQuery("SELECT count(prop_id) FROM #__hp_wishlist WHERE prop_id = '$id' AND user_id = '$my->id'");

			if($database->loadResult() == 1){
				//If it does, return "remove" button
				$func = 'del';
				$class = 'wish_list_bttn_del';
				$text= 'Remove from Shortlist';
			}else{
				//If not return add button
				$func = 'add';
				$class = 'wish_list_bttn_add';
				$text= 'Add to Shortlist';
			}		
			?>
			<div class="hp_wishlist">
				<a class="<?php echo $class ?>" href="index.php?option=com_hotproperty&task=wishlist&id=<?php echo $id; ?>&func=<?php echo $func ?>"><?php echo $text ?></a>
			</div><?php
		}
	}
	
		function getFavouriteTab($id){
		global $database, $my;
				
		if($my->id){
			//Check database to see if the property exists in wishlist
			$database->setQuery("SELECT count(prop_id) FROM #__hp_wishlist WHERE prop_id = '$id' AND user_id = '$my->id'");

			if($database->loadResult() == 1){
				//If it does, return "remove" button
				$func = 'del';
				$class = 'fav_tab_del';
				$text= 'Remove From Shortlist';
			}else{
				//If not return add button
				$func = 'add';
				$class = 'fav_tab_add';
				$text= 'Add To Shortlist';
			}		
			?>
				<a class="<?php echo $class ?>" href="index.php?option=com_hotproperty&task=wishlist&id=<?php echo $id; ?>&func=<?php echo $func ?>"><?php echo $text ?></a>
			<?php
		}
	}

	function treeHTML ($id, $children, $types_hotproperty, $levelmax=9999, $level=0)
	{
		global $database, $Itemid, $hp_fp_show_nbr, $hp_fp_show_empty, $hp_fp_depth_count, $hp_fp_depth_calculous;

		$type_O = new mosHPPropTypes($database);
		$type_O->load($id);

		# Condition d'arret sur l'existence d'un fils, le niveau max
		if (@$children[$id] && $level<$levelmax)
		{
			echo '<ul>';
			$s=0;
			foreach ($children[$id] as $child)
			{
				$child_O = new mosHPPropTypes($database);
				$child_O->load($child->id);

				if($child_O->countProps_R($included=true, $published=1, $forced=false) || $hp_fp_show_empty)
				{
					$s++;

					//on affiche ou non(en fonction de la config) le nombre de propriétés:
					if($hp_fp_show_nbr)
					{
						// pour les catégories de niveaux supérieurs à la limitation d'affichage de profondeur($hp_fp_depth_count), on calucule le nombre de produits sur la profondeur.
						if($level>=$hp_fp_depth_count-1 && $hp_fp_depth_calculous)
							$nbr=$child_O->countProps_R($included=true, $published=1, $forced=false);
						else
							$nbr=$child_O->countProps($published=1);
						$nbr=($nbr>0) ? ' ('.$nbr.')' : ''; // on affiche pas (0) mais rien
					}
					else
					{
						$nbr='';
					}
					echo '<li class="con_types3"><a class="types_title" title="'.$child->desc.'" href="'.sefRelToAbs("index.php?option=com_hotproperty&task=viewtype&id=$child->id&Itemid=$Itemid").'">'.$child->name.'</a>'.$nbr.'<br />';
					echo '<span class="types_desc">'.$child->desc.'</span>';

					# Liste des produits de la catégorie:
					if($child_O->countProps($published=1) > 0)
					{
						echo '<ul class="types_hp">';
							foreach($types_hotproperty[$child->id] AS $row_hp)
							{
								if ($row_hp->name <> "" && $row_hp->id <> "")
									echo '<li><a href="'.sefRelToAbs("index.php?option=com_hotproperty&task=view&id=$row_hp->id&Itemid=$Itemid").'">'.$row_hp->name.'</a></li>';
							}
						echo '</ul>';
					}
					# Affichage de 'Plus...' si plus de 3 propriétés:
					if ($child_O->countProps($published=1) > 3)
						echo '<a href="'.sefRelToAbs("index.php?option=com_hotproperty&task=viewtype&id=$child->id&Itemid=$Itemid").'">'._HP_MORE.'</a>';


					hotproperty_HTML::treeHTML ($child->id, $children, $types_hotproperty, $levelmax, $level+1);

				}

			}
			echo ($s==0) ? '<li></li></ul>' : "</li></ul>";
		}
	}
		
}
?>
