<?php
// $Id: rss.hotproperty.php
/**
* Hot Property RSS Feeds
*
* @package Hot Property 0.9
* @copyright (C) 2004 Lee Cher Yeong
* @url http://www.Mosets.com/
* @author Lee Cher Yeong <cy@mosets.com>
**/

// ensure this file is being included by a parent file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

//global $database, $mainframe, $my, $Itemid;

$now = date( "Y-m-d H:i:s", time()+$mosConfig_offset*60*60 );

$menu= new mosMenu( $database );
$menu->load( 1 );
$params = mosParseParams( $menu->params );

$count = isset( $params->count ) ? $params->count : 6;
$orderby = @$params->orderby;

switch (strtolower( $orderby )) {
	case 'latest':
		$orderby = "p.created DESC";
		break;
	case 'hottest':
		$orderby = "p.hits DESC";
		break;
	default:
		$orderby = "p.created DESC";
		break;
}

$sql = "SELECT p.id, p.name, p.intro_text FROM #__hp_properties AS p"
		. "\nLEFT JOIN #__hp_prop_types AS t ON p.type = t.id"
		. "\nLEFT JOIN #__hp_featured AS f ON f.property = p.id"
		. "\nWHERE p.published='1' AND p.approved='1' AND t.published='1'"
		. "\n	AND (publish_up = '0000-00-00 00:00:00' OR publish_up <= '$now')"
		. "\n	AND (publish_down = '0000-00-00 00:00:00' OR publish_down >= '$now')"
	  . "\nORDER BY ".$orderby
    . ($count ? " LIMIT $count" : "");
    
$database->setQuery( $sql );

$rows = $database->loadObjectList();

header('Content-type: application/xml');
$encoding = split("=", _ISO);
echo "<?xml version=\"1.0\" encoding=\"".$encoding[1]."\"?>"; ?>
<!DOCTYPE rss PUBLIC "-//Netscape Communications//DTD RSS 0.91//EN"
	"http://my.netscape.com/publish/formats/rss-0.91.dtd">
<!-- Hot Property 0.9 RSS Generator Version 1.0 (March 2005) - Mosets Consulting (http://www.mosets.com/ -->
<!-- Copyright (C) 2004-2005 - <?php echo $mosConfig_sitename; ?> -->
<rss version="0.91">
<channel>
<title><?php echo htmlspecialchars($mosConfig_sitename); ?></title>
<link><?php echo $mosConfig_live_site; ?></link>
<description><?php echo $option ?></description>
<language>en-us</language>
<lastBuildDate><?php $date = date("r"); echo "$date";?></lastBuildDate>
	<image>
	<title>Powered by Mambo Open Source 4.5</title>
	<url><?php echo $mosConfig_live_site; ?>/images/M_images/mos_rss.png</url>
	<link><?php echo $mosConfig_live_site; ?></link>
	<width>88</width>
	</image>
<?php
foreach ($rows as $row) {
	echo ("<item>");
	echo ("<title>".htmlspecialchars($row->name)."</title>"."\n");
	echo "<link>";
	if ($mosConfig_sef == "1"){
		echo sefRelToAbs("index.php?option=com_hotproperty&amp;task=view&amp;id=".$row->id);
	} else {
		echo $mosConfig_live_site . "/index.php?option=com_hotproperty&amp;task=view&amp;id=" . $row->id;
	}
	echo "</link>\n";
	$words = $row->intro_text;
	$words = preg_replace("'<script[^>]*>.*?</script>'si","",$words);
	$words = preg_replace('/<a\s+.*?href="([^"]+)"[^>]*>([^<]+)<\/a>/is','\2 (\1)', $words);
	$words = preg_replace('/<!--.+?-->/','',$words);
	$words = preg_replace('/{.+?}/','',$words);
	$words = preg_replace('/&nbsp;/',' ',$words);
	$words = preg_replace('/&amp;/',' ',$words);
	$words = preg_replace('/&quot;/',' ',$words);
	$words = strip_tags($words);
	$words = htmlspecialchars($words);
	echo ("<description>".substr($words,0,100)."...</description>"."\n");
	echo ("</item>"."\n");
}
?>
</channel>
</rss>