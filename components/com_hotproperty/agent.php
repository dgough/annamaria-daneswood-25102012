<?php
// agent.php
/**
* Hot Property
*
* @package Hot Property 0.9
* @copyright (C) 2004 Lee Cher Yeong
* @url http://www.Mosets.com/
* @author Lee Cher Yeong <cy@mosets.com>
**/

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

function editagent() {
	global $database, $my, $lang, $Itemid;
	HP_useDefaultLang( $lang,  "index.php?option=com_hotproperty&task=editagent&Itemid=".$Itemid );

	$agent = new HP_Agent($database);
	$agent->load($my->id);
	
	# Check is this user is an Agent
	if ($agent->user == $my->id && $agent->user > 0 && !empty($agent->id) && $my->id > 0) {
		HP_Agent_HTML::editagent($agent);
	} else {
		mosRedirect( "index.php?option=com_hotproperty" );
	}
}

function saveagent( $option ) {
	global $database, $mosConfig_absolute_path, $Itemid;
	global $hp_img_method, $hp_imgdir_agent, $hp_imgsize_agent, $hp_quality_agent;

	$remove_photo = mosGetParam( $_REQUEST, 'remove_photo', 0 );

	# Prepare database
	$row = new HP_Agent( $database );
	if (!$row->bind( $_POST )) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

	# Check if the login user has already been taken by another agent
	# - removed -

	# Check if this agent has previously uploaded any of his photo. 
	if($row->id > 0) {
		$sql="SELECT photo FROM #__hp_agents WHERE id='".$row->id."'";
		$database->setQUery($sql);
		$old_photo = $database->loadResult();
	}

	# Verifiy photo 
	if ($_FILES['photo']['tmp_name'] != "") {
		# Verify image is supplied 
		if(!isset($_FILES['photo']) || !is_array($_FILES['photo'])) {
			echo "<script> alert('"._HP_IMGUPLOAD_ERR_01."'); window.history.go(-1); </script>\n";
			exit();
		}

		# Verify the file is uploaded
		if (!is_uploaded_file($_FILES['photo']['tmp_name'])) {
			echo "<script> alert('"._HP_AGENT_02."'); window.history.go(-1); </script>\n";
			exit();
		} 

		# Verify this is an acceptable image file (.gif, .jpg, .png, .bmp)
		if ((strcasecmp(substr($_FILES['photo']['name'],-4),".gif")) && (strcasecmp(substr($_FILES['photo']['name'],-4),".jpg")) && (strcasecmp(substr($_FILES['photo']['name'],-4),".png")) && (strcasecmp(substr($_FILES['photo']['name'],-4),".bmp")) ) {
			echo "<script> alert('"._HP_AGENT_03."'); window.history.go(-1); </script>\n";
			exit();
		}

		# No Duplicates!
		if (file_exists($mosConfig_absolute_path.$hp_imgdir_agent.$_FILES['photo']['name'])) {
			echo "<script> alert('"._HP_AGENT_04."'); window.history.go(-1); </script>\n";
			exit();
		}

		if(copy ($_FILES['photo']['tmp_name'], $mosConfig_absolute_path.$hp_imgdir_agent.$_FILES['photo']['name'])) {
			# Generate the Standard image
			if(resize_image($_FILES['photo']['tmp_name'], $mosConfig_absolute_path.$hp_imgdir_agent.$_FILES['photo']['name'], $hp_imgsize_agent, $hp_img_method, $hp_quality_agent)) {
				$row->photo = $_FILES['photo']['name'];
				# SUCCESS!
				# Now delete his previous photo
				if ($old_photo != "") {
					if(!unlink($mosConfig_absolute_path.$hp_imgdir_agent.$old_photo)) {
						echo "<script> alert('"._HP_AGENT_05."'); window.history.go(-1); </script>\n";
						exit();
					}
				}
			} 
		}
	}

	# Remove current photo - No photo for this agent!
	if ($remove_photo) {
		$row->photo = '';
		if(!unlink($mosConfig_absolute_path.$hp_imgdir_agent.$old_photo)) {
				echo "<script> alert('"._HP_IMGUPLOAD_ERR_05."'); window.history.go(-1); </script>\n";
				exit();
		}
	}

	if (!$row->store()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

	mosRedirect( "index.php?option=$option&task=viewagent&id=$row->id&Itemid=$Itemid", _HP_AGENT_MSG_01 );
}

function cancelagent($option) {
	global $Itemid, $my, $database;

	$agent = new HP_Agent($database);
	$agent->load($my->id);

	mosRedirect( "index.php?option=$option&task=viewagent&id=".$agent->id."&Itemid=".$Itemid );
}	

?>