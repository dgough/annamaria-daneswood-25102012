<?php
// agent.html.php
/**
* Hot Property
*
* @package Hot Property 0.9
* @copyright (C) 2004 Lee Cher Yeong
* @url http://www.Mosets.com/
* @author Lee Cher Yeong <cy@mosets.com>**/

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

class HP_Agent_HTML {

	function editagent(&$row) {
		global $mosConfig_live_site, $hp_imgdir_agent, $Itemid, $my;
	?>
	<script language="javascript">
	<!--
		function submitbutton(pressbutton) {
			var form = document.adminForm;
			if (pressbutton == 'cancelagent') {
				submitform( pressbutton );
				return;
			}
			// do field validation
			if (form.name.value == "") {
				alert( "Please fill in the Agent Name." );
			} else if (form.email.value == "") {
				alert( "Please fill in the E-mail." );
			} else {
				<?php getEditorContents( 'editor1', 'desc' ) ; ?>
				submitform( pressbutton );
			}
		}
	//-->
	</script>
	<div id="hp_view_agent_title"><?php echo _HP_EDITAGENT; ?></div>
	<div id="hp_view_agent_con">
			<div class="hp_view_agent">
		<form action="<?php echo sefRelToAbs("index.php?option=com_hotproperty&Itemid=$Itemid"); ?>" method="POST" name="adminForm" enctype="multipart/form-data">
	<table cellpadding="1" cellspacing="0" border="0">
		<tr>
			<td colspan="3" align="right"><a href="javascript:submitbutton('saveagent');" onmouseout="MM_swapImgRestore();"  onmouseover="MM_swapImage('save','','images/save_f2.png',1);"><img src="images/save.png" alt="<?php echo _CMN_SAVE; ?>" border="0" name="save" /></a>
			&nbsp;&nbsp;&nbsp;
			<a href="javascript:submitbutton('cancelagent');" onmouseout="MM_swapImgRestore();"  onmouseover="MM_swapImage('cancel','','images/cancel_f2.png',1);"><img src="images/cancel.png" alt="<?php echo _CMN_CANCEL; ?>" border="0" name="cancel" /></a>
			</td>
		</tr>
		<tr>
			<td align="left" width="12%"><?php echo _HP_AGENT_NAME; ?></td>
			<td align="left"><input class="inputbox" type="text" name="name" size="30" maxlength="60" valign="top" value="<?php echo $row->name; ?>" /></td>
			<td width="200" align="center" rowspan="4">
			<?php
				if ($row->photo != "") {
				?>
					<img style="border: 5px solid #c0c0c0;" alt="<?php echo $row->name; ?>" src="<?php echo $mosConfig_live_site.$hp_imgdir_agent.$row->photo; ?>" />
					<br />
					<input type="checkbox" name="remove_photo" value="1" /> <?php echo _HP_AGENT_REMOVEPHOTO; ?>
				<?php
				}
				else {
					echo _HP_AGENT_NOPHOTO;
				}
			?>
			</td>
		</tr>
		<tr>
			<td align="left"><?php echo _CMN_EMAIL; ?>:</td>
			<td align="left"><input class="inputbox" type="text" name="email" size="30" maxlength="255" value="<?php echo $row->email; ?>" /></td>
		</tr>
		<tr>
			<td align="left"><?php echo _HP_AGENT_MOBILE ?>:</td>
			<td align="left"><input class="inputbox" type="text" name="mobile" size="12" maxlength="20" value="<?php echo $row->mobile; ?>" /></td>
		</tr>
		<tr>
			<td align="left"><?php echo ($row->photo == "") ? _E_ADD : _E_EDIT;?> <?php echo _HP_PHOTO; ?>:</td>
			<td align="left"><input class="inputbox" type="file" name="photo" size="30" /></td>
		</tr>
		<tr>
			<td align="left" valign="top"><?php echo _CMN_DESCRIPTION; ?>:</td>
			<td align="left" colspan="2"><?php editorArea( 'editor1',  $row->desc, 'desc', '430', '200', '50', '5' ) ; ?></td>
		</tr>
	</table>
	<input type="hidden" name="user" value="<?php echo $my->id; ?>" />
	<input type="hidden" name="id" value="<?php echo $row->id; ?>" />
	<input type="hidden" name="task" value="" />
	</form>
	
		</div>
	</div>
	<?php
	}
}

?>