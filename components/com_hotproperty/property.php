<?php
// property.php
/**
* Hot Property
*
* @package Hot Property 0.9
* @copyright (C) 2004 Lee Cher Yeong
* @url http://www.Mosets.com/
* @author Lee Cher Yeong <cy@mosets.com>
**/

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

function editproperty( $option, $activetab='0' ) {
	global $database, $my, $Itemid, $lang;

	# Retrieve ID
	$id = mosGetParam( $_REQUEST, 'id', 0 );

	HP_useDefaultLang( $lang, "index.php?option=com_hotproperty&task=editprop&id=".$id."&Itemid=".$Itemid );
	$agent = new HP_Agent($database);
	$agent->load($my->id);

	# Redirect to HP main page if user is not a valid Agent
	if ( !($agent->user == $my->id && $agent->user > 0 && !empty($agent->id) && $my->id > 0) ) {
		mosRedirect( "index.php?option=com_hotproperty&Itemid=".$Itemid );
	}

	# Retrieve ID
	$id = mosGetParam( $_REQUEST, 'id', 0 );

	# Retrieve activetab
	$activetab = mosGetParam( $_REQUEST, 'tab', '0' );

	# Load Properties's MAIN info
	$row = new mosHPProperties($database);
	$row->load($id);

	# Redirect to HP main page if property does not belong to this agent
	if ( ($row->agent <> $agent->id) && $id <> 0) {
		mosRedirect( "index.php?option=com_hotproperty&Itemid=".$Itemid );
	}

	# Load Properties's Extra Fields CORE ELEMENTS
	$sql = "SELECT ef.*"
		. "\nFROM #__hp_prop_ef AS ef"
		. "\nWHERE iscore='1'"
		. "\nORDER BY ef.ordering ASC";

	$database->setQuery($sql);

	if(!$result = $database->query()) {
		echo $database->stderr();
		return false;
	}
	$core_fields = $database->loadObjectList('name');
	# END: Load Properties's Extra Fields CORE Elements

	# Load Properties's Extra Fields ELEMENTS
	$sql = "SELECT ef.*"
		. "\nFROM #__hp_prop_ef AS ef"
		. "\nWHERE iscore='0' AND published='1'"
		. "\nORDER BY ef.ordering ASC";

	$database->setQuery($sql);

	if(!$result = $database->query()) {
		echo $database->stderr();
		return false;
	}
	$extra_fields = $database->loadObjectList();

	# END: Load Properties's Extra Fields Elements

	# Load Properties's Extra Fields DATA (aka Properties2)
	$sql = "SELECT ef.name AS name, p.value"
		. "\nFROM #__hp_properties2 AS p"
		. "\nLEFT JOIN #__hp_prop_ef AS ef ON ef.id=p.field"
		. "\nWHERE p.property='$id'";
	$database->setQuery($sql);
	if(!$result = $database->query()) {
		echo $database->stderr();
		return false;
	}
	$extra_fields_data = $database->loadObjectList('name');
	# END: Load Properties's Extra Fields DATA

	# Get number of images - so that we can estimate the height for iframe, to remove the vertical scrollbar
	$sql = "SELECT COUNT(*) AS images"
		. "\nFROM #__hp_photos"
		. "\nWHERE property='$id'";
	$database->setQuery($sql);
	if(!$result = $database->query()) {
		echo $database->stderr();
		return false;
	}
	$num_of_images = $database->loadResult();

  # Get the list of agent (companies)
  $agents[] = mosHTML::makeOption( '0', 'Select Agent' );
  $database->setQuery( "SELECT a.id AS id, a.name AS name, c.name AS company FROM #__hp_agents AS a"
		. "\nLEFT JOIN #__hp_companies AS c ON c.id=a.company"
    . "\nORDER BY a.name ASC, c.name ASC" );
	$row_agents = $database->loadObjectList();
	foreach($row_agents AS $row_agent) {
	  $agents[] = mosHTML::makeOption( $row_agent->id, $row_agent->name." (".$row_agent->company.")" );
	}
  $lists["agent"] = mosHTML::selectList( $agents, 'agent', 'class="inputbox" size="1"', 'value', 'text', $row->agent );

  # Get the list of property types
  $query = "SELECT * FROM #__hp_prop_types WHERE id!=0"; //On exclut /root que l'on ajoute manuellement
  $database->setQuery( $query );
  $src_list = $database->loadObjectList();
  $preload = array();
  $preload[] = mosHTML::makeOption( '0', 'Select Property Type' );
  $selected = array();
  $selected[] = mosHTML::makeOption( $row->type ); //S�lectionne par d�faut dans la liste le type courant de la propri�t�
  $lists["prop_type"] = mosHTML::treeSelectList( $src_list, -1, $preload, 'type', 'class="inputbox" size="1" onchange="javascript:submitbutton(\'saveproperty_updateEF\')"', 'value', 'text', $selected );

	# make the select list for the states
	$states[] = mosHTML::makeOption( 0, _CMN_UNPUBLISHED );
	$states[] = mosHTML::makeOption( 1, _CMN_PUBLISHED );
	$lists['state'] = mosHTML::selectList( $states, 'published', 'class="inputbox" size="1"',
	'value', 'text', intval( $row->published ) );

	if (!$id) {
		// AEC HACK hotproperty1 START
global $mosConfig_absolute_path;
include_once( $mosConfig_absolute_path . '/components/com_acctexp/acctexp.class.php' );
include_once( $mosConfig_absolute_path . '/components/com_acctexp/micro_integration/mi_hotproperty.php' );
$mi_hphandler = new aec_hotproperty( $database );
$mi_hphandler->loadUserID( $my->id );
if( $mi_hphandler->id ) {
if( !$mi_hphandler->hasListingsLeft() ) {
echo "No Listings left";
return;
}
} else {
echo "Registration and correct Subscription Required!";
return;
}
// AEC HACK hotproperty1 END

# Assign default value for new data
		$row->featured = 0;
		$row->hits = 0;
		$row->publish_up = date( "Y-m-d", time() );
		$row->publish_down = "Never";

		#Assign for properties2 data
		for ($i=0, $n=count( $extra_fields ); $i < $n; $i++) {
			$extra_field  = $extra_fields[$i];
			if ($extra_field->default_value <> "") {
				$extra_fields_data[$extra_field->name]->value = $extra_field->default_value;
			}
		}
	} else {
		if (trim( $row->publish_down ) == "0000-00-00 00:00:00") {
			$row->publish_down = "Never";
		}
	}

	# Check HP Extension
	$extensions = getHPExtensions();

	HP_Property_HTML::editproperty( $row, $lists, $core_fields, $extra_fields, $extra_fields_data, $num_of_images, $extensions, $option, $activetab );
}

function getHPExtensions() {
	global $database;

	$database->setQuery("SELECT `name`, `option` FROM #__components WHERE `option` LIKE 'com_hp_%' && `option`<>'com_hpmulticats' && parent=0");
	$extensions = $database->loadObjectList();

	return $extensions;
}

function saveproperty( $option, $task='' ) {
	global $database, $Itemid, $my, $hp_auto_approve;

 	$row = new mosHPProperties( $database );
	if (!$row->bind( $_POST )) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

	// AEC HACK hotproperty2 START
global $mosConfig_absolute_path;
include_once( $mosConfig_absolute_path . '/components/com_acctexp/acctexp.class.php' );
include_once( $mosConfig_absolute_path . '/components/com_acctexp/micro_integration/mi_hotproperty.php' );
$mi_hphandler = new aec_hotproperty( $database );
$mi_hphandler->loadUserID( $my->id );
if( $mi_hphandler->id ) {
if( $mi_hphandler->hasListingsLeft() ) {
$mi_hphandler->useListing();
} else {
echo "No Listings left";
return;
}
} else {
echo "Registration and correct Subscription Required!";
return;
}
// AEC HACK hotproperty2 END

# Assign current logon user to Agent field
	if ( $my->id > 0 ) {
		$agent = new HP_Agent($database);
		$agent->load($my->id);
		$row->agent = $agent->id;
	} else {
		mosRedirect( "index.php?option=$option&Itemid=".$Itemid, _HP_EDITPROP_MSG_03 );
		//echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

	if ($row->id < 1) {
		$row->created = $row->created ? $row->created : date( "Y-m-d H:i:s" );
		if ($agent->need_approval && $hp_auto_approve <> '1') { $row->approved = 0; }
		else { $row->approved = 1; }
	} else {
		$row->modified = date( "Y-m-d H:i:s" );
		$row->created = $row->created ? $row->created : date( "Y-m-d H:i:s" );
	}

	# If never fill it with all zeros
	if (trim( $row->publish_down ) == "Never") {
		$row->publish_down = "0000-00-00 00:00:00";
	}

	if (!$row->store()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	$row->updateOrder();

	# Store ef_ elements to our database #__hp_properties2

	# Erase Previous Records, make way for the new data
	$sql="DELETE FROM #__hp_properties2"
		. "\nWHERE property='".$row->id."'";
	$database->setQuery($sql);
	if (!$database->query()) {
		echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
		exit();
	}

	# Assign Properties2 data (Extra Fields)
	foreach($_POST AS $k => $v) {
		if (substr($k,0,3) == "ef_") {

			# -- Get the prop_ef id first
			$sql="SELECT id FROM #__hp_prop_ef WHERE name='".substr($k,3)."'";
			$database->setQuery($sql);
			if (!$database->query()) {
				echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
				exit();
			}
			$prop_ef_id = $database->loadResult();

			# -- Now add the row
			$sql = "INSERT INTO #__hp_properties2"
				. "\n(`property`, `field`, `value`)"
				. "\nVALUES ('".$row->id."', '".$prop_ef_id."', '".mysql_escape_string((is_array($v)) ? implode("|",$v) : $v)."')";
			$database->setQuery($sql);
			if (!$database->query()) {
				echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
				exit();
			}

		} // End if
	} // End of foreach

	# Manage Featured Ordering
	$featured = new mosHPFeatured($database);
	if ($row->featured) {
		if (!$featured->load( $row->id )) {
			// new entry
			$database->setQuery( "INSERT INTO #__hp_featured VALUES ('$row->id','1')" );
			if (!$database->query()) {
				echo "<script> alert('".$database->stderr()."');</script>\n";
				exit();
			}
			$featured->ordering = 1;
		}
	} else {
		if (!$featured->delete( $row->id )) {
			$msg .= $featured->stderr();
		}
		$featured->ordering = 0;
	}
	$featured->updateOrder();

	# Get agent ID
	$agent = new HP_Agent($database);
	$agent->load($my->id);

	if ($task == '')
		mosRedirect( "index.php?option=$option&task=manageprop&Itemid=".$Itemid, _HP_EDITPROP_MSG_01 );
	# Au changement de cat�gorie, on rafraichit la page pour mettre � jour les EFs:
	elseif($task=='updateEF')
		mosRedirect( "index.php?option=$option&task=editprop&id=$row->id&Itemid=".$Itemid."&tab=0" );
	else
		mosRedirect( "index.php?option=$option&task=editprop&id=$row->id&Itemid=".$Itemid."&tab=2" );
}

function cancelproperty( $option ) {
	global $Itemid;
	mosRedirect( "index.php?option=$option&task=manageprop&Itemid=".$Itemid );
}

/********************
 * Image Management *
 *******************/

function listprop_image($option, $msg='') {
	global $my, $database, $Itemid;

	$agent = new HP_Agent($database);
	$agent->load($my->id);

	# Redirect to HP main page if user is not a valid Agent
	if ( !($agent->user == $my->id && $agent->user > 0 && !empty($agent->id) && $my->id > 0) ) {
		mosRedirect( "index.php?option=com_hotproperty&Itemid=".$Itemid );
	}

	$property = mosGetParam( $_REQUEST, 'property', array(0) );

	$sql = "SELECT p.*"
		. "\nFROM #__hp_photos AS p"
		. "\nWHERE property='".$property."'"
		. "\nORDER BY ordering ASC";

	$database->setQuery($sql);

	if(!$result = $database->query()) {
		echo $database->stderr();
		return false;
	}
	$rows = $database->loadObjectList();

	HP_Property_HTML::listprop_image($rows, $property, $option, $msg);
}

function listprop_showupload_image() {
	global $my, $database, $Itemid;

	$agent = new HP_Agent($database);
	$agent->load($my->id);

	# Redirect to HP main page if user is not a valid Agent
	if ( !($agent->user == $my->id && $agent->user > 0 && !empty($agent->id) && $my->id > 0) ) {
		mosRedirect( "index.php?option=com_hotproperty&Itemid=".$Itemid );
	}

	$property = mosGetParam( $_REQUEST, 'property', 0 );
	HP_Property_HTML::listprop_uploadnew_image($property);
}

function listprop_upload_image() {
	global $mosConfig_absolute_path, $database, $Itemid, $my;
	global $hp_imgdir_standard, $hp_imgdir_thumb, $hp_imgdir_original;
	global $hp_img_connector, $hp_img_method, $hp_imgsize_standard, $hp_imgsize_thumb;
  global $hp_img_saveoriginal;

	$agent = new HP_Agent($database);
	$agent->load($my->id);

	# Redirect to HP main page if user is not a valid Agent
	if ( !($agent->user == $my->id && $agent->user > 0 && !empty($agent->id) && $my->id > 0) ) {
		mosRedirect( "index.php?option=com_hotproperty&Itemid=".$Itemid );
	}

	$property = mosGetParam( $_REQUEST, 'property', 0 ); // Property ID
	$id = mosGetParam( $_POST, 'id', 0 ); // Image ID
	$option = mosGetParam( $_REQUEST, 'option', array(0) );

	# If this image has no ID, it means this property is attaching NEW photo - Verify image
	# If this is an existing photo AND has attached a file - Verify image
	if ($id == 0 || ($id != 0 && $_FILES['image']['tmp_name'] != '') ) {
		# Verify image is supplied
		if(!isset($_FILES['image']) || !is_array($_FILES['image'])) {
			echo "<script> alert('"._HP_IMGUPLOAD_ERR_01."'); window.history.go(-1); </script>\n";
			exit();
		}

		# Verify the file is uploaded
		if (!is_uploaded_file($_FILES['image']['tmp_name'])) {
			echo "<script> alert('"._HP_IMGUPLOAD_ERR_02."'); window.history.go(-1); </script>\n";
			exit();
		}

		# Verify this is an acceptable image file (.gif, .jpg, .png, .bmp)
		if ((strcasecmp(substr($_FILES['image']['name'],-4),".gif")) && (strcasecmp(substr($_FILES['image']['name'],-4),".jpg")) && (strcasecmp(substr($_FILES['image']['name'],-4),".png")) && (strcasecmp(substr($_FILES['image']['name'],-4),".bmp")) ) {
			echo "<script> alert('"._HP_IMGUPLOAD_ERR_03."'); window.history.go(-1); </script>\n";
			exit();
		}

		# No Duplicates! - Check original, thumb and standard directory
		if (file_exists($mosConfig_absolute_path.$hp_imgdir_original.$property.$hp_img_connector.$_FILES['image']['name']) || file_exists($mosConfig_absolute_path.$hp_imgdir_standard.$property.$hp_img_connector.$_FILES['image']['name']) || file_exists($mosConfig_absolute_path.$hp_imgdir_thumb.$property.$hp_img_connector.$_FILES['image']['name'])) {
			echo "<script> alert('"._HP_IMGUPLOAD_ERR_04."'); window.history.go(-1); </script>\n";
			exit();
		}
	}

	# This is an existing photo - He's editing it.
	if ($id != 0) {
		# Store the old photo name for later use - to delete if he's uploading any new one
		$sql="SELECT original, standard, thumb FROM #__hp_photos WHERE id='".$id."' LIMIT 1";
		$database->setQUery($sql);
		$old_photos = $database->loadObjectList();
	}

	# Prepare the database
	$row = new mosHPPhotos( $database );
	$row->property = $property;

	# Copy & generate image if (i) New photo (ii) Changing existing photo
	if (empty($id) || ($id > 0 && $_FILES['image']['tmp_name'] != '') ) {

	  # Allow saving of original image. Disabled by default
		if ($hp_img_saveoriginal) {
			if(copy ($_FILES['image']['tmp_name'], $mosConfig_absolute_path.$hp_imgdir_original.$property.$hp_img_connector.$_FILES['image']['name'])) {
				//echo "Original File: SUCCESS<br />";
				$row->original = $property.$hp_img_connector.$_FILES['image']['name'];
			}
		}

		# Move the uploaded file to Mambo /media directory - Safe Mode copatibility
		if ( move_uploaded_file($_FILES['image']['tmp_name'], $mosConfig_absolute_path.'/media/'.basename($_FILES['image']['tmp_name'])) ) {
			$src_file = $mosConfig_absolute_path.'/media/'.basename($_FILES['image']['tmp_name']);
		} else {
			echo "ERROR: Can not move uploaded file!";
		}

		# Generate the Standard image
		if(resize_image($src_file, $mosConfig_absolute_path.$hp_imgdir_standard.$property.$hp_img_connector.$_FILES['image']['name'], $hp_imgsize_standard, $hp_img_method, '80')) {
			//echo "Standard File generate: SUCCESS<br />";
			$row->standard = $property.$hp_img_connector.$_FILES['image']['name'];
		} else {
			//echo "Standard File generate: FAILED<br />";
		}

		# Generate the Thumb image
		if(resize_image($src_file, $mosConfig_absolute_path.$hp_imgdir_thumb.$property.$hp_img_connector.$_FILES['image']['name'], $hp_imgsize_thumb, $hp_img_method, '80')) {
		//echo "Thumb File generate: SUCCESS<br />";
			$row->thumb = $property.$hp_img_connector.$_FILES['image']['name'];
		} else {
			//echo "Thumb File generate: FAILED<br />";
		}

		# Remove the temporary file in /media
		unlink( $src_file );

		# New image generation is done, if this image has any old photo, delete it
		if (!empty($old_photos)) {
			foreach($old_photos AS $old_photo) {
				if(!unlink($mosConfig_absolute_path.$hp_imgdir_original.$old_photo->original)) {
					echo "<script> alert('"._HP_IMGUPLOAD_ERR_06."'); window.history.go(-1); </script>\n";
					exit();
				}
				if(!unlink($mosConfig_absolute_path.$hp_imgdir_standard.$old_photo->standard)) {
					echo "<script> alert('"._HP_IMGUPLOAD_ERR_07."'); window.history.go(-1); </script>\n";
					exit();
				}
				if(!unlink($mosConfig_absolute_path.$hp_imgdir_thumb.$old_photo->thumb)) {
					echo "<script> alert('"._HP_IMGUPLOAD_ERR_08."'); window.history.go(-1); </script>\n";
					exit();
				}
			}
		} // End of Empty

	} // End of IF

	# Get ready and insert to databse
	if (!$row->bind( $_POST )) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

	if($row->id < 1) { # Do for new record
		$row->ordering = 9999;
		if ($row->title == "") $row->title = $_FILES['image']['name'];
	}

	if (!$row->store()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	$row->updateOrder('property='.$row->property);

	# Show message upon success upload
	$msg = "Upload Success! - <b>".$_FILES['image']['name']."</b> (".round(intval($_FILES['image']['size']/1024))." Kb)";

	listprop_showupload_image();
	//listprop_image($option, $msg);
}

function listprop_edit_image() {
	global $my, $database, $Itemid;

	$agent = new HP_Agent($database);
	$agent->load($my->id);

	# Redirect to HP main page if user is not a valid Agent
	if ( !($agent->user == $my->id && $agent->user > 0 && !empty($agent->id) && $my->id > 0) ) {
		mosRedirect( "index.php?option=com_hotproperty&Itemid=".$Itemid );
	}

	$id = mosGetParam( $_REQUEST, 'id', 0 );

  $row = new mosHPPhotos( $database );
	$row->load( $id );

	HP_Property_HTML::listprop_edit_image($row, $option);
}

function listprop_delete_image() {
	global $my, $database, $Itemid;
	global $mosConfig_absolute_path, $hp_imgdir_original, $hp_imgdir_thumb, $hp_imgdir_standard;

	$agent = new HP_Agent($database);
	$agent->load($my->id);

	# Redirect to HP main page if user is not a valid Agent
	if ( !($agent->user == $my->id && $agent->user > 0 && !empty($agent->id) && $my->id > 0) ) {
		mosRedirect( "index.php?option=com_hotproperty&Itemid=".$Itemid );
	}

	$id = mosGetParam( $_REQUEST, 'id', 0 ); // Image ID

	# Get the filename from the DB to delete
	$sql="SELECT original, thumb, standard FROM #__hp_photos WHERE `id`='".$id."' LIMIT 1";
	$database->setQuery($sql);
	$old_photos = $database->loadObjectList();

	# Remove from database
	$sql="DELETE FROM #__hp_photos WHERE `id`='".$id."'";
	$database->setQuery($sql);
	$database->query();

	# Delete the old photos
	if (!empty($old_photos)) {
		foreach($old_photos AS $old_photo) {
			if (!empty($old_photo->original)) {
				if(!unlink($mosConfig_absolute_path.$hp_imgdir_original.$old_photo->original)) {
					echo "<script> alert('"._HP_IMGUPLOAD_ERR_06."'); window.history.go(-1); </script>\n";
					exit();
				}
			}
			if (!empty($old_photo->standard)) {
				if(!unlink($mosConfig_absolute_path.$hp_imgdir_standard.$old_photo->standard)) {
					echo "<script> alert('"._HP_IMGUPLOAD_ERR_07."'); window.history.go(-1); </script>\n";
					exit();
				}
			}
			if (!empty($old_photo->thumb)) {
				if(!unlink($mosConfig_absolute_path.$hp_imgdir_thumb.$old_photo->thumb)) {
					echo "<script> alert('"._HP_IMGUPLOAD_ERR_07."'); window.history.go(-1); </script>\n";
					exit();
				}
			}
		}
	} // End of Empty

	listprop_showupload_image();
}

function listprop_order_image($inc, $option) {
	global $my, $database, $Itemid;

	$agent = new HP_Agent($database);
	$agent->load($my->id);

	# Redirect to HP main page if user is not a valid Agent
	if ( !($agent->user == $my->id && $agent->user > 0 && !empty($agent->id) && $my->id > 0) ) {
		mosRedirect( "index.php?option=com_hotproperty&Itemid=".$Itemid );
	}

	$id = mosGetParam( $_REQUEST, 'id', 0 );

  # oop database connector
  $row = new mosHPPhotos( $database );
	$row->load( $id );
	$row->move( $inc, "property='$row->property'" );

	mosRedirect( "index2.php?option=$option&task=listprop_image&property=$row->property" );
}

function listprop_edit_images($option) {
	global $my, $database, $Itemid;

	$agent = new HP_Agent($database);
	$agent->load($my->id);

	# Redirect to HP main page if user is not a valid Agent
	if ( !($agent->user == $my->id && $agent->user > 0 && !empty($agent->id) && $my->id > 0) ) {
		mosRedirect( "index.php?option=com_hotproperty&Itemid=".$Itemid );
	}

	$id = mosGetParam( $_REQUEST, 'id', 0 );

	$row = new mosHPPhotos( $database );
	$row->load( $id );


}

function manageprop() {
	global $database, $Itemid, $my, $hp_default_limit, $lang, $mosConfig_lang;

	HP_useDefaultLang( $lang,  "index.php?option=com_hotproperty&task=manageprop&Itemid=".$Itemid );

	# Get Agent ID
	$agent = new HP_Agent($database);
	$agent->load($my->id);

	# Redirect to HP main page if user is not a valid Agent
	if ( !($agent->user == $my->id && $agent->user > 0 && !empty($agent->id) && $my->id > 0) ) {
		mosRedirect( "index.php?option=com_hotproperty&Itemid=".$Itemid );
	}

	# Get total of properties
	$database->setQuery("SELECT COUNT(*) FROM #__hp_properties WHERE agent=".$agent->id);

	# Pagination
	$total = $database->loadResult();
	$limit = trim( mosGetParam( $_REQUEST, 'limit', $hp_default_limit ) );
	$limitstart = trim( mosGetParam( $_REQUEST, 'limitstart', 0 ) );
	$pageNav = new mosPageNav( $total, $limitstart, $limit );

	# Get ALL properties
	$database->setQuery("SELECT p.*, t.name AS type"
		. "\nFROM #__hp_properties AS p"
		. "\nLEFT JOIN #__hp_agents AS a ON a.id=p.agent"
		. "\nLEFT JOIN #__hp_companies AS c ON c.id=a.company"
		. "\nLEFT JOIN #__hp_prop_types AS t ON t.id=p.type"
		.	"\nWHERE p.agent='".$agent->id."'"
		. "\nORDER BY p.name ASC"
		. "\nLIMIT $pageNav->limitstart,$pageNav->limit");

	$properties = $database->loadObjectList();
	echo $database->getErrorMsg();

	HP_Property_HTML::manageprop($properties, $pageNav);
}

?>
