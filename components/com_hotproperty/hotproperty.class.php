<?php
/**
* Hot Property class
*
* @package Hot Property 0.9
* @copyright (C) 2004 Lee Cher Yeong
* @url http://www.Mosets.com/
* @author Lee Cher Yeong <cy@mosets.com>
**/

// ensure this file is being included by a parent file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
* Hot Property: Agent class
*/

class HP_Agent extends mosDBTable {

	var $id=null;
	var $user=null;
	var $name=null;
	var $mobile=null;
	var $email=null;
	var $photo=null;
	var $company=null;
	var $desc=null;
	var $need_approval=null;

	function hp_agent( &$db ) {
		$this->mosDBTable( '#__hp_agents', 'user', $db );
	}

}

/**
* Hot Property class
*/

class hotproperty {

	/**
	* Return true if this property is published and it's corresponding type is published. Return false otherwise
	* @param int Property's ID
	*/
	function isPropPublished($id) {
		global $database;

		# If ID is 0, it is invalid
		if ($id == 0) return false;

		# Check if the property and it's type is published
		$database->setQuery("SELECT p.id FROM #__hp_properties AS p"
			. "\nLEFT JOIN #__hp_prop_types AS t ON t.id=p.type"
			.	"\nWHERE p.published=1 AND p.approved='1' AND t.published=1 AND p.id='".$id."'"
			. "\n	AND (publish_up = '0000-00-00 00:00:00' OR publish_up <= NOW())"
			. "\n	AND (publish_down = '0000-00-00 00:00:00' OR publish_down >= NOW())"
			);
		$database->query();
		if($database->getNumRows() <= 0) return false;

		return true;
	}

	/**
	* Return true if this type is published. Return false otherwise
	* @param int Type's ID
	*/
	function HP_isTypePublished($id) {
		global $database;

		# If ID is 0, it is invalid
		if ($id == 0) return false;

		# Check if the type is published
		$database->setQuery("SELECT id FROM #__hp_prop_types"
			.	"\nWHERE published=1 AND id='".$id."'");
		$database->query();
		if($database->getNumRows() <= 0) return false;

		return true;
	}

	/**
	* Return the number of properties for a specific requirement
	* @param int Type's ID (ie. $where='p.agent=#' /  $where='p.type=#' / $where='p.price<###')
	*/
	function getNumProp($where) {
		global $database;
		$database->setQuery( "SELECT COUNT(*) AS total FROM #__hp_properties AS p"
			. "\nLEFT JOIN #__hp_prop_types AS t ON t.id=p.type"
			.	"\nWHERE p.published=1 AND p.approved='1' AND t.published=1"
			. "\n AND ".$where
			. "\n	AND (publish_up = '0000-00-00 00:00:00' OR publish_up <= NOW())"
			. "\n	AND (publish_down = '0000-00-00 00:00:00' OR publish_down >= NOW())");
		$result = $database->loadResult();
		echo $database->getErrorMsg();
		return $result;
	}

}

?>