<?php
// property.html.php
/**
* Hot Property
*
* @package Hot Property 0.9
* @copyright (C) 2004 Lee Cher Yeong
* @url http://www.Mosets.com/
* @author Lee Cher Yeong <cy@mosets.com>
**/

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

require_once( "includes/HTML_toolbar.php" );

class HP_Property_HTML {

	function manageprop($properties, $pageNav) {
		global $Itemid;
	?>
	<script type="text/javascript" src="<?php echo $mosConfig_live_site; ?>/includes/js/overlib.js"></script>
	<div id="hp_ManageProp"><?php echo _HP_MENU_MANAGEPROPERTY2; ?></div>
	<table width="100%" border="0" cellspacing="0" cellpadding="1">
        <tr>
          <td colspan="5" align="right">
						<?php echo $pageNav->writeLimitBox('index.php?option=com_hotproperty&task=manageprop&Itemid='.$Itemid); ?>
					</td>
				</tr>
				<tr>
          <td align="center" class="sectiontableheader" width="10">#</td>
          <td align="left" class="sectiontableheader" width="70%"><?php echo _HP_PROPERTY; ?></td>
					<td align="left" class="sectiontableheader" width="10%"><?php echo _HP_TYPE; ?></td>
					<td align="center" class="sectiontableheader" width="80"><?php echo _CMN_PUBLISHED; ?></td>
					<td align="center" class="sectiontableheader" width="80"><?php echo _HEADER_HITS; ?></td>
				</tr>
				<?php
					$i=0;
					foreach($properties AS $p) {
				?>
				<tr>
					<td align="center"><?php echo $i+$pageNav->limitstart+1;?></td>
					<td align="left"><a href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&task=editprop&id=$p->id&Itemid=$Itemid"); ?>" title="<?php echo _E_EDIT; ?>"><?php echo $p->name; ?></a><?php echo ($p->approved) ? "" : " <small>["._HP_EDITPROP_MSG_02."]</small>";?></td>
					<td align="left"><?php echo $p->type; ?></td>
				 <?php
					$now = date( "Y-m-d h:i:s" );
					if ($p->approved) {
						if ($now <= $p->publish_up && $p->published == "1") {
							$img = 'publish_y.png';
						} else if (($now <= $p->publish_down || $p->publish_down == "0000-00-00 00:00:00") && $p->published == "1") {
							$img = 'publish_g.png';
						} else if ($now > $p->publish_down && $p->published == "1") {
							$img = 'publish_r.png';
						} elseif ($p->published == "0") {
							$img = "publish_x.png";
						}
					} else {
							$img = "publish_x.png";
					}
		?>
						<td align="center"><img src="administrator/images/<?php echo $img;?>" border="0" alt="" /></td>
						<?php
				?>
					<td align="center"><?php echo $p->hits; ?></td>
				</tr>
				<?php
					$i++;
					}
				?>
				<tr>
					<td align="center" colspan="5" class="sectiontableheader"><?php echo $pageNav->writePagesLinks('index.php?option=com_hotproperty&task=manageprop&Itemid='.$Itemid); ?></td>
				</tr>
				<tr><td colspan="4" align="right"><?php echo $pageNav->writePagesCounter(); ?></td></tr>
	</table>

	<?php
	}

	function editproperty( &$row, &$lists, &$core_fields, &$extra_fields, &$extra_fields_data, $num_of_images, $extensions, $option, $activetab ) {
		global $mosConfig_live_site, $Itemid, $database;
		mosMakeHtmlSafe( $row );
	?>
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo $mosConfig_live_site; ?>/includes/js/calendar/calendar-mos.css" title="green" />
	<script type="text/javascript" src="<?php echo $mosConfig_live_site; ?>/includes/js/calendar/calendar.js"></script>
	<script type="text/javascript" src="<?php echo $mosConfig_live_site; ?>/includes/js/calendar/lang/calendar-en.js"></script>
	<div id="overDiv" style="position:absolute; visibility:hidden; z-index:10000;"></div>
	<script language="Javascript" src="includes/js/overlib_mini.js"></script>
	<?php
	$docinfo = "<strong>"._E_SUBJECT."</strong> ";
	$docinfo .= $row->name."<br />";
	$docinfo .= "<strong>"._E_EXPIRES."</strong> ";
	$docinfo .= $row->publish_down."<br />";
	$docinfo .= "<strong>"._E_CREATED."</strong> ";
	$docinfo .= $row->created."<br />";
	$docinfo .= "<strong>"._E_LAST_MOD."</strong> ";
	$docinfo .= $row->modified."<br />";
	$docinfo .= "<strong>"._E_HITS."</strong> ";
	$docinfo .= $row->hits."<br />";
	 ?>
	<script language="javascript">
	<!--
		function submitbutton(pressbutton) {
			var form = document.adminForm;
			if (pressbutton == 'cancelprop') {
				submitform( pressbutton );
				return;
			}
			// do field validation
			if (form.name.value == "") {
				alert( "Please fill in the Property Name." );
				form.type.value = currentType; //on remet le type � sa valeur d'origine, d�finie tout en bas du formulaire
				form.name.focus(); //et on donne le focus au nom c'est pas con
			} else if(form.type.value == "0") {
				alert( "Please enter the Property Type." );
				form.type.focus();
			} else if(isNaN(form.price.value)) {
				alert( "Please enter a valid price." );
			} else {
				<?php getEditorContents( 'editor1', 'intro_text' ) ; ?>
				<?php getEditorContents( 'editor2', 'full_text' ) ; ?>
				submitform( pressbutton );
			}
		}
	//-->
	</script>
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
		<td class="contentheading" >
		<?php echo $row->id ? _E_EDIT : _E_ADD;?>&nbsp;<?php echo _HP_PROPERTY;?> &nbsp;&nbsp;&nbsp;
		<a href="javascript: void(0);" onMouseOver="return overlib('<table border=0 width=250 height=100%><?php echo $docinfo; ?></table>', CAPTION, '<?php echo _E_ITEM_INFO;?>', BELOW, RIGHT);" onMouseOut="return nd();">
		<strong>[Info]</strong>
		</a>
		</td>
		<td width="10%">
		 <?php
		 mosToolBar::startTable();
		 mosToolBar::save('saveprop');
		 mosToolBar::spacer(25);
		 mosToolBar::cancel('cancelprop');
		 mosToolBar::endtable();
		 $tabs = new mosTabs(0);
		?>
		</td>
	</tr>
	</table>

	<form action="index2.php" method="POST" name="adminForm">

	<?php if (!$row->id) { ?>
		<div align="right"><a class="toolbar" href="javascript:submitbutton('saveproperty_image_new');"><img src="images/save.png" alt="Save" border="0" align="middle" />&nbsp;<?php echo _HP_EDITPROP_SAVEINSERTPHOTO ?></a></div>
	<?php } ?>

	<?php
	$tabs->startPane("content-pane");
	$tabs->startTab("Properties","properties-page");
	?>

	<table cellpadding="4" cellspacing="1" border="0" width="100%" class="adminform">
		<?php
		$i = 0;
		foreach($core_fields AS $core_field) {

			#on charge l'objet $core_field_O:
			$core_field_O=new mosHPPropEF($database);
			$core_field_O->load($core_field->id);

			$rel=$core_field_O->isRelToType($row->type);

			$i++;
			# Show Field if it is published
			# 'Type' field will always appear
			if ($core_field->published == '1' && $rel>0 || $core_field->name == 'name' || $core_field->name == 'type') {
			switch($core_field->name) {
				case 'name':?>
		<tr>
			<td align="left" width="10%"><?php echo $core_field->caption; ?>:</td>
			<td align="left"><input class="inputbox" type="text" name="name" size="30" valign="top" value="<?php echo $row->name; ?>"></td>
		</tr><?php
					break;
				case 'type':
	?><tr>
			<td align="left"><?php echo $core_field->caption; ?>:</td>
			<td align="left"><?php echo $lists["prop_type"]; ?></td>
		</tr><?php
					break;
				case 'address':
	?><tr>
			<td align="left"><?php echo $core_field->caption; ?>:</td>
			<td align="left"><input class="inputbox" type="text" name="address" size="30" maxlength="100" value="<?php echo $row->address; ?>"></td>
		</tr><?php
					break;
				case 'suburb':
	?><tr>
			<td align="left"><?php echo $core_field->caption; ?>:</td>
			<td align="left"><input class="inputbox" type="text" name="suburb" size="15" maxlength="50" value="<?php echo $row->suburb; ?>"></td>
		</tr><?php
					break;
				case 'state':
	?><tr>
			<td align="left"><?php echo $core_field->caption; ?>:</td>
			<td align="left"><input class="inputbox" type="text" name="state" size="15" maxlength="50" value="<?php echo $row->state; ?>"></td>
		</tr><?php
					break;
				case 'country':
	?><tr>
			<td align="left"><?php echo $core_field->caption; ?>:</td>
			<td align="left"><input class="inputbox" type="text" name="country" size="15" maxlength="50" value="<?php echo $row->country; ?>"></td>
		</tr><?php
					break;
				case 'postcode':
	?><tr>
			<td align="left"><?php echo $core_field->caption; ?>:</td>
			<td align="left"><input class="inputbox" type="text" name="postcode" size="6" maxlength="10" value="<?php echo $row->postcode; ?>"></td>
		</tr><?php
					break;
				case 'price':
	?><tr>
			<td align="left"><?php echo $core_field->caption; ?>:</td>
			<td align="left"><input class="inputbox" type="text" name="price" size="15" maxlength="20" value="<?php echo $row->price; ?>"></td>
		</tr><?php
					break;
				case 'featured':
	?><tr>
			<td align="left" valign="top"><?php echo $core_field->caption; ?>:</td>
			<td align="left"><input type="checkbox" name="featured" value="1"<?php echo ($row->featured) ? " checked":"";?>></td>
		</tr><?php
					break;
				case 'intro_text':
	?><tr>
			<td align="left" valign="top" colspan="2"><?php echo $core_field->caption; ?>:</td>
		</tr>
		<tr>
			<td align="left" colspan="2">
				<?php
				editorArea( 'editor1',  $row->intro_text, 'intro_text', '480', '200', '50', '5' ) ; ?>
			</td>
		</tr><?php
					break;
				case 'full_text':
	?><tr>
			<td align="left" valign="top" colspan="2"><?php echo $core_field->caption; ?>:</td>
		</tr>
		<tr>
			<td align="left" colspan="2">
				<?php
				editorArea( 'editor2',  $row->full_text, 'full_text', '480', '200', '50', '5' ) ; ?>
			</td>
		</tr><?php
					break;
				case 'notes':
	?><tr>
			<td align="left" valign="top" colspan="2"><?php echo $core_field->caption; ?>:</td>
		</tr>
		<tr>
			<td align="left" colspan="2"><textarea name="note" class="inputbox" rows="5" cols="50"><?php echo $row->note; ?></textarea>
				<br /><em><?php echo _HP_EDITPROP_NOTES_MSG ?></em>
			</td>
		</tr><?php
					break;
			} // End Switch
			} // End if published
		} // End of foreach
?></table>
	<?php
		$price_ef=new mosHPPropEF($database);
		$price_ef->load($core_fields['price']->id);
		# En plus du cas o� l'ef price n'est pas publi�, on v�rifie s'il n'est pas li�:
		if ($core_fields['price']->published == '0' || !$price_ef->isRelToType($row->type)) {
?><input type="hidden" name="price" value="0" /><?php
		}
		#pour conserver l'attribut featured m�me s'il n'est pas related:
		$featured_ef=new mosHPPropEF($database);
		$featured_ef->load($core_fields['featured']->id);
		if (!$featured_ef->isRelToType($row->type)) {
?><input type="hidden" name="featured" value="<?php echo $row->featured; ?>" /><?php
		}
?>
		<?php
		$tabs->endTab();

		if ($row->id) { // On cherche � s'assurer que le type a �t� s�lectionner avant d'afficher l'onglet 'extrafields'
		$tabs->startTab(_HP_EXTRAFIELDS,"extrafields-page");
		?>

    <table cellpadding="5" cellspacing="0" border="0" width="100%" class="adminform">
		<?php
		# New OOH Forms
		$f = new form;

		$s=0;
		# Generate all published fields
		for ($i=0, $n=count( $extra_fields ); $i < $n; $i++) {
			$extra_field  = $extra_fields[$i];

			$extra_field_O=new mosHPPropEF($database);
			$extra_field_O->load($extra_field->id);

			if($extra_field_O->isRelToType($row->type))
			{
				$s++;

				$extra_field->name = "ef_".$extra_field->name;
				echo '<tr><td width="120" align="left" valign="top">'.$extra_field->caption.': </td><td align="left">';
				if ($extra_field->field_type == "text") {

					# --- Text Box ----------------------------------
					$f->add_element(array("type"=>"text",
																"name"=>$extra_field->name,
																"value"=>$extra_fields_data[substr($extra_field->name,3)]->value,
																"size"=>$extra_field->size,
																"extrahtml"=>"class=\"inputbox\""));
					echo $extra_field->prefix_text;
					$f->show_element($extra_field->name);
					echo $extra_field->append_text;

				} elseif ($extra_field->field_type == "link") {

					# --- Web Link ----------------------------------

					$f->add_element(array("type"=>"text",
																"name"=>$extra_field->name,
																"value"=>$extra_fields_data[substr($extra_field->name,3)]->value,
																"size"=>$extra_field->size,
																"extrahtml"=>"class=\"inputbox\""));
					echo $extra_field->prefix_text;
					$f->show_element($extra_field->name);
					echo $extra_field->append_text;
					echo "<br /><i>ie: Google | http://www.google.com/</i>";

				} elseif ($extra_field->field_type == "multitext") {

					# --- Text Area ----------------------------------
					$f->add_element(array("type"=>"textarea",
																"name"=>$extra_field->name,
																"value"=>$extra_fields_data[substr($extra_field->name,3)]->value,
		                            "rows"=>$extra_field->size,
			                          "cols"=>60,
																"extrahtml"=>"class=\"inputbox\""));
					echo $extra_field->prefix_text;
					$f->show_element($extra_field->name);
					echo $extra_field->append_text;

				} elseif ($extra_field->field_type == "selectlist" || $extra_field->field_type == "selectmultiple") {

					# --- Drop Down Menu & Multiple Select List ----------------------------------
					$values = explode("|",$extra_field->field_elements);

					if ( empty($extra_fields_data[substr($extra_field->name,3)]->value) ) {
						$extra_fields_data[substr($extra_field->name,3)]->value = '';
					}

					# - Generate arrays of values and labels
					$values_labels = array();

					$values_labels[] = array("label"=>'', "value"=>'');
					foreach($values AS $value) {
						$values_labels[] = array("label"=>trim($value), "value"=>trim($value));
					}
					$f->add_element(array("type"=>($extra_field->field_type == "selectlist") ? "select" : "select multiple",
	                             "name"=>$extra_field->name,
	                             "options"=>$values_labels,
	                             "size"=>($extra_field->field_type == "selectlist") ? 1 : $extra_field->size,
	                             "value"=>($extra_field->field_type == "selectlist") ?  $extra_fields_data[substr($extra_field->name,3)]->value : explode("|",$extra_fields_data[substr($extra_field->name,3)]->value),
															 "extrahtml"=>"class=\"inputbox\""));
					$f->show_element($extra_field->name);

				} elseif ($extra_field->field_type == "checkbox") {

					# --- Checkboxes ----------------------------------
					$values = explode("|",$extra_field->field_elements);

					# - Generate arrays of values and labels
					$j = 0;
					$f->add_element(array("type"=>"checkbox",
																"name"=>$extra_field->name,
																"multiple"=>1,
																"value"=>explode("|",$extra_fields_data[substr($extra_field->name,3)]->value)));
					foreach($values AS $value) {
						$f->show_element($extra_field->name,trim($value));
						echo "&nbsp;".$value."<br />";
					}
				} elseif ($extra_field->field_type == "radiobutton") {

					# --- Radio Buttons ----------------------------------
					$values = explode("|",$extra_field->field_elements);

					# - Generate arrays of values and labels
					$j = 0;
					$f->add_element(array("type"=>"radio",
															 "name"=>$extra_field->name,
															 "value"=>trim($extra_fields_data[substr($extra_field->name,3)]->value)));
					foreach($values AS $value) {
						$f->show_element($extra_field->name,trim($value));
						echo "&nbsp;".$value."<br />";
					}
				} else {
					echo $extra_field->field_type;
				}
				echo '</td></tr>';
			}
		} // FIN du if(isRelToType)

		if ( count( $extra_fields ) <= 0 || $s==0 ) {
			echo '<tr><td align="center"><br /><a href="index2.php?option=com_hotproperty&task=listprop_ef">' . 'There are no published Extra Fields' . '</a><br /><br /></td></tr>';
		}
		?>
    </table>
	<?php
	$tabs->endTab();
	} // FIN du if($row->id) qui de la m�me mani�re que pour l'onglet 'Photos' s'assure que la propri�t� existe(et donc qu'un type � �t� sp�cifi�') avant de l'afficher

	if ($row->id) {
		$tabs->startTab("Photos","photos-page");
	?>
		<table cellpadding="5" cellspacing="0" border="0" width="100%" class="adminform">
	    <tr>
        <td align="center">
				<iframe src="index2.php?option=com_hotproperty&task=listprop_image&property=<?php echo $row->id; ?>" name="imgManager" id="imgManager" width="100%" height="<?php echo (($num_of_images * 110)+294); ?>" marginwidth="0" marginheight="0" align="top" scrolling="auto" frameborder="0" hspace="0" vspace="0" background="white"></iframe>
				</td>
      </tr>
    </table>
	<?php
		$tabs->endTab();
	} ?>

	<?php
	$tabs->startTab(_HP_PUBLISHING,"publishing-page");
	?>
    <table cellpadding="5" cellspacing="0" border="0" width="100%" class="adminform">
	    <tr>
        <td valign="top" align="right"><?php echo _E_STATE; ?></td>
        <td align="left"><?php
					if ($row->approved)	echo $lists['state'];
					else echo _HP_EDITPROP_MSG_02;
				?></td>
      </tr>
	    <tr>
        <td valign="top" align="right"><?php echo _E_CREATED; ?></td>
        <td align="left"><input class="inputbox" type="text" name="created" id="created" size="25" maxlength="19" value="<?php echo $row->created; ?>" />
          <input name="reset" type="reset" class="button" onClick="return showCalendar('created', 'y-mm-dd');" value="..."></td>
      </tr>
      <tr>
        <td width="20%" align="right"><?php echo _E_START_PUB; ?></td>
        <td width="80%" align="left"> <input class="inputbox" type="text" name="publish_up" id="publish_up" size="25" maxlength="19" value="<?php echo $row->publish_up; ?>" />
          <input type="reset" class="button" value="..." onClick="return showCalendar('publish_up', 'y-mm-dd');">
        </td>
      </tr>
      <tr>
        <td width="20%" align="right"><?php echo _E_FINISH_PUB; ?></td>
        <td width="80%" align="left"> <input class="inputbox" type="text" name="publish_down" id="publish_down" size="25" maxlength="19" value="<?php echo $row->publish_down; ?>" />
          <input type="reset" class="button" value="..." onClick="return showCalendar('publish_down', 'y-mm-dd');">
        </td>
      </tr>
			<tr>
				<td colspan="2"><?php echo _E_METADATA .' '. _E_M_DESC; ?>
				<br /><textarea class="inputbox" cols="30" rows="3" style="width:300px; height:50px" name="metadesc" width="500"><?php echo str_replace('&','&amp;',$row->metadesc); ?></textarea>
				</td>
			</tr>
				<tr>
				<td colspan="2"><?php echo _E_METADATA .' '. _E_M_KEY; ?>
				<br /><textarea class="inputbox" cols="30" rows="3" style="width:300px; height:50px" name="metakey" width="500"><?php echo str_replace('&','&amp;',$row->metakey); ?></textarea>
				</td>
			</tr>

    </table>
	<?php
	$tabs->endTab();
	?>
	<?php
		if (count($extensions) > 0) {
			$j = 5;
			foreach($extensions AS $ext) {
				$tabs->startTab($ext->name,$ext->option."-page");
			?>
    <table cellpadding="5" cellspacing="0" border="0" width="100%" class="adminform">
	    <tr>
        <td align="center">
				<iframe src="index2.php?option=<?php echo $ext->option; ?>&task=ext_main&property_id=<?php echo $row->id; ?>" name="<?php echo $ext->option; ?>" id="<?php echo $ext->option; ?>" width="100%" height="<?php echo ((3 * 110)+294); ?>" marginwidth="0" marginheight="0" align="top" scrolling="auto" frameborder="0" hspace="0" vspace="0" background="white"></iframe>
				</td>
      </tr>
    </table>
			<?php
				$tabs->endTab();
			}
		}
	$tabs->endPane();

	echo "<script type=\"text/javascript\">\n";
  echo "  tabPane1.setSelectedIndex( ".$activetab.");";
	echo "</script>";
	?>
	<input type="hidden" name="option" value="<?php echo $option; ?>">
	<input type="hidden" name="id" value="<?php echo $row->id; ?>">
	<input type="hidden" name="Itemid" value="<?php echo $Itemid; ?>">
	<input type="hidden" name="task" value="">
	</form>
	<script language="javascript">
	<!--
		var currentType = document.adminForm.type.value;
	-->
	</script>
	<?php
	}

 /*******************
 * Image Management *
 *******************/
	function listprop_starthtml_image($msg='') {
	?>
	<html>
	<head>
	<link rel="stylesheet" href="administrator/css/admin.css" type="text/css" />
	</head>
	<body style="background-color:#f5f5f5;">
		<?php if ($msg <> '') { ?>
		<table cellpadding="5" cellspacing="0" border="0" width="100%" style="background-color:#ffffff; border: 3px solid #e5e5e5;">
			<tr>
				<td colspan="2"><?php echo $msg; ?></td>
			</tr>
		</table>
		<?php }
	}

	function listprop_endhtml_image() {
?>
	</body>
	</html>
<?php
	}

	function listprop_uploadnew_image($property) {
	?>
		<form action="index2.php?option=com_hotproperty&task=listprop_upload_image&property=<?php echo $property; ?>" method="post" enctype="multipart/form-data">
		<table cellpadding="5" cellspacing="0" border="0" width="100%" style="background-color:#e5e5e5">
			<tr>
				<td colspan="2"><strong><?php echo _HP_IMGUPLOAD_TITLE; ?><strong></td>
			</tr>
		</table>
		<table cellpadding="5" cellspacing="0" border="0" width="100%">
			<tr>
				<td width="10%"><strong><?php echo _E_TITLE; ?></strong></td>
				<td width="90%"><input type="text" name="title" size="30" class="inputbox" /></td>
			</tr>
			<tr>
				<td valign="top"><strong><?php echo _E_M_DESC; ?></strong></td>
				<td><textarea name="desc" value="<?php echo $row->desc; ?>" class="inputbox" rows="6" cols="50"></textarea></td>
			</tr>
			<tr>
				<td valign="top"><strong><?php echo _HP_IMGUPLOAD_PHOTO; ?></strong></td>
				<td><input class="inputbox" type="file" name="image" /></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" class="button" value="<?php echo _HP_IMGUPLOAD_SUBMIT; ?>" /></td>
			</tr>
		</table>
		</form>
	<?php
	}

	function listprop_edit_image($row, $option) {
		global $mosConfig_live_site, $hp_imgdir_standard;
		?>
		<form action="index2.php?option=com_hotproperty&task=listprop_upload_image&property=<?php echo $row->property; ?>" method="post" enctype="multipart/form-data">
		<table cellpadding="5" cellspacing="0" border="0" width="100%" style="background-color:#e5e5e5">
			<tr>
				<td colspan="2"><strong><?php echo _HP_IMGEDIT_TITLE; ?><strong></td>
			</tr>
		</table>
		<p />
		<center><img style="border: 5px solid #c0c0c0;" alt="<?php echo $row->title; ?>" src="<?php echo $mosConfig_live_site.$hp_imgdir_standard.$row->standard; ?>"></center>
		<p />
		<table cellpadding="5" cellspacing="0" border="0" width="100%">
			<tr>
				<td width="240"><strong><?php echo _E_TITLE; ?></strong></td>
				<td width="85%"><input type="text" name="title" size="30" class="inputbox" value="<?php echo $row->title; ?>" /></td>
			</tr>
			<tr>
				<td valign="top"><strong><?php echo _E_M_DESC; ?></strong></td>
				<td><textarea name="desc" class="inputbox" rows="6" cols="50"><?php echo $row->desc; ?></textarea></td>
			</tr>
			<tr>
				<td valign="top"><strong><?php echo _HP_IMGEDIT_CHANGEPHOTO; ?></strong></td>
				<td><input class="inputbox" type="file" name="image" /></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" class="button" value="<?php echo _CMN_SAVE; ?>" />&nbsp;<input type="button" class="button" value="<?php echo _HP_IMGEDIT_BTN_DELPHOTO; ?>" onclick="javascript:if (confirm('<?php echo _HP_IMGEDIT_MSG_01; ?> ')){ 	document.location.href='index2.php?option=com_hotproperty&task=listprop_delete_image&property=<?php echo $row->property; ?>&id=<?php echo $row->id; ?>';}" />&nbsp;<input type="button" class="button" value="<?php echo _CMN_CANCEL; ?>" onclick="document.location.href='index2.php?option=com_hotproperty&task=listprop_image&property=<?php echo $row->property; ?>'" /></td>
			</tr>
		</table>
		<input type="hidden" name="id" value="<?php echo $row->id; ?>" />
		</form>
		<?php
	}

	function listprop_image(&$rows, $property, $option, $msg='') {
		global $mosConfig_live_site, $hp_imgdir_thumb, $hp_img_noimage_thumb;
	?>
		<p />
		<table cellpadding="5" cellspacing="0" border="0" width="100%" bgcolor="#f5f5f5">
			<tr style="background-color:#e5e5e5;">
				<td align="center">#</td>
				<td colspan="4" width="100%" align="left"><strong><?php echo _HP_IMGEDIT_ASSIGNEDPHOTOS; ?></strong></td>
			</tr>
		<?php
			$i=0;
			$n=count($rows);
			if ($n <= 0) echo '<tr><td colspan="4" align="center"><br />'._HP_IMGEDIT_MSG_02.'</td></tr>';
			foreach($rows AS $row) {	?>
				<tr style="<?php echo (($i%2)==0) ? "background-color: #f5f5f5;":"background-color: #ffffff;" ?>">
					<td width="20" rowspan="3" align="center"><?php echo $i+1; ?></td>
					<td rowspan="3" align="center"><a href="index2.php?option=com_hotproperty&task=listprop_edit_image&property=<?php echo $property; ?>&id=<?php echo $row->id; ?>"><img alt="<?php echo _HP_IMGEDIT_MSG_03; ?>" style="border: 1px solid #d5d5d5;" align="left" hspace="6" src="<?php
						if (!empty($row->thumb)) {
							echo $mosConfig_live_site.$hp_imgdir_thumb.$row->thumb;
						} else {
							echo $mosConfig_live_site.$hp_imgdir_thumb.$hp_img_noimage_thumb;
						}					?>"></a></td>
					<td width="90%"><strong><?php echo $row->title; ?></strong></td>
					<td width="60">

			<?php		if (($i > 0 ) && $row->property == @$rows[$i-1]->property) { ?>
        <a href="index2.php?option=com_hotproperty&task=orderup_images&id=<?php echo $row->id; ?>">
        <img src="administrator/images/uparrow.png" border="0" alt="<?php echo _HO_MOVEUP; ?>">
        </a>
        <?php		} else { echo "&nbsp;"; } ?>
					</td>
					<td width="60">
			<?php		if (($i < $n-1 ) && $row->property == @$rows[$i+1]->property) { ?>
        <a href="index2.php?option=com_hotproperty&task=orderdown_images&id=<?php echo $row->id; ?>">
        <img src="administrator/images/downarrow.png" border="0" alt="<?php echo _HO_MOVEDOWN; ?>">
        </a>
        <?php		} else { echo "&nbsp;"; } ?>
					</td>
				</tr>
				<tr style="<?php echo (($i%2)==0) ? "background-color: #f5f5f5;":"background-color: #ffffff;" ?>">
					<td height="50" valign="top" colspan="3"><?php echo $row->desc; ?></td>
				</tr>
				<tr height="10" style="<?php echo (($i%2)==0) ? "background-color: #f5f5f5;":"background-color: #ffffff;" ?>">
					<td valign="top" colspan="3"><a href="javascript:if (confirm('<?php echo _HP_IMGEDIT_MSG_01; ?>')){ 	document.location.href='index2.php?option=com_hotproperty&task=listprop_delete_image&property=<?php echo $property; ?>&id=<?php echo $row->id; ?>';}"><?php echo _HP_IMGEDIT_BTN_DELPHOTO2; ?></a></td>
				</tr>
				<?php
				$i++;
			}
		?>
		</table>
	<?php
 }

}

?>