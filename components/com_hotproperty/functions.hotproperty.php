<?php
//functions.hotproperty.php

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/**
* Retrieve property fields and caption.
*
* @package Hot Property 0.9
* @copyright (C) 2004-2006 Lee Cher Yeong
* @url http://www.Mosets.com/
* @author Lee Cher Yeong <hotproperty@mosets.com>
* 
* @param string $which			(featured|listing|all) What kind of property to process
* @param string $where			Extra SQL Query
* @param object $prop			Var Blank variable to be assigned with properties data
* @param object $prop_fields	Var Blank variable to be assigned with properties' caption
*/

/**
 * Include virtuebook code
 */

$vb_path = $mosConfig_absolute_path.'/components/com_virtuemart/virtuemart_parser.php';
if (file_exists($vb_path)) {
	require_once($vb_path);		
}

function HP_getProperty($which='all', $where, &$prop, &$prop_fields, $limitstart='0', $limit='9999', $sort='', $order='') {
	global $database, $mosConfig_mbf_content, $hp_default_order, $hp_default_order2, $mosConfig_absolute_path, $my, $ps_booking;
	
	# Assign default order
	if (empty($sort) && empty($order)) {
		$sort = $hp_default_order;
		$order = $hp_default_order2;
	}
	
	if($order == 'sudorand'){
		$myOrdering = $orderBy."random";
	}else{		
		$myOrdering = "p.".$sort." ".$order;
	}
	
	//Create random number in session used to randomize resuilts
	if(!isset($_SESSION['random'])) $_SESSION['random'] = rand(0, 1000);
	
	if($which == "featured"){
		$ordering = 'f.ordering ASC';		
	}else{		
		switch($sort){			
			case 'rand':
				$ordering = 'RAND()';
				break;				
			case 'sudorand':
				$ordering = 'sudorand';
				break;				
			case 'featured':
				$ordering2 = ($hp_default_order == 'sudorand' ? 'sudorand' : "p.$hp_default_order $order");
				$ordering = "p.featured DESC, f.ordering ASC, $ordering2";
				break;				
			case '':
			default:
				$ordering = "p.$sort $order";
				break;		
		}
	}

	if ($which <> "listing" && $which <> "featured" && $which <> "all") $which = "all";

	$sql = "SELECT p.*, a.id AS agentid, a.name AS agent, a.user AS user, c.name AS company, c.id AS companyid, t.name AS type, p.type AS typeid , CRC32(p.id * ".$_SESSION['random'].") as sudorand"
			. "\n FROM (#__hp_properties AS p, #__hp_companies AS c)"
			. "\n LEFT JOIN #__hp_agents AS a ON p.agent = a.id"
			. "\n LEFT JOIN #__hp_prop_types AS t ON p.type = t.id"
			. "\n LEFT JOIN #__hp_featured AS f ON f.property = p.id"
			. "\n WHERE p.published='1' AND p.approved='1' AND t.published='1'"
			. "\n AND a.company=c.id"
			. "\n AND (publish_up = '0000-00-00 00:00:00' OR publish_up <= NOW())"
			. "\n AND (publish_down = '0000-00-00 00:00:00' OR publish_down >= NOW())"
			.(count( $where ) ? " AND " . implode( ' AND ', $where ) : "")
			.(($which == "featured") ? "\nAND p.featured='1'" : "")
			. "\n ORDER BY $ordering "
			. "\n LIMIT ".$limitstart.", ".$limit;
	$database->setQuery( $sql );
	$properties = $database->loadObjectList();
	
	if ($database->getErrorNum() > 0) {
		echo $database->getErrorMsg();
	}
	
	# Get the listing/featured/all fields (For core and non-core fields)
	$database->setQuery("SELECT name FROM #__hp_prop_ef "
						. "\nWHERE published=1 AND hidden=0 AND iscore=0"
						. (($which <> "all") ? " AND ".$which."='1'" : "")
						. "\n ORDER BY ordering");
	$fields_isnotcore = $database->loadResultArray();

	$database->setQuery( "SELECT name FROM #__hp_prop_ef "
		. "\nWHERE published=1 AND hidden=0 AND iscore=1"
		.	(($which <> "all") ? " AND ".$which."='1'" : "")
		.	"\nORDER BY ordering" );
	$fields_iscore = $database->loadResultArray();

	# Get the fields name, captions, field type, prefix text and append text, hideCaption
	$database->setQuery( "SELECT * FROM #__hp_prop_ef"
		. "\nWHERE published=1 AND hidden=0"
		.	(($which <> "all") ? " AND ".$which."='1'" : "") 
		);
	$prop_fields = $database->loadObjectList("name");

	# Retrieve list of Agents, Types and Companies for Mambelfish translation
	# Not needed, but coded as a workaround for Mf to translate
	if ( $mosConfig_mbf_content ) {
		$database->setQuery( "SELECT * FROM #__hp_prop_types WHERE 1=1" );
		$types = $database->loadObjectList("id");
		$database->setQuery( "SELECT * FROM #__hp_companies WHERE 1=1" );
		$companies = $database->loadObjectList("id");
		$database->setQuery( "SELECT * FROM #__hp_agents WHERE 1=1" );
		$agents = $database->loadObjectList("id");
	}

	$i = 0;
	foreach($properties AS $p) {
		# Assign name, property ID, agent ID (Compulsory)
		$prop[$i]->name = $properties[$i]->name;
		$prop[$i]->id = $properties[$i]->id;
		$prop[$i]->companyid = $properties[$i]->companyid;
		$prop[$i]->agentid = $properties[$i]->agentid;
		$prop[$i]->user = $properties[$i]->user;
		$prop[$i]->typeid = $properties[$i]->typeid;
		$prop[$i]->metakey = $properties[$i]->metakey;
		$prop[$i]->metadesc = $properties[$i]->metadesc;
		$prop[$i]->intro_text = $properties[$i]->intro_text;
		
		# Assign core fields
		if (!empty($fields_iscore)) {
			foreach($fields_iscore AS $ff) {
				
				# On charge l'id du champ extra:
				$database->setQuery( "SELECT id FROM #__hp_prop_ef "
				. "\nWHERE name='".$ff."'" );
				$ef_id=$database->loadResult();
				#on charge l'extra-field:
				$ef=new mosHPPropEF($database);
				$ef->load($ef_id);
			
				# Affichage de l'extra-field si celui-ci est li� � une cat�gorie englobante de celle de la propri�t�:
				if( $ef->isRelToType($properties[$i]->typeid) ) {
				
					// Manually handling for Type, Agent Name and Company Name - As a workaround for Mambelfish
					if ( $mosConfig_mbf_content ) {
	
						switch($ff) {
							case "type":
								$prop[$i]->$ff = $types[$properties[$i]->typeid]->name;
								break;
							case "agent":
								$prop[$i]->$ff = $agents[$properties[$i]->agentid]->name;
								break;
							case "company":
								$prop[$i]->$ff = $companies[$properties[$i]->companyid]->name;
								break;
							default:
								$prop[$i]->$ff = $properties[$i]->$ff;
								break;
						}
					} else {						
						$prop[$i]->$ff = $properties[$i]->$ff;
					}

				}

			}
		}

		# Assign non-core fields
		if (!empty($fields_isnotcore)) {
			foreach($fields_isnotcore AS $ff) {
				
				# On charge l'id du champ extra:
				$database->setQuery( "SELECT id FROM #__hp_prop_ef "
				. "\nWHERE name='".$ff."'" );
				$ef_id=$database->loadResult();
				#on charge l'extra-field:
				$ef=new mosHPPropEF($database);
				$ef->load($ef_id);
			
				if( $ef->isRelToType($properties[$i]->typeid) )
				{
					unset($tmp);
					$database->setQuery( 
						"SELECT p.* FROM (#__hp_properties2 AS p, #__hp_prop_ef AS ef) "
						. "\nWHERE p.property=".$prop[$i]->id
						.	"\nAND ef.name='".$ff."'"
						.	"\nAND p.field = ef.id"
						. "\nLIMIT 0,1");
	
					$database->loadObject( $tmp );
					
					$prop[$i]->$ff = (is_object($tmp) ? $tmp->value : '');
				}

			}
		}

		# Get the thumbnail
		$database->setQuery( "SELECT thumb, title FROM #__hp_photos"
			. "\nWHERE property='".$p->id."'"
			. "\nORDER BY ordering ASC"
			. "\nLIMIT 0,1");
		$thumb = $database->loadObjectList();
		if ( count($thumb) > 0 ) {
			$prop[$i]->thumb = $thumb[0]->thumb;
			$prop[$i]->thumb_title = $thumb[0]->title;
		} else {
			$prop[$i]->thumb = '';
			$prop[$i]->thumb_title = '';
		}
		
		// VirtueBook pricing object
		if(is_object($ps_booking)){
			$prop[$i]->price = $ps_booking->getPrice($p->id);
			$prop[$i]->special = $ps_booking->getPropertySpecial($p->id);
		}
						
		$i++;
	}
	return true;
}

/**
* Return true if this property is published and it's corresponding type is published. Return false otherwise
* @param int Property's ID
*/
function HP_isPropPublished($id) {
	global $database;

	# If ID is 0, it is invalid
	if ($id == 0) return false;

	# Check if the property and it's type is published
	$database->setQuery("SELECT p.id FROM #__hp_properties AS p"
		. "\nLEFT JOIN #__hp_prop_types AS t ON t.id=p.type"
		.	"\nWHERE p.published=1 AND p.approved='1' AND t.published=1 AND p.id='".$id."'"
		. "\n	AND (publish_up = '0000-00-00 00:00:00' OR publish_up <= NOW())"
		. "\n	AND (publish_down = '0000-00-00 00:00:00' OR publish_down >= NOW())"
		);
	$database->query();
	if($database->getNumRows() <= 0) return false;

	return true;
}

/**
* Return true if this type is published. Return false otherwise
* @param int Type's ID
*/
function HP_isTypePublished($id) {
	global $database;

	# If ID is 0, it is invalid
	if ($id == 0) return false;

	# Check if the type is published
	$database->setQuery("SELECT id FROM #__hp_prop_types"
		.	"\nWHERE published=1 AND id='".$id."'");
	$database->query();
	if($database->getNumRows() <= 0) return false;

	return true;
}

/**
* Return true if $email resemble an email address
* @param int Type's ID
*/
function is_email($email){
	$rBool=false;
	
	if(preg_match("/[\w\.\-]+@\w+[\w\.\-]*?\.\w{1,4}/", $email)){
		$rBool=true;
	}
	return $rBool;
}

/**
* Return the number of properties for a type
* @param int Type's ID
*/
function HP_getNumProp_GroupType($type) {
	global $database;
	$database->setQuery( "SELECT COUNT(*) AS total FROM #__hp_properties AS p"
		. "\nLEFT JOIN #__hp_prop_types AS t ON t.id=p.type"
		.	"\nWHERE p.published=1 AND p.approved=1 AND t.published=1"
		. "\n AND p.type='".$type."'"
		. "\n	AND (publish_up = '0000-00-00 00:00:00' OR publish_up <= NOW())"
		. "\n	AND (publish_down = '0000-00-00 00:00:00' OR publish_down >= NOW())");
	return $database->loadResult();
}

/**
* Return the number of properties handle by an agent
* @param int Type's ID
*/
function HP_getNumProp_GroupAgent($id) {
	global $database;
	$database->setQuery( "SELECT COUNT(*) AS total FROM #__hp_properties AS p"
		. "\nLEFT JOIN #__hp_prop_types AS t ON t.id=p.type"
		. "\nWHERE p.published='1' AND p.approved='1' AND t.published='1'"
		. "\n AND p.agent='".$id."'"
		. "\n	AND (publish_up = '0000-00-00 00:00:00' OR publish_up <= NOW())"
		. "\n	AND (publish_down = '0000-00-00 00:00:00' OR publish_down >= NOW())");
	return $database->loadResult();
}

function HP_logSearch($search_term) {
	global $database, $hp_log_search;

	if ($hp_log_search) {
		$database->setQuery( "SELECT hits"
		. "\nFROM #__hp_log_searches"
		. "\nWHERE LOWER(search_term)='$search_term'" );
		$hits = intval( $database->loadResult() );
		echo $database->getErrorMsg();
		if ($hits) {
			$database->setQuery( "UPDATE #__hp_log_searches SET hits=(hits+1)"
			. "\nWHERE LOWER(search_term)='$search_term'" );
			$database->query();
			echo $database->getErrorMsg();
		} else {
			$database->setQuery( "INSERT INTO #__hp_log_searches VALUES"
			. "\n('$search_term','1')" );
			$database->query();
			echo $database->getErrorMsg();
		}
	}
}

function HP_useDefaultLang( $lang, $url ) {
	global $database;

	if ( !empty($lang) ) {
		// Get the default mambelfish language
		$database->setQuery("SELECT MIN(ordering) AS first_language FROM #__mbf_language");
		$first_ordering = $database->loadResult();
		$database->setQuery("SELECT * FROM #__mbf_language WHERE ordering = '".$first_ordering."' LIMIT 1");
		$database->loadObject($mbf_language);
		if ( count($mbf_language) == 1 && $mbf_language->iso <> $lang) {
			mosRedirect( $url."&lang=".$mbf_language->iso );
			exit();
		}
	}

}

?>
