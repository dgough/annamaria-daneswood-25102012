<?php

# Main
DEFINE ('_HP_COM_TITLE','Auto Listing');
DEFINE ('_HP_MORE','More...');
DEFINE ('_HP_MOREFEATURED','More featured autos...');

DEFINE ('_HP_COMPANY','Company');
DEFINE ('_HP_CONTACTNUMBER','Contact Number');
DEFINE ('_HP_ADDRESS','Address');
DEFINE ('_HP_PROPERTY','Auto');
DEFINE ('_HP_PROPERTIES','Autos');
DEFINE ('_HP_ARROW','&gt;');
DEFINE ('_HP_SEARCH','Search');
DEFINE ('_HP_SEARCH_ADV','Advanced Search');
DEFINE ('_HP_IN','in');
DEFINE ('_HP_PHOTO', 'Photo');
DEFINE( '_HP_EXTRAFIELDS', 'Extra Fields');
DEFINE( '_HP_PHOTOS', 'Photos');
DEFINE( '_HP_PUBLISHING', 'Publishing');
DEFINE ('_HP_TYPE', 'Type');
DEFINE ('_HP_SUBURB', 'Suburb');
DEFINE ('_HP_STATE', 'State');
DEFINE ('_HP_COUNTRY', 'Country');
DEFINE ('_HP_POSTCODE', 'Postcode');
DEFINE ('_HP_PRICE', 'Price');
DEFINE ('_HP_FEATURED', 'Featured');
DEFINE ('_HP_MOVEUP', 'Move Up');
DEFINE ('_HP_MOVEDOWN', 'Move Down');
DEFINE ('_HP_ANYTIME','Anytime');
DEFINE ('_HP_RSSFEED', 'Get the latest autos direct to your desktop');
DEFINE ('_HP_COM_PATHWAY_MAIN', 'Main');
DEFINE ('_HP_RESET', 'Reset');

# Sort
DEFINE ('_HP_SORT_BY','Sorted by:');
DEFINE ('_HP_SORT_LOWEST_PRICE','Price: Low to High');
DEFINE ('_HP_SORT_HIGHEST_PRICE','Price: High to Low');
DEFINE ('_HP_SORT_PRICE','Price');
DEFINE ('_HP_SORT_AZ','AZ');
DEFINE ('_HP_SORT_A_Z','A-Z');
DEFINE ('_HP_SORT_Z_A','Z-A');
DEFINE ('_HP_SORT_ASC','&lt;&lt;');
DEFINE ('_HP_SORT_DESC','&gt;&gt;');

# Featured
DEFINE ('_HP_FEATURED_EMPTY','No featured auto to display');
DEFINE ('_HP_FEATURED_TITLE','Featured');

# Types
DEFINE ('_HP_TYPES_TITLE','Vehicle Types');

#View
DEFINE ('_HP_VIEW_AGENT_TITLE','Agent');
DEFINE ('_HP_VIEW_FEATURES_TITLE','More Information');
DEFINE ('_HP_VIEW_AGENT_CONTACT','Send enquiry / Make an appointment');
DEFINE ('_HP_SENDENQUIRY','Send Enquiry');

# Search
DEFINE ('_HP_SEARCH_TITLE','Search');
DEFINE ('_HP_SEARCH_ADV_TITLE','Advanced Search');
DEFINE ('_HP_SEARCH_RESULT_TITLE','Search Results');
DEFINE ('_HP_SEARCH_TEXT','Search Auto:');
DEFINE ('_HP_SEARCH_SELECTAGENT', 'Select Agent');
DEFINE ('_HP_SEARCH_SELECTCOMPANY', 'Select Company');
DEFINE ('_HP_SEARCH_FROM','From ');
DEFINE ('_HP_SEARCH_TO','To ');
DEFINE ('_HP_SEARCH_ALLTYPES','All Types');
DEFINE ('_HP_SEARCH_ALLAGENTS','All Agents');
DEFINE ('_HP_SEARCH_ALLCOMPANIES', 'All Companies');
DEFINE ('_HP_SEARCH_EXACTLY', 'Exactly');
DEFINE ('_HP_SEARCH_MORE_THAN', 'More Than');
DEFINE ('_HP_SEARCH_LESS_THAN', 'Less Than');
DEFINE ('_HP_SEARCH_NOSEARCH','You did not type in a search. Please type in your search and try again.');
DEFINE ('_HP_SEARCH_NOTVALID','You did not type in a valid search. Please type in your search and try again.');
DEFINE ('_HP_SEARCH_NORESULT','Your search did not match any auto. Please try again.');
DEFINE ('_HP_SEARCH_TRYAGAIN', 'Try again');

# Agent
DEFINE ('_HP_PROPBYAGENT','Autos handled by ');
DEFINE ('_HP_AGENT_SENDEMAIL','Send an email to this agent');
DEFINE ('_HP_AGENT_COMPANY','Company');
DEFINE ('_HP_AGENT_MOBILE','Mobile');
DEFINE ('_HP_AGENT_ERROR_EMPTY','No Agent');

# Company
DEFINE ('_HP_CO_TITLE','Company');
DEFINE ('_HP_CO_SENDEMAIL','Send an email to this company');
DEFINE ('_HP_CO_CONTACT','Send Enquiry / Feedback');
DEFINE ('_HP_CO_WEBSITE', 'Website:');

#Property
DEFINE ('_HP_PROP_ERROR_EMPTY','No Auto exists');
DEFINE ('_HP_SELECT_PROP_TYPE','Select Auto Type');

# Contact
DEFINE ('_HP_CONTACT_ERR_COMPLETE','Please complete the form before submiting');
DEFINE ('_HP_CONTACT_ERR_ONECONTACT','Please enter at least one contact(email/contact number), so that our agent is able to contact you directly.');
DEFINE ('_HP_CONTACT_ERR_VALIDEMAIL','Please enter a valid email address.');
DEFINE ('_HP_CONTACT_THANK','Thank you for your enquiry. Our agent will contact you as soon as possible.');
DEFINE ('_HP_CONTACT_ENQUIRY_TEXT','This is an enquiry from ');
DEFINE ('_HP_CONTACT_ENQUIRY_TEXT2',"------\n\nYou may see the vehicle's info at the address below:");
DEFINE ('_HP_CONTACT_ENQUIRY_SUBJECTP','Vehicle\'s Enquiry - ');
DEFINE ('_HP_CONTACT_ENQUIRY_SUBJECTA','Agent\'s Enquiry');
DEFINE ('_HP_CONTACT_ENQUIRY_SUBJECTC','Company\'s Enquiry');

# Agent Menu
DEFINE ('_HP_MENU_MANAGEPROPERTY', 'View My Autos');
DEFINE ('_HP_MENU_MANAGEPROPERTY2', 'Manage My Autos');
DEFINE ('_HP_MENU_NOTAVAILABLE', 'Not Available');
DEFINE ('_HP_MENU_EDITAGENT' , 'My Agent Profile');
DEFINE ('_HP_MENU_ADDPROPERTY' , 'Add Vehicle'); 

# Edit Agent
DEFINE ('_HP_EDITAGENT', 'My Agent Profile');
DEFINE ('_HP_AGENT_NAME', 'Agent Name: ');
DEFINE ('_HP_AGENT_REMOVEPHOTO', 'Remove this photo');
DEFINE ('_HP_AGENT_NOPHOTO','No Photo');
DEFINE ('_HP_AGENT_MSG_01', 'Your Agent Profile have been successfully saved!');

# Edit Property
DEFINE ('_HP_EDITPROP_SAVEINSERTPHOTO', 'Save & Insert Photos');
DEFINE ('_HP_EDITPROP_PROPERTYNAME', 'Auto Name:');
DEFINE ('_HP_EDITPROP_INTROTEXT', 'Intro Text');
DEFINE ('_HP_EDITPROP_FULLTEXT', 'Full Text');
DEFINE ('_HP_EDITPROP_NOTES', 'Notes');
DEFINE ('_HP_EDITPROP_NOTES_MSG', '* Notes are hidden to end-user');
DEFINE ('_HP_EDITPROP_MSG_01', 'Your Auto have been successfully saved!');
DEFINE ('_HP_EDITPROP_MSG_02', 'Awaiting Approval');
DEFINE ('_HP_EDITPROP_MSG_03', 'You session has expired. Please login again before submitting any new auto.');
DEFINE ('_HP_EDITPROP_NOPUBLISHEDEF', 'There are no published Extra Fields');

# Image Management
DEFINE ('_HP_IMGEDIT_TITLE', 'Edit Photo');
DEFINE ('_HP_IMGEDIT_CHANGEPHOTO', 'Change Photo:');
DEFINE ('_HP_IMGEDIT_ASSIGNEDPHOTOS', 'Assigned Photos');

DEFINE ('_HP_IMGUPLOAD_TITLE', 'Upload New Photo');
DEFINE ('_HP_IMGUPLOAD_PHOTO', 'Photo:');
DEFINE ('_HP_IMGUPLOAD_SUBMIT', 'Upload Photo');
DEFINE ('_HP_IMGUPLOAD_ERR_01', 'Please make sure you specified an image before uploading.');
DEFINE ('_HP_IMGUPLOAD_ERR_02', 'Please make sure you specified a valid image before uploading.');
DEFINE ('_HP_IMGUPLOAD_ERR_03', 'Only files of type gif, png, jpg or bmp can be uploaded.');
DEFINE ('_HP_IMGUPLOAD_ERR_04', 'There already exists a file with the same name. Please rename and upload again.');
DEFINE ('_HP_IMGUPLOAD_ERR_05', 'Error while deleting old photo');
DEFINE ('_HP_IMGUPLOAD_ERR_06', 'Error while deleting Original\'s old photo');
DEFINE ('_HP_IMGUPLOAD_ERR_07', 'Error while deleting Standard\'s old photo');
DEFINE ('_HP_IMGUPLOAD_ERR_08', 'Error while deleting Thumbnail\'s old photo');
DEFINE ('_HP_IMGUPLOAD_ERR_09', 'Error while deleting photo');

DEFINE ('_HP_IMGEDIT_MSG_01', 'Are you sure you want to delete selected photo?');
DEFINE ('_HP_IMGEDIT_MSG_02', 'No image assigned to this vehicle yet.');
DEFINE ('_HP_IMGEDIT_MSG_03', 'Click to edit this photo');

DEFINE ('_HP_IMGEDIT_BTN_DELPHOTO', 'Delete this photo');
DEFINE ('_HP_IMGEDIT_BTN_DELPHOTO2', '[Delete this photo]');
?>