<?php

# Main
DEFINE ('_HP_COM_TITLE','Liste der Ferienwohnungen und Villen');
DEFINE ('_HP_MORE','weiter...');
DEFINE ('_HP_MOREFEATURED','Weitere außergewöhnliche Ferienwohnungen und Villen...');

DEFINE ('_HP_COMPANY','Firma');
DEFINE ('_HP_CONTACTNUMBER','Kontakt / Telefonnummer');
DEFINE ('_HP_ADDRESS','Adresse');
DEFINE ('_HP_PROPERTY','Feriendomizil');
DEFINE ('_HP_PROPERTIES','Ferienwohnungen und Villen');
DEFINE ('_HP_ARROW','&gt;');
DEFINE ('_HP_SEARCH','Suche');
DEFINE ('_HP_SEARCH_STRAPLINE','Suche in allen Immobilien');
DEFINE ('_HP_SEARCH_ADV','Erweiterte Suche');
DEFINE ('_HP_IN','in');
DEFINE ('_HP_PHOTO', 'Foto');
DEFINE( '_HP_EXTRAFIELDS', 'Extra Fields'); 
DEFINE( '_HP_PHOTOS', 'Fotos');
DEFINE( '_HP_PUBLISHING', 'veröffentlichen');
DEFINE ('_HP_TYPE', 'Art');
DEFINE ('_HP_SUBURB', 'Außenbezirk');
DEFINE ('_HP_STATE', 'Staat');
DEFINE ('_HP_COUNTRY', 'Land');
DEFINE ('_HP_POSTCODE', 'Postleitzahl');
DEFINE ('_HP_PRICE', 'Preis');
DEFINE ('_HP_FEATURED', 'Außergewöhnliche Angebote');
DEFINE ('_HP_MOVEUP', 'nach oben');
DEFINE ('_HP_MOVEDOWN', 'nach unten');
DEFINE ('_HP_ANYTIME','jederzeit');
DEFINE ('_HP_RSSFEED', 'Holen Sie sich die neuesten Ferienwohnungen direkt auf Ihren Desktop');
DEFINE ('_HP_COM_PATHWAY_MAIN', 'Hauptfunktion');                    
DEFINE ('_HP_RESET', 'Zurücksetzen');

# Sort
DEFINE ('_HP_SORT_BY','sortiert nach:'); 
DEFINE ('_HP_SORT_LOWEST_PRICE','Preis: von niedrig nach hoch');
DEFINE ('_HP_SORT_HIGHEST_PRICE','Preis: von hoch nach niedrig');
DEFINE ('_HP_SORT_PRICE','Preis');
DEFINE ('_HP_SORT_AZ','AZ');
DEFINE ('_HP_SORT_A_Z','A-Z');
DEFINE ('_HP_SORT_Z_A','Z-A');
DEFINE ('_HP_SORT_ASC','&lt;&lt;');    
DEFINE ('_HP_SORT_DESC','&gt;&gt;');	

# Featured
DEFINE ('_HP_FEATURED_EMPTY','Keine außergewöhnliche Ferienwohnung verfügbar');
DEFINE ('_HP_FEATURED_TITLE','außergewöhnlich');

# Types
DEFINE ('_HP_TYPES_TITLE','Art der Ferienwohnung/Villa');

#View
DEFINE ('_HP_VIEW_AGENT_TITLE','Agent');
DEFINE ('_HP_VIEW_FEATURES_TITLE','weitere Informationen');
DEFINE ('_HP_VIEW_AGENT_CONTACT','Anfrage senden / Termin vereinbaren');
DEFINE ('_HP_SENDENQUIRY','Anfrage senden');

# Search
DEFINE ('_HP_SEARCH_TITLE','Suche');
DEFINE ('_HP_SEARCH_ADV_TITLE','erweiterte Suche');
DEFINE ('_HP_SEARCH_RESULT_TITLE','Suchergebnisse');
DEFINE ('_HP_SEARCH_TEXT','Suche Ferienwohnungen/Villen:');
DEFINE ('_HP_SEARCH_SELECTAGENT', 'Agent');
DEFINE ('_HP_SEARCH_SELECTCOMPANY', 'Firma auswählen');
DEFINE ('_HP_SEARCH_FROM','von ');
DEFINE ('_HP_SEARCH_TO','bis ');
DEFINE ('_HP_SEARCH_ALLTYPES','Alle Arten');
DEFINE ('_HP_SEARCH_ALLAGENTS','Alle Agenten');
DEFINE ('_HP_SEARCH_ALLCOMPANIES', 'Alle Firmen');
DEFINE ('_HP_SEARCH_EXACTLY', 'genau');
DEFINE ('_HP_SEARCH_MORE_THAN', 'mehr als');
DEFINE ('_HP_SEARCH_LESS_THAN', 'weniger als');
DEFINE ('_HP_SEARCH_NOSEARCH','Sie haben keine Suche eingegeben. Bitte geben Sie Ihre Suche ein und versuchen Sie es erneut.');
DEFINE ('_HP_SEARCH_NOTVALID', 'Sie haben keine gültige Suche eingegeben. Bitte geben Sie Ihre Suche ein und versuchen Sie es erneut.');
DEFINE ('_HP_SEARCH_NORESULT','Ihre Suche ergab keine Ergebnisse. Bitte versuchen Sie es noch einmal oder wählen Sie eine der nachfolgenden Ferienwohnungen und Villen aus.');
DEFINE ('_HP_SEARCH_TRYAGAIN', 'Versuchen Sie es noch einmal');
DEFINE ('_HP_ADVANCED_SEARCH', 'Erweiterte Suche');

DEFINE ('_HP_SEARCH_BY_NAME', 'Suche an Hand von Namen');


DEFINE ('_HP_SEARCH_HEADING', 'Ferienwohnungen - Suche');
DEFINE ('_HP_SEARCH_SEARCHED_FOR', 'Sie suchten nach:');
DEFINE ('_HP_SEARCH_MORE_LINK', 'weiter');
DEFINE ('_HP_SEARCH_PRICE_BAND', 'Preisgruppe');
DEFINE ('_HP_SEARCH_FILTER', 'Filtern');
DEFINE ('_HP_SEARCH_REFINE', 'Suche verfeinern');
DEFINE ('_HP_SEARCH_AREA', 'Anna Maria Island Urlaubsvermietung');
DEFINE ('_HP_SEARCH_BEDS', 'betten');

# Agent
DEFINE ('_HP_PROPBYAGENT','bearbeitet von');
DEFINE ('_HP_AGENT_SENDEMAIL','Eine Email an diesen Agenten senden');
DEFINE ('_HP_AGENT_COMPANY','Firma');
DEFINE ('_HP_AGENT_MOBILE','Handy');
DEFINE ('_HP_AGENT_ERROR_EMPTY','Kein Agent');

# Company
DEFINE ('_HP_CO_TITLE','Firma');
DEFINE ('_HP_CO_SENDEMAIL','Eine Email an diese Firma senden');
DEFINE ('_HP_CO_CONTACT','Anfrage senden / Feedback');
DEFINE ('_HP_CO_WEBSITE', 'Website:');

#Property
DEFINE ('_HP_PROP_ERROR_EMPTY','Ferienwohnung existiert nicht');
DEFINE ('_HP_SELECT_PROP_TYPE','Wählen Sie die Art der Ferienwohnung oder Villa');


DEFINE ('_HP_PROP_RETURN','Zurück zur Liste');
DEFINE ('_HP_PROP_ENLARGE_PHOTO','[ Foto vergrößern ]');
DEFINE ('_HP_PROP_KEY_FEATURES','Auf einen Blick');


DEFINE ('_HP_PROP_TAB_GENERAL_INFO','Allgemeine Informationen');
DEFINE ('_HP_PROP_TAB_LIVING_AREA','Wohnbereich');
DEFINE ('_HP_PROP_TAB_KITCHEN','Küche');
DEFINE ('_HP_PROP_TAB_BEDROOM','Schlafzimmer');
DEFINE ('_HP_PROP_TAB_OUTSIDE','Außenbereich');
DEFINE ('_HP_PROP_TAB_MAP','Standortkarte');
DEFINE ('_HP_PROP_TAB_BATHROOM','Badezimmer');
DEFINE ('_HP_PROP_TAB_VIDEO', 'Video');
DEFINE ('_HP_PROP_TAB_COMMENTS', 'Kommentare');

# Contact
DEFINE ('_HP_CONTACT_ERR_COMPLETE','Vor dem Absenden bitte Formular ausfüllen');
DEFINE ('_HP_CONTACT_ERR_ONECONTACT','Bitte geben Sie mindestens eine Kontaktmöglichkeit an (Email/Telefon-Nr.), damit unser Agent sie direkt erreichen kann.');
DEFINE ('_HP_CONTACT_ERR_VALIDEMAIL','Bitte geben Sie eine gültige E-Mail-Adresse an.');
DEFINE ('_HP_CONTACT_THANK','Vielen Dank für Ihre Anfrage. Unser Agent wird baldmöglichst mit Ihnen Kontakt aufnehmen.');
DEFINE ('_HP_CONTACT_ENQUIRY_TEXT','Dies ist eine Anfrage von');
DEFINE ('_HP_CONTACT_ENQUIRY_TEXT2',"------\n\nInformationen über die Ferienwohnung finden Sie unter nachstehender Adresse:");
DEFINE ('_HP_CONTACT_ENQUIRY_SUBJECTP','Ferienwohnung\Anfrage');
DEFINE ('_HP_CONTACT_ENQUIRY_SUBJECTA','Agent\Anfrage');
DEFINE ('_HP_CONTACT_ENQUIRY_SUBJECTC','Firma\Anfrage');

# Agent Menu
DEFINE ('_HP_MENU_MANAGEPROPERTY', 'Besichtigen Sie meine Ferienwohnungen');
DEFINE ('_HP_MENU_MANAGEPROPERTY2', 'Managen Sie meine Ferienwohnungen');
DEFINE ('_HP_MENU_NOTAVAILABLE', 'Nicht verfügbar');
DEFINE ('_HP_MENU_EDITAGENT' , 'Mein Profil als Agent');
DEFINE ('_HP_MENU_ADDPROPERTY' , 'Ferienwohnung hinzufügen'); 

# Edit Agent
DEFINE ('_HP_EDITAGENT', 'Mein Profil als Agent');
DEFINE ('_HP_AGENT_NAME', 'Name des Agenten: ');
DEFINE ('_HP_AGENT_REMOVEPHOTO', 'Dieses Foto entfernen');
DEFINE ('_HP_AGENT_NOPHOTO','Kein Foto');
DEFINE ('_HP_AGENT_MSG_01', 'Ihr Profil als Agent wurde erfolgreich gespeichert!');

# Edit Property
DEFINE ('_HP_EDITPROP_SAVEINSERTPHOTO', 'Fotos speichern & einfügen >>');
DEFINE ('_HP_EDITPROP_PROPERTYNAME', 'Name der Ferienwohnung:');
DEFINE ('_HP_EDITPROP_INTROTEXT', 'Einführungs-Text');
DEFINE ('_HP_EDITPROP_FULLTEXT', 'Kompletter Text');
DEFINE ('_HP_EDITPROP_NOTES', 'Anmerkungen');
DEFINE ('_HP_EDITPROP_NOTES_MSG', '* Anmerkungen bleiben dem Endverbraucher verborgen');
DEFINE ('_HP_EDITPROP_MSG_01', 'Die Angaben zu Ihrer Ferienwohnung wurden erfolgreich gespeichert!');
DEFINE ('_HP_EDITPROP_MSG_02', 'Zustimmung wird erwartet');
DEFINE ('_HP_EDITPROP_MSG_03', 'Ihre Sitzung ist abgelaufen. Für die Vorstellung weiterer Ferienwohnungen bitte erneut einloggen.');
DEFINE ('_HP_EDITPROP_NOPUBLISHEDEF', 'keine veröffentlichten Extra Fields vorhanden');

# Image Management
DEFINE ('_HP_IMGEDIT_TITLE', 'Foto editieren');
DEFINE ('_HP_IMGEDIT_CHANGEPHOTO', 'Foto ändern:');
DEFINE ('_HP_IMGEDIT_ASSIGNEDPHOTOS', 'zugeordnete Fotos'); 

DEFINE ('_HP_IMGUPLOAD_TITLE', 'neues Foto hochladen');
DEFINE ('_HP_IMGUPLOAD_PHOTO', 'Foto:');
DEFINE ('_HP_IMGUPLOAD_SUBMIT', 'Foto hochladen');
DEFINE ('_HP_IMGUPLOAD_ERR_01', 'Bilder vor dem Hochladen bitte genau benennen.');
DEFINE ('_HP_IMGUPLOAD_ERR_02', 'Gültige Bilder vor dem Hochladen bitte genau benennen.');
DEFINE ('_HP_IMGUPLOAD_ERR_03', 'Es können nur Dateien des Typs gif, png, jpg oder bmp hochgeladen werden.');
DEFINE ('_HP_IMGUPLOAD_ERR_04', 'Eine Datei dieses Namens existiert bereits. Bitte umbenennen und erneut hochladen.');
DEFINE ('_HP_IMGUPLOAD_ERR_05', 'Fehler beim Löschen des alten Fotos.');
DEFINE ('_HP_IMGUPLOAD_ERR_06', 'Fehler beim Löschen des Originals / alten Fotos');
DEFINE ('_HP_IMGUPLOAD_ERR_07', 'Fehler beim Löschen des Standardfotos / alten Fotos');
DEFINE ('_HP_IMGUPLOAD_ERR_08', 'Fehler beim Löschen des Vorschaubildes / alten Fotos');
DEFINE ('_HP_IMGUPLOAD_ERR_09', 'Fehler beim Löschen des Fotos');


DEFINE ('_HP_IMGEDIT_MSG_01', 'Sind Sie sicher, dass Sie das ausgewählte Foto löschen wollen?');
DEFINE ('_HP_IMGEDIT_MSG_02', 'Von dieser Ferienwohnung/Villa liegt noch kein Foto vor.');
DEFINE ('_HP_IMGEDIT_MSG_03', 'Klicken Sie hier um dieses Foto zu editieren');

DEFINE ('_HP_IMGEDIT_BTN_DELPHOTO', 'dieses Foto löschen');
DEFINE ('_HP_IMGEDIT_BTN_DELPHOTO2', '[dieses Foto löschen]');


DEFINE ('_HP_PEOPLE_ALSO_VIEWED', 'Personen interessierten sich auch für');
DEFINE ('_HP_MORE_DETAILS', 'Weitere Details...');

?>
