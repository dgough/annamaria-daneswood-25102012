<?php
// sef_ext_create.html.php
/**
* Hot Property
*
* This extension will give the SEF advance style URLs to Hot Property
* Place this file (sef_ext_create.php) in the main component directory:
*		/components/com_hotproperty/
*
* For SEF advance > v3.7
*
* @package Hot Property 0.9
* @copyright (C) 2004-2006 Lee Cher Yeong
* @url http://www.Mosets.com/
* @author Lee Cher Yeong <hotproperty@mosets.com>**/

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

# Include the config file
global $mosConfig_absolute_path;
require( $mosConfig_absolute_path.'/administrator/components/com_hotproperty/config.hotproperty.php' );

define("_HP_SEF_PROPERTY", "property");
define("_HP_SEF_COMPANY", "company");
define("_HP_SEF_EMAIL", "email");
define("_HP_SEF_FEATURED", "featured");
define("_HP_SEF_SENDENQUIRY", "sendemail");
define("_HP_SEF_SEARCH", "search");
define("_HP_SEF_ADVSEARCH", "advsearch");
define("_HP_SEF_SEARCHRESULT", "advsearchr");

define("_HP_SEF_VIEWAGENT", "viewagent");

define("_HP_SEF_MANAGEPROP", "manage");
define("_HP_SEF_EDITAGENT", "editagent");
define("_HP_SEF_ADDPROPERTY", "addproperty");
define("_HP_SEF_EDITPROPERTY", "editproperty");

class sef_hotproperty {

	/********************************************************
	* CREATE
	********************************************************/

	/**
	* Creates the SEF advance URL out of the Mambo request
	* Input: $string, string, The request URL (index.php?option=com_example&Itemid=$Itemid)
	* Output: $sefstring, string, SEF advance URL ($var1/$var2/)
	**/
	function create ($string) {
		global $database;
		$sefstring = "";

		# View Property
		if (eregi("&amp;task=view&amp;id=",$string)) {

			$temp = split("&amp;id=", $string);
			$temp = split("&", $temp[1]);
			$property_id = $temp[0];

			if ( is_numeric($property_id) ) {
				$database->setQuery( "SELECT t.name AS type_name, p.name AS property_name FROM #__hp_properties AS p" 
					.	"\nLEFT JOIN #__hp_prop_types AS t ON p.type = t.id"
					.	"\nWHERE p.id = '".$property_id."'"
					.	"\nLIMIT 1"
				);
				$database->loadObject( $row );

				$sefstring .= _HP_SEF_PROPERTY."/".sefencode($row->type_name)."/".sefencode($row->property_name)."/";
			}
		}

		# View Type
		if (eregi("&amp;task=viewtype&amp;id=",$string)) {

			$temp = split("&amp;id=", $string);
			$temp = split("&", $temp[1]);
			$type_id = $temp[0];

			if ( is_numeric($type_id) ) {
				$database->setQuery( "SELECT t.name AS type_name FROM #__hp_prop_types AS t" 
					.	"\nWHERE t.id = '".$type_id."'"
					.	"\nLIMIT 1"
				);
				$database->loadObject( $row );

				$sefstring .= _HP_SEF_PROPERTY."/".sefencode($row->type_name)."/";

				// --- Sort & Order
				$temp = split("&amp;sort=", $string);
				if (!empty($temp[1])) {
					$temp = split("&", $temp[1]);
					$sort = $temp[0];
				} else {
					//$sort='';
					global $hp_default_order;
					$sort = $hp_default_order;
				}

				$temp = split("&amp;order=", $string);
				if (!empty($temp[1])) {
					$temp = split("&", $temp[1]);
					$order = $temp[0];
				} else {
					//$order='';
					global $hp_default_order2;
					$order = $hp_default_order2;
				}

				if ( $sort == '' ) {
//					global $hp_default_order;
//					$sort = $hp_default_order;
				}

				if ( $order == '' ) {
//					global $hp_default_order2;
//					$order = $hp_default_order2;
				}

				// limit & limitstart
				$temp = split("&amp;limit=", $string);
				if(!empty($temp[1])) {
					$temp = split("&", $temp[1]);
					$limit = $temp[0];
				} else {
					global $hp_default_limit;
					$limit = $hp_default_limit;
				}

				$temp = split("&amp;limitstart=", $string);
				if(!empty($temp[1])) {
					$temp = split("&", $temp[1]);
					$limitstart = $temp[0];
				} else {
					$limitstart = 0;
				}

				if ( $limit == '' ) {
//					global $hp_default_limit;
//					$limit = $hp_default_limit;
				}
				if ( $limitstart == '' ) {
//					$limitstart = 0;
				}

				global $hp_default_order,$hp_default_order2,$hp_default_limit;

				if ( $sort <> '' && $order <> '' && !( $sort == $hp_default_order && $order == $hp_default_order2 && $limitstart == 0 && $limit == $hp_default_limit) ) {
					$sefstring .= $sort."/".$order."/";
					if ( $limit <> '' && $limitstart <> '' ) {
						$sefstring .= $limit."/".$limitstart."/";
					}
				}

			} // END IF is_numeric

		}

		# View Featured
		if (eregi("&amp;task=viewfeatured",$string)) {

			$sefstring .= _HP_SEF_PROPERTY."/"._HP_SEF_FEATURED."/";

			// limit
			$temp = split("&amp;limit=", $string);
			if( isset($temp[1]) ) {
				$temp = split("&", $temp[1]);
				$limit = $temp[0];
			} else {
				$limit = '';
			}

			// limitstart
			$temp = split("&amp;limitstart=", $string);
			if( isset($temp[1]) ) {
				$temp = split("&", $temp[1]);
				$limitstart = $temp[0];
			} else {
				$limitstart = '';
			}

			if ( $limit <> '' && $limitstart <> '' ) {
				$sefstring .= $limit."/".$limitstart."/";
			}

		}

		# View Agent
		if (eregi("&amp;task=viewagent&amp;id=",$string)) {

			$temp = split("&amp;id=", $string);
			$temp = split("&", $temp[1]);
			$agent_id = $temp[0];

			// This condition allows listagent module to work so that $agent_id can be a javascript expression.
			if ( is_numeric($agent_id) ) {
				$database->setQuery( "SELECT a.name AS agent_name, c.name AS company_name FROM #__hp_agents AS a" 
					.	"\nLEFT JOIN #__hp_companies AS c ON a.company = c.id"
					.	"\nWHERE a.id = '".$agent_id."'"
					.	"\nLIMIT 1"
				);
				$database->loadObject( $row );
			}

			// Default virtual directory would be: _HP_SEF_COMPANY/company_name/agent_name/.
			// There is an alternative way to represent this as to allow List Agents module to work: _HP_SEF_VIEWAGENT/agent_id
			if ( !is_numeric($agent_id) || count($row) <= 0 ) {
				$sefstring .= _HP_SEF_VIEWAGENT."/".$agent_id;
			} else {
				$sefstring .= _HP_SEF_COMPANY."/".sefencode($row->company_name)."/".sefencode($row->agent_name)."/";
			}


			// limit
			$temp = split("&amp;limit=", $string);
			if( isset($temp[1]) ) {
				$temp = split("&", $temp[1]);
				$limit = $temp[0];
			} else {
				$limit='';
			}

			// limitstart
			$temp = split("&amp;limitstart=", $string);
			if( isset($temp[1]) ) {
				$temp = split("&", $temp[1]);
				$limitstart = $temp[0];
			} else {
				$limitstart='';
			}

			if ( $limit <> '' && $limitstart <> '' ) {
				$sefstring .= $limit."/".$limitstart."/";
			}

		}

		# View Company / View Company Email / Send Company Email
		if ( eregi("&amp;task=viewco&amp;id=",$string) || eregi("&amp;task=viewcoemail&amp;id=",$string) || eregi("&amp;task=sendenquiry&amp;id=",$string) ) {

			$temp = split("&amp;id=", $string);
			$temp = split("&", $temp[1]);
			$company_id = $temp[0];

			// This condition allows listagent module to work so that $agent_id can be a javascript expression.
			if ( is_numeric($company_id) ) {
				$database->setQuery( "SELECT c.name AS company_name FROM #__hp_companies AS c" 
					.	"\nWHERE c.id = '".$company_id."'"
					.	"\nLIMIT 1"
				);
				$database->loadObject( $row );
			}
			
			# View Company
			if ( eregi("&amp;task=viewco&amp;id=",$string) ) {

				// Default virtual directory would be: _HP_SEF_COMPANY/company_name/.
				// There is an alternative way to represent this as to allow List Companies module to work: _HP_SEF_COMPANY/company_id
				if ( !is_numeric($company_id) || count($row) <= 0 ) {
					$sefstring .= _HP_SEF_COMPANY."/".$company_id;
				} else {
					$sefstring .= _HP_SEF_COMPANY."/".sefencode($row->company_name)."/";
				}

				// limit
				$temp = split("&amp;limit=", $string);
				if( isset($temp[1]) ) {
					$temp = split("&", $temp[1]);
					$limit = $temp[0];
				} else {
					$limit='';
				}

				// limitstart
				$temp = split("&amp;limitstart=", $string);
				if( isset($temp[1]) ) {
					$temp = split("&", $temp[1]);
					$limitstart = $temp[0];
				} else {
					$limitstart = '';
				}

				if ( $limit <> '' && $limitstart <> '' ) {
					$sefstring .= $limit."/".$limitstart."/";
				}

			# View Company Email
			} elseif ( eregi("&amp;task=viewcoemail&amp;id=",$string) ) {
				$sefstring .= _HP_SEF_EMAIL."/".sefencode($row->company_name)."/";

			# Send Enquiry Email
			} elseif (eregi("&amp;task=sendenquiry&amp;id=",$string) ) {
				$sefstring .= _HP_SEF_SENDENQUIRY."/".$company_id."/"; // Sendenquiry does not use this ID to determine whether this is directed to company or agent, the form has a hidden field called 'sbj' to distinguish this.
			}

		}

		# View Agent Email
		if ( eregi("&amp;task=viewagentemail&amp;id=",$string) ) {

			$temp = split("&amp;id=", $string);
			$temp = split("&", $temp[1]);
			$agent_id = $temp[0];

			if ( is_numeric($agent_id) ) {
				$database->setQuery( "SELECT a.name AS agent_name, c.name AS company_name FROM #__hp_agents AS a" 
					.	"\nLEFT JOIN #__hp_companies AS c ON a.company = c.id"
					.	"\nWHERE a.id = '".$agent_id."'"
					.	"\nLIMIT 1"
				);
				$database->loadObject( $row );
				
				$sefstring .= _HP_SEF_EMAIL."/".sefencode($row->company_name)."/".sefencode($row->agent_name)."/";
			}
		}

		# Standard Search
		if (eregi("&amp;task=search",$string)) {

			global $mosConfig_absolute_path, $sufix;
			require( $mosConfig_absolute_path.'/administrator/components/com_hotproperty/config.hotproperty.php' );

			// search string
			$temp = split("&amp;search=", $string);
			$temp = split("&", $temp[1]);
			// This condition prevents url encoding to be performed on javascript expression in standard search form.
			if ( strpos(trim($temp[0]), "encodeURI(document.searchfrm.search.value)") >= 0 || 
						strpos(trim($temp[0]), "encodeURI(document.searchfrm_mod.search.value)") >= 0 ) {
				$search	= $temp[0];
			} else {
				$search	= urlencode($temp[0]);
			}

			// type
			$temp = split("&amp;type=", $string);
			$temp = split("&", $temp[1]);
			$type	= $temp[0];

			// limit
			$temp = split("&amp;limit=", $string);
			$temp = split("&", $temp[1]);
			$limit = $temp[0];
			if ( $limit == '' ) {
				$limit = $hp_default_limit_search;
			}

			// limitstart
			$temp = split("&amp;limitstart=", $string);
			$temp = split("&", $temp[1]);
			$limitstart = $temp[0];
			if ( $limitstart == '' ) {
				$limitstart = 0;
			}

			$sefstring .= _HP_SEF_SEARCH."/".$type."/".$limit."/".$limitstart."/".$search;

			if ( !empty($sufix) ) {
				$sefstring .= "/";
			}

		}

		# Advanced Search
		if (eregi("&amp;task=advsearch",$string)) {

			$sefstring .= _HP_SEF_ADVSEARCH."/";

		}

		# Advanced Search Result
		if (eregi("&amp;task=asearch",$string)) {

			$sefstring .= _HP_SEF_SEARCHRESULT."/";

			// search_id
			$temp = split("&amp;search_id=", $string);
			if ( isset($temp[1]) ) {
				$temp = split("&", $temp[1]);
				$search_id = $temp[0];
			} else {
				$search_id = '';
			}

			// limit
			$temp = split("&amp;limit=", $string);
			if ( isset($temp[1]) ) {
				$temp = split("&", $temp[1]);
				$limit = $temp[0];
			} else {
				$limit = '';
			}

			// limitstart
			$temp = split("&amp;limitstart=", $string);
			if ( isset($temp[1]) ) {
				$temp = split("&", $temp[1]);
				$limitstart = $temp[0];
			} else {
				$limitstart = '';
			}

			if ( $search_id <> '' ) {
				$sefstring .= $search_id ."/";

				if ( $limit <> '' && $limitstart <> '' ) {
					$sefstring .= $limit ."/". $limitstart ."/";
				} // End if
			} // End if

		}

		# Manage Property
		if (eregi("&amp;task=manageprop",$string)) {

			$sefstring .= _HP_SEF_MANAGEPROP."/";

			// limit
			$temp = split("&amp;limit=", $string);
			$temp = split("&", $temp[1]);
			$limit = $temp[0];

			// limitstart
			$temp = split("&amp;limitstart=", $string);
			$temp = split("&", $temp[1]);
			$limitstart = $temp[0];

			if ( $limit <> '' && $limitstart <> '' ) {
				$sefstring .= $limit ."/". $limitstart ."/";
			}

		}

		# Edit Agent
		if (eregi("&amp;task=editagent",$string)) {

			$sefstring .= _HP_SEF_EDITAGENT."/";

		}

		# Add Property
		if (eregi("&amp;task=addprop",$string)) {

			$sefstring .= _HP_SEF_ADDPROPERTY."/";

		}

		# Edit Property
		if (eregi("&amp;task=editprop",$string)) {

			$temp = split("&amp;id=", $string);
			$temp = split("&", $temp[1]);
			$id = $temp[0];

			// Using IDs instead of Property name to aviod conflicts with same property name.
			$sefstring .= _HP_SEF_EDITPROPERTY."/".$id."/";

		}

		return $sefstring;
	}

	/********************************************************
	* REVERT
	********************************************************/

	/**
	* Reverts to the Mambo query string out of the SEF advance URL
	* Input:
	*    $url_array, array, The SEF advance URL split in arrays (first custom virtual directory beginning at $pos+1)
	*    $pos, int, The position of the first virtual directory (component)
	* Output: $QUERY_STRING, string, Mambo query string (var1=$var1&var2=$var2)
	*    Note that this will be added to already defined first part (option=com_example&Itemid=$Itemid)
	**/
	function revert ($url_array, $pos) {
		global $database;

		// define all variables you pass as globals
//		global $var1, $var2;
 		// Examine the SEF advance URL and extract the variables building the query string
		$QUERY_STRING = "";

		if ( isset($url_array[$pos+2]) ) {
			switch($url_array[$pos+2]) {
				
				# Types & Properties
				case _HP_SEF_PROPERTY:

					# View Property
					if (isset($url_array[$pos+4]) && $url_array[$pos+4]!="" && $url_array[$pos+5]=="") {
						$type_name = sefdecode($url_array[$pos+3]);
						$property_name = sefdecode($url_array[$pos+4]);
						
						$database->setQuery( "SELECT p.id FROM #__hp_properties AS p "
							.	"\nLEFT JOIN #__hp_prop_types AS t ON t.id = p.type"
							.	"\nWHERE p.name ='".$property_name."' AND t.name ='".$type_name."' LIMIT 1" 
							);
						$property_id = $database->loadResult();
						
						$_GET['task'] = "view";
						$_REQUEST['task'] = "view";
						$_GET['id'] = $property_id;
						$_REQUEST['id'] = $property_id;
						$QUERY_STRING .= "&task=view&id=$property_id";

					} elseif ( $url_array[$pos+3] == _HP_SEF_FEATURED ) {
					# View Featured
					
						$_GET['task'] = "viewfeatured";
						$_REQUEST['task'] = "viewfeatured";
						$QUERY_STRING .= "&task=viewfeatured";

						$limit = $url_array[$pos+4];
						if ( isset($url_array[$pos+5]) ) {
							$limitstart = $url_array[$pos+5];
						} else {
							$limitstart = 0;
						}

						// Limit & Limit Start
						if ( $limit <> '' && $limitstart <> '' ) {

							$_GET['limit'] = $limit;
							$_REQUEST['limit'] = $limit;
							$_GET['limitstart'] = $limitstart;
							$_REQUEST['limitstart'] = $limitstart;

							$QUERY_STRING .= "&limit=$limit&limitstart=$limitstart";
						} // End if

					} else {
					# View Type

						$type_name = sefdecode($url_array[$pos+3]);
						
						$database->setQuery( "SELECT t.id FROM #__hp_prop_types AS t WHERE t.name ='".$type_name."' LIMIT 1" );
						$type_id = $database->loadResult();
						
						$_GET['task'] = "viewtype";
						$_REQUEST['task'] = "viewtype";
						$_GET['id'] = $type_id;
						$_REQUEST['id'] = $type_id;
						$QUERY_STRING .= "&task=viewtype&id=$type_id";

						if( isset($url_array[$pos+4]) ) $sort = $url_array[$pos+4];
						if( isset($url_array[$pos+5]) ) $order = $url_array[$pos+5];
						if( isset($url_array[$pos+6]) ) $limit = $url_array[$pos+6];
						if( isset($url_array[$pos+7]) ) $limitstart = $url_array[$pos+7];

						// Sort & Order
						if ( $sort <> '' && $order <> '' ) {

							$_GET['sort'] = $sort;
							$_REQUEST['sort'] = $sort;
							$_GET['order'] = $order;
							$_REQUEST['order'] = $order;
							$QUERY_STRING .= "&sort=$sort&order=$order";
							
							// Limit & Limit Start
							if ( $limit <> '' && $limitstart <> '' ) {

								$_GET['limit'] = $limit;
								$_REQUEST['limit'] = $limit;
								$_GET['limitstart'] = $limitstart;
								$_REQUEST['limitstart'] = $limitstart;

								$QUERY_STRING .= "&limit=$limit&limitstart=$limitstart";
							} // End if

						} // End if

					}
					break;

				# Companies & Agents
				
				// _HP_SEF_COMPANY/company_name/agent_name/

				case _HP_SEF_COMPANY:

					# View Agent
					// _HP_SEF_COMPANY/company_name/agent_name/

					if (isset($url_array[$pos+2]) && isset($url_array[$pos+3]) && isset($url_array[$pos+4]) && $url_array[$pos+2]!="" && (
							($url_array[$pos+3] == '' && $url_array[$pos+4] == '') || ($url_array[$pos+3] <> '' && $url_array[$pos+4] <> '')
						) ) {
						$agent_name = sefdecode($url_array[$pos+4]);
						$company_name = sefdecode($url_array[$pos+3]);
						
						$database->setQuery( "SELECT a.id FROM (#__hp_agents AS a, #__hp_companies AS c) "
							.	"\n WHERE c.id=a.company AND a.name ='".$agent_name."' AND c.name = '".$company_name."' LIMIT 1" );
						$agent_id = $database->loadResult();
						
						$_GET['task'] = "viewagent";
						$_REQUEST['task'] = "viewagent";
						$_GET['id'] = $agent_id;
						$_REQUEST['id'] = $agent_id;
						$QUERY_STRING .= "&task=viewagent&id=$agent_id";

						// Limit & Limit Start
						$limit = $url_array[$pos+5];
						$limitstart = $url_array[$pos+6];

						if ( $limit <> '' && $limitstart <> '' ) {

							$_GET['limit'] = $limit;
							$_REQUEST['limit'] = $limit;
							$_GET['limitstart'] = $limitstart;
							$_REQUEST['limitstart'] = $limitstart;

							$QUERY_STRING .= "&limit=$limit&limitstart=$limitstart";
						}

					} else {
					# View Company
						// _HP_SEF_COMPANY/company_name/
						// _HP_SEF_COMPANY/company_id
						$company_name = sefdecode($url_array[$pos+3]);
						
						$database->setQuery( "SELECT c.id FROM #__hp_companies AS c WHERE c.name ='".$company_name."' LIMIT 1" );
						$company_id = $database->loadResult();
						
						if ( $company_id == '' && is_numeric($company_name) )  {
							$company_id = $company_name;
						}

						$_GET['task'] = "viewco";
						$_REQUEST['task'] = "viewco";
						$_GET['id'] = $company_id;
						$_REQUEST['id'] = $company_id;
						$QUERY_STRING .= "&task=viewco&id=$company_id";

						// Limit & Limit Start
						if(isset($url_array[$pos+4])) $limit = $url_array[$pos+4];
						if(isset($url_array[$pos+5])) $limitstart = $url_array[$pos+5];

						if ( $limit <> '' && $limitstart <> '' ) {

							$_GET['limit'] = $limit;
							$_REQUEST['limit'] = $limit;
							$_GET['limitstart'] = $limitstart;
							$_REQUEST['limitstart'] = $limitstart;

							$QUERY_STRING .= "&limit=$limit&limitstart=$limitstart";
						}

					}
					break;
				# View Agent
				// _HP_SEF_VIEWAGENT/agent_id
				case _HP_SEF_VIEWAGENT:
					$agent_id = sefdecode($url_array[$pos+3]);
					
					$_GET['task'] = "viewagent";
					$_REQUEST['task'] = "viewagent";
					$_GET['id'] = $agent_id;
					$_REQUEST['id'] = $agent_id;
					$QUERY_STRING .= "&task=viewagent&id=$agent_id";

					break;

				case _HP_SEF_EMAIL:

					# View Company Email
					if ( $url_array[$pos+4] == '' ) {

						$company_name = sefdecode($url_array[$pos+3]);
						
						$database->setQuery( "SELECT c.id FROM #__hp_companies AS c WHERE c.name ='".$company_name."' LIMIT 1" );
						$company_id = $database->loadResult();
						
						$_GET['id'] = $company_id;
						$_REQUEST['id'] = $company_id;
						$_GET['task'] = "viewcoemail";
						$_REQUEST['task'] = "viewcoemail";
						$QUERY_STRING .= "&task=viewcoemail&id=$company_id";

					# View Agent Email
					} else {
						$company_name = sefdecode($url_array[$pos+3]);
						$agent_name = sefdecode($url_array[$pos+4]);

						$database->setQuery( "SELECT a.id FROM #__hp_agents AS a"
							.	"\nLEFT JOIN #__hp_companies AS c ON c.id = a.company"
							.	"\nWHERE c.name ='".$company_name."'"
								.	"\nAND a.name ='".$agent_name."'"
							.	"\nLIMIT 1" 
							);
						$agent_id = $database->loadResult();

						$_GET['id'] = $agent_id;
						$_REQUEST['id'] = $agent_id;
						$_GET['task'] = "viewagentemail";
						$_REQUEST['task'] = "viewagentemail";
						$QUERY_STRING .= "&task=viewagentemail&id=$agent_id";
					}

					break;

				case _HP_SEF_SENDENQUIRY:

					# Send Enquiry Email

					$company_id = sefdecode($url_array[$pos+3]);
					
					$_GET['id'] = $company_id;
					$_REQUEST['id'] = $company_id;
					
					$_GET['task'] = "sendenquiry";
					$_REQUEST['task'] = "sendenquiry";
					$QUERY_STRING .= "&task=sendenquiry&id=$company_id";

					break;

				case _HP_SEF_SEARCH:
					# Standard Search

					$_GET['task'] = "search";
					$_REQUEST['task'] = "search";
					$_GET['type'] = $url_array[$pos+3];
					$_REQUEST['type'] = $url_array[$pos+3];
					$_GET['limit'] = $url_array[$pos+4];
					$_REQUEST['limit'] = $url_array[$pos+4];
					$_GET['limitstart'] = $url_array[$pos+5];
					$_REQUEST['limitstart'] = $url_array[$pos+5];
					$_GET['search'] = urldecode($url_array[$pos+6]);
					$_REQUEST['search'] = urldecode($url_array[$pos+6]);

					$QUERY_STRING .= "&task=search&type=".$url_array[$pos+3]."&limit=".$url_array[$pos+4]."&limitstart=".$url_array[$pos+5]."&search=".urldecode($url_array[$pos+6]);

					break;

				case _HP_SEF_ADVSEARCH:
					# Advanced Search

					$_GET['task'] = "advsearch";
					$_REQUEST['task'] = "advsearch";
					$QUERY_STRING .= "&task=advsearch";
					break;
				
				case _HP_SEF_SEARCHRESULT:
					# Advanced Search Result

					$_GET['task'] = "asearch";
					$_REQUEST['task'] = "asearch";
					$QUERY_STRING .= "&task=asearch";

					// search_id
					$search_id = $url_array[$pos+3];

					// Limit & Limit Start
					if( isset($url_array[$pos+4]) ) $limit = $url_array[$pos+4];
					if( isset($url_array[$pos+5]) ) $limitstart = $url_array[$pos+5];

					if ( $search_id <> '' ) {
						$_GET['search_id'] = $search_id;
						$_REQUEST['search_id'] = $search_id;

						$QUERY_STRING .= "&search_id=$search_id";

						if ( $limit <> '' && $limitstart <> '' ) {

							$_GET['limit'] = $limit;
							$_REQUEST['limit'] = $limit;
							$_GET['limitstart'] = $limitstart;
							$_REQUEST['limitstart'] = $limitstart;

							$QUERY_STRING .= "&limit=$limit&limitstart=$limitstart";
						}

					}
					break;
				
				case _HP_SEF_MANAGEPROP:
					# Manage Property
					
					$_GET['task'] = "manageprop";
					$_REQUEST['task'] = "manageprop";
					$QUERY_STRING .= "&task=manageprop";

					// Limit & Limit Start
					$limit = $url_array[$pos+3];
					$limitstart = $url_array[$pos+4];

					if ( $limit <> '' && $limitstart <> '' ) {

						$_GET['limit'] = $limit;
						$_REQUEST['limit'] = $limit;
						$_GET['limitstart'] = $limitstart;
						$_REQUEST['limitstart'] = $limitstart;

						$QUERY_STRING .= "&limit=$limit&limitstart=$limitstart";
					}

					break;

				case _HP_SEF_EDITAGENT:
					# Edit Agent
					$_GET['task'] = "editagent";
					$_REQUEST['task'] = "editagent";
					$QUERY_STRING .= "&task=editagent";
					break;

				case _HP_SEF_ADDPROPERTY:
					# Add Property
					$_GET['task'] = "addprop";
					$_REQUEST['task'] = "addprop";
					$QUERY_STRING .= "&task=addprop";
					break;

				case _HP_SEF_EDITPROPERTY:
					# Edit Property

					$id = sefdecode($url_array[$pos+3]);
					
					$_GET['id'] = $id;
					$_REQUEST['id'] = $id;
					$_GET['task'] = "editprop";
					$_REQUEST['task'] = "editprop";
					$QUERY_STRING .= "&task=editprop&id=$id";
					break;
			}
		}

		return $QUERY_STRING;
	}

}
?>