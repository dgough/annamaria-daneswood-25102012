<?php
/**
* @author    organic-development.com
* @copyright Copyright (C) 2008 Organic Development Ltd. All rights reserved.
* @license	 GNU/GPL
*/

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );

global $mainframe, $mosConfig_absolute_path, $mosConfig_frontend_login, $task, $option, $id;

/*
	if the calendar class exists, then output it - but only if on a property page
*/
if (file_exists($mosConfig_absolute_path.'/administrator/components/com_virtuemart/classes/ps_calendar.php')) {

	if ($option=='com_hotproperty' && $task == 'view') {
	
		echo '<h3>Check Availability</h3>';
		
		echo "<script type='text/javascript' src='$mosConfig_live_site/components/com_virtuemart/js/calendar/js/dhtmlSuite-common.js'></script>";
		/*echo '<script> DHTMLSuite.include("calendar") </script>';*/
		echo "<script type='text/javascript' src='$mosConfig_live_site/components/com_virtuemart/js/calendar/js/dhtmlSuite-calendar.js-uncompressed.js'></script>";
		echo "<link rel='stylesheet' type='text/css' href='$mosConfig_live_site/components/com_virtuemart/js/calendar/css/calendar.css' />";

		require_once($mosConfig_absolute_path.'/administrator/components/com_virtuemart/classes/ps_database.php');
		require_once($mosConfig_absolute_path.'/administrator/components/com_virtuemart/classes/ps_calendar.php');
		
		echo '<div id="avlcal">&nbsp; </div>';
	
		$avlcal = new ps_calendar();
		
		$avlcal->getCalendar($id,2);
	}	
}

?>