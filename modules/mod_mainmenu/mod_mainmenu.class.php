<?php


class mainMenu {

	var $menu_root;
	var $menu_items;
	var $menu_type;
	var $menu_class;
	var $start_level;
	var $end_level;
	var $accordion_level;
	var $show_all_children;
	var $params;

	function mainMenu(&$params) {
		$this->menu_root = null;
		$this->menu_items = array();
		$this->menu_type = $params->get('menutype');
		$this->menu_class = 'menu' . $params->get('class_sfx');
		$this->start_level = $params->get('startLevel');
		$this->end_level = $params->get('endLevel');
		$this->accordion = $params->get('accordion');
		$this->show_all_children = $params->get('showAllChildren');
		$this->params = $params;
		$this->loadMenu();
	}

	function loadMenu() {
		global $database, $my, $cur_template, $Itemid;
		global $mosConfig_absolute_path, $mosConfig_shownoauth;

		$and = '';
		if (!$mosConfig_shownoauth) {
			$and = "\n AND access <= $my->gid";
		}

		$sql = "SELECT m.* FROM #__menu AS m"
		. "\n WHERE menutype = '" . $this->menu_type . "'"
		. "\n AND published = 1"
		. $and
		. "\n ORDER BY parent, ordering";

		$database->setQuery($sql);
		$rows = $database->loadObjectList('id');

		$this->menu_root = &new mainMenuItem();
		$this->menu_root->setName("ROOT");
		$this->menu_root->setActiveWithParentItems(true);		
		$this->addMenuItem($this->menu_root);

		$this->loadMenuLevelItems($this->menu_root, $rows, 0);
	}

	function loadMenuLevelItems(&$parent, $menu_items, $sublevel) {
		global $Itemid;
		
		foreach($menu_items as $menu_item) {
			if($menu_item->parent == $parent->getId()) {
				// create new menu item
				$new_item = &new mainMenuItem($menu_item, $this->params);
				$new_item->setParent($parent);
				$new_item->setSublevel($sublevel);
				$parent->addChild($new_item);
				// set active menu items with its parent items
				if(isset($Itemid) && $new_item->getId() == $Itemid) {
					$new_item->setCurrent(true);
					$new_item->setActiveWithParentItems(true);
				}
				// add new item to menu item list
				$this->addMenuItem($new_item);
				// populate next menu level items
				$this->loadMenuLevelItems($new_item, $menu_items, ($sublevel+1));
			}
		}
	}

	function addMenuItem(&$menu_item) {
		$this->menu_items[$menu_item->id] = &$menu_item;
	}

	function getMenuLevel(&$parent, $levels) {
		$str = "";
		if($levels != 0 && $parent->hasChildren()) {

			// set ul class
			$child_html_level = $parent->getHtmlLevel() + 1;
			$ul_class = ($child_html_level == 1) ? $this->menu_class : "level" . $child_html_level;

			// add accordion style class
			$ul_class .= ($this->accordion && $child_html_level == 2 && $parent->isSeparator()) ? " accordion" : "";
			
			$str .= "<ul class=\"" . $ul_class . "\">\n";				
			foreach($parent->getChildren() as $child) {

				// add accordion toggler
				($this->accordion && $child->getHtmlLevel() == 1 && $child->hasChildren() && $child->isSeparator()) ? $li_class = " toggler" : $li_class = "";
				
				$str .= "<li class=\"" . $child->getHtmlClass() . $li_class . "\">";				
				$str .= $child->getHtmlLink();
				if($this->show_all_children || (!$this->show_all_children && $child->active) ||
				  // show sub items if current child is accordion toggler
				  ($this->accordion && $child->getHtmlLevel() == 1 && $child->isSeparator())) {
					$str .= $this->getMenuLevel($child, ($levels-1));
				}
				
				$str .= "</li>\n";
			}

			$str .= "</ul>\n";
		}
		return $str;
	}

	function getMenu($start_level = 0, $end_level = 0) {
		$levels = $end_level - $start_level;
		
		if($end_level == 0 || $levels < 0) {
			$levels = -1;
		}
		if($start_level >= 0) {
			foreach($this->menu_items as $menu_item) {
				if($menu_item->isActive() && $menu_item->getSublevel() == $start_level - 1) {
					return $this->getMenuLevel($menu_item, $levels);
				}
			}
		}

		return "";
	}

	function showMenu() {
		return $this->getMenu($this->start_level, $this->end_level);
	}

}

class mainMenuItem {

	var $id;
	var $name;
	var $link;
	var $type;
	var $browser_nav;
	var $sublevel;
	var $ordering;
	var $params;	
	var $active;
	var $current;
	var $parent;
	var $children;
	var $menu_start_level;

	function mainMenuItem($row = null, $menu_params = null) {
		$this->id = 0;
		$this->name = "";
		$this->link = "";
		$this->type = "";
		$this->browser_nav = 0;
		$this->sublevel = -1;
		$this->ordering = 0;
		$this->params = null;
		$this->active = false;
		$this->current = false;
		$this->parent = null;
		$this->children = array();
		$this->menu_start_level = 0;

		if($row != null) {
			$this->id = $row->id;
			$this->name = $row->name;
			$this->link = $row->link;
			$this->type = $row->type;
			$this->browser_nav = $row->browserNav;
			$this->ordering = $row->ordering;
			$this->params = new mosParameters($row->params);
		}

		if($menu_params != null) {
			$this->menu_start_level = $menu_params->get('startLevel');
		}
	}

	function getId() {
		return $this->id;
	}

	function getName() {
		return $this->name;
	}

	function getSublevel() {
		return $this->sublevel;
	}

	function isActive() {
		return $this->active;
	}

	function isCurrent() {
		return $this->current;
	}

	function isSeparator() {
		return $this->browser_nav == 3;
	}

	function &getParent() {
		return $this->parent;
	}

	function &getChild($i) {
		if(array_key_exists($i, $this->children))
			return $this->children[$i];
		return null;
	}

	function &getChildren() {
		return $this->children;
	}

	function getChildrenCount() {
		return count($this->children);
	}

	function hasChildren() {
		return count($this->children) > 0;
	}

	function getMenuImage() {
		return $this->params->get('menu_image', false);
	}

	function hasMenuImage() {
		return $this->getMenuImage() != false && $this->getMenuImage() != -1;
	}

	function getOrdering() {
		return $this->ordering;
	}

	function setId($val) {
		$this->id = $val;
	}

	function setName($val) {
		$this->name = $val;
	}

	function setSublevel($val) {
		$this->sublevel = $val;
	}

	function setActiveWithParentItems($val) {
		$this->active = $val;
		if($this->parent != null) {
			$this->parent->setActiveWithParentItems($val);
		}
	}

	function setCurrent($val) {
		$this->current = $val;
	}

	function setParent(&$val) {
		$this->parent = &$val;
	}

	function addChild(&$menu_item) {
		$this->children[] = &$menu_item;
	}

	function getUrl() {
		global $whitelabel;
	
		if($whitelabel && $whitelabel->home_menu == $this->id)
		{
			return '/';
		}
	
		$link = $this->link;

		switch ($this->type) {
			case 'component_item_link':
				global $database;
				$menu = new mosMenu($database);
				$menu->load(str_replace('?Itemid=','', $link));
				$mitem = new mainMenuItem($menu);
				$link = $mitem->getUrl();
				return $link;
				break;
			case 'separator':
				break;

			case 'url':
				if (eregi('index.php\?', $link)) {
					if (!eregi('Itemid=', $link)) {
						$link .= '&Itemid='. $this->id;
					}
				}
				break;

			default:
				$link .= '&Itemid='. $this->id;
				break;
		}

		return sefRelToAbs(ampReplace($link));
	}

	function getHtmlLevel() {
		return $this->sublevel - $this->menu_start_level + 1;
	}

	function getHtmlClass() {
		$first = $this->parent->getChild(0);
		$last  = $this->parent->getChild($this->parent->getChildrenCount() - 1);
		
		$html_class = "level" . $this->getHtmlLevel() . " num" . $this->ordering.' item'.$this->id;

		if($first->getId() == $this->getId())
			$html_class .= " first";

		if($last->getId() == $this->getId())
			$html_class .= " last";

		if($this->browser_nav == 3)
			$html_class .= " separator";

		if($this->hasChildren())
			$html_class .= " parent";

		if($this->active)
			$html_class .= " active";

		if($this->current)
			$html_class .= " current";

		return $html_class;
	}

	function getHtmlLink() {
		global $mosConfig_live_site;
		
		$url = $this->getUrl();
		$span_style = $this->hasMenuImage() ? ' style="background-image: url(' . $mosConfig_live_site . '/images/stories/' . $this->getMenuImage() . ');" ' : '';
		$name = '<span' . $span_style . '>' . stripslashes(ampReplace($this->name)) . '</span>';
		$html_class = $this->getHtmlClass();
		
		if($html_class != '') {
			$html_class = 'class="' . $html_class . '" ';
		}

		switch ($this->browser_nav) {
			case 1:
				$html_link = '<a ' . $html_class . 'href="'. $url .'" target="_blank">'. $name .'</a>';
				break;

			case 2:
				$html_link = "<a ' . $html_class . 'href=\"#\" onclick=\"javascript: window.open('". $url ."', '', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=780,height=550'); return false\">". $name ."</a>";
				break;

			case 3:
				$html_link = '<span ' . $html_class . '>'. $name .'</span>';
				break;

			default:
				$html_link = '<a ' . $html_class . 'href="'. $url .'">'. $name .'</a>';
				break;
		}

		return $html_link;
	}

}

?>