<?php
/* - New 2008 Google Analytics || www.MooreCreative.com - */

defined( '_VALID_MOS' ) or die( 'Restricted access' );

$state = $params->get( 'state', 0 );
$ucode = $params->get( 'ucode', 0 );
$https = $params->get( 'https', 0 );
$output 	= '';
$secureurl = 0;

//Updated to new GoogleAnalytics Code Version 2008:
$output = "<script type=\"text/javascript\">\n";
$output .= "var gaJsHost = ((\"https:\" == document.location.protocol) ? \"https://ssl.\" : \"http://www.\");\n";
$output .= "document.write(unescape(\"%3Cscript src='\" + gaJsHost + \"google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E\"));\n";
$output .= "</script>\n";
$output .= "<script type=\"text/javascript\">\n";
$output .= "var pageTracker = _gat._getTracker(\"" . $ucode . "\");\n";
$output .= "pageTracker._initData();\n";
$output .= "pageTracker._trackPageview();\n";
$output .= "</script>\n";

echo $output;
?>