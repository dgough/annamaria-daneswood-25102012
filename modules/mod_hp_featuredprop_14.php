<?php
/**
* Hot Property Featured Property Module with Pictures - Organic Development Version
* No Tables and more module parameters for changing options
* 
* @package Hot Property 0.9
* @copyright (C) 2008 Organic Development
* @url http://www.organic-development.com/
* @author Organic Development <grow@organic-development.com>
**/

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );
global $mainframe;
# Include the config file
require( $mosConfig_absolute_path.'/administrator/components/com_hotproperty/config.hotproperty.php' );

# Include the language file. Default is English
if ($hp_language=='') {
	$hp_language='english';
}
include_once('components/com_hotproperty/language/'.$hp_language.'.php');

# Get params
$count = intval( $params->get( 'count', 4 ) );
$direction = $params->get( 'direction', 'vertical' );

# Get total number of published featured items
$database->setQuery( "SELECT COUNT(*) AS total FROM #__hp_properties AS p"
	. "\nLEFT JOIN #__hp_prop_types AS t ON t.id=p.type"
	.	"\nWHERE p.published=1 AND p.approved=1 AND t.published=1"
	. "\n AND p.featured=1"
	. "\n	AND (publish_up = '0000-00-00 00:00:00' OR publish_up <= NOW())"
	. "\n	AND (publish_down = '0000-00-00 00:00:00' OR publish_down >= NOW())");
$total = $database->loadResult();

# Get Itemid, determine if the HP component is published
$database->setQuery("SELECT id FROM #__menu"
	.	"\nWHERE link='index.php?option=com_hotproperty'"
	.	"\nLIMIT 1");
$Itemid = $database->loadResult();

# Get all featured property
$database->setQuery( "SELECT p.* FROM #__hp_properties AS p"
		. "\nLEFT JOIN #__hp_prop_types AS t ON p.type = t.id"
		. "\nLEFT JOIN #__hp_featured AS f ON f.property = p.id"
		. "\nWHERE p.published='1' AND p.approved='1' AND t.published='1'"
		. "\n AND (publish_up = '0000-00-00 00:00:00' OR publish_up <= NOW())"
		. "\n AND (publish_down = '0000-00-00 00:00:00' OR publish_down >= NOW())"
		. "\nAND p.featured='1'"
		. "\nORDER BY f.ordering ASC"
		. "\nLIMIT 0, ".$count );
$featured = $database->loadObjectList();

# Get 4 newest properties 
//$database->setQuery( "SELECT p.* FROM #__hp_properties AS p LEFT JOIN #__hp_prop_types AS t ON p.type = t.id WHERE p.published='1' AND p.approved='1' ORDER BY p.created ASC LIMIT 4");
//$newprops = $database->loadObjectList();
$template_baseurl = $mosConfig_live_site . '/templates/' . $mainframe->getTemplate();
?>
<?php 
	function neat_trim($str, $n, $delim='...') {
	   $len = strlen($str);
	   if ($len > $n) {
	       preg_match('/(.{' . $n . '}.*?)\b/', $str, $matches);
	       return rtrim($matches[1]) . $delim;
	   }
	   else {
	       return $str;
	   }
	}
?>
<script type="text/javascript" src="<?php echo $template_baseurl . '/lib/js/addons/slideshow.js' ?>"></script>
<div id="fprop-wrapper">
<ul>
	<?php foreach($featured AS $f) { 
		unset($thumb);
		# Get the first thumbnail of the property
		$database->setQuery( "SELECT standard, title FROM #__hp_photos"
			. "\nWHERE property='".$f->id."'"
			. "\nORDER BY ordering ASC"
			. "\nLIMIT 0,1");
		$database->loadObject($thumb);
	
		if ($thumb->standard <> '') {
			$img = $thumb->standard; 
		} else {
			$img = $hp_img_noimage_thumb;
		}
	
	?>
	<li class="fp_prop" id="fprop-<?php echo $f->id ?>">
	    <div class="fp_thumb">
	    	<img src="/includes/phpthumb/phpThumb.php?zc=1&amp;q=100&amp;w=206&amp;h=220&amp;src=<?php echo $hp_imgdir_standard . $img; ?>" alt="<?php echo $thumb->title; ?>" />
		</div>
		<div class="fp_more">
			<h4><?php echo $f->name; ?></h4>					
			<p>
			<?php echo strip_tags(neat_trim($f->intro_text, 125))."<br />"; ?>
			</p>
			<p class="readmore"><a href="<?php echo sefRelToAbs("index.php?option=com_hotproperty&task=view&id=$f->id&Itemid=$Itemid"); ?>">More details...</a></p>
		</div>
				
	</li>
	<?php } ?>
</ul>
</div>
<script type="text/javascript">
	
	jQuery(function($){	
		
		$("#fprop-wrapper").slideshow({
			autoplay 	: true,
			controls 	: true,
			animation 	: "horizontal",
			numeric 	: true,
			pause 		: 9999,
			speed 		: 2000
		});
		
	});
	
</script>
<?php //if ($total > $count) {
	//echo '<a href="'.sefRelToAbs('index.php?option=com_hotproperty&task=viewfeatured&Itemid='.$Itemid).'">'._HP_MOREFEATURED.'</a>';
//} ?>