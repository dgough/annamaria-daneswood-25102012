<?php

/**
* Hot Property Adv. Search Module
*
* @package Hot Property 0.9
* @copyright (C) 2008 Organic Development
* @url http://www.organic-development.com/
* @author Organic Development <grow@organic-development.com>
**/

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

# include HP config file
require( $mosConfig_absolute_path.'/administrator/components/com_hotproperty/config.hotproperty.php' );

# include HP language file. Default is English
if ($hp_language=='') {
	$hp_language='english';
}
include_once('components/com_hotproperty/language/'.$hp_language.'.php');

$count = intval($params->get( 'count', 1 ));

# Select published types

$sql = 	"SELECT p.name, p.suburb, s.discount, t.thumb, t.title from jos_hp_properties p ".
		"LEFT JOIN jos_hp_photos t on t.property=p.id " . 
		"LEFT JOIN jos_vm_bk_seasons s on s.property_id=p.id " .
		"WHERE s.discount!='' ORDER BY p.id DESC LIMIT ".$count;

$database->setQuery($sql);
$specials = $database->loadObjectList();

/*
* Only show this if looking at the search page
*/
?>
<div id="specials_wrap">
<?php
	foreach ($specials as $special) {
		
		if ($special->thumb <> '') {
			$thumb_imgsize = GetImageSize ($mosConfig_absolute_path.$hp_imgdir_thumb.$special->thumb); 
		} else {
			$thumb_imgsize = GetImageSize ($mosConfig_absolute_path.$hp_imgdir_thumb.$hp_img_noimage_thumb); 
		}		
	?>	
    	<div class="fp_thumb">
        	<a href="<?php echo 'index.php?option=com_hotproperty&task=view&id='.$special->id; ?>"><img <?php $thumb_imgsize[3] ?> border="0" src="<?php echo $mosConfig_live_site."/".$hp_imgdir_thumb.((!empty($special->thumb)) ? $special->thumb : $hp_img_noimage_thumb) ?>" alt="<?php $special->title ?>" /></a>
        </div>
        <div class="discount"><?php echo $special->discount ?>% OFF!</div>
        <div class="suburb"><?php echo $special->suburb ?></div>
        <div class="more"><?php echo $special->discount ?>% off early seasons bookings for 2008! <a href="<?php echo 'index.php?option=com_hotproperty&task=view&id='.$special->id; ?>">More details...</a></div>
	<?php
	}
?>
<div id="specials_footer"></div>
</div>