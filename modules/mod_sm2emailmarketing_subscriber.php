<?php
// $Id: mod_sm2emailmarketing_subscriber.php,v 1.7 2007/09/21 03:54:41 sm2tony Exp $
/**
 * sm2emailmarketing - Email Marketing Newsletter
 *
 * Joomla! Newsletter Component that supports multiple list mailing and message queueing
 *
 * @package Joomla!
 * @subpackage com_sm2emailmarketing
 * @copyright Copyright (C) 2006 SM2 / All Rights Reserved
 * @license commercial
 * @author SM2 <info@sm2joomla.com>
 */

/** ensure this file is being included by a parent file */
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/*
 * Setup patTemplate
 */
require_once($mosConfig_absolute_path.'/includes/patTemplate/patTemplate.php');

// include language file, or default to english
if (file_exists ($mosConfig_absolute_path . '/components/com_sm2emailmarketing/languages/'.$mosConfig_lang.'.php')) {
    include_once ($mosConfig_absolute_path . '/components/com_sm2emailmarketing/languages/'.$mosConfig_lang.'.php');
} else {
    include_once ($mosConfig_absolute_path . '/components/com_sm2emailmarketing/languages/english.php');
} // end if

/* @var $tmpl patTemplate */
$tmpl =& patFactory::createTemplate();
$tmpl->setBasedir($mosConfig_absolute_path.'/modules/mod_sm2emailmarketing');
$tmpl->setNameSpace('jos');
$tmpl->readTemplatesFromFile('display.tpl');

//get list of mailing lists available (public access only)
//only if lists plugin is turned on

$sql = 'SELECT enabled FROM #__emktg_plugin WHERE filename = '.$database->Quote('list.plugin.php');
$database->setQuery($sql);
$showlists = $database->loadResult();
if($showlists == 1){
	$lists = array();

	$database->setQuery('SELECT listid AS value, list_name AS text'
        .' FROM #__emktg_list WHERE access<='.(int)$my->gid.' AND published=1'
        .' ORDER BY list_name');
	$lists = $database->loadObjectList();
	$tmpl->addObject('mailing_lists', $lists);
	if (count($lists)) {
		$tmpl->addVar('subscriber', 'display_lists', count($lists));
	}
}
if (empty($my->id)) {
    $tmpl->addVar('subscriber', 'display_details', 1);
}

// import language:
$tmpl->addVar('subscriber', 'NAME', _EMKTG_MODULE_NAME);
$tmpl->addVar('subscriber', 'EMAIL', _EMKTG_MODULE_EMAIL);
$tmpl->addVar('subscriber', 'HTML', _EMKTG_MODULE_HTML);
$tmpl->addVar('subscriber', 'TEXT', _EMKTG_MODULE_TEXT);
$tmpl->addVar('subscriber', 'SUBSCRIBE', _EMKTG_MODULE_SUBSCRIBE);
$tmpl->addVar('subscriber', 'UNSUBSCRIBE', _EMKTG_MODULE_UNSUBSCRIBE);
$tmpl->addVar('subscriber', 'MAIL_FORMAT', _EMKTG_MODULE_MAIL_FORMAT);
$tmpl->addVar('subscriber', 'ACTION', _EMKTG_MODULE_ACTION);
$tmpl->addVar('subscriber', 'MAILING_LIST', _EMKTG_MODULE_MAILINGLIST);
$tmpl->addVar('subscriber', 'SUBMIT', $params->def( 'submit_txt', _EMKTG_MODULE_SUBMIT));
$tmpl->addVar('subscriber', 'ERRNAME', _EMKTG_MODULE_ERRNAME);
$tmpl->addVar('subscriber', 'ERREMAIL', _EMKTG_MODULE_ERREMAIL);
$tmpl->addVar('subscriber', 'ERROR', _EMKTG_MODULE_ERROR);
$tmpl->addVar('subscriber', 'LISTID', $params->def( 'list_id', 1 ));
$tmpl->addVar('subscriber', 'SHOW_SUBSCRIBE', array(($params->def( 'show_subscribe', 0 ) || $my->id)));
$tmpl->addVar('subscriber', 'SUBSCRIBE_TXT', $params->def( 'submit_txt', 'Signup' ));
$tmpl->addVar('subscriber', 'SPOOF', josSpoofValue('com_sm2emailmarketing'));

$tmpl->displayParsedTemplate('subscriber');
?>