<?php
/**
 * 
 * @copyright (C) 2006 DXTribute Software, Karsten Eichentopf
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

/**
 * ensure this file is being included by a parent file
 */
defined('_VALID_MOS') or die('Direct Access to this location is not allowed.');
// Params
$intro = $params->get('intro', '');
$assoc_name = $params->get('assoc_name', '');
$assoc_web = $params->get('assoc_web', '');
    

?>
<div style='text-align: right; font-size: 0.8em; padding: 5px'>
<?php
echo "<a href='http://www.organicdevelopment.co.uk' title='Visit Organic Development'>$intro Organic Development</a>";

if($assoc_name){
	echo ' and ';
	if($assoc_web) echo "<a href='$assoc_web' title='Visit $assoc_name'>";
	echo $assoc_name;
	if($assoc_web) echo "</a>";
}
?>
</div>