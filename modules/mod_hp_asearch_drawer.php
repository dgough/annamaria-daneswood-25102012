<?php

if(mosGetParam($_REQUEST, 'task') != 'asearch') {
	return;
}

$type=mosGetParam($_GET, 'type', 0);
$type_O=new mosHPPropTypes($database);
$type_O->load($type);

# la requete s�lectionnant les Extra-fields li�s � la cat�gorie s�lectionn�e:
$sql = "SELECT ef.* FROM #__hp_prop_ef AS ef"
. "\nLEFT JOIN #__hp_prop_ef2 AS ef2 ON ef2.id=ef.id"
. "\nLEFT JOIN #__hp_prop_types AS t ON t.id=ef2.type"
. "\nWHERE ef.search=1 AND ((ef.published=1 AND ef.hidden=0) OR (ef.name = 'dateFrom' OR ef.name='dateTo'))"
//. "\nAND ( t.lft<=".$type_O->lft." AND t.rgt>=".$type_O->rgt." ) "
. "\nGROUP BY ef.id ORDER BY ef.iscore DESC, t.lft, ef.ordering ASC";

$database->setQuery($sql);
$rows = $database->loadObjectList();

$database->setQuery($sql);
$rows_elements = $database->loadObjectList("name");
$i = 0;
$fields = array();

foreach($rows AS $row) {
	
	if ($row->name == 'amenities' || $row->name == 'pets') {
		
		$fields[$i]->name = $row->name;
		$fields[$i]->caption = $row->search_caption;
	
		if ( !isset($fields[$i]->input) ) $fields[$i]->input = '';
	
		unset($list);
		
		$olist_translated_caption = explode("|",$row->field_elements);
		$olist = explode("|",$rows_elements[$row->name]->field_elements);
		$j=0;
		$req_value = mosGetParam($_REQUEST,$row->name);
		$type = 'checkbox';
		switch($row->field_type){
			case 'radiobutton': $type = 'radio';
			break;
		}
		foreach($olist AS $li) {			
			$fields[$i]->input .= "<div class=\"filter-item\"><input type='".$type."' name='".$row->name.($row->type == 'checkbox' ? "[]'" : '')."' value='".trim($li)."' ".(is_array($req_value) && in_array($li, $req_value) || $li == $req_value ? 'checked="checked"' : '')."> ".trim($olist_translated_caption[$j++]).'</div>';
		}
	}
	$i++;
}

?>
<script type="text/javascript">
<!--
	jQuery(function($){
		$('a.btn-filter').toggle(function(){
			$('.search-drawer-inner').animate({bottom: '0'}, 'fast');
		},
		function(){
			$('.search-drawer-inner').animate({bottom: '153'}, 'fast');
		});
	});
-->
</script>
<div id='mod-hp-search-drawer'>
	
	<div class='search-drawer-expander'>
		
		<div class='search-drawer-inner'>
			<form action="<?php echo sefRelToAbs("index.php?option=com_hotproperty&task=asearch&Itemid=$Itemid"); ?>" method="get" name="searchfrm" id="searchfrm">
				<h3>Refine Your Search</h3>
				<?php
				global $mosConfig_sef;
				
					if(!$mosConfig_sef){
						?>
						<input type='hidden' name='option' value='com_hotproperty' />
						<input type='hidden' name='task' value='asearch' />
						<input type='hidden' name='Itemid' value='<?php echo $Itemid ?>' />
						<?php
					}
		
					$i = 1;
					$j = 0;
					foreach ($fields AS $field)
					{
					?>
						<div class="filter-list-<?php echo $j; ?>">
						<label><?php echo $field->caption; ?></label><br />
							<?php echo $field->input ?>
						</div>
					<?php
						$j++;
					}
					?>
					
				<input type="submit" value="<?php echo _HP_SEARCH; ?>" class="hp-filter-submit" />
			</form>
			
			<a class="btn-filter" href="#">Filter Results</a>
		</div>
		
	</div>
	
</div>