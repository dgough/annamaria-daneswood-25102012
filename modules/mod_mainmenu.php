<?php
/**
* mainMenu Joomla! Module
*
* Class to extend Joomla 1.0.x with a split menu function and more...
*
* @author    yootheme.com
* @copyright Copyright (C) 2007 YOOtheme Ltd. & Co. KG. All rights reserved.
* @license	 http://www.gnu.org/copyleft/gpl.html GNU/GPL
*/

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );

require_once($mosConfig_absolute_path.'/modules/mod_mainmenu/mod_mainmenu.class.php');

global $menuCounter;

// default params
$params->def('menutype', 'mainmenu');
$params->def('class_sfx', '');
$params->def('startLevel', 0);
$params->def('endLevel', 0);
$params->def('accordion', 0);
$params->def('dropdown', 0);
$params->def('showItemBackgroundImage', 0);
$params->def('showAllChildren', 0);
$params->def('fade', 0);
$params->def('fadeInColor', 'ffffff');
$params->def('fadeOutColor', 'ffffff');
$params->def('fadeInTime', 300);
$params->def('fadeOutTime', 500);
$params->def('fadeLevel', 2);

//Create a counter for each menu output
$menutype = $params->get('menutype', 'mainmenu');;
if(empty($menuCounter)){
	$menuCounter = array();
}
if(!isset($menuCounter[$menutype])) $menuCounter[$menutype] = 0;
else $menuCounter[$menutype]++;

//Set a unique id for the menu
$menuname = $menutype.$menuCounter[$menutype];

// render menu
$menu = new mainMenu($params);
$output = $menu->showMenu();

if($output != ''){
	
	echo '<div id="'.$menuname.'">';
	echo $output;
	echo '</div>';

	//Render the appropriate javascript
	?>
	<script type="text/javascript">
	//<!--
	<?php 
	
	if($params->get('accordion', 0)){ ?>
		/* Accordion menu */
		try{
			new YOOAccordionMenu('div#<?php echo $menuname ?> ul.menu li.toggler', 'ul.accordion', { accordion: 'slide' });
		}catch(e){
			
		}
	<?php } 
	
	if($params->get('dropdown', 0)){ ?>
		/* Dropdown menu */
		try{
			new YOODropdownMenu('div#<?php echo $menuname ?> li.parent');
		}catch(e){
			
		}
	<?php } 
	
	if($params->get('fade', 0)){ ?>
		/* Menu fader */
		try{		
			var <?php echo $menuname ?>Enter = { 'background-color': '#<?php echo $params->get('fadeInColor', '') ?>' };
			var <?php echo $menuname ?>Leave = { 'background-color': '#<?php echo $params->get('fadeOutColor', '') ?>' };
			
			new YOOMorph('div#<?php echo $menuname ?> li.level<?php echo $params->def('fadeLevel', 2) ?> a, div#<?php echo $menuname ?> li.level<?php echo $params->def('fadeLevel', 2) ?> span.separator', <?php echo $menuname ?>Enter, <?php echo $menuname ?>Leave,
				{ transition: Fx.Transitions.expoOut, duration: <?php echo $params->get('fadeInTime', '') ?> },
				{ transition: Fx.Transitions.sineIn, duration: <?php echo $params->get('fadeOutTime', '') ?> });
		}catch(e){
			
		}
	<?php } ?>
	//-->
	</script>
<?php } ?>