<?php
/**
* YOOlogin Joomla! Module
*
* @author    yootheme.com
* @copyright Copyright (C) 2007 YOOtheme Ltd. & Co. KG. All rights reserved.
* @license	 GNU/GPL
*/

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );

// count instances
if (!isset($GLOBALS['yoo_logins'])) {
	$GLOBALS['yoo_logins'] = 1;
} else {
	$GLOBALS['yoo_logins']++;
}

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'mod_yoo_login' . DIRECTORY_SEPARATOR . 'helper.php');

global $mainframe, $mosConfig_frontend_login;

if ( $mosConfig_frontend_login != NULL && ($mosConfig_frontend_login === 0 || $mosConfig_frontend_login === '0')) {
	return;
}

$type 	= modYOOloginHelper::getType();
$return	= modYOOloginHelper::getReturnURL($params, $type);

$params->def('greeting', 1);

// init vars
$style                 = $params->get('style', 'default');
$pretext               = $params->get('pretext', '');
$posttext              = $params->get('posttext', '');
$text_mode             = $params->get('text_mode', 'input');
$login_button          = $params->get('login_button', 'icon');
$logout_button         = $params->get('logout_button', 'text');
$auto_remember         = $params->get('auto_remember', '1');
$lost_password         = $params->get('lost_password', '1');
$registration          = $params->get('registration', '1');
$message_login 		   = $params->get('login_message', 0);
$message_logout 	   = $params->get('logout_message', 0);
$login 				   = $params->get('login', $return);
$logout 			   = $params->get('logout', $return);

// css parameters
$yoologin_id           = $GLOBALS['yoo_logins'];

$module_base           = $mosConfig_live_site . '/modules/mod_yoo_login/';

if ($style == 'quick') {
	require(modYOOloginHelper::getLayoutPath('mod_yoo_login', 'quick'));
} else {
	require(modYOOloginHelper::getLayoutPath('mod_yoo_login', 'default'));
}
?>