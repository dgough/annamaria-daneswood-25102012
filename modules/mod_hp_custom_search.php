<?php
/**
* Hot Property Quick Search Module
*
* @package Hot Property 0.9
* @copyright (C) 2008 Organic Development
* @url http://www.organic-development.com/
* @author Organic Development <grow@organic-development.com>
**/

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

# Include the config file
require( $mosConfig_absolute_path.'/administrator/components/com_hotproperty/config.hotproperty.php' );

# Include the language file. Default is English
if ($hp_language=='') {
	$hp_language='english';
}
include_once('components/com_hotproperty/language/'.$hp_language.'.php');

# Get Itemid, determine if the HP component is published
$database->setQuery("SELECT id FROM #__menu WHERE link='index.php?option=com_hotproperty' LIMIT 1");
$Itemid = $database->loadResult();

$search_field = $params->get('search_field','name');
$field_class = $params->get('field_class','inputbox');
$field_size = $params->get('field_size','20');
$submit_value = $params->get('submit_value',_HP_SEARCH);
$submit_class = $params->get('submit_class','button');

# Guess current type user is viewing
global $option, $task, $id;
?>
<form action="index.php" method="POST" name="searchfrm_mod">
<!-- <input type="text" name="search" class="inputbox" /> -->
<label><?php echo _HP_QUICK_SEARCH; ?></label>
<input type="text" name="<?php echo $search_field; ?>" class="<?php echo $field_class; ?>" size="<?php echo $field_size; ?>" value="Property Ref. No." onfocus="this.value='';" /><input type="submit" class="<?php echo $submit_class; ?>" value="<?php echo $submit_value; ?>" />
<input type="hidden" name="option" value="com_hotproperty" />
<input type="hidden" name="task" value="asearch" />
<input type="hidden" name="Itemid" value="<?php echo $Itemid;?>" />
</form>