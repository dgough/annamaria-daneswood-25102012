<?php

/**
* Hot Property Adv. Search Module
*
* @package Hot Property 0.9
* @copyright (C) 2008 Organic Development
* @url http://www.organic-development.com/
* @author Organic Development <grow@organic-development.com>
**/

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

global $database, $hp_use_advsearch, $Itemid, $mosConfig_live_site, $hp_language, $mosConfig_lang;

# Include the config file
require_once( $mosConfig_absolute_path.'/administrator/components/com_hotproperty/config.hotproperty.php' );

# Backend classes
include_once ( $mosConfig_absolute_path.'/administrator/components/com_hotproperty/admin.hotproperty.class.php' );

# Include the language file. Default is English
if ($hp_language=='') {
	$hp_language='english';
}else{
	$hp_language = $mosConfig_lang;
}
include_once('components/com_hotproperty/language/'.$hp_language.'.php');


if ($hp_use_advsearch <> '1') {
	
}

$search_fields = $params->get('search_fields', array());
$force_select = trim($params->get('force_select')) ? explode(',',$params->get('force_select')) : array();
$ef_ids = implode(',',explode(',',$search_fields));

if(!class_exists('mosHTML_hp')){
	class mosHTML_hp {
	
		function rangeDateSelectList( $tag_name, $tag_attribs, $selected, $from, $to ) {
			$search_created_yyyy = array();
			$search_end_yyyy = array();
			$search_mm = array();
			$search_dd = array();
			
			$result = '<input type="radio" name="'.$tag_name.'" value="0" checked /> '._HP_ANYTIME. "<br />";
			$result .= '<input type="radio" name="'.$tag_name.'" value="1"> ';
	
			# Generate Generic Days drop down
			for($j=1 ; $j<=31 ; $j++) {
				$search_dd[] = mosHTML::makeOption( $j );
			}
	
			# Generate Start's YYYY drop down
			for($j=date('Y',$from) ; $j<=date('Y',$to) ; $j++) {
				$search_created_yyyy[] = mosHTML::makeOption( $j );
			}
			$result .= mosHTML::selectList( $search_created_yyyy, $tag_name."_from_y", 'class="inputbox" size="1"',	'value', 'text', date('Y',$from));
			# Generate Start's MM drop down
			$result .= mosHTML::monthSelectList( $tag_name."_from_m", 'class="inputbox" size="1"',	date('m',$from));
			# Generate Start's DD drop down
			$result .= mosHTML::selectList( $search_dd, $tag_name."_from_d", 'class="inputbox" size="1"',	'value', 'text', date('j',$from));
	
			$result .= _HP_SEARCH_TO;
	
			# Generate End's YYYY drop down
			for($j=date('Y',$to) ; $j<=date('Y',$to) ; $j++) {
				$search_end_yyyy[] = mosHTML::makeOption( $j );
			}
			$result .= mosHTML::selectList( $search_end_yyyy, $tag_name."_to_y", 'class="inputbox" size="1"',	'value', 'text', date('Y',$to));
			# Generate End's MM drop down
			$result .= mosHTML::monthSelectList( $tag_name."_to_m", 'class="inputbox" size="1"',	date('m',$to));
			# Generate End's DD drop down
			$result .= mosHTML::selectList( $search_dd, $tag_name."_to_d", 'class="inputbox" size="1"',	'value', 'text', date('j',$to));
	
			return $result;
		}
	
		function rangeText( $tag_name, $tag_attribs ) {
			$result = _HP_SEARCH_FROM;
			$result .= ' <input type="text" name="'.$tag_name.'_from"'.(($tag_attribs) ? " ".$tag_attribs : "").' />';
			$result .= _HP_SEARCH_TO;
			$result .= ' <input type="text" name="'.$tag_name.'_to"'.(($tag_attribs) ? " ".$tag_attribs : "").' />';
	
			return $result;
		}
		
		
	}
}

$type=mosGetParam($_GET, 'type', 0);
$type_O=new mosHPPropTypes($database);
$type_O->load($type);

# la requete s�lectionnant les Extra-fields li�s � la cat�gorie s�lectionn�e:
$sql = "SELECT DISTINCT ef.id, ef.field_type, ef.name, ef.caption, ef.default_value, ef.size, ef.field_elements, ef.prefix_text, ef.append_text, ef.ordering, ef.hidden, ef.published, ef.featured, ef.listing, ef.hideCaption, ef.iscore, ef.search, ef.search_caption, ef.search_type FROM #__hp_prop_ef AS ef"
. "\nLEFT JOIN #__hp_prop_ef2 AS ef2 ON ef2.id=ef.id"
. "\nLEFT JOIN #__hp_prop_types AS t ON t.id=ef2.type"
. "\nWHERE ef.search=1 AND ((ef.published=1 AND ef.hidden=0) OR (ef.name = 'dateFrom' OR ef.name='dateTo'))"
//. "\nAND ( t.lft<=".$type_O->lft." AND t.rgt>=".$type_O->rgt." ) "
. "\nAND (ef.id IN ($ef_ids))"
. "\nORDER BY ef.iscore DESC, t.lft, ef.ordering ASC";

$database->setQuery($sql);
$rows = $database->loadObjectList();

$database->setQuery($sql);
$rows_elements = $database->loadObjectList("name");
$i = 0;
$fields = array();
foreach($rows AS $row) {

	if(count($force_select) && in_array($row->id, $force_select)) $row->field_type = 'selectlist';

	$fields[$i]->id = $row->id;
	$fields[$i]->name = $row->name;
	$fields[$i]->caption = $row->search_caption;

	if ( !isset($fields[$i]->input) ) $fields[$i]->input = '';

	unset($list);

	# Handle core field's search
	if ($row->iscore) {

		# ------------ Type
		if ($row->name == "type") {
			# Select published types
			$database->setQuery( "SELECT * FROM #__hp_prop_types AS t"
				. "\nWHERE t.published='1'"
				. "\nORDER BY t.ordering ASC");
			$types = $database->loadObjectList();

			// liste indent�e des cat�gories:
			$preload = array();
			$preload[] = mosHTML::makeOption( '0', _HP_SEARCH_ALLTYPES );
			$selected = array();
			$selected[] = mosHTML::makeOption( $type );
			$fields[$i]->input = mosHTML::treeSelectList( $types, -1, $preload, 'type', 'class="inputbox" size="1" onchange="javascript:window.location=\''.$mosConfig_live_site.'/index.php?option=com_hotproperty&task=advsearch&type=\'+this.value+\'&Itemid='.$Itemid.'\'"', 'value', 'text', $selected );

		}
		# ------------ Price
		elseif ($row->name == "price") {
			$fields[$i]->input = mosHTML_hp::rangeText( $row->name, 'size="8" class="inputbox"');
		}
		# ------------ Featured
		elseif ($row->name == "featured") {
			$fields[$i]->input .= ' <input type="checkbox" name="'.$row->name.'" value="1" '.(mosGetParam($_REQUEST,$row->name) ? 'checked="checked"' : '').'/> ';
		}
		# ------------ Agent
		elseif ($row->name == "agent") {
			# Get Agents that has at least one property
			$database->setQuery("SELECT a.*, COUNT(p.id) AS properties, c.id AS companyid, c.name AS company FROM (#__hp_agents AS a, #__hp_prop_types AS t)"
				.	"\nLEFT JOIN #__hp_companies AS c ON c.id=a.company"
				.	"\nLEFT JOIN #__hp_properties AS p ON p.agent=a.id"
				.	"\nWHERE t.id=p.type"
				. "\n AND p.published='1' AND p.approved='1' AND t.published='1'"
				. "\n	AND (publish_up = '0000-00-00 00:00:00' OR publish_up <= NOW())"
				. "\n	AND (publish_down = '0000-00-00 00:00:00' OR publish_down >= NOW())"
				.	"\nGROUP BY p.agent");
			$agents = $database->loadObjectList();

			$search_agents = array();
			$search_agents[] = mosHTML::makeOption( '0', _HP_SEARCH_ALLAGENTS );
			foreach($agents AS $agent) {
				$search_agents[] = mosHTML::makeOption( $agent->id, $agent->name );
			}

			$fields[$i]->input = mosHTML::selectList( $search_agents, $row->name, 'class="inputbox" size="1"',	'value', 'text', mosGetParam($_REQUEST,$row->name));
		}
		# ------------ Company
		elseif ($row->name == "company") {
			# Get Companies that has at least one property
			$database->setQuery("SELECT c.name, c.id FROM #__hp_companies AS c"
				.	"\nLEFT JOIN #__hp_agents AS a ON c.id=a.company"
				.	"\nLEFT JOIN #__hp_properties AS p ON p.agent=a.id"
				.	"\nLEFT JOIN #__hp_prop_types AS t ON t.id=p.type"
				.	"\nWHERE t.id=p.type"
				. "\n AND p.published='1' AND p.approved='1' AND t.published='1'"
				. "\n	AND (publish_up = '0000-00-00 00:00:00' OR publish_up <= NOW())"
				. "\n	AND (publish_down = '0000-00-00 00:00:00' OR publish_down >= NOW())"
				.	"\nGROUP BY a.company");
			$companies = $database->loadObjectList();

			$search_companies = array();
			$search_companies[] = mosHTML::makeOption( '0', _HP_SEARCH_ALLCOMPANIES );
			foreach($companies AS $company) {
				$search_companies[] = mosHTML::makeOption( $company->id, $company->name );
			}

			$fields[$i]->input = mosHTML::selectList( $search_companies, $row->name, 'class="inputbox" size="1"',	'value', 'text', mosGetParam($_REQUEST,$row->name));
		}
		# ------------ Hits
		elseif ($row->name == "hits") {
			$search_hits = array();
			$search_hits[] = mosHTML::makeOption( '0', _HP_SEARCH_LESS_THAN );
			$search_hits[] = mosHTML::makeOption( '1', _HP_SEARCH_MORE_THAN );

			$fields[$i]->input = mosHTML::selectList( $search_hits, "hits_range", 'class="inputbox" size="1"',	'value', 'text', '1');
			$fields[$i]->input .= '&nbsp; <input type="text" name="hits" class="inputbox" size="8" />';
		}
		# ------------ Created
		elseif ($row->name == "created") {
			# Get min & max year from available properties
			$sql = "SELECT MIN(created) AS start, MAX(created) AS end FROM #__hp_properties AS p"
				. "\nLEFT JOIN #__hp_prop_types AS t ON p.type = t.id"
				. "\nWHERE p.published='1' AND p.approved='1' AND t.published='1'"
				. "\n	AND (publish_up = '0000-00-00 00:00:00' OR publish_up <= NOW())"
				. "\n	AND (publish_down = '0000-00-00 00:00:00' OR publish_down >= NOW())"
				.	"\n AND created <> '0000-00-00 00:00:00'";
			$database->setQuery($sql);
			$database->loadObject($created);

			$fields[$i]->input = mosHTML_hp::rangeDateSelectList( $row->name, '', $selected, strtotime($created->start), strtotime($created->end) );
		}
		# ------------ Modified
		elseif ($row->name == "modified") {
			# Get min & max year from available properties
			$sql = "SELECT MIN(modified) AS start, MAX(modified) AS end FROM #__hp_properties AS p"
				. "\nLEFT JOIN #__hp_prop_types AS t ON p.type = t.id"
				. "\nWHERE p.published='1' AND p.approved='1' AND t.published='1'"
				. "\n	AND (publish_up = '0000-00-00 00:00:00' OR publish_up <= NOW())"
				. "\n	AND (publish_down = '0000-00-00 00:00:00' OR publish_down >= NOW())"
				.	"\n AND created <> '0000-00-00 00:00:00'";
			$database->setQuery($sql);
			$database->loadObject($modified);

			$fields[$i]->input = mosHTML_hp::rangeDateSelectList( $row->name, '', $selected, strtotime($modified->start), strtotime($modified->end) );
		}
		elseif ( $row->name == "suburb" ) {
			$olist_translated_caption = explode("|",$row->field_elements);
			$olist = explode("|",$rows_elements[$row->name]->field_elements);
			$list[] = mosHTML::makeOption( '', '' );
			$j=0;
			foreach($olist AS $li) {
				$list[] = mosHTML::makeOption( trim($li),$olist_translated_caption[$j++] );
			}
			
			$searched = mosGetParam($_REQUEST,$row->name);
			if(is_array($searched)){
				$selected = array();
				foreach($searched as $v){
					$selected[] = mosHTML::makeOption( $v );
				}
			}else{
				$selected = $searched;
			}
			$fields[$i]->input = mosHTML::selectList( $list, $row->name, 'class="inputbox '.$row->name.'" size="1"', 'value', 'text', $selected);
		}
		# Other Core fields will use normal inputbox
		# Postcode, Name, State, Country, Suburb, Intro Text, Full Text
		else {
			$fields[$i]->input = ' <input type="text" name="'.$row->name.'" size="35" class="inputbox" value="'.mosGetParam($_REQUEST,$row->name).'" /> ';
		}

	}
	# Non-core Extra Fields
	else {
		if ($row->search_type == "range_1") {
			$range_1_options = array();
			$range_1_options[] = mosHTML::makeOption( '0', _HP_SEARCH_LESS_THAN );
			$range_1_options[] = mosHTML::makeOption( '1', _HP_SEARCH_MORE_THAN );

			$fields[$i]->input = mosHTML::selectList( $range_1_options, $row->name."_range", 'class="inputbox" size="1"',	'value', 'text', '1');
			$fields[$i]->input .= '&nbsp; <input type="text" name="'.$row->name.'" class="inputbox" size="'.($row->size > 0 ? $row->size : 5).'" />';
		}
		elseif ($row->search_type == "range_2") {
			$fields[$i]->input = mosHTML_hp::rangeText( $row->name, 'size="'.($row->size > 0 ? $row->size : 5).'" class="inputbox"');
		}
		else if($row->name == 'dateFrom' || $row->name == 'dateTo'){
		
			/*
			if the calendar class exists, then output it - but only if on a property page
			*/
			$field = $row;
			global $mosConfig_absolute_path;
			if (file_exists($mosConfig_absolute_path.'/components/com_virtuemart/virtuemart_parser.php')) {

				require_once($mosConfig_absolute_path.'/components/com_virtuemart/virtuemart_parser.php');
				global $ps_booking;
				require_once(CLASSPATH.'ps_calendar.php');
	
				if(!isset($ps_calendar)){				
					$ps_calendar = new ps_calendar('mod_hp_asearch','',1);
					$ps_calendar->setScripts();
					$ps_calendar->getCalendar();
				}	
				
				$selected = $field->name == 'dateFrom' ? $ps_booking->dateFrom : $ps_booking->dateTo;
					
				$fields[$i]->input = $ps_calendar->renderInput($field->name, mosGetParam($_REQUEST,$field->name, $selected), ($field->name == 'dateTo' ? 'dateFrom' : ''));
			}else{
				unset($fields[$i]);
			}
		}	
		elseif ($row->field_type == "link" || $row->field_type == "text" || $row->field_type == "multitext") {
			$fields[$i]->input = ' <input type="text" name="'.$row->name.'" size="'.(($row->field_type == "multitext") ? '20' : $row->size).'" class="inputbox" /> ';
		}
		elseif ( $row->field_type == "selectlist" ) {
			$olist_translated_caption = explode("|",$row->field_elements);
			$olist = explode("|",$rows_elements[$row->name]->field_elements);
			$list[] = mosHTML::makeOption( '', '' );
			$j=0;
			foreach($olist AS $li) {
				$list[] = mosHTML::makeOption( trim($li),$olist_translated_caption[$j++] );
			}
			
			$searched = mosGetParam($_REQUEST,$row->name);
			if(is_array($searched)){
				$selected = array();
				foreach($searched as $v){
					$selected[] = mosHTML::makeOption( $v );
				}
			}else{
				$selected = $searched;
			}
			$fields[$i]->input = mosHTML::selectList( $list, $row->name, 'class="inputbox '.$row->name.'" size="1"', 'value', 'text', $selected);
		}
		elseif ( $row->field_type == "radiobutton" ) {
			$type = 'radio';
			$olist = explode("|",$rows_elements[$row->name]->field_elements);
			if(in_array('Yes', $olist) && in_array('No', $olist))
			{
				$row->field_elements = 'Yes';
				$olist = array('Yes');
				$type = 'checkbox';
			}

			$olist_translated_caption = explode("|",$row->field_elements);
			$j=0;
			foreach($olist AS $li) {
				$fields[$i]->input .= '<input type="'.$type.'" name="'.$row->name.'" value="'.trim($li).'" '.(mosGetParam($_REQUEST,$row->name) ? 'checked="checked"' : '').'> '.trim($olist_translated_caption[$j++]). "<br />";
			}
		}
		elseif ( $row->field_type == "checkbox" ) {
			$olist_translated_caption = explode("|",$row->field_elements);
			$olist = explode("|",$rows_elements[$row->name]->field_elements);
			$j=0;
			foreach($olist AS $li) {
				$fields[$i]->input .= "<input type='checkbox' name='".$row->name."[]' value='".trim($li)."' ".(mosGetParam($_REQUEST,$row->name) ? 'checked="checked"' : '')."> ".trim($olist_translated_caption[$j++]). "<br />";
			}
		} elseif ( $row->field_type == "selectmultiple" ) {
			$olist_translated_caption = explode("|",$row->field_elements);
			$olist = explode("|",$rows_elements[$row->name]->field_elements);
			$fields[$i]->input .= "<select multiple name='".$row->name."[]' size='".$row->size."' class='inputbox'>";
			$j=0;
			foreach($olist AS $li) {
				$fields[$i]->input .= "<option value='".$li."' ".(mosGetParam($_REQUEST,$row->name) ? 'checked="checked"' : '')." > ".trim($olist_translated_caption[$j++]). "</option>";
			}
			$fields[$i]->input .= "</select>";
		}

	}
	$i++;
}
?>

<h3><?php echo _ASEARCH_TITLE?></h3>

<div class='mod_hp_search'>
	<form action="<?php echo sefRelToAbs("index.php?option=com_hotproperty&task=asearch&Itemid=$Itemid"); ?>" method="get" name="searchfrm" id="searchfrm">
		<?php
		global $mosConfig_sef;
		
			if(!$mosConfig_sef){
				?>
				<input type='hidden' name='option' value='com_hotproperty' />
				<input type='hidden' name='task' value='asearch' />
				<input type='hidden' name='Itemid' value='<?php echo $Itemid ?>' />
				<?php
			}

			$i = 1;
			$j = 0;
			foreach ($fields AS $field)
			{
			?>
				<div class="row<?php echo ($i++%2); ?> hp_row<?php echo $j ?> <?php echo 'hp_ef_'.$field->id ?>">
					<label><?php echo $field->caption; ?></label><br />
						<?php echo $field->input ?>
				</div>
			<?php
				$j++;
			}
			?>
		<div class="row<?php echo ($i++%2); ?> submit">
			<!--<input type="submit" value="<?php echo _HP_SEARCH; ?>" class="inputbox hp_search_submit" />-->
<!--
			<input type="image" src="<?php echo sefRelToAbs('templates/annamaria_2011/images/site/btn_search_big.png'); ?>" alt="search" value="<?php echo _HP_SEARCH; ?>" class="inputbox hp_search_submit" />
-->
			<input type="image" class="inputbox hp_search_submit" value="" alt="" />
			<a class="hp_adv_search" href='<?php echo sefRelToAbs('index.php?option=com_hotproperty&amp;task=advsearch') ?>' title="Click to view the advanced search criteria"><?php echo _HP_ADVANCED_SEARCH; ?></a>
		</div>
	</form>
	<form action="<?php echo sefRelToAbs("index.php?option=com_hotproperty&task=resetsearch&Itemid=$Itemid"); ?>" method="get" name="resetfrm" id="resetfrm">
		<input type="hidden" name="source_page" value="<?php echo $_SERVER['REQUEST_URI'] ?>" />
		<input type="image" src="" alt=" " value=" " class="hp_adv_reset" />
	</form>
</div>