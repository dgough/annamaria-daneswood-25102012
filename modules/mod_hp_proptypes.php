<?php
/**
* Hot Property Featured Property Module with Pictures - Organic Development Version
* No Tables and more module parameters for changing options
* 
* @package Hot Property 0.9
* @copyright (C) 2008 Organic Development
* @url http://www.organic-development.com/
* @author Organic Development <grow@organic-development.com>
**/

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

global $database;

require( $mosConfig_absolute_path.'/administrator/components/com_hotproperty/config.hotproperty.php' );
include_once( $mosConfig_absolute_path.'/components/com_hotproperty/hotproperty.class.php' );
include_once ( $mosConfig_absolute_path.'/administrator/components/com_hotproperty/admin.hotproperty.class.php' );
include_once( $mosConfig_absolute_path.'/components/com_hotproperty/functions.hotproperty.php' );

# Include the language file. Default is English
if ($hp_language=='') {
	$hp_language='english';
}
include_once('components/com_hotproperty/language/'.$hp_language.'.php');

# Get params
$count = intval( $params->get( 'count', 4 ) );
$direction = $params->get( 'direction', 'vertical' );

$database->setQuery('select * from #__hp_prop_types where parent=0');
$types = $database->loadObjectList();

	?>
	<div class="fp_proptypes">
		<div class="corner_frontdoor">
			<div class="corner_backdoor">
            	<div class="proptype_wrapper">
                    	<?php // index.php?option=com_hotproperty&task=viewtype&id=$type->id
							$j=1;
							foreach ($types as $type) { ?>
                       			<div class="proptype-item" id="item<?php echo $j ?>"> 
                        
					<span class="proptypename"> <?php echo $type->name; ?> </span> 
                    	<div class="proptypeimg"><?php echo $type->desc; ?> </div>
							
                            	</div>
                            <?php
								$j++;
							} ?>
                             <div class="clearit"></div>
                             </div>
			</div> <!-- corner tr -->
		</div> <!-- corner tl -->
	</div>
