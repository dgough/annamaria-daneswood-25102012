<jos:comment>
@package Joomla!
@subpackage com_sm2emailmarketing
@copyright Copyright (C) 2006 SM2 / All Rights Reserved
@license commercial
@author SM2 <info@sm2joomla.com>
</jos:comment>


<jos:tmpl name="subscriber">
<script language="javascript" type="text/javascript">
		function submitSM2EMSubscription() {
			var coll = document.sm2EMSubscribe;
			var r = new RegExp("[^0-9A-Za-z]", "i");
			var errorMSG = '';
			var iserror=0;

            if (coll != null) {
                var elements = coll.elements;
                // loop through all input elements in form
                for (var i=0; i < elements.length; i++) {
                    // check if element is mandatory; here mosReq=1
                    if (elements.item(i).className.indexOf('required') != -1) {
                        // check for empty field value
                        if (elements.item(i).value == '') {
                            // add up all error messages
                            errorMSG += elements.item(i).getAttribute('title') + '\n';
                            // notify user by changing background color, in this case to red
                            elements.item(i).style.backgroundColor = "#CC0000";
                            iserror=1;
                            continue;
                        }
                    }
                }
		    }
			if(iserror==1) {
                alert('{ERROR}\n'+errorMSG);
                return false;
            } else {
                return true;
			}
		}
</script>
        <form action="<jos:sef>index.php?option=com_sm2emailmarketing&amp;task=subscribe</jos:sef>" method="post" name="sm2EMSubscribe" id="sm2EMSubscribe" onsubmit="return submitSM2EMSubscription()">
        <jos:tmpl type="simplecondition" requiredvars="display_details" varscope="subscriber">
                <div>
                <label> <span>{NAME}: </span></label><br />
                        <input type="text" name="name" class="inputbox required" title="{ERRNAME}" />
                </div>
                <div>
                <label> <span>{EMAIL}: </span></label><br />
                        <input type="text" name="email" class="inputbox required" title="{ERREMAIL}" />
                
                </div>
        </jos:tmpl>
        <!-- Enter the 1 for HTML or 0 for text t o enable the auto setting
                         of HTML or TEXT subscriptions.
                -->
                <input type="hidden" name="html" value="1" />
                <!-- -->
        <jos:tmpl type="condition" varscope="subscriber" conditionvar="SHOW_SUBSCRIBE">
                <jos:sub condition="1">
	        		<div>
	                <label> <span>{ACTION}: </span></label>
                    <select name="status">
						<option value="1">{SUBSCRIBE}</option>
						<option value="0">{UNSUBSCRIBE}</option>
                    </select>                
	                </div>
                </jos:sub>
                <jos:sub condition="__default">
                	 <input type="hidden" name="status" value="1" />
                </jos:sub>
        </jos:tmpl>
                <jos:tmpl type="simplecondition" requiredvars="display_lists" varscope="subscriber">
                <!-- Enter the ID of the list to be auto subscribed in the value field below.
                     If you wish to be auto subscribed to multiple lists then copy the following
                     hidden field for each list.
                -->
                <input type="hidden" name="{SPOOF}" value="1" />
                <input type="hidden" name="lists[]" value="{LISTID}" />
                </jos:tmpl>
                <div>
                <input type="submit" value="{SUBMIT}" class="button" />
                </div>
        </form>
</jos:tmpl>