<?php
/**
 * @copyright (C) 2008 by Slashes & Dots Sdn Bhd - All rights reserved!
 * @license http://www.azrul.com Copyrighted Commercial Software
 *   
 **/
 
//no direct access
(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');

global $mainframe;
include_once($mainframe->getCfg('absolute_path'). "/components/com_myblog/modules/mod_myblog.php");
$objModule = new MyblogModule();
$objModule->showLatestEntriesIntro($params);
