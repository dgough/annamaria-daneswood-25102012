<?php

/**
* Hot Property's Search Module
*
* @package Hot Property 0.9
* @copyright (C) 2004-2005 Lee Cher Yeong
* @url http://www.Mosets.com/
* @author Lee Cher Yeong <hotproperty@mosets.com>
**/

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

# Include the config file
require( $mosConfig_absolute_path.'/administrator/components/com_hotproperty/config.hotproperty.php' );

# Include the language file. Default is English
if ($hp_language=='') {
	$hp_language='english';
}
include_once('components/com_hotproperty/language/'.$hp_language.'.php');


# Select published types
$database->setQuery( "SELECT * FROM #__hp_prop_types AS t"
	. "\nWHERE t.published='1'"
	. "\nORDER BY t.ordering ASC");
$types = $database->loadObjectList();

# Get Itemid, determine if the HP component is published
$database->setQuery("SELECT id FROM #__menu"
	.	"\nWHERE link='index.php?option=com_hotproperty'"
	.	"\nLIMIT 1");
$Itemid = $database->loadResult();

# Guess current type user is viewing
global $option, $task, $id;
$current_type=0;

if ($option == "com_hotproperty" && $id > 0) {
	# User is browing type
	if ($task == "viewtype") {
		$current_type = $id;
	} elseif ($task == "view") {
		$database->setQuery( "SELECT type FROM #__hp_properties AS p"
			. "\nWHERE p.published='1' AND p.approved='1' AND p.id='".$id."'"
			.	"\nLIMIT 1");
		$current_type = $database->loadResult();	
	}
}
?>
<form action="index.php" method="post" name="searchfrm_mod">
<input type="text" name="name" class="inputbox" />
<input type="submit" class="button" value="<?php echo _HP_SEARCH; ?>" />
<input type="hidden" name="option" value="com_hotproperty" />
<input type="hidden" name="task" value="asearch" />
<input type="hidden" name="Itemid" value="<?php echo $Itemid;?>" />
</form>


