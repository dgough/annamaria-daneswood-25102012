<?php
/*
 // "Simple Image Rotator" Module for Joomla! 1.0.x - Version 1.2
 // License: http://www.gnu.org/copyleft/gpl.html
 // Authors: Fotis Evangelou - George Chouliaras
 // Copyright (c) 2006 - 2007 JoomlaWorks.gr - http://www.joomlaworks.gr
 // Project page at http://www.joomlaworks.gr - Demos at http://demo.joomlaworks.gr
 // ***Last update: August 30th, 2007***
 */

/** ensure this file is being included by a parent file */
defined('_VALID_MOS') or die('Direct Access to this location is not allowed.');

global $database, $mosConfig_absolute_path, $mosConfig_live_site, $mainframe, $mosConfig_offset, $mosConfig_locale, $jwsir_counter;

if (!$jwsir_counter)
	$jwsir_counter = 0;

$jwsir_counter++;

// module parameters
$moduleclass_sfx = $params -> get('moduleclass_sfx', '');
$imagefolder = trim($params -> get('imagefolder'));
$transition = intval($params -> get('transition', 40));
$delay = intval($params -> get('delay', 3000));
$width = intval($params -> get('width', 600));
$height = intval($params -> get('height', 200));
$forceresize = intval($params -> get('forceresize', 0));
$enablelink = trim($params -> get('enablelink'));
$link = ampReplace($params -> get('link'));
$linktitle = $params -> get('linktitle', 'JoomlaWorks Simple Image Rotator');
$target = trim($params -> get('target'));
$display = trim($params -> get('display'));
$darkbg = intval($params -> get('darkbg', 0));
$useforbanners = intval($params -> get('useforbanners', 0));
$bannerfolder = $params -> get('bannerfolder', 'images/banners/');
?>

<!-- JoomlaWorks "Simple Image Rotator" Module (v1.2) starts here -->
<noscript>
	<div class="message">
		Sorry, but Javascript is not enabled in your browser!
	</div>
</noscript>
<style type="text/css">
	@import "/modules/mod_jw_sir/mod_jw_sir.css";
	
	ul#jw-sir-<?php echo $jwsir_counter ?>,
	ul#jw-sir-<?php echo $jwsir_counter ?> li#jw-sir-loading<?php echo $darkbg ? '-black' : '' ?>-<?php echo $jwsir_counter ?>
	{
		width:<?php echo $width;?>px; 
		height:<?php echo $height;?>px;
	}
	
</style>

<script type="text/javascript" src="/modules/mod_jw_sir/mod_jw_sir.js"></script>
<script type="text/javascript">
	var delay =<?php echo $delay;?>;
	var transition = <?php echo $transition;?>;
	var preLoader = 'jw-sir-loading-<?php echo $jwsir_counter ?><?php echo $darkbg ? '-black' : '' ?>';
	
	window.addEvent('domready', function(){init_jwsir('jw-sir-<?php echo $jwsir_counter ?>',preLoader,transition,delay)})
</script>
<ul id="jw-sir-<?php echo $jwsir_counter ?>" class="<?php echo $moduleclass_sfx;?> jw-sir">
	<?php
	// retrieve the published banners only
	$database -> setQuery("SELECT * FROM  #__banner WHERE showBanner=1");
	$rows = $database -> loadObjectList();
	if ($useforbanners) {
		// banner mode
		if ($darkbg) {
			echo '<li id="jw-sir-loading-black-'.$jwsir_counter.'" class="jw-sir-loading-black"></li>';
		} else {
			echo '<li id="jw-sir-loading-'.$jwsir_counter.'" class="jw-sir-loading-black"></li>';
		}
		foreach ($rows as $row) {
			$link = sefRelToAbs('index.php?option=com_banners&amp;task=click&amp;bid=' . $row -> bid);
			$finalpath = $bannerfolder . $row -> imageurl;
			// resize
			if ($forceresize) {
				$image_info = @getimagesize($finalpath);
				$rwidth = $image_info[0];
				$rheight = $image_info[1];
				if ($image_info[0] > $width) {
					$target = $width;
					$percentage = ($target / $rwidth);
					$rwidth = round($rwidth * $percentage);
					$rheight = round($rheight * $percentage);
					$img_dimensions = 'style="width:' . $rwidth . 'px;height:' . $rheight . 'px;" ';
				} else {
					$img_dimensions = 'style="width:' . $rwidth . 'px;height:' . $rheight . 'px;" ';
				}
			}
			// output
			if ($enablelink) {
				if ($target) {
					echo '
	<li>
		<a href="' . $link . '" target="_blank"><img src="' . $finalpath . '" title="' . $linktitle . '" alt="' . $linktitle . '" ' . $img_dimensions . ' /></a>
	</li>
	';
				} else {
					echo '
	<li>
		<a href="' . $link . '"><img src="' . $finalpath . '" title="' . $linktitle . '" alt="' . $linktitle . '" ' . $img_dimensions . ' /></a>
	</li>
	';
				}
			} else {
				echo '
	<li><img src="' . $finalpath . '" title="' . $linktitle . '" alt="' . $linktitle . '" ' . $img_dimensions . ' />
	</li>
	';
			}
		}
	} else {
		// standard mode
		if (file_exists($imagefolder) && is_readable($imagefolder)) {
			$folder = opendir($imagefolder);
		} else {
			echo '
	<div class="message">
		"Simple Image Rotator" error! Please check the module settings and make sure you have entered a valid image folder path!
	</div>
	';
			return;
		}
		$allowed_types = array("jpg", "JPG", "jpeg", "JPEG", "gif", "GIF", "png", "PNG", "bmp", "BMP");
		$index = array();
		while ($file = readdir($folder)) {
			if (in_array(substr(strtolower($file), strrpos($file, ".") + 1), $allowed_types)) {array_push($index, $file);
			}
		}
		closedir($folder);
		if ($display == 'random') {shuffle($index);
		} else {sort($index);
		}
		if ($darkbg) {
			echo '<li id="jw-sir-loading-black-'.$jwsir_counter.'" class="jw-sir-loading-black"></li>';
		} else {
			echo '<li id="jw-sir-loading-'.$jwsir_counter.'" class="jw-sir-loading-black"></li>';
		}
		foreach ($index as $file) {
			$finalpath = $imagefolder . "/" . $file;
			// resize
			if ($forceresize) {
				$image_info = @getimagesize($finalpath);
				$rwidth = $image_info[0];
				$rheight = $image_info[1];
				if ($image_info[0] > $width) {
					$target = $width;
					$percentage = ($target / $rwidth);
					$rwidth = round($rwidth * $percentage);
					$rheight = round($rheight * $percentage);
					$img_dimensions = 'style="width:' . $rwidth . 'px;height:' . $rheight . 'px;" ';
				} else {
					$img_dimensions = 'style="width:' . $rwidth . 'px;height:' . $rheight . 'px;" ';
				}
			}
			// output
			if ($enablelink) {
				if ($target) {
					echo '
	<li>
		<a href="' . $link . '" target="_blank"><img src="' . $finalpath . '" title="' . $linktitle . '" alt="' . $linktitle . '" ' . $img_dimensions . '/></a>
	</li>
	';
				} else {
					echo '
	<li>
		<a href="' . $link . '"><img src="' . $finalpath . '" title="' . $linktitle . '" alt="' . $linktitle . '" ' . $img_dimensions . '/></a>
	</li>
	';
				}
			} else {
				echo '
	<li><img src="' . $finalpath . '" title="' . $linktitle . '" alt="' . $linktitle . '" ' . $img_dimensions . '/>
	</li>
	';
			}
		}
		// end if
	}
	?>
</ul>
<!-- JoomlaWorks "Simple Image Rotator" Module (v1.2) ends here -->
