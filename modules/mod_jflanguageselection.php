<?php
/**
 * Joom!Fish - Multi Lingual extention and translation manager for Joomla!
 * Copyright (C) 2003-2007 Think Network GmbH, Munich
 *
 * All rights reserved.  The Joom!Fish project is a set of extentions for
 * the content management system Joomla!. It enables Joomla!
 * to manage multi lingual sites especially in all dynamic information
 * which are stored in the database.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * The "GNU General Public License" (GPL) is available at
 * http: *www.gnu.org/copyleft/gpl.html
 * -----------------------------------------------------------------------------
 * $Id: mod_jflanguageselection.php 610 2007-08-30 11:11:58Z geraint $
 *
*/

/**
* @package joomfish
* @subpackage mod_jflanguageselection
* @copyright 2003-2007 Think Network GmbH
* @license http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @version 1.7 - $Id: mod_jflanguageselection.php 610 2007-08-30 11:11:58Z geraint $
* @author Alex Kempkens <Alex@JoomFish.net>
**/

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

// Prevent redifinition of class, when module is used in two separate locations
if( !defined( 'JFMODULE_CLASS' ) ) {
	define( 'JFMODULE_CLASS', true );

	class JFModuleHTML {

	function makeOption( $value, $text='', $style='') {
		$obj = new stdClass;
		$obj->value = $value;
		$obj->text = $text;
		$obj->style = $style;
		return $obj;
	}

	/**
	* Generates an HTML select list
	* @param array An array of objects
	* @param string The value of the HTML name attribute
	* @param string Additional HTML attributes for the <select> tag
	* @param string The name of the object variable for the option value
	* @param string The name of the object variable for the option text
	* @param mixed The key that is selected
	* @returns string HTML for the select list
	*/
	function selectList( &$arr, $tag_name, $tag_attribs, $key, $text, $selected=NULL ) {
		// check if array
		if ( is_array( $arr ) ) {
			reset( $arr );
		}

		$html 	= "\n<select name=\"$tag_name\" $tag_attribs>";
		$count 	= count( $arr );

		for ($i=0, $n=$count; $i < $n; $i++ ) {
			$k = $arr[$i]->$key;
			$t = $arr[$i]->$text;
			$id = ( isset($arr[$i]->id) ? @$arr[$i]->id : null);

			$extra = ' '.$arr[$i]->style." ";
			$extra .= $id ? " id=\"" . $arr[$i]->id . "\"" : '';
			if (is_array( $selected )) {
				foreach ($selected as $obj) {
					$k2 = $obj->$key;
					if ($k == $k2) {
						$extra .= " selected=\"selected\"";
						break;
					}
				}
			} else {
				$extra .= ($k == $selected ? " selected=\"selected\"" : '');
			}
			$html .= "\n\t<option value=\"".$k."\"$extra >" . $t . "</option>";
		}
		$html .= "\n</select>\n";

		return $html;
	}

		/**
	 * internal function to generate a new href link
	 * @param	string	shortcode of the wanted language
	 * @return	string	new href string
	 */
		function _createHRef( $code ) {
			$href= "index.php";
			$hrefVars = '';
			$queryString = mosGetParam($_SERVER, 'QUERY_STRING', null);
			if( !empty($queryString) ) {
				$vars = explode( "&", $queryString );
				if( count($vars) > 0 && $queryString) {
					foreach ($vars as $var) {
						if( eregi('=', $var ) ) {
							list($key, $value) = explode( "=", $var);
							if( $key != "lang" ) {
								if( $hrefVars != "" ) {
									$hrefVars .= "&amp;";
								}
								// ignore mosmsg to ensure it is visible in frontend
								if( $key != 'mosmsg' ) {
									$hrefVars .= "$key=$value";
								}
							}
						}
					}
				}
			}

			// Add the existing variables
			if( $hrefVars != "" ) {
				$href .= '?' .$hrefVars;
			}

			if( $code != null && $code != 'en' ) {
				$lang = 'lang=' .$code;
			} else{
				// it's important that we add at least the basic parameter - as of the sef is adding the actual otherwise!
				$lang = 'lang=en';
			}
			
			// if there are other vars we need to add a & otherwiese ?
			if( $hrefVars == '' ) {
				$href .= '?' . $lang;
			} else {
				$href .= '&amp;' . $lang;
			}

			return sefRelToAbs( $href );
		}
	}
}

$inc_jf_css	= intval( $params->get( 'inc_jf_css', 1 ));
$cache 		= intval( $params->get( 'cache', 0 ) );
$type 		= trim( $params->get( 'type', 'dropdown' ));
$show_active= intval( $params->get( 'show_active', 1 ) );
$spacer		= trim( $params->get( 'spacer', '&nbsp;' ) );
$restrict	= trim( $params->get( 'restrict', '' ) );
$restrict 	= $restrict ? explode(',',$restrict) : array();


global $_JOOMFISH_MANAGER;
global $mosConfig_live_site, $mosConfig_absolute_path, $mosConfig_lang, $iso_client_lang;

if(count($restrict)){
	$langActive = $_JOOMFISH_MANAGER->getActiveLanguages();
	foreach($langActive AS $k => $lang){
		if(!in_array($lang->iso, $restrict)) unset($langActive[$k]);
	}
}

$outString = '';
if( !isset( $langActive ) || count($langActive)==0) {
	// No active languages => nothing to show :-(
	return;
}

// check for unauthorised access to inactive language
if (!array_key_exists($mosConfig_lang,$langActive)){
	reset($langActive);
	$lang = current($langActive);
	mosRedirect(sefRelToAbs("index.php?lang=".$lang->iso));
}

switch ( $type ) {
	default:
	case 'dropdown':			// drop down of names
		if ( count($langActive)>0 ) {
			$langOptions=array();
			$noscriptString='';
			foreach( $langActive as $language )
			{
				$language->shortcode = ($language->shortcode == '') ? $language->iso : $language->shortcode;
				$href = JFModuleHTML::_createHRef ($language->shortcode);
				if( $language->code == $mosConfig_lang && !$show_active ) {
					continue;		// Not showing the active language
				}
				if ($language->code == $mosConfig_lang ) {
					$activehref=$href;
				}

				$langOptions[] = mosHTML::makeOption( $href, $language->name );
				$href = JFModuleHTML::_createHRef ($language->iso);
				$noscriptString .= '<a href="' .$href. '"><span lang="' .$language->iso. '" xml:lang="' .$language->iso. '">' .$language->name. '</span></a>&nbsp;';
			}

			if( count( $langOptions ) > 1 ) {
				$href = JFModuleHTML::_createHRef (null);
				$langlist = mosHTML::selectList( $langOptions, 'lang', ' class="jflanguageselection" size="1" onchange="document.location.replace(this.value);"', 'value', 'text', $activehref);
				$outString = '<div id="jflanguageselection">';
				$outString .= '<label for="jflanguageselection" class="jflanguageselection">' ._CMN_SELECT. '</label>';
				$outString .= $langlist;
				$outString .= '</div>';

				if( $noscriptString != '' ) {
					$outString .= '<noscript>' .$noscriptString. '</noscript>';
				}
			} elseif (count( $langOptions ) == 1) {
				$outString = '<div id="jflanguageselection"><ul class="jflanguageselection"><li id="active_language"><a href="' .$langOptions[0]->value. '"><span lang="' .$langOptions[0]->value. '" xml:lang="' .$langOptions[0]->value. '">' .$langOptions[0]->text. '</a></li></ul></div>';
			}
		}
		break;

	case 'dropdownplusimage':			// drop down of names
		if ( count($langActive)>0 ) {
			//$outString = '<div class="flaggedlist">'."\n";
			//$outString .= '<div class="jflabel">'."\n".'<label for="jflanguageselection" class="jflanguageselection">' ._CMN_SELECT. '</label>'."\n".'</div>'."\n".'<div class="jfselect">'."\n";
			$langOptions=array();
			$noscriptString='';
			$activeLangImg  = null;
			
			foreach( $langActive as $language )
			{				
				$language->shortcode = ($language->shortcode == '') ? $language->iso : $language->shortcode;
				$href = JFModuleHTML::_createHRef ($language->iso);
				if( $language->code == $mosConfig_lang && !$show_active ) {
					continue;		// Not showing the active language
				}

				if( isset($language->image) && $language->image!="" ) {
					$langImg = '/images/' .$language->image;
				} else {
					$langImg = '/components/com_joomfish/images/flags/' .$language->iso .".gif";
				}
				if ($language->code == $mosConfig_lang ){
					$activehref=$href;
					$activeLangImg = array( 'img' => $langImg, 'code' => $language->iso, 'name' => $language->name );
				}
				$langOptions[] = JFModuleHTML::makeOption($href , $language->name, " style='padding-left:22px;background-image: url(\"".$mosConfig_live_site . $langImg."\");background-repeat: no-repeat;background-position:center left;'" );
				$href = JFModuleHTML::_createHRef ($language->iso);
				$noscriptString .= '<a href="' .$href. '"><span lang="' .$language->iso. '" xml:lang="' .$language->iso. '">' .$language->name. '</span></a>&nbsp;';
			}

			if( count( $langOptions ) > 1 ) {
				$href = JFModuleHTML::_createHRef (null);

				$outString = '<div id="jflanguageselection">';
				$outString .= '<label for="jflanguageselection" class="jflanguageselection">' ._CMN_SELECT. '</label>';
				//$langlist = JFModuleHTML::selectList( $langOptions, 'lang', 'id="jflanguageselection" class="inputbox" onchange="document.location.replace(this.value);"  style="padding-left:22px;background-image: url(\''.$mosConfig_live_site . $langImg.'\');background-repeat: no-repeat;background-position:center left;"', 'value', 'text', $activehref);
				//$langlist = mosHTML::selectList( $langOptions, 'lang', 'id="jflanguageselection" class="jflanguageselection" size="1" onchange="document.location.replace(this.value);"', 'value', 'text', $activehref);
				if( $activeLangImg != null ) {
					$outString .='<img src="' .$mosConfig_live_site . $activeLangImg['img']. '" alt="' .$activeLangImg['name']. '" title="' .$activeLangImg['name']. '" border="0" class="langImg"/>';
				}
				$langlist = JFModuleHTML::selectList( $langOptions, 'lang', ' class="jflanguageselection" onchange="document.location.replace(this.value);"', 'value', 'text', $activehref);
				$outString .= ''.$langlist.'';
				$outString .= '</div>'."\n";

				if( $noscriptString != '' ) {
					$outString .= '<noscript>' .$noscriptString. '</noscript>';
				}
			} elseif (count( $langOptions ) == 1) {
				$outString = '<div id="jflanguageselection">';
				if( $activeLangImg != null ) {
					$outString .='<img src="' .$mosConfig_live_site . $activeLangImg['img']. '" alt="' .$activeLangImg['name']. '" title="' .$activeLangImg['name']. '" border="0" class="langImg"/>';
				}
				$outString .= '<ul class="jflanguageselection"><li id="active_language"><a href="' .$langOptions[0]->value. '"><span lang="' .$langOptions[0]->value. '" xml:lang="' .$langOptions[0]->value. '">' .$langOptions[0]->text. '</a></li></ul></div>';
			}
		}
		break;

	case 'names':
	case 'namesplusimages':
		$outString = '<div id="jflanguageselection">';
		$outString .= '<ul class="jflanguageselection">';
		foreach( $langActive as $language )
		{
			$langActive = '';
			if( $language->code == $mosConfig_lang ) {
				if( !$show_active ) {
					continue;		// Not showing the active language
				} else {
					$langActive = ' id="active_language"';
				}
			}

			$language->shortcode = ($language->shortcode == '') ? $language->iso : $language->shortcode;
			$href = JFModuleHTML::_createHRef ($language->iso);
			$outString .= '<li' .$langActive. '>';
			if($type == 'namesplusimages') {
				if( isset($language->image) && $language->image!="" ) {
					$langImg = '/images/' .$language->image;
				} else {
					$langImg = '/components/com_joomfish/images/flags/' .$language->iso .".gif";
				}
				$outString .='<img src="' .$mosConfig_live_site . $langImg. '" alt="' .$language->name. '" title="' .$language->name. '" border="0" class="langImg"/>';
			}
			$outString .= '<a href="' .$href. '" ><span lang="' .$language->iso. '" xml:lang="' .$language->iso. '">' .$language->name. '</span></a></li>';
		}
		$outString .= '</ul></div>';
		break;

	case 'images':
		$outString = '<div id="jflanguageselection">';
		$outString .= '<ul class="jflanguageselection">';
		foreach( $langActive as $language )
		{
			$langActive = '';
			if( $language->code == $mosConfig_lang ) {
				if( !$show_active ) {
					continue;		// Not showing the active language
				} else {
					$langActive = ' id="active_language"';
				}
			}

			$language->shortcode = ($language->shortcode == '') ? $language->iso : $language->shortcode;
			$href = JFModuleHTML::_createHRef ($language->shortcode);

			if( isset($language->image) && $language->image!="" ) {
				$langImg = '/images/' .$language->image;
			} else {
				$langImg = '/components/com_joomfish/images/flags/' .$language->iso .".gif";
			}

			if( file_exists( $mosConfig_absolute_path . $langImg ) ) {
				$outString .= '<li' .$langActive. '><a href="' .$href. '"><img src="' .$mosConfig_live_site . $langImg. '" alt="' .$language->name. '" title="' .$language->name. '" /></a></li>';
			} else {
				$outString .= '<li' .$langActive. '><a href="' .$href. '">' .$language->name. '</a></li>';
			}
		}
		$outString .= '</ul></div>';
		break;

	case 'rawimages':
		$outString = '<div class="rawimages">';
		foreach( $langActive as $language )
		{
			$langActive = '';
			if( $language->code == $mosConfig_lang ) {
				if( !$show_active ) {
					continue;		// Not showing the active language
				} else {
					$langActive = ' id="active_language"';
				}
			}

			$language->shortcode = ($language->shortcode == '') ? $language->iso : $language->shortcode;
			$href = JFModuleHTML::_createHRef ($language->iso);

			if( isset($language->image) && $language->image!="" ) {
				$langImg = '/images/' .$language->image;
			} else {
				$langImg = '/components/com_joomfish/images/flags/' .$language->iso .".gif";
			}

			if( file_exists( $mosConfig_absolute_path . $langImg ) ) {
				$outString .= '<span' .$langActive. '><a href="' .$href. '"><img src="' .$mosConfig_live_site . $langImg. '" alt="' .$language->name. '" title="' .$language->name. '" /></a></span>';
			} else {
				$outString .= '<span' .$langActive. '><a href="' .$href. '">' .$language->name. '</a></span>';
			}
		}
		$outString .= '</div>';
		break;
}


if( $inc_jf_css && file_exists( $mosConfig_absolute_path. '/modules/mod_jflanguageselection.css' ) ) {
	?>
<link href="<?php echo $mosConfig_live_site;?>/modules/mod_jflanguageselection.css" rel="stylesheet" type="text/css"/>
	<?php
}
?>
<?php echo $outString;?>
<!--Joom!fish <?php echo JoomFishManager::getVersion();?>-->
<!-- &copy; 2003-2007 Think Network, released under the GPL. -->
<!-- More information: at http://www.joomfish.net -->