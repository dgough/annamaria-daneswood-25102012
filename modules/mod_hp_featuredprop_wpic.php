<?php

/**
* Hot Property's Featured Property Module with Pictures
*
* @package Hot Property 0.9
* @copyright (C) 2004-2005 Lee Cher Yeong
* @url http://www.Mosets.com/
* @author Lee Cher Yeong <hotproperty@mosets.com>
**/

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

# Include the config file
require( $mosConfig_absolute_path.'/administrator/components/com_hotproperty/config.hotproperty.php' );

# Include the language file. Default is English
if ($hp_language=='') {
	$hp_language='english';
}
include_once('components/com_hotproperty/language/'.$hp_language.'.php');

# Get params
$count = intval( $params->get( 'count', 3 ) );
$direction = $params->get( 'direction', 'vertical' );

# Get total number of published featured items
$database->setQuery( "SELECT COUNT(*) AS total FROM #__hp_properties AS p"
	. "\nLEFT JOIN #__hp_prop_types AS t ON t.id=p.type"
	.	"\nWHERE p.published=1 AND p.approved=1 AND t.published=1"
	. "\n AND p.featured=1"
	. "\n	AND (publish_up = '0000-00-00 00:00:00' OR publish_up <= NOW())"
	. "\n	AND (publish_down = '0000-00-00 00:00:00' OR publish_down >= NOW())");
$total = $database->loadResult();

# Get Itemid, determine if the HP component is published
$database->setQuery("SELECT id FROM #__menu"
	.	"\nWHERE link='index.php?option=com_hotproperty'"
	.	"\nLIMIT 1");
$Itemid = $database->loadResult();

# Get all featured property
$database->setQuery( "SELECT p.* FROM #__hp_properties AS p"
		. "\nLEFT JOIN #__hp_prop_types AS t ON p.type = t.id"
		. "\nLEFT JOIN #__hp_featured AS f ON f.property = p.id"
		. "\nWHERE p.published='1' AND p.approved='1' AND t.published='1'"
		. "\n	AND (publish_up = '0000-00-00 00:00:00' OR publish_up <= NOW())"
		. "\n	AND (publish_down = '0000-00-00 00:00:00' OR publish_down >= NOW())"
		.	"\nAND p.featured='1'"
		. "\nORDER BY f.ordering ASC"
		. "\nLIMIT 0, ".$count );
	$featured = $database->loadObjectList();
?>
<table cellpadding="1" cellspacing="1" border="0" width="100%">
<?php if ($direction == 'horizontal') echo "<tr>"; ?>
<?php foreach($featured AS $f) { 
	unset($thumb);
	# Get the first thumbnail of the property
	$database->setQuery( "SELECT thumb, title FROM #__hp_photos"
		. "\nWHERE property='".$f->id."'"
		. "\nORDER BY ordering ASC"
		. "\nLIMIT 0,1");
	$database->loadObject($thumb);

	if ($thumb->thumb <> '') {
		$thumb_imgsize = GetImageSize ($mosConfig_absolute_path.$hp_imgdir_thumb.$thumb->thumb); 
	} else {
		$thumb_imgsize = GetImageSize ($mosConfig_absolute_path.$hp_imgdir_thumb.$hp_img_noimage_thumb); 
	}

?>
  <?php if ($direction == 'vertical') echo "<tr>"; ?><td align="center">
		<center><a href="
	<?php echo sefRelToAbs('index.php?option=com_hotproperty&task=view&id='.$f->id.'&Itemid='.$Itemid); ?>"><?php echo '<img '.$thumb_imgsize[3].' border="0" src="'.$mosConfig_live_site."/".$hp_imgdir_thumb.((!empty($thumb->thumb)) ? $thumb->thumb : $hp_img_noimage_thumb).'" alt="'.$thumb->title.'">'; ?><br />
	<?php echo $f->name; ?></a></center></td><?php if ($direction == 'vertical') echo "</tr>"; ?>
 <?php } ?>
<?php if ($direction == 'horizontal') echo "</tr>"; ?>
</table>

<?php if ($total > $count) { 
echo '<a href="'
	.	sefRelToAbs('index.php?option=com_hotproperty&task=viewfeatured&Itemid='.$Itemid)
	.	'">'._HP_MOREFEATURED.'</a>';
} ?>