<?php
/**
*/

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );

require_once('mod_bannersmanager/replaceObjEmbed.php');

global $_MAMBOTS,$option,$Itemid,$mosConfig_absolute_path, $jsBanner;
// clientids must be an integer
$groupid = $params->get( 'banner_gid', '' );
$banner = null;

if ( $groupid != '' ) {

if (file_exists("$mosConfig_absolute_path/mambots/bannersmanager/$option.php")) 
{
	$_MAMBOTS->loadBot("bannersmanager",$option, 1);
	$value = $_MAMBOTS->call("onGetValue");
	$where_extension = " e.value LIKE '%,$value,%' OR e.value LIKE '%,0,%' ";
}
else
{
	$where_extension = "";
}	
$where_menu = " b.menuid LIKE '%,$Itemid,%' OR b.menuid LIKE '%,0,%' ";



$random = intval($params->get( 'random',0));
$number_of_banners = intval($params->get( 'number_of_banners',1));
$vertical = intval($params->get( 'vertical',1));

if ($random == 1)
{
	$order_sql = "ORDER BY RAND() ";
}
else
{
	$order_sql = "ORDER BY b.ordering ASC ";
}

if ($number_of_banners <= -1)
	$limit_sql = "";
else if ($number_of_banners <= 1)
	$limit_sql = "LIMIT 1";
else
	$limit_sql = "LIMIT $number_of_banners";

if ($where_extension == "")
{
	$query = "SELECT DISTINCT b.*"
	. "\n FROM #__bannersmanager_banner as b"
	. "\n LEFT JOIN #__bannersmanager_extension as e ON b.id = e.bannerid"
	. "\n WHERE b.published=1 AND b.groupid = $groupid" 
	. "\n AND ($where_menu)"
	. "\n $order_sql $limit_sql"
	;
}
else
{
	$query = "SELECT DISTINCT b.*"
	. "\n FROM #__bannersmanager_banner as b"
	. "\n LEFT JOIN #__bannersmanager_extension as e ON b.id = e.bannerid"
	. "\n WHERE b.published=1 AND b.groupid = $groupid" 
	. "\n AND (($where_menu) OR (e.name ='$option' AND ($where_extension)))"
	. "\n $order_sql $limit_sql"
	;
}

$database->setQuery( $query );
$banners = $database->loadObjectList();

ob_start();


if(isset($banners[0])) {
	echo '<div>';
foreach($banners as $banner)
{
	$query = "UPDATE #__bannersmanager_banner"
	. "\n SET impmade = impmade + 1"
	. "\n WHERE id = " . (int) $banner->id
	;
	$database->setQuery( $query );
	if(!$database->query()) {
		echo $database->stderr( true );
		return;
	}
	$banner->impmade++;
	
	if (($banner->implimit > 0) && ($banner->impmade >= $banner->implimit))
	{
		$query = "UPDATE #__bannersmanager_banner"
		. "\n SET published = 0"
		. "\n WHERE id = " . (int) $banner->id
		;
		$database->setQuery( $query );
		if(!$database->query()) {
			echo $database->stderr( true );
			return;
		}
	}

	switch($banner->type) {
		case 1:
			if (trim($banner->custombannercode )) {
				echo $banner->custombannercode;
			}
			break;
		case 0:
			
			if (eregi( "(\.bmp|\.gif|\.jpg|\.jpeg|\.png)$", $banner->imageurl )) {
				$imageurl 	= $mosConfig_live_site .'/images/banners/'. $banner->imageurl;
				$link		= sefRelToAbs( 'index.php?option=com_bannersmanager&amp;task=click&amp;bid='. $banner->id );
				
				echo ($banner->clickurl ? '<a href="'. $link .'" target="_blank">' : '').'<img src="'. $imageurl .'" border="0" alt="'.$banner->name.'" />'.($banner->clickurl ? '</a>' : '');
				
				/*
				if($banner->clickurl) echo '<a href="'. $link .'" target="_blank">';
				echo '<img src="'. $imageurl .'" border="0" alt="'.$banner->name.'" />';
				if($banner->clickurl) '</a>';
				*/

			} else if (eregi("\.swf$", $banner->imageurl)) {
				
				$width = $banner->width ? "width='$banner->width'" : '';
				$height = $banner->height ? "height='$banner->height'" : '';				
				
				echo "<object classid='clsid:d27cdb6e-ae6d-11cf-96b8-444553540000' codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,00,0' $width $height>\n"
					 ."<param name='movie' value='$mosConfig_live_site/images/banners/$banner->imageurl' />\n"
					 ."<param name='quality' value='high' />\n"
					 ."<param name='loop' value='".($banner->loop ? 'true' : 'false')."' />\n"
					 ."<param name='menu' value='".($banner->menu ? 'true' : 'false')."' />\n"
					 .($banner->bgcolor ? "<param name='bgcolor' value='$banner->bgcolor' />" : '')
					 ."<param name='wmode' value='$banner->wmode' />"
										
					."<embed src='$mosConfig_live_site/images/banners/$banner->imageurl' quality='high' name='mymoviename' type='application/x-shockwave-flash' pluginspage='http://www.macromedia.com/go/getflashplayer' $width $height wmode='$banner->wmode' "
					."loop='".($banner->loop ? 'true' : 'false')."' "
					."menu='".($banner->menu ? 'true' : 'false')."' "
					.($banner->bgcolor ? 	"bgcolor='$banner->bgcolor' " : '')							
					."></embed> 
				</object>";
			}
	}
	if ($vertical == 1)
	{
		echo "<br />";
	}
}
echo '</div>';
}
}
//Do JS transformation
echo replaceObjEmbed(ob_get_clean(),'/modules/mod_bannersmanager');
?>