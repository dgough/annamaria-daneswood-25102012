<?php
/**
* YOOlogin Joomla! Module
*
* @author    yootheme.com
* @copyright Copyright (C) 2007 YOOtheme Ltd. & Co. KG. All rights reserved.
* @license	 GNU/GPL
*/

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );

class modYOOloginHelper
{
	function getReturnURL($params, $type)
	{
		// url of current page that user will be returned to after login
		if ($query_string = mosGetParam( $_SERVER, 'QUERY_STRING', '' )) {
			$return = 'index.php?' . $query_string;
		} else {
			$return = 'index.php';
		}

		// converts & to &amp; for xtml compliance
		$return = str_replace( '&', '&amp;', $return );
		
		return $return;
	}

	function getType()
	{
		global $my;
	    return $my->id ? 'logout' : 'login';
	}

	function getName()
	{
		global $my;
		return $my->name;
	}

	function getLayoutPath($module, $template)
	{
		global $mosConfig_absolute_path;
		
		return $mosConfig_absolute_path . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . $module . DIRECTORY_SEPARATOR . 'tmpl' . DIRECTORY_SEPARATOR . $template . '.php';
	}	
}