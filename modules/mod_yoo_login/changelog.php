<?php
/**
* YOOlogin Joomla! Module
*
* @author    yootheme.com
* @copyright Copyright (C) 2007 YOOtheme Ltd. & Co. KG. All rights reserved.
* @license	 GNU/GPL
*/

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );
?>

Changelog
------------

1.0.5
- Added black styling

1.0.4
- Added outline styling for input fields
- Fixed XHTML validation

1.0.3
- Fixed php zlib warning, for gzipped CSS files

1.0.2
- Added quick style
- Added default style
- Completely reworked

1.0.1
- Fixes

1.0.0
- Initial Release