<?php
/**
* YOOlogin Joomla! Module
*
* @author    yootheme.com
* @copyright Copyright (C) 2007 YOOtheme Ltd. & Co. KG. All rights reserved.
* @license	 GNU/GPL
*/

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );
?>

<?php if($type == 'logout') : ?>
<form action="<?php echo sefRelToAbs( 'index.php?option=logout' ); ?>" method="post" name="login">
<?php else : ?>
<form action="<?php echo sefRelToAbs( 'index.php' ); ?>" method="post" name="login">
<?php endif; ?>

<span class="<?php echo $style ?>" style="display: block;">
	<span class="yoo-login">
	
		<?php if($type == 'logout') : ?>
		<span class="logout">
		
			<?php if ($params->get('greeting')) : ?>
			<span class="greeting"><?php echo modYOOloginHelper::getName(); ?></span>
			<?php endif; ?>
			
			<span class="logout-button<?php echo "-" . $logout_button ?>">
				<button value="<?php if ( $logout_button == "text" ) { echo _BUTTON_LOGOUT; } ?>" name="Submit" type="submit" title="<?php echo _BUTTON_LOGOUT; ?>"><?php if ( $logout_button == "text" ) { echo _BUTTON_LOGOUT; } ?></button>
			</span>
		
			<input type="hidden" name="option" value="logout" />
			<input type="hidden" name="op2" value="logout" />
			<input type="hidden" name="lang" value="<?php echo $mosConfig_lang; ?>" />
			<input type="hidden" name="return" value="<?php echo htmlspecialchars( sefRelToAbs( $logout ) ); ?>" />
			<input type="hidden" name="message" value="<?php echo htmlspecialchars( $message_logout ); ?>" />
			
		</span>
		
		<?php else : ?>
		
		<?php
			// used for spoof hardening
			$validate = josSpoofValue(1);		
		?>

		<span class="login">
		
			<?php echo $params->get('pretext'); ?>
			
			<span class="username">
			
				<input type="text" name="username" size="18" alt="<?php echo _USERNAME; ?>" value="<?php echo _USERNAME; ?>" onblur="if(this.value=='') this.value='<?php echo _USERNAME; ?>';" onfocus="if(this.value=='<?php echo _USERNAME; ?>') this.value='';" />
				
			</span>
			
			<span class="password">
			
				<input type="password" name="passwd" size="10" alt="<?php echo _PASSWORD; ?>" value="<?php echo _PASSWORD; ?>" onblur="if(this.value=='') this.value='<?php echo _PASSWORD; ?>';" onfocus="if(this.value=='<?php echo _PASSWORD; ?>') this.value='';" />
				
			</span>

			<input type="hidden" name="remember" value="yes" />
			
			<span class="login-button<?php echo "-" . $login_button ?>">
				<button value="<?php if ( $login_button == "text" ) { echo _BUTTON_LOGIN; } ?>" name="Submit" type="submit" title="<?php echo _BUTTON_LOGIN; ?>"><?php if ( $login_button == "text" ) { echo _BUTTON_LOGIN; } ?></button>
			</span>
			
			<?php if ( $lost_password ) { ?>
			<span class="lostpassword">
				<a href="<?php echo sefRelToAbs( 'index.php?option=com_registration&amp;task=lostPassword' ); ?>" title="<?php echo _LOST_PASSWORD; ?>"><?php echo _LOST_PASSWORD; ?></a>
			</span>
			<?php } ?>
			
			<?php
			if ($mainframe->getCfg('allowUserRegistration') && $registration) { ?>
			<span class="registration">
				<a href="<?php echo sefRelToAbs( 'index.php?option=com_registration&amp;task=register' ); ?>" title="<?php echo _CREATE_ACCOUNT; ?>"><?php echo _CREATE_ACCOUNT; ?></a>
			</span>
			<?php } ?>
			
			<?php echo $params->get('posttext'); ?>
			
			<input type="hidden" name="option" value="login" />
			<input type="hidden" name="op2" value="login" />
			<input type="hidden" name="lang" value="<?php echo $mosConfig_lang; ?>" />
			<input type="hidden" name="return" value="<?php echo htmlspecialchars( sefRelToAbs( $login ) ); ?>" />
			<input type="hidden" name="message" value="<?php echo htmlspecialchars( $message_login ); ?>" />
			<input type="hidden" name="force_session" value="1" />
			<input type="hidden" name="<?php echo $validate; ?>" value="1" />

		</span>
		
		<?php endif; ?>
		
	</span>
</span>
</form>