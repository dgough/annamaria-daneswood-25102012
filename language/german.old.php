<?php
/**
* @version $Id: english.php 6085 2006-12-24 18:59:57Z robs $
* @package Joomla
* @copyright Copyright (C) 2005 Open Source Matters. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined( '_VALID_MOS' ) or die( 'Beschränkter Zugang' );

// Site page note found
define( '_404', 'Leider konnten wir die von Ihnen angeforderte Seite nicht finden.' );
define( '_404_RTS', 'Zurück zur Website' );

define( '_SYSERR1', 'Der Datenbank Adapter ist nicht verfügbar.' );
define( '_SYSERR2', 'Keine Verbindung zum Datenbankserver' );
define( '_SYSERR3', 'Keine Verbindung zur Datenbank' );

// common
DEFINE('_LANGUAGE','de');
DEFINE('_NOT_AUTH','Sie sind nicht berechtigt diesen Ressource zu sehen.');
DEFINE('_DO_LOGIN','Sie müssen einloggen.');
DEFINE('_VALID_AZ09',"Bitte geben Sie eine gültige %s ein.  Keine Leerzeichen, mehr als %d Zeichen lang und enthaltend 0-9,a-z,A-Z");
DEFINE('_VALID_AZ09_USER',"Bitte geben Sie eine gültige %s ein. Mehr als %d Zeichen lang und enthaltend 0-9,a-z,A-Z");
DEFINE('_CMN_YES','Ja');
DEFINE('_CMN_NO','Nein');
DEFINE('_CMN_SHOW','Zeigen');
DEFINE('_CMN_HIDE','Verbergen');

DEFINE('_CMN_NAME','Name');
DEFINE('_CMN_DESCRIPTION','Beschreibung');
DEFINE('_CMN_SAVE','Speichern');
DEFINE('_CMN_APPLY','Anwenden');
DEFINE('_CMN_CANCEL','Abbrechen');
DEFINE('_CMN_PRINT','Drucken');
DEFINE('_CMN_PDF','PDF');
DEFINE('_CMN_EMAIL','Mail');
DEFINE('_ICON_SEP','|');
DEFINE('_CMN_PARENT','Übergeordnetes');
DEFINE('_CMN_ORDERING','Reihenfolge');
DEFINE('_CMN_ACCESS','Zugangsberechtigung ');
DEFINE('_CMN_SELECT','Wählen');

DEFINE('_CMN_NEXT','Weiter');
DEFINE('_CMN_NEXT_ARROW'," &gt;&gt;");
DEFINE('_CMN_PREV','Zurück');
DEFINE('_CMN_PREV_ARROW',"&lt;&lt; ");

DEFINE('_CMN_SORT_NONE','Kein Sortieren');
DEFINE('_CMN_SORT_ASC','Aufsteigend sortieren');
DEFINE('_CMN_SORT_DESC','Absteigend sortieren');

DEFINE('_CMN_NEW','Neu');
DEFINE('_CMN_NONE','Keine');
DEFINE('_CMN_LEFT','Links');
DEFINE('_CMN_RIGHT','Rechts');
DEFINE('_CMN_CENTER','Mitte');
DEFINE('_CMN_ARCHIVE','Archivieren');
DEFINE('_CMN_UNARCHIVE','Dearchivieren');
DEFINE('_CMN_TOP','Anfang');
DEFINE('_CMN_BOTTOM','Ende');

DEFINE('_CMN_PUBLISHED','Veröffentlicht');
DEFINE('_CMN_UNPUBLISHED','unveröffentlicht');

DEFINE('_CMN_EDIT_HTML','HTML bearbeiten');
DEFINE('_CMN_EDIT_CSS','CSS bearbeiten');

DEFINE('_CMN_DELETE','Löschen');

DEFINE('_CMN_FOLDER','Ordner');
DEFINE('_CMN_SUBFOLDER','Unterordner');
DEFINE('_CMN_OPTIONAL','Wahlfrei');
DEFINE('_CMN_REQUIRED','Notwendig');

DEFINE('_CMN_CONTINUE','Weiter');

DEFINE('_STATIC_CONTENT','Statischer Inhalt');

DEFINE('_CMN_NEW_ITEM_LAST','Neuheiten standardmäβig auf dem letzten Platz. Reihenfolge kann geändert werden nachdem  der Artikel gespeichert wurde.');
DEFINE('_CMN_NEW_ITEM_FIRST','Neuheiten standardmäβig auf dem ersten Platz. Reihenfolge kann geändert werden nachdem der Artikel gespeichert wurde.');
DEFINE('_LOGIN_INCOMPLETE','Bitte Anwendername und Passwort Felder ausfüllen.');
DEFINE('_LOGIN_BLOCKED','Ihr Login wurde gesperrt. Bitte wenden Sie sich an den Administrator.');
DEFINE('_LOGIN_INCORRECT','Falscher Anwendername oder Passwort. Bitte wieder versuchen.');
DEFINE('_LOGIN_NOADMINS','Sie können nicht einloggen. Es sind keine Administratoren eingerichtet.');
DEFINE('_CMN_JAVASCRIPT','!Warnung! JavaScript muss für den einwandfreien Betrieb aktiviert werden.');

DEFINE('_NEW_MESSAGE','Eine neue Privatmail ist angekommen.');
DEFINE('_MESSAGE_FAILED','Der Nutzer hat seine Mailbox gesperrt. Mail Fehler.');

DEFINE('_CMN_IFRAMES', 'Diese Option wird nicht korrekt fuktionieren. Leider unterstützt Ihr Browser keine Inline Frames.');

DEFINE('_INSTALL_3PD_WARN','Warnung: Installieren von 3rd-Party Erweiterungen kann die Sicherheit Ihres Servers gefährden. Upgrade Ihrer Installation aktualisiert nicht Ihre 3rd-Party Erweiterungen.<br />Weitere Information über Absicherung Ihrer Website finden sie bei <a href="http://forum.joomla.org/index.php/board,267.0.html" target="_blank" style="color: blue; text-decoration: underline;">Security Forum</a>.');
DEFINE('_INSTALL_WARN','Zu Ihrer eigenen Sicherheit, bitte entfernen Sie das Installationsverzeichnis, inklusive aller Dateien und Unterordner - dann Seite aktualisieren.');
DEFINE('_TEMPLATE_WARN','<font color=\"red\"><b>Template Datei nicht gefunden! Auf der Suche nach Vorlage:</b></font>');
DEFINE('_NO_PARAMS','Es gibt keine Parameter für diesen Artikel.');
DEFINE('_HANDLER','Handler nicht für Typ definiert');

/** mambots */
DEFINE('_TOC_JUMPTO','Artikel-Übersicht');

/**  content */
DEFINE('_READ_MORE','Lesen Sie mehr...');
DEFINE('_READ_MORE_REGISTER','Registrieren um mehr zu lesen...');
DEFINE('_MORE','Mehr...');
DEFINE('_ON_NEW_CONTENT', "Ein neuer Inhaltseintrag wurde eingereicht durch [ %s ]  betitelt [ %s ]  aus Abschnitt [ %s ]  und Kategorie  [ %s ]" );
DEFINE('_SEL_CATEGORY','- Kategorie auswählen -');
DEFINE('_SEL_SECTION','- Abschnitt auswählen -');
DEFINE('_SEL_AUTHOR','- Autor auswählen -');
DEFINE('_SEL_POSITION','- Position auswählen -');
DEFINE('_SEL_TYPE','- Typ auswählen -');
DEFINE('_EMPTY_CATEGORY','Diese Kategorie ist momentan leer.');
DEFINE('_EMPTY_BLOG','Es sind keine Artikel vorhanden.');
DEFINE('_NOT_EXIST','Die Seite, die Sie zugreifen wollen, existiert nicht.<br />Bitte wählen Sie eine Seite aus dem Hauptmenü.');
DEFINE('_SUBMIT_BUTTON','Absenden');

/** classes/html/modules.php */
DEFINE('_BUTTON_VOTE','Stimmen');
DEFINE('_BUTTON_RESULTS','Ergebnisse');
DEFINE('_USERNAME','Nutzername');
DEFINE('_LOST_PASSWORD','Passwort vergessen?');
DEFINE('_PASSWORD','Passwort');
DEFINE('_BUTTON_LOGIN','Einloggen');
DEFINE('_BUTTON_LOGOUT','Ausloggen');
DEFINE('_NO_ACCOUNT','Noch kein Konto?');
DEFINE('_CREATE_ACCOUNT','Registrieren');
DEFINE('_VOTE_POOR','Gering');
DEFINE('_VOTE_BEST','Beste');
DEFINE('_USER_RATING','Nutzer Wertung');
DEFINE('_RATE_BUTTON','Werten');
DEFINE('_REMEMBER_ME','Angemeldet bleiben');

/** contact.php */
DEFINE('_ENQUIRY','Anfrage');
DEFINE('_ENQUIRY_TEXT','Dies ist eine Anfrage per Mail %s von:');
DEFINE('_COPY_TEXT','Dies ist eine Kopie der Mail die Sie an %s gesendet haben, per %s ');
DEFINE('_COPY_SUBJECT','Kopie von:');
DEFINE('_THANK_MESSAGE','Danke für Ihre Mail.');
DEFINE('_CLOAKING','Diese Mail-Adresse ist gegen Spam Bots geschützt, zum Sehen müssen Sie JavaScript aktivieren');
DEFINE('_CONTACT_HEADER_NAME','Name');
DEFINE('_CONTACT_HEADER_POS','Position');
DEFINE('_CONTACT_HEADER_EMAIL','Mail');
DEFINE('_CONTACT_HEADER_PHONE','Telefon');
DEFINE('_CONTACT_HEADER_FAX','Fax');
DEFINE('_CONTACTS_DESC','Die Kontaktliste dieser Website.');
DEFINE('_CONTACT_MORE_THAN','Sie können nicht mehr als eine Mail-Adresse eingeben.');

/** classes/html/contact.php */
DEFINE('_CONTACT_TITLE','Kontakt');
DEFINE('_EMAIL_DESCRIPTION','Mail senden an diesen Kontakt:');
DEFINE('_NAME_PROMPT',' Name eingeben:');
DEFINE('_EMAIL_PROMPT',' Mail-Adresse:');
DEFINE('_MESSAGE_PROMPT',' Nachricht eingeben:');
DEFINE('_SEND_BUTTON','Senden');
DEFINE('_CONTACT_FORM_NC','Bitte sicherstellen daβ die Form vollständig und gültig ist.');
DEFINE('_CONTACT_TELEPHONE','Telefon: ');
DEFINE('_CONTACT_MOBILE','Handy: ');
DEFINE('_CONTACT_FAX','Fax: ');
DEFINE('_CONTACT_EMAIL','Mail: ');
DEFINE('_CONTACT_NAME','Name: ');
DEFINE('_CONTACT_POSITION','Position: ');
DEFINE('_CONTACT_ADDRESS','Adresse: ');
DEFINE('_CONTACT_MISC','Information: ');
DEFINE('_CONTACT_SEL','Kontakt wählen:');
DEFINE('_CONTACT_NONE','Keine Kontaktdaten gelisted.');
DEFINE('_CONTACT_ONE_EMAIL','Sie können nicht mehr als eine Mail-Adresse eingeben');
DEFINE('_EMAIL_A_COPY','Eine Kopie dieser Nachricht an Ihre eigene Adresse mailen');
DEFINE('_CONTACT_DOWNLOAD_AS','Information herunterladen als');
DEFINE('_VCARD','VCard');

/** pageNavigation */
DEFINE('_PN_LT','&lt;');
DEFINE('_PN_RT','&gt;');
DEFINE('_PN_PAGE','Seite');
DEFINE('_PN_OF','von');
DEFINE('_PN_START','Start');
DEFINE('_PN_PREVIOUS','Zurück');
DEFINE('_PN_NEXT','Weiter');
DEFINE('_PN_END','Ende');
DEFINE('_PN_DISPLAY_NR','# zeigen');
DEFINE('_PN_RESULTS','Ergebnisse');

/** emailfriend */
DEFINE('_EMAIL_TITLE','Einen Freund mailen');
DEFINE('_EMAIL_FRIEND','Dieses an einen Freund mailen.');
DEFINE('_EMAIL_FRIEND_ADDR',"Mail-Adresse Ihres Freundes:");
DEFINE('_EMAIL_YOUR_NAME','Ihr Name:');
DEFINE('_EMAIL_YOUR_MAIL','Ihre Mail-Adresse:');
DEFINE('_SUBJECT_PROMPT',' Betreff:');
DEFINE('_BUTTON_SUBMIT_MAIL','Mail senden');
DEFINE('_BUTTON_CANCEL','Abbrechen');
DEFINE('_EMAIL_ERR_NOINFO','Bitte Ihre gültige E-Mail eingeben, und die gültige E-Mail zum Senden.');
DEFINE('_EMAIL_MSG','Die folgende Seite aus der "%s" Website wurde Ihnen geschickt von %s ( %s ).

Sie können es unter folgender URL zugreifen:
%s');
DEFINE('_EMAIL_INFO','Artikel gesendet von');
DEFINE('_EMAIL_SENT','Dieser Artikel wurde gesendet an');
DEFINE('_PROMPT_CLOSE','Fenster schliessen');

/** classes/html/content.php */
DEFINE('_AUTHOR_BY', ' Beigetragen von');
DEFINE('_WRITTEN_BY', 'Geschrieben von');
DEFINE('_LAST_UPDATED', 'Zuletzt aktualisiert');
DEFINE('_BACK','[ Zurück ]');
DEFINE('_LEGEND','Legende');
DEFINE('_DATE','Datum');
DEFINE('_ORDER_DROPDOWN','Reihenfolge');
DEFINE('_HEADER_TITLE','Artikel Titel');
DEFINE('_HEADER_AUTHOR','Autor');
DEFINE('_HEADER_SUBMITTED','Absenden');
DEFINE('_HEADER_HITS','Hits');
DEFINE('_E_EDIT','Bearbeiten');
DEFINE('_E_ADD','Beifügen');
DEFINE('_E_WARNUSER','Bitte entweder Abbrechen oder Speichern der aktuellen Änderung');
DEFINE('_E_WARNTITLE','Inhaltseintrag muss einen Titel haben');
DEFINE('_E_WARNTEXT','Inhaltseintrag muss Intro-Text haben');
DEFINE('_E_WARNCAT','Bitte wählen Sie eine Kategorie');
DEFINE('_E_CONTENT','Inhalt');
DEFINE('_E_TITLE','Titel:');
DEFINE('_E_CATEGORY','Kategorie:');
DEFINE('_E_INTRO','Intro-Text');
DEFINE('_E_MAIN','Haupttext');
DEFINE('_E_MOSIMAGE','EINFÜGEN {mosimage}');
DEFINE('_E_IMAGES','Bilder');
DEFINE('_E_GALLERY_IMAGES','Galeriebilder');
DEFINE('_E_CONTENT_IMAGES','Inhaltbilder');
DEFINE('_E_EDIT_IMAGE','Bild bearbeiten');
DEFINE('_E_NO_IMAGE','Kein Bild');
DEFINE('_E_INSERT','Einfügen');
DEFINE('_E_UP','Nach oben');
DEFINE('_E_DOWN','Nach unten');
DEFINE('_E_REMOVE','Entfernen');
DEFINE('_E_SOURCE','Quelle:');
DEFINE('_E_ALIGN','Richten:');
DEFINE('_E_ALT','Alternativtext:');
DEFINE('_E_BORDER','Rahmen:');
DEFINE('_E_CAPTION','Beschriftung');
DEFINE('_E_CAPTION_POSITION','Beschriftungsposition');
DEFINE('_E_CAPTION_ALIGN','Beschriftungsausrichtung');
DEFINE('_E_CAPTION_WIDTH','Beschriftungsbreite');
DEFINE('_E_APPLY','Anwenden');
DEFINE('_E_PUBLISHING','Veröffentlichen');
DEFINE('_E_STATE','Zustand:');
DEFINE('_E_AUTHOR_ALIAS','Autorsalias:');
DEFINE('_E_ACCESS_LEVEL','Zugangslevel:');
DEFINE('_E_ORDERING','Reihenfolge:');
DEFINE('_E_START_PUB','Starte Veröffentlichung:');
DEFINE('_E_FINISH_PUB','Veröffentlichung abschließen:');
DEFINE('_E_SHOW_FP','Auf der Startseite zeigen:');
DEFINE('_E_HIDE_TITLE','Artikel Titel verbergen');
DEFINE('_E_METADATA','Metadaten');
DEFINE('_E_M_DESC','Beschreibung:');
DEFINE('_E_M_KEY','Stichwörter');
DEFINE('_E_SUBJECT','Betreff:');
DEFINE('_E_EXPIRES','Gültig bis:');
DEFINE('_E_VERSION','Version:');
DEFINE('_E_ABOUT','Über:');
DEFINE('_E_CREATED','Erstellt:');
DEFINE('_E_LAST_MOD','Zuletzt geändert:');
DEFINE('_E_HITS','Hits:');
DEFINE('_E_SAVE','Speichern');
DEFINE('_E_CANCEL','Abbrechen');
DEFINE('_E_REGISTERED','Nur registrierte Benutzer');
DEFINE('_E_ITEM_INFO','Artikel Information');
DEFINE('_E_ITEM_SAVED','Artikel erfolgreich gespeichert.');
DEFINE('_ITEM_PREVIOUS','&lt; Zurück ');
DEFINE('_ITEM_NEXT','Weiter &gt;');
DEFINE('_KEY_NOT_FOUND','Key nicht gefunden');


/** content.php */
DEFINE('_SECTION_ARCHIVE_EMPTY','Momentan keine archivierten Einträge für diese Sektion, bitte kommen Sie später wieder.');
DEFINE('_CATEGORY_ARCHIVE_EMPTY','Momentan keine archivierten Einträge für diese Kategorie, bitte kommen Sie später wieder.');
DEFINE('_HEADER_SECTION_ARCHIVE','Sektionsarchiv');
DEFINE('_HEADER_CATEGORY_ARCHIVE','Kategorienarchiv');
DEFINE('_ARCHIVE_SEARCH_FAILURE','Es gibt keine archivierten Einträge für %s %s');	// Werte sind Monat dann Jahr 
DEFINE('_ARCHIVE_SEARCH_SUCCESS','Hier sind die archivierten Einträge für %s %s');	// Werte sind Monat dann Jahr
DEFINE('_FILTER','Filter');
DEFINE('_ORDER_DROPDOWN_DA','Datum aufsteigend');
DEFINE('_ORDER_DROPDOWN_DD','Datum absteigend');
DEFINE('_ORDER_DROPDOWN_TA','Titel aufsteigend');
DEFINE('_ORDER_DROPDOWN_TD','Titel absteigend');
DEFINE('_ORDER_DROPDOWN_HA','Hits aufsteigend');
DEFINE('_ORDER_DROPDOWN_HD','Hits absteigend');
DEFINE('_ORDER_DROPDOWN_AUA','Autor aufsteigend');
DEFINE('_ORDER_DROPDOWN_AUD','Autor absteigend');
DEFINE('_ORDER_DROPDOWN_O','Sortieren');

/** poll.php */
DEFINE('_ALERT_ENABLED','Cookies müssen aktiviert sein!');
DEFINE('_ALREADY_VOTE','Sie haben heute bereits für diesen Artikel gestimmt.');
DEFINE('_NO_SELECTION','Keine Auswahl getroffen, bitte wieder probieren.');
DEFINE('_THANKS','Danke für Ihre Stimme!');
DEFINE('_SELECT_POLL','Umfrage aus der Liste wählen');

/** classes/html/poll.php */
DEFINE('_JAN','Januar');
DEFINE('_FEB','Februar');
DEFINE('_MAR','März');
DEFINE('_APR','April');
DEFINE('_MAY','Mai');
DEFINE('_JUN','Junie');
DEFINE('_JUL','Julie');
DEFINE('_AUG','August');
DEFINE('_SEP','September');
DEFINE('_OCT','Oktober');
DEFINE('_NOV','November');
DEFINE('_DEC','Dezember');
DEFINE('_POLL_TITLE','Umfrageresultate');
DEFINE('_SURVEY_TITLE','Umfragetitel:');
DEFINE('_NUM_VOTERS','Anzahl der Stimmen');
DEFINE('_FIRST_VOTE','Erste Stimme');
DEFINE('_LAST_VOTE','Letzte Stimme');
DEFINE('_SEL_POLL','Umfrage wählen:');
DEFINE('_NO_RESULTS','Keine Ergebnisse für diese Umfrage.');

/** registration.php */
DEFINE('_ERROR_PASS','Leider kein entsprechender Benutzer gefunden');
DEFINE('_NEWPASS_MSG','Das Benutzerkonto $checkusername ist mit dieser Mail-Adresse verbunden.\n'
.'Ein Web Nutzer von $mosConfig_live_site hat gerade ein neues Passwort angefragt.\n\n'
.' Ihr Nutzername ist: $username\n'
.' Ihr neues Passwort ist: $newpass\n\n'
.' Wenn dies ein Fehler war, bitte einloggen mit '
.' neuem Passwort und dann Passwort nach Vorzug verändern.');
DEFINE('_NEWPASS_SUB','$_Sitename :: Neues Passwort für  - $Nutzername');
DEFINE('_NEWPASS_SENT','Neues Nutzer Passwort erstellt und gesendet!');
DEFINE('_REGWARN_NAME','Bitte Namen eingeben.');
DEFINE('_REGWARN_UNAME','Bitte einen Nutzernamen eingeben.');
DEFINE('_REGWARN_MAIL','Bitte eine gültige Mail-Adresse eingeben.');
DEFINE('_REGWARN_PASS','Bitte ein gültiges Passwort eingeben.  Keine Leerstellen, mehr als 6 Charaktere und enthaltend 0-9,a-z,A-Z');
DEFINE('_REGWARN_VPASS1','Bitte Passwort bestätigen');
DEFINE('_REGWARN_VPASS2','Passwort und Bestätigung stimmen nicht überein, bitte wieder versuchen.');
DEFINE('_REGWARN_INUSE','Nutzername/Passwort bereits im Gebrauch. Bitte versuchen Sie ein anderes.');
DEFINE('_REGWARN_EMAIL_INUSE', 'Diese Mail-Adresse ist bereits registriert. Wenn Sie Ihr Passwort vergessen haben, auf "Passwort vergessen?" klicken und ein neues Passwort wird Ihnen gesendet.');
DEFINE('_SEND_SUB','Konto Einzelheiten %s bei %s');
DEFINE('_USEND_MSG_ACTIVATE', 'Hallo %s,

Danke für Ihre Registrierung bei %s. Ihr Konto wurde erstellt und muss aktiviert werden, bevor Sie es verwenden können.
Um das Konto zu aktivieren, klicken Sie auf diesen Link oder Kopieren-Einfügen in Ihren Browser:
%s

Nach dem Aktivieren können Sie einloggen zu %s mit dem volgenden Nutzername und Passwort:

Nutzername - %s
Passwort - %s');
DEFINE('_USEND_MSG', "Hallo %s,

Danke für Ihre Registrierung bei %s.

Sie können nun einloggen bei %s mit dem Nutzername und Passwort mit denen Sie registriert haben.");
DEFINE('_USEND_MSG_NOPASS','HAllo $Name,\n\nSie wurden als Nutzer hinzugefügt bei $mosConfig_live_site.\n'
.'Sie können nun einloggen bei $mosConfig_live_site mit dem Nutzername und Passwort mit denen Sie registriert haben.\n\n'
.'Bitte nicht diese Nachricht beantworten. Sie ist automatisch erzeugt und ist nur für Informationszwecke\n');
DEFINE('_ASEND_MSG','Hallo %s,

Ein neuer Nutzer wurde registriert bei %s.
Diese Mail enthält Ihre Anmeldedaten:

Name - %s
Mail - %s
Nutzername - %s

Bitte nicht diese Nachricht beantworten. Sie ist automatisch erzeugt und ist nur für Informationszwecke');
DEFINE('_REG_COMPLETE_NOPASS','<div class="componentheading">Anmeldung vollständig!</div><br />&nbsp;&nbsp;'
.'Sie können jetzt einloggen<br />&nbsp;&nbsp;');
DEFINE('_REG_COMPLETE', '<div class="componentheading">Anmeldung vollständig!</div><br />Sie können jetzt einloggen.');
DEFINE('_REG_COMPLETE_ACTIVATE', '<div class="componentheading">Anmeldung vollständig!</div><br />Ihr Konto wurde erstellt und ein Aktivierungs-Link wurde an Ihre Mail-Adresse gesendet. Bitte das Konto aktivieren wenn Sie die Mail bekommen, durch einen Klick auf den Aktivierungs-Link, bevor Sie einloggen können.');
DEFINE('_REG_ACTIVATE_COMPLETE', '<div class="componentheading">Activation vollständig!</div><br />Ihr Konto wurde erfolgreich aktiviert. Sie können nun einloggen mit dem Nutzernamen und Passwort, die Sie bei der Anmeldung wählten.');
DEFINE('_REG_ACTIVATE_NOT_FOUND', '<div class="componentheading">Ungültiger Aktivierungslink!</div><br />Es gibt kein solches Konto in unserer Datenbank oder das Konto wurde bereits aktiviert.');
DEFINE('_REG_ACTIVATE_FAILURE', '<div class="componentheading">Aktivierung fehlgeschlagen!</div><br />Das System konnte Ihr Konto nicht aktivieren, wenden Sie sich bitte den Website-Administrator.');

/** classes/html/registration.php */
DEFINE('_PROMPT_PASSWORD','Passwort vergessen?');
DEFINE('_NEW_PASS_DESC','Bitte Ihre Mail-Adresse eingeben, dann auf den Passwort senden Knopf klicken.<br />'
.'Sie erhalten bald ein neues Passwort. Verwende dieses Passwort für Website Zugang.');
DEFINE('_NEWPASS_MULTIPLE','Wenn Sie mehrere Konten mit derselben Mail-Adresse registriert haben, dann erhalten ein neues Passwort für <strong>jedes</strong> Konto.');
DEFINE('_PROMPT_UNAME','Nutzername:');
DEFINE('_PROMPT_EMAIL','Mail-Adresse:');
DEFINE('_BUTTON_SEND_PASS','Passwort senden');
DEFINE('_REGISTER_TITLE','Anmeldung');
DEFINE('_REGISTER_NAME','Name:');
DEFINE('_REGISTER_UNAME','Nutzername:');
DEFINE('_REGISTER_EMAIL','Mail:');
DEFINE('_REGISTER_PASS','Passwort:');
DEFINE('_REGISTER_VPASS','Passwort bestätigen:');
DEFINE('_REGISTER_REQUIRED','Mit einem Sternchen (*) gekennzeichnete Felder sind Pflichtfelder.');
DEFINE('_BUTTON_SEND_REG','Anmeldung absenden');
DEFINE('_SENDING_PASSWORD','Ihr Passwort wird an die angegebene Mail-Adresse gesendet.<br />Sobald Sie Ihr '
.' neues Passwort erhalten haben, können Sie einloggen und es verändern.');

/** classes/html/search.php */
DEFINE('_SEARCH_TITLE','Suche');
DEFINE('_PROMPT_KEYWORD','Suchwörter');
DEFINE('_SEARCH_MATCHES','%d Übereinstimmungen zurückgekehrt');
DEFINE('_CONCLUSION','Total $totalRows Ergebnisse.  Suche nach [ <b>$Suchwort</b> ] mit');
DEFINE('_NOKEYWORD','Keine Ergebnisse gefunden');
DEFINE('_IGNOREKEYWORD','Ein oder mehrere häufig vorkommende Wörter wurden bei der Suche ignoriert');
DEFINE('_SEARCH_ANYWORDS','Jedes Wort');
DEFINE('_SEARCH_ALLWORDS','Alle Worte');
DEFINE('_SEARCH_PHRASE','Genauer Begriff');
DEFINE('_SEARCH_NEWEST','Neuestes zuerst');
DEFINE('_SEARCH_OLDEST','Ältestes zuerst');
DEFINE('_SEARCH_POPULAR','Populärste');
DEFINE('_SEARCH_ALPHABETICAL','Alphabetisch');
DEFINE('_SEARCH_CATEGORY','Sektion/Kategorie');
DEFINE('_SEARCH_MESSAGE','Suchbegriff muss mindestens 3 Zeichen und maximal 20 Zeichen lang sein');
DEFINE('_SEARCH_ARCHIVED','Archiviert');
DEFINE('_SEARCH_CATBLOG','Kategorienblog');
DEFINE('_SEARCH_CATLIST','Kategorienliste');
DEFINE('_SEARCH_NEWSFEEDS','Newsfeeds	');
DEFINE('_SEARCH_SECLIST','Sektionenliste');
DEFINE('_SEARCH_SECBLOG','Sektionenblog');


/** templates/*.php */
DEFINE('_ISO','charset=utf-8');
DEFINE('_DATE_FORMAT','l, F d Y');  //Uses PHP's DATE Command Format - Depreciated
/**
* Ändern Sie diese Zeile um zu reflektieren, wie das Datum auf Ihrer Website aussehen soll
*
*e.g. DEFINE("_DATE_FORMAT_LC","%A, %d %B %Y %H:%M"); //Uses PHP's strftime Command Format
*/
DEFINE('_DATE_FORMAT_LC',"%A, %d %B %Y"); //Uses PHP's strftime Command Format*/
DEFINE('_DATE_FORMAT_LC2',"%A, %d %B %Y %H:%M");
DEFINE('_SEARCH_BOX','Suche...');
DEFINE('_NEWSFLASH_BOX','Newsflash!');
DEFINE('_MAINMENU_BOX','Hauptmenü');

/** classes/html/usermenu.php */
DEFINE('_UMENU_TITLE','Nutzermenü');
DEFINE('_HI','Hi, ');

/** user.php */
DEFINE('_SAVE_ERR','Bitte alle Felder ausfüllen.');
DEFINE('_THANK_SUB','Vielen Dank für Ihren Beitrag. Ihre Einsendung wird jetzt bewertet, bevor sie auf der Website erscheint.');
DEFINE('_THANK_SUB_PUB','Vielen Dank für Ihren Beitrag.');
DEFINE('_UP_SIZE','Sie können keine Dateien größer als 15kb hochladen.');
DEFINE('_UP_EXISTS','Bild $userfile_name bereits vorhanden. Bitte Datei umbenennen und wieder versuchen.');
DEFINE('_UP_COPY_FAIL','Fehler beim Kopieren');
DEFINE('_UP_TYPE_WARN','Sie dürfen nur eine GIF oder JPG Bild uploaden.');
DEFINE('_MAIL_SUB','Nutzer eingereicht');
DEFINE('_MAIL_MSG','Hallo $AdminName,\n\n\nA Nutzer eingereicht $Typ:\n [ $Titel ]\n wurde gerade eingereicht von Nutzer:\n [ $Autor ]\n'
.' für $mosConfig_live_site.\n\n\n\n'
.'Bitte gehen Sie zu $mosConfig_live_site/administrator zum Sehen und Genehmigen des $Typen.\n\n'
.'Bitte nicht auf diese Nachricht antworten, da sie automatisch generiert und nur für Informationszwecke ist.\n');
DEFINE('_PASS_VERR1','Wenn Sie Ihr Passwort verändern, bitte Passwort zum überprüfen erneut eingeben.');
DEFINE('_PASS_VERR2','Wenn Sie Ihr Passwort verändern, bitte sicherstellen daβ Passwort und Bestätigung übereinstimmen.');
DEFINE('_UNAME_INUSE','Nutzername bereits verwendet');
DEFINE('_UPDATE','Aktualisierung');
DEFINE('_USER_DETAILS_SAVE','Ihre Einstellungen wurden gespeichert');
DEFINE('_USER_LOGIN','Nutzer Login');

/** components/com_user */
DEFINE('_EDIT_TITLE','Daten bearbeiten');
DEFINE('_YOUR_NAME','Ihr Name:');
DEFINE('_EMAIL','Mail:');
DEFINE('_UNAME','Nutzername:');
DEFINE('_PASS','Passwort:');
DEFINE('_VPASS','Passwort bestätigen:');
DEFINE('_SUBMIT_SUCCESS','Einreichung erfolgreich!');
DEFINE('_SUBMIT_SUCCESS_DESC','Ihr Artikel wurde erfolgreich zu unseren Administratoren eingereicht. Es wird bewertet bevor es auf dieser Website veröffentlicht wird.');
DEFINE('_WELCOME','Wilkommen!');
DEFINE('_WELCOME_DESC','Willkommen zu der Nutzersektion unserer Website');
DEFINE('_CONF_CHECKED_IN','Ausgecheckte Artikel sind nun alle eingecheckt');
DEFINE('_CHECK_TABLE','Tabelle überprüfen');
DEFINE('_CHECKED_IN','Eingecheckt');
DEFINE('_CHECKED_IN_ITEMS',' Artikel');
DEFINE('_PASS_MATCH','Passwörter nicht identisch');

/** components/com_banners */
DEFINE('_BNR_CLIENT_NAME','Sie müssen einen Namen für den Kunden wählen.');
DEFINE('_BNR_CONTACT','Sie müssen einen Kontakt für den Kunden wählen.');
DEFINE('_BNR_VALID_EMAIL','Sie müssen eine gültige Mail-Adresse für den Kunden wählen.');
DEFINE('_BNR_CLIENT','Sie müssen einen Kunden wählen,');
DEFINE('_BNR_NAME','Sie müssen einen Namen für die Banner wählen.');
DEFINE('_BNR_IMAGE','Sie müssen ein Bild für die Banner wählen.');
DEFINE('_BNR_URL','Sie müssen einen URL/Benutzerdefinierten Banner Code für die Banner wählen.');

/** components/com_login */
DEFINE('_ALREADY_LOGIN','Sie sind schon eingeloggt!');
DEFINE('_LOGOUT','Hier klicken zum ausloggen');
DEFINE('_LOGIN_TEXT','Gegenüberliegende Login und Passwort Felder benutzen, um vollen Zugang zu erhalten.');
DEFINE('_LOGIN_SUCCESS','Ihr Login war erfolgreich');
DEFINE('_LOGOUT_SUCCESS','Ihr Logout war erfolgreich');
DEFINE('_LOGIN_DESCRIPTION','Um den privaten Bereich der Website zuzugreifen, bitte einloggen');
DEFINE('_LOGOUT_DESCRIPTION','Sie sind momentan in den privaten Bereich dieser Website eingeloggt');


/** components/com_weblinks */
DEFINE('_WEBLINKS_TITLE','Weblinks');
DEFINE('_WEBLINKS_DESC','Wir sind regelmäßig auf dem Internet. Wenn wir eine interessante Site finden, listen wir sie hier für Ihren Genuß.  <br />Wählen Sie zuerst ein Weblink Thema von der Liste unten, dann ein URL auswählen.');
DEFINE('_HEADER_TITLE_WEBLINKS','Weblink');
DEFINE('_SECTION','Sektion:');
DEFINE('_SUBMIT_LINK','Ein Weblink absenden');
DEFINE('_URL','URL:');
DEFINE('_URL_DESC','Beschreibung:');
DEFINE('_NAME','Name:');
DEFINE('_WEBLINK_EXIST','Ein Weblink mit diesem Namen existiert schon, bitte wieder versuchen.');
DEFINE('_WEBLINK_TITLE','Ihr Weblink muss einen Titel enthalten.');

/** components/com_newfeeds */
DEFINE('_FEED_NAME','Feed-Name');
DEFINE('_FEED_ARTICLES','# Artikel');
DEFINE('_FEED_LINK','Feed -Link');

/** whos_online.php */
DEFINE('_WE_HAVE', 'Wir haben ');
DEFINE('_AND', ' und ');
DEFINE('_GUEST_COUNT','%s Gast');
DEFINE('_GUESTS_COUNT','%s Gäste');
DEFINE('_MEMBER_COUNT','%s Member');
DEFINE('_MEMBERS_COUNT','%s Member');
DEFINE('_ONLINE',' online');
DEFINE('_NONE','Keine Nutzer Online');

/** modules/mod_banners */
DEFINE('_BANNER_ALT','Werbung');

/** modules/mod_random_image */
DEFINE('_NO_IMAGES','Keine Bilder');

/** modules/mod_stats.php */
DEFINE('_TIME_STAT','Zeit');
DEFINE('_MEMBERS_STAT','Member');
DEFINE('_HITS_STAT','Hits');
DEFINE('_NEWS_STAT','News');
DEFINE('_LINKS_STAT','Weblinks');
DEFINE('_VISITORS','Besucher');

/** /adminstrator/components/com_menus/admin.menus.html.php */
DEFINE('_MAINMENU_HOME','* Der erste veröffentlichte Artikel in diesem Menu [Hauptmenu] ist die default `Startseite` für die Website *');
DEFINE('_MAINMENU_DEL','* Sie können dieses Menü nicht `löschen`, weil es für den ordnungsgemäßen Betrieb benötigt wird! *');
DEFINE('_MENU_GROUP','* Manche `Menutypen` erscheinen in mehr als einer Gruppe *');

/** hp advanced search */
DEFINE('_ADVANCED_SEARCH','Erweiterte Suche');
DEFINE('_HP_QUICK_SEARCH','Schnelle Immobiliensuche');

/** hp asearch */
DEFINE('_ASEARCH_TITLE','Verfügbarkeitssuche');

/** currency convertor */
DEFINE('_CURRENCY_CONVERTOR','Währungsumrechner');


/** administrators/components/com_users */
DEFINE('_NEW_USER_MESSAGE_SUBJECT', 'Neue Nutzerdetails' );
DEFINE('_NEW_USER_MESSAGE', 'Hallo %s,


Sie wurden als Nutzer auf %s hinzugefügt durhc einen Administrator.

Diese Mail enthält Ihren Nutzernamen und Ihr Passwort, um in %s einzuloggen

Nutzername - %s
Passwort - %s


Bitte nicht auf diese Nachricht antworten weil sie automatisch erzeugt und nur fur Informationszwecke ist');

/** administrators/components/com_massmail */
DEFINE('_MASSMAIL_MESSAGE', "Dies ist eine Mail von '%s'

Nachricht:
" );


/** includes/pdf.php */
DEFINE('_PDF_GENERATED','Erzeugt:');
DEFINE('_PDF_POWERED','Angetrieben durch Joomla!');

/** modules **/

/** Currency converter **/

DEFINE('_CURRENCY_CHANGE','Währung ändern');


/** Dates */
// Days of week
DEFINE('_DATE_MONDAY', 'Montag');
DEFINE('_DATE_TUESDAY', 'Dienstag');
DEFINE('_DATE_WEDNESDAY', 'Mittwoch');
DEFINE('_DATE_THURSDAY', 'Donnerstag');
DEFINE('_DATE_FRIDAY', 'Freitag');
DEFINE('_DATE_SATURDAY', 'Samstag');
DEFINE('_DATE_SUNDAY', 'Sonntag');

// Months
DEFINE('_DATE_JANUARY', 'Januar');
DEFINE('_DATE_FEBRUARY', 'Februar');
DEFINE('_DATE_MARCH', 'März');
DEFINE('_DATE_APRIL', 'April');
DEFINE('_DATE_MAY', 'Mai');
DEFINE('_DATE_JUNE', 'Junie');
DEFINE('_DATE_JULY', 'Julie');
DEFINE('_DATE_AUGUST', 'August');
DEFINE('_DATE_SEPTEMBER', 'September');
DEFINE('_DATE_OCTOBER', 'Oktober');
DEFINE('_DATE_NOVEMBER', 'November');
DEFINE('_DATE_DECEMBER', 'Dezember');

/** availability page **/
DEFINE('_AVAILABILITY_CALENDAR_TITLE', 'Belegungskalender für');

