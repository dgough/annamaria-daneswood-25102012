<?php
/**
* @version $Id: mod_banners.php 6085 2006-12-24 18:59:57Z robs $
* @package Joomla
* @copyright Copyright (C) 2005 Open Source Matters. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
global $mosConfig_absolute_path, $modulename, $mosConfig_lang, $VM_LANG, $mosConfig_sef, $page, $vars, $option;

$ucode = $params->get( 'ucode', 0 );
$udomain = $params->get( 'udomain', 0 );
$upages = $params->get( 'upages', 0 );

$page = isset($page) ? $page : '';
$pagename = '';

if( $option=='com_virtuemart' && file_exists($mosConfig_absolute_path.'/administrator/components/com_virtuemart/virtuemart.cfg.php')){
	include_once($mosConfig_absolute_path.'/administrator/components/com_virtuemart/virtuemart.cfg.php');
	include_once($mosConfig_absolute_path.'/components/com_virtuemart/virtuemart_parser.php');	
	// The abstract language class
	require_once( CLASSPATH."language.class.php" );
	
	$current_stage = ps_checkout::get_current_stage();
	$checkout_steps = ps_checkout::get_checkout_steps();	
	$step = $checkout_steps[$current_stage];
	
	switch ( $step[0] ) {
		case 'CHECK_OUT_GET_SHIPPING_ADDR':
			$step_msg = '_PHPSHOP_ADD_SHIPTO_2';
			break;
		case 'CHECK_OUT_GET_SHIPPING_METHOD':
			$step_msg = '_PHPSHOP_ISSHIP_LIST_CARRIER_LBL';
			break;
		case 'CHECK_OUT_GET_PAYMENT_METHOD':
			$step_msg = '_PHPSHOP_ORDER_PRINT_PAYMENT_LBL';
			break;
		case 'CHECK_OUT_GET_FINAL_CONFIRMATION':
			$step_msg = '_PHPSHOP_CHECKOUT_CONF_PAYINFO_COMPORDER';
			break;
	}	
	
	/* 
	 * assign confirmation page to use for _trackPageview
	*/
	
	$pages = explode(",", $upages);
	if($page == 'checkout.index') $pagename = '/checkout/'.'step-'.$current_stage.'-of-'.count($checkout_steps).'-'.strtolower(str_replace(' ','-',(isset($VM_LANG->$step_msg) ? $VM_LANG->$step_msg : @$VM_LANG->_($step_msg))));
	if( in_array($page,$pages) ) $pagename = '/checkout/thankyou';
}

?>

<script type="text/javascript">
//<!--
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
//-->
</script>
<script type="text/javascript">
//<!-- 
var pageTracker = _gat._getTracker("<?php echo $ucode ?>")
pageTracker._initData()
<?php if ($udomain) echo "pageTracker._setDomainName('$udomain');\n"; ?>
pageTracker._trackPageview(<?php echo $pagename ? "'$pagename'" : '' ?>);

<?php
//Load thankyou page tracking
if( $option=='com_virtuemart' && in_array($page,$pages) ) {

	$db = new ps_DB();
	$dbo = new ps_DB();
	$dbu = new ps_DB();
	$dbp = new ps_DB();
	$dbc = new ps_DB();
	$order_id = $db->getEscaped( vmGet($_REQUEST, 'order_id', vmGet($vars, 'order_id' )) );
	
	//Retrieve the user info
	$dbu->query("SELECT * FROM #__{vm}_order_user_info WHERE order_id = $order_id AND address_type = 'BT'");
	$dbu->next_record();
	
	//Retrieve the order from the DB
	$dbo->query("SELECT * FROM #__{vm}_orders WHERE order_id = $order_id");
	$dbo->next_record();
	if($dbo->num_rows()){
			
	?>
	pageTracker._addTrans(
		"<?php echo $order_id ?>",						// Order ID
		"",												// Affiliation
		"<?php $dbo->p('order_total') ?>",				// Total
		"<?php $dbo->p('order_tax') ?>",				// Tax
		"<?php $dbo->p('order_shipping') ?>",			// Shipping
		"<?php echo addslashes($dbu->f('city')) ?>",	// City
		"<?php echo addslashes($dbu->f('state')) ?>",	// State
		"<?php echo addslashes($dbu->p('country')) ?>"	// Country
		);
	
		<?php  	
		//Retrieve the product info
	$dbp->query("SELECT * FROM #__{vm}_order_item WHERE order_id = $order_id");
	while($dbp->next_record()){
		
		$product_id = $dbp->f('product_id');
		
		//Get the category
		$dbc->query("SELECT c.category_name FROM #__{vm}_product_category_xref AS x 
					LEFT JOIN #__{vm}_category AS c ON c.category_id = x.category_id 
					WHERE x.product_id = '$product_id' LIMIT 1");
		$dbc->next_record();	
		
	  	?>
		pageTracker._addItem(
		    "<?php echo $product_id ?>",       							// Item Number
		    "<?php $dbp->p('order_item_sku') ?>",       				// SKU
		    "<?php echo addslashes($dbp->f('order_item_name')) ?>",     // Product Name 
		    "<?php echo addslashes($dbc->f('category_name')) ?>",  		// Category
		    "<?php $dbp->p('product_final_price') ?>",  				// Price
		    "<?php $dbp->p('product_quantity') ?>"						// Quantity
	    );
	    <?php 
	}
	?>
	pageTracker._trackTrans();
	<?php
	}
}
?>
//-->
</script>