<?php
/**
* @version $Id: im_addthis_bookmark.php 1.5
* @copyright Information Madness.
* @license GNU/GPL,
* @author Information Madness - http://www.informationmadness.com/cms
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.event.plugin');

class plgContentAddThisBookMark extends JPlugin{

	//This is just a constructor
	function plgContentAddThisBookMark( &$subject ){
		parent::__construct($subject);

        // load plugin parameters
        $this->_plugin = JPluginHelper::getPlugin( 'content', 'addthisbookmark' );
        $this->_params = new JParameter( $this->_plugin->params );
		$this->_beforeOrAfter = $this->_params->get('beforeOrAfter','before');
		$this->_alignment = $this->_params->get('alignment','right');
		$this->_button_type = $this->_params->get('button_type','http://s7.addthis.com/static/btn/sm-plus.gif');
		$this->_cust_image = $this->_params->get('custImage','#');
		$this->_pub_id = $this->_params->get('pub_id');
		$this->_widget_style = $this->_params->get( 'widget_style', 'animated' );
		$this->_showCredit = $this->_params->get( 'showCredit', 'yes' );
		$this->_showOnFront = $this->_params->get( 'showOnFront', 'no' );
	}

	//This function is being called on prepare content
	function onPrepareContent( &$article, &$params, $limitstart){
		global $mainframe;
		
//		if($this->isFrontPage() && $this->_showOnFront == "no"){
	    if(JRequest::getCmd('view', '') != 'article'){
			//don't do anything.
			return '';
		}else{

			$outputValue = "<div style='float:" . $this->_alignment . "'>";

			if($this->_showCredit == "yes"){
				$outputValue .= "<div style='float:left;'><a href='http://www.informationmadness.com/cms' target='_blank'>!</a></div>";
			}

			$outputValue .="<!-- AddThis Bookmark Button BEGIN -->\r\n";
			$outputValue .= "<a class=\"addthis_button\" href=\"http://www.addthis.com/bookmark.php?v=250&amp;username=" . $this->_pub_id . "\" onclick=\"return addthis_open(this,'', '" . $this->plgGetPageUrl($article) . "', '" . $article->title . "')\" onmouseout=\"addthis_close()\" onclick=\"return addthis_sendto()\">";
			if($this->_button_type == 'custom'){
			   $outputValue .= "<img src='" . $this->_cust_image ."' border='0' alt='Bookmark and Share' />";		
			}else{
			$outputValue .= "<img src='" . $this->_button_type ."' border='0' alt='Bookmark and Share' />";
			}
			$outputValue .= "</a><script type='text/javascript' src='http://s7.addthis.com/js/250/addthis_widget.js#username=" . $this->_pub_id . "'></script>\r\n";
			$outputValue .= "<!-- AddThis Bookmark Button END -->";
			$outputValue .= "</div>";

			if($this->_beforeOrAfter == "before"){
				$article->text = $outputValue . $article->text;
			}else{
				$article->text .= $outputValue;
			}
		}
	}

	function plgGetPageUrl(&$obj)
	{

		if (!is_null($obj)) 
		{
			require_once( JPATH_SITE.DS.'components'.DS.'com_content'.DS.'helpers'.DS.'route.php' );
			$url = JRoute::_(ContentHelperRoute::getArticleRoute($obj->slug, $obj->catslug, $obj->sectionid));
			$uri =& JURI::getInstance();
				$base  = $uri->toString( array('scheme', 'host', 'port'));
			$url = $base . $url;
			$url = JRoute::_($url, true, 0);
			return $url;
		}
	}

	function isFrontPage()
	{
		$menu = & JSite::getMenu();
		if ($menu->getActive() == $menu->getDefault()) {
			return true;
		}
		return false;
	}

/*	function getButtonImage($name){
			$buttonImage = 'button0-bm.gif';
			switch($name){
				case 'small':
					$buttonImage = 'button0-bm.gif';
					break;
				case 'medium':
					$buttonImage = 'button1-bm.gif';
					break;
				case 'large':
					$buttonImage = 'button2-bm.png';
					break;
				default:
					$buttonImage = 'button0-bm.gif';
					break;
			}

			return $buttonImage;
	}*/

}
?>