<?php
/**
 * This file is designed to restrict access to domains that are on siteincubator or dev
 * If a valid match is found, disallow /, else read in the original robots.txt
 */

$host = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '';

header('Content-Type: text/plain');

$force = isset($_GET['force']) ? $_GET['force'] : null;
$host = $force ? $force : $host;

$host = preg_replace('#^www\.#','',$host);

if(file_exists(dirname(__FILE__).'/robots.'.$host.'.txt')) echo file_get_contents(dirname(__FILE__).'/robots.'.$host.'.txt');
else echo file_get_contents(dirname(__FILE__).'/robots.txt');